//
//  MultipleBandResponse.h
//  HubCiti
//
//  Created by Nikitha on 9/20/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MultipleBandDetail.h"
#import "bottomButtonDO.h"
@protocol MultipleBandResponse;
@interface MultipleBandResponse : NSObject
@property(nonatomic,strong)NSString *responseCode;
@property(nonatomic,strong)NSString *responseText;
@property(nonatomic,strong) NSNumber *maxCnt;
@property(nonatomic,strong) NSNumber *nextPage;
@property(nonatomic, strong) NSNumber * bottomBtn;
@property(nonatomic, strong) NSNumber * mainMenuId;
@property(nonatomic, strong) NSNumber * maxRowNum;

@property(nonatomic,strong) NSArray <MultipleBandDetail> * retailerDetail;
@property(nonatomic,strong) NSArray <bottomButtonDO> *bottomBtnList;
@end



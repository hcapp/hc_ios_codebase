//
//  MultipleBandDetail.h
//  HubCiti
//
//  Created by Nikitha on 9/20/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol MultipleBandDetail;
@interface MultipleBandDetail : NSObject
@property(nonatomic,strong) NSString * bandName;
@property(nonatomic,strong) NSString * bandImgPath;
@property(nonatomic,strong) NSNumber * bandID;
@property(nonatomic,strong) NSString * sDate;
@property(nonatomic,strong) NSString * eDate;
@property(nonatomic,strong) NSString * sTime;
@property(nonatomic,strong) NSString * eTime;
@property(nonatomic,strong) NSString * bEvtName;
@property(nonatomic,strong) NSNumber * bEvtId;
@property(nonatomic,strong) NSNumber * rowNum;
@property(nonatomic,strong) NSString * stage;
@property(nonatomic,strong) NSString * catName;
@property(nonatomic,strong) NSNumber * bottomBtnId;

@end

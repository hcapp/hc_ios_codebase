//
//  UIAlertController+Rotation.m
//  HubCiti
//
//  Created by Ashika on 11/30/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "UIAlertController+Rotation.h"

@implementation UIAlertController (Rotation)
#pragma mark self rotate
-(BOOL) shouldAutorotate{
    
    return NO;
}


-(UIInterfaceOrientation) preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations

{
    
    return UIInterfaceOrientationMaskPortrait;
    
}
    
    
@end

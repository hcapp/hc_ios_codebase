//
//  ReportProblemViewController.h
//  HubCiti
//
//  Created by Bindu M on 8/25/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "CommonUtility.h"
@class AnyViewController;
@class EmailShareViewController;
@interface MakeRequestDO : NSObject{
    NSMutableArray *requestTableArray;
    NSString *descName;
    
}
@property (nonatomic, strong) NSMutableArray *requestTableArray;
@property (nonatomic, strong) NSString *descName;

@end

@interface ReportProblemViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,MFMessageComposeViewControllerDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>{
    WebRequestState iWebRequestState;
    CommonUtility *common;
    BOOL isRemember;
    UITableView *reportProblemTbl;
    NSMutableArray *reportDataArr;
    NSMutableArray *arrBottomBtnDO;
    NSMutableArray *arrBottomButtonViewContainer;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic,strong) NSMutableArray *arrBottomBtnDO;

@end

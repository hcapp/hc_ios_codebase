//
//  MakeRequestViewController.h
//  HubCiti
//
//  Created by Bindu M on 8/31/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "CommonUtility.h"
#import "SortAndFilter.h"
@class AnyViewController;
@class EmailShareViewController;
@class MakeRequestDetailViewController;
@interface MakeRequestViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,MFMessageComposeViewControllerDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>{
    
    WebRequestState iWebRequestState;
    CommonUtility *common;
    BOOL isRemember;
    NSMutableArray *makeReqArray;
    NSString *navTitle;
    UITableView *makReqTblView;
    NSNumber *typeID;
    NSMutableArray *arrBottomBtnDO;
    NSMutableArray *arrBottomButtonViewContainer;
    NSString *nextPageTitle;
    NSMutableString *descDetail;
    NSString* firstName, *lastName, *phone,*state,*zip,*city,*customerAddress;
    BOOL sortByAlphabet, sortByType, sortByName;
    
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic, strong) MakeRequestDetailViewController* MakeReqDetailObj;
@property (nonatomic, strong) NSMutableArray *makeReqArray;
@property (nonatomic, strong) NSString *navTitle;
@property (nonatomic,strong) NSMutableArray *arrBottomButtonDO;
@property (nonatomic, strong) NSNumber *typeID;
@property (nonatomic, strong) NSString *nextPageTitle;
@property (nonatomic, strong) NSMutableString *descDetail;
@property (nonatomic) BOOL sortByAlphabet;
@property (nonatomic) BOOL sortByType;
@property (nonatomic) BOOL sortByName;
//@property(nonatomic,retain) MakeRequestViewController *makeObj;
@property(nonatomic,retain) SortAndFilter *sortObj;

@end

//
// HotDealsList.h   
//  Scansee
//
//  Created by Dhananjaya C H on 22/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>
#include "GetUserInfoResponse.h"

@class RetailersListViewController;
@class FilterRetailersList;
@class CityExperienceViewController;
@class FundraiserListViewController;
@class FilterListViewController;
@class EventsListViewController;
@class FAQCategoryList;
@class AnyViewController;
@class EmailShareViewController;

BOOL maxCountFlag;

typedef enum dwebServicescalls{
    dgetHotDealProds,
    dhddetail,
    dgetfavloc,
    dgetuserinfo,
    dgetfavlocations
}dWebRequestType;

@interface dHotDealDO : NSObject{
    
    NSString *catId, *catName;
    NSString *apiPartnerId, *apiPartnerName;
    NSMutableArray *hotDealDetailsObjArray;//array of hot deal details objects
}
@property (nonatomic, strong)NSString *catId, *catName;
@property (nonatomic, strong)NSString *apiPartnerId, *apiPartnerName;
@property (nonatomic, strong)NSMutableArray *hotDealDetailsObjArray;
@end

@interface dHotDealDetailsDO : NSObject{
    
    NSString *hdName, *hdSDesc, *hdImgPath, *hdListId;
    NSString *hdId, *hdPrice, *hdSalePrice;
    NSString *hdUrl, *hdNewFlag;
}

@property (nonatomic, strong)NSString *hdName, *hdSDesc, *hdImgPath, *hdListId;
@property (nonatomic, strong)NSString *hdId, *hdPrice, *hdSalePrice;
@property (nonatomic, strong)NSString *hdUrl, *hdNewFlag;

@end

//@class ScanseeManager;

@interface DealHotDealsList : UIViewController<CLLocationManagerDelegate, UIPickerViewDelegate , UITableViewDelegate , UITableViewDataSource , UISearchBarDelegate,MFMessageComposeViewControllerDelegate,HTTPClientDelegate,CustomizedNavControllerDelegate> {

    UIViewController *mViewController;
    NSMutableArray *arrBottomButtonImages;
	IBOutlet UIView *modelView;
	IBOutlet UIPickerView *data_picker;
	IBOutlet UIView *customView;
	IBOutlet UISearchBar *m_searchBar;
    UITableView *hotDealTable;
    UITableView *dealTable;
    IBOutlet UIButton *m_Button;
    IBOutlet UIImageView *hotDealIconImg;
    IBOutlet UILabel *hotDealFoundTxt;
	IBOutlet UIButton *byCategory , *galleryBtn ,*preferenceBtn ;
	
	NSInteger favCat;
	NSMutableArray *categoryName ,*categoryIdHotdeals ;
    
    int lastvalueforLoop;  //@PaginationChange: Added Variable to maintain the pagination count

	//NSMutableArray *sectionArry;
	
	NSMutableArray *categoryArray , *categoryId;
	NSMutableString *categoryValue ;
	int categoryIdValue;
	
	int nextPage,lastVisitedRecord;
    NSIndexPath *index;
    
    dWebRequestType iWebRequestType;
    NSMutableArray *hotDealObjArray;//main array having hot deal objects
    NSMutableArray *nationDealObjArray;
    NSMutableArray *noneArray;
    NSString * searchStr;
    NSMutableString *dealType;
    int bottomBtn,dealTypeCount;
    CommonUtility *common;
    
    UIActivityIndicatorView *activityIndicator;
    
    WebRequestState iWebRequestState;
    NSMutableArray *arrEventBottomButtonDO;
    NSMutableArray *arrBottomButtonViewContainer;
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) FAQCategoryList *faqList;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,strong) FilterListViewController *filterList;
@property(nonatomic,strong) EventsListViewController *iEventsListViewController;
@property(nonatomic,strong) FundraiserListViewController *iFundraiserListViewController;
@property(nonatomic,strong) CityExperienceViewController *cityExp;
@property(nonatomic,strong) RetailersListViewController *retailerListScreen;
@property(nonatomic,strong) FilterRetailersList *pvc;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSMutableArray *locationMeasurements;
@property (nonatomic, strong) CLLocation *bestEffortAtLocation;
@property (nonatomic, strong) IBOutlet UIButton *m_Button;
@property (nonatomic, strong)UIViewController *mViewController;


@property (strong) UIButton *localButton;
@property (strong) UIButton *nationButton;

-(IBAction)doneClicked : (id) sender ;

-(void) removeAllObjectsinArray ;
//-(IBAction) categorybtnPressed : (id) sender ;
//-(IBAction) galleryPressed : (id) sender ;
//-(IBAction) bannerPressed : (id) sender ;
//-(IBAction) favCategories : (id) sender ;

-(void)showSplash;
- (void)hideSplash;
-(void)updateViewonViewWillAppear;
@end

//
//  DealsViewController.h
//  HubCiti
//
//  Created by deepak.agarwal on 1/22/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DealCouponsViewController.h"
#import "SpecialOfferRetailersViewController.h"
#import "DealHotDealsList.h"

@interface DealsViewController : UIViewController
{
    UIButton *btnCoupon;
    UIButton *btnHotDeal;
    UIButton *btnSpecials;
    
    IBOutlet UILabel *lblNonAvailable;
    
    NSMutableArray *arrBottomButtonImages;
    DealCouponsViewController *iCouponsViewController;
    DealHotDealsList *iHotDealsList;
    SpecialOfferRetailersViewController *iSpecialOfferRetailersViewController;
    
    BOOL isCouponAvailable;
    BOOL isHotDealAvailable;
    BOOL isSpecialAvailable;
    
    int selectedButtonNumber;
}
@end

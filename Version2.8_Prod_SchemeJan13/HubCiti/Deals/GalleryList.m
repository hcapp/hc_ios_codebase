//
//  GalleryList.m
//  Scansee
//
//  Created by Ajit on 6/20/13.
//
//

#import "GalleryList.h"
#import "MainMenuViewController.h"
#import "HubCitiManager.h"
#import "bottomButtonView.h"
#import "HotDealDetailViewController.h"
#import "GalleryDisplayViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@interface GalleryList (){
    UIActivityIndicatorView *loading;
}

@end

@implementation GalleryList
@synthesize locationTableView, productTableView,type;
//@synthesize btn1, btn2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    // Do any additional setup after loading the view from its nib.
    [super viewDidLoad];
    
    //set the navigation bar items
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    switch ([SharedManager typeIndex]) {
        case 0:
            self.title = @"Claimed";
            break;
            
        case 1:
            self.title = @"Expired";
            break;
            
        case 2:
            self.title = @"Used";
            break;
            
        default:
            self.title = @"Claimed";
            break;
    }
    
    //default value for response text
    responseText = @"No Records Found.";
    loc_lastVisitedRecord = 0;
    prod_lastVisitedRecord = 0;
    
    //set the back button
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    //set the main menu
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    
    
    //set the tableView frame
    locationTableView.tag = 100;
    productTableView.tag = 200;
    
    btn1Img = [[UIImageView alloc]init];
    btn2Img = [[UIImageView alloc]init];
    
    
    //section arrays allocation
    loc_retailerId = [[NSMutableArray alloc]init];
    loc_retailerName = [[NSMutableArray alloc]init];
    loc_retailerAddress = [[NSMutableArray alloc]init];
    loc_retLocId = [[NSMutableArray alloc]init];
    
    prod_categoryId = [[NSMutableArray alloc]init];
    prod_categoryName = [[NSMutableArray alloc]init];
    
    [SharedManager setRefreshGallery:NO];
    selectedTableViewTag = 0;
    
    viewSelectionControl.selectedSegmentIndex = [SharedManager segmentIndex];
    
    
    /*dispatch_group_t group = dispatch_group_create();
     
     dispatch_group_async(group,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
     // block1
     //NSlog(@"Block1");
     [self request_GallCoupByLoc:self.title];
     [NSThread sleepForTimeInterval:5.0];
     //NSlog(@"Block1 End");
     });
     
     
     dispatch_group_async(group,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
     // block2
     //NSlog(@"Block2");
     [self request_GallCoupByProd:self.title];
     [NSThread sleepForTimeInterval:8.0];
     //NSlog(@"Block2 End");
     });
     
     dispatch_group_notify(group,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
     // block3
     //NSlog(@"Block3");
     });
     
     dispatch_release(group);*/
    
    
    /*dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
     dispatch_group_t group = dispatch_group_create();
     
     // Add a task to the group
     dispatch_group_async(group, queue, ^{
     [self request_GallCoupByLoc:@"Claimed"];
     });
     
     // Add another task to the group
     dispatch_group_async(group, queue, ^{
     [self request_GallCoupByProd:self.title];
     });
     
     // Add a handler function for when the entire group completes
     // It's possible that this will happen immediately if the other methods have already finished
     dispatch_group_notify(group, queue, ^{
     [self methodFinish];
     });*/
    
    
    //create a new NSOperationQueue instance
    /*operationQueue = [NSOperationQueue new];
     
     //create a new NSOperation objectusing the NSInvocationOperation subclass
     NSInvocationOperation *invocationOperation1 = [[NSInvocationOperation alloc]initWithTarget:self selector:@selector(request_GallCoupByLoc:) object:nil];
     //[operationQueue addOperation:invocationOperation1];
     //[invocationOperation1 release];
     
     NSInvocationOperation *invocationOperation2 = [[NSInvocationOperation alloc]initWithTarget:self selector:@selector(request_GallCoupByProd:) object:nil];
     //[operationQueue addOperation:invocationOperation2];
     //[invocationOperation2 release];
     
     [operationQueue addOperations:@[invocationOperation1,invocationOperation2] waitUntilFinished:YES];*/
    
    if ([type isEqualToString:@"Coupons"]) {
        [self request_GallCoupByLoc:self.title];
        [self request_GallCoupByProd:self.title];
    }
    else{
        [self request_GallHDByLoc:self.title];
        [self request_GallHDByProd:self.title];
    }
    
    //[self showSplash];
    
    if ([loc_retailerId count]){
        [self btn1Tapped:nil];
        
    }
    
    else if ([prod_categoryId count]){
        [self btn2Tapped:nil];
        
    }
    
    else{
        [self btn1Tapped:nil];
    }
    
    [locationTableView reloadData];
    [productTableView reloadData];
    
}

//-(void)methodFinish{
//
//    DLog(@"Finished executing methods");
//}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    //selectedTableViewTag is used to track selected table cell, so as to refresh only that table on coming back after redeem
    if ([SharedManager refreshGallery]){
        DLog(@"%d",selectedTableViewTag);
        switch (viewSelectionControl.selectedSegmentIndex) {
            case 0:{//means coupons segment
                
                // If coming from Coupon Details Screen after Redeem
                if([SharedManager refreshGallery])
                {
                    [self removeObjectsOfCouponLocArrays];
                    loc_lastVisitedRecord = 0;
                    [self request_GallCoupByLoc:self.title];
                    
                    [self removeObjectsOfCouponProdArrays];
                    prod_lastVisitedRecord = 0;
                    [self request_GallCoupByProd:self.title];
                    
                    [productTableView reloadData];
                    [locationTableView reloadData];
                    [SharedManager setRefreshGallery:NO];
                }
                else
                {
                    switch (selectedTableViewTag)
                    {
                        case 100://location table
                            [self removeObjectsOfCouponLocArrays];
                            loc_lastVisitedRecord = 0;
                            [self request_GallCoupByLoc:self.title];
                            [locationTableView reloadData];
                            break;
                            
                        case 200://product table
                            [self removeObjectsOfCouponProdArrays];
                            prod_lastVisitedRecord = 0;
                            [self request_GallCoupByProd:self.title];
                            [productTableView reloadData];
                            break;
                            
                        default:
                            break;
                    }
                }
            }
                break;
                
            case 1:{//means hot deals segment
                
                // If coming from HotDeal Details Screen after Redeem
                if([SharedManager refreshGallery])
                {
                    [self removeObjectsOfHDLocArrays];
                    loc_lastVisitedRecord = 0;
                    [self request_GallHDByLoc:self.title];
                    
                    [self removeObjectsOfHDProdArrays];
                    prod_lastVisitedRecord = 0;
                    [self request_GallHDByProd:self.title];
                    
                    [productTableView reloadData];
                    [locationTableView reloadData];
                    [SharedManager setRefreshGallery:NO];
                }
                else
                {
                    switch (selectedTableViewTag) {
                        case 100://location table
                            [self removeObjectsOfHDLocArrays];
                            loc_lastVisitedRecord = 0;
                            [self request_GallHDByLoc:self.title];
                            [locationTableView reloadData];
                            break;
                            
                        case 200://product table
                            [self removeObjectsOfHDProdArrays];
                            prod_lastVisitedRecord = 0;
                            [self request_GallHDByProd:self.title];
                            [productTableView reloadData];
                            break;
                            
                        default:
                            break;
                    }
                }
            }
                break;
                
            default:
                break;
        }
        
    }
    
    [locationTableView deselectRowAtIndexPath:[locationTableView indexPathForSelectedRow] animated:YES];
    [productTableView deselectRowAtIndexPath:[productTableView indexPathForSelectedRow] animated:YES];
}

#pragma mark bottom bar

//-(void)resetBottombar
//{
//    //*myCoupons,*used,*expired,*hotDeals
//    [myCoupons removeFromSuperview];
//    [used removeFromSuperview];
//    [expired removeFromSuperview];
//    [hotDeals removeFromSuperview];
//
//     int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
//
//
//
////    UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-55, SCREEN_WIDTH, 55)];
////    tabImage.image = [UIImage imageNamed:@"BlankBottomButton.png"];
////    [self.view addSubview:tabImage];
////    [tabImage setAccessibilityValue:@"Bottom"];
////    [self.view bringSubviewToFront:tabImage];
////    //[tabImage release];
//
//    CGRect frame = CGRectMake(0,SCREEN_HEIGHT-yVal-55, SCREEN_WIDTH/4, 55);
//
//    [myCoupons setFrame:frame];
//    [myCoupons setImage:[UIImage imageNamed:@"tab_btn_up_cpns.png"] forState:UIControlStateNormal];
//    [myCoupons setImage:[UIImage imageNamed:@"tab_btn_down_cpns.png"] forState:UIControlStateHighlighted];
//    [myCoupons addTarget:self action:@selector(couponsTapped) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:myCoupons];
//    [self.view bringSubviewToFront:myCoupons];
//
//
//
//
//}

#pragma mark splash methods
-(void)showSplash
{
    UIViewController *modalViewController = [[UIViewController alloc] init];
    modelView.backgroundColor = [UIColor colorWithRGBInt:0x69239d];
    modalViewController.view = modelView;
    [self presentViewController:modalViewController animated:NO completion:nil];
    [self performSelector:@selector(hideSplash) withObject:nil afterDelay:1.5];
    ReleaseAndNilify(modalViewController);
}

- (void)hideSplash{
    [[self presentedViewController] dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark search bar delegate methods
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
    [_searchBar resignFirstResponder];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    
    if ([type isEqualToString:@"Coupons"]) {//means coupons segment
        switch (_searchBar.tag) {
            case 0://means location table
                loc_lastVisitedRecord = 0;
                [self removeObjectsOfCouponLocArrays];
                [self request_GallCoupByLoc:self.title];
                [locationTableView reloadData];
                break;
                
            case 1://means products table
                prod_lastVisitedRecord = 0;
                [self removeObjectsOfCouponProdArrays];
                [self request_GallCoupByProd:self.title];
                [productTableView reloadData];
                break;
            default:
                break;
        }
        
    }
    
    
    else{//means hot deals segment
        
        switch (_searchBar.tag) {
            case 0://means location table
                loc_lastVisitedRecord = 0;
                [self removeObjectsOfHDLocArrays];
                [self request_GallHDByLoc:self.title];
                [locationTableView reloadData];
                break;
            case 1://means products table
                prod_lastVisitedRecord = 0;
                [self removeObjectsOfHDProdArrays];
                [self request_GallHDByProd:self.title];
                [productTableView reloadData];
                break;
            default:
                break;
        }
        
    }
    
    
    [_searchBar resignFirstResponder];
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    
    _searchBar.text = @"";
    [_searchBar resignFirstResponder];
}


#pragma mark table animation methods
-(void)btn1Tapped:(id)sender {
    
    [self animateTables:locationTableView];
}

-(void)btn2Tapped:(id)sender {
    
    [self animateTables:productTableView];
}

-(void) animateTables:(UITableView *)table {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    
    float Ypos = _searchBar.frame.origin.y+_searchBar.frame.size.height;
    
    if (table.tag == 100) {
        _searchBar.text = @"";
        _searchBar.placeholder = @"Search By Location";
        _searchBar.tag = 0;
        [btn1Img setImage:[UIImage imageNamed:@"arrow_down"]];
        [btn2Img setImage:[UIImage imageNamed:@"arrow_right"]];
        if (IS_IPHONE5){
            [locationTableView setFrame:CGRectMake(0.0, Ypos, SCREEN_WIDTH, 395.0+45.0)];
            [productTableView setFrame:CGRectMake(0.0, locationTableView.frame.origin.y+locationTableView.frame.size.height-11, SCREEN_WIDTH, 30.0)];
        }
        else{
            [locationTableView setFrame:CGRectMake(0.0, Ypos, SCREEN_WIDTH, 295.0)];
            [productTableView setFrame:CGRectMake(0.0, locationTableView.frame.origin.y+locationTableView.frame.size.height+1, SCREEN_WIDTH, 30.0)];
        }
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
            
            [locationTableView setFrame:CGRectMake(0.0, Ypos, SCREEN_WIDTH, SCREEN_HEIGHT-Ypos-20-40)];
            [productTableView setFrame:CGRectMake(0.0, locationTableView.frame.origin.y+locationTableView.frame.size.height-40, SCREEN_WIDTH, SCREEN_HEIGHT-Ypos-20-40)];
        }
        [productTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        productTableView.scrollEnabled = NO;
        locationTableView.scrollEnabled = YES;
    }
    
    else if (table.tag == 200) {
        _searchBar.text = @"";
        _searchBar.placeholder = @"Search By Products";
        _searchBar.tag = 1;
        [btn2Img setImage:[UIImage imageNamed:@"arrow_down"]];
        [btn1Img setImage:[UIImage imageNamed:@"arrow_right"]];
        if (IS_IPHONE5)
        {
            [locationTableView setFrame:CGRectMake(0.0, Ypos, SCREEN_WIDTH, 30.0)];
            [productTableView setFrame:CGRectMake(0.0, locationTableView.frame.origin.y + locationTableView.frame.size.height, SCREEN_WIDTH, 385.0+50.0)];
        }
        else
        {
            [locationTableView setFrame:CGRectMake(0.0, Ypos, SCREEN_WIDTH, 30.0)];
            [productTableView setFrame:CGRectMake(0.0, locationTableView.frame.origin.y + locationTableView.frame.size.height, SCREEN_WIDTH, 295.0)];
        }
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
            
            [locationTableView setFrame:CGRectMake(0.0, Ypos, SCREEN_WIDTH, SCREEN_HEIGHT-Ypos-20-40)];
            [productTableView setFrame:CGRectMake(0.0, locationTableView.frame.origin.y+30, SCREEN_WIDTH, SCREEN_HEIGHT-Ypos-20-40)];
        }
        
        [locationTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        locationTableView.scrollEnabled = NO;
        productTableView.scrollEnabled = YES;
        
    }
    
    [UIView commitAnimations];
}


#pragma mark tableview delegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    switch (viewSelectionControl.selectedSegmentIndex) {//check for selected segment
        case 0:{//coupon segment
            
            switch (tableView.tag) { //check for whether loc table or prod table
                case 100:{
                    
                    int count = (int)[loc_couponId[section] count];
                    
                    if (!count)
                        return 1;
                    
                    else{
                        
                        if (loc_nextPage == 1 && section == ([loc_retailerId count] - 1)) //check for pagination flag
                            return count+1;
                        else
                            return count;
                    }
                }
                    break;
                    
                case 200:{
                    
                    int count = (int)[prod_couponId[section] count];
                    
                    if (!count)
                        return 1;
                    
                    else{
                        
                        if (prod_nextPage == 1 && section == ([prod_categoryId count]-1))
                            return count+1;
                        else
                            return count;
                    }
                }
                    break;
            }
        }
            break;
            
        case 1:{ //HD segment
            
            switch (tableView.tag) {
                case 100:{//means location table
                    
                    int count = (int)[loc_hotDealId[section] count];
                    
                    if (!count)
                        return 1;
                    
                    else{
                        
                        if (loc_nextPage == 1 && section == ([loc_retailerId count]-1))
                            return count+1;
                        
                        else
                            return count;
                    }
                }
                    break;
                    
                case 200:{//means products table
                    
                    int count = (int)[prod_hotDealId[section] count];
                    
                    if (!count)
                        return 1;
                    
                    else{
                        
                        if (prod_nextPage == 1 && section == ([prod_categoryId count]-1))
                            return count+1;
                        else
                            return count;
                    }
                }
                    break;
            }
        }
            break;
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        return 80;
    }
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell; //= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //if (cell == nil)
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    float asyncImagewidth = 40, asyncImageHeight = 40;
    float textLabelFont = 12;
    float textlabelYValue = 0, detailTextLabelYValue = 35;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        asyncImagewidth = 60;
        asyncImageHeight = 60;
        
        textLabelFont = 17.0;
        textlabelYValue = 10;
        detailTextLabelYValue = 45;
       // voewMoreLabelFont = 20.0;
    }
    SdImageView *couponImage = [[SdImageView alloc]initWithFrame:CGRectMake(2.0, 10.0, asyncImagewidth, asyncImageHeight)];
    couponImage.backgroundColor = [UIColor clearColor];
    
    UILabel *textLabel = [[UILabel alloc]initWithFrame:CGRectMake(asyncImagewidth + couponImage.frame.origin.x + 2.0, textlabelYValue, SCREEN_WIDTH - 70.0, 40.0)];//width 250
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:textLabelFont];
    textLabel.textColor = [UIColor colorWithRGBInt:0x112e72];
    textLabel.numberOfLines = 2;
    [textLabel setLineBreakMode:NSLineBreakByWordWrapping];
    
    UILabel *detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(asyncImagewidth + couponImage.frame.origin.x + 2.0, detailTextLabelYValue, SCREEN_WIDTH - 120.0, 20)];//width 200
    detailLabel.backgroundColor = [UIColor clearColor];
    detailLabel.textColor = [UIColor grayColor];
    detailLabel.numberOfLines = 1;
    detailLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:textLabelFont];
    
    if ([type isEqualToString:@"Coupons"]){//means coupons segment
        
        switch (tableView.tag) {//switch-case for table
            case 100:{//location table
                
                if (![loc_retailerId count]){//means 'No Records Found'
                    cell.accessoryView = UITableViewCellAccessoryNone;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    
                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
                    [label setTextAlignment:NSTextAlignmentCenter];
                    if(responseText)
                        [label setText:responseText];
                    label.font = [UIFont boldSystemFontOfSize:15];
                    label.textColor = [UIColor grayColor];
                    label.backgroundColor = [UIColor clearColor];
                    label.numberOfLines = 2;
                    label.lineBreakMode = NSLineBreakByWordWrapping;
                    [cell.contentView addSubview:label];
                    //[label release];
                    return cell;
                }
                
                if (indexPath.section == ([loc_retailerId count] - 1) && indexPath.row == [loc_couponName[indexPath.section] count] && loc_nextPage == 1){
                    
                    //                        UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 24)] ;
                    //                        label.font = [UIFont boldSystemFontOfSize:voewMoreLabelFont];
                    //                        label.textAlignment = NSTextAlignmentCenter;
                    //                        label.textColor = [UIColor colorWithRGBInt:0x112e72];
                    //                        label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
                    //                        [cell.contentView addSubview:label];
                    
                    loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    
                    loading.center = cell.contentView.center;
                    loading.color = [UIColor blackColor];
                    
                    
                    [cell.contentView addSubview:loading];
                    [loading startAnimating];
                    cell.userInteractionEnabled = NO;
                    return cell;
                }
                
                if ([loc_couponImagePath[indexPath.section] count]>indexPath.row){
                    [couponImage loadImage:[loc_couponImagePath[indexPath.section] objectAtIndex:indexPath.row]];
                    if([loc_couponName[indexPath.section]count]>indexPath.row)
                        textLabel.text = [loc_couponName[indexPath.section] objectAtIndex:indexPath.row];
                    if([loc_couponDiscountAmount[indexPath.section]count]>indexPath.row && [loc_couponExpireDate[indexPath.section]count]>indexPath.row)
                        detailLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ off Expires %@",@"%@ off Expires %@"), [loc_couponDiscountAmount[indexPath.section] objectAtIndex:indexPath.row],[loc_couponExpireDate[indexPath.section] objectAtIndex:indexPath.row]];
                }
            }
                break;
                
            case 200:{//products table
                
                if (![prod_categoryId count]){//means 'No Records Found'
                    
                    cell.accessoryView = UITableViewCellAccessoryNone;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
                    [label setTextAlignment:NSTextAlignmentCenter];
                    if(responseText)
                        [label setText:responseText];
                    label.font = [UIFont boldSystemFontOfSize:15];
                    label.textColor = [UIColor grayColor];
                    label.backgroundColor = [UIColor clearColor];
                    label.numberOfLines = 2;
                    label.lineBreakMode = NSLineBreakByWordWrapping;
                    [cell.contentView addSubview:label];
                    //[label release];
                    return cell;
                }
                
                if (indexPath.section == ([prod_categoryId count] - 1) && indexPath.row == [prod_couponName[indexPath.section] count] && prod_nextPage == 1){
                    
                    //                        UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 24)] ;
                    //                        label.font = [UIFont boldSystemFontOfSize:voewMoreLabelFont];
                    //                        label.textAlignment = NSTextAlignmentCenter;
                    //                        label.textColor = [UIColor colorWithRGBInt:0x112e72];
                    //                        label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
                    //                        [cell.contentView addSubview:label];
                    loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    
                    loading.center = cell.contentView.center;
                    loading.color = [UIColor blackColor];
                    
                    
                    [cell.contentView addSubview:loading];
                    [loading startAnimating];
                    cell.userInteractionEnabled = NO;
                    return cell;
                }
                
                if ([prod_couponImagePath[indexPath.section] count]>indexPath.row){
                    [couponImage loadImage:[prod_couponImagePath[indexPath.section] objectAtIndex:indexPath.row]];
                    
                    if([prod_couponName[indexPath.section]count]>indexPath.row)
                        textLabel.text = [prod_couponName[indexPath.section] objectAtIndex:indexPath.row];
                    if([prod_couponDiscountAmount[indexPath.section]count]>indexPath.row && [prod_couponExpireDate[indexPath.section]count]>indexPath.row)
                        detailLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ off Expires %@",@"%@ off Expires %@"), [prod_couponDiscountAmount[indexPath.section] objectAtIndex:indexPath.row],[prod_couponExpireDate[indexPath.section] objectAtIndex:indexPath.row]];
                }
            }
                break;
            default:
                break;
        }
        
    }
    
    
    else{//means hot deals segment
        
        switch (tableView.tag) {//switch-case for table
            case 100:{//location table
                
                if (![loc_retailerId count]){//means 'No Records Found'
                    
                    cell.accessoryView = UITableViewCellAccessoryNone;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
                    [label setTextAlignment:NSTextAlignmentCenter];
                    if(responseText)
                        [label setText:responseText];
                    label.font = [UIFont boldSystemFontOfSize:15];
                    label.textColor = [UIColor grayColor];
                    label.backgroundColor = [UIColor clearColor];
                    label.numberOfLines = 2;
                    label.lineBreakMode = NSLineBreakByWordWrapping;
                    [cell.contentView addSubview:label];
                    //[label release];
                    ReleaseAndNilify(textLabel);
                    ReleaseAndNilify(detailLabel);
                    ReleaseAndNilify(couponImage);
                    return cell;
                }
                
                
                if (indexPath.section == ([loc_retailerId count] - 1) && indexPath.row == [loc_hotDealName[indexPath.section] count] && loc_nextPage == 1){
                    
                    //                        UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 24)] ;
                    //                        label.font = [UIFont boldSystemFontOfSize:voewMoreLabelFont];
                    //                        label.textAlignment = NSTextAlignmentCenter;
                    //                        label.textColor = [UIColor colorWithRGBInt:0x112e72];
                    //                        label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
                    //                        [cell.contentView addSubview:label];
                    //                        cell.accessoryView = UITableViewCellAccessoryNone;
                    //                        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    
                    loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    
                    loading.center = cell.contentView.center;
                    loading.color = [UIColor blackColor];
                    
                    
                    [cell.contentView addSubview:loading];
                    [loading startAnimating];
                    cell.userInteractionEnabled = NO;
                    return cell;
                }
                
                
                if ([loc_hotDealImagePath[indexPath.section] count]>indexPath.row)
                {
                    [couponImage loadImage:[loc_hotDealImagePath[indexPath.section] objectAtIndex:indexPath.row]];
                    
                    if([loc_hotDealName[indexPath.section]count]>indexPath.row)
                        textLabel.text = [loc_hotDealName[indexPath.section] objectAtIndex:indexPath.row];
                    if([loc_hdDiscountAmount[indexPath.section]count]>indexPath.row && [loc_hdExpDate[indexPath.section]count]>indexPath.row)
                        detailLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ off Expires %@",@"%@ off Expires %@"), [loc_hdDiscountAmount[indexPath.section] objectAtIndex:indexPath.row],[loc_hdExpDate[indexPath.section] objectAtIndex:indexPath.row]];
                }
            }
                break;
                
            case 200:{//products table
                
                if (![prod_categoryId count]){//means 'No Records Found'
                    
                    cell.accessoryView = UITableViewCellAccessoryNone;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
                    [label setTextAlignment:NSTextAlignmentCenter];
                    if(responseText)
                        [label setText:responseText];
                    label.font = [UIFont boldSystemFontOfSize:15];
                    label.textColor = [UIColor grayColor];
                    label.backgroundColor = [UIColor clearColor];
                    label.numberOfLines = 2;
                    label.lineBreakMode = NSLineBreakByWordWrapping;
                    [cell.contentView addSubview:label];
                    //[label release];
                    return cell;
                }
                
                if (indexPath.section == ([prod_categoryId count] - 1) && indexPath.row == [prod_hotDealName[indexPath.section] count] && prod_nextPage == 1){
                    
                    loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    
                    loading.center = cell.contentView.center;
                    loading.color = [UIColor blackColor];
                    
                    
                    [cell.contentView addSubview:loading];
                    [loading startAnimating];
                    cell.userInteractionEnabled = NO;
                    
                    //                        UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 24)] ;
                    //                        label.font = [UIFont boldSystemFontOfSize:voewMoreLabelFont];
                    //                        label.textAlignment = NSTextAlignmentCenter;
                    //                        label.textColor = [UIColor colorWithRGBInt:0x112e72];
                    //                        label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
                    //                        [cell.contentView addSubview:label];
                    //                        cell.accessoryView = UITableViewCellAccessoryNone;
                    //                        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    return cell;
                }
                
                if ([prod_hotDealImagePath[indexPath.section] count]>indexPath.row)
                {
                    [couponImage loadImage:[prod_hotDealImagePath[indexPath.section] objectAtIndex:indexPath.row]];
                    if([prod_hotDealName[indexPath.section] count] > indexPath.row)
                        textLabel.text = [prod_hotDealName[indexPath.section] objectAtIndex:indexPath.row];
                    
                    if([prod_hdDiscountAmount[indexPath.section]count]>indexPath.row && [prod_hdExpDate[indexPath.section]count]>indexPath.row)
                        detailLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ off Expires %@",@"%@ off Expires %@"), [prod_hdDiscountAmount[indexPath.section] objectAtIndex:indexPath.row],[prod_hdExpDate[indexPath.section] objectAtIndex:indexPath.row]];
                }
            }
                break;
            default:
                break;
        }
    }
    
    [cell addSubview:couponImage];
    [cell addSubview:textLabel];
    [cell addSubview:detailLabel];
    
    ReleaseAndNilify(couponImage);
    ReleaseAndNilify(textLabel);
    ReleaseAndNilify(detailLabel);
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([type isEqualToString:@"Coupons"]){//coupons segment
        switch (tableView.tag) {
            case 100:{
                if (indexPath.section == ([loc_retailerId count] - 1) && indexPath.row == [loc_couponName[indexPath.section] count]){
                    if (![defaults boolForKey:@"ViewMore"] && loc_lastVisitedRecord!=0) {
                        [defaults setBool:YES forKey:@"ViewMore"];
                        [self fetchNextRecords:(int)tableView.tag];
                        return;
                    }
                }
            }
                break;
            case 200:{
                if (indexPath.section == ([prod_categoryId count] - 1) && indexPath.row == [prod_couponName[indexPath.section] count]){
                    if (![defaults boolForKey:@"ViewMore"] && prod_lastVisitedRecord!=0) {
                        [defaults setBool:YES forKey:@"ViewMore"];
                        [self fetchNextRecords:(int)tableView.tag];
                        return;
                    }
                }
            }
                break;
            default:
                break;
        }
    }
    else{//hot deals segment
        //req for details screen
        switch (tableView.tag) {
            case 100:{
                if (indexPath.section == ([loc_retailerId count] - 1) && indexPath.row == [loc_hotDealName[indexPath.section] count]){
                    if (![defaults boolForKey:@"ViewMore"] && loc_lastVisitedRecord!=0) {
                        [defaults setBool:YES forKey:@"ViewMore"];
                        [self fetchNextRecords:(int)tableView.tag];
                        return;
                    }
                }
            }
                break;
            case 200:{
                if (indexPath.section == ([prod_categoryId count] - 1) && indexPath.row == [prod_hotDealName[indexPath.section] count]){
                    if (![defaults boolForKey:@"ViewMore"] && prod_lastVisitedRecord!=0) {
                        [defaults setBool:YES forKey:@"ViewMore"];
                        [self fetchNextRecords:(int)tableView.tag];
                        return;
                    }
                }
            }
                break;
            default:
                break;
        }
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [defaults setBool:NO forKey:@"ViewMore"];
    if ([type isEqualToString:@"Coupons"]){//coupons segment
        
        switch (tableView.tag) {
            case 100:{
                
                if (![loc_retailerId count])
                    return;
                
                //                    if (indexPath.section == ([loc_retailerId count] - 1) && indexPath.row == [loc_couponName[indexPath.section] count]){
                //
                //                        [self fetchNextRecords:(int)tableView.tag];
                //                        return;
                //                    }
                
                
                [defaults setValue:[loc_couponId[indexPath.section]objectAtIndex:indexPath.row] forKey:@"couponId"];
                [defaults setValue:nil forKey:@"couponListId"];
                [defaults  setValue:[loc_couponImagePath[indexPath.section]objectAtIndex:indexPath.row] forKey:@"imgPath"];
                
                NewCouponDetailViewController *obj_CouponDetail = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:obj_CouponDetail animated:NO];
                // [obj_CouponDetail release];
                
            }
                break;
                
            case 200:{
                
                if (![prod_categoryId count])
                    return;
                
                //                    if (indexPath.section == ([prod_categoryId count] - 1) && indexPath.row == [prod_couponName[indexPath.section] count]){
                //
                //                        [self fetchNextRecords:(int)tableView.tag];
                //                        return;
                //                    }
                
                [defaults setValue:[prod_couponId[indexPath.section]objectAtIndex:indexPath.row] forKey:@"couponId"];
                [defaults setValue:nil forKey:@"couponListId"];
                [defaults  setValue:[prod_couponImagePath[indexPath.section]objectAtIndex:indexPath.row] forKey:@"imgPath"];
                
                NewCouponDetailViewController *obj_CouponDetail = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:obj_CouponDetail animated:NO];
                // [obj_CouponDetail release];
                
            }
                break;
                
            default:
                break;
        }
        
        if([prod_couponImagePath[indexPath.section]count] > indexPath.row)
            [defaults  setValue:[prod_couponImagePath[indexPath.section] objectAtIndex:indexPath.row]forKey:@"imgPath"];
        
        [defaults  setValue:nil forKey:@"couponListID"];
        [defaults  setValue:VALUE_COUPON forKey:KEY_CLR];
        selectedTableViewTag = (int)tableView.tag;
        
    }//coupons segment - case 0 break
    
    
    else{//hot deals segment
        //req for details screen
        
        switch (tableView.tag) {
                
            case 100:{
                if (![loc_retailerId count])
                    return;
                //                    if (indexPath.section == ([loc_retailerId count] - 1) && indexPath.row == [loc_hotDealName[indexPath.section] count]){
                //
                //                        [self fetchNextRecords:(int)tableView.tag];
                //                        return;
                //                    }
                [defaults setValue:[loc_hotDealId[indexPath.section]objectAtIndex:indexPath.row] forKey:@"HotDealId"];
                [defaults setValue:nil forKey:@"hotDealListID"];
                
            }
                break;
                
            case 200:{
                
                if (![prod_categoryId count])
                    return;
                
                //                    if (indexPath.section == ([prod_categoryId count] - 1) && indexPath.row == [prod_hotDealName[indexPath.section] count]){
                //
                //                        [self fetchNextRecords:(int)tableView.tag];
                //                        return;
                //                    }
                
                [defaults setValue:[prod_hotDealId[indexPath.section]objectAtIndex:indexPath.row] forKey:@"HotDealId"];
                [defaults setValue:nil forKey:@"hotDealListID"];
            }
                break;
                
            default:
                break;
        }
        selectedTableViewTag = (int)tableView.tag;
        
        HotDealDetailViewController *obj_hdDetails = [[HotDealDetailViewController alloc]initWithNibName:@"HotDealDetailViewController" bundle:nil];
        [self.navigationController pushViewController:obj_hdDetails animated:NO];
        //[obj_hdDetails release];
        
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    switch (tableView.tag) {
        case 100:
            return [loc_retailerId count]>0?[loc_retailerId count]:1;
            break;
            
        case 200:
            return [prod_categoryId count]>0?[prod_categoryId count]:1;
            break;
            
        default: return 2;
            break;
    }
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    switch (section) { //for first section needs to display section name & category name
        case 0:
            switch (tableView.tag) {
                case 100:
                    if ([loc_retailerId count])
                        return 60.0;
                    else
                        return 30.0;
                    break;
                case 200:
                    
                    if ([prod_categoryId count])
                        return 60.0;
                    else
                        return 30.0;
                    break;
                    
                default:
                    break;
            }
            return 60.0;
            break;
            
        default:
            return 30.0;
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    float headerlabelFont = 14.0, buttonFont = 16.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        headerlabelFont = 18.0;
        buttonFont = 20.0;
    }
    UIView *headerView = [[UIView alloc]init];
    UILabel *headerLabel = [[UILabel alloc] init];
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:headerlabelFont];
    headerLabel.backgroundColor = [UIColor lightGrayColor];
    
    switch (tableView.tag) {
        case 100:{//means its location table
            
            //check for first section to return both button and category name label
            switch (section) {
                    
                case 0:{
                    
                    if ([loc_retailerId count]){
                        headerView.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, 60.0);
                        //btn1Img.image = [UIImage imageNamed:@"arrow_down.png"];
                    }
                    else{
                        headerView.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, 30.0);
                        //btn1Img.image = [UIImage imageNamed:@"arrow_right.png"];
                    }
                    
                    //add the arrow image to button
                    //btn1Img.image = [UIImage imageNamed:@"arrow_down.png"];
                    btn1Img.frame = CGRectMake(SCREEN_WIDTH-20, 10.0, 10.0, 10.0);
                    
                    btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
                    btn1.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, 30.0);
                    btn1.backgroundColor = [UIColor clearColor];
                    btn1.adjustsImageWhenHighlighted = NO;
                    [btn1 setBackgroundImage:[UIImage imageNamed:@"hotDealsBg.png"] forState:UIControlStateNormal];
                    // [btn1 setImage:[UIImage imageNamed:@"hotDealsBg.png"] forState:UIControlStateNormal];
                    [btn1 setTitleEdgeInsets:UIEdgeInsetsMake(8.0, 10.0, 10.0, 2.0)];
                    [btn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    [btn1.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:buttonFont]];
                    [btn1 setTitle:@"Locations" forState:UIControlStateNormal];
                    btn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                    
                    [btn1 addTarget:self action:@selector(btn1Tapped:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [btn1 addSubview:btn1Img];
                    //[btn1Img release];
                    [btn1 setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
                    [headerView addSubview:btn1];
                    
                    if ([loc_retailerName count])
                    {
                        headerLabel.frame = CGRectMake(0.0, btn1.frame.origin.y+btn1.frame.size.height, SCREEN_WIDTH, 30.0);
                        if([loc_retailerName count] > section && [loc_retailerAddress count]> section)
                            headerLabel.text = [NSString stringWithFormat:@"  %@ - %@", [loc_retailerName objectAtIndex:section],[loc_retailerAddress objectAtIndex:section]];
                        [headerView addSubview:headerLabel];
                    }
                    
                }
                    break;//end of case 0
                    
                    //for all other sections return only the category name label
                default:{
                    headerLabel.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, 30.0);
                    if([loc_retailerName count] > section && [loc_retailerAddress count] > section)
                        headerLabel.text = [NSString stringWithFormat:@"  %@ - %@",[loc_retailerName objectAtIndex:section],[loc_retailerAddress objectAtIndex:section]];
                    [headerView addSubview:headerLabel];
                }
                    break;
            }
        }
            break;
            
        case 200:{//means its products table
            
            //check for first section to return both button and category name label
            switch (section) {
                case 0:{
                    if ([prod_categoryId count]){
                        headerView.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, 60.0);
                        //btn2Img.image = [UIImage imageNamed:@"arrow_down.png"];
                    }
                    else{
                        headerView.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, 30.0);
                        //btn2Img.image = [UIImage imageNamed:@"arrow_right.png"];
                    }
                    
                    //add the arrow image to button
                    // btn2Img.image = [UIImage imageNamed:@"arrow_right.png"];
                    btn2Img.frame = CGRectMake(SCREEN_WIDTH-20, 10.0, 10.0, 10.0);
                    
                    btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
                    btn2.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, 30.0);
                    btn2.backgroundColor = [UIColor clearColor];
                    btn2.adjustsImageWhenHighlighted = NO;
                    [btn2 setBackgroundImage:[UIImage imageNamed:@"hotDealsBg.png"] forState:UIControlStateNormal];
                    [btn2 setTitleEdgeInsets:UIEdgeInsetsMake(8.0, 10.0, 10.0, 2.0)];
                    [btn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    [btn2.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:buttonFont]];
                    btn2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                    [btn2 setTitle:@"Products" forState:UIControlStateNormal];
                    [btn2 addTarget:self action:@selector(btn2Tapped:) forControlEvents:UIControlEventTouchUpInside];
                    [btn2 addSubview:btn2Img];
                    //[btn2Img release];
                    [headerView addSubview:btn2];
                    
                    if ([prod_categoryName count]){
                        headerLabel.frame = CGRectMake(0.0, btn2.frame.origin.y+btn2.frame.size.height, SCREEN_WIDTH, 30.0);
                        if([prod_categoryName count] > section)
                            headerLabel.text = [NSString stringWithFormat:@"  %@",[prod_categoryName objectAtIndex:section]];
                        else
                            headerLabel.text=@" ";
                        [headerView addSubview:headerLabel];
                    }
                }
                    break;//end of case 0 for section check
                    
                    //for other sections return only the category name label
                default:{
                    headerLabel.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, 30.0);
                    if([prod_categoryName count] > section)
                        headerLabel.text = [NSString stringWithFormat:@"  %@",[prod_categoryName objectAtIndex:section]];
                    else
                        headerLabel.text= @" ";
                    [headerView addSubview:headerLabel];
                }
                    break;
            }
        }
            break;
            
        default:
            break;
    }
    return headerView;
}


#pragma mark pagination
-(void) fetchNextRecords:(int)tag {
    
    switch (viewSelectionControl.selectedSegmentIndex) {
        case 0:{//means coupons segment
            
            switch (tag) {
                case 100://means location table
                    [self request_GallCoupByLoc:self.title];
                    [locationTableView reloadData];
                    break;
                case 200://means products table
                    [self request_GallCoupByProd:self.title];
                    [productTableView reloadData];
                    break;
                default:
                    break;
            }
            
        }
            break;
            
        case 1:{//means hot deals segment
            
            switch (tag) {
                case 100://means location table
                    [self request_GallHDByLoc:self.title];
                    [locationTableView reloadData];
                    break;
                case 200://means products table
                    [self request_GallHDByProd:self.title];
                    [productTableView reloadData];
                    break;
                default:
                    break;
            }
            
        }
            break;
            
        default:
            break;
    }
}


#pragma mark segment change methods
-(void)segmentChanged
{
    switch ([viewSelectionControl selectedSegmentIndex]) {
        case 0:
        {
            [self removeObjectsOfHDLocArrays];
            [self removeObjectsOfHDProdArrays];
            loc_lastVisitedRecord = 0;
            prod_lastVisitedRecord = 0;
            [self request_GallCoupByLoc:self.title];
            [self request_GallCoupByProd:self.title];
            [locationTableView reloadData];
            [productTableView reloadData];
            
            if ([loc_retailerId count])
                [self btn1Tapped:nil];
            
            else if ([prod_categoryId count])
                [self btn2Tapped:nil];
            
            else
                [self btn1Tapped:nil];
        }
            break;
        case 1:{
            
            [self removeObjectsOfCouponLocArrays];
            [self removeObjectsOfCouponProdArrays];
            loc_lastVisitedRecord = 0;
            prod_lastVisitedRecord = 0;
            [self request_GallHDByLoc:self.title];
            [self request_GallHDByProd:self.title];
            [locationTableView reloadData];
            [productTableView reloadData];
            
            if ([loc_retailerId count])
                [self btn1Tapped:nil];
            
            else if ([prod_categoryId count])
                [self btn2Tapped:nil];
            else
                [self btn1Tapped:nil];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark navigation bar actions
-(void)popBackToPreviousPage{//called on tap of left bar button
    if (fromGalleryDisplay) {
        fromGalleryDisplay =false;
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        for (UIViewController *aViewController in allViewControllers) {
            if ([aViewController isKindOfClass:[CouponsInDealViewController class]]) {
                [self.navigationController popToViewController:aViewController animated:NO];
            }
        }
    }
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}



-(void)returnToMainPage:(id)sender{//called on tap of right bar button
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}



#pragma mark request methods
//coupons by location
-(void)request_GallCoupByLoc:(NSString*)type1{
    
    NSMutableString *reqStr = [[NSMutableString alloc] init];
    [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([_searchBar.text length]){
        [reqStr appendFormat:@"<searchKey>%@</searchKey>",[_searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    }
    
    if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    if ([defaults valueForKey:KEY_MAINMENUID]){
        
        [reqStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    }
    
    if ([defaults valueForKey:KEY_HUBCITIID]){
        
        [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    }
    
    else if ([defaults valueForKey:KEY_MITEMID]){
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<lowerLimit>%d</lowerLimit><type>%@</type></ProductDetailsRequest>",loc_lastVisitedRecord,type1];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@gallery/gallcoupbyloc",BASE_URL];
    
    iWebServiceType = gallcoupbyloc;
    
    
    //[ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
    NSString *response = [ConnectionManager establishPostConnection:urlStr withParam:reqStr];
    //[reqStr release];
    
    [self parse_GallCoupByLoc:response];
    
}

//coupons by products
-(void)request_GallCoupByProd:(NSString*)type1{
    
    NSMutableString *reqStr = [[NSMutableString alloc] init];
    [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>", [defaults valueForKey:KEY_USERID]];
    if ([_searchBar.text length]){
        [reqStr appendFormat:@"<searchKey>%@</searchKey>",[_searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    }
    
    if ([defaults valueForKey:KEY_MAINMENUID]){
        [reqStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    }
    else if ([defaults valueForKey:KEY_MITEMID]){
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    if ([defaults valueForKey:KEY_HUBCITIID]){
        
        [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    }
    
    [reqStr appendFormat:@"<lowerLimit>%d</lowerLimit><type>%@</type></ProductDetailsRequest>",prod_lastVisitedRecord,type1];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@gallery/gallcoupbyprod",BASE_URL];
    
    iWebServiceType = gallcoupbyprod;
    // [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    NSString *response = [ConnectionManager establishPostConnection:urlStr withParam:reqStr];
    
    // [reqStr release];
    [self parse_GallCoupByProd:response];
}

//HD by location
-(void)request_GallHDByLoc:(NSString*)type1{
    
    NSMutableString *reqStr = [[NSMutableString alloc] init];
    [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([_searchBar.text length]){
        [reqStr appendFormat:@"<searchKey>%@</searchKey>",[_searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    }
    
    if ([defaults valueForKey:KEY_MAINMENUID]){
        
        [reqStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    }
    
    if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    if ([defaults valueForKey:KEY_HUBCITIID]){
        
        [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    }
    
    
    else if ([defaults valueForKey:KEY_MITEMID]){
        
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<lowerLimit>%d</lowerLimit><type>%@</type></ProductDetailsRequest>",loc_lastVisitedRecord,type1];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@gallery/gallhdbyloc",BASE_URL];
    
    iWebServiceType = gallhdbyloc;
    
    //[ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
    NSString *response = [ConnectionManager establishPostConnection:urlStr withParam:reqStr];
    
    // [reqStr release];
    
    [self  parse_GallHDByLoc:response];
}

//HD by products
-(void)request_GallHDByProd:(NSString*)type1{
    
    NSMutableString *reqStr = [[NSMutableString alloc] init];
    [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    if ([_searchBar.text length]){
        [reqStr appendFormat:@"<searchKey>%@</searchKey>",[_searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    }
    
    if ([defaults valueForKey:KEY_MAINMENUID]){
        
        [reqStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    }
    else if ([defaults valueForKey:KEY_MITEMID]){
        
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    if ([defaults valueForKey:KEY_HUBCITIID]){
        
        [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    }
    if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    
    [reqStr appendFormat:@"<lowerLimit>%d</lowerLimit><type>%@</type></ProductDetailsRequest>",prod_lastVisitedRecord,type1];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@gallery/gallhdbyprod",BASE_URL];
    
    iWebServiceType = gallhdbyprod;
    
    //[ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
    NSString *response = [ConnectionManager establishPostConnection:urlStr withParam:reqStr];
    
    // [reqStr release];
    [self parse_GallHDByProd:response];
}

#pragma mark parse methods

-(void)responseData:(NSString*)response{
    
    switch (iWebServiceType) {
        case gallcoupbyloc:
            [self parse_GallCoupByLoc:response];
            break;
            
        case gallcoupbyprod:
            [self parse_GallCoupByProd:response];
            break;
            
        case gallhdbyloc:
            [self parse_GallHDByLoc:response];
            break;
            
        case gallhdbyprod:
            [self parse_GallHDByProd:response];
            break;
            
        default:
            break;
    }
}

-(void)parse_GallCoupByLoc:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
    {
        return;
    }
    
    loc_nextPage = 0;
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPageFlag" parentElement:tbXml.rootXMLElement];
    if (nextPageElement!=nil) {
        
        loc_nextPage = [[TBXML textForElement:nextPageElement]intValue];
        
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbXml.rootXMLElement];
        [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *maxRowNum = [TBXML childElementNamed:@"maxRowNum" parentElement:tbXml.rootXMLElement];
        loc_lastVisitedRecord = [[TBXML textForElement:maxRowNum]intValue];
        
        TBXMLElement *retDetailsList1 = [TBXML childElementNamed:@"retDetailsList" parentElement:tbXml.rootXMLElement];
        
        TBXMLElement *RetailerDetails1 = [TBXML childElementNamed:@"RetailersDetails" parentElement:retDetailsList1];//1st level
        
        int i = (int)[loc_retailerId count];
        while (RetailerDetails1!=nil) {
            
            TBXMLElement *retId = [TBXML childElementNamed:@"retId" parentElement:RetailerDetails1];
            TBXMLElement *retName = [TBXML childElementNamed:@"retName" parentElement:RetailerDetails1];
            
            [loc_retailerId addObject:[TBXML textForElement:retId]];
            [loc_retailerName addObject:[TBXML textForElement:retName]];
            
            TBXMLElement *retDetailsList2 = [TBXML childElementNamed:@"retDetailsList" parentElement:RetailerDetails1];//2nd level
            
            TBXMLElement *RetailerDetails2 = [TBXML childElementNamed:@"RetailersDetails" parentElement:retDetailsList2];
            
            while (RetailerDetails2!=nil) {
                
                TBXMLElement *retailerAddress = [TBXML childElementNamed:@"retailerAddress" parentElement:RetailerDetails2];
                TBXMLElement *retLocId = [TBXML childElementNamed:@"retLocId" parentElement:RetailerDetails2];
                
                [loc_retailerAddress addObject:[TBXML textForElement:retailerAddress]];
                [loc_retLocId addObject:[TBXML textForElement:retLocId]];
                
                TBXMLElement *couponDetailsList = [TBXML childElementNamed:@"couponDetailsList" parentElement:RetailerDetails2];
                TBXMLElement *CouponDetails = [TBXML childElementNamed:@"CouponDetails" parentElement:couponDetailsList];
                
                loc_couponId[i] = [[NSMutableArray alloc]init];
                loc_couponName[i] = [[NSMutableArray alloc]init];
                loc_couponDiscountAmount[i] = [[NSMutableArray alloc]init];
                loc_couponExpireDate[i] = [[NSMutableArray alloc]init];
                loc_couponImagePath[i] = [[NSMutableArray alloc]init];
                loc_coupDesc[i] = [[NSMutableArray alloc]init];
                loc_userCoupGallId[i] = [[NSMutableArray alloc]init];
                
                while (CouponDetails!=nil) {//while for CouponDetails
                    
                    TBXMLElement *couponId = [TBXML childElementNamed:@"couponId" parentElement:CouponDetails];
                    TBXMLElement *couponName = [TBXML childElementNamed:@"couponName" parentElement:CouponDetails];
                    TBXMLElement *couponDiscountAmount = [TBXML childElementNamed:@"couponDiscountAmount" parentElement:CouponDetails];
                    TBXMLElement *couponExpireDate = [TBXML childElementNamed:@"couponExpireDate" parentElement:CouponDetails];
                    TBXMLElement *couponImagePath = [TBXML childElementNamed:@"couponImagePath" parentElement:CouponDetails];
                    TBXMLElement *coupDesc = [TBXML childElementNamed:@"coupDesc" parentElement:CouponDetails];
                    TBXMLElement *userCoupGallId = [TBXML childElementNamed:@"userCoupGallId" parentElement:CouponDetails];
                    
                    if(couponId)
                        [loc_couponId[i] addObject:[TBXML textForElement:couponId]];
                    else
                        [loc_couponId[i] addObject:@" "];
                    
                    if(couponName)
                        [loc_couponName[i] addObject:[TBXML textForElement:couponName]];
                    else
                        [loc_couponName[i] addObject:@""];
                    
                    if(couponDiscountAmount)
                        [loc_couponDiscountAmount[i] addObject:[TBXML textForElement:couponDiscountAmount]];
                    else
                        [loc_couponDiscountAmount[i] addObject:@" "];
                    
                    if(couponExpireDate)
                        [loc_couponExpireDate[i] addObject:[TBXML textForElement:couponExpireDate]];
                    else
                        [loc_couponExpireDate[i] addObject:@" "];
                    
                    if(couponImagePath)
                        [loc_couponImagePath[i] addObject:[TBXML textForElement:couponImagePath]];
                    else
                        [loc_couponImagePath[i] addObject:@" "];
                    
                    if(coupDesc)
                        [loc_coupDesc[i] addObject:[TBXML textForElement:coupDesc]];
                    else
                        [loc_coupDesc[i] addObject:@" "];
                    if(userCoupGallId)
                        [loc_userCoupGallId[i] addObject:[TBXML textForElement:userCoupGallId]];
                    else
                        [loc_userCoupGallId[i] addObject:@" "];
                    
                    CouponDetails = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:CouponDetails];
                }//end of while for CouponDetails
                
                RetailerDetails2 = [TBXML nextSiblingNamed:@"RetailersDetails" searchFromElement:RetailerDetails2];
                i++;
            }//end of while - RetailerDetails2
            
            RetailerDetails1 = [TBXML nextSiblingNamed:@"RetailersDetails" searchFromElement:RetailerDetails1];
        }//end of while - RetailerDetails1
    }
    
    else{
        
        TBXMLElement *responseElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        responseText = [[TBXML textForElement:responseElement]copy];
        
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbXml.rootXMLElement];
        if (mainMenuIdElement!=nil){
            [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
    }
    [defaults setBool:NO forKey:@"ViewMore"];
}
//coupons by location

//coupons by products
-(void)parse_GallCoupByProd:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
    {
        return;
    }
    prod_nextPage = 0;
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPageFlag" parentElement:tbXml.rootXMLElement];
    if (nextPageElement!=nil) {
        
        prod_nextPage = [[TBXML textForElement:nextPageElement]intValue];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbXml.rootXMLElement];
        [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *maxRowNum = [TBXML childElementNamed:@"maxRowNum" parentElement:tbXml.rootXMLElement];
        prod_lastVisitedRecord = [[TBXML textForElement:maxRowNum]intValue];
        
        TBXMLElement *categoryInfoList = [TBXML childElementNamed:@"categoryInfoList" parentElement:tbXml.rootXMLElement];
        
        TBXMLElement *CategoryInfo = [TBXML childElementNamed:@"CategoryInfo" parentElement:categoryInfoList];
        
        int i = (int)[prod_categoryId count];
        while (CategoryInfo!=nil) {
            
            TBXMLElement *categoryID = [TBXML childElementNamed:@"categoryId" parentElement:CategoryInfo];
            TBXMLElement *categoryName = [TBXML childElementNamed:@"categoryName" parentElement:CategoryInfo];
            
            [prod_categoryId addObject:[TBXML textForElement:categoryID]];
            [prod_categoryName addObject:[TBXML textForElement:categoryName]];
            
            TBXMLElement *couponDetailsList = [TBXML childElementNamed:@"couponDetailsList" parentElement:CategoryInfo];
            
            TBXMLElement *CouponDetails = [TBXML childElementNamed:@"CouponDetails" parentElement:couponDetailsList];
            
            prod_couponId[i] = [[NSMutableArray alloc]init];
            prod_couponName[i] = [[NSMutableArray alloc]init];
            prod_couponDiscountAmount[i] = [[NSMutableArray alloc]init];
            prod_couponExpireDate[i] = [[NSMutableArray alloc]init];
            prod_couponImagePath[i] = [[NSMutableArray alloc]init];
            prod_coupDesc[i] = [[NSMutableArray alloc]init];
            prod_userCoupGallId[i] = [[NSMutableArray alloc]init];
            
            while (CouponDetails!=nil) {
                
                TBXMLElement *couponId = [TBXML childElementNamed:@"couponId" parentElement:CouponDetails];
                TBXMLElement *couponName = [TBXML childElementNamed:@"couponName" parentElement:CouponDetails];
                TBXMLElement *couponDiscountAmount = [TBXML childElementNamed:@"couponDiscountAmount" parentElement:CouponDetails];
                TBXMLElement *couponExpireDate = [TBXML childElementNamed:@"couponExpireDate" parentElement:CouponDetails];
                TBXMLElement *couponImagePath = [TBXML childElementNamed:@"couponImagePath" parentElement:CouponDetails];
                TBXMLElement *coupDesc = [TBXML childElementNamed:@"coupDesc" parentElement:CouponDetails];
                TBXMLElement *userCoupGallId = [TBXML childElementNamed:@"userCoupGallId" parentElement:CouponDetails];
                
                if(couponId)
                    [prod_couponId[i] addObject:[TBXML textForElement:couponId]];
                else
                    [prod_couponId[i] addObject:@" "];
                
                if(couponName)
                    [prod_couponName[i] addObject:[TBXML textForElement:couponName]];
                else
                    [prod_couponName[i] addObject:@" "];
                
                if(couponDiscountAmount)
                    [prod_couponDiscountAmount[i] addObject:[TBXML textForElement:couponDiscountAmount]];
                else
                    [prod_couponDiscountAmount[i] addObject:@" "];
                
                if(couponExpireDate)
                    [prod_couponExpireDate[i] addObject:[TBXML textForElement:couponExpireDate]];
                else
                    [prod_couponExpireDate[i] addObject:@" "];
                
                if(couponImagePath)
                    [prod_couponImagePath[i] addObject:[TBXML textForElement:couponImagePath]];
                else
                    [prod_couponImagePath[i] addObject:@" "];
                
                if(coupDesc)
                    [prod_coupDesc[i] addObject:[TBXML textForElement:coupDesc]];
                else
                    [prod_coupDesc[i] addObject:@" "];
                
                if(userCoupGallId)
                    [prod_userCoupGallId[i] addObject:[TBXML textForElement:userCoupGallId]];
                else
                    [prod_userCoupGallId[i] addObject:@" "];
                
                CouponDetails = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:CouponDetails];
            }
            
            CategoryInfo = [TBXML nextSiblingNamed:@"CategoryInfo" searchFromElement:CategoryInfo];
            i++;
        }
    }
    else{
        
        TBXMLElement *responseElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        responseText = [[TBXML textForElement:responseElement]copy];
        
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbXml.rootXMLElement];
        if (mainMenuIdElement!=nil){
            [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
    }
    [defaults setBool:NO forKey:@"ViewMore"];
}
//coupons by products

//HD by location
-(void)parse_GallHDByLoc:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
    {
        return;
    }
    loc_nextPage = 0;
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbXml.rootXMLElement];
    if (nextPageElement!=nil) {
        
        loc_nextPage = [[TBXML textForElement:nextPageElement]intValue];
        
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbXml.rootXMLElement];
        [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *maxRowNum = [TBXML childElementNamed:@"maxRowNum" parentElement:tbXml.rootXMLElement];
        loc_lastVisitedRecord = [[TBXML textForElement:maxRowNum]intValue];
        
        TBXMLElement *retDetailsList1 = [TBXML childElementNamed:@"retDetailsList" parentElement:tbXml.rootXMLElement];
        
        TBXMLElement *RetailerDetails1 = [TBXML childElementNamed:@"RetailersDetails" parentElement:retDetailsList1];//1st level
        
        int i = (int)[loc_retailerId count];
        while (RetailerDetails1!=nil) {
            
            TBXMLElement *retId = [TBXML childElementNamed:@"retId" parentElement:RetailerDetails1];
            TBXMLElement *retName = [TBXML childElementNamed:@"retName" parentElement:RetailerDetails1];
            
            [loc_retailerId addObject:[TBXML textForElement:retId]];
            [loc_retailerName addObject:[TBXML textForElement:retName]];
            
            TBXMLElement *retDetailsList2 = [TBXML childElementNamed:@"retDetailsList" parentElement:RetailerDetails1];//2nd level
            
            TBXMLElement *RetailerDetails2 = [TBXML childElementNamed:@"RetailersDetails" parentElement:retDetailsList2];
            
            while (RetailerDetails2!=nil) {
                
                TBXMLElement *retailerAddress = [TBXML childElementNamed:@"retailerAddress" parentElement:RetailerDetails2];
                TBXMLElement *retLocId = [TBXML childElementNamed:@"retLocId" parentElement:RetailerDetails2];
                
                if(retailerAddress)
                    [loc_retailerAddress addObject:[TBXML textForElement:retailerAddress]];
                else
                    [loc_retailerAddress addObject:@" "];
                
                if(retLocId)
                    [loc_retLocId addObject:[TBXML textForElement:retLocId]];
                else
                    [loc_retLocId addObject:@" "];
                
                TBXMLElement *hDDetailsList = [TBXML childElementNamed:@"hDDetailsList" parentElement:RetailerDetails2];
                TBXMLElement *HotDealsDetails = [TBXML childElementNamed:@"HotDealsDetails" parentElement:hDDetailsList];
                
                loc_hotDealName[i] = [[NSMutableArray alloc]init];
                loc_hotDealId[i] = [[NSMutableArray alloc]init];
                loc_hotDealImagePath[i] = [[NSMutableArray alloc]init];
                loc_hdDiscountAmount[i] = [[NSMutableArray alloc]init];
                loc_hdExpDate[i] = [[NSMutableArray alloc]init];
                loc_hdGalId[i] = [[NSMutableArray alloc]init];
                loc_hdDesc[i] = [[NSMutableArray alloc]init];
                
                while (HotDealsDetails!=nil) {//while for HotDealsDetails
                    
                    TBXMLElement *hotDealName = [TBXML childElementNamed:@"hotDealName" parentElement:HotDealsDetails];
                    TBXMLElement *hotDealId = [TBXML childElementNamed:@"hotDealId" parentElement:HotDealsDetails];
                    TBXMLElement *hotDealImagePath = [TBXML childElementNamed:@"hotDealImagePath" parentElement:HotDealsDetails];
                    TBXMLElement *hDDiscountAmount = [TBXML childElementNamed:@"hDDiscountAmount" parentElement:HotDealsDetails];
                    TBXMLElement *hDEndDate = [TBXML childElementNamed:@"hDExpDate" parentElement:HotDealsDetails];
                    TBXMLElement *hDGallId = [TBXML childElementNamed:@"hDGallId" parentElement:HotDealsDetails];
                    TBXMLElement *hDDesc = [TBXML childElementNamed:@"hDDesc" parentElement:HotDealsDetails];
                    
                    if(hotDealName)
                        [loc_hotDealName[i] addObject:[TBXML textForElement:hotDealName]];
                    else
                        [loc_hotDealName[i] addObject:@" "];
                    
                    if(hotDealId)
                        [loc_hotDealId[i] addObject:[TBXML textForElement:hotDealId]];
                    else
                        [loc_hotDealId[i] addObject:@" "];
                    
                    if(hotDealImagePath)
                        [loc_hotDealImagePath[i] addObject:[TBXML textForElement:hotDealImagePath]];
                    else
                        [loc_hotDealImagePath[i] addObject:@" "];
                    
                    if(hDDiscountAmount)
                        [loc_hdDiscountAmount[i] addObject:[TBXML textForElement:hDDiscountAmount]];
                    else
                        [loc_hdDiscountAmount[i] addObject:@" "];
                    
                    if(hDEndDate)
                        [loc_hdExpDate[i] addObject:[TBXML textForElement:hDEndDate]];
                    else
                        [loc_hdExpDate[i] addObject:@" "];
                    
                    if(hDGallId)
                        [loc_hdGalId[i] addObject:[TBXML textForElement:hDGallId]];
                    else
                        [loc_hdGalId[i] addObject:@" "];
                    
                    if(hDDesc)
                        [loc_hdDesc[i] addObject:[TBXML textForElement:hDDesc]];
                    else
                        [loc_hdDesc[i] addObject:@" "];
                    
                    HotDealsDetails = [TBXML nextSiblingNamed:@"HotDealsDetails" searchFromElement:HotDealsDetails];
                    
                }//end of while for CouponDetails
                
                RetailerDetails2 = [TBXML nextSiblingNamed:@"RetailersDetails" searchFromElement:RetailerDetails2];
                i++;
            }//end of while - RetailerDetails2
            
            RetailerDetails1 = [TBXML nextSiblingNamed:@"RetailersDetails" searchFromElement:RetailerDetails1];
        }//end of while - RetailerDetails1
    }
    
    else{
        
        TBXMLElement *responseElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        responseText = [[TBXML textForElement:responseElement]copy];
        
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbXml.rootXMLElement];
        if (mainMenuIdElement!=nil){
            [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
    }
    [defaults setBool:NO forKey:@"ViewMore"];
}
//HD by location

//HD by products
-(void)parse_GallHDByProd:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
    {
        return;
    }
    prod_nextPage = 0;
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbXml.rootXMLElement];
    if (nextPageElement!=nil) {
        
        prod_nextPage = [[TBXML textForElement:nextPageElement]intValue];
        
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbXml.rootXMLElement];
        [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *maxRowNum = [TBXML childElementNamed:@"maxRowNum" parentElement:tbXml.rootXMLElement];
        loc_lastVisitedRecord = [[TBXML textForElement:maxRowNum]intValue];
        
        TBXMLElement *categoryInfoList = [TBXML childElementNamed:@"categoryInfoList" parentElement:tbXml.rootXMLElement];
        
        TBXMLElement *CategoryInfo = [TBXML childElementNamed:@"CategoryInfo" parentElement:categoryInfoList];
        
        int i = (int)[prod_categoryId count];
        while (CategoryInfo!=nil) {
            
            TBXMLElement *categoryID = [TBXML childElementNamed:@"categoryId" parentElement:CategoryInfo];
            TBXMLElement *categoryName = [TBXML childElementNamed:@"categoryName" parentElement:CategoryInfo];
            
            [prod_categoryId addObject:[TBXML textForElement:categoryID]];
            [prod_categoryName addObject:[TBXML textForElement:categoryName]];
            
            TBXMLElement *hDDetailsList = [TBXML childElementNamed:@"hDDetailsList" parentElement:CategoryInfo];
            
            TBXMLElement *HotDealsDetails = [TBXML childElementNamed:@"HotDealsDetails" parentElement:hDDetailsList];
            
            prod_hotDealName[i] = [[NSMutableArray alloc]init];
            prod_hotDealId[i] = [[NSMutableArray alloc]init];
            prod_hotDealImagePath[i] = [[NSMutableArray alloc]init];
            prod_hdDiscountAmount[i] = [[NSMutableArray alloc]init];
            prod_hdExpDate[i] = [[NSMutableArray alloc]init];
            prod_hdGalId[i] = [[NSMutableArray alloc]init];
            prod_hdDesc[i] = [[NSMutableArray alloc]init];
            
            while (HotDealsDetails!=nil) {
                
                TBXMLElement *hotDealName = [TBXML childElementNamed:@"hotDealName" parentElement:HotDealsDetails];
                TBXMLElement *hotDealId = [TBXML childElementNamed:@"hotDealId" parentElement:HotDealsDetails];
                TBXMLElement *hotDealImagePath = [TBXML childElementNamed:@"hotDealImagePath" parentElement:HotDealsDetails];
                TBXMLElement *hDDiscountAmount = [TBXML childElementNamed:@"hDDiscountAmount" parentElement:HotDealsDetails];
                TBXMLElement *hDEndDate = [TBXML childElementNamed:@"hDExpDate" parentElement:HotDealsDetails];
                TBXMLElement *hDGallId = [TBXML childElementNamed:@"hDGallId" parentElement:HotDealsDetails];
                TBXMLElement *hDDesc = [TBXML childElementNamed:@"hDDesc" parentElement:HotDealsDetails];
                
                if (hotDealName)
                    [prod_hotDealName[i] addObject:[TBXML textForElement:hotDealName]];
                else
                    [prod_hotDealName[i] addObject:@" "];
                if (hotDealId)
                    [prod_hotDealId[i] addObject:[TBXML textForElement:hotDealId]];
                else
                    [prod_hotDealId[i] addObject:@" "];
                
                if (hotDealImagePath)
                    [prod_hotDealImagePath[i] addObject:[TBXML textForElement:hotDealImagePath]];
                else
                    [prod_hotDealImagePath[i] addObject:@" "];
                
                if (hDDiscountAmount)
                    [prod_hdDiscountAmount[i] addObject:[TBXML textForElement:hDDiscountAmount]];
                else
                    [prod_hdDiscountAmount[i] addObject:@" "];
                
                if (hDEndDate)
                    [prod_hdExpDate[i] addObject:[TBXML textForElement:hDEndDate]];
                else
                    [prod_hdExpDate[i] addObject:@" "];
                
                if (hDGallId)
                    [prod_hdGalId[i] addObject:[TBXML textForElement:hDGallId]];
                else
                    [prod_hdGalId[i] addObject:@" "];
                
                if (hDDesc)
                    [prod_hdDesc[i] addObject:[TBXML textForElement:hDDesc]];
                else
                    [prod_hdDesc[i] addObject:@" "];
                
                HotDealsDetails = [TBXML nextSiblingNamed:@"HotDealsDetails" searchFromElement:HotDealsDetails];
            }
            
            CategoryInfo = [TBXML nextSiblingNamed:@"CategoryInfo" searchFromElement:CategoryInfo];
            i++;
        }
    }
    else{
        
        TBXMLElement *responseElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        responseText = [[TBXML textForElement:responseElement]copy];
        
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbXml.rootXMLElement];
        if (mainMenuIdElement!=nil){
            [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
    }
    [defaults setBool:NO forKey:@"ViewMore"];
}
//HD by products

#pragma mark refresh arrays
-(void)removeObjectsOfCouponLocArrays{
    
    if ([loc_retailerId count]){
        
        for (int i = 0; i<[loc_retailerId count]; i++) {
            
            [loc_couponId[i] removeAllObjects];
            [loc_couponName[i] removeAllObjects];
            [loc_couponDiscountAmount[i] removeAllObjects];
            [loc_couponExpireDate[i] removeAllObjects];
            [loc_couponImagePath[i] removeAllObjects];
            [loc_coupDesc[i] removeAllObjects];
            [loc_userCoupGallId[i] removeAllObjects];
        }
        [loc_retailerId removeAllObjects];
        [loc_retailerName removeAllObjects];
    }
}

-(void)removeObjectsOfCouponProdArrays{
    
    if ([prod_categoryId count]){
        for (int i = 0; i<[prod_categoryId count]; i++) {
            
            [prod_couponId[i] removeAllObjects];
            [prod_couponName[i] removeAllObjects];
            [prod_couponDiscountAmount[i] removeAllObjects];
            [prod_couponExpireDate[i] removeAllObjects];
            [prod_couponImagePath[i] removeAllObjects];
            [prod_coupDesc[i] removeAllObjects];
            [prod_userCoupGallId[i] removeAllObjects];
        }
        [prod_categoryId removeAllObjects];
        [prod_categoryName removeAllObjects];
    }
}

-(void)removeObjectsOfHDLocArrays{
    
    if ([loc_retailerId count]){
        for (int i = 0; i< [loc_retailerId count]; i++) {
            
            [loc_hotDealName[i] removeAllObjects];
            [loc_hotDealId[i] removeAllObjects];
            [loc_hotDealImagePath[i] removeAllObjects];
            [loc_hdDiscountAmount[i] removeAllObjects];
            [loc_hdExpDate[i] removeAllObjects];
            [loc_hdGalId[i] removeAllObjects];
            [loc_hdDesc[i] removeAllObjects];
        }
        [loc_retailerId removeAllObjects];
        [loc_retailerName removeAllObjects];
    }
}
-(void)removeObjectsOfHDProdArrays{
    
    if ([prod_categoryId count]){
        for (int i = 0; i< [prod_categoryId count]; i++) {
            
            [prod_hotDealName[i] removeAllObjects];
            [prod_hotDealId[i] removeAllObjects];
            [prod_hotDealImagePath[i] removeAllObjects];
            [prod_hdDiscountAmount[i] removeAllObjects];
            [prod_hdExpDate[i] removeAllObjects];
            [prod_hdGalId[i] removeAllObjects];
            [prod_hdDesc[i] removeAllObjects];
        }
        [prod_categoryId removeAllObjects];
        [prod_categoryName removeAllObjects];
    }
}

-(void)releaseAllArrays{
    
    //coupon location arrays
    if ([loc_retailerId count] && [loc_couponId[0] count]){
        
        for (int i = 0; i<[loc_retailerId count]; i++) {
            
            [loc_couponId[i] removeAllObjects];
            [loc_couponName[i] removeAllObjects];
            [loc_couponDiscountAmount[i] removeAllObjects];
            [loc_couponExpireDate[i] removeAllObjects];
            [loc_couponImagePath[i] removeAllObjects];
            [loc_coupDesc[i] removeAllObjects];
            [loc_userCoupGallId[i] removeAllObjects];
        }
        [loc_retailerId removeAllObjects];
        [loc_retailerName removeAllObjects];
    }
    
    //coupon products arrays
    if ([prod_categoryId count] && [prod_couponId[0] count]){
        for (int i = 0; i<[prod_categoryId count]; i++) {
            
            [prod_couponId[i] removeAllObjects];
            [prod_couponName[i] removeAllObjects];
            [prod_couponDiscountAmount[i] removeAllObjects];
            [prod_couponExpireDate[i] removeAllObjects];
            [prod_couponImagePath[i] removeAllObjects];
            [prod_coupDesc[i] removeAllObjects];
            [prod_userCoupGallId[i] removeAllObjects];
        }
        [prod_categoryId removeAllObjects];
        [prod_categoryName removeAllObjects];
    }
    
    //HD products arrays
    if ([prod_categoryId count]){
        for (int i = 0; i< [prod_categoryId count]; i++) {
            
            [prod_hotDealName[i] removeAllObjects];
            [prod_hotDealId[i] removeAllObjects];
            [prod_hotDealImagePath[i] removeAllObjects];
            [prod_hdDiscountAmount[i] removeAllObjects];
            [prod_hdExpDate[i] removeAllObjects];
            [prod_hdGalId[i] removeAllObjects];
            [prod_hdDesc[i] removeAllObjects];
        }
        [prod_categoryId removeAllObjects];
        [prod_categoryName removeAllObjects];
    }
    
    //HD location arrays
    if ([loc_retailerId count]){
        for (int i = 0; i< [loc_retailerId count]; i++) {
            
            [loc_hotDealName[i] removeAllObjects];
            [loc_hotDealId[i] removeAllObjects];
            [loc_hotDealImagePath[i] removeAllObjects];
            [loc_hdDiscountAmount[i] removeAllObjects];
            [loc_hdExpDate[i] removeAllObjects];
            [loc_hdGalId[i] removeAllObjects];
            [loc_hdDesc[i] removeAllObjects];
        }
        [loc_retailerId removeAllObjects];
        [loc_retailerName removeAllObjects];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

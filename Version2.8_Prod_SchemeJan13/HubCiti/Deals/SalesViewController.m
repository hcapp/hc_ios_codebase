//
//  SalesViewController.m
//  HubCiti
//
//  Created by Kitty on 16/04/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "SalesViewController.h"
#import "salesProductDO.h"
#import "ProductPage.h"
#import "MainMenuViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
@interface SalesViewController ()

@end

@implementation SalesViewController{
    UIActivityIndicatorView *loading;
}
@synthesize salesProductArrays;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Sales";
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    ////[mainPage release];
    
    
    loading = nil;
    //customize back button
   
    self.navigationItem.hidesBackButton = YES;
    
    
    salesTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [salesTableView reloadData];
}

-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}
/*
 *
 Navigate back to Main Menu screen page
 *
 */-(void)returnToMainPage:(id)sender
{
    //return to main page
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


// Parse Sales Product Details
-(void)parse_getproducts:(NSString *)responseXml{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbxml.rootXMLElement];
        NextPageFlagSales = 0;
        if(nextPageElement)
            NextPageFlagSales = (int)[[TBXML textForElement:nextPageElement]integerValue];
        
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        
        TBXMLElement *productDetailElement = [TBXML childElementNamed:@"ProductDetail" parentElement:tbxml.rootXMLElement];
        while (productDetailElement != nil)
        {
            salesProductDO *obj_salesProductDO = [[salesProductDO alloc]init];
            
            TBXMLElement *productNameElement = [TBXML childElementNamed:@"productName" parentElement:productDetailElement];
            TBXMLElement *productDescElement = [TBXML childElementNamed:@"productDescription" parentElement:productDetailElement];
            TBXMLElement *productIdElement = [TBXML childElementNamed:@"productId" parentElement:productDetailElement];
            TBXMLElement *productImageElement = [TBXML childElementNamed:@"imagePath" parentElement:productDetailElement];
            TBXMLElement *rowNumberElement = [TBXML childElementNamed:@"rowNumber" parentElement:productDetailElement];
            TBXMLElement *regularPriceElement = [TBXML childElementNamed:@"regularPrice" parentElement:productDetailElement];
            TBXMLElement *salePriceElement = [TBXML childElementNamed:@"salePrice" parentElement:productDetailElement];
            TBXMLElement *discountElement = [TBXML childElementNamed:@"discount" parentElement:productDetailElement];
            
            TBXMLElement *saleListIDElement = [TBXML childElementNamed:@"saleListID" parentElement:productDetailElement];
            
            if(discountElement)
                obj_salesProductDO.discount = [TBXML textForElement:discountElement];
            
            if(productNameElement)
                obj_salesProductDO.productName = [TBXML textForElement:productNameElement];
            
            if(productDescElement)
                obj_salesProductDO.productDescription = [TBXML textForElement:productDescElement];
            
            if(productIdElement)
                obj_salesProductDO.productId = [TBXML textForElement:productIdElement];
            
            if(productImageElement)
                obj_salesProductDO.imagePath = [TBXML textForElement:productImageElement];
            
            if(rowNumberElement)
                obj_salesProductDO.rowNumber = [TBXML textForElement:rowNumberElement];
            
            if(regularPriceElement)
                obj_salesProductDO.regularPrice = [TBXML textForElement:regularPriceElement];
            
            if(salePriceElement)
                obj_salesProductDO.salePrice = [TBXML textForElement:salePriceElement];
            
            if(saleListIDElement)
                obj_salesProductDO.saleListID = [TBXML textForElement:saleListIDElement];
            
            [salesProductArrays addObject:obj_salesProductDO];
            // [obj_salesProductDO release];
            
            productDetailElement = [TBXML nextSiblingNamed:@"ProductDetail" searchFromElement:productDetailElement];
        }
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        [loading stopAnimating];
        ReleaseAndNilify(loading);
        
        
    }
    
    [salesTableView reloadData];
    [defaults setBool:NO forKey:@"ViewMore"];
}

-(void)request_getproducts{
    iWebRequestState = GETPRODUCTS;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[defaults valueForKey:@"retailLocationID"]];
    [requestStr appendFormat:@"<lastVisitedProductNo>%lu</lastVisitedProductNo>",(unsigned long)[salesProductArrays count]];
    [requestStr appendFormat:@"<retailId>%@</retailId>",[defaults valueForKey:KEY_RETAILERID]];
    
    // For User Tracking
    if ([defaults valueForKey:KEY_RLISTID])
        [requestStr appendFormat:@"<retListId>%@</retListId>",[defaults valueForKey:KEY_RLISTID]];
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></ProductDetailsRequest>",[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getProducts",BASE_URL];
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
        
        
        [self parse_getproducts:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    }
    
    // [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    //[requestStr release];
    
}

-(void)responseData:(NSString *) response{
    switch (iWebRequestState)
    {
        case GETPRODUCTS:
        {
            //            [defaults setValue:response forKey:KEY_RESPONSEXML];
            //            FundraiserListViewController *lSpl = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:nil];
            //            lSpl.fundDeal = YES;
            //            [self.navigationController pushViewController:lSpl animated:YES];
            //            ReleaseAndNilify(lSpl);
            
            [self parse_getproducts:response];
        }
            break;
        default:
            break;
    }
}

#pragma mark tableView delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    DLog(@"%lu",(unsigned long)[salesProductArrays count]);
    if(NextPageFlagSales==0)
        return [salesProductArrays count];
    else
        return [salesProductArrays count]+1;
    
    
    //
    
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        return 70.0;
    }
    return 60.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellSales";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }
        
        cell.userInteractionEnabled = YES;
    }
    SdImageView *asyncImageView = nil;
    UILabel *label = nil;
    UILabel *detailLabel = nil;
    float  detailLabelFont = 12.0, salePriceFont = 12.0, regPriceFont = 12.0, discountPriceFont = 12.0;
    float  label3Font = 14.0;
    float asyncImageViewWidth = 50.0, asyncImageViewHeight = 50.0, labelXValue = 60.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        //textLabelFont = 17.0;
        detailLabelFont = 17.0;
        //labelFont = 20.0;
       // label2Font = 21.0;
        label3Font = 19.0;
        
       // priceLabelFont = 17.0;
        salePriceFont = 17.0;
        regPriceFont = 17.0;
        discountPriceFont = 17.0;
        
        asyncImageViewWidth = 60.0;
        asyncImageViewHeight = 60.0;
        labelXValue = 70.0;
    }
    
    
    
    
    
    //             if(!salesFlag)
    //             {
    //             CGRect frame;
    //             frame.origin.x = 0;
    //             frame.origin.y = 16;
    //             frame.size.width = SCREEN_WIDTH;//width 320
    //             frame.size.height = 24;
    //             label = [[[UILabel alloc] initWithFrame:frame] ;
    //             label.font = [UIFont boldSystemFontOfSize:label2Font];
    //             label.textAlignment = NSTextAlignmentCenter;
    //             label.textColor = [UIColor darkGrayColor];
    //             label.text = NSLocalizedString(@"No Records Found",@"No Records Found");
    //             cell.userInteractionEnabled = NO;
    //             cell.accessoryType = UITableViewCellAccessoryNone;
    //             [cell.contentView addSubview:label];
    //
    //             }
    //             else
    
    if(NextPageFlagSales==1 && indexPath.row == [salesProductArrays count])
    {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
        //        CGRect frame;
        //        frame.origin.x = 0;
        //        frame.origin.y = 16;
        //        frame.size.width = SCREEN_WIDTH;//width 320
        //        frame.size.height = 24;
        //        label = [[[UILabel alloc] initWithFrame:frame] ;
        //        label.font = [UIFont boldSystemFontOfSize:label2Font];
        //        label.textAlignment = NSTextAlignmentCenter;
        //        label.textColor = [UIColor colorWithRGBInt:0x112e72];
        //        label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
        //        [cell.contentView addSubview:label];
    }
    else
    {
        salesProductDO *obj_salesProductDO = [salesProductArrays objectAtIndex:indexPath.row];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        
        NSString *imgPathStr = obj_salesProductDO.imagePath;
        if (![imgPathStr isEqualToString:@"N/A"]) {
            
            asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(5.0, 5.0, asyncImageViewWidth, asyncImageViewHeight)];
            asyncImageView.contentMode = UIViewContentModeScaleToFill;
            asyncImageView.backgroundColor = [UIColor clearColor];
            [asyncImageView loadImage:imgPathStr];
            [cell.contentView addSubview:asyncImageView];
            ReleaseAndNilify(asyncImageView);
        }
        
        
        //add textLabel to cell
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(labelXValue, 2, SCREEN_WIDTH - 70, 30)];//width 250
        label.font = [UIFont boldSystemFontOfSize:label3Font];
        label.backgroundColor = [UIColor clearColor];
        label.text = [NSString stringWithFormat:@"%@", obj_salesProductDO.productName];
        [cell.contentView addSubview:label];
        ReleaseAndNilify(label);
        
        
        //add detailTextLabels to cell
        
        if(obj_salesProductDO.regularPrice)
        {
            
            NSString *str1 = obj_salesProductDO.regularPrice;
            NSString *str2 = obj_salesProductDO.salePrice;
            NSString *str3 = @"N/A";
            if (obj_salesProductDO.discount)
                str3 = obj_salesProductDO.discount;
            
            if (![str1 isEqualToString:@"N/A"])//means reg price not N/A
            {
                UILabel *regPrice = [[UILabel alloc] initWithFrame:CGRectMake(labelXValue, 32, 100, 30)];
                regPrice.font = [UIFont boldSystemFontOfSize:regPriceFont];
                regPrice.textColor = [UIColor darkGrayColor];
                regPrice.numberOfLines = 1;
                regPrice.backgroundColor = [UIColor clearColor];
                regPrice.text = obj_salesProductDO.regularPrice;
                [cell.contentView addSubview:regPrice];
                ReleaseAndNilify(regPrice);
                
                if (![str2 isEqualToString:@"N/A"]){//add sale price label
                    
                    UILabel *salePrice = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 160, 32, 150, 30)];
                    salePrice.font = [UIFont boldSystemFontOfSize:salePriceFont];
                    salePrice.textColor = [UIColor redColor];
                    salePrice.numberOfLines = 1;
                    salePrice.backgroundColor = [UIColor clearColor];
                    salePrice.text = [NSString stringWithFormat:@"Sale: %@",obj_salesProductDO.salePrice];
                    [cell.contentView addSubview:salePrice];
                    ReleaseAndNilify(salePrice);
                }
                
                else if (![str3 isEqualToString:@"N/A"]){//add discount price label
                    
                    UILabel *discountPrice = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 160, 32, 150, 30)];
                    discountPrice.font = [UIFont boldSystemFontOfSize:discountPriceFont];
                    discountPrice.textColor = [UIColor redColor];
                    discountPrice.numberOfLines = 1;
                    discountPrice.backgroundColor = [UIColor clearColor];
                    discountPrice.text = [NSString stringWithFormat:@"Discount: %@",obj_salesProductDO.discount];
                    [cell.contentView addSubview:discountPrice];
                    ReleaseAndNilify(discountPrice);
                }
            }
            
            else //check N/A for sale price and discount price, display suitable label
            {
                if (![str2 isEqualToString:@"N/A"]){//add sale price label
                    
                    UILabel *salePrice = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 160, 32, 150, 30)];
                    salePrice.font = [UIFont boldSystemFontOfSize:salePriceFont];
                    salePrice.textColor = [UIColor redColor];
                    salePrice.numberOfLines = 1;
                    salePrice.backgroundColor = [UIColor clearColor];
                    salePrice.text = [NSString stringWithFormat:@"Sale: %@",obj_salesProductDO.salePrice];
                    [cell.contentView addSubview:salePrice];
                    ReleaseAndNilify(salePrice);
                    
                }
                
                else if (![str3 isEqualToString:@"N/A"]){//add discount price label
                    
                    UILabel *discountPrice = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 160, 32, 150, 30)];
                    discountPrice.font = [UIFont boldSystemFontOfSize:discountPriceFont];
                    discountPrice.textColor = [UIColor redColor];
                    discountPrice.numberOfLines = 1;
                    discountPrice.backgroundColor = [UIColor clearColor];
                    discountPrice.text = [NSString stringWithFormat:@"Discount: %@",obj_salesProductDO.discount];
                    [cell.contentView addSubview:discountPrice];
                    ReleaseAndNilify(discountPrice);
                }
            }
        }
        
        else {
            cell.accessoryType = UITableViewCellAccessoryNone;
            if (obj_salesProductDO.productDescription)
            {
                label.font = [UIFont boldSystemFontOfSize:label3Font];
                detailLabel.text = obj_salesProductDO.productDescription;
                detailLabel.textColor = [UIColor darkGrayColor];
                detailLabel.font = [UIFont boldSystemFontOfSize:detailLabelFont];
            }
            else
            {
                detailLabel.hidden = YES;
                label.frame = CGRectMake(10, 15, SCREEN_WIDTH - 20, 30);//width 300
                label.font = [UIFont boldSystemFontOfSize:label3Font];
            }
            
        }
    }
    
    
    
    
    //
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //add the image
    //                            asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(5.0, 5.0, asyncImageViewWidth, asyncImageViewHeight)];
    //                            asyncImageView.contentMode = UIViewContentModeScaleToFill;
    //                            asyncImageView.backgroundColor = [UIColor clearColor];
    //                            [asyncImageView loadImagefrommainBundel:@"infoIcon.png"];
    //                            [cell.contentView addSubview:asyncImageView];
    //                            ReleaseAndNilify(asyncImageView);
    
    //add the cell textLabel
    if(indexPath.row<[salesProductArrays count]){
        label = [[UILabel alloc]initWithFrame:CGRectMake(labelXValue, 7, SCREEN_WIDTH - 70, 40)];//width 250
        label.font = [UIFont boldSystemFontOfSize:label3Font];
        label.numberOfLines = 2;
        label.text = [salesProductArrays objectAtIndex:indexPath.row];
        [cell.contentView addSubview:label];
        ReleaseAndNilify(label);
    }
    //
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(NextPageFlagSales==1 && indexPath.row == [salesProductArrays count]) {
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"] && NextPageFlagSales !=0) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self request_getproducts];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [salesTableView reloadData];
                });
            });
            
        }
    }
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [defaults setBool:NO forKey:@"ViewMore"];
    
    
    //        if(NextPageFlagSales==1 && indexPath.row == [salesProductArrays count])
    //            [self request_getproducts];
    
    //  else
    
    // {
    
    salesProductDO *obj_salesProductDO = [salesProductArrays objectAtIndex:indexPath.row];
    
    if(obj_salesProductDO.saleListID)
        
        [defaults setValue:obj_salesProductDO.saleListID forKey:@"saleListID"];
    
    if(obj_salesProductDO.productId)
        
        [defaults setValue:obj_salesProductDO.productId forKey:KEY_PRODUCTID];
    
    
    [defaults setValue:@"0" forKey:KEY_SCANTYPEID];
    
    
    ProductPage *prodPage = [[ProductPage alloc]initWithNibName:@"ProductPage" bundle:[NSBundle mainBundle]];
    
    [self.navigationController pushViewController:prodPage animated:NO];
    //[prodPage release];
    
    // }
    
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

//
//  SalesViewController.h
//  HubCiti
//
//  Created by Kitty on 16/04/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CurrentSpecials.h"

@interface SalesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,CustomizedNavControllerDelegate>{
    int NextPageFlagSales;
    UITableView *salesTableView;
    webServicesSpecialOfferState iWebRequestState;
}

@property (nonatomic,strong) NSMutableArray *salesProductArrays;

@end

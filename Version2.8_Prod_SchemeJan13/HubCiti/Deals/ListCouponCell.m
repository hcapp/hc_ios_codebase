//
//  ListCouponCell.m
//  HubCiti
//
//  Created by Lakshmi H R on 11/30/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "ListCouponCell.h"

@implementation ListCouponCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//@property (strong, nonatomic) IBOutlet SdImageView *coupon_Image;
//@property (strong, nonatomic) IBOutlet UILabel *coupon_Name;
//;
//@property (strong, nonatomic) IBOutlet UILabel *coupon_Offer_Price;
//@property (strong, nonatomic) IBOutlet UILabel *couponLocation_Name;
//@property (strong, nonatomic) IBOutlet UILabel *coupon_Location_Distance;
//@property (strong, nonatomic) IBOutlet UILabel *coupon_Location_Count;
-(void)updateUiWithRetailerDetails:(CouponsRetailerDetails *)retailerDetails {
    
    self.coupon_Name.text = nil;
    self.coupon_Image.image = nil;
    self.couponLocation_Name.text = nil;
    self.coupon_Location_Count.text = nil;
    self.coupon_Offer_Price.text = nil;
    self.coupon_Location_Distance.text = nil;
    
    if (!retailerDetails) {
        UIImageView *viewMore = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_tick.png"]];
        viewMore.frame = CGRectMake(0, 0, VARIABLE_WIDTH(60), VARIABLE_HEIGHT(60));
        self.coupon_Image.image = viewMore.image;
    }
    else {
        if(retailerDetails.couponName.length > 0)
        {
            self.coupon_Name.text = retailerDetails.couponName;
        }
        if(retailerDetails.couponImagePath.length > 0)
        {
            [self.coupon_Image loadImage:retailerDetails.couponImagePath];
        }
        if(retailerDetails.retName.length > 0)
        {
            self.couponLocation_Name.text = retailerDetails.retName;
        }
        if([retailerDetails.counts intValue]>0)
        {
            self.coupon_Location_Count.text = [NSString stringWithFormat:@"%d participating locations available",[retailerDetails.counts intValue]];
        }
        if(retailerDetails.bannerTitle.length >0)
        {
            self.coupon_Offer_Price.text = retailerDetails.bannerTitle;
        }
        if(retailerDetails.distance.length > 0 && [retailerDetails.counts intValue] > 1)
        {
            self.coupon_Location_Distance.text = [NSString stringWithFormat:@"Closest location %@", retailerDetails.distance];
        }
        else if (retailerDetails.distance.length > 0)
        {
            self.coupon_Location_Distance.text = retailerDetails.distance;
        }
    }
    NSLog(@"Image for Pavan: %f, %f", self.coupon_Image.frame.size.width,self.coupon_Image.frame.size.height);

}

@end

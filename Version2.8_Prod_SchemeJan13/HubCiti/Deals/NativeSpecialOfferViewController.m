//
//  NativeSpecialOfferViewController.m
//  HubCiti
//
//  Created by Keshava on 6/3/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "NativeSpecialOfferViewController.h"
#import "WebBrowserViewController.h"
#import "RetailerData.h"
#import "MainMenuViewController.h"
#import "RetailerSummaryViewController.h"
#import "EmailShareViewController.h"

@interface NativeSpecialOfferViewController ()

@end

@implementation NativeSpecialOfferViewController

@synthesize titleLbl,specialOfferImage,dateLbl,shortDesc,locationBtn,LongDesc,specialDetails,tableDetails,locationsTable,promoLbl,splOfferView,cancelBtn;

//Used as a cell Identifier
 static NSString *CellIdentifier= @"LocationList";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton addTarget:self action:@selector(shareDetails) forControlEvents:UIControlEventTouchUpInside];
    [shareButton setBackgroundImage:[UIImage imageNamed:@"shareBtn_down.png"] forState:UIControlStateNormal];
    [shareButton setBackgroundImage:[UIImage imageNamed:@"shareBtn_up.png"] forState:UIControlStateHighlighted];
    shareButton.frame=CGRectMake(0, 0, 38 , 30);
    
    UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:shareButton];
    
    
    
    //customize back button
    UIButton *backBtn = [UtilityManager customizeBackButton];
    
    [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    //self.navigationItem.leftBarButtonItem = back;
    self.navigationItem.leftItemsSupplementBackButton=YES;
    self.navigationItem.leftBarButtonItems=@[back,item1];
    self.navigationItem.hidesBackButton = YES;
    [shareButton release];
    [back release];
    
    
    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = mainPage;
    [mainPage release];
    
    
    specialDetails = [[NSMutableDictionary alloc]init];
    tableDetails =[[NSMutableArray alloc]init];
    [self getSpecialOfferDetails];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Navigation bar button actions
-(void)popBackToPreviousPage{
    
[self.navigationController popViewControllerAnimated:NO];
}

-(void)returnToMainPage:(id)sender{
    [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
}



#pragma mark set the layout view Methods

-(void)setSepcialOfferDetailsView
{
    splOfferView.hidden=YES;
    
    titleLbl = [[UILabel alloc]init];
    titleLbl.backgroundColor=[UIColor lightGrayColor];
    titleLbl.text=[specialDetails valueForKey:@"Label"];
    titleLbl.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:titleLbl];
    
    specialOfferImage = [[UIImageView alloc]initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[specialDetails valueForKey:@"splURL"]]]]];
    [self.view addSubview:specialOfferImage];
    
    dateLbl = [[UILabel alloc]init];
    dateLbl.backgroundColor=[UIColor lightGrayColor];
    dateLbl.text=[specialDetails valueForKey:@"dateLabel"];
    dateLbl.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:dateLbl];
    
    shortDesc = [[UIWebView alloc]init];
    shortDesc.delegate=self;
    shortDesc.layer.cornerRadius = 5.0;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        [shortDesc loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#0000;'>%@",[specialDetails valueForKey:@"shortDesc"]] baseURL:nil];
    }
    else
    {
        [shortDesc loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:17px;font-family:Helvetica;color:#0000;'>%@",[specialDetails valueForKey:@"shortDesc"]] baseURL:nil];
    }

    [self.view addSubview:shortDesc];
    
    LongDesc = [[UIWebView alloc]init];
    LongDesc.delegate=self;
    LongDesc.layer.cornerRadius = 5.0;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        [LongDesc loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#0000;'>%@",[specialDetails valueForKey:@"longDesc"]] baseURL:nil];
    }
    else
    {
        [LongDesc loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:17px;font-family:Helvetica;color:#0000;'>%@",[specialDetails valueForKey:@"longDesc"]] baseURL:nil];
    }
    [self.view addSubview:LongDesc];
    
    if ([specialDetails valueForKey:@"promoLabel"]!=nil) {
        promoLbl=[[UILabel alloc]init];
        promoLbl.backgroundColor=[UIColor lightGrayColor];
        promoLbl.text=[specialDetails valueForKey:@"promoLabel"];
        promoLbl.font=[UIFont systemFontOfSize:10.0];
        promoLbl.textAlignment=NSTextAlignmentCenter;
        [self.view addSubview:promoLbl];
    }
    
  
    
    
    locationBtn = [[UIButton alloc]init];
    [locationBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [locationBtn setBackgroundImage:[UIImage imageNamed:@"greyBtnBg"] forState:UIControlStateNormal];
    [locationBtn setTitle:@"Location" forState:UIControlStateNormal];
    [locationBtn.titleLabel setTextAlignment: NSTextAlignmentCenter];
    [locationBtn addTarget:self action:@selector(getLocationDetails) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:locationBtn];
    
    [self setSpecialOfferConstraints];
}

-(void)setSpecialOfferConstraints
{
    float yVal= self.navigationController.navigationBar.frame.size.height;
    float gap =0.01 *(SCREEN_HEIGHT-yVal);;
    float lblBtnWidth= SCREEN_WIDTH-2;
    float lblHeight = 0.06 *(SCREEN_HEIGHT-yVal);
    float btnHeight = 0.08 *(SCREEN_HEIGHT-yVal);
    float imageHeight = 0.25 *(SCREEN_HEIGHT-yVal);
    float shortHeight = 0.12*(SCREEN_HEIGHT-yVal);
    float longHeight;
    if ([specialDetails valueForKey:@"promoLabel"]!=nil) {
       longHeight= 0.30*(SCREEN_HEIGHT-yVal);
    }
    else
    {
        longHeight= 0.25*(SCREEN_HEIGHT-yVal);
    }
    
    float promoLblHeight = 0.04 *(SCREEN_HEIGHT-yVal);

   
    
    titleLbl.translatesAutoresizingMaskIntoConstraints=NO;
    specialOfferImage.translatesAutoresizingMaskIntoConstraints=NO;
    dateLbl.translatesAutoresizingMaskIntoConstraints=NO;
    shortDesc.translatesAutoresizingMaskIntoConstraints=NO;
    LongDesc.translatesAutoresizingMaskIntoConstraints=NO;
    locationBtn.translatesAutoresizingMaskIntoConstraints=NO;
    promoLbl.translatesAutoresizingMaskIntoConstraints=NO;
    
    NSDictionary *viewsDictionary = @{@"TL":titleLbl,@"SI":specialOfferImage,@"DL":dateLbl,@"SD":shortDesc,@"LD":LongDesc,@"LB":locationBtn,@"PL":promoLbl};
    
    //Horizontal Constraints Added
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-1-[TL(==%f)]-1-|",lblBtnWidth]  options:0 metrics:nil views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-1-[DL(==%f)]-1-|",lblBtnWidth]  options:0 metrics:nil views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-1-[SD(==%f)]-1-|",lblBtnWidth]  options:0 metrics:nil views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-1-[LD(==%f)]-1-|",lblBtnWidth]  options:0 metrics:nil views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-1-[LB(==%f)]-1-|",lblBtnWidth]  options:0 metrics:nil views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[SI(==%f)]-(%f)-|",(SCREEN_WIDTH*0.15),(SCREEN_WIDTH*0.70),(SCREEN_WIDTH*0.15)]  options:0 metrics:nil views:viewsDictionary]];
    
    //Vertical Constraints Added
    if ([specialDetails valueForKey:@"promoLabel"]!=nil) {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-1-[PL(==%f)]-1-|",lblBtnWidth]  options:0 metrics:nil views:viewsDictionary]];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-[TL(==%f)]-(%f)-[SI(==%f)]-(%f)-[DL(==%f)]-(%f)-[SD(==%f)]-(%f)-[LD(==%f)]-(%f)-[PL(==%f)]-[LB(==%f)]-|",lblHeight,gap,imageHeight,gap,lblHeight,gap,shortHeight,gap,longHeight,gap,promoLblHeight,btnHeight]  options:0 metrics:nil views:viewsDictionary]];
    }
    else
    {
    
     [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-[TL(==%f)]-(%f)-[SI(==%f)]-(%f)-[DL(==%f)]-(%f)-[SD(==%f)]-(%f)-[LD(==%f)]-(%f)-[LB(==%f)]-|",lblHeight,gap,imageHeight,gap,lblHeight,gap,shortHeight,gap,longHeight,gap,btnHeight]  options:0 metrics:nil views:viewsDictionary]];
    }
    
}

-(void)setSpecialWebView
{
    
    [defaults  setObject:[specialDetails valueForKey:@"extURL"] forKey:KEY_URL];
    [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
    shareFlag=YES;
    [defaults setObject:[defaults valueForKey:@"nativePageId"] forKey:KEY_PAGEID];
    WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:urlDetail animated:NO];
    [urlDetail release];
  
}

#pragma mark Request Methods

-(void)getSpecialOfferDetails
{
    NSMutableString *xmlStr = [[NSMutableString alloc]init];
    
    [xmlStr appendFormat:@"<RetailerDetail><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    [xmlStr appendFormat:@"<pageId>%@</pageId>",[defaults valueForKey:@"nativePageId"]];
    [xmlStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[defaults valueForKey:@"retailLocationId"]];
    [xmlStr appendFormat:@"<retailerId>%@</retailerId></RetailerDetail>",[defaults valueForKey:@"retailerId"]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getspecialofferdetails",BASE_URL];
    
    
    NSString *resp = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    
    [self parseSpecialOfferDetails :resp];
    ReleaseAndNilify(xmlStr);

    
}

-(void)getLocationDetails
{
    NSMutableString *xmlStr = [[NSMutableString alloc]init];
    
    [xmlStr appendFormat:@"<RetailerDetail><userId>%@</userId>",[defaults  valueForKey:KEY_USERID]];
    [xmlStr appendFormat:@"<retailerId>%@</retailerId>",[defaults valueForKey:@"retailerId"]];
    [xmlStr appendFormat:@"<pageId>%@</pageId></RetailerDetail>",[defaults valueForKey:@"nativePageId"]];
   
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getspecialoffloclist",BASE_URL];
    
    
    NSString *resp = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    
    [self parseLocationDetails :resp];
    ReleaseAndNilify(xmlStr);
}


-(void)requestRetailerSummary:(int)index
{
    RetailerData *iRetObj = [tableDetails objectAtIndex:index];
    

    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",iRetObj.retailLocId];
    [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId><scanTypeId>0</scanTypeId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    DLog(@"%@",requestStr);
    [requestStr appendFormat:@"<retailerId>%@</retailerId></RetailerDetail>",iRetObj.retailId];
    
    NSMutableString *urlString = [[BASE_URL mutableCopy] autorelease];
    [urlString appendString:@"thislocation/retsummary"];
    
    NSString *resp = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    [self parse_RetSummary:resp];
                      
    ReleaseAndNilify(requestStr);

}

#pragma mark Parse Methods

-(void)parseSpecialOfferDetails:(NSString *)responseString
{
    
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
      TBXMLElement *extLinkElement = [TBXML childElementNamed:@"extLinkFlag" parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:extLinkElement] isEqualToString:@"1"]) {
            
            TBXMLElement *extLinkURLElement = [TBXML childElementNamed:@"externalLink" parentElement:tbxml.rootXMLElement];
            if (extLinkURLElement) {
               [specialDetails setObject:[TBXML textForElement:extLinkURLElement] forKey:@"extURL"];
            }
            [ self setSpecialWebView];
        }
        else
        {
         TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retDetailList" parentElement:tbxml.rootXMLElement];
        
        if(retailerDetailElement)
        {
            TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
            while (RetailerDetailElement)
            {
            
          
            TBXMLElement *titleElement = [TBXML childElementNamed:@"pageTitle" parentElement:RetailerDetailElement];
            TBXMLElement *sDecElement = [TBXML childElementNamed:@"sDescription" parentElement:RetailerDetailElement];
            TBXMLElement *lDecrElement = [TBXML childElementNamed:@"longDescription" parentElement:RetailerDetailElement];
            TBXMLElement *imageElement = [TBXML childElementNamed:@"retImagePath" parentElement:RetailerDetailElement];
            TBXMLElement *dateElement = [TBXML childElementNamed:@"startDate" parentElement:RetailerDetailElement];
             TBXMLElement *promoElement = [TBXML childElementNamed:@"endDate" parentElement:RetailerDetailElement];
            
                
               if (titleElement) {
                    [specialDetails setObject:[TBXML textForElement:titleElement] forKey:@"Label"];
                }
                
                if (sDecElement) {
                    [specialDetails setObject:[TBXML textForElement:sDecElement] forKey:@"shortDesc"];
                }
                
                if (lDecrElement) {
                    [specialDetails setObject:[TBXML textForElement:lDecrElement] forKey:@"longDesc"];
                }
                
                if (dateElement) {
                    [specialDetails setObject:[TBXML textForElement:dateElement] forKey:@"dateLabel"];
                }
                
                if (imageElement) {
                    [specialDetails setObject:[TBXML textForElement:imageElement] forKey:@"splURL"];
                }
                
                if (promoElement) {
                    NSString *promo = [NSString stringWithFormat:@"Promotion Ends: %@",[TBXML textForElement:promoElement]];
                    [specialDetails setObject:promo forKey:@"promoLabel"];
                }
                
             RetailerDetailElement = [TBXML nextSiblingNamed:@"RetailerDetail" searchFromElement:RetailerDetailElement];
            }

    }
        [self setSepcialOfferDetailsView];
    }
    }
    else
    {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
            // Banner the message given by the server
        [UtilityManager showAlert:@"Info" msg:[TBXML textForElement:responseTextElement]];
        return;
        
    }
}

-(void)parseLocationDetails:(NSString *)responseString
{
    
//    responseString=@"<RetailerDetail><responseCode>10000</responseCode><responseText>Success</responseText><maxCount>4</maxCount><nextPageFlag>false</nextPageFlag><retDetailList><RetailerDetail><retailerId>1046</retailerId><retailerName>Deepthi_J</retailerName><retailLocationId>92466</retailLocationId><completeAddress>QWE2,MarbleFalls,TX,78654</completeAddress><city>MARBLE FALLS</city><state>TX</state><postalCode>78654</postalCode><rowNum>1</rowNum><retImagePath>http://localhost:8080/Images/retailer/1046/BannerAlbertso_98322.png</retImagePath></RetailerDetail><RetailerDetail><retailerId>1046</retailerId><retailerName>Deepthi_J</retailerName><retailLocationId>92617</retailLocationId><completeAddress>QWE2,MarbleFalls,TX,78654</completeAddress><city>MARBLE FALLS</city><state>TX</state><postalCode>78654</postalCode><rowNum>2</rowNum><retImagePath>http://localhost:8080/Images/retailer/1046/BannerAlbertso_98322.png</retImagePath></RetailerDetail></retDetailList></RetailerDetail>";
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retDetailList" parentElement:tbxml.rootXMLElement];
        
        if(retailerDetailElement)
        {
            TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
            while (RetailerDetailElement)
            {
                 TBXMLElement *retailIdlement = [TBXML childElementNamed:@"retailerId" parentElement:RetailerDetailElement];
                TBXMLElement *retailerNamelement = [TBXML childElementNamed:@"retailerName" parentElement:RetailerDetailElement];
                TBXMLElement *retailLocIdElement = [TBXML childElementNamed:@"retailLocationId" parentElement:RetailerDetailElement];
                 TBXMLElement *retailImagelement = [TBXML childElementNamed:@"retImagePath" parentElement:RetailerDetailElement];
                 TBXMLElement *retailAddresslement = [TBXML childElementNamed:@"completeAddress" parentElement:RetailerDetailElement];
                
                RetailerData *iRetailData=[[RetailerData alloc]init];
                
                if (retailIdlement) {
                    iRetailData.retailId=[[NSString alloc]initWithString:[TBXML textForElement:retailIdlement]];
                }
                
                if (retailerNamelement) {
                    iRetailData.retailName=[[NSString alloc]initWithString:[TBXML textForElement:retailerNamelement]];
                }
                
                if (retailLocIdElement) {
                    iRetailData.retailLocId=[[NSString alloc]initWithString:[TBXML textForElement:retailLocIdElement]];
                }
                
                if (retailImagelement) {
                    iRetailData.retailImage=[[NSString alloc]initWithString:[TBXML textForElement:retailImagelement]];
                }
                
                if (retailAddresslement) {
                    iRetailData.completeAddr=[[NSString alloc]initWithString:[TBXML textForElement:retailAddresslement]];
                }
                
                [tableDetails addObject:iRetailData];
                
                RetailerDetailElement = [TBXML nextSiblingNamed:@"RetailerDetail" searchFromElement:RetailerDetailElement];

            }

    }
        [self setLocationsTableView];
    }
    else
    {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
        // Banner the message given by the server
        [UtilityManager showAlert:@"Info" msg:[TBXML textForElement:responseTextElement]];
        return;
        
    }

    
}

-(void)parse_RetSummary:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[TBXML textForElement:responseTextElement] message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
    }
    else {
        
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:rsvc animated:NO];
        [rsvc release];
    }
}


-(void)responseData: (NSString*)response{
    
    switch (iWebRequestState) {
            
        case SPECIAL_OFFER_SHARE:
            [self parse_SpecialOfferShare:response];
            break;
        default:
            break;
    }
    
    
}

-(void)parse_SpecialOfferShare:(NSString*)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        //        TBXMLElement *retailerDetails = [TBXML childElementNamed:@"RetailersDetails" parentElement:tbXml.rootXMLElement];
        
        
        TBXMLElement *shareText=[TBXML childElementNamed:@"pageTitle" parentElement:tbXml.rootXMLElement];
        TBXMLElement *emailURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *imagePath = [TBXML childElementNamed:@"retImage" parentElement:tbXml.rootXMLElement];
        
        if (emailURL!=nil) {
            [defaults setValue:[TBXML textForElement:emailURL] forKey:KEY_HYPERLINK];
            //                NSLog(@"The link to be shared is %@",KEY_HYPERLINK);
        }
        
        if (shareText!=nil) {
            NSMutableString *msgString = [[NSMutableString alloc]init];
            if (shareFlag==YES) {
                [msgString appendFormat:@"I found this special offer page in the %@ mobile app and thought you might be interested:",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            }
            else
            {
                [msgString appendFormat:@"I found this anything page in the %@ mobile app and thought you might be interested:",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            }
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [msgString appendFormat:@"%@",shareString];
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            
        }
        
        if (imagePath!=nil) {
            [defaults setObject:[TBXML textForElement:imagePath] forKey:KEY_PRODUCTIMGPATH];
            //                NSLog(@"The product image path is %@",KEY_PRODUCTIMGPATH);
        }
        
        
        
        
        
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[TBXML textForElement:responseTextElement] message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
            [alert show];
            [alert release];
            return;
        }
        
    }
    
}

#pragma mark WebView Delegate Methods

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    
    if ([[[inRequest URL] scheme] isEqual:@"mailto"]) {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            
            NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,
                                                                                                 (CFStringRef)value,
                                                                                                 CFSTR(""),
                                                                                                 kCFStringEncodingUTF8);
                    [value autorelease];
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            [mailViewController release];
            return NO;
            
        }
        
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cannot Send",@"Cannot Send") message:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles: nil];
            [alert show];
            [alert release];
            
        }
        
        return NO;
        
    }
    
    
    if ([[inRequest URL] fragment]) {
        
        return YES;
        [inWeb stringByEvaluatingJavaScriptFromString:@"window.location.hash='#hashtag'"];
        
        return YES;
    }
    
    
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        
        NSString *url = [[inRequest URL]absoluteString];
        [defaults setValue:url forKey:KEY_URL];
        [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
        
        WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:urlDetail animated:NO];
        [urlDetail release];
        
        return NO;
    }
    
    
    
    
    
    return YES;
}




- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];// ModalViewControllerAnimated:YES];
}

#pragma mark TableView Delegate and DataSource

-(void)setLocationsTableView
{
    float offset_Y = 0.08 * (SCREEN_HEIGHT);
    float offset_X = 0.08 * (SCREEN_WIDTH);
    
    locationsTable = [[UITableView alloc]initWithFrame:CGRectMake(offset_X, offset_Y, (SCREEN_WIDTH-(2*offset_X)), (SCREEN_HEIGHT-(4*offset_Y)))];
    locationsTable.delegate=self;
    locationsTable.dataSource=self;
    locationsTable.layer.cornerRadius = 10.0;
    locationsTable.layer.borderWidth = 1.0;
    locationsTable.layer.borderColor = [UIColor blackColor].CGColor;
    [self.view addSubview:locationsTable];
   
    
   cancelBtn = [[UIButton alloc]initWithFrame:CGRectMake(locationsTable.frame.size.width+8, offset_Y+2 , 10, 10)];
    [cancelBtn setImage:[UIImage imageNamed:@"cross_icon.png"] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelBtn];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableDetails count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        return 70;
    else
        return 50;
}



-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    UILabel *label;
    UILabel *detailLabel;
    AsyncImageView *imgView;
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.backgroundColor =  [UIColor clearColor];
        
        
    }
    else
    {
            NSArray *arr = [cell.contentView subviews];
            for(int i=0; i<[arr count]; i++)
            {
                UIView *view = [arr objectAtIndex:i];
                [view removeFromSuperview];
            }
            
    }
    
        RetailerData *iData = [tableDetails objectAtIndex:indexPath.row];
        
        imgView=[[AsyncImageView alloc]initWithFrame:CGRectMake(5, 5, 40, 40)];
        imgView.backgroundColor = [UIColor clearColor];
        imgView.layer.cornerRadius = 5.0f;
        [imgView loadImage:iData.retailImage];
        [cell.contentView addSubview:imgView];
        [imgView release];
        
        label=[[UILabel alloc]initWithFrame:CGRectMake(50, 0, tableView.frame.size.width-50, 20)];
        label.text=iData.retailName;
        [label setFont:[UIFont systemFontOfSize:12.0]];
        [cell.contentView addSubview:label];
        [label release];
    
        detailLabel=[[UILabel alloc]initWithFrame:CGRectMake(50, 22, tableView.frame.size.width-50, 20)];
        detailLabel.text=iData.completeAddr;
        [detailLabel setFont:[UIFont systemFontOfSize:12.0]];
        [cell.contentView addSubview:detailLabel];
    [detailLabel release];
    
     
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self requestRetailerSummary:(int)indexPath.row];
}

-(void)cancelPressed
{
    [tableDetails removeAllObjects];
    [cancelBtn removeFromSuperview];
    [locationsTable removeFromSuperview];
}

#pragma mark Share methods
-(void)shareClicked
{
    iWebRequestState = SPECIAL_OFFER_SHARE;
    
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    
    [reqStr appendFormat:@"<ShareProductInfo><retailerId>%@</retailerId>",[defaults valueForKey:KEY_RETAILERID ]];
    [reqStr appendFormat:@"<pageId>%@</pageId><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:@"nativePageId"],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharespecialoff",BASE_URL];
    DLog(@"%@",urlString);
    [ConnectionManager establishConnectionFor:reqStr base:urlString withDelegate:self];
    
    [reqStr release];
    
}


-(void)shareDetails{
    
    [self shareClicked];
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        return;
    
    // [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    UIActionSheet *msg;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        
        msg = [[[UIActionSheet alloc]
                initWithTitle:NSLocalizedString(@"Share Special Offer Via",@"Share Special Offer Via")
                delegate:self
                cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel")  destructiveButtonTitle:nil
                otherButtonTitles:@"Facebook",@"Twitter",@"Text",@"Email",NSLocalizedString(@"Cancel",@"Cancel"), nil]
               autorelease];
    } else {
        
        msg = [[[UIActionSheet alloc]
                initWithTitle:NSLocalizedString(@"Share Special Offer Via",@"Share Special Offer Via")
                delegate:self
                cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel")  destructiveButtonTitle:nil
                otherButtonTitles:@"Facebook",@"Twitter",@"Text",@"Email", nil]
               autorelease];
    }
    [msg showInView:self.view];
    
    
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
    for (UIView *_currentView in actionSheet.subviews) {
        if ([_currentView isKindOfClass:[UILabel class]]) {
            [((UILabel *)_currentView) setFont:[UIFont boldSystemFontOfSize:15.f]];
        }
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:{//facebook
            AnyViewController *anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
            //[self.view addSubview:anyVC.view];
            anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
            [UIView transitionWithView:self.view duration:0.5
                               options:UIViewAnimationOptionTransitionCurlDown
                            animations:^ { [self.view addSubview:anyVC.view]; }
                            completion:nil];
        }
            break;
        case 1:{//twitter
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    if (result == SLComposeViewControllerResultCancelled) {
                        DLog(@"Twitter Result: canceled");
                    } else {
                        DLog(@"Twitter Result: sent");
                        NSMutableString *userXmlStr;
                        
                        userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><pageId>%@</pageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:@"nativePageId"]];
                        
                        [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
                    }
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        
                        [controller.view removeFromSuperview];
                    } else
                        [controller dismissViewControllerAnimated:YES completion:Nil];
                };
                controller.completionHandler =myBlock;
                
                //Adding the Text to the facebook post value from iOS
                [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                
                //Adding the URL to the facebook post value from iOS
                
                //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                
                //Adding the Image to the facebook post value from iOS
                
                [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    [self.view addSubview:controller.view];
                    [self.view bringSubviewToFront:controller.view];
                }else
                    [self presentViewController:controller animated:YES completion:Nil];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cannot Share",@"Cannot Share")
                                                                message:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")
                                                               delegate:nil
                                                      cancelButtonTitle:NSLocalizedString(@"OK",@"OK")
                                                      otherButtonTitles: nil];
                [alert show];
                [alert release];
            }
        }
            
            
            break;
        case 2:{//text
            
            if ([self hasCellularCoverage])
            {
                if ([MFMessageComposeViewController canSendText]) {
                    
                    MFMessageComposeViewController *smsComposerController = [[[MFMessageComposeViewController alloc] init] autorelease];
                    [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    smsComposerController.messageComposeDelegate = self;
                    [self presentViewController:smsComposerController animated:YES completion:nil];
                }
            }
            else
            {
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") message:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
            
            
        }
            break;
        case 3:{//email
            //[defaults setValue:PRODUCT_SHARE forKey:KEY_SHARE_TYPE];
            EmailShareViewController *emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
            emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
            [self.navigationController pushViewController:emailSendingVC animated:NO];
            [emailSendingVC release];
        }
            break;
        case 4:
            [actionSheet dismissWithClickedButtonIndex:4 animated:YES];
            break;
        default:
            [defaults setBool:NO forKey:BottomButton];
            break;
    }
    
}

-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Message Sent" message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
            break;
        case MessageComposeResultFailed:{
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Oops message not sent" message:@"Please try later" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)dealloc {
    [splOfferView release];
    [super dealloc];
}
@end

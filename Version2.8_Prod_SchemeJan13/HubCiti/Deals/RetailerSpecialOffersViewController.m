//
//  RetailerSpecialOffersViewController.m
//  HubCiti
//
//  Created by deepak.agarwal on 1/23/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import "RetailerSpecialOffersViewController.h"
#import "MainMenuViewController.h"
#import "WebBrowserViewController.h"
#import "NativeSpecialOfferViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@interface RetailerSpecialOffersViewController ()

@end

@implementation RetailerSpecialOffersViewController{
    UIActivityIndicatorView *loading;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setAccessibilityLabel:@"specialOffers"];
   
    self.navigationItem.title = @"Specials";
    loading = nil;
//    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
//    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
//    self.navigationItem.rightBarButtonItem = mainPage;
//    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"MainMenu"];
//    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    ////[mainPage release];

    
    
   
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    arrSpecialOfferRetailer = [[NSMutableArray alloc]init];
    [tblSpecialOffers setAccessibilityIdentifier:@"specialOffers"];
    [self getspecialofferlist];

    
    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillAppear:(BOOL)animated
{
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [super viewWillAppear:animated];
    [self updateViewonViewWillAppear];
}


-(void)updateViewonViewWillAppear
{
    [tblSpecialOffers deselectRowAtIndexPath:[tblSpecialOffers indexPathForSelectedRow] animated:YES];
    [tblSpecialOffers reloadData];
}


-(void)getspecialofferlist
{
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    else if([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];

    if([defaults valueForKey:@"retailLocationID"])
        [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[defaults valueForKey:@"retailLocationID"]];
    if([defaults valueForKey:@"retailerId"])
        [requestStr appendFormat:@"<retailerId>%@</retailerId>",[defaults valueForKey:@"retailerId"]];
    
    [requestStr appendFormat:@"<lastVisitedNo>%lu</lastVisitedNo></RetailerDetail>",(unsigned long)[arrSpecialOfferRetailer count]];
    
    //NSString *urlString = @"http://10.11.202.220:8080/HubCiti1.0/thislocation/getspecialofferlist";
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getspecialofferlist",BASE_URL];
    
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
        
        
        [self parse_getspecialofferlist:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    }
    
    //[ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);
    
}

#pragma mark parse methods
-(void)responseData:(NSString*)response
{
    [self parse_getspecialofferlist :response];
    [tblSpecialOffers reloadData];
}

-(void)parse_getspecialofferlist:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
		return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbxml.rootXMLElement];
        if(nextPageElement)
            isNextPageAvailable = [[TBXML textForElement:nextPageElement]boolValue];
        
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbxml.rootXMLElement];
        
        if(retailerDetailElement)
        {
            TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
            while (RetailerDetailElement)
            {
                NSMutableDictionary *dicRetailer = [[NSMutableDictionary alloc]init];
                
                TBXMLElement *rowNumberElement = [TBXML childElementNamed:@"rowNum" parentElement:RetailerDetailElement];
                TBXMLElement *specialsListIdElement = [TBXML childElementNamed:@"specialsListId" parentElement:RetailerDetailElement];
                TBXMLElement *pageIdElement = [TBXML childElementNamed:@"pageId" parentElement:RetailerDetailElement];
                TBXMLElement *pageTitleElement = [TBXML childElementNamed:@"pageTitle" parentElement:RetailerDetailElement];
                TBXMLElement *sDescriptionElement = [TBXML childElementNamed:@"sDescription" parentElement:RetailerDetailElement];
                TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:RetailerDetailElement];
                
                if(rowNumberElement)
                    [dicRetailer setObject:[TBXML textForElement:rowNumberElement] forKey:@"rowNumber"];
                if(specialsListIdElement)
                    [dicRetailer setObject:[TBXML textForElement:specialsListIdElement] forKey:@"specialsListId"];
                if(pageIdElement){
                    [dicRetailer setObject:[TBXML textForElement:pageIdElement] forKey:@"pageId"];
                    [defaults setObject:[TBXML textForElement:pageIdElement] forKey:KEY_PAGEID];
                }
                if(pageTitleElement)
                    [dicRetailer setObject:[TBXML textForElement:pageTitleElement] forKey:@"pageTitle"];
                if(sDescriptionElement)
                    [dicRetailer setObject:[TBXML textForElement:sDescriptionElement] forKey:@"sDescription"];
                if(pageLinkElement)
                    [dicRetailer setObject:[TBXML textForElement:pageLinkElement] forKey:@"pageLink"];
                
                [arrSpecialOfferRetailer addObject:dicRetailer];
                //[dicRetailer release];
                
                RetailerDetailElement = [TBXML nextSiblingNamed:@"RetailerDetail" searchFromElement:RetailerDetailElement];
                
            }
        }

    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        [loading stopAnimating];
        ReleaseAndNilify(loading);
    }
    [tblSpecialOffers reloadData];
    [defaults setBool:NO forKey:@"ViewMore"];
}



-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
       // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
    [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}
-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma TableView Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 60.0;
    }
    else
    {
        return 100.0;
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isNextPageAvailable)
        return [arrSpecialOfferRetailer count]+1;
    else
        return [arrSpecialOfferRetailer count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"CellEventListView";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor =  [UIColor clearColor];
        
		cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }
    }
    
    if(isNextPageAvailable && indexPath.row == [arrSpecialOfferRetailer count] )
    {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
        
//        CGRect frame;
//        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
//            frame.origin.x = 0;
//            frame.origin.y = 16;
//            frame.size.width = SCREEN_WIDTH;
//            frame.size.height = 24;
//        }
//        else
//        {
//            frame.origin.x = 0;
//            frame.origin.y = 40;
//            frame.size.width = SCREEN_WIDTH;
//            frame.size.height =40;
//        }
//       
//        UILabel *label = [[[UILabel alloc] initWithFrame:frame] ;
//        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
//           label.font = [UIFont boldSystemFontOfSize:16];
//        }
//        else
//        {
//            label.font = [UIFont boldSystemFontOfSize:21];
//        }
//        
//        label.textAlignment = NSTextAlignmentCenter;
//        label.textColor = [UIColor colorWithRGBInt:0x112e72];
//        label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
//        [cell.contentView addSubview:label];
//        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        if([arrSpecialOfferRetailer count] > indexPath.row)
        {
            NSMutableDictionary *dicRetailer = [arrSpecialOfferRetailer objectAtIndex:indexPath.row];
            
            if([dicRetailer valueForKey:@"pageTitle"])
            {
                UILabel *lblEvtName ;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    lblEvtName= [[UILabel alloc]initWithFrame:CGRectMake(10, 5, SCREEN_WIDTH - 30, 20)];
                    lblEvtName.font = [UIFont boldSystemFontOfSize:15];
                }
                else
                {
                    lblEvtName= [[UILabel alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH - 30, 40)];
                    lblEvtName.font = [UIFont boldSystemFontOfSize:20];
                }
                
                [lblEvtName setText:[dicRetailer valueForKey:@"pageTitle"]];
                [lblEvtName setBackgroundColor:[UIColor clearColor]];
                
                lblEvtName.textColor = [UIColor colorWithRGBInt:0x112e72];
                [cell.contentView addSubview:lblEvtName];
                //[lblEvtName release];
                
            }
            if([dicRetailer valueForKey:@"sDescription"])
            {
                UILabel *lblretailAddressval;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                     lblretailAddressval= [[UILabel alloc]initWithFrame:CGRectMake(10, 27, SCREEN_WIDTH - 30, 30)];
                    [lblretailAddressval setFont:[UIFont boldSystemFontOfSize:13]];

                }
                else
                {
                     lblretailAddressval= [[UILabel alloc]initWithFrame:CGRectMake(10, 50, SCREEN_WIDTH - 30, 40)];
                    [lblretailAddressval setFont:[UIFont boldSystemFontOfSize:18]];

                }
               
                //[lblretailAddressval setText:[dicRetailer valueForKey:@"sDescription"]];
                lblretailAddressval.text = [NSString removeHtmlTags:[dicRetailer valueForKey:@"sDescription"]];
                [lblretailAddressval setBackgroundColor:[UIColor clearColor]];
                                lblretailAddressval.textColor = [UIColor darkGrayColor];
                lblretailAddressval.numberOfLines=2;
                [cell.contentView addSubview:lblretailAddressval];
                //[lblretailAddressval release];
                
            }
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(isNextPageAvailable && indexPath.row == [arrSpecialOfferRetailer count] ) {
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"] && lastVisitedRecord!=0) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self getspecialofferlist];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tblSpecialOffers reloadData];
                });
            });
            
        }
    }
    
}

// Called after the user changes the selection.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if(isNextPageAvailable && indexPath.row == [arrSpecialOfferRetailer count] )
//    {
//        [self getspecialofferlist];
//        return;
//    }
   
    [defaults setBool:NO forKey:@"ViewMore"];


    NSMutableDictionary *dicRetailer = [arrSpecialOfferRetailer objectAtIndex:indexPath.row];

    [defaults  setObject:[dicRetailer valueForKey:@"pageLink"] forKey:KEY_URL];
    [defaults  setObject:[dicRetailer valueForKey:@"pageId"] forKey:@"nativePageId"];
 
	NativeSpecialOfferViewController *urlDetail = [[NativeSpecialOfferViewController alloc]initWithNibName:@"NativeSpecialOfferViewController" bundle:[NSBundle mainBundle]];
    shareFlag=YES;
	[self.navigationController pushViewController:urlDetail animated:NO];
	//[urlDetail release];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  ListCouponsViewController.m
//  HubCiti
//
//  Created by Lakshmi H R on 11/30/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "ListCouponsViewController.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "MainMenuViewController.h"
#import "ListCouponCell.h"
#import "NewCouponDetailViewController.h"
#import "CouponsInDealViewController.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "MainMenuViewController.h"
#import "CouponsSwipeCell.h"
#import "ListCouponsViewController.h"
#import "bottomButtonView.h"
#import "AppDelegate.h"
#import "HTTPClient.h"
#import <QuartzCore/QuartzCore.h>
#import "bottomButtonDO.h"
#import "EventsListViewController.h"
#import "EventsCategoryDO.h"
#import "EventDetailsDO.h"
#import "EventListDetailViewController.h"
#import "EventGroupingAndSortingViewController.h"
#import "HubCitiConstants.h"
#import "SettingsViewController.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "CityExperienceViewController.h"
#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "FindViewController.h"
#import "AnythingPage.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "SingleCatRetailers.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FAQCategoryList.h"
#import "FilterListViewController.h"
#import "FundraiserListViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "SpecialOffersViewController.h"
#import "HTTPClient.h"
#import "GetUserInfoResponse.h"
#import "BandEventType.h"
#import "BandCategories.h"
#import "EventDetailImageViewController.h"
#import "bottomButtonView.h"
#import "RetailerAnnotation.h"
#import "CouponViewCell.h"
#import "CouponsMyAccountsViewController.h"

@interface ListCouponsViewController ()<CustomizedNavControllerDelegate,UITableViewDelegate,UITableViewDataSource,SwipesViewControllerDelegate>
{
    int k;
    NSMutableArray *arrBottomButtonViewContainer, *arrCouponBottomButtonDO;
    bottomButtonView *bottomBtnView;
    WebRequestState iWebRequestState;
    CommonUtility *common;
    BOOL isRemember;
    bottomButtonView *view;
    NSNumber *lastRecord;
    NSNumber *nextPage;
    BOOL showMapFlag;
    int bottomBtn;
    NSNumber *rowCountOfTotalCells;
    NSNumber *lowerLimitValue;
    NSMutableArray *CouponListArray,*nameArr, *allTypeOfCoupons, *savedCouponArr, *redeemedCouponArr, *expiredCouponArr, *typedCouponArr;
    UIActivityIndicatorView *loading;
    NSInteger numberOfCouponItemsInSection;
    BOOL scrollViewEnabled,fetchNext,viewmore;
    UIView* containerView;
}
@end

@implementation ListCouponsViewController
@synthesize listCouponTable,couponTableBottomConst,sortObj,savedSortObj,infoResponse,couponsResponse,couponRetailerDetails,featuredOrNonFeatured,searchBarOutlet,retailId,couponsMyAccResponse,fromCoupons,fromMyAccounts,searchTextFromPreviousScreen,couponsPostalCodeList;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - Service Respone methods
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    if (fetchNext) {
        [self hideFooter];
    }
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription] ];
    
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    if (fetchNext) {
        [self hideFooter];
    }
    
    [HubCitiAppDelegate removeActivityIndicator];
    DLog(@"parameter: %@",responseObject);
    
    [self responseData:responseObject];
    scrollViewEnabled = TRUE;
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    UINib *cellNib = [UINib nibWithNibName:@"ListCouponCell" bundle:nil];
    [listCouponTable registerNib:cellNib forCellReuseIdentifier:@"couponListCell"];
    
    
//    k =1;
//    
//    if (k == 0)
//    {
//        couponTableBottomConst.constant = couponTableBottomConst.constant + 50;
//        
//        [listCouponTable layoutIfNeeded];
//        [listCouponTable reloadData];
//    }
    
    //Setup text color and font for segment control text
   
    
    //set title of the screen
    cusNav =(CustomizedNavController *) self.navigationController;
    if ([featuredOrNonFeatured isEqualToString:@"distance"])
    {
        [cusNav setTitle:@"Distance" forView:self withHambergur:YES];
    }
    else if ([featuredOrNonFeatured isEqualToString:@"atoz"])
    {
        [cusNav setTitle:@"Alphabetically" forView:self withHambergur:YES];
    }
    else
    {
        [cusNav setTitle:featuredOrNonFeatured forView:self withHambergur:YES];
    }
    
    //customized home button navigation bar
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = mainPage;
    if(IPAD)
    {
        listCouponTable.contentInset = UIEdgeInsetsMake(0, 0, 60, 0);
    }
    else
    {
        listCouponTable.contentInset = UIEdgeInsetsMake(0, 0, 80, 0);
    }
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
    [defaults setValue:nil forKey:@"CouponListingMAINMENUID"];
    [defaults setValue:nil forKey:@"CouponMyAccountListingMAINMENUID"];
//    sortObj = [[SortAndFilter alloc] init];
//    sortObj.distanceSelected = YES;

    if(fromCoupons)
    {
        [self requestForCoupons:searchBarOutlet.text :sortObj];
        searchBarOutlet.text = searchTextFromPreviousScreen;
        nextPage = [[NSNumber alloc]init];
        rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
        CouponListArray = [[NSMutableArray alloc]init];
        typedCouponArr = [[NSMutableArray alloc]init];

    }
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    [searchBarOutlet resignFirstResponder];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:NO];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
        nextPage = 0;
        rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
        [CouponListArray removeAllObjects];
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        sortObj = [[SortAndFilter alloc]initWithObject:[self loadCustomObjectWithKey:@"SortFilterObject"]];
        if(fromCoupons)
        {
            [self requestForCoupons:searchBarOutlet.text :sortObj];
        }
        else if (fromMyAccounts)
        {
            [self requestForMyAccounts:searchBarOutlet.text :sortObj];
        }
    }
    else if (fromMyAccounts)
    {
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        [defaults setValue:nil forKey:@"CouponListingMAINMENUID"];
        [defaults setValue:nil forKey:@"CouponMyAccountListingMAINMENUID"];
        searchBarOutlet.text = searchTextFromPreviousScreen;
        nextPage = [[NSNumber alloc]init];
        rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
        CouponListArray = [[NSMutableArray alloc]init];
        typedCouponArr = [[NSMutableArray alloc]init];
        [self requestForMyAccounts:searchBarOutlet.text :sortObj];
    }


}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [super viewWillDisappear:animated];
    
    for (NSOperation *operation in [HTTPClient sharedHTTPClient].manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        NSLog(@"operation:%@",operation);
        AFHTTPRequestOperation *operations = (AFHTTPRequestOperation*)operation;
        if([[[[operations request] URL] absoluteString] containsString:@"gallery/getallcoupbylocjson"] || [[[[operations request] URL] absoluteString] containsString:@"gallery/getmyaccountsjson"]){
            [operation cancel];
        }
        
    }
}
-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(void)popBackToPreviousPage{
    
    [self.navigationController popViewControllerAnimated:NO];
}
-(void) hideFooter
{
    fetchNext = false;
    [loading stopAnimating];
    [containerView removeFromSuperview];
    scrollViewEnabled = false;
    listCouponTable.tableFooterView = nil;
    listCouponTable.userInteractionEnabled = TRUE;
    for (UIScrollView *nview in self.parentViewController.view.subviews) {
        
        if ([nview isKindOfClass:[UIScrollView class]])
        {
            NSLog(@"Recognizer ");
            nview.scrollEnabled = YES;
        }
    }
    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (!scrollViewEnabled)
    {
        return;
    }
    CGSize contentSize = scrollView.contentSize;
    CGPoint contentOffset = scrollView.contentOffset;
    
    if (contentSize.height < SCREEN_HEIGHT && [nextPage intValue]  == 0) {
        return;
    }
    
    if ((contentOffset.y + scrollView.bounds.size.height > contentSize.height) && [nextPage intValue] == 1)
    {
        [self fetchNextNews];
    }
}
-(void)fetchNextNews

{
    viewmore = true;
    scrollViewEnabled = NO;
    fetchNext = true;
    [HubCitiAppDelegate removeActivityIndicator];
    
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    loading.color = [UIColor blackColor];
    [loading startAnimating];
    CGFloat height = loading.frame.size.height + 6.f;
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, height)];
    containerView.backgroundColor = [UIColor clearColor];
    [containerView addSubview:loading];
    loading.center = CGPointMake(self.view.bounds.size.width / 2, height / 2.f);
    listCouponTable.tableFooterView = containerView;
    listCouponTable.userInteractionEnabled = false;
    if(fromCoupons)
    {
        [self requestForCoupons:searchBarOutlet.text :sortObj];
    }
    else if (fromMyAccounts)
    {
        [self requestForMyAccounts:searchBarOutlet.text :sortObj];
    }
    
    
}
//TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return CouponListArray.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return listCouponTable.frame.size.height/2 ;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"couponListCell";
    
    ListCouponCell *cell = (ListCouponCell *)[listCouponTable dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[ListCouponCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    if(indexPath.row == numberOfCouponItemsInSection - 1 && indexPath.row != 0 )
    {
        
        [cell updateUiWithRetailerDetails:nil];
        
    }
    
    
    else if (indexPath.row != [CouponListArray count] && [CouponListArray count] > 0 )
    {
        
        NSLog(@"Image dimension: %f * %f",cell.coupon_Image.frame.size.width,cell.coupon_Image.frame.size.height);
        
        if(CouponListArray.count > 0 && CouponListArray.count != indexPath.row)
        {
            
            CouponsRetailerDetails *cellCoupon ;
            cellCoupon = [CouponListArray objectAtIndex:indexPath.row];
            [cell updateUiWithRetailerDetails:cellCoupon];
            
            
        }
        
    }
    else if (indexPath.row == [CouponListArray count] && [nextPage intValue]== 1)
    {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGRect frame;
        if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
            frame.origin.x = 52;
            frame.origin.y = 12;
            frame.size.width = 250;
            frame.size.height = 24;
        }
        else{
            frame.origin.x = 70;
            frame.origin.y = 20;
            frame.size.width = 600;
            frame.size.height = 30;
        }
        loading.frame = frame;
        
        //loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    
    // ... set up the cell here ...
    
    return cell;
    
    
    
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CouponsRetailerDetails *cellCoupon ;
    cellCoupon = [CouponListArray objectAtIndex:indexPath.row];
    NewCouponDetailViewController *couponDetailVC = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
    [defaults setValue:cellCoupon.couponId forKey:@"couponId"];
    [defaults setValue:cellCoupon.couponListId forKey:@"couponListId"];
    [self.navigationController pushViewController:couponDetailVC animated:NO];
}
// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    // Calculate Y position for View
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [arrCouponBottomButtonDO count]; btnLoop++)
    {
        bottomButtonDO *obj_eventBottomDO = [arrCouponBottomButtonDO objectAtIndex:btnLoop];
        
        bottomBtnView = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            bottomBtnView.contentMode = UIViewContentModeCenter;
            bottomBtnView = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, 80,bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            bottomBtnView = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrCouponBottomButtonDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrCouponBottomButtonDO count],bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [bottomBtnView setAccessibilityLabel:obj_eventBottomDO.btnLinkTypeName];
        [bottomBtnView setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:bottomBtnView];
        [self.view addSubview:bottomBtnView];
        [self.view bringSubviewToFront:bottomBtnView];
    }
}

#pragma bottombutton functionality

-(void)bottomButtonPressed:(id)sender
{
    if([defaults boolForKey:@"ViewMore"])
        return;
    [defaults setBool:NO forKey:@"ViewMoreBand"];
    [defaults setBool:YES forKey:BottomButton];
    //   [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    //NSlog(@"The tag is %d",tag);
    bottomButtonDO *obj_eventBottomDO = [arrCouponBottomButtonDO objectAtIndex:tag];
    
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=27||[[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=28){
        [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    }
    [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_eventBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_eventBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_eventBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_eventBottomDO.btnLinkTypeName);
    if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    
    else{ //other menu types
        switch ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                //[hotDeals release];
                break;
                
            case 1://Scan Now
            {
                
            }
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
                }
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                
                [self navigateToFindView];
            }
                
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
            {
                
            }
                
                break;
                
            case 8://Visitors Bureau
            {
                
            }
                
                break;
                
            case 9://transportation
            {
                
            }
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    //[alert release];
                    
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];                   //[alert release];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {
                    [self shareClicked:btn];
                }
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_eventBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                    
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    // CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];;
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
                //                [self navigateToCouponsViewController];
                //                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                //   [dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }
                break;
            case 22:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }
                break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        //[inform release];
                    }
                    
                }
                
            }break;
            case 25:
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            //  [citPref release];
                        }
                    }
                }
                
            }break;
            case 27:
            case 28://sort/filter
            {
                {
                    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
                    
                    
                    SwipesViewController *iSwipeViewController1 = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                    if ([searchBarOutlet.text length] > 0)
                    {
                        iSwipeViewController1.srchKey = searchBarOutlet.text;
                    }
                    if (fromCoupons)
                    {
                        [defaults setValue:[defaults valueForKey:@"CouponListingMAINMENUID"] forKey:KEY_MITEMID];
                    iSwipeViewController1.module = @"Coupons";
                    }
                    else if (fromMyAccounts)
                    {
                        [defaults setValue:[defaults valueForKey:@"CouponMyAccountListingMAINMENUID"] forKey:KEY_MITEMID];
                        iSwipeViewController1.module = @"myaccounts";
                    }
                    iSwipeViewController1.delegate = self;
                    if(isRetailerEvent == TRUE)
                    {
                        iSwipeViewController1.isRetailerEvent = TRUE;
                    }
                    
                    if([couponsPostalCodeList length])
                        iSwipeViewController1.couponsPostalCodeSort = couponsPostalCodeList;
                    
                    iSwipeViewController1.sortFilObj = [[SortAndFilter alloc]initWithObject:sortObj];
                    [self.navigationController pushViewController:iSwipeViewController1 animated:NO];
                }
            }
                break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                if([couponsPostalCodeList length])
                    dvc.coupnsPostalCode = couponsPostalCodeList;
                
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
               // privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
            default:
                break;
        }
    }
    
}

#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
        //[alert release];
    }
    else
    {
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet:sender];
    }
}
-(void) showActionSheet:(id)sender {
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    UIButton *btnView = (UIButton*) sender;
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = btnView;
        actionSheet.popoverPresentationController.sourceRect = btnView.bounds;
    }

    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {
    _anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:_anyVC.view]; }
                    completion:nil];
    
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:@"Oops, SMS not supported.." msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
        
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    _emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    _emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [_emailSendingVC loadMail];
    
}

-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
            
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)responseData:(id ) response
{
    switch (iWebRequestState)
    {
            
        case COUPONS_REQUEST:
            [self parseCoupons:response];
            break;
        case COUPON_MYACC_REQUEST:
            [self parseMyAccounts:response];
            break;
            //        case BANDS_EVENTS_INFO:
            //            [self parse_EventSearchData:response];
            //            break;
        case PARTNERRET:
            [self parse_PartnerRet:response];
            break;
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
            
            
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
            
        default:
            break;
            
            
            
    }
    
}

-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            
            
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}



//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];                    //[alert release];
                    
                    //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc]init];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }
    
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}

//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    else if ([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}
-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
}
-(void)parseGetUserData:(id)response
{
    
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}

-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
        
        
        isRemember = YES;
        
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}



//-(void)navigateToCouponsViewController
//{
//    [defaults setBool:YES forKey:@"myGallerysplash"];
//    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
//    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
//    [self.navigationController pushViewController:iCouponsViewController animated:NO];
//    //[iCouponsViewController release];
//}


-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}


-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}

//Call Find Service and Class
-(void)navigateToFindView
{
    
    //    [defaults setValue:@"0" forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    
    navigatedFromMainMenu = FALSE;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}
-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}


-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=FALSE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}
//{
//    "userId":"10021",
//    "hubCitiId":"2070",
//    "postalCode":"78654",
//    "isFeatOrNonFeat":null, if 0 to get only featured list and 1 to get only nonfeatured list
//        "lastVisitedNo":"0",
//        "sortColumn":"distance",
//        "sortOrder":"asc"
//        }
//
//
//Other paramters:
//Longitude
//Latitude
//searchKey
//cityIds
//catIds
//retailId
//mainMenuId

//REquest for coupon listing
-(void) requestForCoupons:(NSString *)searchText :(SortAndFilter *) sortFilObj
{
    iWebRequestState = COUPONS_REQUEST;
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    if (searchText.length > 0)
    {
        [parameter setValue:searchText forKey:@"searchKey"];
    }
    if([defaults valueForKey:KEY_USERID])
    {
        [parameter setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if([defaults valueForKey:KEY_HUBCITIID])
    {
        [parameter setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
   
    [parameter setValue:rowCountOfTotalCells forKey:@"lastVisitedNo"];
    if(sortObj.distanceSelected)
    {
        [parameter setValue:@"distance" forKey:@"sortColumn"];
    }
    if(sortObj.alphabeticallySelected)
    {
        [parameter setValue:@"atoz" forKey:@"sortColumn"];
    }
    [parameter setValue:@"asc" forKey:@"sortOrder"];
    if (sortObj.selectedCitiIds)
    {
        [parameter setValue:sortObj.selectedCitiIds forKey:@"cityIds"];
    }
    if (sortObj.selectedCatIds)
    {
        [parameter setValue:sortObj.selectedCatIds forKey:@"catIds"];
    }
    
    if(isRetailerEvent == TRUE)
    {
        [parameter setValue:retailId forKey:@"retailId"];
    }
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length])
    {
        [parameter setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        [parameter setValue:[defaults valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        
    }
    else  if ([[defaults valueForKey:KEYZIPCODE] length])
    {
        [parameter setValue:[defaults valueForKey:KEYZIPCODE]forKey:@"postalCode"];
        
    }
    else if ([couponsPostalCodeList length]){
        
        [parameter setValue:couponsPostalCodeList forKey:@"postalCode"];
    }

    if([defaults valueForKey:KEY_MAINMENUID])
    {
        [defaults setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"CouponListingMAINMENUID"];
        [parameter setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    }
    if([featuredOrNonFeatured containsString:@"Featured"])
    {
        [parameter setValue:@"0" forKey:@"isFeatOrNonFeat"];
    }
    else
    {
        [parameter setValue:@"1" forKey:@"isFeatOrNonFeat"];
    }
    NSString *urlString = [NSString stringWithFormat:@"%@gallery/getallcoupbylocjson",BASE_URL];
    //NSString *urlString = [NSString stringWithFormat:@"http://10.10.221.155:9990/HubCiti2.8.2/gallery/getallcoupbylocjson"];
    NSLog(@"parameter: %@", parameter);
    if ([nextPage intValue] == 1)
    {
        for (UIScrollView *nview in self.parentViewController.view.subviews) {
            
            if ([nview isKindOfClass:[UIScrollView class]])
            {
                NSLog(@"Recognizer ");
                nview.scrollEnabled = NO;
            }
        }
        NSLog(@"Json = %@", parameter);
        HTTPClient* client = [HTTPClient sharedHTTPClient];
        client.delegate = self;
        NSLog(@"URL = %@", urlString);
        [client sendRequest:parameter : urlString];
    }
    

    
    else
    {
        
        HTTPClient *client = [[HTTPClient alloc] init];
        client.delegate = self;
        DLog(@"Url: %@",urlString);
        [client sendRequest : parameter : urlString];
        [HubCitiAppDelegate showActivityIndicator];
    }
    
    
}
-(void)parseCoupons:(id) responseString{
    [HubCitiAppDelegate removeActivityIndicator];
    if(responseString == NULL)
        return;
    
    if(couponsResponse == NULL)
    {
        couponsResponse = [[CouponsResponse alloc]init];
    }
    
    @try{
        [couponsResponse setValuesForKeysWithDictionary:responseString];
    }
    
    
    @catch (NSException *exception) {
        // Do nothing
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
        return;
    }
    
    NSLog(@"CouponsResponse: %@",couponsResponse);
    
    if([couponsResponse.responseCode isEqualToString:@"10000"])
    {
        if(couponsResponse.nextPage)
        {
            nextPage = couponsResponse.nextPage;
        }
        if(couponsResponse.maxRowNum)
        {
            rowCountOfTotalCells = couponsResponse.maxRowNum;
        }
        if(couponsResponse.featuredCouponsList)
        {
            for(int l=0; l < couponsResponse.featuredCouponsList.count;l++)
            {
                CouponsRetailerDetails *couponDetails = [[CouponsRetailerDetails alloc] init];
                
                
                NSDictionary *dictList = couponsResponse.featuredCouponsList[l];
                
                
                @try{
                    [couponDetails setValuesForKeysWithDictionary:dictList];
                }
                @catch (NSException *exception) {
                    // Do nothing
                    [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
                    return;
                }
                
                
                [CouponListArray addObject:couponDetails];
                
            }
        }
        if(couponsResponse.nonFeaturedCouponsList)
        {
            for(int l=0; l < couponsResponse.nonFeaturedCouponsList.count;l++)
            {
                CouponsRetailerDetails *couponDetails = [[CouponsRetailerDetails alloc] init];
                
                
                NSDictionary *dictList = couponsResponse.nonFeaturedCouponsList[l];
                
                
                @try{
                    [couponDetails setValuesForKeysWithDictionary:dictList];
                }
                @catch (NSException *exception) {
                    // Do nothing
                    [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
                    return;
                }
                
                [CouponListArray addObject:couponDetails];
                
            }
        }
        if([couponsResponse.bottomBtn integerValue] == 1)
        {
            bottomBtn = [couponsResponse.bottomBtn intValue];
            arrCouponBottomButtonDO=[[NSMutableArray alloc]init];
            for (int i=0; i < couponsResponse.bottomBtnList.count; i++) {
                bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                NSDictionary* dictList = couponsResponse.bottomBtnList[i];
                
                [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                
                if(obj_findBottomDO.bottomBtnName.length == 0)
                {
                    obj_findBottomDO.bottomBtnName = @" ";
                }
                if(obj_findBottomDO.bottomBtnImg.length == 0)
                {
                    obj_findBottomDO.bottomBtnImg = @" ";
                }
                if(obj_findBottomDO.bottomBtnImgOff.length == 0)
                {
                    obj_findBottomDO.bottomBtnImgOff = @" ";
                }
                if(obj_findBottomDO.btnLinkTypeName)
                {
                    if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"])
                    {//for filters
                        [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                    }
                }
                [arrCouponBottomButtonDO addObject:obj_findBottomDO];
                
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if(![defaults boolForKey:@"ViewMoreEvent"]){
                    if([arrCouponBottomButtonDO count] > 0)
                    {
                        // Set Bottom Menu Button after read from response data
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self setBottomBarMenu];
                        });
                        
                        [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                    }
                    else
                        [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
                    
                }
            });
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            self.listCouponTable.hidden = NO;
            if(bottomBtn == 1 && couponTableBottomConst.constant == 0)
            {
                couponTableBottomConst.constant = couponTableBottomConst.constant + 50;
                [listCouponTable updateConstraints];
            }
            [listCouponTable reloadData];
            if ([nextPage intValue] != 1)
            {
            [listCouponTable setContentOffset:CGPointZero animated:YES];
            }
        });
        
        
    }
    else if([couponsResponse.responseCode isEqualToString:@"10002"])
    {
        nextPage=0;
        
        NSString *responseTextStr;
        if (couponsResponse.responseText != nil) {
            responseTextStr = couponsResponse.responseText;
        }
        else
            responseTextStr = @"No Records Found";
        dispatch_async(dispatch_get_main_queue(), ^{
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
            dispatch_async(dispatch_get_main_queue(), ^{
                listCouponTable.hidden = NO;
                
                [listCouponTable reloadData];
                [listCouponTable setContentOffset:CGPointZero animated:YES];
            });
        });
        
    }
    else
    {//show the error message in alert
        nextPage=0;
        
        
        if (couponsResponse.responseText != nil) {
            
            
            UIAlertController * alert;
            
            alert=[UIAlertController alertControllerWithTitle:couponsResponse.responseText message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            // [loading stopAnimating];
            
            
        }
        else
        {
            UIAlertController * alert;
            
            alert=[UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"No Records found"] message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            //            [loading stopAnimating];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            listCouponTable.hidden = NO;
            [listCouponTable reloadData];
            
            [listCouponTable setContentOffset:CGPointZero animated:YES];
            
            
            [listCouponTable setContentOffset:CGPointZero animated:YES];
        });
        
        
    }
    
}
//REquest for coupon listing
-(void) requestForMyAccounts:(NSString *)searchText :(SortAndFilter *) sortFilObj
{
    iWebRequestState = COUPON_MYACC_REQUEST;
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
//    [parameter setValue:@"10024" forKey:@"userId"];
//    [parameter setValue:@"2070" forKey:@"hubCitiId"];
//    [parameter setValue:@"78654"forKey:@"postalCode"];
    if (searchText.length > 0)
    {
        [parameter setValue:searchText forKey:@"searchKey"];
    }

         if([defaults valueForKey:KEY_USERID])
         {
    
             [parameter setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
         }
        if([defaults valueForKey:KEY_HUBCITIID])
        {
    
            [parameter setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
        }
    
    [parameter setValue:rowCountOfTotalCells forKey:@"lastVisitedNo"];
    if(sortObj.distanceSelected)
    {
        [parameter setValue:@"distance" forKey:@"sortColumn"];
    }
    if(sortObj.alphabeticallySelected)
    {
        [parameter setValue:@"atoz" forKey:@"sortColumn"];
    }
    [parameter setValue:@"asc" forKey:@"sortOrder"];
    if (sortObj.selectedCitiIds)
    {
        [parameter setValue:sortObj.selectedCitiIds forKey:@"cityIds"];
    }
    
    if (sortObj.selectedCatIds)
    {
        [parameter setValue:sortObj.selectedCatIds forKey:@"catIds"];
    }
    
    if(isRetailerEvent == TRUE)
    {
        [parameter setValue:retailId forKey:@"retailId"];
    }

    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length])
    {
        [parameter setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        [parameter setValue:[defaults valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        
    }
    else  if ([[defaults valueForKey:KEYZIPCODE] length])
    {
        [parameter setValue:[defaults valueForKey:KEYZIPCODE]forKey:@"postalCode"];
        
    }
    else if ([couponsPostalCodeList length]){
        
        [parameter setValue:couponsPostalCodeList forKey:@"postalCode"];
    }
    if([defaults valueForKey:KEY_MAINMENUID])
    {
        [parameter setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"CouponMyAccountListingMAINMENUID"];
        [parameter setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    }
    if([featuredOrNonFeatured containsString:@"Saved"])
    {
        [parameter setValue:@"Saved" forKey:@"type"];
    }
    else if([featuredOrNonFeatured containsString:@"Redeemed"])
    {
        [parameter setValue:@"Redeemed" forKey:@"type"];
    }
    else if ([featuredOrNonFeatured containsString:@"Expired"])
    {
        [parameter setValue:@"Expired" forKey:@"type"];
    }
    NSString *urlString = [NSString stringWithFormat:@"%@gallery/getmyaccountsjson",BASE_URL];
    //NSString *urlString = [NSString stringWithFormat:@"http://10.10.221.155:9990/HubCiti2.8.2/gallery/getmyaccountsjson"];
    NSLog(@"parameter: %@", parameter);
    if ([defaults boolForKey:@"ViewMoreCoupon"])
    {
        NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameter];
        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
        id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        [HubCitiAppDelegate removeActivityIndicator];
        [self parseCoupons:responseData];
    }
    
    else
    {
        
        HTTPClient *client = [[HTTPClient alloc] init];
        client.delegate = self;
        DLog(@"Url: %@",urlString);
        [client sendRequest : parameter : urlString];
        [HubCitiAppDelegate showActivityIndicator];
    }
    
    
}
-(void)parseMyAccounts:(id) responseString{
    [HubCitiAppDelegate removeActivityIndicator];
    if(responseString == NULL)
        return;
    
    if(couponsMyAccResponse == NULL)
    {
        couponsMyAccResponse = [[CouponMyACcounts alloc]init];
    }
    
    @try{
        [couponsMyAccResponse setValuesForKeysWithDictionary:responseString];
    }
    
    
    @catch (NSException *exception) {
        // Do nothing
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
        return;
    }
    
    NSLog(@"CouponsResponse: %@",couponsMyAccResponse);
    
    if([couponsMyAccResponse.responseCode isEqualToString:@"10000"])
    {
        if(couponsMyAccResponse.nextPage)
        {
            nextPage = couponsMyAccResponse.nextPage;
        }
        if(couponsMyAccResponse.maxRowNum)
        {
            rowCountOfTotalCells = couponsMyAccResponse.maxRowNum;
        }
        
        if(couponsMyAccResponse.couponList)
        {
            for(int l=0; l < couponsMyAccResponse.couponList.count;l++)
            {
                MyAccountDetails *accDetails = [[MyAccountDetails alloc] init];
                
                
                NSDictionary *dictList = couponsMyAccResponse.couponList[l];
                
                
                @try{
                    [accDetails setValuesForKeysWithDictionary:dictList];
                }
                @catch (NSException *exception) {
                    // Do nothing
                    [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
                    return;
                }
                
                [nameArr addObject:accDetails.type];
                [allTypeOfCoupons addObject:accDetails];
                for(int q = 0; q < [accDetails list].count; q++)
                {
                    CouponsRetailerDetails *typedCouponDetails = [[CouponsRetailerDetails alloc]init];
                    
                    NSDictionary *dictList = [accDetails list][q];
                    
                    [typedCouponDetails setValuesForKeysWithDictionary:dictList];
                    if([[accDetails type] containsString:@"Saved"] )
                    {
                        [CouponListArray addObject:typedCouponDetails];
                    }
                    else if ([[accDetails type] containsString:@"Redeemed"])
                    {
                        [CouponListArray addObject:typedCouponDetails];
                    }
                    else if ([[accDetails type] containsString:@"Expired"])
                    {
                        [CouponListArray addObject:typedCouponDetails];
                    }
                    
                }
                
                
            }
            
        }
        if([couponsMyAccResponse.bottomBtn integerValue] == 1)
        {
            bottomBtn = [couponsMyAccResponse.bottomBtn intValue];
            arrCouponBottomButtonDO=[[NSMutableArray alloc]init];
            for (int i=0; i < couponsMyAccResponse.bottomBtnList.count; i++) {
                bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                NSDictionary* dictList = couponsMyAccResponse.bottomBtnList[i];
                
                [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                
                if(obj_findBottomDO.bottomBtnName.length == 0)
                {
                    obj_findBottomDO.bottomBtnName = @" ";
                }
                if(obj_findBottomDO.bottomBtnImg.length == 0)
                {
                    obj_findBottomDO.bottomBtnImg = @" ";
                }
                if(obj_findBottomDO.bottomBtnImgOff.length == 0)
                {
                    obj_findBottomDO.bottomBtnImgOff = @" ";
                }
                if(obj_findBottomDO.btnLinkTypeName)
                {
                    if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"])
                    {//for filters
                        [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                    }
                }
                [arrCouponBottomButtonDO addObject:obj_findBottomDO];
                
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if(![defaults boolForKey:@"ViewMoreEvent"]){
                    if([arrCouponBottomButtonDO count] > 0)
                    {
                        // Set Bottom Menu Button after read from response data
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self setBottomBarMenu];
                        });
                        
                        [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                    }
                    else
                        [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
                    
                }
            });
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            self.listCouponTable.hidden = NO;
            if(bottomBtn == 1 && couponTableBottomConst.constant == 0)
            {
                couponTableBottomConst.constant = couponTableBottomConst.constant + 50;
                [listCouponTable updateConstraints];
            }
            [listCouponTable reloadData];
            
           // [listCouponTable setContentOffset:CGPointZero animated:YES];
        });
        
        
    }
    else if([couponsMyAccResponse.responseCode isEqualToString:@"10002"])
    {
        nextPage=0;
        
        NSString *responseTextStr;
        if (couponsMyAccResponse.responseText != nil) {
            responseTextStr = couponsMyAccResponse.responseText;
        }
        else
            responseTextStr = @"No Records Found";
        dispatch_async(dispatch_get_main_queue(), ^{
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
            dispatch_async(dispatch_get_main_queue(), ^{
                listCouponTable.hidden = NO;
                [listCouponTable reloadData];
               // [listCouponTable setContentOffset:CGPointZero animated:YES];
            });
        });
        
    }
    else
    {//show the error message in alert
        nextPage=0;
        
        
        if (couponsMyAccResponse.responseText != nil) {
            
            
            UIAlertController * alert;
            
            alert=[UIAlertController alertControllerWithTitle:couponsMyAccResponse.responseText message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            // [loading stopAnimating];
            
            
        }
        else
        {
            UIAlertController * alert;
            
            alert=[UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"No Records found"] message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            //            [loading stopAnimating];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            listCouponTable.hidden = NO;
            [listCouponTable reloadData];
            
           // [listCouponTable setContentOffset:CGPointZero animated:YES];
            
            
            //[listCouponTable setContentOffset:CGPointZero animated:YES];
        });
        
        
    }
    
}

- (void)addItemViewController:(SwipesViewController *)controller didFinishEnteringItem:(SortAndFilter *)item
{
    if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
        sortObj = [[SortAndFilter alloc]initWithObject:item];
        rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
        [CouponListArray removeAllObjects];
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        if(fromCoupons)
        {
            [self requestForCoupons:searchBarOutlet.text :sortObj];
        }
        else if (fromMyAccounts)
        {
            [self requestForMyAccounts:searchBarOutlet.text :sortObj];
        }
        
        
    }
}

- (SortAndFilter *)loadCustomObjectWithKey:(NSString *)key {
    
    NSData *encodedObject = [defaults objectForKey:key];
    SortAndFilter *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    NSLog(@"SortAndFilter object in loadCustomObj: %@",object);
    return object;
}

#pragma Delegates for UISearchBar
-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    nextPage = 0;
    rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
    if (timer){
        [timer invalidate];
        timer = nil;
    }
    if(searchText.length == 0)
    {
        if (timer){
            [timer invalidate];
            timer = nil;
        }
        [CouponListArray removeAllObjects];
        if(fromCoupons)
        {
            [self requestForCoupons:searchText :sortObj];
        }
        else if (fromMyAccounts)
        {
            [self requestForMyAccounts:searchText :sortObj];
        }
        [self.searchBarOutlet performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0.1];

    }
    if(searchText.length >= 3)
    {
        
        if (![timer isValid]){
            CouponListArray = [[NSMutableArray alloc]init];
            timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(call_RequestForCoupon) userInfo:nil  repeats:NO];
        }
    }
    
}
-(void)call_RequestForCoupon
{
    if(fromCoupons)
    {
        [self requestForCoupons:searchBarOutlet.text :sortObj];
    }
    else if (fromMyAccounts)
    {
        [self requestForMyAccounts:searchBarOutlet.text :sortObj];
    }
}
-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBarDelegate
{
    nextPage = 0;
    rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
    if (timer){
        [timer invalidate];
        timer = nil;
    }
    CouponListArray = [[NSMutableArray alloc]init];
    if(fromCoupons)
    {
        [self requestForCoupons:searchBarDelegate.text :sortObj];
    }
    else if (fromMyAccounts)
    {
        [self requestForMyAccounts:searchBarDelegate.text :sortObj];
    }
    [searchBarDelegate resignFirstResponder];
    // [HubCitiAppDelegate removeActivityIndicator];
    //[searchTableView reloadData];
    
    
}
-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBarDelegate
{
    [searchBarDelegate resignFirstResponder];
    
    CouponListArray = [[NSMutableArray alloc]init];
    nextPage = 0;
    rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
    if (timer){
        [timer invalidate];
        timer = nil;
    }
    
        if(fromCoupons)
        {
            [self requestForCoupons:nil :sortObj];
        }
        else if (fromMyAccounts)
        {
            [self requestForMyAccounts:nil :sortObj];
        }
    
    searchBarDelegate.text  = nil;
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end

//
//  CouponsInDealViewController.m
//  HubCiti
//
//  Created by Nikitha on 11/21/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "CouponsInDealViewController.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "MainMenuViewController.h"
#import "CouponsSwipeCell.h"
#import "ListCouponsViewController.h"
#import "bottomButtonView.h"
#import "AppDelegate.h"
#import "HTTPClient.h"
#import <QuartzCore/QuartzCore.h>
#import "bottomButtonDO.h"
#import "EventsListViewController.h"
#import "EventsCategoryDO.h"
#import "EventDetailsDO.h"
#import "EventListDetailViewController.h"
#import "EventGroupingAndSortingViewController.h"
#import "HubCitiConstants.h"
#import "SettingsViewController.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "CityExperienceViewController.h"
#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "FindViewController.h"
#import "AnythingPage.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "SingleCatRetailers.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FAQCategoryList.h"
#import "FilterListViewController.h"
#import "FundraiserListViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "SpecialOffersViewController.h"
#import "HTTPClient.h"
#import "GetUserInfoResponse.h"
#import "BandEventType.h"
#import "BandCategories.h"
#import "EventDetailImageViewController.h"
#import "bottomButtonView.h"
#import "RetailerAnnotation.h"
#import "CouponViewCell.h"
#import "CouponsMyAccountsViewController.h"


@interface CouponsInDealViewController ()<CustomizedNavControllerDelegate,SwipesViewControllerDelegate,MKMapViewDelegate,HTTPClientDelegate,MFMessageComposeViewControllerDelegate,MKMapViewDelegate>

{
    int k;
    NSMutableArray *arrBottomButtonViewContainer, *arrCouponBottomButtonDO;
     bottomButtonView *bottomBtnView;
    WebRequestState iWebRequestState;
    CommonUtility *common;
    BOOL isRemember;
    bottomButtonView *view;
    NSNumber *lastRecord;
    NSNumber *nextPage;
    BOOL showMapFlag;
    int bottomBtn;
    NSNumber *rowCountOfTotalCells;
    NSNumber *lowerLimitValue;
    NSMutableArray *featuredCouponArr,*otherCouponArr,*mapDetailsArray,*couponIdArrForMap;
    UIActivityIndicatorView *loading;
    NSString *couponIdString;
    UIAlertAction *zipSave;
}

@property (nonatomic, assign) NSInteger oldSegmentedIndex;
@end


@implementation CouponsInDealViewController
@synthesize tableViewOutlet,tableBottomConstraint,searchBar, segmentControlOutlet,sortObj,savedSortObj,infoResponse,couponsResponse,couponRetailerDetails,retailId,couponMapDetails, couponMapResponse, searchBarText,showMapFlag,globalRetailerService;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Service Respone methods
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription] ];
    
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    DLog(@"parameter: %@",responseObject);
    
    [self responseData:responseObject];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    
    
    [defaults setValue:nil forKey:@"couponzipcode"];
    
    //Setup text color and font for segment control text
    NSDictionary *attributesNormal = @{
                                       NSFontAttributeName : [UIFont boldSystemFontOfSize:15.0f],
                                       NSForegroundColorAttributeName : [UIColor whiteColor]};
    [segmentControlOutlet setTitleTextAttributes:attributesNormal forState:UIControlStateNormal];
    NSDictionary *attributesSelected = @{
                                         NSFontAttributeName : [UIFont boldSystemFontOfSize:15.0f],
                                         NSForegroundColorAttributeName : [UIColor blackColor]};
    [segmentControlOutlet setTitleTextAttributes:attributesSelected forState:UIControlStateSelected];
    [segmentControlOutlet addTarget:self action:@selector(viewSelectionChanged:) forControlEvents:UIControlEventValueChanged];
    //set title of the screen
    cusNav =(CustomizedNavController *) self.navigationController;
    [cusNav setTitle:@"Deals" forView:self withHambergur:YES];
    
    //customized home button navigation bar
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = mainPage;
    self.tableViewOutlet.hidden = YES;

    {
        UINib *cellNib = [UINib nibWithNibName:@"CouponsSwipeCell" bundle:nil];
        [tableViewOutlet registerNib:cellNib forCellReuseIdentifier:@"couponCollView"];
        
        
//        k = 0;
//        
//        if (k == 0)
//        {
//            tableBottomConstraint.constant = tableBottomConstraint.constant + 50;
//            [tableViewOutlet updateConstraints];
//            // [tableViewOutlet reloadData];
//        }

    [defaults setBool:NO forKey:@"ViewMoreCoupon"];
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
    sortObj = [[SortAndFilter alloc] init];
    lowerLimitValue = [[NSNumber alloc]initWithInt:0];
    sortObj.distanceSelected = YES;
    [defaults setValue:nil forKey:@"CouponMAINMENUID"];
    nextPage = [[NSNumber alloc]init];
    rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
    annArray = [[NSMutableArray alloc] init]; // Map Annotation points
    featuredCouponArr = [[NSMutableArray alloc]init];
    couponIdArrForMap = [[NSMutableArray alloc]init];
    otherCouponArr = [[NSMutableArray alloc]init];
        
        if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE])
        {
             [self requestForCoupons:searchBar.text :sortObj];
        }
        else if ([[defaults valueForKey:KEYZIPCODE]length])
        {
            [self requestForCoupons:searchBar.text :sortObj];
        }
        else
        {
            [self showZipEnter];
            
        }

        
   
    
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    [searchBar resignFirstResponder];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:NO];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    [defaults setValue:nil forKey:@"isFeatOrNonFeat"];
    if([segmentControlOutlet selectedSegmentIndex] == 1){
        
        [segmentControlOutlet setSelectedSegmentIndex:_oldSegmentedIndex];
        
    }
    if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
        nextPage = 0;
        rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
        [featuredCouponArr removeAllObjects];
        [otherCouponArr removeAllObjects];
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        [defaults setValue:nil forKey:@"ViewMoreBand"];
        sortObj = [[SortAndFilter alloc]initWithObject:[self loadCustomObjectWithKey:@"SortFilterObject"]];
        
        if(_oldSegmentedIndex == 0)
            [self requestForCoupons:searchBar.text :sortObj];

        [annArray removeAllObjects]; // Map Annotation points
    }
}
//- (void)viewDidLayoutSubviews
//{
//    [tableViewOutlet reloadData];
//}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [super viewWillDisappear:animated];
    
    for (NSOperation *operation in [HTTPClient sharedHTTPClient].manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        NSLog(@"operation:%@",operation);
        AFHTTPRequestOperation *operations = (AFHTTPRequestOperation*)operation;
        if([[[[operations request] URL] absoluteString] containsString:@"gallery/getallcoupbylocjson"]){
            [operation cancel];
        }
        
    }
}

-(void)returnToMainPage:(id)sender
{
   
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(void)popBackToPreviousPage{
    
    [self.navigationController popViewControllerAnimated:NO];
}

//TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (featuredCouponArr.count > 0 && otherCouponArr.count > 0)
    {
        return 2;
    }
    else if (featuredCouponArr.count > 0 || otherCouponArr.count > 0)
    {
        return 1;
    }
    else
        return 0;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    if(IPAD)
    {
        [defaults setDouble:tableViewOutlet.frame.size.height/2 - 28 - (28/2) forKey:@"collCellHeight"];
        return tableViewOutlet.frame.size.height/2 - 28;

    }
    else
    {
        return tableViewOutlet.frame.size.height/2 - 25;
    }
}
-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
        if(IPAD )
        {
            return 40;
        }
        else
        {
            return 25;
        }
    
}
-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
   // UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH, 25)];
    UIButton *couponHeader;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        couponHeader = [[UIButton alloc]initWithFrame:CGRectMake(20, 1, SCREEN_WIDTH, 18)];
        couponHeader.titleLabel.font = [UIFont boldSystemFontOfSize:15];
       
    }
    else
    {
        couponHeader = [[UIButton alloc]initWithFrame:CGRectMake(20, 1, SCREEN_WIDTH, 28)];
        couponHeader.titleLabel.font = [UIFont boldSystemFontOfSize:20];
        
    }
    
    
    [couponHeader setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [couponHeader.titleLabel setTextAlignment:NSTextAlignmentLeft];
    couponHeader.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    couponHeader.titleLabel.textAlignment = NSTextAlignmentLeft;
    [couponHeader setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
//    couponHeader.tag = section;
    [couponHeader addTarget:self action:@selector(sectionHeaderClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *_arrowRight = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - VARIABLE_WIDTH(25), 4 , VARIABLE_WIDTH(20), VARIABLE_HEIGHT(16))];
    
    _arrowRight.image = [UIImage imageNamed:@"rightArrow.png"];
    
    [couponHeader addSubview:_arrowRight];
    
    

    [tableViewOutlet setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    if(section == 0)
    {
        if(featuredCouponArr.count > 0)
        {
            [couponHeader setTitle:@"  Featured" forState:UIControlStateNormal];
        }
        else if(otherCouponArr.count > 0)
        {
            //[couponHeader setTitle:couponsResponse.Label forState:UIControlStateNormal];
            if ([couponsResponse.Label isEqualToString:@"distance"])
            {
                [couponHeader setTitle:@"  Distance" forState:UIControlStateNormal];
            }
            else if ([couponsResponse.Label isEqualToString:@"atoz"])
            {
                 [couponHeader setTitle:@"  Alphabetically" forState:UIControlStateNormal];
            }
////            if(sortObj.distanceSelected)
////            {
////                [couponHeader setTitle:@"Distance" forState:UIControlStateNormal];
////            }
////            else if (sortObj.alphabeticallySelected)
////            {
////                [couponHeader setTitle:@"A-Z" forState:UIControlStateNormal];
////            }
        }
    }
    else
    {
 //       [couponHeader setTitle:couponsResponse.Label forState:UIControlStateNormal];
        if ([couponsResponse.Label isEqualToString:@"distance"])
        {
            [couponHeader setTitle:@"  Distance" forState:UIControlStateNormal];
        }
        else if ([couponsResponse.Label isEqualToString:@"atoz"])
        {
            [couponHeader setTitle:@"  Alphabetically" forState:UIControlStateNormal];
        }

//        if(sortObj.distanceSelected)
//        {
//            [couponHeader setTitle:@"Distance" forState:UIControlStateNormal];
//        }
//        else if (sortObj.alphabeticallySelected)
//        {
//            [couponHeader setTitle:@"A-Z" forState:UIControlStateNormal];
//        }
    }
    return couponHeader;

}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *sectionTitle;
    if(section == 0)
    {
        if(featuredCouponArr.count > 0)
        {
            sectionTitle = @"Featured";
            
        }
        else if(otherCouponArr.count > 0)
        {
            sectionTitle = couponsResponse.Label;
        }
    }
    else
    {
        sectionTitle = couponsResponse.Label;
        
    }
    
    return sectionTitle;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CouponsSwipeCell *cell;
    static NSString *simpleTableIdentifier = @"couponCollView";
    cell = (CouponsSwipeCell *)[tableViewOutlet dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.sectionTitleLabel = [self tableView:tableView titleForHeaderInSection:indexPath.section];
    cell.fromCoup = YES;
    cell.sortObj = sortObj;
    
    if(indexPath.section == 0)
    {
        if(featuredCouponArr.count > 0)
        {
            cell.swipeCouponsList = [NSMutableArray arrayWithArray:featuredCouponArr];
        }
        else if (otherCouponArr.count > 0)
        {
            cell.swipeCouponsList = [NSMutableArray arrayWithArray:otherCouponArr];
        }
        
    }
    else
    {
        cell.swipeCouponsList = [NSMutableArray arrayWithArray:otherCouponArr];
    }
    
    if(bottomBtn==1)
    {
        [tableViewOutlet layoutIfNeeded];
        cell.couponCollViewTopConst.constant = 0;
        [tableViewOutlet updateConstraints];
        
    }
    cell.couponSection = indexPath.section;
    cell.parentViewController = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [cell.couponCollView reloadData];
        [cell.couponCollView setContentOffset:CGPointZero];
        [cell.contentView setNeedsDisplay];
    });

    
    //    [cell.contentView setNeedsDisplay];
    //    [cell awakeFromNib];
    //    cell.couponCollView.dataSource = cell;
    //    cell.couponCollView.delegate = cell;
    // [cell.couponCollView reloadData];
    // ... set up the cell here ...
    
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    return cell;
    
    
    
}
//- (BOOL)allowsHeaderViewsToFloat{
//    return NO;
//}
//Section Header clicked
-(void)sectionHeaderClicked:(UIButton*)btn
{

    ListCouponsViewController *ListCouponsView = [[ListCouponsViewController alloc]initWithNibName:@"ListCouponsViewController" bundle:[NSBundle mainBundle]];
    ListCouponsView.featuredOrNonFeatured = btn.titleLabel.text;
    ListCouponsView.sortObj =sortObj;
    ListCouponsView.fromCoupons = YES;
    if([[defaults valueForKey:@"couponzipcode"] length])
        ListCouponsView.couponsPostalCodeList = [defaults valueForKey:@"couponzipcode"];
    if ([searchBar.text length] > 0)
    {
        ListCouponsView.searchTextFromPreviousScreen = searchBar.text;
    }
    [self.navigationController pushViewController:ListCouponsView animated:NO];
}
// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    // Calculate Y position for View
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [arrCouponBottomButtonDO count]; btnLoop++)
    {
        bottomButtonDO *obj_eventBottomDO = [arrCouponBottomButtonDO objectAtIndex:btnLoop];
        
        bottomBtnView = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            bottomBtnView.contentMode = UIViewContentModeCenter;
            bottomBtnView = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, 80,bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            bottomBtnView = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrCouponBottomButtonDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrCouponBottomButtonDO count],bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [bottomBtnView setAccessibilityLabel:obj_eventBottomDO.btnLinkTypeName];
        [bottomBtnView setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:bottomBtnView];
        [self.view addSubview:bottomBtnView];
        [self.view bringSubviewToFront:bottomBtnView];
    }
}

#pragma bottombutton functionality

-(void)bottomButtonPressed:(id)sender
{
    if([defaults boolForKey:@"ViewMore"])
        return;
    [defaults setBool:NO forKey:@"ViewMoreBand"];
    [defaults setBool:YES forKey:BottomButton];
    //   [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    //NSlog(@"The tag is %d",tag);
    bottomButtonDO *obj_eventBottomDO = [arrCouponBottomButtonDO objectAtIndex:tag];
    
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=27||[[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=28){
        [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    }
    [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_eventBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_eventBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_eventBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_eventBottomDO.btnLinkTypeName);
    if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    
    else{ //other menu types
        switch ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                //[hotDeals release];
                break;
                
            case 1://Scan Now
            {
                
            }
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
                }
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                
                [self navigateToFindView];
            }
                
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
            {
                
            }
                
                break;
                
            case 8://Visitors Bureau
            {
                
            }
                
                break;
                
            case 9://transportation
            {
                
            }
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    //[alert release];
                    
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];                   //[alert release];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {
                    [self shareClickedAction:btn];
                }
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_eventBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                    
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    // CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];;
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
//                [self navigateToCouponsViewController];
//                break;
            }
            case 19: // Deals
                break;
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }
                break;
            case 22:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }
                break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        //[inform release];
                    }
                    
                }
                
            }break;
            case 25:
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            //  [citPref release];
                        }
                    }
                }
                
            }break;
            case 27:
            case 28://sort/filter
            {
                if([segmentControlOutlet selectedSegmentIndex]==0|| [segmentControlOutlet selectedSegmentIndex] == 1)
                {
                    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
                    [defaults setValue:[defaults valueForKey:@"CouponMAINMENUID"] forKey:KEY_MITEMID];
                    
                    SwipesViewController *iSwipeViewController1 = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                    iSwipeViewController1.module = @"Coupons";
                    iSwipeViewController1.delegate = self;
                    if([[defaults valueForKey:@"couponzipcode"] length])
                        iSwipeViewController1.couponsPostalCodeSort = [defaults valueForKey:@"couponzipcode"];
                if ([searchBar.text length] > 0)
                {
                    iSwipeViewController1.srchKey = searchBar.text;
                }
                    if(isRetailerEvent == TRUE)
                    {
                        iSwipeViewController1.isRetailerEvent = TRUE;
                    }
                    if([segmentControlOutlet selectedSegmentIndex] ==1)
                    {
                        
                    }
                       //iSwipeViewController1.srchKeyBand = searchResultBand;
                    else
                        //iSwipeViewController1.srchKeyBand = searchResultBandName;
                    
                    iSwipeViewController1.sortFilObj = [[SortAndFilter alloc]initWithObject:sortObj];
                    
                    
                    [self.navigationController pushViewController:iSwipeViewController1 animated:NO];
                }
            }
                break;
                case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                if([[defaults valueForKey:@"couponzipcode"] length])
                dvc.coupnsPostalCode = [defaults valueForKey:@"couponzipcode"];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
              //  privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
            default:
                break;
        }
    }
    
}

- (void)addItemViewController:(SwipesViewController *)controller didFinishEnteringItem:(SortAndFilter *)item
{
    if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
        sortObj = [[SortAndFilter alloc]initWithObject:item];
        rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
        [featuredCouponArr removeAllObjects];
        [otherCouponArr removeAllObjects];
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        [self requestForCoupons:searchBar.text :item];
        
        
    }
}

#pragma mark action sheet methods
-(void)shareClickedAction:(id)sender{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
        //[alert release];
    }
    else
    {
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheetForShare:sender];
    }
}
-(void) showActionSheetForShare:(id)sender {
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    UIButton *btnView = (UIButton*) sender;
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = btnView;
        actionSheet.popoverPresentationController.sourceRect = btnView.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {
    _anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:_anyVC.view]; }
                    completion:nil];
    
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:@"Oops, SMS not supported.." msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
        
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    _emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    _emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [_emailSendingVC loadMail];
    
}

-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
            
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)responseData:(id ) response
{
    switch (iWebRequestState)
    {
            
        case COUPONS_REQUEST:
            [self parseCoupons:response];
            break;
            //        case BANDS_EVENTS_INFO:
            //            [self parse_EventSearchData:response];
            //            break;
        case PARTNERRET:
            [self parse_PartnerRet:response];
            break;
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
//        case COUPON_MAP:
//            [self parseMapListing:response];
   //         break;
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
            
            
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
            
        default:
            break;
            
            
            
    }
    
}

-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            
            
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}



//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];                    //[alert release];
                    
                    //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc]init];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }
    
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}

//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    else if ([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}
-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
}
-(void)parseGetUserData:(id)response
{
    
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}

-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
        
        
        isRemember = YES;
        
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}



//-(void)navigateToCouponsViewController
//{
//    [defaults setBool:YES forKey:@"myGallerysplash"];
//    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
//    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
//    [self.navigationController pushViewController:iCouponsViewController animated:NO];
//    //[iCouponsViewController release];
//}


-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}


-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}

//Call Find Service and Class
-(void)navigateToFindView
{
    
    //    [defaults setValue:@"0" forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    
    navigatedFromMainMenu = FALSE;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}
-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}


-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=FALSE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}
//{
//    "userId":"10021",
//    "hubCitiId":"2070",
//    "postalCode":"78654",
//    "isFeatOrNonFeat":null, if 0 to get only featured list and 1 to get only nonfeatured list
//        "lastVisitedNo":"0",
//        "sortColumn":"distance",
//        "sortOrder":"asc"
//        }
//
//
//Other paramters:
//Longitude
//Latitude
//searchKey
//cityIds
//catIds
//retailId
//mainMenuId

//REquest for coupon listing
-(void) requestForCoupons:(NSString *)searchText :(SortAndFilter *) sortFilObj
{
    iWebRequestState = COUPONS_REQUEST;
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    if (searchText.length > 0)
    {
        [parameter setValue:searchText forKey:@"searchKey"];
    }
    if([defaults valueForKey:KEY_USERID])
    {
        [parameter setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if([defaults valueForKey:KEY_HUBCITIID])
    {
        [parameter setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    
    [parameter setValue:rowCountOfTotalCells forKey:@"lastVisitedNo"];
    if(sortObj.distanceSelected)
    {
        [parameter setValue:@"distance" forKey:@"sortColumn"];
    }
    if(sortObj.alphabeticallySelected)
    {
        [parameter setValue:@"atoz" forKey:@"sortColumn"];
    }
    [parameter setValue:@"asc" forKey:@"sortOrder"];
    if (sortObj.selectedCitiIds)
    {
        [parameter setValue:sortObj.selectedCitiIds forKey:@"cityIds"];
    }
    
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length])
    {
        [parameter setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        [parameter setValue:[defaults valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        
    }
    else  if ([[defaults valueForKey:KEYZIPCODE] length])
    {
        [parameter setValue:[defaults valueForKey:KEYZIPCODE]forKey:@"postalCode"];
        
    }
    else if ([[defaults  valueForKey:@"couponzipcode"]length]){
        
        [parameter setValue:[defaults  valueForKey:@"couponzipcode"] forKey:@"postalCode"];
    }
    if (sortObj.selectedCatIds)
    {
        [parameter setValue:sortObj.selectedCatIds forKey:@"catIds"];
    }
    
    if(isRetailerEvent == TRUE)
    {
        [parameter setValue:retailId forKey:@"retailId"];
    }
    
    if([defaults valueForKey:KEY_MAINMENUID])
    {
        [defaults setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"CouponMAINMENUID"];
        [parameter setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    }
    NSString *urlString = [NSString stringWithFormat:@"%@gallery/getallcoupbylocjson",BASE_URL];
    // NSString *urlString = [NSString stringWithFormat:@"http://10.10.221.155:9990/HubCiti2.8.2/gallery/getallcoupbylocjson"];
    NSLog(@"parameter: %@", parameter);
    //    if ([defaults boolForKey:@"ViewMoreCoupon"])
    //    {
    //        NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameter];
    //        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    //        id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    //        [HubCitiAppDelegate removeActivityIndicator];
    //        [self parseCoupons:responseData];
    //    }
    //
    //    else
    
    
    HTTPClient *client = [[HTTPClient alloc] init];
    client.delegate = self;
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameter : urlString];
    [HubCitiAppDelegate showActivityIndicator];
    
    
    
}
-(void)parseCoupons:(id) responseString{
    [HubCitiAppDelegate removeActivityIndicator];
    if(responseString == NULL)
        return;
  
    couponsResponse = [[CouponsResponse alloc]init];
    couponIdArrForMap = [[NSMutableArray alloc]init];
    couponIdString = nil;
    
    
    @try{
        [couponsResponse setValuesForKeysWithDictionary:responseString];
    }
    
    
    @catch (NSException *exception) {
        // Do nothing
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
        return;
    }
    
    NSLog(@"CouponsResponse: %@",couponsResponse);
    
    if([couponsResponse.responseCode isEqualToString:@"10000"])
    {
        if(couponsResponse.nextPage)
        {
            nextPage = couponsResponse.nextPage;
        }
        if(couponsResponse.maxRowNum)
        {
            rowCountOfTotalCells = couponsResponse.maxRowNum;
        }
        if(couponsResponse.featuredCouponsList)
        {
            for(int l=0; l < couponsResponse.featuredCouponsList.count;l++)
            {
                CouponsRetailerDetails *couponDetails = [[CouponsRetailerDetails alloc] init];
                
                
                NSDictionary *dictList = couponsResponse.featuredCouponsList[l];
                
                
                @try{
                    [couponDetails setValuesForKeysWithDictionary:dictList];
                }
                @catch (NSException *exception) {
                    // Do nothing
                    [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
                    return;
                }
                
                
                [featuredCouponArr addObject:couponDetails];
                if(featuredCouponArr.count <= 10)
                {
                    [couponIdArrForMap addObject:couponDetails.couponId];
                }
                
            }
        }
        if(couponsResponse.nonFeaturedCouponsList)
        {
           
            for(int l=0; l < couponsResponse.nonFeaturedCouponsList.count; l++)
            {
                CouponsRetailerDetails *couponDetails = [[CouponsRetailerDetails alloc] init];
                
                
                NSDictionary *dictList = couponsResponse.nonFeaturedCouponsList[l];
                
                
                @try{
                    [couponDetails setValuesForKeysWithDictionary:dictList];
                }
                @catch (NSException *exception) {
                    // Do nothing
                    [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
                    return;
                }
                
                [otherCouponArr addObject:couponDetails];
                if(otherCouponArr.count <=10)
                {
                   [couponIdArrForMap addObject:couponDetails.couponId];
                }
                
            }
        }
        if([couponsResponse.bottomBtn integerValue] == 1)
        {
            bottomBtn = [couponsResponse.bottomBtn intValue];
            arrCouponBottomButtonDO=[[NSMutableArray alloc]init];
            for (int i=0; i < couponsResponse.bottomBtnList.count; i++) {
                bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                NSDictionary* dictList = couponsResponse.bottomBtnList[i];
                
                [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                
                if(obj_findBottomDO.bottomBtnName.length == 0)
                {
                    obj_findBottomDO.bottomBtnName = @" ";
                }
                if(obj_findBottomDO.bottomBtnImg.length == 0)
                {
                    obj_findBottomDO.bottomBtnImg = @" ";
                }
                if(obj_findBottomDO.bottomBtnImgOff.length == 0)
                {
                    obj_findBottomDO.bottomBtnImgOff = @" ";
                }
                if(obj_findBottomDO.btnLinkTypeName)
                {
                    if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"])
                    {//for filters
                        [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                    }
                }
                [arrCouponBottomButtonDO addObject:obj_findBottomDO];
                
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if(![defaults boolForKey:@"ViewMoreEvent"]){
                    if([arrCouponBottomButtonDO count] > 0)
                    {
                        // Set Bottom Menu Button after read from response data
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self setBottomBarMenu];
                        });
                        
                        [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                    }
                    else
                        [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
                    
                }
            });
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if(bottomBtn == 1 && tableBottomConstraint.constant == 0)
            {
                tableBottomConstraint.constant = tableBottomConstraint.constant + 50;
                [tableViewOutlet updateConstraints];
            }
            self.tableViewOutlet.hidden = NO;
            [tableViewOutlet reloadData];
            
            [tableViewOutlet setContentOffset:CGPointZero animated:YES];
        });
        
        
    }
    else if([couponsResponse.responseCode isEqualToString:@"10002"])
    {
        nextPage=0;
        
        NSString *responseTextStr;
        if (couponsResponse.responseText != nil) {
            responseTextStr = couponsResponse.responseText;
        }
        else
            responseTextStr = @"No Records Found";
        dispatch_async(dispatch_get_main_queue(), ^{
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
            dispatch_async(dispatch_get_main_queue(), ^{
                tableViewOutlet.hidden = NO;
                [tableViewOutlet reloadData];
                [tableViewOutlet setContentOffset:CGPointZero animated:YES];
            });
        });
        
    }
    else
    {//show the error message in alert
        nextPage=0;
        
        
        if (couponsResponse.responseText != nil) {
            
            
            UIAlertController * alert;
            
            alert=[UIAlertController alertControllerWithTitle:couponsResponse.responseText message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){

                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
           // [loading stopAnimating];
            
            
        }
        else
        {
            UIAlertController * alert;
            
            alert=[UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"No Records found"] message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){

                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
//            [loading stopAnimating];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            tableViewOutlet.hidden = NO;
            [tableViewOutlet reloadData];
            
            [tableViewOutlet setContentOffset:CGPointZero animated:YES];
            
        });
        
        
    }

}
- (SortAndFilter *)loadCustomObjectWithKey:(NSString *)key {
    
    NSData *encodedObject = [defaults objectForKey:key];
    SortAndFilter *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    NSLog(@"SortAndFilter object in loadCustomObj: %@",object);
    return object;
}

#pragma mapView delegate methods
// Swap betWeen tableView and MapView

-(void)showMap
{
    searchBar.hidden = YES;
     mapView = [[MKMapView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    //self.navigationItem.rightBarButtonItem.tag = 2 ;
    //UIButton *showListBtn = [UtilityManager customizeBarButtonItem:@"Show List"];
    //[showListBtn addTarget:self action:@selector(swapListMap:) forControlEvents:UIControlEventTouchUpInside];
    // showListBtn.tag=2;
    //[self.navigationItem.rightBarButtonItem setCustomView:showListBtn];
    
    mapView.hidden = NO;
    tableViewOutlet.hidden = YES;
    
    mapView.hidden = NO;
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    [mapView setDelegate:self];
    // CLLocationCoordinate2D southWest;
    // CLLocationCoordinate2D northEast;
    
    //check is reqd as in case only scansee data is there and no google data it would crash
    
    //    southWest = CLLocationCoordinate2DMake([[[globalRetailerService objectAtIndex:0]retLatitude] doubleValue],
    //                                           [[[globalRetailerService objectAtIndex:0]retLongitude] doubleValue]);
    // northEast = southWest;
    
    
    for(int i=0; i<[globalRetailerService count]; i++) {
        RetailerAnnotation *ann = [[RetailerAnnotation alloc] init];
        ann.coordinate = CLLocationCoordinate2DMake([[[globalRetailerService objectAtIndex:i]retLatitude] doubleValue],
                                                    [[[globalRetailerService objectAtIndex:i]retLongitude] doubleValue]);
        [mapView addAnnotation:ann];
        [annArray addObject:ann];
        [ann setTitle:[[globalRetailerService objectAtIndex:i]couponName]];
        //[ann setSubtitle:[[globalRetailerService objectAtIndex:i]location]];
        ReleaseAndNilify(ann);
        
    }
    
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in mapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    
    [mapView setVisibleMapRect:zoomRect animated:YES];
    
     [self.view addSubview:mapView];
}




-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    MKPinAnnotationView* pinView = nil;
    
    if ([annotation isKindOfClass:[RetailerAnnotation class]])
    {
        // try to dequeue an existing pin view first
        static NSString* RetailerAnnotationIdentifier = @"retailerAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:RetailerAnnotationIdentifier];
        
        if (!pinView)
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
                                                  initWithAnnotation:annotation reuseIdentifier:RetailerAnnotationIdentifier] ;
            //customPinView.pinColor = MKPinAnnotationColorPurple;
            customPinView.animatesDrop = YES;
            
            customPinView.canShowCallout = YES;
            UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            
            customPinView.rightCalloutAccessoryView = infoButton;
            
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
    }
    pinView.canShowCallout = YES;
    return pinView;
}

-(void)viewSelectionChanged:(id)sender
{
    
    switch ([segmentControlOutlet selectedSegmentIndex]) {
        case 0:
        {
            [searchBar resignFirstResponder];
          //  ReleaseAndNilify(tableViewOutlet);
            [loading stopAnimating];
            [defaults setBool:NO forKey:@"ViewMore"];
            //[locServiceDealsTable setContentOffset:CGPointZero];
            _oldSegmentedIndex = 0;
            
            if(featuredCouponArr)
                [featuredCouponArr removeAllObjects];
            
            if(otherCouponArr)
                [otherCouponArr removeAllObjects];
            
            if(globalRetailerService)
                [globalRetailerService removeAllObjects];
            rowCountOfTotalCells=0;
            
            [self requestForCoupons:searchBar.text :sortObj];
            
            
        }
            break;
        
        case 1:{
            [searchBar resignFirstResponder];
            
           // [self requestForMapLocation: couponIdString];
            if(featuredCouponArr.count>0 || otherCouponArr.count>0){
            CouponsMapViewController *iShowMapController =[[CouponsMapViewController alloc]initWithNibName:@"CouponsMapViewController" bundle:[NSBundle mainBundle]];
            iShowMapController.showMapFlag = TRUE;
                if([[defaults valueForKey:@"couponzipcode"] length])
                    iShowMapController.coponmappostalcode = [defaults valueForKey:@"couponzipcode"];
            if(couponIdArrForMap.count > 0)
            {
                
                couponIdString = [couponIdArrForMap componentsJoinedByString:@","];


            }
            iShowMapController.couponIds = couponIdString;
            //iShowMapController.globalRetailerService = globalRetailerService;
            [self.navigationController pushViewController:iShowMapController animated:NO];
            }
            else{
                [segmentControlOutlet setSelectedSegmentIndex:_oldSegmentedIndex];
               
                [UtilityManager showAlert:nil msg:NSLocalizedString(@"Deals are not available to plot on map",@"Deals are not available to plot on map")];
            }
            
        }
            break;
        default:
            break;
    }
}


// Display Enter ZipCode PopUp
-(void)showZipEnter
{
    
    // use UIAlertController
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Please enter your zip code"
                               message:nil
                               preferredStyle:UIAlertControllerStyleAlert];
    
    zipSave = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action){
                                         //Do Some action here
                                         UITextField *textField = alert.textFields[0];
                                         [defaults setValue:textField.text forKey:@"couponzipcode"];
                                        
                                          [self requestForCoupons:searchBar.text :sortObj];
                                         
                                     }];
    zipSave.enabled = NO;
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                    [self requestForCoupons:searchBar.text :sortObj];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:zipSave];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        [textField setTag:31];
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.font = [UIFont systemFontOfSize:16];
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
}
#pragma keyboard delegate

//check the characters in zipcode textField
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // textField for ZipCode
    if(textField.tag == 31){
        NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        if (range.length == 1){
            NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [zipSave setEnabled:(finalString.length >= 5)];
            return YES;
        }
        else {
            for (int j = 0; j<[string length]; j++) {
                unichar c = [string characterAtIndex:j];
                if ([charSet characterIsMember:c]) {
                    NSUInteger textLength = [textField.text length] + [string length] - range.length;
                    [zipSave setEnabled:(textLength >= 5)];
                    return (textLength > 5)? NO : YES;
                }
                else {
                    return NO;
                }
            }
        }
    }
    
    return YES;
}




#pragma mark UISearchBar
#pragma Delegates for UISearchBar
-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    nextPage = 0;
    rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
    if (timer){
        [timer invalidate];
        timer = nil;
    }
    if(searchText.length == 0)
    {
        if (timer){
            [timer invalidate];
            timer = nil;
        }
        [featuredCouponArr removeAllObjects];
        [otherCouponArr removeAllObjects];
        //[self.searchBar resignFirstResponder];
        [self requestForCoupons:searchText :sortObj];
        [self.searchBar performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0.1];
    }
    if(searchText.length >= 3)
    {
        
        if (![timer isValid]){
            featuredCouponArr = [[NSMutableArray alloc]init];
            otherCouponArr = [[NSMutableArray alloc]init];
            timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(call_RequestForCoupon) userInfo:nil  repeats:NO];
        }
    }
    
}
-(void)call_RequestForCoupon
{
    [self requestForCoupons:searchBar.text :sortObj];
}
-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBarDelegate
{
    nextPage = 0;
    rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
    if (timer){
        [timer invalidate];
        timer = nil;
    }
    featuredCouponArr = [[NSMutableArray alloc]init];
    otherCouponArr = [[NSMutableArray alloc]init];
    [self requestForCoupons:searchBarDelegate.text :sortObj];
    [searchBarDelegate resignFirstResponder];
    // [HubCitiAppDelegate removeActivityIndicator];
    //[searchTableView reloadData];
    
    
}
-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBarDelegate
{
    [searchBarDelegate resignFirstResponder];
    
    if (timer){
        [timer invalidate];
        timer = nil;
    }
   
        featuredCouponArr = [[NSMutableArray alloc]init];
        otherCouponArr = [[NSMutableArray alloc]init];
        nextPage = 0;
        rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
    [self requestForCoupons:nil :sortObj];
   
    searchBarDelegate.text = nil;
    
}
-(void) call_DetailScreen:(CouponsRetailerDetails *)retailerDetails
{
    NewCouponDetailViewController *couponDetailVC = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
    [defaults setValue:retailerDetails.couponId forKey:@"couponId"];
    [defaults setValue:retailerDetails.couponListId forKey:@"couponListId"];
    [self.navigationController pushViewController:couponDetailVC animated:NO];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end

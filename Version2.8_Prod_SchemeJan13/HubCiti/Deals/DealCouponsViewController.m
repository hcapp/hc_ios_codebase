//
//  CouponsViewController.m
//  Scansee
//
//  Created by ajit on 9/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DealCouponsViewController.h"
#import "MainMenuViewController.h"
#import "NewCouponDetailViewController.h"
#import "WebBrowserViewController.h"
#import "ProductPage.h"
#import "RetailerSummaryViewController.h"
//#import "AddRemLoyaltyCardViewController.h"
#import "PreferredCategoriesScreen.h"
//#import "MyCouponPopulationCenterCities.h"
#import "GalleryList.h"
#import "CouponsMyAccountsViewController.h"
#import "LoginViewController.h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "MainMenuViewController.h"
#import "SingleCatRetailers.h"
#import "EventsListViewController.h"
#import "AlertsList.h"
#import "CityExperienceViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "HubCitiConstants.h"
#import "AppDelegate.h"
#import "FilterRetailersList.h"
#import "FilterListViewController.h"
#import "RetailersListViewController.h"
#import "FundraiserListViewController.h"
#import "FAQCategoryList.h"
#import "LocationDetailsViewController.h"
#import "CouponsViewController.h"
#import "EmailShareViewController.h"
#import "PreferredCategoriesScreen.h"
#import "WebBrowserViewController.h"
#import "AnythingPage.h"
#import "SpecialOfferRetailersViewController.h"
#import "DealHotDealsList.h"
#import "GalleryDisplayViewController.h"
#import "CurrentSpecials.h"
#import "UserSettingsController.h"
#import "CityPreferenceViewController.h"
#import "UserInformationViewController.h"
#import "SpecialOffersViewController.h"
#include "HTTPClient.h"
#include "GetUserInfoResponse.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import <Contacts/Contacts.h>



@implementation DealCouponsViewController{
    UIActivityIndicatorView *loading;
    bottomButtonView *view;
    CustomizedNavController *cusNav;
}
//@synthesize searchView,objDeals,mViewController,anyVC;
@synthesize infoResponse;
@synthesize searchView,objDeals,mViewController,anyVC,emailSendingVC;
//static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
#pragma mark - Service Respone methods

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    [self responseData:responseObject];
    
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    [self.view setAccessibilityLabel:@"couponsView"];
   cusNav = (CustomizedNavController*)self.navigationController;
    [cusNav setTitle:@"Deals" forView:self withHambergur:YES];
    //self.title = NSLocalizedString(@"Coupons",@"Coupons");
    [defaults setValue:nil forKey:@"couponzipcode"];
    
    //	if ([defaults  boolForKey:@"myGallerysplash"] == YES)
    //		[self showSplash];
    
    loading = nil;
    [defaults setValue:nil forKey:@"popCentId"];
    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    catPicker.hidden = YES;
    toolBar.hidden = YES;
    searchTextField.delegate = self;
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"MainMenu"];
    //    //[mainPage release];
    //
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    [SharedManager setCouLoySearchActive:NO];
    [SharedManager setRefreshGallery:NO];
    searchView.hidden = YES;
    myGalleryTV.userInteractionEnabled = YES;
    myGalleryTV.backgroundColor = [UIColor whiteColor];
    segmentedControl.userInteractionEnabled = YES;
    couponGalleryBtn.userInteractionEnabled = YES;
    bycityBtn.userInteractionEnabled = YES;
    searchBtn.userInteractionEnabled = YES;
    preferanceBtn.userInteractionEnabled = YES;
    
    searchView.layer.cornerRadius = 10;
    [searchView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [searchView.layer setBorderWidth:1.5f];
    [searchView.layer setShadowColor:[UIColor blackColor].CGColor];
    [searchView.layer setShadowOpacity:0.8];
    [searchView.layer setShadowRadius:3.0];
    [searchView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    searchView.backgroundColor = [UIColor colorWithRed:95 green:183 blue:215 alpha:1.0];
    
    [self.view bringSubviewToFront:searchView];
    
   // NSString *claimOff = @"cs_claimed_off", *claimOn = @"cs_claimed_on";
  //  NSString *expiredOff = @"cs_expired_off", *expiredOn = @"cs_expired_on";
  //  NSString *usedOff = @"cs_used_off", *usedOn = @"cs_used_on";
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
       // claimOff = @"claimed_off_iPad1";
      //  claimOn = @"claimed_on_iPad1";
        
       // expiredOff = @"expired_off_iPad1";
      //  expiredOn = @"expired_on_iPad1";
        
       // usedOff = @"used_off_iPad1";
       // usedOn = @"used_on_iPad1";
        
        couponGalleryBtn.frame = CGRectMake(0, couponGalleryBtn.frame.origin.y, SCREEN_WIDTH/3, couponGalleryBtn.frame.size.height);
        searchBtn.frame = CGRectMake(couponGalleryBtn.frame.origin.x + couponGalleryBtn.frame.size.width, searchBtn.frame.origin.y, SCREEN_WIDTH/3, searchBtn.frame.size.height);
        preferanceBtn.frame = CGRectMake(searchBtn.frame.origin.x + searchBtn.frame.size.width, preferanceBtn.frame.origin.y, SCREEN_WIDTH/3, preferanceBtn.frame.size.height);
        
        available.frame = CGRectMake((SCREEN_WIDTH - available.frame.size.width)/2, available.frame.origin.y, available.frame.size.width, available.frame.size.height);
        available.font = [UIFont boldSystemFontOfSize:23.0];
    }
    
    //    couponGalleryBtn.tag = 1000;
    //
    //        [couponGalleryBtn setImage:[UIImage imageNamed:claimOff] forState:UIControlStateNormal];
    //        [couponGalleryBtn setImage:[UIImage imageNamed:claimOn] forState:UIControlStateHighlighted];
    //
    //    [couponGalleryBtn setAccessibilityLabel:@"claimed"];
    //    //[couponGalleryBtn removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
    //    [couponGalleryBtn addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //
    //
    //	searchBtn.tag = 1001;
    //
    //        [searchBtn setImage:[UIImage imageNamed:expiredOff] forState:UIControlStateNormal];
    //        [searchBtn setImage:[UIImage imageNamed:expiredOn] forState:UIControlStateHighlighted];
    //
    //
    ////    [searchBtn removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
    //    [searchBtn setAccessibilityLabel:@"expired"];
    //    [searchBtn addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //
    //
    //    preferanceBtn.tag = 1002;
    //    [SharedManager setEnableAddCoupon:NO];
    
    //        [preferanceBtn setImage:[UIImage imageNamed:usedOff] forState:UIControlStateNormal];
    //        [preferanceBtn setImage:[UIImage imageNamed:usedOn] forState:UIControlStateHighlighted];
    //
    //
    //    [preferanceBtn removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
    //    [preferanceBtn setAccessibilityLabel:@"used"];
    //    [preferanceBtn addTarget:self action:@selector(bottomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //myGalleryTV.tag = 3;
    //self.title = NSLocalizedString(@"My Coupons",@"My Coupons");
    [SharedManager setMyGallery:YES];
    
    arrCatagories = [[NSMutableArray alloc]init];
    dicProductsCoupon = [[NSMutableDictionary alloc]init];
    
    dicLocationCupon = [[NSMutableDictionary alloc]init];
    arrLocationCategoriesforRetailers = [[NSMutableArray alloc]init];
    arrSectionArrayForRetailers = [[NSMutableArray alloc]init];
    [arrSectionArrayForRetailers addObject:@"1"];
    
    if(localProductFlag)
        localProductFlag=NO;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
     [super viewDidAppear:animated];
    // If user is coming from anywhere other then main menu "getMuCouponItemsforLocation" will call otherwise it will call from HideSplash
    lowerLimitvalue = 0;
    if(localProductFlag){
        [self addsegementcontrolleronView];
        myGalleryTV.tag = 0;
        [self getMuCouponItemsforProducts];
        
    }
    else
    {
        
        [self getMuCouponItemsforLocation];
        
    }
    [self setWhenCalledfromSegementChange];
    [self updateViewonViewWillAppear];
}


//To get Copuons based on retailers location if users location data is there otherwise display only products data
-(void)getMuCouponItemsforLocation
{
    if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE])
    {
        [self addsegementcontrolleronView];
        myGalleryTV.tag = 1;
        [self callLocationbasedCoupons];
    }
    else if ([[defaults valueForKey:KEYZIPCODE]length])
    {
        myGalleryTV.tag = 1;
        [self addsegementcontrolleronView];
        [self callLocationbasedCoupons];
    }
    else
    {
        myGalleryTV.tag = 0;
        [self addsegementcontrolleronView];
        [self showZipEnter];
        
    }
}


// Display Enter ZipCode PopUp
-(void)showZipEnter
{
    
        // use UIAlertController
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Please enter your zip code"
                                   message:nil
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        zipSave = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       UITextField *textField = alert.textFields[0];
                                                       [defaults setValue:textField.text forKey:@"couponzipcode"];
                                                       myGalleryTV.tag = 0;
                                                       [self callLocationbasedCoupons];
                                                       
                                                   }];
        zipSave.enabled = NO;
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           NSLog(@"cancel btn");
                                                           
                                                           segmentedControl.selectedSegmentIndex=1;
                                                           [self getMuCouponItemsforProducts];
                                                           
                                                       }];
        
        [alert addAction:cancel];
        [alert addAction:zipSave];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            [textField setTag:31];
            textField.delegate = self;
            textField.keyboardType = UIKeyboardTypeNumberPad;
            textField.font = [UIFont systemFontOfSize:16];
        }];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    
    
}


//check the characters in zipcode textField
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // textField for ZipCode
    if(textField.tag == 31){
        NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        if (range.length == 1){
            NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [zipSave setEnabled:(finalString.length >= 5)];
            return YES;
        }
        else {
            for (int j = 0; j<[string length]; j++) {
                unichar c = [string characterAtIndex:j];
                if ([charSet characterIsMember:c]) {
                    NSUInteger textLength = [textField.text length] + [string length] - range.length;
                    [zipSave setEnabled:(textLength >= 5)];
                    return (textLength > 5)? NO : YES;
                }
                else {
                    return NO;
                }
            }
        }
    }
    
    return YES;
}




-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}




// If users location data is available then only will display segement otherwise only products will display on the screen
-(void)addsegementcontrolleronView
{
    if(searchBar)
    {
        [searchBar removeFromSuperview];
        //[searchBar release];
    }
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    searchBar.showsCancelButton = YES;
    // [searchBar setBackgroundImage:[UIImage imageNamed:@"NavBar_Bg.png"]];
    [searchBar setBarTintColor:[UIColor darkGrayColor]];
    searchBar.delegate = self;
    
    [self.view addSubview:searchBar];
    
    if(segmentedControl)
    {
        [segmentedControl removeFromSuperview];
        //[segmentedControl release];
    }
    // Set the two segement one for Location and another for Product
    NSArray *items = [NSArray arrayWithObjects:@"Locations",@"Products", nil];
    segmentedControl = [[HubCitiSegmentedControl alloc] initWithItems:items];
    [segmentedControl setFrame:CGRectMake(0, searchBar.frame.origin.y+searchBar.frame.size.height, SCREEN_WIDTH, 44)];
    if(localProductFlag)
        [segmentedControl setSelectedSegmentIndex:1];
    else
        [segmentedControl setSelectedSegmentIndex:0];
    [segmentedControl setSegmentColor];
    [segmentedControl addTarget:self action:@selector(segmentChanged) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setBackgroundColor:[UIColor blackColor]];
    
    
    
    /* if(IOS7)
     myGalleryTV.frame = CGRectMake(0, ypos, SCREEN_WIDTH, SCREEN_HEIGHT- ypos-130);
     else
     myGalleryTV.frame = CGRectMake(0, ypos, SCREEN_WIDTH, SCREEN_HEIGHT- ypos-bottomBarButtonHeight-150);*/
    
    
    [self.view addSubview:segmentedControl];
}


// Method to get the server data for the coupon based on retailers location and business catagory
-(void)callLocationbasedCoupons
{
    isProductTabDisplaying = NO;
    [myGalleryTV setHidden:NO];
    
    iWebrequest = DGETALLCOUPBYLOC;
    
    NSMutableString *reqStr = [[NSMutableString alloc] init];
    [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    if([searchBar.text length] > 0)
    {
        [reqStr appendFormat:@"<searchKey>%@</searchKey>",searchBar.text];
        searchBar.text=@"";
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    else if ([[defaults  valueForKey:@"couponzipcode"]length]){
        
        [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:@"couponzipcode"]];
    }
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [reqStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
    else if([defaults  valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<moduleId>%@</moduleId>", [defaults  valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<retailId>%d</retailId>", selectedbusinesscatRetailerID];
    
    [reqStr appendFormat:@"<busCatIds>%d</busCatIds>", selectedbusinesscatID];
    
    
    [reqStr appendFormat:@"<lowerLimit>%d</lowerLimit>", lowerLimitvalue];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults valueForKey:KEY_HUBCITIID]];
    [reqStr appendFormat:@"<platform>%@</platform>", @"IOS"];
    
    if([defaults  valueForKey:@"popCentId"])
        [reqStr appendFormat:@"<popCentId>%@</popCentId>", [defaults  valueForKey:@"popCentId"]];
    
    [reqStr appendString:@"</ProductDetailsRequest>"];
    
    NSMutableString *urlString1 = [BASE_URL mutableCopy] ;
    [urlString1 appendString:@"gallery/getallcoupbyloc"];
    
    
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString1 withParam:reqStr];
        
        
        [self parseMyCoupLocation:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:reqStr base:urlString1 withDelegate:self];
    }
    
    
    //[ConnectionManager establishConnectionFor:reqStr base:urlString1 withDelegate:self];
    
    
    // [reqStr release];
    
    selectedbusinesscatID = 0;
    selectedbusinesscatRetailerID = 0;
    searchTextField.text=@"";
    
    if (lowerLimitvalue == 0)
        [myGalleryTV setHidden:YES];
    
}


// Parse and get details for Retailers Location data
-(void)parseMyCoupLocation:(NSString*)responseString
{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        // Check if Next flag is '1' means need to show "View More result"
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPageFlag" parentElement:tbxml.rootXMLElement];
        
        nextpageFlag = (int)[[TBXML textForElement:nextPageElement]integerValue];
        
       // TBXMLElement *maxCountElement=[TBXML childElementNamed:@"maxCnt" parentElement:tbxml.rootXMLElement];
        
        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbxml.rootXMLElement]]intValue];
        
        // Parse and Store mainMenuID
        TBXMLElement *mainMenuIDElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if(mainMenuIDElement)
        {
            [defaults setObject:[TBXML textForElement:mainMenuIDElement] forKey:KEY_MAINMENUID];
        }
        
        TBXMLElement *retDetailsListElement = [TBXML childElementNamed:@"retDetailsList" parentElement:tbxml.rootXMLElement];
        
        // This is the main tag under which all catagory and coupons will come
        if(retDetailsListElement)
        {
            //parse catagory Info element
            TBXMLElement *RetailerDetailsElement1 = [TBXML childElementNamed:@"RetailersDetails" parentElement:retDetailsListElement];
            // Loop will run for all retailers, under this all retailers and category will comes
            while(RetailerDetailsElement1)
            {
                int iRepetedcategoryforSameretailers = 0;
                
                TBXMLElement *retIdElement = [TBXML childElementNamed:@"retId" parentElement:RetailerDetailsElement1];
                TBXMLElement *retNameElement = [TBXML childElementNamed:@"retName" parentElement:RetailerDetailsElement1];
                
                
                // Get the categories under each retailers
                TBXMLElement *retDetailsListElement = [TBXML childElementNamed:@"retDetailsList" parentElement:RetailerDetailsElement1];
                
                // Header tag for category under retailer
                if(retDetailsListElement)
                {
                    TBXMLElement *RetailersDetailsElement = [TBXML childElementNamed:@"RetailersDetails" parentElement:retDetailsListElement];
                    NSMutableArray *arrLocationCategories = [[NSMutableArray alloc]init];
                    
                    // Get all category under each retailers
                    // Loop will run for all catagories, under this all the coupons for each catagory will comes
                    while(RetailersDetailsElement)
                    {
                        
                        //parse categoryID element from catagory info
                        TBXMLElement *ccategoryIDElement = [TBXML childElementNamed:@"retLocId" parentElement:RetailersDetailsElement];
                        //parse categoryName element from catagory info
                        TBXMLElement *categoryNameElement = [TBXML childElementNamed:@"retailerAddress" parentElement:RetailersDetailsElement];
                        
                        TBXMLElement *cityElement = [TBXML childElementNamed:@"city" parentElement:RetailersDetailsElement];
                        
                        TBXMLElement *postalCodeElement = [TBXML childElementNamed:@"postalCode" parentElement:RetailersDetailsElement];
                        
                        NSMutableString *strAddress = [[NSMutableString alloc]init];
                        if(categoryNameElement)
                            [strAddress appendString:[TBXML textForElement:categoryNameElement]];
                        if(cityElement)
                            [strAddress appendFormat:@",%@",[TBXML textForElement:cityElement]];
                        if(postalCodeElement)
                            [strAddress appendFormat:@",%@",[TBXML textForElement:postalCodeElement]];
                        
                        // Saved the "retLocId" as categoryID named and "retailerAddress" as categoryName
                        NSMutableDictionary *dicCat = [[NSMutableDictionary alloc]init];
                        if(ccategoryIDElement)
                            [dicCat setValue:[TBXML textForElement:ccategoryIDElement] forKey:@"categoryId"];
                        if(categoryNameElement)
                            [dicCat setValue:strAddress forKey:@"categoryName"];
                        if(retIdElement)
                            [dicCat setValue:[TBXML textForElement:retIdElement] forKey:@"retId"];
                        if(retNameElement)
                            [dicCat setValue:[TBXML textForElement:retNameElement] forKey:@"retName"];
                        
                        //  [strAddress release];
                        
                        
                        // if last category and retailer value is same then will not add same category again
                        if([arrLocationCategoriesforRetailers count] > 0)
                        {
                            if(![[[arrLocationCategoriesforRetailers lastObject]valueForKey:@"retId"]isEqualToString:[TBXML textForElement:retIdElement]])
                            {
                                [arrSectionArrayForRetailers addObject:[NSString stringWithFormat:@"%u",(int)[arrLocationCategoriesforRetailers count]+1]];
                            }
                            BOOL isFound = NO;
                            for(int i=0; i<[arrLocationCategoriesforRetailers count]; i++)
                            {
                                NSDictionary *dic = [arrLocationCategoriesforRetailers objectAtIndex:i];
                                if(([[dic valueForKey:@"retId"]isEqualToString:[TBXML textForElement:retIdElement]] && [[dic valueForKey:@"categoryId"]isEqualToString:[TBXML textForElement:ccategoryIDElement]]))
                                {
                                    iRepetedcategoryforSameretailers+=1;
                                    isFound = YES;
                                    break;
                                }
                            }
                            
                            if(!isFound)
                                [arrLocationCategoriesforRetailers addObject:dicCat];
                        }
                        else
                            [arrLocationCategoriesforRetailers addObject:dicCat];
                        
                        [arrLocationCategories addObject:dicCat];
                        
                        // release local objects
                        // [dicCat release];
                        
                        
                        // Array to contain the all Coupons object for the particular category
                        NSMutableArray *arrCoupons = [[NSMutableArray alloc]init];
                        
                        //parse couponDetailsList element from catagory info
                        TBXMLElement *couponDetailsListElement = [TBXML childElementNamed:@"couponDetailsList" parentElement:RetailersDetailsElement];
                        
                        
                        // Under this all the coupon for particuler catagory will come
                        if(couponDetailsListElement)
                        {
                            //parse couponDetailsList element from catagory info
                            TBXMLElement *CouponDetailsElement = [TBXML childElementNamed:@"CouponDetails" parentElement:couponDetailsListElement];
                            
                            // Under this coupons' data will come
                            while(CouponDetailsElement)
                            {
                                NSMutableDictionary *dicCoupons = [[NSMutableDictionary alloc]init];
                                
                                //Parse and save Coupons data under catagory
                                TBXMLElement *couponIdElement = [TBXML childElementNamed:@"couponId" parentElement:CouponDetailsElement];
                                TBXMLElement *couponNameElement = [TBXML childElementNamed:@"couponName" parentElement:CouponDetailsElement];
                                TBXMLElement *couponStartDateElement = [TBXML childElementNamed:@"couponStartDate" parentElement:CouponDetailsElement];
                                TBXMLElement *couponExpireDateElement = [TBXML childElementNamed:@"couponExpireDate" parentElement:CouponDetailsElement];
                                TBXMLElement *couponImagePathElement = [TBXML childElementNamed:@"couponImagePath" parentElement:CouponDetailsElement];
                                TBXMLElement *coupDescElement = [TBXML childElementNamed:@"coupDesc" parentElement:CouponDetailsElement];
                                TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:CouponDetailsElement];
                                TBXMLElement *claimFlagElement = [TBXML childElementNamed:@"claimFlag" parentElement:CouponDetailsElement];
                                TBXMLElement *redeemFlagElement = [TBXML childElementNamed:@"redeemFlag" parentElement:CouponDetailsElement];
                                TBXMLElement *newFlagElement = [TBXML childElementNamed:@"newFlag" parentElement:CouponDetailsElement];
                                
                                //0 - Not yet added (Red), 1- cliped(Green)
                                TBXMLElement *usedElement = [TBXML childElementNamed:@"used" parentElement:CouponDetailsElement];
                                TBXMLElement *coupToIssueElement = [TBXML childElementNamed:@"coupToIssue" parentElement:CouponDetailsElement];
                                TBXMLElement *couponDiscountAmountElement = [TBXML childElementNamed:@"couponDiscountAmount" parentElement:CouponDetailsElement];
                                TBXMLElement *coupListIDElement = [TBXML childElementNamed:@"couponListId" parentElement:CouponDetailsElement];
                                
                                TBXMLElement *viewableonWebElement = [TBXML childElementNamed:@"viewableOnWeb" parentElement:CouponDetailsElement];
                                
                                if(viewableonWebElement)
                                    [dicCoupons setValue:[TBXML textForElement:viewableonWebElement] forKey:@"viewableOnWeb"];
                                
                                if(coupListIDElement)
                                    [dicCoupons setValue:[TBXML textForElement:coupListIDElement] forKey:@"couponListId"];
                                if(couponDiscountAmountElement)
                                    [dicCoupons setValue:[TBXML textForElement:couponDiscountAmountElement] forKey:@"couponDiscountAmount"];
                                if(couponIdElement)
                                    [dicCoupons setValue:[TBXML textForElement:couponIdElement] forKey:@"couponId"];
                                if(couponNameElement)
                                    [dicCoupons setValue:[TBXML textForElement:couponNameElement] forKey:@"couponName"];
                                if(couponStartDateElement)
                                    [dicCoupons setValue:[TBXML textForElement:couponStartDateElement] forKey:@"couponStartDate"];
                                if(couponExpireDateElement)
                                    [dicCoupons setValue:[TBXML textForElement:couponExpireDateElement] forKey:@"couponExpireDate"];
                                if(couponImagePathElement)
                                    [dicCoupons setValue:[TBXML textForElement:couponImagePathElement] forKey:@"couponImagePath"];
                                if(coupDescElement)
                                    [dicCoupons setValue:[TBXML textForElement:coupDescElement] forKey:@"coupDesc"];
                                if(distanceElement)
                                    [dicCoupons setValue:[TBXML textForElement:distanceElement] forKey:@"distance"];
                                if(claimFlagElement)
                                    [dicCoupons setValue:[TBXML textForElement:claimFlagElement] forKey:@"claimFlag"];
                                if(redeemFlagElement)
                                    [dicCoupons setValue:[TBXML textForElement:redeemFlagElement] forKey:@"redeemFlag"];
                                if(newFlagElement)
                                    [dicCoupons setValue:[TBXML textForElement:newFlagElement] forKey:@"newFlag"];
                                if(usedElement)
                                {
                                    // if Only Claim then 1, if redeemed then 2 else 0
                                    NSString *val = @"0";
                                    if([[TBXML textForElement:claimFlagElement]isEqualToString:@"1"])
                                        val = @"1";
                                    if([[TBXML textForElement:redeemFlagElement]isEqualToString:@"1"])
                                        val = @"2";
                                    
                                    [dicCoupons setValue:val forKey:@"used"];
                                    
                                }
                                if(coupToIssueElement)
                                    [dicCoupons setValue:[TBXML textForElement:coupToIssueElement] forKey:@"coupToIssue"];
                                
                                [arrCoupons addObject:dicCoupons];
                                //[dicCoupons release];
                                
                                
                                // Get the next sibling of the "CouponDetails"
                                CouponDetailsElement = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:CouponDetailsElement];
                                
                            }
                            NSMutableString *str = [[NSMutableString alloc]initWithString:[TBXML textForElement:retIdElement]];
                            [str appendFormat:@"-%@",[TBXML textForElement:ccategoryIDElement]];
                            
                            if([dicLocationCupon count] > 0)
                            {
                                if([[dicLocationCupon allKeys]containsObject:str])
                                {
                                    NSMutableArray *arr = [[NSMutableArray alloc]initWithArray:[dicLocationCupon valueForKey:str]];
                                    [arr addObjectsFromArray:arrCoupons];
                                    [dicLocationCupon setValue:arr forKey:str];
                                    // [arr release];
                                }
                                else
                                    [dicLocationCupon setValue:arrCoupons forKey:str];
                            }
                            else
                                [dicLocationCupon setValue:arrCoupons forKey:str];
                            
                            
                            
                            //[arrCoupons release];
                            // [str release];
                        }
                        
                        
                        
                        // Get the next sibling of the "CategoryInfo"
                        RetailersDetailsElement = [TBXML nextSiblingNamed:@"RetailersDetails" searchFromElement:RetailersDetailsElement];
                        
                    }
                    
                    
                    //[arrLocationCategories release];
                }
                
                RetailerDetailsElement1 = [TBXML nextSiblingNamed:@"RetailersDetails" searchFromElement:RetailerDetailsElement1];
            }
            
        }
        
        //	  NSString *maxCount = [[NSString alloc]initWithString:[TBXML textForElement:maxCountElement]];
        //        if ([maxCount isEqualToString:@"1"]) {
        ////            if (fromCurrentSpecials==FALSE) {
        ////                dealCouponsCountFlag=TRUE;
        ////            }
        //            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        //            [self tableView:myGalleryTV didSelectRowAtIndexPath:indexPath];
        //	}
        if (bottomBtn==1) {
            TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"arBottomBtnList" parentElement:tbxml.rootXMLElement];
            TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
            arrEventBottomButtonDO =[[NSMutableArray alloc]init];
            while(BottomButtonElement)
            {
                
                bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                
                
                TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                
                
                if(bottomBtnIDElement)
                    obj_findBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                
                if(bottomBtnNameElement)
                    obj_findBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                else
                    obj_findBottomDO.bottomBtnName = @" ";
                
                if(bottomBtnImgElement)
                    obj_findBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                else
                    obj_findBottomDO.bottomBtnImg = @" ";
                
                if(bottomBtnImgOffElement)
                    obj_findBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                else
                    obj_findBottomDO.bottomBtnImgOff = @" ";
                
                if(btnLinkTypeIDElement)
                    obj_findBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                
                if(btnLinkTypeNameElement)
                    obj_findBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                
                if(btnLinkIDElement){
                    obj_findBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                    
                    if(btnLinkTypeNameElement){
                        if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
                            [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
                    }
                }
                
                if(positionElement)
                    obj_findBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                
                
                
                BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                [arrEventBottomButtonDO addObject:obj_findBottomDO];
                //[obj_findBottomDO release];
            }
            if([arrEventBottomButtonDO count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                [self setBottomBarMenu];
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
        }
    }
    else
    {
        
        if (myGalleryTV.tag == 1){
            segmentedControl.selectedSegmentIndex=1;
            [self getMuCouponItemsforProducts];
            
            [loading stopAnimating];
            ReleaseAndNilify(loading);
            
            return;
        }
        
        /*TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
         
         NSString *responseTextStr = [TBXML textForElement:saveResponseText];
         [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];*/
        
    }
    
    if([arrLocationCategoriesforRetailers count] > 0)
    {
        [myGalleryTV setHidden:NO];
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        //available.text = responseTextStr ;
        NSArray *twoStringsResponse = [responseTextStr componentsSeparatedByString:@"."];
        NSString *twoLineResponse = [NSString stringWithFormat:@"%@.\n%@",[twoStringsResponse objectAtIndex:0],[twoStringsResponse objectAtIndex:1]];
        available.text = twoLineResponse ;
        
        available.font = [UIFont boldSystemFontOfSize:15.0];
        if(IPAD)
            available.font = [UIFont boldSystemFontOfSize:25.0];
        available.textAlignment = NSTextAlignmentLeft;
        // visitAllCoupons.font=[UIFont systemFontOfSize:14];
        // visitAllCoupons.text = @"There are no coupons by Location";
    }
    float ypos = segmentedControl.frame.origin.y +  segmentedControl.frame.size.height;
    // if (!myGalleryTV) {                                     //change made on 29 june.
    if (bottomBtn==1) {
        myGalleryTV.frame = CGRectMake(0, ypos, SCREEN_WIDTH, SCREEN_HEIGHT- ypos-120);
    }
    else{
        myGalleryTV.frame = CGRectMake(0, ypos, SCREEN_WIDTH,  SCREEN_HEIGHT-ypos-120+bottomBarButtonHeight);
    }
    // }
    [myGalleryTV reloadData];
    [defaults setBool:NO forKey:@"ViewMore"];
}

// Method to get the server data for the coupon based on products catagory
-(void)getMuCouponItemsforProducts
{
    isProductTabDisplaying = YES;
    
    
    iWebrequest = DGETALLCOUPBYPROD;
    
    NSMutableString *reqStr = [[NSMutableString alloc] init];
    [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if([searchBar.text length] > 0)
    {
        [reqStr appendFormat:@"<searchKey>%@</searchKey>",searchBar.text];
        searchBar.text=@"";
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    else if ([[defaults  valueForKey:@"couponzipcode"]length]){
        
        [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:@"couponzipcode"]];
    }
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [reqStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
    else if([defaults  valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<moduleId>%@</moduleId>", [defaults  valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<retailId>%d</retailId>", selectedbusinesscatRetailerID];
    
    [reqStr appendFormat:@"<busCatIds>%d</busCatIds>", selectedbusinesscatID];
    
    
    [reqStr appendFormat:@"<lowerLimit>%d</lowerLimit>", lowerLimitvalue];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults valueForKey:KEY_HUBCITIID]];
    [reqStr appendFormat:@"<platform>%@</platform>", @"IOS"];
    
    if([defaults  valueForKey:@"popCentId"])
        [reqStr appendFormat:@"<popCentId>%@</popCentId>", [defaults  valueForKey:@"popCentId"]];
    
    
    [reqStr appendString:@"</ProductDetailsRequest>"];
    
    NSMutableString *urlString1 = [BASE_URL mutableCopy] ;
    [urlString1 appendString:@"gallery/getallcoupbyprod"];
    
    
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString1 withParam:reqStr];
        
        
        [self parseMyCoupProduct:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:reqStr base:urlString1 withDelegate:self];
    }
    
    
    //[ConnectionManager establishConnectionFor:reqStr base:urlString1 withDelegate:self];
    
    // [reqStr release];
    
    selectedbusinesscatID = 0;
    selectedbusinesscatRetailerID = 0;
    searchTextField.text=@"";
    
    if (lowerLimitvalue == 0)
        [myGalleryTV setHidden:YES];
    
}

-(void)responseData:(NSString *) response
{
    if(isCalledFromSegementChange)
    {
        isCalledFromSegementChange = NO;
        [self setWhenCalledfromSegementChange];
    }
    switch (iWebrequest) {
        case DGETALLCOUPBYPROD:
            [self parseMyCoupProduct:response];
            break;
        case DGETALLCOUPBYLOC:
            [self parseMyCoupLocation:response];
            break;
        case DGETUSERCATCOUPON:
            [self parseUserCategory:response];
            break;
        case GETFAVLOCATIONSS:
            [self parse_GetUserCat:response];
            break;
        case GETUSERINFORMATION:
        {
            [self parseGetUserData:response];
        }
            break;
            
        case HubcitiAnythingInfos:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetailss:
            [self parse_appsitedetails:response];
            break;
            
            
            //        case addRemoveCoupon:
            //            [self parse_addRemoveCoupon:response];
            //            break;
            
        default:
            break;
    }
    
    [myGalleryTV reloadData];
}

-(void)parseUserCategory:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        
        [defaults  setObject:responseString forKey:KEY_RESPONSEXML];
        PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
        [mViewController.navigationController pushViewController:settings animated:NO];
        //[settings release];
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
}

// Parse and get the details for category data under products
-(void)parseMyCoupProduct:(NSString*)responseString
{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        // Check if Next flag is '1' means need to show "View More result"
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPageFlag" parentElement:tbxml.rootXMLElement];
        
        nextpageFlag = (int)[[TBXML textForElement:nextPageElement]integerValue];
        
        // Parse and Store mainMenuID
        TBXMLElement *mainMenuIDElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
       // TBXMLElement *maxCountElement = [TBXML childElementNamed:@"maxCnt" parentElement:tbxml.rootXMLElement];
        if(mainMenuIDElement)
        {
            [defaults setObject:[TBXML textForElement:mainMenuIDElement] forKey:KEY_MAINMENUID];
        }
        
        TBXMLElement *categoryInfoListElement = [TBXML childElementNamed:@"categoryInfoList" parentElement:tbxml.rootXMLElement];
        
        // This is the main tag under which all catagory and coupons will come
        if(categoryInfoListElement)
        {
            //parse catagory Info element
            TBXMLElement *CategoryInfoElement = [TBXML childElementNamed:@"CategoryInfo" parentElement:categoryInfoListElement];
            
            
            // Loop will run for all catagories, under this all the coupons for each catagory will comes
            while(CategoryInfoElement)
            {
                // Array to contain the all Coupons object for the particular category
                NSMutableArray *arrCoupons = [[NSMutableArray alloc]init];
                
                //parse couponDetailsList element from catagory info
                TBXMLElement *couponDetailsListElement = [TBXML childElementNamed:@"couponDetailsList" parentElement:CategoryInfoElement];
                
                
                // Under this all the coupon for particuler catagory will come
                if(couponDetailsListElement)
                {
                    //parse couponDetailsList element from catagory info
                    TBXMLElement *CouponDetailsElement = [TBXML childElementNamed:@"CouponDetails" parentElement:couponDetailsListElement];
                    
                    // Under this coupons' data will come
                    while(CouponDetailsElement)
                    {
                        NSMutableDictionary *dicCoupons = [[NSMutableDictionary alloc]init];
                        
                        //Parse and save Coupons data under catagory
                        TBXMLElement *couponIdElement = [TBXML childElementNamed:@"couponId" parentElement:CouponDetailsElement];
                        TBXMLElement *couponNameElement = [TBXML childElementNamed:@"couponName" parentElement:CouponDetailsElement];
                        TBXMLElement *couponStartDateElement = [TBXML childElementNamed:@"couponStartDate" parentElement:CouponDetailsElement];
                        TBXMLElement *couponExpireDateElement = [TBXML childElementNamed:@"couponExpireDate" parentElement:CouponDetailsElement];
                        TBXMLElement *couponImagePathElement = [TBXML childElementNamed:@"couponImagePath" parentElement:CouponDetailsElement];
                        TBXMLElement *coupDescElement = [TBXML childElementNamed:@"coupDesc" parentElement:CouponDetailsElement];
                        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:CouponDetailsElement];
                        TBXMLElement *claimFlagElement = [TBXML childElementNamed:@"claimFlag" parentElement:CouponDetailsElement];
                        TBXMLElement *redeemFlagElement = [TBXML childElementNamed:@"redeemFlag" parentElement:CouponDetailsElement];
                        TBXMLElement *newFlagElement = [TBXML childElementNamed:@"newFlag" parentElement:CouponDetailsElement];
                        
                        TBXMLElement *viewableonWebElement = [TBXML childElementNamed:@"viewableOnWeb" parentElement:CouponDetailsElement];
                        
                        
                        //0 - Not yet added (Red), 1- cliped(Green)
                        TBXMLElement *usedElement = [TBXML childElementNamed:@"used" parentElement:CouponDetailsElement];
                        TBXMLElement *coupToIssueElement = [TBXML childElementNamed:@"coupToIssue" parentElement:CouponDetailsElement];
                        TBXMLElement *couponDiscountAmountElement = [TBXML childElementNamed:@"couponDiscountAmount" parentElement:CouponDetailsElement];
                        TBXMLElement *coupListIDElement = [TBXML childElementNamed:@"couponListId" parentElement:CouponDetailsElement];
                        
                        if(coupListIDElement)
                            [dicCoupons setValue:[TBXML textForElement:coupListIDElement] forKey:@"couponListId"];
                        if(viewableonWebElement)
                            [dicCoupons setValue:[TBXML textForElement:viewableonWebElement] forKey:@"viewableOnWeb"];
                        if(couponDiscountAmountElement)
                            [dicCoupons setValue:[TBXML textForElement:couponDiscountAmountElement] forKey:@"couponDiscountAmount"];
                        if(couponIdElement)
                            [dicCoupons setValue:[TBXML textForElement:couponIdElement] forKey:@"couponId"];
                        if(couponNameElement)
                            [dicCoupons setValue:[TBXML textForElement:couponNameElement] forKey:@"couponName"];
                        if(couponStartDateElement)
                            [dicCoupons setValue:[TBXML textForElement:couponStartDateElement] forKey:@"couponStartDate"];
                        if(couponExpireDateElement)
                            [dicCoupons setValue:[TBXML textForElement:couponExpireDateElement] forKey:@"couponExpireDate"];
                        if(couponImagePathElement)
                            [dicCoupons setValue:[TBXML textForElement:couponImagePathElement] forKey:@"couponImagePath"];
                        if(coupDescElement)
                            [dicCoupons setValue:[TBXML textForElement:coupDescElement] forKey:@"coupDesc"];
                        if(distanceElement)
                            [dicCoupons setValue:[TBXML textForElement:distanceElement] forKey:@"distance"];
                        if(claimFlagElement)
                            [dicCoupons setValue:[TBXML textForElement:claimFlagElement] forKey:@"claimFlag"];
                        if(redeemFlagElement)
                            [dicCoupons setValue:[TBXML textForElement:redeemFlagElement] forKey:@"redeemFlag"];
                        if(newFlagElement)
                            [dicCoupons setValue:[TBXML textForElement:newFlagElement] forKey:@"newFlag"];
                        if(usedElement)
                        {
                            // if Only Claim then 1, if redeemed then 2 else 0
                            NSString *val = @"0";
                            if([[TBXML textForElement:claimFlagElement]isEqualToString:@"1"])
                                val = @"1";
                            if([[TBXML textForElement:redeemFlagElement]isEqualToString:@"1"])
                                val = @"2";
                            
                            [dicCoupons setValue:val forKey:@"used"];
                            
                        }
                        if(coupToIssueElement)
                            [dicCoupons setValue:[TBXML textForElement:coupToIssueElement] forKey:@"coupToIssue"];
                        
                        [arrCoupons addObject:dicCoupons];
                        //[dicCoupons release];
                        
                        
                        // Get the next sibling of the "CouponDetails"
                        CouponDetailsElement = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:CouponDetailsElement];
                        
                    }
                    
                    
                }
                
                //parse categoryID element from catagory info
                TBXMLElement *ccategoryIDElement = [TBXML childElementNamed:@"categoryId" parentElement:CategoryInfoElement];
                //parse categoryName element from catagory info
                TBXMLElement *categoryNameElement = [TBXML childElementNamed:@"categoryName" parentElement:CategoryInfoElement];
                
                // Saved the categoryID & categoryName
                NSMutableDictionary *dicCat = [[NSMutableDictionary alloc]init];
                if(ccategoryIDElement)
                    [dicCat setValue:[TBXML textForElement:ccategoryIDElement] forKey:@"categoryId"];
                if(categoryNameElement)
                    [dicCat setValue:[TBXML textForElement:categoryNameElement] forKey:@"categoryName"];
                
                
                //If category is more the 1 then check the below conditions
                if([arrCatagories count] > 0)
                {
                    // Check if Last category value and current category value is same then append in the same array else add as new
                    
                    BOOL isAdded = NO;
                    for(int i_check = 0; i_check < [arrCatagories count]; i_check++)
                    {
                        if([[[arrCatagories objectAtIndex:i_check]valueForKey:@"categoryId"] isEqualToString:[TBXML textForElement:ccategoryIDElement]])
                        {
                            NSMutableArray *arr = [dicProductsCoupon valueForKey:[TBXML textForElement:ccategoryIDElement]];
                            [arr addObjectsFromArray:arrCoupons];
                            [dicProductsCoupon setObject:arr forKey:[TBXML textForElement:ccategoryIDElement]];
                            isAdded = YES;
                            break;
                        }
                    }
                    if(isAdded == NO)
                    {
                        // Storing Category objects in array
                        [arrCatagories addObject:dicCat];
                        // Storing all coupons object belongs to category
                        [dicProductsCoupon setObject:arrCoupons forKey:[TBXML textForElement:ccategoryIDElement]];
                        
                    }
                }
                else
                {
                    // Storing Category objects in array
                    [arrCatagories addObject:dicCat];
                    // Storing all coupons object belongs to category
                    [dicProductsCoupon setObject:arrCoupons forKey:[TBXML textForElement:ccategoryIDElement]];
                }
                
                // release local objects
                //[arrCoupons release];
                
                // Get the next sibling of the "CategoryInfo"
                CategoryInfoElement = [TBXML nextSiblingNamed:@"CategoryInfo" searchFromElement:CategoryInfoElement];
                
            }
            
        }
        
        
        //        NSString *maxCount = [[NSString alloc]initWithString:[TBXML textForElement:maxCountElement]];
        //        if ([maxCount isEqualToString:@"1"]) {
        //            dealCouponsCountFlag=TRUE;
        //            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        //            [self tableView:myGalleryTV didSelectRowAtIndexPath:indexPath];
        //}
        
        if (bottomBtn==1) {
            TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"arBottomBtnList" parentElement:tbxml.rootXMLElement];
            TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
            arrEventBottomButtonDO =[[NSMutableArray alloc]init];
            while(BottomButtonElement)
            {
                
                bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                
                
                TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                
                
                if(bottomBtnIDElement)
                    obj_findBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                
                if(bottomBtnNameElement)
                    obj_findBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                else
                    obj_findBottomDO.bottomBtnName = @" ";
                
                if(bottomBtnImgElement)
                    obj_findBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                else
                    obj_findBottomDO.bottomBtnImg = @" ";
                
                if(bottomBtnImgOffElement)
                    obj_findBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                else
                    obj_findBottomDO.bottomBtnImgOff = @" ";
                
                if(btnLinkTypeIDElement)
                    obj_findBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                
                if(btnLinkTypeNameElement)
                    obj_findBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                
                if(btnLinkIDElement){
                    obj_findBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                    
                    if(btnLinkTypeNameElement){
                        if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
                            [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
                    }
                }
                
                if(positionElement)
                    obj_findBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                
                
                
                BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                [arrEventBottomButtonDO addObject:obj_findBottomDO];
                //[obj_findBottomDO release];
            }
            if([arrEventBottomButtonDO count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                [self setBottomBarMenu];
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
        }
        
    }
    
    //Not required to show alert as message is displayed as label
    /* else
     {
     TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
     
     NSString *responseTextStr = [TBXML textForElement:saveResponseText];
     [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
     
     }*/
    
    
    if([arrCatagories count] > 0)
    {
        [myGalleryTV setHidden:NO];
    }
    else
    {
        //available.text = @"None Available";
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        NSArray *twoStringsResponse = [responseTextStr componentsSeparatedByString:@"."];
        NSString *twoLineResponse = [NSString stringWithFormat:@"%@.\n%@",[twoStringsResponse objectAtIndex:0],[twoStringsResponse objectAtIndex:1]];
        available.text = twoLineResponse ;
        available.font = [UIFont boldSystemFontOfSize:15.0];
        if(IPAD)
            available.font = [UIFont boldSystemFontOfSize:25.0];
        available.textAlignment = NSTextAlignmentLeft;
        
        //visitAllCoupons.font=[UIFont systemFontOfSize:14];
        //visitAllCoupons.text = @"There are no coupons by product";
    }
    //    [self addsegementcontrolleronView];
    float ypos = segmentedControl.frame.origin.y +  segmentedControl.frame.size.height;
    if (bottomBtn==1) {
        myGalleryTV.frame = CGRectMake(0, ypos, SCREEN_WIDTH, SCREEN_HEIGHT- ypos-120);
    }
    else{
        myGalleryTV.frame = CGRectMake(0, ypos, SCREEN_WIDTH,  SCREEN_HEIGHT-ypos-120+bottomBarButtonHeight);
    }
    [myGalleryTV reloadData];
    [defaults setBool:NO forKey:@"ViewMore"];
    
}

// This method will call when tap on the gallery button at the bottom of the screen
- (void)galleryTabClicked
{
    if(userIdentifier==TRUE){
        [NewsUtility signupPopUp:self];
    }
}

// This method will call when tap on the byCity button at the bottom of the screen
- (void)byCityTabClicked
{
    
}


// This method will call when tap on the Search button at the bottom of the screen
- (void)searchTabClicked
{
    // Update the Tab Image
    //    [couponGalleryBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [bycityBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [searchBtn setImage:[UIImage imageNamed:@"tab_btn_down_srchTxt.png"] forState:UIControlStateNormal];
    //    [preferanceBtn setImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    
    
    searchView.hidden = NO;
    selectedbusinesscatID = 0;
    selectedbusinesscatRetailerID = 0;
    
    // If product tab is active make product button enable else location button in search view
    if(isProductTabDisplaying)
    {
        isLocationRadioButtonEnable = NO;
        [radioBtn2 setImage:[UIImage imageNamed:@"Login_BlueSolid.png"] forState:UIControlStateNormal];
        [radioBtn1 setImage:[UIImage imageNamed:@"Login_BlueOutline.png"] forState:UIControlStateNormal];
        
        retBtn.hidden = YES;
        retLabel.text = @"";
        
        searchTextField.frame = CGRectMake(catBtn.frame.origin.x, retBtn.frame.origin.y, 231, 31);
        searchTextField.text=@"";
        catLabel.text = @"Category";
        
    }
    else
    {
        isLocationRadioButtonEnable = YES;
        [radioBtn2 setImage:[UIImage imageNamed:@"Login_BlueOutline.png"] forState:UIControlStateNormal];
        [radioBtn1 setImage:[UIImage imageNamed:@"Login_BlueSolid.png"] forState:UIControlStateNormal];
        
        retBtn.hidden=NO;
        searchTextField.frame = CGRectMake(catBtn.frame.origin.x, 165, 231, 31);
        searchTextField.text=@"";
        retLabel.text = @"Retailer";
        catLabel.text = @"Category";
        
        
    }
    
    [self.view bringSubviewToFront:searchView];
    
}


// This method will call when tap on the preferance button at the bottom of the screen
- (void)preferanceTabClicked
{
    iWebrequest = DGETUSERCATCOUPON;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
    NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
    
    [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
}



-(void)showSplash
{
    UIViewController *modalViewController = [[UIViewController alloc] init];
    modelView.backgroundColor = [UIColor colorWithRGBInt:0x6fb540] ;
    modalViewController.view = modelView;
    [self presentViewController:modalViewController animated:NO completion:nil];
    [self performSelector:@selector(hideSplash) withObject:nil afterDelay:1.0];
    ReleaseAndNilify(modalViewController);
}

- (void)hideSplash
{
    [[self presentedViewController] dismissViewControllerAnimated:NO completion:nil];
    [defaults setBool:NO forKey:@"myGallerysplash"];
    [defaults setBool:NO forKey:@"CalledFromMainMenu"];
    [self getMuCouponItemsforLocation];
}


- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: NO];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    //    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
    //        [self  addBottomButtonsiPad];
    //    }
    //
    //    [self updateViewonViewWillAppear];
    
    //    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
    //        [self addBottomButtonsiPad];
    //    }
    
}

-(void)addBottomButtonsiPad
{
    myGalleryTV.frame=CGRectMake(0, 63, SCREEN_WIDTH, SCREEN_HEIGHT-53);
    
    [couponGalleryBtn removeFromSuperview];
    [searchBtn removeFromSuperview];
    [preferanceBtn removeFromSuperview];
    
    
    
}

-(void)updateViewonViewWillAppear
{
    searchTextField.text=@"";
    
    if([SharedManager refreshGallery])
    {
        //        [self refreshAlldatafromUI];
        [myGalleryTV setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    [SharedManager setRefreshGallery:NO];
    
    [myGalleryTV deselectRowAtIndexPath:[myGalleryTV indexPathForSelectedRow] animated:YES];
    [myGalleryTV reloadData];
    
}

-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}


-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}




-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


// No Delete Functionality required in All Coupon


-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}



-(IBAction)radioButtonTapped:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    [self toolbarDoneClicked:nil];
    catLabel.text = @"Category";
    selectedbusinesscatID = 0;
    selectedbusinesscatRetailerID = 0;
    searchTextField.text = @"";
    [searchTextField resignFirstResponder];
    
    if(btn == radioBtn1)
    {
        isLocationRadioButtonEnable = YES;
        retBtn.hidden = NO;
        retLabel.text = @"Retailer";
        searchTextField.frame = CGRectMake(catBtn.frame.origin.x, 165, 231, 31);
        [radioBtn2 setImage:[UIImage imageNamed:@"Login_BlueOutline.png"] forState:UIControlStateNormal];
        [radioBtn1 setImage:[UIImage imageNamed:@"Login_BlueSolid.png"] forState:UIControlStateNormal];
    }
    else if(btn == radioBtn2)
    {
        isLocationRadioButtonEnable = NO;
        retBtn.hidden = YES;
        retLabel.text = @"";
        searchTextField.frame = CGRectMake(catBtn.frame.origin.x, retBtn.frame.origin.y, 231, 31);
        [radioBtn2 setImage:[UIImage imageNamed:@"Login_BlueSolid.png"] forState:UIControlStateNormal];
        [radioBtn1 setImage:[UIImage imageNamed:@"Login_BlueOutline.png"] forState:UIControlStateNormal];
        
    }
    
}



-(IBAction)searchClicked:(id)sender{
    
    [self refreshAlldatafromUIforSearch];
    
}


- (void)refreshAlldatafromUIforSearch
{
    // remove all objects from Products Stored dataset
    [arrCatagories removeAllObjects];
    [dicProductsCoupon removeAllObjects];
    
    // remove all objects from Location Stored dataset
    [dicLocationCupon removeAllObjects];
    [arrLocationCategoriesforRetailers removeAllObjects];
    [arrSectionArrayForRetailers removeAllObjects];
    [arrSectionArrayForRetailers addObject:@"1"];
    lowerLimitvalue = 0;
    //If seach tab is active then hide the search view
    [self toolbarDoneClicked:nil];
    searchView.hidden = YES;
    
    // Set the bottom tab images
    //    [couponGalleryBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [bycityBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [searchBtn setImage:[UIImage imageNamed:@"tab_btn_up_srchTxt.png"] forState:UIControlStateNormal];
    //    [preferanceBtn setImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    
    if(isProductTabDisplaying)
    {
        segmentedControl.selectedSegmentIndex= 1;
        myGalleryTV.tag = 0;
        [self getMuCouponItemsforProducts];
    }
    else
    {
        segmentedControl.selectedSegmentIndex=0;
        myGalleryTV.tag = 0;
        [self callLocationbasedCoupons];
    }
}


- (void)refreshAlldatafromUI
{
    // remove all objects from Products Stored dataset
    [arrCatagories removeAllObjects];
    [dicProductsCoupon removeAllObjects];
    
    // remove all objects from Location Stored dataset
    [dicLocationCupon removeAllObjects];
    [arrLocationCategoriesforRetailers removeAllObjects];
    [arrSectionArrayForRetailers removeAllObjects];
    [arrSectionArrayForRetailers addObject:@"1"];
    lowerLimitvalue = 0;
    //If seach tab is active then hide the search view
    [self toolbarDoneClicked:nil];
    searchView.hidden = YES;
    
    // Set the bottom tab images
    //    [couponGalleryBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [bycityBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [searchBtn setImage:[UIImage imageNamed:@"tab_btn_up_srchTxt.png"] forState:UIControlStateNormal];
    //    [preferanceBtn setImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    
    if(isProductTabDisplaying)
    {
        segmentedControl.selectedSegmentIndex= 1;
        [self getMuCouponItemsforProducts];
    }
    else
    {
        segmentedControl.selectedSegmentIndex=0;
        myGalleryTV.tag = 0;
        [self callLocationbasedCoupons];
    }
}

-(IBAction)selectCategory:(id)sender
{
    selectedbusinesscatID = 0;
    selectedbusinesscatRetailerID = 0;
    isDisplayBusinessCatagory = YES;
    // NSMutableString *urlString = [[BASE_URL mutableCopy] ;
    NSMutableString *reqStr = [[NSMutableString alloc] init];
    NSMutableString *urlStr = [NSMutableString stringWithFormat:@"%@",BASE_URL];
    if(!isLocationRadioButtonEnable)
    {
        
        [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
        
        if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE]){
            
            [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
             [defaults  valueForKey:KEY_LATITUDE]];
            
        }
        else if ([[defaults  valueForKey:KEYZIPCODE]length]){
            
            [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
        }
        else if ([[defaults  valueForKey:@"couponzipcode"]length]){
            
            [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:@"couponzipcode"]];
        }
        
        if([defaults  valueForKey:@"popCentId"])
            [reqStr appendFormat:@"<popCentId>%@</popCentId>", [defaults  valueForKey:@"popCentId"]];
        
        [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ProductDetailsRequest>", [defaults valueForKey:KEY_HUBCITIID]];
        
        [urlStr appendString:@"gallery/coupprodcat"];
    }
    else
    {
        [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
        
        if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE]){
            
            [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
             [defaults  valueForKey:KEY_LATITUDE]];
            
        }
        else if ([[defaults  valueForKey:KEYZIPCODE]length]){
            
            [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
        }
        else if ([[defaults  valueForKey:@"couponzipcode"]length]){
            
            [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:@"couponzipcode"]];
        }
        if([defaults  valueForKey:@"popCentId"])
            [reqStr appendFormat:@"<popCentId>%@</popCentId>", [defaults  valueForKey:@"popCentId"]];
        
        [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ProductDetailsRequest>", [defaults valueForKey:KEY_HUBCITIID]];
        
        [urlStr appendString:@"gallery/couplocbuscat"];
        
    }
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlStr withParam:reqStr]];
    ReleaseAndNilify(reqStr);
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
    {
        ReleaseAndNilify(responseXml);
        return;
    }
    
    if(arrBusinessCatagoriesID)
    {
        [arrBusinessCatagoriesID removeAllObjects];
        //[arrBusinessCatagoriesID release];
    }
    if(arrBusinessCatagoriesName)
    {
        [arrBusinessCatagoriesName removeAllObjects];
        // [arrBusinessCatagoriesName release];
    }
    
    arrBusinessCatagoriesID = [[NSMutableArray alloc]init];
    arrBusinessCatagoriesName = [[NSMutableArray alloc]init];
    
    [self parsecatagoryforsearchresponse:responseXml];
    ReleaseAndNilify(responseXml);
    
    if([arrBusinessCatagoriesID count] > 0)
    {
        [catPicker reloadAllComponents];
        catPicker.hidden = NO;
        toolBar.hidden = NO;
    }
}

-(void)parseRetailerforsearchresponse:(NSString *)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *couponDetailElement = [TBXML childElementNamed:@"couponDetail" parentElement:tbXml.rootXMLElement];
    
    
    if (couponDetailElement == nil) {
        TBXMLElement *respTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        if (respTextElement != nil){
            [UtilityManager showAlert:nil msg:[TBXML textForElement:respTextElement]];
            return;
        }
    }
    else
    {
        TBXMLElement *CouponDetailsElement = [TBXML childElementNamed:@"CouponDetails" parentElement:couponDetailElement];
        
        while (CouponDetailsElement != nil)
        {
            TBXMLElement *busCatIdElement = [TBXML childElementNamed:@"retId" parentElement:CouponDetailsElement];
            TBXMLElement *busCatNameElement = [TBXML childElementNamed:@"retName" parentElement:CouponDetailsElement];
            if (busCatIdElement!=nil)
            {
                [arrBusinessCatagoriesRetailerID addObject:[TBXML textForElement:busCatIdElement]];
                [arrBusinessCatagoriesRetailerName addObject:[TBXML textForElement:busCatNameElement]];
            }
            
            CouponDetailsElement = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:CouponDetailsElement];
        }
    }
}

-(void)parsecatagoryforsearchresponse:(NSString *)responseString
{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *CouponsDetailElement = [TBXML childElementNamed:@"couponDetail" parentElement:tbxml.rootXMLElement];
        
        
        TBXMLElement *CouponDetailsElement = [TBXML childElementNamed:@"CouponDetails" parentElement:CouponsDetailElement];
        
        
        if(!isLocationRadioButtonEnable)
        {
            
            while (CouponDetailsElement != nil)
            {
                TBXMLElement *busCatIdElement = [TBXML childElementNamed:@"catId" parentElement:CouponDetailsElement];
                TBXMLElement *busCatNameElement = [TBXML childElementNamed:@"catName" parentElement:CouponDetailsElement];
                if (busCatIdElement!=nil)
                {
                    [arrBusinessCatagoriesID addObject:[TBXML textForElement:busCatIdElement]];
                    [arrBusinessCatagoriesName addObject:[TBXML textForElement:busCatNameElement]];
                }
                
                CouponDetailsElement = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:CouponDetailsElement];
            }
            
        }
        else
        {
            while (CouponDetailsElement != nil)
            {
                TBXMLElement *busCatIdElement = [TBXML childElementNamed:@"busCatId" parentElement:CouponDetailsElement];
                TBXMLElement *busCatNameElement = [TBXML childElementNamed:@"busCatName" parentElement:CouponDetailsElement];
                if (busCatIdElement!=nil)
                {
                    [arrBusinessCatagoriesID addObject:[TBXML textForElement:busCatIdElement]];
                    [arrBusinessCatagoriesName addObject:[TBXML textForElement:busCatNameElement]];
                }
                
                CouponDetailsElement = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:CouponDetailsElement];
            }
        }
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
    
}

-(IBAction)selectRetailer:(id)sender
{
    if(selectedbusinesscatID > 0)
    {
        selectedbusinesscatRetailerID = 0;
        isDisplayBusinessCatagory = NO;
        
        NSMutableString *reqStr = [[NSMutableString alloc] init];
        NSMutableString *urlStr = [BASE_URL mutableCopy] ;
        
        [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
        
        if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE]){
            
            [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
             [defaults  valueForKey:KEY_LATITUDE]];
            
        }
        else if ([[defaults  valueForKey:KEYZIPCODE]length]){
            
            [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
        }
        else if ([[defaults  valueForKey:@"couponzipcode"]length]){
            
            [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:@"couponzipcode"]];
        }
        
        if([defaults  valueForKey:@"popCentId"])
            [reqStr appendFormat:@"<popCentId>%@</popCentId>", [defaults  valueForKey:@"popCentId"]];
        
        [reqStr appendFormat:@"<busCatIds>%d</busCatIds>",selectedbusinesscatID];
        [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ProductDetailsRequest>",[defaults valueForKey:KEY_HUBCITIID]];
        
        [urlStr appendString:@"gallery/retforbuscat"];
        
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlStr withParam:reqStr]];
        ReleaseAndNilify(reqStr);
        
        if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]){
            ReleaseAndNilify(responseXml);
            return;
        }
        
        if(arrBusinessCatagoriesRetailerID)
        {
            [arrBusinessCatagoriesRetailerID removeAllObjects];
            //[arrBusinessCatagoriesRetailerID release];
        }
        if(arrBusinessCatagoriesRetailerName)
        {
            [arrBusinessCatagoriesRetailerName removeAllObjects];
            // [arrBusinessCatagoriesRetailerName release];
        }
        
        
        arrBusinessCatagoriesRetailerID = [[NSMutableArray alloc]init];
        arrBusinessCatagoriesRetailerName = [[NSMutableArray alloc]init];
        
        [self parseRetailerforsearchresponse:responseXml];
        ReleaseAndNilify(responseXml);
        
        if([arrBusinessCatagoriesRetailerID count] > 0)
        {
            [catPicker reloadAllComponents];
            catPicker.hidden = NO;
            toolBar.hidden = NO;
        }
    }
}


-(IBAction)toolbarDoneClicked:(id)sender{
    
    catPicker.hidden = YES;
    toolBar.hidden = YES;
}

#pragma mark Picker view methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(isDisplayBusinessCatagory)
        return [arrBusinessCatagoriesID count];
    else
        return [arrBusinessCatagoriesRetailerID count];
    
    return 0;
}

/*- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
 {
 if(isDisplayBusinessCatagory)
 {
 //catLabel.text = [arrBusinessCatagoriesName objectAtIndex:row];
 return [arrBusinessCatagoriesName objectAtIndex:row];
 }
 else
 {
 //retLabel.text = [arrBusinessCatagoriesRetailerName objectAtIndex:row];
 return [arrBusinessCatagoriesRetailerName objectAtIndex:row];
 }
 }*/

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel *label = [[UILabel alloc] init];
    
    if (IOS7){
        label.frame = CGRectMake(20, 0, 220, 40);
        //pickerView.backgroundColor = [UIColor colorWithRGBInt:0xEEEEEE];
        pickerView.layer.borderWidth = 5.0;
        pickerView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    }
    
    else
        label.frame = CGRectMake(0, 0, 200, 40);
    
    [label setBackgroundColor:[UIColor clearColor]];
    [label setNumberOfLines:2];
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    [label setTextAlignment:NSTextAlignmentLeft];
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    
    if(isDisplayBusinessCatagory)
        label.text = [arrBusinessCatagoriesName objectAtIndex:row];
    else
        label.text = [arrBusinessCatagoriesRetailerName objectAtIndex:row];
    
    
    return label;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(isDisplayBusinessCatagory)
    {
        DLog(@"%@",arrBusinessCatagoriesID);
        catLabel.text = [arrBusinessCatagoriesName objectAtIndex:row];
        selectedbusinesscatID = (int)[[arrBusinessCatagoriesID objectAtIndex:row]integerValue];
        DLog(@"%d",selectedbusinesscatID);
    }
    else
    {
        retLabel.text = [arrBusinessCatagoriesRetailerName objectAtIndex:row];
        selectedbusinesscatRetailerID = (int)[[arrBusinessCatagoriesRetailerID objectAtIndex:row]integerValue];
    }
    
}


- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50.0;
}

#pragma mark TextField methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    catPicker.hidden = YES;
    toolBar.hidden = YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [defaults setValue:textField.text forKey:KEYZIPCODE];
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)closeSearchView :(id)sender
{
    //If seach tab is active then hide the search view
    [self toolbarDoneClicked:nil];
    searchView.hidden = YES;
    searchTextField.text = 0;
    selectedbusinesscatID = 0;
    selectedbusinesscatRetailerID = 0;
    
    //    // Set the bottom tab images
    //    [couponGalleryBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [bycityBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [searchBtn setImage:[UIImage imageNamed:@"tab_btn_up_srchTxt.png"] forState:UIControlStateNormal];
    //    [preferanceBtn setImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    
}

#pragma mark searchBar delegate methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    [searchBar1 resignFirstResponder];
    [self refreshAlldatafromUIforSearch];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar1
{
    searchBar.text=@"";
    [searchBar1 resignFirstResponder];
    
    if([arrCatagories count] <=0 && isProductTabDisplaying)
        [self refreshAlldatafromUIforSearch];
    else if([arrLocationCategoriesforRetailers count] <=0 && !isProductTabDisplaying)
        [self refreshAlldatafromUIforSearch];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
     [super viewDidUnload];
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}




- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    float containerViewHeight = 26.0, labelFont = 14.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        containerViewHeight = 30.0;
        labelFont = 18.0;
    }
    UIView *containerView = [[UIView alloc] init] ; //WithFrame:CGRectMake(10, 0, 320, 65)] ;
    UILabel *headerLabel1 = [[UILabel alloc] init] ;
    CGRect imageRect1 ;
    containerView.frame = CGRectMake(10, 0, SCREEN_WIDTH, containerViewHeight) ;
    headerLabel1.frame =  CGRectMake(20, 3, SCREEN_WIDTH-20, 20) ;
    imageRect1 = CGRectMake(0.0f, 0.0f, SCREEN_WIDTH, 26.0f);
    
    if(isProductTabDisplaying)
    {
        if([arrCatagories count] > section)
            headerLabel1.text = [[arrCatagories objectAtIndex:section]valueForKey:@"categoryName"];
        else
            headerLabel1.text = @" ";
        headerLabel1.textColor = [UIColor whiteColor];
        headerLabel1.font = [UIFont boldSystemFontOfSize:labelFont];
        headerLabel1.backgroundColor = [UIColor clearColor];
        
    }
    else
    {
        for (int i =0 ; i < [arrSectionArrayForRetailers count]; i++)
        {
            if (([[arrSectionArrayForRetailers objectAtIndex:i] intValue])-1 == section)
            {
                containerView.frame = CGRectMake(10, 0, SCREEN_WIDTH, 60) ;
                imageRect1 = CGRectMake(0.0f, 30.0f, SCREEN_WIDTH, 30.0f);
                
                UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, SCREEN_WIDTH-20, 20)] ;
                
                NSString *strretName = [[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"retName"];
                NSString *str = [[NSString alloc]initWithString:strretName];
                //[str stringByAppendingString:[NSString stringWithFormat:@"- %@",[[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"address"]]];
                
                headerLabel.text = [NSString stringWithFormat:@"%@",str];
                headerLabel.textColor = [UIColor whiteColor];
                headerLabel.font = [UIFont boldSystemFontOfSize:16];
                headerLabel.backgroundColor = [UIColor clearColor];
                
                CGRect imageRect = CGRectMake(0.0f, 0.0f, SCREEN_WIDTH, 30.0f);
                UIImageView *headerImage = [[UIImageView alloc] initWithFrame:imageRect];
                UIImage *image = [UIImage imageNamed:@"hotDealsBg.png"];
                [headerImage setImage:image];
                headerImage.opaque = YES;
                [headerImage addSubview:headerLabel];
                
                [containerView addSubview:headerImage];
                //[headerImage release];
                
                break;
            }
        }
        
        if([arrLocationCategoriesforRetailers count] > section)
            headerLabel1.text = [NSString stringWithFormat:@"%@",[[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"categoryName"]];
        else
            headerLabel1.text = @" ";
        headerLabel1.textColor = [UIColor whiteColor];
        headerLabel1.font = [UIFont boldSystemFontOfSize:labelFont];
        headerLabel1.backgroundColor = [UIColor clearColor];
    }
    
    UIImageView *headerImage1 = [[UIImageView alloc] initWithFrame:imageRect1];
    
    headerImage1.backgroundColor = [UIColor lightGrayColor];
    headerImage1.opaque = YES;
    [headerImage1 addSubview:headerLabel1];
    
    [containerView addSubview:headerImage1];
    //[headerImage1 release];
    containerView.backgroundColor = [UIColor clearColor];
    return containerView;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isProductTabDisplaying)
    {
        NSMutableDictionary *dicLocal = [arrCatagories objectAtIndex:section];
        if(nextpageFlag == 1 && section == [arrCatagories count]-1)
        {
            int count = (int)[[dicProductsCoupon valueForKey:[dicLocal valueForKey:@"categoryId"]]count];
            lowerLimitvalue = lowerLimitvalue + count;
            return count + 1;
        }
        else
        {
            int count = (int)[[dicProductsCoupon valueForKey:[dicLocal valueForKey:@"categoryId"]]count];
            lowerLimitvalue = lowerLimitvalue + count;
            return count;
        }
    }
    else
    {
        if(nextpageFlag == 1 && section == [arrLocationCategoriesforRetailers count]-1)
        {
            NSMutableString *strId = [[NSMutableString alloc]initWithString:[[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"retId"]];
            [strId appendFormat:@"-%@",[[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"categoryId"]];
            lowerLimitvalue = lowerLimitvalue + (int)[[dicLocationCupon valueForKey:strId]count];
            return [[dicLocationCupon valueForKey:strId]count]+1;
        }
        else
        {
            NSMutableString *strId = [[NSMutableString alloc]initWithString:[[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"retId"]];
            [strId appendFormat:@"-%@",[[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"categoryId"]];
            lowerLimitvalue = lowerLimitvalue + (int)[[dicLocationCupon valueForKey:strId]count];
            return [[dicLocationCupon valueForKey:strId]count];
        }
    }
    return 0;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    lowerLimitvalue = 0;
    if(isProductTabDisplaying)
        return [arrCatagories count];
    else
    {
        return [arrLocationCategoriesforRetailers count];
    }
    return 0;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(isProductTabDisplaying)
    {
        if([arrCatagories count] > 0)
            return [[arrCatagories objectAtIndex:section]valueForKey:@"categoryName"];
    }
    //    else
    //    {
    //        return [[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"categoryName"];
    //    }
    return NULL;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell ;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    NSMutableDictionary *dicLocal;
    NSString *catId;
    NSMutableArray *arr;
    NSMutableDictionary *dicCoupon;
    if(isProductTabDisplaying && [arrCatagories count])
    {
        if([arrCatagories count] > indexPath.section)
        {
            dicLocal = [arrCatagories objectAtIndex:indexPath.section];
            
            int count = (int)[[dicProductsCoupon valueForKey:[dicLocal valueForKey:@"categoryId"]]count];
            
            if(nextpageFlag == 1 && indexPath.section == [arrCatagories count]-1 && indexPath.row == count) //Changes made on 29 june on view more results
            {
                loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                
                loading.center = cell.contentView.center;
                loading.color = [UIColor blackColor];
                
                
                [cell.contentView addSubview:loading];
                [loading startAnimating];
            }
            
            
            else{
                catId = [dicLocal valueForKey:@"categoryId"];
                arr = [dicProductsCoupon objectForKey:catId];
                dicCoupon = [arr objectAtIndex:indexPath.row];
                
                if(dicCoupon && [dicCoupon count]>0)
                {
                    //claimFlag
                    //@"redeemFlag"
                    addRemGalley = [UIButton buttonWithType:UIButtonTypeCustom];
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
                        addRemGalley.frame = CGRectMake(7, 13, 34, 34) ;
                    }
                    else
                    {
                        addRemGalley.frame = CGRectMake(10, 15, 44, 44) ;
                    }
                    
                    [addRemGalley setImage:[UIImage imageNamed:@"WL_addCpn_live.png"] forState:UIControlStateNormal];
                    [addRemGalley addTarget:self action:@selector(addRemoveFromGallery:) forControlEvents:UIControlEventTouchUpInside];
                    
                    if (![[dicCoupon valueForKey:@"used"] isEqualToString:@"0"])
                        [addRemGalley setImage:[UIImage imageNamed:@"WL_addCpn_Off.png"] forState:UIControlStateNormal];
                    
                    [cell addSubview:addRemGalley];
                    
                    // Display New Image Icon for the New Hot Deal
                    if ([[dicCoupon valueForKey:@"newFlag"]isEqualToString:@"1"])
                    {
                        AsyncImageView *asyncImageView ;
                        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                            asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 35)];
                        }
                        else
                        {
                            asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 45)];
                        }
                        asyncImageView.backgroundColor = [UIColor clearColor];
                        [asyncImageView loadImagefrommainBundel:@"new_ribbon_70.png"];
                        [cell addSubview:asyncImageView];
                        //[asyncImageView release];
                    }
                    
                    if([dicCoupon valueForKey:@"couponName"])
                    {
                        UILabel *tx_label ;
                        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                            tx_label = [[UILabel alloc] initWithFrame:CGRectMake(104, 0, 200, 40)];
                            tx_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
                        }
                        else
                        {
                            tx_label = [[UILabel alloc] initWithFrame:CGRectMake(134, 0, 600, 60)];
                            tx_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
                        }
                        
                        tx_label.textColor = [UIColor colorWithRGBInt:0x112e72];
                        tx_label.numberOfLines = 2;
                        [tx_label setText:[dicCoupon valueForKey:@"couponName"]];
                        [tx_label setLineBreakMode:NSLineBreakByWordWrapping];
                        [cell addSubview:tx_label];
                        ReleaseAndNilify(tx_label);
                    }
                    
                    if(![[dicCoupon valueForKey:@"couponDiscountAmount"]isEqualToString:@"N/A"])
                    {
                        UILabel *detailLabel;
                        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                            detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(104, 40, 200, 20)];
                            detailLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
                            
                        }
                        else
                        {
                            detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(134, 40, 600, 40)];
                            detailLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
                        }
                        detailLabel.backgroundColor = [UIColor clearColor];
                        detailLabel.textColor = [UIColor grayColor];
                        detailLabel.numberOfLines = 1;
                        [detailLabel setText: [NSString stringWithFormat:NSLocalizedString(@"%@ off Expires %@",@"%@ off Expires %@"),
                                               [dicCoupon valueForKey:@"couponDiscountAmount"],
                                               [dicCoupon valueForKey:@"couponExpireDate"]]];
                        
                        
                        [cell addSubview:detailLabel];
                        ReleaseAndNilify(detailLabel);
                    }
                    
                    // Coupon Image
                    if([dicCoupon valueForKey:@"couponImagePath"])
                    {
                        NSString *imagePathStr = [dicCoupon valueForKey:@"couponImagePath"];
                        SdImageView *asyncImageView ;//= [[AsyncImageView alloc] initWithFrame:CGRectMake(50, 7, 44, 44)];
                        if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                            asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(50, 7, 44, 44)];
                        }
                        else
                        {
                            asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(50, 10, 60, 60)];
                        }
                        asyncImageView.contentMode = UIViewContentModeScaleToFill;
                        asyncImageView.backgroundColor = [UIColor clearColor];
                        [asyncImageView loadImage:imagePathStr];
                        [cell addSubview:asyncImageView];
                        ReleaseAndNilify(asyncImageView);
                    }
                    
                }
            }
        }
    }
    else
    {
        if([arrLocationCategoriesforRetailers count]> indexPath.section)
        {
            NSMutableString *strId = [[NSMutableString alloc]initWithString:[[arrLocationCategoriesforRetailers objectAtIndex:indexPath.section]valueForKey:@"retId"]];
            [strId appendFormat:@"-%@",[[arrLocationCategoriesforRetailers objectAtIndex:indexPath.section]valueForKey:@"categoryId"]];
            int count =(int)[[dicLocationCupon valueForKey:strId]count];
            
            if(nextpageFlag == 1 && indexPath.section == [arrLocationCategoriesforRetailers count]-1 && indexPath.row == count) //Changes made on 29 june on view more results
            {
                
                loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                
                loading.center = cell.contentView.center;
                loading.color = [UIColor blackColor];
                
                
                [cell.contentView addSubview:loading];
                [loading startAnimating];
                
                cell.userInteractionEnabled = NO;
                //            cell.accessoryType = UITableViewCellAccessoryNone;
                //            CGRect frame;
                //            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                //                frame.origin.x = 0;
                //                frame.origin.y = 10;
                //                frame.size.width = 320;
                //                frame.size.height = 24;
                //            }
                //            else
                //            {
                //                frame.origin.x = 0;
                //                frame.origin.y = 10;
                //                frame.size.width = SCREEN_WIDTH;
                //                frame.size.height = 44;
                //            }
                //
                //            UILabel *label = [[[UILabel alloc] initWithFrame:frame] ;
                //            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                //               label.font = [UIFont boldSystemFontOfSize:16];
                //            }
                //            else
                //            {
                //                label.font = [UIFont boldSystemFontOfSize:20];
                //            }
                //
                //            label.textAlignment = NSTextAlignmentCenter;
                //            label.textColor = [UIColor colorWithRGBInt:0x112e72];
                //            label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
                //            [cell.contentView addSubview:label];
                //            return cell;
            }
            
            
            arr = [dicLocationCupon valueForKey:strId];
            dicCoupon = [arr objectAtIndex:indexPath.row];
            
            if(dicCoupon && [dicCoupon count]>0)
            {
                //claimFlag
                //@"redeemFlag"
                addRemGalley = [UIButton buttonWithType:UIButtonTypeCustom];
                if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
                    addRemGalley.frame = CGRectMake(7, 13, 34, 34) ;
                }
                else
                {
                    addRemGalley.frame = CGRectMake(10, 15, 44, 44) ;
                }
                [addRemGalley setImage:[UIImage imageNamed:@"WL_addCpn_live.png"] forState:UIControlStateNormal];
                [addRemGalley addTarget:self action:@selector(addRemoveFromGallery:) forControlEvents:UIControlEventTouchUpInside];
                
                if (![[dicCoupon valueForKey:@"used"] isEqualToString:@"0"])
                    [addRemGalley setImage:[UIImage imageNamed:@"WL_addCpn_Off.png"] forState:UIControlStateNormal];
                [cell addSubview:addRemGalley];
                
                // Display New Image Icon for the New Hot Deal
                if ([[dicCoupon valueForKey:@"newFlag"]isEqualToString:@"1"])
                {
                    AsyncImageView *asyncImageView ;
                    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                        asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 35)];
                    }
                    else
                    {
                        asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 45)];
                    }
                    
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    [asyncImageView loadImagefrommainBundel:@"new_ribbon_70.png"];
                    [cell addSubview:asyncImageView];
                    //[asyncImageView release];
                }
                
                if([dicCoupon valueForKey:@"couponName"])
                {
                    UILabel *tx_label ;
                    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                        tx_label = [[UILabel alloc] initWithFrame:CGRectMake(104, 0, 200, 40)];
                        tx_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
                    }
                    else
                    {
                        tx_label = [[UILabel alloc] initWithFrame:CGRectMake(134, 0, 600, 60)];
                        tx_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
                    }
                    
                    tx_label.textColor = [UIColor colorWithRGBInt:0x112e72];
                    tx_label.numberOfLines = 2;
                    [tx_label setText:[dicCoupon valueForKey:@"couponName"]];
                    [tx_label setLineBreakMode:NSLineBreakByWordWrapping];
                    [cell addSubview:tx_label];
                    ReleaseAndNilify(tx_label);
                }
                
                if(![[dicCoupon valueForKey:@"couponDiscountAmount"]isEqualToString:@"N/A"])
                {
                    
                    UILabel *detailLabel;
                    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                        detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(104, 40, 200, 20)];
                        detailLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
                        
                    }
                    else
                    {
                        detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(134, 40, 600, 40)];
                        detailLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
                    }
                    detailLabel.backgroundColor = [UIColor clearColor];
                    detailLabel.textColor = [UIColor grayColor];
                    detailLabel.numberOfLines = 1;
                    [detailLabel setText: [NSString stringWithFormat:NSLocalizedString(@"%@ off Expires %@",@"%@ off Expires %@"),
                                           [dicCoupon valueForKey:@"couponDiscountAmount"],
                                           [dicCoupon valueForKey:@"couponExpireDate"]]];
                    
                    
                    //detailLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
                    [cell addSubview:detailLabel];
                    ReleaseAndNilify(detailLabel);
                }
                
                // Coupon Image
                if([dicCoupon valueForKey:@"couponImagePath"])
                {
                    NSString *imagePathStr = [dicCoupon valueForKey:@"couponImagePath"];
                    SdImageView *asyncImageView ;
                    if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(50, 7, 44, 44)];
                    }
                    else
                    {
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(50, 10, 60, 60)];
                    }
                    asyncImageView.contentMode = UIViewContentModeScaleToFill;
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    [asyncImageView loadImage:imagePathStr];
                    [cell addSubview:asyncImageView];
                    ReleaseAndNilify(asyncImageView);
                }
            }
        }
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
   // NSString *catId;
  //  NSMutableArray *arr;
   // NSMutableDictionary *dicCoupon;
    if(isProductTabDisplaying)
    {
        NSMutableDictionary *dicLocal = [arrCatagories objectAtIndex:indexPath.section];
        
        int count = (int)[[dicProductsCoupon valueForKey:[dicLocal valueForKey:@"categoryId"]]count];
        
        if(nextpageFlag == 1 && indexPath.section == [arrCatagories count]-1 && indexPath.row == count)
        {
            //
            NSLog(@"value: %@",[defaults valueForKey:@"ViewMore"]);
            if (![defaults boolForKey:@"ViewMore"] && lowerLimitvalue!=0) {
                searchBar.userInteractionEnabled = NO;
                [defaults setBool:YES forKey:@"ViewMore"];
                
                dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
                dispatch_async(dispatchQueue, ^(void){
                    [self getMuCouponItemsforProducts];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [myGalleryTV reloadData];
                        searchBar.userInteractionEnabled = YES;
                    });
                });
                
            }
            
            return;
        }
        
       // catId = [dicLocal valueForKey:@"categoryId"];
        //arr = [dicProductsCoupon objectForKey:catId];
      //  dicCoupon = [arr objectAtIndex:indexPath.row];
    }
    else
    {
        NSMutableString *strId = [[NSMutableString alloc]initWithString:[[arrLocationCategoriesforRetailers objectAtIndex:indexPath.section]valueForKey:@"retId"]];
        [strId appendFormat:@"-%@",[[arrLocationCategoriesforRetailers objectAtIndex:indexPath.section]valueForKey:@"categoryId"]];
        
        int count =(int)[[dicLocationCupon valueForKey:strId]count];
        
        if(nextpageFlag == 1 && indexPath.section == [arrLocationCategoriesforRetailers count]-1 && indexPath.row == count)
        {
            myGalleryTV.tag = 0;
            if (![defaults boolForKey:@"ViewMore"]) {
                [defaults setBool:YES forKey:@"ViewMore"];
                
                dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
                dispatch_async(dispatchQueue, ^(void){
                    [self callLocationbasedCoupons];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [myGalleryTV reloadData];
                    });
                });
                
            }
            
            return;
        }
        
     //   arr = [dicLocationCupon valueForKey:strId];
       // dicCoupon = [arr objectAtIndex:indexPath.row];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 60.0;
    }
    else
    {
        return 90.0;
    }
    
}

#pragma mark -
#pragma mark Table view delegate


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if(!isProductTabDisplaying)
    {
        for (int i =0 ; i < [arrSectionArrayForRetailers count]; i++)
        {
            if (([[arrSectionArrayForRetailers objectAtIndex:i] intValue])-1 == section)
            {
                return 60;
                break;
            }
            
        }
    }
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        return 30.0;
    }
    return 26;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    indexpathRowCouponList = (int)indexPath.row;
    indexpathSectionCouponList = (int)indexPath.section;
    
    [defaults setBool:NO forKey:@"ViewMore"];
    
    
    NSString *catId;
    NSMutableArray *arr;
    NSMutableDictionary *dicCoupon;
    if(isProductTabDisplaying)
    {
        NSMutableDictionary *dicLocal = [arrCatagories objectAtIndex:indexPath.section];
        
        int count = (int)[[dicProductsCoupon valueForKey:[dicLocal valueForKey:@"categoryId"]]count];
        
        if(nextpageFlag == 1 && indexPath.section == [arrCatagories count]-1 && indexPath.row == count)
        {
            //[self getMuCouponItemsforProducts];
            return;
        }
        
        catId = [dicLocal valueForKey:@"categoryId"];
        arr = [dicProductsCoupon objectForKey:catId];
        dicCoupon = [arr objectAtIndex:indexPath.row];
    }
    else
    {
        NSMutableString *strId = [[NSMutableString alloc]initWithString:[[arrLocationCategoriesforRetailers objectAtIndex:indexPath.section]valueForKey:@"retId"]];
        [strId appendFormat:@"-%@",[[arrLocationCategoriesforRetailers objectAtIndex:indexPath.section]valueForKey:@"categoryId"]];
        
        int count =(int)[[dicLocationCupon valueForKey:strId]count];
        
        if(nextpageFlag == 1 && indexPath.section == [arrLocationCategoriesforRetailers count]-1 && indexPath.row == count)
        {
            myGalleryTV.tag = 0;
            //[self callLocationbasedCoupons];
            return;
        }
        
        arr = [dicLocationCupon valueForKey:strId];
        dicCoupon = [arr objectAtIndex:indexPath.row];
    }
    
    [defaults setValue:[dicCoupon valueForKey:@"couponId"] forKey:@"couponId"];
    [defaults setValue:[dicCoupon valueForKey:@"couponListId"] forKey:@"couponListId"];
    [defaults  setValue:[dicCoupon valueForKey:@"couponImagePath"] forKey:@"imgPath"];
    
    NewCouponDetailViewController *obj_CouponGalleryDetail = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:obj_CouponGalleryDetail animated:NO];
    //[obj_CouponGalleryDetail release];
    
    
    [myGalleryTV deselectRowAtIndexPath:[myGalleryTV indexPathForSelectedRow] animated:YES];
}


-(void) addRemoveFromGallery : (UIButton *) sender {
    //NSIndexPath *index = [myGalleryTV indexPathForCell:(UITableViewCell *)[sender superview]];
    //updated for iOS7 bug fixing
    CGPoint location = [sender.superview convertPoint:sender.center toView:myGalleryTV];
    
    
    NSIndexPath *index ;
    index = [myGalleryTV indexPathForRowAtPoint:location];
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    
    if(userIdentifier==TRUE)
    {
        [NewsUtility signupPopUp:self];
    }
    
    
    else
    {
        
        NSString *catId;
        NSMutableArray *arr;
        //NSMutableDictionary *dictCoupon;
        if(isProductTabDisplaying)
        {
            catId = [[arrCatagories objectAtIndex:index.section]valueForKey:@"categoryId"];
            arr = [dicProductsCoupon objectForKey:catId];
            dictCoupon = [arr objectAtIndex:index.row];
        }
        else
        {
            NSMutableString *strId = [[NSMutableString alloc]initWithString:[[arrLocationCategoriesforRetailers objectAtIndex:index.section]valueForKey:@"retId"]];
            [strId appendFormat:@"-%@",[[arrLocationCategoriesforRetailers objectAtIndex:index.section]valueForKey:@"categoryId"]];
            
            arr = [dicLocationCupon valueForKey:strId];
            dictCoupon = [arr objectAtIndex:index.row];
        }
        
        NSString *str = @"";
        // iWebrequest = addRemoveCoupon;
        NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<CLRDetails><couponId>%@</couponId>",[dictCoupon valueForKey:@"couponId"]];
        [reqStr appendFormat:@"<userId>%@</userId></CLRDetails>",[defaults valueForKey:KEY_USERID]];
        
        if ([[dictCoupon valueForKey:@"used"]isEqualToString:@"0"])
            str = @"add";
        else
            str = @"remove";
        
        [defaults  setValue:VALUE_COUPON forKey:KEY_CLR];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@gallery/%@%@",BASE_URL,str,[defaults valueForKey:KEY_CLR]];
        
        //[ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
        NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:reqStr]];
        ReleaseAndNilify(reqStr);
        
        if ([UtilityManager isNullOrEmptyString:response]) {
            ReleaseAndNilify(response);
            return;
        }
        
        else{
            
            [self parse_addRemoveCoupon:response];
        }
    }
}

-(void)parse_addRemoveCoupon:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    int responseCode = [[TBXML textForElement:responseCodeElement] intValue];
    if (responseCode == 10000) {
        
        if ([[[self.navigationController viewControllers] objectAtIndex:[[self.navigationController viewControllers] count]-2 ] isKindOfClass:[ProductPage class]])
        {
            [defaults  setBool:YES forKey:@"refreshProd"];
        }
        
        if ([[dictCoupon valueForKey:@"used"]isEqualToString:@"0"])
            [dictCoupon setValue:@"1" forKey:@"used"];
        else
            [dictCoupon setValue:@"0" forKey:@"used"];
        
        //else if ([[dicCoupon valueForKey:@"used"]isEqualToString:@"1"])
        //[dicCoupon setValue:@"2" forKey:@"used"];
        
        [SharedManager setRefreshList:YES];
        [myGalleryTV reloadData];
    }
    else
    {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
    }
    ReleaseAndNilify(tbxml);
}

- (void)segmentChanged
{
    [arrSectionArrayForRetailers addObject:@"1"];
    
    lowerLimitvalue = 0;
    //If seach tab is active then hide the search view
    [self toolbarDoneClicked:nil];
    searchView.hidden = YES;
    
    // Set the bottom tab images
    //    [couponGalleryBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [bycityBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [searchBtn setImage:[UIImage imageNamed:@"tab_btn_up_srchTxt.png"] forState:UIControlStateNormal];
    //    [preferanceBtn setImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    
    switch(segmentedControl.selectedSegmentIndex)
    {
        case 0:
        {
            isCalledFromSegementChange = YES;
            localProductFlag=NO;
            myGalleryTV.tag = 0;
            [self callLocationbasedCoupons];
            [myGalleryTV reloadData];
            
        }
            break;
        case 1:
        {
            isCalledFromSegementChange = YES;
            localProductFlag=YES;
            myGalleryTV.tag = 0;
            [self getMuCouponItemsforProducts];
            [myGalleryTV reloadData];
        }
            break;
    }
}

-(void)setWhenCalledfromSegementChange
{
    // remove all objects from Products Stored dataset
    [arrCatagories removeAllObjects];
    [dicProductsCoupon removeAllObjects];
    
    // remove all objects from Location Stored dataset
    [dicLocationCupon removeAllObjects];
    [arrLocationCategoriesforRetailers removeAllObjects];
    [arrSectionArrayForRetailers removeAllObjects];
    [arrSectionArrayForRetailers addObject:@"1"];
    
    
}


-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    navigatedFromMainMenu=false;
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            [defaults  setBool:NO forKey:@"showBackButton"];
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}


-(void)navigateToFindView
{
    [defaults  setBool:NO forKey:@"findsplash"];
    [defaults setBool:YES forKey:BottomButton];
    //    [defaults setValue:@"0" forKey:KEY_BOTTOMBUTTONID];
    //[defaults setValue:nil forKey:KEY_MITEMID];
    navigatedFromCity=true;
    // [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    //[self navigateToFindView];
    navigatedFromMainMenu=false;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}


-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}



-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}


//-(void)request_GetUserCat{
//
//    iWebRequestState = GETFAVCATEGORIES;
//    if([Network currentReachabilityStatus]==0)
//    {
//        [common showAlert];
//    }
//    else {
//
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//
//    }
//}

-(void)requestToGetPreferredLocations
{
    iWebrequest= GETFAVLOCATIONSS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}

//parse user favorite categories
-(void)parseGetPreferredCategories:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)request_HubcitiAnythingInfo{
    
    iWebrequest = HubcitiAnythingInfos;
    requestAnythingFromMainMenu=TRUE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebrequest = appsitedetailss;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

//#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        return;
    
    [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    [self showActionSheet];
    
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    if ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusDenied) {
        [UtilityManager showAlert:nil msg:NSLocalizedString(@"Please change the contacts privacy setting for App",@"Please change the contacts privacy setting for App")];
    }
    else{
        
        emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
        __typeof(self) __weak  obj = self;
        emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
            
            [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
        } ;
        
        [emailSendingVC loadMail];
    }
}

-(void) showActionSheet {
   
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}




-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}




#pragma mark parse methods


-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                             actionWithTitle:@"OK"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action){
                                                 [alert dismissViewControllerAnimated:NO completion:nil];
                                                  [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                             }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}


// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    // Calculate Y position for View
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-55, SCREEN_WIDTH, 55)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
        
    }
    
    for(int btnLoop= 0; btnLoop < [arrEventBottomButtonDO count]; btnLoop++)
    {
        bottomButtonDO *obj_eventBottomDO = [arrEventBottomButtonDO objectAtIndex:btnLoop];
        
        view = [[bottomButtonView alloc]init];
        
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, (SCREEN_HEIGHT-yVal-55) + 4, 80,55) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrEventBottomButtonDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrEventBottomButtonDO count],bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_eventBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        //view.tag=99;
        [self.view bringSubviewToFront:view];
    }
}


-(void)bottomButtonPressed:(id)sender
{
    [defaults setBool:YES forKey:BottomButton];
    
    [defaults setBool:NO forKey:@"ViewMore"];
    //    [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    bottomButtonDO *obj_eventBottomDO = [arrEventBottomButtonDO objectAtIndex:tag];
    
    [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_eventBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_eventBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_eventBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_eventBottomDO.btnLinkTypeName);
    if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    else if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_eventBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_eventBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            self.iEventsListViewController = iEventsListViewController;
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            ////[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    else{ //other menu types
        
        switch ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
                //                [defaults  setBool:YES forKey:@"HotDealsplash"];
                //                HotDealsList *hotDeals = [[HotDealsList alloc]initWithNibName:@"HotDealsList" bundle:nil];
                //                [self.navigationController pushViewController:hotDeals animated:NO];
                //                [hotDeals release];
                break;
                
            case 1://Scan Now
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3:{//Events
                [EventsBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
                self.iEventsListViewController = iEventsListViewController;
                [self.navigationController pushViewController:iEventsListViewController animated:NO];
                ////[iEventsListViewController release];;
            }
                break;
            case 4://Whats NearBy
                [NearbyBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                [self navigateToWhatsNearBy];
                break;
                
            case 5://Find
                [FindBottomButtonID addObject:obj_eventBottomDO.bottomBtnID];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                [self navigateToFindView];
                break;
                
            case 6:{//City Experience
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                self.cityExp = citi;
                [self.navigationController pushViewController:citi animated:NO];
                //[citi release];
            }
                break;
                
            case 7://City Services
                
                break;
                
            case 8://Visitors Bureau
                
                break;
                
            case 9://transportation
                
                break;
                
            case 10://Preference
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
                break;
                
            case 13:
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=FALSE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_eventBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    self.cityExp = cevc;
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    self.pvc = filters;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    self.filterList = filterList;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                //                DealsViewController *dvc = [[DealsViewController alloc]initWithNibName:@"DealsViewController" bundle:[NSBundle mainBundle]];
                //                [self.navigationController pushViewController:dvc animated:NO];
                //                [dvc release];
                //
                //                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                self.faqList = faqCatList;
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                [FundraiserBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }
                break;
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    iWebrequest = GETUSERINFORMATION;
                    [self requestToGetUserData];
                }
            }break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    iWebrequest = GETFAVLOCATIONSS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!")  msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        // [inform release];
                    }
                    
                }
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            //  [citPref release];
                        }
                    }
                }
                
            }
            break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                //privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
                
            default:
                break;
        }
    }
    //NSlog(@"%d",tag);
}

//-(void)requestToGetUserData
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
//    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//    ReleaseAndNilify(xmlStr);
//}
-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}
-(void)parseGetUserData:(id)response
{
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}


-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}

@end



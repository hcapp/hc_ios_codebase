//
//  RetailerSpecialOffersViewController.h
//  HubCiti
//
//  Created by deepak.agarwal on 1/23/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RetailerSpecialOffersViewController : UIViewController<CustomizedNavControllerDelegate>
{
    IBOutlet UITableView *tblSpecialOffers;
    int lastVisitedRecord;
    BOOL isNextPageAvailable;
    NSMutableArray *arrSpecialOfferRetailer;
}
@end

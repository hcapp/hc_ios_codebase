//
//  GalleryDisplayViewController.h
//  HubCiti
//
//  Created by Kitty on 18/03/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "CouponsInDealViewController.h"
@class RetailersListViewController;
@class FilterRetailersList;
@class CityExperienceViewController;
@class FundraiserListViewController;
@class FilterListViewController;
@class FAQCategoryList;
@class EventsListViewController;
@class GalleryList;
@class AnyViewController;
@class EmailShareViewController;
BOOL fromGalleryDisplay;
NSDictionary *viewDictionary;

@interface GalleryDisplayViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>{
    UITableView *galleryTableView;
    NSString *cellColor,*sectionColor,*fontColor,*mBackGrdColor;
    NSMutableArray *galleryArray;
    NSMutableArray *arrEventBottomButtonDO;
    NSMutableArray *arrBottomButtonViewContainer;
    int bottomBtn;
    WebRequestState iWebRequestState;
    CommonUtility *common;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,strong) FAQCategoryList *faqList;
@property(nonatomic,strong) GalleryList *gList;
@property(nonatomic,strong) EventsListViewController* iEventsListViewController;
@property(nonatomic,strong) FilterListViewController *filterList;
@property(nonatomic,strong) FundraiserListViewController *iFundraiserListViewController;
@property(nonatomic,strong) CityExperienceViewController *cityExp;
@property(nonatomic,strong) FilterRetailersList *filters;
@property(nonatomic,strong) RetailersListViewController* retailerListScreen;
@property(nonatomic) BOOL isSubMenu;
@property(nonatomic,strong) NSString *type;

@end
@interface DealStateDo : NSObject{
    
    NSString *statename,*stateImagePath,*flag;
}

@property(nonatomic,strong)NSString *statename;
@property(nonatomic,strong)NSString *stateImagePath;
@property(nonatomic,strong)NSString * flag;


@end

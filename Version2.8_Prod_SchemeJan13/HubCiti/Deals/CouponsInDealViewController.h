//
//  CouponsInDealViewController.h
//  HubCiti
//
//  Created by Nikitha on 11/21/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomizedNavController.h"
#import "SortAndFilter.h"
#include "AnyViewController.h"
#import "CouponsRetailerDetails.h"
#import "CouponsResponse.h"
#import "Mapkit/MKMapView.h"
#import "CouponMapDetails.h"
#import "CouponMapResponse.h"
#import "CouponsMapViewController.h"

@class EmailShareViewController;

@interface CouponsInDealViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    CustomizedNavController * cusNav;
    NSMutableArray *annArray;
    MKMapView *mapView;
    BOOL isRetailerEvent;
    NSTimer * timer;
}
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControlOutlet;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableViewOutlet;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableBottomConstraint;
@property(nonatomic,strong) SortAndFilter *sortObj,*savedSortObj;
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (strong,nonatomic) CouponsResponse *couponsResponse;
@property (strong,nonatomic) CouponsRetailerDetails *couponRetailerDetails;
@property(nonatomic,strong) NSString * retailId;
@property (strong,nonatomic) CouponMapResponse *couponMapResponse;
@property (strong,nonatomic) CouponMapDetails *couponMapDetails;
@property (nonatomic)BOOL showMapFlag;
@property (nonatomic,strong) NSMutableArray *globalRetailerService;
@property(nonatomic,strong) NSString * searchBarText;
@end



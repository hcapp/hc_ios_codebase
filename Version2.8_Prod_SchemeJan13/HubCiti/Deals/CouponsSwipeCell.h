//
//  CouponsSwipeCell.h
//  HubCiti
//
//  Created by Lakshmi H R on 11/24/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponsRetailerDetails.h"
#import "SortAndFilter.h"

@interface CouponsSwipeCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UICollectionView *couponCollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *couponCollViewTopConst;
@property (nonatomic,strong) NSMutableArray *swipeCouponsList;
@property (nonatomic,strong) UIViewController *parentViewController;
@property (nonatomic) NSInteger couponSection;
@property (nonatomic,strong) NSString *sectionTitleLabel;
@property (nonatomic)BOOL fromMyAcc;
@property (nonatomic)BOOL fromCoup;
@property(nonatomic,strong) SortAndFilter *sortObj;
@end

//
//  CouponViewCell.m
//  HubCiti
//
//  Created by Lakshmi H R on 11/24/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//
//@property (strong, nonatomic) IBOutlet SdImageView *coupon_Image;
//@property (strong, nonatomic) IBOutlet UILabel *couponName;
//@property (strong, nonatomic) IBOutlet UILabel *couponOffPrice;
//@property (strong, nonatomic) IBOutlet UILabel *couponLocName;
//@property (strong, nonatomic) IBOutlet UILabel *couponNearDistance;
//@property (strong, nonatomic) IBOutlet UILabel *couponNoLocation;
#import "CouponViewCell.h"

@implementation CouponViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}

-(void)updateUiWithRetailerDetails:(CouponsRetailerDetails *)retailerDetails {
    
    self.couponName.text = nil;
    self.coupon_Image.image = nil;
    self.couponLocName.text = nil;
    self.couponNoLocation.text = nil;
    self.couponOffPrice.text = nil;
    self.couponNearDistance.text = nil;
    
    if (!retailerDetails) {
        self.viewMoreLabel.text = @"View More";
        self.viewMoreLabel.layer.borderColor = ([[UIColor lightGrayColor]CGColor]);
        self.viewMoreLabel.layer.borderWidth = 0.5;
        self.viewMoreLabel.clipsToBounds = YES;
    }
    else {
        if(retailerDetails.couponName.length > 0)
        {
            self.couponName.text = retailerDetails.couponName;
        }
        if(retailerDetails.couponImagePath.length > 0)
        {
            [self.coupon_Image loadImage:retailerDetails.couponImagePath];
        }
        if(retailerDetails.retName.length > 0)
        {
            self.couponLocName.text = retailerDetails.retName;
        }
        if([retailerDetails.counts intValue]>0)
        {
            self.couponNoLocation.text = [NSString stringWithFormat:@"%d participating locations available",[retailerDetails.counts intValue]];
        }
        if(retailerDetails.bannerTitle.length >0)
        {
            self.couponOffPrice.text = retailerDetails.bannerTitle;
        }
        if(retailerDetails.distance.length > 0 && [retailerDetails.counts intValue] > 1)
        {
            self.couponNearDistance.text = [NSString stringWithFormat:@"Closest location %@", retailerDetails.distance];
        }
        else if (retailerDetails.distance.length > 0)
        {
            self.couponNearDistance.text = retailerDetails.distance;
        }
        self.viewMoreLabel.text = nil;
        self.viewMoreLabel.layer.borderColor = nil;
        self.viewMoreLabel.layer.borderWidth = 0;
    }
}

@end

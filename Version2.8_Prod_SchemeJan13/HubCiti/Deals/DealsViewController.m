//
//  DealsViewController.m
//  HubCiti
//
//  Created by deepak.agarwal on 1/22/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import "DealsViewController.h"
#import "DealCouponsViewController.h"
#import "HotDealsList.h"
#import "MainMenuViewController.h"
#import "SpecialOfferRetailersViewController.h"

@interface DealsViewController ()

@end

@implementation DealsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setAccessibilityLabel:@"dealsView"];
    [self viewdesign];
    arrBottomButtonImages = [[NSMutableArray alloc]init];
    [arrBottomButtonImages addObject:@"cs_claimed_off"];
    [arrBottomButtonImages addObject:@"cs_expired_off"];
    [arrBottomButtonImages addObject:@"cs_used_off"];
    [arrBottomButtonImages addObject:@"cs_claimed_on"];
    [arrBottomButtonImages addObject:@"cs_expired_on"];
    [arrBottomButtonImages addObject:@"cs_used_on"];
    
    [lblNonAvailable setHidden:YES];
    
    [[HubCitiManager sharedManager]setDealsCouponDisplay:NO];
    [[HubCitiManager sharedManager]setDealsHotDealDisplay:NO];

    UIButton *backBtn = [UtilityManager customizeBackButton];
    self.navigationItem.title = @"Deals";
    
    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = mainPage;
    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"MainMenu"];
    [mainPage release];

    
    [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = back;
    self.navigationItem.hidesBackButton = YES;
    [back release];

    
    // Do any additional setup after loading the view from its nib.
}

-(void)returnToMainPage:(id)sender{
    [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
}
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    // If from Gallery Screen not selected Coupon/HotDeal from Bottom menu
    if([[HubCitiManager sharedManager]dealsCouponDisplay]==NO && [[HubCitiManager sharedManager]dealsHotDealDisplay]==NO)
    {
        switch (selectedButtonNumber)
        {
            case 1: // Coupon
                [iCouponsViewController updateViewonViewWillAppear];
                break;
            case 2: // Hot Deal
                [iHotDealsList updateViewonViewWillAppear];
                break;
                
            case 3: // Specials
                [iSpecialOfferRetailersViewController updateViewonViewWillAppear];
                break;
                
            default:
                break;
        }
    }
    else // When From Galler selected HD/Coupon from Bottom menu bar
    {
        if([[HubCitiManager sharedManager]dealsCouponDisplay]==YES)
        {
            [[HubCitiManager sharedManager]setDealsCouponDisplay:NO];
            [self loadCouponViewOnScreen];
        }
        else if([[HubCitiManager sharedManager]dealsHotDealDisplay]==YES)
        {
            [[HubCitiManager sharedManager]setDealsHotDealDisplay:NO];
            [self loadHotDealViewOnScreen];
        }
    }
}


-(void)viewdesign
{
    float xPos = 0.0;
    float yPos = 0.0;
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        btnCoupon = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCoupon.frame     = CGRectMake(xPos, yPos,SCREEN_WIDTH/3,40);
        [btnCoupon addTarget:self action:@selector(TopBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnCoupon.tag = 1;
        [btnCoupon setTitle:@"Coupons" forState:UIControlStateNormal];
        [btnCoupon.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        [btnCoupon setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnCoupon setAdjustsImageWhenHighlighted:NO];
        [btnCoupon setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        [btnCoupon setBackgroundColor:[UIColor blackColor]];
        [btnCoupon setAccessibilityLabel:@"couponButton"];
        [self.view addSubview:btnCoupon];
        
        xPos = xPos + btnCoupon.frame.size.width+0.5;
        
        btnHotDeal = [UIButton buttonWithType:UIButtonTypeCustom];
        btnHotDeal.frame     = CGRectMake(xPos, yPos,SCREEN_WIDTH/3,40);
        [btnHotDeal addTarget:self action:@selector(TopBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnHotDeal.tag = 2;
        [btnHotDeal setTitle:@"Hot Deals" forState:UIControlStateNormal];
        [btnHotDeal.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        [btnHotDeal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnHotDeal setAdjustsImageWhenHighlighted:NO];
        [btnHotDeal setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        [btnHotDeal setBackgroundColor:[UIColor blackColor]];
        [btnHotDeal setAccessibilityLabel:@"hotDealButton"];
        [self.view addSubview:btnHotDeal];
        
        xPos = xPos + btnHotDeal.frame.size.width+0.5;
        
        btnSpecials = [UIButton buttonWithType:UIButtonTypeCustom];
        btnSpecials.frame     = CGRectMake(xPos, yPos,SCREEN_WIDTH/3,40);
        [btnSpecials addTarget:self action:@selector(TopBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnSpecials.tag = 3;
        [btnSpecials setTitle:@"Specials" forState:UIControlStateNormal];
        [btnSpecials.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        [btnSpecials setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnSpecials setAdjustsImageWhenHighlighted:NO];
        [btnSpecials setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        [btnSpecials setBackgroundColor:[UIColor blackColor]];
        [btnSpecials setAccessibilityLabel:@"specialsButton"];
        [self.view addSubview:btnSpecials];
    }
    else
    {
        btnCoupon = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCoupon.frame     = CGRectMake(xPos, yPos,SCREEN_WIDTH/3,60);
        [btnCoupon addTarget:self action:@selector(TopBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnCoupon.tag = 1;
        [btnCoupon setTitle:@"Coupons" forState:UIControlStateNormal];
        [btnCoupon.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:20]];
        [btnCoupon setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnCoupon setAdjustsImageWhenHighlighted:NO];
        [btnCoupon setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        [btnCoupon setBackgroundColor:[UIColor blackColor]];
        [btnCoupon setAccessibilityLabel:@"couponButton"];
        [self.view addSubview:btnCoupon];
        
        xPos = xPos + btnCoupon.frame.size.width+0.5;
        
        btnHotDeal = [UIButton buttonWithType:UIButtonTypeCustom];
        btnHotDeal.frame     = CGRectMake(xPos, yPos,SCREEN_WIDTH/3,60);
        [btnHotDeal addTarget:self action:@selector(TopBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnHotDeal.tag = 2;
        [btnHotDeal setTitle:@"Hot Deals" forState:UIControlStateNormal];
        [btnHotDeal.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:20]];
        [btnHotDeal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnHotDeal setAdjustsImageWhenHighlighted:NO];
        [btnHotDeal setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        [btnHotDeal setBackgroundColor:[UIColor blackColor]];
        [btnHotDeal setAccessibilityLabel:@"hotDealButton"];
        [self.view addSubview:btnHotDeal];
        
        xPos = xPos + btnHotDeal.frame.size.width+0.5;
        
        btnSpecials = [UIButton buttonWithType:UIButtonTypeCustom];
        btnSpecials.frame     = CGRectMake(xPos, yPos,SCREEN_WIDTH/3,60);
        [btnSpecials addTarget:self action:@selector(TopBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnSpecials.tag = 3;
        [btnSpecials setTitle:@"Specials" forState:UIControlStateNormal];
        [btnSpecials.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:20]];
        [btnSpecials setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnSpecials setAdjustsImageWhenHighlighted:NO];
        [btnSpecials setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        [btnSpecials setBackgroundColor:[UIColor blackColor]];
        [btnSpecials setAccessibilityLabel:@"specialsButton"];
        [self.view addSubview:btnSpecials];
    }
    
    
    
    if ((([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) || ([[HubCitiManager sharedManager]gps_allow_flag] == NO)) && ![defaults valueForKey:KEYZIPCODE])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Deals uses your iPhone's Location Services or your zip code to find what's around you. Please either turn on Location Services and log in again or enter a zip code in the User Information page."
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"OK",@"OK")
                                              otherButtonTitles:nil];
        [alert show];
        alert.tag= 999;
        [alert release];
        
    }
    else
        [self get_hubcitispedeals];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==999)
    {
        [self get_hubcitispedeals];
    }
}

-(void)get_hubcitispedeals
{
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length] && ([[HubCitiManager sharedManager]gps_allow_flag] == YES) &&([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorized))
    {
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
    }
    
    else if ([[defaults valueForKeyPath:KEYZIPCODE]length])
        [requestStr appendFormat:@"<postalcode>%@</postalcode>",[defaults valueForKeyPath:KEYZIPCODE]];

    [requestStr appendString:@"</ProductDetailsRequest>"];
    
    NSString *urlString =[NSString stringWithFormat:@"%@gallery/hubcitispedeals",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);
}

#pragma mark parse methods
-(void)responseData:(NSString*)response
{
    [self parse_hubcitispedeals:response];
}

-(void)parse_hubcitispedeals:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
		return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *SpecialOfferElement = [TBXML childElementNamed:@"SpecialOffer" parentElement:tbxml.rootXMLElement];
        if(SpecialOfferElement)
        {
            TBXMLElement *specialOffFlagElement = [TBXML childElementNamed:@"specialOffFlag" parentElement:SpecialOfferElement];
            TBXMLElement *hotDealFlagElement = [TBXML childElementNamed:@"hotDealFlag" parentElement:SpecialOfferElement];

            TBXMLElement *clrFlagElement = [TBXML childElementNamed:@"clrFlag" parentElement:SpecialOfferElement];
            
            if(specialOffFlagElement)
                isSpecialAvailable = [[TBXML textForElement:specialOffFlagElement]boolValue];
            
            if(hotDealFlagElement)
                isHotDealAvailable = [[TBXML textForElement:hotDealFlagElement]boolValue];

            if(clrFlagElement)
                isCouponAvailable = [[TBXML textForElement:clrFlagElement]boolValue];

        }
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    if(isSpecialAvailable)
        [self loadSpecialRetailerViewOnScreen];
    else if(isCouponAvailable)
        [self loadCouponViewOnScreen];
    else if(isHotDealAvailable)
        [self loadHotDealViewOnScreen];
    else
        [lblNonAvailable setHidden:NO];
}


-(void)TopBarButtonClicked:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    int tagval = (int) btn.tag;
    
    if(tagval != selectedButtonNumber)
    {
        switch (tagval)
        {
            case 1: // Coupon
            {
                [self loadCouponViewOnScreen];
            }
            break;
            case 2: // HD
            {
                [self loadHotDealViewOnScreen];
            }
            break;
            case 3: // Specials
            {
                [self loadSpecialRetailerViewOnScreen];
            }
            break;
                
            default:
                break;
        }
    }
    
}

-(void)loadSpecialRetailerViewOnScreen
{
    [self removePreviousViewBeforeLoadNext];
    selectedButtonNumber = 3;
    [btnCoupon setBackgroundColor:[UIColor blackColor]];
    [btnHotDeal setBackgroundColor:[UIColor blackColor]];
    [btnSpecials setBackgroundColor:[UIColor colorWithRed:0.0157 green:0.6745 blue:0.9412 alpha:1.0]];

    if(isSpecialAvailable)
    {
        [lblNonAvailable setHidden:YES];
        float Y = btnSpecials.frame.origin.y + btnSpecials.frame.size.height+0.5;

        iSpecialOfferRetailersViewController = [[SpecialOfferRetailersViewController alloc]initWithNibName:@"SpecialOfferRetailersViewController" bundle:[NSBundle mainBundle]];
        iSpecialOfferRetailersViewController.mViewController = self;
        iSpecialOfferRetailersViewController.view.frame = CGRectMake(0, Y , SCREEN_WIDTH, SCREEN_HEIGHT - Y-61);
        [self.view addSubview:iSpecialOfferRetailersViewController.view];
    }
    else
        [lblNonAvailable setHidden:NO];
}

-(void)loadHotDealViewOnScreen
{
    [self removePreviousViewBeforeLoadNext];
    selectedButtonNumber =2;
    [btnCoupon setBackgroundColor:[UIColor blackColor]];
    [btnHotDeal setBackgroundColor:[UIColor colorWithRed:0.0157 green:0.6745 blue:0.9412 alpha:1.0]];
    [btnSpecials setBackgroundColor:[UIColor blackColor]];
    
    if(isHotDealAvailable)
    {
        [lblNonAvailable setHidden:YES];
        float Y = btnSpecials.frame.origin.y + btnSpecials.frame.size.height+0.5;
        
        [defaults  setBool:YES forKey:@"HotDealsplash"];
        iHotDealsList = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:[NSBundle mainBundle]];
        iHotDealsList.mViewController = self;
        iHotDealsList.view.frame = CGRectMake(0, Y , SCREEN_WIDTH, SCREEN_HEIGHT - Y-61);
        [self.view addSubview:iHotDealsList.view];
    }
    else
        [lblNonAvailable setHidden:NO];
}

-(void)loadCouponViewOnScreen
{
    [self removePreviousViewBeforeLoadNext];
    selectedButtonNumber = 1;
    
    [btnCoupon setBackgroundColor:[UIColor colorWithRed:0.0157 green:0.6745 blue:0.9412 alpha:1.0]];
    [btnHotDeal setBackgroundColor:[UIColor blackColor]];
    [btnSpecials setBackgroundColor:[UIColor blackColor]];

    if(isCouponAvailable)
    {
        [lblNonAvailable setHidden:YES];
        float Y = btnSpecials.frame.origin.y + btnSpecials.frame.size.height+0.5;
        
        [defaults setBool:YES forKey:@"myGallerysplash"];
        [defaults setBool:YES forKey:@"CalledFromMainMenu"];
        
        iCouponsViewController = [[DealCouponsViewController alloc]initWithNibName:@"DealCouponsViewController" bundle:[NSBundle mainBundle]];
        iCouponsViewController.mViewController = self;
        
        iCouponsViewController.view.frame = CGRectMake(0, Y , SCREEN_WIDTH, SCREEN_HEIGHT - Y-61);
        [self.view addSubview:iCouponsViewController.view];
    }
    else
        [lblNonAvailable setHidden:NO];
}

-(void)removePreviousViewBeforeLoadNext
{
    if(iCouponsViewController)
    {
        [iCouponsViewController.view removeFromSuperview];
        [iCouponsViewController release];
        iCouponsViewController = nil;
    }
    
    if(iHotDealsList)
    {
        [iHotDealsList.view removeFromSuperview];
        [iHotDealsList release];
        iHotDealsList = nil;
    }
    if(iSpecialOfferRetailersViewController)
    {
        [iSpecialOfferRetailersViewController.view removeFromSuperview];
        [iSpecialOfferRetailersViewController release];
        iSpecialOfferRetailersViewController = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






@end

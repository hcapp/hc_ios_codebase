//
//  ListCouponCell.h
//  HubCiti
//
//  Created by Lakshmi H R on 11/30/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponsRetailerDetails.h"

@interface ListCouponCell : UITableViewCell


@property (strong, nonatomic) IBOutlet SdImageView *coupon_Image;
@property (strong, nonatomic) IBOutlet UILabel *coupon_Name;
;
@property (strong, nonatomic) IBOutlet UILabel *coupon_Offer_Price;
@property (strong, nonatomic) IBOutlet UILabel *couponLocation_Name;
@property (strong, nonatomic) IBOutlet UILabel *coupon_Location_Distance;
@property (strong, nonatomic) IBOutlet UILabel *coupon_Location_Count;

-(void)updateUiWithRetailerDetails:(CouponsRetailerDetails *)retailerDetails;

@end

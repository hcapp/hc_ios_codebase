//
//  HotDealsList.m
//  Scansee
//
//  Created by Dhananjaya C H on 22/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DealHotDealsList.h"
#import "MainMenuViewController.h"
#import "PreferredCategoriesScreen.h"
#import "WebBrowserViewController.h"
//#import "HotDealDetails.h"
//#import "PopulationCenterCities.h"
#import <CoreLocation/CoreLocation.h>
#import "CLLocation (Strings).h"
#import "GalleryList.h"
#import "QuartzCore/QuartzCore.h"
#import "HotDealDetailViewController.h"
#import "SpecialOffersViewController.h"



#import "NewCouponDetailViewController.h"
#import "ProductPage.h"
#import "RetailerSummaryViewController.h"
//#import "AddRemLoyaltyCardViewController.h"
//#import "MyCouponPopulationCenterCities.h"
#import "GalleryList.h"
#import "DealsViewController.h"
#import "LoginViewController.h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "MainMenuViewController.h"
#import "SingleCatRetailers.h"
#import "EventsListViewController.h"
#import "AlertsList.h"
#import "CityExperienceViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "HubCitiConstants.h"
#import "AppDelegate.h"
#import "FilterRetailersList.h"
#import "FilterListViewController.h"
#import "RetailersListViewController.h"
#import "FundraiserListViewController.h"
#import "FAQCategoryList.h"
#import "LocationDetailsViewController.h"
#import "CouponsViewController.h"
#import "EmailShareViewController.h"
#import "PreferredCategoriesScreen.h"
#import "AnythingPage.h"
#import "SpecialOfferRetailersViewController.h"
#import "GalleryDisplayViewController.h"
#import "UserInformationViewController.h"
#import "UserSettingsController.h"
#import "CityPreferenceViewController.h"
#include "HTTPClient.h"
#include "GetUserInfoResponse.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import <Contacts/Contacts.h>
#import "CouponsMyAccountsViewController.h"
#define LOCAL_DEAL 1
#define NATION_DEAL 2
#define NONE_AVAILABLE 3
#define Button_Height 40


@implementation dHotDealDO
@synthesize apiPartnerId, apiPartnerName;
@synthesize catId, catName;
@synthesize hotDealDetailsObjArray;
@end

@implementation dHotDealDetailsDO
@synthesize hdName, hdSDesc, hdImgPath, hdListId;
@synthesize hdId, hdPrice, hdSalePrice;
@synthesize hdUrl, hdNewFlag;
@end

//static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;

@implementation DealHotDealsList{
    UIActivityIndicatorView *loading;
    bottomButtonView *view;
}
@synthesize infoResponse;
@synthesize m_Button;
@synthesize mViewController;
@synthesize localButton;
@synthesize nationButton,anyVC,emailSendingVC;

static int count = 0;
static int pickerRow = 0;
static int tempLastVisitedRecord = 0;
static int tempCount = 0;

BOOL searchFlag;
BOOL isLocalFlgPagination;

//BOOL isNationFlgPagination;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization.
//		//isFirstButtonClicked = YES;
//    }
//    return self;
//}
#pragma mark - Service Respone methods

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    [self responseData:responseObject];
    
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
     [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    [self.navigationController setNavigationBarHidden:NO ];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    //Activity Indicator
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
    activityIndicator.hidesWhenStopped = YES;
    
    
    
    loading = nil;
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"Deals" forView:self withHambergur:YES];
    //self.title=@"Deals";
    [self.view setAccessibilityLabel:@"hotDealsView"];
    arrBottomButtonImages = [[NSMutableArray alloc]init];
    searchStr = [[NSString alloc]init];
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        //        [arrBottomButtonImages addObject:@"cs_claimed_off"];
        //        [arrBottomButtonImages addObject:@"cs_expired_off"];
        //        [arrBottomButtonImages addObject:@"cs_used_off"];
        //        [arrBottomButtonImages addObject:@"cs_claimed_on"];
        //        [arrBottomButtonImages addObject:@"cs_expired_on"];
        //        [arrBottomButtonImages addObject:@"cs_used_on"];
    }
    else
    {
        //        [arrBottomButtonImages addObject:@"claimed_off_iPad.png"];
        //        [arrBottomButtonImages addObject:@"expired_off_iPad.png"];
        //        [arrBottomButtonImages addObject:@"used_off_iPad.png"];
        //        [arrBottomButtonImages addObject:@"claimed_on_iPad.png"];
        //        [arrBottomButtonImages addObject:@"expired_on_iPad.png"];
        //        [arrBottomButtonImages addObject:@"used_on_iPad.png"];
        
        hotDealFoundTxt.frame = CGRectMake((SCREEN_WIDTH - hotDealFoundTxt.frame.size.width)/2, hotDealFoundTxt.frame.origin.y, hotDealFoundTxt.frame.size.width, hotDealFoundTxt.frame.size.height);
        hotDealFoundTxt.font = [UIFont boldSystemFontOfSize:23];
    }
    
    
    if (IOS7 == NO)
        m_searchBar.tintColor = [UIColor blackColor];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    lastvalueforLoop = 0; //@PaginationChange: Assigned with "0" to start
    categoryIdValue = 0;
    m_Button.hidden = YES;
    hotDealIconImg.hidden = YES;
    hotDealFoundTxt.hidden = YES;
    
    // if (IS_IPHONE5)
    // m_searchBar.frame = CGRectMake(0,-40, self.view.frame.size.width, 44);
    
    m_searchBar.frame = CGRectMake(0,110, self.view.frame.size.width, 44);
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        m_searchBar.frame = CGRectMake(0,110, SCREEN_WIDTH, 44);
        
    }
    searchFlag=FALSE;
    customView.hidden = YES;
    customView.tag = 0;
    [cus setTitle:@"Deals" forView:self withHambergur:YES];
    //self.title = @"Deals";
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBar.hidden = NO;
    
    
    

    //	[byCategory setImage:[UIImage imageNamed:@"tab_btn_down_byctgry.png"] forState:UIControlStateHighlighted];
    //	[galleryBtn setImage:[UIImage imageNamed:@"claimed_on.png"] forState:UIControlStateHighlighted];
    //	[preferenceBtn setImage:[UIImage imageNamed:@"tab_btn_down_preferences.png"] forState:UIControlStateHighlighted];
    
    hotDealObjArray = [[NSMutableArray alloc]init];
    nationDealObjArray = [[NSMutableArray alloc]init];
    [hotDealTable setAccessibilityIdentifier:@"hotDealsTable"];
    if ([defaults  boolForKey:@"HotDealsplash"] == YES) {
        //[self showSplash];
        byCategory.enabled = YES;
        [SharedManager setRefreshCategories:YES];
        [self request_getHotDealProds];
        [defaults  setBool:NO forKey:@"HotDealsplash"];
    }
    //	[self setBottomButtons];
    lastVisitedRecord = 0;
    [self.view addSubview:nationButton];
   
    
}
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
-(void)localTapped
{
    //    if ([self.view.subviews containsObject:hotDealTable]) {
    //        [hotDealTable removeFromSuperview];
    //        ReleaseAndNilify(hotDealTable);
    //    }
    
    
    if ([self.view.subviews containsObject:nationButton]) {
        [nationButton removeFromSuperview];
        
    }
    //
    if ([self.view.subviews containsObject:localButton]) {
        [localButton removeFromSuperview];
        
    }
    
    localButton = [UIButton buttonWithType:UIButtonTypeCustom];
    localButton .frame = CGRectMake(0.0, self.navigationController.navigationBar.frame.size.height, SCREEN_WIDTH, 35.0);
    localButton .backgroundColor = [UIColor clearColor];
    localButton .adjustsImageWhenHighlighted = NO;
    [  localButton  setBackgroundImage:[UIImage imageNamed:@"hotDealsBg.png"] forState:UIControlStateNormal];
    [  localButton  setTitleEdgeInsets:UIEdgeInsetsMake(8.0, 10.0, 10.0, 2.0)];
    [  localButton  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [  localButton .titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0]];
    [  localButton  setTitle:@"Local" forState:UIControlStateNormal];
    localButton .contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [  localButton  addTarget:self action:@selector(localTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [localButton setImage:[UIImage imageNamed:@"arrow_right"] forState:UIControlStateNormal];
    [localButton setImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateSelected];
    [localButton setSelected:YES];
    [localButton setImageEdgeInsets:UIEdgeInsetsMake(10, SCREEN_WIDTH-25, 10, 0)];
    
    
    [self.view addSubview:  localButton ];
    
    
    int yVal=localButton.frame.size.height+localButton.frame.origin.y;
    if(hotDealTable==nil){
        
        if(bottomBtn==1){
            hotDealTable = [[UITableView alloc]initWithFrame:CGRectMake(0,yVal, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight-(Button_Height*2)-26) style:UITableViewStylePlain];
            
        }
        else{
            
            hotDealTable = [[UITableView alloc]initWithFrame:CGRectMake(0,yVal, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-20-40-Button_Height) style:UITableViewStylePlain];   //44
            
            
        }
        hotDealTable.dataSource=self;
        hotDealTable.delegate=self;
        hotDealTable.tag=LOCAL_DEAL;
        [hotDealTable setBackgroundColor:[UIColor whiteColor]];
        hotDealTable.tableFooterView.hidden=YES;
        [self.view addSubview:hotDealTable];
    }
    else
    {
        if(bottomBtn==1){
            
            
            [hotDealTable setFrame:CGRectMake(0,yVal, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight-(Button_Height*2)-26)];
            
            
        }
        else{
            
            [hotDealTable setFrame:CGRectMake(0,yVal, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-20-40-Button_Height)];//44
            
            
        }
        
        hotDealTable.tag=LOCAL_DEAL;
        hotDealTable.hidden=NO;
        [hotDealTable reloadData];
    }
    
    
    //self.navigationController.navigationBar.frame.size.height+localButton.frame.size.height+hotDealTable.frame.size.height
    nationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    nationButton.frame = CGRectMake(0.0, 8+ yVal+hotDealTable.frame.size.height, SCREEN_WIDTH, 35.0);//
    
    nationButton.backgroundColor = [UIColor clearColor];
    nationButton.adjustsImageWhenHighlighted = NO;
    [nationButton setBackgroundImage:[UIImage imageNamed:@"hotDealsBg.png"] forState:UIControlStateNormal];
    // [button1 setImage:[UIImage imageNamed:@"hotDealsBg.png"] forState:UIControlStateNormal];
    [nationButton setTitleEdgeInsets:UIEdgeInsetsMake(8.0, 10.0, 10.0, 2.0)];
    [nationButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nationButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0]];
    [nationButton setTitle:@"Nationwide" forState:UIControlStateNormal];
    nationButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [nationButton addTarget:self action:@selector(nationTapped) forControlEvents:UIControlEventTouchUpInside];
    [nationButton setImage:[UIImage imageNamed:@"arrow_right"] forState:UIControlStateNormal];
    [nationButton setImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateSelected];
    [nationButton setSelected:NO];
    [nationButton setImageEdgeInsets:UIEdgeInsetsMake(10, SCREEN_WIDTH-25, 10, 0)];
    [self.view addSubview:nationButton];
    
    
    
    
    
}

-(void)nationTapped
{
    
    //    if ([self.view.subviews containsObject:hotDealTable]) {
    //        [hotDealTable removeFromSuperview];
    //    }
    
    if ([self.view.subviews containsObject:nationButton]) {
        [nationButton removeFromSuperview];
    }
    
    if (dealTypeCount==2&&[self.view.subviews containsObject:localButton]) {
        [localButton removeFromSuperview];
        ;
    }
    
    
    int yVal;
    
    
    yVal= self.navigationController.navigationBar.frame.size.height;
    localButton = [UIButton buttonWithType:UIButtonTypeCustom];
    localButton .frame = CGRectMake(0.0, yVal, SCREEN_WIDTH, 35.0);
    localButton .backgroundColor = [UIColor clearColor];
    localButton .adjustsImageWhenHighlighted = NO;
    [  localButton  setBackgroundImage:[UIImage imageNamed:@"hotDealsBg.png"] forState:UIControlStateNormal];
    [  localButton  setTitleEdgeInsets:UIEdgeInsetsMake(8.0, 10.0, 10.0, 2.0)];
    [  localButton  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [  localButton .titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0]];
    [  localButton  setTitle:@"Local" forState:UIControlStateNormal];
    localButton .contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [  localButton  addTarget:self action:@selector(localTapped) forControlEvents:UIControlEventTouchUpInside];
    [localButton setImage:[UIImage imageNamed:@"arrow_right"] forState:UIControlStateNormal];
    [localButton setImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateSelected];
    [localButton setSelected:NO];
    [localButton setImageEdgeInsets:UIEdgeInsetsMake(10, SCREEN_WIDTH-25, 10, 0)];
    [self.view addSubview:  localButton ];
    
    yVal=yVal+localButton.frame.size.height;
    
    
    
    nationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nationButton.frame = CGRectMake(0.0,yVal , SCREEN_WIDTH, 35.0);
    nationButton.backgroundColor = [UIColor clearColor];
    nationButton.adjustsImageWhenHighlighted = NO;
    [nationButton setBackgroundImage:[UIImage imageNamed:@"hotDealsBg.png"] forState:UIControlStateNormal];
    // [button1 setImage:[UIImage imageNamed:@"hotDealsBg.png"] forState:UIControlStateNormal];
    [nationButton setTitleEdgeInsets:UIEdgeInsetsMake(8.0, 10.0, 10.0, 2.0)];
    [nationButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nationButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0]];
    [nationButton setTitle:@"Nationwide" forState:UIControlStateNormal];
    nationButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [nationButton addTarget:self action:@selector(nationTapped) forControlEvents:UIControlEventTouchUpInside];
    [nationButton setImage:[UIImage imageNamed:@"arrow_right"] forState:UIControlStateNormal];
    [nationButton setImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateSelected];
    [nationButton setSelected:YES];
    [nationButton setImageEdgeInsets:UIEdgeInsetsMake(10, SCREEN_WIDTH-25, 10, 0)];
    [self.view addSubview:nationButton];
    //
    yVal=yVal+nationButton.frame.size.height;
    
    if( hotDealTable==nil){
        
        if(bottomBtn==1){
            
            hotDealTable= [[UITableView alloc]initWithFrame:CGRectMake(0,yVal, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight-65) style:UITableViewStylePlain];        //(Button_Height*2)+17
            
        }
        else{
            
            hotDealTable= [[UITableView alloc]initWithFrame:CGRectMake(0,yVal, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-20-Button_Height) style:UITableViewStylePlain];     //yVal-44-40
            
            
        }
        
        hotDealTable.dataSource=self;
        hotDealTable.delegate=self;
        hotDealTable.tag=NATION_DEAL;
        [ hotDealTable setBackgroundColor:[UIColor whiteColor]];
        hotDealTable.tableFooterView.hidden=YES;
        [self.view addSubview: hotDealTable];
    }
    else
    {
        if(bottomBtn==1){
            
            [hotDealTable setFrame:CGRectMake(0,yVal, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight-65 )];     //-(Button_Height*2)-26)
            
            
        }
        else{
            
            [hotDealTable setFrame:CGRectMake(0,yVal, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-20-Button_Height)];   //yVal-44-40
            
            
        }
        
        
        hotDealTable.tag=NATION_DEAL;
        hotDealTable.hidden=NO;
        [hotDealTable reloadData];
    }
    
}

-(void)noneAvailable
{
    //    nationButton.hidden=YES;
    //    localButton.hidden=YES;
}

-(void)settableViewOnScreen
{
    if(hotDealTable==nil){
        int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
        if(bottomBtn==1){
            hotDealTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-50) style:UITableViewStylePlain];
        }
        else{
            hotDealTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-44) style:UITableViewStylePlain];
        }
        hotDealTable.dataSource=self;
        hotDealTable.delegate=self;
        [hotDealTable setBackgroundColor:[UIColor whiteColor]];
        hotDealTable.tableFooterView.hidden=YES;
        [self.view addSubview:hotDealTable];
    }
}


-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    //customize back button
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideBackButton:NO];
    [cusNav hideHambergerButton: NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    [self updateViewonViewWillAppear];
}

-(void)updateViewonViewWillAppear
{
    [m_searchBar resignFirstResponder];
    customView.hidden = YES;
    
    //modelView.backgroundColor = [UIColor colorWithRGBInt:0xe8ab29];
    
    if ([SharedManager refreshList] == YES) {
        
        lastVisitedRecord = 0 ;
        categoryIdValue = 0;
        count = 0;
        tempLastVisitedRecord = 0;
        tempCount = 0;
        lastvalueforLoop = 0;
        [self removeAllObjectsinArray];
        [self request_getHotDealProds];
        [hotDealTable setContentOffset:CGPointMake(0, 0) animated:YES];
        [SharedManager setRefreshList:NO];
    }
    
    [self resizeViews];
    
}

-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)resizeViews
{
    m_Button.hidden = YES;
    m_searchBar.frame = CGRectMake(0, 0, SCREEN_WIDTH, 44);
    
    
}



#pragma mark navigation barbuttonitem action
-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

#pragma mark request methods
-(void) request_getHotDealProds{
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    else
    {
        iWebRequestType = dgetHotDealProds;
        if ([defaults boolForKey:@"ViewMore"]) {
            //[HubCitiAppDelegate showActivityIndicator];
        }
        NSMutableString *requestStr = [[NSMutableString alloc] init];
        [requestStr appendFormat:@"<HotDealsListRequest><userId>%@</userId><hubCitiId>%@</hubCitiId><category>%d</category>",[defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID],categoryIdValue];
        
        NSString * searchString = m_searchBar.text;
        searchStr = [searchString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([searchStr length]>0)
            [requestStr appendFormat:@"<searchItem><![CDATA[%@]]></searchItem>", searchStr];
        
        if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE]){
            
            [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
             [defaults  valueForKey:KEY_LATITUDE]];
            
        }
        else if ([[defaults  valueForKey:KEYZIPCODE]length]){
            
            [requestStr appendFormat:@"<zipCode><![CDATA[%@]]></zipCode>", [defaults  valueForKey:KEYZIPCODE]];
        }
        
        if([defaults  valueForKey:KEY_MAINMENUID])
            [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
        
        else if([defaults  valueForKey:KEY_MITEMID])
            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults  objectForKey:KEY_MITEMID]];
        
        else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
        
        
        [requestStr appendFormat:@"<lastVisitedProductNo>%d</lastVisitedProductNo></HotDealsListRequest>",lastVisitedRecord];
        
        NSString *urlString = [NSString stringWithFormat:@"%@hotdeals/gethotdealprods",BASE_URL];
        
        
        if ([defaults boolForKey:@"ViewMore"]) {
            //changes made on june29
            NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
            
            
            [self parse_getHotDealProds:response];
            
        }
        else{
            [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
        }
        
        
        //[ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
        ReleaseAndNilify(requestStr);
    }
}

//get user favorite categories
//-(void)request_GetUserCat{
//
//    iWebRequestType = dgetusercat;
//    if([Network currentReachabilityStatus]==0)
//    {
//        CommonUtility *common = [[CommonUtility alloc]init];
//        [common showAlert];
//        [common release];
//    }
//    else {
//
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//
//    }
//}

//-(IBAction) bannerPressed : (id) sender {
//
//    [self request_GetUserCat];
//}

-(void)requestToGetPreferredLocations
{
    iWebRequestType = dgetfavloc;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}


#pragma mark parse methods
-(void)responseData:(NSString*)response{
    
    switch (iWebRequestType) {
            
        case dgetHotDealProds:
            [self parse_getHotDealProds:response];
            break;
            
        case dhddetail:
            [self parse_HdDetail:response];
            break;
            
        case dgetfavloc:
            [self parse_GetUserCat:response];
            break;
        case dgetfavlocations:
            [self parse_GetUserCat:response];
            break;
        case dgetuserinfo:
        {
            [self parseGetUserData:response];
        }
            break;
            
            
        default:
            break;
    }
    //    [hotDealTable reloadData];
}

-(void) parse_getHotDealProds:(NSString *)response {
    
    
    
    if ([UtilityManager isNullOrEmptyString:response]) {
        byCategory.enabled = NO;
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbXml.rootXMLElement];
    
    if (nextPageElement == nil) {
        
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        
        if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10005"]) {
            
            TBXMLElement *FavCatFlagElement = [TBXML childElementNamed:@"FavCatFlag" parentElement:tbXml.rootXMLElement];
            favCat = [[TBXML textForElement:FavCatFlagElement]intValue];
            [self resizeViews];
            
            byCategory.enabled = YES;
            
            [categoryArray removeAllObjects];
            [categoryId removeAllObjects];
            [categoryName removeAllObjects];
            [hotDealObjArray removeAllObjects];
            
            
            [categoryArray addObject:@"All"];
            [categoryId addObject:@"0"];
            
            pickerRow = 0;
            hotDealFoundTxt.hidden = NO;
            hotDealFoundTxt.text = @"None Available";
            //            TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            //
            //            NSString *responseTextStr = [TBXML textForElement:saveResponseText];
            //            hotDealFoundTxt.text = responseTextStr ;
            
        }
        
        else{ //any server response code other than 10005
            
            byCategory.enabled = NO;
            
            // Banner the message given by the server
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            
            hotDealFoundTxt.hidden = NO;
            hotDealFoundTxt.text = [TBXML textForElement:responseTextElement];
            
            [loading stopAnimating];            //changes made on june29 for view more...
            ReleaseAndNilify(loading);
        }
        
        hotDealIconImg.hidden = NO;
        hotDealTable.hidden = YES;
        [self noneAvailable];
        return;
    }
    
    hotDealIconImg.hidden = YES;
    hotDealFoundTxt.hidden = YES;
    hotDealFoundTxt.text = @"";
    
    
    NSString *nextPageStr = [TBXML textForElement:nextPageElement];
    nextPage = [nextPageStr intValue];
    
    TBXMLElement *maxRowNumElement = [TBXML childElementNamed:@"maxRowNum" parentElement:tbXml.rootXMLElement];
    lastVisitedRecord = [[TBXML textForElement:maxRowNumElement]intValue];
    
    //TBXMLElement *maxCountElement = [TBXML childElementNamed:@"maxCnt" parentElement:tbXml.rootXMLElement];
    
    //@Deepak: Parsed and saved MainMenuID in User Default list for userTracking
    TBXMLElement *mainMenuIDElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbXml.rootXMLElement];
    
    
    if(mainMenuIDElement !=nil)
    {
        [defaults setObject:[TBXML textForElement:mainMenuIDElement] forKey:KEY_MAINMENUID];
    }
    
    TBXMLElement *favCatElement = [TBXML childElementNamed:@"FavCat" parentElement:tbXml.rootXMLElement];
    if (favCatElement != nil) {
        favCat = [[TBXML textForElement:favCatElement] intValue];
        [self resizeViews];
    }
    
    //@PaginationChange: Assigned the i and J value with paginaion variable
    int j =lastvalueforLoop ;
    int i= lastvalueforLoop;
    
    bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbXml.rootXMLElement]]intValue];
    
    dealTypeCount=[[TBXML textForElement:[TBXML childElementNamed:@"dealTypeCount" parentElement:tbXml.rootXMLElement]]intValue];
    
    TBXMLElement *dealTypeListElement = [TBXML childElementNamed:@"dealTypeList" parentElement:tbXml.rootXMLElement];
    
    TBXMLElement *hotDealsListResultSet = [TBXML childElementNamed:@"HotDealsListResultSet" parentElement:dealTypeListElement];
    if (dealTypeCount == 2) {
        isLocalFlgPagination = NO;
    }
    
    while (hotDealsListResultSet) {
        TBXMLElement *dealTypeNameElement = [TBXML childElementNamed:@"dealType" parentElement:hotDealsListResultSet];
        TBXMLElement *hdAPIResultElement = [TBXML childElementNamed:@"hdAPIResult" parentElement:hotDealsListResultSet];
        
        dealType=[[NSMutableString alloc]initWithString:[TBXML textForElement:dealTypeNameElement]];
        
        if ([dealType isEqualToString:@"Local"]) {
            
            if (dealTypeCount == 1) {
                isLocalFlgPagination = YES;
            }
            TBXMLElement *HotDealAPIResultSetElement = [TBXML childElementNamed:@"HotDealAPIResultSet" parentElement:hdAPIResultElement];
            
            
            while (HotDealAPIResultSetElement != nil)
            {
                hotDealTable.hidden = NO;
                
                TBXMLElement *apiNameElement = [TBXML childElementNamed:@"apiPartnerName" parentElement:HotDealAPIResultSetElement];
                
                TBXMLElement *apiIdElement = [TBXML childElementNamed:@"apiPartnerId" parentElement:HotDealAPIResultSetElement];
                
                TBXMLElement *hdCatInfoListElement = [TBXML childElementNamed:@"hdCatInfoList" parentElement:HotDealAPIResultSetElement];
                
                TBXMLElement *HotDealsCategoryInfoElement = [TBXML childElementNamed:@"HotDealsCategoryInfo" parentElement:hdCatInfoListElement];
                
                while (HotDealsCategoryInfoElement != nil) {
                    dHotDealDO *hotDealObj = [[dHotDealDO alloc]init];
                    
                    TBXMLElement *categoryNameElement = [TBXML childElementNamed:@"categoryName" parentElement:HotDealsCategoryInfoElement];
                    TBXMLElement *categoryIdElement = [TBXML childElementNamed:@"categoryId" parentElement:HotDealsCategoryInfoElement];
                    
                    hotDealObj.apiPartnerName = [TBXML textForElement:apiNameElement];
                    hotDealObj.apiPartnerId = [TBXML textForElement:apiIdElement];
                    
                    hotDealObj.catName = [TBXML textForElement:categoryNameElement];
                    if ((![[categoryName lastObject] isEqualToString:@"Uncategorized"]) && (categoryIdElement!= nil)){ //for uncategorized we wont get any categoryId
                        hotDealObj.catId = [TBXML textForElement:categoryIdElement];
                    }
                    else{
                        hotDealObj.catId = @"-1";
                    }
                    
                    //[sectionArry addObject:[NSString stringWithFormat:@"%i",j]];
                    
                    
                    if (![[[hotDealObjArray lastObject]catId] isEqualToString:[TBXML textForElement:categoryIdElement]])
                    {
                        hotDealObj.hotDealDetailsObjArray = [[NSMutableArray alloc]init];
                    }
                    else
                    {
                        // Copy Last Object Array in current AlertEventDO Object "if Same caegory Repeted"
                        dHotDealDO *hotDealObjRep = [hotDealObjArray lastObject];
                        hotDealObj.hotDealDetailsObjArray = [[NSMutableArray alloc]initWithArray:hotDealObjRep.hotDealDetailsObjArray];
                        [hotDealObjArray removeLastObject];
                    }
                    
                    
                    TBXMLElement *hotDealsDetailsArrayLstElement = [TBXML childElementNamed:@"hotDealsDetailsArrayLst" parentElement:HotDealsCategoryInfoElement];
                    
                    if (hotDealsDetailsArrayLstElement != nil) {
                        
                        TBXMLElement *HotDealsDetails = [TBXML childElementNamed:@"HotDealsDetails" parentElement:hotDealsDetailsArrayLstElement];
                        
                        //hotDealObj.hotDealDetailsObjArray = [[NSMutableArray alloc]init];
                        
                        //not to be included in while loop as it has to execute only once - only first city name
                        //                if (HotDealsDetails != nil) {
                        //
                        //                        hotDealFoundTxt.frame = CGRectMake(52, 3, 248, 48);
                        //                        hotDealFoundTxt.font = [UIFont fontWithName:@"Helvetica" size:15.0];
                        //                        hotDealFoundTxt.text = NSLocalizedString(@"Great News!!\nMany Hot Deals Found Around You",@"Great News!!\nMany Hot Deals Found Around You");
                        //                        hotDealIconImg.hidden = NO;
                        //                }
                        
                        while (HotDealsDetails != nil) {
                            
                            dHotDealDetailsDO *hdDetailObj = [[dHotDealDetailsDO alloc]init];
                            TBXMLElement *hotDealNameElement = [TBXML childElementNamed:@"hotDealName" parentElement:HotDealsDetails];
                            TBXMLElement *hotDealIdElement = [TBXML childElementNamed:@"hotDealId" parentElement:HotDealsDetails];
                            TBXMLElement *hotDealImagePathElement = [TBXML childElementNamed:@"hotDealImagePath" parentElement:HotDealsDetails];
                            TBXMLElement *hDshortDescriptionElement = [TBXML childElementNamed:@"hDshortDescription" parentElement:HotDealsDetails];
                            TBXMLElement *hDPriceElement = [TBXML childElementNamed:@"hDPrice" parentElement:HotDealsDetails];
                            TBXMLElement *hDSalePriceElement = [TBXML childElementNamed:@"hDSalePrice" parentElement:HotDealsDetails];
                            TBXMLElement *hDURLElement = [TBXML childElementNamed:@"hdURL" parentElement:HotDealsDetails];
                            TBXMLElement *hnewFlagElement = [TBXML childElementNamed:@"newFlag" parentElement:HotDealsDetails];
                            
                            //@Deepak: HotDeal parsing for UserTracking
                            TBXMLElement *hDealListIDElement = [TBXML childElementNamed:@"hotDealListId" parentElement:HotDealsDetails];
                            if(hDealListIDElement !=nil)
                            {
                                hdDetailObj.hdListId = [TBXML textForElement:hDealListIDElement];
                            }
                            
                            if (hnewFlagElement!= nil)
                                hdDetailObj.hdNewFlag = [TBXML textForElement:hnewFlagElement];
                            
                            if (hotDealNameElement!= nil)
                                hdDetailObj.hdName = [TBXML textForElement:hotDealNameElement];
                            
                            if (hDshortDescriptionElement!= nil)
                                hdDetailObj.hdSDesc = [TBXML textForElement:hDshortDescriptionElement];
                            
                            if (hotDealImagePathElement!= nil)
                                hdDetailObj.hdImgPath = [TBXML textForElement:hotDealImagePathElement];
                            
                            if (hDPriceElement!= nil)
                                hdDetailObj.hdPrice = [TBXML textForElement:hDPriceElement];
                            
                            if (hotDealIdElement!= nil)
                                hdDetailObj.hdId = [TBXML textForElement:hotDealIdElement];
                            
                            if (hDSalePriceElement!= nil)
                                hdDetailObj.hdSalePrice = [TBXML textForElement:hDSalePriceElement];
                            
                            if (hDURLElement!= nil)
                                hdDetailObj.hdUrl = [TBXML textForElement:hDURLElement];
                            
                            HotDealsDetails = [TBXML nextSiblingNamed:@"HotDealsDetails" searchFromElement:HotDealsDetails];
                            
                            [hotDealObj.hotDealDetailsObjArray addObject:hdDetailObj];
                            ReleaseAndNilify(hdDetailObj);
                        }
                    }
                    i++;
                    j++;
                    lastvalueforLoop++; //@PaginationChange: Increment the value with loop
                    HotDealsCategoryInfoElement = [TBXML nextSiblingNamed:@"HotDealsCategoryInfo" searchFromElement:HotDealsCategoryInfoElement];
                    
                    [hotDealObjArray addObject:hotDealObj];
                }
                
                HotDealAPIResultSetElement = [TBXML nextSiblingNamed:@"HotDealAPIResultSet" searchFromElement:HotDealAPIResultSetElement];
                
            }
        }
        else
        {
            
            if (dealTypeCount == 1) {
                isLocalFlgPagination = NO;
            }
            TBXMLElement *HotDealAPIResultSetElement = [TBXML childElementNamed:@"HotDealAPIResultSet" parentElement:hdAPIResultElement];
            while (HotDealAPIResultSetElement != nil)
            {
                hotDealTable.hidden = NO;
                
                TBXMLElement *apiNameElement = [TBXML childElementNamed:@"apiPartnerName" parentElement:HotDealAPIResultSetElement];
                
                TBXMLElement *apiIdElement = [TBXML childElementNamed:@"apiPartnerId" parentElement:HotDealAPIResultSetElement];
                
                TBXMLElement *hdCatInfoListElement = [TBXML childElementNamed:@"hdCatInfoList" parentElement:HotDealAPIResultSetElement];
                
                TBXMLElement *HotDealsCategoryInfoElement = [TBXML childElementNamed:@"HotDealsCategoryInfo" parentElement:hdCatInfoListElement];
                
                while (HotDealsCategoryInfoElement != nil) {
                    dHotDealDO *hotDealObj = [[dHotDealDO alloc]init];
                    
                    TBXMLElement *categoryNameElement = [TBXML childElementNamed:@"categoryName" parentElement:HotDealsCategoryInfoElement];
                    TBXMLElement *categoryIdElement = [TBXML childElementNamed:@"categoryId" parentElement:HotDealsCategoryInfoElement];
                    
                    hotDealObj.apiPartnerName = [TBXML textForElement:apiNameElement];
                    hotDealObj.apiPartnerId = [TBXML textForElement:apiIdElement];
                    
                    hotDealObj.catName = [TBXML textForElement:categoryNameElement];
                    if ((![[categoryName lastObject] isEqualToString:@"Uncategorized"]) && (categoryIdElement!= nil)){ //for uncategorized we wont get any categoryId
                        hotDealObj.catId = [TBXML textForElement:categoryIdElement];
                    }
                    else{
                        hotDealObj.catId = @"-1";
                    }
                    
                    //[sectionArry addObject:[NSString stringWithFormat:@"%i",j]];
                    
                    
                    if (![[[nationDealObjArray lastObject]catId] isEqualToString:[TBXML textForElement:categoryIdElement]])
                    {
                        hotDealObj.hotDealDetailsObjArray = [[NSMutableArray alloc]init];
                    }
                    else
                    {
                        // Copy Last Object Array in current AlertEventDO Object "if Same caegory Repeted"
                        dHotDealDO *hotDealObjRep = [nationDealObjArray lastObject];
                        hotDealObj.hotDealDetailsObjArray = [[NSMutableArray alloc]initWithArray:hotDealObjRep.hotDealDetailsObjArray];
                        [nationDealObjArray removeLastObject];
                    }
                    
                    
                    TBXMLElement *hotDealsDetailsArrayLstElement = [TBXML childElementNamed:@"hotDealsDetailsArrayLst" parentElement:HotDealsCategoryInfoElement];
                    
                    if (hotDealsDetailsArrayLstElement != nil) {
                        
                        TBXMLElement *HotDealsDetails = [TBXML childElementNamed:@"HotDealsDetails" parentElement:hotDealsDetailsArrayLstElement];
                        
                        //hotDealObj.hotDealDetailsObjArray = [[NSMutableArray alloc]init];
                        
                        //not to be included in while loop as it has to execute only once - only first city name
                        //                if (HotDealsDetails != nil) {
                        //
                        //                        hotDealFoundTxt.frame = CGRectMake(52, 3, 248, 48);
                        //                        hotDealFoundTxt.font = [UIFont fontWithName:@"Helvetica" size:15.0];
                        //                        hotDealFoundTxt.text = NSLocalizedString(@"Great News!!\nMany Hot Deals Found Around You",@"Great News!!\nMany Hot Deals Found Around You");
                        //                        hotDealIconImg.hidden = NO;
                        //                }
                        
                        while (HotDealsDetails != nil) {
                            
                            dHotDealDetailsDO *hdDetailObj = [[dHotDealDetailsDO alloc]init];
                            TBXMLElement *hotDealNameElement = [TBXML childElementNamed:@"hotDealName" parentElement:HotDealsDetails];
                            TBXMLElement *hotDealIdElement = [TBXML childElementNamed:@"hotDealId" parentElement:HotDealsDetails];
                            TBXMLElement *hotDealImagePathElement = [TBXML childElementNamed:@"hotDealImagePath" parentElement:HotDealsDetails];
                            TBXMLElement *hDshortDescriptionElement = [TBXML childElementNamed:@"hDshortDescription" parentElement:HotDealsDetails];
                            TBXMLElement *hDPriceElement = [TBXML childElementNamed:@"hDPrice" parentElement:HotDealsDetails];
                            TBXMLElement *hDSalePriceElement = [TBXML childElementNamed:@"hDSalePrice" parentElement:HotDealsDetails];
                            TBXMLElement *hDURLElement = [TBXML childElementNamed:@"hdURL" parentElement:HotDealsDetails];
                            TBXMLElement *hnewFlagElement = [TBXML childElementNamed:@"newFlag" parentElement:HotDealsDetails];
                            
                            //@Deepak: HotDeal parsing for UserTracking
                            TBXMLElement *hDealListIDElement = [TBXML childElementNamed:@"hotDealListId" parentElement:HotDealsDetails];
                            if(hDealListIDElement !=nil)
                            {
                                hdDetailObj.hdListId = [TBXML textForElement:hDealListIDElement];
                            }
                            
                            if (hnewFlagElement!= nil)
                                hdDetailObj.hdNewFlag = [TBXML textForElement:hnewFlagElement];
                            
                            if (hotDealNameElement!= nil)
                                hdDetailObj.hdName = [TBXML textForElement:hotDealNameElement];
                            
                            if (hDshortDescriptionElement!= nil)
                                hdDetailObj.hdSDesc = [TBXML textForElement:hDshortDescriptionElement];
                            
                            if (hotDealImagePathElement!= nil)
                                hdDetailObj.hdImgPath = [TBXML textForElement:hotDealImagePathElement];
                            
                            if (hDPriceElement!= nil)
                                hdDetailObj.hdPrice = [TBXML textForElement:hDPriceElement];
                            
                            if (hotDealIdElement!= nil)
                                hdDetailObj.hdId = [TBXML textForElement:hotDealIdElement];
                            
                            if (hDSalePriceElement!= nil)
                                hdDetailObj.hdSalePrice = [TBXML textForElement:hDSalePriceElement];
                            
                            if (hDURLElement!= nil)
                                hdDetailObj.hdUrl = [TBXML textForElement:hDURLElement];
                            
                            HotDealsDetails = [TBXML nextSiblingNamed:@"HotDealsDetails" searchFromElement:HotDealsDetails];
                            
                            [hotDealObj.hotDealDetailsObjArray addObject:hdDetailObj];
                            ReleaseAndNilify(hdDetailObj);
                        }
                    }
                    i++;
                    j++;
                    lastvalueforLoop++; //@PaginationChange: Increment the value with loop
                    HotDealsCategoryInfoElement = [TBXML nextSiblingNamed:@"HotDealsCategoryInfo" searchFromElement:HotDealsCategoryInfoElement];
                    
                    [nationDealObjArray addObject:hotDealObj];
                }
                
                HotDealAPIResultSetElement = [TBXML nextSiblingNamed:@"HotDealAPIResultSet" searchFromElement:HotDealAPIResultSetElement];
                
            }
            
        }
        
        hotDealsListResultSet = [TBXML nextSiblingNamed:@"HotDealsListResultSet" searchFromElement:hotDealsListResultSet];
        //        hotDealsListResultSet =[ TBXML nextchildElementNamed:@"ListResultSet" parentElement:dealTypeListElement];
    }
    
    
    
    
    
    
    
    if ([SharedManager refreshCategories] == YES && [categoryName count]) {
        
        byCategory.enabled = YES;
        [categoryArray removeAllObjects];
        [categoryId removeAllObjects];
        
        //Add "All" as a default category
        [categoryArray addObject:@"All"];
        [categoryId addObject:@"0"];
        
        // Parse the list of category from the response and add in the "categoryArray" list to display as byCategory
        TBXMLElement *categoryElement = [TBXML childElementNamed:@"category" parentElement:tbXml.rootXMLElement];
        if(categoryElement!=nil)
        {
            TBXMLElement *categoryElementName = [TBXML childElementNamed:@"Category" parentElement:categoryElement];
            while(categoryElementName!=nil)
            {
                TBXMLElement *CatNameElement = [TBXML childElementNamed:@"category" parentElement:categoryElementName];
                TBXMLElement *CatIdElement = [TBXML childElementNamed:@"catId" parentElement:categoryElementName];
                
                if(CatNameElement!=nil)
                    [categoryArray addObject:[TBXML textForElement:CatNameElement]];
                
                if(CatIdElement!=nil)
                    [categoryId addObject:[TBXML textForElement:CatIdElement]];
                
                categoryElementName = [TBXML nextSiblingNamed:@"Category" searchFromElement:categoryElementName];
            }
        }
        
        pickerRow = 0;
        [data_picker reloadAllComponents];
        [data_picker selectRow:pickerRow inComponent:0 animated:NO];
    }
    else if([categoryName count] == 0)
        byCategory.enabled = NO;
    
    if (searchFlag==FALSE) {
        //   NSString *maxCount=[[NSString alloc]initWithString:[TBXML textForElement:maxCountElement]];
        
        //        if ([maxCount isEqualToString:@"1"]) {
        //            maxCountFlag=TRUE;
        //            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        //            [self tableView:hotDealTable didSelectRowAtIndexPath:indexPath];
        //
        //    }
        
    }
    if (bottomBtn==1) {
        TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"arBottomBtnList" parentElement:tbXml.rootXMLElement];
        TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
        arrEventBottomButtonDO =[[NSMutableArray alloc]init];
        while(BottomButtonElement)
        {
            
            bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
            
            
            TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
            TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
            TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
            TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
            TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
            TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
            TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
            TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
            
            
            if(bottomBtnIDElement)
                obj_findBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
            
            if(bottomBtnNameElement)
                obj_findBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
            else
                obj_findBottomDO.bottomBtnName = @" ";
            
            if(bottomBtnImgElement)
                obj_findBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
            else
                obj_findBottomDO.bottomBtnImg = @" ";
            
            if(bottomBtnImgOffElement)
                obj_findBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
            else
                obj_findBottomDO.bottomBtnImgOff = @" ";
            
            if(btnLinkTypeIDElement)
                obj_findBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
            
            if(btnLinkTypeNameElement)
                obj_findBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
            
            if(btnLinkIDElement){
                obj_findBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                
                if(btnLinkTypeNameElement){
                    if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
                        [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
                }
            }
            
            if(positionElement)
                obj_findBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
            
            
            
            BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
            [arrEventBottomButtonDO addObject:obj_findBottomDO];
            //[obj_findBottomDO release];
        }
        if([arrEventBottomButtonDO count] > 0)
        {
            // Set Bottom Menu Button after read from response data
            [self setBottomBarMenu];
            [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
        }
        else
            [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
    }
    //     [self settableViewOnScreen];
    if ([defaults boolForKey:@"ViewMore"]) {
        if ([hotDealObjArray count]>0 && hotDealTable.tag==1) {
            [self localTapped];
        }
        else if ([nationDealObjArray count]>0 && hotDealTable.tag==2)
        {
            [self nationTapped];
        }
        else
        {
            [self noneAvailable];
        }
        [activityIndicator stopAnimating];
    }
    else
    {
        if ([hotDealObjArray count]>0 ) {
            [self localTapped];
        }
        else if ([nationDealObjArray count]>0)
        {
            [self nationTapped];
        }
        else
        {
            [self noneAvailable];
        }
    }
    
    [hotDealTable reloadData];
    nationButton.enabled=YES;
    nationButton.userInteractionEnabled=YES;
    localButton.enabled=YES;
    localButton.userInteractionEnabled=YES;
    
    [defaults setBool:NO forKey:@"ViewMore"];
}

-(void)parse_HdDetail:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    HotDealDetailViewController *hdd = [[HotDealDetailViewController alloc] initWithNibName:@"HotDealDetailViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:hdd animated:NO];
    //  [hdd release];
}

//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

#pragma mark pickerView delegate methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return [categoryArray count];
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if ([categoryArray count] >0) {
        
        categoryValue = [categoryArray objectAtIndex:row];
        //categoryIdValue = [[categoryId objectAtIndex:row] intValue];
        pickerRow = (int)row;
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *label = [[UILabel alloc] init];
    
    if (IOS7){
        label.frame = CGRectMake(20, 0, 220, 40);
        // pickerView.backgroundColor = [UIColor colorWithRGBInt:0xEEEEEE];
        pickerView.layer.borderWidth = 5.0;
        pickerView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    }
    
    else
        label.frame = CGRectMake(0, 0, 200, 40);
    
    [label setText:[NSString stringWithFormat:@"%@", [categoryArray objectAtIndex:row]]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setNumberOfLines:2];
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    [label setTextAlignment:NSTextAlignmentLeft];
    return label ;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 60.0;
}
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

#pragma mark splash methods
-(void)showSplash
{
    UIViewController *modalViewController = [[UIViewController alloc] init];
    modalViewController.view = modelView;
    modelView.backgroundColor = [UIColor colorWithRGBInt:0xf2b32e];
    [self presentViewController:modalViewController animated:NO completion:nil];
    //[modalViewController release];
    [self performSelector:@selector(hideSplash) withObject:nil afterDelay:1.0];
}

- (void)hideSplash{
    
    [[self presentedViewController] dismissViewControllerAnimated:NO completion:nil];
    byCategory.enabled = YES;
    [SharedManager setRefreshCategories:YES];
    [self request_getHotDealProds];
    
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{//chnages made on june 29
    index = indexPath;
    dHotDealDO *hotDealObj;
    
    if (tableView.tag==LOCAL_DEAL) {
        hotDealObj= [hotDealObjArray objectAtIndex:indexPath.section];
    }
    else if(tableView.tag==NATION_DEAL)
    {
        hotDealObj= [nationDealObjArray  objectAtIndex:indexPath.section];
    }
    
    if (indexPath.section == ([hotDealObjArray count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count] && tableView.tag==LOCAL_DEAL)
    {
        if ([hotDealObjArray count] >1) {
            if (isLocalFlgPagination == YES && nextPage == 1) {
                if (![defaults boolForKey:@"ViewMore"] && lastVisitedRecord!=0) {
                    m_searchBar.userInteractionEnabled = NO;
                    [defaults setBool:YES forKey:@"ViewMore"];
                    //            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
                    //            dispatch_async(dispatchQueue, ^(void){
                    [self viewMoreResults];
                    // If cliecked "View More Result request to load next result
                    //                dispatch_async(dispatch_get_main_queue(), ^{
                    //                    [hotDealTable reloadData];
                    // isLocalFlgPagination = NO;
                    //[HubCitiAppDelegate removeActivityIndicator];
                    m_searchBar.userInteractionEnabled = YES;
                    //                });
                    //            });
                    //
                }
            }
        }
    }
    else if(indexPath.section == ([nationDealObjArray count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count] && tableView.tag==NATION_DEAL)
    {
        if ([nationDealObjArray count]>1  && nextPage == 1) {
            if (![defaults boolForKey:@"ViewMore"] && lastVisitedRecord!=0) {
                m_searchBar.userInteractionEnabled = NO;
                [defaults setBool:YES forKey:@"ViewMore"];
                
                //            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
                //            dispatch_async(dispatchQueue, ^(void){
                
                [self viewMoreResults]; // If cliecked "View More Result request to load next result
                //                dispatch_async(dispatch_get_main_queue(), ^{
                //[hotDealTable reloadData];
                // isNationFlgPagination = NO;
                m_searchBar.userInteractionEnabled = YES;
                //                });
                //            });
                
            }
        }
        
    }
    
}


#pragma mark Table view delegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    index = indexPath;
    dHotDealDO *hotDealObj = nil;
    [defaults setBool:NO forKey:@"ViewMore"];   //changes made on june29
    
    
    if (tableView.tag==LOCAL_DEAL) {
        hotDealObj= [hotDealObjArray objectAtIndex:indexPath.section];
    }
    else if(tableView.tag==NATION_DEAL)
    {
        hotDealObj= [nationDealObjArray  objectAtIndex:indexPath.section];
    }
    
    
    //    if (indexPath.section == ([hotDealObjArray count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count] && tableView.tag==NATION_DEAL){
    //
    //		[self viewMoreResults]; // If cliecked "View More Result request to load next result
    //	}
    //    else if(indexPath.section == ([nationDealObjArray count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count] && tableView.tag==NATION_DEAL)
    //    {
    //        [self viewMoreResults];
    //    }
    
    // else {
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    
    else {
        
        if([hotDealObj.apiPartnerId isEqualToString:@"4"])
            //If selected cell is dealmap
        {
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            [defaults setValue:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdUrl] forKey:KEY_URL];
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
            
        }
        else
        {
            [defaults setObject:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdId] forKey:@"HotDealId"];
            [defaults setObject:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdName] forKey:@"HotDealProductName"];
            [defaults setObject:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdUrl] forKey:KEY_URL];
            [defaults setObject:hotDealObj.apiPartnerName forKey:@"apiName"];
            
            //@Deepak: adding hotDealListID to use in getDeal Button click for usertracking
            [defaults  setObject:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdListId] forKey:@"hotDealListID"];
            [defaults setObject:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdName] forKey:@"HotDealName"];
            [defaults setObject:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdSalePrice] forKey:@"saleProductName"];
            
            iWebRequestType = dhddetail;
            NSString *urlString = [NSString stringWithFormat:@"%@hotdeals/hddetail",BASE_URL];
            NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<HotDealsDetails><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
            [reqStr appendFormat:@"<hotDealId>%@</hotDealId><hotDealListId>%@</hotDealListId></HotDealsDetails>",[defaults valueForKey:@"HotDealId"] ,[defaults valueForKey:@"hotDealListID"]];
            
            [ConnectionManager establishConnectionFor:reqStr base:urlString withDelegate:self];
            ReleaseAndNilify(reqStr);
        }
        [hotDealTable deselectRowAtIndexPath:[hotDealTable indexPathForSelectedRow] animated:YES];
    }
    //}
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView.tag==LOCAL_DEAL) {
        return [hotDealObjArray count];
    }
    else if(tableView.tag==NATION_DEAL)
    {
        return [nationDealObjArray count];
    }
    else
    {
        return 1;
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView.tag==LOCAL_DEAL) {
        dHotDealDO *hotDealObj = [hotDealObjArray objectAtIndex:section];
        
        if (nextPage == 1 && section == ([hotDealObjArray count] - 1))
            return ([hotDealObj.hotDealDetailsObjArray count] + 1);
        else
            return [hotDealObj.hotDealDetailsObjArray count];
    }
    else if(tableView.tag==NATION_DEAL)
    {
        dHotDealDO *hotDealObj = [nationDealObjArray objectAtIndex:section];
        
        if (nextPage == 1 && section == ([nationDealObjArray count] - 1))
            return ([hotDealObj.hotDealDetailsObjArray count] + 1);
        else
            return [hotDealObj.hotDealDetailsObjArray count];
    }
    else
    {
        return 1;
    }
    
    
    
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell ; //= [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    dHotDealDO *hotDealObj;
    if (tableView.tag==LOCAL_DEAL) {
        hotDealObj = [hotDealObjArray objectAtIndex:indexPath.section];
    }
    else if(tableView.tag==NATION_DEAL)
    {
        hotDealObj = [nationDealObjArray objectAtIndex:indexPath.section];
    }
    
    if (tableView.tag==LOCAL_DEAL||tableView.tag==NATION_DEAL) {
        //normal flow
        if (indexPath.section == ([hotDealObjArray count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count] && tableView.tag==LOCAL_DEAL){
            if ([hotDealObjArray count] > 1) {
                
                
                if (isLocalFlgPagination == YES && nextPage == 1) {
                    //isLocalFlgPagination = YES;
                    loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    
                    loading.center = cell.contentView.center;
                    loading.color = [UIColor blackColor];
                    
                    
                    [cell.contentView addSubview:loading];
                    [loading startAnimating];
                    cell.userInteractionEnabled = NO;
                    //            CGRect frame;
                    //            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    //                frame.origin.x = 0;
                    //                frame.origin.y = 10;
                    //                frame.size.width = 320;
                    //                frame.size.height = 24;
                    //            }
                    //            else
                    //            {
                    //                frame.origin.x = 0;
                    //                frame.origin.y = 10;
                    //                frame.size.width = SCREEN_WIDTH;
                    //                frame.size.height = 40;
                    //            }
                    //
                    //            UILabel *label = [[UILabel alloc] initWithFrame:frame];
                    //            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    //                label.font = [UIFont boldSystemFontOfSize:16];
                    //            }
                    //            else
                    //            {
                    //                 label.font = [UIFont boldSystemFontOfSize:20];
                    //            }
                    //
                    //            label.textAlignment = NSTextAlignmentCenter;
                    //            label.textColor = [UIColor colorWithRGBInt:0x112e72];
                    //            label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
                    //            [cell.contentView addSubview:label];
                    //            ReleaseAndNilify(label);
                }
            }
            
            return cell;
        }
        else if(indexPath.section == ([nationDealObjArray count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count] && tableView.tag==NATION_DEAL)
        {
            if([nationDealObjArray count] > 1){
                if (nextPage == 1 ) {
                    //isNationFlgPagination = YES;
                    loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    
                    loading.center = cell.contentView.center;
                    loading.color = [UIColor blackColor];
                    
                    
                    [cell.contentView addSubview:loading];
                    [loading startAnimating];
                    [hotDealTable addSubview:activityIndicator];
                    [activityIndicator startAnimating];
                    cell.userInteractionEnabled = NO;
                    //            CGRect frame;
                    //            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    //                frame.origin.x = 0;
                    //                frame.origin.y = 10;
                    //                frame.size.width = 320;
                    //                frame.size.height = 24;
                    //            }
                    //            else
                    //            {
                    //                frame.origin.x = 0;
                    //                frame.origin.y = 10;
                    //                frame.size.width = SCREEN_WIDTH;
                    //                frame.size.height = 40;
                    //            }
                    //
                    //            UILabel *label = [[UILabel alloc] initWithFrame:frame];
                    //            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    //                label.font = [UIFont boldSystemFontOfSize:16];
                    //            }
                    //            else
                    //            {
                    //                label.font = [UIFont boldSystemFontOfSize:20];
                    //            }
                    //
                    //            label.textAlignment = NSTextAlignmentCenter;
                    //            label.textColor = [UIColor colorWithRGBInt:0x112e72];
                    //            label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
                    //            [cell.contentView addSubview:label];
                    //            ReleaseAndNilify(label);
                }
            }
            return cell;
        }
        
        else {
            UILabel *hdNameLabel;
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                hdNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(75.0, 3, 230.0, 50.0)];
                hdNameLabel.font = [UIFont boldSystemFontOfSize:13];
            }
            else
            {
                hdNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(120.0, 10, SCREEN_WIDTH-120, 70.0)];
                hdNameLabel.font = [UIFont boldSystemFontOfSize:18];
            }
            
            
            
            if ([hotDealObjArray count] == 0 && tableView.tag==LOCAL_DEAL) {
                cell.textLabel.text=@"NONE AVALABLE";
                return cell;
            }
            else if([nationDealObjArray count]==0 && tableView.tag==NATION_DEAL)
            {
                cell.textLabel.text=@"NONE AVALABLE";
                return cell;
                
            }
            
            hdNameLabel.text = [[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdName];
            hdNameLabel.numberOfLines = 3;
            [hdNameLabel setLineBreakMode:NSLineBreakByWordWrapping];
            hdNameLabel.textColor = [UIColor colorWithRGBInt:0x112e72];
            
            hdNameLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:hdNameLabel];
            ReleaseAndNilify(hdNameLabel);
            
            NSString *salePriceStr = [[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdSalePrice];
            if (![salePriceStr isEqualToString:@"N/A"] && ![salePriceStr isEqualToString:@"$0.00"])
            {
                
                UILabel *salePrice;
                if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
                    salePrice= [[UILabel alloc] initWithFrame:CGRectMake(170, 55, 150, 30)];
                    salePrice.font = [UIFont boldSystemFontOfSize:13];
                }
                else
                {
                    salePrice= [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-350, 65, 250, 50)];
                    salePrice.font = [UIFont boldSystemFontOfSize:18];
                }
                
                salePrice.text = [NSString stringWithFormat:NSLocalizedString(@"Sale Price %@ ",@"Sale Price %@ "),salePriceStr];
                salePrice.textColor = [UIColor redColor];
                
                [cell.contentView addSubview:salePrice];
                ReleaseAndNilify(salePrice);
            }
            
            NSString *imgPath = [[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdImgPath];
            if (![imgPath isEqualToString:@"N/A"])
            {
                SdImageView *asyncImageView ;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    asyncImageView= [[SdImageView alloc] initWithFrame:CGRectMake(5, 13, 60, 60)];
                }
                else
                {
                    asyncImageView= [[SdImageView alloc] initWithFrame:CGRectMake(10, 15, 90, 90)];
                }
                
                
                asyncImageView.backgroundColor = [UIColor clearColor];
                [asyncImageView loadImage:imgPath];
                [cell addSubview:asyncImageView];
                ReleaseAndNilify(asyncImageView);
            }
            
            // Display New Image Icon for the New Hot Deal
            if ([[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdNewFlag] isEqualToString:@"1"])
            {
                AsyncImageView *asyncImageView ;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    asyncImageView= [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
                }
                else
                {
                    asyncImageView= [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 50)];
                }
                
                asyncImageView.backgroundColor = [UIColor clearColor];
                [asyncImageView loadImagefrommainBundel:@"new_ribbon_70.png"];
                [cell addSubview:asyncImageView];
                ReleaseAndNilify(asyncImageView);
            }
            cell.backgroundColor = [UIColor whiteColor];
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            
            NSString *priceStr = [[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdPrice];
            if (![priceStr isEqualToString:@"N/A"] && ![priceStr isEqualToString:@"$0.00"])
            {
                UILabel *priceLabel ;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    priceLabel= [[UILabel alloc] initWithFrame:CGRectMake(75, 55, 90, 30)];
                    priceLabel.font = [UIFont boldSystemFontOfSize:13];
                }
                else
                {
                    priceLabel= [[UILabel alloc] initWithFrame:CGRectMake(120, 65, 90, 50)];
                    priceLabel.font = [UIFont boldSystemFontOfSize:18];
                }
                
                priceLabel.text = priceStr;
                priceLabel.textColor = [UIColor darkGrayColor];
                
                priceLabel.backgroundColor = [UIColor clearColor];
                [cell addSubview:priceLabel];
                ReleaseAndNilify(priceLabel);
            }
            return cell;
        }
    }
    else
    {
        cell.textLabel.text=@"NONE AVALABLE";
        return cell;
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        dHotDealDO *hotDealObj;
        if (tableView.tag==LOCAL_DEAL) {
            hotDealObj= [hotDealObjArray objectAtIndex:indexPath.section];
            if (indexPath.section == ([hotDealObjArray count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count] && nextPage == 1){
                return 60;
            }
            return 90;
        }
        else if (tableView.tag==NATION_DEAL)
        {
            hotDealObj= [nationDealObjArray objectAtIndex:indexPath.section];
            if (indexPath.section == ([nationDealObjArray  count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count] && nextPage == 1){
                return 60;
            }
            return 90;
        }
        else
        {
            return 60;
        }
        
        
    }
    else
    {
        dHotDealDO *hotDealObj;
        if (tableView.tag==LOCAL_DEAL) {
            hotDealObj = [hotDealObjArray objectAtIndex:indexPath.section];
            if (indexPath.section == ([hotDealObjArray count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count] && nextPage == 1){
                return 90;
            }
            return 120;
            
        }
        else if(tableView.tag==NATION_DEAL)
        {
            hotDealObj = [nationDealObjArray objectAtIndex:indexPath.section];
            if (indexPath.section == ([nationDealObjArray count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count] && nextPage == 1){
                return 90;
            }
            return 120;
            
        }
        else
        {
            return 60;
        }
        
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        if (tableView.tag==LOCAL_DEAL) {
            if ([hotDealObjArray count] == 0 && (nextPage == 1 || lastVisitedRecord!=0)) {
                return 0.0;
            }
            return 60;
        }
        else
        {
            if ([nationDealObjArray count] == 0 && (nextPage == 1 || lastVisitedRecord!=0)) {
                return 0.0;
            }
            return 60;
        }
        
    }
    else
    {
        if (tableView.tag==LOCAL_DEAL) {
            if ([hotDealObjArray count] == 0 && (nextPage == 1 || lastVisitedRecord!=0)) {
                return 0.0;
            }
            return 60;
        }
        else
        {
            if ([nationDealObjArray count] == 0 && (nextPage == 1 || lastVisitedRecord!=0)) {
                return 0.0;
            }
            return 60;
        }
        
        
        
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (tableView.tag==LOCAL_DEAL) {
        if ([hotDealObjArray count] == 0 && (nextPage == 1 || lastVisitedRecord!=0)) {
            return NULL;
        }
    }
    else
    {
        if ([nationDealObjArray count] == 0 && (nextPage == 1 || lastVisitedRecord!=0)) {
            return NULL;
        }
    }
    dHotDealDO *hdObj;
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH, 26)] ;
    UILabel *catNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, SCREEN_WIDTH, 20)] ;
    CGRect imageRect1 ;
    
    if (tableView.tag==LOCAL_DEAL) {
        hdObj= [hotDealObjArray objectAtIndex:section];
    }
    else
    {
        hdObj= [nationDealObjArray objectAtIndex:section];
    }
    
    
    
    {
        
        {
            
            headerView.frame = CGRectMake(10, 0, SCREEN_WIDTH, 60) ;
            imageRect1 = CGRectMake(0.0f, 30.0f, SCREEN_WIDTH, 30.0f);
            
            UILabel *apiNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, SCREEN_WIDTH, 20)] ;
            //DLog(@"category = %@  ,section array = %@",categoryName ,sectionArry );
            apiNameLabel.text = [NSString stringWithFormat:@"%@",hdObj.apiPartnerName];
            apiNameLabel.textColor = [UIColor whiteColor];
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                apiNameLabel.font = [UIFont boldSystemFontOfSize:16];
            }
            else
            {
                apiNameLabel.font = [UIFont boldSystemFontOfSize:20];
            }
            
            apiNameLabel.backgroundColor = [UIColor clearColor];
            
            CGRect imageRect = CGRectMake(0.0f, 0.0f, SCREEN_WIDTH, 30.0f);
            UIImageView *headerImage = [[UIImageView alloc] initWithFrame:imageRect];
            UIImage *image = [UIImage imageNamed:@"hotDealsBg.png"];
            [headerImage setImage:image];
            headerImage.opaque = YES;
            [headerImage addSubview:apiNameLabel];
            
            [headerView addSubview:headerImage];
            //[headerImage release];
        }
    }
    
    catNameLabel.text = [NSString stringWithFormat:@"%@",hdObj.catName];
    catNameLabel.textColor = [UIColor whiteColor];
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        catNameLabel.font = [UIFont boldSystemFontOfSize:14];
    }
    else
    {
        catNameLabel.font = [UIFont boldSystemFontOfSize:19];
    }
    
    catNameLabel.backgroundColor = [UIColor lightGrayColor];
    
    UIImageView *headerImage1 = [[UIImageView alloc] initWithFrame:imageRect1];
    headerImage1.backgroundColor = [UIColor lightGrayColor];
    headerImage1.opaque = YES;
    [headerImage1 addSubview:catNameLabel];
    
    [headerView addSubview:headerImage1];
    //[headerImage1 release];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    //	[super setEditing:YES animated:animated];
    
    /*   if (index.row == [rowNumberArray count])
     [hotDealTable setEditing:NO animated:YES];
     else
     [hotDealTable setEditing:editing animated:YES]; */
    
    dHotDealDO *hotDealObj = [hotDealObjArray objectAtIndex:index.section];
    if ((index.section == [hotDealObjArray count] -1) && (index.row == [hotDealObj.hotDealDetailsObjArray count])) {
        [hotDealTable setEditing:NO animated:YES];
    }
    else
        [hotDealTable setEditing:YES animated:YES];
}

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    int sections = (int)[tableView numberOfSections];
    
    int rows = 0;
    
    for(int i=0; i < sections; i++)
    {
        rows += [tableView numberOfRowsInSection:i];
        DLog(@"%d",rows);
    }
    DLog(@"%d",rows);
    DLog(@"%ld",(long)indexPath.row);
    
    dHotDealDO *hotDealObj = [hotDealObjArray objectAtIndex:indexPath.section];
    
    if((indexPath.section == [hotDealObjArray count] -1) && (indexPath.row == [hotDealObj.hotDealDetailsObjArray count])) {
        // any row that returns UITableViewCellEditingStyleNone will NOT support delete (in your case, the first row is returning this)
        return UITableViewCellEditingStyleNone;
    }
    
    // any row that returns UITableViewCellEditingStyleDelete will support delete (in your case, all but the first row is returning this)
    return UITableViewCellEditingStyleDelete;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    /*if (index.row == [rowNumberArray count]) {
     return;
     }  */
    
    dHotDealDO *hotDealObj = [hotDealObjArray objectAtIndex:indexPath.section];
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSMutableString *xmlString = [[NSMutableString alloc] init];
        [xmlString appendFormat:@"<HotDealsListRequest><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        [xmlString appendFormat:@"<hotDealId>%@</hotDealId><hDInterested>0</hDInterested></HotDealsListRequest>",[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdId]];
        
        NSString *urlString = [NSString stringWithFormat:@"%@hotdeals/removehdprod",BASE_URL];
        
        NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:xmlString];
        
        //  [xmlString release];
        
        if ([UtilityManager isNullOrEmptyString:responseXml]){
            return;
        }
        
        TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
        
        TBXMLElement *RTE = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        TBXMLElement *RCE = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        
        if([[TBXML textForElement:RCE] isEqualToString:@"10000"]){
            
            [hotDealObj.hotDealDetailsObjArray removeObjectAtIndex:indexPath.row];
            
            if([hotDealObj.hotDealDetailsObjArray count]<=0)
            {
                [hotDealObjArray removeObjectAtIndex:indexPath.section];
            }
            
            //            [hotDealTable reloadData];
        }
        else {
            NSString *resTxtStr = [TBXML textForElement:RTE];
            
            [UtilityManager showAlert:nil msg:resTxtStr];
            return;
            
        }
    }
}

-(IBAction) categorybtnPressed : (id) sender {
    
    //if(favCat==1)
    //  m_Button.frame = CGRectMake(0, m_searchBar.frame.origin.y , m_Button.frame.size.width, m_Button.frame.size.height);
    
    [m_searchBar resignFirstResponder];
    
    if (customView.tag == 0) {
        customView.tag = 1 ;
        customView.hidden = NO;
    }else {
        customView.hidden = !customView.hidden ;
    }
    customView.tag = 1 ;
    [data_picker reloadAllComponents];
    [data_picker selectRow:pickerRow inComponent:0 animated:NO];
}


-(IBAction) galleryPressed : (id) sender
{
    /* GalleryList *galleryList = [[GalleryList alloc] initWithNibName:@"GalleryList" bundle:[NSBundle mainBundle]];
     [self.navigationController pushViewController:galleryList animated:NO];
     [galleryList release];*/
}

#pragma mark searchBar delegate methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    searchFlag=TRUE;
    lastvalueforLoop = 0;
    [searchBar resignFirstResponder];
    lastVisitedRecord = 0;
    count = 0;
    tempLastVisitedRecord = 0;
    tempCount = 0;
    
    [SharedManager setRefreshCategories:YES];
    [self removeAllObjectsinArray];
    [self request_getHotDealProds];
    m_searchBar.text = @"";
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar;{
    searchFlag=FALSE;
    [searchBar resignFirstResponder];
}

-(IBAction)doneClicked : (id) sender {
    
    if ([categoryArray count]>1)
        [SharedManager setRefreshCategories:NO];
    else
        [SharedManager setRefreshCategories:YES];
    //lastVisitedRecord = 0;
    customView.hidden = YES ;
    
    //count = 0;
    lastvalueforLoop = 0;
    DLog(@"%d,%@",pickerRow,[categoryId description]);
    categoryIdValue = [[categoryId objectAtIndex:pickerRow]intValue];
    
    // Removed all Objects, apart from Catagoryarray and catId
    [categoryName removeAllObjects];
    //[sectionArry removeAllObjects];
    [categoryIdHotdeals removeAllObjects];
    
    //remove all the objects from main array
    [hotDealObjArray removeAllObjects];
    [nationDealObjArray removeAllObjects];
    
    
    if (!(categoryIdValue == 0)) {
        lastVisitedRecord = 0;
        count = 0;
    }
    else{
        lastVisitedRecord = tempLastVisitedRecord;
        count = tempCount;
    }
    
    [self request_getHotDealProds];
}

//-(IBAction) favCategories : (id) sender {
//
//    [self request_GetUserCat];
//}


-(void)viewMoreResults{
    nationButton.enabled=NO;
    nationButton.userInteractionEnabled=NO;
    localButton.enabled=NO;
    localButton.userInteractionEnabled=NO;
    [SharedManager setRefreshCategories:YES];
    
    //means if the category is All then only capture the LVR and count value of it
    if (categoryIdValue == 0){
        tempLastVisitedRecord = lastVisitedRecord;
        tempCount = count;
    }
    
    [self request_getHotDealProds];
    
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void) removeAllObjectsinArray {
    [categoryId removeAllObjects];
    [categoryArray removeAllObjects];
    [categoryName removeAllObjects];
    //[sectionArry removeAllObjects];
    [categoryIdHotdeals removeAllObjects];
    [hotDealObjArray removeAllObjects];
    [nationDealObjArray removeAllObjects];
}

-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}



-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    navigatedFromMainMenu=false;
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            [defaults  setBool:NO forKey:@"showBackButton"];
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}


-(void)navigateToFindView
{
    [defaults  setBool:NO forKey:@"findsplash"];
    [defaults setBool:YES forKey:BottomButton];
    //[defaults setValue:@"0" forKey:KEY_BOTTOMBUTTONID];
    //[defaults setValue:nil forKey:KEY_MITEMID];
    navigatedFromCity=true;
    //[FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    //[self navigateToFindView];
    navigatedFromMainMenu=false;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}


-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}



-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}


//-(void)request_GetUserCat{
//
//    iWebRequestState = GETFAVCATEGORIES;
//    if([Network currentReachabilityStatus]==0)
//    {
//        [common showAlert];
//    }
//    else {
//
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//
//    }
//}

-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=TRUE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

//#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        return;
    
    [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    [self showActionSheet];
    
}
-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
    }
}


-(void) textClicked{//text
    if ([MFMessageComposeViewController canSendText]) {
        
        MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
        [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK],[defaults valueForKey:@"androidDownloadLink"]]];
        smsComposerController.messageComposeDelegate = self;
        [self presentViewController:smsComposerController animated:YES completion:nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
}

-(void) emailClick{//email
    if([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusDenied){
    //if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied) {
        [UtilityManager showAlert:nil msg:NSLocalizedString(@"Please change the contacts privacy setting for App",@"Please change the contacts privacy setting for App")];
    }
    else{
        
        emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
        __typeof(self) __weak  obj = self;
        emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
            
            [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
        } ;
        
        [emailSendingVC loadMail];     }
}

-(void) showActionSheet {
   
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}




#pragma mark parse methods



-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            // NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
            }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage =[[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}


// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    // Calculate Y position for View
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-55, SCREEN_WIDTH, 55)];
        tabImage.image = [UIImage imageNamed:@"BlankBottomButton.png"];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
        
    }
    
    for(int btnLoop= 0; btnLoop < [arrEventBottomButtonDO count]; btnLoop++)
    {
        bottomButtonDO *obj_eventBottomDO = [arrEventBottomButtonDO objectAtIndex:btnLoop];
        
        view = [[bottomButtonView alloc]init];
        
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, (SCREEN_HEIGHT-yVal-55) + 4, 80,55) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrEventBottomButtonDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrEventBottomButtonDO count],bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_eventBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        //view.tag=99;
        [self.view bringSubviewToFront:view];
    }
}


-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}



-(void)bottomButtonPressed:(id)sender
{
    
    [defaults setBool:NO forKey:@"ViewMore"];
    [defaults setBool:YES forKey:BottomButton];
    //    [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    bottomButtonDO *obj_eventBottomDO = [arrEventBottomButtonDO objectAtIndex:tag];
    
    [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_eventBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_eventBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_eventBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_eventBottomDO.btnLinkTypeName);
    if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    else if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_eventBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_eventBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            self.iEventsListViewController = iEventsListViewController;
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //  //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    else{ //other menu types
        
        switch ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
                //                [defaults  setBool:YES forKey:@"HotDealsplash"];
                //                HotDealsList *hotDeals = [[HotDealsList alloc]initWithNibName:@"HotDealsList" bundle:nil];
                //                [self.navigationController pushViewController:hotDeals animated:NO];
                //                [hotDeals release];
                break;
                
            case 1://Scan Now
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3:{//Events
                [EventsBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
                self.iEventsListViewController = iEventsListViewController;
                [self.navigationController pushViewController:iEventsListViewController animated:NO];
                //  //[iEventsListViewController release];;
            }
                break;
            case 4://Whats NearBy
                [NearbyBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                [self navigateToWhatsNearBy];
                break;
                
            case 5://Find
                [FindBottomButtonID addObject:obj_eventBottomDO.bottomBtnID];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                [self navigateToFindView];
                break;
                
            case 6:{//City Experience
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                self.cityExp = citi;
                [self.navigationController pushViewController:citi animated:NO];
                //[citi release];
            }
                break;
                
            case 7://City Services
                
                break;
                
            case 8://Visitors Bureau
                
                break;
                
            case 9://transportation
                
                break;
                
            case 10://Preference
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
                break;
                
            case 13:
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=FALSE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_eventBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    self.cityExp = cevc;
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    self.pvc = filters;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    self.filterList = filterList;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                //                DealsViewController *dvc = [[DealsViewController alloc]initWithNibName:@"DealsViewController" bundle:[NSBundle mainBundle]];
                //                [self.navigationController pushViewController:dvc animated:NO];
                //                [dvc release];
                //
                //                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                self.faqList = faqCatList;
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                [FundraiserBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }
                break;
                
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    iWebRequestType = dgetuserinfo;
                    [self requestToGetUserData];
                }
            }break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    iWebRequestType = dgetfavlocations;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!")  msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        //[inform release];
                    }
                    
                }
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            //  [citPref release];
                        }
                    }
                }
                
            }
                break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
               // privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
                
            default:
                break;
        }
    }
    //NSlog(@"%d",tag);
}

-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
}
-(void)parseGetUserData:(id)response
{
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}


-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}


@end

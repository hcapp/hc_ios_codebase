//
//  CouponsMapViewController.m
//  HubCiti
//
//  Created by Lakshmi H R on 12/29/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "CouponsMapViewController.h"
#import "RetailerAnnotation.h"
#import "CouponMapDetails.h"
#import "CouponMapResponse.h"
#import "AppDelegate.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "NewCouponDetailViewController.h"
#import "CouponsRetailerDetails.h"
@interface CouponsMapViewController ()<HTTPClientDelegate,MKMapViewDelegate,CustomizedNavControllerDelegate>
{
    NSMutableArray *mapDetailsArray,*annArray;
    WebRequestState iWebRequestState;
}
@property (strong,nonatomic) CouponMapResponse *couponMapResponse;
@property (strong,nonatomic) CouponMapDetails *couponMapDetails;
@end

@implementation CouponsMapViewController
@synthesize globalRetailerService,showMapFlag,couponMapResponse,couponMapDetails,couponIds,coponmappostalcode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - Service Respone methods
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription] ];
    
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    DLog(@"parameter: %@",responseObject);
    
    [self responseData:responseObject];
    
    
}

-(void)responseData:(id ) response
{
    switch (iWebRequestState)
    {
            
            case COUPON_MAP:
            [self parseMapListing:response];
            break;
               default:
            break;
            
            
            
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    
    annArray = [[NSMutableArray alloc] init];
    //customized home button navigation bar
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = mainPage;
    cusNav =(CustomizedNavController *) self.navigationController;
    [cusNav setTitle:@"Choose your location:" forView:self withHambergur:YES];
    [self requestForMapLocation:couponIds];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}
- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    
    for (NSOperation *operation in [HTTPClient sharedHTTPClient].manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        NSLog(@"operation:%@",operation);
        AFHTTPRequestOperation *operations = (AFHTTPRequestOperation*)operation;
        if([[[[operations request] URL] absoluteString] containsString:@"gallery/getcouponsmaplocjson"]){
            [operation cancel];
        }
        
    }
}
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)showMap
{
    
    mapView = [[MKMapView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
   
    
    mapView.hidden = NO;
    
    
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    [mapView setDelegate:self];
    for(int i= 0; i<couponMapResponse.couponMapLocs.count;i++)
    {
        CouponMapDetails *couponMapDetailsToAdd = [[CouponMapDetails alloc] init];
        
        
        NSDictionary *dictList = couponMapResponse.couponMapLocs[i];
        
        
        @try{
            [couponMapDetailsToAdd setValuesForKeysWithDictionary:dictList];
        }
        @catch (NSException *exception) {
            // Do nothing
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
            return;
        }
        RetailerAnnotation *ann = [[RetailerAnnotation alloc] init];
        ann.coordinate = CLLocationCoordinate2DMake([[couponMapDetailsToAdd retLatitude] doubleValue],
                                                    [[couponMapDetailsToAdd retLongitude] doubleValue]);
        [mapView addAnnotation:ann];
        [annArray addObject:ann];
        [ann setTitle:[couponMapDetailsToAdd couponName]];
        [ann setSubtitle:[couponMapDetailsToAdd location]];
        [ann setIdString:[NSString stringWithFormat:@"%@",[couponMapDetailsToAdd couponId]]];
        
        ReleaseAndNilify(ann);


    }
    
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in mapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
   
    UIEdgeInsets insets = UIEdgeInsetsMake(100, 50, 80, 50);
    
    
    MKMapRect biggerRect = [mapView mapRectThatFits:zoomRect edgePadding:insets];
    

    [mapView setVisibleMapRect:biggerRect animated:YES];
    
    [self.view addSubview:mapView];
}

-(void)returnToMainPage:(id)sender
{
    
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    MKPinAnnotationView* pinView = nil;
    
    if ([annotation isKindOfClass:[RetailerAnnotation class]])
    {
        // try to dequeue an existing pin view first
        static NSString* RetailerAnnotationIdentifier = @"retailerAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:RetailerAnnotationIdentifier];
        
        if (!pinView)
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
                                                  initWithAnnotation:annotation reuseIdentifier:RetailerAnnotationIdentifier] ;
            //customPinView.pinColor = MKPinAnnotationColorPurple;
            customPinView.animatesDrop = YES;
            
            customPinView.canShowCallout = YES;
            
            UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            
            customPinView.rightCalloutAccessoryView = infoButton;
            
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
    }
    pinView.canShowCallout = YES;
    return pinView;
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    
    for(int i= 0; i<couponMapResponse.couponMapLocs.count;i++)
    {
        
            CouponMapDetails *couponMapDetailsToAdd = [[CouponMapDetails alloc] init];
            
            
            NSDictionary *dictList = couponMapResponse.couponMapLocs[i];
            
            
            @try{
                [couponMapDetailsToAdd setCouponDetailsWithDictionary:dictList];
            }
            @catch (NSException *exception) {
                // Do nothing
                [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
                return;
            }
            
        
        RetailerAnnotation *couponAnnotation = (RetailerAnnotation *)view.annotation;

        if ([[couponMapDetailsToAdd couponId] isEqualToString: [couponAnnotation idString]])
        {
            NewCouponDetailViewController *couponDetailVC = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
            [defaults setValue:couponMapDetailsToAdd.couponId forKey:@"couponId"];
            couponDetailVC.isFromMapScreen = TRUE;
            //[defaults setValue:couponMapDetailsToAdd.couponListId forKey:@"couponListId"];
            [self.navigationController pushViewController:couponDetailVC animated:NO];
            return;
        }
        
    }
}
-(void)requestForMapLocation:(NSString *)couponIdToSend

{
    showMapFlag=TRUE;
    iWebRequestState = COUPON_MAP;
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    if([defaults valueForKey:KEY_USERID])
    {
        [parameter setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if([defaults valueForKey:KEY_HUBCITIID])
    {
        [parameter setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length])
    {
        [parameter setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        [parameter setValue:[defaults valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        
    }
    else  if ([[defaults valueForKey:KEYZIPCODE] length])
    {
        [parameter setValue:[defaults valueForKey:KEYZIPCODE]forKey:@"postalCode"];
        
    }
    else if ([coponmappostalcode length]){
        
        [parameter setValue:coponmappostalcode forKey:@"postalCode"];
    }
    if(couponIds.length > 0)
    {
        [parameter setValue:couponIds forKey:@"couponIds"];
    }
    NSString *urlString = [NSString stringWithFormat:@"%@gallery/getcouponsmaplocjson",BASE_URL];
    //NSString *urlString = [NSString stringWithFormat:@"http://10.10.221.155:9990/HubCiti2.8.2/gallery/getcouponsmaplocjson"];
    NSLog(@"parameter: %@", parameter);
    HTTPClient *client = [[HTTPClient alloc] init];
    client.delegate = self;
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameter : urlString];
    [HubCitiAppDelegate showActivityIndicator];
}

-(void)parseMapListing:(id)responseString
{
    [HubCitiAppDelegate removeActivityIndicator];
    if(responseString == NULL)
        return;
    
    if(couponMapResponse == NULL)
    {
        couponMapResponse = [[CouponMapResponse alloc]init];
    }
    
    @try{
        [couponMapResponse setValuesForKeysWithDictionary:responseString];
    }
    
    
    @catch (NSException *exception) {
        // Do nothing
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
        return;
    }
    
    NSLog(@"CouponsResponse: %@",couponMapResponse);
    
    if([couponMapResponse.responseCode isEqualToString:@"10000"])
    {
        
        if(couponMapResponse.couponMapLocs)
        {
            mapDetailsArray = [[NSMutableArray alloc]init];
            for(int l=0; l < couponMapResponse.couponMapLocs.count;l++)
            {
                CouponMapDetails *couponMapDetailsToAdd = [[CouponMapDetails alloc] init];
                
                
                NSDictionary *dictList = couponMapResponse.couponMapLocs[l];
                
                
                @try{
                    [couponMapDetailsToAdd setValuesForKeysWithDictionary:dictList];
                }
                @catch (NSException *exception) {
                    // Do nothing
                    [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
                    return;
                }
                
                //[couponMapDetailsToAdd setValuesForKeysWithDictionary:dictList];
                [mapDetailsArray addObject:couponMapDetailsToAdd];
                
            }
        }
        
        if(mapDetailsArray.count > 0)
        {
            [self showMap];
        }
        else
        {
            [UtilityManager showAlert:nil msg:NSLocalizedString(@"Deals are not available to plot on map",@"Deals are not available to plot on map")];
        }
        
    }
    else
    {
        [UtilityManager showAlert:nil msg:NSLocalizedString(@"Deals are not available to plot on map",@"Deals are not available to plot on map")];
    }

    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

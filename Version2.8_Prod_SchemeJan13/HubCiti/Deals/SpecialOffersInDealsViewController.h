//
//  SpecialOffersInDealsViewController.h
//  HubCiti
//
//  Created by Lakshmi H R on 12/1/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SortAndFilter.h"
#include "AnyViewController.h"

@class EmailShareViewController;

@interface SpecialOffersInDealsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (strong, nonatomic) IBOutlet UISearchBar *specialOfferSearch;
@property (strong, nonatomic) IBOutlet UITableView *specialOfferTable;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableBottomConstraint;
@property(nonatomic,strong) SortAndFilter *sortObj,*savedSortObj;
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@end

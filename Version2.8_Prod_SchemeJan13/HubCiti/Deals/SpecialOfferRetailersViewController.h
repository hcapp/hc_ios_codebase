//
//  SpecialOfferRetailersViewController.h
//  HubCiti
//
//  Created by deepak.agarwal on 1/23/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
BOOL directFlag;
@class RetailersListViewController;
@class FilterRetailersList;
@class CityExperienceViewController;
@class FilterListViewController;
@class FundraiserListViewController;
@class EventsListViewController;
@class FAQCategoryList;
@class AnyViewController;
@class EmailShareViewController;
@interface SpecialOfferRetailersViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>
{
    //IBOutlet UITableView *tblSORetailers;
     UITableView *tblSORetailers;
    int lastVisitedRecord;
    UIViewController *mViewController;
    BOOL isNextPageAvailable;
    WebRequestState iWebRequestState;
    NSMutableArray *arrSpecialOfferRetailer, *arrRetSplOffer;
    CommonUtility *common;
    NSMutableArray *arrBottomButtonViewContainer;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) FAQCategoryList *faqList;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,strong) EventsListViewController* iEventsListViewController;
@property(nonatomic,strong) FilterListViewController *filterList;
@property(nonatomic,strong) FundraiserListViewController *iFundraiserListViewController;
@property(nonatomic,strong) CityExperienceViewController *cityExp;
@property(nonatomic,strong) FilterRetailersList *filters;
@property(nonatomic,strong)UIViewController *mViewController;
@property (nonatomic,strong)NSMutableArray *arrSpecialOfferRetailer;
@property(nonatomic,strong) RetailersListViewController* retailerListScreen;
@property (nonatomic,strong)NSMutableArray *arrEventBottomButtonDO;
@property (nonatomic)BOOL isNextPageAvailable;
@property (nonatomic)int lastVisitedRecord;
-(void)updateViewonViewWillAppear;
@end

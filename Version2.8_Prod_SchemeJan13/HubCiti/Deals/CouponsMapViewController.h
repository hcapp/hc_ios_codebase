//
//  CouponsMapViewController.h
//  HubCiti
//
//  Created by Lakshmi H R on 12/29/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomizedNavController.h"
#import "Mapkit/MKMapView.h"

@interface CouponsMapViewController : UIViewController
{
    CustomizedNavController * cusNav;
    MKMapView *mapView;
}
@property (nonatomic)BOOL showMapFlag;
@property (nonatomic,strong) NSMutableArray *globalRetailerService;
@property (nonatomic,strong) NSString *couponIds,*coponmappostalcode;
@end

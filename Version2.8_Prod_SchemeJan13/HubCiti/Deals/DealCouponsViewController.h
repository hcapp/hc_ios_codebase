//
//  CouponsViewController.h
//  Scansee
//
//  Created by ajit on 9/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"
#import "HubCitiSegmentedControl.h"
#import <MessageUI/MessageUI.h>
#include "GetUserInfoResponse.h"
@class EventsListViewController;
@class FundraiserListViewController;
@class FilterListViewController;
@class FAQCategoryList;
@class FilterRetailersList;
@class CityExperienceViewController;
@class RetailersListViewController;
@class AnyViewController;
@class EmailShareViewController;
//#import "MyCouponPopulationCenterCities.h"

BOOL dealCouponsCountFlag;
BOOL localProductFlag;

typedef enum DealCouponViewServices
{
    DGETALLCOUPBYPROD,
    DGETALLCOUPBYLOC,
    DGETUSERCATCOUPON,
    GETFAVLOCATIONSS,
    GETUSERINFORMATION,
    HubcitiAnythingInfos,
    appsitedetailss
    //addRemoveCoupon
}dealcouponviewservices;


@interface DealCouponsViewController : UIViewController<UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate,UITextFieldDelegate,MFMessageComposeViewControllerDelegate, HTTPClientDelegate,CustomizedNavControllerDelegate>
{
    id objDeals;
    
    UIViewController *mViewController;
    dealcouponviewservices iWebrequest;
    UISearchBar *searchBar;
    
    WebRequestState iWebRequestState;
    
	IBOutlet UIView *modelView;
	IBOutlet UITableView *myGalleryTV;
    IBOutlet UIImageView *scanManBackground;
	
	IBOutlet UIButton *couponGalleryBtn , *bycityBtn ,*searchBtn ,*preferanceBtn ;
    UIButton *addRemGalley;
  	int lowerLimitvalue;
    UIAlertAction* zipSave;
    HubCitiSegmentedControl *segmentedControl;
   // MyCouponPopulationCenterCities *objMyCouponPopulationCenterCities;
    
    //search related variables
    UIView *searchView;    
    IBOutlet UIButton *radioBtn1;
    IBOutlet UIButton *radioBtn2;
    IBOutlet UIButton *catBtn;
    IBOutlet UILabel *catLabel;
    
    IBOutlet UIButton *retBtn;
    IBOutlet UILabel *retLabel;
    
    IBOutlet UITextField *searchTextField;
    IBOutlet UIPickerView *catPicker;
    IBOutlet UIToolbar *toolBar;
    int nextpageFlag;
    //IBOutlet UILabel *visitAllCoupons;//to be hidden in case of Expired and All
    IBOutlet UILabel *available;
    
    NSMutableArray *arrBusinessCatagoriesID;
    NSMutableArray *arrBusinessCatagoriesName;
    
    NSMutableArray *arrBusinessCatagoriesRetailerID;
    NSMutableArray *arrBusinessCatagoriesRetailerName;
    
    BOOL isLocationRadioButtonEnable;

    //Data set for storing the Product information
    NSMutableArray *arrCatagories;  
    NSMutableDictionary *dicProductsCoupon;
    
    //Data set for storing the Location information
    NSMutableDictionary *dicLocationCupon;
    NSMutableDictionary *dicLocationCategories;
    NSMutableArray *arrLocationRetailers;
    NSMutableArray *arrLocationCategoriesforRetailers;
    NSMutableArray *arrSectionArrayForRetailers;
    
    BOOL isCalledFromSegementChange;
    
    int selectedbusinesscatID;
    int selectedbusinesscatRetailerID;
    
    int indexpathRowCouponList;
    int indexpathSectionCouponList;

    BOOL isProductTabDisplaying; // If this is true that means currently active tab or displaying data is "Products"
    BOOL isDisplayBusinessCatagory; // In serach to identify if BusinessCategory is selected then only will dipsplay the retailers
    
    NSMutableDictionary *dictCoupon;
    
    NSMutableArray *arrEventBottomButtonDO;
    NSMutableArray *arrBottomButtonViewContainer;
    
    int bottomBtn;
     CommonUtility *common;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,strong) CityExperienceViewController *cityExp;
@property(nonatomic,strong) FilterRetailersList *pvc;
@property(nonatomic,strong) RetailersListViewController *retailerListScreen;
@property(nonatomic,strong) FAQCategoryList *faqList;
@property(nonatomic,strong) FilterListViewController *filterList;
@property(nonatomic,strong) FundraiserListViewController* iFundraiserListViewController;
@property(nonatomic,strong) EventsListViewController *iEventsListViewController;
@property (nonatomic,strong) IBOutlet UIView *searchView;
@property (nonatomic,strong)id objDeals;
@property (nonatomic,strong) UIViewController *mViewController;
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
-(void)returnToMainPage:(id)sender;
-(void)showSplash;
-(void)hideSplash;
-(void)segmentChanged;
-(IBAction)searchClicked:(id)sender;
-(IBAction)selectCategory:(id)sender;
-(IBAction)selectRetailer:(id)sender;
-(IBAction)toolbarDoneClicked:(id)sender;
-(IBAction)closeSearchView :(id)sender;
-(IBAction)radioButtonTapped:(id)sender;
-(void)updateViewonViewWillAppear;
@end



//
//  CouponViewCell.h
//  HubCiti
//
//  Created by Lakshmi H R on 11/24/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponsRetailerDetails.h"

@interface CouponViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet SdImageView *coupon_Image;
@property (strong, nonatomic) IBOutlet UILabel *couponName;
@property (strong, nonatomic) IBOutlet UILabel *couponOffPrice;
@property (strong, nonatomic) IBOutlet UILabel *couponLocName;
@property (strong, nonatomic) IBOutlet UILabel *couponNearDistance;
@property (strong, nonatomic) IBOutlet UILabel *couponNoLocation;
@property (strong, nonatomic) IBOutlet UILabel *viewMoreLabel;
-(void)updateUiWithRetailerDetails:(CouponsRetailerDetails *)retailerDetails;

@end

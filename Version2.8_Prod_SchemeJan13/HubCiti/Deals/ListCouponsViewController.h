//
//  ListCouponsViewController.h
//  HubCiti
//
//  Created by Lakshmi H R on 11/30/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomizedNavController.h"
#import "AnyViewController.h"
#import "SortAndFilter.h"
#import "EmailShareViewController.h"
#import "CouponsResponse.h"
#import "CouponsRetailerDetails.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponMyACcounts.h"
#import "MyAccountDetails.h"

@interface ListCouponsViewController : UIViewController<HTTPClientDelegate>{
      CustomizedNavController * cusNav;
    
    NSMutableArray *couponsObjectArray;
    NSMutableArray *couponLatArray,*couponLongArray,*couponAddArray,*couponNameArray,*annArray;
    
    NSMutableArray *globalRetailerService;
    NSTimer * timer;
    BOOL isRetailerEvent;
}

@property (strong, nonatomic) IBOutlet UITableView *listCouponTable;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *couponTableBottomConst;
@property(nonatomic,strong) SortAndFilter *sortObj,*savedSortObj;
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarOutlet;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (strong,nonatomic) CouponsResponse *couponsResponse;
@property (strong,nonatomic) CouponsRetailerDetails *couponRetailerDetails;
@property (strong,nonatomic) NSString *featuredOrNonFeatured;
@property(nonatomic,strong) NSString * retailId;
@property (nonatomic,strong) CouponMyACcounts *couponsMyAccResponse;
@property(nonatomic)BOOL fromMyAccounts;
@property (nonatomic)BOOL fromCoupons;
@property(nonatomic,strong) NSString * searchTextFromPreviousScreen,*couponsPostalCodeList;

@end

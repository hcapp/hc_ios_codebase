//
//  NativeSpecialOfferViewController.h
//  HubCiti
//
//  Created by Keshava on 6/3/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>

@interface NativeSpecialOfferViewController : UIViewController<UIWebViewDelegate,MFMailComposeViewControllerDelegate,UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,MFMessageComposeViewControllerDelegate>
{
   
    WebRequestState iWebRequestState;
}
// Special Offer Deatil Page Properties
@property (strong,nonatomic) UILabel *titleLbl;
@property (strong,nonatomic) UIImageView *specialOfferImage;
@property (strong,nonatomic) UILabel *dateLbl;
@property (strong,nonatomic) UILabel *promoLbl;
@property (strong,nonatomic) UIWebView *shortDesc;
@property (strong,nonatomic) UIWebView *LongDesc;
@property (strong,nonatomic) UIButton *locationBtn;
@property (strong,nonatomic) UIButton *cancelBtn;
//Table View that lists the Locations where the special Offers are available
@property (strong,nonatomic) UITableView *locationsTable;

//Used to control the table view behaviour
@property (strong,nonatomic) NSMutableDictionary *specialDetails;
@property (strong,nonatomic) NSMutableArray *tableDetails;

//Loads the special offer details if it is a web link
@property (retain, nonatomic) IBOutlet UIWebView *splOfferView;


@end

//
//  CouponsSwipeCell.m
//  HubCiti
//
//  Created by Lakshmi H R on 11/24/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "CouponsSwipeCell.h"
#import "CouponViewCell.h"
#import "CollectionFooterReusableView.h"
#import "NewCouponDetailViewController.h"
#import "CouponsInDealViewController.h"
#import "ListCouponsViewController.h"
@interface CouponsSwipeCell ()
{
    BOOL scrollLeft;
    NSInteger numberOfCouponItemsInSection;
}
@end

@implementation CouponsSwipeCell
@synthesize swipeCouponsList,couponSection,parentViewController,sectionTitleLabel,fromCoup,fromMyAcc,sortObj;

- (void)awakeFromNib {
    [super awakeFromNib];
    [_couponCollView registerNib:[UINib nibWithNibName:@"CouponViewCell" bundle:nil] forCellWithReuseIdentifier:@"couponCell"];
    
    _couponCollView.dataSource = self;
    _couponCollView.delegate = self;
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//CollectionView Delegates
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if(swipeCouponsList.count <= 10){
        numberOfCouponItemsInSection = swipeCouponsList.count ;
    }
    else
    {
        numberOfCouponItemsInSection = 11;
    }

    return numberOfCouponItemsInSection;

}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CouponViewCell *cell = nil;
    static NSString *cellIdentifier = @"couponCell";
    
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
       NSLog(@"indexpaths: %ld",[collectionView numberOfItemsInSection:indexPath.section] - 1);
    if(indexPath.row == numberOfCouponItemsInSection - 1 && indexPath.row != 0  &&!(swipeCouponsList.count <= 10))
    {
        
        [cell updateUiWithRetailerDetails:nil];
        
    }
    
    else
    {
        
        NSLog(@"Image dimension: %f * %f",cell.coupon_Image.frame.size.width,cell.coupon_Image.frame.size.height);
        
        if(swipeCouponsList.count > 0 && swipeCouponsList.count != indexPath.row)
        {
            if(indexPath.row == 0){
                if(couponSection == 0)
                    [defaults setValue:[[swipeCouponsList objectAtIndex:indexPath.row] couponId] forKey:@"section0"];
                else if(couponSection == 1)
                    [defaults setValue:[[swipeCouponsList objectAtIndex:indexPath.row] couponId] forKey:@"section1"];
                else if(couponSection == 2)
                    [defaults setValue:[[swipeCouponsList objectAtIndex:indexPath.row] couponId] forKey:@"section2"];
                
                NSLog(@"section0%@   section1 %@  section2 %@",[defaults valueForKey:@"section0"],[defaults valueForKey:@"section1"],[defaults valueForKey:@"section2"]);
            }

            CouponsRetailerDetails *cellCoupon ;
            cellCoupon = [swipeCouponsList objectAtIndex:indexPath.row];
            [cell updateUiWithRetailerDetails:cellCoupon];
            
            
        }
        
    }
    
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    CGSize size = CGSizeMake(SCREEN_WIDTH - 40, collectionView.frame.size.height);
    
    return size;
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (scrollView == _couponCollView)
    {
        CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView.superview];
        
        if(translation.x > 0)
        {
            scrollLeft = TRUE;
            // react to dragging left
        } else
        {
            scrollLeft = FALSE;
            // react to dragging right
        }
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == _couponCollView) {
        
//        NSIndexPath *indexPath;
//        CGPoint currentCellOffset = _couponCollView.contentOffset;
//        NSLog(@"CollectionView frame : %f",_couponCollView.frame.size.width);
//        //currentCellOffset.x += SCREEN_WIDTH - 8;
////        if (fmod(_couponCollView.frame.size.width *1/2, 1.0) != 0.0)
////        {
////            currentCellOffset.x += _couponCollView.frame.size.width ;
////        }
////        else
//        {
//            currentCellOffset.x += _couponCollView.frame.size.width * 3/4 ;
//        }
//        NSLog(@"currentCellOffset = %f",currentCellOffset.x);
//        //scrollView.contentOffset = currentCellOffset;
//        indexPath = [_couponCollView indexPathForItemAtPoint:currentCellOffset];
//        if(indexPath.row == 3 && scrollRight)
//        {
//            currentCellOffset.x += SCREEN_WIDTH - 300  ;
//            indexPath = [_couponCollView indexPathForItemAtPoint:currentCellOffset];
//            scrollRight = FALSE;
//        }
//       [_couponCollView scrollToItemAtIndexPath:indexPath
//                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
//                                            animated:YES];
        
        
        float pageWidth = (float)self.couponCollView.bounds.size.width;
        int minSpace = 10;
        NSInteger cellToSwipe;
        //NSInteger nine = 9;
        NSLog(@"Number index = %f",floorf((scrollView.contentOffset.x)/(pageWidth + minSpace) + 0.5));
        if(scrollLeft == TRUE)
        {
            cellToSwipe = (scrollView.contentOffset.x)/(pageWidth + minSpace) + 0.5;
        }
        else
        {
            cellToSwipe = (scrollView.contentOffset.x)/(pageWidth + minSpace) + 1;
        }
//        if(((floorf(scrollView.contentOffset.x)/(pageWidth + minSpace) + 0.5)) == nine)
//        {
//            cellToSwipe = (scrollView.contentOffset.x)/(pageWidth + minSpace) + 1.0;
//        }
//        else
//        {
//            cellToSwipe = (scrollView.contentOffset.x)/(pageWidth + minSpace) + 0.5;
//        }// cell width + min spacing for lines
        if (cellToSwipe < 0) {
            cellToSwipe = 0;
        } else if (cellToSwipe > numberOfCouponItemsInSection) {
            cellToSwipe = numberOfCouponItemsInSection ;
        }
        if(cellToSwipe < 11)
        {
            [self.couponCollView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:cellToSwipe inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
        else
        {
            [self.couponCollView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:cellToSwipe +1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
        
        
        
        
        for (CouponViewCell *cell in [_couponCollView visibleCells]) {
            NSIndexPath *indexPath = [_couponCollView indexPathForCell:cell];
            if(couponSection == 0)
            [defaults setValue:[[swipeCouponsList objectAtIndex:indexPath.row ] couponId] forKey:@"section0"];
            else if(couponSection == 1)
                [defaults setValue:[[swipeCouponsList objectAtIndex:indexPath.row ] couponId] forKey:@"section1"];
            else if(couponSection == 2)
              [defaults setValue:[[swipeCouponsList objectAtIndex:indexPath.row ] couponId] forKey:@"section2"];  NSLog(@"CollectionViewIndexPath%@   section %ld",indexPath,couponSection);
            
            NSLog(@"section0%@   section1 %@  section2 %@",[defaults valueForKey:@"section0"],[defaults valueForKey:@"section1"],[defaults valueForKey:@"section2"]);
        }
    }
}

//- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
//    *targetContentOffset = scrollView.contentOffset; // set acceleration to 0.0
//    float pageWidth = (float)self.couponCollView.bounds.size.width;
//    int minSpace = 10;
//    
//    NSInteger cellToSwipe = (scrollView.contentOffset.x)/(pageWidth + minSpace) + 0.5; // cell width + min spacing for lines
//    if (cellToSwipe < 0) {
//        cellToSwipe = 0;
//    } else if (cellToSwipe >= numberOfCouponItemsInSection) {
//        cellToSwipe = numberOfCouponItemsInSection - 1;
//    }
//    [self.couponCollView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:cellToSwipe inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
//}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 10)
    {
        
        ListCouponsViewController *ListCouponsView = [[ListCouponsViewController alloc]initWithNibName:@"ListCouponsViewController" bundle:[NSBundle mainBundle]];
        ListCouponsView.featuredOrNonFeatured = sectionTitleLabel;
        ListCouponsView.sortObj = sortObj;
        [parentViewController.navigationController pushViewController:ListCouponsView animated:NO];
        if(fromMyAcc == TRUE)
        {
            ListCouponsView.fromMyAccounts = TRUE;
        }
        if(fromCoup == TRUE)
        {
            ListCouponsView.fromCoupons = TRUE;
        }
    }
    else
    {
        CouponsRetailerDetails *cellCoupon ;
        cellCoupon = [swipeCouponsList objectAtIndex:indexPath.row];
        NewCouponDetailViewController *couponDetailVC = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
        [defaults setValue:cellCoupon.couponId forKey:@"couponId"];
        [defaults setValue:cellCoupon.couponListId forKey:@"couponListId"];
        [parentViewController.navigationController pushViewController:couponDetailVC animated:NO];
    }
}


@end

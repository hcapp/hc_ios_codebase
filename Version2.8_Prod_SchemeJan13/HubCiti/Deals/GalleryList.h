//
//  GalleryList.h
//  Scansee
//
//  Created by Ajit on 6/20/13.
//
//

#import <UIKit/UIKit.h>
#import "HubCitiSegmentedControl.h"
//#import "CouponsViewController.h"

#import "DealHotDealsList.h"
//#import "HotDealDetails.h"
#import "NewCouponDetailViewController.h"

typedef enum webservicecalls{
    
    gallcoupbyloc,
    gallcoupbyprod,
    gallhdbyloc,
    gallhdbyprod,
    
}webServiceCallTypes;

@interface GalleryList : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,CustomizedNavControllerDelegate>{
    
    HubCitiSegmentedControl *viewSelectionControl;
    
    IBOutlet UIView *modelView;
	IBOutlet UITableView *locationTableView, *productTableView;
    IBOutlet UISearchBar *_searchBar;
    
    //common variables for COUPONS & HOT DEALS
    int loc_nextPage, prod_nextPage;
    int loc_lastVisitedRecord, prod_lastVisitedRecord;
    
    NSMutableArray *loc_retailerId, *loc_retailerName, *loc_retailerAddress, *loc_retLocId;
    NSMutableArray *prod_categoryId, *prod_categoryName;
    
    //COUPONS variables - Location
    NSMutableArray *loc_couponId[1000];
    NSMutableArray *loc_couponName[1000], *loc_couponDiscountAmount[1000];
    NSMutableArray *loc_couponExpireDate[1000], *loc_couponImagePath[1000];
    NSMutableArray *loc_coupDesc[1000], *loc_userCoupGallId[1000];
    
    //COUPONS variables - Products
    NSMutableArray *prod_couponId[1000];
    NSMutableArray *prod_couponName[1000], *prod_couponDiscountAmount[1000];
    NSMutableArray *prod_couponExpireDate[1000], *prod_couponImagePath[1000];
    NSMutableArray *prod_coupDesc[1000], *prod_userCoupGallId[1000];
    
    //HOT DEALS variables - Location
    NSMutableArray *loc_hotDealName[1000], *loc_hotDealId[1000], *loc_hotDealImagePath[1000];
    NSMutableArray *loc_hdDiscountAmount[1000];
    NSMutableArray *loc_hdExpDate[1000], *loc_hdGalId[1000];
    NSMutableArray *loc_hdDesc[1000];

    //HOT DEALS variables - Products
    NSMutableArray *prod_hotDealName[1000], *prod_hotDealId[1000], *prod_hotDealImagePath[1000];
    NSMutableArray *prod_hdDiscountAmount[1000];
    NSMutableArray *prod_hdExpDate[1000], *prod_hdGalId[1000];
    NSMutableArray *prod_hdDesc[1000];
    
    UIButton *btn1, *btn2;
    UIImageView *btn1Img, *btn2Img;
    NSString *responseText;
    int selectedTableViewTag;
    
    webServiceCallTypes iWebServiceType;
    NSOperationQueue *operationQueue;
    
    NSString *type;
}

@property (nonatomic, strong) IBOutlet UITableView *locationTableView;
@property (nonatomic, strong) IBOutlet UITableView *productTableView;

//-(void)couponsTapped;
//-(void)usedTapped;
//-(void)expiredTapped;
//-(void)hotDealsTapped;
//-(void)btn1Tapped:(id)sender;
//-(void)btn2Tapped:(id)sender;

@property (nonatomic, strong) NSString *type;


@end

//
//  HubCitiConnectionManager.h
//  HubCiti
//
//  Created by Ajit on 9/4/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HubCitiConnectionManagerDelegate

-(void)responseData:(NSString *) respo ;

@end


@interface HubCitiConnectionManager : NSObject<NSURLSessionDelegate, NSURLSessionDataDelegate>
{
    NSMutableData *webData;
    int respondeCode;
    NSURLConnection *connectionMain;
    NSURLSessionDataTask* connectionTask;
    NSURLSession *sessionForEstablishConnectionForGet;
    NSURLSession *session;
    
}
@property (nonatomic) int respondeCode;
@property (nonatomic, weak)id <HubCitiConnectionManagerDelegate> hubcitiConnMgrDelegate;

+ (id)connectionManager;

//Asynchronous Methods
-(id) establishPostConnectionforJsonData:(NSString *) baseUrl withParam:(NSMutableDictionary *)xmlStr;
-(id) establishGetConnectionforJsonData:(NSString *) baseUrl;
-(void)establishConnectionForSmartSearch:(NSString *) xmlStr base:(NSString *)baseUrl withDelegate:(id) _delegate;
-(void)establishConnectionFor:(NSString *) xmlStr base:(NSString *)baseUrl withDelegate:(id) _delegate;
-(void)establishConnectionForGet:(NSString *) baseUrl withDelegate:(id) _delegate;


//Synchronous Methods
-(NSString *) establishPostConnection:(NSString *) baseUrl withParam:(NSString *)xmlStr;
-(NSString *) establishGetConnection:(NSString *) baseUrl;
-(void)cancelSessionRequest : (NSString *) baseUrl;

@end

//
//  ViewMyRequestsViewController.h
//  HubCiti
//
//  Created by Bindu M on 9/24/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "CommonUtility.h"
@class AnyViewController;
@class EmailShareViewController;
@interface ViewMyRequestsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,MFMessageComposeViewControllerDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>{
    UITableView *viewReqTblView;
    NSMutableArray *arrBottomBtnDO;
    NSMutableArray *arrBottomButtonViewContainer;
    WebRequestState iWebRequestState;
    CommonUtility *common;
    BOOL isRemember;
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property (nonatomic,strong) NSMutableArray *arrBottomBtnDO;
@property(nonatomic,strong) AnyViewController * anyVC;
@end

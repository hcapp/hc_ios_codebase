//
//  ScanNowScreen.h
//  ScanSee
//  This sceen is called for RedLaser sdk
//  Created by ajit on 5/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "i_nigmaSDKTestViewController.h"
#import "ProductSearchUtility.h"

@class ScanHistoryViewController;
@class CommonUtility;
@class HubCitiManager;
@class ProductPage;


typedef enum webServicesScanNow
{
    SCAN_UTGETMAINMENUID,
    SCAN_GETSMARTSEARCHCOUNT,
    SCAN_GETSMARTSEARCHCOUNT_DETAIL,
    SCAN_GETSMARTSEARCHPRODLIST
    
}webServicesScanNowState;

@interface ScanNowScreen : UIViewController<CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UISearchDisplayDelegate,UISearchBarDelegate,ProductSearchUtilityDelegate,HubCitiConnectionManagerDelegate,i_nigmaSDKTestViewDelegate,CustomizedNavControllerDelegate>
{
    webServicesScanNowState iWebRequestState;
    
	IBOutlet UIImageView *imageView;

	IBOutlet UIView *modelView;
	IBOutlet UITableView* theTableView;
	IBOutlet UISearchBar * sBar;
	IBOutlet UIActivityIndicatorView *spinner;
	
	IBOutlet UIToolbar *toolBar;
	IBOutlet UIBarButtonItem *scanButton;
	IBOutlet UIBarButtonItem *historyButton;
	
	IBOutlet UIButton *btn_showprodPage;
	
	IBOutlet ScanHistoryViewController * historyController;
	IBOutlet i_nigmaSDKTestViewController * i_nigmaoverlayController;
	
	BOOL Success;
	BOOL m_bIsSearch;
	BOOL searchWasActive;
	BOOL productsFound;
   
	NSString *savedSearchString;
	NSMutableArray *productNameArray;
	NSMutableArray *productIdArray;
	NSMutableArray *plistArray;
	NSMutableArray *tableData ,*tableDataProductID,*filteredList;//will be storing data that will be displayed in table
	NSMutableArray *searchedData;
    NSMutableArray *rowNumberArray;
	
	CLLocationManager *locationManager;
    NSMutableArray *locationMeasurements;
    CLLocation *bestEffortAtLocation;
	NSString *latValue;
	NSString *longValue;
    
    int nextPage;
	int lastVisitedRecord;
    int rowNum;
	
    NSArray *listItems;
    NSMutableArray *tempArray;
    
	NSMutableArray *productInfoItems[NUM_PRODUCTINFOITEMS];

		
	CommonUtility *common;
	ProductPage *prodPage;
	NSMutableArray *productName ,*productDesc, *productId ,*productImagePath,*prodCountArray,*prodListIdArray;
    
    BOOL smartSearch;
	
    HubCitiConnectionManager *connectionManager;
    
    BOOL fetchingNextResults;
    BOOL startScanFlag;//to check whether in viewWillAppear need to call startScan
    UIBarButtonItem* back;
    
}
@property (nonatomic,strong) UIImageView *imageView;
@property (nonatomic,strong) IBOutlet UIBarButtonItem *scanButton;
@property (nonatomic,strong) IBOutlet ProductPage *prodPage;
@property (nonatomic,strong) IBOutlet UIButton *btn_showprodPage;
@property (nonatomic,strong) IBOutlet UIBarButtonItem *historyButton;
@property (nonatomic, strong) IBOutlet UISearchBar * sBar;
@property (nonatomic, strong) IBOutlet UIToolbar *toolBar;
@property (nonatomic,strong) UIButton *selectFromCameraRollButton;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSMutableArray *locationMeasurements;
@property (nonatomic, strong) CLLocation *bestEffortAtLocation;

@property (nonatomic, strong) IBOutlet UITableView* theTableView;
@property (nonatomic,strong) NSMutableArray *productNameArray;
@property (nonatomic,strong) NSMutableArray *plistArray;
//@property (nonatomic, copy) NSString *savedSearchString;
@property (readwrite) BOOL startScanFlag;

-(void) parseSearchResult : (NSString *) responseStr ;
-(void) parseScannedData:(NSString*)responseXml;
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;
-(void) start_iNigmaScanning;
- (void)stopUpdatingLocation:(NSString *)state;
-(void) addToWishList : (NSString *) productName1 productId:(NSString *) prodId ;

-(void)fetchPreviousDeals:(id)sender;
-(void)fetchNextDeals:(id)sender;
-(void) getProducts : ( NSString *) searchString;
-(void) refreshTable;

-(i_nigmaSDKTestViewController*)m_nigmaSDKTestViewController;
-(ScanHistoryViewController*)m_historyController;

-(IBAction)scan;
-(void)startScan;
-(IBAction)scanHistory;
-(void) removesplash;
-(void)showSplash;
- (void)hideSplash;

@end

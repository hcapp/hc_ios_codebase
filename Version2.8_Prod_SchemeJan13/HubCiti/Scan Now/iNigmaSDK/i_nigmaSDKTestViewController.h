//
//  i_nigmaSDKTestViewController.h
//  i-nigmaSDKTest
//
//  Created by 1 on 8/29/10.
//  Copyright 1 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HubCitiConnectionManager.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>

@class CommonUtility;
@class i_nigmaSDKTestViewController;
@class ProductPage;
@class ScanNowScreen;

typedef enum webServicesType{
    retSummary,
    getprodforbarcode
}WebServiceTypes;

@protocol i_nigmaSDKTestViewDelegate<NSObject>
-(void)sendBarcodeRequestToServer:(NSString*)barCodeString barcodeType:(NSString*)barCodeType;
-(void) changeTitle : (NSString *) title;
-(void) onNotify : (NSString *)strLocal;
@end

@interface i_nigmaSDKTestViewController : UIViewController<UINavigationBarDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,HubCitiConnectionManagerDelegate,AVCaptureMetadataOutputObjectsDelegate,CustomizedNavControllerDelegate>

{
    IBOutlet UIButton *ScanButton;
    IBOutlet UIButton *StopButton;
    // IBOutlet UIButton *CloseButton;
    //    IBOutlet UILabel *Label;
    //	IBOutlet UILabel *LabelDecode;
    //    IBOutlet UILabel *LabelNoti;
    //    IBOutlet UILabel *LabelType;
	IBOutlet UIButton *UpdateButton;
    IBOutlet UIButton *TorchButton;
//	void* m_pScanner;
    int m_bTorch;
    
    //Adding the ScanSee Animation
    UIImageView *scanAnim;
	
    //	ProductPage *viewProductDetailScreen;
	CommonUtility *common;
	IBOutlet UIActivityIndicatorView *spinner;
    NSArray *listItems;
    NSMutableArray *tempArray;
    HubCitiConnectionManager *connectionManager;
    //ScanNowScreen *scanNow;
    WebServiceTypes iWebServiceType;
    
@public
	id<i_nigmaSDKTestViewDelegate> __weak m_delegate;
}

@property (strong) IBOutlet UIActivityIndicatorView *spinner;
//@property (retain) IBOutlet ProductPage *viewProductDetailScreen;
@property (weak) id<i_nigmaSDKTestViewDelegate> m_delegate;

//-(void) initLocal;
//-(void) onError: (const char*) str;
//-(void) onNotify: (const char*) str;
////-(void) onDecode: (const unsigned short*) str:(const char*) strType;
//-(void) onDecode: (const unsigned short*) str :(const char*) strType :(const char*) strMode;
//-(void) OnCameraStopOrStart:(int) on;

-(void)sendBarcodeRequestToServer:(NSString*)barCodeString barcodeType:(NSString*)barCodeType;
//-(void)returnToMainPage;
-(void)startScanning;
-(void)stopScanning;
- (void)CloseCamera;
- (IBAction)ScanPressed;
//- (IBAction)UdateLicPressed;
- (IBAction)StopPressed;
- (IBAction)ClosePressed;
-(void)scanHistory;
- (IBAction)TorchPressed;
//Add to List,Fav,Wl
-(void) addToList : (NSString *) prod_id productName : (NSString *)prod_Name;
-(void) addToFavorites : (NSString *) prod_id productName : (NSString *)prod_Name ;
-(void) addToWishList : (NSString *) prod_id productName : (NSString *)prod_Name;
- (void)addproductfromScanNow;
@end


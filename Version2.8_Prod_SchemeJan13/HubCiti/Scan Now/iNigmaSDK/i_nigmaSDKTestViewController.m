//
//  i_nigmaSDKTestViewController.m
//  i-nigmaSDKTest
//
//  Created by 1 on 8/29/10.
//  Copyright 1 2010. All rights reserved.
//

#import "i_nigmaSDKTestViewController.h"
#import "AppDelegate.h"
//#import "Scanner.h"
#import "MainMenuViewController.h"
#import "CommonUtility.h"
#import "ScanHistoryViewController.h"
#import "ScanNowScreen.h"
#import "ProductPage.h"
#import "CommonUtility.h"
//#import "ShoppingCartViewController.h"
//#import "ShoppingListHomePage.h"
//#import "WishListHomePage.h"
#import "WebBrowserViewController.h"
//#import "ProductNotFound.h"
#import "RetailerSummaryViewController.h"
#import "SpecialOffersViewController.h"
#import "LocationManager.h"



@interface i_nigmaSDKTestViewController ()
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic) BOOL isReading;

@property (nonatomic, assign) BOOL shouldSendReadBarcodeToDelegate;

@end

//void WrapError(void* pThis,const char* str)
//{
//	i_nigmaSDKTestViewController* p = (i_nigmaSDKTestViewController*)pThis;
//	[p onError:str];
//	
//}
//void WrapNotify(void* pThis,const char* str)
//{
//	i_nigmaSDKTestViewController* p = (i_nigmaSDKTestViewController*)pThis;
//	[p onNotify:str];
//	
//}
//void WrapDecode(void* pThis,const unsigned short* str,const char* SymbolType,const char* SymbolMode)
//{
//	i_nigmaSDKTestViewController* p = (i_nigmaSDKTestViewController*)pThis;
//    
//	//[p onDecode:str:SymbolType];
//    [p onDecode:str:SymbolType:SymbolMode];
//}
//void WrapCameraStopOrStart(int on,void* pThis)
//{
//	i_nigmaSDKTestViewController* p = (i_nigmaSDKTestViewController*)pThis;
//	[p OnCameraStopOrStart:on];
//}

@implementation i_nigmaSDKTestViewController

@synthesize spinner;
@synthesize m_delegate;
//@synthesize viewProductDetailScreen;

#pragma mark init methods

- (id)initWithCoder:(NSCoder *)coder {
	
	if (self = [super initWithCoder:coder]) {
        [self initLocal];
	}
	return self;
}

/*
 If you were to create the view programmatically, you would use initWithFrame:.
 You want to make sure the placard view is set up in this case as well (as in initWithCoder:).
 */
-(void) initLocal
{
//	m_pScanner = new CScanner(self);
	StopButton = NULL;
	common = [[CommonUtility alloc]init];
    m_bTorch = 0;
	//CloseButton = NULL;
}

-(void)viewDidLoad
{
	[super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
	self.view.hidden = NO;
    listItems = [[NSArray alloc] init];
    tempArray = [[NSMutableArray alloc]init];
    
//    [spinner startAnimating];
//	spinner.hidesWhenStopped = YES;
//	spinner.center = self.view.center;
//	[self.view addSubview:spinner];
    
    //customize back button
    

    self.navigationItem.hidesBackButton = YES;
    //[back release];
	
}
#pragma mark view method

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
	[defaults  setObject:@"" forKey:KEY_INIGMA_BARCODE];
	[defaults  setObject:@"" forKey:KEY_INIGMA_BARCODETYPE];
	self.view.hidden = NO;
}

-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)viewWillDisappear:(BOOL)animated
{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (m_bTorch == 1){
        m_bTorch = 0;
//        ((CScanner*)m_pScanner)->TurnTorch(0);
        if ([device hasTorch] == YES)
        {
            [device setTorchMode:AVCaptureTorchModeOff];
        }
    }
	[super viewWillDisappear:YES];
	
}
//- (void)viewDidUnload {
//    
//	// Release any retained subviews of the main view.
//    //   [TopOverlayImage release];
//    //  [BottomOverlayImage release];
//	// e.g. self.myOutlet = nil;
//}

#pragma mark memory releasing method
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}
- (void)dealloc {
   
    
    m_bTorch = 0;

}

-(BOOL)checkLocationServiceStatus{
    if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) || ([[HubCitiManager sharedManager]gps_allow_flag] == NO)){
        return NO;
    }
    else {
        return YES;
    }
}

#pragma mark returnToMainPage method


#pragma mark Scanning methods
- (IBAction)ScanPressed {
	
//	UIViewController* uiViewCont = self;
//	((CScanner*)m_pScanner)->UpdateLicense();//added by pradeep
//	((CScanner*)m_pScanner)->Scan(uiViewCont);
}

-(void)startScanning
{
    
   
    DLog(@"STARTING SCANNING");
   // [spinner stopAnimating];
  	[defaults  setObject:@"" forKey:KEY_INIGMA_BARCODE];
	[defaults  setObject:@"" forKey:KEY_INIGMA_BARCODETYPE];
	
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        //((CScanner*)m_pScanner)->Scan(self.view,30,30,720,920);
//        ((CScanner*)m_pScanner)->Scan(self.view,30,30,720,920,30,30,920,720,0,0);
        
//        [self.view setFrame:CGRectMake(30, 30, 720, 920)];
    }
    
    else{
        //((CScanner*)m_pScanner)->Scan(self.view,5,5,310,400);
//        ((CScanner*)m_pScanner)->Scan(self.view,5,5,310,400,5,0,470,340,0,0);
//        [self.view setFrame:CGRectMake(5,5,310,400)];
    }
     NSError *error;
    
    
    
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
//        return NO;
    }
    
    // Initialize the captureSession object.
    _captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [_captureSession addInput:input];
    
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    dispatch_async(dispatchQueue, ^(void){
        [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self onNotify:@"Code Found"];
//        });
    });
    
    NSArray *metaData;
    if (IOS7) {
        metaData = [NSArray arrayWithObjects:AVMetadataObjectTypeQRCode,AVMetadataObjectTypePDF417Code,AVMetadataObjectTypeUPCECode,AVMetadataObjectTypeCode39Code,
                             AVMetadataObjectTypeCode39Mod43Code,AVMetadataObjectTypeEAN13Code,AVMetadataObjectTypeEAN8Code,AVMetadataObjectTypeCode93Code,AVMetadataObjectTypeCode128Code,AVMetadataObjectTypeAztecCode, nil];
    }
    else{
    metaData = [NSArray arrayWithObjects:AVMetadataObjectTypeQRCode,AVMetadataObjectTypePDF417Code,AVMetadataObjectTypeUPCECode,AVMetadataObjectTypeCode39Code,
                         AVMetadataObjectTypeCode39Mod43Code,AVMetadataObjectTypeEAN13Code,AVMetadataObjectTypeEAN8Code,AVMetadataObjectTypeCode93Code,AVMetadataObjectTypeCode128Code,AVMetadataObjectTypeAztecCode,AVMetadataObjectTypeInterleaved2of5Code,AVMetadataObjectTypeDataMatrixCode,AVMetadataObjectTypeITF14Code, nil];
    }
    [captureMetadataOutput setMetadataObjectTypes:metaData];
    
    NSLog(@"captureMetadataOutput %@", captureMetadataOutput);
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:self.view.layer.bounds];
    [self.view.layer addSublayer:_videoPreviewLayer];
    
    
    // Start video capture.
    [_captureSession startRunning];
    [self OnCameraStopOrStart:1];
	DLog(@"i_nigmaSDKTestViewController:%@",@"startScanning 333");
}


- (IBAction)StopPressed
{
    if (m_bTorch == 1){
        m_bTorch = 0;
//        ((CScanner*)m_pScanner)->TurnTorch(0);
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] == YES)
        {
            [device setTorchMode:AVCaptureTorchModeOff];
        }
    }
    
	
//	((CScanner*)m_pScanner)->Abort();
	self.view.hidden = YES;
	
}
- (IBAction)ClosePressed
{
	
//	((CScanner*)m_pScanner)->CloseCamera();
	StopButton = NULL;
	//[self returnToMainPage];
	self.view.hidden = YES;
	
	[self scanHistory];
}

- (IBAction)TorchPressed
{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    [device lockForConfiguration:nil];
	if (m_bTorch == 0){
        m_bTorch = 1;
//        ((CScanner*)m_pScanner)->TurnTorch(1);
        
        if ([device hasTorch] == YES)
        {
            [device setTorchMode:AVCaptureTorchModeOn];
        }
        
    }else{
        m_bTorch = 0;
//        ((CScanner*)m_pScanner)->TurnTorch(0);
        if ([device hasTorch] == YES)
        {
            [device setTorchMode:AVCaptureTorchModeOff];
        }
        
    }
}


-(void)stopScanning
{
//	((CScanner*)m_pScanner)->CloseCamera();
	StopButton = NULL;
    
}

-(void)stopReading{
    // Stop video capture and make the capture session object nil.
    
    [_captureSession stopRunning];
    _captureSession = nil;
    
    // Remove the video preview layer from the viewPreview view's layer.
    [_videoPreviewLayer removeFromSuperlayer];
     [self.view removeFromSuperview];
    [self.m_delegate onNotify:@"Code Found"];
//    [HubCitiAppDelegate showActivityIndicator];
}

- (void)CloseCamera
{
	
//	((CScanner*)m_pScanner)->CloseCamera();
	StopButton = NULL;
	[self.view removeFromSuperview];
}

-(void)scanHistory
{
	
	self.view.hidden = YES;
	
	ScanHistoryViewController *viewScanHistory = [[ScanHistoryViewController alloc]initWithNibName:@"ScanHistoryViewController" bundle:[NSBundle mainBundle]];
	viewScanHistory.latValue1 = [defaults  objectForKey:KEY_LATITUDE];
	viewScanHistory.longValue1 = [defaults  objectForKey:KEY_LONGITUDE];
	[self.navigationController pushViewController:viewScanHistory animated:NO];
	//[viewScanHistory release];
	
}

-(void) onError: (const char*) str
{
	NSString *strLocal;
	strLocal = [NSString stringWithFormat:@"%s" , str];
	DLog(@"Label_text:%@",strLocal);
	
    //if( [strLocal isEqualToString:@"Time Out"]){
    
    if(StopButton)
        StopButton.hidden=YES;
    
    if(TorchButton)
        TorchButton.hidden = YES;
    
    StopButton = NULL;
    TorchButton = NULL;
	[self.view removeFromSuperview];
    
}


-(void) onNotify: (NSString*) strLocal{
	
	DLog(@"LabelNoti_text:%@",strLocal);
	//self.view.hidden = YES;
	
    NSString *barCodeString = [defaults  objectForKey:KEY_INIGMA_BARCODE];
    NSString *barCodeStringType = [defaults  objectForKey:KEY_INIGMA_BARCODETYPE];
    
	if([defaults  objectForKey:KEY_INIGMA_BARCODE] && ![[defaults  objectForKey:KEY_INIGMA_BARCODE] isEqualToString:@""])
	{
		
		if([strLocal isEqualToString:@"Code Found"])
		{
            //[spinner startAnimating];
			[defaults  setObject:@"YES" forKey:@"FLAG_BCODE_SCANNED"];
			DLog(@"KEY_INIGMA_BARCODE:%@",[defaults  objectForKey:KEY_INIGMA_BARCODE]);
            
            if([barCodeStringType isEqualToString:@"QR Code"])
            {
                /*Product Page - 1000
                Retailer Summary Page - 2000
                Anything Page - 2100
                Special Offer Page - 2200
                Give Away Page - 2300*/
                
                //@Deepak: If QRCODE called set UserDefault for KEY_SCANTYPEID:2
                [defaults setValue:@"2" forKey:KEY_SCANTYPEID];
                NSString *list = barCodeString;
                
                //check for pdf file
                /*NSRange range = [barCodeString rangeOfString:@".pdf"];
                 if (range.location != NSNotFound)
                 {
                 }*/
                DLog(@"File type %@",[list pathExtension]);
                
                listItems = [list componentsSeparatedByString:@"/"];
                DLog(@"List Array:%@",[listItems description]);
                if ([listItems count]>1){
                    
                    //to detect whether QR code is from ScanSee or outside ScanSee
                    /* else*/ if ([listItems count]>=4 && [[listItems objectAtIndex:2]isEqualToString:[defaults valueForKey:@"DomainName"]] && [[listItems objectAtIndex:3]isEqualToString:@"SSQR"]){
                        
                        //DLog(@"value:%@",[[listItems objectAtIndex:5]substringToIndex:4]);
                        //check for navigate to product summary page
                        if ([listItems count]>=6 && [[[listItems objectAtIndex:5]substringToIndex:4] isEqualToString:@"1000"])
                        {
                             NSString *searchFrom = [defaults  valueForKey:KEY_SEARCHFROM];
                            
                            NSArray *productKey = [[listItems objectAtIndex:5]componentsSeparatedByString:@"="];
                            DLog(@"Product Id:%@",[productKey objectAtIndex:1]);
                            [defaults  setObject:[productKey objectAtIndex:1] forKey:KEY_PRODUCTID];
                            
                            // If scan product belongs to below conditins then addd directly to the list
                            if ([searchFrom isEqualToString:@"fav"] || [searchFrom isEqualToString:@"list"] || [searchFrom isEqualToString:@"wish"])
                            {
                                
                                [[HubCitiManager sharedManager]setShareFromTL:NO];
                                
                                //set KEY_PRODLISTID to 'nil' as its not reqd to send it in getProductSummary req from this scenario
                                
                                [defaults  setObject:nil forKey:KEY_PRODLISTID];
                                
                                [defaults setValue:@"0" forKey:KEY_SCANTYPEID];
                                [self addproductfromScanNow];
                            }
                            else
                            {
                                [defaults  setObject:nil forKey:KEY_PRODLISTID];
                                [defaults setValue:@"0" forKey:KEY_SCANTYPEID];
                                ProductPage *prodPage = [[ProductPage alloc]initWithNibName:@"ProductPage" bundle:[NSBundle mainBundle]];
                                
                                [[HubCitiAppDelegate navigationController] pushViewController:prodPage animated:NO];
                                //[prodPage release];
                            }
                        }
                        
                        //check for navigate to Retailer Summary page
                        else if ([listItems count]>=6 &&[[[listItems objectAtIndex:5]substringToIndex:4] isEqualToString:@"2000"])
                        {
                            DLog(@"Redirect to retailer page");
                            //http://www.ScanSee.net/SSQR/qr/2000.htm?key1=1001&key2=222
                            
                            if (([CLLocationManager  locationServicesEnabled])
                                && ([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) &&[[HubCitiManager sharedManager] gps_allow_flag] == YES){
                                
                                [tempArray addObjectsFromArray:listItems];
                                DLog(@"Temparray: %@",tempArray);
                                [Location updateLocationinfo:self];
                            }
                            else {
                                
                                NSArray *retailerKey = [[listItems objectAtIndex:5]componentsSeparatedByString:@"="];
                                DLog(@"RetailerKey :%@",[retailerKey description]);
                                DLog(@"Retailer Id:%@",[retailerKey objectAtIndex:1]);
                                NSArray *retId = [[retailerKey objectAtIndex:1]componentsSeparatedByString:@"&"];
                                NSArray *retLocnId = [[retailerKey objectAtIndex:2]componentsSeparatedByString:@"&"];
                                
                                iWebServiceType = retSummary;
                                NSMutableString *requestStr = [[NSMutableString alloc] init];
                                [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
                                [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[retLocnId objectAtIndex:0]];
                                
                                 if ([[defaults  valueForKey:KEYZIPCODE]length]){
                                    
                                    [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
                                }
                                
                                // @Deepak: Added this code for "retSummary" in UserTracking
                                [requestStr appendFormat:@"<scanTypeId>%@</scanTypeId>",@"1"];
                                
                                if([defaults valueForKey:KEY_MAINMENUID])
                                    [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
                                
                                if([defaults valueForKey:KEY_RLISTID])
                                    [requestStr appendFormat:@"<retListId>%@</retListId>",[defaults valueForKey:KEY_RLISTID]];

                                [requestStr appendFormat:@"<retailerId>%@</retailerId>",[retId objectAtIndex:0]];
                                if([defaults valueForKey:KEY_HUBCITIID])
                                    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
                                NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
                                
//                                [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
                                
                                NSString *response= [ConnectionManager establishPostConnection:urlString withParam:requestStr];
                                
                                [self parse_retsummary:response];
                            }
                        }//end of elseif
                        
                        //check for navigate to Retailer created page and special offer page or play a video
                        else if ([listItems count]>=6 && [[[listItems objectAtIndex:5]substringToIndex:4] isEqualToString:@"2200"])
                        {
                            
                            DLog(@"List:%@",list);
                            //Added MainmEnu ID
                            NSString *str = [NSString stringWithFormat:@"%@&fromSS=Y&mainMenuId=%@&scanTypeId=1",list,[defaults valueForKey:KEY_MAINMENUID]];
                            [defaults  setObject:str forKey:KEY_URL];
                            
                            SpecialOffersViewController *splOfferPage = [[SpecialOffersViewController alloc] initWithNibName:@"SpecialOffersViewController" bundle:[NSBundle mainBundle]];
                            
                            UINavigationController *myNavController = [HubCitiAppDelegate navigationController];
                            [myNavController pushViewController:splOfferPage animated:NO];
                          //  [splOfferPage release];
                            
                        }
                        
                        else if ([listItems count]>=6 && [[[listItems objectAtIndex:5]substringToIndex:4] isEqualToString:@"2100"]) {//anything page
                            NSString *str = [NSString stringWithFormat:@"%@&fromSS=Y&mainMenuId=%@&scanTypeId=1",barCodeString,[defaults valueForKey:KEY_MAINMENUID]];
                            [defaults  setObject:str forKey:KEY_URL];
                            
                            //[defaults setValue:@"Retailer Page" forKey:@"rightbarbuttontitle"];
                            
                            //changed to SpecialOffersViewController instead WebBrowserViewController, as user tracking was done twice - one while acquiring certificate and once when loading
                            SpecialOffersViewController *urlPage = [[SpecialOffersViewController alloc]initWithNibName:@"SpecialOffersViewController" bundle:[NSBundle mainBundle]];
                            UINavigationController *myNavController = [HubCitiAppDelegate navigationController];
                            [myNavController pushViewController:urlPage animated:NO];
                           // [urlPage release];
                        }
                        
                        else if ([listItems count]>=6 && [[[listItems objectAtIndex:5]substringToIndex:4] isEqualToString:@"2300"]){
                            
                            
                            NSString *str = [NSString stringWithFormat:@"%@&fromSS=Yes&mainmenuid=%@&scantypeid=1",barCodeString,[defaults valueForKey:KEY_MAINMENUID]];
                            
                            [defaults  setObject:str forKey:KEY_URL];
                            
                            SpecialOffersViewController *splOfferPage = [[SpecialOffersViewController alloc] initWithNibName:@"SpecialOffersViewController" bundle:[NSBundle mainBundle]];
                            
                            UINavigationController *myNavController = [HubCitiAppDelegate navigationController];
                            [myNavController pushViewController:splOfferPage animated:NO];
                            ReleaseAndNilify(splOfferPage);
                        }
                        
                    }
                    //end if statement
                    
                    //Checing to see if its a web video, if so, we are going to pop to safari.
                    else if ([barCodeString rangeOfString:@"youtube.com"].location != NSNotFound ||
                             [barCodeString rangeOfString:@"vimeo.com"].location != NSNotFound ||
                             [barCodeString rangeOfString:@"scansee.com/videos/"].location != NSNotFound)
                    {
                        // [self performSelector:@selector(popToSafari:) withObject:urlString afterDelay:1];
                        [self popToSafari:barCodeString];
                    }
                    
                    //else part if QR code not from ScanSee
                    else{
                        DLog(@"Url:%@",barCodeString);
                        
                        [defaults  setObject:barCodeString forKey:KEY_URL];
                        [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                        
                        WebBrowserViewController *urlView = [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                        
                        UINavigationController *myNavController = [HubCitiAppDelegate navigationController];
                        //[self.view addSubview:self.view.window.rootViewController.view];
                        [myNavController pushViewController:urlView animated:NO];
                        //[urlView release];
                    }
                }
                else
                {
                    [self sendBarcodeRequestToServer:[defaults  objectForKey:KEY_INIGMA_BARCODE] barcodeType:[defaults  objectForKey:KEY_INIGMA_BARCODETYPE]];
                }
                
            }//end of checking for QR code
            else
            {
                //@Deepak: If QRCODE called set UserDefault for KEY_SCANTYPEID:1
                 [defaults setValue:@"1" forKey:KEY_SCANTYPEID];
                [self sendBarcodeRequestToServer:[defaults  objectForKey:KEY_INIGMA_BARCODE] barcodeType:[defaults  objectForKey:KEY_INIGMA_BARCODETYPE]];
            }
		}
		else if( [strLocal isEqualToString:@"Camera Closed"]){
			
            if(StopButton)
                StopButton.hidden=YES;
 
            if(TorchButton)
                TorchButton.hidden = YES;
            
 		}
	}
    
    if( [strLocal isEqualToString:@"Time Out"]){
        
        if(StopButton)
            StopButton.hidden=YES;

        if(TorchButton)
            TorchButton.hidden = YES;
        
        ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];

        [self presentViewController:viewScanNowScreen animated:YES completion:nil];
      //  [viewScanNowScreen release];
    }
}

-(void)popToSafari:(NSString *)urlString
{
    
    NSURL *vidURL = [NSURL URLWithString:urlString];
    
    if (![[UIApplication sharedApplication] openURL:vidURL])
        
        DLog(@"%@%@",@"Failed to open url:",[vidURL description]);
}

-(void) updatedLatlongValue :(NSString *) latValue longitudeValue:(NSString *)longValue
{
	[defaults setObject:latValue forKey:KEY_LATITUDE];
    [defaults setObject:longValue forKey:KEY_LONGITUDE];
	
    DLog(@"Temparray: %@",tempArray);
    
    NSArray *retailerKey = [[tempArray objectAtIndex:5]componentsSeparatedByString:@"="];
    // DLog(@"RetailerKey :%@",[retailerKey description]);
    // DLog(@"Retailer Id:%@",[retailerKey objectAtIndex:1]);
    NSArray *retId = [[retailerKey objectAtIndex:1]componentsSeparatedByString:@"&"];
    NSArray *retLocnId = [[retailerKey objectAtIndex:2]componentsSeparatedByString:@"&"];
    
    iWebServiceType = retSummary;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[retLocnId objectAtIndex:0]];
    [requestStr appendFormat:@"<latitude>%@</latitude><longitude>%@</longitude>",latValue,longValue];
    
    // @Deepak: Added this code for "retSummary" in UserTracking
    [requestStr appendFormat:@"<scanTypeId>%@</scanTypeId>",@"1"];
   
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if([defaults valueForKey:KEY_RLISTID])
        [requestStr appendFormat:@"<retListId>%@</retListId>",[defaults valueForKey:KEY_RLISTID]];
    
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",[retId objectAtIndex:0]];
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    
//    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    NSString *response= [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    
    [self parse_retsummary:response];
    ReleaseAndNilify(requestStr);
}

#pragma mark parse methods
-(void)responseData:(NSString *)response{
    
    switch (iWebServiceType) {
        case retSummary:
            [self parse_retsummary:response];
            break;
        case getprodforbarcode:
//            [self parse_getprodforbarcode:response];
            break;
            
        default:
            break;
    }
}
-(void)parse_retsummary:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && ![[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *retailerSummaryPage = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        [[HubCitiAppDelegate navigationController] pushViewController:retailerSummaryPage animated:NO];
       // [retailerSummaryPage release];
    }
}

-(void)sendBarcodeRequestToServer:(NSString*)barCodeString barcodeType:(NSString*)barCodeType{
	DLog(@"i_nigmaSDKTestViewController->sendBarcodeRequestToServer");
    
    // Freeze preview pane
//    [self CloseCamera];
    
    
    
	if((barCodeString!=NULL) && ([barCodeString isEqualToString:@""]==NO))
	{
		if([Network currentReachabilityStatus]==0)
		{
            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
		}
		else
		{
            iWebServiceType = getprodforbarcode;
			NSMutableString *requestStr = [[NSMutableString alloc] init];
            [requestStr appendFormat:@"<ProductDetail><deviceId>%@</deviceId><userId>%@</userId>",[defaults valueForKey:KEY_DEVICEID],[defaults  valueForKey:KEY_USERID]];
            
            //@Deepak: Added code for userTracking
            [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId><scanTypeId>1</scanTypeId>",[defaults  valueForKey:KEY_MAINMENUID]];
            
			[requestStr appendFormat:@"<barCode><![CDATA[%@]]></barCode><scanType><![CDATA[%@]]></scanType></ProductDetail>", barCodeString, barCodeType];
            
			NSString *urlString = [NSString stringWithFormat:@"%@scannow/getprodforbarcode",BASE_URL];
            
            NSString *response  = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
            
             [self parse_getprodforbarcode:response];
//            [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
            
            //[requestStr release];
		}
	}
}


- (void)addproductfromScanNow
{
    [[HubCitiManager sharedManager]setShareFromTL:NO];
    
    if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"fav"]) {
        [HubCitiAppDelegate removeActivityIndicator];
        [self addToFavorites:[defaults valueForKey:KEY_PRODUCTID] productName:@""];
        return;
    }
    if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"list"]) {
        [HubCitiAppDelegate removeActivityIndicator];
        [self addToList:[defaults valueForKey:KEY_PRODUCTID] productName:@""];
        return;
    }
    if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"wish"]) {
        [HubCitiAppDelegate removeActivityIndicator];
        [self addToWishList:[defaults valueForKey:KEY_PRODUCTID] productName:@""];
        return;
    }
    if([UtilityManager isNullOrEmptyString:[defaults valueForKey:KEY_PRODUCTID]])
    {
        [HubCitiAppDelegate removeActivityIndicator];
        [UtilityManager showAlert:nil msg:NSLocalizedString(@"There is a technical problem",@"There is a technical problem")];
        return;
        
    }
}


-(void)parse_getprodforbarcode:(NSString *)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *productIdElement = [TBXML childElementNamed:KEY_PRODUCTID parentElement:tbxml.rootXMLElement];
    TBXMLElement *productNameElement = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:tbxml.rootXMLElement];
    
    if (productIdElement != nil) {
        
        NSString *searchFrom = [defaults  valueForKey:KEY_SEARCHFROM];
        DLog(@"valueForKey:KEY_SEARCHFROM:%@",searchFrom);
        
        NSString *productIdStr = [NSString stringWithFormat:@"%@",[TBXML textForElement:productIdElement]];
        
        if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"fav"]) {
            [HubCitiAppDelegate removeActivityIndicator];
            [self addToFavorites:productIdStr productName:[TBXML textForElement:productNameElement]];
            return;
        }
        if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"list"]) {
            [HubCitiAppDelegate removeActivityIndicator];
            [self addToList:productIdStr productName:[TBXML textForElement:productNameElement]];
            return;
        }
        if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"wish"]) {
            [HubCitiAppDelegate removeActivityIndicator];
            [self addToWishList:productIdStr productName:[TBXML textForElement:productNameElement]];
            return;
        }
        if([UtilityManager isNullOrEmptyString:productIdStr])
        {
            [HubCitiAppDelegate removeActivityIndicator];
            [UtilityManager showAlert:nil msg:NSLocalizedString(@"There is a technical problem",@"There is a technical problem")];
            return;
            
        }
        
        [[HubCitiManager sharedManager]setShareFromTL:NO];
        
        [defaults  setObject:productIdStr forKey:KEY_PRODUCTID];
        
//        NSMutableString *xmlString = [[NSMutableString alloc] init];
//        
//        [xmlString appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults  objectForKey:KEY_USERID]];
//        
//        //@Deepak: Added MainMenuID in the request- userTracking for shoppinglist
//        [xmlString appendFormat:@"<mainMenuId>%@</mainMenuId><scanTypeId>%@</scanTypeId>", [defaults  valueForKey:KEY_MAINMENUID],[defaults  valueForKey:KEY_SCANTYPEID]];
//        
//        //@Deepak: Adding prodListID code for UserTracking
//        //@Ajit: Commented as its not reqd to send prodListID in this scenario
//        //if([defaults  valueForKey:KEY_PRODLISTID])
//          //  [xmlString appendFormat:@"<prodListID>%@</prodListID>", [defaults  valueForKey:KEY_PRODLISTID]];
//
//        if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[HubCitiManager sharedManager]gps_allow_flag] == YES && [[defaults valueForKey:KEY_LATITUDE] length])
//            
//            [xmlString appendFormat:@"<latitude>%@</latitude><longitude>%@</longitude>", [defaults valueForKey:KEY_LATITUDE],[defaults valueForKey:KEY_LONGITUDE]];
//        else if ([[defaults objectForKey:@"ZipCode"] length])
//            [xmlString appendFormat:@"<postalcode>%@</postalcode>", [defaults objectForKey:@"ZipCode"]] ;
//
//        [xmlString appendFormat:@"<productId>%@</productId></ProductDetailsRequest>",productIdStr];
//        
//        
//        NSMutableString *urlString = [[BASE_URL mutableCopy] ;
//        [urlString appendString:@"find/getproductsummary"];
//        
//        NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:xmlString];
//        
//        [xmlString release];
//        
//        if ([UtilityManager isNullOrEmptyString:responseXml]) {
//            [HubCitiAppDelegate removeActivityIndicator];
//            return;
//        }
//        
//        //parsing response
//        if(![common isNullOrEmptyString:responseXml])
//        {
//            
//            [defaults setObject:responseXml forKey:KEY_RESPONSEXML];
            [HubCitiAppDelegate removeActivityIndicator];
            //scanNow.startScanFlag = YES;
            [SharedManager setCallScanner:YES];
//            [defaults setValue:@"0" forKey:KEY_SCANTYPEID];
            ProductPage *viewProductPage = [[ProductPage alloc]initWithNibName:@"ProductPage" bundle:[NSBundle mainBundle]];
            
            UINavigationController *myNavController = [HubCitiAppDelegate navigationController];
            [myNavController pushViewController:viewProductPage animated:NO];
            //[viewProductPage release];
            
//        }
//        else {
//            [HubCitiAppDelegate removeActivityIndicator];
//        }
    }
    else
    {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        DLog(@"%lu",(unsigned long)[[self.navigationController viewControllers] count]);
        if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"fav"]||[[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"list"])
            
        {
            [HubCitiAppDelegate removeActivityIndicator];
            UIAlertController * alert;
            
            alert=[UIAlertController alertControllerWithTitle:[TBXML textForElement:responseTextElement] message:NSLocalizedString(@"Still want to add?",@"Still want to add?") preferredStyle:UIAlertControllerStyleAlert];
            
            
            UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleCancel
                                 handler:nil];
            [alert addAction:cancel];
            UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action){
                                        
                                     }];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [defaults setValue:nil forKey:@"SearchKey"];
        }
        else {
            [HubCitiAppDelegate removeActivityIndicator];
            //scanNow.startScanFlag = NO;
            [SharedManager setCallScanner:NO];
            UIAlertController * alert;
            
            alert=[UIAlertController alertControllerWithTitle:[TBXML textForElement:responseTextElement] message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleCancel
                                     handler:nil];
            [alert addAction:cancel];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
    }
    
}

-(void) addToList : (NSString *) prod_id productName : (NSString *)prod_Name{
	NSMutableString *requestStr = [[NSMutableString alloc] init];

	[requestStr appendFormat:@"<AddShoppingList><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", prod_id];
    [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName>", prod_Name];
    [requestStr appendString:@"</ProductDetail></productDetails>"];
    [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId></AddShoppingList>", [defaults  valueForKey:KEY_MAINMENUID]];
	
	NSMutableString *urlString = [BASE_URL mutableCopy];
	[urlString appendFormat:@"shoppingList/addtslbysearch"];
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml])
        return;
    
	[defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
    
	TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
	TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
	TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
	if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
        [SharedManager setRefreshList:YES];
        
//        ShoppingCartViewController *viewList = [[ShoppingCartViewController alloc]initWithNibName:@"ShoppingCartViewController" bundle:[NSBundle mainBundle]];
//        
//        UINavigationController *myNavController ;//= [[UINavigationController alloc] init];
//        
//        myNavController = [AppDelegate navigationController];
//        
//        [myNavController pushViewController:viewList animated:NO];
//        [viewList release];
        
    }
	else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
	}
	
	//[responseXml release];
	
}

-(void) addToFavorites : (NSString *) prod_id productName : (NSString *)prod_Name {
    
	NSMutableString *requestStr = [[NSMutableString alloc] init];
	[requestStr appendString:@"<AddShoppingList>"];
	[requestStr appendFormat:@"<userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
	
    [requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", prod_id];
    [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName>", prod_Name];
    
    [requestStr appendString:@"</ProductDetail></productDetails>"];
    //@Deepak: Added MainMenuID in the request- userTracking for shoppinglist
    [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId></AddShoppingList>", [defaults  valueForKey:KEY_MAINMENUID]];
	
  	NSMutableString *urlString = [BASE_URL mutableCopy];
	[urlString appendFormat:@"shoppingList/addproducttosl"];
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml])
        return;
    
	[defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
	TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
	TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
	TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
	if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
        [SharedManager setRefreshList:YES];
        
//        ShoppingListHomePage *viewFav = [[ShoppingListHomePage alloc]initWithNibName:@"ShoppingListHomePage" bundle:[NSBundle mainBundle]];
//        
//        UINavigationController *myNavController;// = [[UINavigationController alloc] init];
//        myNavController = [AppDelegate navigationController];
//        [myNavController pushViewController:viewFav animated:NO];
//        //    [myNavController release];
//        [viewFav release];
		//[defaults  removeObjectForKey:KEY_SEARCHFROM];
	}
	else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
	}
	
	//[responseXml release];
	
}

-(void) addToWishList : (NSString *) prod_id productName : (NSString *)prod_Name {
	
	NSMutableString *requestStr = [[NSMutableString alloc] init];
	[requestStr appendString:@"<ProductDetailsRequest>"];
	[requestStr appendFormat:@"<userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
	
    [requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", prod_id];
    [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName>", prod_Name];
    
    [requestStr appendString:@"</ProductDetail></productDetails>"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
    [requestStr appendString:@"</ProductDetailsRequest>"];

	
    NSMutableString *urlString = [BASE_URL mutableCopy];
	[urlString appendFormat:@"wishlist/addWishListProd"];
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml])
        return;
    
	[defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
	TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
	TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
	TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    
	if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"])
    {
        UINavigationController *myNavController ;//= [[UINavigationController alloc] init];
        myNavController = [HubCitiAppDelegate navigationController];

        [myNavController popViewControllerAnimated:NO];
		//[defaults  removeObjectForKey:KEY_SEARCHFROM];
	}
	else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
	}
	
	//[responseXml release];
	
}

//-(void) onDecode: (const unsigned short*) str : (const char*) strType : (const char*) strMode

//{
//    NSString *barCodeString;
//    NSString *barCodeTypeString;
//    
//	[defaults  setObject:@"" forKey:KEY_INIGMA_BARCODE];
//	[defaults  setObject:@"" forKey:KEY_INIGMA_BARCODETYPE];
//	
//	barCodeString = [NSString stringWithFormat:@"%S" , str];
//    
//    barCodeTypeString = [NSString stringWithFormat:@"%s" , strType];
//	DLog(@"LabelType_text:%@",barCodeTypeString);
//    
//    if ([barCodeTypeString isEqualToString:@"EAN 13"])
//    {
//        // Use first digit to differentiate between EAN13 and UPCA
//        // An EAN13 barcode whose first digit is zero is exactly the same as a UPCA barcode.
//        if ([[barCodeString substringToIndex:1] isEqualToString:@"0"])
//        {
//            barCodeString = [barCodeString substringFromIndex:1];
//            barCodeTypeString = @"UPC-A";
//            DLog(@"barCodeString1:%@",barCodeString);
//            DLog(@"barCodeTypeString1:%@",barCodeTypeString);
//        }
//        else
//        {
//            barCodeTypeString = @"EAN 13";
//            DLog(@"barCodeString2:%@",barCodeString);
//            DLog(@"barCodeTypeString2:%@",barCodeTypeString);
//        }
//    }
//    else if ([barCodeTypeString isEqualToString:@"EAN 8"]) {
//        barCodeTypeString = @"EAN-8";
//    }
//    DLog(@"barCodeString3:%@",barCodeString);
//    DLog(@"barCodeTypeString3:%@",barCodeTypeString);
//    
//	if(barCodeString)
//        [defaults  setObject:barCodeString forKey:KEY_INIGMA_BARCODE];
//	
//	DLog(@"LabelDecode_text:%@",barCodeString);//provide the barcode
//    
//	[HubCitiAppDelegate setBarCode:barCodeString];
//    
//	self.view.hidden = YES;
//	
//	if(barCodeTypeString)
//        [defaults  setObject:barCodeTypeString forKey:KEY_INIGMA_BARCODETYPE];
//}

-(void) OnCameraStopOrStart:(int) on
{
    scanAnim = nil;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] > 4.9) {
        if (on == 1){
            AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
            if ([device hasTorch] == YES){
                TorchButton.hidden = NO;
                [TorchButton setNeedsDisplay];
            }
            if (StopButton)
            {
                StopButton.hidden = NO;
                [StopButton setNeedsLayout];
                //			CloseButton.hidden = NO;
                //			[CloseButton setNeedsLayout];
            }else{
//                StopButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//                [StopButton addTarget:self action:@selector(StopPressed) forControlEvents:UIControlEventTouchUpInside];
//                [StopButton setTitle:NSLocalizedString(@"Cancel",@"Cancel") forState:UIControlStateNormal];
//                //Chance's Edits_032812
//                StopButton.frame = CGRectMake(30, 440, 115, 10);
//                //StopButton.frame = CGRectMake(30,340,115,50);
//                //End Chance's Edits_032812
//                [self.view addSubview:StopButton];
//                TorchButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//                [TorchButton addTarget:self action:@selector(TorchPressed) forControlEvents:UIControlEventTouchUpInside];
//                [TorchButton setTitle:@"Light" forState:UIControlStateNormal];
//                
//                //Chance's Edits_032812
//                TorchButton.frame = CGRectMake(175, 440, 115, 10);
//                //TorchButton.frame = CGRectMake(175,340,115,50);
//                //End Chance's Edits_032812
//                [self.view addSubview:TorchButton];
                
                //Chance's Edits_032912
                UIImageView *TopOverlayImage = nil;
                TopOverlayImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ScanNow_topScanOverlay_arrows.png"]];
                TopOverlayImage.frame = CGRectMake(0,-20,SCREEN_WIDTH,90);
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    TopOverlayImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ipadScanImageTop.png"]];
                    TopOverlayImage.frame = CGRectMake(0,-20,SCREEN_WIDTH,120);
                }
                [self.view addSubview:TopOverlayImage];
                ReleaseAndNilify(TopOverlayImage);
                
                
                
                //need to set here
                UIToolbar *toolbar = [[UIToolbar alloc] init];
                [toolbar setBarTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]]];
                
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    toolbar.frame = CGRectMake(0, self.view.frame.size.height-44-50-30, self.view.frame.size.width, 44);
                }
                else{
                    if (IS_IPHONE5)
                        toolbar.frame = CGRectMake(0, 352+88, self.view.frame.size.width, 44);
                    else
                        toolbar.frame = CGRectMake(0, 352, self.view.frame.size.width, 44);
                }
                NSMutableArray *items = [[NSMutableArray alloc] init];
               // [toolbar setBarStyle:UIBarStyleBlackOpaque];
                UIBarButtonItem *firstSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
                firstSpacer.width = 40;
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    firstSpacer.width = 180;
                }
                [items addObject:firstSpacer];
                
                
                UIButton *searchHistBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                searchHistBtn.frame = CGRectMake(0, 0, 125, 30);
                [searchHistBtn setTitle:@"Search/History" forState:UIControlStateNormal];
                [searchHistBtn setTitleColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]] forState:UIControlStateNormal];
                searchHistBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
                //[searchHistBtn setBackgroundImage:[UIImage imageNamed:@"SearchHistDown.png"] forState:UIControlStateHighlighted];
                //[searchHistBtn setBackgroundImage:[UIImage imageNamed:@"SearchHistUp.png"] forState:UIControlStateNormal];
                [searchHistBtn addTarget:self action:@selector(StopPressed) forControlEvents:UIControlEventTouchUpInside];
                UIBarButtonItem *searchHistBtnItem = [[UIBarButtonItem alloc] initWithCustomView:searchHistBtn];
                
                UIButton *lightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                lightBtn.frame = CGRectMake(0, 0, 49, 30);
                [lightBtn setTitle:@"Light" forState:UIControlStateNormal];
                [lightBtn setTitleColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]] forState:UIControlStateNormal];
                lightBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
               // [lightBtn setBackgroundImage:[UIImage imageNamed:@"LightBtnDown.png"] forState:UIControlStateHighlighted];
               // [lightBtn setBackgroundImage:[UIImage imageNamed:@"LightBtnUp.png"] forState:UIControlStateNormal];
                [lightBtn addTarget:self action:@selector(TorchPressed) forControlEvents:UIControlEventTouchUpInside];
                UIBarButtonItem *lightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:lightBtn];
                
                
                [searchHistBtnItem setStyle:UIBarButtonItemStyleBordered];
                [lightBtnItem setStyle:UIBarButtonItemStyleBordered];
                
                [items addObject:searchHistBtnItem];//[[UIBarButtonItem alloc] initWithTitle:@"Search / History" style:UIBarButtonItemStyleBordered target:self action:@selector(StopPressed)]];
                [items addObject:firstSpacer];
                [items addObject:lightBtnItem];
                [toolbar setItems:items animated:NO];
               // [items release];
               // [firstSpacer release];
                
                [self.view addSubview:toolbar];
                
                //End Chance's Edits_032912
                
                UIImageView *BottomOverlayImage = nil;
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    BottomOverlayImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ipadScanImageBottom.png"]];
                    BottomOverlayImage.frame = CGRectMake(0,toolbar.frame.origin.y-120,SCREEN_WIDTH,120);
                }
                
                else{
                    
                    BottomOverlayImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ScanNow_bottomScanOverlay_arrows.png"]];
                    if (IS_IPHONE5)
                        BottomOverlayImage.frame = CGRectMake(0,264+88,SCREEN_WIDTH,90);
                    else
                        BottomOverlayImage.frame = CGRectMake(0,264,SCREEN_WIDTH,90);

                }
                [self.view addSubview:BottomOverlayImage];
                ReleaseAndNilify(BottomOverlayImage);
               // [toolbar release];
               // [searchHistBtnItem release];
               // [lightBtnItem release];
            }
        }
        if (on == 0){
            StopButton.hidden=YES;
            // [StopButton setNeedsLayout];
            //		CloseButton.hidden=YES;
            //		[CloseButton setNeedsLayout];
            TorchButton.hidden = YES;
            //[TorchButton setNeedsDisplay];
            
            
        }
    }
//    else
//    {
//        if (on == 1){
//            
//            if (StopButton)
//            {
//                //Chances Edits
//                //StopButton.hidden = NO;
//                StopButton.hidden = YES;
//                [StopButton setNeedsLayout];
//                
//                UIImageView *TopOverlayImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ScanNow_topScanOverlay_arrows.png"]];
//                TopOverlayImage.frame = CGRectMake(0,-20,320,90);
//                [self.view addSubview:TopOverlayImage];
//                ReleaseAndNilify(TopOverlayImage);
//                
//                UIImageView *BottomOverlayImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ScanNow_bottomScanOverlay_arrows.png"]];
//                if (IS_IPHONE5)
//                    BottomOverlayImage.frame = CGRectMake(0,264+88,320,90);
//                else
//                    BottomOverlayImage.frame = CGRectMake(0,264,320,90);
//                [self.view addSubview:BottomOverlayImage];
//                ReleaseAndNilify(BottomOverlayImage);
//                
//                UIToolbar *toolbar = [[UIToolbar alloc] init];
//                if (IS_IPHONE5)
//                    toolbar.frame = CGRectMake(0, 352+88, self.view.frame.size.width, 44);
//                else
//                    toolbar.frame = CGRectMake(0, 352, self.view.frame.size.width, 44);
//                NSMutableArray *items = [[NSMutableArray alloc] init];
//                [toolbar setBarStyle:UIBarStyleBlackOpaque];
//                UIBarButtonItem *firstSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
//                firstSpacer.width = 40;
//                [items addObject:firstSpacer];
//                [items addObject:[[UIBarButtonItem alloc] initWithTitle:@"Search / History" style:UIBarButtonItemStyleBordered target:self action:@selector(StopPressed)]];
//                [toolbar setItems:items animated:NO];
//                [items release];
//                [firstSpacer release];
//                [self.view addSubview:toolbar];
//                [toolbar release];
//            }
//            
//            else{
//                StopButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//                [StopButton addTarget:self action:@selector(StopPressed) forControlEvents:UIControlEventTouchUpInside];
//                [StopButton setTitle:NSLocalizedString(@"Cancel",@"Cancel") forState:UIControlStateNormal];
//                StopButton.frame = CGRectMake(110,340,115,50);
//                [self.view addSubview:StopButton];
//                
//                StopButton.hidden = YES;
//                UIImageView *TopOverlayImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ScanNow_topScanOverlay_arrows.png"]];
//                TopOverlayImage.frame = CGRectMake(0,-20,320,90);
//                [self.view addSubview:TopOverlayImage];
//                ReleaseAndNilify(TopOverlayImage);
//                
//                UIImageView *BottomOverlayImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ScanNow_bottomScanOverlay_arrows.png"]];
//                
//                if (IS_IPHONE5){
//                    BottomOverlayImage.frame = CGRectMake(0,264+88,320,90);
//                    BottomOverlayImage.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
//                }
//                else
//                    BottomOverlayImage.frame = CGRectMake(0,264,320,90);
//                [self.view addSubview:BottomOverlayImage];
//                ReleaseAndNilify(BottomOverlayImage);
//                
//                UIToolbar *toolbar = [[UIToolbar alloc] init];
//                if (IS_IPHONE5)
//                    toolbar.frame = CGRectMake(0, 352+88, self.view.frame.size.width, 44);
//                else
//                    toolbar.frame = CGRectMake(0, 352, self.view.frame.size.width, 44);
//                NSMutableArray *items = [[NSMutableArray alloc] init];
//                [toolbar setBarStyle:UIBarStyleBlackOpaque];
//                UIBarButtonItem *firstSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
//                firstSpacer.width = 40;
//                [items addObject:firstSpacer];
//                [items addObject:[[UIBarButtonItem alloc] initWithTitle:@"Search / History" style:UIBarButtonItemStyleBordered target:self action:@selector(StopPressed)]];
//                [toolbar setItems:items animated:NO];
//                [items release];
//                [firstSpacer release];
//                [self.view addSubview:toolbar];
//                [toolbar release];
//                
//                //End Chance's Edits
//            }
//        }
//        if (on == 0){
//            StopButton.hidden=YES;
//            //[StopButton setNeedsLayout];
//            
//        }
//    }
    /*NSArray *imageArray = [[NSArray alloc] initWithObjects:
                           [UIImage imageNamed:@"scanAnim1.png"],
                           [UIImage imageNamed:@"scanAnim2.png"],
                           [UIImage imageNamed:@"scanAnim3.png"],
                           [UIImage imageNamed:@"scanAnim4.png"],
                           [UIImage imageNamed:@"scanAnim5.png"],
                           [UIImage imageNamed:@"scanAnim6.png"],
                           [UIImage imageNamed:@"scanAnim7.png"],
                           [UIImage imageNamed:@"scanAnim8.png"],
                           [UIImage imageNamed:@"scanAnim9.png"],
                           [UIImage imageNamed:@"scanAnim10.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"],
                           [UIImage imageNamed:@"scanAnim11.png"]
                           ,nil];
    
    scanAnim = [[UIImageView alloc] initWithFrame:
                CGRectMake(0, -15, 100, 44)];
    scanAnim.animationImages = imageArray;
    scanAnim.animationDuration = 1.5;
    scanAnim.contentMode = UIViewContentModeBottomLeft;
    [self.view addSubview:scanAnim];
    [imageArray release];
    [scanAnim startAnimating];*/
    
    
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate method implementation
-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (self.shouldSendReadBarcodeToDelegate == NO)
    {
        //this means we have already captured at least one event, then we don't want   to call the delegate again
        NSString *type = [[NSString alloc]init];
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];

        
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
             [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            type= @"QR Code";
            _isReading = NO;
            
            if(type)
                [defaults  setObject:type forKey:KEY_INIGMA_BARCODETYPE];
            
            if([metadataObj stringValue])
                [defaults  setObject:[metadataObj stringValue] forKey:KEY_INIGMA_BARCODE];
            
            DLog(@"LabelDecode_text:%@",[metadataObj stringValue]);//provide the barcode
            
            [HubCitiAppDelegate setBarCode:[metadataObj stringValue]];

            


            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
            
        }
        else if ([[metadataObj type] isEqualToString:AVMetadataObjectTypePDF417Code]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
//            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
//            [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
            type=@"PDF 417";
            _isReading = NO;
            
            if(type)
                [defaults  setObject:type forKey:KEY_INIGMA_BARCODETYPE];
            
            if([metadataObj stringValue])
                [defaults  setObject:[metadataObj stringValue] forKey:KEY_INIGMA_BARCODE];
            
            DLog(@"LabelDecode_text:%@",[metadataObj stringValue]);//provide the barcode
            
            [HubCitiAppDelegate setBarCode:[metadataObj stringValue]];
            
           
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
            
        }
        
        else if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeUPCECode]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
//            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
//            [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
            type =@"UPC-E";
            _isReading = NO;
            
            
            if(type)
                [defaults  setObject:type forKey:KEY_INIGMA_BARCODETYPE];
            
            if([metadataObj stringValue])
                [defaults  setObject:[metadataObj stringValue] forKey:KEY_INIGMA_BARCODE];
            
            DLog(@"LabelDecode_text:%@",[metadataObj stringValue]);//provide the barcode
            
            [HubCitiAppDelegate setBarCode:[metadataObj stringValue]];
            
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
            
        }
        else if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeCode39Code]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
//            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
//            [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
             type=@"Code 39";
            _isReading = NO;
            
            
            if(type)
                [defaults  setObject:type forKey:KEY_INIGMA_BARCODETYPE];
            
            if([metadataObj stringValue])
                [defaults  setObject:[metadataObj stringValue] forKey:KEY_INIGMA_BARCODE];
            
            DLog(@"LabelDecode_text:%@",[metadataObj stringValue]);//provide the barcode
            
            [HubCitiAppDelegate setBarCode:[metadataObj stringValue]];
            
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
            
        }
        else if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeCode39Mod43Code]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
//            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
           [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
//            [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
            
            _isReading = NO;
            if(type)
                [defaults  setObject:type forKey:KEY_INIGMA_BARCODETYPE];
            
            if([metadataObj stringValue])
                [defaults  setObject:[metadataObj stringValue] forKey:KEY_INIGMA_BARCODE];
            
            DLog(@"LabelDecode_text:%@",[metadataObj stringValue]);//provide the barcode
            
            [HubCitiAppDelegate setBarCode:[metadataObj stringValue]];
            
          
            
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
            
        }
        else if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeEAN13Code]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
//            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
//            [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
            
            
            NSString *barCodeString = nil;
                // Use first digit to differentiate between EAN13 and UPCA
                // An EAN13 barcode whose first digit is zero is exactly the same as a UPCA barcode.
                if ([[metadataObj stringValue] hasPrefix:@"0"] && [[metadataObj stringValue] length] > 1)
                {
                    barCodeString = [[metadataObj stringValue] substringFromIndex:1];
                    type = @"UPC-A";
                    DLog(@"barCodeString1:%@",barCodeString);
                    DLog(@"barCodeTypeString1:%@",type);
                    if(barCodeString)
                        [defaults  setObject:barCodeString forKey:KEY_INIGMA_BARCODE];
                    
                    DLog(@"LabelDecode_text:%@",barCodeString);//provide the barcode
                    
                    [HubCitiAppDelegate setBarCode:barCodeString];
                    
                    
                }
                else
                {
                    type = @"EAN 13";
                    DLog(@"barCodeString2:%@",barCodeString);
                    DLog(@"barCodeTypeString2:%@",type);
                    if([metadataObj stringValue])
                        [defaults  setObject:[metadataObj stringValue] forKey:KEY_INIGMA_BARCODE];
                    
                    DLog(@"LabelDecode_text:%@",[metadataObj stringValue]);//provide the barcode
                    
                    [HubCitiAppDelegate setBarCode:[metadataObj stringValue]];
                    
                   
                }
            
            if(type)
                [defaults  setObject:type forKey:KEY_INIGMA_BARCODETYPE];
            
            
            _isReading = NO;
            
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
            
        }
        else if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeEAN8Code]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
//            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
//            [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
             type=@"EAN 8";
            _isReading = NO;
            if(type)
                [defaults  setObject:type forKey:KEY_INIGMA_BARCODETYPE];
            
            if([metadataObj stringValue])
                [defaults  setObject:[metadataObj stringValue] forKey:KEY_INIGMA_BARCODE];
            
            DLog(@"LabelDecode_text:%@",[metadataObj stringValue]);//provide the barcode
            
            [HubCitiAppDelegate setBarCode:[metadataObj stringValue]];
            
          
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
            
        }
        else if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeCode93Code]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
//            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
//            [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
            type=@"Code 93";
            _isReading = NO;
            if(type)
                [defaults  setObject:type forKey:KEY_INIGMA_BARCODETYPE];
            
            if([metadataObj stringValue])
                [defaults  setObject:[metadataObj stringValue] forKey:KEY_INIGMA_BARCODE];
            
            DLog(@"LabelDecode_text:%@",[metadataObj stringValue]);//provide the barcode
            
            [HubCitiAppDelegate setBarCode:[metadataObj stringValue]];
            
         
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
            
        }
        
        
        /*
         
         
         
         
         
        
         
         type="Micro QR";
         type="GS1 OMNI";
         type="CODABAR NW7";
        
         
         */
        else if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeCode128Code]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
//            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
//            [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
             type=@"Code 128";
            _isReading = NO;
            self.view.layer.hidden = YES;
//             [self OnCameraStopOrStart:0];
            if(type)
                [defaults  setObject:type forKey:KEY_INIGMA_BARCODETYPE];
            
            if([metadataObj stringValue])
                [defaults  setObject:[metadataObj stringValue] forKey:KEY_INIGMA_BARCODE];
            
            DLog(@"LabelDecode_text:%@",[metadataObj stringValue]);//provide the barcode
            
            [HubCitiAppDelegate setBarCode:[metadataObj stringValue]];
           
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
            
        }
        else if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeAztecCode]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
//            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
//            [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
            
            _isReading = NO;
            if(type)
                [defaults  setObject:type forKey:KEY_INIGMA_BARCODETYPE];
            
            if([metadataObj stringValue])
                [defaults  setObject:[metadataObj stringValue] forKey:KEY_INIGMA_BARCODE];
            
            DLog(@"LabelDecode_text:%@",[metadataObj stringValue]);//provide the barcode
            
            [HubCitiAppDelegate setBarCode:[metadataObj stringValue]];
            
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
            
        }
        
        
        
        
        else if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeITF14Code]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
            //            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            //            [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
            
            _isReading = NO;
            if(type)
                [defaults  setObject:type forKey:KEY_INIGMA_BARCODETYPE];
            
            if([metadataObj stringValue])
                [defaults  setObject:[metadataObj stringValue] forKey:KEY_INIGMA_BARCODE];
            
            DLog(@"LabelDecode_text:%@",[metadataObj stringValue]);//provide the barcode
            
            [HubCitiAppDelegate setBarCode:[metadataObj stringValue]];
            
          
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
            
        }
        
        
        else if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeDataMatrixCode]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
            //            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            //            [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
            type=@"Data Matrix";
            _isReading = NO;
            if(type)
                [defaults  setObject:type forKey:KEY_INIGMA_BARCODETYPE];
            
            if([metadataObj stringValue])
                [defaults  setObject:[metadataObj stringValue] forKey:KEY_INIGMA_BARCODE];
            
            DLog(@"LabelDecode_text:%@",[metadataObj stringValue]);//provide the barcode
            
            [HubCitiAppDelegate setBarCode:[metadataObj stringValue]];
            
                      // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
            
        }
        
        
        
        else if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeInterleaved2of5Code]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
            //            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            //            [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
             type=@"2 of 5";
            _isReading = NO;
            if(type)
                [defaults  setObject:type forKey:KEY_INIGMA_BARCODETYPE];
            
            if([metadataObj stringValue])
                [defaults  setObject:[metadataObj stringValue] forKey:KEY_INIGMA_BARCODE];
            
            DLog(@"LabelDecode_text:%@",[metadataObj stringValue]);//provide the barcode
            
            [HubCitiAppDelegate setBarCode:[metadataObj stringValue]];
            
          
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
            
        }
    }
    
    

     
        self.shouldSendReadBarcodeToDelegate = YES;
    }
    else
    {
        self.shouldSendReadBarcodeToDelegate = YES;
        //Your code for calling  the delegate should be here
    }
}




@end

//
//  ScanHistoryViewController.h
//  Scansee
//
//  Created by developer_span on 19/08/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import <UIKit/UIKit.h>

typedef enum webServices{
    getscanhistory
}WebServiceType;

@class CommonUtility;
@interface ScanHistoryViewController : UIViewController<UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource,CustomizedNavControllerDelegate> {

	CommonUtility *common;
	NSMutableArray *scannnedDateArray;
	NSMutableArray *scannedProductName[NUM_CATEGORIES] ,*scannedProductImage[NUM_CATEGORIES],*scannedProductId[NUM_CATEGORIES];
	NSMutableString *latValue1 ,*longValue1;
    IBOutlet UITableView *historyTable;
    
    int lastVisitedRecord;
    int nextPage;
    NSMutableArray *rowNumberArray;
    
    WebServiceType iWebServiceType;
}

@property(nonatomic,strong)NSMutableString *latValue1;
@property(nonatomic,strong)NSMutableString *longValue1;
@property(nonatomic,strong) IBOutlet UITableView *historyTable;


//-(void)  getScanHistoryItems ;
-(void)fetchPreviousHistoryItems:(id)sender;
-(void)fetchNextHistoryItems:(id)sender;
-(void) refreshTable;
@end

//
//  SpecialOffersViewController.m
//  Scansee
//
//  Created by ajit on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SpecialOffersViewController.h"
#import "Config.h"
#import "MainMenuViewController.h"
#import "AppDelegate.h"
#import "WebBrowserViewController.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "EventListDetailViewController.h"
#import "CurrentSpecialsViewController.h"
#import "CurrentSpecials.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
NSDictionary *viewDictionary;
#define kAlertViewEmail 10000
@interface SpecialOffersViewController ()

@end

@implementation SpecialOffersViewController
@synthesize activityIndicator,isEventLogisticsFlag,isAppsiteLogisticsFlag;
@synthesize webView = mWebView;
@synthesize toolbar = mToolbar;
@synthesize back = mBack;
@synthesize forward = mForward;
@synthesize refresh = mRefresh;
@synthesize stop = mStop;
@synthesize anyVC,emailSendingVC,fromMainmenu;

UITextView *textView;
UIButton *button;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)OkClicked:(id)sender
{
    [textView removeFromSuperview];
    [button removeFromSuperview];
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewDidLoad
{
    self.view.userInteractionEnabled = TRUE;
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    configuration.allowsInlineMediaPlayback = YES;
    configuration.allowsAirPlayForMediaPlayback = YES;
    configuration.requiresUserActionForMediaPlayback = YES;
    configuration.allowsPictureInPictureMediaPlayback = YES;
    cusNav =(CustomizedNavController *) self.navigationController;
    
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
  
    CGRect frame =CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y, SCREEN_WIDTH, SCREEN_HEIGHT-44);
    self.webView = [[WKWebView alloc] initWithFrame:frame configuration:configuration];
   
    
    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;
  self.webView.allowsBackForwardNavigationGestures = YES;
 
    NSAssert(self.back, @"Unconnected IBOutlet 'back'");
    NSAssert(self.forward, @"Unconnected IBOutlet 'forward'");
    NSAssert(self.refresh, @"Unconnected IBOutlet 'refresh'");
    NSAssert(self.stop, @"Unconnected IBOutlet 'stop'");
   
    
    if (shareFlag==YES) {
        //        shareFlag=false;
        UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        shareButton.frame=CGRectMake(0, 0, 20, 18);
        
       
        //customize back button
        UIButton *backBtn = [UtilityManager customizeBackButton];
        [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        //        self.navigationItem.leftItemsSupplementBackButton=YES;
        self.navigationItem.leftBarButtonItems=@[back];
        self.navigationItem.hidesBackButton = YES;
        
        
    }
    
   
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    
    
    if ([defaults boolForKey:@"trainingSlides"]) {
         self.navigationItem.rightBarButtonItem = nil;
    }
    else{
         self.navigationItem.rightBarButtonItem = mainPage;
    }
    
    ////[mainPage release];
    
    
    
    
    //customize back button
    UIButton *backBtn = [UtilityManager customizeBackButton];
    
    [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = back;
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    if (isEventLogisticsFlag) {
        self.title = @"Maps / Event Logistics";
        crossBtn.hidden = NO;
        
    }
    else if(isAppsiteLogisticsFlag){
        self.title = @"Maps / AppSite Logistics";
        crossBtn.hidden = NO;
    }
    
    else{
        crossBtn.hidden = YES;
        mToolbar.hidden = YES;
        
        self.navigationItem.title = @"Details";
        
    }
    
   //[defaults setValue:@"http://66.228.143.27:8080/SSQR/qr/3000.htm?logisticsId=86&hubcitiId=10&oType=landscape" forKey:KEY_URL];
    
    if ([[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
    {
        crossBtn.hidden = NO;
    }
    //has been changed only for this location - as in DB they have not appended "http", it wont load
    NSMutableString *urlString;
    urlString =[NSMutableString stringWithFormat:@"%@",[defaults valueForKey:KEY_URL]];
    
    NSString *string2;
    if ([urlString length]>7) {
        string2 = [urlString substringWithRange: NSMakeRange (0, 7)];
    }else
        string2 = [urlString copy];
    
    DLog(@"String2:%@", string2);
    
    if ([string2 isEqualToString:@"http://"] || [string2 isEqualToString:@"https:/"] ){
        
        urlString = [NSMutableString stringWithFormat:@"%@",[defaults valueForKey:KEY_URL]];
    }
    else{
        urlString = [NSMutableString stringWithString:@"http://"];
        [urlString appendFormat:@"%@",[defaults valueForKey:KEY_URL]];
        
    }
    
    
    
    //check for give away url and display 'enter now' button at the bottom
    if ([urlString hasSuffix:@"&scantypeid=1"]){
        
        //http://www.ScanSee.net/SSQR/qr/2300.htm?key1=1000&key2=2000
        self.webView.frame = CGRectMake(0.0, 0.0, 320.0, self.view.frame.size.height-44.0);
        enterNow.hidden = NO;
        enterNow.enabled = YES;
    }
    else{
        enterNow.hidden = YES;
        if ( isEventLogisticsFlag){
            //            self.webView.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, self.view.frame.size.height-44);
        }
        else{
            if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
            {
                self.webView.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, self.view.frame.size.height);
            }
            else
                self.webView.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, self.view.frame.size.height);
        }
        
    }
    
    NSString *urlAddress = [NSString stringWithString:urlString];
    _Oldrequest = [[NSString alloc]initWithString:urlAddress];
    
    //Create a url object
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    //url request object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];

    
    //load the request in the UIWebView
    [self.webView loadRequest:requestObj];
     [self.view addSubview:self.webView];
    
    activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    activityIndicator.center = CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
    
    crossBtn.frame = CGRectMake(SCREEN_WIDTH -( [[defaults valueForKey:KEY_URL] containsString:@"Portrait"]? VARIABLE_WIDTH(40): VARIABLE_WIDTH(30)),self.view.frame.origin.y + VARIABLE_HEIGHT(10), 40, 40);
    
    [activityIndicator startAnimating];
    activityIndicator.hidden = FALSE;
    
    [self.webView addSubview:activityIndicator];
    [self.webView addSubview:crossBtn];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoNowVisible:) name:@"UIMoviePlayerControllerDidEnterFullscreenNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoNowHidden:) name:@"UIMoviePlayerControllerDidExitFullscreenNotification" object:nil];
    
    
    self.view.backgroundColor = [UIColor whiteColor]/* [UIColor colorWithRGBInt:0xf2b32e]*/;
    [self updateButtons];
    
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [HubCitiAppDelegate setIsVideoOrientationcalled:NO];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
    {
        [HubCitiAppDelegate application:[UIApplication sharedApplication] supportedInterfaceOrientationsForWindow:[HubCitiAppDelegate window]];
    }
    if (shareFlag==YES) {
        [self addShareButton];
    }
}


- (void)videoNowVisible:(NSNotification *)notification
{
    [HubCitiAppDelegate setIsVideoOrientationcalled:YES];
}

#pragma adding share button

-(void)addShareButton{
    // Create down menu button
    UIImageView *homeLabel ;
    
    
    // Create up menu button
    homeLabel = [self createHomeButtonView];
    if ([defaults boolForKey:@"trainingSlides"]) {
        homeLabel.hidden = true;
    }
    else{
         homeLabel.hidden = false;
    }
    DWBubbleMenuButton *upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
                                                                                          self.view.frame.size.height - homeLabel.frame.size.height - 60.f,
                                                                                          homeLabel.frame.size.width,
                                                                                          homeLabel.frame.size.height)
                                                            expansionDirection:DirectionUp];
    upMenuView.homeButtonView = homeLabel;
    
    upMenuView.delegate = self;
    
    [upMenuView addButtons:[self createDemoButtonArray]];
    
    [self.view addSubview:upMenuView];
    
    [self.view bringSubviewToFront:upMenuView];
    
    upMenuView.onLogOutPressed =^(){
        
        [self logOutPressed];
    } ;
    
    
}




- (UIImageView *)createHomeButtonView {
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
    label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}

- (NSArray *)createDemoButtonArray {
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    
    int i = 0;
    for (NSString *title in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:title] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        
        button.clipsToBounds = YES;
        button.tag = i++;
        
        [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
}

- (void)test:(UIButton *)sender {
    
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
      [self shareClicked];
    
    switch (sender.tag) {
        case 0:{//facebook
          
            anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
            //[self.view addSubview:anyVC.view];
            anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
            [UIView transitionWithView:self.view duration:0.5
                               options:UIViewAnimationOptionTransitionCurlDown
                            animations:^ { [self.view addSubview:anyVC.view]; }
                            completion:nil];
        }
            break;
        case 1:{//twitter
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    if (result == SLComposeViewControllerResultCancelled) {
                        DLog(@"Twitter Result: canceled");
                    } else {
                        DLog(@"Twitter Result: sent");
                        NSMutableString *userXmlStr;
                        if (shareFlag==TRUE) {
                            userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><pageId>%@</pageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PAGEID]];
                        }
                        else {
                            userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><sPageId>%@</sPageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PAGEID]];
                        }
                        
                        
                        [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
                    }
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        
                        [controller.view removeFromSuperview];
                    } else
                        [controller dismissViewControllerAnimated:YES completion:Nil];
                };
                controller.completionHandler =myBlock;
                
                //Adding the Text to the facebook post value from iOS
                [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                
                //Adding the URL to the facebook post value from iOS
                
                //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                
                //Adding the Image to the facebook post value from iOS
                
                [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    [self.view addSubview:controller.view];
                    [self.view bringSubviewToFront:controller.view];
                }else
                    [self presentViewController:controller animated:YES completion:Nil];
            }
            else {
                [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
               
            }
        }
            
            
            break;
        case 2:{//text
            
            if ([self hasCellularCoverage])
            {
                if ([MFMessageComposeViewController canSendText]) {
                    
                    MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
                    [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    smsComposerController.messageComposeDelegate = self;
                    [self presentViewController:smsComposerController animated:YES completion:nil];
                }
            }
            else
            {
                [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
               
            }
            
            
        }
            break;
        case 3:{//email
            //[defaults setValue:PRODUCT_SHARE forKey:KEY_SHARE_TYPE];
            __typeof(self) __weak  obj = self;
            emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
            emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
            emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                
                [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
            } ;
            
            [emailSendingVC loadMail];
        }
            break;
            
        default:
            [defaults setBool:NO forKey:BottomButton];
            break;
            
    }
    
}

- (UIButton *)createButtonWithName:(NSString *)imageName {
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button sizeToFit];
    
    [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}



- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView{
    
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

- (void)videoNowHidden:(NSNotification *)notification
{
    UIViewController *c = [[UIViewController alloc]init];
    [self presentViewController:c animated:NO completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)popBackToPreviousPage{
    
    [self.webView loadHTMLString:@"" baseURL:nil];
    [HubCitiAppDelegate setIsVideoOrientationcalled:NO];
    [HubCitiAppDelegate setIsLogistics:NO];
    if (fromCurrentSpecials) {
        fromCurrentSpecials = FALSE;
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        for (UIViewController *aViewController in allViewControllers) {
            if ([aViewController isKindOfClass:[CurrentSpecialsViewController class]]) {
                [self.navigationController popToViewController:aViewController animated:NO];
            }
        }
    }
    else{
        [self.navigationController popViewControllerAnimated:NO];
    }
    
}

#pragma webkit delegates

- (IBAction)webViewBack:(id)sender {
     [self.webView goBack];
}
- (IBAction)webViewCancel:(id)sender {
     [self.webView stopLoading];
}

- (IBAction)webViewRefresh:(id)sender {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.webView.URL];
    [self.webView loadRequest:request];
}
- (IBAction)webViewForward:(id)sender {
    [self.webView goForward];
}

-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    [activityIndicator startAnimating];
    activityIndicator.hidden = FALSE;
    [self updateButtons];
}
-(void) webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    
        
        [activityIndicator stopAnimating];
        activityIndicator.hidden = TRUE;
        [self updateButtons];
}


-(void) webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    DLog(@"%@",error);
    [activityIndicator stopAnimating];
    activityIndicator.hidden = TRUE;
    [self updateButtons];
}

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction *action) {
                                                          completionHandler();
                                                      }]];
    [self presentViewController:alertController animated:YES completion:^{}];
}

-(void) webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    [activityIndicator startAnimating];
    activityIndicator.hidden = FALSE;
    if ([[[navigationAction.request URL] scheme] isEqual:@"mailto"])
    {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[navigationAction.request URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
               // return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,
                                                                                                      (CFStringRef)value,
                                                                                                      CFSTR("")));
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            // [mailViewController release];
           // return NO;
            
        }
        
        else {
            [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
            
            
            
        }
        
       // return NO;
        
        
        
        
        
        
        
        
        
    }
    if (!navigationAction.targetFrame) {
        [activityIndicator stopAnimating];
        activityIndicator.hidden = TRUE;
        
        
        NSURL *url = navigationAction.request.URL;
        
        if( [[url absoluteString] containsString:@"itunes.apple.com"]){
            UIApplication *app = [UIApplication sharedApplication];
                    if ([app canOpenURL:url]) {
                        [app openURL:url];
                    }
            decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }
        else{
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [self.webView loadRequest:request];
        }

        
    }
   // return YES;
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}



-(void)returnToMainPage:(id)sender
{
    if (isEventLogisticsFlag) {
        isEventLogisticsFlag=0;
    }
    else if (isAppsiteLogisticsFlag) {
        isAppsiteLogisticsFlag=0;
    }
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
    
}


- (IBAction)crossClicked:(id)sender {
   
    [HubCitiAppDelegate setIsLogistics: NO];
    if(fromMainmenu){
        if (self.didDismiss)
            self.didDismiss(@"some extra data");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)viewDidUnload
{
    self.webView = nil;
    self.toolbar = nil;
    self.back = nil;
    self.forward = nil;
    self.refresh = nil;
    self.stop = nil;
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)updateButtons
{
    self.forward.enabled = self.webView.canGoForward;
    self.back.enabled = self.webView.canGoBack;
    self.stop.enabled = self.webView.loading;
}


-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
}



-(void) viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    if(isEventLogisticsFlag||isAppsiteLogisticsFlag){
    self.webView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y, SCREEN_WIDTH, SCREEN_HEIGHT- 44);
    }
        else{
            if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
            {
                self.webView.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, SCREEN_HEIGHT);
            }
            else
                self.webView.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, SCREEN_HEIGHT);
        }
    
    //update the UI on rotation
    
    activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    activityIndicator.center = CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
    crossBtn.frame = CGRectMake(SCREEN_WIDTH- ( [[defaults valueForKey:KEY_URL] containsString:@"Portrait"]? VARIABLE_WIDTH(40): VARIABLE_WIDTH(30)),self.view.frame.origin.y + VARIABLE_HEIGHT(10), 40, 40);
    
}
#pragma orientation functions

- (BOOL)shouldAutorotate
{
    
    return NO;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    if ([[defaults valueForKey:KEY_URL] containsString:@"Portrait"]) {
        
        return UIInterfaceOrientationPortrait;
    }
    else
        return UIInterfaceOrientationLandscapeRight;
    
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    
    if ([[defaults valueForKey:KEY_URL] containsString:@"Portrait"]) {
        return  UIInterfaceOrientationMaskPortrait;
    }
    else if (isEventLogisticsFlag) {
        return UIInterfaceOrientationMaskLandscape;
        
    }
    else if (isAppsiteLogisticsFlag) {
        return UIInterfaceOrientationMaskLandscape;
        
    }
    else
        return UIInterfaceOrientationMaskPortrait;
    
}

#pragma mark sender user tracking
-(void)userTracking:(NSString*)xmlStr
{
    
    NSMutableString *urlStr = [[NSMutableString alloc]init];
    [urlStr appendFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    DLog(@"%@",urlStr);
    NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:xmlStr]];
    DLog(@"%@",response);
    // [xmlStr release];
}

#pragma mark share methods

-(void)shareClicked
{
    iWebRequestState = SPECIAL_OFFER_SHARE;
    
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    
    [reqStr appendFormat:@"<ShareProductInfo><retailerId>%@</retailerId>",[defaults valueForKey:KEY_RETAILERID ]];
    [reqStr appendFormat:@"<pageId>%@</pageId><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_PAGEID],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharespecialoff",BASE_URL];
    DLog(@"%@",urlString);
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    
    [self responseData:response];
    
    //[reqStr release];
    
}


-(void)shareDetails{
    
    [self shareClicked];
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        return;
    
    // [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    [self showActionSheet];
    
    
}

-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Special Offer Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    //    if(IPAD){
    //        actionSheet.popoverPresentationController.sourceView = btnCalendar;
    //        actionSheet.popoverPresentationController.sourceRect =  btnCalendar.bounds;
    //    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
                NSMutableString *userXmlStr;
                if (shareFlag==TRUE) {
                    userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><pageId>%@</pageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PAGEID]];
                }
                else {
                    userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><sPageId>%@</sPageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PAGEID]];
                }
                
                
                [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
       
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
       
    }
    
    
}

-(void) emailClick{//email
    //[defaults setValue:PRODUCT_SHARE forKey:KEY_SHARE_TYPE];
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
}




-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
          
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
            
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark parse method
-(void)responseData: (NSString*)response{
    
    switch (iWebRequestState) {
            
        case SPECIAL_OFFER_SHARE:
            [self parse_SpecialOfferShare:response];
            break;
        default:
            break;
    }
    
    
}

-(void)parse_SpecialOfferShare:(NSString*)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        //        TBXMLElement *retailerDetails = [TBXML childElementNamed:@"RetailersDetails" parentElement:tbXml.rootXMLElement];
        
        
        TBXMLElement *shareText=[TBXML childElementNamed:@"pageTitle" parentElement:tbXml.rootXMLElement];
        TBXMLElement *emailURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *imagePath = [TBXML childElementNamed:@"retImage" parentElement:tbXml.rootXMLElement];
        
        if (emailURL!=nil) {
            [defaults setValue:[TBXML textForElement:emailURL] forKey:KEY_HYPERLINK];
            //                NSLog(@"The link to be shared is %@",KEY_HYPERLINK);
        }
        
        if (shareText!=nil) {
            NSMutableString *msgString = [[NSMutableString alloc]init];
            if (shareFlag==YES) {
                [msgString appendFormat:@"I found this special offer page in the %@ mobile app and thought you might be interested:",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            }
            else
            {
                [msgString appendFormat:@"I found this anything page in the %@ mobile app and thought you might be interested:",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            }
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [msgString appendFormat:@"%@",shareString];
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            
        }
        
        if (imagePath!=nil) {
            [defaults setObject:[TBXML textForElement:imagePath] forKey:KEY_PRODUCTIMGPATH];
            //                NSLog(@"The product image path is %@",KEY_PRODUCTIMGPATH);
        }
        
        
        
        
        
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            
            return;
        }
        
    }
    
}



@end

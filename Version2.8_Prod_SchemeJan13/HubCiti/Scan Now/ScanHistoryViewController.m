//
//  ScanHistoryViewController.m
//  Scansee
//
//  Created by developer_span on 19/08/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ScanHistoryViewController.h"

#import "MainMenuViewController.h"
#import "CommonUtility.h"
#import "ProductPage.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@implementation ScanHistoryViewController{
    UIActivityIndicatorView *loading;
    CustomizedNavController *cusNav;
}

@synthesize latValue1 , longValue1,historyTable;
int count = 0, count1 = 0;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    loading = nil;
    common = [[CommonUtility alloc]init];
    
    //customize back button
    

    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    ////[mainPage release];
    
    
    lastVisitedRecord = 0;
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"History" forView:self withHambergur:YES];
    //self.title = @"History";
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: NO];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    scannnedDateArray = [[NSMutableArray alloc ] init];
    [self request_getscanhistory];
    
    self.view.backgroundColor = [UIColor colorWithRGBInt:0xf00a951];
    
    self.navigationController.navigationBar.hidden = NO;
    //self.navigationItem.hidesBackButton = NO;
    self.view.hidden = NO;
    
}
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)request_getscanhistory {
    
    iWebServiceType = getscanhistory;
    NSString *urlString = [NSString stringWithFormat:@"%@scannow/getscanhistory?userId=%@&lastVisitedRecord=%d&hubCitiId=%@",BASE_URL,[defaults objectForKey:KEY_USERID],lastVisitedRecord,[defaults valueForKey:KEY_HUBCITIID]];
    
    
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishGetConnection:urlString];
        
        
        [self parse_getscanhistory:response];
        
    }
    else{
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
    
    
    //[ConnectionManager establishConnectionForGet:urlString withDelegate:self];
}

-(void)responseData:(NSString*)response{
    
    switch (iWebServiceType) {
        case getscanhistory:
            [self parse_getscanhistory:response];
            break;
            
        default:
            break;
    }
}

-(void) parse_getscanhistory : (NSString *) respStr {
    
    
    if ([UtilityManager isResponseXMLNullOrEmpty:respStr])
        return;
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:respStr];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbXml.rootXMLElement];
        if (nextPageElement!= nil)
            nextPage = [[TBXML textForElement:nextPageElement]intValue];
        
        TBXMLElement *productHistoryInfoElement = [TBXML childElementNamed:@"productHistoryInfo" parentElement:tbXml.rootXMLElement];
        //	if (scanDateInfoElement != nil) {
        int i = 0;
        
        if ([rowNumberArray count]) {
            [rowNumberArray removeAllObjects];
        }
        
        if ([scannnedDateArray count]) {
            [scannnedDateArray removeAllObjects];
        }
        rowNumberArray = [[NSMutableArray alloc]init];
        
        TBXMLElement *scanHistoryInfoElement = [TBXML childElementNamed:@"ScanHistoryInfo" parentElement:productHistoryInfoElement];
        
        while (scanHistoryInfoElement != nil)
        {
            
            TBXMLElement *dateScannedElement = [TBXML childElementNamed:@"dateScanned" parentElement:scanHistoryInfoElement];
            [scannnedDateArray addObject:[TBXML textForElement:dateScannedElement]];
            scannedProductName[i] = [[NSMutableArray alloc] init];
            scannedProductImage[i] = [[NSMutableArray alloc] init];
            scannedProductId[i] = [[NSMutableArray alloc] init];
            
            TBXMLElement *ProductDetailsElement = [TBXML childElementNamed:@"ProductDetails" parentElement:scanHistoryInfoElement];
            if (ProductDetailsElement != nil) {
                
                TBXMLElement *ProductDetailElement = [TBXML childElementNamed:KEY_PRODUCTDETAIL parentElement:ProductDetailsElement];
                while (ProductDetailElement != nil) {
                    TBXMLElement *productNameElement = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:ProductDetailElement];
                    
                    TBXMLElement *imageElement = [TBXML childElementNamed:KEY_PRODUCTIMGPATH parentElement:ProductDetailElement];
                    
                    TBXMLElement *productIdElement = [TBXML childElementNamed:KEY_PRODUCTID parentElement:ProductDetailElement];
                    TBXMLElement *rowNumberElement = [TBXML childElementNamed:@"rowNumber" parentElement:ProductDetailElement];
                    
                    [scannedProductName[i] addObject:[TBXML textForElement:productNameElement]];
                    [scannedProductImage[i] addObject:[TBXML textForElement:imageElement]];
                    [scannedProductId[i] addObject:[TBXML textForElement:productIdElement]];
                    [rowNumberArray addObject:[TBXML textForElement:rowNumberElement]];
                    
                    ProductDetailElement = [TBXML nextSiblingNamed:KEY_PRODUCTDETAIL searchFromElement:ProductDetailElement];
                }
            }
            i++;
            scanHistoryInfoElement = [TBXML nextSiblingNamed:@"ScanHistoryInfo" searchFromElement:scanHistoryInfoElement];
            
        }
        
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        [loading stopAnimating];
        ReleaseAndNilify(loading);
        
    }
    
    
    [historyTable reloadData];
    [defaults setBool:NO forKey:@"ViewMore"];
}




-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [common popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////SmartSearch Implementation Start/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableView delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [scannnedDateArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (((nextPage == 1 || lastVisitedRecord!=0)) && ([scannnedDateArray count]-1 == section)){
        
        return [scannedProductName[section] count] + 1;
    }
    
    //DLog(@"No. of rows in Scan History:%i",[scannedProductName[section] count]);
    return [scannedProductName[section] count];//[recordArray count];
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == ([scannnedDateArray count] - 1) && indexPath.row == [scannedProductName[indexPath.section] count]) {
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"] && lastVisitedRecord!=0) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self fetchNextHistoryItems:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [historyTable reloadData];
                });
            });
            
        }
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //	if (indexPath.section == ([scannnedDateArray count] - 1) && indexPath.row == [scannedProductName[indexPath.section] count]){
    //        [self fetchNextHistoryItems:nil];
    //		return;
    //	}
    [defaults setBool:NO forKey:@"ViewMore"];
    // else{
    [[HubCitiManager sharedManager]setShareFromTL:NO];
    [defaults  setObject:[scannedProductId[indexPath.section] objectAtIndex:indexPath.row] forKey:KEY_PRODUCTID];
    if ([scannedProductImage[indexPath.section] objectAtIndex:indexPath.row] != nil) {
        [defaults  setValue:[scannedProductImage[indexPath.section] objectAtIndex:indexPath.row] forKey:@"PRODIMG"];
    }
    else {
        [defaults  setValue:nil forKey:@"PRODIMG"];
    }
    [defaults setValue:@"0" forKey:KEY_SCANTYPEID];
    
    [defaults  setObject:nil forKey:KEY_PRODLISTID];
    
    
    ProductPage *prodPage = [[ProductPage alloc]initWithNibName:@"ProductPage" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:prodPage animated:NO];
    //[prodPage release];
    
    [historyTable deselectRowAtIndexPath:[historyTable indexPathForSelectedRow] animated:YES];
    // }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width,30.0)];
    
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 310, 20)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:15.0];
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.text = [scannnedDateArray objectAtIndex:section];
    [headerView addSubview:headerLabel];
    // [headerLabel release];
    [headerView setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
    return headerView;
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
	return [scannnedDateArray objectAtIndex:section];
 }*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (scannedProductName[indexPath.section] == nil) {
        static NSString *CellIdentifier1 = @"Cell";
        UITableViewCell *cell1 ; //= [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
        return cell1;
    }
    
    static NSString *kCustomCellID = @"MyCellID";
    
    //	tableView.backgroundColor = [UIColor clearColor];
    UILabel *tx_label = [[UILabel alloc] initWithFrame:CGRectMake(60, 15, 250, 30)];
    UITableViewCell *cell ;//= [tableView dequeueReusableCellWithIdentifier:kCustomCellID];
    //	if (cell == nil)
    //	{
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCustomCellID];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    //cell.selectedBackgroundView = [[[UIView alloc] initWithFrame:CGRectZero] ;
    //cell.selectedBackgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"hvrBgBig.png"]];
    
    //	}
    if (indexPath.section == ([scannnedDateArray count] - 1) && indexPath.row == [scannedProductName[indexPath.section] count] ){
        if (nextPage == 1) {
            loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            loading.center = cell.contentView.center;
            loading.color = [UIColor blackColor];
            
            
            [cell.contentView addSubview:loading];
            [loading startAnimating];
            cell.userInteractionEnabled = NO;
            //            CGRect frame;
            //            frame.origin.x = 0;
            //            frame.origin.y = 16;
            //            frame.size.width = 320;
            //            frame.size.height = 24;
            //            UILabel *label = [[[UILabel alloc] initWithFrame:frame] ;
            //            label.font = [UIFont boldSystemFontOfSize:16];
            //            label.textAlignment = NSTextAlignmentCenter;
            //            label.textColor = [UIColor colorWithRGBInt:0x112e72];
            //            label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
            //            [cell.contentView addSubview:label];
        }
        
        //cell.selectionStyle = UITableViewCellSelectionStyleNone;
        ReleaseAndNilify(tx_label);
        return cell;
    }
    else{
        
        //NSURL *url = [NSURL URLWithString:[scannedProductImage[indexPath.section] objectAtIndex:indexPath.row]];
        
        SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(5, 5, 50, 50)];
        asyncImageView.backgroundColor = [UIColor clearColor];
        [asyncImageView loadImage:[scannedProductImage[indexPath.section] objectAtIndex:indexPath.row]];
        [cell addSubview:asyncImageView];
        //[asyncImageView release];
        
        tx_label.font = [UIFont boldSystemFontOfSize:13];
        tx_label.numberOfLines = 2;
        [tx_label setLineBreakMode:NSLineBreakByWordWrapping];
        tx_label.text = [scannedProductName[indexPath.section] objectAtIndex:indexPath.row];//productName;
        [cell addSubview:tx_label];
        //  [tx_label release];
        return cell;
    }
}


-(void)fetchPreviousHistoryItems:(id)sender{
    
    sender = (UIButton *)sender;
    
    if (nextPage != 1) {
        count-= count1;
        lastVisitedRecord = count;
    }
    else{
        count-= [rowNumberArray count];
        lastVisitedRecord = count;
        
    }
    
    [self refreshTable];
}

-(void)fetchNextHistoryItems:(id)sender{
    
    sender = (UIButton *)sender;
    
    // count1 = [rowNumberArray count];
    //count += [rowNumberArray count];
    lastVisitedRecord = [[rowNumberArray objectAtIndex:[rowNumberArray count]-1]intValue];
    
    //[self refreshTable];
    [self request_getscanhistory];
    [historyTable reloadData];
}

-(void) refreshTable {
    
    for (int i = 0; i < [scannnedDateArray count]; i++) {
        //[scannedProductName[i] release];
        scannedProductName[i] = nil;
        //[scannedProductImage[i] release];
        scannedProductImage[i] = nil;
        //[scannedProductId[i] release];
        scannedProductId[i] = nil;
    }
    [scannnedDateArray removeAllObjects];
    [rowNumberArray removeAllObjects];
    
    [self request_getscanhistory];
    
    [historyTable reloadData];
    [historyTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void) viewWillDisappear:(BOOL)animated{
    
    /*for (int i = 0; i < [scannnedDateArray count]; i++) {
     [scannedProductName[i] release];
     scannedProductName[i] = nil;
     [scannedProductImage[i] release];
     scannedProductImage[i] = nil;
     [scannedProductId[i] release];
     scannedProductId[i] = nil;
     }
     [scannnedDateArray removeAllObjects];*/
    
    [super viewWillDisappear:YES];
    
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}

- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end

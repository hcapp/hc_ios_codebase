//
//  SpecialOffersViewController.h
//  Scansee
//
//  Created by ajit on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Twitter/Twitter.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <Foundation/Foundation.h>
#import "DWBubbleMenuButton.h"
#import <WebKit/WebKit.h>

@class AnyViewController;
@class EmailShareViewController;
//#import "AnyViewController.h"
//#import "EmailShareViewController.h"
//#import "FTShare.h"

@interface SpecialOffersViewController : UIViewController<WKNavigationDelegate,WKUIDelegate, MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,DWBubbleMenuViewDelegate,CustomizedNavControllerDelegate>
{
    
//    AnyViewController *anyVC;
    WKWebView *mWebView;
	IBOutlet UIActivityIndicatorView *activityIndicator;
       
    IBOutlet UIButton *crossBtn;
    IBOutlet UIButton *enterNow;
    NSString *_Oldrequest;
    WebRequestState iWebRequestState;
    CustomizedNavController *cusNav;
   
}
@property (nonatomic, copy) void (^didDismiss)(NSString *data);
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic, strong) WKWebView* webView;
@property (nonatomic, strong) IBOutlet UIToolbar* toolbar;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* back;
@property (weak, nonatomic) IBOutlet UIButton *crossMark;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* forward;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* refresh;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* stop;
@property(nonatomic,strong)UIActivityIndicatorView *activityIndicator;
@property(nonatomic)BOOL isEventLogisticsFlag;
@property(nonatomic)BOOL isAppsiteLogisticsFlag;
@property(nonatomic)BOOL fromMainmenu;

- (IBAction)crossClicked:(id)sender;

-(IBAction)facebookClicked:(id)sender;
-(IBAction)twitterClicked:(id)sender;
-(IBAction)smsClicked:(id)sender;
-(IBAction)emailClicked:(id)sender;
-(IBAction)enterNowTapped:(id)sender;
@end

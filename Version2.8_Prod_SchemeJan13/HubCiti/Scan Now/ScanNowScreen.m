//
//  ScanNowScreen.m
//  ScanSee
//  This sceen is called for RedLaser sdk
//  Created by ajit on 5/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "ScanNowScreen.h"
#import "CLLocation (Strings).h"
#import "ScanHistoryViewController.h"
#import "i_nigmaSDKTestViewController.h"
#import "MainMenuViewController.h"
#import "CommonUtility.h"
#import "ProductPage.h"
#import "SearchResultCount.h"
#import "GroupedProductList.h"
#import "HubCitiConstants.h"
#import "ProductSearchUtility.h"
#import "SpecialOffersViewController.h"
#import "WebBrowserViewController.h"
#import "RetailerSummaryViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@implementation ScanNowScreen{
    UIActivityIndicatorView *loading;
    CustomizedNavController *cusNav;
}

@synthesize locationManager;
@synthesize locationMeasurements;
@synthesize bestEffortAtLocation;
@synthesize imageView,selectFromCameraRollButton;
//@synthesize savedSearchString;
@synthesize plistArray,productNameArray,theTableView;
@synthesize scanButton;
@synthesize historyButton;
@synthesize toolBar,sBar;
@synthesize btn_showprodPage;
@synthesize prodPage;
@synthesize startScanFlag;
static int count = 0;
static int count1 = 0;


#pragma mark default methods
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
      [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    loading = nil;
    //    if (IOS7 == NO){
    //        sBar.tintColor = [UIColor blackColor];
    //    }
    fetchingNextResults = NO;
    [sBar setAutocorrectionType:UITextAutocorrectionTypeNo];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    smartSearch = NO;
    theTableView.multipleTouchEnabled = NO;
    self.navigationController.navigationBar.hidden = NO;
    theTableView.hidden = YES;
    searchWasActive = YES;
    common = [CommonUtility utilityManager];
    //	manager = [HubCitiManager sharedManager];
    btn_showprodPage.hidden = YES;
    lastVisitedRecord = 0;
    
    savedSearchString = [[NSString alloc]init];
    
    //startScanFlag = NO;
    [SharedManager setCallScanner:NO];
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem *mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    //tool bar color
    NSLog(@"Scan now:%@",[defaults valueForKey:@"titleTxtColor"]);
    
    [toolBar setBarTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]]];
    
   
    [scanButton setTitleTextAttributes:
     @{
       NSFontAttributeName : [UIFont boldSystemFontOfSize:16.0f],
       NSForegroundColorAttributeName : [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]} forState:UIControlStateNormal];
   
    
    [historyButton setTitleTextAttributes:
     @{
       NSFontAttributeName : [UIFont boldSystemFontOfSize:16.0f],
       NSForegroundColorAttributeName : [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]} forState:UIControlStateNormal];
    
    
    //customize back button
   

    self.navigationItem.hidesBackButton = YES;
    ////[back release];
    
    if ([defaults  boolForKey:@"KEY_SCANSPLASH"] == YES)
        [self showSplash];
    else{
        
        BOOL historyFlag = NO;
        
        historyFlag = [HubCitiAppDelegate getscanHistoryFlag];
        
        if(!historyFlag)
            [self startScan];
    }
    
    for(int i=0;i<10;i++)
        productInfoItems[i] = [[NSMutableArray alloc] init];
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"Scan Now" forView:self withHambergur:YES];
    //self.navigationItem.title = @"Scan Now";
    // self.view.backgroundColor = [UIColor colorWithRGBInt:0x1f94a8];
    
    searchedData = [[NSMutableArray alloc]init];
    productNameArray = [[NSMutableArray alloc]init];//replace dataArray
    productIdArray = [[NSMutableArray alloc]init];
    tableData = [[NSMutableArray alloc]init];
    tableDataProductID = [[NSMutableArray alloc]init];
    filteredList = [[NSMutableArray alloc]init];
    
    productName = [[NSMutableArray alloc]init];
    productDesc = [[NSMutableArray alloc]init];
    productId = [[NSMutableArray alloc]init];
    productImagePath = [[NSMutableArray alloc] init];
    rowNumberArray = [[NSMutableArray alloc] init];
    prodCountArray = [[NSMutableArray alloc] init];
    prodListIdArray = [[NSMutableArray alloc]init];
    
    [self.view addSubview:imageView];
    
    // added to resize tableview depending on keyboard hide/show
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fitTableView) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeTableView) name:UIKeyboardDidHideNotification object:nil];
  
    
}

-(void) fitTableView {
    
    if (IS_IPHONE5)
        theTableView.frame = CGRectMake(theTableView.frame.origin.x, theTableView.frame.origin.y, theTableView.frame.size.width, theTableView.frame.size.height-170-35);
    else
        theTableView.frame = CGRectMake(theTableView.frame.origin.x, theTableView.frame.origin.y, theTableView.frame.size.width, theTableView.frame.size.height-170);
}
-(void) resizeTableView {
    if (IS_IPHONE5)
        theTableView.frame = CGRectMake(theTableView.frame.origin.x, theTableView.frame.origin.y, theTableView.frame.size.width, theTableView.frame.size.height+170+35);
    else
        theTableView.frame = CGRectMake(theTableView.frame.origin.x, theTableView.frame.origin.y, theTableView.frame.size.width, theTableView.frame.size.height+170);
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: NO];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    //[[UIApplication sharedApplication] setStatusBarHidden:NO animated:NO];
//    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    
    [UIApplication sharedApplication].statusBarHidden = NO;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    
    //tableview should not be visible without starting smart search
    //   theTableView.hidden = YES;
    //	imageView.hidden = YES;
    self.view.hidden = NO;
    if ([defaults boolForKey:@"DisplayBackBarButton"] == NO){
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = nil;
    }
    else{
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = back;
    }
    
    BOOL historyFlag = NO;
    DLog(@"ScanNowScreen Called:viewWillAppear 222");
    
    NSString *barCode = [defaults  objectForKey:KEY_INIGMA_BARCODE];
    if(!barCode || [barCode isEqualToString:@""] )
        btn_showprodPage.hidden = YES;
    
    
    if ([SharedManager Wishcolor] == YES)
    {
        // self.view.backgroundColor = [UIColor colorWithRGBInt:0xf57929];
        self.view.backgroundColor = [UIColor clearColor];
        imageView.hidden = YES;
        
        if (sBar.hidden == NO)
        {
            self.view.backgroundColor = [UIColor whiteColor];
            imageView.hidden = YES;
            //spinner.hidden = YES;
        }
    }
    else if (sBar.hidden == NO)
    {
        self.view.backgroundColor = [UIColor whiteColor];
        imageView.hidden = YES;
        
    }
    
    historyFlag = [HubCitiAppDelegate getscanHistoryFlag];
    
    if(historyFlag)
    {
        self.view.hidden = YES;
        
        
        //		spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        //		[spinner setCenter:CGPointMake(320/2.0, 460/2.0)]; // I do this because I'm in landscape mode
        //		[self.view addSubview:spinner]; // spinner is not visible until started
        //
        //		[spinner startAnimating];
        
        DLog(@"scanHistory-->ScanNowScreen");
        ScanHistoryViewController *viewScanHistory = [[ScanHistoryViewController alloc]initWithNibName:@"ScanHistoryViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:viewScanHistory animated:NO];
        //[viewScanHistory release];
        [HubCitiAppDelegate setscanHistoryFlag:NO];
        //[spinner stopAnimating];
    }
    
    else if([SharedManager callScanner] == YES) {
        [self scan];
    }
}

-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)showSplash
{
    UIViewController *modalViewController = [[UIViewController alloc] init];
    modalViewController.view = modelView;
    modelView.backgroundColor = [UIColor colorWithRGBInt:0x1f94a8];
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [self presentViewController:modalViewController animated:NO completion:nil];
    //[modalViewController release];
    [self performSelector:@selector(hideSplash) withObject:nil afterDelay:1.0];
}
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
- (void)hideSplash{
    
    BOOL historyFlag = NO;
    
    historyFlag = [HubCitiAppDelegate getscanHistoryFlag];
    
    if(!historyFlag)
        [self startScan];
    
    [self  dismissViewControllerAnimated:NO completion:nil];
    
}



-(void)returnToMainPage:(id)sender{
    
    [defaults setBool:NO forKey:@"ReturnResult"];
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        [common popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

#pragma mark Scanning methods
-(IBAction)scan
{
    [defaults setBool:NO forKey:@"ReturnResult"];
    self.view.backgroundColor = [UIColor whiteColor];
    imageView.hidden = YES;
    //   [defaults  setValue:@"scanNow" forKey:KEY_SEARCHFROM];
    [self startScan];
    
    
}

-(void)startScan
{
    self.navigationController.navigationBar.hidden = NO;
    
    toolBar.hidden = NO;
    sBar.hidden = NO;
    
    [historyButton setEnabled:YES];
    
    [scanButton setEnabled:YES];
    
    //  [spinner stopAnimating];
    
    //    [self request_utgetmainmenuid];
    
    if([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront] ){
        if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                // until iOS 8. So for iOS 7 permission will always be granted.
                if (granted) {
                    // Permission has been granted. Use dispatch_async for any UI updating
                    // code because this block may be executed in a thread.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self start_iNigmaScanning];
                    });
                } else {
                    // Permission has been denied.
                }
            }];
        } else {
            // We are on iOS <= 6. Just do what we need to do.
            [self start_iNigmaScanning];
        }
        
    }
    
    [self performSelector:@selector(removesplash) withObject:nil afterDelay:2.0];
    
}

#pragma REQUESTFORDATA

-(void)request_getsmartsearchprodlist:(NSInteger)rowIndex
{
    iWebRequestState = SCAN_GETSMARTSEARCHPRODLIST;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [defaults setValue:[productName objectAtIndex:rowIndex] forKey:@"prodName"];
    //Set ParCatId with value 0 in case of manual serach
    [defaults setValue:@"0" forKey:@"ParCatId"];
    [requestStr appendFormat:@"<ProductDetail><searchkey><![CDATA[%@]]></searchkey>", [productName objectAtIndex:rowIndex]];
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><userId>%@</userId>",[defaults valueForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_USERID]];
    
    [requestStr appendFormat:@"<parCatId>0</parCatId><lastVistedProductNo>0</lastVistedProductNo></ProductDetail>"];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@scannow/getsmartsearchprodlist",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    //[requestStr release];
    
}


-(void)request_getsmartsearchcountDetail:(NSInteger)rowIndex
{
    iWebRequestState = SCAN_GETSMARTSEARCHCOUNT_DETAIL;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ProductDetail><searchkey><![CDATA[%@]]></searchkey>", [defaults valueForKey:@"SearchString"]];
    [requestStr appendFormat:@"<parCatId>%@</parCatId><lastVistedProductNo>0</lastVistedProductNo>",[tableDataProductID objectAtIndex:rowIndex] ];
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults valueForKey:KEY_HUBCITIID]];
    //@Deepak: adding parameter for usertracking
    [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId></ProductDetail>", [defaults  valueForKey:KEY_MAINMENUID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@scannow/getsmartsearchcount",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    //[requestStr release];
}


-(void)request_getsmartsearchcount:(NSString*)searchString
{
    iWebRequestState = SCAN_GETSMARTSEARCHCOUNT;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ProductDetail><searchkey><![CDATA[%@]]></searchkey>", searchString];
    // [requestStr appendFormat:@"<parCatId>0</parCatId><lastVistedProductNo>%d</lastVistedProductNo>",lastVisitedRecord];
    //  [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults valueForKey:KEY_HUBCITIID]];
    //@Deepak: adding parameter for usertracking
    // [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId></ProductDetail>", [defaults  valueForKey:KEY_MAINMENUID]];
    
    
    if([defaults  valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
    if([defaults  valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_HUBCITIID]];
    
    
    [requestStr appendFormat:@"<parCatId>0</parCatId><lastVistedProductNo>%d</lastVistedProductNo></ProductDetail>",lastVisitedRecord];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@scannow/getsmartsearchcount",BASE_URL];
    
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
        
        [self parse_getsmartsearchcount:response];
    }
    else{
        [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    }
    
    //[requestStr release];
}


-(void)request_utgetmainmenuid
{
    iWebRequestState = SCAN_UTGETMAINMENUID;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<MenuItem><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>", [defaults  valueForKey:KEY_BOTTOMBUTTONID]];
    
    if([defaults  valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId>", [defaults  valueForKey:KEY_MITEMID]];
    
    if([defaults  valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_HUBCITIID]];
    
    [requestStr appendFormat:@"<platform>%@</platform>",@"IOS"];
    
    [requestStr appendString:@"</MenuItem>"];
    
    
    NSMutableString *urlString = [BASE_URL mutableCopy];
    [urlString appendFormat:@"firstuse/utgetmainmenuid"];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    //[requestStr release];
}


#pragma PARSESERVERRESPONSE


-(void)parse_getsmartsearchcount:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        
        TBXMLElement *productDetailElement = [TBXML childElementNamed:KEY_PRODUCTDETAIL parentElement:tbxml.rootXMLElement];
        
        if (productDetailElement == nil)
        {
            TBXMLElement *responseStrElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
            
            if ([productId count]){
                [productId removeAllObjects];
                [productName removeAllObjects];
                [prodCountArray removeAllObjects];
                [rowNumberArray removeAllObjects];
                [productDesc removeAllObjects];
                [productImagePath removeAllObjects];
                [prodListIdArray removeAllObjects];
            }
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseStrElement]];
            
            return;
            
        }
        else
        {
            
            TBXMLElement *productDetailElement = [TBXML childElementNamed:KEY_PRODUCTDETAIL parentElement:tbxml.rootXMLElement];
            TBXMLElement *prdCountElement = [TBXML childElementNamed:@"prdCount" parentElement:productDetailElement];
            
            if (prdCountElement!=nil){
                
                if ([productName count] && fetchingNextResults == NO) {
                    [productName removeAllObjects];
                }
                
                if ([productId count] && fetchingNextResults == NO)
                {
                    [productId removeAllObjects];
                }
                
                
                TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbxml.rootXMLElement];
                nextPage = [[TBXML textForElement:nextPageElement] intValue];
                
                while (productDetailElement != nil) {
                    TBXMLElement *productNameElement = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:productDetailElement];
                    TBXMLElement *prdCountElement = [TBXML childElementNamed:@"prdCount" parentElement:productDetailElement];
                    TBXMLElement *prodIdElement = [TBXML childElementNamed:@"productId" parentElement:productDetailElement];
                    TBXMLElement *rowNumElement = [TBXML childElementNamed:@"rowNumber" parentElement:productDetailElement];
                    TBXMLElement *prodImgPathElement = [TBXML childElementNamed:@"productImagePath" parentElement:productDetailElement];
                    TBXMLElement *prodDescElement = [TBXML childElementNamed:@"productDescription" parentElement:productDetailElement];
                    TBXMLElement *prodListIdElement = [TBXML childElementNamed:@"prodListId" parentElement:productDetailElement];
                    
                    if(prodIdElement)
                        [productId addObject:[TBXML textForElement:prodIdElement]];
                    if(productNameElement)
                        [productName addObject:[TBXML textForElement:productNameElement]];
                    if(prdCountElement)
                        [prodCountArray addObject:[TBXML textForElement:prdCountElement]];
                    if(rowNumElement)
                        [rowNumberArray addObject:[TBXML textForElement:rowNumElement]];
                    if(prodDescElement)
                        [productDesc addObject:[TBXML textForElement:prodDescElement]];
                    if(prodImgPathElement)
                        [productImagePath addObject:[TBXML textForElement:prodImgPathElement]];
                    if(prodListIdElement)
                        [prodListIdArray addObject:[TBXML textForElement:prodListIdElement]];
                    
                    productDetailElement = [TBXML nextSiblingNamed:KEY_PRODUCTDETAIL searchFromElement:productDetailElement];
                }
                
                theTableView.hidden = NO;
                
            }
        }
        
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
        [loading stopAnimating];
        ReleaseAndNilify(loading);
        
    }
    
    [theTableView  reloadData];
    [defaults setBool:NO forKey:@"ViewMore"];
}

-(void)parse_getsmartsearchprodlist:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        
        [defaults  setObject:responseString forKey:KEY_RESPONSEXML];
        
        //making use of existing, not used, file GroupedProductList
        GroupedProductList *prodListPage = [[GroupedProductList alloc]initWithNibName:@"GroupedProductList" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:prodListPage animated:NO];
        //  [prodListPage release];
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
}

-(void)parse_utgetmainmenuid:(NSString*)responseString
{
    [HubCitiAppDelegate removeActivityIndicator];
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement !=nil)
        {
            [defaults setObject:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        }
        
        if([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront] ){
            
            [self start_iNigmaScanning];
        }
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}


-(void) removesplash {
    self.view.backgroundColor = [UIColor whiteColor];
    imageView.hidden = YES;
}


#pragma mark - i_nigma Scanning methods

-(void)changeTitle:(NSString *)title
{
    
    self.title = title;
    
}

-(void) start_iNigmaScanning
{
    [HubCitiAppDelegate removeActivityIndicator];
    [HubCitiAppDelegate setNavigationController:[HubCitiAppDelegate navigationController]];
    
    DLog(@"ScanNowScreen:%@",@"start_iNigmaScanning"); 
#ifdef SCANSEE_TARGET_OS_IPHONE
    
    [defaults  setObject:@"NO" forKey:@"FLAG_BCODE_SCANNED"];
    if(i_nigmaoverlayController == nil)
    {
        i_nigmaoverlayController = [[i_nigmaSDKTestViewController alloc] initWithCoder:nil];
    }
    else {
        //[i_nigmaoverlayController release];
        i_nigmaoverlayController = nil;
        
        i_nigmaoverlayController = [[i_nigmaSDKTestViewController alloc] initWithCoder:nil];
    }
    
    if (IOS7)
        i_nigmaoverlayController.view.frame=CGRectMake(0,20,i_nigmaoverlayController.view.frame.size.width,i_nigmaoverlayController.view.frame.size.height);
    
    i_nigmaoverlayController.m_delegate = self;
    [self.view addSubview:i_nigmaoverlayController.view];
    [i_nigmaoverlayController startScanning];
#endif
}

-(void) parseScannedData:(NSString*)responseXml {
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *productDetailElement = tbXml.rootXMLElement;
    if (productDetailElement == nil) {
        TBXMLElement *responseStrElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseStrElement]];
        
        
    }
    int i = 0;
    while (productDetailElement != nil) {
        Success = YES;
        TBXMLElement *productNameElement = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:productDetailElement];
        //TBXMLElement *productDescElement = [TBXML childElementNamed:KEY_PRODUCTDESC parentElement:productDetailElement];
        TBXMLElement *productIdElement = [TBXML childElementNamed:KEY_PRODUCTID parentElement:productDetailElement];
        TBXMLElement *productImageElement = [TBXML childElementNamed:TAG_IMGPATH parentElement:productDetailElement];
        
        [productName addObject:[TBXML textForElement:productNameElement]];
        //[productDesc addObject:[TBXML textForElement:productDescElement]];
        [productId addObject: [TBXML textForElement:productIdElement]];
        [productImagePath addObject:[TBXML textForElement:productImageElement]];
        
        TBXMLElement *prdID = [TBXML childElementNamed:KEY_PRODUCTID parentElement:productDetailElement];
        TBXMLElement *prdName = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:productDetailElement];
        
        if(![common isResponseXMLNullOrEmpty:[TBXML textForElement:prdID]])
            [productInfoItems[i] addObject:[TBXML textForElement:prdID]];
        
        if(![common isResponseXMLNullOrEmpty:[TBXML textForElement:prdName]])
            [productInfoItems[i] addObject:[TBXML textForElement:prdName]];
        
    }
}

#pragma mark alertView for Scanning


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////SmartSearch Implementation Start/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableView delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (!(searchWasActive == NO))
    {
        return [tableData count];
    }
    else if ([productId count]>0 && (searchWasActive == NO) && nextPage==1)
        return ([productName count] + 1);
    else
        return [productName count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"Cell";
    if ([productName count] == 0) {
        nextPage = 0;
        lastVisitedRecord = 0;
    }
    if (indexPath.row == [productName count] && searchWasActive == NO){
        
        static NSString *CellIdentifier1 = @"Cell";
        UITableViewCell *cell1 ;
        cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
        cell1.selectionStyle = UITableViewCellSelectionStyleBlue;
        
        if (nextPage == 1) {
            
            loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            loading.center = cell1.contentView.center;
            loading.color = [UIColor blackColor];
            
            
            [cell1.contentView addSubview:loading];
            [loading startAnimating];
            cell1.userInteractionEnabled = NO;
            //            CGRect frame;
            //            frame.origin.x = 0;
            //            frame.origin.y = 16;
            //            frame.size.width = 320;
            //            frame.size.height = 24;
            //            UILabel *label = [[[UILabel alloc] initWithFrame:frame] ;
            //            label.font = [UIFont boldSystemFontOfSize:16];
            //            label.textAlignment = NSTextAlignmentCenter;
            //            label.textColor = [UIColor colorWithRGBInt:0x112e72];
            //            label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
            //            [cell1.contentView addSubview:label];
            //            cell1.accessoryType = UITableViewCellAccessoryNone;
            
        }
        
        return cell1;
        
    }
    else
    {
        UITableViewCell *cell ;
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        if (!(searchWasActive == NO)) {
            if ([tableData count] > 0) {
                
                UILabel *prodName = [[UILabel alloc]initWithFrame:CGRectMake(60, 2, 250, 40)] ;
                prodName.backgroundColor = [UIColor clearColor];
                prodName.textColor = [UIColor blackColor];
                prodName.font = [UIFont fontWithName:@"Helvetica" size:13];
                
                UILabel *catName = [[UILabel alloc]init];
                catName.backgroundColor = [UIColor clearColor];
                catName.textColor = [UIColor blackColor];
                catName.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
                
                NSString *str = [tableData objectAtIndex:indexPath.row];
                NSRange match;
                
                match = [str rangeOfString: @"|~|"];
                if (match.location == NSNotFound){
                    DLog (@"Match not found");
                    prodName.text = str;
                    prodName.font = [UIFont fontWithName:@"Helvetica" size:13];
                    prodName.frame = CGRectMake(10, 10, 250, 40);
                }
                
                else{
                    CGSize txtSz = [[str substringToIndex:match.location-1] sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:13.0f]}];
                    
                    CGRect lblFrame = CGRectMake(10, 10, txtSz.width, 40);
                    
                    prodName.frame = lblFrame;
                    if (lblFrame.size.width >150)
                        catName.frame = CGRectMake(txtSz.width + 15, 10, 100 , 40);
                    else
                        catName.frame = CGRectMake(txtSz.width + 15, 10, 200 , 40);
                    
                    if ([str length]>match.location+3){
                        
                        prodName.text = [str substringToIndex:match.location-1];
                        catName.text = [str substringFromIndex: match.location+3];
                        
                        [cell addSubview:catName];
                        //[catName release];
                        DLog(@"Length:%lu",(unsigned long)[str length]);
                        DLog (@"match found at index %lu", (unsigned long)match.location);
                        DLog(@"String 2:%@",str);
                    }
                    else {
                        prodName.text = str;
                    }
                }
                [cell addSubview:prodName];
                ReleaseAndNilify(prodName);
            }
        }
        else if ([prodCountArray count]>indexPath.row){
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            UILabel *tx_label = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 230, 40)];;
            tx_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
            tx_label.numberOfLines = 1;
            //tx_label.lineBreakMode = YES;
            
            UILabel *tx_label_Detail = [[UILabel alloc]initWithFrame:CGRectMake(60, 35, 230, 15)];
            tx_label_Detail.backgroundColor = [UIColor clearColor];
            tx_label_Detail.textColor = [UIColor grayColor];
            tx_label_Detail.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
            
            NSString *imagePathStr;
            // Configure the cell...
            if ([productName count]>indexPath.row)
                tx_label.text = [productName objectAtIndex:indexPath.row];
            
            
            if ([[prodCountArray objectAtIndex:indexPath.row]intValue]>1){
                
                tx_label_Detail.text = [NSString stringWithFormat:@"(%@ items)",[prodCountArray objectAtIndex:indexPath.row]];
            }
            else if ([[prodCountArray objectAtIndex:indexPath.row]intValue]==1){
                
                tx_label_Detail.text = [productDesc objectAtIndex:indexPath.row];
                
            }
            
            if ([productImagePath count]>indexPath.row){
                
                imagePathStr = [productImagePath objectAtIndex:indexPath.row];
                
                if (![[productImagePath objectAtIndex:indexPath.row] isEqualToString:@"N/A"]){
                    
                    SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(5, 5, 50, 50)];
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    //[asyncImageView loadImageFromURL:url];
                    [asyncImageView loadImage:imagePathStr];
                    asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
                    [cell addSubview:asyncImageView];
                    //[asyncImageView release];
                }
            }
            [cell addSubview:tx_label];
            [cell addSubview:tx_label_Detail];
            // [tx_label release];
            // [tx_label_Detail release];
        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    // count1 = [rowNumberArray count];
    
    if ([productId count]>0 && (indexPath.row == [productId count]) && (searchWasActive == NO) && nextPage==1){
        // This is the last cell
        count = (int)[rowNumberArray count];
        lastVisitedRecord = [[rowNumberArray objectAtIndex:[rowNumberArray count]-1]intValue];
        if (![defaults boolForKey:@"ViewMore"] && lastVisitedRecord !=0) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self fetchNextDeals:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [theTableView reloadData];
                });
            });
            
        }
    }
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *productNameStr =@"";
    NSString *productIdStr =@"";
    
    if ([productName count]) {
        [tableData removeAllObjects];
        [tableData addObjectsFromArray:productName];
    }
    if ([productId count]) {
        [tableDataProductID removeAllObjects];
        [tableDataProductID addObjectsFromArray:productId];
    }
    
    //pagination
    //    if ([productId count]>0 && (indexPath.row == [productId count]) && (searchWasActive == NO) && nextPage==1) {
    //		[self fetchNextDeals:nil];
    //        return;
    //	}
    [defaults setBool:NO forKey:@"ViewMore"];
    if (smartSearch == YES || [SharedManager smartSearchStatus] == YES) {
        
        [SharedManager setSmartSearchStatus:YES];
        
        [defaults setValue:[tableDataProductID objectAtIndex:indexPath.row] forKey:@"ParCatId"];
        
        [self request_getsmartsearchcountDetail:indexPath.row];
        
        return;
    }
    
    else if(smartSearch == NO){
        
        if([tableDataProductID count]>indexPath.row && [tableData count]>indexPath.row)
        {
            [defaults  setObject:[tableDataProductID objectAtIndex:indexPath.row] forKey:KEY_PRODUCTID];
            productNameStr = [tableData objectAtIndex:indexPath.row] ;
            productIdStr = [tableDataProductID objectAtIndex:indexPath.row];
        }
        
    }
    
    if ([prodCountArray count] && [[prodCountArray objectAtIndex:indexPath.row]intValue]>1){
        
        [defaults setValue:[productName objectAtIndex:indexPath.row] forKey:@"prodName"];
        //Set ParCatId with value 0 in case of manual serach
        [defaults setValue:@"0" forKey:@"ParCatId"];
        
        [self request_getsmartsearchprodlist:indexPath.row];
        
    }
    
    else if ([SharedManager Wishcolor]==YES) {
        
        if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"fav"]) {
            [self addToFavorites:productIdStr productName:productNameStr];
            [SharedManager setSmartSearchStatus:NO];
            [theTableView deselectRowAtIndexPath:[theTableView indexPathForSelectedRow] animated:YES];
            return;
            
        }
        if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"list"]) {
            
            [self addToList:productIdStr productName:productNameStr];
            [SharedManager setSmartSearchStatus:NO];
            [theTableView deselectRowAtIndexPath:[theTableView indexPathForSelectedRow] animated:YES];
            return;
        }
        if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"wish"]) {
            
            [self addToWishList:productNameStr productId:productIdStr];
            [SharedManager setSmartSearchStatus:NO];
            [theTableView deselectRowAtIndexPath:[theTableView indexPathForSelectedRow] animated:YES];
            return;
        }
        
        
    }
    else {
        
        if (indexPath.row == [prodCountArray count] && searchWasActive == NO) {
            if (nextPage == 1) {
                [self fetchNextDeals:(nil)];// More records to be shown...pagination
                DLog(@"NEXT PAGE - %d", nextPage);
            }
            return;
        }
        
        if ([[prodCountArray objectAtIndex:indexPath.row]intValue]>1) {
            
            [defaults setValue:[productName objectAtIndex:indexPath.row] forKey:@"prodName"];
            
            [self request_getsmartsearchprodlist:indexPath.row];
            
        }
        
        else
        {
            [defaults  setObject:[productId objectAtIndex:indexPath.row] forKey:KEY_PRODUCTID];
            [defaults setObject:[prodListIdArray objectAtIndex:indexPath.row] forKey:KEY_PRODLISTID];
            
            if([Network currentReachabilityStatus]==0)
            {
                [UtilityManager showAlert];
            }
            else {
                [SharedManager setShareFromTL:NO];
                
                [defaults setValue:@"0" forKey:KEY_SCANTYPEID];
                prodPage = [[ProductPage alloc]initWithNibName:@"ProductPage" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:prodPage animated:NO];
                //[prodPage release];
            }
        }
        
    }
    //sBar.text = @"";
    [theTableView deselectRowAtIndexPath:[theTableView indexPathForSelectedRow] animated:YES];
    
}



//called when smart search result selected
-(void)parseSmartSearchCountResult:(NSString*) responseXml
{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *productDetailElement = [TBXML childElementNamed:KEY_PRODUCTDETAIL parentElement:tbXml.rootXMLElement];
    if (productDetailElement == nil) {
        TBXMLElement *responseStrElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseStrElement]];
              sBar.text = @"";
        //[alert release];
        return;
        
    }
    else
    {
        [SharedManager setFromSL:NO];
        
        SearchResultCount *searchCountScreen = [[SearchResultCount alloc]initWithNibName:@"SearchResultCount" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:searchCountScreen animated:NO];
        //[searchCountScreen release];
    }
    
}


#pragma mark - addToWishList, list and fav method

-(void) addToWishList : (NSString *) productName1 productId:(NSString *) prodId
{
    
}

//Add the product to favorites
-(void) addToFavorites : (NSString *) prod_id productName : (NSString *)prod_Name {
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<AddShoppingList><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", prod_id];
    
    [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName></ProductDetail></productDetails>", prod_Name];
    //@Deepak: Added MainMenuID in the request- userTracking for shoppinglist
    [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID></AddShoppingList>", [defaults  valueForKey:KEY_MAINMENUID]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy];
    [urlString appendFormat:@"shoppingList/addproducttosl"];
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml])
        return;
    
    [defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"])
    {
        [SharedManager setRefreshList:YES];
        
    }
    else
    {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        //[alert release];
    }
}

//Add to list
-(void) addToList : (NSString *) prod_id productName : (NSString *)prod_Name{
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<AddShoppingList><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", prod_id];
    
    [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName></ProductDetail></productDetails>", prod_Name];
    [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID></AddShoppingList>", [defaults  valueForKey:KEY_MAINMENUID]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy];
    [urlString appendFormat:@"shoppingList/addtslbysearch"];
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml])
        return;
    
    [defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"])
    {
        [SharedManager setRefreshList:YES];
        
    }
    else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
       
    }
}


-(void) addToWishList : (NSString *) prod_id productName : (NSString *)prod_Name {
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendString:@"<ProductDetailsRequest>"];
    [requestStr appendFormat:@"<userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    [requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", prod_id];
    [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName>", prod_Name];
    
    [requestStr appendString:@"</ProductDetail></productDetails>"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
    [requestStr appendString:@"</ProductDetailsRequest>"];
    
    
    NSMutableString *urlString = [BASE_URL mutableCopy];
    [urlString appendFormat:@"wishlist/addWishListProd"];
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml])
        return;
    
    [defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    
    if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"])
    {
        UINavigationController *myNavController ;//= [[UINavigationController alloc] init];
        myNavController = [HubCitiAppDelegate navigationController];
        
        [myNavController popViewControllerAnimated:NO];
        //[defaults  removeObjectForKey:KEY_SEARCHFROM];
    }
    else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
       
    }
    
    //[responseXml release];
    
}


-(void)sendBarcodeRequestToServer:(NSString*)barCodeString barcodeType:(NSString*)barCodeType{
    DLog(@"i_nigmaSDKTestViewController->sendBarcodeRequestToServer");
    
    // Freeze preview pane
    //    [self CloseCamera];
    
    
    
    if((barCodeString!=NULL) && ([barCodeString isEqualToString:@""]==NO))
    {
        if([Network currentReachabilityStatus]==0)
        {
            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
            
        }
        else
        {
            //            iWebServiceType = getprodforbarcode;
            NSMutableString *requestStr = [[NSMutableString alloc] init];
            [requestStr appendFormat:@"<ProductDetail><deviceId>%@</deviceId><userId>%@</userId>",[defaults valueForKey:KEY_DEVICEID],[defaults  valueForKey:KEY_USERID]];
            
            //@Deepak: Added code for userTracking
            if([defaults  valueForKey:KEY_MAINMENUID])
                [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId><scanTypeId>1</scanTypeId>",[defaults  valueForKey:KEY_MAINMENUID]];
            
            [requestStr appendFormat:@"<barCode><![CDATA[%@]]></barCode><scanType><![CDATA[%@]]></scanType></ProductDetail>", barCodeString, barCodeType];
            
            NSString *urlString = [NSString stringWithFormat:@"%@scannow/getprodforbarcode",BASE_URL];
            
            NSString *response  = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
            
            [self parse_getprodforbarcode:response];
            //            [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
            
            //[requestStr release];
        }
    }
}


-(void)parse_getprodforbarcode:(NSString *)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *productIdElement = [TBXML childElementNamed:KEY_PRODUCTID parentElement:tbxml.rootXMLElement];
    TBXMLElement *productNameElement = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:tbxml.rootXMLElement];
    
    if (productIdElement != nil) {
        
        NSString *searchFrom = [defaults  valueForKey:KEY_SEARCHFROM];
        DLog(@"valueForKey:KEY_SEARCHFROM:%@",searchFrom);
        
        NSString *productIdStr = [NSString stringWithFormat:@"%@",[TBXML textForElement:productIdElement]];
        
        if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"fav"]) {
            [HubCitiAppDelegate removeActivityIndicator];
            [self addToFavorites:productIdStr productName:[TBXML textForElement:productNameElement]];
            return;
        }
        if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"list"]) {
            [HubCitiAppDelegate removeActivityIndicator];
            [self addToList:productIdStr productName:[TBXML textForElement:productNameElement]];
            return;
        }
        if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"wish"]) {
            [HubCitiAppDelegate removeActivityIndicator];
            [self addToWishList:productIdStr productName:[TBXML textForElement:productNameElement]];
            return;
        }
        if([UtilityManager isNullOrEmptyString:productIdStr])
        {
            [HubCitiAppDelegate removeActivityIndicator];
            [UtilityManager showAlert:nil msg:NSLocalizedString(@"There is a technical problem",@"There is a technical problem")];
            
            return;
            
        }
        
        [[HubCitiManager sharedManager]setShareFromTL:NO];
        
        [defaults  setObject:productIdStr forKey:KEY_PRODUCTID];
        
        //        NSMutableString *xmlString = [[NSMutableString alloc] init];
        //
        //        [xmlString appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults  objectForKey:KEY_USERID]];
        //
        //        //@Deepak: Added MainMenuID in the request- userTracking for shoppinglist
        //        [xmlString appendFormat:@"<mainMenuId>%@</mainMenuId><scanTypeId>%@</scanTypeId>", [defaults  valueForKey:KEY_MAINMENUID],[defaults  valueForKey:KEY_SCANTYPEID]];
        //
        //        //@Deepak: Adding prodListID code for UserTracking
        //        //@Ajit: Commented as its not reqd to send prodListID in this scenario
        //        //if([defaults  valueForKey:KEY_PRODLISTID])
        //          //  [xmlString appendFormat:@"<prodListID>%@</prodListID>", [defaults  valueForKey:KEY_PRODLISTID]];
        //
        //        if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[HubCitiManager sharedManager]gps_allow_flag] == YES && [[defaults valueForKey:KEY_LATITUDE] length])
        //
        //            [xmlString appendFormat:@"<latitude>%@</latitude><longitude>%@</longitude>", [defaults valueForKey:KEY_LATITUDE],[defaults valueForKey:KEY_LONGITUDE]];
        //        else if ([[defaults objectForKey:@"ZipCode"] length])
        //            [xmlString appendFormat:@"<postalcode>%@</postalcode>", [defaults objectForKey:@"ZipCode"]] ;
        //
        //        [xmlString appendFormat:@"<productId>%@</productId></ProductDetailsRequest>",productIdStr];
        //
        //
        //        NSMutableString *urlString = [[BASE_URL mutableCopy] ;
        //        [urlString appendString:@"find/getproductsummary"];
        //
        //        NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:xmlString];
        //
        //        [xmlString release];
        //
        //        if ([UtilityManager isNullOrEmptyString:responseXml]) {
        //            [HubCitiAppDelegate removeActivityIndicator];
        //            return;
        //        }
        //
        //        //parsing response
        //        if(![common isNullOrEmptyString:responseXml])
        //        {
        //
        //            [defaults setObject:responseXml forKey:KEY_RESPONSEXML];
        [HubCitiAppDelegate removeActivityIndicator];
        //scanNow.startScanFlag = YES;
        [SharedManager setCallScanner:YES];
        //            [defaults setValue:@"0" forKey:KEY_SCANTYPEID];
        ProductPage *viewProductPage = [[ProductPage alloc]initWithNibName:@"ProductPage" bundle:[NSBundle mainBundle]];
        
        UINavigationController *myNavController = [HubCitiAppDelegate navigationController];
        [myNavController pushViewController:viewProductPage animated:NO];
        //[viewProductPage release];
        
        //        }
        //        else {
        //            [HubCitiAppDelegate removeActivityIndicator];
        //        }
    }
    else
    {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        DLog(@"%lu",(unsigned long)[[self.navigationController viewControllers] count]);
        if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"fav"]||[[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"list"])
            
        {
            [HubCitiAppDelegate removeActivityIndicator];
             [UtilityManager showAlert:[TBXML textForElement:responseTextElement] msg:NSLocalizedString(@"Still want to add?",@"Still want to add?")];
           
            [defaults setValue:nil forKey:@"SearchKey"];
        }
        else {
            [HubCitiAppDelegate removeActivityIndicator];
            //scanNow.startScanFlag = NO;
            [SharedManager setCallScanner:NO];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            
        }
        
    }
    
}

#pragma mark SearchBar delegates

- (BOOL)searchBarShouldBeginEditing:(UISearchBar*)searchBar {
    //theTableView.hidden = NO;
    //	searchWasActive = NO;
    searchBar.showsCancelButton = YES;
    //    theTableView.frame = CGRectMake(theTableView.frame.origin.x, theTableView.frame.origin.y, theTableView.frame.size.width, theTableView.frame.size.height-170);
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    if(!controller.isActive){
        controller.searchResultsTableView.hidden = YES;
        return NO;
    }
    controller.searchResultsTableView.hidden = NO;
    [sBar becomeFirstResponder];
    return YES;
}

- (void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)searchText {
    //theTableView.hidden = NO;
    m_bIsSearch = YES;
    searchWasActive = YES;
    //	theTableView.hidden = YES;
    
    if([tableData count]){
        [tableData removeAllObjects];
        [tableDataProductID removeAllObjects];
    }
    //	theTableView.backgroundColor = [UIColor clearColor];
    
    if([searchText length]>4)
        ;//theTableView.hidden = NO;
    else {
        theTableView.hidden = YES;
        smartSearch = NO;
        return;
    }
    
    fetchingNextResults = NO;
    // new approach
    
    NSString *trimmedString = [searchBar.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    [defaults setValue:trimmedString forKey:@"SearchString"];
    
    
    [defaults setBool:YES forKey:@"ReturnResult"];
    iWebRequestState = SCAN_GETSMARTSEARCHCOUNT;
    [ProductSearch getProductInfo:trimmedString withDelegate:self];
    if (smartSearch == NO && [productDesc count]) {
        [productId removeAllObjects];
        [productName removeAllObjects];
        [productDesc removeAllObjects];
        [productImagePath removeAllObjects];
        [rowNumberArray removeAllObjects];
        [prodCountArray removeAllObjects];
        [prodListIdArray removeAllObjects];
        [theTableView reloadData];
    }
    else if(smartSearch == NO ){
        [tableData removeAllObjects];
        [tableDataProductID removeAllObjects];
        [theTableView reloadData];
    }
    theTableView.hidden = NO;
    smartSearch = YES;
}

/*
 - (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
 {
 
 NSString *newString = [searchBar.text stringByReplacingCharactersInRange:range withString:text];
 
 NSString *expression = @"^[^ ]+( .*)?$";
 
 NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
 options:NSRegularExpressionCaseInsensitive
 error:nil];
 //    NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
 //                                                        options:0
 //                                                          range:NSMakeRange(0, [newString length])];
 //    if (numberOfMatches > 0)
 //        return YES;
 //
 //    return NO;
 
 return [regex numberOfMatchesInString:newString
 options:0
 range:NSMakeRange(0, [newString length])]?YES:NO;
 }*/

-(void) returnProductInfo :(NSArray*)info
{
    if (smartSearch == YES) {
        [tableData removeAllObjects];
        [tableDataProductID removeAllObjects];
        NSArray *prd = [info copy];
        [tableData addObjectsFromArray:[prd objectAtIndex:0]];
        [tableDataProductID addObjectsFromArray:[prd objectAtIndex:1]];
        [theTableView reloadData];
    }
    
    //    [theTableView setHidden:NO];
}

- (void)searchBarCancelButtonClicked:(UISearchBar*)searchBar {
    
    
    
    m_bIsSearch = NO;
    
    [defaults setBool:NO forKey:@"ReturnResult"];
    
    searchBar.text = @"";
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
    
    if (smartSearch == YES) {
        
        theTableView.hidden = YES;
        [tableData removeAllObjects];
        [tableDataProductID removeAllObjects];
        [theTableView reloadData];
    }
    else
        [theTableView reloadData];
    
    //[self startScan];
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////SmartSearch Implementation End/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Product Search Implementation
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    //expression to trim whitespaces in between search words
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
    NSString *trimmedString = [regex stringByReplacingMatchesInString:searchBar.text options:0 range:NSMakeRange(0, [searchBar.text length]) withTemplate:@" "];
    DLog(@"%@",trimmedString);
    
    searchBar.text = trimmedString;
    smartSearch = NO;
    [SharedManager setSmartSearchStatus:NO];
    [defaults setBool:NO forKey:@"ReturnResult"];
    m_bIsSearch = NO;
    searchWasActive = NO;
    theTableView.hidden = YES;
    fetchingNextResults = NO;
    
    if ([productName count] > 0) {
        [productName removeAllObjects];
    }
    if ([productId count] > 0) {
        [productId removeAllObjects];
    }
    if ([productImagePath count] > 0) {
        [productImagePath removeAllObjects];
    }
    if ([rowNumberArray count] > 0) {
        [rowNumberArray removeAllObjects];
    }
    if ([prodCountArray count]>0) {
        [prodCountArray removeAllObjects];
    }
    if ([productDesc count]>0) {
        [productDesc removeAllObjects];
    }
    
    if ([prodListIdArray count])
        [prodListIdArray removeAllObjects];
    
    lastVisitedRecord = 0;
    count = 0;
    count1 = 0;
    searchBar.showsCancelButton = YES;
    //[searchBar resignFirstResponder];
    
    [defaults setObject:@"Add" forKey:@"Screen"];
    
    savedSearchString = [[trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]copy];
    [searchBar resignFirstResponder];
    
    if (![savedSearchString length]) {
        [UtilityManager showAlert:nil msg:NSLocalizedString(@"Please enter some text",@"Please enter some text")];
        
        [theTableView reloadData];
    }
    else{
        //[AppDelegate showActivityIndicator];
        [self getProducts : savedSearchString];
        [theTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    }
    
}

-(void) getProducts : ( NSString *) searchString
{
    
    //	searchBar.text=@"";
    DLog(@"search bar return with string= %@",searchString);
    
    if (fetchingNextResults == NO){
        
        if ([productName count] > 0) {
            [productName removeAllObjects];
        }
        if ([productId count] > 0) {
            [productId removeAllObjects];
        }
        if ([productImagePath count] > 0) {
            [productImagePath removeAllObjects];
        }
        if ([rowNumberArray count] > 0) {
            [rowNumberArray removeAllObjects];
        }
        if ([productDesc count]>0) {
            [productDesc removeAllObjects];
        }
        if ([prodCountArray count]>0) {
            [prodCountArray removeAllObjects];
        }
        
        if ([prodListIdArray count])
            [prodListIdArray removeAllObjects];
    }
    
    [self request_getsmartsearchcount:searchString];
}

-(void)responseData:(NSString *) respo
{
    switch (iWebRequestState)
    {
        case SCAN_UTGETMAINMENUID:
            [self parse_utgetmainmenuid:respo];
            break;
        case SCAN_GETSMARTSEARCHPRODLIST:
            [self parse_getsmartsearchprodlist:respo];
            break;
        case SCAN_GETSMARTSEARCHCOUNT:
            [self parse_getsmartsearchcount:respo];
            break;
        case SCAN_GETSMARTSEARCHCOUNT_DETAIL:
        {
            [defaults  setObject:respo forKey:KEY_RESPONSEXML];
            [self parseSmartSearchCountResult:respo];
        }
            break;
        default:
            break;
    }
    
    [theTableView  reloadData];
}

-(void) parseSearchResult : (NSString *) responseStr
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseStr];
    TBXMLElement *productDetailElement = [TBXML childElementNamed:KEY_PRODUCTDETAIL parentElement:tbXml.rootXMLElement];
    
    if (productDetailElement == nil) {
        TBXMLElement *responseStrElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseStrElement]];
       
        
    }
    TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbXml.rootXMLElement];
    if (nextPageElement != nil)
        nextPage = [[TBXML textForElement:nextPageElement] intValue];
    while (productDetailElement != nil) {
        TBXMLElement *productNameElement = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:productDetailElement];
        TBXMLElement *productDescElement = [TBXML childElementNamed:KEY_PRODUCTDESC parentElement:productDetailElement];
        TBXMLElement *productIdElement = [TBXML childElementNamed:KEY_PRODUCTID parentElement:productDetailElement];
        TBXMLElement *productImageElement = [TBXML childElementNamed:TAG_IMGPATH parentElement:productDetailElement];
        TBXMLElement *rowNumberElement = [TBXML childElementNamed:@"rowNumber" parentElement:productDetailElement];
        if (rowNumberElement != nil) {
            [rowNumberArray addObject:[TBXML textForElement:rowNumberElement]];
        }
        [productName addObject:[TBXML textForElement:productNameElement]];
        [productDesc addObject:[TBXML textForElement:productDescElement]];
        [productId addObject: [TBXML textForElement:productIdElement]];
        [productImagePath addObject:[TBXML textForElement:productImageElement]];
        productDetailElement = [TBXML nextSiblingNamed:KEY_PRODUCTDETAIL searchFromElement:productDetailElement];
    }
}

#pragma mark scanHistory method
-(IBAction)scanHistory
{
    [defaults setBool:NO forKey:@"ReturnResult"];
    DLog(@"scanHistory-->ScanNow111");
    if(historyController == nil)
    {
        historyController = [self m_historyController];
    }
    
    [self.navigationController pushViewController:historyController animated:NO];
}

#pragma mark memory release methods
//----------------------------------------------------------------------------------------------------
//-------------------------Scanning Start-------------------------------------------------------------
//----------------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}
-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    smartSearch = NO;
    [productNameArray removeAllObjects];
    [productIdArray removeAllObjects];
    
   
}
- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}


#pragma mark Location Manager Interactions

/*
 * We want to get and store a location measurement that meets the desired accuracy. For this example, we are
 *      going to use horizontal accuracy as the deciding factor. In other cases, you may wish to use vertical
 *      accuracy, or both together.
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    // store all of the measurements, just so we can see what kind of data we might receive
    [locationMeasurements addObject:newLocation];
    // test the age of the location measurement to determine if the measurement is cached
    // in most cases you will not want to rely on cached measurements
    NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
    if (locationAge > 100.0) return;
    // test that the horizontal accuracy does not indicate an invalid measurement
    if (newLocation.horizontalAccuracy < 0) return;
    // test the measurement to see if it is more accurate than the previous measurement
    //if (bestEffortAtLocation == nil || bestEffortAtLocation.horizontalAccuracy > newLocation.horizontalAccuracy) {
    if (bestEffortAtLocation == nil){
        // store the location as the "best effort"
        self.bestEffortAtLocation = newLocation;
        // test the measurement to see if it meets the desired accuracy
        //
        // IMPORTANT!!! kCLLocationAccuracyBest should not be used for comparison with location coordinate or altitidue
        // accuracy because it is a negative value. Instead, compare against some predetermined "real" measure of
        // acceptable accuracy, or depend on the timeout to stop updating. This sample depends on the timeout.
        //
        if (newLocation.horizontalAccuracy <= locationManager.desiredAccuracy) {
            // we have a measurement that meets our requirements, so we can stop updating the location
            //
            // IMPORTANT!!! Minimize power usage by stopping the location manager as soon as possible.
            //
            [self stopUpdatingLocation:NSLocalizedString(@"Acquired Location", @"Acquired Location")];
            // we can also cancel our previous performSelector:withObject:afterDelay: - it's no longer necessary
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stopUpdatingLocation:) object:nil];
        }
    }
    // update the display with the new location data
    NSString *gpsValu=@"NA";
    gpsValu = bestEffortAtLocation.localizedCoordinateString;
    
    //NSMutableArray *items =[[NSMutableArray alloc] init];
    NSArray *items = [gpsValu componentsSeparatedByString:@","];
    if([items count]>=2)
    {
        latValue = [[NSString stringWithFormat:@"%f",[[items objectAtIndex:0] doubleValue]] copy];
        longValue = [[NSString stringWithFormat:@"%f",[[items objectAtIndex:1] doubleValue]] copy];
        
    }
    if(latValue && [latValue length]>0 && longValue && [longValue length]>0)
    {
        [self stopUpdatingLocation:NSLocalizedString(@"Acquired Location", @"Acquired Location")];
        // we can also cancel our previous performSelector:withObject:afterDelay: - it's no longer necessary
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stopUpdatingLocation:) object:nil];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    // The location "unknown" error simply means the manager is currently unable to get the location.
    // We can ignore this error for the scenario of getting a single location fix, because we already have a
    // timeout that will stop the location manager to save power.
    if ([error code] != kCLErrorLocationUnknown) {
        [self stopUpdatingLocation:NSLocalizedString(@"Error", @"Error")];
    }
}

- (void)stopUpdatingLocation:(NSString *)state {
    
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
    self.navigationController.navigationBar.hidden = NO;
    toolBar.hidden = NO;
    sBar.hidden = NO;
    
    [historyButton setEnabled:YES];
    [scanButton setEnabled:YES];
    
    //[spinner stopAnimating];
    
    [self request_utgetmainmenuid];
}

- (void)reset {
    self.bestEffortAtLocation = nil;
    [self.locationMeasurements removeAllObjects];
    [UIView beginAnimations:@"Reset" context:nil];
    [UIView setAnimationDuration:0.6];
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];;
    [UIView commitAnimations];
}

-(void)fetchPreviousDeals:(id)sender{
    
    if (nextPage != 1) {
        
        count-= count1;
        lastVisitedRecord = count;
    }
    else{
        count-= [rowNumberArray count];
        lastVisitedRecord = count;
        
    }
    
    [self refreshTable];
    
}

-(void)fetchNextDeals:(id)sender{
    
    //[self refreshTable];
    fetchingNextResults = YES;
    [self addToTable];
    
    
}
-(void) addToTable {
    
    DLog(@"savedSearchString in addToTable :%@",savedSearchString);
    [self getProducts:savedSearchString];
    [theTableView reloadData];
    //[theTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

-(void) refreshTable {
    
    for (int i= 0; i < [productName count]; i++) {
        [productName removeAllObjects];
        [productDesc removeAllObjects];
        [productId removeAllObjects];
        [productImagePath removeAllObjects];
        [prodCountArray removeAllObjects];
        [prodListIdArray removeAllObjects];
    }
    
    [rowNumberArray removeAllObjects];
    
    [self getProducts:savedSearchString];
    
    [theTableView reloadData];
    //to reset the scroll position after paginating
    [theTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}


-(void) onNotify: (NSString*) strLocal{
    
    DLog(@"LabelNoti_text:%@",strLocal);
    //self.view.hidden = YES;
    
    NSString *barCodeString = [defaults  objectForKey:KEY_INIGMA_BARCODE];
    NSString *barCodeStringType = [defaults  objectForKey:KEY_INIGMA_BARCODETYPE];
    
    if([defaults  objectForKey:KEY_INIGMA_BARCODE] && ![[defaults  objectForKey:KEY_INIGMA_BARCODE] isEqualToString:@""])
    {
        
        if([strLocal isEqualToString:@"Code Found"])
        {
            //[spinner startAnimating];
            [defaults  setObject:@"YES" forKey:@"FLAG_BCODE_SCANNED"];
            DLog(@"KEY_INIGMA_BARCODE:%@",[defaults  objectForKey:KEY_INIGMA_BARCODE]);
            
            if([barCodeStringType isEqualToString:@"QR Code"])
            {
                /*Product Page - 1000
                 Retailer Summary Page - 2000
                 Anything Page - 2100
                 Special Offer Page - 2200
                 Give Away Page - 2300*/
                
                //@Deepak: If QRCODE called set UserDefault for KEY_SCANTYPEID:2
                [defaults setValue:@"2" forKey:KEY_SCANTYPEID];
                NSString *list = barCodeString;
                
                //check for pdf file
                /*NSRange range = [barCodeString rangeOfString:@".pdf"];
                 if (range.location != NSNotFound)
                 {
                 }*/
                DLog(@"File type %@",[list pathExtension]);
                
                listItems = [list componentsSeparatedByString:@"/"];
                DLog(@"List Array:%@",[listItems description]);
                if ([listItems count]>1){
                    
                    //to detect whether QR code is from ScanSee or outside ScanSee
                    /* else*/ if ([listItems count]>=4 && [[listItems objectAtIndex:2]isEqualToString:[defaults valueForKey:@"DomainName"]] && [[listItems objectAtIndex:3]isEqualToString:@"SSQR"]){
                        
                        //DLog(@"value:%@",[[listItems objectAtIndex:5]substringToIndex:4]);
                        //check for navigate to product summary page
                        if ([listItems count]>=6 && [[[listItems objectAtIndex:5]substringToIndex:4] isEqualToString:@"1000"])
                        {
                            NSString *searchFrom = [defaults  valueForKey:KEY_SEARCHFROM];
                            
                            NSArray *productKey = [[listItems objectAtIndex:5]componentsSeparatedByString:@"="];
                            DLog(@"Product Id:%@",[productKey objectAtIndex:1]);
                            [defaults  setObject:[productKey objectAtIndex:1] forKey:KEY_PRODUCTID];
                            
                            // If scan product belongs to below conditins then addd directly to the list
                            if ([searchFrom isEqualToString:@"fav"] || [searchFrom isEqualToString:@"list"] || [searchFrom isEqualToString:@"wish"])
                            {
                                
                                [[HubCitiManager sharedManager]setShareFromTL:NO];
                                
                                //set KEY_PRODLISTID to 'nil' as its not reqd to send it in getProductSummary req from this scenario
                                
                                [defaults  setObject:nil forKey:KEY_PRODLISTID];
                                
                                [defaults setValue:@"0" forKey:KEY_SCANTYPEID];
                                //[self addproductfromScanNow];
                            }
                            else
                            {
                                [defaults  setObject:nil forKey:KEY_PRODLISTID];
                                [defaults setValue:@"0" forKey:KEY_SCANTYPEID];
                                ProductPage *scanNowProdPage = [[ProductPage alloc]initWithNibName:@"ProductPage" bundle:[NSBundle mainBundle]];
                                
                                [[HubCitiAppDelegate navigationController] pushViewController:scanNowProdPage animated:NO];
                                // //[prodPage release];
                            }
                        }
                        
                        //check for navigate to Retailer Summary page
                        else if ([listItems count]>=6 &&[[[listItems objectAtIndex:5]substringToIndex:4] isEqualToString:@"2000"])
                        {
                            DLog(@"Redirect to retailer page");
                            //http://www.ScanSee.net/SSQR/qr/2000.htm?key1=1001&key2=222
                            
                            if (([CLLocationManager  locationServicesEnabled])
                                && ([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) &&[[HubCitiManager sharedManager] gps_allow_flag] == YES){
                                
                                [tempArray addObjectsFromArray:listItems];
                                DLog(@"Temparray: %@",tempArray);
                                [Location updateLocationinfo:self];
                            }
                            else {
                                
                                NSArray *retailerKey = [[listItems objectAtIndex:5]componentsSeparatedByString:@"="];
                                DLog(@"RetailerKey :%@",[retailerKey description]);
                                DLog(@"Retailer Id:%@",[retailerKey objectAtIndex:1]);
                                NSArray *retId = [[retailerKey objectAtIndex:1]componentsSeparatedByString:@"&"];
                                NSArray *retLocnId = [[retailerKey objectAtIndex:2]componentsSeparatedByString:@"&"];
                                
                                //                                iWebServiceType = retSummary;
                                NSMutableString *requestStr = [[NSMutableString alloc] init];
                                [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
                                [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[retLocnId objectAtIndex:0]];
                                
                                if ([[defaults  valueForKey:KEYZIPCODE]length]){
                                    
                                    [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
                                }
                                
                                // @Deepak: Added this code for "retSummary" in UserTracking
                                [requestStr appendFormat:@"<scanTypeId>%@</scanTypeId>",@"1"];
                                
                                if([defaults valueForKey:KEY_MAINMENUID])
                                    [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
                                
                                if([defaults valueForKey:KEY_RLISTID])
                                    [requestStr appendFormat:@"<retListId>%@</retListId>",[defaults valueForKey:KEY_RLISTID]];
                                
                                [requestStr appendFormat:@"<retailerId>%@</retailerId>",[retId objectAtIndex:0]];
                                if([defaults valueForKey:KEY_HUBCITIID])
                                    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
                                NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
                                
                                //                                [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
                                
                                NSString *response= [ConnectionManager establishPostConnection:urlString withParam:requestStr];
                                
                                [self parse_retsummary:response];
                            }
                        }//end of elseif
                        
                        //check for navigate to Retailer created page and special offer page or play a video
                        else if ([listItems count]>=6 && [[[listItems objectAtIndex:5]substringToIndex:4] isEqualToString:@"2200"])
                        {
                            
                            DLog(@"List:%@",list);
                            //Added MainmEnu ID
                            NSString *str = [NSString stringWithFormat:@"%@&fromSS=Y&mainMenuId=%@&scanTypeId=1",list,[defaults valueForKey:KEY_MAINMENUID]];
                            [defaults  setObject:str forKey:KEY_URL];
                            
                            SpecialOffersViewController *splOfferPage = [[SpecialOffersViewController alloc] initWithNibName:@"SpecialOffersViewController" bundle:[NSBundle mainBundle]];
                            
                            UINavigationController *myNavController = [HubCitiAppDelegate navigationController];
                            [myNavController pushViewController:splOfferPage animated:NO];
                            //  [splOfferPage release];
                            
                        }
                        
                        else if ([listItems count]>=6 && [[[listItems objectAtIndex:5]substringToIndex:4] isEqualToString:@"2100"]) {//anything page
                            NSString *str = [NSString stringWithFormat:@"%@&fromSS=Y&mainMenuId=%@&scanTypeId=1",barCodeString,[defaults valueForKey:KEY_MAINMENUID]];
                            [defaults  setObject:str forKey:KEY_URL];
                            
                            //  [defaults setValue:@"Retailer Page" forKey:@"rightbarbuttontitle"];
                            
                            //changed to SpecialOffersViewController instead WebBrowserViewController, as user tracking was done twice - one while acquiring certificate and once when loading
                            SpecialOffersViewController *urlPage = [[SpecialOffersViewController alloc]initWithNibName:@"SpecialOffersViewController" bundle:[NSBundle mainBundle]];
                            UINavigationController *myNavController = [HubCitiAppDelegate navigationController];
                            [myNavController pushViewController:urlPage animated:NO];
                            // [urlPage release];
                        }
                        
                        else if ([listItems count]>=6 && [[[listItems objectAtIndex:5]substringToIndex:4] isEqualToString:@"2300"]){
                            
                            
                            NSString *str = [NSString stringWithFormat:@"%@&fromSS=Yes&mainmenuid=%@&scantypeid=1",barCodeString,[defaults valueForKey:KEY_MAINMENUID]];
                            
                            [defaults  setObject:str forKey:KEY_URL];
                            
                            SpecialOffersViewController *splOfferPage = [[SpecialOffersViewController alloc] initWithNibName:@"SpecialOffersViewController" bundle:[NSBundle mainBundle]];
                            
                            UINavigationController *myNavController = [HubCitiAppDelegate navigationController];
                            [myNavController pushViewController:splOfferPage animated:NO];
                            ReleaseAndNilify(splOfferPage);
                        }
                        
                    }
                    //end if statement
                    
                    //Checing to see if its a web video, if so, we are going to pop to safari.
                    else if ([barCodeString rangeOfString:@"youtube.com"].location != NSNotFound ||
                             [barCodeString rangeOfString:@"vimeo.com"].location != NSNotFound ||
                             [barCodeString rangeOfString:@"scansee.com/videos/"].location != NSNotFound)
                    {
                        // [self performSelector:@selector(popToSafari:) withObject:urlString afterDelay:1];
                        [self popToSafari:barCodeString];
                    }
                    
                    //else part if QR code not from ScanSee
                    else{
                        DLog(@"Url:%@",barCodeString);
                        
                        [defaults  setObject:barCodeString forKey:KEY_URL];
                        [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                        
                        WebBrowserViewController *urlView = [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                        
                        UINavigationController *myNavController = [HubCitiAppDelegate navigationController];
                        //[self.view addSubview:self.view.window.rootViewController.view];
                        [myNavController pushViewController:urlView animated:NO];
                        //[urlView release];
                    }
                }
                else
                {
                    [self sendBarcodeRequestToServer:[defaults  objectForKey:KEY_INIGMA_BARCODE] barcodeType:[defaults  objectForKey:KEY_INIGMA_BARCODETYPE]];
                }
                
            }//end of checking for QR code
            else
            {
                //@Deepak: If QRCODE called set UserDefault for KEY_SCANTYPEID:1
                [defaults setValue:@"1" forKey:KEY_SCANTYPEID];
                [self sendBarcodeRequestToServer:[defaults  objectForKey:KEY_INIGMA_BARCODE] barcodeType:[defaults  objectForKey:KEY_INIGMA_BARCODETYPE]];
            }
        }
        //        else if( [strLocal isEqualToString:@"Camera Closed"]){
        //
        //            if(StopButton)
        //                StopButton.hidden=YES;
        //
        //            if(TorchButton)
        //                TorchButton.hidden = YES;
        //
        //        }
    }
    
    if( [strLocal isEqualToString:@"Time Out"]){
        
        //        if(StopButton)
        //            StopButton.hidden=YES;
        //
        //        if(TorchButton)
        //            TorchButton.hidden = YES;
        
        ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];
        
        [self presentViewController:viewScanNowScreen animated:YES completion:nil];
        //[viewScanNowScreen release];
    }
}



-(void)parse_retsummary:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && ![[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
       
        return;
    }
    else {
        
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *retailerSummaryPage = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:retailerSummaryPage animated:NO];
        //[retailerSummaryPage release];
    }
}


-(void)popToSafari:(NSString *)urlString
{
    
    NSURL *vidURL = [NSURL URLWithString:urlString];
    
    if (![[UIApplication sharedApplication] openURL:vidURL])
        
        DLog(@"%@%@",@"Failed to open url:",[vidURL description]);
}

#pragma mark other methods
-(i_nigmaSDKTestViewController*)m_nigmaSDKTestViewController
{
    i_nigmaoverlayController = [[i_nigmaSDKTestViewController alloc] init];
    return i_nigmaoverlayController;
}

-(ScanHistoryViewController*)m_historyController
{
    historyController = [[ScanHistoryViewController alloc] init];
    return historyController;
}


@end

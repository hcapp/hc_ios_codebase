//
//  AppDelegate.h
//  HubCiti
//
//  Created by Ajit on 9/4/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainMenuViewController.h"
#import "LoginDO.h"
#import "FTShare.h"
#import <CoreLocation/CoreLocation.h>
#import "HubCitiConstants.h"
#import <AFNetworking/AFNetworking.h>
#import "SdImageView.h"
#import "CustomizedNavController.h"

@class LoginViewController;
@class SFHFKeychainUtils;

@interface AppDelegate : UIResponder <UIApplicationDelegate,HubCitiConnectionManagerDelegate,CLLocationManagerDelegate>{
    
    UIActivityIndicatorView *activityIndicator;
    UIImageView *img;
    UILabel *label;
    
    NSMutableArray *selIndexArray;
    NSMutableArray *selIndexUserPreferenceArray;
    NSMutableArray *selIndexUserCategoryArray;
    NSMutableArray *selIndexWishListArray;
    NSMutableArray *selIndexArrayCart;
    NSMutableArray *selIndexArrayBasket;
    NSMutableArray *selIndexArrayFind;
    NSMutableArray *retailerIdArray;
    NSMutableArray *retailerLocIdArray;
    
    BOOL pushBack,pushBackground,pushGuest;
    
    LoginDO *loginDO;
    NSString *myKey;
   
    LoginViewController *loginViewController;
    MainMenuViewController *mainMenuController;
    CustomizedNavController *navigationController;
    
    AsyncImageView *splashScreen;
    BOOL scanHistoryFlag;
    NSMutableArray *linkIdArray,*deptIdArray,*typeIdArray;
    
    NSString *barCode;
    
    //location service
    CLLocationManager *locationManager;
    NSMutableArray *locationMeasurements;
    CLLocation *bestEffortAtLocation;
    NSString *latValue;
    NSString *longValue;
    NSString *message;
    BOOL isVideoOrientationcalled;
    MFSideMenuContainerViewController *container;
    BOOL resetEmail;
    
}
@property (nonatomic) BOOL isVideoOrientationcalled,isLogistics;
@property(strong,nonatomic) NSMutableArray *rssFeedListDOArr;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) LoginDO *loginDO;

@property (strong, nonatomic) LoginViewController *loginViewController;
@property (nonatomic, strong) CustomizedNavController *navigationController;
@property (strong, nonatomic) FTShare *share;
@property (strong, nonatomic) SFHFKeychainUtils *viewController;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSMutableArray *locationMeasurements;
@property (nonatomic, strong) NSMutableArray *notificationListArr, *notificationMsgList;
@property (nonatomic, strong) CLLocation *bestEffortAtLocation;

@property(nonatomic,strong) NSString * guestUser;
@property(nonatomic,strong) NSString * guestPassword;

@property(nonatomic,strong) NSString * templateTypeName;
@property(nonatomic,strong) NSString * emailCountString;
@property(nonatomic,strong) NSString * tempUserString;

//APNS
@property (nonatomic) int badgeNumber;
@property (nonatomic, strong) NSString *message;

-(void) showActivityIndicator;
-(void) removeActivityIndicator;
-(BOOL)getscanHistoryFlag;
-(void)setscanHistoryFlag:(BOOL)flag;

- (NSMutableArray*)getLinkIdArray;
- (void)refreshLinkIdArray;

- (NSMutableArray*)getDeptIdArray;
- (void)refreshDeptIdArray;

- (NSMutableArray*)getTypeIdArray;
- (void)refreshTypeIdArray;

-(NSString*)getBarCode;
-(void)setBarCode:(NSString*)str;
-(int)checkZipAndLocation;
-(BOOL)locationServicesOn;
- (NSMutableArray*)selIndexArrayFind;
- (void)RefreshIndexFindArray;
- (void)stopUpdatingLocation:(NSString *)state;

- (NSArray*)SelIndexArray;
- (void)RefreshIndexArray;

- (NSArray*)SelIndexArrayCart;
- (void)RefreshIndexArrayCart;

-(NSArray*)selIndexArrayBasket;
-(void)RefreshIndexArrayBasket;

- (NSMutableArray*)SelIndexUserPreferenceArray;
- (void)RefreshIndexUserPreferenceArray;

- (NSArray*)SelIndexUserCategoryArray;
- (void)RefreshIndexUserCategoryArray;


- (NSArray*)SelIndexWishListArray;
- (void)RefreshIndexWishListArray;

- (NSMutableArray*)getRetIdArray;
- (void)RefreshRetIdArray;

- (NSMutableArray*)getRetLocIdArray;
- (void)RefreshRetLocIdArray;
-(void) navigationColor;

//-(NSString*)shortenURL:(NSString*)longString;

- (void) registerPushNotifications:(UIApplication *) application;

@end

//
//  ReviewDetailViewController.m
//  Scansee
//
//  Created by Sandeep S on 12/20/11.
//  Copyright (c) 2011 span infotech india ptv. ltd. All rights reserved.
//

#import "RateShareViewController.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "MainMenuViewController.h"
#import "ReviewDetailViewController.h"
#import "WebBrowserViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

#define kAlertViewEmail 10000

@implementation ReviewDetailViewController

@synthesize facebookButton, twitterButton, emailButton, smsButton,anyVC,emailSendingVC;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Review";
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    
    /* UIBarButtonItem *mainPage = [[UIBarButtonItem alloc]initWithTitle:MAINMENU style:UIBarButtonItemStyleBordered target:self action:@selector(returnToMainPage:)];
     self.navigationItem.rightBarButtonItem = mainPage;
     //[mainPage release];*/
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    ////[mainPage release];
    
    
    /*UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
     self.navigationItem.backBarButtonItem = back;
     //[back release];*/
    
    //customize back button
    

    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    UIImageView *shareImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 339, 320, 32)];
    shareImgView.image = [UIImage imageNamed:@"ShareBg.png"];
    [self.view addSubview:shareImgView];
    //[shareImgView release];
    
    [facebookButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_facebook.png"] forState:UIControlStateNormal];
    [facebookButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_facebook.png"] forState:UIControlStateHighlighted];
    [facebookButton addTarget:self action:@selector(facebookButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [twitterButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_twitter.png"] forState:UIControlStateNormal];
    [twitterButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_twitter.png"] forState:UIControlStateHighlighted];
    [twitterButton addTarget:self action:@selector(twitterButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [smsButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_sendtxt.png"] forState:UIControlStateNormal];
    [smsButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_sendtxt.png"] forState:UIControlStateHighlighted];
    [smsButton addTarget:self action:@selector(smsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [emailButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_sendmail.png"] forState:UIControlStateNormal];
    [emailButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_sendmail.png"] forState:UIControlStateHighlighted];
    [emailButton addTarget:self action:@selector(emailButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    reviewText.text = [defaults valueForKey:@"Review"];
    
    URL.text = [defaults valueForKey:@"URL"];
    // Do any additional setup after loading the view from its nib.
}
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear: animated];CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}
- (IBAction)urlBtnPressed:(id)sender
{
    [defaults setValue:[defaults valueForKey:@"URL"] forKey:KEY_URL];
    [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
    WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:urlDetail animated:NO];
    //[urlDetail release];
}

- (void)facebookButtonPressed {
    
    [SharedManager setShareEmailHotDeal:NO];
    [SharedManager setShareEmailCoupon:NO];
    [SharedManager setShareEmailSpecialOffer:NO];
    [SharedManager setShareEmailSpecialOfferUrl:NO];
    [SharedManager setShareAppsite:NO];//for UT
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserRatingInfo><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    /*if ([[ScanseeManager sharedManager]shareFromTL] == YES){
     [xmlStr appendFormat:@"<isFromThisLocation>1</isFromThisLocation>"];
     [xmlStr appendFormat:@"<retailerId>%@</retailerId>",[defaults  valueForKey:KEY_RETAILERID]];
     [xmlStr appendFormat:@"<retailerLocationId>%@</retailerLocationId>", [defaults  valueForKey:@"retailLocationID"]];
     }
     else {
     [xmlStr appendFormat:@"<isFromThisLocation>0</isFromThisLocation>"];
     }*/
    [xmlStr appendFormat:@"<productId>%@</productId></UserRatingInfo>",[defaults  valueForKey:KEY_PRODUCTID]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy];
    [urlString appendString:@"ratereview/shareproductretailerinfo"];
    DLog(@"%@",urlString);
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:xmlStr]];
    // [xmlStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml]) {
        ReleaseAndNilify(responseXml);
        return;
    }
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement *productDetailElement = [TBXML childElementNamed:@"productDetail" parentElement:tbxml.rootXMLElement];
    
    if (productDetailElement == nil){
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
        [UtilityManager showAlert:nil msg:responseTextStr];
        return;
        
    }
    
    //TBXMLElement *titleTextElement = [TBXML childElementNamed:@"titleText" parentElement:tbxml.rootXMLElement];
    //TBXMLElement *productNameElement = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:productDetailElement];
    //TBXMLElement *emailTemplateUrlElement = [TBXML childElementNamed:@"emailTemplateURL" parentElement:productDetailElement];
    //TBXMLElement *pruductImagePathElement = [TBXML childElementNamed:TAG_IMGPATH parentElement:productDetailElement];
    
    // code for shar+e from this location
    //NSMutableString *shareContent = [NSMutableString stringWithFormat:@"%@\n %@\n",[TBXML textForElement:titleTextElement],[TBXML textForElement:productNameElement]];
    
    //NSString *productImagePath = [TBXML textForElement:pruductImagePathElement];
    //	DLog(@"Share Content:%@",shareContent);
    //	DLog(@"Product Image Path:%@",productImagePath);
    
    /*TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbxml.rootXMLElement];
     if (retailerDetailElement !=nil){
     TBXMLElement *retailerNameElement = [TBXML childElementNamed:KEY_RTLNAME parentElement:retailerDetailElement];
     TBXMLElement *completeAddressElement = [TBXML childElementNamed:@"completeAddress" parentElement:retailerDetailElement];
     TBXMLElement *salePriceElement = [TBXML childElementNamed:@"salePrice" parentElement:retailerDetailElement];
     
     [shareContent appendFormat:@"\nPrice:%@\nWhere:%@\nPlace:%@",[TBXML textForElement:salePriceElement],[TBXML textForElement:retailerNameElement],[TBXML textForElement:completeAddressElement]];
     DLog(@"Share Content: %@",shareContent);
     
     }
     ReleaseAndNilify(tbxml);
     [defaults   setObject:shareContent forKey:KEY_SHAREMSG];
     [defaults  setObject:[TBXML textForElement:emailTemplateUrlElement] forKey:KEY_HYPERLINK];
     [defaults   setObject:productImagePath forKey:KEY_PRODUCTIMGPATH];*/
    
    // [self.navigationController pushViewController:[[[AnyViewController alloc] initWithNibName:nil bundle:nil]  animated:NO];
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
    ReleaseAndNilify(responseXml);
}


- (void)twitterButtonPressed {
    
    [SharedManager setShareEmailHotDeal:NO];
    [SharedManager setShareEmailCoupon:NO];
    [SharedManager setShareEmailSpecialOffer:NO];
    [SharedManager setShareEmailSpecialOfferUrl:NO];
    [SharedManager setShareAppsite:NO];//for UT
    
   
}


- (void)emailButtonPressed {
    [SharedManager setShareEmailHotDeal:NO];
    [SharedManager setShareEmailCoupon:NO];
    [SharedManager setShareEmailSpecialOffer:NO];
    [SharedManager setShareEmailSpecialOfferUrl:NO];
    [SharedManager setShareAppsite:NO];
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}

- (void)smsButtonPressed {
    
    [SharedManager setShareEmailHotDeal:NO];
    [SharedManager setShareEmailCoupon:NO];
    [SharedManager setShareEmailSpecialOffer:NO];
    [SharedManager setShareEmailSpecialOfferUrl:NO];
    [SharedManager setShareAppsite:NO];//for UT
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserRatingInfo><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    /*if ([[ScanseeManager sharedManager]shareFromTL] == YES){
     [xmlStr appendFormat:@"<isFromThisLocation>1</isFromThisLocation><retailerId>%@</retailerId>",[defaults  valueForKey:KEY_RETAILERID]];
     [xmlStr appendFormat:@"<retailerLocationId>%@</retailerLocationId>", [defaults  valueForKey:@"retailLocationID"]];
     }
     else {
     [xmlStr appendFormat:@"<isFromThisLocation>0</isFromThisLocation>"];
     }*/
    [xmlStr appendFormat:@"<productId>%@</productId></UserRatingInfo>",[defaults  valueForKey:KEY_PRODUCTID]];
    NSMutableString *urlString = [BASE_URL mutableCopy];
    [urlString appendString:@"ratereview/shareproductretailerinfo"];
    DLog(@"%@",urlString);
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:xmlStr]];
    // [xmlStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml]) {
        ReleaseAndNilify(responseXml);
        return;
    }
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement *productDetailElement = [TBXML childElementNamed:@"productDetail" parentElement:tbxml.rootXMLElement];
    
    if (productDetailElement == nil){
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
        [UtilityManager showAlert:nil msg:responseTextStr];
        return;
    }
    

}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
    
}

/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
 {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization
 }
 return self;
 }
 */

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // [anyVC release];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

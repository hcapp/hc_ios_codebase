//
//  ReviewDetailViewController.h
//  Scansee
//
//  Created by Sandeep S on 12/20/11.
//  Copyright (c) 2011 span infotech india ptv. ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <Twitter/Twitter.h>
#import "CommonUtility.h"
@class AnyViewController;
@class EmailShareViewController;
@interface ReviewDetailViewController : UIViewController<MFMessageComposeViewControllerDelegate,UINavigationControllerDelegate,CustomizedNavControllerDelegate>

{
    AnyViewController *anyVC;
    IBOutlet  UIButton *facebookButton;
    IBOutlet UIButton *twitterButton;
    IBOutlet UIButton *emailButton;
    IBOutlet UIButton *smsButton;
    IBOutlet UIButton *urlButton;
    IBOutlet UITextView *reviewText;
    IBOutlet UILabel *URL;
    
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic, strong) UIButton *facebookButton, *twitterButton, *emailButton, *smsButton;

- (void)facebookButtonPressed;
- (void)twitterButtonPressed;
- (void)emailButtonPressed;
- (void)smsButtonPressed;
- (IBAction)urlBtnPressed:(id)sender;
-(void)returnToMainPage:(id)sender;
@end

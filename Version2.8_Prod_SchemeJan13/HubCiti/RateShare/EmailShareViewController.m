//
//  EmailShareViewController.m
//  Scansee
//
//  Created by ajit on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EmailShareViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import <Contacts/Contacts.h>

@interface EmailShareViewController ()

@end

@implementation EmailShareViewController
@synthesize shareType,shareEmail,updateBlock,isEmailDataLoaded;

#pragma mark view methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //selectedIndex = [[NSMutableArray alloc]init];
//    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
//    self.title = NSLocalizedString(@"Email Share",@"Email Share");
//    search.delegate = self;
//    
//    // Set the return key text and keyboard appearance of the search bar
//    for(UIView *subView in [search subviews]) {
//        if([subView conformsToProtocol:@protocol(UITextInputTraits)]) {
//            [(UITextField *)subView setReturnKeyType: UIReturnKeyDone];
//            [(UITextField *)subView setEnablesReturnKeyAutomatically:NO];
//        }
//        
//        else { //for iOS7
//            for(UIView *subSubView in [subView subviews]) {
//                if([subSubView conformsToProtocol:@protocol(UITextInputTraits)]) {
//                    [(UITextField *)subSubView setReturnKeyType: UIReturnKeyDone];
//                    [(UITextField *)subSubView setEnablesReturnKeyAutomatically:NO];
//                }
//            }
//        }
//    }
    
//    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
//    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
//    self.navigationItem.rightBarButtonItem = mainPage;
//    //[mainPage release];
//    
//    //customize back button
//    UIButton *backBtn = [UtilityManager customizeBackButton];
//    
//    [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    self.navigationItem.leftBarButtonItem = back;
//    self.navigationItem.hidesBackButton = YES;
//    //[back release];
    
    /*flag = 1;
     
     if ([SharedManager shareEmailHotDeal] == YES){
     flag = 2 ;
     }
     
     else if ([SharedManager shareEmailCoupon] == YES){
     
     flag = 3 ;
     }
     
     else if([SharedManager shareEmailSpecialOffer] == YES)
     flag = 4;
     
     else if([SharedManager shareEmailSpecialOfferUrl] == YES){
     flag = 5;
     }
     
     else if ([SharedManager shareAppsite] == YES)
     flag = 6;
     
     else
     flag = 1 ;*/
    
//    [self getAddressBookPermission];
    
   
    
    
}


-(void)loadMail{
     if (shareType!=nil) {
         NSString *typeShare = [[NSString alloc]initWithString:shareType];
        if ([typeShare isEqualToString:@"PRODUCT_SHARE"]) {
            flag=PRODUCT_SHARE;
        }
        else if([typeShare isEqualToString:@"SPECIAL_OFFER_SHARE"])
        {
            flag=SPECIAL_OFFER_SHARE;
            
        }
        else if([typeShare isEqualToString:@"ANYTHING_SHARE"])
        {
            flag=ANYTHING_SHARE;
            
        }
        else if([typeShare isEqualToString:@"APPSITE_SHARE"])
        {
            flag=APPSITE_SHARE;
        }
        else if([typeShare isEqualToString:@"EVENT_SHARE"])
        {
            flag=EVENT_SHARE;
        }
        else if([typeShare isEqualToString:@"FUNDRAISER_SHARE"])
        {
            flag=FUNDRAISER_SHARE;
        }
        else if ([typeShare isEqualToString:@"COUPON_SHARE"])
        {
            flag=COUPONS_SHARE;
        }
        else if ([typeShare isEqualToString:@"HOT_DEAL_SHARE"]){
            flag = HOTDEAL_SHARE;
        }
        else if([typeShare isEqualToString:@"BAND_SHARE"])
        {
            flag = BAND_SHARE;
        }
        else if([typeShare isEqualToString:@"BAND_EVENT_SHARE"])
        {
            flag = BAND_EVENT_SHARE;
        }
        
        else if([typeShare isEqualToString:@"NEWS_SHARE"]){
            flag = NEWS_SHARE;
        }
        //flag=[defaults valueForKey:KEY_SHARE_TYPE];
         NSMutableString *xmlStr = [[NSMutableString alloc] init];
        NSMutableString *urlStr = [[NSMutableString alloc]init];
        switch (flag) {
            case HOTDEAL_SHARE:
                iWebserviceType=HOTDEAL_SHARE;
                [xmlStr appendFormat:@"<ShareProductInfo><hotdealId>%@</hotdealId><platform>IOS</platform><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:@"HotDealId"],[defaults objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
                [urlStr appendFormat:@"%@ratereview/sharehotdealbyemail",BASE_URL];
                break;
                
            case COUPONS_SHARE:
                iWebserviceType=COUPONS_SHARE;
                [xmlStr appendFormat:@"<ShareProductInfo><couponId>%@</couponId><platform>IOS</platform><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:@"couponID"],[defaults objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
                [urlStr appendFormat:@"%@ratereview/sharecouponbyemail",BASE_URL];
                break;
                
            case NEWS_SHARE:
                iWebserviceType=NEWS_SHARE;
                [xmlStr appendFormat:@"<ShareProductInfo><newsId>%@</newsId><platform>IOS</platform><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:@"newsID"],[defaults objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
              [urlStr appendFormat:@"%@ratereview/sharenewsbyemail",BASE_URL];
               
                break;
                
                
            case PRODUCT_SHARE:
                iWebserviceType=PRODUCT_SHARE;
                [xmlStr appendFormat:@"<ShareProductInfo><productId>%@</productId><platform>IOS</platform><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_PRODUCTID ],[defaults objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
                [urlStr appendFormat:@"%@ratereview/shareProductInfo",BASE_URL];
                
                break;
                
            case SPECIAL_OFFER_SHARE:
                iWebserviceType=SPECIAL_OFFER_SHARE;
                [xmlStr appendFormat:@"<ShareProductInfo><retailerId>%@</retailerId><platform>IOS</platform><pageId>%@</pageId><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_RETAILERID],[defaults objectForKey:KEY_PAGEID],[defaults objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
                [urlStr appendFormat:@"%@ratereview/sharespecialoffemail",BASE_URL];
                break;
                
            case ANYTHING_SHARE:
                iWebserviceType=ANYTHING_SHARE;
                if (requestAnythingFromMainMenu==TRUE) {
                    [xmlStr appendFormat:@"<ShareProductInfo><platform>IOS</platform><pageId>%@</pageId><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults objectForKey:KEY_ANYPAGEID],[defaults objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
                }
                else
                {
                    
                    [xmlStr appendFormat:@"<ShareProductInfo><retailerId>%@</retailerId><platform>IOS</platform><pageId>%@</pageId><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_RETAILERID],[defaults objectForKey:KEY_ANYPAGEID],[defaults objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
                }
                [urlStr appendFormat:@"%@ratereview/sharespecialoffemail",BASE_URL];
                break;
                
                
            case APPSITE_SHARE: iWebserviceType = APPSITE_SHARE;
                [xmlStr appendFormat:@"<ShareProductInfo><retailerId>%@</retailerId><retailerLocationId>%@</retailerLocationId><platform>IOS</platform><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_RETAILERID],[defaults valueForKey:@"retailLocationID"],[defaults objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
                [urlStr appendFormat:@"%@ratereview/shareappsiteemail",BASE_URL];
                break;
                
            case EVENT_SHARE:
                iWebserviceType=EVENT_SHARE;
                [xmlStr appendFormat:@"<ShareProductInfo><eventId>%@</eventId><platform>IOS</platform><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:EVENTID],[defaults objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
                [urlStr appendFormat:@"%@ratereview/shareeventemail",BASE_URL];
                break;
                
            case FUNDRAISER_SHARE:
                iWebserviceType = FUNDRAISER_SHARE;
                [xmlStr appendFormat:@"<ShareProductInfo><fundraiserId>%@</fundraiserId><platform>IOS</platform><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:FUNDRAISERID],[defaults objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
                [urlStr appendFormat:@"%@ratereview/sharefundraiseremail",BASE_URL];
                break;

             

                
            case BAND_SHARE:
                iWebserviceType = BAND_SHARE;
                [xmlStr appendFormat:@"<ShareProductInfo><userId>%@</userId><bandId>%@</bandId><hubCitiId>%@</hubCitiId><platform>IOS</platform></ShareProductInfo>",[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_BANDID],[defaults valueForKey:KEY_HUBCITIID]];
              [urlStr appendFormat:@"%@ratereview/sharebandemail",BASE_URL];
                
                
//                [xmlStr appendFormat:@"<ShareProductInfo><userId>18130</userId><bandId>2</bandId><hubCitiId>10266</hubCitiId><platform>IOS</platform></ShareProductInfo>"];
               // [urlStr appendFormat:@"http://sdw2730:8080/HubCiti2.6/ratereview/sharebandemail"];
                NSLog(@"Request for share band %@ --> URL for Band %@",xmlStr,urlStr);
                break;
    
                

                
            case BAND_EVENT_SHARE:
                iWebserviceType = BAND_EVENT_SHARE;
                [xmlStr appendFormat:@"<ShareProductInfo><platform>IOS</platform><eventId>%@</eventId><hubCitiId>%@</hubCitiId><userId>%@</userId></ShareProductInfo>",[defaults valueForKey:@"BandEventID"],[defaults valueForKey:KEY_HUBCITIID], [defaults valueForKey:KEY_USERID]];
             [urlStr appendFormat:@"%@ratereview/sharebandeventemail",BASE_URL];
                NSLog(@"Band event request %@  ---> URL for band event %@",xmlStr,urlStr);
                break;
                
            default:
                break;
        }
        
        [ConnectionManager establishConnectionFor:xmlStr base:urlStr withDelegate:self];
         [HubCitiAppDelegate removeActivityIndicator];
        DLog(@"%@",urlStr);
        //[xmlStr release];
        
    }
    else{
        NSMutableString *xmlStr = [[NSMutableString alloc] init];
        NSMutableString *urlStr = [[NSMutableString alloc]init];
//        iWebserviceType = shareapplink;
        
//        search.text = [search.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//        if ([search.text length]) {
//            [newString appendFormat:@"%@",search.text];
//        }
        [xmlStr appendFormat:@"<ShareProductInfo><shareType>email</shareType><platform>IOS</platform><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        [urlStr appendFormat:@"%@ratereview/shareapplink",BASE_URL];
        DLog(@"%@",urlStr);
        NSString* response = [ConnectionManager establishPostConnection:urlStr withParam:xmlStr];
        [self parse_shareapplink:response];
        
       // [xmlStr release];
    }

}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:NO];
    m_bIsSearch = NO;
    
    
   
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark navigation barbutton methods
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}



-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
       // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }

    }
    

    else
    {
    [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

#pragma mark fetch and display contacts
//-(void)getAddressBookPermission
//{
//    
//    if (ABAddressBookRequestAccessWithCompletion) { // if in iOS 6
//        
//        // Request authorization to Address Book
//        CFErrorRef error = NULL;
//        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, &error);
//         ABAddressBookRequestAccessWithCompletion(addressBookRef, nil);
//        if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
//            ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
//                // First time access has been granted, add the contact
//                DLog(@"1");
//                if (granted) {
//                    // First time access has been granted, add the contact
//                    [self processAndDisplayContacts];
//                } else {
//                    // User denied access
//                    // Display an alert telling user the contact could not be added
//                    return;
//                }
//            });
//        }
//        else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
//            // The user has previously given access, add the contact
//            
//            DLog(@"2");
//            [self processAndDisplayContacts];
//        }
//        else {
//            // The user has previously denied access
//            // Send an alert telling user to change privacy setting in settings app
//            DLog(@"3");
//            [self processAndDisplayContacts];
//            return;
//        }
//    }
//    else{ // if not in iOS 6
//        // just get the contacts directly
//        DLog(@"4");
//        [self processAndDisplayContacts];
//    }
//}
//

-(void)processAndDisplayContacts{
    CFErrorRef error = NULL;
    ABAddressBookRef _addressBookRef = ABAddressBookCreateWithOptions(NULL, &error);
    ABAddressBookRequestAccessWithCompletion(_addressBookRef, nil);
    NSArray* allPeople = (NSArray *)CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(_addressBookRef));
    _allItems = [[NSMutableArray alloc] initWithCapacity:[allPeople count]];
    emailAddrArray = [[NSMutableArray alloc] initWithCapacity:[allPeople count]];
    
    for (id record in allPeople) {
        ABMultiValueRef emailMultiValue = ABRecordCopyValue((__bridge ABRecordRef)record, kABPersonEmailProperty);
        NSArray *emails = (NSArray *)CFBridgingRelease(ABMultiValueCopyArrayOfAllValues(emailMultiValue));
        DLog(@"Emails:%@",emails);
        CFRelease(emailMultiValue);
        for (NSString *email in emails) {
            NSString* compositeName = (NSString *)CFBridgingRelease(ABRecordCopyCompositeName((__bridge ABRecordRef)record));
            NSString *field = [NSString stringWithFormat:@"%@ - %@",compositeName,email];
           // [compositeName release];
            [_allItems addObject:field];
            [emailAddrArray addObject:email];
        }
       // [emails release];
    }
    
    if (_addressBookRef) {
        CFRelease(_addressBookRef);
    }
    //[allPeople release];
    allPeople = nil;
    DLog(@"All Items:%@",_allItems);
    searchData = [[NSMutableArray alloc]init];
    contactIndexArray = [[NSMutableArray alloc]init];//has the first char of contact
    if ([_allItems count]){
        for (int i = 0; i<[_allItems count]; i++) {
            
            char alphabet = [[_allItems objectAtIndex:i]characterAtIndex:0];
            
            NSString *uniChar = [NSString stringWithFormat:@"%c", alphabet];//Dont resolve this warning
            //---add each letter to the index array---
            if (![contactIndexArray containsObject:[uniChar uppercaseString]] && ![contactIndexArray containsObject:[uniChar lowercaseString]] )
            {
                //[contactIndexArray addObject:[uniChar uppercaseString]];
                [contactIndexArray addObject:[uniChar uppercaseString]];
                
            }
            [contactIndexArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            DLog(@"%@",contactIndexArray);
        }
    }
    
    //[search becomeFirstResponder];
    
}

//-(IBAction)selectContact:(UIButton *)sender
//{
//    
//    // @@ios7 IndexPath reading issue
//    //index = [contactsTable indexPathForCell:(UITableViewCell *) [sender superview]];
//    
//    CGPoint location = [sender.superview convertPoint:sender.center toView:contactsTable];
//    
//    index = [contactsTable indexPathForRowAtPoint:location];
//    
//    
//    if (sender.tag == 0) {
//        [sender setImage:[UIImage imageNamed:@"CheckMark.png"] forState:UIControlStateNormal];
//        [selectedIndex addObject:index];
//        sender.tag = 1;
//    }
//    else {
//        for (int i = 0; i < [selectedIndex count]; i++) {
//            NSIndexPath *sIndex =[selectedIndex objectAtIndex:i];
//            if (index.row == sIndex.row && index.section == sIndex.section) {
//                [selectedIndex removeObjectAtIndex:i];
//            }
//        }
//        [sender setImage:[UIImage imageNamed:@"CheckMarkUnCheck.png"] forState:UIControlStateNormal];
//        sender.tag = 0 ;
//    }
//}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

#pragma mark tableview delegate methods



#pragma mark request method


#pragma mark parse methods
-(void)responseData:(NSString*)response{
    
    switch (iWebserviceType) {
        case shareapplink:
        case PRODUCT_SHARE:
        case APPSITE_SHARE:
        case EVENT_SHARE:
        case SPECIAL_OFFER_SHARE:
        case ANYTHING_SHARE:
        case FUNDRAISER_SHARE:
        case HOTDEAL_SHARE:
        case COUPONS_SHARE:
        case BAND_SHARE:
        case BAND_EVENT_SHARE:
        case NEWS_SHARE:
            [self parse_shareapplink:response];
            break;
//        case PRODUCT_SHARE:
//            [self parse_productShareEmail:response];
//            break;
//        
//            [self parse_appsiteShareEmail:response];
//            break;
            
        default:
            break;
    }
}

//-(void)parse_productShareEmail:(NSString *)response
//{
//    
//}
//
//-(void)parse_parse_appsiteShareEmail:(NSString *)response
//{
//    
//}

-(void)parse_shareapplink:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]) {
        ReleaseAndNilify(response);
        return;
    }

    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    

    NSString *responseTextStr = [TBXML textForElement:responseTextElement];
    
   
    
    [defaults setValue:responseTextStr forKey:@"responseTextStr"];
    
    if ([[TBXML textForElement:responseCodeElement] intValue] == 10000){
    
        TBXMLElement *sharText = [TBXML childElementNamed:@"shareText" parentElement:tbxml.rootXMLElement];
        
        NSString *shareText=[TBXML textForElement:sharText];
        
        TBXMLElement *imagePath = [TBXML childElementNamed:@"imagePath" parentElement:tbxml.rootXMLElement];
        
        
        TBXMLElement *subject = [TBXML childElementNamed:@"subject" parentElement:tbxml.rootXMLElement];
        
        NSString *sub=[TBXML textForElement:subject];
        
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
              [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
          
            [mailViewController setSubject:sub];
            
            __block  NSData *imageData=nil ;
            
            
            if(imagePath && !([[TBXML textForElement:imagePath] isEqualToString:@"N/A"])){
                
                NSString *image=[[TBXML textForElement:imagePath]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [HubCitiAppDelegate showActivityIndicator];
                __block NSData *storeImageData;
                dispatch_queue_t queue = dispatch_queue_create("com.blah.queue", DISPATCH_QUEUE_SERIAL);
                
                dispatch_async(queue, ^{
                    //load url image into NSData
                    
                    storeImageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:image]];
                    UIImage *emailImage = [UIImage imageWithData:storeImageData];
                    imageData = [NSData dataWithData:UIImagePNGRepresentation(emailImage)];
                 
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [HubCitiAppDelegate removeActivityIndicator];
                        [mailViewController addAttachmentData:imageData mimeType:@"image/png" fileName:@"attachment.png"];
                        [mailViewController setMessageBody:shareText isHTML:YES];
                        self.myMailViewController=mailViewController;
                        self.isEmailDataLoaded=1;
                        updateBlock(_myMailViewController);
                    });
                    
                });
                
                
              
                
            }
            
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [mailViewController setMessageBody:shareText isHTML:YES];
                    self.myMailViewController=mailViewController;
                    self.isEmailDataLoaded=1;
                    updateBlock(_myMailViewController);
                });
            }
            
            
            
            
        }
        else{
            
            [UtilityManager showAlert:nil msg:@"Your device could not send e-mail. Please check e-mail configuration and try again."];
            

        }
    }

    

else {
    
    [UtilityManager showAlert:nil msg:responseTextStr];

    
}
}







- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //NSLog(@"Result: canceled");
            break;
        case MFMailComposeResultSaved:
            //NSLog(@"Result: saved");
            break;
        case MFMailComposeResultSent:
        {

                NSMutableString *userXmlStr = nil;
                switch (iWebserviceType) {
                    case HOTDEAL_SHARE:
                        userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Email</shrTypNam><hotDealId>%@</hotDealId><tarAddr>%@</tarAddr></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:@"HotDealId"],shareEmail];
                        break;
                        
                    case PRODUCT_SHARE:
                        userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Email</shrTypNam><prodId>%@</prodId><tarAddr>%@</tarAddr></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PRODUCTID],shareEmail];
                        break;
                        //1109478
                        //6705
                    case APPSITE_SHARE:
                        userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Email</shrTypNam><retId>%@</retId><retLocId>%@</retLocId><tarAddr>%@</tarAddr></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_RETAILERID],[defaults valueForKey:@"retailLocationID"],shareEmail];
                        break;
                        
                    case EVENT_SHARE:
                        userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Email</shrTypNam><eventId>%@</eventId><tarAddr><![CDATA[%@]]></tarAddr></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:EVENTID],shareEmail];
                        break;
                        
                    case SPECIAL_OFFER_SHARE:
                        userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Email</shrTypNam><sPageId>%@</sPageId><tarAddr>%@</tarAddr></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PAGEID],shareEmail];
                        break;
                        
                    case ANYTHING_SHARE:
                        if (requestAnythingFromMainMenu==FALSE) {
                            userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Email</shrTypNam><aPageId>%@</aPageId><tarAddr>%@</tarAddr><retId>%@</retId><retLocId>%@</retLocId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_ANYPAGEID],shareEmail,[defaults valueForKey:KEY_RETAILERID],[defaults valueForKey:@"retailLocationID"]];
                        }
                        else{
                            userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Email</shrTypNam><aPageId>%@</aPageId><tarAddr>%@</tarAddr></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_ANYPAGEID],shareEmail];
                        }
                        break;
                        
                    case FUNDRAISER_SHARE:userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Email</shrTypNam><fundId>%@</fundId><tarAddr>%@</tarAddr></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:FUNDRAISERID],shareEmail];
                        break;
                    case BAND_SHARE:
                        userXmlStr = [[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><bandId>%@</bandId><tarAddr>%@</tarAddr></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_BANDID], shareEmail];
                        break;
                    case BAND_EVENT_SHARE:
                        userXmlStr = [[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><bandEventId>%@</bandEventId><tarAddr>%@</tarAddr></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:@"BandEventID"],shareEmail];
                        break;
                                                                                                                                                                                                    
                        
                    default: break;
                }
                
                
                [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
                
                //   [ConnectionManager establishConnectionFor:userXmlStr base:userUrlStr withDelegate:self];
                
            
          
            
            
            [UtilityManager showAlert:@"Result" msg:@"Mail Sent Successfully"];

           
           
            [UtilityManager performSelector:@selector(alertControllerDismiss:) withObject:nil afterDelay:2];
          
          
            
        }
            break;
        case MFMailComposeResultFailed:
            //NSLog(@"Result: failed");
            break;
        default:
            //NSLog(@"Result: not sent");
            break;
    }
    
    [controller dismissViewControllerAnimated:NO completion:^{
        //[self.navigationController popViewControllerAnimated:NO];
    }];
    
}






#pragma user tracking

-(void)userTracking:(NSString*)xmlStr
{
     NSMutableString *userUrlStr=[[NSMutableString alloc]initWithFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:userUrlStr withParam:xmlStr]];
    DLog(@"%@",response);
    //[xmlStr release];
}


#pragma mark textField delegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
	[textField resignFirstResponder];
	return YES;
}

#pragma mark searchBar delegate methods
/*- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar;
 {
 [self.view endEditing:YES];
 }*/

//- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
//    
//    [search resignFirstResponder];
//}

@end

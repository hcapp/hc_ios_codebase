//
//  RateShareViewController.m
//  Scansee
//
//  Created by Ramanan Kumaravelu on 23/06/11.
//  Copyright 2011 Span Infotech. All rights reserved.
//

#import "RateShareViewController.h"
#import "AnyViewController.h"
#import "MainMenuViewController.h"
#import "WebBrowserViewController.h"
#import "ReviewDetailViewController.h"
#import "AppDelegate.h"
#import "Config.h"
#import "FTShare.h"
#import "EmailShareViewController.h"
#import <Twitter/Twitter.h>
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

#define kAlertViewEmail 10000

BOOL refreshPrevPage = NO;
@implementation RateShareViewController

@synthesize facebookButton, twitterButton, emailButton, smsButton,reviewTable,anyVC,emailSendingVC;


//@synthesize userRating;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    

    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    self.navigationItem.hidesBackButton = YES;
    
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"Reviews" forView:self withHambergur:NO];
    
    //[titleLabel release];
    
    //customize navigation barButtonItems
    //    UIButton *mainBtn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [mainBtn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainBarButton = [[UIBarButtonItem alloc] initWithCustomView:mainBtn];
    //    self.navigationItem.rightBarButtonItem = mainBarButton;
    //    [mainBarButton release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    // //[mainPage release];
    
    
    UIButton *backbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backbtn setFrame:CGRectMake(0.0f, 0.0f, 30.0f, 30.0f)];
    
    SdImageView *backImage= [[SdImageView alloc]initWithFrame:backbtn.frame];
    [backImage loadImage:[defaults valueForKey:@"bkImgPath"]];
    [backbtn addSubview:backImage];
    
    //[backbtn setImage:[UIImage imageNamed:@"backBtn_up.png"] forState:UIControlStateHighlighted];
    
    
    [backbtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backbtn];
    self.navigationItem.leftBarButtonItem = backBarButton;
    //[backBarButton release];
    
    [self request_GetProductReviews];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}

-(void)setUpBottomButtons{
    
    facebookButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPHONE5)
        facebookButton.frame = CGRectMake(00, 367+88, 80, 49);
    else
        facebookButton.frame = CGRectMake(00, 367, 80, 49);
    [facebookButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_facebook.png"] forState:UIControlStateNormal];
    [facebookButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_facebook.png"] forState:UIControlStateHighlighted];
    [facebookButton addTarget:self action:@selector(facebookButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    facebookButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    facebookButton.tag = 0;
    [self.view addSubview:facebookButton];
    
    twitterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (IS_IPHONE5)
        twitterButton.frame = CGRectMake(80, 367+88, 80, 49);
    else
        twitterButton.frame = CGRectMake(80, 367, 80, 49);
    [twitterButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_twitter.png"] forState:UIControlStateNormal];
    [twitterButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_twitter.png"] forState:UIControlStateHighlighted];
    [twitterButton addTarget:self action:@selector(twitterButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    twitterButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    twitterButton.tag = 1;
    [self.view addSubview:twitterButton];
    
    smsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPHONE5)
        smsButton.frame = CGRectMake(160, 367+88, 80, 49);
    else
        smsButton.frame = CGRectMake(160, 367, 80, 49);
    [smsButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_sendtxt.png"] forState:UIControlStateNormal];
    [smsButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_sendtxt.png"] forState:UIControlStateHighlighted];
    [smsButton addTarget:self action:@selector(smsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    smsButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    smsButton.tag = 2;
    [self.view addSubview:smsButton];
    
    emailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPHONE5)
        emailButton.frame = CGRectMake(240, 367+88, 80, 49);
    else
        emailButton.frame = CGRectMake(240, 367, 80, 49);
    [emailButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_sendmail.png"] forState:UIControlStateNormal];
    [emailButton setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_sendmail.png"] forState:UIControlStateHighlighted];
    [emailButton addTarget:self action:@selector(emailButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    emailButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    emailButton.tag = 3;
    [self.view addSubview:emailButton];
}

#pragma mark navigation barButtonItem actions
-(void)backAction:(id)sender{
    
    /*if (([[ScanseeManager sharedManager]hotDealToolBar] == YES) || (refreshPrevPage == NO)){
     
     [[ScanseeManager sharedManager]setRefreshProdPage:NO];
     
     [self.navigationController popViewControllerAnimated:NO];
     DLog(@"back button clicked");
     }
     
     else {
     
     //[SharedManager setRefreshProdPage:YES];
     NSMutableString *xmlString = [[NSMutableString alloc] init];
     [xmlString appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults  objectForKey:KEY_USERID]];
     [xmlString appendFormat:@"<productId>%@</productId>",[defaults  objectForKey:KEY_PRODUCTID]];
     
     if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES)
     
     {
     [xmlString appendFormat:@"<latitude>%@</latitude><longitude>%@</longitude>", [defaults valueForKey:KEY_LATITUDE],[defaults valueForKey:KEY_LONGITUDE]];
     }
     //@Deepak: Added MainMenuID in the request- userTracking for shoppinglist
     [xmlString appendFormat:@"<mainMenuID>%@</mainMenuID><scanTypeID>%@</scanTypeID>", [defaults  valueForKey:KEY_MAINMENUID],[defaults  valueForKey:KEY_SCANTYPEID]];
     //@Deepak: Adding prodListID code for UserTracking
     if([defaults  valueForKey:KEY_PRODLISTID])
     [xmlString appendFormat:@"<prodListID>%@</prodListID>",[defaults  valueForKey:KEY_PRODLISTID]];
     
     [xmlString appendString:@"</ProductDetailsRequest>"];
     
     DLog(@"Request String for product page = %@", xmlString);
     
     NSMutableString *urlString = [[BASE_URL mutableCopy] ;
     [urlString appendString:@"shoppingList/getproductsummary"];
     NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:xmlString]];
     [xmlString release];
     
     if ([UtilityManager isNullOrEmptyString:responseXml]) {
     ReleaseAndNilify(responseXml);
     return;
     }
     [defaults setValue:responseXml forKey:KEY_RESPONSEXML];
     
     DLog(@"Response xml for product page = %@", responseXml);
     [self.navigationController popViewControllerAnimated:NO];
     ReleaseAndNilify(responseXml);
     }
     */
}



-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

#pragma mark clear user rating
- (void)userRatingClearBtnPressed:(id)sender {
    
    refreshPrevPage = YES;
    // Make a POST request to server to save the new user Rating
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserRatingInfo><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [xmlStr appendFormat:@"<productId>%@</productId><currentRating>0</currentRating></UserRatingInfo>", [defaults  valueForKey:KEY_PRODUCTID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/saveuserrating",BASE_URL];
    DLog(@"%@",urlString);
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:xmlStr]];
    //  [xmlStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml]) {
        ReleaseAndNilify(responseXml);
        return;
    }
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    NSString *responseCodeStr = [TBXML textForElement:responseCodeElement];
    int responseCode = [responseCodeStr intValue];
    
    //[tbxml release];
    //[responseXml release];
    
    if (responseCode == 10000) {
        for (int i=1; i <= 5; i++) {
            UIButton *starBtn = (UIButton *)[self.view viewWithTag:i];
            [starBtn setBackgroundImage:[UIImage imageNamed:@"greyStar.png"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark make rating
- (void)userRatingStarBtnPressed:(id)sender {
    
    refreshPrevPage = YES;
    
    UIButton *userRatingStarBtn = (UIButton *)sender;
    
    // Make a POST request to server to save the new user Rating
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserRatingInfo><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [xmlStr appendFormat:@"<productId>%@</productId>", [defaults  valueForKey:KEY_PRODUCTID]];
    [xmlStr appendFormat:@"<currentRating>%lu</currentRating></UserRatingInfo>", (long)userRatingStarBtn.tag];
    
    DLog(@"xml (Rating) = %@", xmlStr);
    NSMutableString *urlString = [BASE_URL mutableCopy];
    [urlString appendString:@"ratereview/saveuserrating"];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:xmlStr]];
    //[xmlStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml]) {
        ReleaseAndNilify(responseXml);
        return;
    }
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    NSString *responseCodeStr = [TBXML textForElement:responseCodeElement];
    int responseCode = [responseCodeStr intValue];
    
    //[tbxml release];
    //[responseXml release];
    
    if (responseCode == 10000) {
        for (int i=1; i <= userRatingStarBtn.tag; i++) {
            UIButton *starBtn = (UIButton *)[self.view viewWithTag:i];
            [starBtn setBackgroundImage:[UIImage imageNamed:@"Yellow_Fullstar.png"] forState:UIControlStateNormal];
        }
        
        for (int i=((int)userRatingStarBtn.tag + 1); i <= 5; i++) {
            UIButton *starBtn = (UIButton *)[self.view viewWithTag:i];
            [starBtn setBackgroundImage:[UIImage imageNamed:@"greyStar.png"] forState:UIControlStateNormal];
        }
    }
}
#pragma mark request methods
// Make a GET request to server to fetch the rating
-(void)request_GetProductReviews{
    
    iWebServiceCallType = getproductreviews;
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/getproductreviews?userId=%@&productId=%@",BASE_URL,
                           [defaults  objectForKey:KEY_USERID],[defaults  objectForKey:KEY_PRODUCTID]];
    
    [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    
}
-(void)request_fetchuserrating{
    
    /* <UserRatingInfo>
     <userId>2</userId>
     <isFromThisLocation>1</isFromThisLocation>
     <retailerId>2097</retailerId>
     <productId>2</productId>
     <retailerLocationId>94334</retailerLocationId>
     <hubCitiId>70</hubCitiId>
     </UserRatingInfo>*/
    
    iWebServiceCallType = fetchuserrating;
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserRatingInfo><userId>%@</userId><hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    /* if ([[ScanseeManager sharedManager]shareFromTL] == YES){
     [xmlStr appendFormat:@"<isFromThisLocation>1</isFromThisLocation><retailerId>%@</retailerId>",[defaults  valueForKey:KEY_RETAILERID]];
     [xmlStr appendFormat:@"<retailerLocationId>%@</retailerLocationId>", [defaults  valueForKey:@"retailLocationID"]];
     }
     else {
     [xmlStr appendFormat:@"<isFromThisLocation>0</isFromThisLocation>"];
     }
     */
    [xmlStr appendFormat:@"<productId>%@</productId></UserRatingInfo>",[defaults  valueForKey:KEY_PRODUCTID]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy];
    [urlString appendString:@"ratereview/fetchuserrating"];
    
    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //[xmlStr release];
    
}

-(void)shareToTwitter{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        }
}

#pragma mark parse methods
-(void)responseData:(NSString *) response{
    
    switch (iWebServiceCallType) {
            
        case getproductreviews:
            [self parse_GetProductReviews:response];
            break;
            
        case fetchuserrating:
            [self parse_fetchuserrating:response];
            break;
            
        default:
            break;
    }
}

-(void)parse_GetProductReviews:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *totalReviewsElement = [TBXML childElementNamed:@"totalReviews" parentElement:tbxml.rootXMLElement];
    totalReviewsStr = [NSString stringWithFormat:@"(%@)",[TBXML textForElement:totalReviewsElement]];
    DLog(@"Review count: %@",totalReviewsStr);
    
    TBXMLElement *productReviewsElement = [TBXML childElementNamed:@"productReviews" parentElement:tbxml.rootXMLElement];
    
    if (productReviewsElement != nil){
        reviewCommentsArray = [[NSMutableArray alloc]init];
        reviewUrlArray = [[NSMutableArray alloc] init];
        
        TBXMLElement *ProductReviewElement = [TBXML childElementNamed:@"ProductReview" parentElement:productReviewsElement];
        
        while (ProductReviewElement != nil) {
            TBXMLElement *reviewCommentsElement = [TBXML childElementNamed:@"reviewComments" parentElement:ProductReviewElement];
            TBXMLElement *reviewURLElement = [TBXML childElementNamed:@"reviewURL" parentElement:ProductReviewElement];
            
            [reviewCommentsArray addObject:[TBXML textForElement:reviewCommentsElement]];
            [reviewUrlArray addObject:[TBXML textForElement:reviewURLElement]];
            
            ProductReviewElement = [TBXML nextSiblingNamed:@"ProductReview" searchFromElement:ProductReviewElement];
        }
    }
    DLog(@"Reviews: %@",[reviewCommentsArray description]);
    
    TBXMLElement *userRatingInfoElement = [TBXML childElementNamed:@"userRatingInfo" parentElement:tbxml.rootXMLElement];
    
    if (userRatingInfoElement == nil) { // meaning, we do not have a successful response
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        NSString *responseText = [TBXML textForElement:responseTextElement];
        
        // Display an alert with the server returned response text
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:responseText];
        return;
    }
    
    else{
        TBXMLElement *currentRatingElement = [TBXML childElementNamed:@"currentRating" parentElement:userRatingInfoElement];
        TBXMLElement *avgRatingElement = [TBXML childElementNamed:@"avgRating" parentElement:userRatingInfoElement];
        
        NSString *currentRatingStr = [TBXML textForElement:currentRatingElement];
        NSString *avgRatingStr = [TBXML textForElement:avgRatingElement];
        
        //  [tbxml release];
        
        UIImageView *rateImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 149)];
        rateImgView.image = [UIImage imageNamed:@"RateBg.png"];
        [self.view addSubview:rateImgView];
        //  [rateImgView release];
        
        UIButton *userRatingClearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [userRatingClearBtn addTarget:self action:@selector(userRatingClearBtnPressed:) forControlEvents:UIControlEventTouchDown];
        userRatingClearBtn.frame = CGRectMake(85, 40, 20, 20);
        [self.view addSubview:userRatingClearBtn];
        
        NSURL *prodImageURL = [NSURL URLWithString:[defaults  objectForKey:@"productImage"]];
        UIImage *prodImage = [UIImage imageWithData: [NSData dataWithContentsOfURL:prodImageURL]];
        UIImageView *prodImgView = [[UIImageView alloc] initWithFrame:CGRectMake(225, 40, 80, 100)];
        prodImgView.image = prodImage;
        [self.view addSubview:prodImgView];
        // [prodImgView release];
        
        int currentRating_i = [currentRatingStr intValue];
        
        float avgRating_f = [avgRatingStr floatValue];
        int avgRating_i = [avgRatingStr intValue];
        
        int i=0;
        do {
            UIButton *userRatingStarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            userRatingStarBtn.tag = i+1;
            [userRatingStarBtn addTarget:self action:@selector(userRatingStarBtnPressed:) forControlEvents:UIControlEventTouchDown];
            userRatingStarBtn.frame = CGRectMake(10 + (i*25), 65, 21, 21);
            if (i < currentRating_i)
                [userRatingStarBtn setBackgroundImage:[UIImage imageNamed:@"Yellow_Fullstar.png"] forState:UIControlStateNormal];
            else
                [userRatingStarBtn setBackgroundImage:[UIImage imageNamed:@"greyStar.png"] forState:UIControlStateNormal];
            
            [self.view addSubview:userRatingStarBtn];
            i++;
        } while (i < 5);
        
        i=0;
        do{
            UIButton *prodRatingStarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            prodRatingStarBtn.frame = CGRectMake(10 + (i*25), 120, 21, 21);
            if (i < avgRating_i)
                [prodRatingStarBtn setBackgroundImage:[UIImage imageNamed:@"Red_fullstar.png"] forState:UIControlStateNormal];
            else
                [prodRatingStarBtn setBackgroundImage:[UIImage imageNamed:@"greyStar.png"] forState:UIControlStateNormal];
            
            [self.view addSubview:prodRatingStarBtn];
            i++;
        } while (i < 5);
        
        float diff = avgRating_f - (float)avgRating_i;
        if (diff >= 0.5) {
            UIButton *prodRatingStarBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            prodRatingStarBtn1.frame = CGRectMake(10 + (avgRating_i*25), 120, 21, 21);
            [prodRatingStarBtn1 setBackgroundImage:[UIImage imageNamed:@"Red_Halfstar.png"] forState:UIControlStateNormal];
            [self.view addSubview:prodRatingStarBtn1];
        }
    }
    
    UIImageView *reviewImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 149, 320, 32)];
    reviewImgView.image = [UIImage imageNamed:@"ReviewBarBg.png"];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(70, 5, 200, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.text = [NSString stringWithFormat:@"%@",totalReviewsStr];
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor whiteColor];
    [reviewImgView addSubview:label];
    [self.view addSubview:reviewImgView];
    // [reviewImgView release];
    
    reviewTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 182, 320, 153)];
    reviewTable.delegate = self;
    reviewTable.dataSource = self;
    [self.view addSubview:reviewTable];
    
    UIImageView *shareImgView = [[UIImageView alloc] init];
    
    if (IS_IPHONE5)
        shareImgView.frame = CGRectMake(0, 337+88, 320, 32);
    else
        shareImgView.frame = CGRectMake(0, 337, 320, 32);
    
    shareImgView.image = [UIImage imageNamed:@"ShareBg.png"];
    [self.view addSubview:shareImgView];
    //  [shareImgView release];
    
    [self setUpBottomButtons];
    
    
}

-(void)parse_fetchuserrating:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *productDetailElement = [TBXML childElementNamed:@"productDetail" parentElement:tbxml.rootXMLElement];
    
    if (productDetailElement == nil){
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
        [UtilityManager showAlert:nil msg:responseTextStr];
        return;
    }
    TBXMLElement *titleTextElement = [TBXML childElementNamed:@"titleText" parentElement:tbxml.rootXMLElement];
    TBXMLElement *productNameElement = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:productDetailElement];
    TBXMLElement *emailTemplateUrlElement = [TBXML childElementNamed:@"emailTemplateURL" parentElement:productDetailElement];
    TBXMLElement *imagePathElement = [TBXML childElementNamed:@"imagePath" parentElement:productDetailElement];
    
    // code for share from this location
    NSMutableString *shareContent = [NSMutableString stringWithFormat:@"%@\n%@\n",[TBXML textForElement:titleTextElement],[TBXML textForElement:productNameElement]];
    
    if (emailTemplateUrlElement !=nil && ![[TBXML textForElement:emailTemplateUrlElement]isEqualToString:@"N/A"]) {
        
        [shareContent appendFormat:@"%@",[TBXML textForElement:emailTemplateUrlElement]];
    }
    
    [defaults  setObject:[TBXML textForElement:imagePathElement] forKey:KEY_PRODUCTIMGPATH];
    DLog(@"Share Content:%@",shareContent);
    
    //When coming from This Location
    TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbxml.rootXMLElement];
    if (retailerDetailElement !=nil){
        TBXMLElement *retailerNameElement = [TBXML childElementNamed:KEY_RTLNAME parentElement:retailerDetailElement];
        TBXMLElement *completeAddressElement = [TBXML childElementNamed:@"completeAddress" parentElement:retailerDetailElement];
        TBXMLElement *salePriceElement = [TBXML childElementNamed:@"salePrice" parentElement:retailerDetailElement];
        
        [shareContent appendFormat:@"\nPrice:%@\nWhere:%@\nPlace:%@",[TBXML textForElement:salePriceElement],[TBXML textForElement:retailerNameElement],[TBXML textForElement:completeAddressElement]];
        
    }
    ReleaseAndNilify(tbxml);
    [defaults setObject:shareContent forKey:KEY_SHAREMSG];
    [defaults setObject:[TBXML textForElement:emailTemplateUrlElement] forKey:KEY_HYPERLINK];
    
    if (facebookButton.tag == 100)
        [self shareToFacebook];
    
    else if (twitterButton.tag == 100)
        [self shareToTwitter];
    
    else
        [self shareThroughText];
}


#pragma mark tableView delagate and datasource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([reviewCommentsArray count]>0)
        return [reviewCommentsArray count];
    else
        return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell;/* = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];*/
    
    // if (cell == nil){
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    if ([reviewCommentsArray count]>0){
        cell.textLabel.text = [reviewCommentsArray objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        reviewTable.separatorColor = [UIColor blackColor];
    }
    
    else{
        
        reviewTable.hidden = YES;
        //reviewTable.scrollEnabled = NO;
        //reviewTable.separatorColor = [UIColor grayColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.textLabel.numberOfLines = 1;
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [defaults setObject:[reviewCommentsArray objectAtIndex:indexPath.row] forKey:@"Review"];
    [defaults setObject:[reviewUrlArray objectAtIndex:indexPath.row] forKey:@"URL"];
    ReviewDetailViewController *reviewDetail = [[ReviewDetailViewController alloc]initWithNibName:@"ReviewDetailViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:reviewDetail animated:NO];
    // [reviewDetail release];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark share via facebook
- (void)facebookButtonPressed {
    
    /*[SharedManager  setShareEmailCoupon:NO];
     [SharedManager setShareEmailHotDeal:NO];
     [SharedManager setShareEmailSpecialOffer:NO];
     [SharedManager setShareEmailSpecialOfferUrl:NO];
     [SharedManager setShareAppsite:NO];//for UT*/
    
    facebookButton.tag = 100;
    twitterButton.tag = 0;
    smsButton.tag = 0;
    emailButton.tag = 0;
    
    [self request_fetchuserrating];
}

-(void)shareToFacebook{
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


#pragma mark share via twitter
- (void)twitterButtonPressed:(id)sender {
    
    twitterButton.tag = 100;
    facebookButton.tag = 0;
    smsButton.tag = 0;
    emailButton.tag = 0;
    
    [self request_fetchuserrating];
    
}

#pragma mark share via email
- (void)emailButtonPressed {
    
    [SharedManager setShareEmailCoupon:NO];
    [SharedManager setShareEmailHotDeal:NO];
    [SharedManager setShareEmailSpecialOffer:NO];
    [SharedManager setShareEmailSpecialOfferUrl:NO];
    [SharedManager setShareAppsite:NO];
    
    emailButton.tag = 100;
    facebookButton.tag = 0;
    twitterButton.tag = 0;
    smsButton.tag = 0;
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}

#pragma mark share via text - methods
- (void)smsButtonPressed:(id)sender {
    
    smsButton.tag = 100;
    facebookButton.tag = 0;
    twitterButton.tag = 0;
    emailButton.tag = 0;
    
    [self request_fetchuserrating];
}

-(void)shareThroughText{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if (result == MessageComposeResultSent) {
        
        //For User Tracking
        NSMutableString *reqStr = [[NSMutableString alloc] init];
        [reqStr appendFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><prodId>%@</prodId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults  objectForKey:KEY_PRODUCTID]];
        
        
        NSMutableString *urlString = [BASE_URL mutableCopy];
        [urlString appendString:@"ratereview/updatesharetype"];
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
        DLog(@"%@",responseXml);
        ReleaseAndNilify(responseXml);
        // [reqStr release];
        //For User Tracking
    }
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    //[anyVC release];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end

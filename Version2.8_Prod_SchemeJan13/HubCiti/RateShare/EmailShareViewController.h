//
//  EmailShareViewController.h
//  Scansee
//
//  Created by ajit on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "MainMenuViewController.h"
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"
#import "NSData+Base64.h"

//typedef enum webserviceCalls{
//    shareapplink
//}WebserviceCallType;



typedef void (^completionBlock)(MFMailComposeViewController* controllerNeededForPresenting);

@interface EmailShareViewController : UIViewController<MFMailComposeViewControllerDelegate,UITextFieldDelegate> {
//	UISearchDisplayDelegate,UISearchBarDelegate,UITextFieldDelegate,
//	IBOutlet UISearchBar *search;
//	IBOutlet UITableView *contactsTable;
	
    NSMutableArray *searchData;
	
	BOOL m_bIsSearch;
	NSString *savedSearchString;
	
    NSMutableArray *_allItems;
    NSMutableArray *emailAddrArray;
    BOOL validEmail;
    
    NSMutableArray *content;
    NSArray *indices;
    
    NSMutableArray *contactIndexArray;

    NSMutableArray *selectedIndex;
    NSIndexPath *index;
    
    UIButton *contactSelectionButton;
    
    int flag;//to decide share item is a product or coupon or hot deal
    
    UITextField *fromEmailText;
    
    WebRequestState iWebserviceType;
    NSMutableString *shareType;
    NSMutableString *shareEmail;
}
@property(nonatomic,copy) completionBlock updateBlock;
@property(nonatomic,assign) BOOL isEmailDataLoaded;
@property (nonatomic , strong) MFMailComposeViewController *myMailViewController;
@property (nonatomic , strong) NSMutableString *shareEmail;
@property (nonatomic,strong) NSMutableString *shareType;
//-(IBAction)sendEmail:(id)sender;
//-(IBAction)selectContact:(UIButton *)sender;

-(void) loadMail;

@end

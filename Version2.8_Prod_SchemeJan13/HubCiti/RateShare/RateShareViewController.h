//
//  RateShareViewController.h
//  Scansee
//
//  Created by Ramanan Kumaravelu on 23/06/11.
//  Copyright 2011 Span Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "CommonUtility.h"
#import "AnyViewController.h"
@class AnyViewController;
@class EmailShareViewController;
typedef enum webServicescalls{
    
    saveuserrating,
    getproductsummary,
    shareproductretailerinfo,
    utsharetype,
    getproductreviews,
    fetchuserrating
}WebServiceCallType;

@interface RateShareViewController : UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,
UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    AnyViewController *anyVC;
	UIButton *facebookButton;
	UIButton *twitterButton;
	UIButton *emailButton;
	UIButton *smsButton;
	UILabel *userRating;
	NSMutableArray *reviewCommentsArray ,*reviewUrlArray;
	NSString *totalReviewsStr;
	UITableView *reviewTable;
    WebServiceCallType iWebServiceCallType;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic, strong) UIButton *facebookButton, *twitterButton, *emailButton, *smsButton;
@property (nonatomic, strong) UITableView *reviewTable;

- (void)facebookButtonPressed;
- (void)emailButtonPressed;


- (void)userRatingStarBtnPressed:(id)sender;

@end

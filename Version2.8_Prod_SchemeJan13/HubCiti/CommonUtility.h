 //
//  CommonUtility.h
//  HubCiti
//
//  Created by Ajit on 9/4/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIImageView+WebCache.h"
#import "HttpClient.h"
#import "HubCitiConstants.h"



@interface CommonUtility : NSObject <HTTPClientDelegate>  {
 //write your global variables here
    
    UIActivityIndicatorView *activityIndicator;
    UIImageView *img;
    NSInteger iWebRequestState;
    UIViewController* desViewController;
}

//set property for global variables

//declare your global methods here
-(void) dismiss :(id) alert ;
-(void) alertControllerDismiss: (id) alert;
-(void) showAlert;
-(void) showAlert:(NSString*)message;
-(BOOL) isNullOrEmptyString:(NSString *)str;
-(void)requestTimedOut;
-(BOOL)isResponseXMLNullOrEmpty:(NSString *)str;
- (void)showAlert:(NSString*)aTitle msg:(NSString*)aMsg;
- (void)showAlert:(NSString*)aTitle msg:(NSString*)aMsg  onView:(UIViewController*)viewController;
-(UIButton*)customizeBarButtonItem:(NSString*)title;
-(UIButton*)customizeBackButton;
-(UIButton*)customizeBackButtonSubCatColor;
-(void) setTransperentImageColor:(NSString*)imageColor forButton:(UIButton*)buttonTransperent;
-(void) showFormatedAlert: (NSString *) responseTextStr;
- (void)showAlertOnWindow:(NSString*)aTitle msg:(NSString*)aMsg;
-(void)popBackToViewController:(Class)viewControllerClass inNavigationController:(UINavigationController*)navigationController;
-(void)customUrlLoad:(NSURL*)url;
-(NSMutableCharacterSet *) getAllowedNSCharacterSet;
+(id)utilityManager;

-(void) req_hubcitiAnyThingPage : (UIViewController*) viewController ;

@end

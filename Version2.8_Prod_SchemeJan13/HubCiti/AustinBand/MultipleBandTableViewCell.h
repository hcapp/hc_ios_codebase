//
//  MultipleBandTableViewCell.h
//  HubCiti
//
//  Created by Nikitha on 1/18/17.
//  Copyright © 2017 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultipleBandTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *bandName_Lable;
@property (strong, nonatomic) IBOutlet UILabel *subBandGenre_Lable;
@property (strong, nonatomic) IBOutlet UILabel *MultBand_Time;
@property (strong, nonatomic) IBOutlet SdImageView *asyncImageView;
-(void) setDetailsInMultipleBandList : (NSString*) bandname categoryName : (NSString*) genreName StartEndTime :(NSString *) SETime logoOfBand : (NSString*) imagePath;
@end

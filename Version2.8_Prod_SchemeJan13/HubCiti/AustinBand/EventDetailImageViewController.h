//
//  EventDetailImageViewController.h
//  HubCiti
//
//  Created by service on 4/14/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventListDetails.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AnyViewController.h"
@class EmailShareViewController;
@class BandRetailerSummaryViewController;

@interface EventDetailImageViewController : UIViewController<UIWebViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    UIWebView * shortDescWebView;
    UIWebView * longDescWebView;
    WebRequestState iWebRequestState;
}
@property(nonatomic,strong)  EventListDetails* eventDo ;
@property(nonatomic,strong)  BandRetailerSummaryViewController* retailerSummaryPage ;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) NSNumber * noOfBandExistFlag;
@property(nonatomic,strong) NSString * popUpMsgForNoBands;
@end

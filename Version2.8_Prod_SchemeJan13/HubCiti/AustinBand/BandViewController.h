//
//  BandViewController.h
//  HubCiti
//
//  Created by Ashika on 4/14/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HubCitiSegmentedControl.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "CommonUtility.h"
#import "SortAndFilter.h"

@class BandsNearByEventsMapTable;
@class AnyViewController;
@class EmailShareViewController;
@class BandRetailerSummaryViewController;
@class EventDetailImageViewController;

@interface BandViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,MFMessageComposeViewControllerDelegate, UIWebViewDelegate,UIScrollViewDelegate,CustomizedNavControllerDelegate>{
    UISegmentedControl *segment;
    CommonUtility *common;
    NSTimer *timer;
    NSMutableArray *arrEventBottomButtonDO;
    NSMutableArray *arrBottomButtonViewContainer;
    WebRequestState iWebRequestState;
    NSNumber *lowerLimit;
    BOOL isRemember;
    BOOL smartSearchBand,smartSearchEvent;
    int bottomButton;
    int nextPage,nextBefore;
    NSNumber *lastRecord;
    UIActivityIndicatorView *loading;
    CustomizedNavController *cusNav;

}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,strong) BandsNearByEventsMapTable *bandListPage;
@property(nonatomic,strong) BandRetailerSummaryViewController *bandSummaryPage;
@property(nonatomic,strong) EventDetailImageViewController *eventSummaryPage;
@property(nonatomic,strong) SortAndFilter *sortObj,*savedSortObj;
@property (strong, nonatomic) UISearchBar *searchBands;
@property (nonatomic, weak) UIView *buttonView1,*buttonView2,*buttonView3,*buttonView4;
@property (nonatomic, weak) UIView *buttonEventView1,*buttonEventView2,*buttonEventView3,*buttonEventView4;
@property (strong, nonatomic) UITableView *bandTableView;
@property(strong,nonatomic) NSString * bandPageName;



@end

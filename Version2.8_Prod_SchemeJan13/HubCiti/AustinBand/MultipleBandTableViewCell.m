//
//  MultipleBandTableViewCell.m
//  HubCiti
//
//  Created by Nikitha on 1/18/17.
//  Copyright © 2017 Keshavakarthik S. All rights reserved.
//

#import "MultipleBandTableViewCell.h"

@implementation MultipleBandTableViewCell
@synthesize bandName_Lable,subBandGenre_Lable,asyncImageView,MultBand_Time;
- (void)awakeFromNib {
    [super awakeFromNib];
    [subBandGenre_Lable setTextColor:[UIColor darkGrayColor]];
    [MultBand_Time setTextColor:[UIColor darkGrayColor]];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void) setDetailsInMultipleBandList : (NSString*) bandname categoryName : (NSString*) genreName StartEndTime :(NSString *) SETime logoOfBand : (NSString*) imagePath
{
    bandName_Lable.text = nil;
    subBandGenre_Lable.text = nil;
    MultBand_Time.text = nil;
    asyncImageView.image = nil;
    
    bandName_Lable.text = bandname;
    subBandGenre_Lable.text = genreName;
    MultBand_Time.text = SETime;
    [asyncImageView loadImage:imagePath];
}
@end

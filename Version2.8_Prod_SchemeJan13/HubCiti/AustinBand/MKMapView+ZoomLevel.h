//
//  MKMapView+ZoomLevel.h
//  HubCiti
//
//  Created by Lakshmi H R on 4/26/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (ZoomLevel)

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated;

- (void)setZoomLevel:(NSUInteger)zoomLevel;
- (NSUInteger)zoomLevel ;

@end

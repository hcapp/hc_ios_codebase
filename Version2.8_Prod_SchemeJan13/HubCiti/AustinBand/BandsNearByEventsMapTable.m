//
//  BandsNearByEventsMapTable.m
//  HubCiti
//
//  Created by Lakshmi H R on 4/14/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "BandsNearByEventsMapTable.h"
#import "HTTPClient.h"
#import "AppDelegate.h"
#import "RetailerAnnotation.h"
#import "EventDetailImageViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "AnythingPage.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "AppDelegate.h"
//#import "ScanNowScreen.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FAQCategoryList.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "CityExperienceViewController.h"
#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "FindOptionsViewController.h"
#import "SettingsViewController.h"
#import "EventsListViewController.h"
#import "FindSortViewController.h"
#import "FilterListViewController.h"
#import "FundraiserListViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "SingleCatRetailers.h"
#import "SpecialOffersViewController.h"
#import "EventGroupListDetails.h"
#import "CouponsInDealViewController.h"
#import "CouponsMyAccountsViewController.h"
//#import "MarqueeLabel.h"





//#define MINIMUM_ZOOM_ARC 0.014 //approximately 1 miles (1 degree of arc ~= 69 miles)
//#define ANNOTATION_REGION_PAD_FACTOR 1.15
//#define MAX_DEGREES_ARC 360

NSDictionary *tableViewDictionary, *viewDictionary;

@interface BandsNearByEventsMapTable ()<HTTPClientDelegate,UITableViewDataSource,UITableViewDelegate>

@end


@implementation BandsNearByEventsMapTable
{
    NSInteger numberOfRows;
    UIActivityIndicatorView *loading;
    BOOL fromFullscreen;
    UIButton *fullscreenButton;
    BOOL fromFilter;
    NSMutableArray *arrBottomButtonViewContainer;
    NSMutableArray *arrBandEventsBottomButtonDO;
    int bottomBtn;
    NSDictionary *tableViewDictionary, *viewDictionary;
    NSMutableArray *eventGrouingInfo;
    bottomButtonView *view;
    NSString * bandName;
}

@synthesize bandMapView;
@synthesize bandEventsTable;
@synthesize zoomBar;
@synthesize eventTypeID;
@synthesize bandID;
@synthesize bandEventsResponse;
@synthesize eventListDetails;
@synthesize bandTitle;
@synthesize eventImageDetail;
@synthesize sortFilObj;
@synthesize eventTypeName;
@synthesize fromTodayEvents;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject

{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    [self responseData:responseObject];
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    
    lowerLimitValue = [[NSNumber alloc]initWithInt:0];
    nextPage = [[NSNumber alloc]init];
    rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
    //lowerLimitValue = 0;
    [self.view setAccessibilityLabel:@"BandMapTableView"];
    [defaults setBool:NO forKey:@"ViewMoreEvent"];
    [defaults setValue:nil forKey:@"playingTodayMitemId"];
    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = mainPage;
    
    //    if(self.eventTypeID)
    //    {
    //        self.title = self.eventTypeName;
    //    }
    //    else if(self.bandTitle)
    //    {
    //        self.title = self.bandTitle;
    //    }
    CustomizedNavController *cusNavigation =(CustomizedNavController *) self.navigationController;
    [cusNavigation setTitle:@"Shows" forView:self withHambergur:YES];
    //self.title = @"Shows";
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    
    
    
    
    fromFilter = FALSE;
    
    // Do any additional setup after loading the view from its nib.
    bandMapView = [[MKMapView alloc]init];
    bandMapView.frame = CGRectMake(0, 0, SCREEN_WIDTH,(SCREEN_HEIGHT-VARIABLE_HEIGHT(64))/4);
    bandMapView.mapType = MKMapTypeStandard;
    bandMapView.zoomEnabled = true;
    bandMapView.scrollEnabled = true;
    //[mapView setCenterCoordinate:CLLocationCoordinate2DMake(0,0)];
    
    bandMapView.hidden = YES;
    [self.view addSubview:bandMapView];
    
    //    mapPinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(mapPinch:)];
    //    [self.bandMapView addGestureRecognizer:mapPinch];
    
    zoomBar = [[UIView alloc]init];
    zoomBar.frame = CGRectMake(0, self.bandMapView.frame.origin.y+self.bandMapView.frame.size.height-VARIABLE_HEIGHT(25), SCREEN_WIDTH,VARIABLE_HEIGHT(25));
    zoomBar.hidden = NO;
    
    zoomBar.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:.4];
    UIImage *fullscreenImg = [UIImage imageNamed:@"fullscreen.png"];
    
    
    fullscreenButton= [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-VARIABLE_WIDTH(15)-VARIABLE_WIDTH(30), self.bandMapView.frame.origin.y+self.bandMapView.frame.size.height-VARIABLE_HEIGHT(25)+VARIABLE_HEIGHT(5),VARIABLE_WIDTH(15) ,VARIABLE_HEIGHT(15))];
    [fullscreenButton addTarget:self action:@selector(fullScreen) forControlEvents:UIControlEventTouchUpInside];
    [fullscreenButton setBackgroundImage:fullscreenImg forState:UIControlStateNormal];
    //[fullscreenButton setBackgroundColor:[UIColor blackColor]];
    
    
    [self.bandMapView addSubview:zoomBar];
    [self.bandMapView addSubview:fullscreenButton];
    // [zoomBar bringSubviewToFront:fullscreenButton];
    [self settableViewOnScreen];
    
    
    annArray = [[NSMutableArray alloc] init]; // Map Annotation points
    bandEventsTable.dataSource = self;
    bandEventsTable.delegate = self;
    sortFilObj = [[SortAndFilter alloc]init];
    
    if(!fromTodayEvents)
    {
        sortFilObj.sortEventDateSelected = YES;
    }
    else if(fromTodayEvents)
    {
        sortFilObj.distanceSelected = YES;
    }
    sortFilObj.sortEventDateSelected = YES;
    bandEventsListArray = [[NSMutableArray alloc]init];
    eventGrouingInfo = [[NSMutableArray alloc]init];
    eventLatArray = [[NSMutableArray alloc]init];
    eventLongArray = [[NSMutableArray alloc]init];
    eventAddArray = [[NSMutableArray alloc]init];
    eventNameArray = [[NSMutableArray alloc]init];
    
    
    [ self request_EventDetails:sortFilObj];
    
    
    
    
    
}
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:NO];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    fromFilter = FALSE;
    if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
        NSData *encodedObject = [defaults objectForKey:@"SortFilterObject"];
        SortAndFilter *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        NSLog(@"SortAndFilter object in viewWillAppear: %@",object);
        [eventGrouingInfo removeAllObjects];
        fromFilter = TRUE;
        // ReleaseAndNilify(bandEventsTable);
        rowCountOfTotalCells = 0;
        //[bandEventsArray removeAllObjects];
        [eventAddArray removeAllObjects];
        [eventLatArray removeAllObjects];
        [eventLongArray removeAllObjects];
        [eventNameArray removeAllObjects];
        [annArray removeAllObjects];
        fromFullscreen = YES;
        [bandMapView removeAnnotations:bandMapView.annotations];
       
        sortFilObj = [[SortAndFilter alloc]initWithObject:[self loadCustomObjectWithKey:@"SortFilterObject"]];
        DLog(@"%d", sortFilObj.distanceSelected);
        
        [self request_EventDetails:sortFilObj];
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        [self fullScreen];
        
        [defaults setValue:nil forKey:@"SortFilterObject"];
    }
    
    
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}
- (UIImageView *)createHomeButtonView { ///
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
    label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//- (void)mapPinch:(UIPinchGestureRecognizer *)recognizer
//{
//    if(bandMapView.zoomLevel > 4)
//    {
//        bandMapView.userInteractionEnabled = NO;
//    }
//}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)fullScreen
{
    if(!fromFullscreen)
    {
        UIImage *fullscreenImg = [UIImage imageNamed:@"smallscreen.png"];
        [fullscreenButton setBackgroundImage:fullscreenImg forState:UIControlStateNormal];
        [UIView animateWithDuration:0.4
                              delay:0.0
                            options: UIViewAnimationOptionTransitionNone
                         animations:^
         {
             
             bandEventsTable.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 0);
             bandMapView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
             if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
                 if(bottomBtn == 1)
                 {
                     zoomBar.frame = CGRectMake(0, self.bandMapView.frame.origin.y+self.bandMapView.frame.size.height-VARIABLE_HEIGHT(25)-VARIABLE_HEIGHT(30) - 50, SCREEN_WIDTH,VARIABLE_HEIGHT(25));
                     fullscreenButton.frame = CGRectMake(SCREEN_WIDTH-VARIABLE_WIDTH(15)-VARIABLE_WIDTH(30), self.bandMapView.frame.origin.y+self.bandMapView.frame.size.height-VARIABLE_HEIGHT(25)-VARIABLE_HEIGHT(30) - 50 +VARIABLE_HEIGHT(5),VARIABLE_WIDTH(15) ,VARIABLE_HEIGHT(15));
                 }
                 else
                 {
                     zoomBar.frame = CGRectMake(0, self.bandMapView.frame.origin.y+self.bandMapView.frame.size.height-VARIABLE_HEIGHT(25)-VARIABLE_HEIGHT(30), SCREEN_WIDTH,VARIABLE_HEIGHT(25));
                     fullscreenButton.frame = CGRectMake(SCREEN_WIDTH-VARIABLE_WIDTH(15)-VARIABLE_WIDTH(30), self.bandMapView.frame.origin.y+self.bandMapView.frame.size.height-VARIABLE_HEIGHT(25)-VARIABLE_HEIGHT(30)+VARIABLE_HEIGHT(5),VARIABLE_WIDTH(15) ,VARIABLE_HEIGHT(15));
                 }
             }
             else{
                 if(bottomBtn == 1)
                 {
                     zoomBar.frame = CGRectMake(0, self.bandMapView.frame.origin.y+self.bandMapView.frame.size.height-VARIABLE_HEIGHT(25)-VARIABLE_HEIGHT(52) - 50, SCREEN_WIDTH,VARIABLE_HEIGHT(25));
                     fullscreenButton.frame = CGRectMake(SCREEN_WIDTH-VARIABLE_WIDTH(15)-VARIABLE_WIDTH(30), self.bandMapView.frame.origin.y+self.bandMapView.frame.size.height-VARIABLE_HEIGHT(25)-VARIABLE_HEIGHT(52) - 50 +VARIABLE_HEIGHT(5),VARIABLE_WIDTH(15) ,VARIABLE_HEIGHT(15));
                 }
                 else
                 {
                     zoomBar.frame = CGRectMake(0, self.bandMapView.frame.origin.y+self.bandMapView.frame.size.height-VARIABLE_HEIGHT(25)-VARIABLE_HEIGHT(52), SCREEN_WIDTH,VARIABLE_HEIGHT(25));
                     fullscreenButton.frame = CGRectMake(SCREEN_WIDTH-VARIABLE_WIDTH(15)-VARIABLE_WIDTH(30), self.bandMapView.frame.origin.y+self.bandMapView.frame.size.height-VARIABLE_HEIGHT(25)-VARIABLE_HEIGHT(52) +VARIABLE_HEIGHT(5),VARIABLE_WIDTH(15) ,VARIABLE_HEIGHT(15));
                 }
             }
             
         }
                         completion:^(BOOL finished)
         {
             NSLog(@"Completed");
             
         }];
        
        bandEventsTable.hidden = YES;
        
        fromFullscreen = YES;
    }
    else
    {
        bandEventsTable.hidden = NO;
        UIImage *fullscreenImg = [UIImage imageNamed:@"fullscreen.png"];
        [fullscreenButton setBackgroundImage:fullscreenImg forState:UIControlStateNormal];
        fromFullscreen = NO;
        
        [UIView animateWithDuration:0.4
                              delay:0
                            options:UIViewAnimationOptionTransitionNone
                         animations:^
         {
             bandMapView.frame = CGRectMake(0, 0, SCREEN_WIDTH, ((SCREEN_HEIGHT-VARIABLE_HEIGHT(64))/4));
             
             zoomBar.frame = CGRectMake(0, self.bandMapView.frame.origin.y+self.bandMapView.frame.size.height-VARIABLE_HEIGHT(25), SCREEN_WIDTH,VARIABLE_HEIGHT(25));
             fullscreenButton.frame = CGRectMake(SCREEN_WIDTH-VARIABLE_WIDTH(15)-VARIABLE_WIDTH(30), self.bandMapView.frame.origin.y+self.bandMapView.frame.size.height-VARIABLE_HEIGHT(25)+VARIABLE_HEIGHT(5),VARIABLE_WIDTH(15) ,VARIABLE_HEIGHT(15));
             int yVal = self.bandMapView.frame.origin.y + self.bandMapView.frame.size.height;
             if(bottomBtn == 1)
             {
                 if(IPAD)
                 {
                     bandEventsTable.frame = CGRectMake(0, yVal, SCREEN_WIDTH, SCREEN_HEIGHT - yVal - VARIABLE_HEIGHT(55));
                 }
                 else
                 {
                     bandEventsTable.frame = CGRectMake(0, yVal, SCREEN_WIDTH, SCREEN_HEIGHT - yVal - VARIABLE_HEIGHT(95));
                 }
                 
             }
             else
             {
                 bandEventsTable.frame = CGRectMake(0, yVal, SCREEN_WIDTH, SCREEN_HEIGHT-yVal);
             }
             
             
             
         }
                         completion:^(BOOL finished)
         {
             NSLog(@"Completed");
             
         }];
        
    }
}

#pragma mark tableview delegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = VARIABLE_HEIGHT(60);
    if (indexPath.row != [[[eventGrouingInfo objectAtIndex:indexPath.section] eventGrpArray] count])
    {
        if([[[[eventGrouingInfo objectAtIndex:indexPath.section] eventGrpArray] objectAtIndex:indexPath.row] evtLocTitle].length && ![[[[[eventGrouingInfo objectAtIndex:indexPath.section] eventGrpArray] objectAtIndex:indexPath.row] evtLocTitle] isEqualToString:@"N/A"])
        {
            if(bandName != nil)
            {
            cellHeight = VARIABLE_HEIGHT(80);
            }
            else
            {
                cellHeight = VARIABLE_HEIGHT(65);
            }
        }
    }
    else
    {
        cellHeight = VARIABLE_HEIGHT(60);
    }
    return cellHeight;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(fromTodayEvents && eventGrouingInfo.count!=0)
    {
        return 1;
    }
    else if (eventGrouingInfo.count!=0) {
        return [eventGrouingInfo count];
    }
    else{
        
        return 0;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(eventGrouingInfo!=0){
        EventGroupListDetails *eventGroupList =[eventGrouingInfo objectAtIndex:section];
        if([nextPage integerValue] == 1 && section == [eventGrouingInfo count]-1)
            numberOfRows = eventGroupList.eventGrpArray.count + 1;
        else
            numberOfRows = eventGroupList.eventGrpArray.count;
    }
    return numberOfRows;
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    //    float labelFont = 15.0;
    //    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
    //        labelFont = 18.0;
    //    }
    //    EventGroupListDetails *eventGroupList =[eventGrouingInfo objectAtIndex:section];
    //
    //    UILabel *lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH - 10, 18)];
    //    [lblCatName setTextColor:[UIColor blackColor]];
    //    [lblCatName setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
    //    [lblCatName setFont:[UIFont boldSystemFontOfSize:labelFont]];
    //    [lblCatName setText:[NSString stringWithFormat:@" %@",eventGroupList.groupContent]];
    //    return lblCatName;
    if(!fromTodayEvents)
    {
        UILabel *lblCatName;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH - 10, 18)];
            [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
        }
        else
        {
            lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH - 10, 28)];
            [lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
        }
        [lblCatName setTextColor:[UIColor blackColor]];
        [lblCatName setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
        
        EventGroupListDetails *eventGroupList =[eventGrouingInfo objectAtIndex:section];
        [lblCatName setText:[NSString stringWithFormat:@" %@",eventGroupList.groupContent]];
        
        return lblCatName;
    }
    else
    {
        return nil;
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EventGroupListDetails *eventGrouplist =[eventGrouingInfo objectAtIndex:indexPath.section];
    if([nextPage intValue]==1 && indexPath.row == [eventGrouplist.eventGrpArray count] ) {
        // This is the last cell
        if (![defaults boolForKey:@"ViewMoreEvent"] && rowCountOfTotalCells !=0) {
            [defaults setBool:YES forKey:@"ViewMoreEvent"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self fetchNextBandEvents];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [bandEventsTable reloadData];
                    [bandEventsTable setContentOffset:CGPointZero animated:YES];
                    
                });
            });
            
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    SdImageView *eventImageView ;
    UILabel *eventNameLabel ;
    UILabel *bandNameLabel ;
    UILabel *bandTypeLabel ;
    SdImageView *locationPin ;
    UILabel *distanceLabel ;
    UILabel *eventDateLabel ;
    //MarqueeLabel *venueName;
    UILabel *venueName;
    //UIImageView *specImage = nil;
    
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.contentView.tag = 1;
    //if (cell == nil)
    // {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] ;
    // }
    
   // float viewMoreLabelFont = 16.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
       // viewMoreLabelFont = 20.0;
    }
    
    
    
    EventGroupListDetails *eventGroupList =[eventGrouingInfo objectAtIndex:indexPath.section];
    if (indexPath.row != [eventGroupList.eventGrpArray count])
    {
        eventImageView = [[SdImageView alloc] init] ;
        eventImageView.tag = ASYNC_IMAGE_TAG;
        // asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
        eventImageView.backgroundColor = [UIColor clearColor];
        eventImageView.layer.masksToBounds = YES;
        [cell.contentView addSubview:eventImageView];
        
        eventDateLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            eventDateLabel.font = [UIFont boldSystemFontOfSize:12];
        else
            eventDateLabel.font = [UIFont boldSystemFontOfSize:8];
        //eventDateLabel.textColor = [UIColor colorWithRGBInt:0x112e72];
        eventDateLabel.textColor = [UIColor lightGrayColor];
        eventDateLabel.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:eventDateLabel];
        
        
        
        eventNameLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            eventNameLabel.font = [UIFont boldSystemFontOfSize:19];
        else
            eventNameLabel.font = [UIFont boldSystemFontOfSize:14];
        //eventNameLabel.textColor = [UIColor colorWithRGBInt:0x112e72];
        eventNameLabel.textColor = [UIColor blackColor];
        
        [cell.contentView addSubview:eventNameLabel];
        
        bandNameLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            bandNameLabel.font = [UIFont boldSystemFontOfSize:15];
        else
            bandNameLabel.font = [UIFont boldSystemFontOfSize:12];
        bandNameLabel.textColor = [UIColor darkGrayColor];
        bandNameLabel.numberOfLines = 2;
        [cell.contentView addSubview:bandNameLabel];
        
        bandTypeLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            bandTypeLabel.font = [UIFont boldSystemFontOfSize:15];
        else
            bandTypeLabel.font = [UIFont boldSystemFontOfSize:12];
        bandTypeLabel.textColor = [UIColor lightGrayColor];
        // distanceLabel.numberOfLines = 1;
        [cell.contentView addSubview:bandTypeLabel];
        
        locationPin = [[SdImageView alloc] init] ;
        locationPin.tag = ASYNC_IMAGE_TAG;
        // asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
        locationPin.backgroundColor = [UIColor clearColor];
        locationPin.layer.masksToBounds = YES;
        [cell.contentView addSubview:locationPin];
        
        distanceLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            distanceLabel.font = [UIFont boldSystemFontOfSize:15];
        else
            distanceLabel.font = [UIFont boldSystemFontOfSize:12];
        distanceLabel.textColor = [UIColor lightGrayColor];
        // distanceLabel.numberOfLines = 1;
        [cell.contentView addSubview:distanceLabel];
        
        
        //venueName = [[MarqueeLabel alloc]init];
        venueName = [[UILabel alloc]init];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            venueName.font = [UIFont boldSystemFontOfSize:15];
        else
            venueName.font = [UIFont boldSystemFontOfSize:12];
        
        venueName.textColor = [UIColor grayColor];
        venueName.lineBreakMode = NSLineBreakByTruncatingTail;
        venueName.numberOfLines = 1;
        [cell.contentView addSubview:venueName];
        
        
        //        venueName.backgroundColor = [UIColor blueColor];
        //        [cell.contentView addSubview:venueName];
        //        [venueName setTitle:@"Venue Name" forState:UIControlStateNormal];
        //        [venueName setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //        [venueName.layer setBorderWidth:3.0];
        //        [venueName.layer setBorderColor:[[UIColor blackColor] CGColor]];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (indexPath.row<[eventGroupList.eventGrpArray count])
        {
            
          //  NSString *calenderImg = @"calendar.png";
            //            if(![calenderImg isEqualToString:@"N/A"] )
            //            {
            //                [eventImageView loadImagefrommainBundel:calenderImg];
            //            }
            
            
            EventListDetails * eventDetail = [[EventListDetails alloc] init];
            
            if([[eventGroupList.eventGrpArray objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
                NSDictionary* dictList = [eventGroupList.eventGrpArray objectAtIndex:indexPath.row];
                
                [eventDetail setValuesForKeysWithDictionary:dictList];
                
            }
            else {
                eventDetail = [eventGroupList.eventGrpArray objectAtIndex:indexPath.row];
            }
            //   EventListDetails *eventDetail = (EventListDetails*) [eventGroupList.eventList objectAtIndex:indexPath.row];
            
            if([eventDetail eventName].length)
            {
                eventNameLabel.text = [eventDetail eventName];
            }
            else
            {
                eventNameLabel.text = @" ";
            }
            if([eventDetail bandName].length)
            {
                bandNameLabel.text = [eventDetail bandName];
                bandName = bandNameLabel.text;
            }
            else
            {
                bandNameLabel.text = @" ";
                bandName = nil;
            }
            if([eventDetail eventCatName].length)
            {
                bandTypeLabel.text = [eventDetail eventCatName];
            }
            else
            {
                bandTypeLabel.text = @" ";
            }
            
            NSString *pin = @"location_b.png";
            [locationPin loadImagefrommainBundel:pin];
            
            if([eventDetail distance].length)
            {
                distanceLabel.text = [eventDetail distance];
            }
            else
            {
                distanceLabel.text = @" ";
            }
            if([eventDetail startDate].length)
            {
                NSString *eventDate = [eventDetail startDate];
                eventDate = [self dateConverter: eventDate];
                eventDateLabel.text=[eventDate substringToIndex:6];
            }
            else
            {
                eventDateLabel.text = @" ";
            }
            if([eventDetail listingImgPath].length)
            {
                [eventImageView loadImage:[eventDetail listingImgPath]];
            }
            if([eventDetail evtLocTitle].length && ![[eventDetail evtLocTitle] isEqualToString:@"N/A"])
            {
                venueName.text = [NSString stringWithFormat:@"Location: %@",[eventDetail evtLocTitle]];
            }
            else if([[eventDetail evtLocTitle] isEqualToString:@"N/A"])
            {
                venueName.text = @" ";
            }
            
        }
    }
    
    //view more results...
    else if (indexPath.row == [eventGroupList.eventGrpArray count] && [nextPage intValue]== 1)
    {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGRect frame;
        if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
            frame.origin.x = 52;
            frame.origin.y = 12;
            frame.size.width = 250;
            frame.size.height = 24;
        }
        else{
            frame.origin.x = 70;
            frame.origin.y = 20;
            frame.size.width = 600;
            frame.size.height = 30;
        }
        loading.frame = frame;
        
        //loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
        
    }
    
    eventImageView.translatesAutoresizingMaskIntoConstraints = NO;
    eventNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    bandNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    bandTypeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    locationPin.translatesAutoresizingMaskIntoConstraints = NO;
    distanceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    eventDateLabel.translatesAutoresizingMaskIntoConstraints = NO;
    venueName.translatesAutoresizingMaskIntoConstraints = NO;
    
    if(eventImageView != nil && eventNameLabel != nil && bandNameLabel != nil && bandTypeLabel != nil && locationPin != nil && distanceLabel != nil && eventDateLabel != nil )
        tableViewDictionary = NSDictionaryOfVariableBindings(eventImageView,eventNameLabel,bandNameLabel,bandTypeLabel,locationPin,distanceLabel,eventDateLabel,venueName);
    else
        tableViewDictionary = nil;
    
    [self setConstraints:cell tag:1];
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventGroupListDetails *eventGroup =[eventGrouingInfo objectAtIndex:indexPath.section];
    //EventListDetails *eventList = [[EventListDetails alloc]init];
    [defaults setBool:NO forKey:@"ViewMoreEvent"];
    EventDetailImageViewController* evt = [[EventDetailImageViewController alloc] initWithNibName:@"EventDetailImageViewController" bundle:nil];
    self.eventImageDetail = evt;
    
    
    //    NSDictionary* dictList = [eventGroup.eventGrpArray objectAtIndex:indexPath.row];
    //
    //    [eventList setValuesForKeysWithDictionary:dictList];
    evt.eventDo = [eventGroup.eventGrpArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:evt animated:YES];
    [bandEventsTable deselectRowAtIndexPath:[bandEventsTable indexPathForSelectedRow] animated:YES];
    
}
-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(!fromTodayEvents)
    {
        if(IPAD )
        {
            return 28;
        }
        else
        {
            return 18;
        }
    }
    else
    {
        return 0;
    }

}

-(void)settableViewOnScreen
{
    if(!bandEventsTable){
        bandEventsTable = [[UITableView alloc]init];
    }
    bandEventsTable.dataSource=self;
    bandEventsTable.delegate=self;
    [bandEventsTable setBackgroundColor:[UIColor whiteColor]];
    [bandEventsTable setAccessibilityIdentifier:@"findRetailers"];
    bandEventsTable.tableFooterView.hidden=YES;
    [self.view addSubview:bandEventsTable];
    //bandEventsTable.translatesAutoresizingMaskIntoConstraints = NO;
    [self setConstraints:nil tag:0];
    if(bottomBtn == 1)
    {
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad)
        {
            [bandEventsTable setContentInset:UIEdgeInsetsMake(0, 0, VARIABLE_HEIGHT(20), 0)];
        }
        else
        {
            [bandEventsTable setContentInset:UIEdgeInsetsMake(0, 0, VARIABLE_HEIGHT(5), 0)];
        }
    }
    else
    {
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad)
        {
            [bandEventsTable setContentInset:UIEdgeInsetsMake(0, 0, VARIABLE_HEIGHT(30), 0)];
        }
        else
        {
            [bandEventsTable setContentInset:UIEdgeInsetsMake(0, 0, VARIABLE_HEIGHT(50), 0)];
        }
    }
    
}

-(void)setConstraints:(UITableViewCell*)cell tag:(CGFloat)tag
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    if(tag == 0)
    {
        viewDictionary = NSDictionaryOfVariableBindings(bandEventsTable);
        int yVal = self.bandMapView.frame.origin.y + self.bandMapView.frame.size.height;
        
        if(bottomBtn == 1)
        {
            if(IPAD)
            {
                bandEventsTable.frame = CGRectMake(0, yVal, SCREEN_WIDTH, SCREEN_HEIGHT - yVal - VARIABLE_HEIGHT(55));
            }
            else
            {
                bandEventsTable.frame = CGRectMake(0, yVal, SCREEN_WIDTH, SCREEN_HEIGHT - yVal - VARIABLE_HEIGHT(95));
            }
        }
        else
        {
            bandEventsTable.frame = CGRectMake(0, yVal, SCREEN_WIDTH, SCREEN_HEIGHT-yVal);
        }
        
    }
    else
    {
        if(tableViewDictionary != nil)
        {
            float yPos = 0;
            float valueForPin = 0;
            if([tableViewDictionary objectForKey:@"eventImageView"] != nil)
            {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[eventImageView(%f)]",VARIABLE_HEIGHT(5.0),VARIABLE_HEIGHT(30.0)] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[eventImageView(%f)]",VARIABLE_WIDTH(10.0),VARIABLE_WIDTH(50.0)] options:0 metrics:0 views:tableViewDictionary]];
            }
            if([tableViewDictionary objectForKey:@"eventDateLabel"] != nil)
            {
                
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[eventDateLabel(%f)]",VARIABLE_HEIGHT(40.0),VARIABLE_HEIGHT(10.0)] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[eventDateLabel(%f)]",VARIABLE_WIDTH(13.0),VARIABLE_WIDTH(40.0)] options:0 metrics:0 views:tableViewDictionary]];
            }
            
            if([tableViewDictionary objectForKey:@"eventNameLabel"] != nil)
            {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[eventNameLabel(%f)]",VARIABLE_HEIGHT(5.0),VARIABLE_HEIGHT(15.0)] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[eventNameLabel(%f)]",VARIABLE_WIDTH(70.0),VARIABLE_WIDTH(235.0)] options:0 metrics:0 views:tableViewDictionary]];
                yPos = yPos + VARIABLE_HEIGHT(20);
            }
            
            if([tableViewDictionary objectForKey:@"bandNameLabel"] != nil)
            {
              if(bandName != nil)
              {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[bandNameLabel(%f)]",yPos+ VARIABLE_HEIGHT(2),VARIABLE_HEIGHT(12.0)] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[bandNameLabel(%f)]",VARIABLE_WIDTH(70.0),VARIABLE_WIDTH(235.0)] options:0 metrics:0 views:tableViewDictionary]];
                  yPos = yPos+ VARIABLE_HEIGHT(14);
              }
            }
            if([tableViewDictionary objectForKey:@"bandTypeLabel"] != nil)
            {
               valueForPin = yPos+VARIABLE_HEIGHT(6);
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[bandTypeLabel(%f)]",yPos+VARIABLE_HEIGHT(6),VARIABLE_HEIGHT(12.0)] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[bandTypeLabel(%f)]",VARIABLE_WIDTH(70.0),VARIABLE_WIDTH(120.0)] options:0 metrics:0 views:tableViewDictionary]];
                yPos = yPos+ VARIABLE_HEIGHT(18);
            }
            if([tableViewDictionary objectForKey:@"venueName"])
            {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[venueName(%f)]",yPos+VARIABLE_HEIGHT(3),VARIABLE_HEIGHT(15.0)] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[venueName(%f)]",VARIABLE_WIDTH(70.0),VARIABLE_WIDTH(235.0)] options:0 metrics:0 views:tableViewDictionary]];
                //yPos = yPos + VARIABLE_HEIGHT(18);
            }
            
            
            if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            {
                if([tableViewDictionary objectForKey:@"locationPin"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[locationPin(%f)]",valueForPin,VARIABLE_HEIGHT(12.0)] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[locationPin(%f)]",VARIABLE_WIDTH(200.0),VARIABLE_WIDTH(12.0)] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                if([tableViewDictionary objectForKey:@"distanceLabel"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[distanceLabel(%f)]",valueForPin,VARIABLE_HEIGHT(12.0)] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[distanceLabel(%f)]",VARIABLE_WIDTH(220.0),VARIABLE_WIDTH(50.0)] options:0 metrics:0 views:tableViewDictionary]];
                }
                //                if([tableViewDictionary objectForKey:@"venueName"])
                //                {
                //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[venueName(%f)]",VARIABLE_HEIGHT(5.0),VARIABLE_HEIGHT(15.0)] options:0 metrics:0 views:tableViewDictionary]];
                //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[venueName(%f)]",VARIABLE_WIDTH(200.0),VARIABLE_WIDTH(120.0)] options:0 metrics:0 views:tableViewDictionary]];
                //                }
            }
            
            else
            {
                if([tableViewDictionary objectForKey:@"locationPin"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[locationPin(%f)]",valueForPin,VARIABLE_HEIGHT(12.0)] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[locationPin(%f)]",VARIABLE_WIDTH(200.0),VARIABLE_WIDTH(12.0)] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                if([tableViewDictionary objectForKey:@"distanceLabel"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[distanceLabel(%f)]",valueForPin,VARIABLE_HEIGHT(12.0)] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[distanceLabel(%f)]",VARIABLE_WIDTH(215.0),VARIABLE_WIDTH(50.0)] options:0 metrics:0 views:tableViewDictionary]];
                }
                //                if([tableViewDictionary objectForKey:@"venueName"])
                //                {
                //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[venueName(%f)]",VARIABLE_HEIGHT(5.0),VARIABLE_HEIGHT(15.0)] options:0 metrics:0 views:tableViewDictionary]];
                //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[venueName(%f)]",VARIABLE_WIDTH(200.0),VARIABLE_WIDTH(100.0)] options:0 metrics:0 views:tableViewDictionary]];
                //                }
                
            }
        }
    }
}
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
    [defaults setBool:NO forKey:@"ViewMoreEvent"];
}
-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
    
}
-(void)optionsShowMap
{
    [bandMapView setMapType:MKMapTypeStandard];
    [bandMapView setZoomEnabled:YES];
    [bandMapView setScrollEnabled:YES];
    [bandMapView setDelegate:self];
    CLLocationCoordinate2D southWest;
    CLLocationCoordinate2D northEast;
    //check is reqd as in case only scansee data is there and no google data it would crash
    if(eventLatArray.count && eventLongArray.count)
    {
        southWest = CLLocationCoordinate2DMake([[eventLatArray objectAtIndex:0] doubleValue],
                                               [[eventLongArray objectAtIndex:0] doubleValue]);
        northEast = southWest;
    }
    bandMapView.hidden = NO;
    for(int i=0; i<[eventLatArray count]&&[eventLongArray count]; i++) {
        RetailerAnnotation *ann = [[RetailerAnnotation alloc] init];
        ann.coordinate = CLLocationCoordinate2DMake([[eventLatArray objectAtIndex:i]doubleValue],[[eventLongArray objectAtIndex:i]doubleValue]);
        [bandMapView addAnnotation:ann];
        [annArray addObject:ann];
        
        @try
        {
            [ann setTitle:[eventNameArray objectAtIndex:i]];
        }
        @catch (NSException *exception)
        {
            [ann setTitle:@" "];
            return;
        }
        @try
        {
            [ann setSubtitle:[eventAddArray objectAtIndex:i]];
        }
        @catch (NSException *exception)
        {
            [ann setSubtitle:@" "];
            return;
        }
        
        ReleaseAndNilify(ann);
        
    }
    //[self zoomMapViewToFitAnnotations:bandMapView animated:YES];
    
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in bandMapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1,0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    
    
    UIEdgeInsets insets = UIEdgeInsetsMake(100, -70, 80, 0);

    
    MKMapRect biggerRect = [bandMapView mapRectThatFits:zoomRect edgePadding:insets];
    
    
    [bandMapView setVisibleMapRect:biggerRect animated:YES];
   
    
    
    
}
// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    // Calculate Y position for View
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-bottomBarButtonHeight - 44 - 20, SCREEN_WIDTH, bottomBarButtonHeight)];//-yVal
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [arrBandEventsBottomButtonDO count]; btnLoop++)
    {
        bottomButtonDO *obj_findBottomDO = [arrBandEventsBottomButtonDO objectAtIndex:btnLoop];
        
       view = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_findBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, (SCREEN_HEIGHT-bottomBarButtonHeight - 44 -20) , 80,bottomBarButtonHeight) imageName:obj_findBottomDO.bottomBtnImg img_off:obj_findBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];//-yVal
            
        }
        else
            view = [bottomButtonView initWithTitle:obj_findBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrBandEventsBottomButtonDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrBandEventsBottomButtonDO count],bottomBarButtonHeight) imageName:obj_findBottomDO.bottomBtnImg img_off:obj_findBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_findBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
    }
    
}

////size the mapView region to fit its annotations
//- (void)zoomMapViewToFitAnnotations:(MKMapView *)mapView animated:(BOOL)animated
//{
//    NSArray *annotations = mapView.annotations;
//    NSUInteger count = [mapView.annotations count];
//    if ( count == 0) { return; } //bail if no annotations
//
//    //convert NSArray of id <MKAnnotation> into an MKCoordinateRegion that can be used to set the map size
//    //can't use NSArray with MKMapPoint because MKMapPoint is not an id
//    MKMapPoint points[count]; //C array of MKMapPoint struct
//    for( int i=0; i<count; i++ ) //load points C array by converting coordinates to points
//    {
//        CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)[annotations objectAtIndex:i] coordinate];
//        points[i] = MKMapPointForCoordinate(coordinate);
//    }
//    //create MKMapRect from array of MKMapPoint
//    MKMapRect mapRect = [[MKPolygon polygonWithPoints:points count:count] boundingMapRect];
//    //convert MKCoordinateRegion from MKMapRect
//    MKCoordinateRegion region = MKCoordinateRegionForMapRect(mapRect);
//
//    //add padding so pins aren't scrunched on the edges
//    region.span.latitudeDelta  *= ANNOTATION_REGION_PAD_FACTOR;
//    region.span.longitudeDelta *= ANNOTATION_REGION_PAD_FACTOR;
//    //but padding can't be bigger than the world
//    if( region.span.latitudeDelta > MAX_DEGREES_ARC ) { region.span.latitudeDelta  = MAX_DEGREES_ARC; }
//    if( region.span.longitudeDelta > MAX_DEGREES_ARC ){ region.span.longitudeDelta = MAX_DEGREES_ARC; }
//
//    //and don't zoom in stupid-close on small samples
//    if( region.span.latitudeDelta  < MINIMUM_ZOOM_ARC ) { region.span.latitudeDelta  = MINIMUM_ZOOM_ARC; }
//    if( region.span.longitudeDelta < MINIMUM_ZOOM_ARC ) { region.span.longitudeDelta = MINIMUM_ZOOM_ARC; }
//    //and if there is a sample of 1 we want the max zoom-in instead of max zoom-out
//    if( count == 1 )
//    {
//        region.span.latitudeDelta = MINIMUM_ZOOM_ARC;
//        region.span.longitudeDelta = MINIMUM_ZOOM_ARC;
//    }
//    [mapView setRegion:region animated:animated];
//}
-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    MKPinAnnotationView* pinView = nil;
    
    if ([annotation isKindOfClass:[RetailerAnnotation class]])
    {
        // try to dequeue an existing pin view first
        static NSString* RetailerAnnotationIdentifier = @"retailerAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[bandMapView dequeueReusableAnnotationViewWithIdentifier:RetailerAnnotationIdentifier];
        
        if (!pinView ||![defaults valueForKey:@"isComingFromGroupingandSorting"])
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
                                                  initWithAnnotation:annotation reuseIdentifier:RetailerAnnotationIdentifier] ;
            
            customPinView.animatesDrop = YES;
            
            
            for(int i =0; i < [bandEventsListArray count]; i++)
            {
                EventListDetails *eventDetail = [bandEventsListArray objectAtIndex:i];
                if ([[eventDetail eventName] isEqualToString:annotation.title])
                {
                    
                    if([[eventDetail isAppSiteFlag]intValue] == 1)
                    {
                        
                        UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
                        customPinView.rightCalloutAccessoryView = infoButton;
                    }
                    else
                    {
                        customPinView.rightCalloutAccessoryView = NO;
                    }
                }
            }
            customPinView.canShowCallout = YES;
            
            
            
            return customPinView;
            
            
        }
        else
        {
            pinView.annotation = annotation;
        }
    }
    pinView.canShowCallout = YES;
    
    [pinView bringSubviewToFront:bandMapView];
    return pinView;
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    //    if(mapView.zoomLevel > 12 && DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    //        [mapView setZoomLevel:12];
    //    if(mapView.zoomLevel > 40 && DEVICE_TYPE==UIUserInterfaceIdiomPad)
    //        [mapView setZoomLevel:40];
    
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    
    for (int j = 0 ; j < [bandEventsListArray count]; j++) {
        if ([[[bandEventsListArray objectAtIndex:j]eventName] isEqualToString:view.annotation.title]) {
            NSIndexPath *ip = [NSIndexPath indexPathForRow:j inSection:0];
            [linkID insertObject:@"0" atIndex:[linkID count]];
            [self request_retsummary:ip];
            return;
        }
    }
}
//fetch the retailer summary of a ScanSee retailer
-(void)request_retsummary:(id)indexPath1
{
    NSIndexPath *ipath = indexPath1;
    iWebRequestState = RETSUMMARY;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    
    EventListDetails *eventDetail = [bandEventsListArray objectAtIndex:ipath.row];
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",[eventDetail retailId]];
    
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[eventDetail retailLocationId]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [requestStr appendFormat:@"<scanTypeId>%@</scanTypeId>",@"0"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    //[requestStr release];
}

//parse retailer summary
-(void)parse_RetSummary:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        
        [defaults setObject:response forKey:KEY_RESPONSEXML] ;
        rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}
-(void)fetchNextBandEvents
{
    if ([nextPage intValue] == 1)
    {
        [self request_EventDetails:sortFilObj];
    }
    else
    {
        return;
    }
}

//{
//    "userId":1,
//    "hubCitiId":82,
//    "catIds":2,
//
//    "lowerLimit":0,
//    "evtTypeID":2,
//    "latitude":"120.25",                 //may be null
//    "longitude":"-99.82",                //may be null
//    "postalCode":"75701",             //may be null
//    "radius":12                                 //may be null
//}
//userId":1,
//"hubCitiId":82,
//"catIds":2,
//"lowerLimit":0,
//"latitude":"120.25",                 //may be null
//"longitude":"-99.82",                //may be null
//"postalCode":"75701",             //may be null
//"radius":12                                 //may be null
//“sortOrder”:”asc”,
//“sortColumn”:”date”
//"bottomBtnId":14065


-(void)request_EventDetails:(SortAndFilter *)sortFilOb
{
    [HubCitiAppDelegate showActivityIndicator];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    //    userId":1,
    //    "hubCitiId":10263,
    //    "lowerLimit":0,
    //    "sortColumn":"distance",
    //    "radius":50,
    //    {"userId":"10533","hubCitiId":"10263","sortOrder":"ASC","latitude":12.9205987,"longitude":77.572239,"lowerLimit":0,"radius":50}
    
    iWebRequestState = BANDS_EVENTS_INFO;
    //    [parameters setValue:@"10533" forKey:@"userId"];
    //    [parameters setValue:@"10263" forKey:@"hubCitiId"];
    //    [parameters setValue:rowCountOfTotalCells forKey:@"lowerLimit"];
    //   // [parameters setValue:@"date" forKey:@"sortColumn"];
    //    [parameters setValue:@"50"forKey:@"radius"];
    //    [parameters setValue:@"ASC" forKey:@"sortOrder"];
    //    [parameters setValue: @"77.572239"forKey:@"longitude"];
    //    [parameters setValue:@"12.9205987" forKey:@"latitude"];
    //    if (sortFilOb.eventDateSelected==TRUE && [sortFilOb.evtDate length]) {
    //        [parameters setValue:sortFilOb.evtDate forKey:@"evtDate"];
    //    }
    //    if (sortFilOb.selectedCatIds) {
    //        [parameters setValue:sortFilOb.selectedCatIds forKey:@"catIds"];
    //    }
    //    if (sortFilOb.distanceSelected==TRUE) {
    //        [parameters setValue:@"distance" forKey:@"sortColumn"];
    //    }
    //    if(sortFilOb.bandSelected == TRUE)
    //    {
    //        [parameters setValue:@"band" forKey:@"sortColumn"];
    //    }
    //    if(sortFilOb.venueSelected == TRUE)
    //    {
    //        [parameters setValue:@"venue" forKey:@"sortColumn"];
    //    }
    //    if(sortFilOb.sortEventDateSelected == TRUE)
    //    {
    //        [parameters setValue:@"date" forKey:@"sortColumn"];
    //    }
    //
    //        //    //[parameters setValue:@"0" forKey:@"lowerLimit"];
    //        //    [parameters setValue: @"-99.82"forKey:@"longitude"];
    //        //    [parameters setValue:@"120.25" forKey:@"latitude"];
    //        //    [parameters setValue:@"75701" forKey:@"postalCode"];
    //        //    [parameters setValue:@"12"forKey:@"radius"];
    //        //    //[parameters setValue:@"2" forKey:@"evtTypeID"];
    //        //    [parameters setValue:@"12" forKey:@"bandID"];
    //        //    [parameters setValue:rowCountOfTotalCells forKey:@"lowerLimit"];
    //
    //
    if([defaults valueForKey:KEY_USERID])
    {
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
        
    }
    if([defaults valueForKey:KEY_HUBCITIID])
    {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
        
    }
    if([defaults valueForKey:KEY_MAINMENUID])
    {
        [parameters setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    }
    
    [parameters setValue:rowCountOfTotalCells forKey:@"lowerLimit"];
    
    if(self.bandID)
    {
        [parameters setValue:bandID forKey:@"bandId"];
    }
    if([defaults valueForKey:KEY_MITEMID])
    {
        [defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"playingTodayMitemId"];
        [parameters setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
    }
    
    if(fromTodayEvents)
    {
        NSDate *todayDate = [NSDate date]; // get today date
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
        [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //Here we can set the format which we need
        NSString *convertedDateString = [dateFormatter stringFromDate:todayDate];// here convert date in
        [parameters setValue:convertedDateString forKey:@"evtDate"];
    }
    

    if ([[defaults  valueForKey:KEY_LATITUDE]length] && ([[HubCitiManager sharedManager]gps_allow_flag] == YES) &&(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)))
    {
        [parameters setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        
    }
    if ( [[defaults valueForKey:KEY_LONGITUDE]length] && ([[HubCitiManager sharedManager]gps_allow_flag] == YES) &&(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)))
    {
        [parameters setValue:[defaults  valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        
    }
    
    if ([[defaults valueForKey:KEYZIPCODE] length])
    {
        [parameters setValue:[defaults valueForKey:KEYZIPCODE]forKey:@"postalCode"];
        
    }
    // Added by Ravindra for shows sort/filter on done pressed
    if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
        if ([RegionApp isEqualToString:@"1"] && sortFilOb.selectedCitiIds)
        {
            [parameters setValue:sortFilOb.selectedCitiIds forKey:@"cityIds"];
        }
//        else
//        {
//            if([RegionApp isEqualToString:@"1"] && [defaults valueForKey:@"sortedCitiIDs"])
//            {
//                [parameters setValue:[defaults valueForKey:@"sortedCitiIDs"] forKey:@"cityIds"];
//            
//            }
//        }
    }

    if([defaults valueForKey:@"FindRadius"])
    {
        [parameters setValue:[defaults valueForKey:@"FindRadius"] forKey:@"radius"];
        
    }
    else
    {
        [parameters setValue:@"50" forKey:@"radius"];
        
    }
    
    if (sortFilOb.eventDateSelected==TRUE && [sortFilOb.evtDate length]) {
        [parameters setValue:sortFilOb.evtDate forKey:@"evtDate"];
    }
    if (sortFilOb.selectedCatIds) {
        [parameters setValue:sortFilOb.selectedCatIds forKey:@"catIds"];
    }
    if (sortFilOb.distanceSelected==TRUE) {
        [parameters setValue:@"mileage" forKey:@"sortBy"];
    }
    if(sortFilOb.bandSelected == TRUE)
    {
        [parameters setValue:@"band" forKey:@"sortBy"];
    }
    if(sortFilOb.venueSelected == TRUE)
    {
        [parameters setValue:@"venue" forKey:@"sortBy"];
    }
    if(sortFilOb.sortEventDateSelected == TRUE)
    {
        [parameters setValue:@"date" forKey:@"sortBy"];
    }
    
    
    
    DLog(@"Parameter: %@",parameters);
    //NSString *urlString = [NSString stringWithFormat:@"http://sdw2730:8080/HubCiti2.6/band/eventlist"];
    NSString *urlString = [NSString stringWithFormat:@"%@band/eventlist",BASE_URL];
    // NSString *urlString = [NSString stringWithFormat:@"http://sdw2107:9990/HubCiti2.5/band/eventlist"];
    
    if ([defaults boolForKey:@"ViewMoreEvent"])
    {
        DLog(@"Url: %@",urlString);
        
        NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
        [HubCitiAppDelegate removeActivityIndicator];
        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
        id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        DLog(@"%@",responseData);
        [self parse_EventData:responseData];
    }
    else
    {
        HTTPClient *client = [HTTPClient sharedHTTPClient];
        client.delegate = self;
        DLog(@"Url: %@",urlString);
        [client sendRequest : parameters : urlString];
    }
    
    
}
-(NSString *)dateConverter:(NSString *)eventDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    
    NSDate *date = [dateFormatter dateFromString: eventDate];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    
    NSString *convertedString = [dateFormatter stringFromDate:date];
    NSLog(@"Converted String : %@",convertedString);
    
    return convertedString;
    
}
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case BANDS_EVENTS_INFO:
            [self parse_EventData:response];
            
            // [bandEventsTable reloadData];
            
            break;
            
        case RETSUMMARY:
            [self parse_RetSummary:response];
            
            break;
            
            //        case PARTNERRETS:
            //            [self parse_PartnerRet:response];
            //            break;
            //            //        case GETPARTNER:
            //            //            [self parse_GetPartners:response];
            //            //            break;
            
        case PARTNERRET:
            [self parse_PartnerRet:response];
            break;
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
            
            
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
            
            
        default:
            break;
    }
}
-(void)parse_EventData:(id)responseString
{
    if(responseString == NULL)
        return;
    [defaults setObject:responseString forKey:@"EventDetails"];
    
    if(bandEventsResponse == NULL)
    {
        bandEventsResponse = [[BandEventsList alloc]init];
    }
    
    @try{
        [bandEventsResponse setValuesForKeysWithDictionary:responseString];
    }
    
    
    @catch (NSException *exception) {
        // Do nothing
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
        return;
    }
    
    NSLog(@"BandEventsListResponse: %@",bandEventsResponse);
    
    if([bandEventsResponse.responseCode isEqualToString:@"10000"])
    {
        if(bandEventsResponse.nextPage)
        {
            nextPage = bandEventsResponse.nextPage;
        }
        if(bandEventsResponse.maxRowNum)
        {
            rowCountOfTotalCells = bandEventsResponse.maxRowNum;
        }
        
        if(bandEventsResponse.eventGroupList){
            for(int i = 0;i < bandEventsResponse.eventGroupList.count; i++)
            {
                
                BOOL isCatRepeated = NO;
                
                
                EventGroupListDetails *eventGroupList = [[EventGroupListDetails alloc] init];
                eventGroupList.eventGrpArray = [[NSMutableArray alloc] init];
                
                NSDictionary *dictList = bandEventsResponse.eventGroupList[i];
                
                
                @try{
                    [eventGroupList setValuesForKeysWithDictionary:dictList];
                }
                @catch (NSException *exception) {
                    // Do nothing
                    [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
                    return;
                }
                
                if(eventGrouingInfo.count >0){
                    
                    EventGroupListDetails *eventGroupRepeatedList = [[EventGroupListDetails alloc] init];
                    
                    for(int i=0;i<[eventGrouingInfo count]; i++)
                    {
                        //                        if ([defaults boolForKey:@"ViewMoreEvent"])
                        //                        {
                        //                             eventGroupRepeatedList = [eventGrouingInfo objectAtIndex:(i+eventGrouingInfo.count - 1)];
                        //                        }
                        //                        else
                        //                        {
                        //                            eventGroupRepeatedList = [eventGrouingInfo objectAtIndex:i];
                        //                        }
                        eventGroupRepeatedList = [eventGrouingInfo objectAtIndex:i];
                        isCatRepeated = NO;
                        
                        if([eventGroupRepeatedList.groupContent isEqualToString:eventGroupList.groupContent]){
                            
                            isCatRepeated = YES;
                            
                            [eventGroupList.eventGrpArray  addObjectsFromArray:eventGroupRepeatedList.eventGrpArray];
                            [[eventGroupList.eventList mutableCopy] addObjectsFromArray:eventGroupRepeatedList.eventList];
                            [eventGrouingInfo  replaceObjectAtIndex:i withObject:eventGroupList];
                            break;
                            
                        }
                    }
                    if(isCatRepeated == NO){
                        
                        [eventGrouingInfo addObject:eventGroupList];
                    }
                }
                else{
                    [eventGrouingInfo addObject:eventGroupList];
                }
                
                if(eventGroupList.eventList){
                    
                    for(int i = 0;i < eventGroupList.eventList.count; i++)
                    {
                        EventListDetails * eventDetails = [[EventListDetails alloc] init];
                        
                        
                        NSDictionary* dictList = eventGroupList.eventList[i];
                        
                        
                        [eventDetails setValuesForKeysWithDictionary:dictList];
                        
                        [bandEventsListArray addObject:eventDetails];
                        
                        [eventGroupList.eventGrpArray addObject:eventDetails];
                        
                        if(eventDetails.latitude)
                        {
                            [eventLatArray addObject:eventDetails.latitude];
                        }
                        if(eventDetails.longitude)
                        {
                            [eventLongArray addObject:eventDetails.longitude];
                        }
                        if(eventDetails.address)
                        {
                            [eventAddArray addObject:eventDetails.address];
                        }
                        if(eventDetails.eventName)
                        {
                            [eventNameArray addObject:eventDetails.eventName];
                        }
                        
                    }
                }
            }
            
        }
        
        
        
        
        
        if([bandEventsResponse.bottomBtn integerValue] == 1)
        {
            bottomBtn = [bandEventsResponse.bottomBtn intValue];
            arrBandEventsBottomButtonDO=[[NSMutableArray alloc]init];
            for (int i=0; i < bandEventsResponse.bottomBtnList.count; i++) {
                bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                NSDictionary* dictList = bandEventsResponse.bottomBtnList[i];
                
                [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                
                if(obj_findBottomDO.bottomBtnName.length == 0)
                {
                    obj_findBottomDO.bottomBtnName = @" ";
                }
                if(obj_findBottomDO.bottomBtnImg.length == 0)
                {
                    obj_findBottomDO.bottomBtnImg = @" ";
                }
                if(obj_findBottomDO.bottomBtnImgOff.length == 0)
                {
                    obj_findBottomDO.bottomBtnImgOff = @" ";
                }
                if(obj_findBottomDO.btnLinkTypeName)
                {
                    if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"])
                    {//for filters
                        [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                    }
                }
                [arrBandEventsBottomButtonDO addObject:obj_findBottomDO];
                
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
            if(![defaults boolForKey:@"ViewMoreEvent"]){
                if([arrBandEventsBottomButtonDO count] > 0)
                {
                    // Set Bottom Menu Button after read from response data
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self setBottomBarMenu];
                    });
                    
                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                }
                else
                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
                
            }
            });
        }
        //         [self settableViewOnScreen];
        //               [self optionsShowMap];
        //        dispatch_async(dispatch_get_main_queue(), ^{
        //            [bandEventsTable reloadData];
        //        });
        dispatch_async(dispatch_get_main_queue(), ^{
            [bandEventsTable reloadData];
            [self settableViewOnScreen];
            [bandEventsTable setContentOffset:CGPointZero animated:YES];
        });
        //[self setfilter];
    }
    else if([bandEventsResponse.responseCode isEqualToString:@"10002"])
    {
        nextPage=0;
        
        NSString *responseTextStr;
        if (bandEventsResponse.responseText != nil) {
            responseTextStr = bandEventsResponse.responseText;
        }
        else
            responseTextStr = @"No Records Found";
        dispatch_async(dispatch_get_main_queue(), ^{
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        dispatch_async(dispatch_get_main_queue(), ^{
            [bandEventsTable reloadData];
            [self settableViewOnScreen];
            [bandEventsTable setContentOffset:CGPointZero animated:YES];
        });
        });
        
        // [self settableViewOnScreen];
        
    }
    else if ([bandEventsResponse.responseCode isEqualToString:@"10005"])
    {
        {
            bottomBtn = [bandEventsResponse.bottomBtn intValue];
            arrBandEventsBottomButtonDO=[[NSMutableArray alloc]init];
            for (int i=0; i < bandEventsResponse.bottomBtnList.count; i++) {
                bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                NSDictionary* dictList = bandEventsResponse.bottomBtnList[i];
                
                [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                
                if(obj_findBottomDO.bottomBtnName.length == 0)
                {
                    obj_findBottomDO.bottomBtnName = @" ";
                }
                if(obj_findBottomDO.bottomBtnImg.length == 0)
                {
                    obj_findBottomDO.bottomBtnImg = @" ";
                }
                if(obj_findBottomDO.bottomBtnImgOff.length == 0)
                {
                    obj_findBottomDO.bottomBtnImgOff = @" ";
                }
                if(obj_findBottomDO.btnLinkTypeName)
                {
                    if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"])
                    {//for filters
                        [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                    }
                }
                [arrBandEventsBottomButtonDO addObject:obj_findBottomDO];
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if(![defaults boolForKey:@"ViewMoreEvent"]){
                    if([arrBandEventsBottomButtonDO count] > 0)
                    {
                        // Set Bottom Menu Button after read from response data
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self setBottomBarMenu];
                        });
                        
                        [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                    }
                    else
                        [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
                    
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [bandEventsTable reloadData];
                    [self settableViewOnScreen];
                    [bandEventsTable setContentOffset:CGPointZero animated:YES];
                    
                    
                });
                
                
            });
            nextPage=0;
            
            NSString *responseTextStr;
            if (bandEventsResponse.responseText != nil) {
                responseTextStr = bandEventsResponse.responseText;
            }
            else
                responseTextStr = @"No Records Found";
            
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
            //[bandEventsTable reloadData];
            
            
        }
    }
    else {//show the error message in alert
        nextPage=0;
        
        
        if (bandEventsResponse.responseText != nil) {
            
            
            UIAlertController * alert;
            
            alert=[UIAlertController alertControllerWithTitle:bandEventsResponse.responseText message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     if(!fromFilter){
                                             [self.navigationController popViewControllerAnimated:NO];
                                     }
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
           
            [loading stopAnimating];
           
            
        }
        else
        {
            UIAlertController * alert;
            
            alert=[UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"No Records found"] message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     if(!fromFilter){
                                         [self.navigationController popViewControllerAnimated:NO];
                                     }
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
             [loading stopAnimating];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [bandEventsTable reloadData];
            [self settableViewOnScreen];
            [bandEventsTable setContentOffset:CGPointZero animated:YES];

            
            [self settableViewOnScreen];
              [bandEventsTable setContentOffset:CGPointZero animated:YES];
        });
        
      
        if(fromFilter){
            //[self setfilter];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self optionsShowMap];
    });

    
    
    //    UIImageView *homeLabel = [self createHomeButtonView];
    //
    //
    //
    //    DWBubbleMenuButton *upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
    //                                                                                          self.view.frame.size.height - homeLabel.frame.size.height - 35.f,
    //                                                                                          homeLabel.frame.size.width,
    //                                                                                          homeLabel.frame.size.height)
    //                                                            expansionDirection:DirectionUp];
    //    upMenuView.homeButtonView = homeLabel;
    //
    //    upMenuView.delegate = self;
    
    
    [defaults setBool:NO forKey:@"ViewMoreEvent"];
    
}
//parse user favorite categories
//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsviewc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsviewc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsviewc animated:NO];
        //[rsvc release];
    }
}


-(void)presentSpecialOffersAfterDismissAleert {
    
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}



-(void) setfilter{
    UIButton *upMenuView = [[UIButton alloc] init];
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        if(bottomBtn == 1)
        {
            upMenuView.frame = CGRectMake(self.view.frame.size.width - VARIABLE_WIDTH(50) - 10.f,self.view.frame.size.height - VARIABLE_HEIGHT(50) - 35.f - VARIABLE_HEIGHT(50) ,50,50);
        }
        else
        {
            upMenuView.frame = CGRectMake(self.view.frame.size.width - VARIABLE_WIDTH(50) - 10.f,self.view.frame.size.height - VARIABLE_HEIGHT(50) - 35.f ,50,50);
        }
        
    }
    else
    {
        if(bottomBtn == 1)
        {
            upMenuView.frame = CGRectMake(SCREEN_WIDTH-VARIABLE_WIDTH(15)-VARIABLE_WIDTH(30),self.view.frame.size.height - VARIABLE_HEIGHT(50) - 35.f -  50,75,75);
        }
        else
        {
            upMenuView.frame = CGRectMake(SCREEN_WIDTH-VARIABLE_WIDTH(15)-VARIABLE_WIDTH(30),self.view.frame.size.height - VARIABLE_HEIGHT(50) - 35.f,75,75);
        }
    }
    
    upMenuView.layer.cornerRadius = upMenuView.frame.size.height  / 2;
    //upMenuView.backgroundColor =[UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    upMenuView.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    [upMenuView setImage:[UIImage imageNamed:@"sort_filter2.png"] forState:UIControlStateNormal];
    //upMenuView.imageView.image = [UIImage imageNamed:@"share-icon"];
    upMenuView.clipsToBounds = YES; [self.view addSubview:upMenuView];
    [upMenuView addTarget:self action:@selector(request_SortFilter) forControlEvents:UIControlEventTouchUpInside];
    
    
    //[self.view addSubview:upMenuView];
}



-(void)request_SortFilter
{
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
    
    
    SwipesViewController *iSwipeViewController1 = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
    iSwipeViewController1.module = @"BandEvents";//Find All , Find Single , Events, CitiEXP, BandEvents
    //  iSwipeViewController1.delegate = self;
    
    
    iSwipeViewController1.sortFilObj = [[SortAndFilter alloc]initWithObject:sortFilObj];
    
    if(eventTypeID)
        iSwipeViewController1.evtTypeID = eventTypeID;
    else if(bandID)
        iSwipeViewController1.bandId = bandID;
    
    [self.navigationController pushViewController:iSwipeViewController1 animated:NO];
    
}
-(void)bottomButtonPressed:(id)sender
{
    if([defaults boolForKey:@"ViewMore"])
        return;
    [defaults setBool:NO forKey:@"ViewMoreBand"];
    [defaults setBool:YES forKey:BottomButton];
    //   [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    //NSlog(@"The tag is %d",tag);
    bottomButtonDO *obj_eventBottomDO = [arrBandEventsBottomButtonDO objectAtIndex:tag];
    
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=27||[[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=28){
        [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    }
    [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_eventBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_eventBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_eventBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_eventBottomDO.btnLinkTypeName);
    if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    
    else{ //other menu types
        switch ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                //[hotDeals release];
                break;
                
            case 1://Scan Now
            {
                
            }
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
                }
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                
                [self navigateToFindView];
            }
                
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
            {
                
            }
                
                break;
                
            case 8://Visitors Bureau
            {
                
            }
                
                break;
                
            case 9://transportation
            {
                
            }
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                //HTML_URL (PROD_ENVIRON)? @"https://www.scansee.net/Images/hubciti/html/"
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                     [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {
                    [self shareClicked:nil];
                }
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                     [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=FALSE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_eventBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    // CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];;
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                //   [dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }
                break;
            case 22:
            {
                if (userIdentifier==TRUE) {
                     [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }
                break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!")  msg:NSLocalizedString(@"Network is not available",@"Network is not available") ];
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        //[inform release];
                    }
                    
                }
                
            }break;
            case 25:
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                         [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            //  [citPref release];
                        }
                    }
                }
                
            }break;
            case 27:
            case 28://sort/filter
            {
                {
                    if([defaults valueForKey:@"playingTodayMitemId"])
                    {
                        [defaults setValue:[defaults valueForKey:@"playingTodayMitemId"] forKey:KEY_MITEMID];
                    }
                    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
                    
                    // [defaults setValue:[defaults valueForKey:@"eventsMitemId"] forKey:KEY_MITEMID];
                    
                    SwipesViewController *iSwipeViewController1 = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                    iSwipeViewController1.module = @"BandEvents";//Find All , Find Single , Events, CitiEXP
                    
                    iSwipeViewController1.sortFilObj = [[SortAndFilter alloc]initWithObject:sortFilObj];
                    if(bandID)
                    {
                        iSwipeViewController1.bandId = bandID;
                    }
                    
                    [self.navigationController pushViewController:iSwipeViewController1 animated:NO];
                    
                }
            }
                break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;

                
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
               
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
            default:
                break;
        }
    }
    
}
-(void)shareClicked:(id)sender{
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        return;
    
    [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    [self showActionSheet];
    
    
}
-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

-(void) faceBookCliked {
    _anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:_anyVC.view]; }
                    completion:nil];
    
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
    }
}


-(void) textClicked{
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
}

-(void) emailClick{
    
    
    __typeof(self) __weak  obj = self;
    _emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    _emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [_emailSendingVC loadMail];
}

-(void) showActionSheet {
   
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}



-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



//Call Find Service and Class
//#pragma mark REQUEST METHODS

-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
}
-(void)parseGetUserData:(id)response
{
    if (response == nil)
        return;
    if (_infoResponse == nil) {
        _infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [_infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", _infoResponse);
    if ([_infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = _infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}

-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=FALSE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}
//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    else if ([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}
-(void)parse_PartnerRet:(NSString*)response
{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}
-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}
-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}
-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}
-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}
-(void)navigateToFindView
{
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    navigatedFromMainMenu=false;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}

-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    self.iEventsListViewController = iEventsListViewController;
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}


-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}


- (SortAndFilter *)loadCustomObjectWithKey:(NSString *)key {
    
    NSData *encodedObject = [defaults objectForKey:key];
    SortAndFilter *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    NSLog(@"SortAndFilter object in loadCustomObj: %@",object);
    return object;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end

//
//  EventDetailImageViewController.m
//  HubCiti
//
//  Created by service on 4/14/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "EventDetailImageViewController.h"
#import "HubCitiConstants.h"
#import "EventDetailMapViewController.h"
#import "SdImageView.h"
#import "WebBrowserViewController.h"
#import "MainMenuViewController.h"
#import "BandRetailerSummaryViewController.h"
#import "BandCatList.h"
#import "RetailerSummaryViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "AppDelegate.h"
#import "EmailShareViewController.h"
#import "MultipleBandViewController.h"

@interface EventDetailImageViewController () < UIScrollViewDelegate,DWBubbleMenuViewDelegate,CustomizedNavControllerDelegate>
{
    float fontSize;
    UIScrollView* mscrollView;
    float imageHeight;
    float height;
    CustomizedNavController *cusNav;
}

@end



@implementation EventDetailImageViewController
@synthesize eventDo,retailerSummaryPage,anyVC,emailSendingVC,noOfBandExistFlag,popUpMsgForNoBands;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;

    
    
    if (IPAD) {
        
        fontSize = 20;
        
    }
    else{
        
        fontSize = 16;
    }
 
    imageHeight = ((float)432 / (float) 768) * SCREEN_WIDTH;
    
    // Do any additional setup after loading the view from its nib.
    

    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    //label to display screen title
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:eventDo.eventName forView:self withHambergur:NO];
 
    
    //customize back button
  
    self.navigationItem.hidesBackButton = YES;
    mscrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    mscrollView.delegate = self;
    mscrollView.scrollEnabled = YES;
    [self.view addSubview:mscrollView];
    
    [self setupUI];
    
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
}
//Share button at the bottom of the event summary
-(void) shareBandEvent
{
    UIImageView * floatingShareButton;
    floatingShareButton = [self createFloatingShareButtonView];
    DWBubbleMenuButton * bubbleBandShare = [[DWBubbleMenuButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - floatingShareButton.frame.size.width - 10.f, SCREEN_HEIGHT - floatingShareButton.frame.size.height - 85.f, floatingShareButton.frame.size.width, floatingShareButton.frame.size.height) expansionDirection:DirectionUp];
    bubbleBandShare.homeButtonView = floatingShareButton;
    bubbleBandShare.delegate = self;
    [bubbleBandShare addButtons:[self createShareButtonArray]];
    [self.view addSubview:bubbleBandShare];
    [self.view bringSubviewToFront:bubbleBandShare];
    
    bubbleBandShare.onLogOutPressed =^(){
        
        [self logOutPressed];
    } ;
    
}
-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

-(UIImageView*) createFloatingShareButtonView
{
    UIImageView * label = nil;
    if(DEVICE_TYPE == UIUserInterfaceIdiomPhone)
    {
        label = [[UIImageView alloc]initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else
    {
        label = [[UIImageView alloc]initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    label.layer.cornerRadius = label.frame.size.height /2.f;
    label.backgroundColor = [UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    return label;
    
}

-(NSArray *) createShareButtonArray
{
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    
    int i = 0;
    for (NSString *title in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:title] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        
        button.clipsToBounds = YES;
        button.tag = i++;
        
        [button addTarget:self action:@selector(testband:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
    
}

- (void)testband:(UIButton *)sender
{
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
    [self shareClicked];
    
    switch (sender.tag) {
        case 0:{
            //facebook
            anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
            anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"BAND_EVENT_SHARE"];
            [UIView transitionWithView:self.view duration:0.5
                               options:UIViewAnimationOptionTransitionCurlDown
                            animations:^ { [self.view addSubview:anyVC.view]; }
                            completion:nil];
        }
            break;
        case 1:{
            //twitter
            
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    if (result == SLComposeViewControllerResultCancelled) {
                        DLog(@"Twitter Result: canceled");
                    } else {
                        DLog(@"Twitter Result: sent");
                        NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><bandEventId>%@</bandEventId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:@"BandEventID"]];
                        
                        [NSThread detachNewThreadSelector:@selector(userTrack:) toTarget:self withObject:userXmlStr];
                    }
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        
                        [controller.view removeFromSuperview];
                    } else
                        [controller dismissViewControllerAnimated:YES completion:Nil];
                };
                controller.completionHandler =myBlock;
                
                //Adding the Text to the facebook post value from iOS
                [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                
                [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[defaults valueForKey:KEY_PRODUCTIMGPATH] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]]]]];
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    [self.view addSubview:controller.view];
                    [self.view bringSubviewToFront:controller.view];
                }else
                    [self presentViewController:controller animated:YES completion:Nil];
                
                
            }
            else {
                [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
                
             
                
            }
        }
            
            
            break;
        case 2:{
            
            //text
            
            if ([self hasCellularCoverage])
            {
                if ([MFMessageComposeViewController canSendText]) {
                    
                    MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
                    [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    smsComposerController.messageComposeDelegate = self;
                    [self presentViewController:smsComposerController animated:YES completion:nil];
                }
            }
            else
            {
                [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
                
            }
            
            
        }
            break;
        case 3:{
            
            //email
            
            emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
            emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"BAND_EVENT_SHARE"];
            __typeof(self) __weak  obj = self;
            emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                
                [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
            } ;
            
            [emailSendingVC loadMail];
        }
            break;
        default:
            [defaults setBool:NO forKey:BottomButton];
            break;
    }
    
}


-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode)
    {
        return NO;
    }
    return YES;
}



-(void)shareClicked
{
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    [reqStr appendFormat:@"<ShareProductInfo><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
   [reqStr appendFormat:@"<eventId>%@</eventId><platform>IOS</platform>",[defaults valueForKey:@"BandEventID"]];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_HUBCITIID]];
    

    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharebandevent",BASE_URL];
    DLog(@"%@",urlString);
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    
    [self parseProductShare:response];
  
}

-(void)parseProductShare:(NSString*)response
{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000)
    {
        TBXMLElement *shareText=[TBXML childElementNamed:@"eventName" parentElement:tbXml.rootXMLElement];
        TBXMLElement *emailURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *shreMsg = [TBXML childElementNamed:@"shareText" parentElement:tbXml.rootXMLElement];
        TBXMLElement *imagePath = [TBXML childElementNamed:@"imagePath" parentElement:tbXml.rootXMLElement];
        
        if (emailURL!=nil) {
            [defaults setValue:[TBXML textForElement:emailURL] forKey:KEY_HYPERLINK];
            NSLog(@"The link to be shared is %@",KEY_HYPERLINK);
        }
        
        if (shareText!=nil) {
            NSMutableString *msgString = [[NSMutableString alloc]init];
            [msgString appendFormat:@"%@",[TBXML textForElement:shreMsg]];
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [msgString appendFormat:@"%@",shareString];
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            NSLog(@"The share message value is %@",[defaults valueForKey:KEY_SHAREMSG]);
        }
        
        if (imagePath!=nil) {
            [defaults setObject:[TBXML textForElement:imagePath] forKey:KEY_PRODUCTIMGPATH];
            NSLog(@"The product image path is %@",KEY_PRODUCTIMGPATH);
        }
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
           
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
           
            return;
        }
        
    }
}
- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView{
    
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}


- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
            
            NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><bandEventId>%@</bandEventId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:@"BandEventID"]];
            
            [NSThread detachNewThreadSelector:@selector(userTrack:) toTarget:self withObject:userXmlStr];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
           
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)userTrack:(NSString*)xmlStr
{
    
    NSMutableString *urlStr = [[NSMutableString alloc]init];
    [urlStr appendFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    DLog(@"%@",urlStr);
    NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:xmlStr]];
    DLog(@"%@",response);
    //[xmlStr release];
}

//Mainmenu return
-(void)returnToMainPage:(id)sender{
    
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }

}
-(void) webViewDidFinishLoad:(UIWebView *)webView{
    
   // float contentHeight = webView.scrollView.contentSize.height;
}

- (NSString *)stringByStrippingHTML : (NSString*) htmlStr
{
    
    NSRange r;
    NSString *s = htmlStr ;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound) s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
    
}
-(void) setupUI
{
    
    float yPos = 0.0f;
    
    // ImageView
    SdImageView *asyncImageView ;
    if(eventDo.imgPath){
        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, imageHeight)];
        asyncImageView.backgroundColor = [UIColor clearColor];
        [asyncImageView loadImage:eventDo.imgPath];
        
        [mscrollView addSubview:asyncImageView];
        yPos = yPos+asyncImageView.frame.size.height+VARIABLE_HEIGHT(5);
    }
    
    
    if (eventDo.eventId)
    {
        [defaults setValue:eventDo.eventId forKey:@"BandEventID"];
    }
    // Title
    UILabel *titleLbl;
    NSString* titleName;
    if ([eventDo.isAppSiteFlag boolValue])
    {
        titleName = eventDo.retailName;
    }
    else{
        
        if (eventDo.retailName.length > 0) {
            
            titleName = eventDo.retailName;
        }
        else if (eventDo.evtLocTitle > 0 && ![eventDo.evtLocTitle isEqualToString:@"N/A"]) {
            
            titleName = eventDo.evtLocTitle;
        }
        else
        {
            titleName = eventDo.bandName;
        }
    }
    
    titleLbl = [self createLabel:CGRectMake(VARIABLE_WIDTH(10), asyncImageView.frame.size.height + VARIABLE_HEIGHT(5), SCREEN_WIDTH - VARIABLE_WIDTH(10), VARIABLE_HEIGHT(30)) :titleName];
    
    titleLbl.textColor = [UIColor blackColor];
    titleLbl.font = [UIFont boldSystemFontOfSize:fontSize];
    [mscrollView addSubview:titleLbl];
    
    yPos = yPos+titleLbl.frame.size.height+VARIABLE_HEIGHT(5);
    
    
    
    // CalIconImage
    
    UIImageView* calImage = [[UIImageView alloc] initWithFrame:CGRectMake(titleLbl.frame.origin.x + VARIABLE_WIDTH(2), titleLbl.frame.origin.y +  titleLbl.frame.size.height + VARIABLE_HEIGHT(5), VARIABLE_WIDTH(18), VARIABLE_HEIGHT(18))];
    calImage.image = [UIImage imageNamed:@"calendar1"];
    [mscrollView addSubview:calImage];
    
    // CalText
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    
    NSDate *dateFromString ;
    
    dateFromString = [dateFormatter dateFromString:eventDo.startDate];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd"];
    
    NSString *stDate = [dateFormatter stringFromDate:dateFromString];
    
    UILabel *calLbl = [self createLabel:CGRectMake(calImage.frame.origin.x + calImage.frame.size.width + VARIABLE_WIDTH(10), calImage.frame.origin.y - VARIABLE_HEIGHT(5), VARIABLE_WIDTH(60), VARIABLE_HEIGHT(30)) :stDate];
    
    calLbl.textColor = [UIColor grayColor];
    calLbl.font = [UIFont systemFontOfSize:IPAD ? 18 : 14];
    [mscrollView addSubview:calLbl];
    yPos = yPos + calImage.frame.size.height+VARIABLE_HEIGHT(5);
    
    // ClockIconImage
    if (eventDo.startTime)
    {
        
        UIImageView* clockImage = [[UIImageView alloc] initWithFrame:CGRectMake(calImage.frame.origin.x + calImage.frame.size.width + calLbl.frame.origin.x + calLbl.frame.size.width + VARIABLE_WIDTH(6), calImage.frame.origin.y + VARIABLE_HEIGHT(2) ,IPAD ? VARIABLE_WIDTH(16) : VARIABLE_WIDTH(18), IPAD? VARIABLE_HEIGHT(18) : VARIABLE_HEIGHT(16))];
        clockImage.image = [UIImage imageNamed:@"clocktime"];
        [mscrollView addSubview:clockImage];
        
        // ClockText
        
        UILabel *clockLbl = [self createLabel:CGRectMake(clockImage.frame.origin.x + clockImage.frame.size.width + VARIABLE_WIDTH(10), clockImage.frame.origin.y - VARIABLE_HEIGHT(6), VARIABLE_WIDTH(80), VARIABLE_HEIGHT(30)) :eventDo.startTime];
        
        clockLbl.textColor = [UIColor grayColor];
        clockLbl.font = [UIFont systemFontOfSize:IPAD ? 18 : 14];
        [mscrollView addSubview:clockLbl];
        
    }
    UILabel *recurringDays;
    UIFont *fontForlabel = [UIFont systemFontOfSize:IPAD ? 16 : 12];
   CGFloat  recurringHeight =  [self getLabelSize:eventDo.recurringDays : fontForlabel];
    if(eventDo.recurringDays)
    {
        recurringDays = [[UILabel alloc]initWithFrame:CGRectMake(VARIABLE_WIDTH(10), yPos + VARIABLE_HEIGHT(5), SCREEN_WIDTH-VARIABLE_WIDTH(10), recurringHeight+VARIABLE_HEIGHT(5))];
        recurringDays.numberOfLines=0;
        recurringDays.text= eventDo.recurringDays;
        recurringDays.textColor = [UIColor blackColor];
        recurringDays.font = [UIFont systemFontOfSize:IPAD ? 16 : 12];
        [mscrollView addSubview:recurringDays];
        yPos = yPos+ recurringDays.frame.size.height+ VARIABLE_HEIGHT(5);
    }
    
    // Band Name Title
    
    UIButton *nameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nameBtn.frame = CGRectMake(VARIABLE_WIDTH(10), yPos+VARIABLE_HEIGHT(5),  (SCREEN_WIDTH - VARIABLE_WIDTH(30))/2, VARIABLE_HEIGHT(30));
    
    nameBtn.backgroundColor = [UIColor lightGrayColor];
    //nameBtn.layer.cornerRadius = 10;
    nameBtn.tag = 0;
    nameBtn.titleLabel.font = [UIFont boldSystemFontOfSize:IPAD ? 18 : 14];
    nameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    nameBtn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    if([eventDo.bandCntFlag isEqual: [NSNumber numberWithInt:0]])
    {
        if(eventDo.popUpMsg)
        {
            popUpMsgForNoBands = eventDo.popUpMsg;
        }
    noOfBandExistFlag = [NSNumber numberWithInt:0];
    [nameBtn setTitle:@"Bands" forState:UIControlStateNormal];
    }
    else
        if([eventDo.bandCntFlag isEqual: [NSNumber numberWithInt:1]])
        {
            noOfBandExistFlag = [NSNumber numberWithInt:1];
            [nameBtn setTitle:eventDo.bandName forState:UIControlStateNormal];
        }
    else
         if([eventDo.bandCntFlag isEqual: [NSNumber numberWithInt:2]])
    {
         noOfBandExistFlag = [NSNumber numberWithInt:2];
        [nameBtn setTitle:@"Bands" forState:UIControlStateNormal];
    }
    [nameBtn addTarget:self action:@selector(bandButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [nameBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [mscrollView addSubview:nameBtn];
    
    
    
    UIButton *venueBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    venueBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    venueBtn.frame = CGRectMake(nameBtn.frame.origin.x + nameBtn.frame.size.width +  VARIABLE_WIDTH(10),  yPos+VARIABLE_HEIGHT(5),(SCREEN_WIDTH - VARIABLE_WIDTH(30))/2, VARIABLE_HEIGHT(30));
    venueBtn.backgroundColor = [UIColor lightGrayColor];
    //venueBtn.layer.cornerRadius = 10;
    venueBtn.titleLabel.font = [UIFont boldSystemFontOfSize:IPAD ? 18 : 14];
    [venueBtn setTitle:@"Venue" forState:UIControlStateNormal];
    
    venueBtn.tag = 1;
    [venueBtn addTarget:self action:@selector(bandButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [venueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [mscrollView addSubview:venueBtn];
    yPos = yPos + nameBtn.frame.size.height+VARIABLE_HEIGHT(8);
    
    
    
    //Ticket button
    if(eventDo.ticketUrl)
    {
        UIFont * ticket_Font;
        //buy ticket
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone)
        {
            ticket_Font = [UIFont systemFontOfSize:14.0f];
        }
        else
        {
            ticket_Font = [UIFont systemFontOfSize:18.0f];
        }
        UIButton * buyTck_In_Band = [UIButton buttonWithType:UIButtonTypeCustom];
        [buyTck_In_Band addTarget:self action:@selector(bandButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        buyTck_In_Band.tag =3;
        buyTck_In_Band.frame = CGRectMake(VARIABLE_WIDTH(10), yPos+VARIABLE_HEIGHT(8), (IPAD ? VARIABLE_WIDTH(36) : VARIABLE_WIDTH(18))+[self widthOfString:@"Buy Tickets" withFont:ticket_Font] , IPAD? VARIABLE_HEIGHT(18) : VARIABLE_HEIGHT(16));
        [mscrollView addSubview:buyTck_In_Band];
        
        UIImageView* ticket_Image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, IPAD ? VARIABLE_WIDTH(16) : VARIABLE_WIDTH(18), IPAD? VARIABLE_HEIGHT(18) : VARIABLE_HEIGHT(16))];
        ticket_Image.image = [UIImage imageNamed:@"Ticket.png"];
        [buyTck_In_Band addSubview:ticket_Image];
        
        UILabel * ticket_Label = [[UILabel alloc]init];
        ticket_Label.frame = CGRectMake(ticket_Image.frame.size.width+ticket_Image.frame.origin.x + VARIABLE_WIDTH(5), 0,[self widthOfString:@"Buy Tickets" withFont:ticket_Font], IPAD? VARIABLE_HEIGHT(18) : VARIABLE_HEIGHT(16));
        //ticket_Label.backgroundColor = [UIColor blueColor];
        ticket_Label.numberOfLines =1;
        ticket_Label.text = @"Buy Tickets";
        ticket_Label.font = ticket_Font;
        
        [buyTck_In_Band addSubview:ticket_Label];
        yPos = yPos+ buyTck_In_Band.frame.size.height + VARIABLE_HEIGHT(5);
    }
    
    
   
    
    UIFont *fontForShordes = [UIFont systemFontOfSize:IPAD ? 18 : 14];
    height = [self getLabelSize:eventDo.shortDes:fontForShordes];
    
    UILabel *shortDesc;
    if (eventDo.shortDes)
    {
        shortDesc  = [[UILabel alloc]initWithFrame:CGRectMake(VARIABLE_WIDTH(10), yPos+VARIABLE_HEIGHT(5), SCREEN_WIDTH - 2*VARIABLE_WIDTH(15), height+VARIABLE_HEIGHT(5))];
        shortDesc.numberOfLines = 0;
        shortDesc.textColor = [UIColor darkGrayColor];
        shortDesc.textAlignment= NSTextAlignmentLeft;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone)
        {
            shortDesc.font = [UIFont systemFontOfSize:14.0f];
        }
        else
        {
            shortDesc.font = [UIFont systemFontOfSize:18.0f];
        }
        
        
        shortDesc.backgroundColor = [UIColor clearColor];
        shortDesc.text = eventDo.shortDes;
        [mscrollView addSubview:shortDesc];
        yPos = yPos + shortDesc.frame.size.height+ VARIABLE_HEIGHT(5);
    }
    
    
    NSString* htmlWithoutTag = [self stringByStrippingHTML : eventDo.longDes];
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        
        height = [self calculateLabelHeight: htmlWithoutTag labelWidth:SCREEN_WIDTH - VARIABLE_WIDTH(10)fontSize:13];
    }
    else{
        height = [self calculateLabelHeight: htmlWithoutTag labelWidth:SCREEN_WIDTH - VARIABLE_WIDTH(10) fontSize:18];
    }
    if (eventDo.longDes)
    {
        longDescWebView = [[UIWebView alloc]init];
        longDescWebView.delegate = self;
        longDescWebView.opaque = NO;
        longDescWebView.scrollView.bounces = NO;
        longDescWebView.layer.cornerRadius = 5.0;
        longDescWebView.backgroundColor = [UIColor clearColor];
        longDescWebView.frame = CGRectMake(VARIABLE_WIDTH(5), yPos+VARIABLE_HEIGHT(5), SCREEN_WIDTH - VARIABLE_WIDTH(10),height);
        longDescWebView.backgroundColor = [UIColor clearColor];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#555555;word-wrap: break-word'>%@",eventDo.longDes] baseURL:nil];
        }
        else
        {
            [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:18px;font-family:Helvetica;color:#555555;word-wrap: break-word'>%@",eventDo.longDes] baseURL:nil];
        }
        
        [mscrollView addSubview:longDescWebView];
        
        yPos = yPos+ longDescWebView.frame.size.height+VARIABLE_HEIGHT(5);
    }
    else
    {
        [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:18px;font-family:Helvetica;color:#000;word-wrap: break-word'>%@",eventDo.longDes] baseURL:nil];
    }
    
    
    
    // More Info Button
    NSLog(@"%f long description height", longDescWebView.frame.origin.y + longDescWebView.frame.size.height);
    UIButton* moreInfoButton ;
    if (eventDo.moreInfoURL.length != 0)
    {
        moreInfoButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        moreInfoButton.frame = CGRectMake(VARIABLE_WIDTH(10), yPos, VARIABLE_WIDTH(200), VARIABLE_HEIGHT(30));
        
        moreInfoButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        moreInfoButton.titleLabel.font = [UIFont boldSystemFontOfSize:IPAD ? 18 : 14];
        [moreInfoButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [moreInfoButton setTitle:@"> More Information" forState:UIControlStateNormal];
        [moreInfoButton addTarget:self action:@selector(bandButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        moreInfoButton.tag = 2;
        [mscrollView addSubview:moreInfoButton];
        yPos = yPos + moreInfoButton.frame.size.height;
    }
    
    [mscrollView setContentSize:CGSizeMake(SCREEN_WIDTH, yPos+80)];
    mscrollView.contentInset = UIEdgeInsetsMake(0, 0, VARIABLE_HEIGHT(20), 0);
    [self shareBandEvent];
    
}

-(int)calculateLabelHeight:(NSString*)text labelWidth:(int)labelWidth fontSize:(int)fontSizeText
{
    CGSize constraint;
    CGSize size ;
    CGFloat heightWebview ;
    constraint = CGSizeMake(labelWidth, 20000.0f);
//    size = [text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:fontSizeText] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
   size = [text boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:fontSizeText], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    
  heightWebview = MAX(size.height, 40.0f);
    return heightWebview;
}


-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ([[[inRequest URL] scheme] isEqual:@"mailto"]) {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
             [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,(CFStringRef)value,
                                                                                                                   CFSTR("")));
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            // [mailViewController release];
            return NO;
            
        }
        
        else {
            [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
            
            
        }
        
        return NO;
        
    }
    
    if ([[[inRequest URL] scheme] isEqual:@"tel"]) {
        NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
        
        NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
        NSString *phoneString  = [[defaultRecipient componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        NSString *phoneNumber = [NSString stringWithFormat:@"tel:%@", phoneString];
        NSURL    *url= [NSURL URLWithString:phoneNumber];
        
        UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        webview.alpha = 0.0;
        
        [webview loadRequest:[NSURLRequest requestWithURL:url]];
        
        // Assume we are in a view controller and have access to self.view
        [self.view insertSubview:webview belowSubview:longDescWebView]; //ask ashika
               return NO;
    }
    
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        
        NSString *url = [[inRequest URL]absoluteString];
        [defaults setValue:url forKey:KEY_URL];
        //[defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
        
        WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:urlDetail animated:NO];
        //[urlDetail release];
        
        return NO;
    }
    return YES;
}



-(float) getLabelSize : (NSString*) labelString : (UIFont *) font
{
    if ([labelString isEqualToString:@" "])
    {
        return 0;
    }
   // float descFontSize;
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
     //   descFontSize=15;
    }
    else
    {
     //   descFontSize=12;
    }
    CGSize constraint = CGSizeMake(SCREEN_WIDTH, CGFLOAT_MAX);
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    CGSize size = [labelString boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraph } context: nil].size;
    
    
    
    
    return  size.height+20;
    
}

- (CGFloat)widthOfString:(NSString *)labelString withFont:(UIFont *)font
{
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    NSLog(@"Ticket width %f",[[[NSAttributedString alloc] initWithString:labelString attributes:attributes] size].width);
    return [[[NSAttributedString alloc] initWithString:labelString attributes:attributes] size].width;
    
}
-(CGSize) getStringWidth : (NSString*) str
{
    CGSize textsize = [str sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]}];
    
    CGSize newSize = CGSizeMake(ceilf(textsize.width), ceilf(textsize.height));
    
    return newSize;
    
}


-(void) bandButtonClicked : (id) sender
{
    UIButton* btn = (UIButton*) sender;
    switch (btn.tag) {
        case 0:
        {
            //no bands are associated
            if(noOfBandExistFlag == [NSNumber numberWithInt:0])
            {
                [UtilityManager showAlert:nil msg:popUpMsgForNoBands];
            }
            else // one band is associated
            if(noOfBandExistFlag == [NSNumber numberWithInt:1])
            {
                BandRetailerSummaryViewController* evt = [[BandRetailerSummaryViewController alloc] initWithNibName:@"BandRetailerSummaryViewController" bundle:nil];
                self.retailerSummaryPage = evt;
                evt.retailerBandId = eventDo.bandIds;
                [self.navigationController pushViewController:evt animated:YES];
            }
            else if(noOfBandExistFlag == [NSNumber numberWithInt:2])//multiple bands are associated
            {
                MultipleBandViewController* multipleVC = [[MultipleBandViewController alloc] initWithNibName:@"MultipleBandViewController" bundle:nil];
                multipleVC.navigationTitleText = eventDo.eventName;
                [self.navigationController pushViewController:multipleVC animated:YES];
            }
        }
            
            break;
            
        case 1: // show map
        {
            if ([eventDo.isAppSiteFlag boolValue]) {
               
                [self request_appsitedetails];
               
            }
            else{
                
                EventDetailMapViewController *evtMap = [[EventDetailMapViewController alloc]initWithNibName:@"EventDetailMapViewController" bundle:nil];
                evtMap.eventDo = eventDo;
                [self.navigationController pushViewController:evtMap animated:NO];
            }
            
        }
            
            break;

        case 2: // more info
        {
            [defaults setValue:eventDo.moreInfoURL forKey:KEY_URL];
            
            WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
        }
            break;

            
        case 3 ://Buy Ticket
        {
            //[defaults setValue:eventDo.moreInfoURL forKey:KEY_URL];
            [defaults setValue:eventDo.ticketUrl forKey:KEY_URL];
            
            WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
        }
            break;
            
        default:
            break;
    }
    
}




-(UILabel*) createLabel :  (CGRect) frame : (NSString*) text
{
    UILabel *titleLbl = [[UILabel alloc]initWithFrame:frame];
    titleLbl.backgroundColor = [UIColor clearColor];
    titleLbl.text = text;
    
    return titleLbl;
}


-(void)popBackToPreviousPage{
    
   
    [self.navigationController popViewControllerAnimated:NO];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)parse_retsumm:(NSString*)response{
    
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
       
        return;
    }
    else {
        
        [defaults setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
       
        [self.navigationController pushViewController:rsvc animated:NO];
      
    }

}


-(void)request_appsitedetails{
    
    
    iWebRequestState = RETSUMMARY;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",eventDo.retailLocationId];
    if([defaults valueForKey:KEY_MAINMENUID])
    [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    [requestStr appendFormat:@"<scanTypeId>0</scanTypeId>"];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",eventDo.longitude,
         eventDo.latitude];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",eventDo.retailId];
    
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *urlString =  [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);
}

-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case RETSUMMARY:
            [self parse_retsumm:response];
            break;
            
        default:
            break;
            
            
            
    }
    
}

@end

//
//  EventDetailMapViewController.h
//  HubCiti
//
//  Created by service on 4/14/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MKMapView.h>
#import <MapKit/MKAnnotation.h>
#import "EventListDetails.h"
@interface EventDetailMapViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate,CustomizedNavControllerDelegate>
@property(nonatomic,strong)  EventListDetails* eventDo ;
@property(nonatomic, strong)  MKMapView *mapView;
@property(nonatomic, strong) CLLocationManager *locationManager;
@end

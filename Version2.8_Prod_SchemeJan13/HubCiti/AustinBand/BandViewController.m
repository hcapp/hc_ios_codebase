//
//  BandViewController.m
//  HubCiti
//
//  Created by Ashika on 4/14/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "BandViewController.h"
#import "MainMenuViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "HTTPClient.h"
#import "AppDelegate.h"

#import "EventsListViewController.h"
#import "EventsCategoryDO.h"
#import "EventDetailsDO.h"
#import "EventListDetailViewController.h"
#import "EventGroupingAndSortingViewController.h"
#import "HubCitiConstants.h"
#import "SettingsViewController.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "CityExperienceViewController.h"
#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "FindViewController.h"
#import "AnythingPage.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "SingleCatRetailers.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FAQCategoryList.h"
#import "FilterListViewController.h"
#import "FundraiserListViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "SpecialOffersViewController.h"
#import "HTTPClient.h"
#import "GetUserInfoResponse.h"
#import "BandEventType.h"
#import "BandCategories.h"
#import "EventDetailImageViewController.h"
#import "BandsNearByEventsMapTable.h"
#import "BandRetailerSummaryViewController.h"
#import "SwipesViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponsInDealViewController.h"

NSDictionary *viewDictionary,*tableViewDictionary;

@interface BandViewController ()<HTTPClientDelegate>
{
    NSInteger numberOfRows;
    NSMutableArray *bandSubLabel;
    NSMutableArray *bandDefaultResult;
    NSMutableArray *bandGrouingInfo;
    NSString *searchResultBandName, *searchResultBand;
    BOOL eventTypeSearch,bandSearch,eventSearch;
    bottomButtonView *view;
    
}
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property(nonatomic,strong) BandEventType *bandEventType;
@property(nonatomic,strong) BandCategories *bandCategories;
@property (strong,nonatomic) BandEventsList *bandEventsResponse;
@property (nonatomic,strong) NSMutableArray *eventArray;
@property (nonatomic,strong) NSMutableArray *eventSearchArray;
@property (nonatomic,strong) NSMutableArray *bandArray;
@property (nonatomic, assign) NSInteger oldSegmentedIndex;


@end
@implementation BandViewController
@synthesize bandTableView,searchBands;
@synthesize infoResponse,bandListPage,bandSummaryPage,eventSummaryPage;
@synthesize anyVC,emailSendingVC,bandEventType,bandCategories,bandEventsResponse,eventArray,bandArray,eventSearchArray,buttonView1,buttonView2,buttonView3,buttonView4,sortObj,savedSortObj,buttonEventView1,buttonEventView2,buttonEventView3,buttonEventView4,bandPageName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Service Respone methods
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription] ];
    
    }

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    DLog(@"parameter: %@",responseObject);
    
    [self responseData:responseObject];
    
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
     [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    
    self.navigationController.navigationBar.hidden = NO;
    
    //customized back button navigation bar
    
   
    self.navigationItem.hidesBackButton = YES;
    
    //customized home button navigation bar
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = mainPage;
    
    //title for bands landing page
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:bandPageName forView:self withHambergur:YES];
    
   
   // self.navigationController.navigationBar.topItem.title = bandPageName;
    lastRecord = 0;
    [defaults setBool:NO forKey:@"ViewMoreBand"]; // For pagination implementation
    
    searchBands.placeholder = @"Search by Band Name";
    if (IOS7 == NO){
        searchBands.tintColor = [UIColor blackColor];
        
    }
    
    
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
    sortObj = [[SortAndFilter alloc] init];
    sortObj.alphabeticallySelected = YES;
    savedSortObj = [[SortAndFilter alloc] init];
    savedSortObj.alphabeticallySelected = YES;
    [self customizedSegmentController];
    // [self requestForEventType];
    
    [self requestForBandList:nil:savedSortObj];
   
    
    
}
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:NO];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    
    [defaults setBool:NO forKey:@"ViewMore"];
    if([segment selectedSegmentIndex] == 2){
        
        [segment setSelectedSegmentIndex:_oldSegmentedIndex];
        [self setSegmentColor:segment];
        
    }
    
    if([defaults valueForKey:@"isComingFromGroupingandSorting"]){
        // ReleaseAndNilify(bandTableView);
        lastRecord = 0;
        nextPage = 0;
        
        [bandGrouingInfo removeAllObjects];
        sortObj = [[SortAndFilter alloc]initWithObject:[self loadCustomObjectWithKey:@"SortFilterObject"]];
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        [defaults setValue:nil forKey:@"ViewMoreBand"];
        if(_oldSegmentedIndex == 0)
            [self requestForBandList:searchResultBandName :sortObj];
        else
            [self requestForBandList:searchResultBand :sortObj];
    }
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}
- (SortAndFilter *)loadCustomObjectWithKey:(NSString *)key {
    
    NSData *encodedObject = [defaults objectForKey:key];
    SortAndFilter *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}
#pragma customized segment controller

-(void) customizedSegmentController{
    
    
    segment=[[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Name",@"Genre",@"Shows", nil]];
    
    [segment setSelectedSegmentIndex:0];
    [segment setBackgroundColor:[UIColor grayColor]];
   
    [segment setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:0];
    [segment setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:1];
    [segment setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:2];
    [self setSegmentColor:segment]; //add customized color
    
    segment.translatesAutoresizingMaskIntoConstraints = NO;
    [segment addTarget:self action:@selector(viewSelectionChanged:) forControlEvents:UIControlEventValueChanged];
    
    
    searchBands = [[UISearchBar alloc] init];
    searchBands.showsCancelButton = YES;
    [searchBands setBarTintColor:[UIColor darkGrayColor]];
    searchBands.delegate = self;
    searchBands.translatesAutoresizingMaskIntoConstraints = NO;
    searchBands.searchBarStyle = UISearchBarStyleDefault;
    searchBands.translucent = YES;
    searchBands.layer.borderWidth = 1;
    searchBands.layer.borderColor = [UIColor darkGrayColor].CGColor;
    searchBands.placeholder = @"Search by Band Name";
    // inset for table view
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad)
    {
        [bandTableView setContentInset:UIEdgeInsetsMake(0, 0, VARIABLE_HEIGHT(25), 0)];
    }
    else
    {
        [bandTableView setContentInset:UIEdgeInsetsMake(0, 0, VARIABLE_HEIGHT(40), 0)];
    }
    
    viewDictionary = NSDictionaryOfVariableBindings(segment,searchBands);
    
    [self.view addSubview:segment];
    [self.view addSubview:searchBands];
    //set constraits for segment controller
    
    // Vertical constrains
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-2)-[segment(48)]"] options:0 metrics:0 views:viewDictionary]];
    
    
    // Horizontal constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(-2)-[segment(%f)]",SCREEN_WIDTH+4] options:0 metrics:0 views:viewDictionary]];
    
    
    // Vertical constrains
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(44)-[searchBands(44)]"] options:0 metrics:0 views:viewDictionary]];
    
    
    // Horizontal constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[searchBands(%f)]",SCREEN_WIDTH] options:0 metrics:0 views:viewDictionary]];
    
    
    
}
-(void) setSegmentColor:(UISegmentedControl *) segmentSender
{
    
    for (int i=0; i<[segmentSender.subviews count]; i++)
    {
        if ([[segmentSender.subviews objectAtIndex:i]isSelected] )
        {
            UIColor *tintcolor=[UIColor colorWithRed:80/255.0 green:199/255.0 blue:228/255.0 alpha:1.0];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
            //break;
        }
        else {
            UIColor *tintcolor=[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
        }
    }
    for (int i=0; i<[segmentSender.subviews count]; i++)
    {
        if (![[segmentSender.subviews objectAtIndex:i]isSelected] )
        {
            UIColor *tintcolor=[UIColor darkGrayColor];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
            // break;
        }
        
    }
    
    float segmentFont = 14.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        segmentFont = 18.0;
    }
    
 

    NSDictionary *attributes = @{
                                  NSFontAttributeName : [UIFont boldSystemFontOfSize:segmentFont],
                                  NSForegroundColorAttributeName : [UIColor blackColor]};
    
    [segmentSender setTitleTextAttributes:attributes forState:UIControlStateSelected];
    
    NSDictionary *attributes2 = @{
                                  NSFontAttributeName : [UIFont boldSystemFontOfSize:segmentFont],
                                  NSForegroundColorAttributeName : [UIColor whiteColor]};
    [segmentSender setTitleTextAttributes:attributes2 forState:UIControlStateNormal];
    
}


-(void)viewSelectionChanged:(id)sender
{
    
    switch ([segment selectedSegmentIndex])
    {
            
        case 0:{
            [searchBands resignFirstResponder];
            _oldSegmentedIndex = 0;
            [defaults setBool:NO forKey:@"ViewMore"];
            //[self setSegmentImage];
            eventSearch = NO;
            bandSearch = NO;
            searchBands.text = @"";
            searchResultBand = nil;
            lastRecord = 0;
            searchBands.placeholder = @"Search by Band Name";
            [self setSegmentColor:segment];
            sortObj = [[SortAndFilter alloc] init];
            sortObj.alphabeticallySelected = YES;
            [self requestForBandList:nil:sortObj];
        }
            break;
        case 1:
        {
            [searchBands resignFirstResponder];
            
            [defaults setBool:NO forKey:@"ViewMore"];
            //[self setSegmentImage];
            eventSearch = NO;
            bandSearch = NO;
            searchBands.text = @"";
            searchResultBandName = nil;
            lastRecord = 0;
            searchBands.placeholder = @"Search by Band Name/Category";
            [self setSegmentColor:segment];
            sortObj = [[SortAndFilter alloc] init];
            sortObj.alphabeticallySelected = YES;
            _oldSegmentedIndex = 1;
            [self requestForBandList:nil:sortObj];
            //   [bandTableView reloadData];
            
        }
            break;
        case 2:{
            [searchBands resignFirstResponder];
            //[self setSegmentColor:segment];
            BandsNearByEventsMapTable *eventList = [[BandsNearByEventsMapTable alloc] initWithNibName:@"BandsNearByEventsMapTable" bundle:nil];
            self.bandListPage = eventList;
            [self.navigationController pushViewController:eventList animated:YES];
            
        }
            
    }
    
}

-(void)settableViewOnScreen
{
    
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    if (!bandTableView) {
        if(bottomButton==1){
            bandTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 88, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-50-88) style:UITableViewStylePlain];
        }
        else{
            bandTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 88, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-88) style:UITableViewStylePlain];
        }
        bandTableView.dataSource=self;
        bandTableView.delegate=self;
        [bandTableView setBackgroundColor:[UIColor whiteColor]];
        [bandTableView setAccessibilityIdentifier:@"eventsList"];
        bandTableView.tableFooterView.hidden=YES;
        [self.view addSubview:bandTableView];
    }
    
}

// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    // Calculate Y position for View
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        if([segment selectedSegmentIndex] == 1)
            tabImage.tag = 26;
        
        else if([segment selectedSegmentIndex] == 0)
            tabImage .tag = 16;
        [self.view bringSubviewToFront:tabImage];
        
    }
    
    
    
    for(int btnLoop= 0; btnLoop < [arrEventBottomButtonDO count]; btnLoop++)
    {
        bottomButtonDO *obj_eventBottomDO = [arrEventBottomButtonDO objectAtIndex:btnLoop];
        
        view = [[bottomButtonView alloc]init];
        
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, (SCREEN_HEIGHT-yVal-bottomBarButtonHeight) , 80,bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrEventBottomButtonDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrEventBottomButtonDO count],bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        
        
        if([segment selectedSegmentIndex] == 0){
            if(btnLoop ==0)
                self.buttonView1 = view;
            else if(btnLoop == 1)
                self.buttonView2 = view;
            else if (btnLoop == 2)
                self.buttonView3 = view;
            else if (btnLoop == 3)
                self.buttonView4 = view;
        }
        else{
            if(btnLoop ==0)
                self.buttonEventView1 = view;
            else if(btnLoop == 1)
                self.buttonEventView2 = view;
            else if (btnLoop == 2)
                self.buttonEventView3 = view;
            else if (btnLoop == 3)
                self.buttonEventView4 = view;
        }
        [view setAccessibilityLabel:obj_eventBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
    }
}

#pragma tableview delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    switch ([segment selectedSegmentIndex])
    {
        case 0:
        {
            if(bandGrouingInfo.count>0){
                BandGrouping *bandCategoryDo =[bandGrouingInfo objectAtIndex:0];
                if(nextPage == 1)
                    numberOfRows = bandCategoryDo.bandArray.count + 1;
                else
                    numberOfRows = bandCategoryDo.bandArray.count;
                
                
            }
            else {
                numberOfRows =0;
            }
            break;
        }
        case 1:
        {
            if(bandGrouingInfo.count>0){
                BandGrouping *bandCategoryDo =[bandGrouingInfo objectAtIndex:section];
                if(nextPage == 1 && section == [bandGrouingInfo count]-1)
                    numberOfRows = bandCategoryDo.bandArray.count + 1;
                else
                    numberOfRows = bandCategoryDo.bandArray.count;
                
                
            }
            else {
                numberOfRows =0;
            }
            break;
        }
            
    }
    return numberOfRows;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if([segment selectedSegmentIndex] == 0)
        return 0.0;
    else{
        
        return 18;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if([segment selectedSegmentIndex] == 1){
        float labelFont = 15.0;
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            labelFont = 18.0;
        }
        if(bandGrouingInfo.count >0){
            BandGrouping *bandCategoryDo =[bandGrouingInfo objectAtIndex:section];
            
            UILabel *lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH - 10, 18)];
            [lblCatName setTextColor:[UIColor blackColor]];
            [lblCatName setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
            [lblCatName setFont:[UIFont boldSystemFontOfSize:labelFont]];
            [lblCatName setText:[NSString stringWithFormat:@" %@",bandCategoryDo.groupContent]];
            return lblCatName;
        }
        else
            return nil;
    }
    else
        return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([segment selectedSegmentIndex] == 1||[segment selectedSegmentIndex] == 0){
        if (bandGrouingInfo.count>0) {
            return [bandGrouingInfo count];
        }
        else{
            
            return 0;
        }
    }
    else{
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        
        return 50.0;
    }
    else
    {
        return 70.0;
        
    }
    
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [defaults setBool:NO forKey:@"ViewMoreBand"];
    switch ([segment selectedSegmentIndex]) {
        case 2:
            // BandsNearByEventsMapTable *bandsNearByEventsMapTable = [BandsNearByEventsMapTable alloc] init
        {
            if(eventSearch){
               
                EventDetailImageViewController* eventSummary = [[EventDetailImageViewController alloc] initWithNibName:@"EventDetailImageViewController" bundle:nil];
                //eventSearchArray
                self.eventSummaryPage = eventSummary;
                eventSummary.eventDo = [eventSearchArray objectAtIndex:indexPath.row];
                [self.navigationController pushViewController:eventSummary animated:YES];
               
                
            }
            else{
               
                BandsNearByEventsMapTable *eventList = [[BandsNearByEventsMapTable alloc] initWithNibName:@"BandsNearByEventsMapTable" bundle:nil];
                self.bandListPage = eventList;
                EventCatList *localEvent = [eventArray objectAtIndex:indexPath.row];
                eventList.eventTypeID = [localEvent evtTypeID];
                eventList.eventTypeName = [localEvent evtTypeName];
                NSLog(@"sadjhkjsa: %@",[localEvent evtTypeID]);
                [self.navigationController pushViewController:eventList animated:YES];
                
            }
        }
            
            break;
        case 0:
        case 1:
        {
            BandGrouping *bandCategoryDo =[bandGrouingInfo objectAtIndex:indexPath.section];
            
            BandRetailerSummaryViewController* evt = [[BandRetailerSummaryViewController alloc] initWithNibName:@"BandRetailerSummaryViewController" bundle:nil];
            self.bandSummaryPage = evt;
            BandCatList *bandCatList = [bandCategoryDo.bandArray objectAtIndex:indexPath.row];
            evt.retailerBandId = [bandCatList.bandID stringValue];
            [self.navigationController pushViewController:evt animated:YES];
            
            
        }
            break;
            
            
        default:
            break;
    }
    
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // pagination implementation in band section
    
    switch ([segment selectedSegmentIndex]) {
        case 0:
        case 1:
        {
            BandGrouping *bandCategoryDo =[bandGrouingInfo objectAtIndex:indexPath.section];
            if(indexPath.row == [bandCategoryDo.bandArray count] && nextPage ==1){
                if (![defaults boolForKey:@"ViewMoreBand"] && lastRecord !=0) {
                    
                    searchBands.userInteractionEnabled = NO;
                    [defaults setBool:YES forKey:@"ViewMoreBand"];
                    [defaults setBool:YES forKey:@"ViewMore"];
                    segment.userInteractionEnabled = NO;
                    
                    dispatch_queue_t dispatchQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(dispatchQueue, ^(void){
                        [self fetchNextBandList];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [loading stopAnimating];
                            
                            
                            segment.userInteractionEnabled = YES;
                            searchBands.userInteractionEnabled = YES;
                            [defaults setBool:NO forKey:@"ViewMore"];
                        });
                        
                    });
                }
            }
            
        }
            break;
        default:
            break;
    }
    
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"myCell";
    static NSString *cellIdentifier1 = @"Cell";
    UITableViewCell *cell = nil;
    
    
            BandGrouping *bandCategoryDo =[bandGrouingInfo objectAtIndex:indexPath.section];
            if(indexPath.row != [bandCategoryDo.bandArray count]){
                cell=  [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                if (cell == nil)
                {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] ;
                    cell.backgroundColor =  [UIColor clearColor];
                    
                }
                else
                {
                    NSArray *arr = [cell.contentView subviews];
                    for(int i=0; i<[arr count]; i++)
                    {
                        UIView *views = [arr objectAtIndex:i];
                       // if(![views isKindOfClass:[UIImageView class]])
                            [views removeFromSuperview];
                    }
                }
                
            }
            else if(indexPath.row == [bandCategoryDo.bandArray count] && nextPage == 1){
                cell=  [tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
                if (cell == nil)
                {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier1] ;
                    cell.backgroundColor =  [UIColor clearColor];
                    
                }
                else
                {
                    NSArray *arr = [cell.contentView subviews];
                    for(int i=0; i<[arr count]; i++)
                    {
                        UIView *views = [arr objectAtIndex:i];
                        [views removeFromSuperview];
                    }
                }
                
            }

    // UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell)
    {
    //cell customized Label
    UILabel *largeLabel = [[UILabel alloc] init];
    [largeLabel setTextColor:[UIColor blackColor]];
    if(DEVICE_TYPE == UIUserInterfaceIdiomPhone)
        largeLabel.font = [UIFont boldSystemFontOfSize:14];
    else
        largeLabel.font = [UIFont boldSystemFontOfSize:17];
    [cell.contentView addSubview:largeLabel];
    
    //cell customized detail Label
    UILabel *detailLabel = [[UILabel alloc] init];
    [detailLabel setTextColor:[UIColor darkGrayColor]];
    if(DEVICE_TYPE == UIUserInterfaceIdiomPhone)
        detailLabel.font = [UIFont boldSystemFontOfSize:12];
    else
        detailLabel.font = [UIFont boldSystemFontOfSize:15];
    
    //cel imageview
    CGRect frame;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
        frame.origin.x = 5;
        frame.origin.y = 5;
        frame.size.width = 40;
        frame.size.height = 40;
    }
    else
    {
        frame.origin.x = 5;
        frame.origin.y = 5;
        frame.size.width = 60;
        frame.size.height = 60;
    }
    
    SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:frame];
    asyncImageView.contentMode = UIViewContentModeScaleToFill;
    asyncImageView.backgroundColor = [UIColor clearColor];
    
    //set autolayout constrait mask
    largeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    detailLabel.translatesAutoresizingMaskIntoConstraints = NO;
    asyncImageView.translatesAutoresizingMaskIntoConstraints = NO;
    
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"right_arrow.png"]];
    
    //toogle for segement controller
    switch ([segment selectedSegmentIndex]) {
            //event section
            //band section
        case 0:
        case 1:
        {
            BandGrouping *bandCategoryDo =[bandGrouingInfo objectAtIndex:indexPath.section];
            
            if(indexPath.row != [bandCategoryDo.bandArray count]){
                
                largeLabel.text = [[bandCategoryDo.bandArray objectAtIndex:indexPath.row] bandName];
                if(largeLabel.text == nil)
                    largeLabel.text = @" ";
                
                
                detailLabel.text =[[bandCategoryDo.bandArray objectAtIndex:indexPath.row] catName];
                if(detailLabel.text == nil)
                    detailLabel.text = @" ";
                [cell.contentView addSubview:detailLabel];
                [asyncImageView loadImage:[[bandCategoryDo.bandArray objectAtIndex:indexPath.row] bandImgPath]];
                [cell.contentView addSubview:asyncImageView];
                
                
                tableViewDictionary = NSDictionaryOfVariableBindings(largeLabel,detailLabel,asyncImageView);
                
                [self setConstraits:cell]; //function call to set constraits to cell contents
            }
            
            else if(indexPath.row == [bandCategoryDo.bandArray count] && nextPage == 1) //pagination call to load next set of data
            {
                
                loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                loading.center = cell.contentView.center;
                CGRect frame;
                
                frame.origin.x = SCREEN_WIDTH/2;
                frame.origin.y = 5;
                frame.size.width = 40;
                frame.size.height = 40;
                
                if(DEVICE_TYPE == UIUserInterfaceIdiomPad)
                    
                    loading.frame = frame;
                loading.color = [UIColor blackColor];
                loading.tag = 31;
                [cell.contentView addSubview:loading];
                [loading startAnimating];
                cell.userInteractionEnabled = NO;
            }
        }
            break;
    }
    }
    return cell;
}

//constraits for cell contents

-(void) setConstraits:(UITableViewCell*)cell{
    
       //band section
        if (  DEVICE_TYPE==UIUserInterfaceIdiomPad )
        {
            
            
            if([tableViewDictionary objectForKey:@"detailLabel"] != nil)
            {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[asyncImageView(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[largeLabel(20)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[largeLabel(400)]"] options:0 metrics:0 views:tableViewDictionary]];
                
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(35)-[detailLabel(20)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[detailLabel(650)]"] options:0 metrics:0 views:tableViewDictionary]];
            }
            
            else{
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(20)-[largeLabel(30)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(25)-[largeLabel(400)]"] options:0 metrics:0 views:tableViewDictionary]];
                
            }
        }
        else{
            
            if([tableViewDictionary objectForKey:@"detailLabel"] != nil)
            {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[asyncImageView(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[largeLabel(20)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[largeLabel(200)]"] options:0 metrics:0 views:tableViewDictionary]];
                
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(24)-[detailLabel(20)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[detailLabel(250)]"] options:0 metrics:0 views:tableViewDictionary]];
                
            }
            else{
                
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(15)-[largeLabel(20)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(15)-[largeLabel(%f)]",SCREEN_WIDTH-VARIABLE_WIDTH(50)] options:0 metrics:0 views:tableViewDictionary]];
            }
            
        }

}

-(void)popBackToPreviousPage{
    
    [defaults setBool:NO forKey:@"ViewMoreBand"];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

//smart search functionality

-(void) smartSearch{
    bandTableView.scrollEnabled = YES;
    bandTableView.userInteractionEnabled = YES;
    
    
    switch ([segment selectedSegmentIndex]) {
            
        case 0:{
            {
                bandSearch = TRUE;
                lastRecord = 0;
                
                searchResultBandName = searchBands.text;
                [self requestForBandList:searchResultBandName:sortObj];
                
            }
            
        }
            break;
        case 1:
        {
            
            bandSearch = TRUE;
            lastRecord = 0;
            searchResultBand = searchBands.text;
            [self requestForBandList:searchResultBand:sortObj];
            
        }
            break;
        default:
            break;
    }
    [timer invalidate];
    timer = nil;
    
}

#pragma mark api methods

-(void) fetchNextBandList{
    if (nextPage == 1)
    {
        
        if([segment selectedSegmentIndex] ==1)
            [self requestForBandList:searchResultBand:sortObj];
        else
            
            [self requestForBandList:searchResultBandName:sortObj];
        
    }
    else
    {
        return;
    }
}



//request for band type listing with and without search
-(void) requestForBandList:(NSString *)searchText :(SortAndFilter *) sortFilObj{
    
    iWebRequestState = BAND_CAT_TYPE;
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    
    
    
    if([defaults valueForKey:KEY_USERID])
        [parameter setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    [parameter setValue:@"ios" forKey:@"platform"];
    [parameter setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    
    if([defaults valueForKey:KEY_MITEMID])
        [parameter setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [parameter setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    
    if ([RegionApp isEqualToString:@"1"])
        [parameter setValue:@"1" forKey:@"isRegApp"];
    
    
    if ([[defaults valueForKey:KEYZIPCODE] length])
    {
        [parameter setValue:[defaults valueForKey:KEYZIPCODE]forKey:@"postalCode"];
        
    }
   
    /*if([RegionApp isEqualToString:@"1"] && [defaults valueForKey:@"sortedCitiIDs"])
    {
        [parameter setValue:[defaults valueForKey:@"sortedCitiIDs"] forKey:@"cityIds"];
            
    }*/
        
    if([defaults valueForKey:@"FindRadius"])
    {
        [parameter setValue:[defaults valueForKey:@"FindRadius"] forKey:@"radius"];
        
    }
    else
    {
        [parameter setValue:@"50" forKey:@"radius"];
        
        
    }
    [parameter setValue:lastRecord forKey:@"lastVisitedNo"];
    
    if(searchText)
        [parameter setValue:searchText forKey:@"searchKey"];
    
    if (sortFilObj.selectedCatIds) {
        [parameter setValue:sortFilObj.selectedCatIds forKey:@"catIds"];
    }
    
    if([segment selectedSegmentIndex] == 0){
        [parameter setValue:@"name" forKey:@"groupBy"];
    }
    else if([segment selectedSegmentIndex] ==1){
        [parameter setValue:@"type" forKey:@"groupBy"];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@band/getbandlist",BASE_URL];
    // NSString *urlString = [NSString stringWithFormat:@"http://sdw2107:9990/HubCiti2.5/band/getbandlist"];
    NSLog(@"parameter: %@", parameter);
    if ([defaults boolForKey:@"ViewMoreBand"])
    {
        NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameter];
        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
        id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        [HubCitiAppDelegate removeActivityIndicator];
        [self parseBandCatType:responseData];
    }
    
    else
    {
        
        HTTPClient *client = [[HTTPClient alloc] init];
        client.delegate = self;
        DLog(@"Url: %@",urlString);
        [client sendRequest : parameter : urlString];
        [HubCitiAppDelegate showActivityIndicator];
    }
    
}


#pragma response parsing
//-(void) parse_EventSearchData: (id) responseData{
//
//}

-(void) parseBandCatType:(id) responseData{
    dispatch_async(dispatch_get_main_queue(), ^{
        [HubCitiAppDelegate removeActivityIndicator];
    });

    
    bandCategories = [[BandCategories alloc] init];
    
    if(![defaults boolForKey:@"ViewMoreBand"]){
        // bandArray = [[NSMutableArray alloc] init];
        bandGrouingInfo = [[NSMutableArray alloc] init];
    }
    @try{
        [bandCategories setValuesForKeysWithDictionary:responseData];
    }
    @catch (NSException *exception) {
        // Do nothing
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
        return;
        
    }
    if([bandCategories.responseCode isEqualToString:@"10000"]){
        
        bottomButton = [bandCategories.bottomBtn intValue];
        if(bandCategories.nextPage)
            nextPage = [bandCategories.nextPage intValue];
        
        if(bandCategories.maxRowNum)
            lastRecord = bandCategories.maxRowNum;
        if([segment selectedSegmentIndex] ==1){
            if(!searchResultBand){
                lowerLimit =lastRecord;
                nextBefore = nextPage;
            }
        }
        else{
            if(!searchResultBandName){
                lowerLimit =lastRecord;
                nextBefore = nextPage;
            }
        }
        
        if(bandCategories.mainMenuId)
            [defaults setObject:bandCategories.mainMenuId forKey:KEY_MAINMENUID];
        
        if(bandCategories.catList){
            for(int i = 0;i < bandCategories.catList.count; i++)
            {
                
                BOOL isCatRepeated = NO;
                
                
                BandGrouping *bandCategoriesDo = [[BandGrouping alloc] init];
                bandCategoriesDo.bandArray = [[NSMutableArray alloc] init];
                
                NSDictionary *dictList = bandCategories.catList[i];
                [bandCategoriesDo setValuesForKeysWithDictionary:dictList];
                
                if(bandGrouingInfo.count >0){
                    
                    BandGrouping *bandCategoriesRepetedDo = [[BandGrouping alloc] init];
                    for(int i=0;i<[bandGrouingInfo count]; i++)
                    {
                        bandCategoriesRepetedDo = [bandGrouingInfo objectAtIndex:i];
                        isCatRepeated = NO;
                        
                        if([bandCategoriesRepetedDo.groupContent isEqualToString:bandCategoriesDo.groupContent]){
                            
                            isCatRepeated = YES;
                            [bandCategoriesDo.bandArray addObjectsFromArray:bandCategoriesRepetedDo.bandArray];
                            [bandGrouingInfo replaceObjectAtIndex:i withObject:bandCategoriesDo];
                            break;
                            
                        }
                    }
                    if(isCatRepeated == NO){
                        
                        [bandGrouingInfo addObject:bandCategoriesDo];
                    }
                }
                else{
                    [bandGrouingInfo addObject:bandCategoriesDo];
                }
                
                if(bandCategoriesDo.retDetList){
                    
                    for(int i=0;i<bandCategoriesDo.retDetList.count;i++){
                        
                        BandCatList *batCatList = [[BandCatList alloc] init];
                        NSDictionary *dictList = bandCategoriesDo.retDetList[i];
                        [batCatList setValuesForKeysWithDictionary:dictList];
                        [bandCategoriesDo.bandArray addObject:batCatList];
                        
                    }
                    
                }
            }
            
        }
        
        if(bottomButton==1)
        {
            if (bandCategories.bottomBtnList)
            {
                arrEventBottomButtonDO =[[NSMutableArray alloc]init];
                for(int i=0;i<bandCategories.bottomBtnList.count;i++)
                {
                    bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                    NSDictionary* dictList = bandCategories.bottomBtnList[i];
                    [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                    
                    
                    if(!obj_findBottomDO.bottomBtnName)
                    {
                        obj_findBottomDO.bottomBtnName = @" ";
                    }
                    
                    if(!obj_findBottomDO.bottomBtnImg)
                    {
                        
                        obj_findBottomDO.bottomBtnImg = @" ";
                    }
                    
                    if(!obj_findBottomDO.bottomBtnImgOff)
                    {
                        
                        obj_findBottomDO.bottomBtnImgOff = @" ";
                    }
                    
                    
                    if(obj_findBottomDO.btnLinkID){
                        if(obj_findBottomDO.btnLinkTypeName){
                            if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"]) //for filters
                                [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                        }
                    }
                    
                    [arrEventBottomButtonDO addObject:obj_findBottomDO];
                }
                if(![defaults boolForKey:@"ViewMoreBand"]){
                if([arrEventBottomButtonDO count] > 0)
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self setBottomBarMenu];
                    });
                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                    
                }
                else
                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
                }
            }
            
        }
        
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
            if([segment selectedSegmentIndex] == 0){
                if(DEVICE_TYPE == UIUserInterfaceIdiomPad)
                {
                    UIView *viewToRemove = [self.view viewWithTag:26];
                    [viewToRemove removeFromSuperview];
                }
                if(!searchResultBandName){
                    [self.buttonEventView1 removeFromSuperview];
                    [self.buttonEventView2 removeFromSuperview];
                    [self.buttonEventView3 removeFromSuperview];
                    [self.buttonEventView4 removeFromSuperview];
                }
            }
            else{
                if(DEVICE_TYPE == UIUserInterfaceIdiomPad)
                {
                    UIView *viewToRemove = [self.view viewWithTag:16];
                    [viewToRemove removeFromSuperview];
                }
                if(!searchResultBand){
                    [self.buttonView1 removeFromSuperview];
                    [self.buttonView2 removeFromSuperview];
                    [self.buttonView3 removeFromSuperview];
                    [self.buttonView4 removeFromSuperview];
                }
                
            }
            });
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self settableViewOnScreen];
        });

    }
    else if ([bandCategories.responseCode isEqualToString:@"10005"]){
        nextPage =0;
        if(bandCategories.mainMenuId)
            [defaults setObject:bandCategories.mainMenuId forKey:KEY_MAINMENUID];
        bottomButton = [bandCategories.bottomBtn intValue];
        
        if(bottomButton ==1){
            {
                if (bandCategories.bottomBtnList)
                {
                    arrEventBottomButtonDO =[[NSMutableArray alloc]init];
                    for(int i=0;i<bandCategories.bottomBtnList.count;i++)
                    {
                        bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                        NSDictionary* dictList = bandCategories.bottomBtnList[i];
                        [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                        
                        
                        if(obj_findBottomDO.bottomBtnName)
                        {
                            
                        }
                        else
                        {
                            obj_findBottomDO.bottomBtnName = @" ";
                        }
                        
                        if(obj_findBottomDO.bottomBtnImg)
                        {
                            
                        }
                        else
                        {
                            obj_findBottomDO.bottomBtnImg = @" ";
                        }
                        
                        if(obj_findBottomDO.bottomBtnImgOff)
                        {
                            
                        }
                        else
                        {
                            obj_findBottomDO.bottomBtnImgOff = @" ";
                        }
                        
                        
                        if(obj_findBottomDO.btnLinkID){
                            if(obj_findBottomDO.btnLinkTypeName){
                                if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"]) //for filters
                                    [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                            }
                        }
                        
                        [arrEventBottomButtonDO addObject:obj_findBottomDO];
                    }
                    if(![defaults boolForKey:@"ViewMoreBand"]){
                    if([arrEventBottomButtonDO count] > 0)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self setBottomBarMenu];
                        });
                        
                        [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                        
                    }
                    else
                        [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
                    }
                }
            }
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
            if([segment selectedSegmentIndex] == 0){
                if(DEVICE_TYPE == UIUserInterfaceIdiomPad)
                {
                    UIView *viewToRemove = [self.view viewWithTag:26];
                    [viewToRemove removeFromSuperview];
                }
                if(!searchResultBandName){
                    [self.buttonEventView1 removeFromSuperview];
                    [self.buttonEventView2 removeFromSuperview];
                    [self.buttonEventView3 removeFromSuperview];
                    [self.buttonEventView4 removeFromSuperview];
                }
            }
            else{
                if(DEVICE_TYPE == UIUserInterfaceIdiomPad)
                {
                    UIView *viewToRemove = [self.view viewWithTag:16];
                    [viewToRemove removeFromSuperview];
                }
                if(!searchResultBand){
                    [self.buttonView1 removeFromSuperview];
                    [self.buttonView2 removeFromSuperview];
                    [self.buttonView3 removeFromSuperview];
                    [self.buttonView4 removeFromSuperview];
                }
                
            }
            
            });
        }
        
        if(!smartSearchBand){
            
            NSString *responseTextStr;
            if (bandCategories.responseText != nil) {
                responseTextStr = bandCategories.responseText;
            }
            else
                responseTextStr = @"No Records Found";
            
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self settableViewOnScreen];
        });

    }
    else{
        nextPage=0;
        if(!smartSearchBand){
            if (bandCategories.responseText != nil) {
                [UtilityManager showAlert:nil msg:bandCategories.responseText];
                
                
                [loading stopAnimating];
                
                
                
            }
            else
            {
                [UtilityManager showAlert:nil msg:@"No Records found"];
              
                [loading stopAnimating];
                
                
            }
        }
         dispatch_async(dispatch_get_main_queue(), ^{
        [self settableViewOnScreen];
         });
    }
   
    dispatch_async(dispatch_get_main_queue(), ^{
        if(!smartSearchBand)
            [searchBands resignFirstResponder];
        [bandTableView reloadData];
        if(![defaults boolForKey:@"ViewMoreBand"]){
            [bandTableView setContentOffset:CGPointZero animated:NO];
        }
        [defaults setBool:NO forKey:@"ViewMoreBand"];
        
    });
}


#pragma mark searchbar delegate methods

-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    bandTableView.scrollEnabled = NO;
    bandTableView.userInteractionEnabled = NO;
    
    switch ([segment selectedSegmentIndex]) {
        case 0:
        {
            if (![searchText length]){ //when search bar text is cleared
                if (timer){
                    [timer invalidate];
                    timer = nil;
                }
                // searchResultBand = nil;
                bandTableView.scrollEnabled = YES;
                bandTableView.userInteractionEnabled = YES;
                // bandSearch = NO;
                smartSearchBand = NO;
                searchResultBandName = nil;
                sortObj = [[SortAndFilter alloc] init];
                sortObj.alphabeticallySelected = YES;
                [searchBar resignFirstResponder];
                [self requestForBandList:nil :nil];
                
                
            }
            else if([searchBar.text length] >= 3)
            {
                if (![timer isValid]){
                    smartSearchBand = YES;
                    searchResultBandName = [searchBar.text copy];
                    timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(smartSearch) userInfo:nil repeats:NO];
                }
            }
            
        }
            break;
        case 1:
        {
            if (![searchText length]){ //when search bar text is cleared
                if (timer){
                    [timer invalidate];
                    timer = nil;
                }
                // searchResultBand = nil;
                bandTableView.scrollEnabled = YES;
                bandTableView.userInteractionEnabled = YES;
                // bandSearch = NO;
                smartSearchBand = NO;
                searchResultBand = nil;
                sortObj = [[SortAndFilter alloc] init];
                sortObj.alphabeticallySelected = YES;
                [searchBar resignFirstResponder];
                [self requestForBandList:nil :nil];
                
                
            }
            else if([searchBar.text length] >= 3)
            {
                if (![timer isValid]){
                    smartSearchBand = YES;
                    searchResultBand = [searchBar.text copy];
                    timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(smartSearch) userInfo:nil repeats:NO];
                }
            }
            
        }
            break;
    }
}

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    
    
    if (timer){
        [timer invalidate];
        timer = nil;
    }
    
    switch ([segment selectedSegmentIndex]) {
        case 0:{
            bandSearch = YES;
            searchResultBandName = [searchBar.text copy];
            lastRecord = 0;
            smartSearchBand = NO;
            [searchBar resignFirstResponder];
            [self smartSearch];
            
        }
            break;
        case 1:{
            bandSearch = YES;
            searchResultBand = [searchBar.text copy];
            lastRecord = 0;
            smartSearchBand = NO;
            [searchBar resignFirstResponder];
            [self smartSearch];
        }
            break;
        default:
            break;
    }
    
    
}
-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar resignFirstResponder];
    
    if (timer){
        [timer invalidate];
        timer = nil;
    }
    searchBar.text = @"";
    switch ([segment selectedSegmentIndex]) {
            
        case 0:{
            bandSearch = NO;
            searchResultBandName = nil;
            lastRecord = 0;
            bandTableView.scrollEnabled = YES;
            bandTableView.userInteractionEnabled = YES;
            smartSearchBand = NO;
            sortObj = [[SortAndFilter alloc] init];
            sortObj.alphabeticallySelected = YES;
            [searchBar resignFirstResponder];
            [self requestForBandList:nil :nil];
            
        }
            break;
        case 1:{
            bandSearch = NO;
            searchResultBand = nil;
            //            bandGrouingInfo = [[NSMutableArray alloc] init];
            //            bandGrouingInfo = bandDefaultResult;
            //            lastRecord =lowerLimit;
            //            nextPage = nextBefore;
            sortObj = [[SortAndFilter alloc] init];
            sortObj.alphabeticallySelected = YES;
            lastRecord = 0;
            bandTableView.scrollEnabled = YES;
            bandTableView.userInteractionEnabled = YES;
            smartSearchBand = NO;
            [searchBar resignFirstResponder];
            [self requestForBandList:nil :nil];
            
        }
            break;
        default: break;
    }
}


// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
            
        case BAND_CAT_TYPE:
            [self parseBandCatType:response];
            break;
            //        case BANDS_EVENTS_INFO:
            //            [self parse_EventSearchData:response];
            //            break;
        case PARTNERRET:
            [self parse_PartnerRet:response];
            break;
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
            
            
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
            
        default:
            break;
            
            
            
    }
    
}

#pragma bottombutton functionality

-(void)bottomButtonPressed:(id)sender
{
    if([defaults boolForKey:@"ViewMore"])
        return;
    [defaults setBool:NO forKey:@"ViewMoreBand"];
    [defaults setBool:YES forKey:BottomButton];
    //   [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    //NSlog(@"The tag is %d",tag);
    bottomButtonDO *obj_eventBottomDO = [arrEventBottomButtonDO objectAtIndex:tag];
    
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=27||[[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=28){
        [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    }
    [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_eventBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_eventBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_eventBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_eventBottomDO.btnLinkTypeName);
    if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    
    else{ //other menu types
        switch ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                //[hotDeals release];
                break;
                
            case 1://Scan Now
            {
                
            }
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
                }
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                
                [self navigateToFindView];
            }
                
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
            {
                
            }
                
                break;
                
            case 8://Visitors Bureau
            {
                
            }
                
                break;
                
            case 9://transportation
            {
                
            }
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                                  //[alert release];
                    
                     [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                     [NewsUtility signupPopUp:self];                   //[alert release];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {
                    [self shareClicked:nil];
                }
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                     [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=FALSE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_eventBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                  
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    // CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];;
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                //   [dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }
                break;
            case 22:
            {
                if (userIdentifier==TRUE) {
                     [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }
                break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                     [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                     [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                       
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        //[inform release];
                    }
                    
                }
                
            }break;
            case 25:
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                         [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                             [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            //  [citPref release];
                        }
                    }
                }
                
            }break;
            case 27:
            case 28://sort/filter
            {
                if([segment selectedSegmentIndex]==0|| [segment selectedSegmentIndex] == 1){
                    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
                    
                    // [defaults setValue:[defaults valueForKey:@"eventsMitemId"] forKey:KEY_MITEMID];
                    
                    SwipesViewController *iSwipeViewController1 = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                    iSwipeViewController1.module = @"Band";//Find All , Find Single , Events, CitiEXP
                    if([segment selectedSegmentIndex] ==1)
                        iSwipeViewController1.srchKeyBand = searchResultBand;
                    else
                        iSwipeViewController1.srchKeyBand = searchResultBandName;
                    
                    iSwipeViewController1.sortFilObj = [[SortAndFilter alloc]initWithObject:sortObj];
                    
                    
                    [self.navigationController pushViewController:iSwipeViewController1 animated:NO];
                }
            }
            break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
               // privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
            default:
                break;
        }
    }
    
}


-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
}
-(void)parseGetUserData:(id)response
{
    
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}

-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
    
        
        isRemember = YES;
        
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}



-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}


-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}


-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}

//Call Find Service and Class
-(void)navigateToFindView
{
    
    //    [defaults setValue:@"0" forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    
    navigatedFromMainMenu = FALSE;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}
-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}


-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=FALSE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    else if ([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}

#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    if (userIdentifier==TRUE) {
         [NewsUtility signupPopUp:self];
        //[alert release];
    }
    else
    {
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
    }
}
-(void) faceBookCliked {
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
    
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:@"Oops, SMS not supported.." msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
        
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}

-(void) showActionSheet {
   
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}



-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
           
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
           
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            
           
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}



//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
       
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];                    //[alert release];
                    
                    //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
       
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}


-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  EventDetailMapViewController.m
//  HubCiti
//
//  Created by service on 4/14/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
// Ref:CitiExperienceMapViewController

#import "EventDetailMapViewController.h"
#import "DisplayMap.h"
#import "MainMenuViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@class DisplayMap;

@interface EventDetailMapViewController ()
{
    NSMutableArray* annArray,*latArray,*longArray;
    UIButton* fullImageIcon;
    BOOL isFullMap;
    UIView * backgroundView;
    UIView* shadowView;
    UIButton* moreInfoButton;
    CustomizedNavController *cusNav;
    
}

@end

@implementation EventDetailMapViewController
@synthesize mapView,locationManager,eventDo;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;

    
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    //label to display screen title
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:eventDo.bandName forView:self withHambergur:NO];
    
    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    
    
    
    mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
  
    
    annArray = [[NSMutableArray alloc] init];
   
    latArray = [[NSMutableArray alloc] initWithObjects:eventDo.latitude, nil];
    longArray =  [[NSMutableArray alloc] initWithObjects:eventDo.longitude, nil];
    
    [self.view addSubview:mapView];
    
    [self setUpMapView];
    
    
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
}
-(void)returnToMainPage:(id)sender{
    
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(void)setUpMapView{
    
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    [mapView setDelegate:self];
   
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([[latArray objectAtIndex:0]doubleValue], [[longArray objectAtIndex:0]doubleValue]);


    //check is reqd as in case only scansee data is there and no gooPLe data it would crash
    if ([latArray count]){
       
        for(int i=0; i<[latArray count]; i++)
        {
            DisplayMap *annotation = [[DisplayMap alloc] init];
            annotation.coordinate = CLLocationCoordinate2DMake([[latArray objectAtIndex:i] doubleValue],
                                                        [[longArray objectAtIndex:i] doubleValue]);
            
            [annotation setTitle:eventDo.eventName ]; // [retailerNameArr objectAtIndex:i]
            [annotation setSubtitle:eventDo.address]; // [retailAddressArr objectAtIndex:i]

            [mapView addAnnotation:annotation];
            
            [annArray addObject:annotation];
        }
        
        MKCoordinateRegion region;
        MKCoordinateSpan span;
        
        span.latitudeDelta=0.01;  //or whatever zoom level
        span.longitudeDelta=0.01;
        
        region.span=span;
        region.center=location;

        [mapView setRegion:region animated:YES];
        [mapView regionThatFits:region];
    }
    else{
        
        return;
    }
  
 
    


}



-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    MKPinAnnotationView* pinView=nil;
    
    
    
    if ([annotation isKindOfClass:[DisplayMap class]])
    {
        // try to dequeue an existing pin view first
        static NSString* displayAnnotationIdentifier = @"displayAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:displayAnnotationIdentifier];
        
        if (!pinView)
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
                                                  initWithAnnotation:annotation reuseIdentifier:displayAnnotationIdentifier] ;
            //customPinView.pinColor = MKPinAnnotationColorPurple;
            customPinView.animatesDrop = YES;
            
            customPinView.canShowCallout = YES; // Pin Click will work , if it is yes
          
            
            // Info Details Button on pinView :
            
            /*UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            customPinView.rightCalloutAccessoryView = infoButton;*/
            
            return customPinView;
        }
        else
        {
           
            pinView.annotation = annotation;
        }
    }
    pinView.canShowCallout = YES;
   
    return pinView;
}


-(UILabel*) createLabel :  (CGRect) frame : (NSString*) text
{
    UILabel *titleLbl = [[UILabel alloc]initWithFrame:frame];
    titleLbl.backgroundColor = [UIColor clearColor];
    titleLbl.text = text;
    
    return titleLbl;
}


-(void)popBackToPreviousPage{
    
    
    [self.navigationController popViewControllerAnimated:NO];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

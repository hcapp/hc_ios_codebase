//
//  BandsNearByEventsMapTable.h
//  HubCiti
//
//  Created by Lakshmi H R on 4/14/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapView.h"
#import "MapKitDisplayViewController.h"
#import "BandEventsList.h"
#import "EventListDetails.h"
#import "EventDetailImageViewController.h"
#import "DWBubbleMenuButton.h"
#import "SwipesViewController.h"
#import "RetailerSummaryViewController.h"
//#import "SingleCatRetailers.h"
//#import "EventsListViewController.h"
//#import "DealHotDealsList.h"
//#import "AlertsList.h"
//#import "CityExperienceViewController.h"
//#import "AboutAndPrivacyScreen.h"
//#import "SettingsViewController.h"
//#import "FilterRetailersList.h"
//#import "FilterListViewController.h"
@class EventDetailImageViewController;
@class DealHotDealsList;
@class FilterRetailersList;
@class CityExperienceViewController;
@class FundraiserListViewController;
@class FilterListViewController;
@class EventsListViewController;
@class RetailersListViewController;
@class AnyViewController;
@class EmailShareViewController;
@class SingleCatRetailers;

@interface BandsNearByEventsMapTable : UIViewController<DWBubbleMenuViewDelegate,MFMessageComposeViewControllerDelegate,MKMapViewDelegate,CustomizedNavControllerDelegate >
{
    WebRequestState iWebRequestState;
    NSMutableArray *annArray,/**bandEventsArray,*/ *bandEventsListArray, *eventLatArray, *eventLongArray, *eventAddArray, *eventNameArray,*eventBandArray ;
    NSNumber *nextPage;
    NSNumber *lowerLimitValue;
    NSNumber *rowCountOfTotalCells;
    UIPinchGestureRecognizer *mapPinch;
    int selectedindexRow,selectedSection;
    RetailerSummaryViewController *rsvc;
    CommonUtility *common;
    CustomizedNavController *cusNav;
    
}
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,strong) EventsListViewController* iEventsListViewController;
@property(nonatomic,strong) FundraiserListViewController *iFundraiserListViewController;
@property(nonatomic,strong) FilterListViewController *filterList;
@property(nonatomic,strong) CityExperienceViewController *cityExp;
@property(nonatomic,strong) FilterRetailersList *filters;
@property(nonatomic,strong) DealHotDealsList* hotDeals;
@property(nonatomic,strong) RetailersListViewController* retailerListScreen;


@property(nonatomic,strong) BandRetailerSummaryViewController *bandSummaryPage;
@property(nonatomic,strong) EventDetailImageViewController *eventSummaryPage;
@property(nonatomic,strong) SortAndFilter *sortObj,*savedSortObj;
@property (strong, nonatomic) UITableView *bandEventsTable;
@property (strong,nonatomic) UIView *zoomBar;
@property (strong, nonatomic) MKMapView *bandMapView;
@property (strong, nonatomic) NSNumber *eventTypeID;
@property (strong,nonatomic) NSString *bandID;
@property (strong,nonatomic) NSString *eventTypeName;
@property (strong,nonatomic) BandEventsList *bandEventsResponse;
@property (strong,nonatomic) EventListDetails *eventListDetails;
@property (strong,nonatomic) NSString *bandTitle;
@property (strong,nonatomic) EventDetailImageViewController *eventImageDetail;
@property(nonatomic,retain) SortAndFilter *sortFilObj;
@property(nonatomic,assign) BOOL fromTodayEvents;

@end

//
//  BandRetailerSummaryViewController.h
//  HubCiti
//
//  Created by Nikitha on 4/14/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "BandsNearByEventsMapTable.h"
@class EmailShareViewController;
@class BandsNearByEventsMapTable;

@interface BandRetailerSummaryViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,CustomizedNavControllerDelegate,signUpDelegate>
{
    
    SdImageView *bannerAdImageView;
    NSMutableDictionary * bandRetSummary;
    UIButton *callStore;
    UIViewController * splashPage;
    SdImageView *splashImageView;
    UIImageView * tableView_image;
    NSMutableArray *titleArray;
    NSMutableArray *indexArray;
    UILabel * bandName;
    NSMutableArray * keyArray;
    CustomizedNavController *cusNav;
  
}
@property(nonatomic,strong) NSString * retailerBandId;
@property(nonatomic,strong) UITableView *bandListTableView;
@property(nonatomic,strong)  NSMutableArray * bandData;
@property(nonatomic,strong)  NSMutableArray * bandDetail;
@property(nonatomic,strong) NSMutableArray * tblViewImageArray;
@property (nonatomic, strong) NSMutableDictionary *bandRetSummary;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong)NSString *retBandName;
@property(nonatomic,strong)BandsNearByEventsMapTable *eventsMapList;
@property(nonatomic,strong) AnyViewController * anyVC;
@end

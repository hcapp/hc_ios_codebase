//
//  BandRetailerSummaryViewController.m
//  HubCiti
//
//  Created by Nikitha on 4/14/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "BandRetailerSummaryViewController.h"
#import "MainMenuViewController.h"
#import "FindOptionsViewController.h"
#import "AppDelegate.h"
#import "WebBrowserViewController.h"
#import "HubCitiConstants.h"
#import "FindNearByRetailerMap.h"
#import "EmailShareViewController.h"
#import "SpecialOffersViewController.h"
#import "EventsListViewController.h"
#import "BandRetailerSummaryResponse.h"
#import "BandsNearByEventsMapTable.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@interface BandRetailerSummaryViewController () <HTTPClientDelegate,DWBubbleMenuViewDelegate>
@property(nonatomic,retain) BandRetailerSummaryResponse * bandRetResponse ;

@end

@implementation BandRetailerSummaryViewController
@synthesize bandData,bandListTableView,bandDetail,anyVC,emailSendingVC;
@synthesize bandRetSummary, tblViewImageArray,bandRetResponse,retailerBandId,retBandName,eventsMapList;


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
   
}


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    [self parse_bandRetSummary:responseObject];
    
  
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        if (bandRetSummary == nil) {//assigning number to each weekday - in order to
            bandRetSummary = [[NSMutableDictionary alloc]init];
            
            [bandRetSummary setObject:[NSNumber numberWithInt:0] forKey:@"bandURL"];
            [bandRetSummary setObject:[NSNumber numberWithInt:1] forKey:@"contactPhone"];
            [bandRetSummary setObject:[NSNumber numberWithInt:2] forKey:@"mail"];
           [bandRetSummary setObject:[NSNumber numberWithInt:3] forKey:@"Events"];
            [bandRetSummary setObject:[NSNumber numberWithInt:4] forKey:@"AnythingPage"];
            
            
        }
        
    }
    return self;
}



- (void)viewDidLoad {
    
    [super viewDidLoad];
  
   
     float imageHeight =  ((float)432 / (float)768) * SCREEN_WIDTH;
    self.bandListTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, imageHeight, SCREEN_WIDTH,SCREEN_HEIGHT-imageHeight) style:UITableViewStylePlain];
    NSLog(@"screen height %f", SCREEN_HEIGHT);
    self.bandListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.bandListTableView.dataSource = self;
    self.bandListTableView.delegate= self;
     bandListTableView.scrollEnabled = YES;
    self.bandListTableView.contentInset = UIEdgeInsetsMake(0, 0,VARIABLE_HEIGHT(60), 0);

    
    
    
    //customize back button
  
    
    
    self.navigationItem.hidesBackButton = YES;
    
    //Customized home button
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    
    
    [self.view addSubview:bandListTableView];
        [self request_bandRetSummary];
    
    
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
}
- (void)removeBannerAd:(id)indexPath
{
    [splashImageView removeFromSuperview];
    [[self presentedViewController] dismissViewControllerAnimated:NO completion:nil];
    
}
-(IBAction)bannerAdClicked:(id)sender
{
    
    NSString *url = [defaults  objectForKey:@"ribbonAdURL"];
    [defaults  setObject:url forKey:KEY_URL];
    
    [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
    WebBrowserViewController *webView = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:webView animated:NO];
    
}
-(void) request_bandRetSummary
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
//    [parameters setValue:@"1" forKey:@"userId"];
//    [parameters setValue:@"1"forKey:@"bandID"];
  
   
    if ([defaults valueForKey:KEY_USERID])
    {
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if (retailerBandId)
    {
        [parameters setValue:retailerBandId forKey:@"bandID"];
    }
    if ([defaults valueForKey:KEY_MAINMENUID])
    {
        [parameters setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    }
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length] && ([[HubCitiManager sharedManager]gps_allow_flag] == YES) &&(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)))
    {
        [parameters setValue:[defaults  valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        [parameters setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
    
               if ([[defaults valueForKey:KEYZIPCODE] length])
               {
                   [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"postalCode"];
               }
    }
    else
    {
    if ([[defaults valueForKey:KEYZIPCODE] length])
        {
            [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"postalCode"];
        }
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@band/bandsummary",BASE_URL];
    DLog(@"Url: %@",urlString);
    [HubCitiAppDelegate showActivityIndicator];
    [client sendRequest : parameters : urlString];

}
-(void)popBackToPreviousPage{
    [linkID removeLastObject];
    [self.navigationController popViewControllerAnimated:NO];
}
-(void) parse_bandRetSummary :(id)response
{
    if (response == nil)
        return;
    if (bandRetResponse == nil) {
        bandRetResponse = [[BandRetailerSummaryResponse alloc] init];
    }
    [bandRetResponse setValuesForKeysWithDictionary:response];
    NSLog(@"response = %@", bandRetResponse);
     if ([bandRetResponse.responseCode isEqualToString:@"10000"] )
     {
         self.bandListTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//         if([defaults valueForKey : KEY_MAINMENUID])
//             [defaults setObject: forKey:<#(nonnull NSString *)#>]
         if (bandRetResponse.retailerDetail!=nil)
         {
             for (int i = 0; i< bandRetResponse.retailerDetail.count; i++)
             {
             RetailerDetailsResponse * bandRetDetails = [[RetailerDetailsResponse alloc]init];
             NSDictionary * dict_bandRetDetails = bandRetResponse.retailerDetail[i];
             [bandRetDetails setValuesForKeysWithDictionary:dict_bandRetDetails];
                
                 if (bandRetDetails.bandID!=nil)
                 {
                
                     [defaults setValue:bandRetDetails.bandID forKey:KEY_BANDID];
                 }
                 
                 if (bandRetDetails.bandName!=nil)
                 {
                     CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
                     [cus setTitle:bandRetDetails.bandName forView:self withHambergur:NO];
                     
                     
                     self.retBandName = bandRetDetails.bandName;
                 }
                if (bandRetDetails.ribbonAdImagePath!=nil && ![bandRetDetails.ribbonAdImagePath isEqualToString:@"N/A"])
                {
                    [defaults setObject:bandRetDetails.ribbonAdImagePath forKey:@"ribbonAdImagePath"];
                
                    float imageHeight =  ((float)432 / (float)768) * SCREEN_WIDTH;
                    self.bandListTableView.frame = CGRectMake(0, imageHeight, SCREEN_WIDTH,SCREEN_HEIGHT-imageHeight);
                    NSLog(@"screen height %f", SCREEN_HEIGHT);
                    self.bandListTableView.contentInset = UIEdgeInsetsMake(0, 0,VARIABLE_HEIGHT(60), 0);
                    
                    UIButton *bannerBtn ;
                    bannerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                    bannerBtn.frame = CGRectMake(0.0, 0.0,SCREEN_WIDTH,imageHeight);
                    bannerAdImageView.backgroundColor = [UIColor clearColor];
                    bannerAdImageView = [[SdImageView alloc]initWithFrame:bannerBtn.frame] ;
                    [bannerAdImageView loadImage:bandRetDetails.ribbonAdImagePath];
                    
                    [bannerBtn addSubview:bannerAdImageView];
                    bannerAdImageView.userInteractionEnabled = NO;
                    bannerBtn.userInteractionEnabled=YES;
                    [self.view addSubview:bannerBtn];
                    
                    if (bandRetDetails.ribbonAdURL!=nil && ![bandRetDetails.ribbonAdURL isEqualToString:@"N/A"])
                    {
                        [defaults setValue:bandRetDetails.ribbonAdURL forKey:KEY_URL];
                        [defaults setObject:bandRetDetails.ribbonAdURL  forKey:@"ribbonAdURL"];
                        [bannerBtn addTarget:self action:@selector(bannerAdClicked:) forControlEvents:UIControlEventTouchUpInside];
                    }

                 }
                 
                 else if(bandRetDetails.ribbonAdImagePath == nil || [bandRetDetails.ribbonAdImagePath isEqualToString:@"N/A"])
                 {
                   
                     [defaults  setObject:bandRetDetails.ribbonAdImagePath forKey:@"ribbonAdImagePath"];
                     self.bandListTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                     NSLog(@"screen height %f", SCREEN_HEIGHT);
                     self.bandListTableView.contentInset = UIEdgeInsetsMake(0, 0,VARIABLE_HEIGHT(60), 0);
                     
                     
                }
                 if ([bandRetDetails.bandImgPath length] && ![bandRetDetails.bandImgPath isEqualToString:@"N/A"])
                 {
                    
                   
                     splashImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT+STATUSBAR_HEIGHT)];
                     splashImageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
                     splashImageView.contentMode = UIViewContentModeScaleAspectFit;
                     [splashImageView setBackgroundColor:[UIColor whiteColor]];
                     
                     splashPage = [[UIViewController alloc] init];
                     bandRetDetails.bandImgPath = [bandRetDetails.bandImgPath stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
                     NSURL *splashImageURL = [NSURL URLWithString:bandRetDetails.bandImgPath];
                     [defaults setURL:splashImageURL forKey:@"AdUrl"];
                     [splashImageView loadImage:bandRetDetails.bandImgPath];
                     [splashPage.view addSubview:splashImageView];
                     
                     if ([[splashPage.view subviews] count])
                     {
                         [self presentViewController:splashPage animated:NO completion:nil];
                         [self performSelector:@selector(removeBannerAd:) withObject:nil afterDelay:3.0];
                     }
                     
                 }
                 else
                     bandRetDetails.bandImgPath = @"";

                 keyArray = [[NSMutableArray alloc]init];
             titleArray = [[NSMutableArray alloc]init];
             indexArray = [[NSMutableArray alloc]init];
                 tblViewImageArray = [[NSMutableArray alloc]init];
            
                 if (bandRetDetails.contactPhone != nil && ![bandRetDetails.contactPhone isEqualToString:@"N/A"])
                 {
                     [keyArray addObject:@"contactPhone"];
                     [titleArray addObject:bandRetDetails.contactPhone];
                     [indexArray addObject:bandRetDetails.contactPhone];
                     [tblViewImageArray addObject:bandRetDetails.phoneImg];
                 }
                
                 if (bandRetDetails.bandURL != nil && ![bandRetDetails.bandURL isEqualToString:@"N/A"])
                 {
                     [keyArray addObject:@"bandURL"];
                     [titleArray addObject:bandRetDetails.bandURL];
                     [indexArray addObject:bandRetDetails.bandURL];
                     [tblViewImageArray addObject:bandRetDetails.bandURLImgPath];
                 }
                 if (bandRetDetails.mail != nil && ![bandRetDetails.mail isEqualToString:@"N/A"])
                 {
                     [keyArray addObject:@"mail"];
                     [titleArray addObject:bandRetDetails.mail];
                     [indexArray addObject:bandRetDetails.mail];
                     [tblViewImageArray addObject:bandRetDetails.mailImg];
                 }
         
             } //end of for loop
         
         if (bandRetResponse.retailerCreatedPageList != nil)
         {
             for (int i = 0; i< bandRetResponse.retailerCreatedPageList.count; i++)
             {
                 RetailerCreatedPageResponse * bandRetCreatedPage= [[RetailerCreatedPageResponse alloc]init];
                 NSDictionary * dict_bandRetCreatedPage = bandRetResponse.retailerCreatedPageList[i];
                 [bandRetCreatedPage setValuesForKeysWithDictionary:dict_bandRetCreatedPage];
                
                 if( [bandRetCreatedPage.pageLink isEqualToString:@"N/A"] && [bandRetResponse.eventExist integerValue] == 1)
                 {
                     [titleArray addObject:@"Shows"];
                     [indexArray addObject:bandRetCreatedPage.pageLink];
                     [keyArray addObject:@"Events"];
                     [tblViewImageArray addObject:bandRetCreatedPage.pageImage];
                     
                 }
                 else{
                      [titleArray addObject:bandRetCreatedPage.pageTitle];
                     [indexArray addObject:bandRetCreatedPage.pageLink];
                      [keyArray addObject:@"AnythingPage"];
                     [tblViewImageArray addObject:bandRetCreatedPage.pageImage];
                   // [anythingPageListId addObject:bandRetCreatedPage.anythingPageListId];
                     
                 }
                 
                 
         }
     }
     }
         else
         {
             if (bandRetResponse.responseText!=nil)
             {
                 [UtilityManager showAlert:nil msg:bandRetResponse.responseText];
                 
                
                 return;
             }

     }
    }
    
     else
     {
         self.bandListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
         [UtilityManager showAlert:nil msg:bandRetResponse.responseText];
         
     }
        [bandListTableView reloadData];
        [self shareBubbleButton];
    

}

//Share button at bottom

-(void) shareBubbleButton
{
    UIImageView * floatingShareButton;
    floatingShareButton = [self createFloatingShareButtonView];
    DWBubbleMenuButton * bubbleBandShare = [[DWBubbleMenuButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width - floatingShareButton.frame.size.width - 10.f, self.view.frame.size.height - floatingShareButton.frame.size.height - 20.f, floatingShareButton.frame.size.width, floatingShareButton.frame.size.height) expansionDirection:DirectionUp];
    bubbleBandShare.homeButtonView = floatingShareButton;
    bubbleBandShare.delegate = self;
    [bubbleBandShare addButtons:[self createShareButtonArray]];
    [self.view addSubview:bubbleBandShare];
    
    
    bubbleBandShare.onLogOutPressed =^(){
        
        [self logOutPressed];
    } ;
    
}
-(void) logOutPressed
{
    
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}


-(UIImageView*) createFloatingShareButtonView
{
    UIImageView * label = nil;
    if(DEVICE_TYPE == UIUserInterfaceIdiomPhone)
    {
        label = [[UIImageView alloc]initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else
    {
        label = [[UIImageView alloc]initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    label.layer.cornerRadius = label.frame.size.height /2.f;
    label.backgroundColor = [UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    return label;
    
}

-(NSArray *) createShareButtonArray
{
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    
    int i = 0;
    for (NSString *title in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:title] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        
        button.clipsToBounds = YES;
        button.tag = i++;
        
        [button addTarget:self action:@selector(testband:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
    
}
- (void)testband:(UIButton *)sender
{
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
    [self shareClicked];
    
    switch (sender.tag) {
        case 0:{
            //facebook
            anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
            anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"BAND_SHARE"];
            [UIView transitionWithView:self.view duration:0.5
                               options:UIViewAnimationOptionTransitionCurlDown
                            animations:^ { [self.view addSubview:anyVC.view]; }
                            completion:nil];
        }
            break;
        case 1:{
            //twitter
            
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    if (result == SLComposeViewControllerResultCancelled) {
                        DLog(@"Twitter Result: canceled");
                    } else {
                        DLog(@"Twitter Result: sent");
                        NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><bandId>%@</bandId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_BANDID]];
                        
                        [NSThread detachNewThreadSelector:@selector(userTrack:) toTarget:self withObject:userXmlStr];
                    }
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        
                        [controller.view removeFromSuperview];
                    } else
                        [controller dismissViewControllerAnimated:YES completion:Nil];
                };
                controller.completionHandler =myBlock;
                
                //Adding the Text to the facebook post value from iOS
                [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                
                [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[defaults valueForKey:KEY_PRODUCTIMGPATH] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]]]]];
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    [self.view addSubview:controller.view];
                    [self.view bringSubviewToFront:controller.view];
                }else
                    [self presentViewController:controller animated:YES completion:Nil];
                
                
            }
            else {
                [UtilityManager showAlert:@"Cannot Share" msg:@"Please Go To Settings and Set Up A Twitter Account"];
                
                
            }
        }
            
            
            break;
        case 2:{
            
            //text
            
            if ([self hasCellularCoverage])
            {
                if ([MFMessageComposeViewController canSendText]) {
                    
                    MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
                    [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    smsComposerController.messageComposeDelegate = self;
                    [self presentViewController:smsComposerController animated:YES completion:nil];
                }
            }
            else
            {
                [UtilityManager showAlert:@"Oops, SMS not supported.." msg:@"Looks like the sim card is missing or the device does not support texting"];
                
            }
            
            
        }
            break;
        case 3:{
            
            //email
            
            emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
            emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"BAND_SHARE"];
            __typeof(self) __weak  obj = self;
            emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                
                [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
            } ;
            
            [emailSendingVC loadMail];
        }
            break;
        default:
            [defaults setBool:NO forKey:BottomButton];
            break;
    }
    
}


-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode)
    {
        return NO;
    }
    return YES;
}


-(void)shareClicked
{
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    [reqStr appendFormat:@"<ShareProductInfo><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [reqStr appendFormat:@"<bandId>%@</bandId><platform>IOS</platform>",[defaults valueForKey:KEY_BANDID]];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/shareband",BASE_URL];
    
    DLog(@"%@",urlString);
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    
    [self parseProductShare:response];
    
    
}

-(void)parseProductShare:(NSString*)response
{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000)
    {
        TBXMLElement *shareText=[TBXML childElementNamed:@"shareText" parentElement:tbXml.rootXMLElement];
        TBXMLElement *emailURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *imagePath = [TBXML childElementNamed:@"retImage" parentElement:tbXml.rootXMLElement];
        
        if (emailURL!=nil) {
            [defaults setValue:[TBXML textForElement:emailURL] forKey:KEY_HYPERLINK];
            NSLog(@"The link to be shared is %@",KEY_HYPERLINK);
        }
        
        if (shareText!=nil) {
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [defaults setObject:shareString forKey:KEY_SHAREMSG];
            NSLog(@"The share message value is %@",[defaults valueForKey:KEY_SHAREMSG]);
        }
        
        if (imagePath!=nil) {
            [defaults setObject:[TBXML textForElement:imagePath] forKey:KEY_PRODUCTIMGPATH];
            NSLog(@"The product image path is %@",KEY_PRODUCTIMGPATH);
        }
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
                       //[alert release];
            return;
        }
        
    }
}
- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView{
    
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}


- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
            
            NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><bandId>%@</bandId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_BANDID]];
            
            [NSThread detachNewThreadSelector:@selector(userTrack:) toTarget:self withObject:userXmlStr];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
           
            //[alert release];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)userTrack:(NSString*)xmlStr
{
    
    NSMutableString *urlStr = [[NSMutableString alloc]init];
    [urlStr appendFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    DLog(@"%@",urlStr);
    NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:xmlStr]];
    DLog(@"%@",response);
    //[xmlStr release];
}
//mainpage
-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 50.0;
    }
    else
    {
        return 80.0;
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return titleArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell ;
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    
    //cell.selectionStyle = UITableViewCellSelectionStyleBlue;
   // cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
     cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"right_arrow.png"]];
    cell.backgroundColor = [UIColor whiteColor];

  // table cell label
    UILabel *textLabel ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        textLabel = [[UILabel alloc]initWithFrame:CGRectMake(50.0, 15.0, 200.0, 20.0)];
        textLabel.font = [UIFont systemFontOfSize:14.0f];
    }
    else
    {
        textLabel = [[UILabel alloc]initWithFrame:CGRectMake(80.0, 15.0, 600.0, 40.0)];
        textLabel.font = [UIFont systemFontOfSize:17.0f];
    }
    textLabel.backgroundColor = [UIColor clearColor];
    
    textLabel.textColor = [UIColor blackColor];
    
    textLabel.text = [titleArray objectAtIndex:indexPath.row];
    [cell.contentView addSubview:textLabel];
    
    
    SdImageView *asyncImageView;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(5, 10, 30, 30)];
    }
    else
    {
        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(5, 12, 60, 60)];
    }
    asyncImageView.backgroundColor = [UIColor clearColor];
    [asyncImageView loadImage:[tblViewImageArray objectAtIndex:indexPath.row]];
    [cell.contentView addSubview:asyncImageView];
    
    return cell;
    
}


-(BOOL)locationServicesOn
{
    if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorized) || ([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) || ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse))
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld",[[bandRetSummary objectForKey:[keyArray objectAtIndex:indexPath.row]] integerValue]);
    switch ([[bandRetSummary objectForKey:[keyArray objectAtIndex:indexPath.row]] integerValue])
    {
        case 0:
        {
           
            
            [defaults  setObject:[indexArray objectAtIndex:indexPath.row] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            
        }
            break;
            
        case 1:
        {
            

            NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",[titleArray objectAtIndex:indexPath.row]]];
            
            if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
                [[UIApplication sharedApplication] openURL:phoneUrl];
            }
            else
            {
                [UtilityManager showAlert:@"Alert" msg:@"Call facility is not available!!!"];
              
            }

            
        }
            break;
        case 2 :
        {//email
            
            
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            [[mc navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
//            NSString* email = [indexArray objectAtIndex:indexPath.row];
//            NSString *combinedEmail = [email stringByReplacingOccurrencesOfString:@"," withString:@";"];
            NSArray* emailArray = [[indexArray objectAtIndex:indexPath.row] componentsSeparatedByString: @","];
            NSLog(@"Array email %@",emailArray);
  
           
            [mc setToRecipients:emailArray];
            
            // Present mail view controller on screen
            [self presentViewController:mc animated:YES completion:NULL];
        }
            break;
        case 3 :
        {
            
            
            BandsNearByEventsMapTable *iEventsListViewController = [[BandsNearByEventsMapTable alloc]initWithNibName:@"BandsNearByEventsMapTable" bundle:[NSBundle mainBundle]];
            self.eventsMapList = iEventsListViewController;
            iEventsListViewController.bandID = retailerBandId;
            iEventsListViewController.bandTitle = self.retBandName;
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
            
            
        }
            break;
            
        case 4 :{
            
            
            NSURL* url = [NSURL URLWithString:[indexArray objectAtIndex:indexPath.row]];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if(![[url absoluteString] containsString:@"3000.htm"])
            {
                
                [defaults  setObject:[indexArray objectAtIndex:indexPath.row] forKey:KEY_URL];
                
                WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                // [self.view addSubview:self.view.window.rootViewController.view];
                // [self.view.window.rootViewController.view setHidden:YES];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
                
                
            }
            else{
                
                [defaults setValue:[indexArray objectAtIndex:indexPath.row] forKey:KEY_URL];
                
                
                BOOL lsFlag = [self locationServicesOn];
                if (!lsFlag) {
                    UIAlertController * alert;
                    
                        alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                   
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                              [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                                      //[alert release];
                    
                    //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
                }
                else{
                    
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                   
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            
        }
            
            break;
            
    }
    [bandListTableView deselectRowAtIndexPath:[bandListTableView indexPathForSelectedRow] animated:YES];
}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end

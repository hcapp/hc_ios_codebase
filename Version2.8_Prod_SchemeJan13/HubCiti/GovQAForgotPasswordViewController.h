//
//  GovQAForgotPasswordViewController.h
//  HubCiti
//
//  Created by Ashika on 8/19/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "CommonUtility.h"
@class AnyViewController;
@class EmailShareViewController;
@interface GovQAForgotPasswordViewController : UIViewController<UITextFieldDelegate,MFMessageComposeViewControllerDelegate, UIWebViewDelegate,CustomizedNavControllerDelegate>{
    WebRequestState iWebRequestState;
    CommonUtility *common;
    BOOL isRemember;
    UILabel *emailLabel,*mandatoryField;
    UITextField *txtField_emailId;
    UIButton *submitButton,*cancelButton;
    NSMutableArray *arrBottomButtonDO;
    NSMutableArray *arrBottomButtonViewContainer;
    UIActivityIndicatorView *activityIndicator;
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic, strong)UITextField *txtField_emailId;
@property (nonatomic,strong) NSMutableArray *arrBottomButtonDO;
@property (nonatomic,strong) UIActivityIndicatorView *activityIndicator;

//-(UILabel *)createLabel:(NSString*)title rect:(CGRect)rect;
//- (UITextField *)createTxtField:(NSString*)placeHolder rect:(CGRect)rect;
@end

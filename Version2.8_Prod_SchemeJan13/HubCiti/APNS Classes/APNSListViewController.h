//
//  APNSListViewController.h
//  HubCiti
//
//  Created by Service on 1/6/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface APNSListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,CustomizedNavControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *notificationListView;
@property(strong,nonatomic) NSMutableArray *rssFeedListDOArr;
//@property(retain,nonatomic) NSMutableArray *pushNoteList;
@end
@interface RssFeedListDO : NSObject {
    
    NSString *title;
    NSString * dealId;
    NSString *dealName;
    NSString *dealDesc;
    NSString *type;
    NSString *splUrl;
    NSString *link;
    NSString *retailerId;
    NSString *retailLocationId;
 
}

@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString * dealId;
@property (nonatomic,strong) NSString *dealName;
@property (nonatomic,strong) NSString *dealDesc;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSString *splUrl;
@property (nonatomic,strong) NSString *link;
@property (nonatomic,strong) NSString *retailerId;
@property (nonatomic,strong) NSString *endDate;
@property (nonatomic,strong) NSString *endTime;
@property (nonatomic,strong) NSString *timeStamp;

@property (nonatomic,strong) NSString *retailLocationId;
@property (nonatomic , strong) NSString* pushFlag;

@end
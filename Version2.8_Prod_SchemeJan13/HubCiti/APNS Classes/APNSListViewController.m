//
//  APNSListViewController.m
//  HubCiti
//
//  Created by Service on 1/6/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "APNSListViewController.h"
#import "AppDelegate.h"
#import "WebBrowserViewController.h"
//#import "HotDealDetails.h"
#import "HotDealDetailViewController.h"
#import "NewCouponDetailViewController.h"
#import "RetailerSpecialOffersViewController.h"
#import "HTTPClient.h"
#import "NativeSpecialOfferViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "SideViewController.h"

@implementation RssFeedListDO
@synthesize title;
@synthesize dealDesc,dealId,dealName,type,splUrl,link, retailerId, retailLocationId,pushFlag,timeStamp,endDate,endTime;
@end

@interface APNSListViewController () <HTTPClientDelegate>

@end

@implementation APNSListViewController{
    BOOL expiredDeal;
}
@synthesize rssFeedListDOArr;


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    self.navigationItem.hidesBackButton=YES;
    [self.notificationListView setDelegate:self];
    [self.notificationListView setDataSource:self];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"Notification List";
    [defaults setBool:NO forKey:@"canDisplayAPNSListScreen"];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30,30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    
    // //[mainPage release];
   
    self.notificationListView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if ([defaults boolForKey:@"fromLogin"]){
        rssFeedListDOArr = [[NSMutableArray alloc]init];
        //DLog(@"pushNoteList %@", pushNoteList);
        [defaults setBool:YES forKey:@"LoginSuccess"];
        
        if ([defaults valueForKey:@"PushNotifyResponse"]) {
            
            NSMutableArray* notificationMsgList = [defaults valueForKey:@"PushNotifyResponse"];
            NSArray* uniqueArray = [self removeDuplicate : notificationMsgList];
            
            [self parsePushNotify:uniqueArray];
        }
    }
    
    
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    
    if ([defaults  boolForKey:@"hideBackBtn"] == YES){
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = nil;
        [cusNav hideBackButton:YES];
        [cusNav hideHambergerButton:YES];
        [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    }
    else{
        
        cusNav.customNavBardelegate = self;
        [cusNav hideHambergerButton:YES];
        [cusNav hideBackButton:NO];
        [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
        
    }
}

-(NSArray*) removeDuplicate : (NSMutableArray*) responseObj
{
    NSSet *set = [NSSet setWithArray:responseObj];
    NSArray *uniqueArray = [set allObjects];
    return uniqueArray;
}

#pragma mark navigation barButtonItem actions
-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"detailsPage"];
    
    if ([defaults boolForKey:@"fromLogin"]) {
        [defaults setBool:NO forKey:@"fromLogin"];
        if([defaults boolForKey:@"newsTemplateExist"])
        {

            if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
                [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
                
            }
            else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
                [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
            }
            else{
                [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
            }
            
            
        }
        else
        {
            
            [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        [self.navigationController popViewControllerAnimated:NO];
    }
    [defaults setBool:NO forKey:@"canDisplayAPNSListScreen"];
}



-(void)returnToMainPage:(id)sender{
    [defaults setBool:NO forKey:@"canDisplayAPNSListScreen"];
    [defaults setBool:NO forKey:@"detailsPage"];
    if ([defaults boolForKey:@"fromLogin"]) {
        [defaults setBool:NO forKey:@"fromLogin"];
    }
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
        
    }
    else
    {
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}



-(int) checkForExpiry : (NSString*) endDate : (NSString*) endTime
{
    NSString* dateString;
    if (endDate.length > 0  && endTime.length > 0) {
        
        dateString = [NSString stringWithFormat:@"%@ %@", endDate, endTime];
    }
    NSDateFormatter *datePickerFormat = [[NSDateFormatter alloc] init];
    [datePickerFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    datePickerFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    
    
    NSString *today = [datePickerFormat stringFromDate:[NSDate date]];
    NSDate *currentDate = [datePickerFormat dateFromString:today];
    NSDate *serverDate = [datePickerFormat dateFromString:dateString];
    
    NSComparisonResult result;
    
    result = [currentDate compare:serverDate]; // comparing two dates
    
    if(result == NSOrderedAscending)
    {
        NSLog(@"current date is less than server date");
        return -1;
        
    }
    else if(result == NSOrderedDescending)
    {
        NSLog(@"current date is greater than server date ");
        return 1;
    }
    
    else if(result == NSOrderedSame)
    {
        NSLog(@"both are equal");
        return 0;
    }
    return 0;
    
}


-(void)parsePushNotify:(NSArray*)responseObj{
    NSMutableArray *notificationExpireFilter = [responseObj mutableCopy];
    NSMutableArray *indexes = [[NSMutableArray alloc] init];
    for (int i = 0; i < [responseObj count]; i++) {
        
        NSDictionary *dic=[responseObj objectAtIndex:i];
        
        NSArray *rssFeedArr = nil; //[[NSArray alloc]init];
        NSArray *dealArr = nil;//[[NSArray alloc]init];
        
        if ([dic valueForKey:@"rssFeedList"]) {
            rssFeedArr = [dic valueForKey:@"rssFeedList"];
            RssFeedListDO *obj_rssFeedListDO = [[RssFeedListDO alloc]init];
            for (NSDictionary *dictionary in rssFeedArr) {
                //Staff *staff = [[Staff alloc] init];
                obj_rssFeedListDO.title = [dictionary objectForKey:@"title"];
                obj_rssFeedListDO.link = [dictionary objectForKey:@"link"];
                
            }
            
            [rssFeedListDOArr addObject:obj_rssFeedListDO];
        }
        if ([dic valueForKey:@"dealList"]) {
            dealArr = [dic valueForKey:@"dealList"];
            RssFeedListDO *obj_rssFeedListDO = [[RssFeedListDO alloc]init];
            for (NSDictionary *dictionary in dealArr) {
                //Staff *staff = [[Staff alloc] init];
                obj_rssFeedListDO.dealName  = [dictionary objectForKey:@"dealName"];
                obj_rssFeedListDO.type  = [dictionary objectForKey:@"type"];
                obj_rssFeedListDO.splUrl = [dictionary objectForKey:@"splUrl"];
                obj_rssFeedListDO.dealId = [dictionary objectForKey:@"dealId"];
                obj_rssFeedListDO.retailerId = [dictionary objectForKey:@"retailerId"];
                obj_rssFeedListDO.retailLocationId = [dictionary objectForKey:@"retailLocationId"];
                obj_rssFeedListDO.endDate = [dictionary objectForKey:@"endDate"];
                obj_rssFeedListDO.endTime = [dictionary objectForKey:@"endTime"];
            }
            
            if (![HUBCITIKEY isEqualToString:@"Tyler19"])
            {
                
                
                int compare = [self checkForExpiry : obj_rssFeedListDO.endDate : obj_rssFeedListDO.endTime];
                
                if (compare > 0) {
                    
                    NSLog(@"don't add expired coupon");
                    [indexes addObject:[NSNumber numberWithInt:i]];
                    
                    
                    
                }
                else
                {
                    [rssFeedListDOArr addObject:obj_rssFeedListDO];
                }
            }
            else{
                
                [rssFeedListDOArr addObject:obj_rssFeedListDO];
            }
            
            
        }
    }
    
    DLog(@"rssFeedListDOArr %@", rssFeedListDOArr);
    if(indexes.count>0)
    {
        for(int i=0;i<indexes.count;i++){
            NSInteger indexDel= [[indexes objectAtIndex:i] integerValue];
            
            if(notificationExpireFilter.count > 0)
                [notificationExpireFilter removeObjectAtIndex:indexDel-i];
            
            // remove expired deals from global array
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            if(appDelegate.notificationListArr.count > 0)
                [appDelegate.notificationListArr removeObjectAtIndex:indexDel-i];
        }
    }
    
    if (rssFeedListDOArr.count == 0) {
        
        [UtilityManager showAlert:nil msg:@"Deals has expired"];
      
        
        return;
    }
    
    if(responseObj.count > rssFeedListDOArr.count)
    {
        [UtilityManager showAlert:nil msg:@"Some Deals are expired"];
        
        [defaults setValue:notificationExpireFilter forKey:@"PushNotifyResponse"];
        NSLog(@"PUSHNOTI:%@",[defaults valueForKey:@"PushNotifyResponse"]);
        
    }
    
    if ([rssFeedListDOArr count] < 2) {
        
        RssFeedListDO *obj_rssFeedListDO ;
        obj_rssFeedListDO = [rssFeedListDOArr objectAtIndex:0];
        
        //[defaults setBool:YES forKey:@"detailsPage"];
        
        if (obj_rssFeedListDO.title == nil) {
            
            
            if ([obj_rssFeedListDO.type isEqualToString:@"Hotdeals"]) {
                [defaults setValue:obj_rssFeedListDO.dealId forKey:@"HotDealId"];
                HotDealDetailViewController *hdd = [[HotDealDetailViewController alloc] initWithNibName:@"HotDealDetailViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:hdd animated:NO];
                // [hdd release];
            }
            
            else if([obj_rssFeedListDO.type isEqualToString:@"Coupons"]){
                [defaults setValue:obj_rssFeedListDO.dealId forKey:@"couponId"];
                NewCouponDetailViewController *obj_CouponGalleryDetail = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
                
                [self.navigationController pushViewController:obj_CouponGalleryDetail animated:NO];
                // //[obj_CouponGalleryDetail release];
            }
            
            else if ([obj_rssFeedListDO.type isEqualToString:@"SpecialOffers"]){
                
                if (obj_rssFeedListDO.splUrl == nil) {
                    [defaults setValue:obj_rssFeedListDO.dealId forKey:@"nativePageId"];
                    [defaults setValue:obj_rssFeedListDO.retailLocationId forKey:@"retailLocationID"];
                    [defaults setValue:obj_rssFeedListDO.retailerId forKey:@"retailerId"];
                    NativeSpecialOfferViewController *splOfferVC = [[NativeSpecialOfferViewController alloc]initWithNibName:@"NativeSpecialOfferViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:splOfferVC animated:NO];
                    // //[splOfferVC release];
                }
                
                else{
                    
                    [defaults setValue:obj_rssFeedListDO.splUrl forKey:KEY_URL];
                    
                    WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:urlDetail animated:NO];
                    ////[urlDetail release];
                }
            }
        }
        else{
            [defaults setValue:obj_rssFeedListDO.link forKey:KEY_URL];
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
           
        }
    }
    
    
    
    
}

-(void)parse_HdDetail:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    
}

#pragma mark table delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RssFeedListDO *obj_Rss;
    obj_Rss = [rssFeedListDOArr objectAtIndex: indexPath.row];
    
    
    if (obj_Rss.title == nil) {
        
        if ([obj_Rss.type isEqualToString:@"Hotdeals"]) {
            [defaults setValue:obj_Rss.dealId forKey:@"HotDealId"];
            HotDealDetailViewController *hdd = [[HotDealDetailViewController alloc] initWithNibName:@"HotDealDetailViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:hdd animated:NO];
            //[hdd release];
        }
        
        else if([obj_Rss.type isEqualToString:@"Coupons"]){
            [defaults setValue:obj_Rss.dealId forKey:@"couponId"];
            NewCouponDetailViewController *obj_CouponGalleryDetail = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:obj_CouponGalleryDetail animated:NO];
            ////[obj_CouponGalleryDetail release];
        }
        
        else if ([obj_Rss.type isEqualToString:@"SpecialOffers"]){
            if (obj_Rss.splUrl == nil) {
                [defaults setValue:obj_Rss.dealId forKey:@"nativePageId"];
                [defaults setValue:obj_Rss.retailLocationId forKey:@"retailLocationID"];
                [defaults setValue:obj_Rss.retailerId forKey:@"retailerId"];
                NativeSpecialOfferViewController *splOfferVC = [[NativeSpecialOfferViewController alloc]initWithNibName:@"NativeSpecialOfferViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:splOfferVC animated:NO];
                ////[splOfferVC release];
            }
            
            else{
                
                [defaults setValue:obj_Rss.splUrl forKey:KEY_URL];
                
                WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                ////[urlDetail release];
            }
        }
        
    }
    else{
        [defaults setValue:obj_Rss.link forKey:KEY_URL];
        WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:urlDetail animated:NO];
        ////[urlDetail release];
    }
    [self.notificationListView deselectRowAtIndexPath:[self.notificationListView indexPathForSelectedRow] animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 44.0;
    }
    else
    {
        return 70.0;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"reusableCell";
    UITableViewCell *cell = nil;
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    UILabel *lbl;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lbl= [[UILabel alloc]initWithFrame:CGRectMake(10, 13, SCREEN_WIDTH - 20, 20)];
        [lbl setFont:[UIFont boldSystemFontOfSize:13]];
    }
    else
    {
        lbl= [[UILabel alloc]initWithFrame:CGRectMake(10, 18, SCREEN_WIDTH - 20, 40)];
        [lbl setFont:[UIFont boldSystemFontOfSize:18]];
    }
    
    
    RssFeedListDO *obj_Rss ;
    obj_Rss = [rssFeedListDOArr objectAtIndex:  indexPath.row ];
    
    
    if (obj_Rss.title==nil) {
        [lbl setText:obj_Rss.dealName];
    }
    else{
        [lbl setText:obj_Rss.title];
    }
    
    [lbl setTextColor:[UIColor blackColor]];
    [lbl setBackgroundColor:[UIColor clearColor]];
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell.contentView addSubview:lbl];
    //[lbl release];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [rssFeedListDOArr count];
}


#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  RequestViewFiltersController.m
//  HubCiti
//
//  Created by Ashika on 1/4/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "RequestViewFiltersController.h"


@interface RequestViewFiltersController (){
    NSString *selectFilter;
    CustomizedNavController *cusNav;
}

@end

@implementation RequestViewFiltersController
@synthesize sortObj,selectedFilter;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    UIButton *btn = [UtilityManager customizeBarButtonItem:@"Done"];
    [btn addTarget:self action:@selector(doneButtonTouch:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = mainPage;
    
    
    UIButton *btncancel = [UtilityManager customizeBarButtonItem:NSLocalizedString(@"Cancel",@"Cancel")];
    [btncancel addTarget:self action:@selector(cancelButtonTouch:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* btncancelbar = [[UIBarButtonItem alloc] initWithCustomView:btncancel];
    self.navigationItem.rightBarButtonItem = btncancelbar;
    
    requestViewFilter = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-60) style:UITableViewStylePlain];
    requestViewFilter.dataSource=self;
    requestViewFilter.delegate=self;
    requestViewFilter.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:requestViewFilter];
    

    arrFilters= [[NSMutableArray alloc]initWithObjects:@"All",@"New Request",@"Initiated",@"Assigned",@"Completed", nil];
    selectedFilter=sortObj.requestViewFilter;
    if(!selectedFilter)
    selectedFilter=@"NONE";
    self.navigationItem.title = @"Filter";

}
-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    cusNav =(CustomizedNavController *) self.navigationController;
    
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];

}
-(void)doneButtonTouch:(id)sender{
    
    [defaults setValue:@"YES" forKey:@"isComingFromFilter"];
    if(!sortObj){
        sortObj=[[SortAndFilter alloc]init];
    }
    sortObj.requestViewFilter=selectedFilter;
    [self saveCustomObject:sortObj key:@"SortObject"];
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)saveCustomObject:(SortAndFilter *)object key:(NSString *)key {
    
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
    
}

-(void)cancelButtonTouch:(id)sender{
    [defaults setValue:nil forKey:@"isComingFromFilter"];
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 40.0;
    }
    else
    {
        return 60.0;
    }
    

}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 26.0;
    }
    else
    {
        return 36.0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrFilters.count;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *lblCatName ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 18)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
    }
    else
    {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 28)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
    }
        [lblCatName setText:@" Filter Items by"];

            
    [lblCatName setTextColor:[UIColor blackColor]];
    [lblCatName setBackgroundColor:[UIColor grayColor]];
    return lblCatName;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UILabel *lblEvtDate;
    static NSString *CellIdentifier = @"CellFilterView";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==nil){
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor =[UIColor clearColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    else{
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }
        
    }
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 8, SCREEN_WIDTH - 20, 20)];
        [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
    }
    else
    {
        lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 11, SCREEN_WIDTH - 20, 30)];
        [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
    }
    [lblEvtDate setText:[arrFilters objectAtIndex:indexPath.row]];
    [lblEvtDate setTextColor:[UIColor blackColor]];
    [lblEvtDate setBackgroundColor:[UIColor clearColor]];
    
    [cell.contentView addSubview:lblEvtDate];
    
    
    if([selectedFilter isEqualToString:@"NONE"])
        selectFilter=@"All";
        else
            selectFilter=selectedFilter;
    
        NSUInteger indexSelected=[arrFilters indexOfObject:selectFilter];
        if(indexPath.row==indexSelected){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;

        }
        else
            cell.accessoryType =UITableViewCellAccessoryNone;
    
   
       return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *tCell=[tableView cellForRowAtIndexPath:indexPath];
    
    // Set value to show checkmark in table row
    if(indexPath.section==0){
        if(tCell.accessoryType == UITableViewCellAccessoryCheckmark){
            //tCell.accessoryType=UITableViewCellAccessoryNone;
            //selectedFilter=@"NONE";
        }
        else{
            tCell.accessoryType=UITableViewCellAccessoryCheckmark;
            if(indexPath.row==0)
                selectedFilter=@"NONE";
            else
                selectedFilter=[arrFilters objectAtIndex:indexPath.row];
        }
    }
    [requestViewFilter reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

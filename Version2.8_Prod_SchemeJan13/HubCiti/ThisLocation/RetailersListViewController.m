//
//  RetailersListViewController.m
//  ScanSee
//
//  Created by ramanan on 5/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "SpecialOffersViewController.h"
#import "FindOptionsSortViewController.h"
#import "RetailersListViewController.h"
#import "math.h"
#import "MainMenuViewController.h"
//#import "SubCategoriesViewController.h"
#import "LocationDetailsViewController.h"
#import "RetailerSummaryViewController.h"
//Added for Location Update
#import "CLLocation (Strings).h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "SettingsViewController.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "ButtonSetupViewController.h"
#import "SESpringBoard.h"
#import "bottomButtonView.h"
#import "CityExperienceViewController.h"
#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "FindViewController.h"
#import "AnythingPage.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "AppDelegate.h"
#import "SingleCatRetailers.h"
#import "CouponsMyAccountsViewController.h"
//#import "ScanNowScreen.h"
#import "EventsListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FAQCategoryList.h"
#import "MapKitDisplayViewController.h"
#import "FindOptionsViewController.h"
#import "Mapkit/MKMapView.h"
#import "RetailerAnnotation.h"
#import "FilterListViewController.h"
#import "FundraiserListViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "HTTPClient.h"
#import "GetUserInfoResponse.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "CouponsInDealViewController.h"

@interface RetailersListViewController() <HTTPClientDelegate>
{
    UISegmentedControl* segment;
    UILabel* lblCatName;
    UIActivityIndicatorView *loading;
    int segmentIndex;
    NSDictionary *tableViewDictionary;
    BOOL fetchNext;
    NSIndexPath *ipath;
    NSMutableArray* statusArray;
    SortAndFilter* sortFilObj;
    bottomButtonView *bottomBtnView;
    UIFont *addressFont;
    float numberOfLines;
    CustomizedNavController *cusNav;
}

@end

@implementation RetailersListViewController

@synthesize isOnlyOnePartner,anyVC,infoResponse,emailSendingVC;
#pragma mark - Service Respone methods
//@synthesize isOnlyOnePartner,anyVC,emailSendingVC;



- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    

    [HubCitiAppDelegate removeActivityIndicator];
    
    
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}





- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject

{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    
    
    
    [self responseData:responseObject];
    
    
    
}

-(void)getRetailersFromZip
{
    DLog(@"getting Retailers From Zip");
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<ThisLocationRequest><preferredRadius>20</preferredRadius><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [xmlStr appendFormat:@"<hubCitiId>%@</hubCitiId><locOnMap>false</locOnMap>",[defaults valueForKey:KEY_HUBCITIID]];
    
    //@Deepak: added moduleID and LocOnmap Keys for user tracking
    if([defaults valueForKey:KEY_MAINMENUID])
        [xmlStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    else
    {
        if([defaults valueForKey:KEY_MITEMID])
            [xmlStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
        
        if([defaults valueForKey:KEY_BOTTOMBUTTONID])
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    [xmlStr appendFormat:@"<zipcode><![CDATA[%@]]></zipcode><gpsEnabled>false</gpsEnabled><lastVisitedRecord>0</lastVisitedRecord></ThisLocationRequest>", [defaults  objectForKey:@"ZipCode"]];
    DLog(@"Request for This Location (Fetching Retailer List) = %@",xmlStr);
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getretailersinfo",BASE_URL];
    [defaults setBool:NO forKey:@"locOnMap"];
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    
    // [xmlStr release];
    
    if([Network currentReachabilityStatus]==0) {
        
        [UtilityManager showAlert];
        return;
    }
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]) {
        return;
    }
    
    [defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
    
}
//Added for Location Update
-(void)getNewLatLong
{
    UIAlertController * alert;
    
    alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Refresh Your Location?",@"Refresh Your Location?") message:NSLocalizedString(@"Do you want to refresh your location?",@"Do you want to refresh your location?") preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* cancel = [UIAlertAction
                         actionWithTitle:@"Cancel"
                         style:UIAlertActionStyleCancel
                         handler:nil];
    [alert addAction:cancel];
    UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action){
                                 [defaults setValue:@"" forKey:KEY_LATITUDE];
                                 [defaults setValue:@"" forKey:KEY_LONGITUDE];
                                 
                                 [Location updateLocationinfo:self];
                             }];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)optionsNearby : (UIBarButtonItem *) sender
{
    ReleaseAndNilify(retailersList);
    nextPage=0;
    fromNearBy=TRUE;
    fromFind=FALSE;
    fromFilters=FALSE;
    //added
    if ([RegionApp isEqualToString:@"1"]){
        fromRegionFind = FALSE;//from region app
    }
    
    
    if ([retailerLatArray count] == 0 || retailerLatArray == nil || [retailerLongArray count] == 0 || retailerLongArray == nil)
    {
        return;
    }
    
    for (int i=0; i<[retailerLatArray count]; i++) {
        [globalNearByLatArray addObject:[retailerLatArray objectAtIndex:i]];
    }
    for (int j =0 ; j<[retailerLongArray count]; j++) {
        [globalNearByLongArray addObject:[retailerLongArray objectAtIndex:j]];
    }
    for (int i=0; i<[retailerNameArray count]; i++) {
        [globalRetailerNameArray addObject:[retailerNameArray objectAtIndex:i]];
    }
    for (int j=0; j<[retailAddressArray count]; j++) {
        [globalRetailerAddressArray addObject:[retailAddressArray objectAtIndex:j]];
    }
    for (int j=0; j<[statusArray count]; j++) {
        [globalRetailerAddressArray addObject:[statusArray objectAtIndex:j]];
    }

    for (int j=0; j<[retailLocationIDArray count]; j++) {
        [globalRetLocId addObject:[retailLocationIDArray objectAtIndex:j]];
    }
    for (int j=0; j<[retailerIdArray count]; j++) {
        [globalRetailerID addObject:[retailerIdArray objectAtIndex:j]];
    }
    for (int j=0; j<[retListIDArray count]; j++) {
        [globalRetailerListID addObject:[retListIDArray objectAtIndex:j]];
    }
    
    FindOptionsViewController *iFindOptionsListViewContoller = [[FindOptionsViewController alloc]initWithNibName:@"FindOptionsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iFindOptionsListViewContoller animated:NO];
    //[iFindOptionsListViewContoller release];
    
    
}

-(void)optionsNearbyShow
{
    retailersList.hidden=YES;
    mapView = [[MKMapView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    //mapView.frame=
    mapView.hidden = NO;
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    [mapView setDelegate:self];
   // CLLocationCoordinate2D southWest;
   // CLLocationCoordinate2D northEast;
    
    
    if ([globalNearByLatArray count]){
        
//        southWest = CLLocationCoordinate2DMake([[globalNearByLatArray objectAtIndex:0] doubleValue],
//                                               [[globalNearByLongArray objectAtIndex:0] doubleValue]);
       // northEast = southWest;
    }
    
    //NSlog(@"%d-%d",[globalNearByLatArray count],[globalRetailerNameArray count]);
    for(int i=0; i<[globalNearByLatArray count]; i++)
    {
        RetailerAnnotation *ann = [[RetailerAnnotation alloc] init];
        ann.coordinate = CLLocationCoordinate2DMake([[globalNearByLatArray objectAtIndex:i] doubleValue],
                                                    [[globalNearByLongArray objectAtIndex:i] doubleValue]);
        [mapView addAnnotation:ann];
        [annArray addObject:ann];
        [ann setTitle:[globalRetailerNameArray objectAtIndex:i]];
        [ann setSubtitle:[globalRetailerAddressArray objectAtIndex:i]];
        ReleaseAndNilify(ann);
        
    }
    
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in mapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    //[mapView setVisibleMapRect:zoomRect animated:YES];
    
    UIEdgeInsets insets = UIEdgeInsetsMake(40, 20, 60, 20);
    
    MKMapRect biggerRect = [mapView mapRectThatFits:zoomRect edgePadding:insets];
    [mapView setVisibleMapRect:biggerRect animated:YES];
    [self.view addSubview:mapView];

    
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    MKPinAnnotationView* pinView = nil;
    
    if ([annotation isKindOfClass:[RetailerAnnotation class]])
    {
        // try to dequeue an existing pin view first
        static NSString* RetailerAnnotationIdentifier = @"retailerAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:RetailerAnnotationIdentifier];
        
        if (!pinView)
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
                                                  initWithAnnotation:annotation reuseIdentifier:RetailerAnnotationIdentifier] ;
            //customPinView.pinColor = MKPinAnnotationColorPurple;
            customPinView.animatesDrop = YES;
            
            customPinView.canShowCallout = YES;
            UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            //infoButton.backgroundColor=[UIColor blueColor];
            infoButton.tintColor=[UIColor convertToHexString:@"#00abff"];
            customPinView.rightCalloutAccessoryView = infoButton;
            
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
    }
    pinView.canShowCallout = YES;
    return pinView;
    
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    for (int j = 0 ; j < [globalRetailerNameArray count]; j++) {
        if ([[globalRetailerNameArray objectAtIndex:j] isEqualToString:view.annotation.title]) {
            NSIndexPath *ip = [NSIndexPath indexPathForRow:j inSection:0];
            [linkID insertObject:@"0" atIndex:[linkID count]];
            [self displayCategoryInfo:ip];
            return;
        }
    }
}

-(void) releaseOptionsArrays
{
    if ([globalNearByLatArray count]>0){
        [globalNearByLatArray removeAllObjects];
        //[globalNearByLatArray release];
    }
    
    if(  [globalNearByLongArray count]>0){
        [globalNearByLongArray removeAllObjects];
        //[globalNearByLongArray release];
    }
    
    if( [globalRetailerNameArray count]>0)
    {
        [globalRetailerNameArray removeAllObjects];
        // [globalRetailerNameArray release];
    }
    if([globalRetailerAddressArray count]) {
        
        [globalRetailerAddressArray removeAllObjects];
        //  [globalRetailerAddressArray release];
    }
    
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];//alertGPs
    
     [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    [defaults setValue:@"distance" forKey:@"NearBySort"];
    addressFont = [UIFont systemFontOfSize:(IPAD ? 15.0f:10.0f)];
    if(showMapFlag==TRUE)
    {
        //showMapFlag=FALSE;
       
      
        self.navigationItem.hidesBackButton = YES;
        //[back release];
        self.title=NSLocalizedString(@" Choose Your Location:", @"  Choose Your Location:");
        [self optionsNearbyShow];
    }
    else
    {
        [defaults setInteger:0 forKey:@"selectedSegment"];
        [self filtersOptionsReleaseArrays];
        self.navigationController.navigationBar.hidden = NO;
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        
        [retailersList setAccessibilityLabel:@"retailerList"];
        [retailersList setAccessibilityIdentifier:@"retailerTable"];
        
        retailersList.delegate = self;
        retailerIdArray = [[NSMutableArray alloc] init];
        rowNumberArray = [[NSMutableArray alloc] init];
        retailerNameArray = [[NSMutableArray alloc] init];
        completeAddress = [[NSMutableArray alloc] init];
        retailerAddressArray = [[NSMutableArray alloc] init];
        retailLocationIDArray = [[NSMutableArray alloc] init];
        logoImagePathArray = [[NSMutableArray alloc] init];
        distanceArray = [[NSMutableArray alloc] init];
        retailAddressArray = [[NSMutableArray alloc] init];
        bannerAdImagePathArray = [[NSMutableArray alloc] init];
        ribbonAdImagePathArray = [[NSMutableArray alloc] init];
        ribbonAdURLArray = [[NSMutableArray alloc] init];
        advertisementIDArray = [[NSMutableArray alloc] init];
        saleFlagArray = [[NSMutableArray alloc] init];
        retListIDArray = [[NSMutableArray alloc] init];
        retailerLatArray = [[NSMutableArray alloc] init];
        retailerLongArray = [[NSMutableArray alloc] init];
        statusArray = [[NSMutableArray alloc] init];
        annArray = [[NSMutableArray alloc] init];
        citistatezipArray = [[NSMutableArray alloc] init];
        globalNearByLatArray = [[NSMutableArray alloc]init];
        globalNearByLongArray = [[NSMutableArray alloc]init];
        globalRetailerNameArray=[[NSMutableArray alloc]init];
        globalRetailerAddressArray=[[NSMutableArray alloc]init];
        
        globalRetailerListID = [[NSMutableArray alloc]init];
        globalRetailerID =[[NSMutableArray alloc]init];
        globalRetLocId = [[NSMutableArray alloc]init];
        
        
        //    UIButton *options = [UtilityManager customizeBarButtonItem:@"Map/Sort"];
        //    [options addTarget:self action:@selector(optionsNearby:) forControlEvents:UIControlEventTouchUpInside];
        //    options.tag=1;
        //    UIBarButtonItem *mainMenu = [[UIBarButtonItem alloc]initWithCustomView:options];
        //    self.navigationItem.rightBarButtonItem = mainMenu;
        // [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"Map/Sort"];
        //    [mainMenu release];
        
       
               [self releaseOptionsArrays];
        
    }
    
    [self showSegments];
   
    
}

-(void) setSegmentColor:(UISegmentedControl *) segmentSender
{
    
    for (int i=0; i<[segmentSender.subviews count]; i++)
    {
        if ([[segmentSender.subviews objectAtIndex:i]isSelected] )
        {
            UIColor *tintcolor=[UIColor colorWithRed:80/255.0 green:199/255.0 blue:228/255.0 alpha:1.0];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
            //break;
        }
        else {
            UIColor *tintcolor=[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
        }
    }
    for (int i=0; i<[segmentSender.subviews count]; i++)
    {
        if (![[segmentSender.subviews objectAtIndex:i]isSelected] )
        {
            UIColor *tintcolor=[UIColor darkGrayColor];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
            // break;
        }
        
    }
    
    float segmentFont = 14.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        segmentFont = 18.0;
    }
    NSDictionary *attributes = @{
                                 NSFontAttributeName : [UIFont boldSystemFontOfSize:segmentFont],
                                 NSForegroundColorAttributeName : [UIColor blackColor]};
    [segmentSender setTitleTextAttributes:attributes forState:UIControlStateSelected];
    
    NSDictionary *attributes2 = @{
                                  NSFontAttributeName : [UIFont boldSystemFontOfSize:segmentFont],
                                  NSForegroundColorAttributeName : [UIColor whiteColor]};
    [segmentSender setTitleTextAttributes:attributes2 forState:UIControlStateNormal];
    
}

-(void) callMap
{
    if ([globalRetailerNameArray  count] > 0) {
        
        RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
        self.retailerListScreen = retailerListScreen;
        [self.navigationController pushViewController:retailerListScreen animated:NO];
      
    }

}

-(void) viewSelectionChanged : (id) sender
{
    nextPage = 0;
    lastVisitedRecord = 0;
    switch ([segment selectedSegmentIndex])
    {
        case 0:
        {
            [defaults setValue:@"distance" forKey:@"NearBySort"];
            [defaults setInteger:0 forKey:@"selectedSegment"];
            [self updateSegment];
            [self request_getRetailersInfo:lastVisitedRecord];
            
        }
            break;
        case 1:
        {
            [defaults setValue:@"atoz" forKey:@"NearBySort"];
            [defaults setInteger:1 forKey:@"selectedSegment"];
            [self updateSegment];
            [self request_getRetailersInfo:lastVisitedRecord];
            
        }
            break;
        case 2:
        {
            
            [segment setSelectedSegmentIndex:2];
            [self setSegmentColor:segment];
            // Navigate to Map
            ReleaseAndNilify(retailersList);
            showMapFlag=TRUE;
            nextPage=0;
            fromNearBy=TRUE;
            fromFind=FALSE;
            fromFilters=FALSE;
            //added
            if ([RegionApp isEqualToString:@"1"]){
                fromRegionFind = FALSE;//from region app
            }
            
            
            if ([retailerLatArray count] == 0 || retailerLatArray == nil || [retailerLongArray count] == 0 || retailerLongArray == nil)
            {
                return;
            }
            
            for (int i=0; i<[retailerLatArray count]; i++) {
                [globalNearByLatArray addObject:[retailerLatArray objectAtIndex:i]];
            }
            for (int j =0 ; j<[retailerLongArray count]; j++) {
                [globalNearByLongArray addObject:[retailerLongArray objectAtIndex:j]];
            }
            for (int i=0; i<[retailerNameArray count]; i++) {
                [globalRetailerNameArray addObject:[retailerNameArray objectAtIndex:i]];
            }
            for (int j=0; j<[retailAddressArray count]; j++) {
                [globalRetailerAddressArray addObject:[retailAddressArray objectAtIndex:j]];
            }
            for (int j=0; j<[statusArray count]; j++) {
                [globalRetailerAddressArray addObject:[statusArray objectAtIndex:j]];
            }
            for (int j=0; j<[citistatezipArray count]; j++) {
                [globalRetailerAddressArray addObject:[citistatezipArray objectAtIndex:j]];
            }
            for (int j=0; j<[retailLocationIDArray count]; j++) {
                [globalRetLocId addObject:[retailLocationIDArray objectAtIndex:j]];
            }
            for (int j=0; j<[retailerIdArray count]; j++) {
                [globalRetailerID addObject:[retailerIdArray objectAtIndex:j]];
            }
            for (int j=0; j<[retListIDArray count]; j++) {
                [globalRetailerListID addObject:[retListIDArray objectAtIndex:j]];
            }
            [self performSelector:@selector(callMap) withObject:nil afterDelay:0.1];
            
        }
            break;
            
        default:
            break;
    }

}

-(void) updateSegment
{
    
    segmentIndex = (int)[defaults integerForKey:@"selectedSegment"];
    
    if (segmentIndex == 0) {
         [defaults setValue:@"distance" forKey:@"NearBySort"];
    }
    else if (segmentIndex == 1) {
        [defaults setValue:@"atoz" forKey:@"NearBySort"];
    }
    
    [segment setSelectedSegmentIndex:segmentIndex];
    [self setSegmentColor:segment];
}
-(void) showSegments
{
    if (!segment) {
        segment=[[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Distance",@"Name",@"Map", nil]];
        
    }
    segmentIndex = (int)[defaults integerForKey:@"selectedSegment"];
    
    [segment setSelectedSegmentIndex:segmentIndex];
    
    [segment setBackgroundColor:[UIColor grayColor]];
    [segment setSegmentedControlStyle:UISegmentedControlStylePlain];
    [segment setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:0];
    [segment setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:1];
    [segment setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:2];
    segment.frame = CGRectMake(0, 0, segment.frame.size.width, 48);
    [self setSegmentColor:segment]; //add customized color
    
    [segment addTarget:self action:@selector(viewSelectionChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segment];
  
}


-(void)settableViewOnScreen
{
    if (retailersList == nil) {
        
        retailersList = [[UITableView alloc]init];
        retailersList.dataSource=self;
        retailersList.delegate=self;
        [retailersList setBackgroundColor:[UIColor whiteColor]];
        [retailersList setAccessibilityIdentifier:@"RetailersList"];
        retailersList.tableFooterView.hidden=YES;
        [self.view addSubview:retailersList];
        retailersList.translatesAutoresizingMaskIntoConstraints = NO;
        dispatch_async (dispatch_get_main_queue(), ^{
            [self setConstraints:nil tag:0];
        });

    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   return 25.0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //    CECategoryDO *iCitiExpCategoryDO = [arrCitiExpCategoryInfo objectAtIndex:section];
   // UILabel *lblCatName;
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH - 10, 18)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
    }
    else
    {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH - 10, 24)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
    }
    
    [lblCatName setTextColor:[UIColor blackColor]];
    [lblCatName setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
    if ([segment selectedSegmentIndex] == 0) {
        [lblCatName setText:[NSString stringWithFormat:@"Sorted by %@",@"Distance"]];
    }
    else if ([segment selectedSegmentIndex] == 1){
         [lblCatName setText:[NSString stringWithFormat:@"Sorted by %@",@"Name"]];
    }
    
    
    //[lblCatName setText:[NSString stringWithFormat:@" Sort by Distance"]];
    
//    if (sortFilterObj.distanceSelected == YES) {
//        [lblCatName setText:[NSString stringWithFormat:@" Sort by Distance"]];
//    }
//    else if (sortFilterObj.alphabeticallySelected == YES) {
//        [lblCatName setText:[NSString stringWithFormat:@" Sort by Alphabetically"]];
//    }
    return lblCatName;
}




-(void)popBackToPreviousPage{
    
    [defaults setBool:NO forKey:@"ViewMore"];
    showMapFlag=FALSE;
    [NearbyBottomButtonID removeLastObject];
    //[defaults setValue:nil forKey:@"NearBySort"];
    [defaults setValue:@"distance" forKey:@"NearBySort"];
    
    [self.navigationController popViewControllerAnimated:NO];
    //navigatedFromMapKit=false;
}

- (SortAndFilter *)loadCustomObjectWithKey:(NSString *)key {
    
    NSData *encodedObject = [defaults objectForKey:key];
    SortAndFilter *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
   
    [super viewWillAppear:animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [self refreshArrays];
    self.navigationController.navigationBar.hidden = NO;
    
    if(showMapFlag==TRUE)
    {
        //showMapFlag=FALSE;
        UIButton *backBtn = [UtilityManager customizeBackButton];
        loading = nil;
        [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        self.navigationItem.leftBarButtonItem = back;
        self.navigationItem.hidesBackButton = YES;
        //[back release];
        
        self.title=NSLocalizedString(@" Choose Your Location:", @"  Choose Your Location:");
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:CGRectMake(0, 0, 30, 30)];
        SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
        [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
        NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
        
        
        [btn addSubview:homeImage];
        
        
        [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
        mainPage.style = UIBarButtonItemStyleBordered;
        
        self.navigationItem.rightBarButtonItem = mainPage;
        [self optionsNearbyShow];
    }
    else
    {
         [cusNav setTitle:@"Choose Your Location:" forView:self withHambergur:YES];
         [cusNav hideBackButton:NO];
         [cusNav hideHambergerButton: NO];
         [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
         self.navigationItem.hidesBackButton = YES;
        
        
        //Added for Location Update
        if ([HubCitiAppDelegate locationServicesOn] && [HubCitiAppDelegate checkZipAndLocation] == 0) {
            
            
            UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
            
            self.navigationItem.backBarButtonItem = backButton;
            //[backButton release];
        }
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
        
        self.navigationItem.backBarButtonItem = backButton;
        
        //to configure back button
        if ([HubCitiAppDelegate locationServicesOn]) {
            
            UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
            
            self.navigationItem.backBarButtonItem = backButton;
      
            
            UIButton *locBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [locBtn setBackgroundImage:[UIImage imageNamed:@"search_down.png"] forState:UIControlStateNormal];
            [locBtn setBackgroundImage:[UIImage imageNamed:@"search_up.png"] forState:UIControlStateHighlighted];
                locBtn.frame = CGRectMake(30, 0, 30, 30);
    
            [locBtn addTarget:self action:@selector(getNewLatLong) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem* locBtnItem = [[UIBarButtonItem alloc] initWithCustomView:locBtn];
            
            
            //home button
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setFrame:CGRectMake(0, 0, 30, 30)];
            SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
            [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
            NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
            [btn addSubview:homeImage];
            
            
            [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
            mainPage.style = UIBarButtonItemStyleBordered;
            
            self.navigationItem.rightBarButtonItems = @[mainPage, locBtnItem];
            
            
        }
        else
        {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setFrame:CGRectMake(0, 0, 30, 30)];
            SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
            [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
            NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
            
            
            [btn addSubview:homeImage];
            
            
            [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
            mainPage.style = UIBarButtonItemStyleBordered;
            self.navigationItem.rightBarButtonItem = mainPage;
        }
        
        
        if ([defaults boolForKey:@"locOnMap"] == YES && [defaults valueForKey:KEY_RESPONSEXML])
            [self parseRetailerList:[defaults valueForKey:KEY_RESPONSEXML]];
       
        else
        {
            lastVisitedRecord = 0;
            // ReleaseAndNilify(retailersList);
            if ([segment selectedSegmentIndex] != 2)
            {
                if ([[defaults valueForKey:@"NearBySort"] isEqualToString:@"distance"])
                {
                    [defaults setInteger:0 forKey:@"selectedSegment"];
                }
                else if ([[defaults valueForKey:@"NearBySort"] isEqualToString:@"atoz"]){
                    [defaults setInteger:1 forKey:@"selectedSegment"];
                }
                else
                {
                    [defaults setInteger:0 forKey:@"selectedSegment"];
                }
            }
            
            [self updateSegment];
            [self filtersOptionsReleaseArrays];
            [self request_getRetailersInfo:lastVisitedRecord];
            
        }
        
    }
    
}

-(NSString *)getUTCFormateDate:(NSDate *)localDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:localDate];
   
    return dateString;
}
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
-(void)filtersOptionsReleaseArrays
{
    if ([globalLatArray count]>0) {
        [globalLatArray removeAllObjects];
        //[globalLatArray release];
    }
    if ([globalLongArray count]>0) {
        [globalLongArray removeAllObjects];
        //[globalLongArray release];
    }
    if( [globalNameArray count]>0)
    {
        [globalNameArray removeAllObjects];
        //[globalNameArray release];
    }
    if([globalAddressArray count]) {
        
        [globalAddressArray removeAllObjects];
        //[globalAddressArray release];
    }
    if([globalRetailerListID count]) {
        
        [globalRetailerListID removeAllObjects];
        //[globalRetailerListID release];
    }
    if([globalRetLocId count]) {
        
        [globalRetLocId removeAllObjects];
        //[globalRetLocId release];
    }
    if([globalRetailerID count]) {
        
        [globalRetailerID removeAllObjects];
        //[globalRetailerID release];
    }
    if ([globalNearByLatArray count]>0){
        [globalNearByLatArray removeAllObjects];
        //[globalNearByLatArray release];
    }
    
    if(  [globalNearByLongArray count]>0){
        [globalNearByLongArray removeAllObjects];
        // [globalNearByLongArray release];
    }
    
    if( [globalRetailerNameArray count]>0)
    {
        [globalRetailerNameArray removeAllObjects];
        //  [globalRetailerNameArray release];
    }
    if([globalRetailerAddressArray count]) {
        
        [globalRetailerAddressArray removeAllObjects];
        //  [globalRetailerAddressArray release];
    }
    
    
    
}

-(void) refreshTable : (int) lastRecord{
    
    [retailerIdArray removeAllObjects];
    [rowNumberArray removeAllObjects];
    [retailLocationIDArray removeAllObjects];
    [logoImagePathArray removeAllObjects];
    [bannerAdImagePathArray removeAllObjects];
    [ribbonAdImagePathArray removeAllObjects];
    [ribbonAdURLArray removeAllObjects];
    [saleFlagArray removeAllObjects];
    [retailerNameArray removeAllObjects];
    [retailAddressArray removeAllObjects];
    [citistatezipArray removeAllObjects];
    [distanceArray removeAllObjects];
    [retListIDArray removeAllObjects];
    [statusArray removeAllObjects];
    [self request_getRetailersInfo:lastRecord];
    [retailersList scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [retailersList reloadData];
    retailersList.userInteractionEnabled = YES;
}

-(void)refreshArrays{
    [statusArray removeAllObjects];
    [retailerIdArray removeAllObjects];
    [rowNumberArray removeAllObjects];
    [retailLocationIDArray removeAllObjects];
    [logoImagePathArray removeAllObjects];
    [bannerAdImagePathArray removeAllObjects];
    [ribbonAdImagePathArray removeAllObjects];
    [ribbonAdURLArray removeAllObjects];
    [saleFlagArray removeAllObjects];
    [retailerNameArray removeAllObjects];
    [retailAddressArray removeAllObjects];
    [distanceArray removeAllObjects];
    [retListIDArray removeAllObjects];
    [retailerLatArray removeAllObjects];
    [retailerLongArray removeAllObjects];
    
}

-(void) addToTable: (int) lastRecord{
    
    [self request_getRetailersInfo:lastRecord];
    [retailersList reloadData];
    
}

-(void)fetchNextPage:(id)sender{
//
//    if (nextPage == 1)
//    {
//        lastVisitedRecord = [[rowNumberArray objectAtIndex:[rowNumberArray count]-1 ]intValue];
//        [self addToTable:lastVisitedRecord];
//
//    }
//    else {
//		return;
//	}
}



-(void)returnToMainPage:(id)sender
{
    showMapFlag = FALSE;
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case GETRETAILERSINFO:
        {
            dispatch_async (dispatch_get_main_queue(), ^{
                [self parseRetailerList:response];
                [retailersList reloadData];
                
                });
        }
            break;
        case GETRETSUMMARY:
        {
            dispatch_async (dispatch_get_main_queue(), ^{
                [self parse_retSummary:response];
                [retailersList reloadData];
            });
        }
        break;
        case GETUSERINF:
        {
            [self parseGetUserData:response];
        }
            break;
        case CITIEXPRE:
            
            [self parse_CitiExpRet:response];
            
            break;
        case PARTNERET:
            [self parse_PartnerRet:response];
            break;
            //        case gETPARTNER:
            //            [self parse_GetPartners:response];
            //            break;
            
        case gETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
            
        case HubcitiAnythingInf:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case Appsitedetail:
            [self parse_appsitedetails:response];
            break;
            
        default:
            break;
            
            
            
    }
    
}


/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

#pragma mark -
#pragma mark Table view
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (nextPage == 1 && retailerNameArray.count > 0 ){
        NSLog(@"total Rows= %lu",([retailerNameArray count]+1));
       return ([retailerNameArray count]+1);
    }
    else
        return [retailerNameArray count];
    
}
-(CGFloat) getLabelSize : (NSString*) labelString withSize:(CGFloat) labelwidth withFont:(UIFont *) font
{
    
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    CGFloat noOflines = ceil(rect.size.height / font.lineHeight);
    
    
    return noOflines;
    
}

/*
 #define ASYNC_IMAGE_TAG 9999
 #define LABEL_TAG 8888
 #define DETAIL_LABEL_TAG 7777
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    static NSString *cellIdentifier = @"Cell";
   
    
    SdImageView *asyncImageView = nil;
    UILabel *label = nil;
    UILabel *detailLabel = nil;
    UILabel *addressLabel = nil;
    UILabel *mileLabel = nil;
    UIImageView *specImage = nil;
    UILabel *statusLabel = nil;
    
    UITableViewCell *cell;
   
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] ;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
   
   // float viewMoreLabelFont = 16.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
       // viewMoreLabelFont = 20.0;
    }
    
    
    CGRect frame;
    
    if (indexPath.row != [retailerNameArray count]) {
        
       
        if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
            frame.origin.x = 5;
            frame.origin.y = 5;
            frame.size.width = 40;
            frame.size.height = 40;
        }
        else
        {
            frame.origin.x = 5;
            frame.origin.y = 5;
            frame.size.width = 60;
            frame.size.height = 60;
        }
        
        
        asyncImageView = [[SdImageView alloc] initWithFrame:frame] ;
        
        asyncImageView.tag = ASYNC_IMAGE_TAG;
        // asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        asyncImageView.backgroundColor = [UIColor clearColor];
        asyncImageView.layer.masksToBounds = YES;
        
        [cell.contentView addSubview:asyncImageView];
        statusLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            statusLabel.font = [UIFont boldSystemFontOfSize:15];
        else
            statusLabel.font = [UIFont boldSystemFontOfSize:10];
        statusLabel.textColor = [UIColor darkGrayColor];
        [cell.contentView addSubview:statusLabel];
        
        label = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            label.font = [UIFont boldSystemFontOfSize:19];
        else
            label.font = [UIFont boldSystemFontOfSize:14];
        label.textColor = [UIColor colorWithRGBInt:0x112e72];
        [cell.contentView addSubview:label];
        
        detailLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            detailLabel.font = [UIFont boldSystemFontOfSize:15];
        else
            detailLabel.font = [UIFont boldSystemFontOfSize:10];
        detailLabel.textColor = [UIColor darkGrayColor];
        detailLabel.numberOfLines = 2;
        detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        detailLabel.preferredMaxLayoutWidth = 150.0;
        [cell.contentView addSubview:detailLabel];
        
        addressLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
            addressLabel.font = [UIFont boldSystemFontOfSize:15];
            addressLabel.preferredMaxLayoutWidth = 400.0;
            addressLabel.numberOfLines = 1;
        }
        else
        {
            addressLabel.font = [UIFont boldSystemFontOfSize:10];
            addressLabel.preferredMaxLayoutWidth = 170.0;
            addressLabel.numberOfLines = 2;
        }
        addressLabel.textColor = [UIColor darkGrayColor];
        
        addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        addressLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        [cell.contentView addSubview:addressLabel];
        
        
        mileLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            mileLabel.font = [UIFont boldSystemFontOfSize:15];
        else
            mileLabel.font = [UIFont boldSystemFontOfSize:10];
        mileLabel.textColor = [UIColor darkGrayColor];
        mileLabel.numberOfLines = 1;
        [cell.contentView addSubview:mileLabel];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (indexPath.row<[retailerNameArray count]){
            
            NSString *logoImageStr = [logoImagePathArray objectAtIndex:indexPath.row];
            if(![logoImageStr isEqualToString:@"N/A"] )
            {
                [asyncImageView loadImage:logoImageStr];
            }
            RetailersInDetail *distanceDic=[retailerNameArray objectAtIndex:indexPath.row];
            NSLog(@"%@",distanceDic);
           label.text = [NSString stringWithFormat:@"%@", [retailerNameArray objectAtIndex:indexPath.row]];
            
            //distance in miles
            mileLabel.text =[NSString stringWithFormat:@"%@", [distanceArray objectAtIndex:indexPath.row]];
            
            //address1, address2 label
            
            addressLabel.text = [retailAddressArray objectAtIndex:indexPath.row];
            
            if(![[statusArray objectAtIndex:indexPath.row] isEqualToString:@"N/A"])
                statusLabel.text = [statusArray objectAtIndex:indexPath.row];
            else
                statusLabel.text = @"";
            
            //extracting city,state,zip from complete address
            
            detailLabel.text = [citistatezipArray objectAtIndex:indexPath.row];
            
            if ([[saleFlagArray objectAtIndex:indexPath.row] intValue] == 1) {
                
                specImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"retSpecialsIcon_o.png"]];
                [cell.contentView addSubview:specImage];
                // [specImage release];
            }
            
            float width = (IPAD ? 400.0f:170.0f);
            numberOfLines =[ self getLabelSize:addressLabel.text withSize:width withFont:addressFont];
        }
        
        if (specImage == nil) {
            tableViewDictionary = @{@"asyncImageView":asyncImageView,@"statusLabel":statusLabel,@"label":label,@"detailLabel":detailLabel,@"addressLabel":addressLabel,@"mileLabel":mileLabel};

        }
        
        asyncImageView.translatesAutoresizingMaskIntoConstraints = NO;
        label.translatesAutoresizingMaskIntoConstraints = NO;
        detailLabel.translatesAutoresizingMaskIntoConstraints = NO;
        specImage.translatesAutoresizingMaskIntoConstraints = NO;
        addressLabel.translatesAutoresizingMaskIntoConstraints = NO;
        mileLabel.translatesAutoresizingMaskIntoConstraints = NO;
        statusLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        if(specImage == nil && asyncImageView == nil && label == nil && detailLabel == nil && statusLabel == nil && mileLabel == nil && addressLabel == nil)
            tableViewDictionary = nil;
        else if(specImage == nil )
            tableViewDictionary = NSDictionaryOfVariableBindings(asyncImageView,label,detailLabel,addressLabel,mileLabel,statusLabel);
        else if(specImage == nil && asyncImageView == nil)
            tableViewDictionary = NSDictionaryOfVariableBindings(label,detailLabel,addressLabel,mileLabel,statusLabel);
        else if(specImage != nil && asyncImageView == nil)
        {
            tableViewDictionary = NSDictionaryOfVariableBindings(specImage, label,detailLabel,addressLabel,mileLabel,statusLabel);
        }
        else if(asyncImageView != nil && specImage == nil)
        {
            tableViewDictionary = NSDictionaryOfVariableBindings(asyncImageView, label,detailLabel,addressLabel,mileLabel,statusLabel);
        }
        else if(specImage != nil && asyncImageView != nil && label != nil && detailLabel != nil && statusLabel != nil && mileLabel != nil && addressLabel != nil)
            tableViewDictionary = NSDictionaryOfVariableBindings(asyncImageView,label,detailLabel,specImage,statusLabel,mileLabel,addressLabel);
      [self setConstraints:cell tag:1];
    }
    
    //view more results...
    else if (indexPath.row == [retailerNameArray count]  && nextPage == 1) {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        CGRect frame;
        
        frame.origin.x = (SCREEN_WIDTH - 40)/2;
        frame.origin.y = 5;
        frame.size.width = 40;
        frame.size.height = 40;
        loading.frame = frame;
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
      
    }
    
    
    
    return cell;
}
/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *cellIdentifier = @"Cell";
    
    
    SdImageView *asyncImageView = nil;
    UILabel *label = nil;
    UILabel *detailLabel = nil;
    UILabel *addressLabel = nil;
    UILabel *mileLabel = nil;
    UIImageView *specImage = nil;
    UILabel *statusLabel = nil;
    
    UITableViewCell *cell;
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] ;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    // float viewMoreLabelFont = 16.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        // viewMoreLabelFont = 20.0;
    }
    
    
    CGRect frame;
    
    if (indexPath.row != [retailerNameArray count]) {
        
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
            frame.origin.x = 5;
            frame.origin.y = 5;
            frame.size.width = 40;
            frame.size.height = 40;
        }
        else
        {
            frame.origin.x = 5;
            frame.origin.y = 5;
            frame.size.width = 60;
            frame.size.height = 60;
        }
        
        
        asyncImageView = [[SdImageView alloc] initWithFrame:frame] ;
        
        asyncImageView.tag = ASYNC_IMAGE_TAG;
        // asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        asyncImageView.backgroundColor = [UIColor clearColor];
        asyncImageView.layer.masksToBounds = YES;
        
        [cell.contentView addSubview:asyncImageView];
        statusLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            statusLabel.font = [UIFont boldSystemFontOfSize:15];
        else
            statusLabel.font = [UIFont boldSystemFontOfSize:10];
        statusLabel.textColor = [UIColor darkGrayColor];
        [cell.contentView addSubview:statusLabel];
        
        label = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            label.font = [UIFont boldSystemFontOfSize:19];
        else
            label.font = [UIFont boldSystemFontOfSize:14];
        label.textColor = [UIColor colorWithRGBInt:0x112e72];
        [cell.contentView addSubview:label];
        
        detailLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            detailLabel.font = [UIFont boldSystemFontOfSize:15];
        else
            detailLabel.font = [UIFont boldSystemFontOfSize:10];
        detailLabel.textColor = [UIColor darkGrayColor];
        detailLabel.numberOfLines = 2;
        detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        detailLabel.preferredMaxLayoutWidth = 150.0;
        [cell.contentView addSubview:detailLabel];
        
        addressLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
            addressLabel.font = [UIFont boldSystemFontOfSize:15];
            addressLabel.preferredMaxLayoutWidth = 400.0;
            addressLabel.numberOfLines = 1;
        }
        else
        {
            addressLabel.font = [UIFont boldSystemFontOfSize:10];
            addressLabel.preferredMaxLayoutWidth = 170.0;
            addressLabel.numberOfLines = 2;
        }
        addressLabel.textColor = [UIColor darkGrayColor];
        
        addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        addressLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        [cell.contentView addSubview:addressLabel];
        
        
        mileLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            mileLabel.font = [UIFont boldSystemFontOfSize:15];
        else
            mileLabel.font = [UIFont boldSystemFontOfSize:10];
        mileLabel.textColor = [UIColor darkGrayColor];
        mileLabel.numberOfLines = 1;
        [cell.contentView addSubview:mileLabel];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (indexPath.row<[retailerNameArray count]){
            
            NSString *logoImageStr = [logoImagePathArray objectAtIndex:indexPath.row];
            if(![logoImageStr isEqualToString:@"N/A"] )
            {
                [asyncImageView loadImage:logoImageStr];
            }
            RetailersInDetail *distanceDic=[retailerNameArray objectAtIndex:indexPath.row];
            NSLog(@"%@",distanceDic);
            label.text = [NSString stringWithFormat:@"%@", [retailerNameArray objectAtIndex:indexPath.row]];
            
            //distance in miles
            mileLabel.text =[NSString stringWithFormat:@"%@", [distanceArray objectAtIndex:indexPath.row]];
            
            //address1, address2 label
            
            addressLabel.text = [retailAddressArray objectAtIndex:indexPath.row];
            
            if(![[statusArray objectAtIndex:indexPath.row] isEqualToString:@"N/A"])
                statusLabel.text = [statusArray objectAtIndex:indexPath.row];
            else
                statusLabel.text = @"";
            
            //extracting city,state,zip from complete address
            
            detailLabel.text = [citistatezipArray objectAtIndex:indexPath.row];
            
            if ([[saleFlagArray objectAtIndex:indexPath.row] intValue] == 1) {
                
                specImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"retSpecialsIcon_o.png"]];
                [cell.contentView addSubview:specImage];
                // [specImage release];
            }
            
            float width = (IPAD ? 400.0f:170.0f);
            numberOfLines =[ self getLabelSize:addressLabel.text withSize:width withFont:addressFont];
        }
        
        if (specImage == nil) {
            tableViewDictionary = @{@"asyncImageView":asyncImageView,@"statusLabel":statusLabel,@"label":label,@"detailLabel":detailLabel,@"addressLabel":addressLabel,@"mileLabel":mileLabel};
            
        }
        
        asyncImageView.translatesAutoresizingMaskIntoConstraints = NO;
        label.translatesAutoresizingMaskIntoConstraints = NO;
        detailLabel.translatesAutoresizingMaskIntoConstraints = NO;
        specImage.translatesAutoresizingMaskIntoConstraints = NO;
        addressLabel.translatesAutoresizingMaskIntoConstraints = NO;
        mileLabel.translatesAutoresizingMaskIntoConstraints = NO;
        statusLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        if(specImage == nil && asyncImageView == nil && label == nil && detailLabel == nil && statusLabel == nil && mileLabel == nil && addressLabel == nil)
            tableViewDictionary = nil;
        else if(specImage == nil )
            tableViewDictionary = NSDictionaryOfVariableBindings(asyncImageView,label,detailLabel,addressLabel,mileLabel,statusLabel);
        else if(specImage == nil && asyncImageView == nil)
            tableViewDictionary = NSDictionaryOfVariableBindings(label,detailLabel,addressLabel,mileLabel,statusLabel);
        else if(specImage != nil && asyncImageView == nil)
        {
            tableViewDictionary = NSDictionaryOfVariableBindings(specImage, label,detailLabel,addressLabel,mileLabel,statusLabel);
        }
        else if(asyncImageView != nil && specImage == nil)
        {
            tableViewDictionary = NSDictionaryOfVariableBindings(asyncImageView, label,detailLabel,addressLabel,mileLabel,statusLabel);
        }
        else if(specImage != nil && asyncImageView != nil && label != nil && detailLabel != nil && statusLabel != nil && mileLabel != nil && addressLabel != nil)
            tableViewDictionary = NSDictionaryOfVariableBindings(asyncImageView,label,detailLabel,specImage,statusLabel,mileLabel,addressLabel);
        [self setConstraints:cell tag:1];
    }
    
    //view more results...
    else if (indexPath.row == [retailerNameArray count]  && nextPage == 1) {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        CGRect frame;
        
        frame.origin.x = (SCREEN_WIDTH - 40)/2;
        frame.origin.y = 5;
        frame.size.width = 40;
        frame.size.height = 40;
        loading.frame = frame;
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
        
    }
    
    
    
    return cell;
}
*/
-(void)setConstraints:(UITableViewCell*)cell tag:(CGFloat)tag
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    if(tag == 0)
    {
        viewDictionary = NSDictionaryOfVariableBindings(retailersList);
        int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
        
        if(bottomBtn == 1)
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(48)-[retailersList(%f)]",SCREEN_HEIGHT-yVal-50-48] options:0 metrics:0 views:viewDictionary]];
        else
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(48)-[retailersList(%f)]",SCREEN_HEIGHT-yVal-48] options:0 metrics:0 views:viewDictionary]];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[retailersList(%f)]",SCREEN_WIDTH] options:0 metrics:0 views:viewDictionary]];
    }
    else
    {
      if (tableViewDictionary != nil) {
            
        if (DEVICE_TYPE==UIUserInterfaceIdiomPad )
        {
            if([tableViewDictionary objectForKey:@"asyncImageView"] != nil)
            {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[asyncImageView(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(50)]"] options:0 metrics:0 views:tableViewDictionary]];
            }
            
            if([tableViewDictionary objectForKey:@"label"] != nil)
            {
                if(numberOfLines<2)
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(22)]-(1)-[addressLabel(30)]-(1)-[detailLabel(12)]"] options:0 metrics:0 views:tableViewDictionary]];
                else
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(22)]-(1)-[addressLabel(30)]-(1)-[detailLabel(12)]"] options:0 metrics:0 views:tableViewDictionary]];
                
                if([tableViewDictionary objectForKey:@"specImage"] == nil)
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[label(400)]"] options:0 metrics:0 views:tableViewDictionary]];
                else
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[label(350)]"] options:0 metrics:0 views:tableViewDictionary]];
            }
            
            if([tableViewDictionary objectForKey:@"detailLabel"] != nil)
            {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[detailLabel(300)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[addressLabel]"] options:0 metrics:0 views:tableViewDictionary]];
            }
            
            
            
            if([tableViewDictionary objectForKey:@"specImage"] != nil)
            {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[specImage(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[mileLabel(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(600)-[specImage(50)]-10-[mileLabel(100)]"] options:0 metrics:0 views:tableViewDictionary]];
            }
            else{
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[mileLabel(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(660)-[mileLabel(100)]"] options:0 metrics:0 views:tableViewDictionary]];
            }
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(24)-[statusLabel(50)]"] options:0 metrics:0 views:tableViewDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(660)-[statusLabel(50)]"] options:0 metrics:0 views:tableViewDictionary]];
            
        }
        else
        {
            if([tableViewDictionary objectForKey:@"asyncImageView"] != nil)
            {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[asyncImageView(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(40)]"] options:0 metrics:0 views:tableViewDictionary]];
            }
            
            if([tableViewDictionary objectForKey:@"label"] != nil)
            {
                if(numberOfLines<2)
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(2)-[label(15)]-(1)-[addressLabel(15)]-(1)-[detailLabel(10)]"] options:0 metrics:0 views:tableViewDictionary]];
                else
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(2)-[label(15)]-(2)-[addressLabel(25)]-(1)-[detailLabel(10)]"] options:0 metrics:0 views:tableViewDictionary]];
                
                if([tableViewDictionary objectForKey:@"specImage"] == nil)
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[label(200)]"] options:0 metrics:0 views:tableViewDictionary]];
                else
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[label(150)]"] options:0 metrics:0 views:tableViewDictionary]];
                
            }
            
            if([tableViewDictionary objectForKey:@"detailLabel"] != nil)
            {
               
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[detailLabel]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[addressLabel]"] options:0 metrics:0 views:tableViewDictionary]];
            }
            
            if([tableViewDictionary objectForKey:@"specImage"] != nil)
            {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[specImage(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[mileLabel(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(210)-[specImage(40)]-10-[mileLabel(60)]"] options:0 metrics:0 views:tableViewDictionary]];
            }
            else{
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[mileLabel(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(260)-[mileLabel(60)]"] options:0 metrics:0 views:tableViewDictionary]];
            }
            
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[statusLabel(40)]"] options:0 metrics:0 views:tableViewDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(260)-[statusLabel(40)]"] options:0 metrics:0 views:tableViewDictionary]];
            
            
        }

      }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if([retailerNameArray count]>0){
        if (indexPath.row == [retailerNameArray count]  && nextPage == 1) {
            // This is the last cell
            lastVisitedRecord = [[rowNumberArray objectAtIndex:[rowNumberArray count]-1 ]intValue];
            self.view.userInteractionEnabled = false;
            if (![defaults boolForKey:@"ViewMore"] && lastVisitedRecord !=0) {
                [defaults setBool:YES forKey:@"ViewMore"];
                
                dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
                dispatch_async(dispatchQueue, ^(void){
                    fetchNext = true;
                    [self request_getRetailersInfo:lastVisitedRecord];
                    
                    //[self fetchNextPage:self];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        fetchNext = false;
                        [loading stopAnimating];
                        [retailersList reloadData];
                        [HubCitiAppDelegate removeActivityIndicator];
                        //[retailersList performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
                    });
                });
             
            }
            
        }
    }
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    [defaults setBool:NO forKey:@"ViewMore"];
    nextPage=0;
    selectedIndexPath = indexPath;
    
    [defaults  setObject:[retailLocationIDArray objectAtIndex:indexPath.row]
                  forKey:@"retailLocationID"];
    
    [defaults  setObject:[retailerIdArray objectAtIndex:indexPath.row]
                  forKey:KEY_RETAILERID];
    
    
    [defaults  setObject:[ribbonAdImagePathArray objectAtIndex:indexPath.row]
                  forKey:@"ribbonAdImagePath"];
    
    [defaults setObject:[retailerNameArray objectAtIndex:indexPath.row]
                 forKey:KEY_RTLNAME];
    
    //@Deepak send RetListID to next controller
    [defaults setObject:[retListIDArray objectAtIndex:indexPath.row]
                 forKey:KEY_RLISTID];
    
    [defaults setValue:@"" forKey:@"AdUrl"];
    for (int i=0; i<[retailerLatArray count]; i++) {
        [globalNearByLatArray addObject:[retailerLatArray objectAtIndex:i]];
    }
    for (int j =0 ; j<[retailerLongArray count]; j++) {
        [globalNearByLongArray addObject:[retailerLongArray objectAtIndex:j]];
    }
    for (int i=0; i<[retailerNameArray count]; i++) {
        [globalRetailerNameArray addObject:[retailerNameArray objectAtIndex:i]];
    }
    for (int j=0; j<[retailAddressArray count]; j++) {
        [globalRetailerAddressArray addObject:[retailAddressArray objectAtIndex:j]];
    }
    for (int j=0; j<[retailLocationIDArray count]; j++) {
        [globalRetLocId addObject:[retailLocationIDArray objectAtIndex:j]];
    }
    for (int j=0; j<[retailerIdArray count]; j++) {
        [globalRetailerID addObject:[retailerIdArray objectAtIndex:j]];
    }
    for (int j=0; j<[retListIDArray count]; j++) {
        [globalRetailerListID addObject:[retListIDArray objectAtIndex:j]];
    }
    
    [self displayCategoryInfo:indexPath];
    [retailersList deselectRowAtIndexPath:[retailersList indexPathForSelectedRow] animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float width = (IPAD ? 400.0f:170.0f);
    float noOflines = 0;
    if([retailAddressArray count]>0){
        if (indexPath.row != [retailerNameArray count])
            noOflines = [self getLabelSize: [retailAddressArray objectAtIndex:indexPath.row] withSize:width withFont:addressFont];
    }
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone){
        if(noOflines<2)
            return 50.0;
        else
            return 65.0;
    }
    else
    {
        return 85.0;
    }
}
- (void)displayCategoryInfo:(id)indexPath1
{
    iWebRequestState = GETRETSUMMARY;
    ipath = indexPath1;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[globalRetLocId objectAtIndex:ipath.row]];
    [defaults setValue:[globalRetLocId objectAtIndex:ipath.row] forKey:@"retailLocationID"];
    
    // @Deepak: Added this code for "getretsummary" in UserTracking
    [requestStr appendFormat:@"<scanTypeId>%@</scanTypeId>",@"0"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if([globalRetailerListID count] > ipath.row)
    {
        [requestStr appendFormat:@"<retListId>%@</retListId>",[globalRetailerListID objectAtIndex:ipath.row]];
        [defaults setObject:[retListIDArray objectAtIndex:ipath.row] forKey:KEY_RLISTID];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    else if ( [defaults  valueForKey:@"postalCode"] && (![[defaults  valueForKey:@"postalCode"] isEqualToString:@""]))
        [requestStr appendFormat:@"<postalCode>%@</postalCode>",[defaults  valueForKey:@"postalCode"]];
    
    
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",[globalRetailerID objectAtIndex:ipath.row]];
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    
    DLog(@"requestStr = %@",requestStr);
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
 
    
    
}





-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}




-(void) updatedLatlongValue :(NSString *) latValue longitudeValue:(NSString *)longValue; {
    
    [defaults setBool:YES forKey:@"gpsEnabled"];
    [defaults setObject:latValue forKey:KEY_LATITUDE];
    [defaults setObject:longValue forKey:KEY_LONGITUDE];
    
    //Added the setting for these keys because its what the Map is using.
    [defaults  setValue:latValue forKey:@"LatVal"];
    [defaults  setValue:longValue forKey:@"LongVal"];
    
    //[self toolbarDonePressed:nil];
    [self refreshArrays];
    lastVisitedRecord = 0;
    [self addToTable:lastVisitedRecord];
    //[retailersList reloadData];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma-mark Request Methods

-(void)request_fetchlatlong
{
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendFormat:@"thislocation/fetchlatlong?zipcode=%@",[defaults  valueForKey:@"ZipCode_TL"]];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
    {
        ReleaseAndNilify(responseXml);
        return;
    }
    
    //NSlog(@"RESPONSE - %@",responseXml);
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement *responseElement = [TBXML childElementNamed:@"responsCode" parentElement:tbxml.rootXMLElement];
    if (responseElement == nil) {
        TBXMLElement *latElement = [TBXML childElementNamed:@"Latitude" parentElement:tbxml.rootXMLElement];
        TBXMLElement *longElement = [TBXML childElementNamed:@"Longitude" parentElement:tbxml.rootXMLElement];
        if (latElement != nil && longElement != nil)
        {
            [defaults setValue:[TBXML textForElement:latElement] forKey:KEY_LATITUDE];
            [defaults setValue:[TBXML textForElement:longElement] forKey:KEY_LONGITUDE];
        }
        
    }
    else {
        [UtilityManager showAlert:@"Error" msg:[TBXML textForElement:responseElement]];
          }
    // [responseXml release];
    
}


-(void) request_getRetailersInfo:(int)lastVisitedRec
{
    BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    // If GPS Not allowed but ZIP code is available
    if(lsFlag == NO && zipCodeStatus==1)
    {
        //        [self request_fetchlatlong];
    }
    
    NSMutableDictionary* param = [[NSMutableDictionary alloc] init];
    
    iWebRequestState = GETRETAILERSINFO;
    
    
    
    [param setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [timeFormatter setTimeZone:timeZone];
    [timeFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *timeInUTC = [timeFormatter stringFromDate:currentTime];
    NSLog(@"Current time %@", timeInUTC);
    
    [param setValue:timeInUTC forKey:@"requestedTime"];
    
    if (lsFlag == NO)
    {
         [param setValue:@"false" forKey:@"gpsEnabled"];
    }
    
    else
    {
        [param setValue:@"true" forKey:@"gpsEnabled"];
    }
    
    [param setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    [param setValue:@"true" forKey:@"locOnMap"];
    
   
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length] && [defaults  boolForKey:@"gpsEnabled"]){
        
        [param setValue:[defaults  valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        [param setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE_TL]length]){
        
        [param setValue:[defaults  valueForKey:KEYZIPCODE_TL] forKey:@"zipcode"];
        
    }
    
    
    if([defaults valueForKey:KEY_MAINMENUID])
         [param setValue:[defaults  valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    
    if (![defaults boolForKey:BottomButton]) {
        if([defaults valueForKey:KEY_MITEMID])
            [param setValue:[defaults  valueForKey:KEY_MITEMID] forKey:@"mItemId"];
        
        
    }
    
    else{
        if ([NearbyBottomButtonID count]>=1)
        {
            [param setValue:[NearbyBottomButtonID objectAtIndex:[NearbyBottomButtonID count]-1] forKey:@"bottomBtnId"];
        }
        
        else if([defaults valueForKey:KEY_BOTTOMBUTTONID])
        {
            [param setValue:[defaults valueForKey:KEY_BOTTOMBUTTONID]forKey:@"bottomBtnId"];
        }
        
    }
    
 
        if([segment selectedSegmentIndex] == 0)
        {
            [param setValue:@"ASC"forKey:@"sortOrder"];
            [param setValue:@"distance"forKey:@"sortColumn"];
           
        }
        else if([segment selectedSegmentIndex] == 1){
            [param setValue:@"ASC"forKey:@"sortOrder"];
            [param setValue:@"atoz"forKey:@"sortColumn"];
            
        }
    [param setValue:[NSString stringWithFormat:@"%d",lastVisitedRec] forKey:@"lastVisitedRecord"];
    [param setValue:@"IOS" forKey:@"platform"];
    NSLog(@"param: %@", param);
    
  
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getretailersinfojson",BASE_URL];
    NSLog(@"The urlString is %@",urlString);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    if (![defaults boolForKey:@"ViewMore"]) {
        [HubCitiAppDelegate showActivityIndicator];
    }
    else{
        [HubCitiAppDelegate removeActivityIndicator];
    }
    
    [client sendRequest : param : urlString];
    
  /*  BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    // If GPS Not allowed but ZIP code is available
    if(lsFlag == NO && zipCodeStatus==1)
    {
        //        [self request_fetchlatlong];
    }
    
    iWebRequestState = GETRETAILERSINFO;
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<ThisLocationRequest><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    NSString* timeZone = [self getUTCFormateDate:[NSDate date]];
    
    [xmlStr appendFormat:@"<requestedTime>%@</requestedTime>",timeZone];
    
    if (lsFlag == NO)
        [xmlStr appendString:@"<gpsEnabled>false</gpsEnabled>"];
    else
        [xmlStr appendString:@"<gpsEnabled>true</gpsEnabled>"];
    
    
    [xmlStr appendFormat:@"<userId>%@</userId><locOnMap>true</locOnMap>", [defaults valueForKey:KEY_USERID]];
    
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length] && [defaults  boolForKey:@"gpsEnabled"]){
        
        [xmlStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE_TL]length]){
        
        [xmlStr appendFormat:@"<zipcode>%@</zipcode>",[defaults valueForKey:KEYZIPCODE_TL]];
    }
    
    
    
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [xmlStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    if (![defaults boolForKey:BottomButton]) {
        if([defaults valueForKey:KEY_MITEMID])
            [xmlStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    
    else{
        if ([NearbyBottomButtonID count]>=1)
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[NearbyBottomButtonID objectAtIndex:[NearbyBottomButtonID count]-1]];
        
        
        else if([defaults valueForKey:KEY_BOTTOMBUTTONID])
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    /*  if ([defaults valueForKey:@"NearBySort"]==nil) {
     [xmlStr appendFormat:@"<sortOrder>ASC</sortOrder><sortColumn>distance</sortColumn>"];
     }
     else
     {
     [xmlStr appendFormat:@"<sortOrder>ASC</sortOrder><sortColumn>%@</sortColumn>",[defaults valueForKey:@"NearBySort"]];
     }*/
    
    /*if([defaults valueForKey:@"isComingFromGroupingandSorting"])
     {
     [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
     if (sortFilObj.localSpecialSelected) {
     [xmlStr appendFormat:@"<locSpecials>%@</locSpecials>",@"1"];
     }
     else{
     [xmlStr appendFormat:@"<locSpecials>%@</locSpecials>",@"0"];
     }
     if (sortFilObj.selectedCatIds) {
     [xmlStr appendFormat:@"<subCatIds>%@</subCatIds>",sortFilObj.selectedCatIds];
     }
     if (sortFilObj.selectedOptIds) {
     [xmlStr appendFormat:@"<filterId>%@</filterId>",sortFilObj.selectedOptIds];
     }
     if (sortFilObj.selectedOthersIds) {
     [xmlStr appendFormat:@"<fValueId>%@</fValueId>",sortFilObj.selectedOthersIds];
     }
     if (sortFilObj.selectedCitiIds) {
     [xmlStr appendFormat:@"<cityIds>%@</cityIds>",sortFilObj.selectedCitiIds];
     }
     if (sortFilObj.selectedInterestsIds) {
     [xmlStr appendFormat:@"<interests>%@</interests>",sortFilObj.selectedInterestsIds];
     }
     
     }
     else{*/
    
    
    /*if([segment selectedSegmentIndex] == 0)
    {
        [xmlStr appendFormat:@"<sortOrder>ASC</sortOrder><sortColumn>distance</sortColumn>"];
    }
    else if([segment selectedSegmentIndex] == 1){
        [xmlStr appendFormat:@"<sortOrder>ASC</sortOrder><sortColumn>atoz</sortColumn>"];
    }
    //}
    
    
    
    [xmlStr appendFormat:@"<lastVisitedRecord>%d</lastVisitedRecord></ThisLocationRequest>", lastVisitedRec];
    
    NSLog(@"param: %@", xmlStr);
    // NSString *urlString = @" http://sdw2107:8080/HubCiti1.3/thislocation/getretailersinfo";
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getretailersinfo",BASE_URL];
    NSLog(@"The urlString is %@",urlString);
    //[defaults setBool:NO forKey:@"locOnMap"];
    
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
        
        dispatch_async (dispatch_get_main_queue(), ^{
            [self parseRetailerList:response];
            
        });
        
        
        
    }
    else{
        
        [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
        
        
    }*/
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    for (NSOperation *operation in [HTTPClient sharedHTTPClient].manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        NSLog(@"operation:%@",operation);
        AFHTTPRequestOperation *operations = (AFHTTPRequestOperation*)operation;
        if([[[[operations request] URL] absoluteString] containsString:@"thislocation/getretailersinfojson"]){
            [operation cancel];
        }
        
    }
    
    //   [[HTTPClient sharedHTTPClient].manager.operationQueue cancelAllOperations];
    
}

//-(void)request_GetUserCat{
//
//    iWebRequestState = gETFAVCATEGORIE;
//    if([Network currentReachabilityStatus]==0)
//    {
//        [common showAlert];
//    }
//    else {
//
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//
//    }
//}


-(void)requestToGetPreferredLocations
{
    iWebRequestState= gETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}


-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInf;
    requestAnythingFromMainMenu=FALSE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = Appsitedetail;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    else if ([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}

//in case of multiple partners
//-(void)request_GetPartners{
//
//    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getpartners?citiExperId=%@",BASE_URL,[defaults valueForKey:KEY_LINKID]];
//
//    iWebRequestState = gETPARTNER;
//    [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
//
//}




#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
        
    }
}

-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = bottomBtnView;
        actionSheet.popoverPresentationController.sourceRect = bottomBtnView.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}


-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)backClicked{
    
    [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
}

//-(void)popBackToPreviousPage{
//    if(isOnlyOnePartner)
//    {
//        isOnlyOnePartner = NO;
//        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
//    }
//    else
//        [self.navigationController popViewControllerAnimated:NO];
//}

#pragma-mark parse methods


-(void) parse_retSummary :(NSString *)response{
    [HubCitiAppDelegate removeActivityIndicator];
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
        return;
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        
        RetailerSummaryViewController *retailerSummaryPage = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        
        retailerSummaryPage.distanceFromPreviousScreen = [distanceArray objectAtIndex:ipath.row];
        [self.navigationController pushViewController:retailerSummaryPage animated:NO];
        
    }
    else
    {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
}
-(void)parseRetailerList:(NSString *)response{
    self.view.userInteractionEnabled = true;
   
    if(nextPage == 0  && lastVisitedRecord == 0)
    {
        [self refreshArrays];
        
         dispatch_async(dispatch_get_main_queue(), ^{
        [self settableViewOnScreen];
         });
        [retailersList setContentOffset:CGPointMake(0, 0)];
    }
    if (fetchNext == false) {
       // [self refreshArrays];
    }
    
    NSDictionary* responseData = (NSDictionary*) response;
   
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    
    bottomBtn = [[responseData objectForKey:@"bottomBtn"] intValue];
    
    if (responseCode == 10000) {
        
       
        
        nextPage =  [[responseData objectForKey:@"nextPage"] intValue];
        
        [defaults setValue:[responseData objectForKey:KEY_MAINMENUID] forKey:KEY_MAINMENUID];
        
        NSArray* retailerElement = [responseData objectForKey:@"retailers"];
        
        for(int i = 0; i< retailerElement.count; i++){
            
             [rowNumberArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"rowNumber"]];
             [retListIDArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"retListId"]];
             [retailerIdArray addObject:[[retailerElement objectAtIndex:i] objectForKey:KEY_RETAILERID]];
             [retailerNameArray addObject:[[retailerElement objectAtIndex:i] objectForKey:KEY_RTLNAME]];
             [distanceArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"distance"]];
            NSString* add1 = [[retailerElement objectAtIndex:i] objectForKey:@"retailAddress1"];
            
            NSString* add2 = [[retailerElement objectAtIndex:i] objectForKey:@"retailAddress2"];
            
            
            if ([add2 isEqualToString:@"N/A"] || add2.length == 0 ) {
                [retailAddressArray addObject:add1];
            }
            
            else{
                NSString* add3 = [NSString stringWithFormat:@"%@, %@",add1,add2];
                [retailAddressArray addObject:add3];
            }
           
            [statusArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"locationOpen"]];
            
            NSString* citi = [[retailerElement objectAtIndex:i] objectForKey:@"city"];
            NSString* state = [[retailerElement objectAtIndex:i] objectForKey:@"state"];
            NSString* zip = [[retailerElement objectAtIndex:i] objectForKey:@"postalCode"];
          
            if (citi.length > 0 && state.length > 0 && zip.length > 0) {
                NSString* citistatezip = [NSString stringWithFormat:@"%@, %@, %@",citi,state,zip];
                [citistatezipArray addObject:citistatezip];
            }
                      
            [retailLocationIDArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"retailLocationId"]];
            [retailerLatArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"latitude"]];
            [retailerLongArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"longitude"]];
           
            
            [logoImagePathArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"logoImagePath"]];
            [bannerAdImagePathArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"bannerAdImagePath"]];
            [ribbonAdImagePathArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"ribbonAdImagePath"]];
            [ribbonAdURLArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"ribbonAdURL"]];
            [saleFlagArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"saleFlg"]];
          
        }
        
        if (bottomBtn==1) {
            
            bottomButtonList = [[NSMutableArray alloc]init];
            
            NSArray* bottomArray = [responseData objectForKey:@"bottomBtnList"];
            
            for(int i = 0; i< bottomArray.count; i++){
                
                bottomButtonDO *bottomButton = [[bottomButtonDO alloc]init];
                
                bottomButton.bottomBtnID  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnID"];
                
                bottomButton.bottomBtnName  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnName"];
                
                bottomButton.bottomBtnImg  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImg"];
                
                bottomButton.bottomBtnImgOff  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImgOff"];
                
                bottomButton.btnLinkTypeID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeID"];
                
                bottomButton.btnLinkTypeName  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeName"];
                
                bottomButton.btnLinkID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkID"];
                
                bottomButton.position  = [[bottomArray objectAtIndex:i] objectForKey:@"position"];
                
                [bottomButtonList addObject:bottomButton];
                
                
                
            }
            
        }
        dispatch_async (dispatch_get_main_queue(), ^{
            if (!fetchNext) {
                [retailersList reloadData];
                [loading stopAnimating];
            }
            if ([bottomButtonList count]>0) {
                
                [self setBottomBarMenu:bottomButtonList];
            }
        });
        
    }
    
    else if(responseCode == 10005)
    {
        
        if (bottomBtn==1) {
            
            bottomButtonList = [[NSMutableArray alloc]init];
            
            NSArray* bottomArray = [responseData objectForKey:@"bottomBtnList"];
            
            for(int i = 0; i< bottomArray.count; i++)  {
                
                bottomButtonDO *bottomButton = [[bottomButtonDO alloc]init];
                
                bottomButton.bottomBtnID  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnID"];
                
                bottomButton.bottomBtnName  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnName"];
                
                bottomButton.bottomBtnImg  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImg"];
                
                bottomButton.bottomBtnImgOff  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImgOff"];
                
                bottomButton.btnLinkTypeID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeID"];
                
                bottomButton.btnLinkTypeName  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeName"];
                
                bottomButton.btnLinkID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkID"];
                
                bottomButton.position  = [[bottomArray objectAtIndex:i] objectForKey:@"position"];
                NSString* filter  = [[bottomArray objectAtIndex:i] objectForKey:@"Filters"];
                [SharedManager setRetGroupId: [filter intValue]];
            
                [bottomButtonList addObject:bottomButton];
                
            }
            
            if([bottomButtonList count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self setBottomBarMenu:bottomButtonList];
                });
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            
        }
     
        if([responseText isEqualToString:@"No Records Found."])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseText];
            [self settableViewOnScreen];
            [retailersList reloadData];
            });

            return;
        
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
         [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseText];
         [loading stopAnimating];
         ReleaseAndNilify(loading);
     });

    }
    
    [defaults setBool:NO forKey:@"ViewMore"];
    dispatch_async(dispatch_get_main_queue(), ^{
    [retailersList reloadData];
    });
    
    
   /* self.view.userInteractionEnabled = true;
    ////NSlog(@"The response is %@",response);
    if(nextPage == 0  && lastVisitedRecord == 0)
    {
        [self refreshArrays];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self settableViewOnScreen];
        });
        [retailersList setContentOffset:CGPointMake(0, 0)];
    }
    if (fetchNext == false) {
        [self refreshArrays];
    }
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *retmainMenuIDElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        
        if(retmainMenuIDElement!=nil)
            [defaults setObject:[TBXML textForElement:retmainMenuIDElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbxml.rootXMLElement];
        
        if (nextPageElement == nil) {
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
            
            if (responseTextElement!=nil)
                [UtilityManager showAlert:@"Sorry" msg:@"No retailers found in your area"];
            
            // [tbxml release];
            nextPage = 0;
            return;
        }
        
        NSString *nextPageStr = [TBXML textForElement:nextPageElement];
        nextPage = [nextPageStr intValue];
        
        TBXMLElement *retailers = [TBXML childElementNamed:@"retailers" parentElement:tbxml.rootXMLElement];
        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbxml.rootXMLElement]]intValue];
        
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"Retailer" parentElement:retailers];
        
        while (retailerDetailElement != nil) {
            TBXMLElement *rowNumberElement = [TBXML childElementNamed:@"rowNumber" parentElement:retailerDetailElement];
            TBXMLElement *retailerIdElement = [TBXML childElementNamed:KEY_RETAILERID parentElement:retailerDetailElement];
            TBXMLElement *retailerNameElement = [TBXML childElementNamed:KEY_RTLNAME parentElement:retailerDetailElement];
            TBXMLElement *retailerAddress1 = [TBXML childElementNamed:@"retailAddress1" parentElement:retailerDetailElement];
            TBXMLElement *retailerAddress2 = [TBXML childElementNamed:@"retailAddress2" parentElement:retailerDetailElement];
            TBXMLElement *retailLocationIDElement = [TBXML childElementNamed:@"retailLocationId" parentElement:retailerDetailElement];
            TBXMLElement *distanceElement = [TBXML childElementNamed:KEY_DISTANCE parentElement:retailerDetailElement];
            TBXMLElement *retLatitudeElement = [TBXML childElementNamed:@"latitude" parentElement:retailerDetailElement];
            TBXMLElement *retLongitudeElement = [TBXML childElementNamed:@"longitude" parentElement:retailerDetailElement];
            // TBXMLElement *retailAddressElement = [TBXML childElementNamed:@"retailAddress" parentElement:retailerDetailElement];
            TBXMLElement *logoImagePathElement = [TBXML childElementNamed:@"logoImagePath" parentElement:retailerDetailElement];
            TBXMLElement *bannerAdImagePathElement = [TBXML childElementNamed:@"bannerAdImagePath" parentElement:retailerDetailElement];
            TBXMLElement *ribbonAdImagePathElement = [TBXML childElementNamed:@"ribbonAdImagePath" parentElement:retailerDetailElement];
            TBXMLElement *ribbonAdURLElement = [TBXML childElementNamed:@"ribbonAdURL" parentElement:retailerDetailElement];
            TBXMLElement *citi = [TBXML childElementNamed:@"city" parentElement:retailerDetailElement];
            TBXMLElement *state = [TBXML childElementNamed:@"state" parentElement:retailerDetailElement];
            TBXMLElement *zip = [TBXML childElementNamed:@"postalCode" parentElement:retailerDetailElement];
            TBXMLElement *status = [TBXML childElementNamed:@"locationOpen" parentElement:retailerDetailElement];
            
            TBXMLElement *saleFlagElement = [TBXML childElementNamed:@"saleFlg" parentElement:retailerDetailElement];
            
            //@Deepak : Parse and add retListID into UserDefault
            TBXMLElement *retListID = [TBXML childElementNamed:@"retListId" parentElement:retailerDetailElement];
            if( retListID != nil)
            {
                [retListIDArray addObject:[TBXML textForElement:retListID]];
                
            }
            
            if (rowNumberElement != nil)
            {
                [rowNumberArray addObject:[TBXML textForElement:rowNumberElement]];
            }
            
            if (retailerIdElement != nil) {
                [retailerIdArray addObject:[TBXML textForElement:retailerIdElement]];
            }
            
            if (retailerNameElement != nil) {
                [retailerNameArray addObject:[TBXML textForElement:retailerNameElement]];
            }
            
            if (retailerAddress1 !=nil) {
                
                NSString* add1 = [TBXML textForElement:retailerAddress1];
                NSString* add2;
                
                if (retailerAddress2 != nil) {
                    add2 = [TBXML textForElement:retailerAddress2];
                }
                
                if ([add2 isEqualToString:@"N/A"] || add2.length == 0) {
                    [retailAddressArray addObject:add1];
                }
                else{
                    NSString* add3 = [NSString stringWithFormat:@"%@, %@",add1,add2];
                    [retailAddressArray addObject:add3];
                }
                
                
            }
            if (status != nil) {
                NSString * statusText = [TBXML textForElement:status];
                
                [statusArray addObject:statusText];
                
            }
            if (citi != nil && state != nil && zip != nil) {
                NSString* citistatezip = [NSString stringWithFormat:@"%@, %@,%@",[TBXML textForElement:citi],[TBXML textForElement:state],[TBXML textForElement:zip]];
                [citistatezipArray addObject:citistatezip];
            }
            
            if (retailLocationIDElement != nil) {
                
                [retailLocationIDArray addObject:[TBXML textForElement:retailLocationIDElement]];
            }
            
            if (logoImagePathElement != nil) {
                [logoImagePathArray addObject:[TBXML textForElement:logoImagePathElement ]];
            }
            
            
            if (distanceElement != nil) {
                [distanceArray addObject:[TBXML textForElement:distanceElement]];
            }
            if (retLatitudeElement!=nil) {
                [retailerLatArray addObject:[TBXML textForElement:retLatitudeElement]];
            }
            if (retLongitudeElement!=nil) {
                [retailerLongArray addObject:[TBXML textForElement:retLongitudeElement]];
            }
            
            //            if (retailAddressElement != nil) {
            //
            //                [retailAddressArray addObject:[TBXML textForElement:retailAddressElement]];
            //            }
            
            if (bannerAdImagePathElement != nil) {
                
                [bannerAdImagePathArray addObject:[TBXML textForElement:bannerAdImagePathElement ]];
            }
            
            if (ribbonAdImagePathElement != nil) {
                [ribbonAdImagePathArray addObject:[TBXML textForElement:ribbonAdImagePathElement ]];
            }
            
            if (ribbonAdURLElement != nil) {
                [ribbonAdURLArray addObject:[TBXML textForElement:ribbonAdURLElement ]];
            }
            if (saleFlagElement != nil) {
                [saleFlagArray addObject:[TBXML textForElement:saleFlagElement ]];
            }
            
            retailerDetailElement = [TBXML nextSiblingNamed:@"Retailer" searchFromElement:retailerDetailElement];
        }
        
        
        //        NSRegularExpression *regex = [NSRegularExpression
        //                                      regularExpressionWithPattern:@"<bottomBtnList>"
        //                                      options:NSRegularExpressionCaseInsensitive
        //                                      error:nil];
        //
        //        NSArray *matches = [regex matchesInString:response
        //                                          options:0
        //                                            range:NSMakeRange(0, [response length])];
        //
        //        if ([matches count]==1) {
        
        if(bottomBtn==1){
            bottomButtonList = [[NSMutableArray alloc]init];
            
            TBXMLElement *bottomList = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbxml.rootXMLElement];
            
            TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomList];
            
            while (BottomButtonElement != nil) {
                
                bottomButtonDO *bottomButton = [[bottomButtonDO alloc]init];
                
                TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                
                if(bottomBtnIDElement)
                    bottomButton.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                
                if(bottomBtnNameElement)
                    bottomButton.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                else
                    bottomButton.bottomBtnName = @" ";
                
                if(bottomBtnImgElement)
                    bottomButton.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                else
                    bottomButton.bottomBtnImg = @" ";
                
                if(bottomBtnImgOffElement)
                    bottomButton.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                else
                    bottomButton.bottomBtnImgOff = @" ";
                
                if(btnLinkTypeIDElement)
                    bottomButton.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                
                if(btnLinkTypeNameElement)
                    bottomButton.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                
                if(btnLinkIDElement){
                    bottomButton.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                    
                }
                
                if(positionElement)
                    bottomButton.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                
                
                
                BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                [bottomButtonList addObject:bottomButton];
                //[bottomButton release];
            }
        }
        
        
        dispatch_async (dispatch_get_main_queue(), ^{
            if (!fetchNext) {
                [retailersList reloadData];
                [loading stopAnimating];
            }
            if ([bottomButtonList count]>0) {
                
                [self setBottomBarMenu:bottomButtonList];
            }
        });
        
        
        
        
    }
    
    //Added by Keshava to provide bottom buttom features
    // Added outside the if-else so that bottom buttons are displayed
    // even if no records are found.
    else if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10005"])
    {
        
        
        //@Deepak: Parsed MainMenuID for userTracking
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbxml.rootXMLElement]]intValue];
        if(mainMenuIdElement !=nil)
        {
            [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        if(bottomBtn==1){
            
            TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbxml.rootXMLElement];
            
            if(bottomButtonListElement !=nil)
            {
                TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
                bottomButtonList=[[NSMutableArray alloc]init];
                while(BottomButtonElement)
                {
                    
                    bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                    
                    
                    TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                    TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                    
                    
                    if(bottomBtnIDElement)
                        obj_findBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                    
                    if(bottomBtnNameElement)
                        obj_findBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                    else
                        obj_findBottomDO.bottomBtnName = @" ";
                    
                    if(bottomBtnImgElement)
                        obj_findBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                    else
                        obj_findBottomDO.bottomBtnImg = @" ";
                    
                    if(bottomBtnImgOffElement)
                        obj_findBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                    else
                        obj_findBottomDO.bottomBtnImgOff = @" ";
                    
                    if(btnLinkTypeIDElement)
                        obj_findBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                    
                    if(btnLinkTypeNameElement)
                        obj_findBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                    
                    if(btnLinkIDElement){
                        obj_findBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                        if(btnLinkTypeNameElement){
                            if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
                                [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
                        }
                    }
                    
                    if(positionElement)
                        obj_findBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                    
                    
                    
                    BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                    [bottomButtonList addObject:obj_findBottomDO];
                    //[obj_findBottomDO release];
                }
                if([bottomButtonList count] > 0)
                {
                    // Set Bottom Menu Button after read from response data
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self setBottomBarMenu:bottomButtonList];
                    });
                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                }
                else
                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            }
        }
        
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        dispatch_async(dispatch_get_main_queue(), ^{
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
            [self settableViewOnScreen];
            [retailersList reloadData];
        });
        
    }
    
    
    
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        dispatch_async(dispatch_get_main_queue(), ^{
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
            [loading stopAnimating];
            ReleaseAndNilify(loading);
        });
        
        
    }
    
    [defaults setBool:NO forKey:@"ViewMore"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [retailersList reloadData];
    });*/

}


-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}




-(void)parse_CitiExpRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [defaults setObject:nil forKey:@"Response_Austin"];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Austin"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:100 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
        }
    }
}

//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            // NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                              [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage =[[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}

#pragma-mark Bottom Button Methods

-(void) setBottomBarMenu : (NSMutableArray *)bottomList
{
    NSMutableArray *arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [bottomList count]; btnLoop++)
    {
        bottomButtonDO *obj_bottomButtonDO = [bottomList objectAtIndex:btnLoop];
        
        bottomBtnView = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            bottomBtnView.contentMode = UIViewContentModeCenter;
            bottomBtnView = [bottomButtonView initWithTitle:obj_bottomButtonDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, 80,bottomBarButtonHeight) imageName:obj_bottomButtonDO.bottomBtnImg img_off:obj_bottomButtonDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            bottomBtnView = [bottomButtonView initWithTitle:obj_bottomButtonDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[bottomList count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[bottomList count],bottomBarButtonHeight) imageName:obj_bottomButtonDO.bottomBtnImg img_off:obj_bottomButtonDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [bottomBtnView setAccessibilityLabel:obj_bottomButtonDO.btnLinkTypeName];
        [bottomBtnView setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:bottomBtnView];
        [self.view addSubview:bottomBtnView];
        [self.view bringSubviewToFront:bottomBtnView];
    }
    
}

-(void)bottomButtonPressed:(id)sender
{
    [defaults setBool:NO forKey:@"ViewMore"];
    [defaults setBool:YES forKey:BottomButton];
    //   [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    //NSlog(@"The tag is %d",tag);
    bottomButtonDO *obj_bottomBottomDO ;
    obj_bottomBottomDO = [bottomButtonList objectAtIndex:tag];
    if ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=26||[[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=27||[[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=28)
        [defaults setValue:obj_bottomBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_bottomBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_bottomBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_bottomBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_bottomBottomDO.btnLinkTypeName);
    if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            self.iEventsListViewController = iEventsListViewController;
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    
    else{ //other menu types
        switch ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                //[hotDeals release];
                break;
                
            case 1://Scan Now
            {
                //[self navigateToScanNow];
            }
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                [EventsBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                //[self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                [self navigateToFindView];
            }
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
            {
                
            }
                
                break;
                
            case 8://Visitors Bureau
            {
                
            }
                
                break;
                
            case 9://transportation
            {
            }
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=TRUE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_bottomBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];;
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    self.filterList = filterList;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                //[dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                self.faqList = faqCatList;
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                [FundraiserBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }break;
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETUSERINF;
                    [self requestToGetUserData];
                }
            }break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = gETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        // [inform release];
                    }
                    
                }
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!")  msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            // [citPref release];
                        }
                    }
                }
                
            }break;
                
            case 26://Map
            {
                [segment setSelectedSegmentIndex:2];
                ReleaseAndNilify(retailersList);
                showMapFlag=TRUE;
                nextPage=0;
                fromNearBy=TRUE;
                fromFind=FALSE;
                fromFilters=FALSE;
                //added
                if ([RegionApp isEqualToString:@"1"]){
                    fromRegionFind = FALSE;//from region app
                }
                
                
                if ([retailerLatArray count] == 0 || retailerLatArray == nil || [retailerLongArray count] == 0 || retailerLongArray == nil)
                {
                    return;
                }
                
                for (int i=0; i<[retailerLatArray count]; i++) {
                    [globalNearByLatArray addObject:[retailerLatArray objectAtIndex:i]];
                }
                for (int j =0 ; j<[retailerLongArray count]; j++) {
                    [globalNearByLongArray addObject:[retailerLongArray objectAtIndex:j]];
                }
                for (int i=0; i<[retailerNameArray count]; i++) {
                    [globalRetailerNameArray addObject:[retailerNameArray objectAtIndex:i]];
                }
                for (int j=0; j<[retailAddressArray count]; j++) {
                    [globalRetailerAddressArray addObject:[retailAddressArray objectAtIndex:j]];
                }
                for (int j=0; j<[retailAddressArray count]; j++) {
                    [globalRetailerAddressArray addObject:[citistatezipArray objectAtIndex:j]];
                }
                for (int j=0; j<[retailLocationIDArray count]; j++) {
                    [globalRetLocId addObject:[retailLocationIDArray objectAtIndex:j]];
                }
                for (int j=0; j<[retailerIdArray count]; j++) {
                    [globalRetailerID addObject:[retailerIdArray objectAtIndex:j]];
                }
                for (int j=0; j<[retListIDArray count]; j++) {
                    [globalRetailerListID addObject:[retListIDArray objectAtIndex:j]];
                }
                
                if ([globalRetailerNameArray  count] > 0) {
                    
                    RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                    self.retailerListScreen = retailerListScreen;
                    [self.navigationController pushViewController:retailerListScreen animated:NO];
                    //[retailerListScreen release];
                    
                }
            }
                break;
                
            case 27:
            case 28://Sort/Filter
            {
                nextPage=0;
                fromNearBy=TRUE;
                fromFind=FALSE;
                fromFilters=FALSE;
                FindOptionsSortViewController *findSort=[[FindOptionsSortViewController alloc]initWithNibName:@"FindOptionsSortViewController" bundle:Nil];
                //[defaults setInteger:100 forKey:@"CityExpTableTag"];
                [self.navigationController pushViewController:findSort animated:YES];
                
                
               /* [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
                
                // [defaults setValue:[defaults valueForKey:@"eventsMitemId"] forKey:KEY_MITEMID];
                
                SwipesViewController *iSwipeViewController1 = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                iSwipeViewController1.module = @"thislocation";//Find All , Find Single , Events, CitiEXP
                
                [self.navigationController pushViewController:iSwipeViewController1 animated:NO];*/
            }
                
                break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                //privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
            default:
                break;
        }
    }
    
}

//-(void)requestToGetUserData
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
//    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//    ReleaseAndNilify(xmlStr);
//}


-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}
-(void)parseGetUserData:(id)response
{
   
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}

#pragma -mark bottom button press methods
-(void)navTonextPage
{
    [self navigateToCityExperience];
    
}

-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}

-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=FALSE;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}


-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    self.iEventsListViewController = iEventsListViewController;
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}





//-(void) navigateToScanNow
//{
//    [defaults setBool:YES forKey:@"KEY_SCANSPLASH"];
//    [defaults setValue:@"scanNow" forKey:KEY_SEARCHFROM];
//    [defaults setObject:@"YES" forKey:@"KEY_SCANNERCALLED"];
//    
//    int menulevel = [[defaults valueForKey:KEY_MENULEVEL]integerValue];
//    if(menulevel > 1)
//        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
//    else
//        [defaults  setBool:NO forKey:@"DisplayBackBarButton"];
//    
//    
//    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
//    ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];
//    [self.navigationController pushViewController:viewScanNowScreen animated:NO];
//    [viewScanNowScreen release];
//    
//}


//Call CityExperiance Servie and Class
-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}

//Call CityExperiance Servie and Class
-(void) navigateToCityExperience
{
    //[self request_CitiExpRet];
    [defaults setInteger:100 forKey:@"CityExpTableTag"];
}

//Call Find Service and Class
-(void)navigateToFindView
{
    
    //    [defaults setValue:@"0" forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    
    navigatedFromMainMenu = FALSE;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}





@end

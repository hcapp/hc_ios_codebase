//
//  MapKitDisplayViewController.h
//  MapKitDisplay
//
//  Created by ramanan on 5/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CommonUtility.h"
@class RetailersListViewController;
@class DisplayMap;
//bool navigatedFromMapKit;
@interface MapKitDisplayViewController : UIViewController<MKMapViewDelegate,CustomizedNavControllerDelegate> {
	IBOutlet MKMapView *mapView;
	DisplayMap *ann;
    UIBarButtonItem *ok;
    float latVal;
    float longVal;
}

@property(nonatomic,strong)IBOutlet MKMapView *mapView;
@property(nonatomic,strong) RetailersListViewController* retailerListScreen;
- (void)findRetailers;

@end


//
//  RetailersListViewController.h
//  ScanSee
//
//  Created by ramanan on 5/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "GetUserInfoResponse.h"
NSDictionary *viewDictionary;
@class EventsListViewController;
@class FundraiserListViewController;
@class FilterListViewController;
@class FAQCategoryList;
@class AnyViewController;
@class EmailShareViewController;
typedef enum webServicesRetailerList
{
    GETRETAILERSINFO,
    GETRETSUMMARY,
    //GETRETSUMMARY,
    GETUSERINF,
    CITIEXPRE,
    PARTNERET,
    gETFAVCATEGORIE,
    gETFAVLOCATIONS,
    HubcitiAnythingInf,
    Appsitedetail,
    catsearc
   
}webServicesRetailerListState;

NSMutableArray *globalNearByLatArray,*globalNearByLongArray;
NSMutableArray  *globalRetailerNameArray,*globalRetailerAddressArray;
NSMutableArray *globalFilterRetailerArray;
NSMutableArray *globalRetailerListID, *globalRetailerID,*globalRetLocId;


BOOL fromNearBy;

@interface RetailersListViewController : UIViewController<CLLocationManagerDelegate,UIPickerViewDelegate, UITableViewDelegate, MKMapViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate,CustomizedNavControllerDelegate> {
    
    webServicesRetailerListState iWebRequestState;
    UITableView *retailersList;
    MKMapView *mapView;
    
    NSMutableArray *retailerIdArray, *retailerNameArray,*retailerAddressArray, *retailLocationIDArray,*completeAddress;
    NSMutableArray *retailerLatArray,*retailerLongArray,*annArray;
	NSMutableArray *distanceArray, *retailAddressArray,*rowNumberArray,*citistatezipArray;
	NSMutableArray *bannerAdImagePathArray, *ribbonAdImagePathArray, *ribbonAdURLArray, *logoImagePathArray;
	NSMutableArray *advertisementIDArray;
    
    //@Deepak : Added Array to save retListID
    NSMutableArray *retListIDArray;
    
    NSMutableArray *saleFlagArray;
	NSIndexPath *selectedIndexPath;
    CommonUtility *common;
    
	int lastVisitedRecord;
    int nextPage;
    
    //added to hold the list of bottomButtons
    NSMutableArray *bottomButtonList;
    WebRequestState IWebRequestState;
    
    BOOL isOnlyOnePartner;
    int bottomBtn;
}
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,retain) FAQCategoryList *faqList;
@property(nonatomic,retain) RetailersListViewController* retailerListScreen;
@property(nonatomic,retain) FilterListViewController *filterList;
@property(nonatomic,retain) EventsListViewController* iEventsListViewController;
@property (assign)BOOL isOnlyOnePartner;
@property(nonatomic,retain) FundraiserListViewController *iFundraiserListViewController;
-(void)returnToMainPage:(id)sender;
-(void)displayCategoryInfo:(id)indexPath;
//-(void)updateRetailersList:(int)lastVisitedRecord;
-(void) refreshTable : (int) lastRecord;
-(void)fetchNextPage:(id)sender;
-(void)parseRetailerList:(NSString *)response;


@end

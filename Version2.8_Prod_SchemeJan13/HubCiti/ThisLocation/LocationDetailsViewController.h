//
//  LocationDetailsViewController.h
//  SourceCodeScanSee
//
//  Created by ramanan on 5/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"
@class RetailersListViewController;
//NSString *zipText;
@interface LocationDetailsViewController : UIViewController<UIPickerViewDelegate,UITextFieldDelegate,CustomizedNavControllerDelegate> {
	
    IBOutlet UITextField *zipCodeText;
	IBOutlet UIPickerView *searchRadiusPicker;
	IBOutlet UILabel *searchRadiusValue;
	IBOutlet UIButton *dropDown, *okButton;
	IBOutlet UIButton *locateOnMap;
	IBOutlet UIToolbar *pickerToolbar;
    
    IBOutlet UILabel *lblZipCodeText;
    IBOutlet UILabel *OrText;
    
    //Chance's Edits
    IBOutlet UIButton *doneButton;
    //End Chance's Edits
    NSMutableArray *searchRadiusArray;
    NSMutableArray *distInMilesArray;
	NSString *postalCodeStr;
    
	double longitude, latitude;
    int radiusPickerRow;
}
@property (readwrite)int radiusPickerRow;
@property(nonatomic,strong) RetailersListViewController* retailerListScreen;
-(IBAction)unhidePicker:(id)sender;
-(IBAction)okButtonPressed:(id)sender;
-(IBAction)locateOnMapPressed:(id)sender;
-(IBAction)toolbarDonePressed:(id)sender;

@end

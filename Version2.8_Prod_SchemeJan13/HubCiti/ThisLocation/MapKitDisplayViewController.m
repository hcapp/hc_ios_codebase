//
//  MapKitDisplayViewController.m
//  MapKitDisplay
//
//  Created by ramanan on 5/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MapKitDisplayViewController.h"
#import "DisplayMap.h"
#import "RetailersListViewController.h"
#import "LocationDetailsViewController.h"

@implementation MapKitDisplayViewController
@synthesize mapView;

//added to test

-(void)okPressed
{
    NSString *zipText = [[NSString alloc]initWithString:[defaults valueForKey:KEYZIPCODE_TL]];
    if ([zipText length]<5 || [zipText length]>10) {
		[defaults setValue:nil forKey:KEYZIPCODE_TL];
		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:NSLocalizedString(@"Please enter valid zipcode",@"Please enter valid zipcode")];
	}
	else
    {
        //[defaults  setValue:zipCodeText.text forKey:@"ZipCode"];
        
		//[defaults  setValue:zipText forKey:KEYZIPCODE_TL];
		[defaults  setBool:NO forKey:@"gpsEnabled"];
        [defaults setBool:NO forKey:@"locOnMap"];
        
		
		RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
        self.retailerListScreen = retailerListScreen;
		[self.navigationController pushViewController:retailerListScreen animated:NO];
		//[retailerListScreen release];
        
	}
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    ok.enabled = YES;
    self.title = @"Location Map";
	self.navigationItem.hidesBackButton = NO;
	self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
	[defaults  setObject:@"YES" forKey:@"showBackButton"];
	
    //customize back button
       self.navigationItem.hidesBackButton = YES;
    //[back release];
	
    
    //customize the rightbarbuttonitem
	ok = [[UIBarButtonItem alloc]init];
    
    UIButton *customOkBtn = [UtilityManager customizeBarButtonItem:@"OK"];
   // [customOkBtn addTarget:self action:@selector(findRetailers) forControlEvents:UIControlEventTouchUpInside];
    [customOkBtn addTarget:self action:@selector(okPressed) forControlEvents:UIControlEventTouchUpInside];
    ok.customView = customOkBtn;
    self.navigationItem.rightBarButtonItem = ok;
	//[ok release];
	
	[mapView setMapType:MKMapTypeStandard];
	[mapView setZoomEnabled:YES];
	[mapView setScrollEnabled:YES];
	
	UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(dropPin:)];
    [mapView addGestureRecognizer:longPressGesture];
    //[longPressGesture release];
	
	MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } }; 
	region.center.latitude = [[defaults  objectForKey:KEY_LATITUDE_TL] doubleValue];
	region.center.longitude = [[defaults  objectForKey:KEY_LONGITUDE_TL] doubleValue];
	region.span.longitudeDelta = 0.01f;
	region.span.latitudeDelta = 0.01f;
	
	[mapView setRegion:region animated:YES]; 
	[mapView setDelegate:self];
	
	ann = [[DisplayMap alloc] init]; 
	ann.coordinate = region.center; 

    latVal = region.center.latitude;
    longVal = region.center.longitude;
    
    
	[mapView addAnnotation:ann];
	
	self.view.backgroundColor = [UIColor colorWithRGBInt:0x084178];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];

}
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}
- (void)dropPin:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan) {
        CGPoint location = [gestureRecognizer locationInView:[gestureRecognizer view]];
        
		CLLocationCoordinate2D coord= [mapView convertPoint:location toCoordinateFromView:mapView];
		[mapView removeAnnotation:ann];
		
		ann.coordinate = coord;
		[mapView addAnnotation:ann];
		
		[defaults  setObject:[NSString stringWithFormat:@"%.6g", coord.latitude] forKey:KEY_LATITUDE_TL];
		[defaults  setObject:[NSString stringWithFormat:@"%.6g", coord.longitude] forKey:KEY_LONGITUDE_TL];
        latVal = coord.latitude;
        longVal = coord.longitude;
        ok.enabled = YES;
    }
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
	MKPinAnnotationView *pinView = nil; 
	if(annotation != mapView.userLocation) 
	{
		static NSString *defaultPinID = @"com.invasivecode.pin";
		pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
		if ( pinView == nil ) pinView = [[MKPinAnnotationView alloc]
										  initWithAnnotation:annotation reuseIdentifier:defaultPinID];
		
		pinView.pinColor = MKPinAnnotationColorRed; 
		pinView.canShowCallout = YES;
		pinView.animatesDrop = YES;
	} 
	else {
		[mapView.userLocation setTitle:NSLocalizedString(@"I am here",@"I am here")];
	}
    return pinView;
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
    self.mapView = nil;
}


- (void)findRetailers {
    ok.enabled = NO;
    //[defaults  setValue:@"" forKey:KEYZIPCODE];
    [defaults  setValue:@"" forKey:KEYZIPCODE_TL];
    [defaults  setBool:YES forKey:@"gpsEnabled"];
    
    Reachability *zipObjReachability = [Reachability reachabilityForInternetConnection];
    if([zipObjReachability currentReachabilityStatus]==0) {
        [UtilityManager showAlert];
        return;
    }
    
    NSString *zipString = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/xml?latlng=%f,%f&sensor=false", latVal, longVal];
    
    NSString *zipResponseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:zipString]];
    
    // DLog(@"Response xml for This Location (Fetching Retailer List) = %@", zipResponseXml);
    
    if ([UtilityManager isResponseXMLNullOrEmpty:zipResponseXml]) {
        ReleaseAndNilify(zipResponseXml);
        return;
    }
    DLog(@"RESPONSE - %@", zipResponseXml);
    //[defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:zipResponseXml];
    TBXMLElement *statusTextElement = [TBXML childElementNamed:@"status" parentElement:tbxml.rootXMLElement];
    if  (statusTextElement == nil)
    {
        [UtilityManager showAlert:nil msg:@"Cannot use this location. Please try another, or enter a zip code instead"];
        
        return;
        
    }
    
    BOOL postalCodeFound = NO;
    TBXMLElement *resultElement = [TBXML childElementNamed:@"result" parentElement:tbxml.rootXMLElement];
    while (resultElement != nil) {
        TBXMLElement *typeElement = [TBXML childElementNamed:@"type" parentElement:resultElement];
        if (typeElement != nil) {
            if ([[TBXML textForElement:typeElement] isEqualToString:@"postal_code"] ) {
                DLog(@"Postal Code Type Found");
                postalCodeFound = YES;
                break;
            }
        }
        resultElement = [TBXML nextSiblingNamed:@"result" searchFromElement:resultElement];
    }
    if (postalCodeFound) {
        
        TBXMLElement *addressComponentElement = [TBXML childElementNamed:@"address_component" parentElement:resultElement];
        TBXMLElement *postalCodeElement = [TBXML childElementNamed:@"short_name" parentElement:addressComponentElement];
        if (postalCodeElement != nil) {
            [defaults  setValue:[TBXML textForElement:postalCodeElement] forKey:@"postalCode"];
        }
        else {
            [UtilityManager showAlert:nil msg:@"Cannot use this location. Please try another, or enter a zip code instead"];
            
            return;
        }
        
    }
    else {
        
        [UtilityManager showAlert:nil msg:@"Cannot use this location. Please try another, or enter a zip code instead"];
        
        return;
    }
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<ThisLocationRequest><userId>%@</userId><preferredRadius>20</preferredRadius>", [defaults  valueForKey:KEY_USERID]];
    [xmlStr appendFormat:@"<zipcode><![CDATA[%@]]></zipcode><gpsEnabled>false</gpsEnabled>", [defaults  objectForKey:@"postalCode"]];
    //navigatedFromMapKit = TRUE;
    /* if([defaults valueForKey:KEY_MAINMENUID])
     [xmlStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
     else
     {
     if([defaults valueForKey:KEY_BOTTOMBUTTONID])
     [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
     
     if([defaults valueForKey:KEY_MITEMID])
     [xmlStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
     
     }*/
    if([defaults valueForKey:KEY_MAINMENUID])
        [xmlStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    else
    {
        if (![defaults boolForKey:BottomButton]) {
            if([defaults valueForKey:KEY_MITEMID])
                [xmlStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
        }
        
        else{
            if([defaults valueForKey:KEY_BOTTOMBUTTONID])
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
    }
    
    [xmlStr appendFormat:@"<locOnMap>true</locOnMap><lastVisitedRecord>0</lastVisitedRecord><hubCitiId>%@</hubCitiId></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID]];
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [timeFormatter setTimeZone:timeZone];
    [timeFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *timeInUTC = [timeFormatter stringFromDate:currentTime];
    NSLog(@"Current time %@", timeInUTC);
    
    [xmlStr appendFormat:@"<requestedTime>%@</requestedTime>",timeInUTC];
    DLog(@"Request for This Location (Fetching Retailer List) = %@",xmlStr);
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getretailersinfo",BASE_URL];
    [defaults setBool:TRUE forKey:@"locOnMap"];
    
    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    ReleaseAndNilify(xmlStr);
}

-(void)responseData:(NSString *) responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
		return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        [defaults  setObject:responseString forKey:KEY_RESPONSEXML];
        
        
        RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
        self.retailerListScreen = retailerListScreen;
        [self.navigationController pushViewController:retailerListScreen animated:NO];
        //[retailerListScreen release];
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }


}


@end

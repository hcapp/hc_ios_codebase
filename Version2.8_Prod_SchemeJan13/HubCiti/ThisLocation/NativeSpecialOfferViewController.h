//
//  NativeSpecialOfferViewController.h
//  HubCiti
//
//  Created by Keshava on 6/3/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "DWBubbleMenuButton.h"
#import "SpecialOfferDetailResponse.h"
#import "SpecialOfferMYODetailResponse.h"
@class AnyViewController;
@class EmailShareViewController;
@interface NativeSpecialOfferViewController : UIViewController<UIWebViewDelegate,MFMailComposeViewControllerDelegate,UITableViewDataSource,UITableViewDelegate,MFMessageComposeViewControllerDelegate,DWBubbleMenuViewDelegate,CustomizedNavControllerDelegate>
{
    CustomizedNavController *cusNav;
    WebRequestState iWebRequestState;
}
// Special Offer Deatil Page Properties

@property(nonatomic,strong) SpecialOfferDetailResponse * obj_SpecialOfferResponse;
@property(nonatomic,strong) SpecialOfferMYODetailResponse * obj_MYODetail;

@property (strong,nonatomic) UILabel *titleLbl;
@property (strong,nonatomic) UIImageView *specialOfferImage;
@property (strong,nonatomic) UILabel *dateLbl;
@property (strong,nonatomic) UILabel *promoLbl;
@property (strong,nonatomic) UIWebView *shortDesc;
@property (strong,nonatomic) UIWebView *LongDesc;
@property (strong,nonatomic) UIButton *locationBtn;
@property (strong,nonatomic) UIButton *cancelBtn;
//Table View that lists the Locations where the special Offers are available
@property (strong,nonatomic) UITableView *locationsTable;

//Used to control the table view behaviour
@property (strong,nonatomic) NSMutableDictionary *specialDetails;
@property (strong,nonatomic) NSMutableArray *tableDetails;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;


@property (strong, nonatomic) IBOutlet UIView *specialDetailViewOulet;
@property (strong, nonatomic) IBOutlet UIWebView *shortDescritionWebView;
@property (strong, nonatomic) IBOutlet UIWebView *longDescriptionWebView;
@property (strong, nonatomic) IBOutlet UIImageView *imageOutlet;
@property (strong, nonatomic) IBOutlet UILabel *specialStartDateOutlet;

@property (strong, nonatomic) IBOutlet UILabel *promotionEndsDateOutlet;
@property (strong, nonatomic) IBOutlet UILabel *specialNameLabelOutlet;
@property (strong, nonatomic) IBOutlet UIButton *locationButtonOutlet;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightForPromotionEndsLAbel;


@end

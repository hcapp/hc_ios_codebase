//
//  LocationDetailsViewController.m
//  SourceCodeScanSee
//
//  Created by ramanan on 5/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "MapKitDisplayViewController.h"
#import "MainMenuViewController.h"
#import "CityExperienceViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@implementation LocationDetailsViewController
{
    CustomizedNavController *cusNav;
}
@synthesize  radiusPickerRow;

//Adding a Done Button
- (void)addButtonToKeyboard {
    // create custom button
    //    if (doneButton == nil) {
    //
    //        doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //        doneButton.frame = CGRectMake(0, 163, 106, 53);
    //        [doneButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    //        [doneButton setTitle:NSLocalizedString(@"DONE", @"DONE") forState:UIControlStateNormal];
    //    }
    //    else {
    //        [doneButton setHidden:NO];
    //    }
    //
    //    [doneButton addTarget:self action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    // locate keyboard view
    //    UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    //    UIView* keyboard = nil;
    //    for(int i=0; i<[tempWindow.subviews count]; i++) {
    //        keyboard = [tempWindow.subviews objectAtIndex:i];
    //        // keyboard found, add the button
    //        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.2) {
    //            if([[keyboard description] hasPrefix:@"<UIPeripheralHost"] == YES){
    //                if (doneButton == nil)
    //                [keyboard addSubview:doneButton];
    //            }
    //        } else {
    //            if([[keyboard description] hasPrefix:@"<UIKeyboard"] == YES)
    //                [keyboard addSubview:doneButton];
    //        }
    //    }
    
    doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    doneButton.frame = CGRectMake(0, 163+44, 106, 53);
    [doneButton setTag:67123];
    [doneButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [doneButton setTitle:NSLocalizedString(@"DONE", @"DONE") forState:UIControlStateNormal];
    
    
    [doneButton addTarget:self action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    // locate keyboard view
    int windowCount = (int)[[[UIApplication sharedApplication] windows] count];
    if (windowCount < 2) {
        return;
    }
    
    UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    UIView* keyboard;
    
    for(int i = 0 ; i < [tempWindow.subviews count] ; i++)
    {
        keyboard = [tempWindow.subviews objectAtIndex:i];
        // keyboard found, add the button
        
        if([[keyboard description] hasPrefix:@"<UIPeripheralHost"] == YES){
            UIButton* searchbtn = (UIButton*)[keyboard viewWithTag:67123];
            if (searchbtn == nil)//to avoid adding again and again as per my requirement (previous and next button on keyboard)
                [keyboard addSubview:doneButton];
            
        }//This code will work on iOS 8.0
        else if([[keyboard description] hasPrefix:@"<UIInputSetContainerView"] == YES){
            
            for(int i = 0 ; i < [keyboard.subviews count] ; i++)
            {
                UIView* hostkeyboard = [keyboard.subviews objectAtIndex:i];
                
                if([[hostkeyboard description] hasPrefix:@"<UIInputSetHost"] == YES){
                    UIButton* donebtn = (UIButton*)[hostkeyboard viewWithTag:67123];
                    if (donebtn == nil)//to avoid adding again and again as per my requirement (previous and next button on keyboard)
                        [hostkeyboard addSubview:doneButton];
                }
            }
        }
    }
}
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"Nearby" forView:self withHambergur:YES];
    //self.title = NSLocalizedString(@"Nearby",@"Nearby");
    [self setAccessibilityLabel:@"LocationDetails"];
    [locateOnMap setAccessibilityLabel:@"locateOnMap"];
    [okButton setAccessibilityLabel:@"OK"];
    [zipCodeText setAccessibilityLabel:@"zipCode"];
    
    self.navigationController.navigationBar.hidden = NO;
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"MainMenu"];
    //    //[mainPage release];
    //
    
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    
    //    if (navigatedFromMainMenu==TRUE) {
    //        self.navigationItem.hidesBackButton = YES;
    //    }
    //	else
    //    {
    //        UIButton *backBtn = [UtilityManager customizeBackButton];
    //        [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
    //        UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    //        self.navigationItem.leftBarButtonItem = back;
    //        self.navigationItem.hidesBackButton =NO;
    //        //[back release];
    //    }
    
    radiusPickerRow = 0;
    searchRadiusArray = [[NSMutableArray alloc]init];
    distInMilesArray = [[NSMutableArray alloc]init];
    
    if ([searchRadiusArray count]){
        searchRadiusValue.text= [searchRadiusArray objectAtIndex:0];
        [defaults  setObject:[searchRadiusArray objectAtIndex:0] forKey:@"Search Radius"];
    }
    searchRadiusPicker.hidden = YES;
    searchRadiusPicker.delegate = self;
    [searchRadiusPicker reloadAllComponents];
    
    pickerToolbar.hidden = YES;
    zipCodeText.delegate = self;
    
    if (IOS7){
        locateOnMap.backgroundColor = [UIColor whiteColor];
        locateOnMap.layer.cornerRadius = 5.0;
        locateOnMap.layer.borderWidth = 1.0f;
        locateOnMap.layer.borderColor = [UIColor grayColor].CGColor;
        
        okButton.backgroundColor = [UIColor whiteColor];
        okButton.layer.cornerRadius = 5.0;
        okButton.layer.borderWidth = 1.0f;
        okButton.layer.borderColor = [UIColor grayColor].CGColor;
    }
    
    [dropDown addTarget:self action:@selector(unhidePicker:) forControlEvents:UIControlEventTouchUpInside];
    [okButton addTarget:self action:@selector(okButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController setToolbarHidden:YES];
    
    /* if(![zipCodeText.text length]) //if(![defaults valueForKey:KEYZIPCODE_TL])
     {
     locateOnMap.userInteractionEnabled = NO;
     locateOnMap.alpha = 0.5;
     }*/
    zipCodeText.text = @"";
    
    
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}
-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    [cusNav hideHambergerButton: NO];
    cusNav.customNavBardelegate =self;
    
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    // If coming from SubMenu then Show BackButton
    //if ([defaults  boolForKey:@"showBackButton"] == YES)
    if (navigatedFromMainMenu==TRUE)
    {
        //[self showSplash];
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = nil;
        [cusNav hideBackButton:NO];
    }
    else{
        self.navigationItem.hidesBackButton = YES;
        [cusNav hideBackButton:NO];
    }
    //Set the Background image/color as per Main Menu BG color
    if([defaults valueForKey:@"menuBkgrdImage"])
    {
        SdImageView *asyncImageView;
        
        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        
        asyncImageView.backgroundColor = [UIColor clearColor];
        asyncImageView.layer.cornerRadius = 5.0f;
        [asyncImageView loadImage:[defaults valueForKey:@"menuBkgrdImage"]];
        asyncImageView.tag=99;
        [self.view insertSubview:asyncImageView atIndex:0];
        
        [self.view sendSubviewToBack:asyncImageView];
        //[asyncImageView release];
        
    }
    else if([defaults valueForKey:@"menuBkgrdColor"])
    {
        self.view.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"menuBkgrdColor"]];
    }
    else
        self.view.backgroundColor = [UIColor colorWithRGBInt:0x084178];
    
    if([defaults valueForKey:@"menuBtnColor"])
    {
        okButton.layer.borderWidth = 1.0;
        okButton.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
        [okButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
        okButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        okButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        
        locateOnMap.layer.borderWidth = 1.0;
        locateOnMap.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
        [locateOnMap setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
        locateOnMap.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        locateOnMap.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        
        [okButton setBackgroundColor:[UIColor convertToHexString:[defaults valueForKey:@"menuBtnColor"]]];
        [okButton setTitleColor:[UIColor convertToHexString:[defaults valueForKey:@"menuBtnFontColor"]] forState:UIControlStateNormal];
        [okButton setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        okButton.layer.cornerRadius = 5; // this value vary as per your desire
        okButton.clipsToBounds = YES;
        
        
        [locateOnMap setBackgroundColor:[UIColor convertToHexString:[defaults valueForKey:@"menuBtnColor"]]];
        [locateOnMap setTitleColor:[UIColor convertToHexString:[defaults valueForKey:@"menuBtnFontColor"]] forState:UIControlStateNormal];
        [locateOnMap setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        locateOnMap.layer.cornerRadius = 5; // this value vary as per your desire
        locateOnMap.clipsToBounds = YES;
        
        
        [lblZipCodeText setTextColor:[UIColor convertToHexString:[defaults valueForKey:@"menuBtnFontColor"]]];
        [OrText setTextColor:[UIColor convertToHexString:[defaults valueForKey:@"menuBtnFontColor"]]];
        
        
        
        
    }
    
    
    /*if([defaults  valueForKey:KEYZIPCODE])
     zipCodeText.text = [defaults  valueForKey:KEYZIPCODE];*/
    
    if([defaults  valueForKey:KEYZIPCODE_TL])
        zipCodeText.text = [defaults  valueForKey:KEYZIPCODE_TL];
    
    if(![zipCodeText.text length])
    {
        locateOnMap.userInteractionEnabled = NO;
        locateOnMap.alpha = 0.5;
    }
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        [self setConstraints];
    }
}

-(void)setConstraints
{
    
    NSDictionary *viewsDictionary=@{@"zip":zipCodeText,@"locate":locateOnMap,@"ok":okButton,@"or":OrText,@"ziplabel":lblZipCodeText};
    
    zipCodeText.translatesAutoresizingMaskIntoConstraints=NO;
    locateOnMap.translatesAutoresizingMaskIntoConstraints=NO;
    okButton.translatesAutoresizingMaskIntoConstraints=NO;
    OrText.translatesAutoresizingMaskIntoConstraints=NO;
    lblZipCodeText.translatesAutoresizingMaskIntoConstraints=NO;
    
    [self.view addConstraints: [NSLayoutConstraint
                                constraintsWithVisualFormat:@"V:|-200-[locate(40)]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary]];
    
    [self.view addConstraints: [NSLayoutConstraint
                                constraintsWithVisualFormat:@"V:|-400-[zip(40)]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary]];
    
    [self.view addConstraints: [NSLayoutConstraint
                                constraintsWithVisualFormat:@"V:|-400-[ziplabel(40)]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary]];
    
    [self.view addConstraints: [NSLayoutConstraint
                                constraintsWithVisualFormat:@"V:|-300-[or(40)]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary]];
    
    [self.view addConstraints: [NSLayoutConstraint
                                constraintsWithVisualFormat:@"V:|-600-[ok(40)]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary]];
    
    [self.view addConstraints: [NSLayoutConstraint
                                constraintsWithVisualFormat:@"H:|-70-[ziplabel(200)]-30-[zip(300)]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary]];
    
    [self.view addConstraints: [NSLayoutConstraint
                                constraintsWithVisualFormat:@"H:|-300-[locate(300)]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary]];
    
    [self.view addConstraints: [NSLayoutConstraint
                                constraintsWithVisualFormat:@"H:|-430-[or(30)]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary]];
    
    [self.view addConstraints: [NSLayoutConstraint
                                constraintsWithVisualFormat:@"H:|-400-[ok(100)]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary]];
    
    
}

-(void)popBackToPreviousPage{
    popFromCity=TRUE;
    [self.navigationController popViewControllerAnimated:NO];
}



-(void)returnToMainPage:(id)sender {
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //   [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(IBAction)unhidePicker:(id)sender {
    
    searchRadiusPicker.hidden = NO;
    pickerToolbar.hidden = NO;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"luxt field length %lu",[textField.text length]);
    
    if([textField.text length] >= 5)
    {
        locateOnMap.userInteractionEnabled = YES;
        locateOnMap.alpha = 1.0;
    }
    else
    {
        locateOnMap.userInteractionEnabled = NO;
        locateOnMap.alpha = 0.5;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [defaults  setValue:textField.text forKey:KEYZIPCODE_TL];
    
    if ([textField.text length] >= 5) {
        locateOnMap.userInteractionEnabled = YES;
        locateOnMap.alpha = 1.0;
    }
}



//check the characters in zipcode textField
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSLog(@" Length %lu",[textField.text length]);
    NSLog(@" Length %lu", (unsigned long)[string length]);
    NSLog(@" Length %lu", (unsigned long)range.length);
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    [self addButtonToKeyboard];
    BOOL flag = (newLength > 5) ? NO : YES;
    
    if (flag == NO) {
        
        [UtilityManager showAlert:nil msg:NSLocalizedString(@"Cannot enter more than 5 characters for zipcode",@"Cannot enter more than 5 characters for zipcode")];
    }
    if (newLength >= 5) {
        locateOnMap.userInteractionEnabled = YES;
        locateOnMap.alpha = 1.0;
    }
    else
    {
        locateOnMap.userInteractionEnabled = NO;
        locateOnMap.alpha = 0.5;
    }
    return flag;
    
}


-(void)request_fetchlatlong
{
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/fetchlatlong?zipcode=%@",BASE_URL,[defaults  valueForKey:KEYZIPCODE_TL]];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
    {
        ReleaseAndNilify(responseXml);
        return;
    }
    
    DLog(@"RESPONSE - %@",responseXml);
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]){
        
        TBXMLElement *latElement = [TBXML childElementNamed:@"Latitude" parentElement:tbxml.rootXMLElement];
        TBXMLElement *longElement = [TBXML childElementNamed:@"Longitude" parentElement:tbxml.rootXMLElement];
        if (latElement != nil && longElement != nil)
        {
            [defaults setValue:[TBXML textForElement:latElement] forKey:KEY_LATITUDE_TL];
            [defaults setValue:[TBXML textForElement:longElement] forKey:KEY_LONGITUDE_TL];
            
            MapKitDisplayViewController *mapViewScreen = [[MapKitDisplayViewController alloc] initWithNibName:@"MapKitDisplayViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:mapViewScreen animated:NO];
            //[mapViewScreen release];
        }
    }
    
    else {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:@"Error" msg:[TBXML textForElement:responseTextElement]];
    }
    //[responseXml release];
    
}

-(IBAction)locateOnMapPressed:(id)sender
{
    //zipText=zipCodeText.text;
    [defaults setValue:zipCodeText.text forKeyPath:KEYZIPCODE_TL];
    [self request_fetchlatlong];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    doneButton = nil;
    return YES;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [searchRadiusArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [searchRadiusArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    searchRadiusValue.text = [searchRadiusArray objectAtIndex:row];
    radiusPickerRow = (int)row;
}

-(IBAction)toolbarDonePressed:(id)sender{
    searchRadiusPicker.hidden = YES;
    pickerToolbar.hidden = YES;
    
    //To carry the selected radius value to next screen
    //[defaults setObject:[NSString stringWithFormat:@"%d", ((radiusPickerRow + 1) * 5)] forKey:@"Search Radius"];
    [defaults setObject:[searchRadiusArray objectAtIndex:radiusPickerRow] forKey:@"Search Radius"];
}

//To slide out the keyboard
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches]anyObject];
    if (touch.tapCount >=1) {
        [zipCodeText resignFirstResponder];
        doneButton = nil;
    }
}


-(IBAction)okButtonPressed:(id)sender
{
    if ([zipCodeText.text length]<5 || [zipCodeText.text length]>10) {
        
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:NSLocalizedString(@"Please enter valid zipcode",@"Please enter valid zipcode")];
    }
    else
    {
        //[defaults  setValue:zipCodeText.text forKey:@"ZipCode"];
        
        [defaults  setValue:zipCodeText.text forKey:KEYZIPCODE_TL];
        [defaults  setBool:NO forKey:@"gpsEnabled"];
        [defaults setBool:NO forKey:@"locOnMap"];
        
        
        RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
        self.retailerListScreen = retailerListScreen;
        [self.navigationController pushViewController:retailerListScreen animated:NO];
        //[retailerListScreen release];
        
    }
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)doneButtonClicked:(id)Sender {
    [zipCodeText resignFirstResponder];
    doneButton = nil;
}


@end




//
//  NativeSpecialOfferViewController.m
//  HubCiti
//
//  Created by Keshava on 6/3/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "NativeSpecialOfferViewController.h"
#import "WebBrowserViewController.h"
#import "RetailerData.h"
#import "MainMenuViewController.h"
#import "RetailerSummaryViewController.h"
#import "EmailShareViewController.h"
#import "SpecialOfferRetailersViewController.h"
#import "CurrentSpecialsViewController.h"
#import "SpecialOffersViewController.h"
#import "CurrentSpecials.h"
#import "RetailerSpecialOffersViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@interface NativeSpecialOfferViewController ()<HTTPClientDelegate>
{
    SpecialOfferMYODetailResponse * obj_MYODetail;
    UIActivityIndicatorView *activityIndicator;
}

@end

@implementation NativeSpecialOfferViewController

@synthesize titleLbl,specialOfferImage,dateLbl,shortDesc,locationBtn,LongDesc,specialDetails,tableDetails,locationsTable,promoLbl,cancelBtn,anyVC,emailSendingVC, obj_SpecialOfferResponse, shortDescritionWebView,longDescriptionWebView,promotionEndsDateOutlet, specialDetailViewOulet,specialNameLabelOutlet,specialStartDateOutlet,imageOutlet,heightForPromotionEndsLAbel,obj_MYODetail;

//Used as a cell Identifier
static NSString *CellIdentifier= @"LocationList";


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error
{
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription] ];
    
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    [self parseSpecialOfferDetails:responseObject];
}
- (void)viewDidLoad {
    
    self.title=@"Details";
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
   
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    self.navigationItem.hidesBackButton = YES;
    
    specialDetailViewOulet.hidden = YES;
    // [shareButton release];
    //[back release];
    
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    

    tableDetails =[[NSMutableArray alloc]init];
    [self getSpecialOfferDetails];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];

    cusNav =(CustomizedNavController *) self.navigationController;
    
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    cusNav.customNavBardelegate = self;
    locationBtn.userInteractionEnabled=YES;
   
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    if ([defaults boolForKey:@"backfromWebView"]) {
        
        [defaults setBool:NO forKey:@"backfromWebView"];
        
        for (UIViewController *aViewController in allViewControllers)
        {
            if ([aViewController isKindOfClass: [CurrentSpecials class]] || [aViewController isKindOfClass: [self class]] ) {
                
                [self.navigationController popViewControllerAnimated:NO];
            }
        }
        
    }
    
}



-(void)addShareButton{
    // Create down menu button
    UIImageView *homeLabel ;
    
    
    // Create up menu button
    homeLabel = [self createHomeButtonView];
    
    DWBubbleMenuButton *upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
                                                                                          self.view.frame.size.height - homeLabel.frame.size.height - 50.f,
                                                                                          homeLabel.frame.size.width,
                                                                                          homeLabel.frame.size.height)
                                                            expansionDirection:DirectionUp];
    upMenuView.homeButtonView = homeLabel;
    
    upMenuView.delegate = self;
    
    [upMenuView addButtons:[self createDemoButtonArray]];
    
    [self.view addSubview:upMenuView];
    
    [self.view bringSubviewToFront:upMenuView];
    
    upMenuView.onLogOutPressed =^(){
        
        [self logOutPressed];
    } ;
    
    
}




- (UIImageView *)createHomeButtonView {
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
    label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}

- (NSArray *)createDemoButtonArray {
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    
    int i = 0;
    for (NSString *title in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:title] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        
        button.clipsToBounds = YES;
        button.tag = i++;
        
        [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
}

- (void)test:(UIButton *)sender {
    
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
     [self shareClicked];
    switch (sender.tag) {
        case 0:{//facebook
           
            
            anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
            //[self.view addSubview:anyVC.view];
            anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
            [UIView transitionWithView:self.view duration:0.5
                               options:UIViewAnimationOptionTransitionCurlDown
                            animations:^ { [self.view addSubview:anyVC.view]; }
                            completion:nil];
        }
            break;
        case 1:{//twitter
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    if (result == SLComposeViewControllerResultCancelled) {
                        DLog(@"Twitter Result: canceled");
                    } else {
                        DLog(@"Twitter Result: sent");
                        NSMutableString *userXmlStr;
                        
                        userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><pageId>%@</pageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:@"nativePageId"]];
                        
                        [NSThread detachNewThreadSelector:@selector(userTrack:) toTarget:self withObject:userXmlStr];
                    }
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        
                        [controller.view removeFromSuperview];
                    } else
                        [controller dismissViewControllerAnimated:YES completion:Nil];
                };
                controller.completionHandler =myBlock;
                
                //Adding the Text to the facebook post value from iOS
                [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                
                //Adding the URL to the facebook post value from iOS
                
                //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                
                //Adding the Image to the facebook post value from iOS
                
                [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    [self.view addSubview:controller.view];
                    [self.view bringSubviewToFront:controller.view];
                }else
                    [self presentViewController:controller animated:YES completion:Nil];
            }
            else {
                [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
                }
        }
            
            
            break;
        case 2:{//text
            
            if ([self hasCellularCoverage])
            {
                if ([MFMessageComposeViewController canSendText]) {
                    
                    MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
                    [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    smsComposerController.messageComposeDelegate = self;
                    [self presentViewController:smsComposerController animated:YES completion:nil];
                }
            }
            else
            {
                [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
                
            }
            
            
        }
            break;
        case 3:{//email
            //[defaults setValue:PRODUCT_SHARE forKey:KEY_SHARE_TYPE];
            __typeof(self) __weak  obj = self;
            emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
             emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
            emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                
                [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
            } ;
            
            [emailSendingVC loadMail];
        }
            break;
            
        default:
            [defaults setBool:NO forKey:BottomButton];
            break;
    }
    
}

- (UIButton *)createButtonWithName:(NSString *)imageName {
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button sizeToFit];
    
    [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}



- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView{
    
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Navigation bar button actions
-(void)popBackToPreviousPage{
    
    
    
    if ([defaults boolForKey:@"detailsPage"]) {
        
        [defaults setBool:NO forKey:@"detailsPage"];
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:NO];
        
    }
    
    
    
}



-(void)returnToMainPage:(id)sender{
    
    [defaults setBool:NO forKey:@"detailsPage"];
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}



#pragma mark set the layout view Methods

-(void)setSepcialOfferDetailsView
{
    specialNameLabelOutlet.text = obj_MYODetail.pageTitle;
    
    NSString *utf = [obj_MYODetail.retImagePath stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
    imageOutlet.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:utf]]];
    
    specialStartDateOutlet.text = obj_MYODetail.startDate;
    
    
    shortDescritionWebView.layer.cornerRadius = 5.0;
    shortDescritionWebView.layer.borderColor = [UIColor grayColor].CGColor;
    shortDescritionWebView.layer.borderWidth = 1.0;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        [shortDescritionWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#000;'>%@",obj_MYODetail.shortDesc] baseURL:nil];
    }
    else
    {
        [shortDescritionWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:17px;font-family:Helvetica;color:#000;'>%@",obj_MYODetail.shortDesc] baseURL:nil];
    }
    
    
    longDescriptionWebView.layer.cornerRadius = 5.0;
    longDescriptionWebView.layer.borderColor = [UIColor grayColor].CGColor;
    longDescriptionWebView.layer.borderWidth = 1.0;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        [longDescriptionWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#000;'>%@",obj_MYODetail.longDesc] baseURL:nil];
    }
    else
    {
        [longDescriptionWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:17px;font-family:Helvetica;color:#000;'>%@",obj_MYODetail.longDesc] baseURL:nil];
    }
    
    if (obj_MYODetail.endDate && ![obj_MYODetail.endDate isEqualToString:@"N/A"])
    {
        NSString *endDateWithString = [NSString stringWithFormat:@"Promotion Ends: %@",obj_MYODetail.endDate];
        promotionEndsDateOutlet.text = endDateWithString;
    }
    else
    {
        [self.view layoutIfNeeded];
        heightForPromotionEndsLAbel.constant = 0;
        [self.view updateConstraints];
    }
    //activity indicator
    activityIndicator = [[UIActivityIndicatorView alloc]
                         initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityIndicator.hidden = NO;
    activityIndicator.frame  = CGRectMake((shortDescritionWebView.frame.size.width - (IPAD?30:20))/2, (shortDescritionWebView.frame.size.height - (IPAD?30:20))/2, (IPAD?30:20), (IPAD?30:20));
    [shortDescritionWebView addSubview:activityIndicator];
    [activityIndicator startAnimating];

}

-(void)setSpecialOfferConstraints
{
    float yVal= self.navigationController.navigationBar.frame.size.height;
    float gap =0.01 *(SCREEN_HEIGHT-yVal);;
    float lblBtnWidth= SCREEN_WIDTH-2;
    float lblHeight = 0.06 *(SCREEN_HEIGHT-yVal);
    float btnHeight = 0.08 *(SCREEN_HEIGHT-yVal);
    float imageHeight = 0.25 *(SCREEN_HEIGHT-yVal);
    float shortHeight = 0.18*(SCREEN_HEIGHT-yVal);
    float longHeight;
    if (![[specialDetails valueForKey:@"promoLabel"] isEqualToString:@"Promotion Ends: N/A"]) {
        longHeight= 0.23*(SCREEN_HEIGHT-yVal);
    }
    else
    {
        longHeight= 0.25*(SCREEN_HEIGHT-yVal);
    }
    
    float promoLblHeight = 0.04 *(SCREEN_HEIGHT-yVal);
    
    
    
    titleLbl.translatesAutoresizingMaskIntoConstraints=NO;
    specialOfferImage.translatesAutoresizingMaskIntoConstraints=NO;
    dateLbl.translatesAutoresizingMaskIntoConstraints=NO;
    shortDesc.translatesAutoresizingMaskIntoConstraints=NO;
    LongDesc.translatesAutoresizingMaskIntoConstraints=NO;
    locationBtn.translatesAutoresizingMaskIntoConstraints=NO;
    promoLbl.translatesAutoresizingMaskIntoConstraints=NO;
    NSDictionary *viewsDictionary;
    if (![[specialDetails valueForKey:@"promoLabel"] isEqualToString:@"Promotion Ends: N/A"]) {
        viewsDictionary = @{@"TL":titleLbl,@"SI":specialOfferImage,@"DL":dateLbl,@"SD":shortDesc,@"LD":LongDesc,@"LB":locationBtn,@"PL":promoLbl};
    }
    else
    {
        viewsDictionary = @{@"TL":titleLbl,@"SI":specialOfferImage,@"DL":dateLbl,@"SD":shortDesc,@"LD":LongDesc,@"LB":locationBtn};
    }
    //Horizontal Constraints Added
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-1-[TL(==%f)]-1-|",lblBtnWidth]  options:0 metrics:nil views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-1-[DL(==%f)]-1-|",lblBtnWidth]  options:0 metrics:nil views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-1-[SD(==%f)]-1-|",lblBtnWidth]  options:0 metrics:nil views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-1-[LD(==%f)]-1-|",lblBtnWidth]  options:0 metrics:nil views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-1-[LB(==%f)]-1-|",lblBtnWidth]  options:0 metrics:nil views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[SI(==%f)]-(%f)-|",(SCREEN_WIDTH*0.15),(SCREEN_WIDTH*0.70),(SCREEN_WIDTH*0.15)]  options:0 metrics:nil views:viewsDictionary]];
    
    //Vertical Constraints Added
    if (![[specialDetails valueForKey:@"promoLabel"] isEqualToString:@"Promotion Ends: N/A"]) {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-1-[PL(==%f)]-1-|",lblBtnWidth]  options:0 metrics:nil views:viewsDictionary]];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-[TL(==%f)]-(%f)-[SI(==%f)]-(%f)-[DL(==%f)]-(%f)-[SD(==%f)]-(%f)-[LD(==%f)]-(%f)-[PL(==%f)]-[LB(==%f)]-|",lblHeight,gap,imageHeight,gap,lblHeight,gap,shortHeight,gap,longHeight,gap,promoLblHeight,btnHeight]  options:0 metrics:nil views:viewsDictionary]];
    }
    else
    {
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-[TL(==%f)]-(%f)-[SI(==%f)]-(%f)-[DL(==%f)]-(%f)-[SD(==%f)]-(%f)-[LD(==%f)]-(%f)-[LB(==%f)]-|",lblHeight,gap,imageHeight,gap,lblHeight,gap,shortHeight,gap,longHeight,gap,btnHeight]  options:0 metrics:nil views:viewsDictionary]];
    }
    
}

-(void)setSpecialWebView
{
    
     [defaults  setObject:obj_SpecialOfferResponse.externalLink forKey:KEY_URL];
    [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
    shareFlag=NO;
    [defaults setObject:[defaults valueForKey:@"nativePageId"] forKey:KEY_PAGEID];
    WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:urlDetail animated:NO];
    //[urlDetail release];
    
}

#pragma mark Request Methods

-(void)getSpecialOfferDetails
{
    iWebRequestState = SPECIALS_REQUEST;
    //NSMutableString *xmlStr = [[NSMutableString alloc]init];
    
    // [xmlStr appendFormat:@"<RetailerDetail><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    // [xmlStr appendFormat:@"<pageId>%@</pageId><platform>IOS</platform>",[defaults valueForKey:@"nativePageId"]];
    
    // [xmlStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[defaults  objectForKey:@"retailLocationID"]];
    // DLog(@"retailLocationID %@",[defaults valueForKey:@"retailLocationID"]);
    //  [xmlStr appendFormat:@"<retailerId>%@</retailerId></RetailerDetail>",[defaults valueForKey:@"retailerId"]];
    
    // NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getspecialofferdetails",BASE_URL];
    
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    if ([defaults valueForKey:KEY_USERID])
    {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID])
    {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    
    [parameters setValue:[defaults valueForKey:@"nativePageId"] forKey:@"pageId"];
    
    [parameters setValue:@"IOS" forKey:@"platform"];
    [parameters setValue:[defaults  objectForKey:@"retailLocationID"] forKey:@"retailLocationId"];
    [parameters setValue:[defaults valueForKey:@"retailerId"] forKey:@"retailerId"];
    
    
    /* NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
     
     if ([defaults valueForKey:KEY_USERID])
     {
     [parameters setValue:@"10091" forKey:@"userId"];
     }
     if ([defaults valueForKey:KEY_HUBCITIID])
     {
     [parameters setValue:@"2233" forKey:@"hubCitiId"];
     }
     
     [parameters setValue:@"7002" forKey:@"pageId"];
     
     [parameters setValue:@"IOS" forKey:@"platform"];
     [parameters setValue:@"2094020" forKey:@"retailLocationId"];
     [parameters setValue:@"1114103" forKey:@"retailerId"]; */
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getspecialofferdetailjson",BASE_URL];
    NSLog(@"URL %@", urlString);
    DLog(@"parameter: %@",parameters);
    HTTPClient *client = [[HTTPClient alloc] init];
    client.delegate = self;
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    [HubCitiAppDelegate showActivityIndicator];
    
    
    //NSString *resp = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    // [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    // [self parseSpecialOfferDetails :resp];
    //ReleaseAndNilify(xmlStr);
    
    
}

-(void)getLocationDetails
{
    NSMutableString *xmlStr = [[NSMutableString alloc]init];
    
    [xmlStr appendFormat:@"<RetailerDetail><hubCitiId>%@</hubCitiId><userId>%@</userId>",[defaults valueForKey:KEY_HUBCITIID],[defaults  valueForKey:KEY_USERID]];
    [xmlStr appendFormat:@"<retailerId>%@</retailerId>",[defaults valueForKey:@"retailerId"]];
    [xmlStr appendFormat:@"<pageId>%@</pageId></RetailerDetail>",[defaults valueForKey:@"nativePageId"]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getspecialoffloclist",BASE_URL];
    
    
    NSString *resp = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    
    [self parseLocationDetails :resp];
    ReleaseAndNilify(xmlStr);
}


-(void)requestRetailerSummary:(int)index
{
    RetailerData *iRetObj = [tableDetails objectAtIndex:index];
    
    
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",iRetObj.retailLocId];
    
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    
    [requestStr appendFormat:@"<scanTypeId>0</scanTypeId>"];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    DLog(@"%@",requestStr);
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",iRetObj.retailId];
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    NSMutableString *urlString = [BASE_URL mutableCopy];
    [urlString appendString:@"thislocation/retsummary"];
    
    NSString *resp = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    [self parse_RetSummary:resp];
    
    ReleaseAndNilify(requestStr);
    
}

#pragma mark Parse Methods

-(void)parseSpecialOfferDetails:(id)responseString
{
    if (responseString == nil)
        return;
    if (obj_SpecialOfferResponse == nil) {
        obj_SpecialOfferResponse = [[SpecialOfferDetailResponse alloc] init];
    }
    [obj_SpecialOfferResponse setValuesForKeysWithDictionary:responseString];
    if ([obj_SpecialOfferResponse.responseCode isEqualToString:@"10000"])
    {
        if ([obj_SpecialOfferResponse.extLinkFlag isEqualToNumber:[NSNumber numberWithInt:1]])
        {
            [self setSpecialWebView];
            [self addShareButton];
        }
        else
        {
            if (obj_SpecialOfferResponse.retDetailList != nil)
            {
                specialDetailViewOulet.hidden = NO;
                obj_MYODetail = [[SpecialOfferMYODetailResponse alloc]init];
                NSDictionary * dict_MYOSpecialDetail = obj_SpecialOfferResponse.retDetailList[0];
                [obj_MYODetail setValuesForKeysWithDictionary:dict_MYOSpecialDetail];
                [self setSepcialOfferDetailsView];
                [self addShareButton];
            }
        }
    }
    else
    {
        [UtilityManager showAlert:@"Info" msg:obj_SpecialOfferResponse.responseText];
        return;
        
    }
    
    //    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    //    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    //    {
    //        TBXMLElement *extLinkElement = [TBXML childElementNamed:@"extLinkFlag" parentElement:tbxml.rootXMLElement];
    //
    //        if ([[TBXML textForElement:extLinkElement] isEqualToString:@"1"]) {
    //
    //            TBXMLElement *extLinkURLElement = [TBXML childElementNamed:@"externalLink" parentElement:tbxml.rootXMLElement];
    //            if (extLinkURLElement) {
    //                [specialDetails setObject:[TBXML textForElement:extLinkURLElement] forKey:@"extURL"];
    //            }
    //
    //            [ self setSpecialWebView];
    //             [self addShareButton];
    //        }
    //        else
    //        {
    //            TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retDetailList" parentElement:tbxml.rootXMLElement];
    //
    //            if(retailerDetailElement)
    //            {
    //                TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
    //                while (RetailerDetailElement)
    //                {
    //
    //
    //                    TBXMLElement *titleElement = [TBXML childElementNamed:@"pageTitle" parentElement:RetailerDetailElement];
    //                    TBXMLElement *sDecElement = [TBXML childElementNamed:@"shortDesc" parentElement:RetailerDetailElement];
    //                    TBXMLElement *lDecrElement = [TBXML childElementNamed:@"longDesc" parentElement:RetailerDetailElement];
    //                    TBXMLElement *imageElement = [TBXML childElementNamed:@"retImagePath" parentElement:RetailerDetailElement];
    //                    TBXMLElement *dateElement = [TBXML childElementNamed:@"startDate" parentElement:RetailerDetailElement];
    //                    TBXMLElement *promoElement = [TBXML childElementNamed:@"endDate" parentElement:RetailerDetailElement];
    //
    //
    //                    if (titleElement) {
    //                        [specialDetails setObject:[TBXML textForElement:titleElement] forKey:@"Label"];
    //                    }
    //
    //                    if (sDecElement) {
    //                        [specialDetails setObject:[TBXML textForElement:sDecElement] forKey:@"shortDesc"];
    //                    }
    //
    //                    if (lDecrElement) {
    //                        [specialDetails setObject:[TBXML textForElement:lDecrElement] forKey:@"longDesc"];
    //                    }
    //
    //                    if (dateElement) {
    //                        [specialDetails setObject:[TBXML textForElement:dateElement] forKey:@"dateLabel"];
    //                    }
    //
    //                    if (imageElement) {
    //                        NSString *utf = [[TBXML textForElement:imageElement] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
    //                        [specialDetails setObject:utf forKey:@"splURL"];
    //                    }
    //
    //                    if (promoElement) {
    //                        NSString *promo = [NSString stringWithFormat:@"Promotion Ends: %@",[TBXML textForElement:promoElement]];
    //                        [specialDetails setObject:promo forKey:@"promoLabel"];
    //                    }
    //
    //                    RetailerDetailElement = [TBXML nextSiblingNamed:@"RetailerDetail" searchFromElement:RetailerDetailElement];
    //                }
    //
    //            }
    //
    //            [self setSepcialOfferDetailsView];
    //             [self addShareButton];
    //        }
    //    }
    //    else
    //    {
    //        TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
    //        // Banner the message given by the server
    //        [UtilityManager showAlert:@"Info" msg:[TBXML textForElement:responseTextElement]];
    //        return;
    //        
    //    }
}

-(void)parseLocationDetails:(NSString *)responseString
{
    
    //    responseString=@"<RetailerDetail><responseCode>10000</responseCode><responseText>Success</responseText><maxCount>4</maxCount><nextPageFlag>false</nextPageFlag><retDetailList><RetailerDetail><retailerId>1046</retailerId><retailerName>Deepthi_J</retailerName><retailLocationId>92466</retailLocationId><completeAddress>QWE2,MarbleFalls,TX,78654</completeAddress><city>MARBLE FALLS</city><state>TX</state><postalCode>78654</postalCode><rowNum>1</rowNum><retImagePath>http://localhost:8080/Images/retailer/1046/BannerAlbertso_98322.png</retImagePath></RetailerDetail><RetailerDetail><retailerId>1046</retailerId><retailerName>Deepthi_J</retailerName><retailLocationId>92617</retailLocationId><completeAddress>QWE2,MarbleFalls,TX,78654</completeAddress><city>MARBLE FALLS</city><state>TX</state><postalCode>78654</postalCode><rowNum>2</rowNum><retImagePath>http://localhost:8080/Images/retailer/1046/BannerAlbertso_98322.png</retImagePath></RetailerDetail></retDetailList></RetailerDetail>";
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retDetailList" parentElement:tbxml.rootXMLElement];
        
        if(retailerDetailElement)
        {
            TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
            while (RetailerDetailElement)
            {
                TBXMLElement *retailIdlement = [TBXML childElementNamed:@"retailerId" parentElement:RetailerDetailElement];
                TBXMLElement *retailerNamelement = [TBXML childElementNamed:@"retailerName" parentElement:RetailerDetailElement];
                TBXMLElement *retailLocIdElement = [TBXML childElementNamed:@"retailLocationId" parentElement:RetailerDetailElement];
                TBXMLElement *retailImagelement = [TBXML childElementNamed:@"retImagePath" parentElement:RetailerDetailElement];
                TBXMLElement *retailAddresslement = [TBXML childElementNamed:@"completeAddress" parentElement:RetailerDetailElement];
                
                RetailerData *iRetailData=[[RetailerData alloc]init];
                
                if (retailIdlement) {
                    iRetailData.retailId=[[NSString alloc]initWithString:[TBXML textForElement:retailIdlement]];
                }
                
                if (retailerNamelement) {
                    iRetailData.retailName=[[NSString alloc]initWithString:[TBXML textForElement:retailerNamelement]];
                }
                
                if (retailLocIdElement) {
                    iRetailData.retailLocId=[[NSString alloc]initWithString:[TBXML textForElement:retailLocIdElement]];
                }
                
                if (retailImagelement) {
                    iRetailData.retailImage=[[NSString alloc]initWithString:[TBXML textForElement:retailImagelement]];
                }
                
                if (retailAddresslement) {
                    iRetailData.completeAddr=[[NSString alloc]initWithString:[TBXML textForElement:retailAddresslement]];
                }
                
                [tableDetails addObject:iRetailData];
                
                RetailerDetailElement = [TBXML nextSiblingNamed:@"RetailerDetail" searchFromElement:RetailerDetailElement];
                
            }
            
        }
        [self setLocationsTableView];
    }
    else
    {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
        // Banner the message given by the server
        [UtilityManager showAlert:@"Info" msg:[TBXML textForElement:responseTextElement]];
        return;
        
    }
    
    
}

-(void)parse_RetSummary:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        [tableDetails removeAllObjects];
        [cancelBtn removeFromSuperview];
        [locationsTable removeFromSuperview];
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}


-(void)responseData: (NSString*)response{
    
    switch (iWebRequestState) {
            
        case SPECIAL_OFFER_SHARE:
            [self parse_SpecialOfferShare:response];
            break;
        case SPECIALS_REQUEST:
            [self parseSpecialOfferDetails:response];
            break;
        default:
            break;
    }
    
    
}

-(void)parse_SpecialOfferShare:(NSString*)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        //        TBXMLElement *retailerDetails = [TBXML childElementNamed:@"RetailersDetails" parentElement:tbXml.rootXMLElement];
        
        
        TBXMLElement *shareText=[TBXML childElementNamed:@"pageTitle" parentElement:tbXml.rootXMLElement];
        TBXMLElement *emailURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *imagePath = [TBXML childElementNamed:@"retImage" parentElement:tbXml.rootXMLElement];
        
        if (emailURL!=nil) {
            [defaults setValue:[TBXML textForElement:emailURL] forKey:KEY_HYPERLINK];
            //                NSLog(@"The link to be shared is %@",KEY_HYPERLINK);
        }
        
        if (shareText!=nil) {
            NSMutableString *msgString = [[NSMutableString alloc]init];
            if (shareFlag==YES) {
                [msgString appendFormat:@"I found this special offer page in the %@ mobile app and thought you might be interested:",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            }
            else
            {
                [msgString appendFormat:@"I found this anything page in the %@ mobile app and thought you might be interested:",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            }
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [msgString appendFormat:@"%@",shareString];
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            
        }
        
        if (imagePath!=nil) {
            [defaults setObject:[TBXML textForElement:imagePath] forKey:KEY_PRODUCTIMGPATH];
            //                NSLog(@"The product image path is %@",KEY_PRODUCTIMGPATH);
        }
        
        
        
        
        
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            return;
        }
        
    }
    
}

#pragma mark WebView Delegate Methods

-(void) webViewDidFinishLoad:(UIWebView *)webView
{
    [activityIndicator stopAnimating];
    [longDescriptionWebView.scrollView flashScrollIndicators];
    [shortDescritionWebView.scrollView flashScrollIndicators];
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    
    if ([[[inRequest URL] scheme] isEqual:@"mailto"]) {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,
                                                                                                                   (CFStringRef)value,
                                                                                                                   CFSTR("")));
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            //  [mailViewController release];
            return NO;
            
        }
        
        else {
            [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
        }
        
        return NO;
        
    }
    
    else  if ([[[inRequest URL] scheme] isEqual:@"tel"]) {
        NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
        
        NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
        NSString *phoneString  = [[defaultRecipient componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        NSString *phoneNumber = [NSString stringWithFormat:@"tel:%@", phoneString];
        NSURL    *url= [NSURL URLWithString:phoneNumber];
        
        UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        webview.alpha = 0.0;
        
        [webview loadRequest:[NSURLRequest requestWithURL:url]];
        
        // Assume we are in a view controller and have access to self.view
        [self.view insertSubview:webview belowSubview:dateLbl];
        //[webview release];
        return NO;
        
    }
    
    if ([[inRequest URL] fragment]) {
        
        return YES;
        [inWeb stringByEvaluatingJavaScriptFromString:@"window.location.hash='#hashtag'"];
        
        return YES;
    }
    
    
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        
        shareFlag=NO;
        
        
        
        NSString *url = [[inRequest URL]absoluteString];
        [defaults setValue:url forKey:KEY_URL];
        [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
        
        NSURL* urls = [NSURL URLWithString:[[inRequest URL]absoluteString]];
        
        
        NSString *urlString=[[inRequest URL]absoluteString];
        
        if ([[urls absoluteString] containsString:@"itunes.apple.com"]) {
            [UtilityManager customUrlLoad:urls];
        }
        
        else if ([urlString containsString:@"3000.htm"]) {
            // NSString *name =[NSString stringWithFormat:@"%@",[urls.pathComponents objectAtIndex:3]];
            
            
            
            if([urlString isEqualToString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    }
                else{
                    
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    splOfferVC.isEventLogisticsFlag = 1;
                    

                   
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            else{
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        else{
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
            
        }
        return NO;
    }
    
    
    
    
    
    return YES;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];// ModalViewControllerAnimated:YES];
}

#pragma mark TableView Delegate and DataSource

-(void)setLocationsTableView
{
    locationBtn.userInteractionEnabled=NO;
    
    float offset_Y = 0.08 * (SCREEN_HEIGHT);
    float offset_X = 0.08 * (SCREEN_WIDTH);
    
    locationsTable = [[UITableView alloc]initWithFrame:CGRectMake(offset_X, offset_Y, (SCREEN_WIDTH-(2*offset_X)), (SCREEN_HEIGHT-(4*offset_Y)))];
    locationsTable.delegate=self;
    locationsTable.dataSource=self;
    locationsTable.layer.cornerRadius = 10.0;
    locationsTable.layer.borderWidth = 1.0;
    locationsTable.layer.borderColor = [UIColor blackColor].CGColor;
    [self.view addSubview:locationsTable];
    
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        cancelBtn = [[UIButton alloc]initWithFrame:CGRectMake(locationsTable.frame.size.width-2+20, offset_Y-2 , 40, 40)];
    }
    else{
        cancelBtn = [[UIButton alloc]initWithFrame:CGRectMake(locationsTable.frame.size.width-2, offset_Y-2 , 30, 30)];
    }
    [cancelBtn setImage:[UIImage imageNamed:@"cross_icon.png"] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelBtn];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableDetails count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        return 70;
    else
        return 50;
}



-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UILabel *label;
    UILabel *detailLabel;
    SdImageView *imgView;
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor =  [UIColor clearColor];
        
        
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }
        
    }
    
    RetailerData *iData = [tableDetails objectAtIndex:indexPath.row];
    if (DEVICE_TYPE== UIUserInterfaceIdiomPad) {
        imgView=[[SdImageView alloc]initWithFrame:CGRectMake(10, 5, 60, 60)];
    }
    else{
        imgView=[[SdImageView alloc]initWithFrame:CGRectMake(5, 5, 40, 40)];
    }
    imgView.backgroundColor = [UIColor clearColor];
    imgView.layer.cornerRadius = 5.0f;
    [imgView loadImage:iData.retailImage];
    [cell.contentView addSubview:imgView];
    //  [imgView release];
    
    if (DEVICE_TYPE== UIUserInterfaceIdiomPad) {
        label=[[UILabel alloc]initWithFrame:CGRectMake(90, 5, tableView.frame.size.width-50, 25)];
        [label setFont:[UIFont boldSystemFontOfSize:18.0]];
    }
    else{
        label=[[UILabel alloc]initWithFrame:CGRectMake(50, 0, tableView.frame.size.width-50, 20)];
        [label setFont:[UIFont systemFontOfSize:12.0]];
    }
    label.text=iData.retailName;
    
    [label setTextColor:[UIColor blueColor]];
    [cell.contentView addSubview:label];
    //[label release];
    if (DEVICE_TYPE== UIUserInterfaceIdiomPad) {
        detailLabel=[[UILabel alloc]initWithFrame:CGRectMake(90, 33, tableView.frame.size.width-50, 25)];
        [detailLabel setFont:[UIFont systemFontOfSize:18.0]];
        
    }
    else{
        detailLabel=[[UILabel alloc]initWithFrame:CGRectMake(50, 22, tableView.frame.size.width-50, 20)];
        [detailLabel setFont:[UIFont systemFontOfSize:12.0]];
        
    }
    detailLabel.text=iData.completeAddr;
    [cell.contentView addSubview:detailLabel];
    //[detailLabel release];
    
    
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self requestRetailerSummary:(int)indexPath.row];
}

-(void)cancelPressed
{
    locationBtn.userInteractionEnabled=YES;
    
    [tableDetails removeAllObjects];
    [cancelBtn removeFromSuperview];
    [locationsTable removeFromSuperview];
}

-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil) {
        
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isEventLogisticsFlag = 1;
        [HubCitiAppDelegate setIsLogistics:YES];        
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}




-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}


#pragma mark Share methods
-(void)shareClicked
{
    iWebRequestState = SPECIAL_OFFER_SHARE;
    
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    
    [reqStr appendFormat:@"<ShareProductInfo><retailerId>%@</retailerId>",[defaults valueForKey:KEY_RETAILERID ]];
    [reqStr appendFormat:@"<pageId>%@</pageId><userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:@"nativePageId"],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharespecialoff",BASE_URL];
    DLog(@"%@",urlString);
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    
    [self responseData:response];
    
    
}


-(void)shareDetails{
    if (userIdentifier==TRUE) {
        
        [NewsUtility signupPopUp:self];
    }
    else
    {
        [self shareClicked];
        
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        // [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
    }
    
}
-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Special Offer Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    //    if(IPAD){
    //        actionSheet.popoverPresentationController.sourceView = btnCalendar;
    //        actionSheet.popoverPresentationController.sourceRect =  btnCalendar.bounds;
    //    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
                NSMutableString *userXmlStr;
                
                userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><pageId>%@</pageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:@"nativePageId"]];
                
                [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
         }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    //[defaults setValue:PRODUCT_SHARE forKey:KEY_SHARE_TYPE];
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
}



-(void)userTrack:(NSString*)xmlStr
{
    NSMutableString *userUrlStr=[[NSMutableString alloc]initWithFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:userUrlStr withParam:xmlStr]];
    DLog(@"%@",userUrlStr);
    DLog(@"%@",response);
    //[xmlStr release];
}
-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)locationButtonPressed:(id)sender {
    [self getLocationDetails];
}

@end

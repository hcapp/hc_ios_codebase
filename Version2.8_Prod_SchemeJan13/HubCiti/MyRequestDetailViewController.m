//
//  MyRequestDetailViewController.m
//  HubCiti
//
//  Created by Bindu M on 10/7/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//
#import "SpecialOffersViewController.h"
#import "MyRequestDetailViewController.h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "HubCitiConstants.h"
#import "AppDelegate.h"
#import "SingleCatRetailers.h"
#import "EventsListViewController.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "FindViewController.h"
#import "CityExperienceViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "SettingsViewController.h"
#import "MainMenuViewController.h"
#import "FilterListViewController.h"
#import "FilterRetailersList.h"
#import "CouponsViewController.h"
#import "FAQCategoryList.h"
#import "FundraiserListViewController.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "PreferredCategoriesScreen.h"
#import "WebBrowserViewController.h"
#import "AnythingPage.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "UserInformationViewController.h"
#import "HTTPClient.h"
#import "GetUserInfoResponse.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponsInDealViewController.h"



#define MAX_ITEM 10

@interface MyRequestDetailViewController (){
    NSArray *requestLblArray;
    NSArray *reqDescArray;
    NSMutableArray *serviceTypeArray;
    CGFloat desHeight;
    UIButton* submitButton,*cancelButton;
    CGFloat yValue;
    BOOL isKeyBoradOpen;
    NSString *emailID;
    UILabel *label[MAX_ITEM] ;
    UILabel *detailLabel[MAX_ITEM];
    UILabel *detailServiceLabel[MAX_ITEM];
    UILabel *serviceLbl[MAX_ITEM],*customerFieldLbl[MAX_ITEM] ;
    UITextField * serviceTextFld[MAX_ITEM],*customTextFld[MAX_ITEM];
    UIButton *updateFldBtn[MAX_ITEM];
    UITextView* textViewText[MAX_ITEM];
    float fontSize;
    NSString *pickerStr;
    UIToolbar* numberToolbar;
    bottomButtonView *view;
}
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@end

@implementation MyRequestDetailViewController
@synthesize arrBottomBtnDO, pageTitle,descNote, anyVC,  customReqResponse, ReqDetailArray, referenceNum, staticReqDictionary,infoResponse,emailSendingVC;
#pragma mark - Service Respone methods




- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    
    
    [HubCitiAppDelegate removeActivityIndicator];
    [NewsUtility showAlert:@"Network Error" msg:[error localizedDescription]];
    
    
}





- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject

{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    
    
    
    [self responseData:responseObject];
    
    
    
}
-(void) initTableData
{
    
    
    for (int j = 0; j< requestLblArray.count; j++) {
        
        
        label[j] = [self createLabel];
        detailLabel[j] = [self createLabel];
       
    }
    for (int j = 0; j< MAX_ITEM; j++)
    {
         detailServiceLabel[j] = [self createLabel];
    }
    
    NSString* address1 = [staticReqDictionary objectForKey:@"address1"];
    NSString* address2 = [staticReqDictionary objectForKey:@"address2"];
    NSString* city = [staticReqDictionary objectForKey:@"city"];
    NSString* state = [staticReqDictionary objectForKey:@"state"];
    NSString* zipCode = [staticReqDictionary objectForKey:@"zipCode"];
    
    if (![address1 isEqualToString:@""]) {
        [serviceTypeArray addObject:@"Service Address 1"];
    }
    
    if (![address2 isEqualToString:@""]) {
        [serviceTypeArray addObject:@"Service Address 2"];
    }
    
    if (![city isEqualToString:@""]) {
        [serviceTypeArray addObject:@"Service City"];
    }
    
    if (![state isEqualToString:@""]) {
        [serviceTypeArray addObject:@"Service State"];
    }
    
    if (![zipCode isEqualToString:@""]) {
        [serviceTypeArray addObject:@"Service Zip/Postal Code"];
    }
    
    for (int j = 0; j< serviceTypeArray.count; j++) {
        
        serviceLbl[j] = [self createLabel];
        serviceTextFld[j] = [self createTextField];
    }
    
    
    for (int j = 0; j< [ReqDetailArray count]; j++) {
        
        NSString* text = [[ReqDetailArray objectAtIndex:j] objectForKey:@"fldName"];
        if ([text rangeOfString:@"City"].location != NSNotFound || [text rangeOfString:@"State"].location != NSNotFound || [text rangeOfString:@"Number"].location != NSNotFound || [text rangeOfString:@"Name"].location != NSNotFound || [text rangeOfString:@"Zip"].location != NSNotFound || [text rangeOfString:@"Date"].location != NSNotFound)
        {
            customerFieldLbl[j] = [self createLabel];
            customTextFld[j] = [self createTextField];
            customTextFld[j].text = [[ReqDetailArray objectAtIndex:j]objectForKey:@"fldValue"];

        }
        else{
            customerFieldLbl[j] = [self createLabel];
            textViewText[j] = [self getTextView];
            textViewText[j].text = [[ReqDetailArray objectAtIndex:j]objectForKey:@"fldValue"];
        }
    }
    
    
}
-(void) dismissKeyBoard
{
    if (isKeyBoradOpen) {
        isKeyBoradOpen = false;
        [self animateTextView:NO];
        for(int i = 0; i< [ReqDetailArray count];i++)
        {
            [textViewText[i] resignFirstResponder];
        }
        for(int i = 0; i< serviceTypeArray.count;i++)
        {
            [serviceTextFld[i] resignFirstResponder];
        }
    }
    
}
-(void) HideKeyBard:(NSNotification *)noty
{
    if (isKeyBoradOpen) {
        isKeyBoradOpen = false;
        
        for(int i = 0; i< [ReqDetailArray count];i++)
        {
            [textViewText[i] resignFirstResponder];
        }
        for(int i = 0; i< serviceTypeArray.count;i++)
        {
            [serviceTextFld[i] resignFirstResponder];
        }
    }
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    numberToolbar = [[UIToolbar alloc]init];
    
   // int bottomBarHeight;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HideKeyBard:) name:@"HIDE" object:nil];
    
    emailID = [defaults stringForKey:@"emailID"];
    DLog(@"emailID %@ ", emailID);
    DLog(@"title %@ refNum %@",pageTitle , referenceNum);
    DLog(@"descNote %@", descNote);
    DLog(@"ReqDetailArray %@",ReqDetailArray);
    DLog(@"ReqDetailArray %@",staticReqDictionary);
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:pageTitle forView:self withHambergur:NO];

    
    if (IPAD) {
        fontSize = 16;
    }
    else
    {
        fontSize = 10;
    }
    
   
    self.navigationItem.hidesBackButton = YES;
    
    if (descNote == nil) {
        requestLblArray = [[NSArray alloc]initWithObjects:@"Service Request Type", @"Contact E-Mail",@"Reference Number", nil];
        reqDescArray = [[NSArray alloc]initWithObjects:pageTitle, emailID , referenceNum,nil]; //pageTitle, emailID ,nil
    }
    else
    {
        requestLblArray = [[NSArray alloc]initWithObjects:@"Service Request Type", @"Description", @"Contact E-Mail",@"Reference Number", nil];
        reqDescArray = [[NSArray alloc]initWithObjects:pageTitle, descNote,emailID, referenceNum ,nil];
    }
    
    serviceTypeArray = [[NSMutableArray alloc]init];
        
//        NSString* address1 = [staticReqDictionary objectForKey:@"address1"];
//        NSString* address2 = [staticReqDictionary objectForKey:@"address2"];
//        NSString* city = [staticReqDictionary objectForKey:@"city"];
//        NSString* state = [staticReqDictionary objectForKey:@"state"];
//        NSString* zipCode = [staticReqDictionary objectForKey:@"zipCode"];
    
    
//        if ([address1 isEqualToString:@""]) {
//        
//            if (![address2 isEqualToString:@""] )
//                serviceTypeArray = [[NSArray alloc]initWithObjects:@"Service Address 2",@"Service City", @"Service State", @"Service Zip/Postal Code", nil];
//            if (![city isEqualToString:@""]) {
//                
//            }
//           
//        }
//        else if ([address2 isEqualToString:@""] ) {
//            
//            if (![address1 isEqualToString:@""])
//            serviceTypeArray = [[NSArray alloc]initWithObjects:@"Service Address 1",@"Service City", @"Service State", @"Service Zip/Postal Code", nil];
//            
//        }
//
//        else if (![address1 isEqualToString:@""] && ![address2 isEqualToString:@""] ) {
//            
//            serviceTypeArray = [[NSArray alloc]initWithObjects:@"Service Address 1",@"Service Address 2",@"Service City", @"Service State", @"Service Zip/Postal Code", nil];
//          
//        }
//    
//        if (![address2 isEqualToString:@""])
//        {
//            serviceTypeArray = [[NSArray alloc]initWithObjects:@"Service Address 1",@"Service Address 2",@"Service City", @"Service State", @"Service Zip/Postal Code", nil];
//        }
//        else if (![address1 isEqualToString:@""] && ![address2 isEqualToString:@""] && ![city isEqualToString:@"" ] && ![state isEqualToString:@""] && ![zipCode isEqualToString:@""]) {
//            serviceTypeArray = [[NSArray alloc]initWithObjects:@"Service Address 1",@"Service City", @"Service State", @"Service Zip/Postal Code", nil];
//        
//        }

    
    [self initTableData];
    
    
    if(([arrBottomBtnDO count] > 0))
    {
        [self setBottomBarMenu];
       // bottomBarHeight = ( IPAD ? bottomBarButtonHeight_iPad : bottomBarButtonHeight);
    }
    else{
       // bottomBarHeight = 0;
    }
    
    
    CGRect submitFrame = CGRectMake(0,0,0,0);//VARIABLE_WIDTH(10), SCREEN_HEIGHT - bottomBarHeight - 2*VARIABLE_HEIGHT(72) + (IPAD ? VARIABLE_HEIGHT(25) : VARIABLE_HEIGHT(0)) , SCREEN_WIDTH - VARIABLE_WIDTH(20), VARIABLE_HEIGHT(40)
    submitButton =  [self addButton : @"Submit" : submitFrame];
    submitButton.tag = 1;
    submitButton.hidden = YES;
    
    CGRect cancelFrame = CGRectMake(0,0,0,0);//VARIABLE_WIDTH(10), submitFrame.origin.y + submitFrame.size.height + VARIABLE_HEIGHT(5), SCREEN_WIDTH - VARIABLE_WIDTH(20), VARIABLE_HEIGHT(40)
    cancelButton = [self addButton : @"Cancel" : cancelFrame];
    cancelButton.tag = 2;
    cancelButton.hidden = YES;
    
    [self settableViewOnScreen];
    pickerArrData= [[NSMutableArray alloc] initWithObjects:@"AK",@"AL",@"AR",@"AS",
                    @"AZ",@"CA",@"CO",@"CT",@"DC",@"DE",@"FL",@"FM",
                    @"GA",@"HI",@"IA",@"ID",@"IL",@"IN",@"KS",@"KY",
                    @"LA",@"MA",@"MD",@"ME",@"MI",@"MN",@"MO",@"MS",
                    @"MT",@"NC",@"ND",@"NE",@"NH",@"NJ",@"NM",@"NV",
                    @"NY",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",
                    @"TN",@"TX",@"UT",@"VA", @"VT",@"WA",@"WI",@"WV",
                    @"WY",nil];
    
    pickerStr = @"TX";
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
}
-(void)animateTextView: (BOOL)up
{
    
    const int movementDistance = -130;
    const float movementDuration = 0.3f;
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

-(void) showAlert : (NSString*) title
{
    
    [UtilityManager showAlert:nil msg:title];
    
    
}

-(BOOL) checkTextViewEntryText
{
    for (int i = 0; i<[ReqDetailArray count]; i++) {
        
        NSString* req = [[ReqDetailArray objectAtIndex:i] objectForKey:@"required"];
        int x = [req intValue];
        if(x == 1)
        {
            NSString* textFldValue = [[[[ReqDetailArray objectAtIndex:0]makeRequestDetailArray]objectAtIndex:i] objectForKey:@"name"];
            if ([textFldValue rangeOfString:@"City"].location != NSNotFound || [textFldValue rangeOfString:@"State"].location != NSNotFound || [textFldValue rangeOfString:@"Number"].location != NSNotFound || [textFldValue rangeOfString:@"Name"].location != NSNotFound || [textFldValue rangeOfString:@"Zip"].location != NSNotFound || [textFldValue rangeOfString:@"Date"].location != NSNotFound)
            {
                if (customTextFld[i].text.length == 0 ) {
                    [self showAlert : @"Mandatory fields cannot be empty"];
                    return FALSE;
                }
                
                
            }
            
            else
            {
                if (textViewText[i].text.length == 0 ) {
                    [self showAlert : @"Mandatory fields cannot be empty"];
                    return FALSE;
                }
            }
        }
    }
    return TRUE;
}

-(BOOL) checkTextFieldEntryText
{
    for (int i = 0; i< serviceTypeArray.count; i++){
        if (serviceTextFld[i].text.length == 0 ) {
            [self showAlert : @"Fields cannot be empty"];
            return FALSE;
        }
    }
    return TRUE;
}

-(void) buttonClicked : (id)sender
{
    UIButton* btn = (UIButton*) sender;
  //  BOOL isTextEntered = FALSE;
    if (btn.tag == 1) {
        
        //isTextEntered = [self checkTextViewEntryText];
//        if (!isTextEntered) {
//            
//            return;
//        }
        
       // [self requestForMakeReqDetails];
    }
    else if(btn.tag == 2){
        
        [self popBackToPreviousPage];
        
    }
    
}

-(UIButton*) addButton : (NSString*) string : (CGRect) frame
{
    UIButton* button;
    button  = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    button.layer.cornerRadius = 5.0f;
    button.titleLabel.font=[UIFont boldSystemFontOfSize: (IPAD ? 16.0 : 14.0)];
    button.backgroundColor = [UIColor convertToHexString:@"#AAA8AA"];
    [button setTitle:string forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setAccessibilityLabel:string];
    [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
    button.showsTouchWhenHighlighted=YES;
    [self.view addSubview:button];
    
    return button;
}

-(void)settableViewOnScreen
{
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    if (!detailReqTblView) {
        
        if([arrBottomBtnDO count] > 0){
            detailReqTblView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-50) style:UITableViewStylePlain];
        }
        else{
            detailReqTblView = [[UITableView alloc]initWithFrame:CGRectMake(0, -30, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStyleGrouped];
        }
    }

    detailReqTblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    detailReqTblView.dataSource=self;
    detailReqTblView.delegate=self;
    detailReqTblView.allowsSelection = NO;
    [detailReqTblView setBackgroundColor:[UIColor clearColor]];
    detailReqTblView.tableFooterView.hidden=YES;
    detailReqTblView.tableHeaderView.hidden=YES;
    detailReqTblView.translatesAutoresizingMaskIntoConstraints=YES;
    [self.view addSubview:detailReqTblView];
    
    
}



#pragma mark TextField methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self hidePickerView];
    if (!isKeyBoradOpen) {
        
        isKeyBoradOpen = true;
        [self animateTextView:YES];
    }
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    isKeyBoradOpen = false;
    [self hidePickerView];
    [self animateTextView:NO];
    
    return YES;
}
-(void) requestForUpdatingDetails:(id)sender{
    
    [self dismissKeyBoard];
    if([Network currentReachabilityStatus]==0){
        [UtilityManager showAlert];
        
    }
    else{
        [HubCitiAppDelegate showActivityIndicator];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:detailReqTblView];
        NSIndexPath *indexPath = [detailReqTblView indexPathForRowAtPoint:buttonPosition];
        
        
        NSLog(@"%@", textViewText[indexPath.row].text);
        
        NSString* fldValue = @"";
        
        NSString* textFldValue = [[ReqDetailArray objectAtIndex:indexPath.row] objectForKey:@"fldName"];
        if ([textFldValue rangeOfString:@"City"].location != NSNotFound || [textFldValue rangeOfString:@"State"].location != NSNotFound || [textFldValue rangeOfString:@"Number"].location != NSNotFound || [textFldValue rangeOfString:@"Name"].location != NSNotFound || [textFldValue rangeOfString:@"Zip"].location != NSNotFound || [textFldValue rangeOfString:@"Date"].location != NSNotFound)
        {
            fldValue = customTextFld[indexPath.row].text;
        }
        else
        {
            fldValue = textViewText[indexPath.row].text;
        }
        
        NSDictionary *dicParameters= @{@"referNum":referenceNum, @"fldName":[[ReqDetailArray objectAtIndex:indexPath.row] objectForKey:@"fldName"],
                                       @"fldValue": fldValue};
        
        DLog(@"dicParameters %@",dicParameters);
        
        NSString *baseUrl= [NSString stringWithFormat:@"%@govqa/updatereqcustfield",BASE_URL];
        [manager POST:baseUrl parameters:dicParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            [HubCitiAppDelegate removeActivityIndicator];
            [self parseUpdateDetails:responseObject];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [HubCitiAppDelegate removeActivityIndicator];
            
            NSLog(@"Error: %@", error);
        }];
    }
}

-(void)parseUpdateDetails:(id)responseObj{
    
    
    DLog(@"responseObj %@", responseObj);
    NSDictionary *responseData=responseObj;
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    DLog(@"responseCode %ld", (long)responseCode);
    DLog(@"responsestr %@", responseText);
    
    if (responseCode == 10000) {
        
        NSString *str = [NSString stringWithFormat:@"%@",@"Updated successfully"];
        [self showAlert:str];
    }
    else{
        [UtilityManager showAlert:nil msg:responseText];
      
        return;
    }
    
    
}

#pragma mark tableView delegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 0) {
        return [requestLblArray count];
    }
    if(section == 1)
    {
        return [serviceTypeArray count];
    }
    if(section == 2)
    {
        return [ReqDetailArray count];
    }
    
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        
        return [self lineCountForLabel : label[indexPath.row]]+ VARIABLE_HEIGHT(15);
    }
    
    if (indexPath.section == 1)
    {
        
        if ([[serviceTypeArray objectAtIndex:indexPath.row] isEqualToString:@"Service Address 1"]) {
            
            CGSize size = [self getStaticLabelSize:indexPath :[staticReqDictionary objectForKey:@"address1"]];
            desHeight = MAX(size.height, VARIABLE_HEIGHT(26));
            return desHeight;
          
        }
        else if ([[serviceTypeArray objectAtIndex:indexPath.row] isEqualToString:@"Service Address 2"]) {
            
            CGSize size = [self getStaticLabelSize:indexPath :[staticReqDictionary objectForKey:@"address2"]];
            desHeight = MAX(size.height, VARIABLE_HEIGHT(26));
            return desHeight;
            
        }
        else
        {
             return VARIABLE_HEIGHT(26);
        }
     
    }
    if (indexPath.section == 2)
    {
        NSString* text = [[ReqDetailArray objectAtIndex:indexPath.row] objectForKey:@"fldName"];
        if ([text rangeOfString:@"City"].location != NSNotFound || [text rangeOfString:@"State"].location != NSNotFound || [text rangeOfString:@"Number"].location != NSNotFound || [text rangeOfString:@"Name"].location != NSNotFound || [text rangeOfString:@"Zip"].location != NSNotFound || [text rangeOfString:@"Date"].location != NSNotFound)
            
        {
            return VARIABLE_HEIGHT(40);
        }
        return VARIABLE_HEIGHT(90);
    }
    
    return VARIABLE_HEIGHT(40.0);
    
    
}
-(BOOL)textView:(UITextView *)textView1 shouldChangeTextInRange:
(NSRange)range replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]) {
        
        [self hidePickerView];
        [textView1 resignFirstResponder];
        isKeyBoradOpen = false;
        
    }
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    NSLog(@"Did begin editing");
    
    [self hidePickerView];
    
    if (!isKeyBoradOpen) {
        isKeyBoradOpen = true;
        [self animateTextView:YES];
    }
    
}
-(void)textViewDidChange:(UITextView *)textView{
    
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    
    
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    if (isKeyBoradOpen) {
        isKeyBoradOpen = false;
        [self animateTextView:NO];
    }
    
    [self hidePickerView];
    
    
    return YES;
}
- (float)lineCountForLabel:(UILabel *) textLabel {
    
    CGSize constraint = CGSizeMake(VARIABLE_WIDTH(185) - VARIABLE_WIDTH(20), CGFLOAT_MAX);
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    CGSize size = [textLabel.text boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:fontSize], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    return size.height ;

    
}

-(CGSize) getLabelSize : (NSIndexPath*) indexPath
{
    CGSize constraint = CGSizeMake(VARIABLE_WIDTH(185) - VARIABLE_WIDTH(20), CGFLOAT_MAX);
    
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    CGSize size = [[[ReqDetailArray objectAtIndex:indexPath.row]objectForKey:@"fldName"] boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:fontSize], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    return size ;

}

-(CGSize) getStaticLabelSize : (NSIndexPath*) indexPath : (NSString*) text
{
    CGSize constraint = CGSizeMake(VARIABLE_WIDTH(185) - VARIABLE_WIDTH(20), CGFLOAT_MAX);
      NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    CGSize size = [text boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:fontSize], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    return size ;

}

-(NSString *) stringByStrippingHTML : (NSString*) s{
    NSRange r;
    
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    
    return s;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier1 = @"cell1";
    static NSString *CellIdentifier2 = @"cell2";
    static NSString *CellIdentifier3 = @"cell3";
    
    
    NSString *CellIdentifier;
    
    
    
    CGRect frame;
    UITableViewCell *cell = nil;
    
    
    if(indexPath.section==0)
        CellIdentifier=CellIdentifier1;
    else if(indexPath.section==1)
        CellIdentifier=CellIdentifier2;
    else
        CellIdentifier=CellIdentifier3;
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    NSArray *subviews=[cell.contentView subviews];
    
    [subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    
    if (indexPath.section == 0) {
        
        label[indexPath.row].text = [requestLblArray objectAtIndex:indexPath.row];
        
        label[indexPath.row].frame =  CGRectMake(VARIABLE_WIDTH(5), VARIABLE_HEIGHT(5), VARIABLE_WIDTH(120), [self lineCountForLabel : label[indexPath.row]]+ VARIABLE_HEIGHT(5));
        [cell.contentView addSubview:label[indexPath.row]];
        
        frame = CGRectMake(label[indexPath.row].frame.origin.x + label[indexPath.row].frame.size.width + VARIABLE_WIDTH(10), label[indexPath.row].frame.origin.y , VARIABLE_WIDTH(185), label[indexPath.row].frame.size.height );
        
        
        detailLabel[indexPath.row].frame = frame;
        detailLabel[indexPath.row].font = [UIFont systemFontOfSize:fontSize];
        detailLabel[indexPath.row].text = [reqDescArray objectAtIndex:indexPath.row];
        [cell.contentView addSubview:detailLabel[indexPath.row]];
        
    }
    else if (indexPath.section == 1){
      
            
            serviceLbl[indexPath.row].text = [serviceTypeArray objectAtIndex:indexPath.row];
            serviceLbl[indexPath.row].frame =  CGRectMake(VARIABLE_WIDTH(5), VARIABLE_HEIGHT(0), VARIABLE_WIDTH(120), [self lineCountForLabel : serviceLbl[indexPath.row]]+VARIABLE_HEIGHT(20));
            
            [cell.contentView addSubview:serviceLbl[indexPath.row]];
            
           
            
            frame = CGRectMake(serviceLbl[indexPath.row].frame.origin.x + serviceLbl[indexPath.row].frame.size.width  + VARIABLE_WIDTH(10),serviceLbl[indexPath.row].frame.origin.y + VARIABLE_HEIGHT(2), VARIABLE_WIDTH(160), VARIABLE_HEIGHT(26));
            detailServiceLabel[indexPath.row].frame = frame;
            detailServiceLabel[indexPath.row].font = [UIFont systemFontOfSize:fontSize];
            
            
            if ([[serviceTypeArray objectAtIndex:indexPath.row] isEqualToString:@"Service Address 1"]) {
                
                CGSize size = [self getStaticLabelSize:indexPath :[staticReqDictionary objectForKey:@"address1"]];
                desHeight = MAX(size.height, VARIABLE_HEIGHT(26));
                frame.size.height = desHeight;
                detailServiceLabel[indexPath.row].frame = frame;
                
                detailServiceLabel[indexPath.row].text = [staticReqDictionary objectForKey:@"address1"];
            }
            if ([[serviceTypeArray objectAtIndex:indexPath.row] isEqualToString:@"Service Address 2"]) {
                
                CGSize size = [self getStaticLabelSize:indexPath :[staticReqDictionary objectForKey:@"address2"]];
                desHeight = MAX(size.height, VARIABLE_HEIGHT(26));
                frame.size.height = desHeight;
                detailServiceLabel[indexPath.row].frame = frame;
                
                detailServiceLabel[indexPath.row].text = [staticReqDictionary objectForKey:@"address2"];
            }
            if ([[serviceTypeArray objectAtIndex:indexPath.row] isEqualToString:@"Service City"]) {
                
                detailServiceLabel[indexPath.row].text = [staticReqDictionary objectForKey:@"city"];
            }
            if ([[serviceTypeArray objectAtIndex:indexPath.row] isEqualToString:@"Service State"]) {
                
                detailServiceLabel[indexPath.row].text = [staticReqDictionary objectForKey:@"state"];
            }
            if ([[serviceTypeArray objectAtIndex:indexPath.row] isEqualToString:@"Service Zip/Postal Code"]) {
                
                detailServiceLabel[indexPath.row].text = [staticReqDictionary objectForKey:@"zipCode"];
                if (detailServiceLabel[indexPath.row].text.length == 0) {
                    detailServiceLabel[indexPath.row].text = [defaults valueForKey:@"Zip"];
                }
            }
            
            [cell.contentView addSubview:detailServiceLabel[indexPath.row]];
      
    }
    else if (indexPath.section == 2){
        
        
        customerFieldLbl[indexPath.row].text = [[ReqDetailArray objectAtIndex:indexPath.row]objectForKey:@"fldName"];
        
        customerFieldLbl[indexPath.row].frame =  CGRectMake(VARIABLE_WIDTH(5), VARIABLE_HEIGHT(5), VARIABLE_WIDTH(120), [self lineCountForLabel : customerFieldLbl[indexPath.row]] + VARIABLE_HEIGHT(15));
//        NSString* req = [[[[ReqDetailArray objectAtIndex:0]makeRequestDetailArray]objectAtIndex:indexPath.row] objectForKey:@"required"];
//        int x = [req intValue];
//        if(x == 1)
//        {
//            customerFieldLbl[indexPath.row].text = [customerFieldLbl[indexPath.row].text stringByAppendingFormat:@"%@",@"*"];
//            customerFieldLbl[indexPath.row].attributedText = [self getTagColor : customerFieldLbl[indexPath.row].text];
//            
//        }
        [cell.contentView addSubview:customerFieldLbl[indexPath.row]];
        
        NSString* text = [[ReqDetailArray objectAtIndex:indexPath.row] objectForKey:@"fldName"];
        if ([text rangeOfString:@"City"].location != NSNotFound || [text rangeOfString:@"State"].location != NSNotFound || [text rangeOfString:@"Number"].location != NSNotFound || [text rangeOfString:@"Name"].location != NSNotFound || [text rangeOfString:@"Zip"].location != NSNotFound || [text rangeOfString:@"Date"].location != NSNotFound)
        {
            frame = CGRectMake(customerFieldLbl[indexPath.row].frame.origin.x + customerFieldLbl[indexPath.row].frame.size.width  + VARIABLE_WIDTH(10), VARIABLE_HEIGHT(5), VARIABLE_WIDTH(140), VARIABLE_HEIGHT(30));
           // customTextFld[indexPath.row].text = [[ReqDetailArray objectAtIndex:indexPath.row]objectForKey:@"fldValue"];
            customTextFld[indexPath.row].frame = frame;
            customTextFld[indexPath.row].font = [UIFont systemFontOfSize:fontSize];
            customTextFld[indexPath.row].tag = indexPath.row;
            
            
            updateFldBtn[indexPath.row] = [UIButton buttonWithType:UIButtonTypeSystem];
            updateFldBtn[indexPath.row].frame = CGRectMake(customTextFld[indexPath.row].frame.origin.x + customTextFld[indexPath.row].frame.size.width  + VARIABLE_WIDTH(10), VARIABLE_HEIGHT(10), VARIABLE_WIDTH(20), VARIABLE_HEIGHT(20));
            [updateFldBtn[indexPath.row] addTarget:self action:@selector(updateRequestField:) forControlEvents:UIControlEventTouchUpInside];
            [updateFldBtn[indexPath.row] setBackgroundImage:[UIImage imageNamed:@"Update_Image.png"]forState:UIControlStateNormal];
            
            [cell.contentView addSubview:customTextFld[indexPath.row]];
            [cell.contentView addSubview:updateFldBtn[indexPath.row]];
            
        }
        else
        {
            frame = CGRectMake(customerFieldLbl[indexPath.row].frame.origin.x + customerFieldLbl[indexPath.row].frame.size.width  + VARIABLE_WIDTH(10), VARIABLE_HEIGHT(5), VARIABLE_WIDTH(140), VARIABLE_HEIGHT(60));
           // textViewText[indexPath.row].text = [[ReqDetailArray objectAtIndex:indexPath.row]objectForKey:@"fldValue"];
            textViewText[indexPath.row].frame = frame;
            textViewText[indexPath.row].font = [UIFont systemFontOfSize:fontSize];
            textViewText[indexPath.row].tag = indexPath.row;
            
            updateFldBtn[indexPath.row] = [UIButton buttonWithType:UIButtonTypeSystem];
            updateFldBtn[indexPath.row].frame = CGRectMake(textViewText[indexPath.row].frame.origin.x + textViewText[indexPath.row].frame.size.width  + VARIABLE_WIDTH(10), VARIABLE_HEIGHT(25), VARIABLE_WIDTH(20), VARIABLE_HEIGHT(20));
            [updateFldBtn[indexPath.row] addTarget:self action:@selector(updateRequestField:) forControlEvents:UIControlEventTouchUpInside];
            
            [updateFldBtn[indexPath.row] setBackgroundImage:[UIImage imageNamed:@"Update_Image.png"]forState:UIControlStateNormal];
            
            
            [cell.contentView addSubview:textViewText[indexPath.row]];
            [cell.contentView addSubview:updateFldBtn[indexPath.row]];
        }
        
        if ([text rangeOfString:@"contacted"].location != NSNotFound || [text rangeOfString:@"Number"].location != NSNotFound ||[text rangeOfString:@"Zip"].location != NSNotFound || [text rangeOfString:@"Amount"].location != NSNotFound || [text rangeOfString:@"Time"].location != NSNotFound)
        {
            customTextFld[indexPath.row].keyboardType = UIKeyboardTypeNumberPad;
            numberToolbar.frame = CGRectMake(0, 0, 320, 50);
            numberToolbar.barStyle = UIBarStyleBlackTranslucent;
            numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelOnNumberPad)],
                                    [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                    [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneOnNumberPad)]];
            [numberToolbar sizeToFit];
            
            customTextFld[indexPath.row].inputAccessoryView = numberToolbar;
        }
    }
    
    
    return cell;
    
}

-(void)cancelOnNumberPad{
    if (isKeyBoradOpen) {
        isKeyBoradOpen = false;
        [serviceTextFld[4] resignFirstResponder];
        for (int i = 0; i< [ReqDetailArray count]; i++) {
            if ([customerFieldLbl[i].text containsString:@"Number"] || [customerFieldLbl[i].text containsString:@"contacted"] || [customerFieldLbl[i].text containsString:@"Amount"] || [customerFieldLbl[i].text containsString:@"Zip"] || [customerFieldLbl[i].text containsString:@"Time"]) {
                [customTextFld[i] resignFirstResponder];
                // break;
            }
        }
        [self animateTextView:NO];
    }
    
    
}

-(void)doneOnNumberPad{
    if (isKeyBoradOpen) {
        isKeyBoradOpen = false;
        [serviceTextFld[4] resignFirstResponder];
        for (int i = 0; i< [ReqDetailArray count]; i++) {
            if ([customerFieldLbl[i].text containsString:@"Number"] || [customerFieldLbl[i].text containsString:@"contacted"]|| [customerFieldLbl[i].text containsString:@"Amount"] || [customerFieldLbl[i].text containsString:@"Zip"] || [customerFieldLbl[i].text containsString:@"Time"]) {
                [customTextFld[i] resignFirstResponder];
                // break;
            }
        }
        [self animateTextView:NO];
    }
    
}


-(void)updateRequestField:(id) sender{

    [self requestForUpdatingDetails:sender];
}



- (NSAttributedString *)getAttributedZoomTitle : (NSString*) title  {
    
    
    UIFont *fnt = [UIFont boldSystemFontOfSize:12];
    CGFloat offset = 1;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:[[NSMutableAttributedString alloc] initWithString:title]];
    
    NSRange smRange = [attributedString.string rangeOfString:@"*"];
    NSRange zoomRange = [attributedString.string rangeOfString:[title substringToIndex:[title rangeOfString:@"*"].location]];
    [attributedString setAttributes:@{NSFontAttributeName : fnt, NSBaselineOffsetAttributeName : @(offset)} range:smRange];
    [attributedString setAttributes:@{NSFontAttributeName : [fnt fontWithSize:9]} range:zoomRange];
    
    
    return attributedString;
}

-(NSMutableAttributedString*) getTagColor : (NSString*) text
{
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:text];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(text.length - 1,1)];
    
    return string;
}

-(UITextField*) createTextField
{
    UITextField* textField;
    textField = [[UITextField alloc]init];
    textField.backgroundColor = [UIColor clearColor];
    textField.layer.borderColor = [UIColor blackColor].CGColor;
    textField.layer.borderWidth = 1;
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.font = [UIFont systemFontOfSize:15];
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.delegate = self;
    
    return textField;
}
-(UILabel*) createLabel
{
    UILabel * labelView = [[UILabel alloc] init] ;
    labelView.translatesAutoresizingMaskIntoConstraints=YES;
    labelView.lineBreakMode = NSLineBreakByWordWrapping;
    labelView.numberOfLines = 0;
    labelView.textColor = [UIColor blackColor];
    labelView.font = [UIFont boldSystemFontOfSize:fontSize];
    return labelView;
}

//-(UIButton*)createButton{
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    button.titleLabel.font = [UIFont systemFontOfSize:15];
//    button.backgroundColor = [UIColor clearColor];
//    return button;
//}

-(UITextView*) getTextView
{
    UITextView * textField = [[UITextView alloc]init];
    textField.backgroundColor = [UIColor clearColor];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor = [UIColor blackColor].CGColor;
    textField.font = [UIFont systemFontOfSize:15];
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.delegate = self;
    return textField;
}
-(void)setBottomBarMenu
{
    
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [arrBottomBtnDO count]; btnLoop++)
    {
        bottomButtonDO *obj_eventBottomDO = [arrBottomBtnDO objectAtIndex:btnLoop];
        
        view = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, 80,bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
             view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrBottomBtnDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrBottomBtnDO count],bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_eventBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
    }
}

-(int)calculateLabelHeight:(NSString*)text labelWidth:(int)labelWidth fontSize:(int)Size
{
    CGSize constraint;
    CGSize size ;
    CGFloat heightWebview ;
    constraint = CGSizeMake(labelWidth, 20000.0f);
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    size = [text boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:Size], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    
    heightWebview = MAX(size.height+20, 40.0f);
    return heightWebview;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}


-(void) releaseMemory
{
    
   /* if(arrBottomButtonViewContainer)
        [arrBottomButtonViewContainer release];
    if(arrBottomBtnDO)
        [arrBottomBtnDO release];
    
    for (int i = 0; i < MAX_ITEM; i++) {
        
        if (customerFieldLbl[i]) {
            [customerFieldLbl[i] release];
        }
        if (textViewText[i]) {
            [textViewText[i] release];         }
        
    }
    
    for (int i = 0; i < MAX_ITEM; i++)
    {
        if (label[i]) {
            [label[i] release];
        }
        if (detailLabel[i]) {
            [detailLabel[i] release];
        }
        
    }
    for (int i = 0; i < MAX_ITEM; i++) {
        
        if (serviceLbl[i]) {
            [serviceLbl[i] release];
        }
        if (serviceTextFld[i]) {
            [serviceTextFld[i] release];
        }
    }
    
    if (serviceTypeArray) {
        [arrBottomBtnDO release];
    }
    if (descNote) {
        [descNote release];
    }
    if (detailReqTblView) {
        [detailReqTblView release];
    }
    if (requestLblArray) {
        [requestLblArray release];
    }
    if (reqDescArray) {
        [reqDescArray release];
    }
    
    if (ReqDetailArray) {
        [ReqDetailArray removeAllObjects];
        [ReqDetailArray release];
    }
    */
}




-(void)bottomButtonPressed:(id)sender
{
    [defaults setBool:NO forKey:@"ViewMore"];
    [defaults setBool:YES forKey:BottomButton];
    
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    //NSlog(@"The tag is %d",tag);
    bottomButtonDO *obj_bottomBottomDO = [arrBottomBtnDO  objectAtIndex:tag];
    
    [defaults setValue:obj_bottomBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_bottomBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_bottomBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_bottomBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_bottomBottomDO.btnLinkTypeName);
    if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    
    else{ //other menu types
        switch ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
               // [hotDeals release];
                break;
                
            case 1://Scan Now
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
                [self navigateToWhatsNearBy];
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                
                
                [self navigateToFindView];
            }
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
                
                break;
                
            case 8://Visitors Bureau
                
                break;
                
            case 9://transportation
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                   
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=TRUE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:obj_bottomBottomDO.btnLinkID];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                   
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                  //  CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];;
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
              //  [dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }break;
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                      //  [inform release];
                    }
                }
                
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                       
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                           
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                           // [citPref release];
                        }
                    }
                }
                
            }break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                //privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
            default:
                break;
        }
    }
    
}



-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}
-(void)parseGetUserData:(id)response
{
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}
-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
       
        
        isRemember = YES;
        
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}



-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}


-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}

// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case PARTNERRET:
            [self parse_PartnerRet:response];
            break;
            //        case gETPARTNER:
            //            [self parse_GetPartners:response];
            //            break;
            
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
                        
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
            
        default:
            break;
            
            
            
    }
    
}

-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}

//Call Find Service and Class
-(void)navigateToFindView
{
    
    //    [defaults setValue:@"0" forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    
    navigatedFromMainMenu = FALSE;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }

    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;

        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
    [self.navigationController popToRootViewControllerAnimated:NO];
    }
}
-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}


-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=FALSE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    else if ([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}
#pragma mark navigation barButtonItem actions
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
       
    }
    else
    {
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
        
    }
}

-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
        
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}



-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
           
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
           
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}

//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
     
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];                    //[alert release];
                    
                    //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        

        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}


-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    if (isKeyBoradOpen) {
        isKeyBoradOpen = false;
        [self animateTextView:NO];
    }
}
- (void)dealloc {
    
  //  [self releaseMemory];
    
    
}

#pragma mark Picker view methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [pickerArrData count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [pickerArrData objectAtIndex:row];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel *labelPick = [[UILabel alloc] init];
    if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
        if (IOS7){
            labelPick.frame = CGRectMake(20, 0, 220, 30);
        }
        
        else
            labelPick.frame = CGRectMake(0, 0, 200, 30);
    }
    else{
        labelPick.frame = CGRectMake(0, 0, 220, 50);
    }
    
    
    [labelPick setBackgroundColor:[UIColor clearColor]];
    if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
        [labelPick setFont:[UIFont boldSystemFontOfSize:16]];
    }
    else
        [labelPick setFont:[UIFont boldSystemFontOfSize:18]];
    
    
    labelPick.text = [pickerArrData objectAtIndex:row];
    
    return labelPick ;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    pickerStr =[[pickerArrData objectAtIndex:row] copy];
    
}


- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 30.0;
}


-(void)showStatePicker{
    
    if(!statePicker.superview){
        [self dismissKeyBoard];
        if (statePicker == nil) {
            if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
                statePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT/2+75, self.view.frame.size.width, 150)];
            }
            else
                statePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT/2+235, self.view.frame.size.width, 300)];
        }
        
        [self.view addSubview:statePicker];
        [statePicker setDataSource: self];
        [statePicker setDelegate: self];
        statePicker.hidden = NO;
        statePicker.showsSelectionIndicator = YES;
        statePicker.backgroundColor = [UIColor whiteColor];
        doneView = [[UIView alloc]initWithFrame:CGRectMake(0, statePicker.frame.origin.y-30, self.view.frame.size.width, 30)];
        doneView.backgroundColor = [UIColor lightGrayColor];
        UIButton *doneBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 0, 50, 30)];
        doneBtn.backgroundColor = [UIColor clearColor];
        [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
        [doneBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [doneBtn addTarget:self action:@selector(doneOnPicker) forControlEvents:UIControlEventTouchUpInside];
        [doneBtn setTintColor:[UIColor blackColor]];
        [doneView addSubview:doneBtn];
        UIButton *cancelPickBtn = [[UIButton alloc]initWithFrame:CGRectMake(doneView.frame.size.width-70, 0, 60, 30)];
        cancelPickBtn.backgroundColor = [UIColor clearColor];
        [cancelPickBtn setTitle:@"Cancel" forState:UIControlStateNormal];
        [cancelPickBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cancelPickBtn addTarget:self action:@selector(cancelOnPicker) forControlEvents:UIControlEventTouchUpInside];
        [cancelPickBtn setTintColor:[UIColor blackColor]];
        [doneView addSubview:cancelPickBtn];
        [self.view addSubview:doneView];
    }
}

-(void)doneOnPicker{
    statePicker.hidden = YES;
    doneView.hidden = YES;
    if (pickerStr == nil) {
        [pickerBtn setTitle:@"TX" forState:UIControlStateNormal];
    }
    else
    {
        [pickerBtn setTitle:pickerStr forState:UIControlStateNormal];
    }
    [statePicker removeFromSuperview];
    [doneView removeFromSuperview];
    
}

-(void)cancelOnPicker{
    statePicker.hidden = YES;
    doneView.hidden = YES;
    pickerStr = @"TX";
    [pickerBtn setTitle:@"TX" forState:UIControlStateNormal];
    [statePicker removeFromSuperview];
    [doneView removeFromSuperview];
    
}
-(void) hidePickerView
{
    if (statePicker.hidden == NO) {
        statePicker.hidden = YES;
        doneView.hidden = YES;
        [statePicker removeFromSuperview];
        [doneView removeFromSuperview];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
        [HubCitiAppDelegate setIsLogistics:YES];
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

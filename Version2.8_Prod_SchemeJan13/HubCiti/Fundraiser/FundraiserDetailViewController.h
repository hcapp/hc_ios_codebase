//
//  FundraiserDetailViewController.h
//  HubCiti
//
//  Created by Kitty on 24/11/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import "DWBubbleMenuButton.h"
NSDictionary *viewDictionary;
@class EventsListViewController;
@class AnyViewController;
@class EmailShareViewController;
@interface FundraiserDetailViewController: UIViewController<UIWebViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UITableViewDataSource,UITableViewDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate,DWBubbleMenuViewDelegate>
{
    UIScrollView *fundraiserView;
    
    UIImage *fundraiserImagePath;
    
    UILabel *fundraiserTitle;
    UILabel *fundraiserStartDate;
    UILabel *fundraiserEndDate;
    UILabel *fundraiserShortDescription;
    UIWebView *fundraiserLongDescription;
    UILabel *fundraiserCurrentLevel;
    UILabel *fundraiserPurchaseProducts;
    UIButton *fundraiserPurchaseProductslink;
    UILabel *fundraiserGoal;
    
    UIButton *moreInformation;
    UIButton *eventInformation;
    UIButton *locationInformation;
    
    NSMutableArray *arrFundraisingDetails;
    WebRequestState iWebRequestState;
    
    
    NSMutableArray *arrRetailers;
    NSMutableArray *arrRetailerAddress;
    NSMutableArray *arrRetailerLogo;
    NSMutableArray *arrRetailerId;
    NSMutableArray *arrRetailerLocId;
    NSMutableArray *arrRetailerLat;
    NSMutableArray *arrRetailerLong;
    NSMutableArray *arrDistance;

    //@Added by Keshava
    UITableView *tblEventLocations;
    UIButton    *cross_button;
    UIView  *tblEventHolder;
    
    
    
    NSArray *evtArrRetailers;
    NSArray *evtArrAddress;
    NSArray *evtArrAddress1;
    NSArray *evtArrAddress2;
    NSArray *evtArrRetailerLogo;
    NSArray *evtArrRetailerId;
    NSArray *evtArrRetailerLocId;
    NSArray *evtLatitude;
    NSArray *evtLongitude;
    NSString *evtStrEventName;
    NSString *evtLocation;
    NSMutableArray *evtArrRetailersAddress;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) EventsListViewController* iEventsListViewController;
@property(nonatomic,strong) AnyViewController * anyVC;
@end


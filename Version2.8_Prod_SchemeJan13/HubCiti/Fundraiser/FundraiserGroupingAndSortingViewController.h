//
//  FundraiserGroupingAndSortingViewController.h
//  HubCiti
//
//  Created by Kitty on 19/08/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FundraiserGroupingAndSortingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UITableView *tblGroupingNSorting;
    NSMutableArray *arrGroups;
    NSMutableArray *arrSorting;
    NSMutableArray *arrDepartMenu;
    int groupSelectionVal;
    int sortSelectionval;
    UIPickerView *pickerView;
    UIToolbar *pickerToolbar;
    int displayValue;
    int cityFlag;
    UIImageView *checkButton;
    BOOL isExpanded;
}

@property(nonatomic,strong)NSDictionary *citiesDic;

@property(nonatomic,strong)NSMutableArray *arrCities;

@property(nonatomic,strong)NSMutableArray *arrCitiId;

@property(nonatomic,strong)NSMutableArray *selectedCitiIds;

@property(nonatomic,strong) NSString *retailId;

@property(nonatomic,strong) NSString *retailLocationId;

@property(nonatomic) BOOL isFundraisingEvent,fundDeal;
@end

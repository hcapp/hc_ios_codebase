//
//  FundraiserListViewController.m
//  HubCiti
//
//  Created by Keerthi on 19/08/14.
//  Copyright (c) 2014 span infotech india ptv. ltd. All rights reserved.
//
#import "SpecialOffersViewController.h"
#import "FundraiserListViewController.h"
#import "FundraiserGroupingAndSortingViewController.h"
#import "HubCitiConstants.h"
#import "FundraiserCategoryDO.h"
#import "FundraiserDeatilsDO.h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "EventsListViewController.h"
#import "EventsCategoryDO.h"
#import "EventDetailsDO.h"
#import "EventListDetailViewController.h"
#import "EventGroupingAndSortingViewController.h"
#import "SettingsViewController.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "CityExperienceViewController.h"
#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "FindViewController.h"
#import "AnythingPage.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "AppDelegate.h"
#import "SingleCatRetailers.h"
//#import "ScanNowScreen.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FAQCategoryList.h"
#import "FilterListViewController.h"
#import "FundraiserDetailViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "HTTPClient.h"
#import "GetUserInfoResponse.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponsInDealViewController.h"
@interface FundraiserListViewController ()
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;


@end

@implementation FundraiserListViewController{
    UIActivityIndicatorView *loading;
    bottomButtonView *view;
    CustomizedNavController *cusNav;
}
@synthesize FundraiserTableview,retailLocationId,retailId,isFundraiserEvent,fundDeal,anyVC,infoResponse,emailSendingVC;
//@synthesize FundraiserTableview,retailLocationId,retailId,isFundraiserEvent,fundDeal,anyVC,emailSendingVC;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - Service Respone methods



- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{


    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}





- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject

{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    [self responseData:responseObject];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    // Default value for Grouping and Sorting
    [defaults setValue:@"date" forKey:@"GroupFundraiserBy"];
    [defaults setValue:@"date" forKey:@"SortFundraiserBy"];
    [defaults setValue:nil forKey:@"SelectedCityIds"];
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
    loading = nil;
    arrFundraisersCategoryInfo = [[NSMutableArray alloc]init];
   
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
//    UIButton *btn = [UtilityManager customizeBarButtonItem:@"Filter"];
//    [btn addTarget:self action:@selector(sortFundraisers:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
//    self.navigationItem.rightBarButtonItem = mainPage;
//    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"Filter"];
//    //[mainPage release];
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"Fundraisers" forView:self withHambergur:YES];

     //self.title = NSLocalizedString(@"Fundraisers", @"Fundraisers");
    
    //home button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
    
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;

    [self Request_Fundraiserslist];
    
   
    

    
}
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
-(void)popBackToPreviousPage{
//    if (SingleCatFlag==TRUE) {
//        if ([SingleEventCatID count]) {
//            [SingleEventCatID removeLastObject];
//            // [SingleEventCatID removeLastObject];
//        }
//    }
//    
//    else if (MultipleCatFlag==TRUE) {
//        if ([MultipleEventCatID count]) {
//            [MultipleEventCatID removeLastObject];
//        }
//    }

    if (fundDeal) {
        fundDeal = FALSE;
    }
    [FundraiserBottomButtonID removeLastObject];
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}


-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:NO];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
        rowCountOfTotalCells=0;
        [arrFundraisersCategoryInfo removeAllObjects];
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        [self Request_Fundraiserslist];
        
    }
    [super viewWillAppear:animated];
}

-(void)sortFundraisers:(id)sender
{
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
    FundraiserGroupingAndSortingViewController *iFundraiserGroupingAndSortingViewController = [[FundraiserGroupingAndSortingViewController alloc]initWithNibName:@"FundraiserGroupingAndSortingViewController" bundle:[NSBundle mainBundle]];
    if(isFundraiserEvent == TRUE)
    {
        iFundraiserGroupingAndSortingViewController.isFundraisingEvent = TRUE;
    }
  
    if ( fundDeal == TRUE) {
        iFundraiserGroupingAndSortingViewController.fundDeal = TRUE;
    }
    
    iFundraiserGroupingAndSortingViewController.retailId = retailId ;
    iFundraiserGroupingAndSortingViewController.retailLocationId = retailLocationId ;
    [self.navigationController pushViewController:iFundraiserGroupingAndSortingViewController animated:NO];
   // [iFundraiserGroupingAndSortingViewController release];
}

-(void)settableViewOnScreen
{
    if(FundraiserTableview==nil){
        int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
        if(bottomBtn==1){
            FundraiserTableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-50) style:UITableViewStylePlain];
        }
        else{
            FundraiserTableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-yVal) style:UITableViewStylePlain];
        }
        FundraiserTableview.dataSource=self;
        FundraiserTableview.delegate=self;
        [FundraiserTableview setBackgroundColor:[UIColor whiteColor]];
        FundraiserTableview.tableFooterView.hidden=YES;
        [self.view addSubview:FundraiserTableview];
    }
}


-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }

    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
    [self.navigationController popToRootViewControllerAnimated:NO];
    }
}



-(void)bottomButtonPressed:(id)sender
{
    [defaults setBool:NO forKey:@"ViewMore"];
    [defaults setBool:YES forKey:BottomButton];
    //    [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
//    SingleEventFromMainMenu=FALSE;
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    bottomButtonDO *obj_eventBottomDO = [arrFundraisersBottomButtonDO objectAtIndex:tag];
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=27||[[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=28)
    [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_eventBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_eventBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_eventBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_eventBottomDO.btnLinkTypeName);
    if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];

            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    else if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_eventBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_eventBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
             self.iEventsListViewController = iEventsListViewController;
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    else{ //other menu types
        
        switch ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                self.hotDeals = hotDeals;
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                //[hotDeals release];
                break;
                
            case 1://Scan Now
            {
                //[self navigateToScanNow];
            }
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                //[self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [NearbyBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];

                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:obj_eventBottomDO.bottomBtnID];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                [self navigateToFindView];
            }
                break;
                
            case 6://City Experience
            {
        
            
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];

                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                 self.cityExp = citi;
                [self.navigationController pushViewController:citi animated:NO];
                [CityExperienceBottomButtonID addObject:obj_eventBottomDO.bottomBtnID];
            }
                //[citi release];
                break;
                
            case 7://City Services
            {
                
            }
                
                break;
                
            case 8://Visitors Bureau
            {
                
            }
                
                break;
                
            case 9://transportation
            {
                
            }
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];}
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=TRUE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_eventBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                   self.cityExp = cevc;
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    self.filters = filters;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    self.filterList = filterList;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
               // [dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                self.faqList = faqCatList;
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
//                                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
//                                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
//                                //[iFundraiserListViewController release];
//                
            }
            break;
                
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{

                iWebReqState = GETUSERINFO;
                [self requestToGetUserData];
                }
            }break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{

                iWebReqState = GETFAVLOCATIONS;
                [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{

                if([Network currentReachabilityStatus]==0)
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                }
                else{
                    
                    UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:inform animated:NO];
                  //  [inform release];
                }
                
                }
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{

                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    }
                    else{
                        [SharedManager setUserInfoFromSignup:NO];
                        CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                        [self.navigationController pushViewController:citPref animated:NO];
                        //[citPref release];
                    }
                }
                }
                
            }break;
                
            case 27:
            case 28://FIlter
            {
                [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
                FundraiserGroupingAndSortingViewController *iFundraiserGroupingAndSortingViewController = [[FundraiserGroupingAndSortingViewController alloc]initWithNibName:@"FundraiserGroupingAndSortingViewController" bundle:[NSBundle mainBundle]];
                if(isFundraiserEvent == TRUE)
                {
                    iFundraiserGroupingAndSortingViewController.isFundraisingEvent = TRUE;
                }
                
                if ( fundDeal == TRUE) {
                    iFundraiserGroupingAndSortingViewController.fundDeal = TRUE;
                }
                
                iFundraiserGroupingAndSortingViewController.retailId = retailId ;
                iFundraiserGroupingAndSortingViewController.retailLocationId = retailLocationId ;
                [self.navigationController pushViewController:iFundraiserGroupingAndSortingViewController animated:NO];
               // [iFundraiserGroupingAndSortingViewController release];
            }
                break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
               // privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;

        
            default:
                break;
        }
    }
    //NSlog(@"%d",tag);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    for (NSOperation *operation in [HTTPClient sharedHTTPClient].manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        NSLog(@"operation:%@",operation);
        AFHTTPRequestOperation *operations = (AFHTTPRequestOperation*)operation;
        if([[[[operations request] URL] absoluteString] containsString:@"hotdeals/getfundrserlistjson"]){
            [operation cancel];
        }
        
    }
    
    //   [[HTTPClient sharedHTTPClient].manager.operationQueue cancelAllOperations];
    
}

#pragma mark Request Methods


-(void)Request_Fundraiserslist
{
    iWebReqState = FundraisersList;
    
    NSMutableDictionary* param = [[NSMutableDictionary alloc] init];
    [param setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    [param setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    [param setValue:@"IOS" forKey:@"platform"];
    
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length] && ([[HubCitiManager sharedManager]gps_allow_flag] == YES) &&([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse))
    {
        [param setValue:[defaults  valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        [param setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
       
    }
    if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
         [param setValue:[defaults  valueForKey:KEYZIPCODE] forKey:@"postalCode"];
        
    }
    [param setValue:[defaults  valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    [param setValue:[NSString stringWithFormat:@"%d",rowCountOfTotalCells] forKey:@"lowerLimit"];
    
    if ([[defaults valueForKey:@"SortFundraiserBy"]isEqualToString:@"department"]) {
        [param setValue:[defaults valueForKeyPath:@"departmentId"] forKey:@"departmentId"];
        
    }
    
    else if ([[defaults valueForKey:@"SortFundraiserBy"] isEqualToString:@"City"] && [defaults valueForKeyPath:@"commaSeperatedCities"]) {
        [param setValue:[defaults valueForKeyPath:@"commaSeperatedCities"] forKey:@"cityIds"];
        
    }
    [param setValue:[defaults valueForKeyPath:@"GroupFundraiserBy"] forKey:@"groupBy"];
    [param setValue:[defaults valueForKeyPath:@"SortFundraiserBy"] forKey:@"sortBy"];
    
    
    NSString *urlString ;
    
    
    if (fundDeal == TRUE) {
        
        urlString = [NSString stringWithFormat:@"%@hotdeals/getfundrserlistjson",BASE_URL];

       // urlString = [NSString stringWithFormat:@"%@hotdeals/fundraiserslist",BASE_URL];
    }
    
    else{
        
        if(isFundraiserEvent == TRUE)
        {
            [param setValue:retailId forKey:@"retailId"];
            [param setValue:retailLocationId forKey:@"retailLocationId"];
            
        }
        
        else{
            if (SingleCatFlag==TRUE && SingleEventFromMainMenu==TRUE){
                if ([defaults valueForKey:KEY_MITEMID]) {
                     [param setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
                    
                }
            }
            
            else if (SingleCatFlag==TRUE){
                if ([SingleEventCatID count]!=0) {
                    [param setValue:[SingleEventCatID objectAtIndex:[SingleEventCatID count]-1] forKey:@"bottomBtnId"];
                    
                }
            }
            else if (MultipleCatFlag==TRUE){
                if ([MultipleEventCatID count]!=0)
                {
                    [param setValue:[MultipleEventCatID objectAtIndex:[MultipleEventCatID count]-1] forKey:@"bottomBtnId"];
                    
                }
                else{
                    [param setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
                    
                }
            }
            
            
            else {
                if([defaults valueForKey:KEY_MITEMID]){
                     [param setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
                }
                else if ([FundraiserBottomButtonID count]>=1)
                {
                     [param setValue:[FundraiserBottomButtonID objectAtIndex:[FundraiserBottomButtonID count]-1] forKey:@"bottomBtnId"];
                }
                    
                else{
                     [param setValue:[defaults valueForKey:KEY_BOTTOMBUTTONID] forKey:@"bottomBtnId"];
                   
                }
            }
         
        }
        
        [param setValue:@"0" forKey:@"catId"];
        
        urlString = [NSString stringWithFormat:@"%@alertevent/getfundrserlistjson",BASE_URL];
        
       // urlString = [NSString stringWithFormat:@"%@alertevent/getfundrserlistjson",BASE_URL];

    }
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    DLog(@"Param: %@",param);
    DLog(@"Url: %@",urlString);
    if (![defaults boolForKey:@"ViewMore"]) {
        [HubCitiAppDelegate showActivityIndicator];
    }
    else{
        [HubCitiAppDelegate removeActivityIndicator];
    }
    [client sendRequest : param : urlString];
    
    
    
    
   /* iWebReqState = FundraisersList;
    
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<MenuItem><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<platform>%@</platform>",@"ios"];
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    
    
    
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length] && ([[HubCitiManager sharedManager]gps_allow_flag] == YES) &&([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse))
    {
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    [requestStr appendFormat:@"<lowerLimit>%d</lowerLimit>",rowCountOfTotalCells];
    
    if ([[defaults valueForKey:@"SortFundraiserBy"]isEqualToString:@"department"]) {
        [requestStr appendFormat:@"<departmentId>%@</departmentId>",[defaults valueForKey:@"departmentId"]];
    }
    
    else if ([[defaults valueForKey:@"SortFundraiserBy"] isEqualToString:@"City"] && [defaults valueForKeyPath:@"commaSeperatedCities"]) {
        [requestStr appendFormat:@"<cityIds>%@</cityIds>",[defaults valueForKeyPath:@"commaSeperatedCities"]];
    }
    
    [requestStr appendFormat:@"<groupBy>%@</groupBy>",[defaults valueForKey:@"GroupFundraiserBy"]];
    [requestStr appendFormat:@"<sortBy>%@</sortBy>",[defaults valueForKey:@"SortFundraiserBy"]];
    
    
    
    NSString *urlString ;
    
    
    if (fundDeal == TRUE) {
        
        urlString = [NSString stringWithFormat:@"%@hotdeals/fundraiserslist",BASE_URL];
    }
    
    
    else{
        
        if(isFundraiserEvent == TRUE)
        {
            [requestStr appendFormat:@"<retailId>%@</retailId><retailLocationId>%@</retailLocationId>",retailId,retailLocationId];
        }
        
        
        
        else{
            if (SingleCatFlag==TRUE && SingleEventFromMainMenu==TRUE){
                if ([defaults valueForKey:KEY_MITEMID]) {
                    [requestStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];
                }
            }
            
            else if (SingleCatFlag==TRUE){
                if ([SingleEventCatID count]!=0) {
                    [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[SingleEventCatID objectAtIndex:[SingleEventCatID count]-1]];
                }
            }
            else if (MultipleCatFlag==TRUE){
                if ([MultipleEventCatID count]!=0)
                {
                    [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[MultipleEventCatID objectAtIndex:[MultipleEventCatID count]-1]];
                }
                else{
                    [requestStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];
                }
            }
            
            
            else {
                if([defaults valueForKey:KEY_MITEMID]){
                    [requestStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
                else if ([FundraiserBottomButtonID count]>=1)
                    [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FundraiserBottomButtonID objectAtIndex:[FundraiserBottomButtonID count]-1]];
                else{
                    [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                }
                
                //                if([defaults valueForKey:KEY_BOTTOMBUTTONID])
                //                    [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                //                else
                //                    [requestStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];
            }
        }
        
        [requestStr appendFormat:@"<catId>0</catId>"];
        urlString = [NSString stringWithFormat:@"%@alertevent/fundrserlist",BASE_URL];
    }
    
    [requestStr appendFormat:@"</MenuItem>"];
    
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
        
        
        [self parse_FundraiserList:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    }
    
*/
    
}
-(void)request_HubcitiAnythingInfo{
    
    iWebReqState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=TRUE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if (![defaults boolForKey:BottomButton]){
        
        if([defaults valueForKey:KEY_MITEMID])
            [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else{
        if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebReqState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if(![defaults boolForKey:BottomButton]){
        if ([defaults valueForKey:KEY_MITEMID])
            [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else {
        
        if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

//-(void)request_GetUserCat{
//    
//    iWebReqState = GETFAVCATEGORIES;
//    if([Network currentReachabilityStatus]==0)
//    {
//        [common showAlert];
//    }
//    else {
//        
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//        
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//        
//    }
//}


-(void)requestToGetPreferredLocations
{
    iWebReqState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}

//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebReqState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if(![defaults boolForKey:BottomButton]){
        if ([defaults valueForKey:KEY_MITEMID])
            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else {
        
        if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}



#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        return;
    
    [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    [self showActionSheet];
    
}

-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}




-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}








#pragma mark parse methods





#pragma mark parse methods
-(void)responseData:(NSString *) response
{
    switch (iWebReqState)
    {
        case FundraisersList:
            [self parse_FundraiserList:response];
            rowCountOfTotalCells = 0;
            [FundraiserTableview reloadData];
            break;
            
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
        case CITIEXPRET:
            
            [self parse_CitiExpRet:response];
            
            break;
        case PARTNERRET:
            [self parse_PartnerRet:response];
            break;
//        case GETPARTNERS:
//             [self parse_GetPartners:response];
//              break;
            
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
            
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
//        case catsearch:
//            [self parse_sscatsearch:response];
//            break;
        default:
            break;
    }
}

-(void)parse_FundraiserList:(NSString*)responseString
{
    
    
    NSDictionary* responseData = (NSDictionary*) responseString;
    
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    
    bottomBtn = [[responseData objectForKey:@"bottomBtn"] intValue];
    
   
    
    if (responseCode == 10000) {
        
        isNextPageAvailable =  [[responseData objectForKey:@"nextPage"] intValue];
        
        [defaults setValue:[responseData objectForKey:KEY_MAINMENUID] forKey:KEY_MAINMENUID];
        
        NSString* departFlag = [[responseData objectForKey:@"isDeptFlag"] stringValue];
        
        if (departFlag) {
            [defaults setValue:departFlag forKey:@"fundDepartmentFlag"];
        }
       
        
        DLog(@"%@",[defaults valueForKey:@"fundDepartmentFlag"]);
        
        NSArray*  categoryListElement = [responseData objectForKey:@"categoryList"];
        
        for (int i = 0; i<categoryListElement.count; i++) {
            BOOL isCatRepeted = NO;
            arrFundraisersBottomButtonDO =[[NSMutableArray alloc]init];
            FundraiserCategoryDO *iFundraisersCategoryDO = [[FundraiserCategoryDO alloc]init];
            iFundraisersCategoryDO.catObjArray = [[NSMutableArray alloc]init];
            iFundraisersCategoryDO.categoryId = [[categoryListElement[i] objectForKey:@"categoryId"] stringValue];
            iFundraisersCategoryDO.categoryName = [categoryListElement[i] objectForKey:@"groupContent"];
            
            if([[defaults valueForKey:@"GroupFundraiserBy"]isEqualToString:@"type"])
            {
                if([arrFundraisersCategoryInfo count] > 0)
                {
                    FundraiserCategoryDO *iFundraisersCategoryDOrepted;
                    
                    for(int i=0; i<[arrFundraisersCategoryInfo count]; i++)
                    {
                        iFundraisersCategoryDOrepted = [arrFundraisersCategoryInfo objectAtIndex:i];
                        isCatRepeted = NO;
                        if([iFundraisersCategoryDOrepted.categoryId isEqualToString:iFundraisersCategoryDO.categoryId])
                        {
                            isCatRepeted = YES;
                            
                            [iFundraisersCategoryDO.catObjArray addObjectsFromArray:iFundraisersCategoryDOrepted.catObjArray];
                            
                            [arrFundraisersCategoryInfo replaceObjectAtIndex:i withObject:iFundraisersCategoryDO];
                            break;
                        }
                        
                    }
                    if(isCatRepeted==NO)
                    {
                        [arrFundraisersCategoryInfo addObject:iFundraisersCategoryDO];
                    }
                    
                }
                else
                {
                    [arrFundraisersCategoryInfo addObject:iFundraisersCategoryDO];
                    
                }
            }
            else
            {
                // Check if Last Object of Category and Incoming First Category both are same then append data
                if([arrFundraisersCategoryInfo count] > 0)
                {
                    FundraiserCategoryDO *iFundraisersCategoryDOrepted;
                    
                    for(int i=0;i<[arrFundraisersCategoryInfo count]; i++)
                    {
                        iFundraisersCategoryDOrepted = [arrFundraisersCategoryInfo objectAtIndex:i];
                        isCatRepeted=NO;
                        if([iFundraisersCategoryDOrepted.categoryName isEqualToString:iFundraisersCategoryDO.categoryName])
                        {
                            isCatRepeted = YES;
                            
                            [iFundraisersCategoryDO.catObjArray addObjectsFromArray:iFundraisersCategoryDOrepted.catObjArray];
                            
                            [arrFundraisersCategoryInfo replaceObjectAtIndex:i withObject:iFundraisersCategoryDO];
                            
                            break;
                            
                        }
                    }
                    if(isCatRepeted==NO)
                    {
                        [arrFundraisersCategoryInfo addObject:iFundraisersCategoryDO];
                    }
                    
                }
                else
                {
                    [arrFundraisersCategoryInfo addObject:iFundraisersCategoryDO];
                    
                }
                
             }
            
           // --------
            NSArray* FundraiserDetailsElement = [categoryListElement[i] objectForKey:@"fundraiserList"];
            
            for (int i = 0; i< FundraiserDetailsElement.count; i++) {
                FundraiserDeatilsDO *iFundraiserDetailsDO = [[FundraiserDeatilsDO alloc]init];
                iFundraiserDetailsDO.fundraiserId = [FundraiserDetailsElement[i] objectForKey:@"fundId"];
                iFundraiserDetailsDO.title = [FundraiserDetailsElement[i] objectForKey:@"fundName"];
                iFundraiserDetailsDO.imagePath = [FundraiserDetailsElement[i] objectForKey:@"imgPath"];
                iFundraiserDetailsDO.startDate = [FundraiserDetailsElement[i] objectForKey:@"startDate"];
                iFundraiserDetailsDO.fundraiserListId = [FundraiserDetailsElement[i] objectForKey:@"fundCatId"];
                iFundraiserDetailsDO.imagePath = [FundraiserDetailsElement[i] objectForKey:@"imgPath"];
                [iFundraisersCategoryDO.catObjArray addObject:iFundraiserDetailsDO];
                
            }
            
        }
       
        if (bottomBtn==1) {
            
            arrFundraisersBottomButtonDO = [[NSMutableArray alloc]init];
            
            NSArray* bottomArray = [responseData objectForKey:@"bottomBtnList"];
            
            for(int i = 0; i< bottomArray.count; i++){
                
                bottomButtonDO *bottomButton = [[bottomButtonDO alloc]init];
                
                bottomButton.bottomBtnID  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnID"];
                
                bottomButton.bottomBtnName  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnName"];
                
                bottomButton.bottomBtnImg  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImg"];
                
                bottomButton.bottomBtnImgOff  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImgOff"];
                
                bottomButton.btnLinkTypeID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeID"];
                
                bottomButton.btnLinkTypeName  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeName"];
                
                bottomButton.btnLinkID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkID"];
                
                bottomButton.position  = [[bottomArray objectAtIndex:i] objectForKey:@"position"];
                
                if ([bottomButton.btnLinkTypeName isEqualToString:@"Filters"]) {
                     [SharedManager setRetGroupId:[bottomButton.btnLinkID intValue]];
                }
               
                
                [arrFundraisersBottomButtonDO addObject:bottomButton];
              
            }
            
            if([arrFundraisersBottomButtonDO count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                [self setBottomBarMenu];
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            
            
            
        }
        
        NSString* maxCount = [responseData objectForKey:@"maxCnt"];
        if (maxCount.length > 0) {
            
            if ([maxCount isEqualToString:@"1"] ) {
                maxFundraiserCountFlag=TRUE;
                NSIndexPath *indexPath=[NSIndexPath indexPathForItem:0 inSection:0];
                [self tableView:FundraiserTableview didDeselectRowAtIndexPath:indexPath];
                
            }
        }
        
         [self settableViewOnScreen];
    }
    
    else if(responseCode == 10005)
    {
        if (bottomBtn==1) {
            
            arrFundraisersBottomButtonDO = [[NSMutableArray alloc]init];
            
            NSArray* bottomArray = [responseData objectForKey:@"bottomBtnList"];
            
            for(int i = 0; i< bottomArray.count; i++){
                
                bottomButtonDO *bottomButton = [[bottomButtonDO alloc]init];
                
                bottomButton.bottomBtnID  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnID"];
                
                bottomButton.bottomBtnName  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnName"];
                
                bottomButton.bottomBtnImg  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImg"];
                
                bottomButton.bottomBtnImgOff  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImgOff"];
                
                bottomButton.btnLinkTypeID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeID"];
                
                bottomButton.btnLinkTypeName  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeName"];
                
                bottomButton.btnLinkID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkID"];
                
                bottomButton.position  = [[bottomArray objectAtIndex:i] objectForKey:@"position"];
                
                if ([bottomButton.btnLinkTypeName isEqualToString:@"Filters"]) {
                    [SharedManager setRetGroupId:[bottomButton.btnLinkTypeID intValue]];
                }
                
                
                [arrFundraisersBottomButtonDO addObject:bottomButton];
                
            }
            
            if([arrFundraisersBottomButtonDO count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                [self setBottomBarMenu];
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            
            
            if([responseText isEqualToString:@"No Records Found."])
                
            {
                
                [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseText];
                
            }
            
            else{
                
                NSString *responseTextStr = [responseText stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
                
                [UtilityManager showFormatedAlert:responseTextStr];
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
                [self settableViewOnScreen];
                
            });
        }
    }
    
    else
    {
        if(responseText.length == 0)
        {
            responseText = @"No Records found";
        }
        
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseText];
        [loading stopAnimating];
        [FundraiserTableview reloadData];
        [defaults setBool:NO forKey:@"ViewMore"];
        
    }
    
}


/*-(void)parse_FundraiserList:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *maxCountElement = [TBXML childElementNamed:@"maxCnt" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbxml.rootXMLElement];
        if(nextPageElement)
            isNextPageAvailable = [[TBXML textForElement:nextPageElement]boolValue];
        
        TBXMLElement *categoryListElement = [TBXML childElementNamed:@"categoryList" parentElement:tbxml.rootXMLElement];
        
        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbxml.rootXMLElement]]intValue];
        TBXMLElement *departFlagElement = [TBXML childElementNamed:@"isDeptFlag" parentElement:tbxml.rootXMLElement];
        if (departFlagElement) {
            [defaults setValue:[TBXML textForElement:departFlagElement]forKey:@"fundDepartmentFlag"];
        }
        
        DLog(@"%@",[defaults valueForKey:@"fundDepartmentFlag"]);
        
        if(categoryListElement)
        {
            TBXMLElement *CategoryInfoElement = [TBXML childElementNamed:@"CategoryInfo" parentElement:categoryListElement];
            while (CategoryInfoElement)
            {
                BOOL isCatRepeted = NO;
                arrFundraisersBottomButtonDO =[[NSMutableArray alloc]init];
                TBXMLElement *categoryIdElement = [TBXML childElementNamed:@"categoryId" parentElement:CategoryInfoElement];
                TBXMLElement *groupContentElement = [TBXML childElementNamed:@"groupContent" parentElement:CategoryInfoElement];
                
                FundraiserCategoryDO *iFundraisersCategoryDO = [[FundraiserCategoryDO alloc]init];
                
                iFundraisersCategoryDO.catObjArray = [[NSMutableArray alloc]init];
                
                if(categoryIdElement)
                    iFundraisersCategoryDO.categoryId = [TBXML textForElement:categoryIdElement];
                if(groupContentElement)
                    iFundraisersCategoryDO.categoryName = [TBXML textForElement:groupContentElement];
                
                if([[defaults valueForKey:@"GroupFundraiserBy"]isEqualToString:@"type"])
                {
                    // Check if Last Object of Category and Incoming First Category both are same then append data
                    if([arrFundraisersCategoryInfo count] > 0)
                    {
                        FundraiserCategoryDO *iFundraisersCategoryDOrepted;
                        
                        for(int i=0; i<[arrFundraisersCategoryInfo count]; i++)
                        {
                            iFundraisersCategoryDOrepted = [arrFundraisersCategoryInfo objectAtIndex:i];
                            isCatRepeted = NO;
                            if([iFundraisersCategoryDOrepted.categoryId isEqualToString:iFundraisersCategoryDO.categoryId])
                            {
                                isCatRepeted = YES;
                                
                                [iFundraisersCategoryDO.catObjArray addObjectsFromArray:iFundraisersCategoryDOrepted.catObjArray];
                                
                                [arrFundraisersCategoryInfo replaceObjectAtIndex:i withObject:iFundraisersCategoryDO];
                                break;
                            }
                            
                        }
                        if(isCatRepeted==NO)
                        {
                            [arrFundraisersCategoryInfo addObject:iFundraisersCategoryDO];
                        }
                        
                    }
                    else
                    {
                        [arrFundraisersCategoryInfo addObject:iFundraisersCategoryDO];
                        
                    }
                }
                else
                {
                    // Check if Last Object of Category and Incoming First Category both are same then append data
                    if([arrFundraisersCategoryInfo count] > 0)
                    {
                        FundraiserCategoryDO *iFundraisersCategoryDOrepted;
                        
                        for(int i=0;i<[arrFundraisersCategoryInfo count]; i++)
                        {
                            iFundraisersCategoryDOrepted = [arrFundraisersCategoryInfo objectAtIndex:i];
                            isCatRepeted=NO;
                            if([iFundraisersCategoryDOrepted.categoryName isEqualToString:iFundraisersCategoryDO.categoryName])
                            {
                                isCatRepeted = YES;
                                
                                [iFundraisersCategoryDO.catObjArray addObjectsFromArray:iFundraisersCategoryDOrepted.catObjArray];
                                
                                [arrFundraisersCategoryInfo replaceObjectAtIndex:i withObject:iFundraisersCategoryDO];
                                
                                break;
                                
                            }
                        }
                        if(isCatRepeted==NO)
                        {
                            [arrFundraisersCategoryInfo addObject:iFundraisersCategoryDO];
                        }
                        
                    }
                    else
                    {
                        [arrFundraisersCategoryInfo addObject:iFundraisersCategoryDO];
                        
                    }
                    
                }
                
                TBXMLElement *fundraiserListElement = [TBXML childElementNamed:@"fundraiserList" parentElement:CategoryInfoElement];
                
                if(fundraiserListElement)
                {
                    TBXMLElement *FundraiserDetailsElement = [TBXML childElementNamed:@"Fundraiser" parentElement:fundraiserListElement];
                    
                    while (FundraiserDetailsElement)
                    {
                        FundraiserDeatilsDO *iFundraiserDetailsDO = [[FundraiserDeatilsDO alloc]init];
                        
                        TBXMLElement *eventIdElement = [TBXML childElementNamed:@"fundId" parentElement:FundraiserDetailsElement];
                        TBXMLElement *eventNameElement = [TBXML childElementNamed:@"fundName" parentElement:FundraiserDetailsElement];
                        
                        TBXMLElement *imgPathElement = [TBXML childElementNamed:@"imgPath" parentElement:FundraiserDetailsElement];
                        
                        TBXMLElement *startDateElement = [TBXML childElementNamed:@"startDate" parentElement:FundraiserDetailsElement];
                        
                        
                        //                        TBXMLElement *mItemExistElement = [TBXML childElementNamed:@"mItemExist" parentElement:EventDetailsElement];
                        TBXMLElement *fundListIdElement = [TBXML childElementNamed:@"fundCatId" parentElement:FundraiserDetailsElement];
                        //                        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:EventDetailsElement];
                        
                        
                        if(eventIdElement)
                            iFundraiserDetailsDO.fundraiserId = [TBXML textForElement:eventIdElement];
                        
                        if(eventNameElement)
                            iFundraiserDetailsDO.title = [TBXML textForElement:eventNameElement];
                        
                        
                        if(imgPathElement)
                            iFundraiserDetailsDO.imagePath = [TBXML textForElement:imgPathElement];
                        
                        
                        if(startDateElement)
                            iFundraiserDetailsDO.startDate = [TBXML textForElement:startDateElement];
                        
                        
                        if(fundListIdElement)
                            iFundraiserDetailsDO.fundraiserListId = [TBXML textForElement:fundListIdElement];
                        
                        
                        FundraiserDetailsElement = [TBXML nextSiblingNamed:@"Fundraiser" searchFromElement:FundraiserDetailsElement];
                        
                        
                        [iFundraisersCategoryDO.catObjArray addObject:iFundraiserDetailsDO];
                        //[iFundraiserDetailsDO release];
                    }
                    
                }
                //[iFundraisersCategoryDO release];
                
                CategoryInfoElement = [TBXML nextSiblingNamed:@"CategoryInfo" searchFromElement:CategoryInfoElement];
            }
            
        }
        
        if (bottomBtn==1) {
            
            TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbxml.rootXMLElement];
            TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
            while(BottomButtonElement)
            {
                
                bottomButtonDO *obj_eventBottomDO = [[bottomButtonDO alloc]init];
                
                
                TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                
                
                if(bottomBtnIDElement)
                    obj_eventBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                
                if(bottomBtnNameElement)
                    obj_eventBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                else
                    obj_eventBottomDO.bottomBtnName = @" ";
                
                if(bottomBtnImgElement)
                    obj_eventBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                else
                    obj_eventBottomDO.bottomBtnImg = @" ";
                
                if(bottomBtnImgOffElement)
                    obj_eventBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                else
                    obj_eventBottomDO.bottomBtnImgOff = @" ";
                
                if(btnLinkTypeIDElement)
                    obj_eventBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                
                if(btnLinkTypeNameElement)
                    obj_eventBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                
                if(btnLinkIDElement){
                    obj_eventBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                    if(btnLinkTypeNameElement){
                        if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
                            [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
                    }
                }
                
                if(positionElement)
                    obj_eventBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                
                
                
                BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                [arrFundraisersBottomButtonDO addObject:obj_eventBottomDO];
                // [obj_eventBottomDO release];
            }
            if([arrFundraisersBottomButtonDO count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                [self setBottomBarMenu];
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            
        }
        if (maxCountElement) {
            
            NSString *maxCount = [[NSString alloc]initWithString:[TBXML textForElement:maxCountElement]];
            if ([maxCount isEqualToString:@"1"]&&fromDealsView==TRUE) {
                maxFundraiserCountFlag=TRUE;
                NSIndexPath *indexPath=[NSIndexPath indexPathForItem:0 inSection:0];
                [self tableView:FundraiserTableview didDeselectRowAtIndexPath:indexPath];
                
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
        [self settableViewOnScreen];
        });
    }
    
    else if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10005"])
    {
        
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbxml.rootXMLElement];
        if(nextPageElement)
            isNextPageAvailable = [[TBXML textForElement:nextPageElement]boolValue];
        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbxml.rootXMLElement]]intValue];
        
        if (bottomBtn==1) {
            
            TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbxml.rootXMLElement];
            if (bottomButtonListElement)
            {
                arrFundraisersBottomButtonDO =[[NSMutableArray alloc]init];
                
                TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
                while(BottomButtonElement)
                {
                    
                    bottomButtonDO *obj_eventBottomDO = [[bottomButtonDO alloc]init];
                    
                    
                    TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                    TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                    
                    
                    if(bottomBtnIDElement)
                        obj_eventBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                    
                    if(bottomBtnNameElement)
                        obj_eventBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                    else
                        obj_eventBottomDO.bottomBtnName = @" ";
                    
                    if(bottomBtnImgElement)
                        obj_eventBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                    else
                        obj_eventBottomDO.bottomBtnImg = @" ";
                    
                    if(bottomBtnImgOffElement)
                        obj_eventBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                    else
                        obj_eventBottomDO.bottomBtnImgOff = @" ";
                    
                    if(btnLinkTypeIDElement)
                        obj_eventBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                    
                    if(btnLinkTypeNameElement)
                        obj_eventBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                    
                    if(btnLinkIDElement){
                        obj_eventBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                        if(btnLinkTypeNameElement){
                            if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
                                [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
                        }
                    }
                    
                    if(positionElement)
                        obj_eventBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                    
                    
                    
                    BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                    [arrFundraisersBottomButtonDO addObject:obj_eventBottomDO];
                    // [obj_eventBottomDO release];
                }
                if([arrFundraisersBottomButtonDO count] > 0)
                {
                    // Set Bottom Menu Button after read from response data
                    [self setBottomBarMenu];
                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                    
                }
                else
                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            }
        }
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
      
        dispatch_async(dispatch_get_main_queue(), ^{
        [self settableViewOnScreen];
        });
    }
    
    
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
        [loading stopAnimating];
        ReleaseAndNilify(loading);
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
    [FundraiserTableview reloadData];
    });
    [defaults setBool:NO forKey:@"ViewMore"];
}
*/



-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            self.cityExp = cevc;
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            self.filters = pvc;

            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}

/*
 *
 Parsing logic when response received for getting user details.
 Method called when navigated from Settings screen.
 *
 */

-(void)parse_CitiExpRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [defaults setObject:nil forKey:@"Response_Austin"];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Austin"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            self.cityExp = cevc;
            [defaults setInteger:100 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
        }
    }
}

//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                              [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                        [HubCitiAppDelegate setIsLogistics:YES];
                        splOfferVC.isEventLogisticsFlag = 1;
                    
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}

-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}




    
//-(void) navigateToScanNow
//{
//    [defaults setBool:YES forKey:@"KEY_SCANSPLASH"];
//    [defaults setValue:@"scanNow" forKey:KEY_SEARCHFROM];
//    [defaults setObject:@"YES" forKey:@"KEY_SCANNERCALLED"];
//    
//    int menulevel = [[defaults valueForKey:KEY_MENULEVEL]integerValue];
//    if(menulevel > 1)
//        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
//    else
//        [defaults  setBool:NO forKey:@"DisplayBackBarButton"];
//    
//    
//    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
//    ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];
//    [self.navigationController pushViewController:viewScanNowScreen animated:NO];
//    [viewScanNowScreen release];
//    
//}

-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}

//Call Find Service and Class
-(void)navigateToFindView
{
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    [defaults setBool:YES forKey:BottomButton];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    navigatedFromMainMenu=false;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}

-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
     self.iEventsListViewController = iEventsListViewController;
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}


-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}




-(void)releaseAllocatedObjects
{
    NSArray *arrSubViews = [self.view subviews];
    for(int i=0; i<[arrSubViews count]; i++)
    {
        UIView *releaseObjView = (UIView*)[arrSubViews objectAtIndex:i];
        if(releaseObjView.tag == 99)
            [releaseObjView removeFromSuperview];
        if([releaseObjView isKindOfClass:[bottomButtonView class]])
        {
            [releaseObjView removeFromSuperview];
        }
    }
    
    if([arrFundraisersBottomButtonDO count]>0)
    {
        [arrFundraisersBottomButtonDO removeAllObjects];
        //[arrFundraisersBottomButtonDO release];
        arrFundraisersBottomButtonDO = nil;
    }
    if([arrBottomButtonViewContainer count]>0)
    {
        [arrBottomButtonViewContainer removeAllObjects];
        //[arrBottomButtonViewContainer release];
        arrBottomButtonViewContainer = nil;
    }
    
}




// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    // Calculate Y position for View
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
        /*UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-55, SCREEN_WIDTH, 55)];
         tabImage.image = [UIImage imageNamed:@"BlankBottomButton.png"];
         [self.view addSubview:tabImage];
         [tabImage setAccessibilityValue:@"Bottom"];
         [self.view bringSubviewToFront:tabImage];
         //[tabImage release];
*/
    }
    
    for(int btnLoop= 0; btnLoop < [arrFundraisersBottomButtonDO count]; btnLoop++)
    {
        bottomButtonDO *obj_eventBottomDO = [arrFundraisersBottomButtonDO objectAtIndex:btnLoop];
         view = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, 80,bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            /*view.contentMode = UIViewContentModeCenter;
             view = [bottomButtonView initWithTitle:obj_mainBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-55, 80,55) imageName:obj_mainBottomDO.bottomBtnImg img_off:obj_mainBottomDO.bottomBtnImg_Off delegate:self tag:btnLoop];*/
            
          
            
        }
        else
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrFundraisersBottomButtonDO  count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrFundraisersBottomButtonDO  count],bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_eventBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        //view.tag=99;
        [self.view bringSubviewToFront:view];
    }
}



#pragma mark tableView delegate and datasource methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 50.0;
    }
    else
    {
        return 80.0;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 20.0;
    }
    else
    {
        return 30.0;
    }}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arrFundraisersCategoryInfo count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    FundraiserCategoryDO *iFundraisersCategoryDO = [arrFundraisersCategoryInfo objectAtIndex:section];
    
    rowCountOfTotalCells = rowCountOfTotalCells+ (int)[iFundraisersCategoryDO.catObjArray count];
    if(isNextPageAvailable && section == [arrFundraisersCategoryInfo count]-1)
        return [iFundraisersCategoryDO.catObjArray count]+1;
    else
        return [iFundraisersCategoryDO.catObjArray count];
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    FundraiserCategoryDO *iFundraisersCategoryDO = [arrFundraisersCategoryInfo objectAtIndex:section];
    UILabel *lblCatName;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH - 10, 18)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
    }
    else
    {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH - 10, 28)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
    }
    [lblCatName setTextColor:[UIColor blackColor]];
    [lblCatName setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
    
    [lblCatName setText:[NSString stringWithFormat:@" %@",iFundraisersCategoryDO.categoryName]];
    return lblCatName;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellEventListView";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.backgroundColor =  [UIColor clearColor];
        
		cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *fundView = [arr objectAtIndex:i];
            [fundView removeFromSuperview];
        }
    }
    
    
    FundraiserCategoryDO *iFundraisersCategoryDO = [arrFundraisersCategoryInfo objectAtIndex:indexPath.section];
    
    if(isNextPageAvailable && indexPath.row == [iFundraisersCategoryDO.catObjArray count] )
    {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
//        CGRect frame;
//        frame.origin.x = 0;
//        frame.origin.y = 16;
//        frame.size.width = 320;
//        frame.size.height = 24;
//        UILabel *label = [[[UILabel alloc] init] ;
//       
//        label.textAlignment = NSTextAlignmentCenter;
//        label.textColor = [UIColor colorWithRGBInt:0x112e72];
//        label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
//        label.translatesAutoresizingMaskIntoConstraints=NO;
//        [cell.contentView addSubview:label];
//        cell.accessoryType = UITableViewCellAccessoryNone;
//        
//        NSDictionary *vDictionary = @{@"label":label};
//        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
//            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[label(24)]-|"] options:0 metrics:0 views:vDictionary]];
//            
//            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(320)]-|"] options:0 metrics:0 views:vDictionary]];
//            label.font = [UIFont boldSystemFontOfSize:16];
//        }
//        else
//        {
//            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[label(44)]-|"] options:0 metrics:0 views:vDictionary]];
//            
//            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(768)]-|"] options:0 metrics:0 views:vDictionary]];
//            label.font = [UIFont boldSystemFontOfSize:20];
//        }
    }
    else
    {
        FundraiserDeatilsDO *Evtdetail = [iFundraisersCategoryDO.catObjArray objectAtIndex:indexPath.row];
        
        if(Evtdetail.imagePath)
        {
            
            CGRect frame;
            if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
                frame.origin.x = 5;
                frame.origin.y = 5;
                frame.size.width = 40;
                frame.size.height = 40;
            }
            else
            {
                frame.origin.x = 5;
                frame.origin.y = 5;
                frame.size.width = 60;
                frame.size.height = 60;
            }
            
           // asyncImageView = [[SdImageView alloc] initWithFrame:frame] ;
            SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:frame];
            asyncImageView.backgroundColor = [UIColor clearColor];
           // asyncImageView.layer.cornerRadius = 5.0f;
            [asyncImageView loadImage:Evtdetail.imagePath];
            asyncImageView.translatesAutoresizingMaskIntoConstraints=NO;
            [cell.contentView addSubview:asyncImageView];
            NSDictionary *iDictionary = @{@"image":asyncImageView};
            
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(3)-[image(44)]"] options:0 metrics:0 views:iDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(44)]"] options:0 metrics:0 views:iDictionary]];
            }
            else
            {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[image(60)]"] options:0 metrics:0 views:iDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[image(60)]"] options:0 metrics:0 views:iDictionary]];
            }
            
            //[asyncImageView release];
        }
        
        int start_X = 55;
        
        if(Evtdetail.startDate)
        {
            NSMutableString *strDateandTime = [[NSMutableString alloc]initWithString:Evtdetail.startDate];
            
            UILabel *lblEvtName = [[UILabel alloc]init];
            [lblEvtName setText:strDateandTime];
            [lblEvtName setTextColor:[UIColor blackColor]];
            [lblEvtName setBackgroundColor:[UIColor clearColor]];
           
            lblEvtName.translatesAutoresizingMaskIntoConstraints=NO;
            [cell.contentView addSubview:lblEvtName];
            
            NSDictionary *lblDictionary=@{@"lbl":lblEvtName};
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(23)-[lbl(20)]"] options:0 metrics:0 views:lblDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(55)-[lbl(%f)]",(SCREEN_WIDTH-start_X)] options:0 metrics:0 views:lblDictionary]];
                 [lblEvtName setFont:[UIFont systemFontOfSize:12]];
            }
            else
            {
                start_X=85;
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(33)-[lbl(30)]"] options:0 metrics:0 views:lblDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(85)-[lbl(%f)]",(SCREEN_WIDTH-start_X)] options:0 metrics:0 views:lblDictionary]];
                 [lblEvtName setFont:[UIFont systemFontOfSize:17]];
            }
            
           // [lblEvtName release];
        }

        UILabel *lblEvtName = [[UILabel alloc]initWithFrame:CGRectMake(start_X, 3, SCREEN_WIDTH - start_X-15, 20)];//(57, 15, SCREEN_WIDTH - 50, 20)
        [lblEvtName setText: Evtdetail.title];
        lblEvtName.numberOfLines=1;
        [lblEvtName setTextColor:[UIColor blackColor]];
        [lblEvtName setBackgroundColor:[UIColor clearColor]];
        lblEvtName.translatesAutoresizingMaskIntoConstraints=NO;
        [cell.contentView addSubview:lblEvtName];
        
        NSDictionary *dictionary= @{@"label":lblEvtName};
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(3)-[label(20)]"] options:0 metrics:0 views:dictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(55)-[label(%f)]",(SCREEN_WIDTH-start_X)] options:0 metrics:0 views:dictionary]];
            [lblEvtName setFont:[UIFont systemFontOfSize:15]];
        }
        else
        {
            start_X=85;
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[label(30)]"] options:0 metrics:0 views:dictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(85)-[label(%f)]",(SCREEN_WIDTH-start_X)] options:0 metrics:0 views:dictionary]];
            [lblEvtName setFont:[UIFont systemFontOfSize:20]];
        }
        
      //  [lblEvtName release];
        
        
    }
    
    return cell;
}


/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellEventListView";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.backgroundColor =  [UIColor clearColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *fundView = [arr objectAtIndex:i];
            [fundView removeFromSuperview];
        }
    }
    
    
    FundraiserCategoryDO *iFundraisersCategoryDO = [arrFundraisersCategoryInfo objectAtIndex:indexPath.section];
    
    if(isNextPageAvailable && indexPath.row == [iFundraisersCategoryDO.catObjArray count] )
    {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
        //        CGRect frame;
        //        frame.origin.x = 0;
        //        frame.origin.y = 16;
        //        frame.size.width = 320;
        //        frame.size.height = 24;
        //        UILabel *label = [[[UILabel alloc] init] ;
        //
        //        label.textAlignment = NSTextAlignmentCenter;
        //        label.textColor = [UIColor colorWithRGBInt:0x112e72];
        //        label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
        //        label.translatesAutoresizingMaskIntoConstraints=NO;
        //        [cell.contentView addSubview:label];
        //        cell.accessoryType = UITableViewCellAccessoryNone;
        //
        //        NSDictionary *vDictionary = @{@"label":label};
        //        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        //            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[label(24)]-|"] options:0 metrics:0 views:vDictionary]];
        //
        //            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(320)]-|"] options:0 metrics:0 views:vDictionary]];
        //            label.font = [UIFont boldSystemFontOfSize:16];
        //        }
        //        else
        //        {
        //            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[label(44)]-|"] options:0 metrics:0 views:vDictionary]];
        //
        //            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(768)]-|"] options:0 metrics:0 views:vDictionary]];
        //            label.font = [UIFont boldSystemFontOfSize:20];
        //        }
    }
    else
    {
        FundraiserDeatilsDO *Evtdetail = [iFundraisersCategoryDO.catObjArray objectAtIndex:indexPath.row];
        
        if(Evtdetail.imagePath)
        {
            
            CGRect frame;
            if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
                frame.origin.x = 5;
                frame.origin.y = 5;
                frame.size.width = 40;
                frame.size.height = 40;
            }
            else
            {
                frame.origin.x = 5;
                frame.origin.y = 5;
                frame.size.width = 60;
                frame.size.height = 60;
            }
            
            // asyncImageView = [[SdImageView alloc] initWithFrame:frame] ;
            SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:frame];
            asyncImageView.backgroundColor = [UIColor clearColor];
            // asyncImageView.layer.cornerRadius = 5.0f;
            [asyncImageView loadImage:Evtdetail.imagePath];
            asyncImageView.translatesAutoresizingMaskIntoConstraints=NO;
            [cell.contentView addSubview:asyncImageView];
            NSDictionary *iDictionary = @{@"image":asyncImageView};
            
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(3)-[image(44)]"] options:0 metrics:0 views:iDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(44)]"] options:0 metrics:0 views:iDictionary]];
            }
            else
            {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[image(60)]"] options:0 metrics:0 views:iDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[image(60)]"] options:0 metrics:0 views:iDictionary]];
            }
            
            //[asyncImageView release];
        }
        
        int start_X = 55;
        
        if(Evtdetail.startDate)
        {
            NSMutableString *strDateandTime = [[NSMutableString alloc]initWithString:Evtdetail.startDate];
            
            UILabel *lblEvtName = [[UILabel alloc]init];
            [lblEvtName setText:strDateandTime];
            [lblEvtName setTextColor:[UIColor blackColor]];
            [lblEvtName setBackgroundColor:[UIColor clearColor]];
            
            lblEvtName.translatesAutoresizingMaskIntoConstraints=NO;
            [cell.contentView addSubview:lblEvtName];
            
            NSDictionary *lblDictionary=@{@"lbl":lblEvtName};
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(23)-[lbl(20)]"] options:0 metrics:0 views:lblDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(55)-[lbl(%f)]",(SCREEN_WIDTH-start_X)] options:0 metrics:0 views:lblDictionary]];
                [lblEvtName setFont:[UIFont systemFontOfSize:12]];
            }
            else
            {
                start_X=85;
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(33)-[lbl(30)]"] options:0 metrics:0 views:lblDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(85)-[lbl(%f)]",(SCREEN_WIDTH-start_X)] options:0 metrics:0 views:lblDictionary]];
                [lblEvtName setFont:[UIFont systemFontOfSize:17]];
            }
            
            // [lblEvtName release];
        }
        
        UILabel *lblEvtName = [[UILabel alloc]initWithFrame:CGRectMake(start_X, 3, SCREEN_WIDTH - start_X-15, 20)];//(57, 15, SCREEN_WIDTH - 50, 20)
        [lblEvtName setText: Evtdetail.title];
        lblEvtName.numberOfLines=1;
        [lblEvtName setTextColor:[UIColor blackColor]];
        [lblEvtName setBackgroundColor:[UIColor clearColor]];
        lblEvtName.translatesAutoresizingMaskIntoConstraints=NO;
        [cell.contentView addSubview:lblEvtName];
        
        NSDictionary *dictionary= @{@"label":lblEvtName};
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(3)-[label(20)]"] options:0 metrics:0 views:dictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(55)-[label(%f)]",(SCREEN_WIDTH-start_X)] options:0 metrics:0 views:dictionary]];
            [lblEvtName setFont:[UIFont systemFontOfSize:15]];
        }
        else
        {
            start_X=85;
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[label(30)]"] options:0 metrics:0 views:dictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(85)-[label(%f)]",(SCREEN_WIDTH-start_X)] options:0 metrics:0 views:dictionary]];
            [lblEvtName setFont:[UIFont systemFontOfSize:20]];
        }
        
        //  [lblEvtName release];
        
        
    }
    
    return cell;
}
*/

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FundraiserCategoryDO *iFundraisersCategoryDO = [arrFundraisersCategoryInfo objectAtIndex:indexPath.section];
    if(isNextPageAvailable && indexPath.row == [iFundraisersCategoryDO.catObjArray count] ) {
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"] && rowCountOfTotalCells !=0) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self Request_Fundraiserslist];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [FundraiserTableview reloadData];
                });
            });
            
        }
    }
    
}



// Called after the user changes the selection.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FundraiserCategoryDO *iFundraisersCategoryDO = [arrFundraisersCategoryInfo objectAtIndex:indexPath.section];
//    if(isNextPageAvailable && indexPath.row == [iFundraisersCategoryDO.catObjArray count] )
//    {
//        [self Request_Fundraiserslist];
//        return;
//    }
    [defaults setBool:NO forKey:@"ViewMore"];

    
    FundraiserDeatilsDO *Fundraiserdetail = [iFundraisersCategoryDO.catObjArray  objectAtIndex:indexPath.row];
    
    if(Fundraiserdetail.fundraiserId)
        [defaults setValue:Fundraiserdetail.fundraiserId forKey:FUNDRAISERID];
    if(Fundraiserdetail.fundraiserListId)
        [defaults setValue:Fundraiserdetail.fundraiserListId forKey:FUNDRAISERLISTID];
    
    FundraiserDetailViewController *iFundraiserDetailViewController = [[FundraiserDetailViewController alloc]initWithNibName:@"FundraiserDetailViewController" bundle:[NSBundle mainBundle]];
    
    [self.navigationController pushViewController:iFundraiserDetailViewController animated:NO];
    
   // [iFundraiserDetailViewController release];
    
    [FundraiserTableview deselectRowAtIndexPath:[FundraiserTableview indexPathForSelectedRow] animated:NO];
}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)requestToGetUserData
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
//    
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
//    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//    ReleaseAndNilify(xmlStr);
//}

-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}
-(void)parseGetUserData:(id)response
{
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}

-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }
}


@end

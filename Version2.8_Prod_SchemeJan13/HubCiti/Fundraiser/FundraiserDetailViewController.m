//
//  FundraiserDetailViewController.m
//  HubCiti
//
//  Created by Kitty on 24/11/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import "FundraiserDetailViewController.h"
#import "FundraiserDeatilsDO.h"
#import "MainMenuViewController.h"
#import "WebBrowserViewController.h"
#import "EventsListViewController.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "FundraiserListViewController.h"
#import "EventsMapViewController.h"
#import "RetailerSummaryViewController.h"
#import "SpecialOffersViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "CouponsInDealViewController.h"

@interface FundraiserDetailViewController ()

@end

@implementation FundraiserDetailViewController
@synthesize anyVC,emailSendingVC;
static int objIndex = 0;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"Fundraiser";
   
   
    
    
    
    //customize back button
   
    self.navigationItem.hidesBackButton = YES;
   // home icon
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    
    [self requestFundraiserDetails];
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
}
-(void)addShareButton{
    // Create down menu button
    UIImageView *homeLabel ;
    
    
    // Create up menu button
    homeLabel = [self createHomeButtonView];
    
    DWBubbleMenuButton *upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
                                                                                          self.view.frame.size.height - homeLabel.frame.size.height - 20.f,
                                                                                          homeLabel.frame.size.width,
                                                                                          homeLabel.frame.size.height)
                                                            expansionDirection:DirectionUp];
    upMenuView.homeButtonView = homeLabel;
    
    upMenuView.delegate = self;
    
    [upMenuView addButtons:[self createDemoButtonArray]];
    
    [self.view addSubview:upMenuView];
    
    [self.view bringSubviewToFront:upMenuView];
    
    upMenuView.onLogOutPressed =^(){
        
        [self logOutPressed];
    } ;
    
}


- (UIImageView *)createHomeButtonView {
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
    label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}

- (NSArray *)createDemoButtonArray {
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    
    int i = 0;
    for (NSString *title in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:title] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        
        button.clipsToBounds = YES;
        button.tag = i++;
        
        [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
}

- (void)test:(UIButton *)sender {
    
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
    
     [self shareClicked];
    switch (sender.tag) {
        case 0:{//facebook
           
            anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
            anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"FUNDRAISER_SHARE"];
            [UIView transitionWithView:self.view duration:0.5
                               options:UIViewAnimationOptionTransitionCurlDown
                            animations:^ { [self.view addSubview:anyVC.view]; }
                            completion:nil];
        }
            break;
        case 1:{//twitter
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    if (result == SLComposeViewControllerResultCancelled) {
                        DLog(@"Twitter Result: canceled");
                    } else {
                        DLog(@"Twitter Result: sent");
                        NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><fundId>%@</fundId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:FUNDRAISERID]];
                        
                        [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
                    }
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        
                        [controller.view removeFromSuperview];
                    } else
                        [controller dismissViewControllerAnimated:YES completion:Nil];
                };
                controller.completionHandler =myBlock;
                
                //Adding the Text to the facebook post value from iOS
                [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                
                //Adding the URL to the facebook post value from iOS
                
                //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                
                //Adding the Image to the facebook post value from iOS
                
                [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    [self.view addSubview:controller.view];
                    [self.view bringSubviewToFront:controller.view];
                }else
                    [self presentViewController:controller animated:YES completion:Nil];
                
                
            }
            else {
                [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
            }
        }
            
            
            break;
        case 2:{//text
            
            if ([self hasCellularCoverage])
            {
                if ([MFMessageComposeViewController canSendText]) {
                    
                    MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
                    [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    smsComposerController.messageComposeDelegate = self;
                    [self presentViewController:smsComposerController animated:YES completion:nil];
                }
            }
            else
            {
                [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
            }
            
            
        }
            break;
        case 3:{//email
            //[defaults setValue:FUNDRAISER_SHARE forKey:KEY_SHARE_TYPE];
            __typeof(self) __weak  obj = self;
            emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
            emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"FUNDRAISER_SHARE"];
            emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                
                [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
            } ;
            
            [emailSendingVC loadMail];
        }
            break;
            
        default:
            [defaults setBool:NO forKey:BottomButton];
            break;
    }
    
}

- (UIButton *)createButtonWithName:(NSString *)imageName {
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button sizeToFit];
    
    [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}



- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView{
    
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark button action methods

-(void)popBackToPreviousPage
{
    if (maxFundraiserCountFlag==TRUE ) {
        maxFundraiserCountFlag=FALSE;
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        for (UIViewController *aViewController in allViewControllers) {
            if ([aViewController isKindOfClass:[CouponsInDealViewController class]]) {
                [self.navigationController popToViewController:aViewController animated:NO];
            }
        }
    }
    else
    {
        [self.navigationController popViewControllerAnimated:NO];
    }
}



-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

#pragma mark view set methods

-(void) setiPadView
{
    FundraiserDeatilsDO *fundDetails;
    fundDetails = [arrFundraisingDetails objectAtIndex:0];
    
    fundraiserView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, -20, SCREEN_WIDTH, SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT)];
    [fundraiserView setBackgroundColor:[UIColor clearColor]];
    
    int xPos=10;
    int yPos=-10;
    int gap=5;
    int height = 0;
    int width = SCREEN_WIDTH-20;
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        gap=10;
    }
    
    //Set the tile lable
    if (fundDetails.title) {
        fundraiserTitle=[[UILabel alloc]init];
        fundraiserTitle.textAlignment = NSTextAlignmentCenter;
        fundraiserTitle.textColor = [UIColor whiteColor];
        fundraiserTitle.backgroundColor= [UIColor lightGrayColor];
        fundraiserTitle.numberOfLines=2;
        fundraiserTitle.lineBreakMode = NSLineBreakByWordWrapping;
        fundraiserTitle.text=fundDetails.title;
        fundraiserTitle.translatesAutoresizingMaskIntoConstraints=NO;
        
    }
    
    if (fundDetails.startDate) {
        fundraiserStartDate=[[UILabel alloc]init];
        NSString *sdate = @"Start Date:";
        NSString *fundStartDate = [sdate stringByAppendingString:fundDetails.startDate];
        fundraiserStartDate.font =[UIFont fontWithName:@"Helvetica" size:13];
        fundraiserStartDate.numberOfLines=1;
        fundraiserStartDate.textColor = [UIColor whiteColor];
        fundraiserStartDate.lineBreakMode= NSLineBreakByWordWrapping;
        fundraiserStartDate.text=fundStartDate;
        fundraiserStartDate.translatesAutoresizingMaskIntoConstraints=NO;
        
    }
    
    if (![fundDetails.endDate isEqualToString:@"N/A"]) {
        NSString *edate = @"  End Date:";
        NSString *fundEndDate = [edate stringByAppendingString:fundDetails.endDate];
        fundraiserEndDate =[[UILabel alloc]init];
        fundraiserEndDate.font =[UIFont fontWithName:@"Helvetica" size:13];
        fundraiserEndDate.textAlignment= NSTextAlignmentLeft;
        fundraiserEndDate.numberOfLines=1;
        fundraiserEndDate.textColor = [UIColor whiteColor];
        fundraiserEndDate.lineBreakMode= NSLineBreakByWordWrapping;
        fundraiserEndDate.text=fundEndDate;
        fundraiserEndDate.translatesAutoresizingMaskIntoConstraints=NO;
        [fundraiserTitle addSubview:fundraiserEndDate];
    }
    
    [fundraiserTitle addSubview:fundraiserStartDate];
    [fundraiserView addSubview:fundraiserTitle];
    
    height = 0.1*SCREEN_HEIGHT;
    NSDictionary *dictonaryOne;
    
    if (![fundDetails.endDate isEqualToString:@"N/A"]){
        dictonaryOne = @{@"title":fundraiserTitle,@"start":fundraiserStartDate,@"end":fundraiserEndDate};
    }
    else
    {
        dictonaryOne = @{@"title":fundraiserTitle,@"start":fundraiserStartDate};
    }
    
    [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[title(%d)]",height] options:0 metrics:nil views:dictonaryOne]];
    [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[title(%f)]",SCREEN_WIDTH] options:0 metrics:nil views:dictonaryOne]];
    fundraiserTitle.font = [UIFont boldSystemFontOfSize:19.0];
    if ([fundDetails.endDate isEqualToString:@"N/A"])
    {
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[start(%d)]",(height/2),(height/2)] options:0 metrics:nil views:dictonaryOne]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[start(%f)]",SCREEN_WIDTH] options:0 metrics:nil views:dictonaryOne]];
        fundraiserStartDate.textAlignment= NSTextAlignmentCenter;
    }
    else
    {
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[start(%d)]",(height/2),(height/2)] options:0 metrics:nil views:dictonaryOne]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[start(%f)]",SCREEN_WIDTH/2] options:0 metrics:nil views:dictonaryOne]];
        fundraiserStartDate.textAlignment= NSTextAlignmentRight;
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[end(%d)]",(height/2),(height/2)] options:0 metrics:nil views:dictonaryOne]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[end(%f)]",SCREEN_WIDTH/2,SCREEN_WIDTH/2] options:0 metrics:nil views:dictonaryOne]];
        fundraiserEndDate.textAlignment= NSTextAlignmentLeft;
    }
    fundraiserStartDate.font =[UIFont fontWithName:@"Helvetica" size:18];
    fundraiserEndDate.font =[UIFont fontWithName:@"Helvetica" size:18];
    
    
    
    
    //yPos = yPos+fundraiserTitle.frame.size.height + gap;
    yPos = height+ gap;
    
    if (fundDetails.shortDescription) {
        
        fundraiserShortDescription = [[UILabel alloc]init ];
        fundraiserShortDescription.textAlignment= NSTextAlignmentCenter;
        fundraiserShortDescription.numberOfLines=0;
        fundraiserShortDescription.lineBreakMode= NSLineBreakByWordWrapping;
        fundraiserShortDescription.textColor = [UIColor blackColor];
        fundraiserShortDescription.backgroundColor =[UIColor clearColor];
        fundraiserShortDescription.text=fundDetails.shortDescription;
        
        
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            height=[self calculateLabelHeight:fundDetails.shortDescription labelWidth:width fontSize:13];
        }
        else
        {
            height=[self calculateLabelHeight:fundDetails.shortDescription labelWidth:width fontSize:18];
        }
        
        
        [fundraiserView addSubview:fundraiserShortDescription];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            
        }
        else
        {
            fundraiserShortDescription.translatesAutoresizingMaskIntoConstraints=NO;
            
            NSDictionary *shortDesc = @{@"short":fundraiserShortDescription};
            
            [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[short(%d)]",yPos,height] options:0 metrics:nil views:shortDesc]];
            [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%d)-[short(%d)]",xPos,width] options:0 metrics:nil views:shortDesc]];
            fundraiserShortDescription.font =[UIFont fontWithName:@"Helvetica" size:18];
            
            
        }
        
        yPos = yPos + height+gap;
    }
    
    if (fundDetails.imagePath) {
        if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
        {
            height=0.35*SCREEN_HEIGHT;
        }
        else
        {
            height=0.25*SCREEN_HEIGHT;
        }
        
        
        
        SdImageView *asyncImageView = [[SdImageView alloc] init];
        asyncImageView.backgroundColor = [UIColor clearColor];
        // asyncImageView.layer.cornerRadius = 5.0f;
        [asyncImageView loadImage:fundDetails.imagePath];
        
        asyncImageView.translatesAutoresizingMaskIntoConstraints=NO;
        [fundraiserView addSubview:asyncImageView];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            
        }
        else
        {
            NSDictionary *imageDic=@{@"image":asyncImageView};
            
            [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[image(%d)]",yPos,height] options:0 metrics:nil views:imageDic]];
            [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%d)-[image(%d)]",width/8,(6*width)/8] options:0 metrics:nil views:imageDic]];
            yPos=yPos+height +gap;
            
        }
        
        //[asyncImageView release];
    }
    
    
    
    if (fundDetails.longDescription) {
        
        height= 0.2*SCREEN_HEIGHT;
        
        fundraiserLongDescription = [[UIWebView alloc]init];
        fundraiserLongDescription.delegate=self;
        fundraiserLongDescription.backgroundColor = [UIColor clearColor];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            [fundraiserLongDescription loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:11px;font-family:Helvetica;color:#000;'>%@",fundDetails.longDescription] baseURL:nil];
        }
        else
        {
            [fundraiserLongDescription loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:17px;font-family:Helvetica;color:#000;'>%@",fundDetails.longDescription] baseURL:nil];
        }
        fundraiserLongDescription.translatesAutoresizingMaskIntoConstraints=NO;
        [fundraiserView addSubview:fundraiserLongDescription];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            
        }
        else
        {
            NSDictionary *longDictionary=@{@"long":fundraiserLongDescription};
            
            [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[long(%d)]",yPos,height] options:0 metrics:nil views:longDictionary]];
            [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%d)-[long(%d)]",xPos,width] options:0 metrics:nil views:longDictionary]];
            yPos = yPos + height +gap;
        }
        
        
        
    }
    
    
    if (fundDetails.purchasedProducts) {
        
        height=0.04*SCREEN_HEIGHT;
        
        NSString *products = @"Purchase Products: ";
        
        //fundraiserPurchaseProducts =[[UILabel alloc]initWithFrame:CGRectMake(xPos, yPos, width/2.5, 20)];
        fundraiserPurchaseProducts =[[UILabel alloc]init];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            fundraiserPurchaseProducts.font = [UIFont boldSystemFontOfSize:11];
            fundraiserPurchaseProducts.textAlignment = NSTextAlignmentLeft;
        }
        else
        {
            fundraiserPurchaseProducts.font = [UIFont boldSystemFontOfSize:16];
            fundraiserPurchaseProducts.textAlignment = NSTextAlignmentLeft;
        }
        
        //fundraiserPurchaseProducts.textAlignment = NSTextAlignmentLeft;
        fundraiserPurchaseProducts.lineBreakMode = NSLineBreakByWordWrapping;
        fundraiserPurchaseProducts.textColor = [UIColor blackColor];
        fundraiserPurchaseProducts.backgroundColor = [UIColor clearColor];
        fundraiserPurchaseProducts.translatesAutoresizingMaskIntoConstraints=NO;
        fundraiserPurchaseProducts.text = products;
        
        
        // fundraiserPurchaseProductslink =[[UIButton alloc]initWithFrame:CGRectMake(width/2.5, yPos, width-(width/2.5), 20)];
        fundraiserPurchaseProductslink =[[UIButton alloc]init];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            fundraiserPurchaseProductslink.titleLabel.font = [UIFont boldSystemFontOfSize:11];
            fundraiserPurchaseProductslink.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        }
        else
        {
            fundraiserPurchaseProductslink.titleLabel.font = [UIFont boldSystemFontOfSize:16];
            fundraiserPurchaseProductslink.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        }
        
        //fundraiserPurchaseProductslink.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        [fundraiserPurchaseProductslink setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [fundraiserPurchaseProductslink setTitle:fundDetails.purchasedProducts forState:UIControlStateNormal];
        
        [fundraiserPurchaseProductslink addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        fundraiserPurchaseProductslink.tag =3;
        fundraiserPurchaseProductslink.translatesAutoresizingMaskIntoConstraints=NO;
        
        [fundraiserView addSubview:fundraiserPurchaseProductslink];
        
        [fundraiserView addSubview:fundraiserPurchaseProducts];
        
        NSDictionary *productDictionary=@{@"products":fundraiserPurchaseProducts,@"link":fundraiserPurchaseProductslink};
        
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[products(%d)]",yPos,height] options:0 metrics:nil views:productDictionary]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%d)-[products(%f)]",xPos,(width/3.5)] options:0 metrics:nil views:productDictionary]];
        
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[link(%d)]",yPos,height] options:0 metrics:nil views:productDictionary]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[link(%f)]",(width/3.5),(width-(width/3.5))] options:0 metrics:nil views:productDictionary]];
        
        yPos = yPos + height +gap;
        
        
        
        
        // yPos = yPos +  fundraiserPurchaseProducts.frame.size.height+gap;
    }
    //
    if (fundDetails.goal) {
        height=0.04*SCREEN_HEIGHT;
        
        NSString *goal = @"Fundraising Goal: ";
        NSString *fundGoal = [goal stringByAppendingString:fundDetails.goal];
        fundraiserGoal =[[UILabel alloc]initWithFrame:CGRectMake(xPos, yPos, width, 20)];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            fundraiserGoal.font = [UIFont boldSystemFontOfSize:11];
            fundraiserGoal.textAlignment = NSTextAlignmentLeft;
        }
        else
        {
            fundraiserGoal.font = [UIFont boldSystemFontOfSize:16];
            fundraiserGoal.textAlignment = NSTextAlignmentLeft;
        }
        
        
        fundraiserGoal.lineBreakMode = NSLineBreakByWordWrapping;
        fundraiserGoal.textColor = [UIColor blackColor];
        fundraiserGoal.backgroundColor = [UIColor clearColor];
        fundraiserGoal.text = fundGoal;
        fundraiserGoal.translatesAutoresizingMaskIntoConstraints=NO;
        [fundraiserView addSubview:fundraiserGoal];
        
        NSDictionary *goalDictionary = @{@"goal":fundraiserGoal};
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[goal(%d)]",yPos,height] options:0 metrics:nil views:goalDictionary]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%d)-[goal(%d)]",xPos,width] options:0 metrics:nil views:goalDictionary]];
        
        yPos = yPos +  height+gap;
    }
    
    if (fundDetails.currentValue) {
        
        height=SCREEN_HEIGHT*0.04;
        
        NSString *level = @"Current Level: ";
        NSString *fundlevel = [level stringByAppendingString:fundDetails.currentValue];
        fundraiserCurrentLevel =[[UILabel alloc]init];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            fundraiserCurrentLevel.font = [UIFont boldSystemFontOfSize:11];
            fundraiserCurrentLevel.textAlignment = NSTextAlignmentLeft;
        }
        else
        {
            fundraiserCurrentLevel.font = [UIFont boldSystemFontOfSize:16];
            fundraiserCurrentLevel.textAlignment = NSTextAlignmentLeft;
        }
        fundraiserCurrentLevel.lineBreakMode = NSLineBreakByWordWrapping;
        fundraiserCurrentLevel.textColor = [UIColor blackColor];
        fundraiserCurrentLevel.backgroundColor = [UIColor clearColor];
        fundraiserCurrentLevel.text = fundlevel;
        fundraiserCurrentLevel.translatesAutoresizingMaskIntoConstraints=NO;
        [fundraiserView addSubview:fundraiserCurrentLevel];
        
        NSDictionary *valueDictionary = @{@"level":fundraiserCurrentLevel};
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[level(%d)]",yPos,height] options:0 metrics:nil views:valueDictionary]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%d)-[level(%d)]",xPos,width] options:0 metrics:nil views:valueDictionary]];
        
        
        
        
        yPos = yPos + height+gap;
    }
    
    if(fundDetails.moreInfoURL)
    {
        
        moreInformation=[[UIButton alloc]init];
        [defaults setValue:fundDetails.moreInfoURL forKey:KEY_URL];
        moreInformation.clipsToBounds = YES;
        
        moreInformation.titleLabel.font =[UIFont boldSystemFontOfSize:28];
        
        [moreInformation setTitleColor:[UIColor colorWithRGBInt:0x112e72] forState:UIControlStateNormal];
        
        [moreInformation setTitle:@"More Information" forState:UIControlStateNormal];
        
        [moreInformation addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        moreInformation.tag =1;
        
        moreInformation.layer.borderWidth = 2.0;
        moreInformation.layer.cornerRadius=10;
        moreInformation.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
        moreInformation.translatesAutoresizingMaskIntoConstraints=NO;
        
        UIImage *btnImage = [UIImage imageNamed:@"info.png"];
        UIImageView *buttonImage =[[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 60, 60)];
        [buttonImage setImage:btnImage];
        
        
        [moreInformation addSubview:buttonImage];
        [fundraiserView addSubview:moreInformation];
        
        NSDictionary *moreDictionary = @{@"more":moreInformation,@"image":btnImage};
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[more(70)]",yPos] options:0 metrics:nil views:moreDictionary]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[more(%f)]",SCREEN_WIDTH/4,SCREEN_WIDTH/2] options:0 metrics:nil views:moreDictionary]];
        yPos=yPos+70+gap;
        
        UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake((moreInformation.frame.size.width-15),35, 10,10)];
        [arrow setImage:[UIImage imageNamed:@"right_arrow"]];
        [moreInformation addSubview:arrow];
        [fundraiserView addSubview:moreInformation];
    }
    
    if(fundDetails.isEventFlag && [fundDetails.isEventFlag intValue]==1)
    {
        
        eventInformation =[[UIButton alloc]init];
        eventInformation.clipsToBounds = YES;
        eventInformation.titleLabel.font =[UIFont boldSystemFontOfSize:28];
        [eventInformation setTitleColor:[UIColor colorWithRGBInt:0x112e72] forState:UIControlStateNormal];
        [eventInformation setTitle:@"Event Information" forState:UIControlStateNormal];
        [eventInformation addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchDown];
        eventInformation.tag =2;
        eventInformation.layer.borderWidth = 2.0;
        eventInformation.layer.cornerRadius=10;
        eventInformation.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
        eventInformation.translatesAutoresizingMaskIntoConstraints=NO;
        
        
        UIImage *btnImage = [UIImage imageNamed:@"icon_events"];
        UIImageView *buttonImage =[[UIImageView alloc]initWithFrame:CGRectMake(10, 5 , 60, 60)];
        [buttonImage setImage:btnImage];
        
        [eventInformation addSubview:buttonImage];
        [fundraiserView addSubview:eventInformation];
        
        NSDictionary *eventDictionary=@{@"event":eventInformation};
        
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[event(70)]",yPos] options:0 metrics:nil views:eventDictionary]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[event(%f)]",SCREEN_WIDTH/4,SCREEN_WIDTH/2] options:0 metrics:nil views:eventDictionary]];
        yPos=yPos+70+gap;
    }
    {
    locationInformation =[[UIButton alloc]init];
    
    locationInformation.clipsToBounds = YES;
    
    locationInformation.titleLabel.font =[UIFont boldSystemFontOfSize:28];
    
    [locationInformation setTitleColor:[UIColor colorWithRGBInt:0x112e72] forState:UIControlStateNormal];
    
    [locationInformation setTitle:@"Location" forState:UIControlStateNormal];
    
    [locationInformation addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    locationInformation.tag = 4;
    
    locationInformation.layer.borderWidth = 2.0;
    locationInformation.layer.cornerRadius=10;
    
    locationInformation.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
    locationInformation.translatesAutoresizingMaskIntoConstraints=NO;
    
    UIImageView *icon1 = [[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 60, 60)];
    [icon1 setImage:[UIImage imageNamed:@"location.png"]];
    
    [locationInformation addSubview:icon1];
    
    UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake(width, locationInformation.frame.size.height/2-2, 7, 7)];
    [arrow setImage:[UIImage imageNamed:@"right_arrow"]];
    
    //yPos=yPos+70+gap;
    [locationInformation addSubview:arrow];
    
    [fundraiserView addSubview:locationInformation];
    
    NSDictionary *locationDictionary=@{@"Location":locationInformation};
    
    [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[Location(70)]",yPos] options:0 metrics:nil views:locationDictionary]];
    [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[Location(%f)]",SCREEN_WIDTH/4,SCREEN_WIDTH/2] options:0 metrics:nil views:locationDictionary]];
        yPos=yPos+70+gap;
    }
    int addFactor;
    if (yPos>(SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT)) {
        addFactor=yPos+100-(SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT);
    }
    else
    {
        addFactor=0;
    }
    
    
    fundraiserView.contentSize = CGSizeMake(SCREEN_WIDTH, yPos+addFactor+70);
    fundraiserView.scrollEnabled=YES;
    [self.view addSubview:fundraiserView];
    
    [self addShareButton];
    
}

-(void) setFundraiserView
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        [self setiPadView];
    }
    else
    {
        FundraiserDeatilsDO *fundDetails;
        fundDetails = [arrFundraisingDetails objectAtIndex:0];
        
        fundraiserView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, -20, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [fundraiserView setBackgroundColor:[UIColor clearColor]];
        
        int xPos=10;
        int yPos=-10;
        int gap=5;
        int height = 0;
        int width = SCREEN_WIDTH-20;
        
        //Set the tile lable
        if (fundDetails.title) {
            fundraiserTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 80)];
            fundraiserTitle.font = [UIFont boldSystemFontOfSize:14.0];
            fundraiserTitle.textAlignment = NSTextAlignmentCenter;
            fundraiserTitle.textColor = [UIColor whiteColor];
            fundraiserTitle.backgroundColor= [UIColor lightGrayColor];
            fundraiserTitle.numberOfLines=2;
            fundraiserTitle.lineBreakMode = NSLineBreakByWordWrapping;
            fundraiserTitle.text=fundDetails.title;
            
        }
        
        if (fundDetails.startDate) {
            
            if ([fundDetails.endDate isEqualToString:@"N/A"]) {
                fundraiserStartDate=[[UILabel alloc]initWithFrame:CGRectMake(0, 45, SCREEN_WIDTH, 40)];
                fundraiserStartDate.textAlignment= NSTextAlignmentCenter;
            }
            else{
                fundraiserStartDate=[[UILabel alloc]initWithFrame:CGRectMake(0, 45, SCREEN_WIDTH/2, 40)];
                fundraiserStartDate.textAlignment= NSTextAlignmentRight;
            }
            NSString *sdate = @"Start Date:";
            NSString *fundStartDate = [sdate stringByAppendingString:fundDetails.startDate];
            
            fundraiserStartDate.font =[UIFont fontWithName:@"Helvetica" size:13];
            fundraiserStartDate.numberOfLines=1;
            fundraiserStartDate.textColor = [UIColor whiteColor];
            fundraiserStartDate.lineBreakMode= NSLineBreakByWordWrapping;
            fundraiserStartDate.text=fundStartDate;
            
        }
        
        if (![fundDetails.endDate isEqualToString:@"N/A"]) {
            NSString *edate = @"  End Date:";
            NSString *fundEndDate = [edate stringByAppendingString:fundDetails.endDate];
            fundraiserEndDate =[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2, 45, SCREEN_WIDTH/2, 40)];
            fundraiserEndDate.font =[UIFont fontWithName:@"Helvetica" size:13];
            fundraiserEndDate.textAlignment= NSTextAlignmentLeft;
            fundraiserEndDate.numberOfLines=1;
            fundraiserEndDate.textColor = [UIColor whiteColor];
            fundraiserEndDate.lineBreakMode= NSLineBreakByWordWrapping;
            fundraiserEndDate.text=fundEndDate;
            [fundraiserTitle addSubview:fundraiserEndDate];
        }
        
        [fundraiserTitle addSubview:fundraiserStartDate];
        [fundraiserView addSubview:fundraiserTitle];
        yPos = yPos+fundraiserTitle.frame.size.height + gap;
        
        if (fundDetails.shortDescription) {
            
            fundraiserShortDescription = [[UILabel alloc]init ];//WithFrame:CGRectMake(xPos,yPos,width, 40)];
            fundraiserShortDescription.font =[UIFont fontWithName:@"Helvetica" size:13];
            fundraiserShortDescription.textAlignment= NSTextAlignmentCenter;
            fundraiserShortDescription.numberOfLines=0;
            fundraiserShortDescription.lineBreakMode= NSLineBreakByWordWrapping;
            fundraiserShortDescription.textColor = [UIColor blackColor];
            fundraiserShortDescription.backgroundColor =[UIColor clearColor];
            fundraiserShortDescription.text=fundDetails.shortDescription;
            height=[self calculateLabelHeight:fundDetails.shortDescription labelWidth:width fontSize:13];
            fundraiserShortDescription.frame = CGRectMake(xPos, yPos, width, height);
            [fundraiserView addSubview:fundraiserShortDescription];
            yPos = yPos + height+gap;
        }
        
        if (fundDetails.imagePath) {
            
            SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(xPos, yPos, width, 150)];
            asyncImageView.backgroundColor = [UIColor clearColor];
            // asyncImageView.layer.cornerRadius = 5.0f;
            [asyncImageView loadImage:fundDetails.imagePath];
            
            
            [fundraiserView addSubview:asyncImageView];
            yPos=yPos+asyncImageView.frame.size.height +gap;
            //[asyncImageView release];
        }
        
        
        
        if (fundDetails.longDescription) {
            fundraiserLongDescription = [[UIWebView alloc]initWithFrame: CGRectMake(xPos, yPos, width, 80)];
            fundraiserLongDescription.delegate=self;
            fundraiserLongDescription.backgroundColor = [UIColor clearColor];
            [fundraiserLongDescription loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:11px;font-family:Helvetica;color:#000;'>%@",fundDetails.longDescription] baseURL:nil];
            [fundraiserView addSubview:fundraiserLongDescription];
            yPos = yPos + fundraiserLongDescription.frame.size.height+gap;
            
        }
        
        
        if (fundDetails.purchasedProducts) {
            NSString *products = @"Purchase Products: ";
            
            fundraiserPurchaseProducts =[[UILabel alloc]initWithFrame:CGRectMake(xPos, yPos, width/2.5, 20)];
            fundraiserPurchaseProducts.font = [UIFont boldSystemFontOfSize:11];
            fundraiserPurchaseProducts.textAlignment = NSTextAlignmentLeft;
            fundraiserPurchaseProducts.lineBreakMode = NSLineBreakByWordWrapping;
            fundraiserPurchaseProducts.textColor = [UIColor blackColor];
            fundraiserPurchaseProducts.backgroundColor = [UIColor clearColor];
            fundraiserPurchaseProducts.text = products;
            
            
            fundraiserPurchaseProductslink =[[UIButton alloc]initWithFrame:CGRectMake(width/2.5, yPos, width-(width/2.5), 20)];
            fundraiserPurchaseProductslink.titleLabel.font = [UIFont boldSystemFontOfSize:11];
            fundraiserPurchaseProductslink.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            
            [fundraiserPurchaseProductslink setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [fundraiserPurchaseProductslink setTitle:fundDetails.purchasedProducts forState:UIControlStateNormal];
            
            [fundraiserPurchaseProductslink addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            fundraiserPurchaseProductslink.tag =3;
            
            [fundraiserView addSubview:fundraiserPurchaseProductslink];
            [fundraiserView addSubview:fundraiserPurchaseProducts];
            yPos = yPos +  fundraiserPurchaseProducts.frame.size.height+gap;
        }
        
        if (fundDetails.goal) {
            NSString *goal = @"Fundraising Goal: ";
            NSString *fundGoal = [goal stringByAppendingString:fundDetails.goal];
            fundraiserGoal =[[UILabel alloc]initWithFrame:CGRectMake(xPos, yPos, width, 20)];
            fundraiserGoal.font = [UIFont boldSystemFontOfSize:11];
            fundraiserGoal.textAlignment = NSTextAlignmentLeft;
            fundraiserGoal.lineBreakMode = NSLineBreakByWordWrapping;
            fundraiserGoal.textColor = [UIColor blackColor];
            fundraiserGoal.backgroundColor = [UIColor clearColor];
            fundraiserGoal.text = fundGoal;
            
            [fundraiserView addSubview:fundraiserGoal];
            yPos = yPos +  fundraiserGoal.frame.size.height+gap;
        }
        
        if (fundDetails.currentValue) {
            
            NSString *level = @"Current Level: ";
            NSString *fundlevel = [level stringByAppendingString:fundDetails.currentValue];
            fundraiserCurrentLevel =[[UILabel alloc]initWithFrame:CGRectMake(xPos, yPos, width, 20)];
            fundraiserCurrentLevel.font = [UIFont boldSystemFontOfSize:11];
            fundraiserCurrentLevel.textAlignment = NSTextAlignmentLeft;
            fundraiserCurrentLevel.lineBreakMode = NSLineBreakByWordWrapping;
            fundraiserCurrentLevel.textColor = [UIColor blackColor];
            fundraiserCurrentLevel.backgroundColor = [UIColor clearColor];
            fundraiserCurrentLevel.text = fundlevel;
            [fundraiserView addSubview:fundraiserCurrentLevel];
            yPos = yPos + fundraiserCurrentLevel.frame.size.height+gap;
        }
        
        
        if(fundDetails.moreInfoURL)
        {
            //        moreInformation.frame = CGRectMake(startX, yPos+5, width, 40);
            
            moreInformation =[[UIButton alloc]initWithFrame:CGRectMake(10, yPos, SCREEN_WIDTH-20, 50)];
            [defaults setValue:fundDetails.moreInfoURL forKey:KEY_URL];
            moreInformation.clipsToBounds = YES;
            
            moreInformation.titleLabel.font =[UIFont boldSystemFontOfSize:17];
            
            [moreInformation setTitleColor:[UIColor colorWithRGBInt:0x112e72] forState:UIControlStateNormal];
            
            [moreInformation setTitle:@"More Information" forState:UIControlStateNormal];
            
            [moreInformation addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            moreInformation.tag =1;
            
            moreInformation.layer.borderWidth = 1.0;
            moreInformation.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
            
            moreInformation.layer.cornerRadius=10;
            
            moreInformation.translatesAutoresizingMaskIntoConstraints=NO;
            
            UIImageView *icon = [[UIImageView alloc]initWithFrame:CGRectMake(15, 5, 40, 40)];
            [icon setImage:[UIImage imageNamed:@"info.png"]];
            
            [moreInformation addSubview:icon];
            
            UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake(width, moreInformation.frame.size.height/2-2, 7, 7)];
            [arrow setImage:[UIImage imageNamed:@"right_arrow"]];
            
            yPos = yPos + moreInformation.frame.size.height + 3;
            
            // [moreInformation addSubview:icon];
            [moreInformation addSubview:arrow];
            [fundraiserView addSubview:moreInformation];
        }
        
        if(fundDetails.isEventFlag && [fundDetails.isEventFlag intValue]==1)
        {
            //        eventInformation.frame = CGRectMake(startX, yPos+5, width, 40);
            
            eventInformation =[[UIButton alloc]initWithFrame:CGRectMake(10, yPos, SCREEN_WIDTH - 20, 50)];
            
            eventInformation.clipsToBounds = YES;
            
            eventInformation.titleLabel.font =[UIFont boldSystemFontOfSize:17];
            
            [eventInformation setTitleColor:[UIColor colorWithRGBInt:0x112e72] forState:UIControlStateNormal];
            
            [eventInformation setTitle:@"Event Information" forState:UIControlStateNormal];
            
            [eventInformation addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            eventInformation.tag =2;
            
            eventInformation.layer.borderWidth = 1.0;
            
            eventInformation.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
            
            eventInformation.layer.cornerRadius=10;
            
            eventInformation.translatesAutoresizingMaskIntoConstraints=NO;
            
            UIImageView *icon1 = [[UIImageView alloc]initWithFrame:CGRectMake(15, 5, 40, 40)];
            [icon1 setImage:[UIImage imageNamed:@"icon_events.png"]];
            
            [eventInformation addSubview:icon1];
            
            UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake(width, eventInformation.frame.size.height/2-2, 7, 7)];
            [arrow setImage:[UIImage imageNamed:@"right_arrow"]];
            
            yPos = yPos + eventInformation.frame.size.height + 3;
            
            
            [eventInformation addSubview:arrow];
            
            [fundraiserView addSubview:eventInformation];
        }
        
        {
            //        eventInformation.frame = CGRectMake(startX, yPos+5, width, 40);
            
            locationInformation =[[UIButton alloc]initWithFrame:CGRectMake(10, yPos, SCREEN_WIDTH - 20, 50)];
            
            locationInformation.clipsToBounds = YES;
            
            locationInformation.titleLabel.font =[UIFont boldSystemFontOfSize:17];
            
            [locationInformation setTitleColor:[UIColor colorWithRGBInt:0x112e72] forState:UIControlStateNormal];
            
            [locationInformation setTitle:@"Location" forState:UIControlStateNormal];
            
            [locationInformation addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            locationInformation.tag = 4;
            
            locationInformation.layer.borderWidth = 1.0;
            
            locationInformation.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
            
            locationInformation.layer.cornerRadius=10;
            
            locationInformation.translatesAutoresizingMaskIntoConstraints=NO;
            
            UIImageView *icon1 = [[UIImageView alloc]initWithFrame:CGRectMake(15, 5, 40, 40)];
            [icon1 setImage:[UIImage imageNamed:@"location.png"]];
            
            [locationInformation addSubview:icon1];
            
            UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake(width, locationInformation.frame.size.height/2-2, 7, 7)];
            [arrow setImage:[UIImage imageNamed:@"right_arrow"]];
            
            yPos = yPos + locationInformation.frame.size.height+3;
            
            
            [locationInformation addSubview:arrow];
            
            [fundraiserView addSubview:locationInformation];
        }
        
        fundraiserView.contentSize = CGSizeMake(SCREEN_WIDTH, yPos+ 80);
        [self.view addSubview:fundraiserView];
    }
    
    [self addShareButton];
    
}

#pragma mark request methods

-(void)requestFundraiserDetails
{
    iWebRequestState = FUNDRAISER_DETAILS;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    
    [requestStr appendFormat:@"<MenuItem><fundId>%@</fundId>",[defaults valueForKey:FUNDRAISERID]];
    
    [requestStr appendFormat:@"<platform>%@</platform>",@"ios"];
    
    [requestStr appendFormat:@"<fundListId>%@</fundListId>",[defaults valueForKey:FUNDRAISERLISTID]];
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    
    
    
    [requestStr appendFormat:@"</MenuItem>"];
    
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@alertevent/getfundrsdetail",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    //    [self setFundraiserView];
    
}


-(void)Request_fundappsiteloc
{
    
    
    /*
     
     <Fundraiser>
     <userId>14</userId>           *
     <hubCitiId>17</hubCitiId>     *
     <fundId>2</fundId>            *
     //	<postalCode>78654</postalCode>
     //	<latitude></latitude>
     //	<longitude></longitude>
     </Fundraiser>
     
     */
    iWebRequestState = FUNDRAISER_LOCATION;
    NSString *urlString = [NSString stringWithFormat:@"%@alertevent/getfundloclist",BASE_URL];
    NSMutableString *reqStr=[[NSMutableString alloc]initWithFormat:@"<Fundraiser><userId>%@</userId><fundId>%@</fundId><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_USERID],[defaults valueForKey:FUNDRAISERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"</Fundraiser>"];
    
    [ConnectionManager establishConnectionFor:reqStr base:urlString withDelegate:self];
}


-(void)responseData:(NSString *) response
{
    if (iWebRequestState==FUNDRAISER_DETAILS) {
        [self parseFundraiserDetails:response];
        
        if([arrFundraisingDetails count] > 0)
            [self setFundraiserView];
    }
    else if (iWebRequestState==FUNDRAISER_SHARE)
    {
        [self parse_shareFundraiser:response];
    }
    
    else if(iWebRequestState==FUNDRAISER_LOCATION)
    {
        [self parse_fundappsiteloc:response];
    }
    
    else if(iWebRequestState == RETSUMMARY )
    {
        [self parse_retSummary:response];
    }
    
}

#pragma mark parse methods


-(void)parse_fundappsiteloc:(NSString*)responseString
{
    
    
    
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *isAppSiteFlagElement = [TBXML childElementNamed:@"isAppSiteFlag" parentElement:tbxml.rootXMLElement];
        if(isAppSiteFlagElement){
            [defaults setValue:[TBXML textForElement:isAppSiteFlagElement] forKey:@"isAppsiteFlag"];
        }
        else{
            [defaults setValue:@"" forKey:@"isAppsiteFlag"];
        }
        
        TBXMLElement *fundraiserListElement = [TBXML childElementNamed:@"fundraiserList" parentElement:tbxml.rootXMLElement];
        if(fundraiserListElement)
        {
            
            arrRetailers = [[NSMutableArray alloc]init];
            
           
            arrRetailerAddress = [[NSMutableArray alloc]init];
            
            arrRetailerLogo = [[NSMutableArray alloc]init];
            
            arrRetailerId = [[NSMutableArray alloc]init];
            
            arrRetailerLocId = [[NSMutableArray alloc]init];
            
            arrRetailerLat = [[NSMutableArray alloc]init];
            
            arrRetailerLong = [[NSMutableArray alloc]init];
            
            arrDistance = [[NSMutableArray alloc]init];
            
            
            TBXMLElement *FundraiserElement = [TBXML childElementNamed:@"Fundraiser" parentElement:fundraiserListElement];
            while (FundraiserElement)
            {
                
                
               // TBXMLElement *appSiteIdElement = [TBXML childElementNamed:@"appSiteId" parentElement:FundraiserElement];
                
                TBXMLElement *appSiteNameElement = [TBXML childElementNamed:@"appSiteName" parentElement:FundraiserElement];
                
                TBXMLElement *retailerNameElement = [TBXML childElementNamed:@"retailerName" parentElement:FundraiserElement];
                
                TBXMLElement *retailerIdElement = [TBXML childElementNamed:@"retailId" parentElement:FundraiserElement];
                TBXMLElement *distanceElement = [TBXML childElementNamed:KEY_DISTANCE parentElement:FundraiserElement];
                TBXMLElement *retailerLocElement = [TBXML childElementNamed:@"retailLocationId" parentElement:FundraiserElement];
                TBXMLElement *addressElement = [TBXML childElementNamed:@"completeAddress" parentElement:FundraiserElement];
                TBXMLElement *logoImagePathElement = [TBXML childElementNamed:@"imagePath" parentElement:FundraiserElement];
                TBXMLElement *retailerLatElement = [TBXML childElementNamed:@"latitude" parentElement:FundraiserElement];
                TBXMLElement *retailerLongElement = [TBXML childElementNamed:@"longitude" parentElement:FundraiserElement];
                
                if(appSiteNameElement)
                    [arrRetailers addObject:[TBXML textForElement:appSiteNameElement]];
                
                else if(retailerNameElement)
                    [arrRetailers addObject:[TBXML textForElement:retailerNameElement]];
                
                if(addressElement)
                    [arrRetailerAddress addObject:[TBXML textForElement:addressElement]];
                
                if(logoImagePathElement)
                    [arrRetailerLogo addObject:[TBXML textForElement:logoImagePathElement]];
                
                if (retailerIdElement) {
                    [arrRetailerId addObject:[TBXML textForElement:retailerIdElement]];
                }
                if (retailerLocElement) {
                    [arrRetailerLocId addObject:[TBXML textForElement:retailerLocElement]];
                }
                
                if (retailerLatElement) {
                    [arrRetailerLat addObject:[TBXML textForElement:retailerLatElement]];
                }
                
                if (retailerLongElement) {
                    [arrRetailerLong addObject:[TBXML textForElement:retailerLongElement]];
                }
                
                if (distanceElement) {
                    [arrDistance addObject:[TBXML textForElement:distanceElement]];
                }
                
                
                FundraiserElement = [TBXML nextSiblingNamed:@"Fundraiser" searchFromElement:FundraiserElement];
                
            }
            
            if([arrRetailerAddress count]>0)
                [self loadRetailerAddressView];
            
        }
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
}



-(void)loadRetailerAddressView
{
    
    //    EventDetailsDO *iEventDetailsDO = [[EventDetailsDO alloc]init];
    //    iEventDetailsDO = [arrEventDetails objectAtIndex:0];
    
    tblEventHolder=[[UIView alloc]init];
    
    
    if (IS_IPHONE5)
        tblEventHolder.frame = CGRectMake(10, 10, 300, 425);
    else if (DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        tblEventHolder.frame = CGRectMake(40, 80, 680, 750);
    }
    else
        tblEventHolder.frame = CGRectMake(10, 10, 300, 350);
    
    tblEventHolder.layer.backgroundColor = [UIColor whiteColor].CGColor;
    tblEventHolder.layer.cornerRadius = 20.0;
    tblEventHolder.layer.borderColor = [UIColor blackColor].CGColor;
    tblEventHolder.layer.shadowOffset = CGSizeMake(1, 0);
    tblEventHolder.layer.shadowColor = [[UIColor blackColor] CGColor];
    tblEventHolder.layer.shadowRadius = 5;
    tblEventHolder.layer.shadowOpacity = .25;
    
    
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        cross_button=[[UIButton alloc]initWithFrame:CGRectMake(620,-3, 75, 55)];
        [cross_button setImage:[UIImage imageNamed:@"cross_icon_ipad.png"] forState:UIControlStateNormal];
    }
    else
    {
        cross_button=[[UIButton alloc]initWithFrame:CGRectMake(250,-5, 65, 45)];
        [cross_button setImage:[UIImage imageNamed:@"cross_icon.png"] forState:UIControlStateNormal];
    }
    
    [cross_button addTarget:self action:@selector(crossClicked) forControlEvents:UIControlEventTouchDown];
    
    tblEventLocations=[[UITableView alloc]init];
    if (IS_IPHONE5)
        tblEventLocations = [[UITableView alloc]initWithFrame:CGRectMake(10, 30, 280, 350) style:UITableViewStylePlain];
    else if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        tblEventLocations = [[UITableView alloc]initWithFrame:CGRectMake(40, 100, 620, 650) style:UITableViewStylePlain];
    }
    else
        tblEventLocations = [[UITableView alloc]initWithFrame:CGRectMake(10, 30, 280, 270) style:UITableViewStylePlain];
    tblEventLocations.dataSource=self;
    tblEventLocations.delegate=self;
    [tblEventLocations setBackgroundColor:[UIColor clearColor]];
    
    //    evtStrEventName =[[NSString alloc]initWithString:iEventDetailsDO.eventName];
    
    evtArrRetailers = [[NSArray alloc]initWithArray:arrRetailers];
    evtArrRetailersAddress = [[NSMutableArray alloc]initWithArray:arrRetailerAddress];
    evtArrRetailerLogo = [[NSMutableArray alloc]initWithArray:arrRetailerLogo];
    evtArrRetailerId = [[NSMutableArray alloc]initWithArray:arrRetailerId];
    evtArrRetailerLocId=[[NSMutableArray alloc]initWithArray:arrRetailerLocId];
    evtLatitude = [[NSMutableArray alloc]initWithArray:arrRetailerLat];
    evtLongitude = [[NSMutableArray alloc]initWithArray:arrRetailerLong];
    
    [tblEventLocations reloadData];
    [tblEventHolder addSubview:cross_button];
    [tblEventHolder addSubview:tblEventLocations];
    [self.view addSubview:tblEventHolder];
    
    
}


-(void)parseFundraiserDetails : (NSString *)responseString
{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        if(arrFundraisingDetails)
        {
            ReleaseAndNilify(arrFundraisingDetails);
        }
        
        arrFundraisingDetails = [[NSMutableArray alloc]init];
        
        
        FundraiserDeatilsDO *iEventDetailsDO = [[FundraiserDeatilsDO alloc]init];
        
        
        TBXMLElement *titleElement = [TBXML childElementNamed:@"title" parentElement:tbxml.rootXMLElement];
        TBXMLElement *shortDesElement = [TBXML childElementNamed:@"sDescription" parentElement:tbxml.rootXMLElement];
        TBXMLElement *longDesElement = [TBXML childElementNamed:@"lDescription" parentElement:tbxml.rootXMLElement];
        TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:@"hubCitiId" parentElement:tbxml.rootXMLElement];
        TBXMLElement *imgPathElement = [TBXML childElementNamed:@"imgPath" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *startDateElement = [TBXML childElementNamed:@"sDate" parentElement:tbxml.rootXMLElement];
        TBXMLElement *endDateElement = [TBXML childElementNamed:@"eDate" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *purchasedProductsElement = [TBXML childElementNamed:@"purchaseProducts" parentElement:tbxml.rootXMLElement];
        TBXMLElement *fundGoalElement = [TBXML childElementNamed:@"fundGoal" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *currentValueElement = [TBXML childElementNamed:@"currentLevel" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *moreInfoURLElement = [TBXML childElementNamed:@"moreInfoURL" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *isEventFlagElement = [TBXML childElementNamed:@"isEventFlag" parentElement:tbxml.rootXMLElement];
        
        
        if(moreInfoURLElement)
            iEventDetailsDO.moreInfoURL = [TBXML textForElement:moreInfoURLElement];
        
        if(isEventFlagElement)
            iEventDetailsDO.isEventFlag = [TBXML textForElement:isEventFlagElement];
        
        if(titleElement)
            iEventDetailsDO.title = [TBXML textForElement:titleElement];
        
        if(shortDesElement)
            iEventDetailsDO.shortDescription = [TBXML textForElement:shortDesElement];
        
        if(longDesElement)
            iEventDetailsDO.longDescription = [TBXML textForElement:longDesElement];
        
        if(imgPathElement)
            iEventDetailsDO.imagePath = [TBXML textForElement:imgPathElement];
        
        
        if(hubCitiIdElement)
            iEventDetailsDO.hubCitiId = [TBXML textForElement:hubCitiIdElement];
        
        if(currentValueElement)
            iEventDetailsDO.currentValue = [TBXML textForElement:currentValueElement];
        
        if(purchasedProductsElement)
            iEventDetailsDO.purchasedProducts = [TBXML textForElement:purchasedProductsElement];
        
        if(fundGoalElement)
            iEventDetailsDO.goal = [TBXML textForElement:fundGoalElement];
        
        if(startDateElement)
            iEventDetailsDO.startDate = [TBXML textForElement:startDateElement];
        
        if(endDateElement)
            iEventDetailsDO.endDate = [TBXML textForElement:endDateElement];
        
        [arrFundraisingDetails addObject:iEventDetailsDO];
        // [iEventDetailsDO release];
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
}



#pragma mark Table View Delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [evtArrRetailersAddress count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        return 90.0;
    else
        return 70.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //	UITableViewCell *cell2;
    static NSString *CellIdentifier = @"CellEventLocation";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i =0; i < [arr count]; i++)
        {
            UIView *v = (UIView*)[arr objectAtIndex:i];
            [v removeFromSuperview];
        }
    }
    
    SdImageView *asyncImageView = nil;
    UILabel *lblAppSiteName = nil;
    UILabel *lblLocation = nil;
    UILabel *lblLocation1 = nil;
    UILabel *lblLocation2 = nil;
    UILabel *eventLocation = nil;
    if([[defaults valueForKey:@"isAppsiteFlag"]isEqualToString:@"1"] || [[defaults valueForKey:@"isAppsiteFlag"]isEqualToString:@"0"])
    {
        
        if([evtArrRetailerLogo count]>indexPath.row)
        {
            asyncImageView = [[SdImageView alloc] init];
            asyncImageView.backgroundColor = [UIColor clearColor];
            // asyncImageView.layer.cornerRadius = 5.0f;
            [asyncImageView loadImage:arrRetailerLogo[indexPath.row]];
            
            [cell.contentView addSubview:asyncImageView];
            //[asyncImageView release];
        }
        
        if([evtArrRetailers count]>indexPath.row)
        {
            lblAppSiteName = [[UILabel alloc]init];
            lblAppSiteName.backgroundColor = [UIColor clearColor];
            lblAppSiteName.textColor=[UIColor blueColor];
            if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                lblAppSiteName.font = [UIFont boldSystemFontOfSize:18];
            else
                lblAppSiteName.font = [UIFont boldSystemFontOfSize:13];
            //lblAppSiteName.numberOfLines = 2;
            lblAppSiteName.lineBreakMode=NSLineBreakByWordWrapping;
            lblAppSiteName.text =[NSString stringWithFormat:@"%@:",[evtArrRetailers objectAtIndex:indexPath.row]];
            [cell.contentView addSubview:lblAppSiteName];
        }
        
        lblLocation = [[UILabel alloc]init];
        lblLocation.backgroundColor = [UIColor clearColor];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            lblLocation.font = [UIFont systemFontOfSize:14];
        else
            lblLocation.font = [UIFont systemFontOfSize:9];
        lblLocation.numberOfLines = 2;
        lblLocation.lineBreakMode=NSLineBreakByWordWrapping;
        if([evtArrRetailersAddress count]>indexPath.row)
            lblLocation.text =[evtArrRetailersAddress objectAtIndex:indexPath.row];
        [cell.contentView addSubview:lblLocation];
        NSLog(@"Fundraiser %lu %lu %ld",(unsigned long)evtArrRetailerLogo.count,(unsigned long)evtArrRetailers.count,(long)indexPath.row);
        if([evtArrRetailerLogo count]>indexPath.row && [evtArrRetailers count]>indexPath.row)
            viewDictionary = NSDictionaryOfVariableBindings(asyncImageView,lblAppSiteName,lblLocation);
        
        else if(!([evtArrRetailerLogo count]>indexPath.row && [evtArrRetailers count]>indexPath.row))
            viewDictionary = NSDictionaryOfVariableBindings(lblAppSiteName,lblLocation);
        else
            viewDictionary = NSDictionaryOfVariableBindings(asyncImageView,lblLocation);
    }
    else
    {
        //        isAppSiteFlag=NO;
        
        if (![[evtArrRetailersAddress objectAtIndex:indexPath.row] isEqualToString:@"N/A"]) {
            if(DEVICE_TYPE ==UIUserInterfaceIdiomPad ){
                eventLocation = [[UILabel alloc]initWithFrame:CGRectMake(-65, -3, SCREEN_WIDTH-55, 20)];
            }
            else{
                eventLocation = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, SCREEN_WIDTH-65, 70)];
            }
            eventLocation.backgroundColor = [UIColor clearColor];
            if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                eventLocation.font = [UIFont systemFontOfSize:18];
            else
                eventLocation.font = [UIFont boldSystemFontOfSize:13];
            eventLocation.numberOfLines = 3;
            eventLocation.textAlignment=NSTextAlignmentCenter;
            eventLocation.lineBreakMode=NSLineBreakByWordWrapping;
            eventLocation.text= [evtArrRetailersAddress objectAtIndex:indexPath.row];
            [cell.contentView addSubview:eventLocation];
        }
        
        
        lblLocation1 = [[UILabel alloc]init];
        lblLocation1.backgroundColor = [UIColor clearColor];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            lblLocation1.font = [UIFont systemFontOfSize:18];
        else
            lblLocation1.font = [UIFont systemFontOfSize:13];
        //lblLocation1.numberOfLines = 3;
        lblLocation1.textAlignment=NSTextAlignmentCenter;
        lblLocation1.lineBreakMode=NSLineBreakByWordWrapping;
        if([evtArrAddress1 count]>indexPath.row)
            lblLocation1.text =[evtArrAddress1 objectAtIndex:indexPath.row];
        [cell.contentView addSubview:lblLocation1];
        
        lblLocation2 = [[UILabel alloc]init];
        lblLocation2.backgroundColor = [UIColor clearColor];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            lblLocation2.font = [UIFont systemFontOfSize:18];
        else
            lblLocation2.font = [UIFont systemFontOfSize:13];
        //lblLocation1.numberOfLines = 3;
        lblLocation2.textAlignment=NSTextAlignmentCenter;
        lblLocation2.lineBreakMode=NSLineBreakByWordWrapping;
        if([evtArrAddress2 count]>indexPath.row)
            lblLocation2.text =[evtArrAddress2 objectAtIndex:indexPath.row];
        [cell.contentView addSubview:lblLocation2];
        viewDictionary = NSDictionaryOfVariableBindings(lblLocation1, lblLocation2);
    }
    
    asyncImageView.translatesAutoresizingMaskIntoConstraints = NO;
    lblAppSiteName.translatesAutoresizingMaskIntoConstraints = NO;
    lblLocation.translatesAutoresizingMaskIntoConstraints = NO;
    lblLocation1.translatesAutoresizingMaskIntoConstraints = NO;
    lblLocation2.translatesAutoresizingMaskIntoConstraints = NO;
    //    eventLocation.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self setConstraints:cell];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([[defaults valueForKey:@"isAppsiteFlag"]isEqualToString:@""]) {
        [self showMapAddress];
    }
    else if ([[defaults valueForKey:@"isAppsiteFlag"]isEqualToString:@"1"]||[[defaults valueForKey:@"isAppsiteFlag"]isEqualToString:@"0"])
    {
        //        [linkID insertObject:@"0" atIndex:[linkID count]];
        [self pullAppSite:(int)indexPath.row];
    }
    [self crossClicked];
}

-(void)crossClicked
{
    fundraiserView.scrollEnabled=YES;
    fundraiserView.userInteractionEnabled = YES;
    [cross_button removeFromSuperview];
    [tblEventLocations removeFromSuperview];
    [tblEventHolder removeFromSuperview];
    
}


-(void)setConstraints:(UITableViewCell*)cell
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        
        if([viewDictionary objectForKey:@"asyncImageView"]!=nil)
        {
            // Vertical constrain
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[asyncImageView(60)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(60)]"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblAppSiteName"]!=nil)
        {
            // Vertical constrain
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[lblAppSiteName(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(75)-[lblAppSiteName(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
        }
        
        
        
        if([viewDictionary objectForKey:@"lblLocation"]!=nil)
        {
            // Vertical constrain
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(35)-[lblLocation(40)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(75)-[lblLocation(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblLocation1"]!=nil)
        {
            // Vertical constrain
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(18)-[lblLocation1(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(-65)-[lblLocation1(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblLocation2"]!=nil)
        {
            // Vertical constrain
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(43)-[lblLocation2(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(-65)-[lblLocation2(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
        }
    }
    else
    {
        if([viewDictionary objectForKey:@"asyncImageView"]!=nil)
        {
            // Vertical constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(3)-[asyncImageView(44)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(44)]"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblAppSiteName"]!=nil)
        {
            // Vertical constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(3)-[lblAppSiteName(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(55)-[lblAppSiteName(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
        }
        
        
        if([viewDictionary objectForKey:@"lblLocation"]!=nil)
        {
            // Vertical constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(23)-[lblLocation(30)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(55)-[lblLocation(%f)]",SCREEN_WIDTH - 110] options:0 metrics:0 views:viewDictionary]];
        }
        
        
        if ([evtLocation isEqualToString:@"N/A"]) {
            if([viewDictionary objectForKey:@"lblLocation1"]!=nil)
            {
                // Vertical constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(3)-[lblLocation1(20)]"] options:0 metrics:0 views:viewDictionary]];
                
                // Horizontal constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[lblLocation1(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
            }
            
            if([viewDictionary objectForKey:@"lblLocation2"]!=nil)
            {
                // Vertical constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(23)-[lblLocation2(20)]"] options:0 metrics:0 views:viewDictionary]];
                
                // Horizontal constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[lblLocation2(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
            }
            
        }
        else
        {
            
            if([viewDictionary objectForKey:@"lblLocation1"]!=nil)
            {
                // Vertical constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(23)-[lblLocation1(20)]"] options:0 metrics:0 views:viewDictionary]];
                
                // Horizontal constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[lblLocation1(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
            }
            
            if([viewDictionary objectForKey:@"lblLocation2"]!=nil)
            {
                // Vertical constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(43)-[lblLocation2(20)]"] options:0 metrics:0 views:viewDictionary]];
                
                // Horizontal constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[lblLocation2(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
            }
            
        }
        
    }
}




#pragma mark map show functionality

-(void)showMapAddress
{
    
    BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
    if (!lsFlag) {
        UIAlertController * alert;
        
        alert=[UIAlertController alertControllerWithTitle:@"Fundraiser uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action){
                                 EventsMapViewController *iMapViewController = [[EventsMapViewController alloc]initWithNibName:@"EventsMapViewController" bundle:[NSBundle mainBundle]];
                                 
                                 iMapViewController.latitude=[[NSArray alloc]initWithArray:evtLatitude];
                                 iMapViewController.longitude=[[NSArray alloc]initWithArray:evtLongitude];
                                 iMapViewController.address1=[[NSArray alloc]initWithArray:evtArrRetailersAddress];
                                 //            iMapViewController.address2=[[NSArray alloc]initWithArray:evtArrAddress2];
                                 
                                 [self.navigationController pushViewController:iMapViewController animated:NO];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else{
        
        if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse))
        {
            [defaults setValue:@"" forKey:KEY_LATITUDE];
            [defaults setValue:@"" forKey:KEY_LONGITUDE];
            
            [Location updateLocationinfo:self];
        }
        
        EventsMapViewController *iMapViewController = [[EventsMapViewController alloc]initWithNibName:@"EventsMapViewController" bundle:[NSBundle mainBundle]];
        
        iMapViewController.latitude=[[NSArray alloc]initWithArray:evtLatitude];
        iMapViewController.longitude=[[NSArray alloc]initWithArray:evtLongitude];
        iMapViewController.address1=[[NSArray alloc]initWithArray:evtArrRetailersAddress];
        //        iMapViewController.address2=[[NSArray alloc]initWithArray:evtArrAddress2];
        
        [self.navigationController pushViewController:iMapViewController animated:NO];
    }
    
    
    
}


#pragma mark - Location Service Methods
-(void) updatedLatlongValue :(NSString *) latValue longitudeValue:(NSString *)longValue{
    
    [defaults setBool:YES forKey:@"gpsEnabled"];
    [defaults setObject:latValue forKey:KEY_LATITUDE];
    [defaults setObject:longValue forKey:KEY_LONGITUDE];
    
    //Added the setting for these keys because its what the Map is using.
    [defaults  setValue:latValue forKey:@"LatVal"];
    [defaults  setValue:longValue forKey:@"LongVal"];
}

-(void) pullAppSite:(int)rowNumber
{
    objIndex = rowNumber;
    iWebRequestState = RETSUMMARY;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[evtArrRetailerLocId objectAtIndex:rowNumber]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && evtLatitude){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[evtLongitude  objectAtIndex:rowNumber],[evtLatitude objectAtIndex:rowNumber]]
        ;
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",[evtArrRetailerId objectAtIndex:rowNumber]];
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    NSString *urlString =  [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);
}



-(void)parse_retSummary:(NSString *)response
{
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else
    {
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        //        rsvc.distanceFromPreviousScreen = [arrDistance objectAtIndex:objIndex];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
    
}
#pragma mark sender user tracking
-(void)userTracking:(NSString*)xmlStr
{
    
    NSMutableString *urlStr = [[NSMutableString alloc]init];
    [urlStr appendFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    
    NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:xmlStr]];
    DLog(@"%@",response);
    //[xmlStr release];
}

-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil) {
        
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isEventLogisticsFlag = 1;
        [HubCitiAppDelegate setIsLogistics:YES];       
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}

#pragma mark webview delegate methods


-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    
    if ([[[inRequest URL] scheme] isEqual:@"mailto"]) {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,
                                                                                                      (CFStringRef)value,
                                                                                                      CFSTR("")));
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            //[mailViewController release];
            return NO;
            
        }
        
        else {
            [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
        }
        
        return NO;
        
    }
    else  if ([[[inRequest URL] scheme] isEqual:@"tel"]) {
        NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
        
        NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
        NSString *phoneString  = [[defaultRecipient componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        NSString *phoneNumber = [NSString stringWithFormat:@"tel:%@", phoneString];
        NSURL    *url= [NSURL URLWithString:phoneNumber];
        
        UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        webview.alpha = 0.0;
        
        [webview loadRequest:[NSURLRequest requestWithURL:url]];
        
        // Assume we are in a view controller and have access to self.view
        [self.view insertSubview:webview belowSubview:fundraiserView];
        // [webview release];
        return NO;
        
    }
    
    
    else if ([[inRequest URL] fragment]) {
        
        
        [inWeb stringByEvaluatingJavaScriptFromString:@"window.location.hash='#hashtag'"];
        
        return YES;
    }
    
    
    else if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        
        
        NSString *url = [[inRequest URL]absoluteString];
        [defaults setValue:url forKey:KEY_URL];
        [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
        
        NSURL* urls = [NSURL URLWithString:[[inRequest URL]absoluteString]];
        
        NSString *urlString=[[inRequest URL]absoluteString];
        
        if ([[urls absoluteString] containsString:@"itunes.apple.com"]) {
            [UtilityManager customUrlLoad:urls];
        }
        
        else if ([urlString containsString:@"3000.htm"]) {
            // NSString *name =[NSString stringWithFormat:@"%@",[urls.pathComponents objectAtIndex:3]];
            
            if([urlString isEqualToString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                else{
                    
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                        [HubCitiAppDelegate setIsLogistics:YES];
                        splOfferVC.isEventLogisticsFlag = 1;
                    

                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            else{
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
                
            }
        }
        else{
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
            
        }
        return NO;
    }
    
    return YES;
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];// ModalViewControllerAnimated:YES];
}



-(int)calculateLabelHeight:(NSString*)text labelWidth:(int)labelWidth fontSize:(int)fontSize
{
    CGSize constraint;
    CGSize size ;
    CGFloat height ;
    constraint = CGSizeMake(labelWidth, CGFLOAT_MAX);
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    size = [text boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:fontSize], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    
    height = MAX(size.height+20, 40.0f);
    return height;
}



-(void)DetailsButtonClicked:(id)sender
{
    UIButton *btnEvent = (UIButton*)sender;
    
    FundraiserDeatilsDO *iEventDetailsDO;
    iEventDetailsDO = [arrFundraisingDetails objectAtIndex:0];
    
    switch (btnEvent.tag)
    {
            
        case 1: // More Info
        {
            WebBrowserViewController *iWebBrowserViewController = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:iWebBrowserViewController animated:NO];
            // [iWebBrowserViewController release];
        }
            break;
        case 2: // Events
        {
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            self.iEventsListViewController = iEventsListViewController;
            iEventsListViewController.fundId = [defaults valueForKey:FUNDRAISERID];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
        }
            break;
            
        case 3: // More Info
        {
            WebBrowserViewController *iWebBrowserViewController = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [defaults setValue:iEventDetailsDO.purchasedProducts forKey:KEY_URL];
            [self.navigationController pushViewController:iWebBrowserViewController animated:NO];
            //[iWebBrowserViewController release];
        }
            break;
            
            
        case 4:
        {
            fundraiserView.scrollEnabled=NO;
            fundraiserView.userInteractionEnabled = NO;
            [self Request_fundappsiteloc];
           
            
        }
            
        default:
            break;
    }
}

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}




#pragma mark share events methods

-(void)shareClicked
{
    iWebRequestState = FUNDRAISER_SHARE;
    
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    
    
    [reqStr appendFormat:@"<ShareProductInfo><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [reqStr appendFormat:@"<fundraiserId>%@</fundraiserId>",[defaults valueForKey:FUNDRAISERID]];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharefundraiser",BASE_URL];
    DLog(@"%@",urlString);
    [ConnectionManager establishConnectionFor:reqStr base:urlString withDelegate:self];
    
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    
    [self responseData:response];
    //[reqStr release];
    
}


-(void)shareDetails{
    
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        
        [self shareClicked];
        
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        // [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
    }
    
}

-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Fundraiser Details Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    //    if(IPAD){
    //        actionSheet.popoverPresentationController.sourceView = btnCalendar;
    //        actionSheet.popoverPresentationController.sourceRect =  btnCalendar.bounds;
    //    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"FUNDRAISER_SHARE"];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
                NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><fundId>%@</fundId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:FUNDRAISERID]];
                
                [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
        
        
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    //[defaults setValue:FUNDRAISER_SHARE forKey:KEY_SHARE_TYPE];
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"FUNDRAISER_SHARE"];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
}



-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];

            NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><fundId>%@</fundId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:FUNDRAISERID]];
            
            [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)parse_shareFundraiser:(NSString*)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        //        TBXMLElement *fundDetails = [TBXML childElementNamed:@"Fundraiser" parentElement:tbXml.rootXMLElement];
        
        
        TBXMLElement *shareText=[TBXML childElementNamed:@"fundName" parentElement:tbXml.rootXMLElement];
        TBXMLElement *emailURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *imagePath = [TBXML childElementNamed:@"imagePath" parentElement:tbXml.rootXMLElement];
        
        if (emailURL!=nil) {
            [defaults setValue:[TBXML textForElement:emailURL] forKey:KEY_HYPERLINK];
            NSLog(@"The link to be shared is %@",KEY_HYPERLINK);
        }
        
        if (shareText!=nil) {
            NSMutableString *msgString = [[NSMutableString alloc]init];
            [msgString appendFormat:@"I found this Fundraiser in the %@ mobile app and thought you might be interested:",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [msgString appendFormat:@"%@",shareString];
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            NSLog(@"The share message value is %@",KEY_SHAREMSG);
        }
        
        if (imagePath!=nil) {
            [defaults setObject:[TBXML textForElement:imagePath] forKey:KEY_PRODUCTIMGPATH];
            NSLog(@"The product image path is %@",KEY_PRODUCTIMGPATH);
        }
        
        
        
        
        
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            return;
        }
        
    }
}


@end

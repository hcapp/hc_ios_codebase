//
//  FundraiserGroupingAndSortingViewController.m
//  HubCiti
//
//  Created by Kitty on 19/08/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import "FundraiserGroupingAndSortingViewController.h"
#import  "QuartzCore/QuartzCore.h"
#import "MainMenuViewController.h"
#import "CouponsInDealViewController.h"

@interface FundraiserGroupingAndSortingViewController ()

@end

@implementation FundraiserGroupingAndSortingViewController
@synthesize citiesDic,arrCities,arrCitiId,selectedCitiIds,isFundraisingEvent,retailId,retailLocationId,fundDeal;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    UIButton *btn = [UtilityManager customizeBarButtonItem:@"Done"];
    [btn addTarget:self action:@selector(doneButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = mainPage;
    [self.navigationItem.leftBarButtonItem setAccessibilityLabel:@"Done"];
    //[mainPage release];
    
    UIButton *btncancel = [UtilityManager customizeBarButtonItem:NSLocalizedString(@"Cancel",@"Cancel")];
    [btncancel addTarget:self action:@selector(cancelButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* btncancelbar = [[UIBarButtonItem alloc] initWithCustomView:btncancel];
    self.navigationItem.rightBarButtonItem = btncancelbar;
    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"Cancel"];
    //[btncancelbar release];
    
    
    self.navigationItem.title = @"Group & Sort";
    
    arrDepartMenu = [[NSMutableArray alloc]init];
//    arrGroups = [[NSMutableArray alloc]initWithObjects:@"Event Date",@"Alphabetically",@"Type",nil];
//    arrSorting = [[NSMutableArray alloc]initWithObjects:@"Date",@"Name",nil];
//    
//    if([[defaults valueForKey:@"departmentFlag"]isEqualToString:@"1"])
//        [arrGroups addObject:@"Department"];
    
//    [self setViewForGroupingNSorting];
    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([RegionApp isEqualToString:@"1"]) {
        [self fetchCityPreference];
    }
    
    arrGroups = [[NSMutableArray alloc]initWithObjects:@"Date",@"Alphabetically",@"Type",nil];
    
    [self setViewForGroupingNSorting];
}



-(void)fetchCityPreference{
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId><userId>%@</userId><module>Fund</module>", [defaults  valueForKey:KEY_HUBCITIID],[defaults  valueForKey:KEY_USERID]];
    
    if(isFundraisingEvent == TRUE)
    {
        [xmlStr appendFormat:@"<retailId>%@</retailId><retailLocationId>%@</retailLocationId>",retailId,retailLocationId];
    }
       
    else{
        
        if([defaults valueForKey:KEY_MITEMID]){
            [xmlStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
        else if ([FundraiserBottomButtonID count]>=1)
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FundraiserBottomButtonID objectAtIndex:[FundraiserBottomButtonID count]-1]];
        else{
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
    }
    
    [xmlStr appendFormat:@"</UserDetails>"];
    
    //    [xmlStr appendFormat:@"<UserDetails><userId>19</userId><hubCitiId>1143</hubCitiId><module>Events</module><mItemId>12345</mItemId></UserDetails>"];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercitypref",BASE_URL];
    
    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    [self parseCityPreference:responseXML];
    ReleaseAndNilify(xmlStr);
    
}

-(void)parseCityPreference:(NSString *)responseXml{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
    {
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        arrCities =[[NSMutableArray alloc]init];
        arrCitiId =[[NSMutableArray alloc]init];
        TBXMLElement *cityListElement = [TBXML childElementNamed:@"cityList" parentElement:tbXml.rootXMLElement];
        if(cityListElement)
        {
            cityFlag = 1;
            TBXMLElement *CityElement = [TBXML childElementNamed:@"City" parentElement:cityListElement];
            while (CityElement)
            {
                
                TBXMLElement *cityIdElement = [TBXML childElementNamed:@"cityId" parentElement:CityElement];
                TBXMLElement *cityNameElement = [TBXML childElementNamed:@"cityName" parentElement:CityElement];
                
                if (cityIdElement) {
                    
                    [arrCitiId addObject:[TBXML textForElement:cityIdElement]];
                }
                
                if (cityNameElement) {
                    
                    [arrCities addObject:[TBXML textForElement:cityNameElement]];
                }
                CityElement = [TBXML nextSiblingNamed:@"City" searchFromElement:CityElement];            }
        }
        
        
        if ([arrCities count]!=0) {
            citiesDic = [[NSDictionary alloc] initWithObjects:arrCitiId forKeys:arrCities];
        }
        
        
        if ([defaults valueForKey:@"SelectedCityIds"]==nil) {
            
            [defaults setValue:arrCitiId forKey:@"SelectedCityIds"];
        }
        
        selectedCitiIds = [[NSMutableArray alloc]initWithArray:[defaults valueForKey:@"SelectedCityIds"]];
    }
    
//    else
//    {
//        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
//        
//        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
//        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
//        cityFlag=0;
//        
//    }

    if ([defaults valueForKey:@"SelectedCityIds"]==nil) {
        
        [defaults setValue:arrCitiId forKey:@"SelectedCityIds"];
    }
    
    selectedCitiIds = [[NSMutableArray alloc]initWithArray:[defaults valueForKey:@"SelectedCityIds"]];
}




-(void)cancelButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)doneButtonTouched:(id)sender
{
    [defaults setValue:@"YES" forKey:@"isComingFromGroupingandSorting"];
    // Check the selected group
    if(groupSelectionVal == 0)
        [defaults setValue:@"date" forKey:@"GroupFundraiserBy"];
    else if(groupSelectionVal == 1)
        [defaults setValue:@"atoz" forKey:@"GroupFundraiserBy"];
    else if(groupSelectionVal == 2)
        [defaults setValue:@"type" forKey:@"GroupFundraiserBy"];
    
    // Check the Selected Sorting Value
    if(sortSelectionval == 0){
        [defaults setValue:@"date" forKey:@"SortFundraiserBy"];
        [defaults setValue:nil forKey:@"SelectedCityIds"];
    }
    else if(sortSelectionval == 1){
        [defaults setValue:@"name" forKey:@"SortFundraiserBy"];
        [defaults setValue:nil forKey:@"SelectedCityIds"];
    }
    else if(sortSelectionval == 2){
        NSString *str=[defaults valueForKey:@"departmentId"];
        if (![str isEqualToString:@"0"]) {
            [defaults setValue:@"department" forKey:@"SortFundraiserBy"];
            [defaults setValue:nil forKey:@"SelectedCityIds"];
            [defaults setValue:nil forKey:@"commaSeperatedCities"];
        }
        else{
            [defaults setValue:@"City" forKey:@"SortFundraiserBy"];
            
            if ([selectedCitiIds count]==0) {
                [defaults setValue:nil forKey:@"SelectedCityIds"];
                [defaults setValue:nil forKey:@"commaSeperatedCities"];
            }
            else{
                [defaults setValue:selectedCitiIds forKey:@"SelectedCityIds"];
                NSMutableString *requestStr = [[NSMutableString alloc] init];
                for (int i=0; i<[selectedCitiIds count]; i++) {
                    
                    [requestStr appendFormat:@"%@,", selectedCitiIds[i]];
                }
                
                if ([requestStr hasSuffix:@","])
                    [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
                [defaults setValue:requestStr forKey:@"commaSeperatedCities"];
            }
            
        }
       
    }
    else if(sortSelectionval == 3){
        [defaults setValue:@"City" forKey:@"SortFundraiserBy"];
        
        if ([selectedCitiIds count]==0) {
            [defaults setValue:nil forKey:@"SelectedCityIds"];
            [defaults setValue:nil forKey:@"commaSeperatedCities"];
        }
        else{
            [defaults setValue:selectedCitiIds forKey:@"SelectedCityIds"];
            NSMutableString *requestStr = [[NSMutableString alloc] init];
            for (int i=0; i<[selectedCitiIds count]; i++) {
                
                [requestStr appendFormat:@"%@,", selectedCitiIds[i]];
            }
            
            if ([requestStr hasSuffix:@","])
                [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
            [defaults setValue:requestStr forKey:@"commaSeperatedCities"];
        }
        
        
    }
    
    
    
    
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)setViewForGroupingNSorting
{
    
    if(cityFlag ==1 && [RegionApp isEqualToString:@"1"]){
        if ([[defaults valueForKey:@"fundDepartmentFlag"]isEqualToString:@"1"]) {
            arrSorting = [[NSMutableArray alloc]initWithObjects:@"Date",@"Name",@"Department",@"City",nil];
        }
        
        else{
            arrSorting = [[NSMutableArray alloc]initWithObjects:@"Date",@"Name",@"City",nil];
        }
    }
    else{
        if ([[defaults valueForKey:@"fundDepartmentFlag"]isEqualToString:@"1"]) {
            arrSorting = [[NSMutableArray alloc]initWithObjects:@"Date",@"Name",@"Department",nil];
        }
        else{
            arrSorting = [[NSMutableArray alloc]initWithObjects:@"Date",@"Name",nil];
        }
    }
    

    
    
    NSString *strG = [defaults valueForKey:@"GroupFundraiserBy"];
    NSString *strS = [defaults valueForKey:@"SortFundraiserBy"];
    
    if([strG isEqualToString:@"date"])
        groupSelectionVal = 0;
    else if([strG isEqualToString:@"atoz"])
        groupSelectionVal = 1;
    else if([strG isEqualToString:@"type"])
        groupSelectionVal = 2;
    
    if([strS isEqualToString:@"date"])
        sortSelectionval = 0;
    else if([strS isEqualToString:@"name"])
        sortSelectionval = 1;
    else if([strS isEqualToString:@"department"])
        sortSelectionval = 2;
    else if([strS isEqualToString:@"City"]){
        if ([[defaults valueForKey:@"fundDepartmentFlag"]isEqualToString:@"1"]) {
            sortSelectionval = 3;
        }
        else{
            sortSelectionval = 2;
        }
    }
    
    
    //table height = 2 sections height + row height * arrays count
    tblGroupingNSorting = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,SCREEN_HEIGHT-60) style:UITableViewStylePlain];
    tblGroupingNSorting.dataSource=self;
    tblGroupingNSorting.delegate=self;
    [tblGroupingNSorting setAccessibilityLabel:@"sortEventsTable"];
    [tblGroupingNSorting setAccessibilityIdentifier:@"eventsTableSort"];
    [tblGroupingNSorting setBackgroundColor:[UIColor whiteColor]];
    tblGroupingNSorting.tableFooterView.hidden=YES;
    
    [self.view addSubview:tblGroupingNSorting];
    
    [tblGroupingNSorting reloadData];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 44.0;
    }
    else
    {
        return 70.0;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 20.0;
    }
    else
    {
        return 30.0;
    }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
        return [arrGroups count];
    else
        return [arrSorting count];
    
    return 0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *lblCatName ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 18)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
    }
    else
    {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 28)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
    }
    if(section==0)
        [lblCatName setText:@" Group Items by"];
    else
        [lblCatName setText:@" Sort Items by"];
    
    [lblCatName setTextColor:[UIColor blackColor]];
    [lblCatName setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
    [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
    
    return lblCatName;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellEventListView";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor =  [UIColor clearColor];
        
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }
        
    }
    
    
    
    if(indexPath.section == 0)
    {
        UILabel *lblEvtDate;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 20)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        }
        else
        {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 30)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
        }
        [lblEvtDate setText:[arrGroups objectAtIndex:indexPath.row]];
        [lblEvtDate setTextColor:[UIColor blackColor]];
        [lblEvtDate setBackgroundColor:[UIColor clearColor]];
        //[lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        [cell.contentView addSubview:lblEvtDate];
        //[lblEvtDate release];
        
        if(indexPath.row == groupSelectionVal)
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        UILabel *lblEvtDate;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 20)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        }
        else
        {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 30)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
        }
        [lblEvtDate setText:[arrSorting objectAtIndex:indexPath.row]];
        [lblEvtDate setTextColor:[UIColor blackColor]];
        [lblEvtDate setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:lblEvtDate];
        //[lblEvtDate release];
        
        
        if(isExpanded==TRUE && indexPath.row>([arrSorting indexOfObject:@"City"])){
            
            UIButton *content ;
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                content = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 45)];
            }
            else
            {
                content = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
            }
            [content setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
            content.tag = indexPath.row;
            [content addTarget:self action:@selector(didSelectCity:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            if ([selectedCitiIds containsObject:[citiesDic valueForKey:[arrSorting objectAtIndex:content.tag]]]) {
                checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOn.png"]];
                if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                    checkButton.frame = CGRectMake(10, 15, 20, 20);
                    
                }
                else
                {
                    checkButton.frame = CGRectMake(10, 20, 30, 30);
                    
                }                checkButton.backgroundColor =[UIColor clearColor];
            }
            
            else{
                checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOff.png"]];
                if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                    checkButton.frame = CGRectMake(10, 15, 20, 20);
                    
                }
                else
                {
                    checkButton.frame = CGRectMake(10, 20, 30, 30);
                    
                }
                checkButton.backgroundColor =[UIColor clearColor];
                
                
            }
            
            UILabel *lblCiti ;
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                lblCiti = [[UILabel alloc]initWithFrame:CGRectMake(40, 0, SCREEN_WIDTH, 45)];
                [lblCiti setFont:[UIFont boldSystemFontOfSize:13]];
            }
            else
            {
                lblCiti = [[UILabel alloc]initWithFrame:CGRectMake(50, 0, SCREEN_WIDTH, 70)];
                [lblCiti setFont:[UIFont boldSystemFontOfSize:18]];
            }
            [lblCiti setText:[arrSorting objectAtIndex:indexPath.row]];
            [lblCiti setTextColor:[UIColor blackColor]];
            [lblCiti setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
           // [lblCiti setFont:[UIFont boldSystemFontOfSize:13]];
            
            [content addSubview:checkButton];
            [content addSubview:lblCiti];
            [cell.contentView addSubview:content];
           // [content release];
        }
        
        if(indexPath.row == sortSelectionval)
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
        groupSelectionVal =(int) indexPath.row;
    
    else if(indexPath.section==1 && indexPath.row==2 && [[defaults valueForKey:@"fundDepartmentFlag"]isEqualToString:@"1"])
    {
        sortSelectionval = 2;
        [arrSorting removeObjectsInArray:[citiesDic allKeys]];
        isExpanded = FALSE;
//        if ([selectedCitiIds count]>0) {
//            [selectedCitiIds removeAllObjects];
//        }
//        
//        [selectedCitiIds addObjectsFromArray:[citiesDic allValues]];

        if([arrDepartMenu count] <=0)
        {
            [self request_deptandmenutype];
        }
        else
            [self showpickerView];
    }
   
  
    else{
        if(indexPath.section==1 && cityFlag==1){
            
            if(indexPath.row == [arrSorting indexOfObject:@"City"])
            {
                
                if (isExpanded==FALSE )
                {
                    isExpanded = TRUE;
                    [arrSorting addObjectsFromArray:arrCities];
                    
                }
                
            }
            
            else{
                [arrSorting removeObjectsInArray:[citiesDic allKeys]];
                isExpanded = FALSE;
//                if ([selectedCitiIds count]>0) {
//                    [selectedCitiIds removeAllObjects];
//                }
//                
//                [selectedCitiIds addObjectsFromArray:[citiesDic allValues]];
                
            }
            
        }
        sortSelectionval = (int)indexPath.row;
    }
    
    [tblGroupingNSorting reloadData];
}

-(void)didSelectCity:(id)sender
{
    
    if([[citiesDic allValues] count]>= 1)
    {
        UIButton *btnCity = (UIButton*)sender;
        
        
        if ([selectedCitiIds containsObject:[citiesDic valueForKey:[arrSorting objectAtIndex:btnCity.tag]]]) {
            
            [selectedCitiIds removeObject:[citiesDic valueForKey:[arrSorting objectAtIndex:btnCity.tag]]];
            
        }
        
        else{
            [selectedCitiIds addObject:[citiesDic valueForKey:[arrSorting objectAtIndex:btnCity.tag]]];
            
        }
        [tblGroupingNSorting reloadData];
    }
    
}


-(void)request_deptandmenutype//Database Procedure Name usp_HcFundraisingDepartmentList
{
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<MenuItem><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [xmlStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    if(isFundraisingEvent == TRUE)
    {
        if (retailId && retailLocationId) {
        [xmlStr appendFormat:@"<retailId>%@</retailId><retailLocationId>%@</retailLocationId>",retailId,retailLocationId];
        }
    }
    
    
    else if (fundDeal == TRUE){
        
    }
    
    else{
        
        if([defaults valueForKey:KEY_MITEMID]){
            [xmlStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
        else if ([FundraiserBottomButtonID count]>=1)
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FundraiserBottomButtonID objectAtIndex:[FundraiserBottomButtonID count]-1]];
        else{
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
    }
    [xmlStr appendFormat:@"</MenuItem>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@alertevent/fundrserdeptlist",BASE_URL];
    
    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    ReleaseAndNilify(xmlStr);
    
}

-(void)responseData:(NSString *) response
{
    [self parse_deptandmenutype:response];

}


-(void)parse_deptandmenutype:(NSString*)responseXML

{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXML])
		return;
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXML];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mItemList = [TBXML childElementNamed:@"mItemList" parentElement:tbXml.rootXMLElement];
        if(mItemList)
        {
            TBXMLElement *MenuItemElement = [TBXML childElementNamed:@"MenuItem" parentElement:mItemList];
            while (MenuItemElement)
            {
                NSMutableDictionary *dicdepart = [[NSMutableDictionary alloc]init];
                
                
                // Add Department
                TBXMLElement *departmentIdElement = [TBXML childElementNamed:@"departmentId" parentElement:MenuItemElement];
                TBXMLElement *departmentNameElement = [TBXML childElementNamed:@"departmentName" parentElement:MenuItemElement];
                
                if(departmentIdElement)
                    [dicdepart setValue:[TBXML textForElement:departmentIdElement] forKey:@"departmentId"];
                if(departmentNameElement)
                    [dicdepart setValue:[TBXML textForElement:departmentNameElement] forKey:@"departmentName"];
                
                
                if([dicdepart count] > 0)
                    [arrDepartMenu addObject:dicdepart];
                
             //   [dicdepart release];
                
                MenuItemElement = [TBXML nextSiblingNamed:@"MenuItem" searchFromElement:MenuItemElement];
            }
            
        }
        
        if([arrDepartMenu count] > 0)
            [self showpickerView];
    }
//    else
//    {
//        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
//        
//		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
//		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
//    }
    
}

-(void)showpickerView
{
    tblGroupingNSorting.userInteractionEnabled = NO;
    
    // By Default selected first value
    if(sortSelectionval == 2 && [arrDepartMenu count] > 0)
    {
        NSMutableDictionary *dic = [arrDepartMenu objectAtIndex:0];
        [defaults setValue:[dic valueForKey:@"departmentId"] forKey:@"departmentId"];
        
    }
    
    if(pickerView)
    {
        [pickerView removeFromSuperview];
       // [pickerView release];
    }
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        if (IOS7){
            pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 221+70+44, 320, 170)];
        }
        else{
           pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 221+44, 320, 170)];
        }
    }
    else
    {
        pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 3*(SCREEN_HEIGHT/4), SCREEN_WIDTH, SCREEN_HEIGHT/4)];
    }
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    pickerView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    [self.view addSubview:pickerView];
    
    
    if(pickerToolbar)
    {
        [pickerToolbar removeFromSuperview];
       // [pickerToolbar release];
    }
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        if (IOS7){
            pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 221+70, 320, 44)];
        }
        else{
            pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 221, 320, 44)];
        }
    }
    else
    {
        pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,  3*(SCREEN_HEIGHT/4)-44, SCREEN_WIDTH, 44)];
    }
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    //[flexSpace release];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDone:)];
    [barItems addObject:doneBtn];
    //[doneBtn release];
    
    UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 20)];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.text = @"Select Type";
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.font = [UIFont boldSystemFontOfSize:15];
    lblTitle.textAlignment= NSTextAlignmentCenter;
    [pickerToolbar addSubview:lblTitle];
   // [lblTitle release];
    
    [pickerToolbar setItems:barItems animated:YES];
    //[barItems release];
    [self.view addSubview:pickerToolbar];
    
    [pickerView reloadAllComponents];
    [self.view bringSubviewToFront:pickerView];
}


-(void)pickerDone:(id)sender
{

    tblGroupingNSorting.userInteractionEnabled = YES;
    [pickerToolbar removeFromSuperview];
    [pickerView removeFromSuperview];
    
    //[pickerToolbar release];
    pickerToolbar = nil;
    //[pickerView release];
    pickerView = nil;
}






#pragma PICKERVIEW
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
   
        NSMutableDictionary *dic = [arrDepartMenu objectAtIndex:row];
        [defaults setValue:[dic valueForKey:@"departmentId"] forKey:@"departmentId"];

    
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{

    return [arrDepartMenu count];
 
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
   
        NSMutableDictionary *dic = [arrDepartMenu objectAtIndex:row];
        return [dic valueForKey:@"departmentName"];
   
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

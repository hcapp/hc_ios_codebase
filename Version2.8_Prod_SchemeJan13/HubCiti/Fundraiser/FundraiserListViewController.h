//
//  FundraiserListViewController.h
//  HubCiti
//
//  Created by Keerthi on 19/08/14.
//  Copyright (c) 2014 span infotech india ptv. ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>

BOOL maxFundraiserCountFlag;
@class DealHotDealsList;
@class RetailersListViewController;
@class FilterRetailersList;
@class CityExperienceViewController;
@class EventsListViewController;
@class FilterListViewController;
@class FAQCategoryList;
@class AnyViewController;
@class EmailShareViewController;

@interface FundraiserListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMessageComposeViewControllerDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>
{
    UITableView *FundraiserTableview;
    NSMutableArray *arrFundraisersCategoryInfo;
    NSMutableArray *arrFundraisersBottomButtonDO,*arrBottomButtonViewContainer;
    NSString *androidDownloadLink;
    WebRequestState iWebReqState;
    int rowCountOfTotalCells,bottomBtn;
    CommonUtility *common;
    BOOL isNextPageAvailable,isFundraiserEvent;
    
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,retain) FAQCategoryList *faqList;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,retain) FilterListViewController *filterList;
@property(nonatomic,retain)UITableView *FundraiserTableview;
@property(nonatomic,retain) NSString *categoryName;
@property(nonatomic,retain) NSString *retailId;
@property(nonatomic,retain) NSString *retailLocationId;
@property(nonatomic) BOOL isFundraiserEvent,fundDeal;
@property(nonatomic,retain) EventsListViewController* iEventsListViewController;
@property(nonatomic,retain) CityExperienceViewController *cityExp;
@property(nonatomic,retain) RetailersListViewController* retailerListScreen;
@property(nonatomic,retain) DealHotDealsList* hotDeals;
@property(nonatomic,retain) FilterRetailersList *filters;


@end

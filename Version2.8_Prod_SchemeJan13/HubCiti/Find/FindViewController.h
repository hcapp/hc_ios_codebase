//
//  FindViewController.h
//  Scansee
//
//  Created by Chaitra on 19/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ProductSearchUtility.h"
#import "HubCitiConnectionManager.h"
#import "HubCitiSegmentedControl.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "SwipesViewController.h"
#import "GetUserInfoResponse.h"
#import "HTTPClient.h"
#import "CatList.h"
#import "Categories.h"
#import "RetailersInDetail.h"
#import "RetailersDetails.h"

@class RetailersListViewController;
@class DealHotDealsList;
@class FilterRetailersList;
@class CityExperienceViewController;
@class FilterListViewController;
@class FundraiserListViewController;
@class EventsListViewController;
@class FAQCategoryList;
@class AnyViewController;
@class EmailShareViewController;
bool popFromNearBy;
bool tappedSearch;
NSString *keyWord,*catRetSearchKey;

typedef enum webServicesFind
{
    GETCATEGORY,
    SSCATSEARCH,
    SSRETSEARCH,
    GOOGLECATSEARCH,
    GOOGLERETSEARCH,
    GETSMARTSEAPRODS,
    GETPRODSEARCH,
    SCANSEESEARCH,
    SSRETSUMMARY,
    getsmartsearchprodlist,
    hubcitiAnythingInfo,
    appsitedetail,
    GETFAVCATEGORIE,
    PARTNERRETS,
    GETPARTNER,
    GETUSERINFOM,
    GETFAVLOCATION,
    DGETUSERINFO
}webServicesFindState;


@interface FindViewController : UIViewController<UITableViewDelegate , UITableViewDataSource ,CLLocationManagerDelegate,ProductSearchUtilityDelegate,UISearchBarDelegate, HubCitiConnectionManagerDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,HTTPClientDelegate,CustomizedNavControllerDelegate> {
    
    webServicesFindState iWebRequestState;
    
    UITableView *categoryTable ;
    IBOutlet NSLayoutConstraint *categoryTableHeightConstraint;
    HubCitiSegmentedControl *viewSelectionControl;
    
    IBOutlet UISearchBar *search, *googleSearch;
    UIButton *cancelBtn;
    NSString *fontColor,*cellColor;
	IBOutlet UIView *modelView;
	BOOL previousValue;
    NSString *androidDownloadLink;
	NSMutableArray *findCategoryArray;
	NSMutableArray *productName , *productId ,*productImageArray ,*rowCountArray,*prodListIDArray;
	NSMutableArray *shortDescription,*prodCountArray ;
    NSMutableArray *arrFindBottomButtonDO;
    NSMutableArray *arrBottomButtonViewContainer;
	NSIndexPath *index;
	CommonUtility *common;
    //pagination related
	int nextPage;
	int lowerLimit;
	int rowNum ,rowNumSize;
    
    //google search related
    IBOutlet UILabel *label1;
    IBOutlet UIView *view1;
    
    //smart search related
    BOOL smartSearch;
    HubCitiConnectionManager *connectionManager;
    BOOL fetchingNextResults;
    BOOL fromRetailerList;
    UIButton *locBtn;
    int keystrokeDelay;
    NSDate *date, *date1;
    NSTimeInterval ti1,ti2;
    NSTimer *timer;
    
    NSString *retSearchText;//to save retailer search and carry to next screen
    
    CGRect viewFrame;

    IBOutlet UIImageView *img;
    
    int bottomBtn;
    
    SortAndFilter *sortFilterObj;
    NSString *findTitle;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,retain) FAQCategoryList *faqList;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,retain) EventsListViewController* iEventsListViewController;
@property(nonatomic,retain) FilterListViewController *filterList;
@property(nonatomic,retain) FundraiserListViewController *iFundraiserListViewController;
@property(nonatomic,retain) CityExperienceViewController *cityExp;
@property(nonatomic,retain) RetailersListViewController* retailerListScreen;
@property(nonatomic,retain) DealHotDealsList* hotDeals;
@property(nonatomic,retain) FilterRetailersList *filters;
@property(nonatomic,retain) NSString *findTitle;
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property(nonatomic,strong) Categories* catResponse;
@property(nonatomic,strong) CatList* catListResponse;
@property(nonatomic,strong) RetailersDetails* ssretResponse,*sscatResponse;
@property(nonatomic,strong) RetailersInDetail* ssretdetail, *sscatdetail ;
//location categories
//-(void) getCategoryInfo ;
//-(void) parseStoreCategoryInfo : (NSString *) str ;

//product search
//-(void) getProductList :(NSString *) str ;
//-(void) parseSearchResult : (NSString *) responseStr ;

// pagination
-(void)fetchNextDeals:(id)sender;

//splash screen
-(void)showSplash;
-(void)hideSplash;

@end

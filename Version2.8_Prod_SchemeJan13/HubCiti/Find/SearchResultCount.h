//
//  SearchResultCount.h
//  Scansee
//
//  Created by ajit on 9/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum webServicesResultCount
{
    GETSMARTSEARCHCOUNT,
    GETSMARTSEARCHPRODLIST
 
}webServicesResultCountState;

@interface SearchResultCount : UIViewController<UITableViewDelegate,UITableViewDataSource,CustomizedNavControllerDelegate>{
    
    webServicesResultCountState iWebRequestState;
    IBOutlet UITableView *searchResultTable;
    NSMutableArray *prodIdArray,*prodNameArray,*prodListIDArray;
    NSMutableArray *prodCountArray,*rowNumArray;
    NSMutableArray *prodImgPathArray, *prodDescArray;
    NSString *latValue,*longValue;
    int nextPage,lastVisitedRecord;
    NSIndexPath *index;
    
}


@end

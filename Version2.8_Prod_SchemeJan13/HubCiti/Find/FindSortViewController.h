//
//  FindSortViewController.h
//  HubCiti
//
//  Created by Keerthi on 24/06/14.
//  Copyright (c) 2014 span infotech india ptv. ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
BOOL isSubCat;
BOOL fromSortFind;

@interface FindSortViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UITableView *tblGroupingNSorting;
    NSMutableArray *arrGroups;
    NSArray *arrSorting;
    int groupSelectionVal;
    int sortSelectionval;
    UIImageView *checkButton;
    UIView *view_picker;
    UIPickerView *dinningTypePickerView;
    UIToolbar *pickerToolbar;
    NSMutableArray *arrDinningTypes;
    NSString *typeStr;
    
}

@property (nonatomic, strong)NSString *typeStr,*catId;
@property(nonatomic,strong)NSMutableDictionary *OptionsDic;
@property(nonatomic,strong)NSMutableArray *selectedFilterValueIds,*selectedFilterId;
@property (nonatomic, strong)NSMutableArray *filterCount,*filterId;
@property(nonatomic,strong) NSString *srchKey;
@end

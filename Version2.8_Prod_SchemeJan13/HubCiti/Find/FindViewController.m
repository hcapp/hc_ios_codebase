//
//  FindViewController.m
//  Scansee
//
//  Created by Chaitra on 19/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "SpecialOffersViewController.h"
#import "FindViewController.h"
#import "MainMenuViewController.h"
#import "FindLocationServiceViewController.h"
//#import "SavingsAccount.h"
//#import "MoreViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "ProductPage.h"
#import <CoreLocation/CoreLocation.h>
#import "CLLocation (Strings).h"
#import "WebBrowserViewController.h"
#import "AppDelegate.h"
#import "SearchResultCount.h"
//#import "FindLocationViewController.h"
#import "HubCitiConstants.h"
#import "FindCategoryDO.h"
#import "GroupedProductList.h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "AnythingPage.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "AppDelegate.h"
#import "SingleCatRetailers.h"
//#import "ScanNowScreen.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FAQCategoryList.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "CityExperienceViewController.h"
#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "EventsListViewController.h"
#import "FindSortViewController.h"
#import "FilterListViewController.h"
#import "FundraiserListViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#include "HTTPClient.h"
#include "GetUserInfoResponse.h"
#include "Categories.h"
#include "CatList.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "BlockViewController.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponsInDealViewController.h"
NSDictionary *viewDictionary, *cellViewDictionary;

@implementation FindViewController{
    UIActivityIndicatorView *loading;
    NSString *smBkgrdColor;
    NSString *smBkgrdImage;
    NSString *mBkgrdColor;
    NSString *mBkgrdImage;
    bottomButtonView *view;
    CustomizedNavController *cusNav;
}
@synthesize findTitle,anyVC,infoResponse,emailSendingVC, catResponse,catListResponse,ssretdetail,ssretResponse,sscatdetail,sscatResponse;
//@synthesize findTitle,anyVC,emailSendingVC;
static int animate = 1;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
		//sortByLabel.text = @"";
    }
    return self;
}


#pragma mark - Service Respone methods



- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}





- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject

{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    
    
    
    [self responseData:responseObject];
    
    
    
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    [self.view setAccessibilityLabel:@"FindAll"];
    //self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    loading = nil;
    tappedSearch=FALSE;
    fetchingNextResults = NO;
    img.image = [UIImage imageNamed:@"searchGoogle"];
    
                        
    if (IOS7 == NO){
        search.tintColor = [UIColor blackColor];
        
    }
    
    for(id subview in [search subviews])
    {
        if ([subview isKindOfClass:[UIButton class]]) {
            
            cancelBtn = (UIButton*)subview;
            cancelBtn.tag = 100;
        }
    }
    

    smartSearch = NO;
    
   
    
    if ([defaults  boolForKey:@"findsplash"] == YES){
        //[self showSplash];
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = nil;
    }
    else{
        //self.navigationItem.hidesBackButton = NO;
        //customize back button
        
        
    }
    
    categoryTable=[[UITableView alloc]init];
    categoryTable.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [categoryTable setAccessibilityIdentifier:@"categoryTable"];
    categoryTable.tag = 1;
    categoryTable.hidden = YES;
    categoryTable.translatesAutoresizingMaskIntoConstraints = NO;
    if([[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
    categoryTable.backgroundColor=[UIColor convertToHexString:[defaults valueForKey:@"backgroundColor"]];
    else
        categoryTable.backgroundColor=[UIColor blueColor];

    view1.hidden = YES;
    
    googleSearch.text = @"";

        viewDictionary = NSDictionaryOfVariableBindings(search);
        // Category table constraints
        // Vertical constraints
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[search(44)]"] options:0 metrics:0 views:viewDictionary]];
        
        // Horizontal constraints
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[search(%f)]",SCREEN_WIDTH] options:0 metrics:0 views:viewDictionary]];
        
//    }
	[defaults  setBool:NO forKey:@"prodSearch"];
	
	previousValue = NO;
    
    findCategoryArray = [[NSMutableArray alloc]init];
    
    prodListIDArray = [[NSMutableArray alloc]init];
	productName = [[NSMutableArray alloc] init];
	productId = [[NSMutableArray alloc] init];
	productImageArray = [[NSMutableArray alloc] init];
    shortDescription = [[NSMutableArray alloc] init];
	rowCountArray = [[NSMutableArray alloc] init];
    prodCountArray = [[NSMutableArray alloc] init];
    


    

    self.navigationItem.hidesBackButton=YES;
	
	self.navigationController.navigationBar.hidden = NO;
	
 
  
    
    fromRetailerList = NO;
    locBtn.hidden = YES;
    smartSearch = NO;

   
    if ((([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) || ([[HubCitiManager sharedManager]gps_allow_flag] == NO)))
    {

        [UtilityManager showAlertOnWindow:nil msg:NSLocalizedString(@"\"Find\" uses your iPhone's Location Services or your zip code to find what's around you. Please either turn on Location Services and log in again or enter a zip code in the User Information page." , @"\"Find\" uses your iPhone's Location Services or your zip code to find what's around you. Please either turn on Location Services and log in again or enter a zip code in the User Information page.")];
        
        

       

    }
  
    //to configure back button
    
    BOOL checkFlag=true;
    
    
    
  
    if(checkFlag){
       // checkFlag=false;
        locBtn.hidden = NO;
        [self request_getcategory];
        
    }
    
    
}


-(void) viewWillAppear:(BOOL)animated{
    
	[super viewWillAppear:animated];
   
    //added by ashika
   
   cusNav =(CustomizedNavController *) self.navigationController;
    
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: NO];
    [cusNav hideBackButton:NO];
    
    
    if(![[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
         [cusNav setTitle:@"Find" forView:self withHambergur:YES];
    else
         [cusNav setTitle:findTitle forView:self withHambergur:YES];
    
   
    
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    if ([HubCitiAppDelegate locationServicesOn] && [HubCitiAppDelegate checkZipAndLocation] == 0) {
        locBtn.hidden = NO;
        [cusNav hideBackButton:NO];
        locBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [locBtn setBackgroundImage:[UIImage imageNamed:@"search_down.png"] forState:UIControlStateNormal];
        [locBtn setBackgroundImage:[UIImage imageNamed:@"search_up.png"] forState:UIControlStateHighlighted];
        locBtn.frame = CGRectMake(30, 0, 30, 30);
        [locBtn addTarget:self action:@selector(getNewLatLong) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* locBtnItem = [[UIBarButtonItem alloc] initWithCustomView:locBtn];

       
        //home button
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:CGRectMake(0, 0, 30, 30)];
        SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
        [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
        NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
        [btn addSubview:homeImage];
        
        
        [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
        mainPage.style = UIBarButtonItemStyleBordered;
        
         self.navigationItem.rightBarButtonItems = @[mainPage, locBtnItem];
        
        
 
        
        self.navigationItem.hidesBackButton=YES;
    }
    if ([HubCitiAppDelegate locationServicesOn]) {
        locBtn.hidden=NO;
    }
    else
    {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:CGRectMake(0, 0, 30, 30)];
        SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
        [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
        NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
        
        
        [btn addSubview:homeImage];
        
        
        [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
        mainPage.style = UIBarButtonItemStyleBordered;
        self.navigationItem.rightBarButtonItem = mainPage;
        
        
        if ([defaults valueForKey:KEYZIPCODE] /*&& navigatedFromMainMenu==FALSE*/)
        {
            
            cusNav.customNavBardelegate = self;
            [cusNav hideHambergerButton: NO];
            [cusNav hideBackButton:NO];
            [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
            self.navigationItem.hidesBackButton=YES;
        }
        
    }
    if (keyWord==NULL) {
        
        keyWord = [[NSString alloc]init];
    }
    else
    {
        //[keyWord release];
        keyWord = [[NSString alloc]init];
    }
    
    if (catRetSearchKey==NULL) {
        catRetSearchKey=[[NSString alloc]init];
    }
    else
    {
        //[catRetSearchKey release];
        catRetSearchKey=[[NSString alloc]init];
        
    }
//    NSLog(@"%@",tempString);
//    if([defaults boolForKey:@"PoppedBack"] && tempString.length!=0){
//       
//        if(!IPAD){
//            UITextField *textField;
//            for (UIView *subView in search.subviews)
//            {
//                for (UIView *ndLeveSubView in subView.subviews)
//                {
//                    if ([ndLeveSubView isKindOfClass:[UITextField class]])
//                    {
//                        textField = (UITextField *)ndLeveSubView;
//                        [textField setFont:[UIFont fontWithName:@"Helvetica" size:16]]; break;
//                    }
//                }
//            }
//        }
//        tempString=NULL;
//        [defaults setBool:NO forKey:@"PoppedBack"];
//    }
//
    
//    [defaults setValue:@"0" forKey:@"SubCategoryId"];
//    [defaults setValue:nil forKey:@"GroupFindBy"];
//    [defaults setValue:@"Distance" forKey:@"SortFindBy"];
//    [defaults setValue:nil forKey:@"SelectedCityIds"];
//    [defaults setValue:nil forKey:@"SelectedFilterId"];
//    [defaults setValue:nil forKey:@"SelectedFilterValueIds"];
//    [defaults setValue:nil forKeyPath:@"commaSeperatedFilterValueIds"];
//    [defaults setValue:nil forKeyPath:@"commaSeperatedFilterIds"];
//    [defaults setValue:nil forKey:@"afterFilterValueSelection"];
//    [defaults setValue:nil forKey:@"afterFilterIdSelection"];
    
    sortFilterObj = [[SortAndFilter alloc]init];
    sortFilterObj.distanceSelected = YES;
    
    isSubCat=FALSE;
    [search resignFirstResponder];
 
  
}

- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
-(void)setConstraints:(UITableViewCell*)cell tag:(int)tag heightIphone:(float)heigtIphone heightIpad:(float)heightIpad
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
   // if(![[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
//    {
//        
//        
//        
//        viewDictionary = NSDictionaryOfVariableBindings(categoryTable, viewSelectionControl);
//        
//        if(tag == 0)
//        {
//            
//            
//            // Category table constraints
//            if (  DEVICE_TYPE==UIUserInterfaceIdiomPad )
//            {
//                
//                // Vertical constraints
//                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(88)-[categoryTable(%f)]",heightIpad] options:0 metrics:0 views:viewDictionary]];
//                
//                // Horizontal constraints
//                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[categoryTable(770)]"] options:0 metrics:0 views:viewDictionary]];
//                
//                
//            }
//            else
//            {
//                // Vertical constraints
//                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(88)-[categoryTable(%f)]",heigtIphone] options:0 metrics:0 views:viewDictionary]];
//                
//                // Horizontal constraints
//                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[categoryTable(323)]"] options:0 metrics:0 views:viewDictionary]];
//                
//                
//            }
//        }
//        else if(tag == 1)
//        {
//            // Segment control constraints
//            if (  DEVICE_TYPE==UIUserInterfaceIdiomPad )
//            {
//                // Vertical constrains
//                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[viewSelectionControl(44)]"] options:0 metrics:0 views:viewDictionary]];
//                
//                
//                // Horizontal constraints
//                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(-7)-[viewSelectionControl(792)]"] options:0 metrics:0 views:viewDictionary]];
//                
//            }
//            else
//            {
//                // Vertical constrains
//                
//                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[viewSelectionControl(44)]"] options:0 metrics:0 views:viewDictionary]];
//                
//                // Horizontal constraints
//                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(-7)-[viewSelectionControl(334)]"] options:0 metrics:0 views:viewDictionary]];
//                
//            }
//        }
//        else
//        {
//            // PickerView row constraints
//            if (  DEVICE_TYPE==UIUserInterfaceIdiomPad )
//            {
//                // Vertical constraints
//                [cell.contentView  addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(40)]"] options:0 metrics:0 views:viewDictionary]];
//                
//                // Horizontal constraints
//                [cell.contentView  addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[label(%f)]",SCREEN_WIDTH-20] options:0 metrics:0 views:viewDictionary]];
//            }
//            else
//            {
//                // Vertical constraints
//                [cell.contentView  addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(40)]"] options:0 metrics:0 views:viewDictionary]];
//                
//                // Horizontal constraints
//                [cell.contentView  addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[label(%f)]",SCREEN_WIDTH-20] options:0 metrics:0 views:viewDictionary]];
//            }
//        }
//
//       }
   // else
    {
        viewDictionary = NSDictionaryOfVariableBindings(categoryTable);
        
        if(tag == 0)
        {
            
            
            // Category table constraints
            if (  DEVICE_TYPE==UIUserInterfaceIdiomPad )
            {
                
                // Vertical constraints
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(44)-[categoryTable(%f)]",heightIpad] options:0 metrics:0 views:viewDictionary]];
                
                // Horizontal constraints
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[categoryTable(770)]"] options:0 metrics:0 views:viewDictionary]];
                
                
            }
            else
            {
                // Vertical constraints
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(44)-[categoryTable(%f)]",heigtIphone] options:0 metrics:0 views:viewDictionary]];
                
                // Horizontal constraints
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[categoryTable(323)]"] options:0 metrics:0 views:viewDictionary]];
                
                
            }
        }
    }

}

//Added for Location Update
-(void)getNewLatLong
{
    UIAlertController * alert;
    
    alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Refresh Your Location?",@"Refresh Your Location?") message:NSLocalizedString(@"Do you want to refresh your location?",@"Do you want to refresh your location?") preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* cancel = [UIAlertAction
                         actionWithTitle:@"Cancel"
                         style:UIAlertActionStyleCancel
                         handler:nil];
    [alert addAction:cancel];
    UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action){
                             
                             [defaults setValue:@"" forKey:KEY_LATITUDE];
                             [defaults setValue:@"" forKey:KEY_LONGITUDE];
                             
                             [Location updateLocationinfo:self];
                             
                         }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }

    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;

        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
    [self.navigationController popToRootViewControllerAnimated:NO];
    }
}



-(void) updatedLatlongValue :(NSString *) latValue longitudeValue:(NSString *)longValue; {
	
    [defaults setObject:latValue forKey:KEY_LATITUDE];
	[defaults setObject:longValue forKey:KEY_LONGITUDE];
    
    //Added the setting for these keys because its what the Map is using.
    [defaults  setValue:latValue forKey:@"LatVal"];
    [defaults  setValue:longValue forKey:@"LongVal"];
}

-(void)viewSelectionChanged:(id)sender
{
    search.text = @"";
    googleSearch.text = @"";
    cancelBtn.tag = 101;
    [search resignFirstResponder];
    [googleSearch resignFirstResponder];
    
    [defaults setBool:NO forKey:@"ReturnResult"];
    
	switch ([viewSelectionControl selectedSegmentIndex])
    {
		case 1:
		{
            search.placeholder = @"Search Product";
            iWebRequestState=GETSMARTSEAPRODS;
            locBtn.hidden = YES;
            view1.hidden = YES;
			previousValue = NO;
			categoryTable.hidden = YES;
            categoryTable.tag = 2;
            
            if(bottomBtn==1){
                if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
                    [UIView animateWithDuration:0.25 animations:^{
                        categoryTableHeightConstraint.constant = 820;//760
                        [self.view setNeedsUpdateConstraints];
                    }];
                else if (IS_IPHONE5){
                    [UIView animateWithDuration:0.25 animations:^{
                        categoryTableHeightConstraint.constant = 240+88-50;
                        [self.view setNeedsUpdateConstraints];
                    }];
                }
                else
                   [UIView animateWithDuration:0.25 animations:^{
                        categoryTableHeightConstraint.constant = 240-50;
                        [self.view setNeedsUpdateConstraints];
                    }];
            }
            else{
                if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
                    [UIView animateWithDuration:0.25 animations:^{
                        categoryTableHeightConstraint.constant = 820;//760
                        [self.view setNeedsUpdateConstraints];
                    }];
                else if (IS_IPHONE5){
                    [UIView animateWithDuration:0.25 animations:^{
                        categoryTableHeightConstraint.constant = 240+88;
                        [self.view setNeedsUpdateConstraints];
                    }];
                }
                else
                    [UIView animateWithDuration:0.25 animations:^{
                        categoryTableHeightConstraint.constant = 240;
                        [self.view setNeedsUpdateConstraints];
                    }];
            }
		}
			break;
            
		case 0:
            iWebRequestState=GETCATEGORY;
            locBtn.hidden = NO;
            smartSearch = NO;
            
            // If the user has declined to use LS and Zip and wants to Find by Location
            if ((([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) || ([[HubCitiManager sharedManager]gps_allow_flag] == NO)) && ![defaults valueForKey:KEYZIPCODE])
            {
                
                locBtn.hidden = YES;
                viewSelectionControl.selectedSegmentIndex = 0;
                search.placeholder = @"Search Product";
                view1.hidden = YES;
                previousValue = NO;
                categoryTable.hidden = YES;
                categoryTable.tag = 1;
                [self request_getcategory];
                [categoryTable reloadData];
                 [UtilityManager showAlertOnWindow:nil msg:NSLocalizedString(@"\"Find\" uses your iPhone's Location Services or your zip code to find what's around you. Please either turn on Location Services and log in again or enter a zip code in the User Information page." , @"\"Find\" uses your iPhone's Location Services or your zip code to find what's around you. Please either turn on Location Services and log in again or enter a zip code in the User Information page.")];
                break;
            }
            else {
                
                view1.hidden = YES;
                googleSearch.text = @"";
                if (previousValue == NO) {
                    previousValue = YES;
                    [defaults  setBool:NO forKey:@"prodSearch"];
                    [search resignFirstResponder];
                    search.text =@"";
                    categoryTable.hidden = NO;
                    categoryTable.tag = 1;
                    
                    if(bottomBtn==1){
                        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
                            [UIView animateWithDuration:0.25 animations:^{
                                categoryTableHeightConstraint.constant = 820;//760
                                [self.view setNeedsUpdateConstraints];
                            }];
                       else if (IS_IPHONE5){
                           [UIView animateWithDuration:0.25 animations:^{
                               categoryTableHeightConstraint.constant = 278+88-50;
                               [self.view setNeedsUpdateConstraints];
                           }];
                        }
                        else
                                [UIView animateWithDuration:0.25 animations:^{
                                categoryTableHeightConstraint.constant = 278-50;
                                [self.view setNeedsUpdateConstraints];
                            }];
                    }
                    else{
                        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
                            [UIView animateWithDuration:0.25 animations:^{
                                categoryTableHeightConstraint.constant = 820;//790
                                [self.view setNeedsUpdateConstraints];
                            }];
                    else if (IS_IPHONE5) {
                        [UIView animateWithDuration:0.25 animations:^{
                            categoryTableHeightConstraint.constant = 278+88;
                            [self.view setNeedsUpdateConstraints];
                        }];
                    }
                    else
                        [UIView animateWithDuration:0.25 animations:^{
                            categoryTableHeightConstraint.constant = 278;
                            [self.view setNeedsUpdateConstraints];
                        }];
                    }
    
                    [self request_getcategory];
                    [categoryTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    [categoryTable reloadData];
                }
                else {
                    
                    search.placeholder = @"Search";
                  
                    
                    categoryTable.tag = 1;
                    [categoryTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    [categoryTable reloadData];
                }
                
                break;
                
            }
            
			return;
			break;
		default:
			break;
	}
}

-(void)setViewandTable
{
    categoryTable.multipleTouchEnabled = NO;
    categoryTable.hidden = NO;
    categoryTable.dataSource=self;
    categoryTable.delegate=self;
    [categoryTable setBackgroundColor:[UIColor whiteColor]];
      categoryTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    categoryTable.tableFooterView.hidden=YES;
    [self.view addSubview:categoryTable];
    
//    if(![[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"]){
//        
//        if(bottomBtn==1){
//            
//            if (IS_IPHONE5){
//                [self setConstraints:nil tag:0 heightIphone:278+88 heightIpad:820];//790
//            }
//            else
//                [self setConstraints:nil tag:0 heightIphone:278 heightIpad:820];//790
//        }
//        else{
//            if (IS_IPHONE5){
//                [self setConstraints:nil tag:0 heightIphone:278+88+50 heightIpad:820];//790
//            }
//            else
//                [self setConstraints:nil tag:0 heightIphone:278+50 heightIpad:820+50];//790
//        }
//        
//    }// changed by ashika
  //  else
    {
        if(bottomBtn==1){
        
        if (IS_IPHONE5){
            [self setConstraints:nil tag:0 heightIphone:278+88+44 heightIpad:820+48];//790
        }
        else
            [self setConstraints:nil tag:0 heightIphone:278+44 heightIpad:820+50];//790
    }
    else{
        if (IS_IPHONE5){
            [self setConstraints:nil tag:0 heightIphone:278+88+50+44 heightIpad:820+48];//790
        }
        else
            [self setConstraints:nil tag:0 heightIphone:278+50+44 heightIpad:820+48+50];//790
    }
        
    }
    
    // added to resize tableview depending on keyboard hide/show
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fitTableView) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeCategoryTableView) name:UIKeyboardDidHideNotification object:nil];
    
    
}



-(void)bottomButtonPressed:(id)sender
{
    [defaults setBool:NO forKey:@"ViewMore"];
    [defaults setBool:YES forKey:BottomButton];
//   [defaults setValue:nil forKey:KEY_MAINMENUID];
//    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
//    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    UIButton *btn = (UIButton*)sender;
    int tag =(int) btn.tag;
    
    bottomButtonDO *obj_findBottomDO = [arrFindBottomButtonDO objectAtIndex:tag];
    
    [defaults setValue:obj_findBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_findBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_findBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_findBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_findBottomDO.btnLinkID forKey:KEY_LINKID];
        [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_findBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_findBottomDO.btnLinkTypeName);
    if ([obj_findBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_findBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];

            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    else if ([obj_findBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_findBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
               // [SingleEventCatID addObject:obj_findBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_findBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
               // [SingleEventCatID addObject:obj_findBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_findBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
             self.iEventsListViewController = iEventsListViewController;
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    else{ //other menu types
    switch ([[obj_findBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_findBottomDO.btnLinkTypeName]intValue]) {
            
        case 0://Hot Deals
        {
            [defaults  setBool:YES forKey:@"HotDealsplash"];
            DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
            self.hotDeals = hotDeals;
            [self.navigationController pushViewController:hotDeals animated:NO];
        }
            //[hotDeals release];
            break;
            
        case 1://Scan Now
            //[self navigateToScanNow];
            break;
            
        case 2:{//Alerts
            AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
            [self.navigationController pushViewController:alert animated:NO];
            //[alert release];
        }
            break;
            
        case 3://Events
        {
            if (MultipleEventCatID==Nil) {
                MultipleEventCatID=[[NSMutableArray alloc]init];
                MultipleCatFlag = TRUE;
                [MultipleEventCatID addObject:obj_findBottomDO.bottomBtnID];
            }
            else
            {
                [MultipleEventCatID addObject:obj_findBottomDO.bottomBtnID];
            }
            [EventsBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];

                [self navigateToEventList];
        }
            break;
        case 4://Whats NearBy
        {
            popFromNearBy=TRUE;
            [NearbyBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];

            [self navigateToWhatsNearBy];
        }
            break;
            
        case 5://Find
        {
            [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
             if([[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
             [self navigateToFindView];
            if([defaults valueForKey:@"findFlag"]){
                [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
            }
        }
            break;
            
        case 6://City Experience
        {
            [defaults setInteger:100 forKey:@"CityExpTableTag"];
            [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];

            CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
            self.cityExp = citi;
            [self.navigationController pushViewController:citi animated:NO];
        }
            break;
            
        case 7://City Services
            
            break;
            
        case 8://Visitors Bureau
            
            break;
            
        case 9://transportation
            
            break;
            
        case 10://Preference
        {
            if (userIdentifier==TRUE) {
                [NewsUtility signupPopUp:self];
            }
            else
            {
                [self requestToGetPreferredLocations];}
        }
            break;
            
        case 11:{//About
            AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
            aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
            [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
            aboutScreen.comingFromPrivacyScreen = NO;
            [self.navigationController pushViewController:aboutScreen animated:NO];
            //[aboutScreen release];
        }
            break;
            
        case 12://Share
        {
            if (userIdentifier==TRUE) {
                [NewsUtility signupPopUp:self];
                [defaults setBool:NO forKey:BottomButton];
            }
            else
            {[self shareClicked:nil];}
        }
            break;
            
        case 13:
        {
            if (userIdentifier==TRUE) {
                [NewsUtility signupPopUp:self];
                [defaults setBool:NO forKey:BottomButton];
            }
            else
            {//settings
                SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:svc animated:NO];
                //[svc release];
            }
        }
            break;
        case 14:{//AnythingPage
            requestAnythingFromMainMenu=TRUE;
            [UtilityManager req_hubcitiAnyThingPage:self];
            break;
        }
        case 15:{//AppSite
            [self request_appsitedetails];
            break;
        }
        case 16: // SubMenu
        {
            //Added for navigation between menu levels
            NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
            [array addObject:[NSString stringWithFormat:@"%@",obj_findBottomDO.btnLinkID]];
            [HubCitiAppDelegate refreshLinkIdArray];
            [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
            
            NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
            [array1 addObject:@"0"];
            [HubCitiAppDelegate refreshDeptIdArray];
            [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
            
            NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
            [array2 addObject:@"0"];
            [HubCitiAppDelegate refreshTypeIdArray];
            [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
            
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
            [defaults setValue:levelString forKey:KEY_MENULEVEL];
            
            
            [self navigateToSubMenu];
            break;
        }
        case 17: // Filters
        {
            popFromCity=YES;
            if ([SharedManager filtersCount] == 0){
                
                [UtilityManager showAlert:nil msg:@"No Records Found"];
            }
            
            else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                
                CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                self.cityExp = cevc;
                [SharedManager setRefreshAustinRetailers:YES];
                //[cevc release];

                
                FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                self.filters = filters;
                // [defaults setValue:retAffName forKey:@"Title"];
                [self.navigationController pushViewController:filters animated:NO];
                //[filters release];
            }
            
            else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                
                FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                
                // [defaults setValue:retAffName forKey:@"Title"];
                [self.navigationController pushViewController:filterList animated:NO];
                //[filterList release];
                
                //[self request_GetPartners];
            }
            
        }
            break;
            
        case 18: // Coupon
        {
            [self navigateToCouponsViewController];
            break;
        }
        case 19: // Deals
        {
            CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:dvc animated:NO];
            //[dvc release];
            
            break;
        }

            
        case 20:
        {
            
            FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:faqCatList animated:NO];
            self.faqList = faqCatList;
            //[faqCatList release];
            
        }
            break;
            
        case 21:
            
        {
            [FundraiserBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];

            FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
            self.iFundraiserListViewController = iFundraiserListViewController;
            [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
            //[iFundraiserListViewController release];
            
        }break;
        case 22:
            
        {
            if (userIdentifier==TRUE) {
                [NewsUtility signupPopUp:self];
                [defaults setBool:NO forKey:BottomButton];
            }
            else{

            iWebRequestState = GETUSERINFOM;
            [self requestToGetUserData];
            }
        }break;
        case 23:
            
        {
            if (userIdentifier==TRUE) {
                [NewsUtility signupPopUp:self];
                [defaults setBool:NO forKey:BottomButton];
            }
            else{

            iWebRequestState = GETFAVLOCATION;
            [self requestToGetPreferredLocations];
            }
            
        }break;
            
        case 24:
            
        {
            if (userIdentifier==TRUE) {
                [NewsUtility signupPopUp:self];
                [defaults setBool:NO forKey:BottomButton];
            }
            else{

            if([Network currentReachabilityStatus]==0)
            {
                [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
            }
            else{
                
                UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:inform animated:NO];
                //[inform release];
            }
            
            }
            
        }break;
        case 25:
            
        {
            if ([RegionApp isEqualToString:@"1"]) {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{

                if([Network currentReachabilityStatus]==0)
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                }
                else{
                    [SharedManager setUserInfoFromSignup:NO];
                    CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                    [self.navigationController pushViewController:citPref animated:NO];
                   // [citPref release];
                }
            }
            }
            
        }break;
        case 29:
        {
            CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:dvc animated:NO];
        }
            break;

        case 30:
        {
            AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
            privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
           // privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
            [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
            privacy.comingFromPrivacyScreen = YES;
            [self.navigationController pushViewController:privacy animated:NO];
        }
            break;
        default:
            break;
    }
    }
    //NSlog(@"%d",tag);
}


// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    // Calculate Y position for View
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-bottomBarButtonHeight - 44 - 20, SCREEN_WIDTH, bottomBarButtonHeight)];//-yVal
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [arrFindBottomButtonDO count]; btnLoop++)
    {
        bottomButtonDO *obj_findBottomDO = [arrFindBottomButtonDO objectAtIndex:btnLoop];
        
        view = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_findBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, (SCREEN_HEIGHT-bottomBarButtonHeight - 44 -20) , 80,bottomBarButtonHeight) imageName:obj_findBottomDO.bottomBtnImg img_off:obj_findBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];//-yVal
            
        }
        else
        view = [bottomButtonView initWithTitle:obj_findBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrFindBottomButtonDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrFindBottomButtonDO count],bottomBarButtonHeight) imageName:obj_findBottomDO.bottomBtnImg img_off:obj_findBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_findBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
    }
   
}







#pragma Request and Parse for GetCategory
-(void)request_getcategory
{
    [findCategoryArray removeAllObjects];
    
    [HubCitiAppDelegate showActivityIndicator];
    
    iWebRequestState = GETCATEGORY;
    
    //    NSMutableString *requestStr = [[NSMutableString alloc] init];
    //    [requestStr appendFormat:@"<MenuItem><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    //
    //
    //    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length] && ([[HubCitiManager sharedManager]gps_allow_flag] == YES) &&([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorized))
    //    {
    //        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
    //         [defaults  valueForKey:KEY_LATITUDE]];
    //    }
    //
    //    if([defaults valueForKey:KEY_MAINMENUID])
    //        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    //    if ([FindBottomButtonID count]>=1) {
    //
    //            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
    //
    //    }
    //    else if([defaults valueForKey:KEY_MITEMID])
    //        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    //
    //    else
    //    {
    //                    [requestStr appendFormat:@"<bottomBtnId>0</bottomBtnId><platform>IOS</platform>"];
    //    }
    //    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    //    if(menulevel > 1)
    //    {
    //     [requestStr appendFormat:@"<levelFlag>true</levelFlag>"];
    //    }
    //    else{
    //        [requestStr appendFormat:@"<levelFlag>false</levelFlag>"];
    //    }
    //
    //    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></MenuItem>",[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@find/getcategory",BASE_URL];
    //
    //    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    //[requestStr release];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
//    if([defaults valueForKey:KEY_USERID])
//    {
//        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
//    }
    
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length] && ([[HubCitiManager sharedManager]gps_allow_flag] == YES) &&([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse))
    {
        [parameters setValue:[defaults  valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        [parameters setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        if ([[defaults valueForKey:KEYZIPCODE] length]){
            [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"userPostalCode"];
        }
    }
    else
    {
        if ([[defaults valueForKey:KEYZIPCODE] length]){
            [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"postalCode"];
        }
    }
    if([defaults valueForKey:KEY_MAINMENUID])
    {
        [parameters setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    }
    
    if ([FindBottomButtonID count]>=1)
    {
        [parameters setValue:[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1] forKey:@"bottomBtnId"];
        [parameters setValue:@"IOS" forKey:@"platform"];
    }
//    else if([defaults valueForKey:KEY_MITEMID])
//    {
//        
//        [parameters setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
//        [parameters setValue:@"IOS" forKey:@"platform"];
//    }
    else if([defaults valueForKey:@"findCatMitemid"]){
        [parameters setValue:[defaults valueForKey:@"findCatMitemid"] forKey:@"mItemId"];
        [parameters setValue:@"IOS" forKey:@"platform"];
    }
    else
    {
        [parameters setValue:@"0" forKey:@"bottomBtnId"];
        [parameters setValue:@"IOS" forKey:@"platform"];
        
    }
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
    {
        [parameters setValue:@"true" forKey:@"levelFlag"];
    }
    else
    {
        [parameters setValue:@"false" forKey:@"levelFlag"];
    }
    
    if([defaults valueForKey:KEY_HUBCITIID] )
    {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
//    if([[defaults  valueForKey:KEYZIPCODE]length])
//    {
//        [parameters setValue:[defaults  valueForKey:KEYZIPCODE] forKey:@"postalCode"];
//    }
    if([RegionApp isEqualToString:@"1"] && [defaults valueForKey:@"sortedCitiIDs"])
    {
        [parameters setValue:[defaults valueForKey:@"sortedCitiIDs"] forKey:@"cityIds"];
        
    }
    if([defaults valueForKey:@"FindRadius"])
    {
        [parameters setValue:[defaults valueForKey:@"FindRadius"] forKey:@"radius"];
        
    }
    else
    {
        [parameters setValue:@"50" forKey:@"radius"];
        
    }
    NSLog(@"parameter: %@",parameters);
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    //NSString *urlString = [NSString stringWithFormat:@"http://sdw2107:9990/HubCiti2.4.2/find/getcategoryjson"];
    NSString *urlString = [NSString stringWithFormat:@"%@find/getcategoryjson",BASE_URL];
    DLog(@"Url: %@",urlString);
    NSLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
}


// Request to get the Category Details when tab on Any Category from tableView
-(void)request_sscatsearch
{
    iWebRequestState = SSCATSEARCH;
    
    [HubCitiAppDelegate showActivityIndicator];
    CatList *obj_FindCategoryDO = [findCategoryArray objectAtIndex:index.row];
    [defaults setValue:obj_FindCategoryDO.CatID forKey:@"categId"];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
//    if([defaults valueForKey:KEY_USERID])
//    {
//        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
//    }
    if(obj_FindCategoryDO.CatDisName)
    {
        [parameters setValue:obj_FindCategoryDO.CatDisName forKey:@"catName"];
    }
    [parameters setValue:0 forKey:@"lastVisitedNo"];
    
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length])
    {
        [parameters setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        [parameters setValue:[defaults valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        if ([[defaults valueForKey:KEYZIPCODE] length]){
            [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"userPostalCode"];
        }
    }
    else
    {
        if ([[defaults valueForKey:KEYZIPCODE] length]){
            [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"postalCode"];
        }
    }
    if([defaults valueForKey:KEY_MAINMENUID])
    {
        [parameters setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    }
    if (![defaults boolForKey:BottomButton])
    {
        if([defaults valueForKey:@"findCatMitemid"]){
            [parameters setValue:[defaults valueForKey:@"findCatMitemid"] forKey:@"mItemId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
        }
        //        if([defaults valueForKey:KEY_MITEMID])
        //        {
        //            [parameters setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
        //            [parameters setValue:@"IOS" forKey:@"platform"];
        //        }
        else if ([FindBottomButtonID count]>=1)
        {
            [parameters setValue:[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1] forKey:@"bottomBtnId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
            
        }
    }
    else
    {
        if([defaults valueForKey:@"findCatMitemid"] && [[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"]){
            [parameters setValue:[defaults valueForKey:@"findCatMitemid"] forKey:@"mItemId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
        }
        //        if ([defaults valueForKey:KEY_MITEMID]&& [[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
        //        {
        //            [parameters setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
        //            [parameters setValue:@"IOS" forKey:@"platform"];
        //        }
        [defaults setBool:YES forKey:BottomButton];
        if ([FindBottomButtonID count]>=1)
        {
            [parameters setValue:[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1] forKey:@"bottomBtnId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
        }
    }
    [parameters setValue:@"ASC" forKey:@"sortOrder"];
    
        [parameters setValue:@"distance" forKey:@"sortColumn"];
    
//    if (sortFilterObj.alphabeticallySelected)
//    {
//        [parameters setValue:@"atoz" forKey:@"sortColumn"];
//    }
    if (sortFilterObj.selectedCitiIds)
    {
        [parameters setValue:[defaults valueForKey:@"commaSeperatedCities"] forKey:@"cityIds"];
    }
    if(isSubCat)
    {
        [parameters setValue:[defaults valueForKey:@"SubCategoryId"] forKey:@"subCatIds"];
    }
    if (sortFilterObj.localSpecialSelected)
    {
        [parameters setValue:@"1" forKey:@"locSpecials"];
    }
    else
    {
        [parameters setValue:@"0" forKey:@"locSpecials"];
    }
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    if([RegionApp isEqualToString:@"1"] && [defaults valueForKey:@"sortedCitiIDs"])
    {
        [parameters setValue:[defaults valueForKey:@"sortedCitiIDs"] forKey:@"cityIds"];
        
    }
    if([defaults valueForKey:@"FindRadius"])
    {
        [parameters setValue:[defaults valueForKey:@"FindRadius"] forKey:@"radius"];
        
    }
    else
    {
        [parameters setValue:@"50" forKey:@"radius"];
        
    }
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [timeFormatter setTimeZone:timeZone];
    [timeFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *timeInUTC = [timeFormatter stringFromDate:currentTime];
    NSLog(@"Current time %@", timeInUTC);
    
    [parameters setValue:timeInUTC forKey:@"requestedTime"];
    DLog(@"parameter: %@",parameters);
    NSLog(@"parameter: %@",parameters);
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    //NSString *urlString = [NSString stringWithFormat:@"http://sdw2107:9990/HubCiti2.4.2/find/sscatsearchjson"];
    NSString *urlString = [NSString stringWithFormat:@"%@find/sscatsearchjson",BASE_URL];
    DLog(@"Url: %@",urlString);
    NSLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    //    NSMutableString *requestStr = [[NSMutableString alloc] init];
    //    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    //    [requestStr appendFormat:@"<catName>%@</catName><lastVisitedNo>0</lastVisitedNo>",obj_FindCategoryDO.CatDisName];
    //
    //    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length]){
    //
    //        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
    //         [defaults  valueForKey:KEY_LATITUDE]];
    //    }
    //
    //    //for user tracking
    //    if([defaults valueForKey:KEY_MAINMENUID]){
    //        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    //    }
    //
    //
    //    if (![defaults boolForKey:BottomButton]) {
    //        //        if (userIdentifier==TRUE) {
    //        //            //[defaults setBool:YES forKey:BottomButton];
    //        //            [requestStr appendFormat:@"<bottomBtnId>0</bottomBtnId><platform>IOS</platform>"];
    //        //        }
    //        if([defaults valueForKey:KEY_MITEMID]){
    //            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    //        }
    //        else if ([FindBottomButtonID count]>=1) {
    //
    //            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
    //
    //        }
    //
    //    }
    //    else{
    //        if ([defaults valueForKey:KEY_MITEMID]&& [[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
    //        {
    //            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    //        }
    //        [defaults setBool:YES forKey:BottomButton];
    //        if ([FindBottomButtonID count]>=1) {
    //        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
    //        }
    //    }
    //    [requestStr appendFormat:@"<sortOrder>ASC</sortOrder>"];
    //
    //    if (sortFilterObj.distanceSelected) {
    //        [requestStr appendFormat:@"<sortColumn>distance</sortColumn>"];
    //    }
    //
    //    if (sortFilterObj.alphabeticallySelected) {
    //        [requestStr appendFormat:@"<sortColumn>atoz</sortColumn>"];
    //    }
    //
    //
    //    if (sortFilterObj.selectedCitiIds) {
    //        [requestStr appendFormat:@"<cityIds>%@</cityIds>",[defaults valueForKey:@"commaSeperatedCities"]];
    //    }
    //
    ////    if ([defaults valueForKey:@"GroupFindBy"]) {
    ////        [requestStr appendFormat:@"<groupBy>%@</groupBy>",[defaults valueForKey:@"GroupFindBy"]];
    ////    }
    //    if(isSubCat)
    //    [requestStr appendFormat:@"<subCatIds>%@</subCatIds>",[defaults valueForKey:@"SubCategoryId"]];
    //
    //
    //    if (sortFilterObj.localSpecialSelected) {
    //        [requestStr appendFormat:@"<locSpecials>1</locSpecials>"];
    //    }
    //    else{
    //        [requestStr appendFormat:@"<locSpecials>0</locSpecials>"];
    //    }
    //
    //    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    //    
    //    
    //    NSString *urlString = [NSString stringWithFormat:@"%@find/sscatsearch",BASE_URL];
    //    
    //    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    //    
    //    //[requestStr release];
    
}


-(void)request_fetchlatlong
{
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendFormat:@"thislocation/fetchlatlong?zipcode=%@",[defaults  valueForKey:@"ZipCode"]];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
    {
        ReleaseAndNilify(responseXml);
        return;
    }
    
    //NSlog(@"RESPONSE - %@",responseXml);
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement *responseElement = [TBXML childElementNamed:@"responsCode" parentElement:tbxml.rootXMLElement];
    if (responseElement == nil) {
        TBXMLElement *latElement = [TBXML childElementNamed:@"Latitude" parentElement:tbxml.rootXMLElement];
        TBXMLElement *longElement = [TBXML childElementNamed:@"Longitude" parentElement:tbxml.rootXMLElement];
        if (latElement != nil && longElement != nil)
        {
            [defaults setValue:[TBXML textForElement:latElement] forKey:KEY_LATITUDE];
            [defaults setValue:[TBXML textForElement:longElement] forKey:KEY_LONGITUDE];
            
            //if (fromRetailerList == NO)
              //  [self stopUpdatingLocation:NSLocalizedString(@"UsingZip", @"UsingZip")];
        }
        
        
    }
    else {
        [UtilityManager showAlert:@"Error" msg:[TBXML textForElement:responseElement]];
    }
    //[responseXml release];
    
}

-(void)request_ssretsearch:(NSString*)serachKey
{
    [HubCitiAppDelegate showActivityIndicator];
    BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    // If GPS Not allowed but ZIP code is available
    if(lsFlag == NO && zipCodeStatus==1)
    {
        //        [self request_fetchlatlong];
    }
    
    retSearchText = [serachKey copy];
    keyWord=retSearchText;
    iWebRequestState = SSRETSEARCH;
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if([defaults valueForKey:KEY_USERID])
    {
        [parameters setObject:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if(keyWord)
    {
        [parameters setObject:keyWord forKey:@"searchKey"];
    }
    
    [parameters setObject:@"0" forKey:@"lastVisitedNo"];
    
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length])
    {
        [parameters setObject:[defaults  valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        [parameters setObject:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
    }
    else
    {
        if ([defaults valueForKeyPath:KEYZIPCODE])
        {
            [parameters setObject:[defaults valueForKeyPath:KEYZIPCODE] forKey:@"postalCode"];
        }
    }
    
    if([defaults valueForKey:KEY_MAINMENUID])
    {
        [parameters setObject:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    }
    if (![defaults boolForKey:BottomButton])
    {
        if([defaults valueForKey:@"findCatMitemid"])
        {
            [parameters setObject:[defaults valueForKey:@"findCatMitemid"] forKey:@"mItemId"];
            [parameters setObject:@"IOS" forKey:@"platform"];
        }
        //        if([defaults valueForKey:KEY_MITEMID])
        //        {
        //            [parameters setObject:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
        //[parameters setObject:@"IOS" forKey:@"platform"];
        //        }
        else if ([FindBottomButtonID count]>=1)
        {
            [parameters setObject:[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1] forKey:@"bottomBtnId"];
            [parameters setObject:@"IOS" forKey:@"platform"];
        }
    }
    else
    {
        if([defaults valueForKey:@"findCatMitemid"]&&[[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
        {
            [parameters setObject:[defaults valueForKey:@"findCatMitemid"] forKey:@"mItemId"];
            [parameters setObject:@"IOS" forKey:@"platform"];
        }
        //        if([defaults valueForKey:KEY_MITEMID]&& [[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
        //        {
        //            [parameters setObject:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
        //            [parameters setObject:@"IOS" forKey:@"platform"];
        //        }
        [defaults setBool:YES forKey:BottomButton];
        if([FindBottomButtonID count]>=1)
        {
            [parameters setObject:[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1] forKey:@"bottomBtnId"];
            [parameters setObject:@"IOS" forKey:@"platform"];
        }
    }
    [parameters setObject:@"ASC" forKey:@"sortOrder"];
    [parameters setObject:@"distance" forKey:@"sortColumn"];
    if([defaults valueForKey:KEY_HUBCITIID])
    {
        [parameters setObject:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    NSDate *localDate =[[NSDate alloc] init];
    NSDateFormatter *dateFormatterNew = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [dateFormatterNew setTimeZone:timeZone];
    [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatterNew stringFromDate:localDate];
    NSLog(@"date:%@",dateString);
    
    [parameters setValue:dateString forKey:@"requestedTime"];
//    if([RegionApp isEqualToString:@"1"] && [defaults valueForKey:@"sortedCitiIDs"])
//    {
//        [parameters setValue:[defaults valueForKey:@"sortedCitiIDs"] forKey:@"cityIds"];
//        
//    }
//    if([defaults valueForKey:@"FindRadius"])
//    {
//        [parameters setValue:[defaults valueForKey:@"FindRadius"] forKey:@"radius"];
//        
//    }
//    else
//    {
//        [parameters setValue:@"50" forKey:@"radius"];
//        
//    }
    DLog(@"parameter: %@",parameters);
    NSLog(@"parameter: %@",parameters);
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    //NSString *urlString = [NSString stringWithFormat:@"http://sdw2107:9990/HubCiti2.4.2/find/ssretsearchjson"];
    NSString *urlString = [NSString stringWithFormat:@"%@find/ssretsearchjson",BASE_URL];
    DLog(@"Url: %@",urlString);
    NSLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    
    
    //    NSMutableString *requestStr = [[NSMutableString alloc] init];
    //    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    //
    //         [requestStr appendFormat:@"<searchKey>%@</searchKey>",keyWord];
    //
    //    [requestStr appendFormat:@"<lastVisitedNo>%@</lastVisitedNo>",@"0"];
    //    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length]){
    //
    //        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
    //         [defaults  valueForKey:KEY_LATITUDE]];
    //
    //    }
    //    else
    //    {
    //        if ([defaults valueForKeyPath:KEYZIPCODE]) {
    //            [requestStr appendFormat:@"<postalCode>%@</postalCode>",[defaults valueForKeyPath:KEYZIPCODE]];
    //        }
    //    }
    //
    //    if([defaults valueForKey:KEY_MAINMENUID])
    //        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    //
    ////    else if([defaults valueForKey:KEY_MITEMID])
    ////        [requestStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];
    ////
    ////    else if([defaults valueForKey:KEY_BOTTOMBUTTONID])
    ////        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    //
    //    //SHould be used when new find search is implemented from DB end.
    //
    //    if (![defaults boolForKey:BottomButton]) {
    //        //        if (userIdentifier==TRUE) {
    //        //            //[defaults setBool:YES forKey:BottomButton];
    //        //            [requestStr appendFormat:@"<bottomBtnId>0</bottomBtnId><platform>IOS</platform>"];
    //        //        }
    //        if([defaults valueForKey:KEY_MITEMID]){
    //            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    //        }
    //        else if ([FindBottomButtonID count]>=1) {
    //
    //            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
    //
    //        }
    //
    //    }
    //    else{
    //        if ([defaults valueForKey:KEY_MITEMID]&& [[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
    //        {
    //            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    //        }
    //        [defaults setBool:YES forKey:BottomButton];
    //        if ([FindBottomButtonID count]>=1) {
    //            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
    //        }
    //    }
    //
    //    [requestStr appendFormat:@"<sortOrder>ASC</sortOrder><sortColumn>distance</sortColumn>"];
    //
    ////    if ([[defaults valueForKey:@"SortFindBy"] isEqualToString:@"City"] && [defaults valueForKeyPath:@"commaSeperatedCities"]) {
    ////        [requestStr appendFormat:@"<cityIds>%@</cityIds>",[defaults valueForKey:@"commaSeperatedCities"]];
    ////    }
    //
    //
    ////    if ([defaults valueForKey:@"GroupFindBy"]) {
    ////        [requestStr appendFormat:@"<groupBy>%@</groupBy>",[defaults valueForKey:@"GroupFindBy"]];
    ////    }
    //
    //    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    //
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@find/ssretsearch",BASE_URL];
    //    // NSString *urlString = [NSString stringWithFormat:@"%@find/ssretsearch", @"http://sdw2107:8080/HubCiti2.3.2/"];
    //    
    //    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    //    
    //    //[requestStr release];
    
}



-(void)request_getsmartseacount:(int)rownumber
{
    iWebRequestState = GETSMARTSEAPRODS;
    
    [defaults setValue:[productId objectAtIndex:rownumber] forKey:@"ParCatId"];
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ProductDetail><searchkey><![CDATA[%@]]></searchkey>", [defaults valueForKey:@"SearchString"]];
    
    if([defaults  valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
    if([defaults  valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_HUBCITIID]];
    
    
    [requestStr appendFormat:@"<parCatId>%@</parCatId><lastVistedProductNo>0</lastVistedProductNo></ProductDetail>",[productId objectAtIndex:rownumber] ];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@scannow/getsmartsearchcount",BASE_URL];
    NSLog(@"Req:%@ %@",requestStr,urlString);
    DLog(@"Req:%@ %@",requestStr,urlString);
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    //[requestStr release];
    
}



-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = hubcitiAnythingInfo;
    requestAnythingFromMainMenu=TRUE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetail;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if (![defaults boolForKey:BottomButton]) {
        if([defaults valueForKey:KEY_MITEMID])
            [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    
    else{
        if([defaults valueForKey:KEY_BOTTOMBUTTONID])
            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

//-(void)request_GetUserCat{
//    
//    iWebRequestState = GETFAVCATEGORIE;
//    if([Network currentReachabilityStatus]==0)
//    {
//        [common showAlert];
//    }
//    else {
//        
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//        
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//        
//    }
//}

-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATION;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}

//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRETS;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if (![defaults boolForKey:BottomButton]) {
        if([defaults valueForKey:KEY_MITEMID])
            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    
    else{
        if([defaults valueForKey:KEY_BOTTOMBUTTONID])
            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }

    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}

//in case of multiple partners
//-(void)request_GetPartners{
//    
//    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getpartners?citiExperId=%@",BASE_URL,[defaults valueForKey:KEY_LINKID]];
//    
//    iWebRequestState = GETPARTNER;
//    [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
//    
//}

#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        return;
    
    [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    [self showActionSheet];
	
	
}

-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
        
    } else{
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
    }
    
  
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}



- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
	[self dismissViewControllerAnimated:YES completion:nil];
    
}





#pragma mark Parse methods

// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case GETCATEGORY: //
            [self parse_getcategory:(id)response];
              [categoryTable reloadData];
            break;
        case SSCATSEARCH:
            [self parse_sscatsearch:(id)response];
            
             iWebRequestState=GETCATEGORY;
             [categoryTable reloadData];
            
            break;
        case SSRETSEARCH:
            [self parse_ssretsearch:(id)response];
            iWebRequestState=GETCATEGORY;
            [categoryTable reloadData];
            break;
        case GETSMARTSEAPRODS:
            [self parseSmartSearchCountResult:response];
              [categoryTable reloadData];
            break;
        case GETPRODSEARCH:
            [self parseProductSearchResult:response];
              [categoryTable reloadData];
            break;
            
        case getsmartsearchprodlist:
            [self parse_getsmartsearchprodlist:response];
              [categoryTable reloadData];
            break;
            
        case GETUSERINFOM:
        {
            [self parseGetUserData:response];
        }
            break;
        case CITIEXPRET:
            
            [self parse_CitiExpRet:response];
            
            break;
        case PARTNERRETS:
            [self parse_PartnerRet:response];
            break;
//        case GETPARTNER:
//            [self parse_GetPartners:response];
//            break;
            
        case GETFAVLOCATION:
            [self parse_GetUserCat:response];
            break;
            
        case hubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetail:
            [self parse_appsitedetails:response];
            break;
//        case catsearch:
//            [self parse_sscatsearch:response];
//            break;
            
            
        default:
            break;
    }
    
  
}




-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            self.cityExp = cevc;
            [SharedManager setRefreshAustinRetailers:YES];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            self.filters = pvc;
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}

/*
 *
 Parsing logic when response received for getting user details.
 Method called when navigated from Settings screen.
 *
 */


-(void)parse_CitiExpRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [defaults setObject:nil forKey:@"Response_Austin"];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Austin"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:100 forKey:@"CityExpTableTag"];
            self.cityExp = cevc;
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
        }
    }
}

//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
           // NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        

        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}

-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}


//-(void) navigateToScanNow
//{
//    [defaults setBool:YES forKey:@"KEY_SCANSPLASH"];
//    [defaults setValue:@"scanNow" forKey:KEY_SEARCHFROM];
//    [defaults setObject:@"YES" forKey:@"KEY_SCANNERCALLED"];
//    
//    int menulevel = [[defaults valueForKey:KEY_MENULEVEL]integerValue];
//    if(menulevel > 1)
//        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
//    else
//        [defaults  setBool:NO forKey:@"DisplayBackBarButton"];
//    
//    
//    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
//    ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];
//    [self.navigationController pushViewController:viewScanNowScreen animated:NO];
//    [viewScanNowScreen release];
//    
//}

-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
		}
    }
}

//Call Find Service and Class
-(void)navigateToFindView
{
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    navigatedFromMainMenu=false;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}


-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
     self.iEventsListViewController = iEventsListViewController;
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}



-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}




-(void)releaseAllocatedObjects
{
    NSArray *arrSubViews = [self.view subviews];
    for(int i=0; i<[arrSubViews count]; i++)
    {
        UIView *releaseObjView = (UIView*)[arrSubViews objectAtIndex:i];
        if(releaseObjView.tag == 99)
            [releaseObjView removeFromSuperview];
        if([releaseObjView isKindOfClass:[bottomButtonView class]])
        {
            [releaseObjView removeFromSuperview];
        }
    }
    
    if([arrFindBottomButtonDO count]>0)
    {
        [arrFindBottomButtonDO removeAllObjects];
        //[arrFindBottomButtonDO release];
        arrFindBottomButtonDO = nil;
    }
    if([arrBottomButtonViewContainer count]>0)
    {
        [arrBottomButtonViewContainer removeAllObjects];
        //[arrBottomButtonViewContainer release];
        arrBottomButtonViewContainer = nil;
    }
    
}





-(void)parseProductSearchResult:(NSString*) responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
		return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *productDetailElement = [TBXML childElementNamed:KEY_PRODUCTDETAIL parentElement:tbxml.rootXMLElement];
        TBXMLElement *prdCountElement = [TBXML childElementNamed:@"prdCount" parentElement:productDetailElement];
        
        if (prdCountElement!=nil)
        {
            TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbxml.rootXMLElement];
            nextPage = [[TBXML textForElement:nextPageElement] intValue];
            // [prodNameArray addObject:@"adasjdkljaklsdjklajdklajd adksjaskldjaklsjdklajsdkljasdkljaklsdjaklsdjklasjdklajdsklajsdkljadklsj"];
            while (productDetailElement != nil)
            {
                TBXMLElement *productNameElement = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:productDetailElement];
                TBXMLElement *prdCountElement = [TBXML childElementNamed:@"prdCount" parentElement:productDetailElement];
                TBXMLElement *prodIdElement = [TBXML childElementNamed:@"productId" parentElement:productDetailElement];
                TBXMLElement *rowNumElement = [TBXML childElementNamed:@"rowNumber" parentElement:productDetailElement];
                TBXMLElement *prodImgPathElement = [TBXML childElementNamed:@"productImagePath" parentElement:productDetailElement];
                TBXMLElement *prodDescElement = [TBXML childElementNamed:@"productDescription" parentElement:productDetailElement];
                
                TBXMLElement *prodListIDElement = [TBXML childElementNamed:KEY_PRODLISTID parentElement:productDetailElement];
                [prodListIDArray addObject:[TBXML textForElement:prodListIDElement]];
                
                if(prodIdElement)
                    [productId addObject:[TBXML textForElement:prodIdElement]];
                if(productNameElement)
                    [productName addObject:[TBXML textForElement:productNameElement]];
                if(prdCountElement)
                    [prodCountArray addObject:[TBXML textForElement:prdCountElement]];
                if(rowNumElement)
                    [rowCountArray addObject:[TBXML textForElement:rowNumElement]];
                if(shortDescription)
                    [shortDescription addObject:[TBXML textForElement:prodDescElement]];
                if(productImageArray)
                    [productImageArray addObject:[TBXML textForElement:prodImgPathElement]];
                
                productDetailElement = [TBXML nextSiblingNamed:KEY_PRODUCTDETAIL searchFromElement:productDetailElement];
            }
//            view1.hidden = NO;
        
            if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
                [UIView animateWithDuration:0.25 animations:^{
                    categoryTableHeightConstraint.constant = 820;//790
                    [self.view setNeedsUpdateConstraints];
                }];
                else if (IS_IPHONE5) {
                    [UIView animateWithDuration:0.25 animations:^{
                        categoryTableHeightConstraint.constant = 278+88;
                        [self.view setNeedsUpdateConstraints];
                    }];
                }
                else
                {
                    [UIView animateWithDuration:0.25 animations:^{
                        categoryTableHeightConstraint.constant = 278;
                        [self.view setNeedsUpdateConstraints];
                    }];
                }
            
            categoryTable.hidden = NO;
            
        }
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:nil msg:responseTextStr];
        [loading stopAnimating];
        ReleaseAndNilify(loading);

//        view1.hidden = NO;
 
    }
    [categoryTable reloadData];
    [defaults setBool:NO forKey:@"ViewMore"];
}


-(void)parseSmartSearchCountResult:(NSString*) responseXml
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
		return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        [defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
        cancelBtn.tag = 101;
        SearchResultCount *searchCountScreen = [[SearchResultCount alloc]initWithNibName:@"SearchResultCount" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:searchCountScreen animated:NO];
        //[searchCountScreen release];
        
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
//        googleSearch.text = @"";
    }
    
}

-(void)parse_ssretsearch:(id)responseString
{
    if (responseString == NULL)
        return;
    else{
        if(ssretResponse == NULL)
        {
            ssretResponse = [[RetailersDetails alloc]init];
        }
        [ssretResponse setValuesForKeysWithDictionary:responseString];
        
        NSLog(@"response = %@", ssretResponse);
        
        if ([ssretResponse.responseCode isEqualToString:@"10000"] )//success response
        {
            [defaults  setObject:responseString forKey:KEY_RESPONSEXML];
            FindLocationServiceViewController *locVC = [[FindLocationServiceViewController alloc] initWithNibName:@"FindLocationServiceViewController" bundle:[NSBundle mainBundle]];
            locVC.typeStr = @"";
            locVC.findTitle=findTitle;
            if([defaults valueForKey:@"findCatMitemid"])
                locVC.findmitemid = [defaults valueForKey:@"findCatMitemid"];
            //locVC.respFromFind=responseString;
            locVC.searchStr = [[NSString alloc]initWithString:retSearchText];
            [self.navigationController pushViewController:locVC animated:NO];
        }
        else
        {
            NSString *responseTextStr;
            if(ssretResponse.responseText.length == 0)
            {
                responseTextStr = @"No Records Found";
            }
            else
            {
                responseTextStr= ssretResponse.responseText;
            }
            [UtilityManager showAlert:nil msg:responseTextStr];
        }
    }
    
}

-(void)parse_sscatsearch:(id)responseString
{
    if(responseString == NULL)
    {
        return;
    }
    else
    {
        if(sscatResponse == NULL)
        {
            sscatResponse = [[RetailersDetails alloc]init];
        }
        [sscatResponse setValuesForKeysWithDictionary:responseString];
        
        NSLog(@"response = %@", sscatResponse);
        
        if ([sscatResponse.responseCode isEqualToString:@"10000"] )//success response
        {
            CatList *obj_FindCategoryDO = [findCategoryArray objectAtIndex:index.row];
            [defaults  setObject:responseString forKey:KEY_RESPONSEXML];
            FindLocationServiceViewController *locVC = [[FindLocationServiceViewController alloc] initWithNibName:@"FindLocationServiceViewController" bundle:[NSBundle mainBundle]];
            locVC.catId=[obj_FindCategoryDO.CatID copy];
            locVC.typeStr = [obj_FindCategoryDO.CatDisName copy];
            if([defaults valueForKey:@"findCatMitemid"])
                locVC.findmitemid = [defaults valueForKey:@"findCatMitemid"];
            locVC.findTitle=findTitle;
            locVC.searchStr = @"";
            [self.navigationController pushViewController:locVC animated:NO];
        }
        else
        {
            
            NSString *responseTextStr;
            if(sscatResponse.responseText.length ==0)
            {
                responseTextStr = @"No Records found";
            }
            else
            {
                responseTextStr = sscatResponse.responseText;
            }
            [UtilityManager showAlert:nil msg:responseTextStr];
        }
    }
    
}

-(void)parse_getcategory:(id)responseString
{
    
    if(responseString == NULL)
        return;
    if(catResponse == NULL)
    {
        catResponse = [[Categories alloc]init];
    }
    [catResponse setValuesForKeysWithDictionary:responseString];
    
    NSLog(@"response = %@", catResponse);
    
    if ([catResponse.responseCode isEqualToString:@"10000"] )//success response
    {
        search.placeholder = @"Search";
        
        if (catResponse.mColor != nil)
        {
            cellColor = [catResponse.mColor copy];
        }
        if(catResponse.mFontColor != nil)
        {
            fontColor = [catResponse.mFontColor copy];
        }
        if(catResponse.mainMenuId != nil)
        {
            [defaults setObject:catResponse.mainMenuId forKey:KEY_MAINMENUID];
        }
        if(catResponse.catList)
        {
            findCategoryArray = [[NSMutableArray alloc]init];
            for(int i = 0;i < catResponse.catList.count; i++)
            {
                CatList * catListObj = [[CatList alloc] init];
                
                
                NSDictionary* dictList = catResponse.catList[i];
                
                [catListObj setValuesForKeysWithDictionary:dictList];
                [findCategoryArray addObject:catListObj];
                
            }
        }
       
        if([catResponse.bottomBtn integerValue] == 1)
        {
             bottomBtn = [catResponse.bottomBtn intValue];
            arrFindBottomButtonDO=[[NSMutableArray alloc]init];
            for (int i=0; i < catResponse.bottomBtnList.count; i++) {
                bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                NSDictionary* dictList = catResponse.bottomBtnList[i];
                
                [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                
                if(obj_findBottomDO.bottomBtnName.length == 0)
                {
                    obj_findBottomDO.bottomBtnName = @" ";
                }
                if(obj_findBottomDO.bottomBtnImg.length == 0)
                {
                    obj_findBottomDO.bottomBtnImg = @" ";
                }
                if(obj_findBottomDO.bottomBtnImgOff.length == 0)
                {
                    obj_findBottomDO.bottomBtnImgOff = @" ";
                }
                if(obj_findBottomDO.btnLinkTypeName)
                {
                    if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"])
                    {//for filters
                        [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                    }
                }
                [arrFindBottomButtonDO addObject:obj_findBottomDO];
                
               
            }
            if([arrFindBottomButtonDO count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                [self setBottomBarMenu];
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            
            
        }
        [self setViewandTable];
        
        
    }
    else if ([catResponse.responseCode isEqualToString:@"10005"])
    {
        if(catResponse.mainMenuId != nil)
        {
            [defaults setObject:catResponse.mainMenuId forKey:KEY_MAINMENUID];
        }
        if([catResponse.bottomBtn integerValue] == 1)
        {
            bottomBtn = [catResponse.bottomBtn intValue];
            arrFindBottomButtonDO=[[NSMutableArray alloc]init];
            for (int i=0; i < catResponse.bottomBtnList.count; i++) {
                bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                NSDictionary* dictList = catResponse.bottomBtnList[i];
                
                [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                
                if(obj_findBottomDO.bottomBtnName.length == 0)
                {
                    obj_findBottomDO.bottomBtnName = @" ";
                }
                if(obj_findBottomDO.bottomBtnImg.length == 0)
                {
                    obj_findBottomDO.bottomBtnImg = @" ";
                }
                if(obj_findBottomDO.bottomBtnImgOff.length == 0)
                {
                    obj_findBottomDO.bottomBtnImgOff = @" ";
                }
                if(obj_findBottomDO.btnLinkTypeName)
                {
                    if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"])
                    {//for filters
                        [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                    }
                }
                [arrFindBottomButtonDO addObject:obj_findBottomDO];
                
            }
           
            if([arrFindBottomButtonDO count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                [self setBottomBarMenu];
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            [self setViewandTable];
        }
         search.placeholder = @"";
        NSString *response=catResponse.responseText;
        if([response isEqualToString:@"No Records Found."])
        {
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:response];
        }
        else{
            NSString *responseTextStr = [catResponse.responseText stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
            [UtilityManager showFormatedAlert:responseTextStr];
           
        }
        
    }
    else if ([catResponse.responseCode isEqualToString:@"10001"]){
        search.placeholder = @"";
        NSString *responseTextStr;
        if(catResponse.responseText.length == 0)
        {
            responseTextStr = @"No Records found";
        }
        else
        {
            responseTextStr = catResponse.responseText;
        }
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
    }
    else
    {
        search.placeholder = @"";
        NSString *responseTextStr;
        if(catResponse.responseText.length == 0)
        {
            responseTextStr = @"No Records found";
        }
        else
        {
            responseTextStr = catResponse.responseText;
        }
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
        
    }
    
    
    
    
    
    
    //    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
    //		return;
    //
    //    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    //
    //    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    //    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    //    {
    //        search.placeholder = @"Search";
    //
    //
    //        TBXMLElement *catListElement = [TBXML childElementNamed:@"catList" parentElement:tbxml.rootXMLElement];
    //
    //        //@Deepak: Parsed MainMenuID for userTracking
    //        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
    //
    //        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbxml.rootXMLElement]]intValue];
    //
    //        TBXMLElement *mColorElement = [TBXML childElementNamed:@"mColor" parentElement:tbxml.rootXMLElement];
    //
    //        TBXMLElement *mFontColorElement = [TBXML childElementNamed:@"mFontColor" parentElement:tbxml.rootXMLElement];
    //
    //        if (mColorElement!=nil){
    //            cellColor= [[TBXML textForElement:mColorElement]copy];
    //        }
    //
    //        if (mFontColorElement!=nil){
    //            fontColor= [[TBXML textForElement:mFontColorElement]copy];
    //        }
    //
    //        if(mainMenuIdElement !=nil)
    //        {
    //            [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
    //        }
    //
    //        if(catListElement !=nil)
    //        {
    //            TBXMLElement *categoryElement = [TBXML childElementNamed:@"GoogleCategory" parentElement:catListElement];
    //            arrFindBottomButtonDO=[[NSMutableArray alloc]init];
    //            while (categoryElement != nil)
    //            {
    //                FindCategoryDO *obj_FindCategoryDO = [[FindCategoryDO alloc]init];
    //
    //                TBXMLElement *catDisNameElement = [TBXML childElementNamed:@"CatDisName" parentElement:categoryElement];
    //                TBXMLElement *catImgPthElement = [TBXML childElementNamed:@"CatImgPth" parentElement:categoryElement];
    //
    //                //@Deepak: adding code to parse catID
    //                TBXMLElement *catIDElement = [TBXML childElementNamed:KEY_CATID parentElement:categoryElement];
    //                if(catIDElement)
    //                    obj_FindCategoryDO.catID = [[NSString alloc]initWithString:[TBXML textForElement:catIDElement]];
    //
    //                if(catDisNameElement)
    //                    obj_FindCategoryDO.CatDisName = [[NSString alloc]initWithString:[TBXML textForElement:catDisNameElement]];
    //
    //                if(catImgPthElement)
    //                    obj_FindCategoryDO.CatImgPth = [[NSString alloc]initWithString:[TBXML textForElement:catImgPthElement]];
    //
    //                categoryElement = [TBXML nextSiblingNamed:@"GoogleCategory" searchFromElement:categoryElement];
    //
    //                [findCategoryArray addObject:obj_FindCategoryDO];
    //                //[obj_FindCategoryDO release];
    //            }
    //        }
    ////        NSRegularExpression *regex = [NSRegularExpression
    ////                 regularExpressionWithPattern:@"<bottomBtnList>"
    ////                 options:NSRegularExpressionCaseInsensitive
    ////                 error:nil];
    ////
    ////        NSArray *matches = [regex matchesInString:responseString
    ////                                          options:0
    ////                                            range:NSMakeRange(0, [responseString length])];
    ////
    ////        if ([matches count]==1) {
    //
    //
    //        if(bottomBtn==1){
    //
    //        TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbxml.rootXMLElement];
    //        TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
    //        while(BottomButtonElement)
    //        {
    //
    //            bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
    //
    //
    //            TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
    //            TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
    //            TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
    //            TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
    //            TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
    //            TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
    //            TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
    //            TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
    //
    //
    //            if(bottomBtnIDElement)
    //                obj_findBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
    //
    //            if(bottomBtnNameElement)
    //                obj_findBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
    //            else
    //                obj_findBottomDO.bottomBtnName = @" ";
    //
    //            if(bottomBtnImgElement)
    //                obj_findBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
    //            else
    //                obj_findBottomDO.bottomBtnImg = @" ";
    //
    //            if(bottomBtnImgOffElement)
    //                obj_findBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
    //            else
    //                obj_findBottomDO.bottomBtnImgOff = @" ";
    //
    //            if(btnLinkTypeIDElement)
    //                obj_findBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
    //
    //            if(btnLinkTypeNameElement)
    //                obj_findBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
    //
    //            if(btnLinkIDElement){
    //                obj_findBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
    //
    //                if(btnLinkTypeNameElement){
    //                if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
    //                    [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
    //                }
    //            }
    //
    //            if(positionElement)
    //                obj_findBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
    //
    //
    //
    //            BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
    //            [arrFindBottomButtonDO addObject:obj_findBottomDO];
    //            //[obj_findBottomDO release];
    //        }
    //        if([arrFindBottomButtonDO count] > 0)
    //        {
    //            // Set Bottom Menu Button after read from response data
    //            [self setBottomBarMenu];
    //            [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
    //        }
    //        else
    //            [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
    //        }
    //         [self setViewandTable];
    //    }
    //
    //
    //    else if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10005"])
    //    {
    //
    //
    //        //@Deepak: Parsed MainMenuID for userTracking
    //        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
    //        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbxml.rootXMLElement]]intValue];
    //        if(mainMenuIdElement !=nil)
    //        {
    //            [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
    //        }
    //
    //
    //        if (bottomBtn==1) {
    //
    //        TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbxml.rootXMLElement];
    //
    //        if(bottomButtonListElement !=nil)
    //        {
    //        TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
    //            arrFindBottomButtonDO=[[NSMutableArray alloc]init];
    //        while(BottomButtonElement)
    //        {
    //
    //            bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
    //
    //
    //            TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
    //            TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
    //            TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
    //            TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
    //            TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
    //            TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
    //            TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
    //            TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
    //
    //
    //            if(bottomBtnIDElement)
    //                obj_findBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
    //
    //            if(bottomBtnNameElement)
    //                obj_findBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
    //            else
    //                obj_findBottomDO.bottomBtnName = @" ";
    //
    //            if(bottomBtnImgElement)
    //                obj_findBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
    //            else
    //                obj_findBottomDO.bottomBtnImg = @" ";
    //
    //            if(bottomBtnImgOffElement)
    //                obj_findBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
    //            else
    //                obj_findBottomDO.bottomBtnImgOff = @" ";
    //
    //            if(btnLinkTypeIDElement)
    //                obj_findBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
    //
    //            if(btnLinkTypeNameElement)
    //                obj_findBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
    //
    //            if(btnLinkIDElement){
    //                obj_findBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
    //                if(btnLinkTypeNameElement){
    //                if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
    //                    [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
    //                }
    //            }
    //
    //            if(positionElement)
    //                obj_findBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
    //
    //
    //
    //            BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
    //            [arrFindBottomButtonDO addObject:obj_findBottomDO];
    //            //[obj_findBottomDO release];
    //        }
    //            if([arrFindBottomButtonDO count] > 0)
    //            {
    //            // Set Bottom Menu Button after read from response data
    //                [self setBottomBarMenu];
    //                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
    //            }
    //            else
    //                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
    //            }
    //        }
    //        search.placeholder = @"";
    //        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    //        
    //        NSString *response=[TBXML textForElement:saveResponseText];
    //        if([response isEqualToString:@"No Records Found."])
    //        {
    //            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:response];
    //        }
    //        else{
    //                NSString *responseTextStr = [[TBXML textForElement:saveResponseText] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    //                [self showFormatedAlert:responseTextStr];
    //        }
    //        
    //
    //
    //        [self setViewandTable];
    //    }
    //    
    //    
    //    
    //    else
    //    {
    //        search.placeholder = @"";
    //        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    //        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
    //        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
    //        [self showFormatedAlert:responseTextStr];
    //        
    //    }
    
}






-(void)getProductSearchResult :(NSString *) str
{
    iWebRequestState = GETPRODSEARCH;
    
    //	NSString *str = ste.text;
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([str length] == 0) {
        [UtilityManager showAlert:nil msg:NSLocalizedString(@"Please enter some text",@"Please enter some text")];
    }
    else {
        if (fetchingNextResults == NO){
            [prodListIDArray removeAllObjects];
            [productName removeAllObjects];
            [productId removeAllObjects];
            [productImageArray removeAllObjects];
            [shortDescription removeAllObjects];
            [rowCountArray removeAllObjects];
            [prodCountArray removeAllObjects];
            nextPage = 0;
        }
        //	savedSearchString = searchBar.text;
        [search resignFirstResponder];
        
        NSMutableString *requestStr = [[NSMutableString alloc] init];
        [requestStr appendFormat:@"<ProductDetail><searchkey><![CDATA[%@]]></searchkey>", str];
        [requestStr appendFormat:@"<parCatId>0</parCatId><lastVistedProductNo>%d</lastVistedProductNo>",lowerLimit];
        
        if([defaults  valueForKey:KEY_MAINMENUID])
            [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
        
        if([defaults  valueForKey:KEY_HUBCITIID])
            [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_HUBCITIID]];
        
        [requestStr appendString:@"</ProductDetail>"];
        
        NSString *urlString = [NSString stringWithFormat:@"%@scannow/getsmartsearchcount",BASE_URL];
        
        if ([defaults boolForKey:@"ViewMore"]) {
            NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
            
            
            [self parseProductSearchResult:response];
            
        }
        else{
            [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
        }

        
        //[ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
        
        ReleaseAndNilify(requestStr);
        
    }
	
}

-(void)smartSearch
{
    NSString *trimmedString = [search.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    [defaults setValue:trimmedString forKey:@"SearchString"];
    
    iWebRequestState = GETSMARTSEAPRODS;
    [ProductSearch getProductInfo:trimmedString withDelegate:self];
    [timer invalidate];
    timer = nil;
}


-(void) returnProductInfo :(NSArray*)info
{
    if (smartSearch == YES)
    {
        [prodListIDArray removeAllObjects];
        [productName removeAllObjects];
        [productId removeAllObjects];
        [productImageArray removeAllObjects];
        [shortDescription removeAllObjects];
        [rowCountArray removeAllObjects];
        [prodCountArray removeAllObjects];
        
        NSArray *prd = [info copy] ;
        [productName addObjectsFromArray:[prd objectAtIndex:0]];
        [productId addObjectsFromArray:[prd objectAtIndex:1]];
        [categoryTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        [categoryTable reloadData];
    }
}

#pragma mark searchbar delegate methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    switch (searchBar.tag) {//searchBar tag = 0 for search and 1 for googleSearch
        case 0:
            animate = 1;
            cancelBtn.tag = 100;
            break;
            
//        case 1:
//            animate = 0;
//            viewFrame = view1.frame;
//            [UIView beginAnimations:@"slideIn" context:NULL];
//            [UIView setAnimationCurve:UIViewAnimationCurveLinear];
//            [UIView setAnimationDuration:0.3];
//            view1.center = CGPointMake(160, 50);
//            [UIView commitAnimations];
//            break;
            
        default:
            break;
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{// called when text changes (including clear)
    //ONLY FOR SEARCH
    if (searchBar.tag == 0 && ![searchText length]){
        //means only in case clear is tapped
        lowerLimit = 0;
        if (timer){
            [timer invalidate];
            timer = nil;
        }
//        if(![[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"]){
//        if (viewSelectionControl.selectedSegmentIndex == 1){
//            [defaults setBool:NO forKey:@"ReturnResult"];
//            categoryTable.hidden = YES;
//            
//            //remove all array items
//            [prodListIDArray removeAllObjects];
//            [productName removeAllObjects];
//            [productId removeAllObjects];
//            [productImageArray removeAllObjects];
//            [shortDescription removeAllObjects];
//            [rowCountArray removeAllObjects];
//            [prodCountArray removeAllObjects];
//            [categoryTable reloadData];
//            nextPage = 0;
//        }
//        }
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    tappedSearch=TRUE;
    
    switch (searchBar.tag) {
        case 0:{//FOR SEARCH
            
            if (timer){
                [timer invalidate];
                timer = nil;
            }
            if (cancelBtn.tag == 101)
                return;
            
            //expression to trim whitespaces in between search words
            NSError *error = nil;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
            NSString *trimmedString = [regex stringByReplacingMatchesInString:searchBar.text options:0 range:NSMakeRange(0, [searchBar.text length]) withTemplate:@" "];
            DLog(@"%@",trimmedString);
            
            NSString *searchText = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            searchBar.text = trimmedString;
            
//            if(![[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"]){
//                switch (viewSelectionControl.selectedSegmentIndex) {
//                        
//                    case 0:
//                        if (![searchText length]){
//                            categoryTable.hidden = NO;
//                            searchBar.text = @""; //reset the textField
//                            return;
//                        }
//                        [searchBar resignFirstResponder];
//                        [self request_ssretsearch:searchText];
//                        break;
//                        
//                    case 1:{
//                        
//                        if (![searchText length]) {
//                            categoryTable.hidden = YES;
//                            searchBar.text = @"";//reset the textField
//                            return;
//                        }
//                        
//                        
//                        lowerLimit = 0 ;
//                        [defaults  setBool:YES forKey:@"prodSearch"];
//                        //[categoryTable setHidden:YES];
//                        smartSearch = NO;
//                        fetchingNextResults = NO;
//                        [defaults setBool:NO forKey:@"ReturnResult"];
//                        // [defaults setBool:YES forKey:@"ReturnProductSearchResult"];
//                        [searchBar resignFirstResponder];
//                        [self getProductSearchResult:searchText];
//                    }
//                        break;
//                        
//                    default:
//                        break;
//                }
//            }
          //  else
            {
                if (![searchText length]){
                    categoryTable.hidden = NO;
                    searchBar.text = @""; //reset the textField
                    return;
                }
                [searchBar resignFirstResponder];
                [self request_ssretsearch:searchText];
                break;
            }
           
            }
             break;
//            switch (viewSelectionControl.selectedSegmentIndex) {
//                    
//                case 0:
            
                    
//                case 1:{
//                    
//                    if (![searchText length]) {
//                        categoryTable.hidden = YES;
//                        searchBar.text = @"";//reset the textField
//                        return;
//                    }
//                    
//                    
//                    lowerLimit = 0 ;
//                    [defaults  setBool:YES forKey:@"prodSearch"];
//                    //[categoryTable setHidden:YES];
//                    smartSearch = NO;
//                    fetchingNextResults = NO;
//                    [defaults setBool:NO forKey:@"ReturnResult"];
//                    // [defaults setBool:YES forKey:@"ReturnProductSearchResult"];
//                    [searchBar resignFirstResponder];
//                    [self getProductSearchResult:searchText];
//                }
//                    break;
//                    
//                default:
//                    break;
//            }
//        }
//            break;
            
//        case 1:{//for google search
//            
//            //searchBar.text = [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//            
//            if (![searchBar.text length]) {
//                
//                return;
//            }
//            DLog(@"Text:%@", searchBar.text);
//            
//            [defaults setValue:searchBar.text forKey:@"Google Search"];
//            
//            NSString *str = [NSString stringWithFormat:@"http://www.google.com/search?q=%@",searchBar.text/*[defaults valueForKey:@"Google Search"]*/];
//            
//            NSString *query = [str stringByReplacingOccurrencesOfString:@" " withString:@"+"];
//            // NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.google.com/search?q=%@",query]];
//            // str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//            
//            
//            
//            [searchBar resignFirstResponder];
//            [UIView beginAnimations:@"slideOut" context:NULL];
//            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//            [UIView setAnimationDuration:0.3];
//            view1.frame = viewFrame;
//            [UIView commitAnimations];
//            
//            
//            
//            
//            [defaults setValue:query forKey:KEY_URL];
//            WebBrowserViewController *wbvc = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
//            [self.navigationController pushViewController:wbvc animated:NO];
//            [wbvc release];
//            
//        }
//            break;
        default:
            break;
    }
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
//     if(![[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"]){
//    switch (searchBar.tag) {
//        case 0:{//only in case of search
//            categoryTable.tag = 2;
//            
//            if (viewSelectionControl.selectedSegmentIndex == 1)
//            {
//                if(([searchBar.text length] >= 4 && [text length] != 0) || ([text length] == 0 &&  [searchBar.text length] > 5))
//                {
//                    [defaults  setBool:NO forKey:@"prodSearch"];
//                    // [timer invalidate];
//                    if (![timer isValid])
//                        timer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(smartSearch) userInfo:nil repeats:NO];
//                }
//                else{
//                    //    [categoryTable reloadData];
//                    [defaults  setBool:YES forKey:@"prodSearch"];
//                    return YES;
//                }
//                
//                fetchingNextResults = NO;
//                [defaults setBool:YES forKey:@"ReturnResult"];
//                
//                
//                //[ProductSearch getProductInfo:str withDelegate:self];
//                if (smartSearch == NO && ([shortDescription count])) {
//                    [prodListIDArray removeAllObjects];
//                    [productName removeAllObjects];
//                    [productId removeAllObjects];
//                    [productImageArray removeAllObjects];
//                    [shortDescription removeAllObjects];
//                    [rowCountArray removeAllObjects];
//                    [prodCountArray removeAllObjects];
//                    
//                    [categoryTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
//                    categoryTable.tag = 2;
//                    nextPage = 0;
//                    [categoryTable reloadData];
//                }
//                else if (smartSearch == NO) {
//                    [productName removeAllObjects];
//                    [productId removeAllObjects];
//                    [categoryTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
//                    categoryTable.tag = 2;
//                    [categoryTable reloadData];
//                }
//                categoryTable.hidden = NO;
//                smartSearch = YES;
//                return YES;
//            }
//            
//            return YES;
//        }
//            break;
//            
//            
//        default:
//            return YES;
//            break;
//    }
//     }
   
    return YES;

}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
   
    switch (searchBar.tag) {
        case 0:{//for search
            if (timer){
                [timer invalidate];
                timer = nil;
            }
            
            if (smartSearch == YES) {
                
                categoryTable.hidden = YES;
                [productName removeAllObjects];
                [productId removeAllObjects];
                [categoryTable reloadData];
            }
            
            
            [defaults setBool:NO forKey:@"ReturnResult"];
            cancelBtn.tag = 101;
            searchBar.text = @"";
            //ReleaseAndNilify(categoryTable);
            
            [search resignFirstResponder];
            [prodListIDArray removeAllObjects];
            [productName removeAllObjects];
            [productId removeAllObjects];
            [productImageArray removeAllObjects];
            [shortDescription removeAllObjects];
            [rowCountArray removeAllObjects];
            [prodCountArray removeAllObjects];
            [categoryTable reloadData];
            nextPage = 0;
        }
            break;
            
//        case 1:{//for google search
//            searchBar.text = @"";
//            [searchBar resignFirstResponder];
//            [UIView beginAnimations:@"slideOut" context:NULL];
//            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//            [UIView setAnimationDuration:0.3];
//            
//            view1.frame = viewFrame;
//            [UIView commitAnimations];
//        }
//            break;
        default:
            break;
    }
   
}

#pragma mark TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if(![[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"]){
//    switch ([viewSelectionControl selectedSegmentIndex])
//    {
//        case 0:
//        {
//            switch (iWebRequestState)
//            {
//                case GETCATEGORY:
//                    return [findCategoryArray count];
//                    break;
//                    
//                default:
//                    break;
//            }
//        }
//            break;
//        case 1:
//        {
//            switch (iWebRequestState)
//            {
//                case GETSMARTSEAPRODS:
//                case GETPRODSEARCH:
//                {
//                    if (((nextPage == 1) || (lowerLimit != 0)) && [productId count] > 0){
//                        return [productId count]+1;
//                    }
//                    else
//                        return [productId count];
//                }
//                    break;
//                    
//                default:
//                    break;
//            }
//        }
//            break;
//            
//        default:
//            break;
//    }
//    }
// else
    {
        switch (iWebRequestState)
        {
            case GETCATEGORY:
                return [findCategoryArray count];
                break;
                
            default:
                break;
        }
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  [self getTableViewCellHeight];
}

-(CGFloat) getTableViewCellHeight
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        if(categoryTable.tag == 1)
            return 50.0;
        else
            return 60.0;
    }
    else
    {
        if(categoryTable.tag == 1)
            return 70.0;
        else
            return 80.0;
    }
}


-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    if (keyWord!=NULL) {
       // [keyWord release];
    }
    if (catRetSearchKey!=NULL) {
     //   [catRetSearchKey release];
    }
    [FindBottomButtonID removeLastObject];
    if([defaults valueForKey:@"findFlag"]){
        [defaults setValue:@"subMenu" forKey:KEY_BOTTOMBUTTONNAME];
    }
    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    [self.navigationController popViewControllerAnimated:NO];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;/* = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];*/
	
	//if( cell == nil)
	cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellFind"] ;
	cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    if (indexPath.row != findCategoryArray.count - 1)
    {
        UIView *separatorStyleView = [[UIView alloc]initWithFrame:CGRectMake(0, [self getTableViewCellHeight] - (IPAD?2:1.5) , SCREEN_WIDTH, (IPAD?2:1.5))];
        separatorStyleView.backgroundColor = [UIColor grayColor];
        [cell.contentView addSubview:separatorStyleView];
    }
    
//    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//    tableView.separatorColor=[UIColor blackColor];
//    tableView.separatorInset = UIEdgeInsetsZero;
    
    UILabel *prodName = [[UILabel alloc]init] ;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        prodName.frame = CGRectMake(60, 2, 650, 40);
    else
        prodName.frame = CGRectMake(60, 2, 250, 40);
	prodName.backgroundColor = [UIColor clearColor];
	prodName.textColor = [UIColor convertToHexString:fontColor];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
    prodName.font = [UIFont fontWithName:@"Helvetica" size:18];
    else
        prodName.font = [UIFont fontWithName:@"Helvetica" size:13];
    prodName.numberOfLines = 2;
    
    UILabel *catName = [[UILabel alloc]init];
	catName.backgroundColor = [UIColor clearColor];
	catName.textColor = [UIColor blackColor];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
    catName.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    else
        catName.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
	
	SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(10, 9, 40, 40)];//10
	asyncImageView.contentMode = UIViewContentModeScaleToFill;
	asyncImageView.backgroundColor = [UIColor clearColor];
    if(![[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"]){

    switch ([viewSelectionControl selectedSegmentIndex])
    {
        case 0:
        {
            switch (iWebRequestState)
            {
                case GETCATEGORY:
                {
                    if([findCategoryArray count]!=0){
                    CatList *obj_FindCategoryDO = [findCategoryArray objectAtIndex:indexPath.row];
                    
                    categoryTable.tag = 1;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.backgroundColor =  [UIColor convertToHexString:cellColor];
                        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                        {
                            prodName.font = [UIFont boldSystemFontOfSize:21];
                            prodName.frame = CGRectMake(70, 15, 650, 50);
                            asyncImageView.frame = CGRectMake(10.0, 12.0, 50.0, 50.0);
                            asyncImageView.clipsToBounds = YES;
                            
                            asyncImageView.layer.cornerRadius = 25.0;
                        }
                        else
                        {
                            prodName.font = [UIFont boldSystemFontOfSize:16];
                            prodName.frame = CGRectMake(60, 5, 250, 40);
                            asyncImageView.frame = CGRectMake(10.0, 5.0, 40.0, 40.0);
                            asyncImageView.clipsToBounds = YES;
                          
                            asyncImageView.layer.cornerRadius = 20.0;
                        }
                    prodName.text = obj_FindCategoryDO.CatDisName;
                    
                    
                    [asyncImageView loadImage:obj_FindCategoryDO.CatImgPth];
                        [cell.contentView addSubview:prodName];
                    [cell.contentView addSubview:asyncImageView];
                    }
                    
                }
                    break;
                default:
                    break;
            }
            break;
        }
        case 1:
        {
            switch (iWebRequestState)
            {
                case GETSMARTSEAPRODS:
                case GETPRODSEARCH:
                {
                    categoryTable.tag = 2;
                    if (indexPath.row == [prodCountArray count] && nextPage == 1){
                        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                        
                        loading.center = cell.contentView.center;
                        loading.color = [UIColor blackColor];
                        
                        
                        [cell.contentView addSubview:loading];
                        [loading startAnimating];
                        cell.userInteractionEnabled = NO;
                      
                    }
                    else
                    {
                        UILabel *DetailLabel = [[UILabel alloc] init];
                        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                        {
                            DetailLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
                            DetailLabel.frame = CGRectMake(60, 40, 650, 15);
                        }
                        else
                        {
                            DetailLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
                            DetailLabel.frame = CGRectMake(60, 40, 250, 15);
                        }
                        DetailLabel.textColor = [UIColor grayColor];
                        
                        
                        if ([productImageArray count] > indexPath.row) {//assuming its not smart search
                            
                            if ([[prodCountArray objectAtIndex:indexPath.row]intValue]>1){
                                
                                DetailLabel.text = [NSString stringWithFormat:@"(%@ items)",[prodCountArray objectAtIndex:indexPath.row]];
                            }
                            else if ([[prodCountArray objectAtIndex:indexPath.row]intValue]==1){
                                
                                DetailLabel.text = [shortDescription objectAtIndex:indexPath.row];
                                
                            }
                            
                            if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                            {
                                catName.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
                                catName.frame = CGRectMake(60, 2, 650, 40);
                            }
                            else
                            {
                                catName.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
                                catName.frame = CGRectMake(60, 2, 250, 40);
                            }
                            
                            
                            catName.text = [productName objectAtIndex:indexPath.row];
                            
                            if (![[productImageArray objectAtIndex:indexPath.row] isEqualToString:@"N/A"]){
                                
                                [asyncImageView loadImage:[productImageArray objectAtIndex:indexPath.row]];
                                [cell.contentView addSubview:asyncImageView];
                            }
                        }
                        
                        
                        else {
                            //display only "in <cat name> as bold
                            
                            if ([productName count]>indexPath.row){
                                
                                NSString *str = [productName objectAtIndex:indexPath.row];
                                
                                NSRange match;
                                
                                match = [str rangeOfString: @"|~|"];
                                
                                if (match.location == NSNotFound){
                                    DLog (@"Match not found");
                                    catName.text = str;
                                    
                                    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                                    {
                                        catName.frame = CGRectMake(10, 10, 650, 40);
                                        catName.font = [UIFont fontWithName:@"Helvetica" size:18];
                                    }
                                    else
                                    {
                                        catName.frame = CGRectMake(10, 10, 250, 40);
                                        catName.font = [UIFont fontWithName:@"Helvetica" size:13];
                                    }
                                }
                                
                                else{
                                    CGSize txtSz;
                                    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                                        
                                        txtSz = [[str substringToIndex:match.location-1] sizeWithAttributes: @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18.0f]}];
                                    else
                                        txtSz = [[str substringToIndex:match.location-1] sizeWithAttributes: @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:13.0f]}];
                                    CGRect lblFrame = CGRectMake(10, 10, txtSz.width, 40);
                                    
                                    catName.frame = lblFrame;
                                    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                                        if (lblFrame.size.width >150)
                                            catName.frame = CGRectMake(txtSz.width + 15, 10, 100 , 40);
                                        else
                                            catName.frame = CGRectMake(txtSz.width + 15, 10, 200 , 40);
                                    }
                                    else
                                    {
                                        catName.frame = CGRectMake(txtSz.width + 15, 10, 600 , 40);
                                    }
                                    
                                    
                                    if ([str length]>match.location+3){
                                        
                                        catName.text = [str substringToIndex:match.location-1];
                                        catName.text = [str substringFromIndex: match.location+3];
                                        
                                        [cell.contentView addSubview:catName];
                                        DLog(@"Length:%lu",(unsigned long)[str length]);
                                        DLog (@"match found at index %lu", (unsigned long)match.location);
                                        DLog(@"String 2:%@",str);
                                    }
                                    else {
                                        catName.text = str;
                                    }
                                }
                            }
                            
                        }
                        [cell.contentView addSubview:DetailLabel];
                        [cell.contentView addSubview:catName];
                        ReleaseAndNilify(DetailLabel);
                    }
                }
                    break;
                default:
                    break;
            }
            break;
        }
    }
    }
    else
    {
       
        int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
        if(menulevel > 1){
            smBkgrdImage=[defaults valueForKey:@"menuBkgrdImage"];
            smBkgrdColor=[defaults valueForKey:@"menuBkgrdColor"];
        }
        else
        {
            mBkgrdImage=[defaults valueForKey:@"menuBkgrdImage"];
            mBkgrdColor=[defaults valueForKey:@"menuBkgrdColor"];
        }
        
            if(menulevel > 1)
            {
                if(smBkgrdImage && ![smBkgrdImage isEqualToString:@"N/A"])
                {
                    SdImageView *asyncImageView;
                    
                    asyncImageView = [[SdImageView alloc] initWithFrame:tableView.frame];
                    
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    asyncImageView.layer.cornerRadius = 5.0f;
                    [asyncImageView loadImage:[smBkgrdImage stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                    asyncImageView.tag=99;
                    [tableView setBackgroundView:asyncImageView];
                    //[asyncImageView release];
                    
                    [defaults setValue:smBkgrdImage forKey:@"menuBkgrdImage"];
                    [defaults setValue:nil forKey:@"menuBkgrdColor"];
                    
                }
                else if(smBkgrdColor && ![smBkgrdColor isEqualToString:@"N/A"])
                {
                    tableView.backgroundColor = [UIColor convertToHexString:smBkgrdColor];
                    
                    self.view.backgroundColor = [UIColor convertToHexString:smBkgrdColor];
                    [defaults setValue:smBkgrdColor forKey:@"menuBkgrdColor"];
                    [defaults setValue:nil forKey:@"menuBkgrdImage"];
                }
                else
                    tableView.backgroundColor = [UIColor whiteColor];
                
            }
            else
            {
                if(mBkgrdImage && ![mBkgrdImage isEqualToString:@"N/A"])
                {
                    SdImageView *asyncImageView;
                    
                    asyncImageView = [[SdImageView alloc] initWithFrame:tableView.frame];
                    
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    asyncImageView.layer.cornerRadius = 5.0f;
                    [asyncImageView loadImage:[mBkgrdImage stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                    asyncImageView.tag=99;
                    [tableView setBackgroundView:asyncImageView];
                    //[asyncImageView release];
                    
                    [defaults setValue:mBkgrdImage forKey:@"menuBkgrdImage"];
                    [defaults setValue:nil forKey:@"menuBkgrdColor"];
                    
                    
                    
                    
                }
                else if(mBkgrdColor && ![mBkgrdColor isEqualToString:@"N/A"])
                {
                    tableView.backgroundColor = [UIColor convertToHexString:mBkgrdColor];
                    
                    [defaults setValue:nil forKey:@"menuBkgrdImage"];
                    [defaults setValue:mBkgrdColor forKey:@"menuBkgrdColor"];
                    
                }
                else
                    tableView.backgroundColor = [UIColor whiteColor];
                
                
            }
        
        
        
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        switch (iWebRequestState)
        {
            case GETCATEGORY:
            {
                if([findCategoryArray count]!=0){
                    CatList *obj_FindCategoryDO = [findCategoryArray objectAtIndex:indexPath.row];
                    
                    categoryTable.tag = 1;
                    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                    btn.frame = cell.frame;
                    
                    btn.backgroundColor = [UIColor convertToHexString:cellColor];
                    
                    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"right_arrow"]];
                    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
                    {
                        [btn setImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [btn setImage:[UIImage imageNamed:@"menuGradientBig.png"] forState:UIControlStateNormal];
                    }
                    btn.layer.borderWidth = 1.0;
                    btn.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
                    cell.backgroundView = btn; //[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menuBtnGradient"]] ;
                    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
                        btn.contentEdgeInsets = UIEdgeInsetsMake(-3, -2, 0, 0);
                    cell.backgroundView = btn;

                  // cell.backgroundColor =  [UIColor grayColor];
                    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                    {
                        prodName.font = [UIFont boldSystemFontOfSize:21];
                        prodName.frame = CGRectMake(70, 15, 650, 50);
                        asyncImageView.frame = CGRectMake(10.0, 12.0, 50.0, 50.0);
                       asyncImageView.clipsToBounds = YES;
                        asyncImageView.layer.cornerRadius = 25.0;
                    }
                    else
                    {
                        prodName.font = [UIFont boldSystemFontOfSize:16];
                        prodName.frame = CGRectMake(60, 5, 250, 40);
                        asyncImageView.frame = CGRectMake(10.0, 5.0, 40.0, 40.0);
                      asyncImageView.clipsToBounds = YES;
                        asyncImageView.layer.cornerRadius = 20.0;
                    }
                    prodName.text = obj_FindCategoryDO.CatDisName;
                    
                    
                    [asyncImageView loadImage:obj_FindCategoryDO.CatImgPth];
                    [cell.contentView addSubview:prodName];
                    [cell.contentView addSubview:asyncImageView];
                }
                
            }
                break;
            default:
                break;
        }
    }
    
	cell.selectionStyle = UITableViewCellSelectionStyleBlue;
	
   // cellViewDictionary = NSDictionaryOfVariableBindings(prodName,catName,asyncImageView,label,DetailLabel);
	ReleaseAndNilify(prodName);
    ReleaseAndNilify(asyncImageView);
    ReleaseAndNilify(catName);
	return cell;
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (categoryTable.tag == 2) {
//        rowNum =[[rowCountArray objectAtIndex:[rowCountArray count]-1] intValue];
//        rowNumSize = (int)[rowCountArray count];
//        lowerLimit = rowNum;
    }
    if (indexPath.row == [prodCountArray count] && nextPage == 1) {
        // This is the last cell
        rowNum =[[rowCountArray objectAtIndex:[rowCountArray count]-1] intValue];
        rowNumSize = (int)[rowCountArray count];
        lowerLimit = rowNum;
        if (![defaults boolForKey:@"ViewMore"] && lowerLimit !=0) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self fetchNextDeals:(nil)];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [categoryTable reloadData];
                });
            });
            
        }
    }
    
}




// Called after the user changes the selection.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [defaults setBool:NO forKey:@"ViewMore"];

    index = [indexPath copy] ;
    tappedSearch=FALSE;
    inCategorySort = false;
    // If any text written in the search bar and will call the location list details then no need to call search service, to avid this cncel.tag=101 changed.
    cancelBtn.tag=101;

//    if(![[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"]){
//    switch ([viewSelectionControl selectedSegmentIndex])
//    {
//        case 0: // Location tab
//        {
//            // Check which state is Active in Location Tab
//            switch (iWebRequestState)
//            {
//                case GETCATEGORY:
//                    [self request_sscatsearch];
//                    break;
//                case SSRETSEARCH:
//                    break;
//                default:
//                    break;
//            }
//            break;
//        }
//        case 1: // Product tab
//        {
//            switch (iWebRequestState)
//            {
//                case GETSMARTSEAPRODS:
//                {
//                    [self request_getsmartseacount:(int)indexPath.row];
//                }
//                    break;
//                case GETPRODSEARCH:
//                {
//                    [self getResultForManulSearchItem:(int)indexPath.row];
//                }
//                    break;
//                default:
//                    break;
//            }
//            break;
//        }
//    }
//    }
// else
    {
        switch (iWebRequestState)
        {
            case GETCATEGORY:
                [self request_sscatsearch];
                break;
            case SSRETSEARCH:
                break;
            default:
                break;
        }
        
    }
    [categoryTable deselectRowAtIndexPath:[categoryTable indexPathForSelectedRow] animated:YES];
}

-(void)fetchNextDeals:(id)sender{
	
	rowNum =[[rowCountArray objectAtIndex:[rowCountArray count]-1] intValue];
	rowNumSize = (int)[rowCountArray count];
    if (nextPage == 1) {
        lowerLimit = rowNum;
        fetchingNextResults = YES;
        [self getProductSearchResult:search.text];
        categoryTable.tag = 2;
        //[categoryTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        [categoryTable reloadData];
    }else {
        return;
    }
    
}

-(void)getResultForManulSearchItem:(int)rowCount        //Changes made on june 30 on View More..
{
//    if (rowCount == [prodCountArray count])
//    {
//        if (nextPage == 1)
//        {
//            [self fetchNextDeals:(nil)];// More records to be shown...pagination
//        }
//        return;
//    }
    
    //if multiple products then navigate to product count page i.e GroupedProductList
    if ([[prodCountArray objectAtIndex:rowCount]intValue]>1)
    {
        iWebRequestState = getsmartsearchprodlist;
        NSMutableString *requestStr = [[NSMutableString alloc] init];
        [defaults setValue:[productName objectAtIndex:rowCount] forKey:@"prodName"];
        //Set ParCatId with value 0 in case of manual serach
        [defaults setValue:@"0" forKey:@"ParCatId"];
        
        [requestStr appendFormat:@"<ProductDetail><searchkey><![CDATA[%@]]></searchkey>", [productName objectAtIndex:rowCount]];
        [requestStr appendFormat:@"<userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
        
        if([defaults valueForKey:KEY_HUBCITIID])
            [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
        
        [requestStr appendFormat:@"<parCatId>0</parCatId><lastVistedProductNo>0</lastVistedProductNo></ProductDetail>"];
        NSString *urlString = [NSString stringWithFormat:@"%@scannow/getsmartsearchprodlist",BASE_URL];
        [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
        //[requestStr release];
    }
    
    else
    {
        //if count is only one then navigate to product page
       // latValue = [defaults valueForKey:KEY_LATITUDE];
        //longValue = [defaults valueForKey:KEY_LONGITUDE];
        
        [defaults  setObject:[productId objectAtIndex:rowCount] forKey:KEY_PRODUCTID];
        [defaults  setObject:[prodListIDArray objectAtIndex:rowCount] forKey:KEY_PRODLISTID];
        
        if([Network currentReachabilityStatus]==0)
        {
            [UtilityManager showAlert];
        }
        else
        {
            [SharedManager setShareFromTL:NO];
            //@Deepak: for UserTracking
            if([prodListIDArray count] > 0)
                [defaults setObject:[prodListIDArray objectAtIndex:rowCount] forKey:KEY_PRODLISTID];
            
            [defaults setValue:@"0" forKey:KEY_SCANTYPEID];
            
            ProductPage *prodPage = [[ProductPage alloc]initWithNibName:@"ProductPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:prodPage animated:NO];
            //[prodPage release];
        }
    }
}

-(void)parse_getsmartsearchprodlist:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *productDetailElement = [TBXML childElementNamed:KEY_PRODUCTDETAIL parentElement:tbXml.rootXMLElement];
    
    if (productDetailElement!=nil) {
        
        iWebRequestState= GETPRODSEARCH;
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        GroupedProductList *prodListPage = [[GroupedProductList alloc]initWithNibName:@"GroupedProductList" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:prodListPage animated:NO];
        //[prodListPage release];
    }
    else
    {
        TBXMLElement *respTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        if (respTextElement!=nil){
            [UtilityManager showAlert:nil msg:[TBXML textForElement:respTextElement]];
        }
        else {
            return;
        }
    }
}


-(void) fitTableView {
    if (animate == 1){
        ////NSlog(@"CHANGE TO  - %f", categoryTable.frame.origin.y);
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [UIView animateWithDuration:0.25 animations:^{
                categoryTableHeightConstraint.constant = 790-264;
                [self.view setNeedsUpdateConstraints];
            }];
        else if (IS_IPHONE5) {
            [UIView animateWithDuration:0.25 animations:^{
                categoryTableHeightConstraint.constant = 130+88;
                [self.view setNeedsUpdateConstraints];
            }];
        }
        else
           [UIView animateWithDuration:0.25 animations:^{
                categoryTableHeightConstraint.constant = 130;
                [self.view setNeedsUpdateConstraints];
            }];
    }
}
-(void) resizeCategoryTableView {
    
//    if(![[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"]){
//        if(bottomBtn == 1){
//            if (animate == 1 && viewSelectionControl.selectedSegmentIndex == 0){
//                
//                if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
//                    categoryTable.frame = CGRectMake(0, 88, categoryTable.frame.size.width, 820);//323
//                else if (IS_IPHONE5) {
//                    
//                    categoryTable.frame = CGRectMake(0, 88, 323, 278+88);
//                    
//                    //[self setConstraints:nil tag:0 heightIphone:278+88 heightIpad:860];
//                }
//                else
//                    categoryTable.frame = CGRectMake(0, 88, 323, 278);
//                //[self setConstraints:nil tag:0 heightIphone:278 heightIpad:860];
//            }
//            
//            else if(animate == 1 && viewSelectionControl.selectedSegmentIndex == 1){
//                if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
//                    categoryTable.frame = CGRectMake(categoryTable.frame.origin.x, 88, categoryTable.frame.size.width, 820);//790 height
//                else if (IS_IPHONE5) {
//                    categoryTable.frame = CGRectMake(0, 88, 323, 278+88);
//                    //categoryTable.frame = CGRectMake(categoryTable.frame.origin.x, 88, categoryTable.frame.size.width, categoryTable.frame.size.height+160);
//                    //[self setConstraints:nil tag:0 heightIphone:categoryTable.frame.size.height+160 heightIpad:830];
//                }
//                else
//                    categoryTable.frame = CGRectMake(categoryTable.frame.origin.x, 88, 323, 278);
//                
//                // categoryTable.frame = CGRectMake(categoryTable.frame.origin.x, 88, categoryTable.frame.size.width, categoryTable.frame.size.height+120);
//                //[self setConstraints:nil tag:0 heightIphone:categoryTable.frame.size.height+120 heightIpad:830];
//            }
//        }
//        else if(bottomBtn == 0){
//            if (animate == 1 && viewSelectionControl.selectedSegmentIndex == 0){
//                
//                if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
//                    categoryTable.frame = CGRectMake(0, 88, categoryTable.frame.size.width, 820+50);//323
//                else if (IS_IPHONE5) {
//                    
//                    categoryTable.frame = CGRectMake(0, 88, 323, 278+88+50);
//                    
//                    //[self setConstraints:nil tag:0 heightIphone:278+88 heightIpad:860];
//                }
//                else
//                    categoryTable.frame = CGRectMake(0, 88, 323, 278+50);
//                //[self setConstraints:nil tag:0 heightIphone:278 heightIpad:860];
//            }
//            
//            else if(animate == 1 && viewSelectionControl.selectedSegmentIndex == 1){
//                if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
//                    categoryTable.frame = CGRectMake(categoryTable.frame.origin.x, 88, categoryTable.frame.size.width, 820+50);//790 height
//                else if (IS_IPHONE5) {
//                    categoryTable.frame = CGRectMake(0, 88, 323, 278+88+50);
//                    //categoryTable.frame = CGRectMake(categoryTable.frame.origin.x, 88, categoryTable.frame.size.width, categoryTable.frame.size.height+160);
//                    //[self setConstraints:nil tag:0 heightIphone:categoryTable.frame.size.height+160 heightIpad:830];
//                }
//                else
//                    categoryTable.frame = CGRectMake(categoryTable.frame.origin.x, 88, 323, 278+50);
//                
//                // categoryTable.frame = CGRectMake(categoryTable.frame.origin.x, 88, categoryTable.frame.size.width, categoryTable.frame.size.height+120);
//                //[self setConstraints:nil tag:0 heightIphone:categoryTable.frame.size.height+120 heightIpad:830];
//            }
//
//        }
//    }
//    else
    {
        if(bottomBtn==1) {
            if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                categoryTable.frame = CGRectMake(0, 44, categoryTable.frame.size.width, 820+48);//323
            else if (IS_IPHONE5) {
                categoryTable.frame = CGRectMake(0, 44, 323, 278+88+44);
                //[self setConstraints:nil tag:0 heightIphone:278+88 heightIpad:860];
            }
            else
                categoryTable.frame = CGRectMake(0, 44, 323, 278+44);
        }
        else{
            if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                categoryTable.frame = CGRectMake(0, 44, categoryTable.frame.size.width, 820+48+50);//323
            else if (IS_IPHONE5) {
                categoryTable.frame = CGRectMake(0, 44, 323, 278+88+50+44);
                //[self setConstraints:nil tag:0 heightIphone:278+88 heightIpad:860];
            }
            else
                categoryTable.frame = CGRectMake(0, 44, 323, 278+44+50);
            
        }
    }
}

-(void)showSplash
{
    UIViewController *modalViewController = [[UIViewController alloc] init];
	modelView.backgroundColor = [UIColor colorWithRGBInt:0xc93133] ;
    
    modalViewController.view = modelView;
    [self presentViewController:modalViewController animated:NO completion:nil];
    ReleaseAndNilify(modalViewController);
    [self performSelector:@selector(hideSplash) withObject:nil afterDelay:1.0];
}

- (void)hideSplash{
    [[self presentedViewController] dismissViewControllerAnimated:NO completion:nil];
}



-(void)returnToMainPage:(id)sender{
	
    cancelBtn.tag = 101;
    [defaults setBool:NO forKey:@"ReturnResult"];
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }


    }
    
    else
    {
	[UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
	[defaults  removeObjectForKey:@"prodSearch"];
   
}

//-(void)requestToGetUserData
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
//    
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
//    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//    ReleaseAndNilify(xmlStr);
//}

-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}
-(void)parseGetUserData:(id)response
{
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
        [HubCitiAppDelegate setIsLogistics:YES];
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}


@end

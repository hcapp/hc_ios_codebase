//
//  FindOptionsSortViewController.m
//  HubCiti
//
//  Created by Keshava on 6/23/14.
//  Copyright (c) 2014 Keshava. All rights reserved.
//
#import "MainMenuViewController.h"
#import "FindOptionsSortViewController.h"
#import "FindOptionsViewController.h"
#import "RetailersListViewController.h"
#import "FilterRetailersList.h"
#import "CityExperienceViewController.h"

NSDictionary *viewDictionary;
float rowHeight = 44, sectionHeaderHeight = 20;

@interface FindOptionsSortViewController ()

@end

@implementation FindOptionsSortViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setViewForSorting
{
        NSString *strS = [defaults valueForKey:@"filtersSort"];
        NSString *strG = [defaults valueForKey:@"NearBySort"];
        
        if (fromFilters==TRUE) {
            if([strS isEqualToString:@"distance"]||[strS isEqualToString:@"Distance"])
                sortSelectionval = 1;
            else if([strS isEqualToString:@"name"])
                sortSelectionval = 0;
        }
        else if (fromNearBy==TRUE)
        {
            if([strG isEqualToString:@"distance"]||[strG isEqualToString:@"Distance"])
                sortSelectionval = 1;
            else if([strG isEqualToString:@"name"])
                sortSelectionval = 0;
        }
        
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            rowHeight = 60;
            sectionHeaderHeight = 30;
        }
    
        tblGroupingNSorting = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,([arrSorting count]*rowHeight)+sectionHeaderHeight) style:UITableViewStylePlain];
        tblGroupingNSorting.dataSource=self;
        tblGroupingNSorting.delegate=self;
        [tblGroupingNSorting setBackgroundColor:[UIColor whiteColor]];
        tblGroupingNSorting.tableFooterView.hidden=YES;
    [tblGroupingNSorting setAccessibilityIdentifier:@"sortTable"];
        [self.view addSubview:tblGroupingNSorting];
        
        [tblGroupingNSorting reloadData];
        
        
        
}
    
   


-(void)cancelButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)doneButtonTouched:(id)sender
{
    // Check the Selected Sorting Value
    
    if (fromNearBy==TRUE) {
        showMapFlag=FALSE;
        if(sortSelectionval == 1)
        {
            [defaults setValue:@"distance" forKey:@"NearBySort"];
                    }
        else if(sortSelectionval == 0)
        {
            
            [defaults setValue:@"atoz" forKey:@"NearBySort"];

        }
//        RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
//        [self.navigationController pushViewController:retailerListScreen animated:NO];
//        //[retailerListScreen release];
//        NSArray *viewControllers = [[self navigationController] viewControllers];
//               for( int i=[viewControllers count]-1;i>=0;i--){
//                    id obj=[viewControllers objectAtIndex:i];
//                    if([obj isKindOfClass:[RetailersListViewController class]]){
//                        [[self navigationController] popToViewController:obj animated:YES];
//                    }
//                }

         [self.navigationController popViewControllerAnimated:NO];
        
    }
    else if (fromFilters==TRUE){
        showMapFlag=FALSE;
        if(sortSelectionval == 1)
             [defaults setValue:@"Distance" forKey:@"filtersSort"];
        
        else if(sortSelectionval == 0)
             [defaults setValue:@"RetailerName" forKey:@"filtersSort"];
        
        if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
            
//            NSArray *viewControllers = [[self navigationController] viewControllers];
//            for( int i=[viewControllers count]-1;i>=0;i--){
//                id obj=[viewControllers objectAtIndex:i];
//                if([obj isKindOfClass:[FilterRetailersList class]]){
//                    [[self navigationController] popToViewController:obj animated:YES];
//                }
//            }
            [UtilityManager popBackToViewController:[FilterRetailersList class] inNavigationController:self.navigationController];
        }
        
        else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
            
//            NSArray *viewControllers = [[self navigationController] viewControllers];
//            for( int i=[viewControllers count]-1;i>=0;i--){
//                id obj=[viewControllers objectAtIndex:i];
//                if([obj isKindOfClass:[FilterRetailersList class]]){
//                    [[self navigationController] popToViewController:obj animated:YES];
//                }
//                DLog(@"%@",obj);
//            }
            [UtilityManager popBackToViewController:[FilterRetailersList class] inNavigationController:self.navigationController];
        }
        
    }
    
    
}

#pragma - Filter Methods
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    else if ([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([NearbyBottomButtonID count]>=1) {
            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[NearbyBottomButtonID objectAtIndex:[NearbyBottomButtonID count]-1]];
        }
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }

    
    
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}



-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case PARTNERRET:
            [self parse_PartnerRet:response];
            break;
        default:
            break;
    }
}

-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
//            [defaults setObject:response forKey:@"Response_Filters"];
//            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
//            [defaults setInteger:200 forKey:@"CityExpTableTag"];
//            [SharedManager setRefreshAustinRetailers:YES];
//            [self.navigationController pushViewController:cevc animated:NO];
//            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            self.filters = pvc;
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}

#pragma View Controller Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setAccessibilityLabel:@"sortandgroup"];
    [super viewDidLoad];
    
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    
    UIButton *btn = [UtilityManager customizeBarButtonItem:@"Done"];
    [btn addTarget:self action:@selector(doneButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = mainPage;
    [self.navigationItem.leftBarButtonItem setAccessibilityLabel:@"Done"];
    //[mainPage release];
    
    UIButton *btncancel = [UtilityManager customizeBarButtonItem:NSLocalizedString(@"Cancel",@"Cancel")];
    [btncancel addTarget:self action:@selector(cancelButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* btncancelbar = [[UIBarButtonItem alloc] initWithCustomView:btncancel];
    self.navigationItem.rightBarButtonItem = btncancelbar;
    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"Cancel"];
    //[btncancelbar release];
    
    self.navigationItem.title = @"Sort";
    
    arrSorting = [[NSArray alloc]initWithObjects:@"Name",@"Distance",nil];
   
    [self setViewForSorting];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table View Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return sectionHeaderHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrSorting count];
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *lblCatName ;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 28)];
    }
    else
    {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 18)];
    }

    [lblCatName setText:@" Sort Items by"];
    [lblCatName setTextColor:[UIColor blackColor]];
    [lblCatName setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        [lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
    else
        [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
    
    return lblCatName;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellEventListView";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.backgroundColor =  [UIColor clearColor];
        
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }
        
    }
    
    UILabel *lblEvtDate = [[UILabel alloc]init];
    [lblEvtDate setText:[arrSorting objectAtIndex:indexPath.row]];
    [lblEvtDate setTextColor:[UIColor blackColor]];
    [lblEvtDate setBackgroundColor:[UIColor clearColor]];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
    else
        [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
    [cell.contentView addSubview:lblEvtDate];
   // [lblEvtDate release];
    lblEvtDate.translatesAutoresizingMaskIntoConstraints = NO;
    viewDictionary = NSDictionaryOfVariableBindings(lblEvtDate);
    [self setConstraints:cell];
    
    if(indexPath.row == sortSelectionval)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    sortSelectionval = (int)indexPath.row;
    
    [tblGroupingNSorting reloadData];
}

-(void)setConstraints:(UITableViewCell*)cell
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    if (  DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        // Vertical constrains
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(20)-[lblEvtDate(20)]"] options:0 metrics:0 views:viewDictionary]];
        
        // Horizontal constraints
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(12)-[lblEvtDate(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
    }
    else
    {
        // Vertical constrains
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(12)-[lblEvtDate(20)]"] options:0 metrics:0 views:viewDictionary]];
        
        // Horizontal constraints
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[lblEvtDate(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
    }
}

@end

//
//  SearchResultCount.m
//  Scansee
//
//  Created by ajit on 9/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "SearchResultCount.h"
#import "AppDelegate.h"
#import "ProductPage.h"
#import "MainMenuViewController.h"
#import "GroupedProductList.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

NSDictionary *viewDictionary;

@interface SearchResultCount ()

@end

@implementation SearchResultCount{
    UIActivityIndicatorView *loading;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = NSLocalizedString(@"Search Result",@"Search Result");
//    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
//    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
//    loading = nil;
//    self.navigationItem.rightBarButtonItem = mainPage;
//    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
   
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;
   // //[mainPage release];

    
    /*UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithTitle:BACKMENU style:UIBarButtonItemStylePlain target:nil action:nil];
     self.navigationItem.backBarButtonItem = back;
     //[back release];*/
    
    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    prodListIDArray = [[NSMutableArray alloc]init];
    prodNameArray = [[NSMutableArray alloc]init];
    prodCountArray = [[NSMutableArray alloc]init];
    prodIdArray = [[NSMutableArray alloc]init];
    rowNumArray = [[NSMutableArray alloc]init];
    prodDescArray = [[NSMutableArray alloc]init];
    prodImgPathArray = [[NSMutableArray alloc]init];
    lastVisitedRecord = 0;
    
    [self parseSmartSearchCountResult:[defaults objectForKey:KEY_RESPONSEXML]];
    [searchResultTable reloadData];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [searchResultTable deselectRowAtIndexPath:[searchResultTable indexPathForSelectedRow] animated:YES];
}
-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}


#pragma REQUEST Methods

-(void)request_getsmartsearchcount
{
    iWebRequestState = GETSMARTSEARCHCOUNT;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ProductDetail><searchkey><![CDATA[%@]]></searchkey>", [defaults valueForKey:@"SearchString"]];
    
    if([defaults  valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
    if([defaults  valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_HUBCITIID]];
    
    
    [requestStr appendFormat:@"<parCatId>%@</parCatId><lastVistedProductNo>%d</lastVistedProductNo></ProductDetail>",[defaults valueForKey:@"ParCatId"],lastVisitedRecord];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@scannow/getsmartsearchcount",BASE_URL];
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
        
        
        [self parseSmartSearchCountResult:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    }

    //[ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    //[requestStr release];
}

-(void)request_getsmartsearchprodlist:(int)rowIndex
{
    iWebRequestState = GETSMARTSEARCHPRODLIST;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    
    [defaults setValue:[prodNameArray objectAtIndex:rowIndex] forKey:@"prodName"];
    [requestStr appendFormat:@"<ProductDetail><searchkey><![CDATA[%@]]></searchkey>", [prodNameArray objectAtIndex:rowIndex]];
    [requestStr appendFormat:@"<parCatId>%@</parCatId><lastVistedProductNo>0</lastVistedProductNo></ProductDetail>",[defaults valueForKey:@"ParCatId"]];
    
    [requestStr appendFormat:@"<userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></ProductDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSMutableString *urlString = [BASE_URL mutableCopy];
    [urlString appendFormat:@"scannow/getsmartsearchprodlist"];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    //[requestStr release];
    
}



// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case GETSMARTSEARCHCOUNT:
        {
            [defaults  setObject:response forKey:KEY_RESPONSEXML];
            [self parseSmartSearchCountResult:response];
            [searchResultTable reloadData];
        }
            break;
        case GETSMARTSEARCHPRODLIST:
        {
            [self parse_getsmartsearchprodlist:response];
        }
            
        default:
            break;
    }
    
}


#pragma PARSE METHOD
-(void)parse_getsmartsearchprodlist:(NSString*)responseXml
{
    [self parseProductListResult:responseXml];
}

-(void)parseProductListResult:(NSString*)responseXml{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *productDetailElement = [TBXML childElementNamed:KEY_PRODUCTDETAIL parentElement:tbXml.rootXMLElement];
    
    if (productDetailElement!=nil) {
        
        [defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
        
        //making use of existing, not used, file GroupedProductList
        GroupedProductList *prodListPage = [[GroupedProductList alloc]initWithNibName:@"GroupedProductList" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:prodListPage animated:NO];
       // [prodListPage release];
    }
    else {
        TBXMLElement *respTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        if (respTextElement!=nil){
            [UtilityManager showAlert:[TBXML textForElement:respTextElement] msg:@" "];
            
           
        }
        else {
            return;
        }
    }
}


-(void)parseSmartSearchCountResult:(NSString*) responseXml{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *productDetailElement = [TBXML childElementNamed:KEY_PRODUCTDETAIL parentElement:tbXml.rootXMLElement];
    
    TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbXml.rootXMLElement];
    if (nextPageElement != nil)
        nextPage = [[TBXML textForElement:nextPageElement] intValue];

    while (productDetailElement != nil) {
        TBXMLElement *productNameElement = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:productDetailElement];
        TBXMLElement *prdCountElement = [TBXML childElementNamed:@"prdCount" parentElement:productDetailElement];
        TBXMLElement *prodIdElement = [TBXML childElementNamed:@"productId" parentElement:productDetailElement];
        TBXMLElement *rowNumElement = [TBXML childElementNamed:@"rowNumber" parentElement:productDetailElement];
        TBXMLElement *prodImgPathElement = [TBXML childElementNamed:@"productImagePath" parentElement:productDetailElement];
        TBXMLElement *prodDescElement = [TBXML childElementNamed:@"productDescription" parentElement:productDetailElement];

        TBXMLElement *prodListIDElement = [TBXML childElementNamed:KEY_PRODLISTID parentElement:productDetailElement];
        
        if(prodListIDElement !=nil)
            [prodListIDArray addObject:[TBXML textForElement:prodListIDElement]];
        
        [prodIdArray addObject:[TBXML textForElement:prodIdElement]];
        [prodNameArray addObject:[TBXML textForElement:productNameElement]];
        [prodCountArray addObject:[TBXML textForElement:prdCountElement]];
        [rowNumArray addObject:[TBXML textForElement:rowNumElement]];
        [prodDescArray addObject:[TBXML textForElement:prodDescElement]];
        [prodImgPathArray addObject:[TBXML textForElement:prodImgPathElement]];
        
        productDetailElement = [TBXML nextSiblingNamed:KEY_PRODUCTDETAIL searchFromElement:productDetailElement];
    }
    [defaults setBool:NO forKey:@"ViewMore"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(nextPage == 1)
        return [prodNameArray count]+1;
    else
        return [prodNameArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    UILabel  *textLabel = nil, *prodDesc = nil;
    SdImageView *asyncImageView = nil;
   // float viewmoreLableFont = 16.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
       // viewmoreLableFont = 20.0;
    }
    if (indexPath.row == [prodCountArray count] && nextPage == 1) {
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
//        CGRect frame;
//        frame.origin.x = 0;
//        frame.origin.y = 10;
//        frame.size.width = SCREEN_WIDTH;
//        frame.size.height = 24;
//        label = [[[UILabel alloc] init] ;
//        label.font = [UIFont boldSystemFontOfSize:viewmoreLableFont];
//        label.textAlignment = NSTextAlignmentCenter;
//        label.textColor = [UIColor colorWithRGBInt:0x112e72];
//        label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
//        cell.accessoryType = UITableViewCellAccessoryNone;
//        [cell.contentView addSubview:label];
//        label.translatesAutoresizingMaskIntoConstraints = NO;
//        viewDictionary = NSDictionaryOfVariableBindings(label);
    }
    else {
        textLabel = [[UILabel alloc]init];
        [textLabel setBackgroundColor:[UIColor clearColor]];
        textLabel.numberOfLines = 2;
        //textLabel1.adjustsFontSizeToFitWidth = YES;
        textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        textLabel.text = [NSString stringWithFormat:@"%@",[prodNameArray objectAtIndex:indexPath.row]];
        //CGSize textSize = [[textLabel1 text] sizeWithFont:[textLabel1 font]];
        
        if (![[prodImgPathArray objectAtIndex:indexPath.row]isEqualToString:@"N/A"]){
            asyncImageView = [[SdImageView alloc] init];
            asyncImageView.backgroundColor = [UIColor clearColor];
            [asyncImageView loadImage:[NSString stringWithFormat:@"%@",[prodImgPathArray objectAtIndex:indexPath.row]]];
            asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
            [cell.contentView addSubview:asyncImageView];
            //[asyncImageView release];
        }
        
        prodDesc = [[UILabel alloc]init];
        prodDesc.backgroundColor = [UIColor clearColor];
        prodDesc.textColor = [UIColor grayColor];
        prodDesc.lineBreakMode = NSLineBreakByWordWrapping;
        prodDesc.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
 
        if ([[prodCountArray objectAtIndex:indexPath.row]intValue]>1){
            
            prodDesc.text = [NSString stringWithFormat:@"(%@ items)",[prodCountArray objectAtIndex:indexPath.row]];
        }
        else if ([[prodCountArray objectAtIndex:indexPath.row]intValue]==1){
            
            prodDesc.text = [prodDescArray objectAtIndex:indexPath.row];
            prodDesc.text = [NSString removeHtmlTags:prodDesc.text];
            
        }
        
        [cell.contentView addSubview:textLabel];
        [cell.contentView addSubview:prodDesc];
        textLabel.translatesAutoresizingMaskIntoConstraints = NO;
        prodDesc.translatesAutoresizingMaskIntoConstraints = NO;
        asyncImageView.translatesAutoresizingMaskIntoConstraints = NO;
        viewDictionary = NSDictionaryOfVariableBindings(textLabel, prodDesc, asyncImageView);
        [self setConstraints:cell];
        ReleaseAndNilify(textLabel);
        ReleaseAndNilify(prodDesc);
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 55.0;
    }
    else
    {
        return 70.0;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == [prodCountArray count] && nextPage == 1) {
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"] && lastVisitedRecord!=0) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self fetchNextPage:self];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [searchResultTable reloadData];
                });
            });
            
        }
    }
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    index = indexPath;
//    if (indexPath.row == [prodCountArray count]) {
//        if (nextPage == 1) {
//            [self fetchNextPage:self];// More records to be shown...pagination
//            DLog(@"NEXT PAGE - %d", nextPage);
//        }
//        return;
//    }
    [defaults setBool:NO forKey:@"ViewMore"];

    if ([[prodCountArray objectAtIndex:indexPath.row]intValue]>1)
    {
        [self request_getsmartsearchprodlist:(int)indexPath.row];
    }
    
    else if ([[prodCountArray objectAtIndex:indexPath.row]intValue]==1 && [SharedManager fromSL] == YES) {
        
        if([Network currentReachabilityStatus]==0)
        {
            [UtilityManager showAlert];
        }
        else {
            
            if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"fav"]) {
                [self addToFavorites];
            }
            if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"list"]) {
                [self addToList];
            }
            if ([[defaults  valueForKey:KEY_SEARCHFROM] isEqualToString:@"wish"]) {
                [self addToWishList];
            }
        }
    }
    
    else {
        [defaults  setObject:[prodIdArray objectAtIndex:indexPath.row] forKey:KEY_PRODUCTID];
        
        if([Network currentReachabilityStatus]==0)
        {
            [UtilityManager showAlert];
        }
        else
        {
			[[HubCitiManager sharedManager]setShareFromTL:NO];
            
            [defaults setObject:[prodListIDArray objectAtIndex:indexPath.row] forKey:KEY_PRODLISTID];
            
            [defaults setValue:@"0" forKey:KEY_SCANTYPEID];
			ProductPage *prodPage = [[ProductPage alloc]initWithNibName:@"ProductPage" bundle:[NSBundle mainBundle]];
			[self.navigationController pushViewController:prodPage animated:NO];
			//[prodPage release];
            
        }
    }
}

-(void)setConstraints:(UITableViewCell*)cell
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        if([viewDictionary objectForKey:@"asyncImageView"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[asyncImageView(60)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(60)]"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"textLabel"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[textLabel(35)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(70)-[textLabel(650)]"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"prodDesc"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(45)-[prodDesc(15)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(70)-[prodDesc(650)]"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"label"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(24)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(320)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
    }
    else
    {
        if([viewDictionary objectForKey:@"asyncImageView"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[asyncImageView(50)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(50)]"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"textLabel"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[textLabel(35)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(60)-[textLabel(235)]"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"prodDesc"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(40)-[prodDesc(15)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(60)-[prodDesc(240)]"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"label"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(24)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(320)]"] options:0 metrics:0 views:viewDictionary]];
        }
    }
}


-(void) addToList {
    
	NSMutableString *requestStr = [[NSMutableString alloc] init];
	[requestStr appendFormat:@"<AddShoppingList><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
	[requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", [prodIdArray objectAtIndex:index.row]];
	[requestStr appendFormat:@"<productName><![CDATA[%@]]></productName>", [prodNameArray objectAtIndex:index.row]];
    
    [requestStr appendString:@"</ProductDetail></productDetails>"];
    [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID></AddShoppingList>", [defaults  valueForKey:KEY_MAINMENUID]];
	
	NSMutableString *urlString = [BASE_URL mutableCopy];
	[urlString appendFormat:@"shoppingList/addtslbysearch"];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml]) {
        ReleaseAndNilify(responseXml);
        return;
    }
	[defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
    
	TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
	TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
	TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
	if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
        [SharedManager setRefreshList:YES];
		[defaults  removeObjectForKey:KEY_SEARCHFROM];
    }
    
	else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
		
	}
	
	//[responseXml release];
	
}

-(void) addToFavorites {
    
	NSMutableString *requestStr = [[NSMutableString alloc] init];
	[requestStr appendFormat:@"<AddShoppingList><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
	[requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", [prodIdArray objectAtIndex:index.row]];
	[requestStr appendFormat:@"<productName><![CDATA[%@]]></productName>", [prodNameArray objectAtIndex:index.row]];
    
	[requestStr appendString:@"</ProductDetail></productDetails>"];
    [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID></AddShoppingList>", [defaults  valueForKey:KEY_MAINMENUID]];
	
	NSMutableString *urlString = [BASE_URL mutableCopy];
	[urlString appendFormat:@"shoppingList/addproducttosl"];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml]){
        ReleaseAndNilify(responseXml);
        return;
    }
	[defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
	TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
	TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
	TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
	if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
        [SharedManager setRefreshList:YES];
		//[UtilityManager pushPopToFav:self.navigationController];
		[defaults  removeObjectForKey:KEY_SEARCHFROM];
	}
	else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
		
	}
	
	//[responseXml release];
	
}

-(void) addToWishList {
	
	NSMutableString *requestStr = [[NSMutableString alloc] init];
	[requestStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
	[requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", [prodIdArray objectAtIndex:index.row]];
	[requestStr appendFormat:@"<productName><![CDATA[%@]]></productName>", [prodNameArray objectAtIndex:index.row]];
    [requestStr appendString:@"</ProductDetail></productDetails>"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID>", [defaults  valueForKey:KEY_MAINMENUID]];
    [requestStr appendString:@"</ProductDetailsRequest>"];

	
	NSMutableString *urlString = [BASE_URL mutableCopy];
	[urlString appendFormat:@"wishlist/addWishListProd"];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml]) {
        ReleaseAndNilify(responseXml);
        return;
    }
	[defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
	TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
	TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
	TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
	if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
		//[UtilityManager pushPopToWishList:self.navigationController];
		[defaults  removeObjectForKey:KEY_SEARCHFROM];
	}
	else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
		
	}
	
	//[responseXml release];
	
}


-(void)fetchNextPage:(id)sender{
    
    if (nextPage == 1) {
        
        lastVisitedRecord = [[rowNumArray objectAtIndex:[rowNumArray count]-1 ]intValue];
        
        [self request_getsmartsearchcount];
        
    }
    else {
		return;
	}
}





-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
       // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
	[UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

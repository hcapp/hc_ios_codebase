//
//  FindLocationDetailsViewController.m
//  Scansee
//
//  Created by Chaitra on 29/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FindLocationDetailsViewController.h"
#import "WebBrowserViewController.h"
#import "MainMenuViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "AppDelegate.h"

@implementation FindLocationDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    //Resetting GPS flag if user has turned it on
    
    //if ([CLLocationManager  authorizationStatus] != kCLAuthorizationStatusDenied) {
    //    [defaults  setObject:@"YES" forKey:@"allowingGPS"];
   // }

    infoKeys = [[NSMutableArray alloc] init];
    infoValue = [[NSMutableArray alloc] init];
    
    detailTv = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 480) style:UITableViewStylePlain];
    detailTv.delegate = self;
    detailTv.dataSource = self;
    [self.view addSubview:detailTv];
    
    /*UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithTitle:BACKMENU style:UIBarButtonItemStylePlain target:nil action:nil];
	self.navigationItem.backBarButtonItem = back;
	//[back release];*/
    
  
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    //set the main menu 
//    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
//    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
//    self.navigationItem.rightBarButtonItem = mainPage;
//    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];

    
    [self parseDetails];
    [detailTv reloadData];
    // Do any additional setup after loading the view from its nib.
}
-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
}
-(void)popBackToPreviousPage{
    //[keyWord release];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void) parseDetails {
    
    NSString *resp = [defaults  objectForKey:KEY_RESPONSEXML];
    TBXML *tbXml = [TBXML tbxmlWithXMLString:resp];
    TBXMLElement *address = [TBXML childElementNamed:@"address" parentElement:tbXml.rootXMLElement];
    TBXMLElement *phoneNumber = [TBXML childElementNamed:@"formatted__phone__number" parentElement:tbXml.rootXMLElement];
    TBXMLElement *url = [TBXML childElementNamed:@"url" parentElement:tbXml.rootXMLElement];
    if (address != nil) {
        [infoKeys addObject:@"Address"];
        [infoValue addObject:[TBXML textForElement:address]];
    }
    if (phoneNumber != nil) {
        [infoKeys addObject:@"PhoneNumber"];
        [infoValue addObject:[TBXML textForElement:phoneNumber]];
    }
    if (url != nil) {
        [infoKeys addObject:@"Url"];
        [infoValue addObject:[TBXML textForElement:url]];
    }
    DLog(@"%@",infoValue);    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [infoValue count];
}

//Chance's Edits
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 80.0;
    }  
    else {
        return 60.0;
    }
    
}
//End Chance's Edits

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;/* = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];*/
	
	//if( cell == nil)
	cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"] ;   
    
    int flag = 0;
    if ([[infoKeys objectAtIndex:indexPath.row] isEqualToString:@"Address"]) {
        flag = 1;
    }
    else if ([[infoKeys objectAtIndex:indexPath.row] isEqualToString:@"PhoneNumber"]) {
        flag = 2;
    }
    else if ([[infoKeys objectAtIndex:indexPath.row] isEqualToString:@"Url"]) {
        flag = 3;
    }
        
    //Chance's Edits
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundColor = [UIColor whiteColor];
    cell.imageView.frame = CGRectMake(5, 5, 40, 40);
        
    UILabel *textLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 8, 200, 18)];
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.font = [UIFont boldSystemFontOfSize:16];
    textLabel.textColor = [UIColor colorWithRGBInt:0x112e72];
    
    UILabel *detailTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 38, 200, 14)];
	detailTextLabel.backgroundColor = [UIColor clearColor];
	detailTextLabel.font = [UIFont boldSystemFontOfSize:13];
    detailTextLabel.adjustsFontSizeToFitWidth = YES;
    detailTextLabel.minimumScaleFactor = 10.0;
    
    //End Chance's Edits
    
    switch (flag) {
        case 1:
        {
            detailTextLabel.text = [infoValue objectAtIndex:indexPath.row];
            detailTextLabel.numberOfLines = 2;
            [detailTextLabel setLineBreakMode:NSLineBreakByWordWrapping];
            
            //Chance's Edits
            detailTextLabel.frame = CGRectMake(60, 32, 200, 40);
            detailTextLabel.adjustsFontSizeToFitWidth = YES;
            detailTextLabel.minimumScaleFactor = 10.0;
            textLabel.text = @"Get Directions";
            
            cell.imageView.image = [UIImage imageNamed:@"FNB_getdirnivn.png"];

            //End Chance's Edits
            
        }
            break;
        case 2:
        {
            //Chance's Edits
            detailTextLabel.text = [infoValue objectAtIndex:indexPath.row];
            textLabel.text = NSLocalizedString(@"Call Location",@"Call Location");
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            callStore = [UIButton buttonWithType:UIButtonTypeCustom];
            callStore.tag = indexPath.row;
            [callStore setEnabled:YES];
            callStore.frame = CGRectMake(0, 0, 320, 60);
            [callStore setBackgroundColor:[UIColor clearColor]];
            [callStore setShowsTouchWhenHighlighted:YES];
            [callStore addTarget:self action:@selector(callPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:callStore];

            cell.imageView.image = [UIImage imageNamed:@"phone_icon.png"];
            //End Chance's Edits
        }
            break;
        case 3:
        {
            detailTextLabel.text = NSLocalizedString(@"More Info",@"More Info");
            //Chance's Edits
            textLabel.text = NSLocalizedString(@"See More Information",@"See More Information");
            //NSURL *asyncImgUrl = [NSURL URLWithString:imgPathUrl];
            cell.imageView.image = [UIImage imageNamed:@"FNB_bwseIcn.png"];
            
            //End Chance's Edits
        }
            break;
        default:
            break;
    }
    [cell addSubview:detailTextLabel];
    //Chance's Edits
    [cell addSubview:textLabel];
    //End Chance's Edits
    
    ReleaseAndNilify(textLabel);
    ReleaseAndNilify(detailTextLabel);
	return cell;
}

-(void) callPressed: (UIButton *)sender{
  
    NSString *phoneString  = [[[infoValue objectAtIndex:sender.tag] componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
    NSString *phoneNumber = [NSString stringWithFormat:@"tel:%@", phoneString];
    NSURL    *url= [NSURL URLWithString:phoneNumber];
    
    UIWebView *webview = [[UIWebView alloc] initWithFrame:[callStore frame]];
    webview.alpha = 0.0;
    
    [webview loadRequest:[NSURLRequest requestWithURL:url]];
    
    // Assume we are in a view controller and have access to self.view
    [self.view insertSubview:webview belowSubview:callStore];
   // [webview release];
    
}

//Chance's Edits

-(void) websitePressed: (int) index {
    [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
    [defaults  setObject:[infoValue objectAtIndex:index] forKey:KEY_URL];
    WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:urlDetail animated:NO];
    //[urlDetail release];
}

-(void) directionsPressed {
    
    //Checking to see if LS is on, or if allowed to be. If not, we dont need to try and calculate directions, as we may not have a valid Lat and Long in the standardDefaults. This is why we were getting the Invalid Region error.
    
    if ([HubCitiAppDelegate checkZipAndLocation] != 0) {
        [defaults setValue:@"" forKey:@"Directions"];
    }
    else {
        if (![HubCitiAppDelegate locationServicesOn]) {
            [defaults setValue:@"" forKey:@"Directions"];
        }
        else {
            [defaults setValue:@"Directions" forKey:@"Directions"];
        }
    }
    
//    FindNearByRetailerMap *retailerMap = [[FindNearByRetailerMap alloc]initWithNibName:@"FindNearByRetailerMap" bundle:[NSBundle mainBundle]];
//    [self.navigationController pushViewController:retailerMap animated:NO];
//    [retailerMap release];
}

//End Chance's Edits
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //Chance's Edits
 /*   if (indexPath.row == 0) {
        [self directionsPressed];
    }
    if (indexPath.row == 1) {
        [self callPressed:(callStore)];
    }
    if (indexPath.row == 2) {
        [self websitePressed:indexPath.row];
    }
  */
    
    int flag = 0;
    if ([[infoKeys objectAtIndex:indexPath.row] isEqualToString:@"Address"]) {
        flag = 0;
    }
    else if ([[infoKeys objectAtIndex:indexPath.row] isEqualToString:@"PhoneNumber"]) {
        flag = 1;
    }
    else if ([[infoKeys objectAtIndex:indexPath.row] isEqualToString:@"Url"]) {
        flag = 2;
    }
    switch (flag) {
        case 0:
        {
           [self directionsPressed];
        }
            break;
        case 1:
        {
            [self callPressed:(callStore)];
        }
            break;  
        case 2:
        {
           [self websitePressed:(int)indexPath.row];
        }
            break; 
        default:
            break;
    }
    
    /*
    int flag = 0;
    if ([[infoKeys objectAtIndex:indexPath.row] isEqualToString:@"Address"]) {
        flag = 0;
    }
    else if ([[infoKeys objectAtIndex:indexPath.row] isEqualToString:@"PhoneNumber"]) {
        flag = 0;
    }
    else if ([[infoKeys objectAtIndex:indexPath.row] isEqualToString:@"Url"]) {
        flag = 1;
    }
    switch (flag) {
        case 0:
            return;
            break;
        case 1:
        {
            [defaults  setObject:[infoValue objectAtIndex:indexPath.row] forKey:KEY_URL];
            UrlDetailViewController *urlDetail = [[UrlDetailViewController alloc]initWithNibName:@"UrlDetailViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:YES];
            //[urlDetail release];
        }
            break; 
        default:
            break;
    } */
    //End Chance's Edits
    [detailTv deselectRowAtIndexPath:[detailTv indexPathForSelectedRow] animated:YES];
}



-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
       // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    
    else
    {
    [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

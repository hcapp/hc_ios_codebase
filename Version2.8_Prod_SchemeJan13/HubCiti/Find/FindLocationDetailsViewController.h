//
//  FindLocationDetailsViewController.h
//  Scansee
//
//  Created by Chaitra on 29/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FindLocationDetailsViewController : UIViewController <UITableViewDataSource ,UITableViewDelegate ,CustomizedNavControllerDelegate>
{
    UITableView *detailTv;
    NSMutableArray *infoKeys ,*infoValue;
    UIButton *callStore;
    
    //Chance's Edits
    UIButton *getDirections;
    UIButton *website;
    NSString *imgPathDir;
    NSString *imgPathUrl;
    //End Chance's Edits
    
}
-(void) parseDetails;
-(void) callPressed: (UIButton *)sender;


@end

//
//  FindOptionsSortViewController.h
//  HubCiti
//
//  Created by Keshava on 6/23/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>

BOOL fromFindFiltersSort;
@class FilterRetailersList;
@interface FindOptionsSortViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *tblGroupingNSorting;
    NSArray *arrGroups;
    NSArray *arrSorting;
    int groupSelectionVal;
    int sortSelectionval;

     WebRequestState iWebRequestState;
}
@property(nonatomic,strong) FilterRetailersList *filters;
@end

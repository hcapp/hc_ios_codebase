//
//  FindSortViewController.m
//  HubCiti
//
//  Created by Keerthi on 24/06/14.
//  Copyright (c) 2014 span infotech india ptv. ltd. All rights reserved.
//

#import "FindSortViewController.h"
#import "FindLocationServiceViewController.h"
#import "Multivalue.h"
float rowHeightForSort = 44;
float sectionHeaderHeightForSort = 26;
NSDictionary *viewDictionary, *pickerViewDictionary, *pickerRowDictionary;

@interface FindSortViewController () {
    
    NSIndexPath *globalIndexpath;
}

@end

@implementation FindSortViewController
@synthesize typeStr,selectedFilterId,selectedFilterValueIds,OptionsDic,filterCount,catId,srchKey,filterId;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    UIButton *btn = [UtilityManager customizeBarButtonItem:@"Done"];
    [btn addTarget:self action:@selector(doneButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = mainPage;
    [self.navigationItem.leftBarButtonItem setAccessibilityLabel:@"Done"];
    //[mainPage release];
    
    UIButton *btncancel = [UtilityManager customizeBarButtonItem:NSLocalizedString(@"Cancel",@"Cancel")];
    [btncancel addTarget:self action:@selector(cancelButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* btncancelbar = [[UIBarButtonItem alloc] initWithCustomView:btncancel];
    self.navigationItem.rightBarButtonItem = btncancelbar;
    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"Cancel"];
    //[btncancelbar release];
//    arrGroups = [[NSMutableArray alloc]initWithObjects:nil];
//    arrSorting = [[NSArray alloc]initWithObjects:@"Name",@"Distance",nil];
    
    arrDinningTypes = [[NSMutableArray alloc]init];
    filterCount = [[NSMutableArray alloc]init];
    filterId = [[NSMutableArray alloc]init];
    OptionsDic = [[NSMutableDictionary alloc]init];
   
   
   
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        rowHeightForSort = 60;
        sectionHeaderHeightForSort = 36;
    }
    
    if (![typeStr isEqualToString:@""]) {
        [self Request_filter];
        [self Request_fetcheventdetail];
    }
    
    else{
        [self setViewForGroupingNSorting];
    }
    
}


-(void)cancelButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)doneButtonTouched:(id)sender
{
    [defaults setValue:@"YES" forKey:@"isComingFromGroupingandSorting"];
    
    if([arrDinningTypes count]== 0)
        [defaults setValue:@"0" forKey:@"SubCategoryId"];
    
    // Check the Selected Sorting Value
    if(sortSelectionval == 0){
        [defaults setValue:@"name" forKey:@"SortFindBy"];
    }
    
    else if(sortSelectionval == 1){
        [defaults setValue:@"Distance" forKey:@"SortFindBy"];
    }
    
    
    if ([selectedFilterValueIds count]==0) {
        [defaults setValue:nil forKey:@"SelectedFilterValueIds"];
        [defaults setValue:nil forKey:@"commaSeperatedFilterValueIds"];
        [defaults setValue:@"1" forKey:@"afterFilterValueSelection"];
    }
    
//    else if ([selectedFilterId count]== [filterId count]){
//        [defaults setValue:nil forKey:@"SelectedFilterValueIds"];
//        [defaults setValue:nil forKey:@"commaSeperatedFilterValueIds"];
//        [defaults setValue:@"1" forKey:@"afterFilterValueSelection"];
//    }
    
    else{
        [defaults setValue:nil forKey:@"afterFilterValueSelection"];
        [defaults setValue:selectedFilterValueIds forKey:@"SelectedFilterValueIds"];
        NSMutableString *requestStr = [[NSMutableString alloc] init];
        for (int i=0; i<[selectedFilterValueIds count]; i++) {
            
            [requestStr appendFormat:@"%@,", selectedFilterValueIds[i]];
        }
        
        if ([requestStr hasSuffix:@","])
            [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
        [defaults setValue:requestStr forKey:@"commaSeperatedFilterValueIds"];
        
    }
    
    
    if ([selectedFilterId count]==0) {
        [defaults setValue:nil forKey:@"SelectedFilterId"];
        [defaults setValue:nil forKey:@"commaSeperatedFilterIds"];
        [defaults setValue:@"1" forKey:@"afterFilterIdSelection"];
    }
    
//    else if ([selectedFilterId count]== [filterId count]){
//        [defaults setValue:nil forKey:@"SelectedFilterValueIds"];
//        [defaults setValue:nil forKey:@"commaSeperatedFilterValueIds"];
//        [defaults setValue:@"1" forKey:@"afterFilterValueSelection"];
//    }
    
    else{
        [defaults setValue:nil forKey:@"afterFilterIdSelection"];
        [defaults setValue:selectedFilterId forKey:@"SelectedFilterId"];
        NSMutableString *requestStr = [[NSMutableString alloc] init];
        for (int i=0; i<[selectedFilterId count]; i++) {
            
            [requestStr appendFormat:@"%@,", selectedFilterId[i]];
        }
        
        if ([requestStr hasSuffix:@","])
            [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
        [defaults setValue:requestStr forKey:@"commaSeperatedFilterIds"];
        
    }

    
    if (tappedSearch==FALSE) {
        [self request_sortcatretailersWithsubCatId:[defaults valueForKey:@"SubCategoryId"] sortBy:[defaults valueForKey:@"SortFindBy"] groupBy:[defaults valueForKey:@"GroupFindBy"] searchKey:@"" fValueIds:[defaults valueForKeyPath:@"commaSeperatedFilterValueIds"]  filterIds:[defaults valueForKeyPath:@"commaSeperatedFilterIds"]];

        [UtilityManager popBackToViewController:[FindLocationServiceViewController class] inNavigationController:self.navigationController];

    }
    else
    {
        fromSortFind=TRUE;        

        [UtilityManager popBackToViewController:[FindLocationServiceViewController class] inNavigationController:self.navigationController];
    }


}

-(void)showpickerView
{
    tblGroupingNSorting.userInteractionEnabled = NO;
    
    NSMutableDictionary *dic = [arrDinningTypes objectAtIndex:0];
    [defaults setValue:[dic valueForKey:@"catId"] forKey:@"SubCategoryId"];
    [defaults setValue:[dic valueForKey:@"catName"] forKey:@"SubCategoryName"];
    
    view_picker = [[UIView alloc]init];
    view_picker.backgroundColor = [UIColor clearColor];
    view_picker.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    view_picker.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    view_picker.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    view_picker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    if(dinningTypePickerView)
    {
        [dinningTypePickerView removeFromSuperview];
        //[dinningTypePickerView release];
    }
    
    dinningTypePickerView = [[UIPickerView alloc] init];
    dinningTypePickerView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    dinningTypePickerView.delegate = self;
    dinningTypePickerView.dataSource = self;
    dinningTypePickerView.showsSelectionIndicator = YES;
    dinningTypePickerView.backgroundColor=[UIColor whiteColor];
    [view_picker addSubview:dinningTypePickerView];
    
    if(pickerToolbar)
    {
        [pickerToolbar removeFromSuperview];
        //[pickerToolbar release];
    }
    
    pickerToolbar = [[UIToolbar alloc] init];
    pickerToolbar.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
   // [flexSpace release];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDone:)];
    [barItems addObject:doneBtn];
   // [doneBtn release];
    
    UILabel *lblTitle = [[UILabel alloc]init];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.text = @"Select Type";
    lblTitle.textColor = [UIColor whiteColor];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        lblTitle.font = [UIFont boldSystemFontOfSize:20];
    else
        lblTitle.font = [UIFont boldSystemFontOfSize:15];
    lblTitle.textAlignment= NSTextAlignmentCenter;
    [pickerToolbar addSubview:lblTitle];
   // [lblTitle release];
    
    [pickerToolbar setItems:barItems animated:YES];
   // [barItems release];
    [view_picker addSubview:pickerToolbar];
    [self.view addSubview:view_picker];
    
    view_picker.translatesAutoresizingMaskIntoConstraints = NO;
    dinningTypePickerView.translatesAutoresizingMaskIntoConstraints = NO;
    pickerToolbar.translatesAutoresizingMaskIntoConstraints = NO;
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    pickerViewDictionary = NSDictionaryOfVariableBindings(view_picker, dinningTypePickerView, pickerToolbar, lblTitle);
    [self setConstraints:nil tag:1];
    
    [dinningTypePickerView reloadAllComponents];
    [self.view bringSubviewToFront:dinningTypePickerView];
}

-(void)pickerDone:(id)sender
{
    tblGroupingNSorting.userInteractionEnabled = YES;
    
    
    [pickerToolbar removeFromSuperview];
    [dinningTypePickerView removeFromSuperview];
    [view_picker removeFromSuperview];
    
  //  [pickerToolbar release];
    pickerToolbar = nil;
   // [dinningTypePickerView release];
    dinningTypePickerView = nil;
    
    ReleaseAndNilify(view_picker);
    
    // [arrDinningTypes removeAllObjects];
}


-(void)setViewForGroupingNSorting
{
    if([arrDinningTypes count]!=0)
    {
        if ([[defaults valueForKey:@"SubCategoryId"]isEqualToString:@"0"]) {
            groupSelectionVal=-1;
        }
        else
        {
            groupSelectionVal = 0;
        }
        self.navigationItem.title = @"Group & Sort";
    }
    else
        self.navigationItem.title = @"Sort";
    
    if([[defaults valueForKey:@"SortFindBy"]isEqualToString:@"name"])
        sortSelectionval = 0;
    else if([[defaults valueForKey:@"SortFindBy"]isEqualToString:@"Distance"])
        sortSelectionval = 1;
    
    
    if(tblGroupingNSorting)
    {
        [tblGroupingNSorting removeFromSuperview];
       // [tblGroupingNSorting release];
    }
    tblGroupingNSorting = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-80-EXCLUDE_BAR_HEIGHT) style:UITableViewStylePlain];
    tblGroupingNSorting.dataSource=self;
    tblGroupingNSorting.delegate=self;
    [tblGroupingNSorting setAccessibilityIdentifier:@"groupandsort"];
    [tblGroupingNSorting setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:tblGroupingNSorting];
    
    [tblGroupingNSorting reloadData];
    
}

-(void)Request_fetcheventdetail
{
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId><catId>%@</catId>",[defaults valueForKey:KEY_USERID] ,[defaults valueForKey:KEY_HUBCITIID],[defaults valueForKey:@"categId"]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length])
    {
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    if([defaults valueForKey:KEY_MITEMID]){
        [reqStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    
    else{
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    [reqStr appendFormat:@"</UserDetails>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/getsubcategory",BASE_URL];
    
    
    [ConnectionManager establishConnectionFor:reqStr base:urlString withDelegate:self];
    ReleaseAndNilify(reqStr);
    
}

-(void)Request_filter
{
    

    /*
     <MenuItem>
     *<userId>66</userId>
     *<hubCitiId>93</hubCitiId>
     <latitude>30.57228</latitude>
     <longitude>-98.306992</longitude>
     *<catId>11</catId>
     <radius>100</radius>
     // <cityIds></cityIds>
     <mItemId>209</mItemId>
     <bottomBtnId>304</bottomBtnId>
     //<searchKey> </searchKey>
     <filterName>smoking</filterName>
     </MenuItem>
     
     http://localhost:8080/HubCiti1.6/find/getfilterlist
     */
    
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<MenuItem><userId>%@</userId><hubCitiId>%@</hubCitiId><catId>%@</catId>",[defaults valueForKey:KEY_USERID] ,[defaults valueForKey:KEY_HUBCITIID],catId] ;//],[defaults valueForKey:KEY_LINKID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length])
    {
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    if([defaults valueForKey:KEY_MITEMID]){
        [reqStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    
    else{
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    if (![srchKey isEqualToString:@""]) {
        if (srchKey) {
            [reqStr appendFormat:@"<searchKey>%@</searchKey>",srchKey];
        }
    }
    
    [reqStr appendFormat:@"<filterName>smoking</filterName>"];
    
    [reqStr appendFormat:@"</MenuItem>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/getfilterlist",BASE_URL];
    
    NSString *response = [ConnectionManager establishPostConnection:urlString withParam:reqStr];
    
    [self parsefilters:response];
    
    ReleaseAndNilify(reqStr);
    
    
    
}


-(void)parsefilters:(NSString*)response{
    
    selectedFilterValueIds = [[NSMutableArray alloc]init];
    selectedFilterId = [[NSMutableArray alloc]init];
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {

        
                
                TBXMLElement *eventListElement = [TBXML childElementNamed:@"filterList" parentElement:tbxml.rootXMLElement];
                
                if(eventListElement)
                {
                    NSMutableDictionary *temp;
                    NSMutableArray *valueNames = [[NSMutableArray alloc]init];
                    NSMutableArray *valueIds = [[NSMutableArray alloc]init];
                    TBXMLElement *EventDetailsElement = [TBXML childElementNamed:@"Filter" parentElement:eventListElement];
                    
                    while (EventDetailsElement)
                    {
                        


                            TBXMLElement *filterValueIdElement = [TBXML childElementNamed:@"fValueId" parentElement:EventDetailsElement];
                            TBXMLElement *filterValueNameElement = [TBXML childElementNamed:@"fValueName" parentElement:EventDetailsElement];
                            
                            if(filterValueIdElement)
                                [valueIds addObject:[TBXML textForElement:filterValueIdElement]];
                            
                            if(filterValueNameElement){
                                [valueNames addObject:[TBXML textForElement:filterValueNameElement]];
                            }
                        
                        
                       
                        EventDetailsElement = [TBXML nextSiblingNamed:@"Filter" searchFromElement:EventDetailsElement];
                        
                    }
                    temp = [NSMutableDictionary dictionaryWithObjects:valueIds forKeys:valueNames];
                    [OptionsDic addEntriesFromDictionary:temp];
                    
                }

        
        }
        
        
        

        
        if([defaults valueForKey:@"SelectedFilterId"]!=nil && [defaults valueForKey:@"commaSeperatedFilterIds"]!=nil)
        {
            [selectedFilterId removeAllObjects];
            [selectedFilterId addObjectsFromArray:[NSMutableArray arrayWithArray:[[defaults valueForKey:@"commaSeperatedFilterIds"] componentsSeparatedByString:@","]]];
        }
        
        else{
            if ([[defaults valueForKey:@"afterFilterIdSelection"] isEqualToString:@"1"]) {
                [selectedFilterId removeAllObjects];
            }
            else{
                [defaults setValue:selectedFilterId forKey:@"SelectedFilterId"];
            }
        }
        
        
        
        if([defaults valueForKey:@"SelectedFilterValueIds"]!=nil && [[defaults valueForKey:@"commaSeperatedFilterValueIds"] componentsSeparatedByString:@","]!=nil)
        {
            [selectedFilterValueIds removeAllObjects];
            [selectedFilterValueIds addObjectsFromArray:[NSMutableArray arrayWithArray:[[defaults valueForKey:@"commaSeperatedFilterValueIds"] componentsSeparatedByString:@","]]];
        }
        
        else{
            if ([[defaults valueForKey:@"afterFilterValueSelection"] isEqualToString:@"1"]) {
                [selectedFilterValueIds removeAllObjects];
            }
            else{
                [defaults setValue:selectedFilterValueIds forKey:@"SelectedFilterValueIds"];
            }
        }
        
    }




//for get method
-(void)responseData:(NSString*)response
{
    [self parseDinningCat:response];
    if([arrDinningTypes count] > 0)
    {
       [arrGroups addObject:@"Type"];
        
    }
    [self setViewForGroupingNSorting];
}

-(void)parseDinningCat:(NSString*)response
{
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        TBXMLElement *listCatDetailsElement = [TBXML childElementNamed:@"listCatDetails" parentElement:tbxml.rootXMLElement];
        if(listCatDetailsElement)
        {
            TBXMLElement *CategoryDetailsElement = [TBXML childElementNamed:@"CategoryDetails" parentElement:listCatDetailsElement];
            while (CategoryDetailsElement)
            {
                NSMutableDictionary *dicCat = [[NSMutableDictionary alloc]init];
                TBXMLElement *catIdElement = [TBXML childElementNamed:@"catId" parentElement:CategoryDetailsElement];
                TBXMLElement *catNameElement = [TBXML childElementNamed:@"catName" parentElement:CategoryDetailsElement];
                if(catIdElement)
                    [dicCat setObject:[TBXML textForElement:catIdElement] forKey:@"catId"];
                if(catNameElement)
                    [dicCat setObject:[TBXML textForElement:catNameElement] forKey:@"catName"];
                
                [arrDinningTypes addObject:dicCat];
               // [dicCat release];
                
                CategoryDetailsElement = [TBXML nextSiblingNamed:@"CategoryDetails" searchFromElement:CategoryDetailsElement];
            }
            
        }
        isSubCat=TRUE;
    }
    else
    {

        //        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        //
        //		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        //		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        isSubCat=FALSE;
        
    }
}

-(void)setConstraints:(UITableViewCell*)cell tag:(int)tag
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    if(tag == 0)
    {
        if(globalIndexpath.section<=1) {
            // Table cell constraints
            if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
            {
                if([viewDictionary objectForKey:@"lblGroup"] != nil)
                {
                    // Vertical constraints
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[lblGroup(20)]"] options:0 metrics:0 views:viewDictionary]];
                    
                    // Horizontal constraints
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(13)-[lblGroup(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
                }
                else
                {
                    // Vertical constraints
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[lblSort(20)]"] options:0 metrics:0 views:viewDictionary]];
                    
                    // Horizontal constraints
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[lblSort(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
                }
            }
            else
            {
                if([viewDictionary objectForKey:@"lblGroup"] != nil)
                {
                    // Vertical constraints
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(13)-[lblGroup(20)]"] options:0 metrics:0 views:viewDictionary]];
                    
                    // Horizontal constraints
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(13)-[lblGroup(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
                }
                else
                {
                    // Vertical constraints
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[lblSort(20)]"] options:0 metrics:0 views:viewDictionary]];
                    
                    // Horizontal constraints
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[lblSort(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
                }
            }
        }
    }
    else if(tag == 1)
    {
        // PickerView constraints
        if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        {
            // Vertical constrains
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[view_picker(216)]",SCREEN_HEIGHT-self.navigationController.navigationBar.frame.size.height-236] options:0 metrics:0 views:pickerViewDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[dinningTypePickerView(216)]"] options:0 metrics:0 views:pickerViewDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[pickerToolbar(44)]"] options:0 metrics:0 views:pickerViewDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[lblTitle(20)]"] options:0 metrics:0 views:pickerViewDictionary]];
            
            // Horizontal constraints
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[view_picker(%f)]",SCREEN_WIDTH] options:0 metrics:0 views:pickerViewDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[dinningTypePickerView(%f)]",SCREEN_WIDTH] options:0 metrics:0 views:pickerViewDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[pickerToolbar(%f)]",SCREEN_WIDTH] options:0 metrics:0 views:pickerViewDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[lblTitle(%f)]",SCREEN_WIDTH] options:0 metrics:0 views:pickerViewDictionary]];
        }
        else
        {
            // Vertical constrains
            if(IS_IPHONE5)
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(291)-[view_picker(216)]"] options:0 metrics:0 views:pickerViewDictionary]];
            else
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(221)-[view_picker(216)]"] options:0 metrics:0 views:pickerViewDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[dinningTypePickerView(216)]"] options:0 metrics:0 views:pickerViewDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[pickerToolbar(44)]"] options:0 metrics:0 views:pickerViewDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[lblTitle(20)]"] options:0 metrics:0 views:pickerViewDictionary]];
            
            // Horizontal constraints
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[view_picker(320)]"] options:0 metrics:0 views:pickerViewDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[dinningTypePickerView(320)]"] options:0 metrics:0 views:pickerViewDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[pickerToolbar(320)]"] options:0 metrics:0 views:pickerViewDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[lblTitle(320)]"] options:0 metrics:0 views:pickerViewDictionary]];
        }
    }
    else
    {
        // PickerView row constraints
        if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        {
                // Vertical constraints
                [[pickerRowDictionary objectForKey:@"rowView"] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(40)]"] options:0 metrics:0 views:pickerRowDictionary]];
                
                // Horizontal constraints
            if(IOS7)
                [[pickerRowDictionary objectForKey:@"rowView"] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[label(%f)]",SCREEN_WIDTH-20] options:0 metrics:0 views:pickerRowDictionary]];
            else
                [[pickerRowDictionary objectForKey:@"rowView"] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(%f)]",SCREEN_WIDTH] options:0 metrics:0 views:pickerRowDictionary]];
        }
        else
        {
                // Vertical constraints
                [[pickerRowDictionary objectForKey:@"rowView"] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[label(40)]"] options:0 metrics:0 views:pickerRowDictionary]];
                
                // Horizontal constraints
            if(IOS7)
                [[pickerRowDictionary objectForKey:@"rowView"] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[label(220)]"] options:0 metrics:0 views:pickerRowDictionary]];
            else
                [[pickerRowDictionary objectForKey:@"rowView"] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(200)]"] options:0 metrics:0 views:pickerRowDictionary]];
        }
    }
}


#pragma mark tableview delegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return rowHeightForSort;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return sectionHeaderHeightForSort;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    if([typeStr isEqualToString:@"Dining"] || [typeStr isEqualToString:@"Bars"]){
        if([arrDinningTypes count]==0 && [filterCount count]==0)
            return [filterCount count]+1;
        else if ([arrDinningTypes count]==0){
            return [filterCount count]+1;
        }
        else
            return [filterCount count]+2;
    }
    else{
        if([arrDinningTypes count]==0)
            return 1;
        else
            return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0 ){
        if ([arrDinningTypes count]!=0) {
            return [arrGroups count];
        }
        else{
            return [arrSorting count];
        }
    }
    else if(section==1){
        if ([arrDinningTypes count]==0 && [filterCount count]>0) {
            
                Multivalue *multipleValue ;
                multipleValue = [filterCount objectAtIndex:section-1];
                if (multipleValue.isExpanded) {
                    return [multipleValue.valueNames count];
                }
                else{
                    return 0;
                }
            
        }
        else{
            return [arrSorting count];
        }
    }
    
    
    else{
        Multivalue *multipleValue ;
        multipleValue = [filterCount objectAtIndex:section-2];
        if (multipleValue.isExpanded) {
            return [multipleValue.valueNames count];
        }
        else{
            return 0;
        }
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	Multivalue *multipleValue ;
    UIButton *sectionHeader = [UIButton buttonWithType:UIButtonTypeCustom];
    sectionHeader.layer.borderColor = [UIColor blackColor].CGColor;
    sectionHeader.layer.borderWidth = 0.25;
    UILabel *lblCatName ;
     if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
     {
         lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 28)];
     	[lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
	 }
    else
    {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 18)];
    	 [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
	}
    
    if(section==0 ){
        if ([arrDinningTypes count]!=0) {
            [lblCatName setText:@"Group Items by"];
        }
        else{
            lblCatName.text = @"Sort Items by" ;
        }
    }
        
    
    else if(section==1)
    {
        if ([arrDinningTypes count]==0 && [filterCount count]>0) {
            
            multipleValue = [filterCount objectAtIndex:section-1];
            lblCatName.text = multipleValue.header ;
            sectionHeader.tag = section;
            UIImageView *caratImage =[[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-25, 7, 12, 13)];
            [sectionHeader addTarget:self action:@selector(clickedonHeader:) forControlEvents:UIControlEventTouchUpInside];
            [caratImage setBackgroundColor:[UIColor clearColor]];
            [sectionHeader addSubview:caratImage];
            
            if(multipleValue.isExpanded)
                [caratImage setImage:[UIImage imageNamed:@"carat-open"]];
            else
                [caratImage setImage:[UIImage imageNamed:@"carat"]];
            
            [sectionHeader addSubview:lblCatName];
            [sectionHeader setBackgroundColor:[UIColor grayColor]];
            
            
            return sectionHeader;
        }
        
        else{
            lblCatName.text = @"Sort Items by" ;
        }
    }
    
    
    
    else {
        multipleValue = [filterCount objectAtIndex:section-2];
        lblCatName.text = multipleValue.header ;
        sectionHeader.tag = section;
        UIImageView *caratImage =[[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-25, 7, 12, 13)];
        [sectionHeader addTarget:self action:@selector(clickedonHeader:) forControlEvents:UIControlEventTouchUpInside];
        [caratImage setBackgroundColor:[UIColor clearColor]];
        [sectionHeader addSubview:caratImage];
        
        if(multipleValue.isExpanded)
            [caratImage setImage:[UIImage imageNamed:@"carat-open"]];
        else
            [caratImage setImage:[UIImage imageNamed:@"carat"]];
    }
    
    [sectionHeader addSubview:lblCatName];
    [sectionHeader setBackgroundColor:[UIColor grayColor]];

    
    return sectionHeader;
}



-(void)clickedonHeader:(UIButton *)sender
{
    
    Multivalue *multipleValue ;
    if ([arrDinningTypes count]==0 && [filterCount count]>0){
        multipleValue = [filterCount objectAtIndex:(sender.tag-1)];
    }
    else
    multipleValue = [filterCount objectAtIndex:(sender.tag-2)];
    //   multipleValue = [filterCount objectAtIndex:sender.tag];
    
    
    if (multipleValue.isExpanded==NO) {
        multipleValue.isExpanded=YES;
        
        [tblGroupingNSorting reloadData];
        
        NSIndexPath *selectedRowIndexPath = [NSIndexPath indexPathForRow:(sender.tag+1) inSection:sender.tag];
        
        if ([[tblGroupingNSorting visibleCells] containsObject:[tblGroupingNSorting cellForRowAtIndexPath:selectedRowIndexPath]]==NO) {
            NSIndexPath *tempIndexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
            [tblGroupingNSorting scrollToRowAtIndexPath:tempIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
    }
    else{
        
        multipleValue.isExpanded=NO;
        [tblGroupingNSorting reloadData];
    }
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellEventListView";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor =  [UIColor clearColor];
        
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }
        
    }
    
    globalIndexpath = indexPath;
    UILabel *lblGroup = nil,*lblSort = nil;
    
    if(indexPath.section == 0)
    {
         if ([arrDinningTypes count]!=0) {
        lblGroup = [[UILabel alloc]init];
        [lblGroup setText:[arrGroups objectAtIndex:indexPath.row]];
        [lblGroup setTextColor:[UIColor blackColor]];
        [lblGroup setBackgroundColor:[UIColor clearColor]];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [lblGroup setFont:[UIFont boldSystemFontOfSize:18]];
        else
            [lblGroup setFont:[UIFont boldSystemFontOfSize:13]];
        [cell.contentView addSubview:lblGroup];
             lblGroup.translatesAutoresizingMaskIntoConstraints = NO;
             [cell.contentView setBackgroundColor:[UIColor clearColor]];
        //[lblGroup release];
        
        if(indexPath.row == groupSelectionVal)
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
        viewDictionary = NSDictionaryOfVariableBindings(lblGroup);
    }
         else{
             lblSort = [[UILabel alloc]init];
             [lblSort setText:[arrSorting objectAtIndex:indexPath.row]];
             [lblSort setTextColor:[UIColor blackColor]];
             [lblSort setBackgroundColor:[UIColor clearColor]];
             if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                 [lblSort setFont:[UIFont boldSystemFontOfSize:18]];
             else
                 [lblSort setFont:[UIFont boldSystemFontOfSize:13]];
             [cell.contentView addSubview:lblSort];
             lblSort.translatesAutoresizingMaskIntoConstraints = NO;

             [cell.contentView setBackgroundColor:[UIColor clearColor]];
             //[lblSort release];
             
             if(indexPath.row == sortSelectionval)
                 cell.accessoryType = UITableViewCellAccessoryCheckmark;
             else
                 cell.accessoryType = UITableViewCellAccessoryNone;
             
             viewDictionary = NSDictionaryOfVariableBindings(lblSort);
         }
    }
    
    else if(indexPath.section == 1)
    {
        if ([arrDinningTypes count]==0 && [filterCount count]>0){
                
                Multivalue *multipleValue ;
                multipleValue = [filterCount objectAtIndex:indexPath.section-1];
                
                if(multipleValue.isExpanded==YES)
                {
                    
                    if ([multipleValue.header isEqualToString:@"Options"]) {
                        if([selectedFilterId containsObject:[OptionsDic valueForKey:[multipleValue.valueNames objectAtIndex:indexPath.row]]] ){
                            checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOn.png"]];
                            if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                                checkButton.frame = CGRectMake(10, 15, 20, 20);
                                
                            }
                            else
                            {
                                checkButton.frame = CGRectMake(10, 20, 30, 30);
                                
                            }
                            
                            checkButton.backgroundColor =[UIColor clearColor];
                        }
                        
                        else{
                            checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOff.png"]];
                            if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                                checkButton.frame = CGRectMake(10, 15, 20, 20);
                                
                            }
                            else
                            {
                                checkButton.frame = CGRectMake(10, 20, 30, 30);
                                
                            }
                            
                            checkButton.backgroundColor =[UIColor clearColor];
                        }
                    }
                    
                    else {
                        
                        if([selectedFilterValueIds containsObject:[OptionsDic valueForKey:[multipleValue.valueNames objectAtIndex:indexPath.row]]])
                        {
                            checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOn.png"]];
                            if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                                checkButton.frame = CGRectMake(10, 15, 20, 20);
                                
                            }
                            else
                            {
                                checkButton.frame = CGRectMake(10, 20, 30, 30);
                                
                            }
                            
                            checkButton.backgroundColor =[UIColor clearColor];
                        }
                        
                        else{
                            checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOff.png"]];
                            if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                                checkButton.frame = CGRectMake(10, 15, 20, 20);
                                
                            }
                            else
                            {
                                checkButton.frame = CGRectMake(10, 20, 30, 30);
                                
                            }
                            
                            checkButton.backgroundColor =[UIColor clearColor];                }
                    }
                    UILabel *lblCiti ;
                    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                        lblCiti = [[UILabel alloc]initWithFrame:CGRectMake(40, 0, SCREEN_WIDTH, 45)];
                        [lblCiti setFont:[UIFont boldSystemFontOfSize:13]];
                    }
                    else
                    {
                        lblCiti = [[UILabel alloc]initWithFrame:CGRectMake(60, 0, SCREEN_WIDTH, 70)];
                        [lblCiti setFont:[UIFont boldSystemFontOfSize:18]];
                    }
                    [lblCiti setText:[multipleValue.valueNames objectAtIndex:indexPath.row]];
                    [lblCiti setTextColor:[UIColor blackColor]];
                    [lblCiti setBackgroundColor:[UIColor clearColor]];
                    
                    //            [content addSubview:checkButton];
                    //            [content addSubview:lblCiti];
                    [cell.contentView addSubview:checkButton];
                    [cell.contentView addSubview:lblCiti];
                    [cell.contentView setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    //            [content release];
                }
            }
        
        else
        {
        lblSort = [[UILabel alloc]init];
        [lblSort setText:[arrSorting objectAtIndex:indexPath.row]];
        [lblSort setTextColor:[UIColor blackColor]];
        [lblSort setBackgroundColor:[UIColor clearColor]];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [lblSort setFont:[UIFont boldSystemFontOfSize:18]];
        else
            [lblSort setFont:[UIFont boldSystemFontOfSize:13]];
        [cell.contentView addSubview:lblSort];
            lblSort.translatesAutoresizingMaskIntoConstraints = NO;

		[cell.contentView setBackgroundColor:[UIColor clearColor]];
       // [lblSort release];
        
        if(indexPath.row == sortSelectionval)
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
        viewDictionary = NSDictionaryOfVariableBindings(lblSort);
      }
    }
    
     else if(indexPath.section > 1){
        
        
        Multivalue *multipleValue ;
        multipleValue = [filterCount objectAtIndex:indexPath.section-2];
        
        if(multipleValue.isExpanded==YES)
        {
            
            if ([multipleValue.header isEqualToString:@"Options"]) {
                if([selectedFilterId containsObject:[OptionsDic valueForKey:[multipleValue.valueNames objectAtIndex:indexPath.row]]] ){
                    checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOn.png"]];
                    if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                        checkButton.frame = CGRectMake(10, 15, 20, 20);
                        
                    }
                    else
                    {
                        checkButton.frame = CGRectMake(10, 20, 30, 30);
                        
                    }
                    
                    checkButton.backgroundColor =[UIColor clearColor];
                }
                
                else{
                    checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOff.png"]];
                    if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                        checkButton.frame = CGRectMake(10, 15, 20, 20);
                        
                    }
                    else
                    {
                        checkButton.frame = CGRectMake(10, 20, 30, 30);
                        
                    }
                    
                    checkButton.backgroundColor =[UIColor clearColor];
                }
            }
            
            else {
                
                if([selectedFilterValueIds containsObject:[OptionsDic valueForKey:[multipleValue.valueNames objectAtIndex:indexPath.row]]])
                {
                    checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOn.png"]];
                    if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                        checkButton.frame = CGRectMake(10, 15, 20, 20);
                        
                    }
                    else
                    {
                        checkButton.frame = CGRectMake(10, 20, 30, 30);
                        
                    }
                    
                    checkButton.backgroundColor =[UIColor clearColor];
                }
                
                else{
                    checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOff.png"]];
                    if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                        checkButton.frame = CGRectMake(10, 15, 20, 20);
                        
                    }
                    else
                    {
                        checkButton.frame = CGRectMake(10, 20, 30, 30);
                        
                    }
                    
                    checkButton.backgroundColor =[UIColor clearColor];                }
            }
            UILabel *lblCiti ;
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                lblCiti = [[UILabel alloc]initWithFrame:CGRectMake(40, 0, SCREEN_WIDTH, 45)];
                [lblCiti setFont:[UIFont boldSystemFontOfSize:13]];
            }
            else
            {
                lblCiti = [[UILabel alloc]initWithFrame:CGRectMake(60, 0, SCREEN_WIDTH, 70)];
                [lblCiti setFont:[UIFont boldSystemFontOfSize:18]];
            }
            [lblCiti setText:[multipleValue.valueNames objectAtIndex:indexPath.row]];
            [lblCiti setTextColor:[UIColor blackColor]];
            [lblCiti setBackgroundColor:[UIColor clearColor]];
            
            //            [content addSubview:checkButton];
            //            [content addSubview:lblCiti];
            [cell.contentView addSubview:checkButton];
            [cell.contentView addSubview:lblCiti];
            [cell.contentView setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
            cell.accessoryType = UITableViewCellAccessoryNone;
            //            [content release];
        }
    }
   



    
    [self setConstraints:cell tag:0];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0 && indexPath.row==0 && [arrDinningTypes count])
    {
        [self showpickerView];
    }
    
    if(indexPath.section==0  && [arrDinningTypes count]!=0){
        [defaults setValue:@"type" forKey:@"GroupFindBy"];
        groupSelectionVal = (int)indexPath.row;
    }
    
//    else{
//        if (groupSelectionVal==-1 || groupSelectionVal==1) {
//            groupSelectionVal = indexPath.row;
//        }
//        else
//            groupSelectionVal = -1;
//    }
//    if (indexPath.section==0) {
//        if([arrDinningTypes count]!=0)
//        {
//            [defaults setValue:@"type" forKeyPath:@"GroupFindBy"];
//            groupSelectionVal = indexPath.row;
//            [self showpickerView];
//        }
//        else{
//            if (groupSelectionVal==-1 || groupSelectionVal==1) {
//                groupSelectionVal = indexPath.row;
//            }
//            else
//                groupSelectionVal = -1;
//        }
//    }

    else if(indexPath.section==1){
         if ([arrDinningTypes count]==0 && [filterCount count]>0){
             [self didSelectFilters:indexPath];
         }
        else
        sortSelectionval = (int)indexPath.row;
    }
    
    else{
        if(indexPath.section==0)
            sortSelectionval = (int)indexPath.row;
        else
            [self didSelectFilters:indexPath];
    }
    
    [tblGroupingNSorting reloadData];
}



-(void)didSelectFilters:(NSIndexPath*)sender
{
    Multivalue *multipleValue ;
    DLog(@"%ld",(long)sender.section);
    if ([arrDinningTypes count]==0 && [filterCount count]>0){
    multipleValue = [filterCount objectAtIndex:sender.section-1];
    }
    else{
        multipleValue = [filterCount objectAtIndex:sender.section-2];
    }
    
    if ([multipleValue.header isEqualToString:@"Options"]) {
        if ( ![selectedFilterId containsObject:[multipleValue.filterId objectAtIndex:sender.row]]) {
            [selectedFilterId addObject:[multipleValue.filterId objectAtIndex:sender.row]];
        }
        else{
            [selectedFilterId removeObject:[multipleValue.filterId objectAtIndex:sender.row]];
        }
        
    }
    else if([multipleValue.valueNames count]>= 1)
    {
        
        
        if ([selectedFilterValueIds containsObject:[OptionsDic valueForKey:[multipleValue.valueNames objectAtIndex:sender.row]]]) {
            BOOL containsValue=FALSE;
            [selectedFilterValueIds removeObject:[OptionsDic valueForKey:[multipleValue.valueNames objectAtIndex:sender.row]]];
            
            for (int i=0;i<[multipleValue.valueIds count];i++ ) {
                if([selectedFilterValueIds containsObject:[multipleValue.valueIds objectAtIndex:i]])
                {
                    containsValue=TRUE;
                }
            }
            if (containsValue==FALSE) {
                [selectedFilterId removeObject:[multipleValue.filterId objectAtIndex:sender.row]];
            }
            
        }
        
        else{
            
            if ( ![selectedFilterId containsObject:[multipleValue.filterId objectAtIndex:sender.row]]) {
                [selectedFilterId addObject:[multipleValue.filterId objectAtIndex:sender.row]];
            }
            
            [selectedFilterValueIds addObject:[OptionsDic valueForKey:[multipleValue.valueNames objectAtIndex:sender.row]]];
        }
        
    }
    DLog(@"selectedFilterValueIds\n%@",selectedFilterValueIds);
    DLog(@"selectedFilterId\n%@",selectedFilterId);
    [tblGroupingNSorting reloadData];
    
}



#pragma mark PICKERVIEW
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    NSMutableDictionary *dic = [arrDinningTypes objectAtIndex:row];
    [defaults setValue:[dic valueForKey:@"catId"] forKey:@"SubCategoryId"];
    [defaults setValue:[dic valueForKey:@"catName"] forKey:@"SubCategoryName"];
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [arrDinningTypes count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        return 70.0;
    else
        return 50.0;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UIView * rowView = [[UIView alloc] init];
    UILabel *label = [[UILabel alloc] init];
    
    if (IOS7){
        //label.frame = CGRectMake(20, 0, 220, 40);
        pickerView.layer.borderWidth = 5.0;
        pickerView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    }
//    else
//        label.frame = CGRectMake(0, 0, 200, 40);
    
    [label setBackgroundColor:[UIColor clearColor]];
    [label setNumberOfLines:2];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        [label setFont:[UIFont boldSystemFontOfSize:21]];
        [label setTextAlignment:NSTextAlignmentCenter];
    }
    else
    {
        [label setFont:[UIFont boldSystemFontOfSize:16]];
        [label setTextAlignment:NSTextAlignmentLeft];
    }
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    
    NSMutableDictionary *dic = [arrDinningTypes objectAtIndex:row];
    label.text = [dic valueForKey:@"catName"];
    
    label.translatesAutoresizingMaskIntoConstraints = NO;
    rowView.translatesAutoresizingMaskIntoConstraints = NO;
    pickerRowDictionary = NSDictionaryOfVariableBindings(rowView, label);
    [rowView addSubview:label];
    //[label release];
    [self setConstraints:nil tag:2];
    
    return rowView;
}



-(void)request_sortcatretailersWithsubCatId:(NSString*)subCatId sortBy:(NSString*)sortCriteria groupBy:(NSString*)groupCriteria searchKey:(NSString*)searchKey fValueIds:(NSMutableArray*)fValueIds  filterIds:(NSMutableArray*)filterIds
{    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<catName>%@</catName>",typeStr];
    
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
    }
    
    //for user tracking
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    //either of mItemId or bottomBtnId is must - i.e if from menu botton then mItemId else bottomBtnId
    if (![defaults boolForKey:BottomButton]) {
//        if (userIdentifier==TRUE) {
//             [requestStr appendFormat:@"<bottomBtnId>0</bottomBtnId><platform>IOS</platform>"];
//        }
        if([defaults valueForKey:KEY_MITEMID]){
            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
        }
    }
    else{
        [defaults setBool:YES forKey:BottomButton];
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        
    }
    
    
        if ([sortCriteria length])
           [requestStr appendFormat:@"<sortOrder>ASC</sortOrder><sortColumn>%@</sortColumn>",sortCriteria];
    
//    if ([groupCriteria length]) {
//        [requestStr appendFormat:@"<groupBy>%@</groupBy>",groupCriteria];
//    }
    
        if ([searchKey length]) {
            [requestStr appendFormat:@"<searchKey>%@</searchKey>",searchKey];
        }
        if (![subCatId isEqualToString:@"0"])
            [requestStr appendFormat:@"<subCatIds>%@</subCatIds>",subCatId];
    
    
    if ([defaults valueForKeyPath:@"commaSeperatedFilterValueIds"]) {
        [requestStr appendFormat:@"<fValueId>%@</fValueId>",fValueIds];
    }
    
    if ([defaults valueForKeyPath:@"commaSeperatedFilterIds"]) {
        [requestStr appendFormat:@"<filterId>%@</filterId>",filterIds];
    }
    
    if ([SharedManager localSpecilas]) {
        [requestStr appendFormat:@"<locSpecials>1</locSpecials>"];
    }
    else{
        [requestStr appendFormat:@"<locSpecials>0</locSpecials>"];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/sscatsearch",BASE_URL];
    
    NSString* response= [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    
    //[requestStr release];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

//
//  FindLocationServiceViewController.h
//  Scansee
//
//  Created by Chaitra on 19/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MapKit/MKMapView.h>
#import "SectionView.h"
#import "Category.h"
#import "SectionInfo.h"
#import "FindViewController.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "GetUserInfoResponse.h"
#import "SwipesViewController.h"
@class DealHotDealsList;
@class FilterRetailersList;
@class CityExperienceViewController;
@class FundraiserListViewController;
@class FilterListViewController;
@class EventsListViewController;
@class RetailersListViewController;
@class AnyViewController;
@class EmailShareViewController;
@interface FindLocationServiceDO : NSObject{
    
    NSString *retName, *retId, *retLocnId, *retAddr;
    NSString *retCompleteAddress;
    NSString *retLat, *retLong, *retDist, *retLogoImgPath;
    NSString *retbannerAdImgPath, *retRibbonAdImgPath, *retRibbonAdUrl, *retSaleFlag;
    NSString *retListId;
    
}
@property (nonatomic, strong)NSString *retCompleteAddress;
@property (nonatomic, strong)NSString *retName, *retId, *retLocnId, *retAddr;
@property (nonatomic, strong)NSString *retLat, *retLong, *retDist, *retLogoImgPath;
@property (nonatomic, strong)NSString *retbannerAdImgPath, *retRibbonAdImgPath, *retRibbonAdUrl, *retSaleFlag;
@property (nonatomic, strong)NSString *retListId;

@end

//#import "CollapsableTableViewDelegate.h"

@class DisplayMap;
NSMutableArray *globalRetailerService;
BOOL fromFind,fromRegionFind;
BOOL inCategorySort;
NSString *type;

@interface FindLocationServiceViewController : UIViewController<UITableViewDelegate , UITableViewDataSource ,MKMapViewDelegate,UISearchBarDelegate,MFMessageComposeViewControllerDelegate,SwipesViewControllerDelegate,HTTPClientDelegate,CustomizedNavControllerDelegate> {
    UISegmentedControl *segmentFindController;
    webServicesFindState iWebRequestState;
    UITableView *locServiceDealsTable;
	IBOutlet MKMapView *mapView;
    
    int selectedindexRow,selectedSection;
    
    NSMutableArray *findLocationServiceObjArray, *retailerIDArray, *retailLocationIDArray;
    
    NSMutableArray *arrFindCategoryInfo;
    
	NSMutableArray *annArray;
	NSString *externalApiName;
    NSMutableArray *arrFindSingleBottomButtonDO;
    NSMutableArray *arrBottomButtonViewContainer;
    NSString *androidDownloadLink;
     CommonUtility *common;
    
    //pagination variables for ScanSee
    NSInteger nextPage;
   NSInteger lastVisitedRecord;
    
    //pagination variable for Google
    NSString *pageToken;
    
    //google search related
    IBOutlet UISearchBar *g_searchBar;
    IBOutlet UILabel *g_label;
//    IBOutlet UIView *g_view;
    
   UISearchBar *s_searchBar;
	
    NSString *typeStr,*searchStr;//to carry category name & keyword from previous screen
    
    //collapsable table
    NSMutableArray *categoryArray;
    Category *category;
    
    Category *tempCategory;
    int currentsection;
        
    CGRect viewFrame;
    
    IBOutlet UIImageView *img;
    
    NSInteger bottomBtn;
    
    SwipesViewController *iSwipeViewController;
    
    SortAndFilter *sortFilObj;
    NSString *findTitle;
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,strong) EventsListViewController* iEventsListViewController;
@property(nonatomic,strong) FundraiserListViewController *iFundraiserListViewController;
@property(nonatomic,strong) FilterListViewController *filterList;
@property(nonatomic,strong) CityExperienceViewController *cityExp;
@property(nonatomic,strong) FilterRetailersList *filters;
@property(nonatomic,strong) DealHotDealsList* hotDeals;
@property(nonatomic,strong) RetailersListViewController* retailerListScreen;
@property (nonatomic, strong)NSString *typeStr;
@property (nonatomic, strong)NSString *retailerStr;
@property (nonatomic, strong)NSString *catId;
@property (nonatomic, strong)NSString *searchStr;
@property (nonatomic, assign) NSInteger oldSegmentedIndex;
@property (nonatomic, strong) SortAndFilter *sortFilObj;
@property(nonatomic,strong) NSString *findTitle,*findmitemid;
-(void) swapListMap : (UIBarButtonItem *) sender;

@end

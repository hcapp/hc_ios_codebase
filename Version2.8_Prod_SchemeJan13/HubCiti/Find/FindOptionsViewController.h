//
//  FindOptionsViewController.h
//  HubCiti
//
//  Created by Keshava on 6/23/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MKMapView.h>
#import "SwipesViewController.h"
BOOL showMapFlag;
@class FilterRetailersList;
@class RetailersListViewController;
@interface FindOptionsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MKMapViewDelegate,CustomizedNavControllerDelegate>
{
    UITableView *tableOptions;
    NSArray     *options;
    
    NSString *typeStr;
    NSString *searchKey;
    int filterId;
    SwipesViewController *iSwipeViewController;
    
}
@property(nonatomic,strong) RetailersListViewController* retailerListScreen;
@property(nonatomic,strong) FilterRetailersList *filters;
@property (nonatomic, strong)NSString *typeStr;
@property (nonatomic, strong)NSString *searchKey;
@property (nonatomic, strong)NSString *catId;

@property (readwrite) int filterId;

@property (nonatomic, strong)SortAndFilter *srtFilterObj;

@end

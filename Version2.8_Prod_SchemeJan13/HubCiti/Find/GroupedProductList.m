//
//  GroupedProductList.m
//  Scansee
//
//  Created by ajit on 9/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GroupedProductList.h"
#import "MainMenuViewController.h"
#import "ProductPage.h"
#import "SearchResultCount.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

NSDictionary *viewDictionary;

@implementation GroupedProductList{
    UIActivityIndicatorView *loading;
}

//static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    loading = nil;
    self.title = NSLocalizedString(@"Search Result",@"Search Result");
    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBar.hidden = NO;
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    
    //customize back button
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    prodNameArray = [[NSMutableArray alloc]init];
    prodIdArray = [[NSMutableArray alloc]init];
    prodImgPathArray = [[NSMutableArray alloc]init];
    prodDescArray = [[NSMutableArray alloc]init];
    rowNumArray = [[NSMutableArray alloc]init];
    lastVisitedRecord = 0;
    
    [self parseDisplayProdList:[defaults valueForKey:KEY_RESPONSEXML]];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [galleryTV deselectRowAtIndexPath:[galleryTV indexPathForSelectedRow] animated:YES];
    [super viewWillAppear:animated];
}

-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)parseDisplayProdList:(NSString*)responseXml{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbXml.rootXMLElement];
    if (nextPageElement != nil)
        nextPage = [[TBXML textForElement:nextPageElement] intValue];
    
    TBXMLElement *ProductDetailElement = [TBXML childElementNamed:@"ProductDetail" parentElement:tbXml.rootXMLElement];
    while (ProductDetailElement != nil) {
        TBXMLElement *productNameElement = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:ProductDetailElement];
        TBXMLElement *productIdElement = [TBXML childElementNamed:@"productId" parentElement:ProductDetailElement];
        TBXMLElement *productDescriptionElement = [TBXML childElementNamed:@"productDescription" parentElement:ProductDetailElement];
        TBXMLElement *rowNumberElement = [TBXML childElementNamed:@"rowNumber" parentElement:ProductDetailElement];
        TBXMLElement *productImagePathElement = [TBXML childElementNamed:@"productImagePath" parentElement:ProductDetailElement];
        
        [prodNameArray addObject:[TBXML textForElement:productNameElement]];
        [prodIdArray addObject:[TBXML textForElement:productIdElement]];
        [prodDescArray addObject:[TBXML textForElement:productDescriptionElement]];
        [prodImgPathArray addObject:[TBXML textForElement:productImagePathElement]];
        [rowNumArray addObject:[TBXML textForElement:rowNumberElement]];
        
        ProductDetailElement = [TBXML nextSiblingNamed:KEY_PRODUCTDETAIL searchFromElement:ProductDetailElement];
    }
    [defaults setBool:NO forKey:@"ViewMore"];
}



-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [prodNameArray removeAllObjects];
    // [prodNameArray release];
    
    [prodIdArray removeAllObjects];
    //  [prodIdArray release];
    
    [prodDescArray removeAllObjects];
    //[prodDescArray release];
    
    [prodImgPathArray removeAllObjects];
    //[prodImgPathArray release];
    
    [rowNumArray removeAllObjects];
    // [rowNumArray release];
    
    // [super dealloc];
}

#pragma mark -
#pragma mark Table view delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        return 70.0;
    else
        return 62.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (nextPage == 1)
        return [prodNameArray count]+1;
    else
        return [prodNameArray count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    UILabel *label = nil,*tx_label = nil, *tx_label_Detail = nil;
    SdImageView *asyncImageView = nil;
    
    if (indexPath.row == [prodNameArray count] && nextPage == 1) {
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
        //        label = [[[UILabel alloc] init] ;
        //        label.textAlignment = NSTextAlignmentCenter;
        //        label.textColor = [UIColor colorWithRGBInt:0x112e72];
        //        label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
        //        [cell.contentView addSubview:label];
        //        label.translatesAutoresizingMaskIntoConstraints = NO;
        //        viewDictionary = NSDictionaryOfVariableBindings(label);
    }
    else {
        tx_label = [[UILabel alloc] init];
        tx_label.numberOfLines = 1;
        //tx_label.lineBreakMode = YES;
        
        tx_label_Detail = [[UILabel alloc]init];
        tx_label_Detail.backgroundColor = [UIColor clearColor];
        tx_label_Detail.textColor = [UIColor grayColor];
        
        // Configure the cell...
        
        tx_label.text = [prodNameArray objectAtIndex:indexPath.row];
        tx_label_Detail.text = [prodDescArray objectAtIndex:indexPath.row];
        
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            label.font = [UIFont boldSystemFontOfSize:21];
            tx_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
            tx_label_Detail.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
        }
        else
        {
            label.font = [UIFont boldSystemFontOfSize:16];
            tx_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
            tx_label_Detail.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
        }
        
        if (![[prodImgPathArray objectAtIndex:indexPath.row] isEqualToString:@"N/A"]) {
            
            asyncImageView = [[SdImageView alloc] init];
            asyncImageView.backgroundColor = [UIColor clearColor];
            //[asyncImageView loadImageFromURL:url];
            [asyncImageView loadImage:[prodImgPathArray objectAtIndex:indexPath.row]];
            asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
            [cell.contentView addSubview:asyncImageView];
            //[asyncImageView release];
            
        }
        [cell.contentView addSubview:tx_label];
        [cell.contentView addSubview:tx_label_Detail];
        tx_label.translatesAutoresizingMaskIntoConstraints = NO;
        tx_label_Detail.translatesAutoresizingMaskIntoConstraints = NO;
        asyncImageView.translatesAutoresizingMaskIntoConstraints = NO;
        viewDictionary = NSDictionaryOfVariableBindings(tx_label,tx_label_Detail,asyncImageView);
        ReleaseAndNilify(tx_label);
        ReleaseAndNilify(tx_label_Detail);
    }
    
    [self setConstraints:cell];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == [prodNameArray count] && nextPage == 1) {
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"] && lastVisitedRecord!=0) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self fetchNextPage:self];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [galleryTV reloadData];
                });
            });
            
        }
    }
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    index = indexPath;
    //    if (indexPath.row == [prodNameArray count]) {
    //        if (nextPage == 1) {
    //            [self fetchNextPage:self];// More records to be shown...pagination
    //            DLog(@"NEXT PAGE - %d", nextPage);
    //        }
    //        return;
    //    }
    [defaults setBool:NO forKey:@"ViewMore"];
    latValue = [defaults valueForKey:KEY_LATITUDE];
    longValue = [defaults valueForKey:KEY_LONGITUDE];
    [defaults  setObject:[prodIdArray objectAtIndex:indexPath.row] forKey:KEY_PRODUCTID];
    [defaults  setObject:nil forKey:KEY_PRODLISTID];
    
    if([Network currentReachabilityStatus]==0){
        
        [UtilityManager showAlert];
    }
    
    [defaults setValue:@"0" forKey:KEY_SCANTYPEID];
    [[HubCitiManager sharedManager]setShareFromTL:NO];
    
    ProductPage *prodPage = [[ProductPage alloc]initWithNibName:@"ProductPage" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:prodPage animated:NO];
    //[prodPage release];
    
}

-(void)setConstraints:(UITableViewCell*)cell
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        if([viewDictionary objectForKey:@"asyncImageView"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[asyncImageView(60)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(60)]"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"tx_label"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[tx_label(40)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(75)-[tx_label(650)]"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"tx_label_Detail"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(40)-[tx_label_Detail(25)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(75)-[tx_label_Detail(650)]"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"label"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(20)-[label(24)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(768)]"] options:0 metrics:0 views:viewDictionary]];
        }
    }
    else
    {
        if([viewDictionary objectForKey:@"asyncImageView"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[asyncImageView(50)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(50)]"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"tx_label"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[tx_label(40)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(60)-[tx_label(250)]"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"tx_label_Detail"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(33)-[tx_label_Detail(15)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(60)-[tx_label_Detail(250)]"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"label"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(24)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(320)]"] options:0 metrics:0 views:viewDictionary]];
        }
    }
}

-(void) addToList {
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<AddShoppingList><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", [prodIdArray objectAtIndex:index.row]];
    [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName></ProductDetail></productDetails>", [prodNameArray objectAtIndex:index.row]];
    
    //@Deepak: Added MainMenuID in the request- userTracking for shoppinglist
    [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID></AddShoppingList>", [defaults  valueForKey:KEY_MAINMENUID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@shoppingList/addtslbysearch",BASE_URL];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml]) {
        ReleaseAndNilify(responseXml);
        return;
    }
    [defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
        [SharedManager setRefreshList:YES];
        [defaults  removeObjectForKey:KEY_SEARCHFROM];
    }
    
    else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        }
    
    //[responseXml release];
    
}

-(void) addToFavorites {
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<AddShoppingList><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", [prodIdArray objectAtIndex:index.row]];
    [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName></ProductDetail></productDetails>", [prodNameArray objectAtIndex:index.row]];
    //@Deepak: Added MainMenuID in the request- userTracking for shoppinglist
    [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID></AddShoppingList>", [defaults  valueForKey:KEY_MAINMENUID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@shoppingList/addproducttosl",BASE_URL];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml]){
        ReleaseAndNilify(responseXml);
        return;
    }
    [defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
        [SharedManager setRefreshList:YES];
        //[UtilityManager pushPopToFav:self.navigationController];
        [defaults  removeObjectForKey:KEY_SEARCHFROM];
    }
    else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        }
    
    //[responseXml release];
    
}

-(void) addToWishList {
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", [prodIdArray objectAtIndex:index.row]];
    [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName></ProductDetail></productDetails>", [prodNameArray objectAtIndex:index.row]];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID>", [defaults  valueForKey:KEY_MAINMENUID]];
    [requestStr appendString:@"</ProductDetailsRequest>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@wishlist/addWishListProd",BASE_URL];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml]) {
        ReleaseAndNilify(responseXml);
        return;
    }
    [defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
        //[UtilityManager pushPopToWishList:self.navigationController];
        [defaults  removeObjectForKey:KEY_SEARCHFROM];
    }
    else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        }
    
    //[responseXml release];
    
}

-(void)fetchNextPage:(id)sender{
    
    if (nextPage == 1)
    {
        lastVisitedRecord = [[rowNumArray objectAtIndex:[rowNumArray count]-1 ]intValue];
        
        NSMutableString *requestStr = [[NSMutableString alloc] init];
        [requestStr appendFormat:@"<ProductDetail><searchkey><![CDATA[%@]]></searchkey>", [defaults valueForKey:@"prodName"]];
        [requestStr appendFormat:@"<userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        [requestStr appendFormat:@"<parCatId>%@</parCatId><lastVistedProductNo>%d</lastVistedProductNo></ProductDetail>",[defaults valueForKey:@"ParCatId"],lastVisitedRecord];
        
        NSString *urlString = [NSString stringWithFormat:@"%@scannow/getsmartsearchprodlist",BASE_URL];
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
        // //[requestStr release];
        
        if ([UtilityManager isNullOrEmptyString:responseXml]) {
            ReleaseAndNilify(responseXml);
            
            return;
        }
        
        [self parseDisplayProdList:responseXml];
        [galleryTV reloadData];
        //[responseXml release];
        return;
        
    }
    else
    {
        return;
    }
}

@end



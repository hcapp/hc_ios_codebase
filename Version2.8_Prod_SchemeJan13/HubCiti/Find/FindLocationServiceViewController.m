//
//  FindLocationServiceViewController.m
//  Scansee
//
//  Created by Chaitra on 19/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "SpecialOffersViewController.h"
#import "FindLocationServiceViewController.h"
#import "UserSettingsController.h"
#import "AboutAndPrivacyScreen.h"
#import "RetailerAnnotation.h"
#import "FindLocationDetailsViewController.h"
#import "RetailerSummaryViewController.h"
#import "SingleCatGroupAndSort.h"
#import "SingleCatRetailers.h"
#import "HubCitiConstants.h"
#import "FindCategoryDO.h"
#import "GroupedProductList.h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "AnythingPage.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "AppDelegate.h"
//#import "ScanNowScreen.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FAQCategoryList.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "CityExperienceViewController.h"
#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "FindOptionsViewController.h"
#import "SettingsViewController.h"
#import "EventsListViewController.h"
#import "FindSortViewController.h"
#import "FilterListViewController.h"
#import "FundraiserListViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "HTTPClient.h"
#import "CombinationViewController.h"
#import "ScrollTemplateViewController.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponsInDealViewController.h"

NSDictionary *tableViewDictionary, *viewDictionary;

@implementation FindLocationServiceDO

@synthesize retName, retId, retLocnId, retAddr;
@synthesize retLat, retLong, retDist, retLogoImgPath;
@synthesize retbannerAdImgPath, retRibbonAdImgPath, retRibbonAdUrl, retSaleFlag;
@synthesize retListId;
@synthesize retCompleteAddress;
@end

@implementation FindLocationServiceViewController{
    UIActivityIndicatorView *loading;
    bottomButtonView *bottomBtnView;
    UILabel *label ;
    UILabel *detailLabel ;
    UILabel *addressLabel ;
    UILabel *mileLabel;
    UIImageView *specImage ;
    UILabel *hourFilterLabel;
    UIFont *titleFont,*addressFont,*cityFont;
    float numberOfLines;
}


//@synthesize typeStr, searchStr,catId,sortFilObj,anyVC;
@synthesize infoResponse;
@synthesize typeStr, searchStr,catId,sortFilObj,anyVC,emailSendingVC,retailerStr;
@synthesize findTitle,oldSegmentedIndex,findmitemid;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */
#pragma mark - Service Respone methods



- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}





- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject

{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    
    
    
    [self responseData:responseObject];
    
    
    
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
     NSLog(@"MItemID -- %@",[defaults valueForKey:KEY_MITEMID]);
    [super viewDidLoad];
      [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    //    g_view.hidden=YES;
    [self.view setAccessibilityLabel:@"FindLocation"];
    loading = nil;
//    UISwipeGestureRecognizer *mSwipeUpRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
//    [mSwipeUpRecognizer setDirection: UISwipeGestureRecognizerDirectionLeft];
//    [self.view addGestureRecognizer:mSwipeUpRecognizer];
    addressFont = [UIFont systemFontOfSize:(IPAD ? 15.0f:10.0f)];
//    UISwipeGestureRecognizer *mSwipeUpRecognizerRht = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
//    [mSwipeUpRecognizerRht setDirection: UISwipeGestureRecognizerDirectionRight];
//    [self.view addGestureRecognizer:mSwipeUpRecognizerRht];
    
    DLog(@"inCategorySort:%d",inCategorySort);
    //home button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
    
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    if(showMapFlag==TRUE)
    {
        
        tappedSearch=FALSE;
        //NSlog(@"The value of keyword and catRetSearchkey %@ and %@",keyWord,catRetSearchKey);
       
        self.navigationItem.hidesBackButton = YES;
        //[back release];
        
        [cus setTitle:@"Choose Your Location:" forView:self withHambergur:NO];
        [self optionsShowMap];
    }
    else
    {
        img.image = [UIImage imageNamed:@"searchGoogle"];
        fromFind=FALSE;
//        if (IOS7 == NO){
//            g_searchBar.tintColor = [UIColor blackColor];
//            
//        }
        globalRetailerService=[[NSMutableArray alloc]init];
        
        
        //    viewFrame = g_view.frame;
        
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        
        
        //added by ashika
        NSLog(@"Find category name  %@",typeStr);
        if (typeStr.length == 0)
        {
            [cus setTitle:@"Find" forView:self withHambergur:YES];
        }
        else
        {
            [cus setTitle:typeStr forView:self withHambergur:YES];
        }
        
        mapView.hidden = YES ;
        g_searchBar.delegate = self;
        locServiceDealsTable.tag = 0;
        
        //copying to KEY_LATITUDE & KEY_LONGITUDE as there is issue when coming back from retailer summary page since we are setting 'nil' value in viewWillAppear of retailer summary.
        //    [defaults setValue:[defaults valueForKey:@"LatVal"] forKey:KEY_LATITUDE];
        //    [defaults setValue:[defaults valueForKey:@"LongVal"] forKey:KEY_LONGITUDE];
        
        nextPage = 0;
        lastVisitedRecord = 0;
        arrFindCategoryInfo = [[NSMutableArray alloc]init];
        
        findLocationServiceObjArray = [[NSMutableArray alloc]init];
        retailerIDArray = [[NSMutableArray alloc]init];
        retailLocationIDArray = [[NSMutableArray alloc]init];
        
        annArray = [[NSMutableArray alloc] init]; // Map Annotation points
       
        self.navigationItem.hidesBackButton = YES;
        //[back release];
        
        

        
        /* UIButton *showMapBtn = [UtilityManager customizeBarButtonItem:@"Show Map"];
         [showMapBtn addTarget:self action:@selector(swapListMap:) forControlEvents:UIControlEventTouchUpInside];
         showMapBtn.tag=1;
         UIBarButtonItem *mainMenu = [[UIBarButtonItem alloc]initWithCustomView:showMapBtn];*/
        
        //    UIButton *options = [UtilityManager customizeBarButtonItem:@"Map/Sort"];
        //    [options addTarget:self action:@selector(options:) forControlEvents:UIControlEventTouchUpInside];
        //    options.tag=1;
        //    UIBarButtonItem *mainMenu = [[UIBarButtonItem alloc]initWithCustomView:options];
        //
        //	self.navigationItem.rightBarButtonItem = mainMenu;
        //    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"Map/Sort"];
        //	self.navigationItem.rightBarButtonItem.tag = 1 ;
        //	[mainMenu release];
        //
        //collapsable table
        categoryArray = [[NSMutableArray alloc] init];
        category = [[Category alloc] init];
        if ([globalRetailerService count]>0) {
            [globalRetailerService removeAllObjects];
            //  [globalRetailerService release];
        }
        
        sortFilObj = [[SortAndFilter alloc]init];
        sortFilObj.distanceSelected =true;
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        if(![defaults valueForKey:KEY_BOTTOMBUTTONID])
        [defaults setValue:nil forKey:@"findLocationMitemId"];
        if(findmitemid){
            [defaults setValue:findmitemid forKey:findmitemid];
            [defaults setValue:findmitemid forKey:@"findLocationMitemId"];
        }
        retailerStr = keyWord;
        
        [self customizedSegmentController];
        [self parseScanSeeRetailerSet];
    }
  
    
}



-(void)returnToMainPage:(id)sender
{
    showMapFlag = FALSE;
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

#pragma customized segment controller
-(void) customizedSegmentController{
    
    segmentFindController = [[UISegmentedControl  alloc] initWithItems:[NSArray arrayWithObjects:@"Distance",@"Name",@"Map", nil]];
    [segmentFindController setSelectedSegmentIndex:0];
    [segmentFindController setBackgroundColor:[UIColor grayColor]];
    [segmentFindController setSegmentedControlStyle:UISegmentedControlStylePlain];
    
    //set width to show divider in between
    [segmentFindController setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:0];
    [segmentFindController setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:1];
    [segmentFindController setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:2];
    
    [self setSegmentColor:segmentFindController]; //add customized color
    
    segmentFindController.translatesAutoresizingMaskIntoConstraints = NO;
    [segmentFindController addTarget:self action:@selector(viewFindSelectionChanged:) forControlEvents:UIControlEventValueChanged];

    s_searchBar = [[UISearchBar alloc] init];
    s_searchBar.showsCancelButton = YES;
    [s_searchBar setBarTintColor:[UIColor darkGrayColor]];
    s_searchBar.delegate = self;
    s_searchBar.translatesAutoresizingMaskIntoConstraints = NO;
    s_searchBar.searchBarStyle = UISearchBarStyleDefault;
    s_searchBar.translucent = YES;
    s_searchBar.layer.borderWidth = 1;
    s_searchBar.tag = 1;
    s_searchBar.layer.borderColor = [UIColor darkGrayColor].CGColor;

    
    viewDictionary = NSDictionaryOfVariableBindings(segmentFindController,s_searchBar);
    
    [self.view addSubview:segmentFindController];
    [self.view addSubview:s_searchBar];
    //set constraits for segment controller
    
    // Vertical constrains
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-2)-[segmentFindController(48)]"] options:0 metrics:0 views:viewDictionary]];
    
    
    // Horizontal constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(-2)-[segmentFindController(%f)]",SCREEN_WIDTH+4] options:0 metrics:0 views:viewDictionary]];
    
    
    // Vertical constrains
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(44)-[s_searchBar(44)]"] options:0 metrics:0 views:viewDictionary]];
    
    
    // Horizontal constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[s_searchBar(%f)]",SCREEN_WIDTH] options:0 metrics:0 views:viewDictionary]];
    

    
}

//set color to selected segment
-(void) setSegmentColor:(UISegmentedControl *) segmentSender
{
    
    for (int i=0; i<[segmentSender.subviews count]; i++)
    {
        if ([[segmentSender.subviews objectAtIndex:i]isSelected] )
        {
            UIColor *tintcolor=[UIColor colorWithRed:80/255.0 green:199/255.0 blue:228/255.0 alpha:1.0];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
            //break;
        }
        else {
            UIColor *tintcolor=[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
        }
    }
    for (int i=0; i<[segmentSender.subviews count]; i++)
    {
        if (![[segmentSender.subviews objectAtIndex:i]isSelected] )
        {
            UIColor *tintcolor=[UIColor darkGrayColor];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
            // break;
        }
        
    }
    
    float segmentFont = 14.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        segmentFont = 18.0;
    }
    
    // Text font color for selected segment
    NSDictionary *attributes = @{
                                 NSFontAttributeName : [UIFont boldSystemFontOfSize:segmentFont],
                                 NSForegroundColorAttributeName : [UIColor blackColor]};
    [segmentSender setTitleTextAttributes:attributes forState:UIControlStateSelected];
    
    NSDictionary *attributes2 = @{
                                  NSFontAttributeName : [UIFont boldSystemFontOfSize:segmentFont],
                                  NSForegroundColorAttributeName : [UIColor whiteColor]};
    [segmentSender setTitleTextAttributes:attributes2 forState:UIControlStateNormal];

    
}

-(void) viewFindSelectionChanged:(id)sender{
    
    switch ([segmentFindController selectedSegmentIndex]) {
        case 0:
        {
            [s_searchBar resignFirstResponder];
            ReleaseAndNilify(locServiceDealsTable);
            [loading stopAnimating];
             [defaults setBool:NO forKey:@"ViewMore"];
            //[locServiceDealsTable setContentOffset:CGPointZero];
            oldSegmentedIndex = 0;
            [self setSegmentColor:segmentFindController];
            if(findLocationServiceObjArray)
                [findLocationServiceObjArray removeAllObjects];
            
            if(arrFindCategoryInfo)
                [arrFindCategoryInfo removeAllObjects];
            
            if(globalRetailerService)
                [globalRetailerService removeAllObjects];
            lastVisitedRecord=0;
            sortFilObj.distanceSelected = TRUE;
            sortFilObj.alphabeticallySelected = FALSE;

            if ([keyWord length]) // Ret Search
            {
                [self request_ssretsearch:keyWord withObj:sortFilObj];
                
            }
            
            else  if ([typeStr length]) // Cat Search
            {
                [self request_sscatsearch:catRetSearchKey withObj:sortFilObj];
                
            }
        }
            break;
        case 1:
        {
            [s_searchBar resignFirstResponder];
            [loading stopAnimating];
             ReleaseAndNilify(locServiceDealsTable);
             [defaults setBool:NO forKey:@"ViewMore"];
           // [locServiceDealsTable setContentOffset:CGPointZero];
            oldSegmentedIndex = 1;
             [self setSegmentColor:segmentFindController];
            if(findLocationServiceObjArray)
                [findLocationServiceObjArray removeAllObjects];
            
            if(arrFindCategoryInfo)
                [arrFindCategoryInfo removeAllObjects];
            
            if(globalRetailerService)
                [globalRetailerService removeAllObjects];
            lastVisitedRecord=0;
            sortFilObj.distanceSelected = FALSE;
            sortFilObj.alphabeticallySelected = TRUE;

            if ([keyWord length]) // Ret Search
            {
                [self request_ssretsearch:keyWord withObj:sortFilObj];
                
            }
            
            else  if ([typeStr length]) // Cat Search
            {
                [self request_sscatsearch:catRetSearchKey withObj:sortFilObj];
                
            }

        }
            break;
        case 2:{
            [s_searchBar resignFirstResponder];
            for (int i=0; i<[findLocationServiceObjArray count]; i++) {
                [globalRetailerService addObject:[findLocationServiceObjArray objectAtIndex:i]];
            }
            if ([RegionApp isEqualToString:@"1"]){
                fromRegionFind = TRUE;//from region app
            }
            else
                fromFind=TRUE;
            
            fromFilters=FALSE;
            fromNearBy=FALSE;
            if ((fromFind==TRUE || fromRegionFind == TRUE /*|| fromFindSingleCategory==TRUE*/) && [globalRetailerService count]>0) {
                showMapFlag=TRUE;
                FindLocationServiceViewController *iShowMapController =[[FindLocationServiceViewController alloc]initWithNibName:@"FindLocationServiceViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:iShowMapController animated:NO];
                // [iShowMapController release];
                
            }
            else{
                [segmentFindController setSelectedSegmentIndex:oldSegmentedIndex];
                [self setSegmentColor:segmentFindController];
                [UtilityManager showAlert:nil msg:NSLocalizedString(@"Retailers are not available to plot on map",@"Retailers are not available to plot on map")];
            }
            
            
            
        }
            break;
        default:
            break;
    }
    
}


-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"mitmidfind------>%@",[defaults valueForKey:@"findLocationMitemId"]);
    if(showMapFlag == TRUE){
        cusNav =(CustomizedNavController *) self.navigationController;
        cusNav.customNavBardelegate = self;
        [cusNav hideHambergerButton:YES];
        [cusNav hideBackButton:NO];
        
        [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    }
    else{
        cusNav =(CustomizedNavController *) self.navigationController;
        cusNav.customNavBardelegate = self;
        [cusNav hideBackButton:NO];
        [cusNav hideHambergerButton:NO];
        [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
        

    }
    [super viewWillAppear:animated];
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (UIViewController *controller in controllerArray){
        //Code here.. e.g. print their titles to see the array setup;
        NSLog(@"%@",controller.class);
    }
    //[super viewWillAppear:YES];
    
    locServiceDealsTable.scrollEnabled = YES;
    locServiceDealsTable.allowsSelection = YES;
    if([segmentFindController selectedSegmentIndex] == 2){
        
        [segmentFindController setSelectedSegmentIndex:oldSegmentedIndex];
        [self setSegmentColor:segmentFindController];
        
    }
   [locServiceDealsTable setContentOffset:CGPointZero];
    if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
//        [defaults setBool:NO forKey:@"ViewMore"];
        
        ReleaseAndNilify(locServiceDealsTable);
//        lastVisitedRecord=0;
//        
//        [loading stopAnimating];
        nextPage = 0;
        lastVisitedRecord=0;
        if(findLocationServiceObjArray)
            [findLocationServiceObjArray removeAllObjects];
        
        if(arrFindCategoryInfo)
            [arrFindCategoryInfo removeAllObjects];
        
        if(globalRetailerService)
            [globalRetailerService removeAllObjects];
        
        sortFilObj = [[SortAndFilter alloc]initWithObject:[self loadCustomObjectWithKey:@"SortFilterObject"]];
        DLog(@"%d", sortFilObj.distanceSelected);
        if(sortFilObj.distanceSelected){
            
            [segmentFindController setSelectedSegmentIndex:0];
            [self setSegmentColor:segmentFindController];
            
        }
        else if(sortFilObj.alphabeticallySelected){
            
            [segmentFindController setSelectedSegmentIndex:1];
            [self setSegmentColor:segmentFindController];

        }
        if (inCategorySort) {
            [self parseScanSeeRetailerSet];
        }
        else{
            lastVisitedRecord=0;
            
            if ([keyWord length]) // Ret Search
            {
                [self request_ssretsearch:keyWord withObj:sortFilObj];
            }
            
            else  if ([typeStr length]) // Cat Search
            {
                [self request_sscatsearch:catRetSearchKey withObj:sortFilObj];
            }
        }
        
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        [defaults setValue:nil forKey:@"SortFilterObject"];
    }
    
}
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}
- (SortAndFilter *)loadCustomObjectWithKey:(NSString *)key {
    
    NSData *encodedObject = [defaults objectForKey:key];
    SortAndFilter *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}


- (void)addItemViewController:(SwipesViewController *)controller didFinishEnteringItem:(SortAndFilter *)item
{
    
    [super viewWillAppear:YES];
    for (UIView *addItemView in self.view.subviews)
        addItemView.userInteractionEnabled=YES;
    locServiceDealsTable.scrollEnabled = YES;
    locServiceDealsTable.allowsSelection = YES;
   iSwipeViewController= nil;
    sortFilObj = [[SortAndFilter alloc]initWithObject:item];
    //    if (tappedSearch==TRUE && fromSortFind==TRUE) {
    //
    //        lastVisitedRecord=0;
    //        if(globalRetailerService)
    //            [globalRetailerService removeAllObjects];
    //
    //        if(findLocationServiceObjArray)
    //            [findLocationServiceObjArray removeAllObjects];
    //
    //        if(arrFindCategoryInfo)
    //            [arrFindCategoryInfo removeAllObjects];
    //
    //        if (inCategorySort==TRUE) {
    //            [self request_sscatsearch:catRetSearchKey withObj:item];
    //        }
    //        else
    //        {
    //            [self request_ssretsearch:keyWord withObj:item];
    //        }
    //    }
    //    else
    //    {
    
    if([defaults valueForKey:@"isComingFromGroupingandSorting"] && showMapFlag!=TRUE)
    {
        lastVisitedRecord=0;
        
        if(globalRetailerService)
            [globalRetailerService removeAllObjects];
        
        if(findLocationServiceObjArray)
            [findLocationServiceObjArray removeAllObjects];
        if(arrFindCategoryInfo)
            [arrFindCategoryInfo removeAllObjects];
        
        if ([keyWord length]>0 || [catRetSearchKey length]>0) {
            tappedSearch=TRUE;
        }
        
        [locServiceDealsTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        if(sortFilObj.distanceSelected){
            
            [segmentFindController setSelectedSegmentIndex:0];
            [self setSegmentColor:segmentFindController];
            
        }
        else if(sortFilObj.alphabeticallySelected){
            
            [segmentFindController setSelectedSegmentIndex:1];
            [self setSegmentColor:segmentFindController];
            
        }
        if (inCategorySort) {
            [self parseScanSeeRetailerSet];
        }
        else{
            lastVisitedRecord=0;
            
            if ([typeStr length]) // Cat Search
            {
                [self request_sscatsearch:catRetSearchKey withObj:item];
            }
            
            else if ([keyWord length]) // Ret Search
            {
                [self request_ssretsearch:keyWord withObj:item];
            }
        }
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        //[table_Dining deselectRowAtIndexPath:[table_Dining indexPathForSelectedRow]  animated:YES];
    }
    //    }
    [locServiceDealsTable deselectRowAtIndexPath:[locServiceDealsTable indexPathForSelectedRow] animated:YES];
    
    
}

-(void)settableViewOnScreen
{
    if(!locServiceDealsTable){
        locServiceDealsTable = [[UITableView alloc]init];
        locServiceDealsTable.dataSource=self;
        locServiceDealsTable.delegate=self;
        [locServiceDealsTable setBackgroundColor:[UIColor whiteColor]];
        [locServiceDealsTable setAccessibilityIdentifier:@"findRetailers"];
        locServiceDealsTable.tableFooterView.hidden=YES;
        [self.view addSubview:locServiceDealsTable];
        locServiceDealsTable.translatesAutoresizingMaskIntoConstraints = NO;
        [self setConstraints:nil tag:0];
    }
}


-(void)popBackToPreviousPage{
    //[catRetSearchKey release];
    
    [defaults setBool:YES forKey:@"PoppedBack"];
    if (showMapFlag==TRUE) {
        showMapFlag=FALSE;
        if (keyWord!=NULL) {
            if ([keyWord length]>0) {
                tappedSearch=TRUE;
            }
            else
            {
                tappedSearch=FALSE;
            }
        }
        
        if (catRetSearchKey!=NULL) {
            if ([catRetSearchKey length]>0){
                inCategorySort=TRUE;
            }
            else
            {
                inCategorySort=FALSE;
            }
        }
        //        fromSortFind=TRUE;
        if(inCategorySort==TRUE)
        {
            tappedSearch=TRUE;
        }
    }
    else
    {
        //    fromSortFind=FALSE;
        if (inCategorySort) {
            inCategorySort=FALSE;
        }
    }
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (UIViewController *controller in controllerArray){
        //Code here.. e.g. print their titles to see the array setup;
        NSLog(@"%@",controller.class);
    }
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}

//options

-(void)options:(UIBarButtonItem*) sender
{
    ReleaseAndNilify(locServiceDealsTable);
    FindOptionsViewController *iFindOptionsListViewContoller = [[FindOptionsViewController alloc]initWithNibName:@"FindOptionsViewController" bundle:[NSBundle mainBundle]];
    iFindOptionsListViewContoller.typeStr=typeStr;
    iFindOptionsListViewContoller.catId=catId;
    iFindOptionsListViewContoller.srtFilterObj = sortFilObj;
    if (![catRetSearchKey isEqualToString:@""]){
        iFindOptionsListViewContoller.searchKey=catRetSearchKey;
    }
    
    else if (![keyWord isEqualToString:@""]){
        iFindOptionsListViewContoller.searchKey=keyWord;
    }
    
    
    
    for (int i=0; i<[findLocationServiceObjArray count]; i++) {
        [globalRetailerService addObject:[findLocationServiceObjArray objectAtIndex:i]];
    }
    if ([RegionApp isEqualToString:@"1"]){
        fromRegionFind = TRUE;//from region app
    }
    else
        fromFind=TRUE;
    
    fromFilters=FALSE;
    fromNearBy=FALSE;
    [self.navigationController pushViewController:iFindOptionsListViewContoller animated:NO];
    //  [iFindOptionsListViewContoller release];
    
}

-(void)optionsShowMap
{
    s_searchBar.hidden = YES;
    //self.navigationItem.rightBarButtonItem.tag = 2 ;
    //UIButton *showListBtn = [UtilityManager customizeBarButtonItem:@"Show List"];
    //[showListBtn addTarget:self action:@selector(swapListMap:) forControlEvents:UIControlEventTouchUpInside];
   // showListBtn.tag=2;
    //[self.navigationItem.rightBarButtonItem setCustomView:showListBtn];
    
    mapView.hidden = NO;
    locServiceDealsTable.hidden = YES;
    
    mapView.hidden = NO;
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    [mapView setDelegate:self];
   // CLLocationCoordinate2D southWest;
   // CLLocationCoordinate2D northEast;
    
    //check is reqd as in case only scansee data is there and no google data it would crash
    
//    southWest = CLLocationCoordinate2DMake([[[globalRetailerService objectAtIndex:0]retLatitude] doubleValue],
//                                           [[[globalRetailerService objectAtIndex:0]retLongitude] doubleValue]);
   // northEast = southWest;
    
    
    for(int i=0; i<[globalRetailerService count]; i++) {
        RetailerAnnotation *ann = [[RetailerAnnotation alloc] init];
        ann.coordinate = CLLocationCoordinate2DMake([[[globalRetailerService objectAtIndex:i]retLatitude] doubleValue],
                                                    [[[globalRetailerService objectAtIndex:i]retLongitude] doubleValue]);
        [mapView addAnnotation:ann];
        [annArray addObject:ann];
        [ann setTitle:[[globalRetailerService objectAtIndex:i]retailerName]];
        [ann setSubtitle:[[globalRetailerService objectAtIndex:i]completeAddress]];
        ReleaseAndNilify(ann);
        
    }
    
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in mapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    
    [mapView setVisibleMapRect:zoomRect animated:YES];
    
    
}


// Swap betWeen tableView and MapView
-(void) swapListMap : (UIBarButtonItem *) sender {
    
    if ([g_searchBar isFirstResponder]){
        [g_searchBar resignFirstResponder];
        [UIView beginAnimations:@"slideOut" context:NULL];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.3];
        [UIView commitAnimations];
        
        //        g_view.frame = viewFrame;
    }
    
    switch (sender.tag) {
        case 2:
        {
            //            g_view.hidden = YES;
            s_searchBar.hidden = NO;
            self.navigationItem.rightBarButtonItem.tag = 1 ;
            UIButton *showMapBtn = [UtilityManager customizeBarButtonItem:@"Show Map"];
            [showMapBtn addTarget:self action:@selector(swapListMap:) forControlEvents:UIControlEventTouchUpInside];
            showMapBtn.tag=1;
            [self.navigationItem.rightBarButtonItem setCustomView:showMapBtn];
            
            mapView.hidden = YES;
            locServiceDealsTable.hidden = NO;
            [mapView removeAnnotations:annArray];
        }
            break;
        case 1:
        {
            //            g_view.hidden = YES;
            s_searchBar.hidden = YES;
            self.navigationItem.rightBarButtonItem.tag = 2 ;
            UIButton *showListBtn = [UtilityManager customizeBarButtonItem:@"Show List"];
            [showListBtn addTarget:self action:@selector(swapListMap:) forControlEvents:UIControlEventTouchUpInside];
            showListBtn.tag=2;
            [self.navigationItem.rightBarButtonItem setCustomView:showListBtn];
            
            mapView.hidden = NO;
            locServiceDealsTable.hidden = YES;
            
            mapView.hidden = NO;
            [mapView setMapType:MKMapTypeStandard];
            [mapView setZoomEnabled:YES];
            [mapView setScrollEnabled:YES];
            [mapView setDelegate:self];
            //CLLocationCoordinate2D southWest;
            //CLLocationCoordinate2D northEast;
            if ([findLocationServiceObjArray count] == 0 || findLocationServiceObjArray == nil )
            {
                return;
            }
            //check is reqd as in case only scansee data is there and no google data it would crash
            
//            southWest = CLLocationCoordinate2DMake([[[findLocationServiceObjArray objectAtIndex:0]retLatitude] doubleValue],
//                                                   [[[findLocationServiceObjArray objectAtIndex:0]retLongitude] doubleValue]);
           // northEast = southWest;
            
            
            for(int i=0; i<[findLocationServiceObjArray count]; i++) {
                RetailerAnnotation *ann = [[RetailerAnnotation alloc] init];
                ann.coordinate = CLLocationCoordinate2DMake([[[findLocationServiceObjArray objectAtIndex:i]retLatitude] doubleValue],
                                                            [[[findLocationServiceObjArray objectAtIndex:i]retLongitude] doubleValue]);
                [mapView addAnnotation:ann];
                [annArray addObject:ann];
                [ann setTitle:[[findLocationServiceObjArray objectAtIndex:i]retailerName]];
                [ann setSubtitle:[[findLocationServiceObjArray objectAtIndex:i]completeAddress]];
                ReleaseAndNilify(ann);
                
            }
            
            MKMapRect zoomRect = MKMapRectNull;
            for (id <MKAnnotation> annotation in mapView.annotations)
            {
                MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
                MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
                zoomRect = MKMapRectUnion(zoomRect, pointRect);
            }
            [mapView setVisibleMapRect:zoomRect animated:YES];
            
        }
            break;
        default:
            break;
    }
}


-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    MKPinAnnotationView* pinView = nil;
    
    if ([annotation isKindOfClass:[RetailerAnnotation class]])
    {
        // try to dequeue an existing pin view first
        static NSString* RetailerAnnotationIdentifier = @"retailerAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:RetailerAnnotationIdentifier];
        
        if (!pinView)
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
                                                  initWithAnnotation:annotation reuseIdentifier:RetailerAnnotationIdentifier] ;
            //customPinView.pinColor = MKPinAnnotationColorPurple;
            customPinView.animatesDrop = YES;
            
            customPinView.canShowCallout = YES;
            UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            
            customPinView.rightCalloutAccessoryView = infoButton;
            
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
    }
    pinView.canShowCallout = YES;
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    
    for (int j = 0 ; j < [globalRetailerService count]; j++) {
        if ([[[globalRetailerService objectAtIndex:j]retailerName] isEqualToString:view.annotation.title]) {
            [linkID insertObject:@"0" atIndex:[linkID count]];
            [self request_retsummary:j withSection:0];
            return;
        }
    }
    
}

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

-(void)bottomButtonPressed:(id)sender
{
    [defaults setBool:NO forKey:@"ViewMore"];
    showMapFlag = FALSE;
    [defaults setBool:YES forKey:BottomButton];
    //    [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    if(![[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
        [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    UIButton *btn = (UIButton*)sender;
    int tag =(int) btn.tag;
    
    bottomButtonDO *obj_findBottomDO = [arrFindSingleBottomButtonDO objectAtIndex:tag];
    if(!([[obj_findBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_findBottomDO.btnLinkTypeName]intValue]==26||[[obj_findBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_findBottomDO.btnLinkTypeName]intValue]==27||[[obj_findBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_findBottomDO.btnLinkTypeName]intValue]==28) )
    {

    [defaults setValue:obj_findBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    }
    if ([[obj_findBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_findBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_findBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_findBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_findBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_findBottomDO.btnLinkTypeName);
    
    if ([obj_findBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_findBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_findBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_findBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_findBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_findBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_findBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_findBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            self.iEventsListViewController = iEventsListViewController;
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    else{ //other menu types
        switch ([[obj_findBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_findBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                self.hotDeals = hotDeals;
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                //[hotDeals release];
                break;
                
            case 1://Scan Now
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_findBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_findBottomDO.bottomBtnID];
                }
                [EventsBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                [self navigateToEventList];
                break;
            case 4://Whats NearBy
                [NearbyBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                [self navigateToFindView];
            }
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                self.cityExp = citi;
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
            {
                
            }
                
                break;
                
            case 8://Visitors Bureau
            {
                
            }
                
                break;
                
            case 9://transportation
            {
                
            }
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=TRUE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_findBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    
                    CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    self.cityExp = cevc;
                    [SharedManager setRefreshAustinRetailers:YES];
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    self.filters = filters;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                //   [dvc release];
                
                break;
            }
                
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                [FundraiserBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }break;
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETUSERINFOM;
                    [self requestToGetUserData];
                }
            }break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETFAVLOCATION;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!")  msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        // [inform release];
                    }
                    
                }
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            // [citPref release];
                        }
                    }
                }
                
            }break;
                
            case 26://Map
            {
                for (int i=0; i<[findLocationServiceObjArray count]; i++) {
                    [globalRetailerService addObject:[findLocationServiceObjArray objectAtIndex:i]];
                }
                if ([RegionApp isEqualToString:@"1"]){
                    fromRegionFind = TRUE;//from region app
                }
                else
                    fromFind=TRUE;
                
                fromFilters=FALSE;
                fromNearBy=FALSE;
                if ((fromFind==TRUE || fromRegionFind == TRUE /*|| fromFindSingleCategory==TRUE*/) && [globalRetailerService count]>0) {
                    showMapFlag=TRUE;
                    FindLocationServiceViewController *iShowMapController =[[FindLocationServiceViewController alloc]initWithNibName:@"FindLocationServiceViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:iShowMapController animated:NO];
                    // [iShowMapController release];
                    
                }
                else{
                    [UtilityManager showAlert:nil msg:NSLocalizedString(@"Retailers are not available to plot on map",@"Retailers are not available to plot on map")];
                }
                
                
                
            }
                break;
                
            case 27:
            case 28://Sort/Filter
            {
               
                for (int i=0; i<[findLocationServiceObjArray count]; i++) {
                    [globalRetailerService addObject:[findLocationServiceObjArray objectAtIndex:i]];
                }
                if ([RegionApp isEqualToString:@"1"]){
                    fromRegionFind = TRUE;//from region app
                }
                else
                    fromFind=TRUE;
                
                fromFilters=FALSE;
                fromNearBy=FALSE;
                [defaults setValue:[defaults valueForKey:@"findLocationMitemId"] forKey:KEY_MITEMID];
                
                if (fromFind==TRUE)
                {
                    
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    
                    
                    /*  if(iSwipeViewController)
                     [iSwipeViewController release];*/
                    
                    SwipesViewController *iSwipeViewController1 = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                    iSwipeViewController1.module = @"Find All";//Find All , Find Single , Events, CitiEXP
                    NSArray *viewControllers = [self.navigationController viewControllers];
                    long vIndex=[viewControllers count]-2;
                    iSwipeViewController1.delegate = [self.navigationController.viewControllers objectAtIndex:vIndex];
                    iSwipeViewController1.categoryName = typeStr;
                    iSwipeViewController1.catId = catId;
                    iSwipeViewController1.sortFilObj = sortFilObj;
                    if (![catRetSearchKey isEqualToString:@""]){
                        iSwipeViewController1.srchKey=catRetSearchKey;
                    }
                    
                    else if (![keyWord isEqualToString:@""]){
                        iSwipeViewController1.srchKey=keyWord;
                    }
                    iSwipeViewController.segmentSelected = oldSegmentedIndex;
                    [self.navigationController pushViewController:iSwipeViewController1 animated:NO];
                    
                }
                else if (fromRegionFind==TRUE)
                {
                    
                    /* if(iSwipeViewController)
                     [iSwipeViewController release];*/
                    
                    SwipesViewController* iSwipeViewController2 = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                    iSwipeViewController2.module = @"Find All";//Find All , Find Single , Events, CitiEXP
                    NSArray *viewControllers = [self.navigationController viewControllers];
                    long vIndex=[viewControllers count]-2;
                    iSwipeViewController2.delegate = [self.navigationController.viewControllers objectAtIndex:vIndex];
                    iSwipeViewController2.categoryName = typeStr;
                    iSwipeViewController2.catId = catId;
                    iSwipeViewController2.sortFilObj = sortFilObj;
                    if (![catRetSearchKey isEqualToString:@""]){
                        iSwipeViewController2.srchKey=catRetSearchKey;
                    }
                    
                    else if (![keyWord isEqualToString:@""]){
                        iSwipeViewController2.srchKey=keyWord;
                    }
                    [self.navigationController pushViewController:iSwipeViewController2 animated:NO];
                    
                }
            }
                break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
               // privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
                
                
            default:
                break;
        }
    }
    //NSlog(@"%d",tag);
}


// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    // Calculate Y position for View
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [arrFindSingleBottomButtonDO count]; btnLoop++)
    {
        bottomButtonDO *obj_eventBottomDO = [arrFindSingleBottomButtonDO objectAtIndex:btnLoop];
        
        bottomBtnView = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            bottomBtnView.contentMode = UIViewContentModeCenter;
            bottomBtnView = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, 80,bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            bottomBtnView = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrFindSingleBottomButtonDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrFindSingleBottomButtonDO count],bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [bottomBtnView setAccessibilityLabel:obj_eventBottomDO.btnLinkTypeName];
        [bottomBtnView setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:bottomBtnView];
        [self.view addSubview:bottomBtnView];
        [self.view bringSubviewToFront:bottomBtnView];
    }
}







#pragma mark REQUEST METHODS



-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = hubcitiAnythingInfo;
    requestAnythingFromMainMenu=TRUE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetail;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

//-(void)request_GetUserCat{
//
//    iWebRequestState = GETFAVCATEGORIE;
//    if([Network currentReachabilityStatus]==0)
//    {
//        [common showAlert];
//    }
//    else {
//
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//
//    }
//}


-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATION;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}

//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRETS;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if (![defaults boolForKey:BottomButton]) {
        if([defaults valueForKey:KEY_MITEMID])
            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    
    else{
        if([defaults valueForKey:KEY_BOTTOMBUTTONID])
            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}

//in case of multiple partners
//-(void)request_GetPartners{
//
//    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getpartners?citiExperId=%@",BASE_URL,[defaults valueForKey:KEY_LINKID]];
//
//    iWebRequestState = GETPARTNER;
//    [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
//
//}


//fetch the retailer summary of a ScanSee retailer
-(void)request_retsummary:(int)arrayIndex withSection:(int)sectionIndex
{
    selectedindexRow = arrayIndex;
    selectedSection = sectionIndex;
    iWebRequestState = SSRETSUMMARY;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    //    if([defaults valueForKey:@"GroupFindBy"])
    //    {
    //        FindCategoryDO *iCitiExpCategoryDO = [arrFindCategoryInfo objectAtIndex:selectedSection];
    //
    //        [requestStr appendFormat:@"<retailerId>%@</retailerId>",[[iCitiExpCategoryDO.locationServiceObjArray objectAtIndex:selectedindexRow]retId]];
    //        [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[[iCitiExpCategoryDO.locationServiceObjArray objectAtIndex:selectedindexRow]retLocnId]];
    //        [requestStr appendFormat:@"<retListId>%@</retListId>",[[iCitiExpCategoryDO.locationServiceObjArray objectAtIndex:selectedindexRow]retListId]];
    //
    //    }
    //
    //   else
    
    if([findLocationServiceObjArray count] > arrayIndex){
        [requestStr appendFormat:@"<retailerId>%@</retailerId>",[[findLocationServiceObjArray objectAtIndex:arrayIndex]retailerId]];
        
        [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[[findLocationServiceObjArray objectAtIndex:arrayIndex]retailLocationId]];
        [requestStr appendFormat:@"<retListId>%@</retListId>",[[findLocationServiceObjArray objectAtIndex:arrayIndex]retListId]];
    }
    else
    {
        [requestStr appendFormat:@"<retailerId>%@</retailerId>",[[globalRetailerService objectAtIndex:arrayIndex]retailerId]];
        
        [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[[globalRetailerService objectAtIndex:arrayIndex]retailLocationId]];
        [requestStr appendFormat:@"<retListId>%@</retListId>",[[globalRetailerService objectAtIndex:arrayIndex]retListId]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [requestStr appendFormat:@"<scanTypeId>%@</scanTypeId>",@"0"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
//    NSString *urlString = [NSString stringWithFormat:@"http://10.10.220.231:9990/HubCiti2.9/thislocation/retsummary"];
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    //[requestStr release];
}

-(void)request_ssretsearch:(NSString*)serachKey withObj:(SortAndFilter *)sortFilterObj
{
    iWebRequestState = SSRETSEARCH;
    if(![defaults boolForKey:@"ViewMore"]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [HubCitiAppDelegate showActivityIndicator];
        });
    }
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    if([defaults valueForKey:KEY_USERID])
    {
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if(keyWord)
    {
        [parameters setValue:keyWord forKey:@"searchKey"];
    }
    
    [parameters setValue:[NSString stringWithFormat:@"%ld",(long)lastVisitedRecord] forKey:@"lastVisitedNo"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
    {
        [parameters setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    }
    if (![defaults boolForKey:BottomButton])
    {
        if([defaults valueForKey:KEY_MITEMID])
        {
           // [defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"findLocationMitemId"];
            [parameters setValue:[defaults valueForKey:@"findLocationMitemId"] forKey:@"mItemId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
        }
        else if ([FindBottomButtonID count]>=1)
        {
            [parameters setValue:[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1] forKey:@"bottomBtnId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
            
        }
    }
    else
    {
        if ([defaults valueForKey:KEY_MITEMID]&& [[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
        {
            //[defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"findLocationMitemId"];
            [parameters setValue:[defaults valueForKey:@"findLocationMitemId"] forKey:@"mItemId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
        }
        [defaults setBool:YES forKey:BottomButton];
        if ([FindBottomButtonID count]>=1)
        {
            [parameters setValue:[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1] forKey:@"bottomBtnId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
        }
    }
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length])
    {
        [parameters setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        [parameters setValue:[defaults valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
    }
    else
    {
        if ([defaults valueForKey:KEYZIPCODE]){
            [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"postalCode"];
        }
    }
    
    [parameters setValue:@"ASC" forKey:@"sortOrder"];
    if([segmentFindController selectedSegmentIndex]==0){
    
        [parameters setValue:@"distance" forKey:@"sortColumn"];
    
    }else{
    
        [parameters setValue:@"atoz" forKey:@"sortColumn"];
    
    }
    if (sortFilterObj.selectedCitiIds)
    {
        [parameters setValue:sortFilterObj.selectedCitiIds forKey:@"cityIds"];
    }
    
    if(sortFilterObj.selectedOthersIds)
    {
        [parameters setValue:sortFilterObj.selectedOthersIds forKey:@"fValueId"];
    }
    if (sortFilterObj.selectedOptIds)
    {
        [parameters setValue:sortFilterObj.selectedOptIds forKey:@"filterId"];
    }
    if(sortFilterObj.selectedCatIds)
    {
        [parameters setValue:sortFilterObj.selectedCatIds forKey:@"subCatIds"];
    }
    if (sortFilterObj.selectedInterestsIds)
    {
        [parameters setValue:sortFilterObj.selectedInterestsIds forKey:@"interests"];
    }
    
    if (sortFilterObj.localSpecialSelected)
    {
        [parameters setValue:@"1" forKey:@"locSpecials"];
    }
    else
    {
        [parameters setValue:@"0" forKey:@"locSpecials"];
    }
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [timeFormatter setTimeZone:timeZone];
    [timeFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *timeInUTC = [timeFormatter stringFromDate:currentTime];
    NSLog(@"Current time %@", timeInUTC);
    [parameters setValue:timeInUTC forKey:@"requestedTime"];

    //    if([RegionApp isEqualToString:@"1"] && [defaults valueForKey:@"sortedCitiIDs"])
    //    {
    //        [parameters setValue:[defaults valueForKey:@"sortedCitiIDs"] forKey:@"cityIds"];
    //
    //    }
    //    if([defaults valueForKey:@"FindRadius"])
    //    {
    //        [parameters setValue:[defaults valueForKey:@"FindRadius"] forKey:@"radius"];
    //
    //    }
    //    else
    //    {
    //        [parameters setValue:@"50" forKey:@"radius"];
    //
    //    }
    
    DLog(@"parameter: %@",parameters);
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/ssretsearchjson",BASE_URL];
    //NSString *urlString = [NSString stringWithFormat:@"http://sdw2107:9990/HubCiti2.4.2/find/ssretsearchjson"];
    if ([defaults boolForKey:@"ViewMore"])
    {
        DLog(@"Url: %@",urlString);
        
        NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
        
        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
        id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        DLog(@"%@",responseData);
        [self parse_CatRetSearch:responseData];
    }
    else
    {
        HTTPClient *client = [HTTPClient sharedHTTPClient];
        client.delegate = self;
        DLog(@"Url: %@",urlString);
        [client sendRequest : parameters : urlString];
    }
    
    
    
    //    NSMutableString *requestStr = [[NSMutableString alloc] init];
    //    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    //
    //
    //        [requestStr appendFormat:@"<searchKey>%@</searchKey>",keyWord];
    //
    //
    //    [requestStr appendFormat:@"<lastVisitedNo>%d</lastVisitedNo>",lastVisitedRecord];
    //
    //    if([defaults valueForKey:KEY_MAINMENUID])
    //        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    ////    else if([defaults valueForKey:KEY_MITEMID])
    ////        [requestStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];
    ////
    ////    else if([defaults valueForKey:KEY_BOTTOMBUTTONID])
    ////        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    //
    //        if (![defaults boolForKey:BottomButton]) {
    //            //        if (userIdentifier==TRUE) {
    //            //            //[defaults setBool:YES forKey:BottomButton];
    //            //            [requestStr appendFormat:@"<bottomBtnId>0</bottomBtnId><platform>IOS</platform>"];
    //            //        }
    //            if([defaults valueForKey:KEY_MITEMID]){
    //                [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    //            }
    //            else if ([FindBottomButtonID count]>=1) {
    //
    //                [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
    //
    //            }
    //
    //        }
    //        else{
    //            if ([defaults valueForKey:KEY_MITEMID]&& [[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
    //            {
    //                [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    //            }
    //            [defaults setBool:YES forKey:BottomButton];
    //            if ([FindBottomButtonID count]>=1) {
    //                [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
    //            }
    //        }
    //    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length]){
    //
    //        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
    //         [defaults  valueForKey:KEY_LATITUDE]];
    //
    //    }
    //    else
    //    {
    //        if ([defaults valueForKeyPath:KEYZIPCODE]) {
    //            [requestStr appendFormat:@"<postalCode>%@</postalCode>",[defaults valueForKeyPath:KEYZIPCODE]];
    //        }
    //    }
    //    [requestStr appendFormat:@"<sortOrder>ASC</sortOrder>"];
    //
    //    if (sortFilterObj.distanceSelected) {
    //        [requestStr appendFormat:@"<sortColumn>distance</sortColumn>"];
    //    }
    //
    //    if (sortFilterObj.alphabeticallySelected) {
    //        [requestStr appendFormat:@"<sortColumn>atoz</sortColumn>"];
    //    }
    //
    //
    //    if (sortFilterObj.selectedCitiIds) {
    //        [requestStr appendFormat:@"<cityIds>%@</cityIds>",sortFilterObj.selectedCitiIds];
    //    }
    //
    //    if (sortFilterObj.selectedOthersIds) {
    //        [requestStr appendFormat:@"<fValueId>%@</fValueId>",sortFilterObj.selectedOthersIds];
    //    }
    //
    //    if (sortFilterObj.selectedOptIds) {
    //        [requestStr appendFormat:@"<filterId>%@</filterId>",sortFilterObj.selectedOptIds];
    //    }
    //
    //    if (sortFilterObj.selectedCatIds) {
    //        [requestStr appendFormat:@"<subCatIds>%@</subCatIds>",sortFilterObj.selectedCatIds];
    //    }
    //
    //    if (sortFilterObj.selectedInterestsIds) {
    //        [requestStr appendFormat:@"<interests>%@</interests>",sortFilterObj.selectedInterestsIds];
    //    }
    //
    //    if (sortFilterObj.localSpecialSelected) {
    //        [requestStr appendFormat:@"<locSpecials>1</locSpecials>"];
    //    }
    //    else{
    //        [requestStr appendFormat:@"<locSpecials>0</locSpecials>"];
    //    }
    //
    //
    //    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@find/ssretsearch",BASE_URL];
    //   // NSString *urlString = [NSString stringWithFormat:@"%@find/ssretsearch", @"http://sdw2107:8080/HubCiti2.3.2/"];
    //
    //    if ([defaults boolForKey:@"ViewMore"]) {
    //        NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    //
    //
    //        [self parse_CatRetSearch:response];
    //
    //    }
    //    else{
    //        [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    //    }
    //
    //
    //    //[ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    //    //[requestStr release];
}

// Request to get the Category Details when tab on Any Category from tableView
-(void)request_sscatsearch:(NSString*)searchText withObj:(SortAndFilter*)sortFilterObj
{
    if(![defaults boolForKey:@"ViewMore"]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [HubCitiAppDelegate showActivityIndicator];
        });
    }
    
    iWebRequestState = SSCATSEARCH;
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    //    if([defaults valueForKey:KEY_USERID])
    //    {
    //        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    //    }
    if (![typeStr isEqualToString:@""])
    {
        [parameters setValue:typeStr forKey:@"catName"];
    }
    [parameters setValue:[NSString stringWithFormat:@"%ld",(long)lastVisitedRecord] forKey:@"lastVisitedNo"];
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length])
    {
        [parameters setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        [parameters setValue:[defaults valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        if ([defaults valueForKey:KEYZIPCODE]){
            [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"userPostalCode"];
        }
    }
    else
    {
        if ([defaults valueForKey:KEYZIPCODE]){
            [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"postalCode"];
        }
    }
    if([defaults valueForKey:KEY_MAINMENUID])
    {
        [parameters setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    }
     NSLog(@"MItemID -- %@",[defaults valueForKey:KEY_MITEMID]);
    if (![defaults boolForKey:BottomButton])
    {
        NSLog(@"MItemID -- %@",[defaults valueForKey:KEY_MITEMID]);
        if([defaults valueForKey:KEY_MITEMID])
        {
          //  [defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"findLocationMitemId"];
            [parameters setValue:[defaults valueForKey:@"findLocationMitemId"] forKey:@"mItemId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
        }
        else if ([FindBottomButtonID count]>=1)
        {
            [parameters setValue:[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1] forKey:@"bottomBtnId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
            
        }
    }
    else
    {
        if ([defaults valueForKey:KEY_MITEMID]&& [[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
        {
            //[defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"findLocationMitemId"];
            [parameters setValue:[defaults valueForKey:@"findLocationMitemId"] forKey:@"mItemId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
        }
        [defaults setBool:YES forKey:BottomButton];
        if ([FindBottomButtonID count]>=1)
        {
            [parameters setValue:[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1] forKey:@"bottomBtnId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
        }
    }
    if ([searchText length])
    {
        [parameters setValue:searchText forKey:@"searchKey"];
    }
    [parameters setValue:@"ASC" forKey:@"sortOrder"];
     if([segmentFindController selectedSegmentIndex]==0){
    
        [parameters setValue:@"distance" forKey:@"sortColumn"];
    
     }
     else{
    
        [parameters setValue:@"atoz" forKey:@"sortColumn"];
    
     }
    if (sortFilterObj.selectedCitiIds)
    {
        [parameters setValue:sortFilterObj.selectedCitiIds forKey:@"cityIds"];
    }
    else{
        if([RegionApp isEqualToString:@"1"] && [defaults valueForKey:@"sortedCitiIDs"])
        {
            [parameters setValue:[defaults valueForKey:@"sortedCitiIDs"] forKey:@"cityIds"];
            
        }
    }
    if(sortFilterObj.selectedOthersIds)
    {
        [parameters setValue:sortFilterObj.selectedOthersIds forKey:@"fValueId"];
    }
    if (sortFilterObj.selectedOptIds)
    {
        [parameters setValue:sortFilterObj.selectedOptIds forKey:@"filterId"];
    }
    if(sortFilterObj.selectedCatIds)
    {
        [parameters setValue:sortFilterObj.selectedCatIds forKey:@"subCatIds"];
    }
    if (sortFilterObj.selectedInterestsIds)
    {
        [parameters setValue:sortFilterObj.selectedInterestsIds forKey:@"interests"];
    }
    
    if (sortFilterObj.localSpecialSelected)
    {
        [parameters setValue:@"1" forKey:@"locSpecials"];
    }
    else
    {
        [parameters setValue:@"0" forKey:@"locSpecials"];
    }
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    
    
    if([defaults valueForKey:@"FindRadius"])
    {
        [parameters setValue:[defaults valueForKey:@"FindRadius"] forKey:@"radius"];
        
    }
    else
    {
        [parameters setValue:@"50" forKey:@"radius"];
        
    }
    NSDate *localDate =[[NSDate alloc] init];
    NSDateFormatter *dateFormatterNew = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [dateFormatterNew setTimeZone:timeZone];
    [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatterNew stringFromDate:localDate];
    NSLog(@"date:%@",dateString);
    
    [parameters setValue:dateString forKey:@"requestedTime"];

    DLog(@"parameter: %@",parameters);
    
    
    // NSString *urlString = [NSString stringWithFormat:@"http://sdw2107:9990/HubCiti2.4.2/find/sscatsearchjson"];
    
    //    DLog(@"parameter: %@",parameters);
    //
    //
    NSString *urlString = [NSString stringWithFormat:@"%@find/sscatsearchjson",BASE_URL];
    if ([defaults boolForKey:@"ViewMore"])
    {
        DLog(@"Url: %@",urlString);
        
        NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
        // [HubCitiAppDelegate removeActivityIndicator];
        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
        id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        DLog(@"%@",responseData);
        [self parse_CatRetSearch:responseData];
    }
    else
    {
        HTTPClient *client = [HTTPClient sharedHTTPClient];
        client.delegate = self;
        DLog(@"Url: %@",urlString);
        [client sendRequest : parameters : urlString];
    }
    
    
    
    //    NSMutableString *requestStr = [[NSMutableString alloc] init];
    //    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    //    if (![typeStr isEqualToString:@""]) {
    //       [requestStr appendFormat:@"<catName>%@</catName>",typeStr];
    //    }
    //    else
    //    {
    //        [requestStr appendFormat:@"<catName>%@</catName>",type];
    //    }
    //    [requestStr appendFormat:@"<lastVisitedNo>%d</lastVisitedNo>",lastVisitedRecord];
    //
    //    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length]){
    //
    //        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
    //         [defaults  valueForKey:KEY_LATITUDE]];
    //    }
    //
    //    //for user tracking
    //    if([defaults valueForKey:KEY_MAINMENUID])
    //        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    //
    //    if (![defaults boolForKey:BottomButton]) {
    //        //        if (userIdentifier==TRUE) {
    //        //            //[defaults setBool:YES forKey:BottomButton];
    //        //            [requestStr appendFormat:@"<bottomBtnId>0</bottomBtnId><platform>IOS</platform>"];
    //        //        }
    //        if([defaults valueForKey:KEY_MITEMID]){
    //            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    //        }
    //        else if ([FindBottomButtonID count]>=1) {
    //
    //            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
    //
    //        }
    //
    //    }
    //
    //
    //    else{
    //       if ([defaults valueForKey:KEY_MITEMID]&& [[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
    //        {
    //            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    //        }
    //        [defaults setBool:YES forKey:BottomButton];
    //        if ([FindBottomButtonID count]>=1) {
    //        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
    //        }
    //         }
    //
    //
    //    if ([searchText length])
    //        [requestStr appendFormat:@"<searchKey>%@</searchKey>",searchText];
    //    [requestStr appendFormat:@"<sortOrder>ASC</sortOrder>"];
    //
    //    if (sortFilterObj.distanceSelected) {
    //        [requestStr appendFormat:@"<sortColumn>distance</sortColumn>"];
    //    }
    //
    //    if (sortFilterObj.alphabeticallySelected) {
    //        [requestStr appendFormat:@"<sortColumn>atoz</sortColumn>"];
    //    }
    //
    //    if (sortFilterObj.selectedCitiIds) {
    //        [requestStr appendFormat:@"<cityIds>%@</cityIds>",sortFilterObj.selectedCitiIds];
    //    }
    //
    //    if (sortFilterObj.selectedOthersIds) {
    //        [requestStr appendFormat:@"<fValueId>%@</fValueId>",sortFilterObj.selectedOthersIds];
    //    }
    //
    //    if (sortFilterObj.selectedOptIds) {
    //        [requestStr appendFormat:@"<filterId>%@</filterId>",sortFilterObj.selectedOptIds];
    //    }
    //
    //    if (sortFilterObj.selectedCatIds) {
    //         [requestStr appendFormat:@"<subCatIds>%@</subCatIds>",sortFilterObj.selectedCatIds];
    //    }
    //
    //    if (sortFilterObj.selectedInterestsIds) {
    //        [requestStr appendFormat:@"<interests>%@</interests>",sortFilterObj.selectedInterestsIds];
    //    }
    //
    //
    //    if (sortFilterObj.localSpecialSelected) {
    //        [requestStr appendFormat:@"<locSpecials>1</locSpecials>"];
    //    }
    //    else{
    //        [requestStr appendFormat:@"<locSpecials>0</locSpecials>"];
    //    }
    //
    //    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@find/sscatsearch",BASE_URL];
    //    if ([defaults boolForKey:@"ViewMore"]) {
    //        NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    //
    //
    //        [self parse_CatRetSearch:response];
    //
    //    }
    //    else{
    //        [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    //    }
    //    //[ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    //
    //    //[requestStr release];
    
}
-(void)requestToGetUserData
{
    [HubCitiAppDelegate showActivityIndicator];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}
-(void)parseGetUserData:(id)response
{
    
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}

// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case SSRETSEARCH:
        case SSCATSEARCH:
            [self parse_CatRetSearch:response];
            break;
            
        case SSRETSUMMARY:
            [self parse_RetSummary:response];
            break;
            
        case PARTNERRETS:
            [self parse_PartnerRet:response];
            break;
            //        case GETPARTNER:
            //            [self parse_GetPartners:response];
            //            break;
            
        case GETFAVLOCATION:
            [self parse_GetUserCat:response];
            break;
            
        case hubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetail:
            [self parse_appsitedetails:response];
            break;
            
        case GETUSERINFOM:
        {
            [self parseGetUserData:response];
        }
            break;
            
        default:
            break;
    }
}





#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        return;
    
    [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    [self showActionSheet];
    
    
}
-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = bottomBtnView;
        actionSheet.popoverPresentationController.sourceRect = bottomBtnView.bounds;
    }
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}




-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}






#pragma mark PARSE METHODS

//parse retailer summary
-(void)parse_RetSummary:(NSString*)response
{
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else
    {
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        RetailersInDetail *distanceDic;
        distanceDic = [findLocationServiceObjArray objectAtIndex:selectedindexRow];
        rsvc.distanceFromPreviousScreen = distanceDic.distance;
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}

-(void)parse_CatRetSearch:(id)response
{
    
    if(response == NULL)
    {
        [loading stopAnimating];
        return;
    }
    NSArray * controllerArray = [[self navigationController] viewControllers];
    for (UIViewController *controller in controllerArray)
    {
        //Code here.. e.g. print their titles to see the array setup;
        NSLog(@"%@",controller.class);
    }
    
    [defaults setObject:response forKey:KEY_RESPONSEXML];
    [self parseScanSeeRetailerSet];
    
   // [defaults setBool:NO forKey:@"ViewMore"];
    
    
    
}

//parses the ScanSee result set
-(void) parseScanSeeRetailerSet
{
     NSLog(@"MItemID -- %@",[defaults valueForKey:KEY_MITEMID]);
    if([defaults valueForKey:KEY_MITEMID] && ![defaults valueForKey:@"findLocationMitemId"] && ![defaults valueForKey:KEY_BOTTOMBUTTONID])
    {
        [defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"findLocationMitemId"];
    }
    RetailersDetails *resSsRet=[[RetailersDetails alloc]init];
    DLog(@"%@",[defaults  objectForKey:KEY_RESPONSEXML]);
    // DLog(@"%@",respFromFind);
    
    //NSDictionary* tempResp;
    //resSsRet = [defaults  objectForKey:KEY_RESPONSEXML];
    [resSsRet setValuesForKeysWithDictionary:[defaults  objectForKey:KEY_RESPONSEXML]];
    
    if([resSsRet.responseCode isEqualToString:@"10000"])
    {
        nextPage = [resSsRet.nextPage integerValue];
        if(resSsRet.maxRowNum)
        {
            lastVisitedRecord = [resSsRet.maxRowNum intValue];
        }
        bottomBtn = [resSsRet.bottomBtn intValue];
        if(resSsRet.RetailerDetail!=nil)
            
            
        {
            //findLocationServiceObjArray = [[NSMutableArray alloc]init];
            for(int i=0;i < resSsRet.RetailerDetail.count;i++)
            {
                NSDictionary* dictList = resSsRet.RetailerDetail[i];
                
                
                RetailersInDetail *findLocnServiceObj = [[RetailersInDetail alloc] init];
                
                [findLocnServiceObj setValuesForKeysWithDictionary:dictList];
                
                [findLocationServiceObjArray addObject:findLocnServiceObj];
                [retailerIDArray addObject:findLocnServiceObj.retailerId];
                
                [retailLocationIDArray addObject:findLocnServiceObj.retailLocationId];
                
            }
        }
        if(bottomBtn == 1)
        {
            arrFindSingleBottomButtonDO=[[NSMutableArray alloc]init];
            for (int i=0; i < resSsRet.bottomBtnList.count; i++) {
                bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                NSDictionary* dictList = resSsRet.bottomBtnList[i];
                
                [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                
                if(obj_findBottomDO.bottomBtnName.length == 0)
                {
                    obj_findBottomDO.bottomBtnName = @" ";
                }
                if(obj_findBottomDO.bottomBtnImg.length == 0)
                {
                    obj_findBottomDO.bottomBtnImg = @" ";
                }
                if(obj_findBottomDO.bottomBtnImgOff.length == 0)
                {
                    obj_findBottomDO.bottomBtnImgOff = @" ";
                }
                if(obj_findBottomDO.btnLinkTypeName)
                {
                    if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"])
                    {//for filters
                        [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                    }
                }
                
                [arrFindSingleBottomButtonDO addObject:obj_findBottomDO];
                
            }
            if([arrFindSingleBottomButtonDO count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                // [self setBottomBarMenu];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self setBottomBarMenu];
                });
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self settableViewOnScreen];
        });
      
    }
    else if([resSsRet.responseCode isEqualToString:@"10005"])
    {
        if(resSsRet.mainMenuId)
        {
            [defaults setValue:resSsRet.mainMenuId forKey:KEY_MAINMENUID];
        }
        if(resSsRet.nextPage)
        {
            nextPage = [resSsRet.nextPage integerValue];
        }
        
        if(bottomBtn == 1)
        {
            arrFindSingleBottomButtonDO=[[NSMutableArray alloc]init];
            for (int i=0; i < resSsRet.bottomBtnList.count; i++) {
                bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                NSDictionary* dictList = resSsRet.bottomBtnList[i];
                
                [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                
                if(obj_findBottomDO.bottomBtnName.length == 0)
                {
                    obj_findBottomDO.bottomBtnName = @" ";
                }
                if(obj_findBottomDO.bottomBtnImg.length == 0)
                {
                    obj_findBottomDO.bottomBtnImg = @" ";
                }
                if(obj_findBottomDO.bottomBtnImgOff.length == 0)
                {
                    obj_findBottomDO.bottomBtnImgOff = @" ";
                }
                if(obj_findBottomDO.btnLinkTypeName)
                {
                    if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"])
                    {//for filters
                        [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                    }
                }
                
                [arrFindSingleBottomButtonDO addObject:obj_findBottomDO];
                
            }
            if([arrFindSingleBottomButtonDO count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                [self setBottomBarMenu];
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            
        }
        NSString *responseTextStr;
        if (resSsRet.responseText != nil) {
            responseTextStr = resSsRet.responseText;
        }
        else
            responseTextStr = @"No Records Found";
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
            [self settableViewOnScreen];
        });
      
        
    }
    else {//show the error message in alert
        nextPage=0;
        
        if (resSsRet.responseText != nil) {
            [UtilityManager showAlert:nil msg:resSsRet.responseText];
            [loading stopAnimating];
           
        }
        else
        {
            [UtilityManager showAlert:nil msg:[NSString stringWithFormat:@"No Records found"]];
            [loading stopAnimating];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
                       [self settableViewOnScreen];
        });
       
    }
    
    DLog(@"%d",sortFilObj.distanceSelected);
    dispatch_async(dispatch_get_main_queue(), ^{
        if(![defaults boolForKey:@"ViewMore"]){
            [locServiceDealsTable setContentOffset:CGPointZero];
        }
        [locServiceDealsTable reloadData];
        [defaults setBool:NO forKey:@"ViewMore"];
       
    });
    
    
    [self sendClicksNImpressions];
   
}

//Clicks and Impressions
-(void)sendClicksNImpressions
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    NSMutableString *retailerIDString = [[NSMutableString alloc]init];
    NSMutableString *retailLocationIDString = [[NSMutableString alloc]init];
    
    if([defaults valueForKey:KEY_MAINMENUID])
    {
        //[parameters setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
        [parameters setValue:@"138" forKey:@"mainMenuId"];
    }
    if (![typeStr isEqualToString:@""])
    {
        [parameters setValue:typeStr forKey:@"catName"];
    }
    if(retailerIDArray.count > 0)
    {
        for(int i=0;i<retailerIDArray.count;i++)
        {
            [retailerIDString appendString:[NSString stringWithFormat:@"%@,",retailerIDArray[i]]];
        }
        if ([retailerIDString hasSuffix:@","]) {
            [retailerIDString  setString:[retailerIDString substringToIndex:[retailerIDString length]-1]];
        }
        [parameters setValue:retailerIDString forKey:@"retailerId"];
        
    }
    if(retailLocationIDArray.count > 0)
    {
        for(int i=0;i<retailLocationIDArray.count;i++)
        {
            [retailLocationIDString appendString:[NSString stringWithFormat:@"%@,",retailLocationIDArray[i]]];
        }
        if ([retailLocationIDString hasSuffix:@","]) {
            [retailLocationIDString  setString:[retailLocationIDString substringToIndex:[retailLocationIDString length]-1]];
        }
        [parameters setValue:retailLocationIDString forKey:@"retailLocationId"];
    }
    DLog(@"parameter: %@",parameters);
    
    //
    NSString *urlString = [NSString stringWithFormat:@"%@hotdeals/retclickimpression",BASE_URL];
    //    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    //    [manager.requestSerializer setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    //    [manager POST:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
    //
    //        NSLog(@"response: %@",responseObject);
    //     //   [self parse_getSideNav:responseObject];
    //
    //    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    //
    //        NSLog(@"Error: %@", error);
    //    }];
    //
    //
    
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    DLog(@"Url: %@",urlString);
    [client sendRequestAsync : parameters : urlString];
    
}



-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            self.cityExp = cevc;
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            self.filters = pvc;
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}

/*
 *
 Parsing logic when response received for getting user details.
 Method called when navigated from Settings screen.
 *
 */

//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}

//-(void) navigateToScanNow
//{
//    [defaults setBool:YES forKey:@"KEY_SCANSPLASH"];
//    [defaults setValue:@"scanNow" forKey:KEY_SEARCHFROM];
//    [defaults setObject:@"YES" forKey:@"KEY_SCANNERCALLED"];
//
//    int menulevel = [[defaults valueForKey:KEY_MENULEVEL]integerValue];
//    if(menulevel > 1)
//        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
//    else
//        [defaults  setBool:NO forKey:@"DisplayBackBarButton"];
//
//
//    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
//    ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];
//    [self.navigationController pushViewController:viewScanNowScreen animated:NO];
//    [viewScanNowScreen release];
//
//}



-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}

-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}

//Call Find Service and Class
-(void)navigateToFindView
{
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    navigatedFromMainMenu=false;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}

-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    self.iEventsListViewController = iEventsListViewController;
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}


-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}




-(void)releaseAllocatedObjects
{
    NSArray *arrSubViews = [self.view subviews];
    for(int i=0; i<[arrSubViews count]; i++)
    {
        UIView *releaseObjView = (UIView*)[arrSubViews objectAtIndex:i];
        if(releaseObjView.tag == 99)
            [releaseObjView removeFromSuperview];
        if([releaseObjView isKindOfClass:[bottomButtonView class]])
        {
            [releaseObjView removeFromSuperview];
        }
    }
    
    if([arrFindSingleBottomButtonDO count]>0)
    {
        [arrFindSingleBottomButtonDO removeAllObjects];
        //[arrFindSingleBottomButtonDO release];
        arrFindSingleBottomButtonDO = nil;
    }
    if([arrBottomButtonViewContainer count]>0)
    {
        [arrBottomButtonViewContainer removeAllObjects];
        // [arrBottomButtonViewContainer release];
        arrBottomButtonViewContainer = nil;
    }
    
}




#pragma mark tableview delegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float width = (IPAD ? 585.0f:200.0f);
    
    float noOflines = [self getLabelSize: [self retailerAddress:indexPath] withSize:width withFont:addressFont];
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad){
        return 85.0;
    }
    else
    {
        if(noOflines<2)
            return 50.0;
        else
            return 65.0;
   }
    
}


-(NSString *) retailerAddress:(NSIndexPath *)indexPath{
        NSString *address = @"";
        if([findLocationServiceObjArray count]){
            if (indexPath.row != [findLocationServiceObjArray count]){
                if((![[[findLocationServiceObjArray objectAtIndex:indexPath.row]retaileraddress2] isEqualToString:@"N/A"])&& [[findLocationServiceObjArray objectAtIndex:indexPath.row]retaileraddress2].length>0){
                    address =  [NSString stringWithFormat:@"%@, %@",
                                [[findLocationServiceObjArray objectAtIndex:indexPath.row]retaileraddress1],[[findLocationServiceObjArray objectAtIndex:indexPath.row]retaileraddress2]];
                }
                else{
                    address = [NSString stringWithFormat:@"%@",
                               [[findLocationServiceObjArray objectAtIndex:indexPath.row]retaileraddress1]];
                }
            }
        }
        return address;
}

-(CGFloat) getLabelSize : (NSString*) labelString withSize:(CGFloat) labelwidth withFont:(UIFont *) font
{
    
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    CGFloat noOflines = ceil(rect.size.height / font.lineHeight);
    
    
    return noOflines;
    
}
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    if(nextPage && [findLocationServiceObjArray count])
        return [findLocationServiceObjArray count]+1;
    
    else
        return [findLocationServiceObjArray count];
    
    //    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (sortFilObj.distanceSelected==YES || (sortFilObj.alphabeticallySelected==YES && [findLocationServiceObjArray count]))//((![[defaults valueForKey:@"SortFindBy"]isEqualToString:@"Distance"] && !([[defaults valueForKey:@"SubCategoryId"]isEqualToString:@"0"]) && isSubCat==TRUE) || ([defaults valueForKey:@"GroupFindBy"]))
    {
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
            return 30.0;
        else
            return 22.0;
    }
    else
        return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    float labelFont = 15.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        labelFont = 18.0;
    }
    
    
    UILabel *headerName = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 315, 26)];
    headerName.backgroundColor = [UIColor lightGrayColor];
    headerName.font = [UIFont boldSystemFontOfSize:labelFont];
   
   if([segmentFindController selectedSegmentIndex]==0){
        headerName.text = [NSString stringWithFormat:@" Sorted by Distance"];
        return headerName ;
    }
   else if([segmentFindController selectedSegmentIndex]==1) {
        headerName.text = [NSString stringWithFormat:@" Sorted by Name"];
        return headerName;
    }
  
    else{
        return NULL;
    }
}


- (void)reloadSections:(NSIndexSet *)sections withRowAnimation:(UITableViewRowAnimation)animation{
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    SdImageView *asyncImageView = nil;
    label = nil;
    detailLabel = nil ;
    addressLabel = nil;
    mileLabel = nil;
    specImage = nil;
    hourFilterLabel = nil;
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //if (cell == nil)
    // {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] ;
    // }
    
   // float viewMoreLabelFont = 16.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
       // viewMoreLabelFont = 20.0;
    }
    
    
    if([findLocationServiceObjArray count]){
    
    if (indexPath.row != [findLocationServiceObjArray count]) {
        
        CGRect frame;
        if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
            frame.origin.x = 5;
            frame.origin.y = 5;
            frame.size.width = 40;
            frame.size.height = 40;
        }
        else
        {
            frame.origin.x = 5;
            frame.origin.y = 5;
            frame.size.width = 60;
            frame.size.height = 60;
        }
        
        
        asyncImageView = [[SdImageView alloc] initWithFrame:frame] ;
        
        asyncImageView.tag = ASYNC_IMAGE_TAG;
        // asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        asyncImageView.backgroundColor = [UIColor clearColor];
        asyncImageView.layer.masksToBounds = YES;
        
        [cell.contentView addSubview:asyncImageView];
        
        label = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            label.font = [UIFont boldSystemFontOfSize:19];
        else
            label.font = [UIFont boldSystemFontOfSize:14];
        label.textColor = [UIColor colorWithRGBInt:0x112e72];
        [cell.contentView addSubview:label];
        
        detailLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            detailLabel.font = [UIFont boldSystemFontOfSize:15];
        else
            detailLabel.font = [UIFont boldSystemFontOfSize:10];
        detailLabel.textColor = [UIColor darkGrayColor];
        detailLabel.numberOfLines = 1;
        detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        detailLabel.preferredMaxLayoutWidth = 150.0;
        [cell.contentView addSubview:detailLabel];
        
        addressLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
            addressLabel.font = [UIFont boldSystemFontOfSize:15];
            addressLabel.preferredMaxLayoutWidth = 250.0;
            addressLabel.numberOfLines = 1;
        }
        else
        {
            addressLabel.font = [UIFont boldSystemFontOfSize:10];
            addressLabel.preferredMaxLayoutWidth = 170.0;
            addressLabel.numberOfLines = 2;
        }
        addressLabel.textColor = [UIColor darkGrayColor];
        
        addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        addressLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        [cell.contentView addSubview:addressLabel];
        
        
        mileLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            mileLabel.font = [UIFont boldSystemFontOfSize:15];
        else
            mileLabel.font = [UIFont boldSystemFontOfSize:10];
        mileLabel.textColor = [UIColor darkGrayColor];
        mileLabel.numberOfLines = 1;
        [cell.contentView addSubview:mileLabel];
        
        hourFilterLabel = [[UILabel alloc] init] ;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            hourFilterLabel.font = [UIFont boldSystemFontOfSize:15];
        else
            hourFilterLabel.font = [UIFont boldSystemFontOfSize:10];
        hourFilterLabel.textColor = [UIColor darkGrayColor];
        hourFilterLabel.numberOfLines = 1;
        [cell.contentView addSubview:hourFilterLabel];

        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (indexPath.row<[findLocationServiceObjArray count]){
            
            NSString *logoImageStr = [[findLocationServiceObjArray objectAtIndex:indexPath.row]logoImagePath];
            if(![logoImageStr isEqualToString:@"N/A"] )
            {
                [asyncImageView loadImage:logoImageStr];
            }
            RetailersInDetail *distanceDic=[findLocationServiceObjArray objectAtIndex:indexPath.row];
            NSLog(@"%@",distanceDic);
            label.text = [NSString stringWithFormat:@"%@", [[findLocationServiceObjArray objectAtIndex:indexPath.row]retailerName]];
            
            //distance in miles
            mileLabel.text =[NSString stringWithFormat:@"%@", [(RetailersInDetail*)[findLocationServiceObjArray objectAtIndex:indexPath.row]distance]];
           
            //address1, address2 label
            
            addressLabel.text =[self retailerAddress:indexPath];
            
            //extracting city,state,zip from complete address
           
            detailLabel.text = [NSString stringWithFormat:@"%@,%@,%@",
                                [[findLocationServiceObjArray objectAtIndex:indexPath.row]city],[(RetailersInDetail*)[findLocationServiceObjArray objectAtIndex:indexPath.row]state], [(RetailersInDetail*)[findLocationServiceObjArray objectAtIndex:indexPath.row]postalCode]];
           
            if(![distanceDic.locationOpen isEqualToString:@"N/A"] )
           hourFilterLabel.text =[NSString stringWithFormat:@"%@",distanceDic.locationOpen];
            else
                hourFilterLabel.text = @"";
           float width = (IPAD ? 585.0f:200.0f);
            if ([[[findLocationServiceObjArray objectAtIndex:indexPath.row]saleFlag]intValue] == 1) {
                
                specImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"retSpecialsIcon_o.png"]];
                [cell.contentView addSubview:specImage];
                // [specImage release];
            }
            
               numberOfLines =[ self getLabelSize:addressLabel.text withSize:width withFont:addressFont];
        }
        //return cell;
    }
    
    //view more results...
    else if (indexPath.row == [findLocationServiceObjArray count] && nextPage == 1) {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        CGRect frame;
        
        frame.origin.x = SCREEN_WIDTH/2;
        frame.origin.y = 5;
        frame.size.width = 40;
        frame.size.height = 40;
        
        if(DEVICE_TYPE == UIUserInterfaceIdiomPad)
            
            loading.frame = frame;
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
        
       
    }
    
    asyncImageView.translatesAutoresizingMaskIntoConstraints = NO;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    detailLabel.translatesAutoresizingMaskIntoConstraints = NO;
    specImage.translatesAutoresizingMaskIntoConstraints = NO;
    addressLabel.translatesAutoresizingMaskIntoConstraints = NO;
    mileLabel.translatesAutoresizingMaskIntoConstraints = NO;
    hourFilterLabel.translatesAutoresizingMaskIntoConstraints = NO;
    if(specImage == nil && asyncImageView == nil && detailLabel == nil && label != nil)
        tableViewDictionary = NSDictionaryOfVariableBindings(label);
    else if(specImage == nil && detailLabel != nil)
        tableViewDictionary = NSDictionaryOfVariableBindings(asyncImageView,label,detailLabel,addressLabel,mileLabel,hourFilterLabel);
    else if(specImage != nil)
        tableViewDictionary = NSDictionaryOfVariableBindings(asyncImageView,label,detailLabel,specImage,addressLabel,mileLabel,hourFilterLabel);
    else
        tableViewDictionary = nil;
    [self setConstraints:cell tag:1];
    }
    return cell;
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == [findLocationServiceObjArray count] && nextPage == 1) {
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"]&& lastVisitedRecord !=0) {
            s_searchBar.userInteractionEnabled = NO;
            segmentFindController.userInteractionEnabled = NO;
            [defaults setBool:YES forKey:@"ViewMore"];
            NSLog(@"viewmore:%d",(int)[defaults boolForKey:@"ViewMore"]);
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self fetchNextScanSeeRetailers];// More records to be shown...pagination
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HubCitiAppDelegate removeActivityIndicator];
                    [loading stopAnimating];
                    //[locServiceDealsTable reloadData];
                    //  [self setBottomBarMenu];
                    s_searchBar.userInteractionEnabled = YES;
                    segmentFindController.userInteractionEnabled = YES;
                    if(iSwipeViewController){
                        for (UIView *views in self.view.subviews)
                            views.userInteractionEnabled=NO;
                        iSwipeViewController.view.userInteractionEnabled = YES;
                    }

                });
            });
            
        }
    }
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [defaults setBool:NO forKey:@"ViewMore"];
   
    
    [defaults setObject:[[findLocationServiceObjArray objectAtIndex:indexPath.row]retListId] forKey:KEY_RLISTID];
    
    [defaults  setObject:[[findLocationServiceObjArray objectAtIndex:indexPath.row]ribbonAdImagePath]
                  forKey:@"ribbonAdImagePath"];
    
    [self request_retsummary:(int)indexPath.row withSection:0];
    
    [locServiceDealsTable deselectRowAtIndexPath:[locServiceDealsTable indexPathForSelectedRow] animated:YES];
}

-(void)setConstraints:(UITableViewCell*)cell tag:(CGFloat)tag
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    if(tag == 0)
    {
        viewDictionary = NSDictionaryOfVariableBindings(locServiceDealsTable);
        int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            if(bottomBtn == 1)
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(88)-[locServiceDealsTable(%f)]",SCREEN_HEIGHT-yVal-bottomBarButtonHeight-44-48] options:0 metrics:0 views:viewDictionary]];//840
            else
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(88)-[locServiceDealsTable(%f)]",SCREEN_HEIGHT-yVal-44-48] options:0 metrics:0 views:viewDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[locServiceDealsTable(%f)]",SCREEN_WIDTH] options:0 metrics:0 views:viewDictionary]];
        }
        else
        {
            if(bottomBtn == 1)
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(88)-[locServiceDealsTable(%f)]",SCREEN_HEIGHT-yVal-50-44-48] options:0 metrics:0 views:viewDictionary]];
            else
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(88)-[locServiceDealsTable(%f)]",SCREEN_HEIGHT-yVal-44-48] options:0 metrics:0 views:viewDictionary]];
            
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[locServiceDealsTable(%f)]",SCREEN_WIDTH] options:0 metrics:0 views:viewDictionary]];
        }
    }
    else
    {
        if(tableViewDictionary != nil)
        {
            if (  DEVICE_TYPE==UIUserInterfaceIdiomPad )
            {
                if([tableViewDictionary objectForKey:@"asyncImageView"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[asyncImageView(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                if([tableViewDictionary objectForKey:@"label"] != nil)
                {
                    
                    if(numberOfLines<2)
                        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(22)]-(1)-[addressLabel(30)]-(1)-[detailLabel(15)]"] options:0 metrics:0 views:tableViewDictionary]];
                    else
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(22)]-(2)-[addressLabel(30)]-(1)-[detailLabel(15)]"] options:0 metrics:0 views:tableViewDictionary]];
                    if([tableViewDictionary objectForKey:@"specImage"] == nil)
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[label(400)]"] options:0 metrics:0 views:tableViewDictionary]];
                    else
                        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[label(350)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                if([tableViewDictionary objectForKey:@"detailLabel"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[detailLabel(585)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[addressLabel(585)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                
              
                
                if([tableViewDictionary objectForKey:@"specImage"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[specImage(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[mileLabel(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(600)-[specImage(50)]-10-[mileLabel(70)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                else{
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[mileLabel(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(660)-[mileLabel(70)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(24)-[hourFilterLabel(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(660)-[hourFilterLabel(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                
            }
            else
            {
                if([tableViewDictionary objectForKey:@"asyncImageView"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[asyncImageView(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                if([tableViewDictionary objectForKey:@"label"] != nil)
                {
                    if(numberOfLines<2)
                        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(2)-[label(15)]-(1)-[addressLabel(15)]-(1)-[detailLabel(13)]"] options:0 metrics:0 views:tableViewDictionary]];
                    else
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(2)-[label(15)]-(2)-[addressLabel(25)]-(1)-[detailLabel(13)]"] options:0 metrics:0 views:tableViewDictionary]];
                    if([tableViewDictionary objectForKey:@"specImage"] == nil)
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[label(200)]"] options:0 metrics:0 views:tableViewDictionary]];
                    else
                       [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[label(150)]"] options:0 metrics:0 views:tableViewDictionary]];
                    
                }
                
                if([tableViewDictionary objectForKey:@"detailLabel"] != nil)
                {
                    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(24)-[addressLabel(20)]-30-[detailLabel(50)]-|"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[detailLabel]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[addressLabel]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                if([tableViewDictionary objectForKey:@"specImage"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[specImage(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[mileLabel(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(215)-[specImage(40)]-5-[mileLabel(60)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                else{
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[mileLabel(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(260)-[mileLabel(60)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[hourFilterLabel(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(260)-[hourFilterLabel(40)]"] options:0 metrics:0 views:tableViewDictionary]];
               
            }
        }
    }
}

#pragma mark pagination methods
-(void)fetchNextScanSeeRetailers
{
    if (nextPage == 1)
    {
        
        if ([keyWord length]) // Ret Search
        {
            [self request_ssretsearch:keyWord withObj:sortFilObj];
        }
        
        else if ([typeStr length]) // Cat Search
        {
            [self request_sscatsearch:catRetSearchKey withObj:sortFilObj];
        }
        
    }
    else
    {
        return;
    }
}

#pragma mark search bar delegate methods
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    //animate = 0;
    if (searchBar.tag == 0){
        //        viewFrame = g_view.frame;
        [UIView beginAnimations:@"slideIn" context:NULL];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        [UIView setAnimationDuration:0.3];
        //        g_view.center = CGPointMake(160, 20);
        [UIView commitAnimations];
    }
    
   
    
   
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    //searchBar.text = [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    //    if ([s_searchBar.text length]>0) {
    //       keyWord = s_searchBar.text;
    //    }
    [searchBar resignFirstResponder];
    switch (searchBar.tag) {
        case 0:{//google search
            
            if (![searchBar.text length]) {
                
                return;
            }
            DLog(@"Text:%@", searchBar.text);
            
            [defaults setValue:searchBar.text forKey:@"Google Search"];
            
            NSString *str = [NSString stringWithFormat:@"http://www.google.com/search?q=%@",searchBar.text/*[defaults valueForKey:@"Google Search"]*/];
            
            NSString *query = [str stringByReplacingOccurrencesOfString:@" " withString:@"+"];
            // NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.google.com/search?q=%@",query]];
            // str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [g_searchBar resignFirstResponder];
            [UIView beginAnimations:@"slideOut" context:NULL];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:0.3];
            //            g_view.frame = viewFrame;
            [UIView commitAnimations];
            
            [defaults setValue:query forKey:KEY_URL];
            WebBrowserViewController *googleSearch = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:googleSearch animated:NO];
            //[googleSearch release];
        }
            break;
            
        case 1://scansee search
            searchBar.text = [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (![searchBar.text length]) {
                
                return;
            }
            
            tappedSearch=TRUE;
            lastVisitedRecord = 0;
            [globalRetailerService removeAllObjects];
            [findLocationServiceObjArray removeAllObjects];
            [arrFindCategoryInfo removeAllObjects];
            
            
            if ([typeStr length]) // Cat Search
            {   type=[typeStr copy];
                catRetSearchKey = [s_searchBar.text copy];
                inCategorySort=TRUE;
                [self request_sscatsearch:catRetSearchKey withObj:sortFilObj];
                ReleaseAndNilify(locServiceDealsTable);
            }
            
            
            else if ([s_searchBar.text length]) // Ret Search
            {
                keyWord = [s_searchBar.text copy];
                [self request_ssretsearch:keyWord withObj:sortFilObj];
            }
            // [self request_sscatsearch:s_searchBar.text];
            
            break;
        default:
            break;
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    switch (searchBar.tag) {
        case 0://google search
            //searchBar.text = nil;
            [searchBar resignFirstResponder];
            [UIView beginAnimations:@"slideOut" context:NULL];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:0.3];
            //            g_view.frame = viewFrame;
            [UIView commitAnimations];
            
            break;
            
        case 1://scansee search
            //searchBar.text = nil;
            [searchBar resignFirstResponder];
            [UIView beginAnimations:@"slideOut" context:NULL];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:0.3];
            [UIView commitAnimations];
            break;
        default:
            
            break;
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if (![searchText length] && searchBar.tag == 1){
        [searchBar performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0.1];
        lastVisitedRecord = 0;
        //remove objects of array and reload tableview
        [findLocationServiceObjArray removeAllObjects];
        [arrFindCategoryInfo removeAllObjects];
        [locServiceDealsTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        
       //when search bar text is cleared
        
            if ([typeStr length]) // Cat Search
            {   //type=[typeStr copy];
                catRetSearchKey = nil;
                inCategorySort=TRUE;
                sortFilObj = [[SortAndFilter alloc] init];
                if(segmentFindController.selectedSegmentIndex == 0){
                    sortFilObj.distanceSelected = TRUE;
                    sortFilObj.alphabeticallySelected = FALSE;
                }
                else if(segmentFindController.selectedSegmentIndex == 1){
                    sortFilObj.distanceSelected = FALSE;
                    sortFilObj.alphabeticallySelected = TRUE;
                }
                [searchBar resignFirstResponder];
                [self request_sscatsearch:nil withObj:nil];
                
            }
            
            
            else  // Ret Search
            {
                keyWord = retailerStr;
                sortFilObj = [[SortAndFilter alloc] init];
                if(segmentFindController.selectedSegmentIndex == 0){
                    sortFilObj.distanceSelected = TRUE;
                    sortFilObj.alphabeticallySelected = FALSE;
                }
                else if(segmentFindController.selectedSegmentIndex == 1){
                    sortFilObj.distanceSelected = FALSE;
                    sortFilObj.alphabeticallySelected = TRUE;
                }
                [searchBar resignFirstResponder];
                [self request_ssretsearch:keyWord withObj:nil];
                //
            }
            
            
        

        
    }
    
}
// tool bar button connection

-(IBAction) aboutPressed : (id) sender {
    
    AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
    privacy.responseXml = [NSMutableString stringWithFormat:@"%@About%%20ScanSee.html", BASE_URL];
    [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
    privacy.comingFromPrivacyScreen = NO;
    [self.navigationController pushViewController:privacy animated:NO];
    //  [privacy release];
    
}
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//-(void)requestToGetUserData
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
//    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//    ReleaseAndNilify(xmlStr);
//}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
        [HubCitiAppDelegate setIsLogistics:YES];
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}

@end

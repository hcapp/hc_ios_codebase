//
//  GroupedProductList.h
//  Scansee
//
//  Created by ajit on 9/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"

@class SearchResultCount;

@interface GroupedProductList : UIViewController<UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource,CustomizedNavControllerDelegate> {
    
	IBOutlet UITableView *galleryTV;
    //NSString *responseXml;
	NSMutableArray *prodNameArray, *prodIdArray ;
	NSMutableArray *prodImgPathArray, *prodDescArray;
	NSMutableArray *rowNumArray;
    int nextPage,lastVisitedRecord;
    NSString *latValue,*longValue;
    NSIndexPath *index;
	
}
-(void)parseDisplayProdList:(NSString*)responseXml;


@end


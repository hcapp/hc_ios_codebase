//
//  FindOptionsViewController.m
//  HubCiti
//
//  Created by Keshava on 6/23/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import "FindOptionsViewController.h"
#import "FindLocationServiceViewController.h"
#import "FilterRetailersList.h"
#import "FindSortViewController.h"
#import "FindOptionsSortViewController.h"
#import "RetailersListViewController.h"
#import "RegionFindGSViewController.h"
#import "SingleCatGroupAndSort.h"
#import "SingleCatRetailers.h"
#import "SwipesViewController.h"

NSDictionary *viewDictionary;
float rowHeightForOptions = 44;

@interface FindOptionsViewController ()

@end

@implementation FindOptionsViewController
@synthesize typeStr,searchKey,catId,srtFilterObj,filterId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setAccessibilityLabel:@"optionsView"];
    // Do any additional setup after loading the view from its nib.
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //customize back button
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    options = [[NSArray alloc]initWithObjects:@"Show Map",@"Sort",nil];
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        rowHeightForOptions = 60;
   tableOptions = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, ([options count]*rowHeightForOptions)) style:UITableViewStylePlain];
    tableOptions.dataSource=self;
    tableOptions.delegate=self;
    [tableOptions setAccessibilityIdentifier:@"optionsTable"];
    [tableOptions setBackgroundColor:[UIColor whiteColor]];
    self.title=@"Map/Sort";
    [self.view addSubview:tableOptions];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)popBackToPreviousPage{
    
    [self.navigationController popViewControllerAnimated:NO];
    showMapFlag=FALSE;
    
}

-(void)showMap
{
    showMapFlag=TRUE;
    if (fromFind==TRUE || fromRegionFind==TRUE || fromFindSingleCategory == TRUE) {
        FindLocationServiceViewController *iShowMapController =[[FindLocationServiceViewController alloc]initWithNibName:@"FindLocationServiceViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:iShowMapController animated:NO];
        //[iShowMapController release];
    }
    else if (fromFilters==TRUE)
    {
        FilterRetailersList *iFindRetailersList = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
        self.filters = iFindRetailersList;
        [self.navigationController pushViewController:iFindRetailersList animated:NO];
        //[iFindRetailersList release];
    }
    else if (fromNearBy==TRUE)
    {
        RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
        self.retailerListScreen = retailerListScreen;
        [self.navigationController pushViewController:retailerListScreen animated:NO];
        //[retailerListScreen release];
    }
   
}



#pragma mark tableView delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [options count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return rowHeightForOptions;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"CellOptionsListView";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.backgroundColor =  [UIColor clearColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    UILabel *lblOption = [[UILabel alloc]init];
    [lblOption setText:[options objectAtIndex:indexPath.row]];
    [lblOption setTextColor:[UIColor blackColor]];
    [lblOption setBackgroundColor:[UIColor clearColor]];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        [lblOption setFont:[UIFont boldSystemFontOfSize:21]];
    else
        [lblOption setFont:[UIFont boldSystemFontOfSize:16]];
    lblOption.textColor = [UIColor colorWithRGBInt:0x112e72];
    [cell.contentView addSubview:lblOption];
   // [lblOption release];
    lblOption.translatesAutoresizingMaskIntoConstraints = NO;
    viewDictionary = NSDictionaryOfVariableBindings(lblOption);
    [self setConstraints:cell];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int i = (int)indexPath.row;
    
    switch (i) {
        case 0:
            if ((fromFind==TRUE || fromRegionFind == TRUE || fromFindSingleCategory==TRUE) && [globalRetailerService count]>0) {
                
                [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
                [self showMap];
            }
            else if (fromFilters==TRUE)
            {
                [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
                if ([globalLatArray  count] > 0) {
                
                
                     [self showMap];
                }
                else{
                    [UtilityManager showAlert:nil msg:NSLocalizedString(@"Retailers are not available to plot on map",@"Retailers are not available to plot on map")];
                    }

                
            }
            else if(fromNearBy==TRUE)
            {
                [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
                if ([globalRetailerNameArray  count] > 0) {
                [self showMap];
                }
            }
            else{
                [UtilityManager showAlert:nil msg:NSLocalizedString(@"Retailers are not available to plot on map",@"Retailers are not available to plot on map")];
                }
            break;
        case 1:
            if (fromFilters==TRUE) {
                [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
//                FindOptionsSortViewController *findSort=[[FindOptionsSortViewController alloc]initWithNibName:@"FindOptionsSortViewController" bundle:Nil];
//                //[defaults setInteger:100 forKey:@"CityExpTableTag"];
//                [self.navigationController pushViewController:findSort animated:YES];
                showMapFlag = FALSE;
               /// if(iSwipeViewController)
                 //   [iSwipeViewController release];
                
                iSwipeViewController = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                NSArray *viewControllers = [self.navigationController viewControllers];
                long vIndex=[viewControllers count]-2;
                iSwipeViewController.delegate = [self.navigationController.viewControllers objectAtIndex:vIndex];
                iSwipeViewController.module = @"Filter";//Find All , Find Single , Events, CitiEXP
                iSwipeViewController.filterID =  [SharedManager retAffId];
      //          iSwipeViewController.categoryName = typeStr;
      //          iSwipeViewController.catId = catId;
                iSwipeViewController.sortFilObj = [[SortAndFilter alloc]initWithObject:srtFilterObj];
        //        iSwipeViewController.srchKey =searchKey;
                
                [self.navigationController pushViewController:iSwipeViewController animated:NO];
                

            }
            else if (fromNearBy==TRUE)
            {
                [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
                FindOptionsSortViewController *findSort=[[FindOptionsSortViewController alloc]initWithNibName:@"FindOptionsSortViewController" bundle:Nil];
                //[defaults setInteger:100 forKey:@"CityExpTableTag"];
                [self.navigationController pushViewController:findSort animated:YES];
            }
            else if (fromFind==TRUE)
            {
                [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];

                [defaults setInteger:100 forKey:@"CityExpTableTag"];

                
              //  if(iSwipeViewController)
                   // [iSwipeViewController release];
                
                iSwipeViewController = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                iSwipeViewController.module = @"Find All";//Find All , Find Single , Events, CitiEXP
                NSArray *viewControllers = [self.navigationController viewControllers];
                long vIndex=[viewControllers count]-2;
                iSwipeViewController.delegate = [self.navigationController.viewControllers objectAtIndex:vIndex];
                iSwipeViewController.categoryName = typeStr;
                iSwipeViewController.catId = catId;
                iSwipeViewController.sortFilObj = [[SortAndFilter alloc]initWithObject:srtFilterObj];;
                iSwipeViewController.srchKey =searchKey;
                
                [self.navigationController pushViewController:iSwipeViewController animated:NO];
                
            }
            else if (fromRegionFind==TRUE)
            {
                [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
                //if(iSwipeViewController)
                  //  [iSwipeViewController release];
                
                iSwipeViewController = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                iSwipeViewController.module = @"Find All";//Find All , Find Single , Events, CitiEXP
                NSArray *viewControllers = [self.navigationController viewControllers];
                long vIndex=[viewControllers count]-2;
                iSwipeViewController.delegate = [self.navigationController.viewControllers objectAtIndex:vIndex];
                iSwipeViewController.categoryName = typeStr;
                iSwipeViewController.catId = catId;
                iSwipeViewController.sortFilObj = [[SortAndFilter alloc]initWithObject:srtFilterObj];;
                iSwipeViewController.srchKey =searchKey;
                
                [self.navigationController pushViewController:iSwipeViewController animated:NO];
                
            }
            else if (fromFindSingleCategory == TRUE)
            {


                  //  if(iSwipeViewController)
                      //  [iSwipeViewController release];
                    
                    iSwipeViewController = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                NSArray *viewControllers = [self.navigationController viewControllers];
                long vIndex=[viewControllers count]-2;
                iSwipeViewController.delegate = [self.navigationController.viewControllers objectAtIndex:vIndex];
                    iSwipeViewController.module = @"Find Single";//Find All , Find Single , Events, CitiEXP
                    iSwipeViewController.categoryName = typeStr;
                    iSwipeViewController.catId = catId;
                    iSwipeViewController.sortFilObj = [[SortAndFilter alloc]initWithObject:srtFilterObj];
                    iSwipeViewController.srchKey =searchKey;
                        
                    [self.navigationController pushViewController:iSwipeViewController animated:NO];
                
                
                    
                }
            break;
            
        default:
            break;
    }
    //[tableOptions deselectRowAtIndexPath:[tableOptions indexPathForSelectedRow]  animated:YES];
}

-(void)setConstraints:(UITableViewCell*)cell
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    if (  DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        // Vertical constrains
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(18)-[lblOption(20)]"] options:0 metrics:0 views:viewDictionary]];
        
        // Horizontal constraints
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(12)-[lblOption(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
    }
    else
    {
        // Vertical constrains
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(12)-[lblOption(20)]"] options:0 metrics:0 views:viewDictionary]];
        
        // Horizontal constraints
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[lblOption(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
    }
}

@end

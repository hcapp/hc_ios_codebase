//
//  EventsCategoryDO.h
//  HubCiti
//
//  Created by ionnor on 11/22/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventsCategoryDO : NSObject
{
    NSString *categoryId;
    NSString *categoryName;
    NSString *categoryImgPath;
    NSMutableArray *catObjArray;
}
@property (nonatomic, strong)NSMutableArray *catObjArray;
@property (nonatomic, strong)NSString *categoryId;
@property (nonatomic, strong)NSString *categoryName;
@property (nonatomic, strong)NSString *categoryImgPath;
@end

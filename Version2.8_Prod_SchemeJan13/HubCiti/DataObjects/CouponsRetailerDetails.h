//
//  CouponsRetailerDetails.h
//  HubCiti
//
//  Created by Lakshmi H R on 12/27/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//


//"couponId": 73989,
//"couponName": "3 ",
//"couponImagePath": "http://66.228.143.27:8080/Images/hubciti/2070/4_91056.png",
//"bannerTitle": "bannertitlesample",
//"counts": 2,
//"retId": 1108571,
//"retName": "3 White Doves",
//"distance": "0.4 mi",
//"rowNum": 5,
//"couponListId": "42544"


//"couponId": 73964,
//"couponName": "122",
//"couponImagePath": "http://66.228.143.27:8080/Images/hubciti/2070/4_91056.png",
//"bannerTitle": "bannertitlesample",
//"counts": 1,
//"retId": 1126670,
//"retName": "spanqa.r2",
//"label": "Saved",
//"distance": "0.5 mi",
//"rowNum": 2,
//"couponListId": "65002"

@protocol CouponsRetailerDetails;
#import <Foundation/Foundation.h>

@interface CouponsRetailerDetails : NSObject

@property (nonatomic,strong) NSString *couponId;
@property (nonatomic,strong) NSString *couponName;
@property (nonatomic,strong) NSString *couponImagePath;
@property (nonatomic,strong) NSString *bannerTitle;
@property (nonatomic,strong) NSString *counts;
@property (nonatomic,strong) NSString *retId;
@property (nonatomic,strong) NSString *retName;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *rowNum;
@property (nonatomic,strong) NSString *couponListId;
@property (nonatomic,strong) NSString *label;


@end

//
//  rethotdealsDO.h
//  HubCiti
//
//  Created by ionnor on 10/30/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface rethotdealsDO : NSObject
{
    NSString *hotDealName;
    NSString *hotDealId;
    NSString *hotDealImagePath;
    NSString *hDPrice;
    NSString *hDSalePrice;
    NSString *apiPartnerId;
    NSString *hdURL;
    NSString *apiPartnerName;
    NSString *hotDealListID;

}
@property (nonatomic, strong) NSString *hotDealName;
@property (nonatomic, strong) NSString *hotDealId;
@property (nonatomic, strong) NSString *hotDealImagePath;
@property (nonatomic, strong) NSString *hDPrice;
@property (nonatomic, strong) NSString *hDSalePrice;
@property (nonatomic, strong) NSString *apiPartnerId;
@property (nonatomic, strong) NSString *hdURL;
@property (nonatomic, strong) NSString *apiPartnerName;
@property (nonatomic, strong) NSString *hotDealListID;

@end

//
//  CouponsResponse.h
//  HubCiti
//
//  Created by Lakshmi H R on 12/27/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//


//"responseCode": "10000",
//"responseText": "Success",
//"bottomBtn": 1,
//"maxCnt": 29,
//"nextPage": 1,
//"maxRowNum": 20,
//"Label": "distance",
//"featuredCouponsList":
//"nonFeaturedCouponsList": [

#import <Foundation/Foundation.h>
#import "bottomButtonDO.h"
#import "CouponsRetailerDetails.h"

@interface CouponsResponse : NSObject

@property (nonatomic,strong) NSString *responseCode;
@property (nonatomic,strong) NSString *responseText;
@property (nonatomic,strong) NSNumber *bottomBtn;
@property (nonatomic,strong) NSNumber *maxCnt;
@property (nonatomic,strong) NSNumber *nextPage;
@property (nonatomic,strong) NSNumber *maxRowNum;
@property (nonatomic,strong) NSString *Label;
@property (nonatomic,strong) NSArray<CouponsRetailerDetails> *featuredCouponsList;
@property (nonatomic,strong) NSArray<CouponsRetailerDetails> *nonFeaturedCouponsList;

@property (strong, nonatomic) NSArray<bottomButtonDO> *bottomBtnList;


@end

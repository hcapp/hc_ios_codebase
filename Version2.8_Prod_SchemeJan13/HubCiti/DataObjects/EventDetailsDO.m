//
//  EventDetailsDO.m
//  HubCiti
//
//  Created by ionnor on 11/22/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "EventDetailsDO.h"

@implementation EventDetailsDO

@synthesize eventId;
@synthesize eventCatId;
@synthesize eventName;
@synthesize shortDes;
@synthesize longDes;
@synthesize hubCitiId;
@synthesize imgPath;
@synthesize busEvent;
@synthesize pkgEvent;
@synthesize startDate;
@synthesize endDate;
@synthesize startTime;
@synthesize endTime;
@synthesize mItemExist;
@synthesize eventListId;
@synthesize distance;
@synthesize isOnGoing;
@synthesize address;
@synthesize latitude;
@synthesize longitude;
@synthesize pkgDes;
@synthesize pkgTicketURL;
@synthesize hotelFlag;
@synthesize moreInfoURL;
@synthesize recurringDays;

@synthesize isAppSiteFlag;
@synthesize address1;
@synthesize address2;
@synthesize location;
@synthesize logisticsURL;

//Event calendar
@synthesize recurrencePattern;
@synthesize isWeekDay;
@synthesize recurrenceInterval;
@synthesize daysOfWeek;
@synthesize dateOfMonth;
@synthesize everyWeekDayMonth;
@synthesize recurrencePatternName;
@synthesize byDayNumber;
@synthesize dayNumber;

@end

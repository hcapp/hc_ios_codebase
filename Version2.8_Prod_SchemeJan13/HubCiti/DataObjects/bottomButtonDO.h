//
//  bottomButtonDO.h
//  HubCiti
//
//  Created by Keerthi on 22/04/14.
//  Copyright (c) 2014 span infotech india ptv. ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol bottomButtonDO;

@interface bottomButtonDO : NSObject
{
    NSString *bottomBtnID;
    NSString *bottomBtnName;
    NSString *bottomBtnImg;
    NSString *bottomBtnImgOff;
    NSString *btnLinkTypeID;
    NSString *btnLinkTypeName;
    NSString *btnLinkID;
    NSString *position;
    NSMutableDictionary *dict_linkTypeName_BottomButton;

}
@property (nonatomic, strong) NSString *bottomBtnID;
@property (nonatomic, strong) NSString *bottomBtnName;
@property (nonatomic, strong) NSString *bottomBtnImg;
@property (nonatomic, strong) NSString *bottomBtnImgOff;
@property (nonatomic, strong) NSString *btnLinkTypeID;
@property (nonatomic, strong) NSString *btnLinkTypeName;
@property (nonatomic, strong) NSString *btnLinkID;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, strong) NSMutableDictionary *dict_linkTypeName_BottomButton;

@end

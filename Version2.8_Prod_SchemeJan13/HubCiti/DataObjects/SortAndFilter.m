//
//  SortAndFilter.m
//  HubCiti
//
//  Created by Bindu M on 5/6/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "SortAndFilter.h"

@implementation SortAndFilter

@synthesize selectedCitiIds, selectedCatIds, selectedOptIds, selectedInterestsIds, selectedOthersIds,distanceSelected,localSpecialSelected,eventDateSelected,sortEventDateSelected, alphabeticallySelected, alphabetSelectedGovQA, typeSelectedGovQA, nameSelectedGovQA,alphabetFindInfoGovQA,bandSelected,venueSelected;

@synthesize evtDate,categoryGovQA,subCategoryGovQA,requestViewFilter;

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.selectedCitiIds forKey:@"selectedCitiIds"];
    [encoder encodeObject:self.selectedCatIds forKey:@"selectedCatIds"];
    [encoder encodeObject:self.selectedOptIds forKey:@"selectedOptIds"];
    [encoder encodeObject:self.selectedInterestsIds forKey:@"selectedInterestsIds"];
    [encoder encodeObject:self.selectedOthersIds forKey:@"selectedOthersIds"];
    [encoder encodeBool:self.distanceSelected forKey:@"distanceSelected"];
    [encoder encodeBool:self.alphabeticallySelected forKey:@"alphabeticallySelected"];
    [encoder encodeBool:self.localSpecialSelected forKey:@"localSpecialSelected"];
    [encoder encodeBool:self.sortEventDateSelected forKey:@"sortEventDateSelected"];
    [encoder encodeBool:self.eventDateSelected forKey:@"eventDateSelected"];
    [encoder encodeObject:self.evtDate forKey:@"evtDate"];
    [encoder encodeBool:self.alphabetSelectedGovQA forKey:@"AlphabetSelectedGovQA"];
    [encoder encodeBool:self.typeSelectedGovQA forKey:@"typeSelectedGovQA"];
    [encoder encodeBool:self.nameSelectedGovQA forKey:@"nameSelectedGovQA"];
    [encoder encodeBool:self.alphabetFindInfoGovQA forKey:@"alphabetFindInfoGovQA"];
    [encoder encodeObject:self.categoryGovQA forKey:@"categoryGovQA"];
    [encoder encodeObject:self.subCategoryGovQA forKey:@"subCategoryGovQA"];
    [encoder encodeObject:self.requestViewFilter forKey:@"requestViewFilter"];
    [encoder encodeBool:self.bandSelected forKey:@"bandSelected"];
    [encoder encodeBool:self.venueSelected forKey:@"venueSelected"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.selectedCitiIds = [decoder decodeObjectForKey:@"selectedCitiIds"];
        self.selectedCatIds = [decoder decodeObjectForKey:@"selectedCatIds"];
        self.selectedOptIds = [decoder decodeObjectForKey:@"selectedOptIds"];
        self.selectedInterestsIds = [decoder decodeObjectForKey:@"selectedInterestsIds"];
        self.selectedOthersIds = [decoder decodeObjectForKey:@"selectedOthersIds"];
        self.distanceSelected = [decoder decodeBoolForKey:@"distanceSelected"];
        self.alphabeticallySelected = [decoder decodeBoolForKey:@"alphabeticallySelected"];
        self.localSpecialSelected = [decoder decodeBoolForKey:@"localSpecialSelected"];
        self.sortEventDateSelected = [decoder decodeBoolForKey:@"sortEventDateSelected"];
        self.eventDateSelected = [decoder decodeBoolForKey:@"eventDateSelected"];
        self.evtDate = [decoder decodeObjectForKey:@"evtDate"];
        self.alphabetSelectedGovQA = [decoder decodeBoolForKey:@"AlphabetSelectedGovQA"];
        self.typeSelectedGovQA = [decoder decodeBoolForKey:@"typeSelectedGovQA"];
        self.nameSelectedGovQA = [decoder decodeBoolForKey:@"nameSelectedGovQA"];
        self.alphabetFindInfoGovQA = [decoder decodeBoolForKey:@"alphabetFindInfoGovQA"];
        self.categoryGovQA = [decoder decodeObjectForKey:@"categoryGovQA"];
        self.subCategoryGovQA = [decoder decodeObjectForKey:@"subCategoryGovQA"];
        self.requestViewFilter = [decoder decodeObjectForKey:@"requestViewFilter"];
        self.bandSelected = [decoder decodeBoolForKey:@"bandSelected"];
        self.venueSelected = [decoder decodeBoolForKey:@"venueSelected"];
      
    }
    return self;
}


-(SortAndFilter *)initWithObject:(SortAndFilter *)SrtAndFilterObj{
    
    SortAndFilter *obj = [[SortAndFilter alloc]init];
    obj.selectedCitiIds = SrtAndFilterObj.selectedCitiIds;
    obj.selectedCatIds = SrtAndFilterObj.selectedCatIds;
    obj.selectedOptIds = SrtAndFilterObj.selectedOptIds;
    obj.selectedInterestsIds = SrtAndFilterObj.selectedInterestsIds;
    obj.selectedOthersIds = SrtAndFilterObj.selectedOthersIds;
    obj.distanceSelected = SrtAndFilterObj.distanceSelected;
    obj.alphabeticallySelected = SrtAndFilterObj.alphabeticallySelected;
    obj.localSpecialSelected = SrtAndFilterObj.localSpecialSelected;
    obj.sortEventDateSelected = SrtAndFilterObj.sortEventDateSelected;
    obj.eventDateSelected = SrtAndFilterObj.eventDateSelected;
    obj.evtDate = SrtAndFilterObj.evtDate;
    obj.alphabetSelectedGovQA = SrtAndFilterObj.alphabetSelectedGovQA;
    obj.typeSelectedGovQA = SrtAndFilterObj.typeSelectedGovQA;
    obj.nameSelectedGovQA = SrtAndFilterObj.nameSelectedGovQA;
    obj.alphabetFindInfoGovQA=SrtAndFilterObj.alphabetFindInfoGovQA;
    obj.categoryGovQA=SrtAndFilterObj.categoryGovQA;
    obj.subCategoryGovQA=SrtAndFilterObj.subCategoryGovQA;
    obj.requestViewFilter=SrtAndFilterObj.requestViewFilter;
    obj.bandSelected = SrtAndFilterObj.bandSelected;
    obj.venueSelected = SrtAndFilterObj.venueSelected;
    return obj;
    
}

@end


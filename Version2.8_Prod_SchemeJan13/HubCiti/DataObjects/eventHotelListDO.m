//
//  eventHotelListDO.m
//  HubCiti
//
//  Created by ionnor on 11/25/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "eventHotelListDO.h"

@implementation eventHotelListDO

@synthesize retailerName;
@synthesize retailLocationId;
@synthesize distance;
@synthesize hotelPrice;
@synthesize hotelrating;
@synthesize imgPath;
@synthesize retailerId;
@synthesize retListId;

@synthesize address;
@synthesize roomCheckUrl;
@synthesize roomBookUrl;
@synthesize packagePrice;
@synthesize description;
@end

//
//  mainMenuDO.h
//  HubCiti
//
//  Created by ionnor on 10/11/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface mainMenuDO : NSObject
{
    NSString *menuId;
    NSString *mItemId;
    NSString *linkTypeId;
    NSString *linkId;
    NSString *position;
    NSString *mItemName;
    NSString *mItemImg;
    NSString *linkTypeName;
    
    NSString *mBtnColor;
    NSString *mBtnFontColor;
    
    NSString *smBtnColor;
    NSString *smBtnFontColor;
    
    NSString *mGrpBkgrdColor;
    NSString *mGrpFntColor;
    
    NSString *smGrpBkgrdColor;
    NSString *smGrpFntColor;
    
    //Added only for Iconic and combo template
    NSString *mFontColor;
    NSString *smFontColor;
    
    NSString *mShapeId;
    NSString *mShapeName;
    NSString *noOfColumns;
}


@property (nonatomic, strong) NSString *noOfColumns;
@property (nonatomic, strong) NSString *mShapeId;
@property (nonatomic, strong) NSString *mShapeName;


@property (nonatomic, strong) NSString *mBtnColor;
@property (nonatomic, strong) NSString *mBtnFontColor;


@property (nonatomic, strong) NSString *smBtnColor;
@property (nonatomic, strong) NSString *smBtnFontColor;


@property (nonatomic, strong) NSString *mGrpBkgrdColor;
@property (nonatomic, strong) NSString *mGrpFntColor;


@property (nonatomic, strong) NSString *smGrpBkgrdColor;
@property (nonatomic, strong) NSString *smGrpFntColor;

@property (nonatomic, strong) NSString *mFontColor;
@property (nonatomic, strong) NSString *smFontColor;

@property (nonatomic, strong) NSString *menuId;
@property (nonatomic, strong) NSString *mItemId;
@property (nonatomic, strong) NSString *linkTypeId;
@property (nonatomic, strong) NSString *linkId;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, strong) NSString *mItemName;
@property (nonatomic, strong) NSString *mItemImg;
@property (nonatomic, strong) NSString *linkTypeName;
@end


@interface mainMenuBottomDO : NSObject
{
    NSString *bottomBtnID;
    NSString *bottomBtnName;
    NSString *bottomBtnImg;
    NSString *bottomBtnImgOff;
    NSString *btnLinkTypeID;
    NSString *btnLinkTypeName;
    NSString *btnLinkID;
    NSString *position;
    NSMutableDictionary *dict_linkTypeName_BottomButton;
}
@property (nonatomic, strong) NSString *bottomBtnID;
@property (nonatomic, strong) NSString *bottomBtnName;
@property (nonatomic, strong) NSString *bottomBtnImg;
@property (nonatomic, strong) NSString *bottomBtnImgOff;
@property (nonatomic, strong) NSString *btnLinkTypeID;
@property (nonatomic, strong) NSString *btnLinkTypeName;
@property (nonatomic, strong) NSString *btnLinkID;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, strong) NSMutableDictionary *dict_linkTypeName_BottomButton;
@end
//
//  SingleCatRetResponse.h
//  HubCiti
//
//  Created by Nikitha on 2/15/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SingleCatRetDetailsResponse.h"
#import "bottomButtonDO.h"

@interface SingleCatRetResponse : NSObject
@property(nonatomic,strong) NSNumber * bottomBtn;
@property(nonatomic,strong) NSString * responseCode;
@property(nonatomic,strong) NSString * responseText;
@property(nonatomic,strong) NSNumber * maxCnt;
@property(nonatomic,strong) NSNumber * maxRowNum;
@property(nonatomic,strong) NSNumber * nextPage;
@property(nonatomic,strong) NSString * poweredby;
@property(nonatomic, strong)NSArray <SingleCatRetDetailsResponse> *retailerDetail;
@property(nonatomic,strong) NSArray <bottomButtonDO> * bottomBtnList;
@property(nonatomic,strong) NSString * mainMenuId;
@end

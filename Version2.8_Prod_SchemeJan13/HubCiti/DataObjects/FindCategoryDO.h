//
//  FindCategoryDO.h
//  HubCiti
//
//  Created by ionnor on 11/7/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FindCategoryDO : NSObject
{
    NSString *CatDisName;
    NSString *CatImgPth;
    NSString *catID;
    NSMutableArray *locationServiceObjArray;
    
}
@property (nonatomic, strong)NSString *CatDisName;
@property (nonatomic, strong)NSString *CatImgPth;
@property (nonatomic, strong)NSString *catID;
@property (nonatomic, strong)NSMutableArray *locationServiceObjArray;

@end

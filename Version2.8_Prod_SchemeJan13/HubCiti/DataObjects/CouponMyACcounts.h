//
//  CouponMyACcounts.h
//  HubCiti
//
//  Created by Lakshmi H R on 12/30/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//
//{
//    "responseCode": "10000",
//    "responseText": "Success",
//    "bottomBtn": 1,
//    "maxCnt": 0,
//    "nextPage": 0,
//    "couponList": [
//                   {
//                       "type": "Saved",
//                       "list": [
//                                {
//                                    "couponId": 73964,
//                                    "couponName": "122",
//                                    "couponImagePath": "http://66.228.143.27:8080/Images/hubciti/2070/4_91056.png",
//                                    "bannerTitle": "bannertitlesample",
//                                    "counts": 1,
//                                    "retId": 1126670,
//                                    "retName": "spanqa.r2",
//                                    "label": "Saved",
//                                    "distance": "0.5 mi",
//                                    "rowNum": 1,
//                                    "couponListId": "75487"
//                                },
//                                {
//                                    "couponId": 73944,
//                                    "couponName": "Push Coupon 3 white dove",
//                                    "couponImagePath": "http://66.228.143.27:8080/Images/hubciti/2070/4_91056.png",
//                                    "bannerTitle": "bannertitlesample",
//                                    "counts": 4,
//                                    "retId": 1108571,
//                                    "retName": "3 White Doves",
//                                    "label": "Saved",
//                                    "distance": "0.4 mi",
//                                    "rowNum": 2,
//                                    "couponListId": "75483"
//                                },

#import <Foundation/Foundation.h>
#import "bottomButtonDO.h"
#import "MyAccountDetails.h"

@interface CouponMyACcounts : NSObject
@property (nonatomic,strong) NSString *responseCode;
@property (nonatomic,strong) NSString *responseText;
@property (nonatomic,strong) NSNumber *bottomBtn;
@property (nonatomic,strong) NSNumber *maxCnt;
@property (nonatomic,strong) NSNumber *nextPage;
@property (nonatomic,strong) NSNumber *maxRowNum;
@property (strong, nonatomic) NSArray<bottomButtonDO> *bottomBtnList;
@property(nonatomic,strong) NSArray<MyAccountDetails> * couponList;
@end

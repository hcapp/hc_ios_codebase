//
//  EventDetailsDO.h
//  HubCiti
//
//  Created by ionnor on 11/22/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventDetailsDO : NSObject
{
     NSString *eventId;
    NSString *eventCatId;
     NSString *eventName;
     NSString *shortDes;
     NSString *longDes;
     NSString *hubCitiId;
     NSString *imgPath;
     NSString *busEvent;
     NSString *pkgEvent;
     NSString *startDate;
     NSString *endDate;
     NSString *startTime;
     NSString *endTime;
     NSString *mItemExist;
     NSString *eventListId;
     NSString *distance;
    NSString *isOnGoing;
     NSString *address;
     NSString *latitude;
     NSString *longitude;
     NSString *pkgDes;
     NSString *pkgTicketURL;
     NSString *hotelFlag;
    NSString *moreInfoURL;
    NSString *recurringDays;
    NSString *isAppSiteFlag;
    
    NSString *address1;
    NSString *address2;
    NSString *location;
    
    NSString *recurrencePattern;
    NSString *isWeekDay;
    NSInteger recurrenceInterval;
    NSArray *daysOfWeek;
    NSInteger dateOfMonth;
    NSArray *everyWeekDayMonth;
    NSString *recurrencePatternName;
    NSString *byDayNumber;
    NSInteger dayNumber;
}

@property (nonatomic, strong)NSString *address;
@property (nonatomic, strong)NSString *address1;
@property (nonatomic, strong)NSString *address2;
@property (nonatomic, strong)NSString *latitude;
@property (nonatomic, strong)NSString *longitude;
@property (nonatomic, strong)NSString *pkgDes;
@property (nonatomic, strong)NSString *pkgTicketURL;
@property (nonatomic, strong)NSString *hotelFlag;
@property (nonatomic, strong)NSString *isOnGoing;
@property (nonatomic, strong)NSString *distance;
@property (nonatomic, strong)NSString *eventId;
@property (nonatomic, strong)NSString *eventCatId;
@property (nonatomic, strong)NSString *eventName;
@property (nonatomic, strong)NSString *shortDes;
@property (nonatomic, strong)NSString *longDes;
@property (nonatomic, strong)NSString *hubCitiId;
@property (nonatomic, strong)NSString *imgPath;
@property (nonatomic, strong)NSString *busEvent;
@property (nonatomic, strong)NSString *pkgEvent;
@property (nonatomic, strong)NSString *startDate;
@property (nonatomic, strong)NSString *endDate;
@property (nonatomic, strong)NSString *startTime;
@property (nonatomic, strong)NSString *endTime;
@property (nonatomic, strong)NSString *mItemExist;
@property (nonatomic, strong)NSString *eventListId;
@property (nonatomic, strong)NSString *moreInfoURL;
@property (nonatomic, strong)NSString *recurringDays;
@property (nonatomic, strong)NSString * isAppSiteFlag;
@property (nonatomic, strong)NSString *logisticsURL;
@property (nonatomic,strong)NSString *location;

@property (nonatomic,strong) NSString *recurrencePattern;
@property (nonatomic,strong)NSString *isWeekDay;
@property (nonatomic) NSInteger recurrenceInterval;
@property (nonatomic)NSInteger dateOfMonth;
@property (nonatomic)NSInteger dayNumber;
@property (nonatomic,strong)NSArray *daysOfWeek;
@property (nonatomic,strong)NSArray *everyWeekDayMonth;
@property (nonatomic,strong) NSString *recurrencePatternName;
@property (nonatomic,strong) NSString *byDayNumber;

@end

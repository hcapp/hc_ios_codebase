//
//  SGTableSubRow.h
//  SwipeGesture
//
//  Created by Bindu M on 5/11/15.
//  Copyright (c) 2015 Bindu M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SGTableSubRow : NSObject
@property (strong, nonatomic) NSString *subRowName;
@property (assign, nonatomic) int16_t subRowsCount;
@property (assign, nonatomic) BOOL isRow;
@property (assign, nonatomic) BOOL isSelected;
@property (assign, nonatomic) BOOL isFirstRow;
@property (assign, nonatomic) BOOL isFirstRowSelected;

@property (strong, nonatomic) NSString *parentRow;


@end

//
//  RetailerData.h
//  HubCiti
//
//  Created by Keshava on 6/3/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RetailerData : NSObject
{
    NSString *retailId;
    NSString *retailName;
    NSString *retailLocId;
    NSString *retailImage;
    NSString *completeAddr;
    NSString * retailerId;
    NSString * retailerName;
    NSString * retailLocationId;
    NSString * completeAddress;
    NSString * logoImagePath;
    NSString * latitude;
    NSString * longitude;
    NSString * retListId;
}

@property (strong,nonatomic)  NSString *retailId;
@property (strong,nonatomic)  NSString *retailName;
@property (strong,nonatomic)  NSString *retailLocId;
@property (strong,nonatomic)  NSString *retailImage;
@property (strong,nonatomic)  NSString *completeAddr;
@property (strong,nonatomic) NSString * retailerId;
@property (strong,nonatomic) NSString * retailerName;
@property (strong,nonatomic) NSString * retailLocationId;
@property (strong,nonatomic) NSString * completeAddress;
@property (strong,nonatomic) NSString * logoImagePath;
@property (strong,nonatomic) NSString * latitude;
@property (strong,nonatomic) NSString * longitude;
@property (strong,nonatomic) NSString * retListId;
- (void)setRetailerDetailWithDictionary:(NSDictionary*)retailerDetails;
@end

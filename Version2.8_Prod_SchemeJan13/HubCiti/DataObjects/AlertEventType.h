//
//  AlertEventType.h
//  HubCiti
//
//  Created by Anjana on 9/10/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

/************************************************
Class to hold the details of alert and event types.
*************************************************/

@interface AlertEventDODetail : NSObject
{
    NSString    *mainTitle;
    NSString    *subTitle;
}
@property (nonatomic, retain) NSString * mainTitle;
@property (nonatomic, retain) NSString * subTitle;
@end


/************************************************
 Class to hold the alert and event data objects.
 header name which holds the name of each categorysent.
 alertArray holds the object of AlertEventDODetail.
 *************************************************/
@interface AlertEventDO : NSObject
{
    NSString    *headerName;
    NSMutableArray *alertArray;
}
@property (nonatomic,retain) NSMutableArray *alertArray;
@property (nonatomic, retain) NSString  *headerName;


@end


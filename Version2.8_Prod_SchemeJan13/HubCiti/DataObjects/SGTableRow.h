//
//  SGTableRow.h
//  SwipeGesture
//
//  Created by Bindu M on 5/11/15.
//  Copyright (c) 2015 Bindu M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SGTableRow : NSObject
@property (strong, nonatomic) NSString *rowName;
@property (strong, nonatomic) NSMutableArray *subrows;
@property (assign, nonatomic) BOOL isRowExpanded;
@property (assign, nonatomic) BOOL isRow;
@property (assign, nonatomic) BOOL isSubrowsLoaded;
//@property (assign, nonatomic) BOOL isFirstRow;
//@property (assign, nonatomic) BOOL isFirstRowSelected;

@end

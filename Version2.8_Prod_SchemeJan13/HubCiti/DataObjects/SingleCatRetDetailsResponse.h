//
//  SingleCatRetDetailsResponse.h
//  HubCiti
//
//  Created by Nikitha on 2/15/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol SingleCatRetDetailsResponse;
@interface SingleCatRetDetailsResponse : NSObject
@property(nonatomic,strong) NSString * advertisementId;
@property(nonatomic,strong) NSString * bannerAdImagePath;
@property(nonatomic,strong) NSString * city;
@property(nonatomic,strong) NSString * completeAddress;
@property(nonatomic,strong) NSString * distance;
@property(nonatomic,strong) NSString * logoImagePath;
@property(nonatomic,strong) NSString * postalCode;
@property(nonatomic,strong) NSString * retLatitude;
@property(nonatomic,strong) NSNumber * retListId;
@property(nonatomic,strong) NSString * retLongitude;
@property(nonatomic,strong) NSNumber * retailLocationId;
@property(nonatomic,strong) NSNumber * retailerId;
@property(nonatomic,strong) NSString * retailerName;
@property(nonatomic,strong) NSString * retaileraddress1;
@property(nonatomic,strong) NSString * retaileraddress2;
@property(nonatomic,strong) NSString * ribbonAdURL;
@property(nonatomic,strong) NSString * locationOpen;
@property(nonatomic,strong) NSNumber * rowNumber;
@property(nonatomic,assign) BOOL  saleFlag;
@property(nonatomic,strong) NSString * splashAdId;
@property(nonatomic,strong) NSString * ribbonAdImagePath;
@property(nonatomic,strong) NSString * state;

@end

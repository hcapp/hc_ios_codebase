//
//  RetailerData.m
//  HubCiti
//
//  Created by Keshava on 6/3/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "RetailerData.h"

@implementation RetailerData

@synthesize retailId,retailImage,retailLocId,retailName,completeAddr,completeAddress,longitude,latitude,logoImagePath,retailerId,retailerName,retListId,
retailLocationId;
- (void)setRetailerDetailWithDictionary:(NSDictionary*)retailerDetails {
    if (self) {
        self.retailerId = [NSString stringWithFormat:@"%@", [retailerDetails valueForKey:@"retailerId"]];
        self.retailerName = [NSString stringWithFormat:@"%@", [retailerDetails valueForKey:@"retailerName"]];
        self.retailLocationId = [NSString stringWithFormat:@"%@", [retailerDetails valueForKey:@"retailLocationId"]];
        self.latitude = [NSString stringWithFormat:@"%@", [retailerDetails valueForKey:@"latitude"]];
        self.longitude = [NSString stringWithFormat:@"%@", [retailerDetails valueForKey:@"longitude"]];
        self.completeAddress = [NSString stringWithFormat:@"%@", [retailerDetails valueForKey:@"completeAddress"]];
        self.logoImagePath = [NSString stringWithFormat:@"%@",[retailerDetails valueForKey:@"logoImagePath"]];
        self.retListId = [NSString stringWithFormat:@"%@",[retailerDetails valueForKey:@"retListId"]];
    }
}
@end

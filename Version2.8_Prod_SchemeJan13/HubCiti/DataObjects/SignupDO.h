//
//  SignupDO.h
//  HubCiti
//
//  Created by Anjana on 10/11/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SignupDO : NSObject
{
    NSString *fontColor;
    NSString *buttonFontColor;
    NSString *backgroundColor;
    NSString *logoImagePath;
    NSString *smallImage;
    NSString *buttonColor;
    NSString *description;
    
    
}
@property (nonatomic, strong) NSString *fontColor;
@property (nonatomic, strong) NSString *buttonFontColor;
@property (nonatomic, strong) NSString *backgroundColor;
@property (nonatomic, strong) NSString *logoImagePath;
@property (nonatomic, strong) NSString *smallImage;
@property (nonatomic, strong) NSString *buttonColor;
@property (nonatomic, strong) NSString *description;



@end

//
//  CouponMapDetails.h
//  HubCiti
//
//  Created by Lakshmi H R on 12/29/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

//"couponId": 73988,
//"couponName": "Error",
//"retailLocationId": 2105735,
//"distance": "4.2 mi",
//"retLatitude": "30.568007",
//"retLongitude": "-98.199427",
//"location": "1839 County Road 343, Marble Falls, TX 78654, USA, Marble Falls, TX, 78654"

#import <Foundation/Foundation.h>
@protocol CouponMapDetails;

@interface CouponMapDetails : NSObject

@property (nonatomic,strong) NSString *couponId;
@property (nonatomic,strong) NSString *couponName;
@property (nonatomic,strong) NSString *retailLocationId;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *retLatitude;
@property (nonatomic,strong) NSString *retLongitude;
@property (nonatomic,strong) NSString *location;

- (void)setCouponDetailsWithDictionary:(NSDictionary*)couponDetails;

@end

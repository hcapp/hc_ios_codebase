//
//  CityExperienceDO.m
//  HubCiti
//
//  Created by Anjana on 10/9/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "CityExperienceDO.h"

@implementation CityExperienceDO

@synthesize retailAddress,retailerId,retailerName,retListId,retailLocationId,rowNumber;
@synthesize logoImagePath,distanceCE,bannerAdImagePath,ribbonAdImagePath,ribbonAdURL;
@synthesize saleFlag, latitude, longitude,postalcode,city,state,locationOpen,retailerAddress2,retailerAddress1;
@end

@implementation CEPartnerListDO

@synthesize PLretailerId,PLretailerName,PLretailerLogo;

@end

@implementation CECategoryDO

@synthesize catId,catImg,catName,catObjArray;
@end

@implementation RetailerSummaryDO
@synthesize retId, retName, retLocId, retaddress;
@synthesize distance, bannerAdImagePath, ribbonAdImagePath;
@synthesize ribbonAdURL, saleFlag, retLatitude, retLongitude;
@synthesize retDetId, contactPhone, retListId;
@synthesize title, subTitle, index, imgUrl;

@end
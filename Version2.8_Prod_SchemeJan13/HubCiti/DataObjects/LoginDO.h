//
//  LoginDO.h
//  HubCiti
//
//  Created by Anjana on 9/16/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

/**************************************
 Login data object created to hold the data from server to design the UI for Login screen
 Assumption made on few fields. Will vary once service is ready
 *************************************/
#import <Foundation/Foundation.h>

@interface LoginDO : NSObject
{
    NSString *fontColor;
    NSString *buttonFontColor;
    NSString *backgroundColor;
    NSString *logoPath;
    NSString *smallLogoPath;
    NSString *poweredByPath;
    NSString *buttonColor;
    NSString * bkImgPath;
    NSString * homeImgPath;
    NSString * titleTxtColor;
    NSString * titleBkGrdColor;
    NSString * hamburgerImg;
    
}
@property (nonatomic, strong) NSString *fontColor;
@property (nonatomic, strong) NSString *buttonFontColor;
@property (nonatomic, strong) NSString *backgroundColor;
@property (nonatomic, strong) NSString *logoPath;
@property (nonatomic, strong) NSString *smallLogoPath;
@property (nonatomic, strong) NSString *poweredByPath;
@property (nonatomic, strong) NSString *buttonColor;
@property (nonatomic, strong) NSString *bkImgPath;
@property (nonatomic, strong) NSString *homeImgPath;
@property (nonatomic, strong) NSString *titleTxtColor;
@property (nonatomic, strong) NSString *titleBkGrdColor;
@property (nonatomic, strong) NSString *hamburgerImg;

@end

//
//  CouponDetailResponse.h
//  HubCiti
//
//  Created by Nikitha on 12/6/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CouponDetailResponse : NSObject

@property(nonatomic,strong) NSString * responseCode;
@property(nonatomic,strong) NSString * responseText;
@property(nonatomic,strong) NSNumber * viewOnWeb;
@property(nonatomic,strong) NSString * termAndConditions;
@property(nonatomic,strong) NSNumber* usedFlag;
@property(nonatomic,strong) NSString * couponURL;
@property(nonatomic,strong) NSNumber * couponId;
@property(nonatomic,strong) NSString * couponName;
@property(nonatomic,strong) NSString * couponDesc;
@property(nonatomic,strong) NSString * couponStartDate;
@property(nonatomic,strong) NSString * couponExpireDate;
@property(nonatomic,strong) NSString * couponImagePath;
@property(nonatomic,strong) NSNumber * expireFlag;
@property(nonatomic,strong) NSString * retName;
@property(nonatomic,strong) NSNumber * prodFlag;
@property(nonatomic,strong) NSNumber * locatnFlag;
@property (nonatomic,strong) NSString * bannerName;

@end



//
//  MyAccountDetails.h
//  HubCiti
//
//  Created by Lakshmi H R on 12/30/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//
@protocol MyAccountDetails;
#import <Foundation/Foundation.h>
#import "CouponsRetailerDetails.h"

@interface MyAccountDetails : NSObject
@property(nonatomic,strong) NSArray<CouponsRetailerDetails> * list;
@property(nonatomic,strong) NSString *type;
@end

//
//  specialofferlistDO.h
//  HubCiti
//
//  Created by ionnor on 10/30/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface specialofferlistDO : NSObject
{
    NSString *pageTitle;
    NSString *pageLink;
    NSString *titleImage;
    NSString *specialsListID;
}
@property (nonatomic, strong)NSString *pageTitle;
@property (nonatomic, strong)NSString *pageLink;
@property (nonatomic, strong)NSString *titleImage;
@property (nonatomic, strong)NSString *specialsListID;
@end

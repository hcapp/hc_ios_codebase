//
//  couponViewDO.h
//  HubCiti
//
//  Created by ionnor on 10/30/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface couponViewDO : NSObject
{
    NSString *couponName;
    NSString *couponId;
    NSString *couponImagePath;
    NSString *couponLongDescription;
    NSString *usage;
    NSString *viewableOnWeb;
    NSString *couponListID;
    NSString * isFeatured;
}
@property (nonatomic, strong)NSString *couponName;
@property (nonatomic, strong)NSString *couponId;
@property (nonatomic, strong)NSString *couponImagePath;
@property (nonatomic, strong)NSString *couponLongDescription;
@property (nonatomic, strong)NSString *usage;
@property (nonatomic, strong)NSString *viewableOnWeb;
@property (nonatomic, strong)NSString *couponListID;
@property(nonatomic,strong) NSString * isFeatured;
@end

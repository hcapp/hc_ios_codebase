//
//  CouponMapResponse.h
//  HubCiti
//
//  Created by Lakshmi H R on 12/29/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//


//"responseCode": "10000",
//"responseText": "Success",
//"couponMapLocs":

#import <Foundation/Foundation.h>
#import "CouponMapDetails.h"

@interface CouponMapResponse : NSObject
@property (nonatomic,strong) NSString *responseCode;
@property (nonatomic,strong) NSString *responseText;
@property (nonatomic,strong) NSArray<CouponMapDetails> *couponMapLocs;
@end

//
//  FundraiserDeatilsDO.h
//  HubCiti
//
//  Created by Keshava on 8/18/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FundraiserDeatilsDO : NSObject
{
    NSString *fundraiserId;
    NSString *fundraiserListId;
    NSString *title;
    NSString *imagePath;
    NSString *startDate;
    NSString *endDate;
    NSString *shortDescription;
    NSString *longDescription;
    NSString *currentValue;
    NSString *goal;
    NSString *purchasedProducts;
    NSString *hubCitiId;
    NSString *moreInfoURL;
    NSString *isEventFlag;
}
@property(nonatomic,strong) NSString *fundraiserId;
@property(nonatomic,strong) NSString *fundraiserListId;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *imagePath;
@property(nonatomic,strong) NSString *startDate;
@property(nonatomic,strong) NSString *endDate;
@property(nonatomic,strong) NSString *shortDescription;
@property(nonatomic,strong) NSString *longDescription;
@property(nonatomic,strong) NSString *currentValue;
@property(nonatomic,strong) NSString *goal;
@property(nonatomic,strong) NSString *purchasedProducts;
@property(nonatomic,strong) NSString *hubCitiId;
@property(nonatomic,strong) NSString *moreInfoURL;
@property(nonatomic,strong) NSString *isEventFlag;
@end

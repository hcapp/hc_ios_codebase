//
//  CouponMapDetails.m
//  HubCiti
//
//  Created by Lakshmi H R on 12/29/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//


//@property (nonatomic,strong) NSString *couponId;
//@property (nonatomic,strong) NSString *couponName;
//@property (nonatomic,strong) NSString *retailLocationId;
//@property (nonatomic,strong) NSString *distance;
//@property (nonatomic,strong) NSString *retLatitude;
//@property (nonatomic,strong) NSString *retLongitude;
//@property (nonatomic,strong) NSString *location;

#import "CouponMapDetails.h"

@implementation CouponMapDetails

- (void)setCouponDetailsWithDictionary:(NSDictionary*)couponDetails {
    if (self) {
        self.couponId = [NSString stringWithFormat:@"%@", couponDetails[@"couponId"]];
        self.couponName = [NSString stringWithFormat:@"%@", couponDetails[@"couponName"]];
        self.retailLocationId = [NSString stringWithFormat:@"%@", couponDetails[@"retailLocationId"]];
        self.distance = [NSString stringWithFormat:@"%@", couponDetails[@"distance"]];
        self.retLatitude = [NSString stringWithFormat:@"%@", couponDetails[@"retLatitude"]];
        self.retLongitude = [NSString stringWithFormat:@"%@", couponDetails[@"retLongitude"]];
        self.location = [NSString stringWithFormat:@"%@", couponDetails[@"location"]];
        
        
    }
}

@end

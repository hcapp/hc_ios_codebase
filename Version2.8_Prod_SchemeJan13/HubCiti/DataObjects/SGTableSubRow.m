//
//  SGTableSubRow.m
//  SwipeGesture
//
//  Created by Bindu M on 5/11/15.
//  Copyright (c) 2015 Bindu M. All rights reserved.
//

#import "SGTableSubRow.h"

@implementation SGTableSubRow

@synthesize isFirstRow;
@synthesize isFirstRowSelected;
@synthesize isRow;
@synthesize isSelected;
@synthesize subRowName;
@synthesize parentRow;
@end

//
//  couponViewDO.m
//  HubCiti
//
//  Created by ionnor on 10/30/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "couponViewDO.h"

@implementation couponViewDO
    @synthesize couponName;
    @synthesize couponId;
    @synthesize couponImagePath;
    @synthesize couponLongDescription;
    @synthesize usage;
    @synthesize viewableOnWeb;
    @synthesize couponListID;
@synthesize isFeatured;
@end

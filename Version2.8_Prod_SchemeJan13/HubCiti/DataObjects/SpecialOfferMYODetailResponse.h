//
//  SpecialOfferMYODetailResponse.h
//  HubCiti
//
//  Created by Nikitha on 12/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SpecialOfferMYODetailResponse ;
@interface SpecialOfferMYODetailResponse : NSObject
@property(nonatomic,strong) NSNumber * retailerId;
@property(nonatomic,strong) NSNumber * pageId;
@property(nonatomic,strong) NSString * pageTitle;
@property(nonatomic,strong) NSString * startDate;
@property(nonatomic,strong) NSString * shortDesc;
@property(nonatomic,strong) NSString * longDesc;
@property(nonatomic,strong) NSString * retImagePath;
@property(nonatomic,strong) NSString * endDate;
@end


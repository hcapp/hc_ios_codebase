//
//  FaqDetailsDo.h
//  HubCiti
//
//  Created by Service on 28/02/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FaqDetailsDo : NSObject
{
    NSString    *categoryId, *categoryName;
    int rowNumber;
    NSMutableArray *arrQuestions;
    BOOL isExpanded;
}
@property(readwrite)BOOL isExpanded;
@property(nonatomic, strong)NSMutableArray *arrQuestions;
@property(nonatomic, strong)NSString *categoryId;
@property(nonatomic, strong)NSString *categoryName;

@end

//
//  Multivalue.h
//  HubCiti
//
//  Created by Kitty on 18/12/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Multivalue : NSObject{
    NSString *header;
    NSMutableArray *valueNames,*valueIds,*filterId;
    BOOL isExpanded;
}

@property(nonatomic,strong)NSMutableArray *valueNames,*valueIds,*filterId;
@property(nonatomic,strong) NSString *header;
@property(nonatomic) BOOL isExpanded;

@end

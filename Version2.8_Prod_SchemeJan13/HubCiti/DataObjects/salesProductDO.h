//
//  salesProductDO.h
//  HubCiti
//
//  Created by ionnor on 10/30/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface salesProductDO : NSObject
{
    NSString *productName;
    NSString *productDescription;
    NSString *productId;
    NSString *imagePath;
    NSString *rowNumber;
    NSString *regularPrice;
    NSString *salePrice;
    NSString *saleListID;
    NSString *discount;
}
@property (nonatomic, strong)NSString *productName;
@property (nonatomic, strong)NSString *productDescription;
@property (nonatomic, strong)NSString *productId;
@property (nonatomic, strong)NSString *imagePath;
@property (nonatomic, strong)NSString *rowNumber;
@property (nonatomic, strong)NSString *regularPrice;
@property (nonatomic, strong)NSString *salePrice;
@property (nonatomic, strong)NSString *saleListID;
@property (nonatomic, strong)NSString *discount;
@end

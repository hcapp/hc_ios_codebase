//
//  FundraiserCategoryDO.h
//  HubCiti
//
//  Created by Keerthi on 19/08/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FundraiserCategoryDO : NSObject
{
    NSString *categoryId;
    NSString *categoryName;
    NSMutableArray *catObjArray;
}
@property (nonatomic, strong)NSMutableArray *catObjArray;
@property (nonatomic, strong)NSString *categoryId;
@property (nonatomic, strong)NSString *categoryName;

@end

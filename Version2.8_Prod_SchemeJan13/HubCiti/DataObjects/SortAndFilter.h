//
//  SortAndFilter.h
//  HubCiti
//
//  Created by Bindu M on 5/6/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SortAndFilter : NSObject{
    NSMutableString *selectedCitiIds, *selectedCatIds, *selectedOptIds, *selectedInterestsIds, *selectedOthersIds;
    
    BOOL distanceSelected,localSpecialSelected,sortEventDateSelected,eventDateSelected, alphabeticallySelected, alphabetSelectedGovQA,typeSelectedGovQA, nameSelectedGovQA,alphabetFindInfoGovQA,bandSelected, venueSelected;
    
    NSString *evtDate,*categoryGovQA,*subCategoryGovQA,*requestViewFilter;
}
@property(nonatomic,strong)NSMutableString *selectedCitiIds, *selectedCatIds, *selectedOptIds, *selectedInterestsIds, *selectedOthersIds;
@property(nonatomic) BOOL distanceSelected,localSpecialSelected,eventDateSelected,sortEventDateSelected , alphabeticallySelected, alphabetSelectedGovQA,typeSelectedGovQA, nameSelectedGovQA,alphabetFindInfoGovQA,bandSelected, venueSelected;
@property(nonatomic,strong) NSString *evtDate,*categoryGovQA,*subCategoryGovQA,*requestViewFilter;


-(SortAndFilter *)initWithObject:(SortAndFilter *)SrtAndFilterObj;

@end
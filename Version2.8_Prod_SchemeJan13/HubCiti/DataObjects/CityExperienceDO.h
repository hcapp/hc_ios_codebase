//
//  CityExperienceDO.h
//  HubCiti
//
//  Created by Anjana on 10/9/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityExperienceDO : NSObject
{
    NSString *retailerId;
    NSString *retailerName;
    NSString *retailLocationId;
    NSString *retListId;
    NSString *logoImagePath;
    NSString *distanceCE;
    NSString *retailAddress;
    NSString *bannerAdImagePath;
    NSString *ribbonAdImagePath;
    NSString *ribbonAdURL;
    NSString *rowNumber;
    NSString *saleFlag;
    NSString *latitude;
    NSString *longitude;
    NSString * state;
    NSString * locationOpen;
    NSString * city;
    NSString * retailerAddress2;
    NSString * retailerAddress1;
    NSString * postalcode;

}

@property (nonatomic, strong) NSString *retailerId;
@property (nonatomic, strong) NSString *retailerName;
@property (nonatomic, strong) NSString *retailLocationId;
@property (nonatomic, strong) NSString *retListId;
@property (nonatomic, strong) NSString *logoImagePath;
@property (nonatomic, strong) NSString *distanceCE;
@property (nonatomic, strong) NSString *retailAddress;
@property (nonatomic, strong) NSString *bannerAdImagePath;
@property (nonatomic, strong) NSString *ribbonAdImagePath;
@property (nonatomic, strong) NSString *ribbonAdURL;
@property (nonatomic, strong) NSString *rowNumber;
@property (nonatomic, strong) NSString *saleFlag;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property(nonatomic,strong) NSString * retailerAddress1;
@property(nonatomic,strong) NSString * retailerAddress2;
@property(nonatomic,strong) NSString * city;
@property(nonatomic,strong) NSString * state;
@property(nonatomic,strong) NSString * postalcode;
@property(nonatomic,strong) NSString * locationOpen;


@end

@interface CEPartnerListDO : NSObject
{
    NSString *PLretailerId;
    NSString *PLretailerName;
    NSString *PLretailerLogo;

}

@property (nonatomic, strong) NSString *PLretailerId;
@property (nonatomic, strong) NSString *PLretailerName;
@property (nonatomic, strong) NSString *PLretailerLogo;

@end


@interface CECategoryDO : NSObject
{
    NSString *catId;
    NSString *catName;
    NSString *catImg;
    NSMutableArray *catObjArray;
}

@property (nonatomic, strong) NSString *catId;
@property (nonatomic, strong) NSString *catName;
@property (nonatomic, strong) NSString *catImg;
@property (nonatomic, strong) NSMutableArray *catObjArray;

@end

@interface RetailerSummaryDO : NSObject{
    
    NSString *retId, *retName, *retLocId, *retaddress;
    NSString *distance, *bannerAdImagePath, *ribbonAdImagePath;
    NSString *ribbonAdURL, *saleFlag, *retLatitude, *retLongitude;
    NSString *retDetId, *contactPhone, *retListId;
    NSString *title, *subTitle, *index, *imgUrl;
}
@property (nonatomic, strong)NSString *retId, *retName, *retLocId, *retaddress;
@property (nonatomic, strong)NSString *distance, *bannerAdImagePath, *ribbonAdImagePath;
@property (nonatomic, strong)NSString *ribbonAdURL, *saleFlag, *retLatitude, *retLongitude;
@property (nonatomic, strong)NSString *retDetId, *contactPhone, *retListId;
@property (nonatomic, strong)NSString *title, *subTitle, *index, *imgUrl;

@end



//
//  eventHotelListDO.h
//  HubCiti
//
//  Created by ionnor on 11/25/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface eventHotelListDO : NSObject
{
    NSString *retailerName;
    NSString *retailLocationId;
    NSString *distance;
    NSString *hotelPrice;
    NSString *hotelrating;
    NSString *imgPath;
    NSString *retailerId;
    NSString *retListId;
    
    NSString *address;
    NSString *roomCheckUrl;
    NSString *roomBookUrl;
    NSString *packagePrice;
    NSString *description;
}
@property (nonatomic, strong)NSString *description;
@property (nonatomic, strong)NSString *retailerName;
@property (nonatomic, strong)NSString *retailLocationId;
@property (nonatomic, strong)NSString *distance;
@property (nonatomic, strong)NSString *hotelPrice;
@property (nonatomic, strong)NSString *hotelrating;
@property (nonatomic, strong)NSString *imgPath;
@property (nonatomic, strong)NSString *retailerId;
@property (nonatomic, strong)NSString *retListId;

@property (nonatomic, strong)NSString *address;
@property (nonatomic, strong)NSString *roomCheckUrl;
@property (nonatomic, strong)NSString *roomBookUrl;
@property (nonatomic, strong)NSString *packagePrice;
@end

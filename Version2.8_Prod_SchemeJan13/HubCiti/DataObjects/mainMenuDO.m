//
//  mainMenuDO.m
//  HubCiti
//
//  Created by ionnor on 10/11/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "mainMenuDO.h"

@implementation mainMenuDO

@synthesize menuId;
@synthesize mItemId;
@synthesize linkTypeId;
@synthesize linkId;
@synthesize position;
@synthesize mItemName;
@synthesize mItemImg;
@synthesize linkTypeName;

@synthesize mBtnColor, mBtnFontColor;
@synthesize smBtnColor, smBtnFontColor;
@synthesize mGrpBkgrdColor, mGrpFntColor;
@synthesize smGrpBkgrdColor, smGrpFntColor;
@synthesize mShapeId,mShapeName,noOfColumns;
@synthesize mFontColor,smFontColor;
@end

@implementation mainMenuBottomDO

@synthesize bottomBtnID;
@synthesize bottomBtnName;
@synthesize bottomBtnImg;
@synthesize bottomBtnImgOff;
@synthesize btnLinkTypeID;
@synthesize btnLinkTypeName;
@synthesize btnLinkID;
@synthesize position;
@synthesize dict_linkTypeName_BottomButton;
-(id)init
{
    if (!(self = [super init])) return nil;
    if (self) {
        [self initTheDictionary];
    }
    return self;
}

- (void)initTheDictionary
{
   // [super init];
    // Custom initialization
    if (dict_linkTypeName_BottomButton == nil) {//assigning number to each bottom buttons - in order to
        dict_linkTypeName_BottomButton = [[NSMutableDictionary alloc]init];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:0] forKey:@"Hot Deals"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:1] forKey:@"Scan Now"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:2] forKey:@"Alerts"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:3] forKey:@"Events"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:4] forKey:@"Whats NearBy"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:5] forKey:@"Find"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:6] forKey:@"City Experience"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:7] forKey:@"City Services"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:8] forKey:@"Visitors Bureau"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:9] forKey:@"Transportation"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:10] forKey:@"Preference"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:11] forKey:@"About"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:12] forKey:@"Share"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:13] forKey:@"Settings"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:14] forKey:@"AnythingPage"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:15] forKey:@"AppSite"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:16] forKey:@"SubMenu"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:17] forKey:@"Filters"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:18] forKey:@"Coupon"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:19] forKey:@"Deals"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:20] forKey:@"FAQ"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:21] forKey:@"Fundraiser"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:29] forKey:@"My Deals"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:30] forKey:@"Privacy Policy"];
        
    }
}


@end

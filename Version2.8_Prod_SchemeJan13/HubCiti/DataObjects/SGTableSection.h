//
//  SGTableRow.h
//  SwipeGesture
//
//  Created by Bindu M on 5/11/15.
//  Copyright (c) 2015 Bindu M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SGTableSection : NSObject
@property (strong, nonatomic) NSString *sectionName;
@property (strong, nonatomic) NSMutableArray *rows;
@property (strong, nonatomic) NSMutableArray *isFirstSection;

@end

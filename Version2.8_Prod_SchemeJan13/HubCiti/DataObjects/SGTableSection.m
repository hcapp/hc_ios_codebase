//
//  SGTableRow.m
//  SwipeGesture
//
//  Created by Bindu M on 5/11/15.
//  Copyright (c) 2015 Bindu M. All rights reserved.
//

#import "SGTableSection.h"

@implementation SGTableSection

@synthesize sectionName;
@synthesize rows;
@synthesize isFirstSection;

@end

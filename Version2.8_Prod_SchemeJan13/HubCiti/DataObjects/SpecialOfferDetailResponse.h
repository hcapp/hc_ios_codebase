//
//  SpecialOfferDetailResponse.h
//  HubCiti
//
//  Created by Nikitha on 12/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpecialOfferMYODetailResponse.h"

@interface SpecialOfferDetailResponse : NSObject
@property(nonatomic,strong) NSString * responseCode;
@property(nonatomic,strong) NSString * responseText;
@property(nonatomic,strong) NSNumber * extLinkFlag;
@property(nonatomic,strong) NSString * externalLink;
@property(nonatomic,strong) NSArray <SpecialOfferMYODetailResponse> * retDetailList;

@end


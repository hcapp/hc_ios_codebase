//
//  SGTableRow.m
//  SwipeGesture
//
//  Created by Bindu M on 5/11/15.
//  Copyright (c) 2015 Bindu M. All rights reserved.
//

#import "SGTableRow.h"

@implementation SGTableRow

@synthesize rowName;
@synthesize subrows;
@synthesize isRow;
@synthesize isRowExpanded;
@synthesize isSubrowsLoaded;

@end

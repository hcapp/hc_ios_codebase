//
//  salesProductDO.m
//  HubCiti
//
//  Created by ionnor on 10/30/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "salesProductDO.h"

@implementation salesProductDO

    @synthesize productName;
    @synthesize productDescription;
    @synthesize productId;
    @synthesize imagePath;
    @synthesize rowNumber;
    @synthesize regularPrice;
    @synthesize salePrice;
    @synthesize saleListID;
    @synthesize discount;
@end

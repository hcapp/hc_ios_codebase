//
//  ViewMyRequestsTableViewCell.h
//  HubCiti
//
//  Created by Bindu M on 10/1/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewMyRequestsTableViewCell : UITableViewCell

@property(nonatomic , strong) UILabel* refNumField;
@property(nonatomic , strong) UILabel* statusField;
@property(nonatomic , strong) UILabel* summaryField;
@property(nonatomic , strong) UILabel* refNum;
@property(nonatomic , strong) UILabel* status;
@property(nonatomic , strong) UILabel* summary;
//@property(nonatomic , retain) UIButton* btnField;

@end

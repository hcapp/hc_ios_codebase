//
//  CustomizedNavController.m
//  HubCiti
//
//  Created by Ashika on 8/18/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "CustomizedNavController.h"
#import "MFSideMenu.h"
@interface CustomizedNavController ()


{
    UIButton *backButton;
    UIButton *hamburger;
    SdImageView * hamburgerImage;
    SdImageView *backImage;
}
@end

@implementation CustomizedNavController
@synthesize customNavBardelegate;
- (id)initWithRootViewController:(UIViewController *)rootViewController {
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        [self setHamburgerButtonToController:rootViewController];
    }
    return self;
}

- (void)setHamburgerButtonToController:(UIViewController *)viewController {
    backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setAccessibilityLabel:@"Back"];
    backButton.frame=CGRectMake(5.0, 4.0, 30.0, 30.0);
    backImage= [[SdImageView alloc]initWithFrame:backButton.frame];
    [backImage loadImage:[[defaults valueForKey:@"bkImgPath"] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
    [backButton addSubview:backImage];
    [backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    backButton.tag =1;
    [viewController.navigationController.navigationBar addSubview:backButton];
    
    hamburger = [UIButton buttonWithType:UIButtonTypeCustom];
   
    hamburger.frame = CGRectMake(5, 4, 30, 30);
    hamburgerImage = [[SdImageView alloc]initWithFrame:hamburger.frame];
    NSLog(@"Hamberger name %@",[defaults valueForKey:@"hamburgerImg"]);
    [hamburgerImage loadImage:[[defaults valueForKey:@"hamburgerImg"]stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
    [hamburger addSubview:hamburgerImage];
    [hamburger addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
     hamburger.tag =2;
    //UIBarButtonItem *hamburgers = [[UIBarButtonItem alloc] initWithCustomView:hamburger];
    //[viewController.navigationItem setLeftBarButtonItem:hamburgers];
    [viewController.navigationController.navigationBar addSubview:hamburger];
    
}
-(void) backButtonPressed : (id)sender{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    [customNavBardelegate popBackToPreviousPage];
}
-(void) hideBackButton : (BOOL) hide{
  
    backButton.hidden = hide;
    if(hide){
        CGRect frame;

            frame = CGRectMake(5, 4, 30, 30);
        
        hamburger.frame = frame;
    }
    else{
        CGRect frame;
        frame = CGRectMake(40, 4,30, 30);
        hamburger.frame = frame;
    }
    
}

-(void) hideBackButtonForTransperentImage{
    
    backButton.hidden = YES;
    CGRect frame;
    frame = CGRectMake(34, 4,30, 30);
    hamburger.frame = frame;
}
-(void) changeHambugerImage
{
    [[NSNotificationCenter defaultCenter] postNotificationName:hamburgerImageChangeNotification object:self];
    [self updateHamburgerImage];
}

-(void) updateHamburgerImage
{
    [hamburgerImage loadImage:[defaults valueForKey:@"hamburgerImg"]];
}

-(void) changeBackImage : (NSString*) imageURL
{
    [backImage loadImage:imageURL];
}
-(void) hideHambergerButton : (BOOL) hide{
    
    hamburger.hidden = hide;
}
- (void)leftSideMenuButtonPressed:(id)sender
{
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) setTitle:(NSString*)titleString forView:(UIViewController*) viewController withHambergur:(BOOL)isHamburger{
    
    
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,isHamburger? VARIABLE_WIDTH(150):VARIABLE_WIDTH(200), 40)];
    
    
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, isHamburger? VARIABLE_WIDTH(150):VARIABLE_WIDTH(200), 40)];
    titleLabel.textColor = [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont boldSystemFontOfSize: IPAD?18:16];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = titleString;
    titleLabel.numberOfLines = 2;
    [titleView addSubview:titleLabel];
    viewController.navigationItem.titleView = titleView;
}
-(void) setTitle:(NSString*)titleString forView:(UIViewController*) viewController withHambergur:(BOOL)isHamburger withColor:(NSString*)colorForTitle{
    
    
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,isHamburger? VARIABLE_WIDTH(150):VARIABLE_WIDTH(200), 40)];
    
    
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, isHamburger? VARIABLE_WIDTH(150):VARIABLE_WIDTH(200), 40)];
    titleLabel.textColor = [UIColor convertToHexString:colorForTitle];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont boldSystemFontOfSize: IPAD?18:16];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = titleString;
    titleLabel.numberOfLines = 2;
    [titleView addSubview:titleLabel];
    viewController.navigationItem.titleView = titleView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

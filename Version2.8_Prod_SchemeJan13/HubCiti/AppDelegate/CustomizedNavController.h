//
//  CustomizedNavController.h
//  HubCiti
//
//  Created by Ashika on 8/18/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomizedNavControllerDelegate<NSObject>

@optional
-(void) popBackToPreviousPage;
@end

@interface CustomizedNavController : UINavigationController
@property (nonatomic,weak) id <CustomizedNavControllerDelegate> customNavBardelegate;
-(void) setTitle:(NSString*)titleString forView:(UIViewController*) viewController withHambergur:(BOOL)isHamburger withColor:(NSString*)colorForTitle;

- (id)initWithRootViewController:(UIViewController *)rootViewController;
-(void) setTitle:(NSString*)titleString forView:(UIViewController*) viewController withHambergur:(BOOL)isHamburger;
-(void) hideBackButton : (BOOL) hide;
-(void) hideHambergerButton : (BOOL) hide;
-(void) changeHambugerImage;
-(void) changeBackImage : (NSString*) imageURL;
-(void) hideBackButtonForTransperentImage;
@end

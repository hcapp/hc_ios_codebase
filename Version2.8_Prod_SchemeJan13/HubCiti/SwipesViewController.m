//
//  SwipesViewController.m
//  HubCiti
//
//  Created by Kitty on 19/05/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "SwipesViewController.h"
#import "SGTableSection.h"
#import "SGTableRow.h"
#import "SGTableSubRow.h"
#import "SingleCatRetailers.h"
#import "EventsListViewController.h"
#import "FilterRetailersList.h"
#import "CityExperienceViewController.h"
#import "FindLocationServiceViewController.h"
#import "MainMenuViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface SwipesViewController (){
    UIButton *expandRowBtn;
    UIImageView *caratImage;
    UIImageView *checkButtonImg;
    UILabel *lblRow;
    UIButton *checkBtn ;
    NSMutableDictionary *tempOthersDisc;
    NSMutableDictionary *tempSelectedIdDic;
    NSMutableDictionary *retainOthersSelectedIdDic;
    NSMutableDictionary *fetchRetainedSelectedOthersDic;
    BOOL expandOnce;
    BOOL donePressedDate;
}

#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)

@end

@implementation SwipesViewController


@synthesize  showDone,bottomBtn ,swipeTV, sortByItemArray, secHeaderData, activityIndicator, filterByItemArray, mainTableArr,done, catArray,dataArray,  optArray, cityArray,intArray,othersArray,module,categoryName,catId,srchKey,retailId,retailLocationId,isRetailerEvent,fundraiserId,distanceSelected,localSpecialSelected,eventDateSelected,sortFilObj,sortEventDateSelected,cityExpId, alphabeticallySelected, filterID,bandSelected,venueSelected;
////////////////// Changes on 23rd may////////////////

@synthesize arrCategoryId,arrCategoryNames,arrSelectedCatIds,arrSelectedCatNames,arrOptionsId,arrOptionsNames,arrSelectedOptionsIds,arrSelectedOptionsNames,arrCitiId,arrCitiesNames,arrSelectedCitiIds,arrSelectedCitiNames,arrInterestsId,arrInterestsNames,arrSelectedInterestsIds,arrSelectedInterestsNames,arrOthersId,arrOthersNames,arrSelectedOthersIds,arrSelectedOthersNames,categoryDic,optionsDic,citiesDic,interestsDic,othersDic,srchKeyBand,evtTypeID,bandId,segmentSelected;

@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    expandOnce = YES;
    
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    UIButton *btn = [UtilityManager customizeBarButtonItem:@"Done"];
    [btn addTarget:self action:@selector(sortingDone) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = mainPage;
   
    
    
    UIButton *btncancel = [UtilityManager customizeBarButtonItem:NSLocalizedString(@"Cancel",@"Cancel")];
    [btncancel addTarget:self action:@selector(cancelButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* btncancelbar = [[UIBarButtonItem alloc] initWithCustomView:btncancel];
    self.navigationItem.rightBarButtonItem = btncancelbar;
    
    
    if([module isEqualToString:@"Events"] || [module isEqualToString:@"CitiEXP"] || [module isEqualToString:@"Filter"] || [module isEqualToString:@"thislocation"] || [module isEqualToString:@"Find Single"] || [module isEqualToString:@"Find All"])
    {
        self.navigationItem.title = @"Filter";
    }
    else
    {
        self.navigationItem.title = @"Filter & Sort";
    }
    
   
        swipeTV = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT-45-40)];
    
    
    swipeTV.delegate = self;
    swipeTV.dataSource = self;
    
    [self.view addSubview:swipeTV];
    
    secHeaderData = [[NSMutableArray alloc]init];// :@"Sort", @"Filter", nil];
    
    sortByItemArray = [[NSMutableArray alloc]init];
    
    
    filterByItemArray = [[NSMutableArray alloc]init];
    
    
    arrSelectedInterestsIds = [[NSMutableArray alloc]init];
    
    arrSelectedCatIds = [[NSMutableArray alloc]init];
    arrSelectedCitiIds = [[NSMutableArray alloc]init];
    arrSelectedOthersIds = [[NSMutableArray alloc]init];
    arrSelectedOptionsIds = [[NSMutableArray alloc]init];
    
    
    
    othersDic = [[NSMutableDictionary alloc]init];
    
    tempOthersDisc = [[NSMutableDictionary alloc] init];
    tempSelectedIdDic = [[NSMutableDictionary alloc] init];
    fetchRetainedSelectedOthersDic = [[NSMutableDictionary alloc] init];
    retainOthersSelectedIdDic = [[NSMutableDictionary alloc] init];
    
    
    
    
    NSLog(@"RowData %@", filterByItemArray);
    
    ////////////////// Changes on 23rd may////////////////
    
    
    if(sortFilObj.selectedCatIds){
        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@","];
        NSRange range = [sortFilObj.selectedCatIds rangeOfCharacterFromSet:cset];
        if (range.location == NSNotFound) {
            [arrSelectedCatIds addObject:sortFilObj.selectedCatIds];
        } else {
            [arrSelectedCatIds addObjectsFromArray:[NSMutableArray arrayWithArray:[sortFilObj.selectedCatIds componentsSeparatedByString:@","]]];
        }
    }
    
    
    if(sortFilObj.selectedCitiIds){
        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@","];
        NSRange range = [sortFilObj.selectedCitiIds rangeOfCharacterFromSet:cset];
        if (range.location == NSNotFound) {
            [arrSelectedCitiIds addObject:sortFilObj.selectedCitiIds];
        } else {
            [arrSelectedCitiIds addObjectsFromArray:[NSMutableArray arrayWithArray:[sortFilObj.selectedCitiIds componentsSeparatedByString:@","]]];
        }
    }
    
    if(sortFilObj.selectedOthersIds){
        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@","];
        NSRange range = [sortFilObj.selectedOthersIds rangeOfCharacterFromSet:cset];
        if (range.location == NSNotFound) {
            [arrSelectedOthersIds addObject:sortFilObj.selectedOthersIds];
        } else {
            [arrSelectedOthersIds addObjectsFromArray:[NSMutableArray arrayWithArray:[sortFilObj.selectedOthersIds componentsSeparatedByString:@","]]];
        }
    }
    
    if(sortFilObj.selectedInterestsIds){
        DLog(@"%@",sortFilObj.selectedInterestsIds);
        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@","];
        NSRange range = [sortFilObj.selectedInterestsIds rangeOfCharacterFromSet:cset];
        if (range.location == NSNotFound) {
            [arrSelectedInterestsIds addObject:sortFilObj.selectedInterestsIds];
        } else {
            [arrSelectedInterestsIds addObjectsFromArray:[NSMutableArray arrayWithArray:[sortFilObj.selectedInterestsIds componentsSeparatedByString:@","]]];
        }
        
    }
    
    if(sortFilObj.selectedOptIds){
        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@","];
        NSRange range = [sortFilObj.selectedOptIds rangeOfCharacterFromSet:cset];
        if (range.location == NSNotFound) {
            [arrSelectedOptionsIds addObject:sortFilObj.selectedOptIds];
        } else {
            [arrSelectedOptionsIds addObjectsFromArray:[NSMutableArray arrayWithArray:[sortFilObj.selectedOptIds componentsSeparatedByString:@","]]];
        }
    }
    
    
    distanceSelected = sortFilObj.distanceSelected;
    localSpecialSelected = sortFilObj.localSpecialSelected;
    eventDateSelected = sortFilObj.eventDateSelected;
    sortEventDateSelected =sortFilObj.sortEventDateSelected;
    alphabeticallySelected = sortFilObj.alphabeticallySelected;
    bandSelected = sortFilObj.bandSelected;
    venueSelected = sortFilObj.venueSelected;
    
    DLog(@"%d",distanceSelected);
    
    arrSelectedCatNames = [[NSMutableArray alloc]init];
    
    arrSelectedCitiNames = [[NSMutableArray alloc]init];
    
    arrSelectedOptionsNames = [[NSMutableArray alloc]init];
    
    arrSelectedInterestsNames = [[NSMutableArray alloc]init];
    
    arrSelectedOthersNames = [[NSMutableArray alloc]init];
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    // [self.view addSubview:self.activityIndicator];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.view addSubview:activityIndicator];
    activityIndicator.hidden = YES;
    [swipeTV reloadData];
    [swipeTV layoutIfNeeded];
}


-(void)cancelButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}





-(void)getSubCategory
{
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_USERID] ,[defaults valueForKey:KEY_HUBCITIID]];
    if(catId)
     [reqStr appendFormat:@"<catId>%@</catId>",catId];
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length])
    {
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    else if([_couponsPostalCodeSort length]){
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", _couponsPostalCodeSort];
    }
    
    if([defaults valueForKey:KEY_MITEMID]){
        [reqStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    
    else if ([module isEqualToString:@"thislocation"])
    {
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[NearbyBottomButtonID objectAtIndex:[NearbyBottomButtonID count]-1]];
    }
    else if ([module isEqualToString:@"Find Single"])
    {
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindSingleBottomButtonID objectAtIndex:[FindSingleBottomButtonID count]-1]];
    }
    else if ([module isEqualToString:@"CitiEXP"])
    {
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[CityExperienceBottomButtonID  objectAtIndex:[CityExperienceBottomButtonID count]-1]];
    }
    
    else if ([module isEqualToString:@"Events"])
    {
        if ([EventsBottomButtonID count]>=1){
            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[EventsBottomButtonID objectAtIndex:[EventsBottomButtonID count]-1]];
        }
        else
            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    else if ([module isEqualToString:@"Find All"]) {
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
    }
    else{
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    if([module isEqualToString:@"Find Single"] || [module isEqualToString:@"Find All"])
    {
        if (![srchKey isEqualToString:@""]) {
            if (srchKey) {
                [reqStr appendFormat:@"<srchKey>%@</srchKey>",srchKey];
            }
        }
    }
    
    
    [reqStr appendFormat:@"</UserDetails>"];
    
    // NSString *urlString = [NSString stringWithFormat:@"http://sdw2107:9990/HubCiti_Claim/find/getsubcategory"];
    NSString *urlString = [NSString stringWithFormat:@"%@find/getsubcategory",BASE_URL];
    
    
    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:reqStr];
    [self parseDinningCat:responseXML];
    
    
    ReleaseAndNilify(reqStr);
    
}




////////////////// Changes on 23rd may////////////////
-(void)parseDinningCat:(NSString*)response
{
    
    arrCategoryNames = [[NSMutableArray alloc]init];
    arrCategoryId = [[NSMutableArray alloc]init];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        TBXMLElement *listCatDetailsElement = [TBXML childElementNamed:@"listCatDetails" parentElement:tbxml.rootXMLElement];
        if(listCatDetailsElement)
        {
            TBXMLElement *CategoryDetailsElement = [TBXML childElementNamed:@"CategoryDetails" parentElement:listCatDetailsElement];
            while (CategoryDetailsElement)
            {
                
                TBXMLElement *catIdElement = [TBXML childElementNamed:@"catId" parentElement:CategoryDetailsElement];
                TBXMLElement *catNameElement = [TBXML childElementNamed:@"catName" parentElement:CategoryDetailsElement];
                if(catIdElement)
                    [arrCategoryId addObject:[TBXML textForElement:catIdElement]];
                if(catNameElement)
                    [arrCategoryNames addObject:[TBXML textForElement:catNameElement]];
                
                CategoryDetailsElement = [TBXML nextSiblingNamed:@"CategoryDetails" searchFromElement:CategoryDetailsElement];
            }
            
        }
        if ([arrCategoryNames count]!=0) {
            categoryDic = [[NSMutableDictionary alloc] initWithObjects:arrCategoryId forKeys:arrCategoryNames];
            
            NSLog(@"categoryDic %@", categoryDic);
        }
        
    }
    
}




-(void)getCategoryList{
    //    <Filter>
    //    <userId>66</userId> *
    //    <hubCitiId>93</hubCitiId> *
    //    <cityExpId>2133</cityExpId>
    //    //	 <searchKey></searchKey>
    //    //	<latitude></latitude>
    //    //	<longitude></longitude>
    //    //     <cityId></cityId>
    //    <moduleName>cityexp</moduleName> //cityexp is constant
    //    </Filter>
    
    
    //    <Filter>
    //    <userId>6</userId> *
    //    <hubCitiId>1143</hubCitiId> *
    //    <filterId>1130</filterId>
    //    //	<zipCode><zipCode>
    //    //	<searchKey></searchKey>
    //    //	<latitude></latitude>
    //    //	<longitude></longitude>
    //    <moduleName>filter</moduleName> //filter is constant
    //    </Filter>
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    
    [xmlStr appendFormat:@"<Filter><hubCitiId>%@</hubCitiId><userId>%@</userId><moduleName>%@</moduleName>", [defaults  valueForKey:KEY_HUBCITIID],[defaults  valueForKey:KEY_USERID],module];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [xmlStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [xmlStr appendFormat:@"<zipCode><![CDATA[%@]]></zipCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    else if([_couponsPostalCodeSort length]){
        [xmlStr appendFormat:@"<zipCode><![CDATA[%@]]></zipCode>", _couponsPostalCodeSort];
    }
    if (![srchKey isEqualToString:@""]) {
        if (srchKey) {
            [xmlStr appendFormat:@"<searchKey>%@</searchKey>",srchKey];
        }
    }
    if ([module isEqualToString:@"Band"]) {//Find All , Find Single , Events, CitiEXP,SubMenu
        
        if (![srchKeyBand isEqualToString:@""]) {
            if (srchKeyBand) {
                [xmlStr appendFormat:@"<searchKey>%@</searchKey>",srchKeyBand];
            }
        }
    }
    if([module isEqualToString:@"BandEvents"]){
        if(bandId)
        {
            [xmlStr appendFormat:@"<bandID>%@</bandID>",bandId];
        }
        if([defaults valueForKey:@"FindRadius"])
        {
            [xmlStr appendFormat:@"<radius>%@</radius>",[defaults valueForKey:@"FindRadius"]];
            
        }
        else
        {
            [xmlStr appendFormat:@"<radius>%@</radius>",@"50"];
            
        }
    }
    if ([module isEqualToString:@"CitiEXP"]){
        [xmlStr appendFormat:@"<citiExpId>%@</citiExpId>",[defaults valueForKey:KEY_LINKID]];
    }
    
    //    else if ([module isEqualToString:@"filter"]){
    //        [xmlStr appendFormat:@"<filterId>%d</filterId>",10];
    //
    //
    //        if([defaults valueForKey:KEY_MITEMID]){
    //            [xmlStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    //
    //        else{
    //            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    //        }
    //
    //    }
    
    else if ([module isEqualToString:@"Events"]){
        if (fundraiserId) {
            [xmlStr appendFormat:@"<fundId>%@</fundId>",fundraiserId]; // changed from <fundRaisId> to <fundId>
        }
        
        else{
            if (retailId) {
                [xmlStr appendFormat:@"<retailId>%@</retailId>",retailId];
            }
            
            if (retailLocationId) {
                [xmlStr appendFormat:@"<retailLocationId>%@</retailLocationId>",retailLocationId];
            }
            
            
        }
    }
    else if ([module isEqualToString:@"Filter"]){
        [xmlStr appendFormat:@"<filterId>%d</filterId>",filterID];
    }
    
    
    
    if([defaults valueForKey:KEY_MITEMID]){
        [xmlStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    else if ([module isEqualToString:@"Events"])
    {
        if ([EventsBottomButtonID count]>=1){
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[EventsBottomButtonID objectAtIndex:[EventsBottomButtonID count]-1]];
        }
        else
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    
    else if ([module isEqualToString:@"thislocation"])
    {
        [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[NearbyBottomButtonID objectAtIndex:[NearbyBottomButtonID count]-1]];
    }
    
    else if ([module isEqualToString:@"Find Single"])
    {
        [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindSingleBottomButtonID objectAtIndex:[FindSingleBottomButtonID count]-1]];
    }
    else if ([module isEqualToString:@"CitiEXP"])
    {
        [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[CityExperienceBottomButtonID objectAtIndex:[CityExperienceBottomButtonID count]-1]];
    }
    else if ([module isEqualToString:@"Find All"]) {
        if([FindBottomButtonID count]>=1)
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
    }
    else{
        [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    [xmlStr appendFormat:@"</Filter>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/getcategorylist",BASE_URL];
    // NSString *urlString = [NSString stringWithFormat:@"http://sdw2107:9990/HubCiti2.5/find/getcategorylist"];
    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    [self parse_CategoryList:responseXML];
    ReleaseAndNilify(xmlStr);
    
    //    http://localhost:8080/HubCiti2.1/find/getcategorylist
    //
    
    
    
}


////////////////// Changes on 23rd may////////////////
-(void)parse_CategoryList:(NSString*)response{
    //    <Filter>
    //    <responseCode>10005</responseCode>
    //    <responseText>Success</responseText>
    //    <categoryList>
    //    <CategoryInfo>
    //    <busCatName>Dining</busCatName>
    //    <busCatId>11</busCatId>
    //    </CategoryInfo>
    //    <CategoryInfo>
    //    <busCatName>Grocery </busCatName>
    //    <busCatId>10</busCatId>
    //    </CategoryInfo>
    //    </categoryList>
    //    </Filter>
    
    arrCategoryNames = [[NSMutableArray alloc]init];
    arrCategoryId = [[NSMutableArray alloc]init];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        TBXMLElement *categoryListElement = [TBXML childElementNamed:@"categoryList" parentElement:tbxml.rootXMLElement];
        if(categoryListElement)
        {
            TBXMLElement *CategoryInfoElement = [TBXML childElementNamed:@"CategoryInfo" parentElement:categoryListElement];
            while (CategoryInfoElement)
            {
                
                TBXMLElement *catIdElement = [TBXML childElementNamed:@"busCatId" parentElement:CategoryInfoElement];
                TBXMLElement *catNameElement = [TBXML childElementNamed:@"busCatName" parentElement:CategoryInfoElement];
                if(catIdElement)
                    [arrCategoryId addObject:[TBXML textForElement:catIdElement]];
                
                if(catNameElement)
                    [arrCategoryNames addObject:[TBXML textForElement:catNameElement]];
                
                
                CategoryInfoElement = [TBXML nextSiblingNamed:@"CategoryInfo" searchFromElement:CategoryInfoElement];
            }
            
        }
        if ([arrCategoryNames count]!=0) {
            categoryDic = [[NSMutableDictionary alloc] initWithObjects:arrCategoryId forKeys:arrCategoryNames];
            
            NSLog(@"categoryDic %@", categoryDic);
        }
    }
}



-(void)getFilterList:(NSString *)inputFilter{
    /*
     http://localhost:8080/HubCiti1.6/find/getfilterlist
     
     <MenuItem>
     *<userId>66</userId>
     *<hubCitiId>93</hubCitiId>
     <latitude>30.57228</latitude>
     <longitude>-98.306992</longitude>
     *<catId>11</catId>
     <radius>100</radius>
     // <cityIds></cityIds>
     <mItemId>209</mItemId>
     <bottomBtnId>304</bottomBtnId>
     //<searchKey> </searchKey>
     <filterName>smoking</filterName>
     </MenuItem>
     */
    
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<MenuItem><hubCitiId>%@</hubCitiId><userId>%@</userId>", [defaults  valueForKey:KEY_HUBCITIID],[defaults  valueForKey:KEY_USERID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [xmlStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    
    
    
    if([defaults valueForKey:KEY_MITEMID]){
        [xmlStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    
    else{
        if ([module isEqualToString:@"Find All"]) {
            if ([FindBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"thislocation"]) {
            if ([NearbyBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[NearbyBottomButtonID objectAtIndex:[NearbyBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"Find Single"]) {
            if ([FindSingleBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindSingleBottomButtonID objectAtIndex:[FindSingleBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"CitiEXP"]) {
            if ([CityExperienceBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[CityExperienceBottomButtonID objectAtIndex:[CityExperienceBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"Events"]) {
            if ([EventsBottomButtonID count]>=1){
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[EventsBottomButtonID objectAtIndex:[EventsBottomButtonID count]-1]];
            }
            else
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
        else{
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
    }
    
    if (![srchKey isEqualToString:@""]) {
        if (srchKey) {
            [xmlStr appendFormat:@"<searchKey>%@</searchKey>",srchKey];
        }
    }
    
    [xmlStr appendFormat:@"<catId>%@</catId>",catId];
    
    [xmlStr appendFormat:@"<fName>%@</fName>",inputFilter];
    
    
    [xmlStr appendFormat:@"</MenuItem>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/getfilterlist",BASE_URL];
    
    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    [self parse_FilterOtherList:responseXML];
    ReleaseAndNilify(xmlStr);
    
    
}


-(void)parse_FilterOtherList:(NSString*)response{
    /*
     <Filter>
     <responseCode>10000</responseCode>
     <responseText>Success</responseText>
     <filterList>
     <Filter>
     <fValueId>1018</fValueId>
     <fValueName>asdfsd</fValueName>
     </Filter>
     <Filter>
     <fValueId>3</fValueId>
     <fValueName>Name</fValueName>
     </Filter>
     <Filter>
     <fValueId>1021</fValueId>
     <fValueName>scan)1</fValueName>
     </Filter>
     <Filter>
     <fValueId>1019</fValueId>
     <fValueName>scansee</fValueName>
     </Filter>
     <Filter>
     <fValueId>1020</fValueId>
     <fValueName>ScanSee_1</fValueName>
     </Filter>
     </filterList>
     </Filter>
     */
    
    
    
    
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
        
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        
        
        
        TBXMLElement *filterListElement = [TBXML childElementNamed:@"filterList" parentElement:tbxml.rootXMLElement];
        
        if(filterListElement)
        {
            arrOthersId = [[NSMutableArray alloc]init];
            arrOthersNames = [[NSMutableArray alloc]init];
            
            TBXMLElement *FilterElement = [TBXML childElementNamed:@"Filter" parentElement:filterListElement];
            
            while (FilterElement)
            {
                
                TBXMLElement *filterValueIdElement = [TBXML childElementNamed:@"fValueId" parentElement:FilterElement];
                TBXMLElement *filterValueNameElement = [TBXML childElementNamed:@"fValueName" parentElement:FilterElement];
                
                if(filterValueIdElement)
                    [arrOthersId addObject:[TBXML textForElement:filterValueIdElement]];
                
                if(filterValueNameElement){
                    [arrOthersNames addObject:[TBXML textForElement:filterValueNameElement]];
                }
                
                
                
                FilterElement = [TBXML nextSiblingNamed:@"Filter" searchFromElement:FilterElement];
                
            }
            NSMutableDictionary *temp = [[NSMutableDictionary alloc] initWithObjects:arrOthersId forKeys:arrOthersNames];
            [othersDic addEntriesFromDictionary:temp];
            
        }
        
        
    }
    
    else{
        arrOthersId = [[NSMutableArray alloc]init];
        arrOthersNames = [[NSMutableArray alloc]init];
    }
}



-(void)getInterestList{
    /*
     http://localhost:8080/HubCiti2.1/find/getinterestlist
     
     <Filter>
     <userId>3</userId> *
     <hubCitiId>4173</hubCitiId> *
     <fCategoryName>Dining</fCategoryName>
     //<latitude></latitude>
     //<longitude>234</longitude>
     <mItemId>23770</mItemId>
     //<bottomBtnId></bottomBtnId>
     //<bsubCatId></bsubCatId>
     <spType>singlefind</spType> * // singlefind constant,findoptions,cityexperience
     </Filter>
     */
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<Filter><hubCitiId>%@</hubCitiId><userId>%@</userId><spType>%@</spType>", [defaults  valueForKey:KEY_HUBCITIID],[defaults  valueForKey:KEY_USERID],module];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [xmlStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    
    if (![srchKey isEqualToString:@""]) {
        if (srchKey) {
            [xmlStr appendFormat:@"<searchKey>%@</searchKey>",srchKey];
        }
    }
    
    //Find All , Find Single , Events, CitiEXP,SubMenu
    
    if ([module isEqualToString:@"CitiEXP"]) {
        [xmlStr appendFormat:@"<citiExpId>%@</citiExpId>",cityExpId];
    }
    
    if([defaults valueForKey:KEY_MITEMID]){
        [xmlStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    
    else{
        if ([module isEqualToString:@"Find All"]) {
            if ([FindBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"thislocation"]) {
            if ([NearbyBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[NearbyBottomButtonID objectAtIndex:[NearbyBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"Find Single"]) {
            if ([FindSingleBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindSingleBottomButtonID objectAtIndex:[FindSingleBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"CitiEXP"]) {
            if ([CityExperienceBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[CityExperienceBottomButtonID objectAtIndex:[CityExperienceBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"Events"]) {
            if ([EventsBottomButtonID count]>=1){
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[EventsBottomButtonID objectAtIndex:[EventsBottomButtonID count]-1]];
            }
            else
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
        else{
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
        
    }
    
    if ([module isEqualToString:@"Find Single"] || [module isEqualToString:@"Find All"]) {
        if ([categoryName length] > 0) {
            [xmlStr appendFormat:@"<fCategoryName><![CDATA[%@]]></fCategoryName>",categoryName];
        }
        
        
    }
    
    [xmlStr appendFormat:@"</Filter>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/getinterestlist",BASE_URL];
    
    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    [self parse_InterstList:responseXML];
    ReleaseAndNilify(xmlStr);
    
}


-(void)parse_InterstList:(NSString*)response{
    
    /*
     <Filter>
     <responseCode>10000</responseCode>
     <responseText>Success</responseText>
     <filterList>
     <Filter>
     <filterId>1130</filterId>
     <filterName>Filter2Test1</filterName>
     </Filter>
     <Filter>
     <filterId>1131</filterId>
     <filterName>Filter2Test2</filterName>
     </Filter>
     </filterList>
     </Filter>
     */
    arrInterestsNames = [[NSMutableArray alloc]init];
    arrInterestsId = [[NSMutableArray alloc]init];
    
    
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        
        TBXMLElement *filterListElement = [TBXML childElementNamed:@"filterList" parentElement:tbxml.rootXMLElement];
        
        if(filterListElement)
        {
            
            
            TBXMLElement *FilterElement = [TBXML childElementNamed:@"Filter" parentElement:filterListElement];
            
            while (FilterElement)
            {
                
                
                
                TBXMLElement *filterIdElement = [TBXML childElementNamed:@"filterId" parentElement:FilterElement];
                TBXMLElement *filterNameElement = [TBXML childElementNamed:@"filterName" parentElement:FilterElement];
                
                if(filterIdElement)
                    [arrInterestsId addObject:[TBXML textForElement:filterIdElement]];
                
                if(filterNameElement){
                    [arrInterestsNames addObject:[TBXML textForElement:filterNameElement]];
                }
                
                
                
                FilterElement = [TBXML nextSiblingNamed:@"Filter" searchFromElement:FilterElement];
                
            }
            interestsDic = [[NSMutableDictionary alloc] initWithObjects:arrInterestsId forKeys:arrInterestsNames];
            
            
        }
    }
}


-(void)getOptionList{
    /*
     <Filter>
     <userId>66</userId> *
     <hubCitiId>93</hubCitiId> *
     // <cityId></cityId>
     <latitude> 30.57228</latitude>
     <longitude> -98.306992</longitude>
     <mItemId>23769</mItemId> *
     //<bottomBtnId></bottomBtnId>
     <radius>100</radius>
     // <searchKey></searchKey>
     <businessId>11</businessId>
     <spType> options</spType> * // options constant
     </Filter>
     */
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<Filter><hubCitiId>%@</hubCitiId><userId>%@</userId><spType>options</spType>", [defaults  valueForKey:KEY_HUBCITIID],[defaults  valueForKey:KEY_USERID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [xmlStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    
    
    //Find All , Find Single , Events, CitiEXP,SubMenu
    
    if([defaults valueForKey:KEY_MITEMID]){
        [xmlStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    
    else{
        if ([module isEqualToString:@"Find All"]) {
            if ([FindBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"thislocation"]) {
            if ([NearbyBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[NearbyBottomButtonID objectAtIndex:[NearbyBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"Find Single"]) {
            if ([FindSingleBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindSingleBottomButtonID objectAtIndex:[FindSingleBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"CitiEXP"]) {
            if ([CityExperienceBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[CityExperienceBottomButtonID objectAtIndex:[CityExperienceBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"Events"]) {
            if ([EventsBottomButtonID count]>=1){
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[EventsBottomButtonID objectAtIndex:[EventsBottomButtonID count]-1]];
            }
            else
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
        else{
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
    }
    
    if (![srchKey isEqualToString:@""]) {
        if (srchKey) {
            [xmlStr appendFormat:@"<searchKey>%@</searchKey>",srchKey];
        }
    }
    
    [xmlStr appendFormat:@"<businessId>%@</businessId>",catId];
    
    if([defaults valueForKey:@"FindRadius"])
    {
        
        [xmlStr appendFormat:@"<radius>%@</radius>",[defaults valueForKey:@"FindRadius"]];
        
        
    }
    else
    {
        [xmlStr appendFormat:@"<radius>50</radius>"];
        
    }
    
    
    [xmlStr appendFormat:@"</Filter>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/getoptionlist",BASE_URL];
    
    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    [self parse_OptionList:responseXML];
    ReleaseAndNilify(xmlStr);
    
}


-(void)parse_OptionList:(NSString*)response{
    
    /*
     <Filter>
     <responseCode>10000</responseCode>
     <responseText>Success</responseText>
     <filterList>
     <Filter>
     <filterId>1130</filterId>
     <filterName>Filter2Test1</filterName>
     </Filter>
     <Filter>
     <filterId>1131</filterId>
     <filterName>Filter2Test2</filterName>
     </Filter>
     </filterList>
     </Filter>
     */
    
    arrOptionsNames = [[NSMutableArray alloc]init];
    arrOptionsId = [[NSMutableArray alloc]init];
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        
        
        
        TBXMLElement *filterListElement = [TBXML childElementNamed:@"filterList" parentElement:tbxml.rootXMLElement];
        
        if(filterListElement)
        {
            
            //            NSMutableArray *filterNames = [[NSMutableArray alloc]init];
            //            NSMutableArray *filterIds = [[NSMutableArray alloc]init];
            
            TBXMLElement *FilterElement = [TBXML childElementNamed:@"Filter" parentElement:filterListElement];
            
            while (FilterElement)
            {
                
                
                
                TBXMLElement *filterIdElement = [TBXML childElementNamed:@"filterId" parentElement:FilterElement];
                TBXMLElement *filterNameElement = [TBXML childElementNamed:@"filterName" parentElement:FilterElement];
                
                if(filterIdElement)
                    [arrOptionsId addObject:[TBXML textForElement:filterIdElement]];
                
                if(filterNameElement){
                    [arrOptionsNames addObject:[TBXML textForElement:filterNameElement]];
                }
                
                
                FilterElement = [TBXML nextSiblingNamed:@"Filter" searchFromElement:FilterElement];
                
            }
            optionsDic = [[NSMutableDictionary alloc] initWithObjects:arrOptionsId forKeys:arrOptionsNames];
            
            
        }
        
        
    }
    
    
    
    
    
}


-(void)fetchCityPreference{
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    
    //       <catName>Dining</catName>
    //<srchKey></srchKey> // Input can be NULL.
    //<catIds>0</catIds> //Input can be NULL.
    [xmlStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId><userId>%@</userId><module>%@</module>", [defaults  valueForKey:KEY_HUBCITIID],[defaults  valueForKey:KEY_USERID],module];
    
    if (![categoryName isEqualToString:@""] && categoryName) {
        [xmlStr appendFormat:@"<catName>%@</catName>",categoryName];
    }
    if (![srchKey isEqualToString:@""]){
        if (srchKey) {
            [xmlStr appendFormat:@"<srchKey>%@</srchKey>",srchKey];
        }
        
    }
    
    if ([module isEqualToString:@"Events"]){
        if(isRetailerEvent == TRUE)
        {
            [xmlStr appendFormat:@"<retailId>%@</retailId><retailLocationId>%@</retailLocationId>",retailId,retailLocationId];
        }
    }
    if ([module isEqualToString:@"CitiEXP"]) {
        [xmlStr appendFormat:@"<citiExpId>%@</citiExpId>",cityExpId];
    }
    
    if([defaults valueForKey:KEY_MITEMID]){
        [xmlStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    
    else{
        if ([module isEqualToString:@"Find All"]) {
            if ([FindBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"Events"])
        {
            if ([EventsBottomButtonID count]>=1){
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[EventsBottomButtonID objectAtIndex:[EventsBottomButtonID count]-1]];
            }
            else
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
        else if ([module isEqualToString:@"thislocation"])
        {
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[NearbyBottomButtonID objectAtIndex:[NearbyBottomButtonID count]-1]];
        }
        else if ([module isEqualToString:@"Find Single"])
        {
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindSingleBottomButtonID objectAtIndex:[FindSingleBottomButtonID count]-1]];
        }
        
        
        else if ([module isEqualToString:@"CitiEXP"])
        {
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[CityExperienceBottomButtonID objectAtIndex:[CityExperienceBottomButtonID count]-1]];
        }
        
        else{
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
    }
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [xmlStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        [xmlStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    else if([_couponsPostalCodeSort length]){
        [xmlStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", _couponsPostalCodeSort];
    }
    //    [xmlStr appendFormat:@"<subCatId>%@</subCatId>",[defaults valueForKey:@"SubCategoryId"]];
    
    if([defaults valueForKey:@"FindRadius"])
    {
        
        [xmlStr appendFormat:@"<radius>%@</radius>",[defaults valueForKey:@"FindRadius"]];
        
        
    }
    else
    {
        [xmlStr appendFormat:@"<radius>50</radius>"];
        
    }
    
    
    [xmlStr appendFormat:@"</UserDetails>"];
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercitypref",BASE_URL];
    
    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    [self parseCityPreference:responseXML];
    ReleaseAndNilify(xmlStr);
    
}

-(void)parseCityPreference:(NSString *)responseXml{
    
    
    
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
    {
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        arrCitiesNames =[[NSMutableArray alloc]init];
        arrCitiId =[[NSMutableArray alloc]init];
        NSMutableArray *prevSelectedIds =[[NSMutableArray alloc]initWithArray:arrSelectedCitiIds];
        BOOL flag =NO;
        if ([prevSelectedIds count]) {
            flag = YES;
        }
        
        TBXMLElement *cityListElement = [TBXML childElementNamed:@"cityList" parentElement:tbXml.rootXMLElement];
        if(cityListElement)
        {
            cityFlag = 1;
            TBXMLElement *CityElement = [TBXML childElementNamed:@"City" parentElement:cityListElement];
            while (CityElement)
            {
                
                TBXMLElement *cityIdElement = [TBXML childElementNamed:@"cityId" parentElement:CityElement];
                TBXMLElement *cityNameElement = [TBXML childElementNamed:@"cityName" parentElement:CityElement];
                TBXMLElement *isCityElement = [TBXML childElementNamed:@"isCityChecked" parentElement:CityElement];
                
                if (cityIdElement) {
                    
                    [arrCitiId addObject:[TBXML textForElement:cityIdElement]];
                    
                }
                
                if (cityNameElement) {
                    
                    [arrCitiesNames addObject:[TBXML textForElement:cityNameElement]];
                    
                }
                
                if (isCityElement) {
                    if ([[TBXML textForElement:isCityElement] isEqualToString:@"1"]) {
                        if (!flag) {
                            [arrSelectedCitiIds addObject:[TBXML textForElement:cityIdElement]];
                            [arrSelectedCitiNames addObject:[TBXML textForElement:cityNameElement]];
                        }
                        
                        
                    }
                    
                }
                CityElement = [TBXML nextSiblingNamed:@"City" searchFromElement:CityElement];
            }
        }
        
        
        if ([arrCitiesNames count]!=0) {
            citiesDic = [[NSMutableDictionary alloc] initWithObjects:arrCitiId forKeys:arrCitiesNames];
        }
        
        
    }
    
}



-(void) viewWillAppear:(BOOL)animated{
    DLog(@"10");
    
    [super viewWillAppear:YES];
    [self getSortFilterList];
    
    
    //  [swipeTV reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    //    NSIndexPath *indexPath = [NSIndexPath indexPathWithIndex:1];
    //    [self expandSubrowsAtIndexPath:indexPath forCell:tCell];
    NSLog(@"viewdidappear");
    [super viewDidAppear:YES];
    //  [swipeTV reloadData];
    
    
    
}

- (void)enableUserInteraction {
    self.view.userInteractionEnabled = YES;
}

-(void)getSortFilterList{
    
    iWebRequestState = SORT_FILTER;
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><hubCitiId>%d</hubCitiId><userId>%d</userId><module>%@</module>", 2162,10533,module];
    //    [xmlStr appendFormat:@"<longitude>%f</longitude><latitude>%f</latitude>",77.57230327,12.92064139];
    [xmlStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId><userId>%@</userId><module>%@</module>", [defaults  valueForKey:KEY_HUBCITIID],[defaults  valueForKey:KEY_USERID],module];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [xmlStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    
    else{
        if ([module isEqualToString:@"CitiEXP"] || [module isEqualToString:@"Events"] || [module isEqualToString:@"Filter"]) {
            
            if ([[defaults  valueForKey:KEYZIPCODE]length]){
                
                [xmlStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
            }
        }
        else if ([module isEqualToString:@"Coupons"]||[module isEqualToString:@"myaccounts"]){
            
            if ([[defaults  valueForKey:KEYZIPCODE]length]){
                
                [xmlStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
            }
            else if([_couponsPostalCodeSort length]){
                [xmlStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", _couponsPostalCodeSort];
            }
        }
    }
    
    if ([module isEqualToString:@"Filter"]) {
        [xmlStr appendFormat:@"<filterId>%d</filterId>", filterID];
    }
    if ([module isEqualToString:@"Band"]) {//Find All , Find Single , Events, CitiEXP,SubMenu
        
        if (![srchKeyBand isEqualToString:@""]) {
            if (srchKeyBand) {
                [xmlStr appendFormat:@"<srchKey>%@</srchKey>",srchKeyBand];
            }
        }
    }
    if([module isEqualToString:@"BandEvents"]){
        if(evtTypeID)
            [xmlStr appendFormat:@"<evtTypeID>%@</evtTypeID>",evtTypeID];
        else if(bandId)
            [xmlStr appendFormat:@"<bandID>%@</bandID>",bandId];
    }
    if ([module isEqualToString:@"Find Single"] || [module isEqualToString:@"Find All"]) {//Find All , Find Single , Events, CitiEXP,SubMenu
        
        if (![srchKey isEqualToString:@""]) {
            if (srchKey) {
                [xmlStr appendFormat:@"<srchKey>%@</srchKey>",srchKey];
            }
        }
        
        
        if (![categoryName isEqualToString:@""]) {
            [xmlStr appendFormat:@"<catName>%@</catName>",categoryName];
        }
        
    }
    if ([module isEqualToString:@"Coupons"])
    {
        if (![srchKey isEqualToString:@"Coupons"])
        {
            if (srchKey)
            {
                [xmlStr appendFormat:@"<srchKey>%@</srchKey>",srchKey];
            }
        }
    }
    if ([module isEqualToString:@"myaccounts"])
    {
        if (![srchKey isEqualToString:@"myaccounts"])
        {
            if (srchKey)
            {
                [xmlStr appendFormat:@"<srchKey>%@</srchKey>",srchKey];
            }
        }
    }
    
    else if ([module isEqualToString:@"CitiEXP"]){
        [xmlStr appendFormat:@"<citiExpId>%@</citiExpId>",[defaults valueForKey:KEY_LINKID]];
    }
    
    else if ([module isEqualToString:@"SubMenu"]){
        [xmlStr appendFormat:@"<menuId>%@</menuId>",[defaults valueForKey:@"menuId"]];
    }
    
    //    if ([module isEqualToString:@"Find Single"] || [module isEqualToString:@"Find All"] || [module isEqualToString:@"Events"]) {
    
    if([defaults valueForKey:KEY_MITEMID]){
        [xmlStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    
    else{
        if ([module isEqualToString:@"Find All"]) {
            if ([FindBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"thislocation"]) {
            if ([NearbyBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[NearbyBottomButtonID objectAtIndex:[NearbyBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"Find Single"]) {
            if ([FindSingleBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindSingleBottomButtonID objectAtIndex:[FindSingleBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"CitiEXP"]) {
            if ([CityExperienceBottomButtonID count]>=1) {
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[CityExperienceBottomButtonID objectAtIndex:[CityExperienceBottomButtonID count]-1]];
            }
        }
        else if ([module isEqualToString:@"Events"]) {
            if ([EventsBottomButtonID count]>=1){
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[EventsBottomButtonID objectAtIndex:[EventsBottomButtonID count]-1]];
            }
            else
                [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
        else{
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
    }
    //    }
    
    [xmlStr appendFormat:@"</UserDetails>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/getsortfilterlist",BASE_URL];
    
    // NSString *urlString = [NSString stringWithFormat:@"http://10.10.221.27:9990/HubCiti_Toggle/find/getsortfilterlist"];
   
        [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    
    
    //    [self parseSortFilterList:responseXML];
    //    ReleaseAndNilify(xmlStr);
    
}


-(void)parseSortFilterList:(NSString*)response{
    
    [HubCitiAppDelegate removeActivityIndicator];
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        TBXMLElement *filterList = [TBXML childElementNamed:@"filterList" parentElement:tbXml.rootXMLElement];
        
        TBXMLElement *filterElement = [TBXML childElementNamed:@"Filter" parentElement:filterList];
        
        while(filterElement){
            TBXMLElement *fHeaderElement = [TBXML childElementNamed:@"fHeader" parentElement:filterElement];
            if (fHeaderElement) {
                [secHeaderData addObject:[TBXML textForElement:fHeaderElement]];
            }
            NSLog(@"Header %@", secHeaderData);
            TBXMLElement *filterListElement = [TBXML childElementNamed:@"filterList" parentElement:filterElement];
            if (filterListElement) {
                TBXMLElement *filterElement = [TBXML childElementNamed:@"Filter" parentElement:filterListElement];
                while (filterElement){
                    if ([[TBXML textForElement:fHeaderElement]isEqualToString:@"Sort Items by"]){
                        TBXMLElement *filterNameDist = [TBXML childElementNamed:@"filterName" parentElement:filterElement];
                        while (filterNameDist) {
                            [sortByItemArray addObject:[TBXML textForElement:filterNameDist]];
                            filterNameDist = [TBXML nextSiblingNamed:@"filterName" searchFromElement:filterNameDist];
                        }
                        
                    }
                    else if ([[TBXML textForElement:fHeaderElement]isEqualToString:@"Filter Items by"]){
                        TBXMLElement *filterNameCat = [TBXML childElementNamed:@"filterName" parentElement:filterElement];
                        
                        while (filterNameCat) {
                            [filterByItemArray addObject:[TBXML textForElement:filterNameCat]];
                            filterNameCat = [TBXML nextSiblingNamed:@"filterName" searchFromElement:filterNameCat];
                        }
                        
                    }
                    filterElement = [TBXML nextSiblingNamed:@"Filter" searchFromElement:filterElement];
                }
            }
            filterElement = [TBXML nextSiblingNamed:@"Filter" searchFromElement:filterElement];
        }
    }
    
    NSLog(@"SortEle %@", sortByItemArray);
    NSLog(@"FilterEle %@", filterByItemArray);
    
    for(int i=0;i<filterByItemArray.count;i++)
    {
        if([[filterByItemArray objectAtIndex:i] isEqualToString:@"Genre"])
        {
            self.view.userInteractionEnabled = NO;
        }
    }
    
    NSMutableArray *dataArr=[[NSMutableArray alloc] init];
    
    self.dataArray=dataArr;
    
    for(int i=0;i<[secHeaderData count];i++)
    {
        
        SGTableSection *sgsec=[SGTableSection alloc];
        
        sgsec.sectionName=[NSString stringWithFormat:@"%@",[secHeaderData objectAtIndex:i]];
        
        
        NSMutableArray *marr=[[NSMutableArray alloc] init];
        
        
        
        NSUInteger cnnt=0;
        NSArray *arrcnn=nil;
        if([secHeaderData count] == 1 && [sgsec.sectionName isEqualToString:@"Filter Items by"]){
            arrcnn=filterByItemArray;
        }
        else{
            
            if(i==0)
            {
                arrcnn=sortByItemArray;
                
            }
            else if(i==1)
            {
                arrcnn=filterByItemArray;
                
            }
        }
        cnnt=arrcnn.count;
        
        for(int i=0;i<cnnt;i++)
        {
            
            SGTableRow *sgrow=[SGTableRow alloc];
            sgrow.isRow=1;
            sgrow.rowName= [NSString stringWithFormat:@"%@",[arrcnn objectAtIndex:i]];
            
            [marr addObject:sgrow];
            
        }
        sgsec.rows=marr;
        
        
        
        [dataArr addObject:sgsec];
        
        
    }
    
    
    [swipeTV reloadData];
    
    if(filterByItemArray.count)
    {
        int i;
        for(i=0; i<filterByItemArray.count;i++)
        {
            if([[filterByItemArray objectAtIndex:i] isEqualToString:@"Genre"])
            {
                break;
            }
        }
        if([filterByItemArray containsObject:@"Genre"])
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:1];
            if(expandOnce)
            {
                if([[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isRow])
                {
                    [self expandSubrowsAtIndexPath:indexPath forCell:[swipeTV cellForRowAtIndexPath:indexPath]];
                    [swipeTV setContentOffset:CGPointZero animated:YES];
                    expandOnce = NO;
                    [self performSelector:@selector(enableUserInteraction) withObject:nil afterDelay:0.5f];
                }
            }
        }
    }
}


-(void)removeFromSuperview{
    [activityIndicator stopAnimating];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [dataArray count];
    
}

-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [NSString stringWithFormat:@" %@",[[dataArray objectAtIndex:section] sectionName]];
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    float labelFont = 15.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        labelFont = 18.0;
    }
    UILabel *headerName = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 315, 26)];
    headerName.backgroundColor = [UIColor grayColor];
    headerName.font = [UIFont boldSystemFontOfSize:labelFont];
    headerName.text = [NSString stringWithFormat:@" %@",[[dataArray objectAtIndex:section] sectionName]];
    return headerName;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[[dataArray objectAtIndex:section] rows] count];
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 44.0;
    }
    else
    {
        return 70.0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 26.0;
    }
    else
    {
        return 36.0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    /////////////////////
    for(id vw in cell.contentView.subviews)
        [(UIView*)vw removeFromSuperview];
    
    
    /////////////////////
    
    [cell setBackgroundColor:[UIColor whiteColor]];
    
    
    if (tableView == swipeTV)
    {
        
        if([[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isRow]){
            // cell.textLabel.text = [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName];
            
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                
                lblRow= [[UILabel alloc]initWithFrame:CGRectMake(10, 5, SCREEN_WIDTH/2 - 30, 40)];
                [lblRow setFont:[UIFont boldSystemFontOfSize:13]];
            }
            else
            {
                lblRow= [[UILabel alloc]initWithFrame:CGRectMake(10, 18, SCREEN_WIDTH - 20, 40)];
                [lblRow setFont:[UIFont boldSystemFontOfSize:18]];
            }
            
            [lblRow setText:[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]];
            
            [lblRow setTextColor:[UIColor blackColor]];
            [lblRow setBackgroundColor:[UIColor clearColor]];
            [lblRow setFont:[UIFont boldSystemFontOfSize:13]];
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            expandRowBtn  = [UIButton buttonWithType:UIButtonTypeSystem];
            [expandRowBtn setTintColor:[UIColor blackColor]];
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                expandRowBtn.frame = CGRectMake(swipeTV.frame.size.width-35,15, 20, 20);
            }
            else{
                expandRowBtn.frame = CGRectMake(swipeTV.frame.size.width-35,20, 30, 30);
            }
            [expandRowBtn setBackgroundColor:[UIColor clearColor]];
            [expandRowBtn addTarget:self action:@selector(clickOnTableViewRow:)forControlEvents:UIControlEventTouchUpInside];
            
            NSString *titleForHeader=[self tableView:tableView titleForHeaderInSection:indexPath.section];
            if([titleForHeader isEqualToString:@" Filter Items by"]){
                if(!(indexPath.row==0 && ([[filterByItemArray objectAtIndex:0]isEqualToString:@"Local Specials !"] || [[filterByItemArray objectAtIndex:0]isEqualToString:@"Event Date"] || [[filterByItemArray objectAtIndex:0]isEqualToString:@"Date"]))){
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    if(![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isRowExpanded]){
                        //                    caratImage =[[UIImageView alloc]init];
                        //                    [caratImage setImage:[UIImage imageNamed:@"carat"]];
                        [expandRowBtn setImage:[UIImage imageNamed:@"carat.png"] forState:UIControlStateNormal];
                    }
                    else {
                        // [caratImage setImage:[UIImage imageNamed:@"carat-open"]];
                        [expandRowBtn setImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateNormal];
                    }
                    [cell.contentView addSubview:expandRowBtn];
                }
                
                
                else{
                    if(localSpecialSelected == YES || eventDateSelected ==YES){
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    }
                    else{
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                }
            }
            //            if( [[dataArray objectAtIndex:indexPath.section] rows].count)
            //            {
            //            if(indexPath.section == 1 && ![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isSubrowsLoaded])
            //            {
            //                [self expandSubrowsAtIndexPath:indexPath forCell:cell];
            //            }
            //            }
            if([titleForHeader isEqualToString:@" Sort Items by"]){
                if([module isEqualToString:@"BandEvents"])
                {
                    if(indexPath.row == 0 && [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]isEqualToString:@"Date"]){
                        if(sortEventDateSelected == YES){
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        }
                        else{
                            cell.accessoryType = UITableViewCellAccessoryNone;
                        }
                    }
                    else if (([[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]isEqualToString:@"Mileage"])) {
                        if(distanceSelected == YES){
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        }
                        else{
                            cell.accessoryType = UITableViewCellAccessoryNone;
                        }
                        
                    }
                    else if ([[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]isEqualToString:@"Band"])
                    {
                        if(bandSelected == YES)
                        {
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        }
                        else
                        {
                            cell.accessoryType = UITableViewCellAccessoryNone;
                        }
                    }
                    else if ([[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]isEqualToString:@"Venue"])
                    {
                        if(venueSelected == YES)
                        {
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        }
                        else
                        {
                            cell.accessoryType = UITableViewCellAccessoryNone;
                        }
                    }
                    
                }
                else if ([module isEqualToString:@"Events"]) {
                    if(indexPath.row == 0){
                        if(sortEventDateSelected == YES){
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        }
                        else{
                            cell.accessoryType = UITableViewCellAccessoryNone;
                        }
                    }
                    
                    else if (indexPath.row ==1) {
                        if(distanceSelected == YES){
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        }
                        else{
                            cell.accessoryType = UITableViewCellAccessoryNone;
                        }
                        
                    }
                    
                    
                    
                    else{                                                              // @Bindu Sort by alphabet
                        if(alphabeticallySelected == YES){
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        }
                        else{
                            cell.accessoryType = UITableViewCellAccessoryNone;
                        }
                    }
                }
                else if([module isEqualToString:@"Band"]){
                    if(alphabeticallySelected == YES){
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    }
                    else{
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                }
                else{
                    if(indexPath.row ==0){
                        if(distanceSelected == YES){
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        }
                        else{
                            cell.accessoryType = UITableViewCellAccessoryNone;
                        }
                    }
                    else{                                                               // @Bindu Sort by alphabet
                        if(alphabeticallySelected == YES){
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        }
                        else{
                            cell.accessoryType = UITableViewCellAccessoryNone;
                        }
                    }
                }
                
            }
            
            
            
            expandRowBtn.tag=10;
            
            // caratImage.frame = CGRectMake(10, 10, 20, 20);
            
            // [caratImage setBackgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:lblRow];
            //  [expandRowBtn addSubview:caratImage];
            
        }
        else{
            
            NSString *titleForHeader=[self tableView:tableView titleForHeaderInSection:indexPath.section];
            if([titleForHeader isEqualToString:@" Filter Items by"]){
                cell.accessoryType =UITableViewCellAccessoryNone;
                UILabel *lblSubRow;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    
                    lblSubRow= [[UILabel alloc]initWithFrame:CGRectMake(35, 5, self.view.frame.size.width - 20, 40)];
                    [lblSubRow setFont:[UIFont boldSystemFontOfSize:13]];
                }
                else
                {
                    lblSubRow= [[UILabel alloc]initWithFrame:CGRectMake(55, 20, self.view.frame.size.width - 20, 40)];
                    [lblSubRow setFont:[UIFont boldSystemFontOfSize:18]];
                };
                
                [lblSubRow setText:[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] subRowName]];
                
                [lblSubRow setFont:[UIFont boldSystemFontOfSize:13]];
                [cell.contentView addSubview:lblSubRow];
                
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
                
                //UIButton *checkBtn ;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    checkBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 15, 20, 20)];//(0, 0, 20, 45)
                }
                else{
                    checkBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 20, 40, 40)];
                }
                checkBtn.tag = indexPath.row;
                [checkBtn addTarget:self action:@selector(didSubRowClick:)forControlEvents:UIControlEventTouchUpInside];
                [checkBtn setBackgroundColor:[UIColor clearColor]];
                
                
                BOOL issel=   [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isSelected];
                
                BOOL isfstsel=   [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isFirstRowSelected];
                
                
                SGTableSubRow *sRow= [[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row];
                if([[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isFirstRow])
                {
                    
                    sRow.subRowsCount=0;
                }
                else
                {
                    SGTableSubRow *sRow1=  [[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)];
                    sRow.subRowsCount=sRow1.subRowsCount+1;
                }
                
                
                if (issel == TRUE || isfstsel==TRUE) {
                    
                    
                    [checkBtn setImage:[UIImage imageNamed:@"checkboxOn.png"] forState:UIControlStateNormal];
                    
                }
                else{
                    [checkBtn setImage:[UIImage imageNamed:@"checkboxOff.png"] forState:UIControlStateNormal];
                    
                }
                [cell.contentView addSubview:checkBtn];
            }
        }
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *tCell=[tableView cellForRowAtIndexPath:indexPath];
    NSString *titleForHeader=[self tableView:tableView titleForHeaderInSection:indexPath.section];
    
    
    if (tableView == swipeTV) {
        if([titleForHeader isEqualToString:@" Filter Items by"] && indexPath.row == 0 && [module isEqualToString:@"BandEvents"])
        {
            if ([[filterByItemArray objectAtIndex:0]isEqualToString:@"Date"]){
                
                if(tCell.accessoryType == UITableViewCellAccessoryCheckmark){
                    tCell.accessoryType = UITableViewCellAccessoryNone;
                    eventDateSelected =NO;
                    sortFilObj.evtDate = nil;
                }
                else{
                    eventDateSelected =YES;
                    [self showDatePicker];
                    // tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                
            }
            [swipeTV reloadData];
        }
        
        
        
        if ([titleForHeader isEqualToString:@" Filter Items by"] &&  indexPath.row==0) {
            
            if ([[filterByItemArray objectAtIndex:0]isEqualToString:@"Local Specials !"]) {
                
                if([tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryCheckmark){
                    tCell.accessoryType = UITableViewCellAccessoryNone;
                    localSpecialSelected =NO;
                }
                else{
                    localSpecialSelected =YES;
                    tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
            }
            
            else if ([[filterByItemArray objectAtIndex:0]isEqualToString:@"Event Date"]){
                
                if(tCell.accessoryType == UITableViewCellAccessoryCheckmark){
                    tCell.accessoryType = UITableViewCellAccessoryNone;
                    eventDateSelected =NO;
                    sortFilObj.evtDate = nil;
                }
                else{
                    eventDateSelected =YES;
                    [self showDatePicker];
                    tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                
            }
            
            else{
                if([[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isRow])
                {
                    [self expandSubrowsAtIndexPath:indexPath forCell:tCell];
                }
                
                else
                {
                    [self selectSubRowsData : indexPath forCell:tCell];
                    
                }
            }
        }
        
        
        else if ([titleForHeader isEqualToString:@" Sort Items by"]){
            if([module isEqualToString:@"BandEvents"])
            {
                if([[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]isEqualToString:@"Mileage"]&& tCell.accessoryType == UITableViewCellAccessoryNone)
                {
                    distanceSelected = YES;
                    sortEventDateSelected =NO;
                    bandSelected = NO;
                    alphabeticallySelected = NO;
                    venueSelected = NO;
                    
                    tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                if([[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]isEqualToString:@"Date"]&& tCell.accessoryType == UITableViewCellAccessoryNone)
                {
                    sortEventDateSelected =YES;
                    distanceSelected = NO;
                    alphabeticallySelected = NO;
                    bandSelected = NO;
                    venueSelected = NO;
                    
                    tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                if([[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]isEqualToString:@"Band"]&& tCell.accessoryType == UITableViewCellAccessoryNone)
                {
                    sortEventDateSelected =NO;
                    distanceSelected = NO;
                    alphabeticallySelected = NO;
                    bandSelected = YES;
                    venueSelected = NO;
                    tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                if([[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]isEqualToString:@"Venue"]&& tCell.accessoryType == UITableViewCellAccessoryNone)
                {
                    sortEventDateSelected =NO;
                    distanceSelected = NO;
                    alphabeticallySelected = NO;
                    bandSelected = NO;
                    venueSelected = YES;
                    tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                [swipeTV reloadData];
            }
            else if ([module isEqualToString:@"Events"]) {
                
                //                if (distanceSelected) {
                //                    tCell.accessoryType = UITableViewCellAccessoryNone;
                //                    distanceSelected =NO;
                //                }
                //
                //                else if (eventDateSelected){
                //                    tCell.accessoryType = UITableViewCellAccessoryNone;
                //                    eventDateSelected =NO;
                //                }
                //                else{
                // do implement here on ***** Alphabeticall sort******
                
                if (indexPath.row == 0 && tCell.accessoryType == UITableViewCellAccessoryNone) {
                    sortEventDateSelected =YES;
                    distanceSelected = NO;
                    alphabeticallySelected = NO;
                    tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                    
                }
                else if (indexPath.row == 1 && tCell.accessoryType == UITableViewCellAccessoryNone) {
                    distanceSelected = YES;
                    sortEventDateSelected =NO;
                    alphabeticallySelected = NO;
                    tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                    
                }
                else{
                    if (tCell.accessoryType == UITableViewCellAccessoryNone) {
                        alphabeticallySelected = YES;
                        sortEventDateSelected =NO;
                        distanceSelected = NO;
                        tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                    }
                    
                }
                
                //                }
                [swipeTV reloadData];
                
            }
            
            else{
                if (indexPath.row == 0) {
                    if (distanceSelected) {
                        if(tCell.accessoryType == UITableViewCellAccessoryCheckmark){
                            tCell.accessoryType = UITableViewCellAccessoryNone;
                            distanceSelected =NO;
                        }
                        //                    distanceSelected =NO;
                        //                    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
                        else{
                            distanceSelected =YES;
                            alphabeticallySelected = NO;                            // @Bindu Sort by alphabet
                            tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                        }
                    }
                    else{
                        //                    distanceSelected =YES;
                        //                    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
                        if(tCell.accessoryType == UITableViewCellAccessoryCheckmark){
                            tCell.accessoryType = UITableViewCellAccessoryNone;
                            distanceSelected =NO;
                        }
                        //                    distanceSelected =NO;
                        //                    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
                        else{
                            distanceSelected =YES;
                            alphabeticallySelected = NO;                                         // @Bindu Sort by alphabet
                            tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                        }
                    }
                }
                
                else{
                    if (alphabeticallySelected) {
                        if(tCell.accessoryType == UITableViewCellAccessoryCheckmark){
                            tCell.accessoryType = UITableViewCellAccessoryNone;
                            alphabeticallySelected =NO;
                        }
                        //                    distanceSelected =NO;
                        //                    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
                        else{
                            alphabeticallySelected =YES;
                            distanceSelected = NO;                            // @Bindu Sort by alphabet
                            tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                        }
                    }
                    else{
                        //                    distanceSelected =YES;
                        //                    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
                        if(tCell.accessoryType == UITableViewCellAccessoryCheckmark){
                            tCell.accessoryType = UITableViewCellAccessoryNone;
                            alphabeticallySelected =NO;
                        }
                        //                    distanceSelected =NO;
                        //                    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
                        else{
                            alphabeticallySelected =YES;
                            distanceSelected = NO;                                         // @Bindu Sort by alphabet
                            tCell.accessoryType = UITableViewCellAccessoryCheckmark;
                        }
                    }
                }
                [swipeTV reloadData];
            }
            
        }
        
        else
        {
            if([[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isRow])
            {
                [self expandSubrowsAtIndexPath:indexPath forCell:tCell];
            }
            
            else
            {
                [self selectSubRowsData : indexPath forCell:tCell];
                
            }
            
        }
    }
}

-(void)expandSubrowsAtIndexPath:(NSIndexPath*)indexPath forCell:(UITableViewCell*)tCell
{
    
    UIButton *caratvw=(UIButton*) [tCell.contentView viewWithTag:10 ];
    //[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsSubrowsLoaded:0];
    NSInteger indexPathIncrement=0;
    // rowIndexNum = indexPath.row;
    // selectedRowName = [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName];
    if([[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]isEqualToString:@"Category"] || [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName] isEqualToString:@"Genre"]){
        if(![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isSubrowsLoaded] )
        {
            ///make request
            if ([module isEqualToString:@"Find Single"] || [module isEqualToString:@"Find All"]) {
                [self getSubCategory];
            }
            
            else{
                [self getCategoryList];
            }
            //get response n add object to city array
            catArray = [[NSMutableArray alloc]initWithArray:arrCategoryNames];
            [self updateCategorySubRows:(int)indexPath.row:(int)indexPath.section];//call update subrow
            
            
            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsSubrowsLoaded:1];
            
            indexPathIncrement = indexPath.row+[catArray count];
            
            
            
        }
        
    }
    else if([[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]isEqualToString:@"Options"]){
        if(![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isSubrowsLoaded] )
        {
            ///make request
            //get response n add object to city array
            [self getOptionList];
            optArray = [[NSMutableArray alloc]initWithArray:arrOptionsNames];
            [self updateOptionsSubRows:(int)indexPath.row:(int)indexPath.section];//call update subrow
            
            
            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsSubrowsLoaded:1];
            
            indexPathIncrement = indexPath.row+[optArray count];
            
        }
        
    }
    else if([[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]isEqualToString:@"City"]){
        
        if(![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isSubrowsLoaded] )
        {
            ///make request
            //get response n add object to city array
            
            [self fetchCityPreference];
            
            cityArray = [[NSMutableArray alloc]initWithArray:arrCitiesNames];
            //            cityArray = [[NSMutableArray alloc]initWithObjects:@"test",@"abc", @"lmn", nil];
            [self updateCitySubRows:(int)indexPath.row:(int)indexPath.section];//call update subrow
            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsSubrowsLoaded:1];
            
            indexPathIncrement = indexPath.row+[cityArray count];
            
        }
        
    }
    else if([[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]isEqualToString:@"Interests"]){
        if(![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isSubrowsLoaded] )
        {
            ///make request
            //get response n add object to city array
            [self getInterestList];
            intArray =  [[NSMutableArray alloc]initWithArray:arrInterestsNames];
            [self updateInterestsSubRows:(int)indexPath.row:(int)indexPath.section];//call update subrow
            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsSubrowsLoaded:1];
            
            
            indexPathIncrement = indexPath.row+[intArray count];
            
        }
        
    }
    
    else{
        if(![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isSubrowsLoaded] )
        {
            [self getFilterList:[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]];
            
            
            othersArray = [[NSMutableArray alloc]initWithArray:arrOthersNames];
            
            [self updateFilterSubRows:(int)indexPath.row:(int)indexPath.section:[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]];//call update subrow
            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsSubrowsLoaded:1];
            
            
            indexPathIncrement = indexPath.row+[othersArray count];
            
        }
    }
    
    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] subrows] count];
    // subRowCount = cnt;
    NSRange range=NSMakeRange((indexPath.row+1), [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] subrows] count]);
    
    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
    @autoreleasepool {
        
        NSIndexSet *indexSet=[NSIndexSet indexSetWithIndexesInRange:range];
        
        for(int i=1;i<=cnt;i++)
        {
            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
            
            [mutarr addObject:indexPath1];
        }
        
        [swipeTV beginUpdates];
        if(! [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isRowExpanded])/////Add
        {
            [caratvw setImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateNormal];
            [[[dataArray objectAtIndex:indexPath.section] rows] insertObjects:[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] subrows] atIndexes:indexSet];
            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsRowExpanded:1];
            
            [swipeTV insertRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationTop];
        }
        else
        {
            [caratvw setImage:[UIImage imageNamed:@"carat.png"] forState:UIControlStateNormal];
            [[[dataArray objectAtIndex:indexPath.section] rows] removeObjectsAtIndexes:indexSet];
            [swipeTV deleteRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationTop];
            
            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsRowExpanded:0];
        }
        [swipeTV endUpdates];
    }
    
    [self makeTableViewCellsVisible:indexPathIncrement:indexPath];
    
    
}

-(void)makeTableViewCellsVisible :(NSInteger)increment :(NSIndexPath*)originalIndexPath
{
    NSIndexPath *selectedRowIndexPath = [NSIndexPath indexPathForRow:(increment) inSection:originalIndexPath.section];
    
    if ([[swipeTV visibleCells] containsObject:[swipeTV cellForRowAtIndexPath:selectedRowIndexPath]]==NO) {
        //        NSIndexPath *tempIndexPath = [NSIndexPath indexPathForRow:0 inSection:section];
        [swipeTV scrollToRowAtIndexPath:originalIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        
    }
    
}


-(void)selectSubRowsData:(NSIndexPath*)indexPath forCell:(UITableViewCell*)tCell{
    [self hidePicker];
    
    UIButton *caratvw1=(UIButton*) [tCell.contentView viewWithTag:indexPath.row];
    SGTableSubRow *sRow= [[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row];
    NSString *str = [[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow];
    NSLog(@"parentRow %@", str);
    if(![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isFirstRow])
    {
        
        if(![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isSelected])
        {
            [caratvw1 setImage:[UIImage imageNamed:@"checkboxOn.png"] forState:UIControlStateNormal];
            
            // [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsSelected:1];
            
            
            if ([catArray containsObject:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]) {
                if (![arrSelectedCatIds containsObject:[categoryDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]) {
                    //NSLog(@"categoryDic %@", categoryDic);
                    NSLog(@"arrSelectedCatIds %@", arrSelectedCatIds);
                    [arrSelectedCatIds addObject:[categoryDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    // NSLog(@"categoryDic %@", categoryDic);
                    NSLog(@"arrSelectedCatIds %@", arrSelectedCatIds);
                }
                [sRow setIsSelected:1];
                NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                int16_t fstrwno=indexPath.row-sRow.subRowsCount;
                if([arrSelectedCatIds count] == [catArray count]){
                    @autoreleasepool {
                        
                        NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:fstrwno inSection:indexPath.section];
                        
                        [mutarr addObject:indexPath1];
                        
                        [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsFirstRowSelected:1];
                        
                    }
                    
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
                
            }
            
            else if ([optArray containsObject:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]) {
                if (![arrSelectedOptionsIds containsObject:[optionsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]) {
                    NSLog(@"optionsDic %@", optionsDic);
                    NSLog(@"arrOptionsId %@", arrSelectedOptionsIds);
                    [arrSelectedOptionsIds addObject:[optionsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"optionsDic %@", optionsDic);
                    NSLog(@"arrSelectedOptionsIds %@", arrSelectedOptionsIds);
                }
                [sRow setIsSelected:1];
                NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                int16_t fstrwno=indexPath.row-sRow.subRowsCount;
                if([arrSelectedOptionsIds count] == [optArray count]){
                    @autoreleasepool {
                        
                        NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:fstrwno inSection:indexPath.section];
                        
                        [mutarr addObject:indexPath1];
                        
                        [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsFirstRowSelected:1];
                        
                    }
                    
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
                
            }
            
            else if ([cityArray containsObject:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]) {
                if (![arrSelectedCitiIds containsObject:[citiesDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]) {
                    NSLog(@"citiesDic %@", citiesDic);
                    NSLog(@"selectedCitiIds %@", arrSelectedCitiIds);
                    [arrSelectedCitiIds addObject:[citiesDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"citiesDic %@", citiesDic);
                    NSLog(@"selectedCitiIds %@", arrSelectedCitiIds);
                }
                [sRow setIsSelected:1];
                NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                int16_t fstrwno=indexPath.row-sRow.subRowsCount;
                if([arrSelectedCitiIds count] == [cityArray count]){
                    @autoreleasepool {
                        
                        NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:fstrwno inSection:indexPath.section];
                        
                        [mutarr addObject:indexPath1];
                        
                        [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsFirstRowSelected:1];
                        
                    }
                    
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
                
            }
            
            else if ([intArray containsObject:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]) {
                if (![arrSelectedInterestsIds containsObject:[interestsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]) {
                    NSLog(@"interestsDic %@", interestsDic);
                    NSLog(@"arrSelectedInterestsIds %@", arrSelectedInterestsIds);
                    [arrSelectedInterestsIds addObject:[interestsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"interestsDic %@", interestsDic);
                    NSLog(@"arrSelectedInterestsIds %@", arrSelectedInterestsIds);
                }
                [sRow setIsSelected:1];
                NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                int16_t fstrwno=indexPath.row-sRow.subRowsCount;
                if([arrSelectedInterestsIds count] == [intArray count]){
                    @autoreleasepool {
                        
                        NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:fstrwno inSection:indexPath.section];
                        
                        [mutarr addObject:indexPath1];
                        
                        [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsFirstRowSelected:1];
                        
                    }
                    
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
                
            }
            
            else {
                if (![arrSelectedOthersIds containsObject:[othersDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]) {
                    NSLog(@"othersDic %@", othersDic);
                    NSLog(@"arrSelectedOthersIds %@", arrSelectedOthersIds);
                    [arrSelectedOthersIds addObject:[othersDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    
                    NSMutableArray *tempSelectedIdArr = [[tempSelectedIdDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]]mutableCopy];
                    
                    NSMutableArray *tempArr = [tempOthersDisc valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]];
                    //                    if(![tempArr containsObject:@"All"]){
                    //                        [tempArr insertObject:@"All" atIndex:0];
                    //                    }
                    NSLog(@"othersDic %@", othersDic);
                    NSLog(@"arrSelectedOthersIds %@", arrSelectedOthersIds);
                    
                    [sRow setIsSelected:1];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    int16_t fstrwno=indexPath.row-sRow.subRowsCount;
                    
                    [tempSelectedIdArr addObject:[othersDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    
                    [tempSelectedIdDic setObject:tempSelectedIdArr forKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]];
                    
                    if([tempArr count] == [tempSelectedIdArr count]){
                        
                        @autoreleasepool {
                            
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:fstrwno inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsFirstRowSelected:1];
                            
                        }
                        
                        [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                        mutarr=nil;
                    }
                    
                    
                    
                }
                
            }
        }
        else
        {
            [caratvw1 setImage:[UIImage imageNamed:@"checkboxOff.png"] forState:UIControlStateNormal];
            
            if ([catArray containsObject:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]) {
                if ([arrSelectedCatIds containsObject:[categoryDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]) {
                    NSLog(@"categoryDic %@", categoryDic);
                    NSLog(@"arrSelectedCatIds %@", arrSelectedCatIds);
                    [arrSelectedCatIds removeObject:[categoryDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"categoryDic %@", categoryDic);
                    NSLog(@"arrSelectedCatIds %@", arrSelectedCatIds);
                }
                [sRow setIsSelected:0];
                NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                
                int16_t fstrwno=indexPath.row-sRow.subRowsCount;
                
                @autoreleasepool {
                    
                    NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:fstrwno inSection:indexPath.section];
                    
                    [mutarr addObject:indexPath1];
                    
                    [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsFirstRowSelected:0];
                    
                }
                
                [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                mutarr=nil;
                
            }
            
            else if ([optArray containsObject:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]) {
                if ([arrSelectedOptionsIds containsObject:[optionsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]) {
                    NSLog(@"optionsDic %@", optionsDic);
                    NSLog(@"arrOptionsId %@", arrSelectedOptionsIds);
                    [arrSelectedOptionsIds removeObject:[optionsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"optionsDic %@", optionsDic);
                    NSLog(@"arrSelectedOptionsIds %@", arrSelectedOptionsIds);
                    [sRow setIsSelected:0];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    int16_t fstrwno=indexPath.row-sRow.subRowsCount;
                    
                    @autoreleasepool {
                        
                        NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:fstrwno inSection:indexPath.section];
                        
                        [mutarr addObject:indexPath1];
                        
                        [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsFirstRowSelected:0];
                        
                    }
                    
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
            }
            
            else if ([cityArray containsObject:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]) {
                if ([arrSelectedCitiIds containsObject:[citiesDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]) {
                    NSLog(@"citiesDic %@", citiesDic);
                    NSLog(@"selectedCitiIds %@", arrSelectedCitiIds);
                    [arrSelectedCitiIds removeObject:[citiesDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    //[selectedCitiNames removeObject:[cityArray objectAtIndex:subRowNum]];
                    NSLog(@"arrSelectedCitiIds %@", arrSelectedCitiIds);
                    NSLog(@"citiesDic %@", citiesDic);
                    
                }
                [sRow setIsSelected:0];
                NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                
                int16_t fstrwno=indexPath.row-sRow.subRowsCount;
                
                @autoreleasepool {
                    
                    NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:fstrwno inSection:indexPath.section];
                    
                    [mutarr addObject:indexPath1];
                    
                    [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsFirstRowSelected:0];
                    
                }
                
                [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                mutarr=nil;
                
            }
            
            else if ([intArray containsObject:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]) {
                if ([arrSelectedInterestsIds containsObject:[interestsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]) {
                    NSLog(@"interestsDic %@", interestsDic);
                    NSLog(@"arrSelectedInterestsIds %@", arrSelectedInterestsIds);
                    [arrSelectedInterestsIds removeObject:[interestsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"interestsDic %@", interestsDic);
                    NSLog(@"arrSelectedInterestsIds %@", arrSelectedInterestsIds);
                }
                [sRow setIsSelected:0];
                NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                
                int16_t fstrwno=indexPath.row-sRow.subRowsCount;
                
                @autoreleasepool {
                    
                    NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:fstrwno inSection:indexPath.section];
                    
                    [mutarr addObject:indexPath1];
                    
                    [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsFirstRowSelected:0];
                    
                }
                
                [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                mutarr=nil;
            }
            
            else{
                if ([arrSelectedOthersIds containsObject:[othersDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]) {
                    NSLog(@"othersDic %@", othersDic);
                    NSLog(@"arrSelectedOthersIds %@", arrSelectedOthersIds);
                    [arrSelectedOthersIds removeObject:[othersDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"othersDic %@", othersDic);
                    NSLog(@"arrSelectedOthersIds %@", arrSelectedOthersIds);
                }
                [sRow setIsSelected:0];
                NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                
                int16_t fstrwno=indexPath.row-sRow.subRowsCount;
                
                DLog(@"tempSelectedIdDic%@",tempSelectedIdDic);
                
                //                if([[tempSelectedIdDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]] count]== [[tempOthersDisc valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]] count]){
                
                @autoreleasepool {
                    
                    NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:fstrwno inSection:indexPath.section];
                    
                    [mutarr addObject:indexPath1];
                    
                    [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsFirstRowSelected:0];
                    
                }
                
                [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                mutarr=nil;
                // }
                
                DLog(@"tempSelectedIdDic%@",tempSelectedIdDic);
                
                [[tempSelectedIdDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]] removeObject:[othersDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                
                
                         
            }
            
        }
        
    }
    
    
    else
    {
        BOOL selval=0;
        if(![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isFirstRowSelected])
        {
            [caratvw1 setImage:[UIImage imageNamed:@"checkboxOn.png"] forState:UIControlStateNormal];
            
            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsFirstRowSelected:1];
            
            selval=1;
            if ([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow] isEqualToString:@"Category"]|| [[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow] isEqualToString:@"Genre"]) {
                if([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]isEqualToString:@"All"])
                {
                    [arrSelectedCatIds removeAllObjects];
                    [arrSelectedCatIds addObjectsFromArray:arrCategoryId];
                    NSLog(@"arrSelectedCatIds %@", arrSelectedCatIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=1;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
                else if (![arrSelectedCatIds containsObject:[categoryDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]){
                    
                    
                    [arrSelectedCatIds addObject:[categoryDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"arrSelectedCatIds %@", arrSelectedCatIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=0;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
            }
            else if ([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow] isEqualToString:@"Options"]) {
                if([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]isEqualToString:@"All"])
                {
                    [arrSelectedOptionsIds removeAllObjects];
                    [arrSelectedOptionsIds addObjectsFromArray:arrOptionsId];
                    NSLog(@"arrSelectedOptionsIds %@", arrSelectedOptionsIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=1;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
                else if (![arrSelectedOptionsIds containsObject:[optionsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]){
                    
                    
                    [arrSelectedOptionsIds addObject:[optionsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"arrSelectedOptionsIds %@", arrSelectedOptionsIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=0;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
            }
            else if ([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow] isEqualToString:@"City"]) {
                if([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]isEqualToString:@"All"])
                {
                    
                    [arrSelectedCitiIds removeAllObjects];
                    [arrSelectedCitiIds addObjectsFromArray:arrCitiId];
                    NSLog(@"arrSelectedCitiIds %@", arrSelectedCitiIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=1;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
                else if (![arrSelectedCitiIds containsObject:[citiesDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]){
                    
                    
                    [arrSelectedCitiIds addObject:[citiesDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"arrSelectedCitiIds %@", arrSelectedCitiIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=0;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
            }
            
            else if ([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow] isEqualToString:@"Interests"]) {
                if([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]isEqualToString:@"All"])
                {
                    [arrSelectedInterestsIds removeAllObjects];
                    [arrSelectedInterestsIds addObjectsFromArray:arrInterestsId];
                    NSLog(@"arrSelectedInterestsIds %@", arrSelectedInterestsIds);
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=1;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
                else if (![arrSelectedInterestsIds containsObject:[interestsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]){
                    
                    
                    [arrSelectedInterestsIds addObject:[interestsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"arrSelectedInterestsIds %@", arrSelectedInterestsIds);
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=0;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
            }
            else{
                if([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]isEqualToString:@"All"])
                {
                    NSMutableArray *arr1 = [[tempOthersDisc valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]]mutableCopy];
                    //[arr removeObjectAtIndex:0];
                    
                    if (![arrSelectedOthersIds containsObject:[othersDic allValues]]) {
                        for (int i = 0; i<[arr1 count]; i++) {
                            if (![arrSelectedOthersIds containsObject:[arr1 objectAtIndex:i]]) {
                                [arrSelectedOthersIds addObject:[arr1 objectAtIndex:i]];           ////////// do add object
                            }
                        }
                        //arrSelectedOthersIds = arr1;
                        [tempSelectedIdDic setObject:arr1 forKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]];
                        NSLog(@"arrSelectedOthersIds %@", arrSelectedOthersIds);
                        NSLog(@"tempSelectedIdDic %@", tempSelectedIdDic);
                    }
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=1;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
                else if (![arrSelectedOthersIds containsObject:[othersDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]){
                    
                    
                    [arrSelectedOthersIds addObject:[othersDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSMutableArray *arr =[othersDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]];
                    
                    [tempSelectedIdDic setObject:arr forKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]];
                    
                    NSLog(@"arrSelectedOthersIds %@", arrSelectedOthersIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=0;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
            }
            
        }
        else
        {
            [caratvw1 setImage:[UIImage imageNamed:@"checkboxOff.png"] forState:UIControlStateNormal];
            
            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsFirstRowSelected:0];
            selval=0;
            if ([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow] isEqualToString:@"Category"]|| [[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow] isEqualToString:@"Genre"]) {
                if([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]isEqualToString:@"All"])
                {
                    [arrSelectedCatIds removeAllObjects];
                    NSLog(@"arrSelectedCatIds %@", arrSelectedCatIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=1;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
                else if ([arrSelectedCatIds containsObject:[categoryDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]){
                    [arrSelectedCatIds removeObject:[categoryDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"arrSelectedCatIds %@", arrSelectedCatIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=0;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
            }
            else if ([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow] isEqualToString:@"Options"]) {
                if([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]isEqualToString:@"All"])
                {
                    [arrSelectedOptionsIds removeAllObjects];
                    NSLog(@"arrSelectedOptionsIds %@", arrSelectedOptionsIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=1;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
                else if ([arrSelectedOptionsIds containsObject:[optionsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]){
                    [arrSelectedOptionsIds removeObject:[optionsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"arrSelectedOptionsIds %@", arrSelectedOptionsIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=0;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
            }
            else if ([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow] isEqualToString:@"City"]) {
                if([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]isEqualToString:@"All"])
                {
                    [arrSelectedCitiIds removeAllObjects];
                    NSLog(@"arrSelectedCitiIds %@", arrSelectedCitiIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=1;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
                else if ([arrSelectedCitiIds containsObject:[citiesDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]){
                    [arrSelectedCitiIds removeObject:[citiesDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"arrSelectedCitiIds %@", arrSelectedCitiIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=0;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
            }
            else if ([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow] isEqualToString:@"Interests"]) {
                if([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]isEqualToString:@"All"])
                {
                    [arrSelectedInterestsIds removeAllObjects];
                    NSLog(@"arrSelectedInterestsIds %@", arrSelectedInterestsIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=1;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
                else if ([arrSelectedInterestsIds containsObject:[interestsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]){
                    [arrSelectedInterestsIds removeObject:[interestsDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSLog(@"arrSelectedInterestsIds %@", arrSelectedInterestsIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=0;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
            }
            else{
                if([[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]isEqualToString:@"All"])
                {
                    BOOL containsIds =[self ContainsValue:[[tempOthersDisc valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]]mutableCopy]];
                    
                    DLog(@"tempOthersDisc%@",tempOthersDisc);
                    
                    if (containsIds) {
                        //
                        //                    }
                        //                    if ([arrSelectedOthersIds containsObject:[tempSelectedIdDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]]]) {
                        //                        [arrSelectedOthersIds removeObject:[tempSelectedIdDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]]];
                        
                        DLog(@"tempOthersDisc %@",tempOthersDisc);
                        DLog(@"selected %@",tempSelectedIdDic);
                        
                        NSMutableArray *arr =[[tempOthersDisc valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]]mutableCopy];
                        DLog(@"arr %p",arr);
                        
                        //                        arr =[tempOthersDisc valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]];
                        
                        [self removeIds:arr];
                        DLog(@"tempOthersDisc %p",tempOthersDisc);
                        DLog(@"selected %p",tempSelectedIdDic);
                        //                         [arrSelectedOthersIds removeObjectsInArray:arr];
                        
                        DLog(@"tempOthersDisc%@",tempOthersDisc);
                        
                        NSMutableArray *tempArr = [[NSMutableArray alloc]init];
                        
                        [tempSelectedIdDic setObject:tempArr forKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]];
                        
                        // [tempSelectedIdDic removeObjectForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]];
                        
                        
                        NSLog(@"arrSelectedOthersIds %@", arrSelectedOthersIds);
                        NSLog(@"tempSelectedIdDic %@", tempSelectedIdDic);
                    }
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=1;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                    
                }
                else if ([arrSelectedOthersIds containsObject:[othersDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]]){
                    [arrSelectedOthersIds removeObject:[othersDic valueForKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]subRowName]]];
                    NSMutableArray *arr = [[NSMutableArray alloc]init];
                    [tempSelectedIdDic setObject:arr forKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]];
                    NSLog(@"arrSelectedOthersIds %@", arrSelectedOthersIds);
                    
                    NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];
                    
                    @autoreleasepool {
                        for(int i=0;i<cnt;i++)
                        {
                            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                            
                            [mutarr addObject:indexPath1];
                            
                            [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                            
                        }
                    }
                    [swipeTV reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                }
            }
        }
    }
    
    
    [retainOthersSelectedIdDic setDictionary:[tempSelectedIdDic mutableCopy]];
    DLog(@"retainOthersSelectedIdDic %@", retainOthersSelectedIdDic);
    [defaults setObject:retainOthersSelectedIdDic forKey:[[[[dataArray objectAtIndex:indexPath.section]rows]objectAtIndex:indexPath.row]parentRow]];
    
    
}


-(BOOL)ContainsValue : (NSArray*)selectedIds{
    for (int i = 0; i<[selectedIds count]; i++) {
        if ([arrSelectedOthersIds containsObject:[selectedIds objectAtIndex:i]]) {
            NSLog(@"arrSelectedOthersIds %@", arrSelectedOthersIds);
            continue;
        }
        else
            return NO;
    }
    return YES;
}


-(void)removeIds :(NSMutableArray*)arr{
    for (int i = 0; i<[arr count]; i++) {
        [arrSelectedOthersIds removeObject:[arr objectAtIndex:i]];
    }
    
    
    NSLog(@"arrSelectedOthersIds %@", arrSelectedOthersIds);
}


-(void)updateCategorySubRows:(int)row : (int)section{
    SGTableRow *sgrow=[[[dataArray objectAtIndex:section] rows] objectAtIndex:row];//[SGTableRow alloc];
    NSMutableArray *marr1=[[NSMutableArray alloc] init];
    
    for(int i=0;i< catArray.count;i++)
    {
        SGTableSubRow *subrow=[SGTableSubRow alloc];
        subrow.isRow=0;
        subrow.subRowName=[NSString stringWithFormat:@"%@",[catArray objectAtIndex:i]];
        if(subrow.parentRow == nil){
            
            if([[[[[dataArray objectAtIndex:section] rows] objectAtIndex:row] rowName]isEqualToString:@"Genre"]){
                subrow.parentRow = @"Genre";
            }
            else{
                subrow.parentRow = @"Category";
            }
        }
        
        if ( [arrSelectedCatIds containsObject:[categoryDic objectForKey:[catArray objectAtIndex:i]]]) {
            [subrow setIsSelected:1];
        }
        if ([catArray count] == 1) {
            subrow.isFirstRow=1;
        }
        [marr1 addObject:subrow];
    }
    if(marr1.count>1)
    {
        SGTableSubRow *subrow=[SGTableSubRow alloc];
        subrow.isRow=0;
        subrow.subRowName=@"All";
        if ([arrSelectedCatIds count] == [catArray count]) {
            subrow.isFirstRowSelected=1;
        }
        
        else{
            subrow.isFirstRowSelected=0;
        }
        subrow.isFirstRow=1;
        [marr1 insertObject:subrow atIndex:0];
        if(subrow.parentRow == nil){
            if([[[[[dataArray objectAtIndex:section] rows] objectAtIndex:row] rowName]isEqualToString:@"Genre"]){
                subrow.parentRow = @"Genre";
            }
            else{
                subrow.parentRow = @"Category";
            }
        }
        
    }
    sgrow.subrows=marr1;
    
}


-(void)updateOptionsSubRows:(int)row : (int)section{
    SGTableRow *sgrow=[[[dataArray objectAtIndex:section] rows] objectAtIndex:row];//[SGTableRow alloc];
    
    NSMutableArray *marr1=[[NSMutableArray alloc] init];
    
    for(int i=0;i< optArray.count;i++)
    {
        SGTableSubRow *subrow=[SGTableSubRow alloc];
        subrow.isRow=0;
        subrow.subRowName=[NSString stringWithFormat:@"%@",[optArray objectAtIndex:i]];
        if(subrow.parentRow == nil)
            subrow.parentRow = @"Options";
        if ( [arrSelectedOptionsIds containsObject:[optionsDic objectForKey:[optArray objectAtIndex:i]]]) {
            [subrow setIsSelected:1];
        }
        if ([optArray count] == 1) {
            subrow.isFirstRow=1;
        }
        [marr1 addObject:subrow];
    }
    if(marr1.count>1)
    {
        SGTableSubRow *subrow=[SGTableSubRow alloc];
        subrow.isRow=0;
        subrow.subRowName=@"All";
        if ([arrSelectedOptionsIds count] == [optArray count]) {
            subrow.isFirstRowSelected=1;
        }
        
        else{
            subrow.isFirstRowSelected=0;
        }
        subrow.isFirstRow=1;
        
        [marr1 insertObject:subrow atIndex:0];
        if(subrow.parentRow == nil)
            subrow.parentRow = @"Options";
    }
    
    sgrow.subrows=marr1;
}


-(void)updateCitySubRows: (int)row : (int) section{
    SGTableRow *sgrow=[[[dataArray objectAtIndex:section] rows] objectAtIndex:row];//[SGTableRow alloc];
    
    NSMutableArray *marr1=[[NSMutableArray alloc] init];
    BOOL fSet=1;
    for(int i=0;i< cityArray.count;i++)
    {
        SGTableSubRow *subrow=[SGTableSubRow alloc];
        subrow.isRow=0;
        subrow.subRowName=[NSString stringWithFormat:@"%@",[cityArray objectAtIndex:i]];
        if(subrow.parentRow == nil)
            subrow.parentRow = @"City";
        if ( [arrSelectedCitiIds containsObject:[citiesDic objectForKey:[cityArray objectAtIndex:i]]]) {
            [subrow setIsSelected:1];
        }
        else
        {
            fSet=0;
        }
        if ([cityArray count] == 1) {
            subrow.isFirstRow=1;
        }
        if ([cityArray count] == 1 && fSet==1) {
            subrow.isFirstRow=1;
            subrow.isFirstRowSelected = 1;
        }
        [marr1 addObject:subrow];
    }
    
    if(marr1.count>1)
    {
        SGTableSubRow *subrow=[SGTableSubRow alloc];
        subrow.isRow=0;
        subrow.subRowName=@"All";
        
        subrow.isFirstRow=1;
        
        if(!fSet)
            subrow.isFirstRowSelected=0;
        else if(fSet || [arrSelectedCitiIds count] == [cityArray count])
            subrow.isFirstRowSelected=1;
        [marr1 insertObject:subrow atIndex:0];
        if(subrow.parentRow == nil)
            subrow.parentRow = @"City";
    }
    sgrow.subrows=marr1;
    
}

-(void)updateInterestsSubRows:(int)row : (int) section{
    SGTableRow *sgrow=[[[dataArray objectAtIndex:section] rows] objectAtIndex:row];//[SGTableRow alloc];
    
    NSMutableArray *marr1=[[NSMutableArray alloc] init];
    
    for(int i=0;i< intArray.count;i++)
    {
        SGTableSubRow *subrow=[SGTableSubRow alloc];
        subrow.isRow=0;
        subrow.subRowName=[NSString stringWithFormat:@"%@",[intArray objectAtIndex:i]];
        if(subrow.parentRow == nil)
            subrow.parentRow = @"Interests";
        if ( [arrSelectedInterestsIds containsObject:[interestsDic objectForKey:[intArray objectAtIndex:i]]]) {
            [subrow setIsSelected:1];
        }
        if ([intArray count] == 1) {
            subrow.isFirstRow=1;
        }
        [marr1 addObject:subrow];
    }
    
    
    if(marr1.count>1)
    {
        SGTableSubRow *subrow=[SGTableSubRow alloc];
        subrow.isRow=0;
        subrow.subRowName=@"All";
        if ([arrSelectedInterestsIds count] == [intArray count]) {
            subrow.isFirstRowSelected=1;
        }
        
        else{
            subrow.isFirstRowSelected=0;
        }
        subrow.isFirstRow=1;
        [marr1 insertObject:subrow atIndex:0];
        if(subrow.parentRow == nil)
            subrow.parentRow = @"Interests";
    }
    sgrow.subrows=marr1;
}

-(void)updateFilterSubRows :(int)row : (int) section : (NSString*)parentStr{
    SGTableRow *sgrow=[[[dataArray objectAtIndex:section] rows] objectAtIndex:row];//[SGTableRow alloc];
    NSMutableArray *marr1 = [[NSMutableArray alloc] init];
    NSMutableArray *selectedArray = [[NSMutableArray alloc] init];
    for(int i=0;i< othersArray.count;i++)
    {
        SGTableSubRow *subrow=[SGTableSubRow alloc];
        subrow.isRow=0;
        subrow.subRowName=[NSString stringWithFormat:@"%@",[othersArray objectAtIndex:i]];
        subrow.parentRow = parentStr;
        if ( [arrSelectedOthersIds containsObject:[othersDic objectForKey:[othersArray objectAtIndex:i]]]) {
            [selectedArray addObject:[othersArray objectAtIndex:i]];
            [subrow setIsSelected:1];
        }
        if ([othersArray count] == 1) {
            subrow.isFirstRow=1;
        }
        [marr1 addObject:subrow];
    }
    
    NSLog(@"arrOthersId %@",arrOthersId);
    [tempOthersDisc setObject:arrOthersId forKey:parentStr];
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    [tempSelectedIdDic setObject:arr forKey:parentStr];
    NSLog(@"tempSelectedIdDic %@",tempSelectedIdDic);
    NSLog(@"tempOthersDisc %@",tempOthersDisc);
    
    if(marr1.count>1)
    {
        SGTableSubRow *subrow=[SGTableSubRow alloc];
        subrow.isRow=0;
        subrow.subRowName=@"All";
        fetchRetainedSelectedOthersDic = [defaults valueForKey:parentStr];
        DLog(@"fetchRetainedSelectedOthersDic %@", fetchRetainedSelectedOthersDic);
        if([selectedArray count] == [[tempOthersDisc valueForKey:parentStr ] count]){
            subrow.isFirstRowSelected=1;
        }
        
        else{
            subrow.isFirstRowSelected=0;
        }
        
        subrow.isFirstRow=1;
        subrow.parentRow = parentStr;
        [marr1 insertObject:subrow atIndex:0];
    }
    sgrow.subrows=marr1;
}


-(void)clickOnTableViewRow:(UIButton*)sender
{
    UITableViewCell *tCell;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        tCell=(UITableViewCell *) sender.superview.superview;
    }
    
    else
        tCell=(UITableViewCell *) sender.superview.superview.superview;
    //sender.superview.superview.superview;
    
    NSIndexPath *indexPath=[swipeTV indexPathForCell:tCell];
    
    [self expandSubrowsAtIndexPath:indexPath  forCell:tCell];
}



-(void)didSubRowClick:(UIButton*)sender{
    
    UITableViewCell *tCell;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        tCell=(UITableViewCell *) sender.superview.superview;
    }
    else
        tCell=(UITableViewCell *) sender.superview.superview.superview;
    
    NSIndexPath *indexPath=[swipeTV indexPathForCell:tCell];
    
    [self selectSubRowsData:indexPath forCell:tCell];
}



-(void)sortingDone{
    
    if (!sortFilObj) {
        sortFilObj =[[SortAndFilter alloc]init];
    }
    
    NSMutableString *requestStr;
    
    if ([arrSelectedCatIds count]) {
        requestStr = [[NSMutableString alloc] init];
        for (int i=0; i<[arrSelectedCatIds count]; i++) {
            
            [requestStr appendFormat:@"%@,", arrSelectedCatIds[i]];
        }
        
        if ([requestStr hasSuffix:@","])
            [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
        
        sortFilObj.selectedCatIds = [[NSMutableString alloc]initWithString:requestStr];
    }
    else{
        sortFilObj.selectedCatIds = nil;
    }
    
    if ([arrSelectedCitiIds count]) {
        requestStr = [[NSMutableString alloc] init];
        for (int i=0; i<[arrSelectedCitiIds count]; i++) {
            
            [requestStr appendFormat:@"%@,", arrSelectedCitiIds[i]];
        }
        
        if ([requestStr hasSuffix:@","])
            [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
        
        sortFilObj.selectedCitiIds = [[NSMutableString alloc]initWithString:requestStr];
    }
    else{
        sortFilObj.selectedCitiIds = nil;
    }
    
    if ([arrSelectedOthersIds count]) {
        requestStr = [[NSMutableString alloc] init];
        for (int i=0; i<[arrSelectedOthersIds count]; i++) {
            
            [requestStr appendFormat:@"%@,", arrSelectedOthersIds[i]];
        }
        
        if ([requestStr hasSuffix:@","])
            [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
        
        sortFilObj.selectedOthersIds = [[NSMutableString alloc]initWithString:requestStr];
    }
    else{
        sortFilObj.selectedOthersIds = nil;
    }
    
    
    
    if ([arrSelectedInterestsIds count]) {
        requestStr = [[NSMutableString alloc] init];
        for (int i=0; i<[arrSelectedInterestsIds count]; i++) {
            
            [requestStr appendFormat:@"%@,", arrSelectedInterestsIds[i]];
        }
        
        if ([requestStr hasSuffix:@","])
            [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
        
        sortFilObj.selectedInterestsIds = [[NSMutableString alloc]initWithString:requestStr];
    }
    
    else{
        sortFilObj.selectedInterestsIds = nil;
    }
    if ([arrSelectedOptionsIds count]) {
        requestStr = [[NSMutableString alloc] init];
        for (int i=0; i<[arrSelectedOptionsIds count]; i++) {
            
            [requestStr appendFormat:@"%@,", arrSelectedOptionsIds[i]];
        }
        
        if ([requestStr hasSuffix:@","])
            [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
        
        sortFilObj.selectedOptIds = [[NSMutableString alloc]initWithString:requestStr];
    }
    else{
        sortFilObj.selectedOptIds = nil;
    }
    
    
    sortFilObj.distanceSelected = distanceSelected;
    sortFilObj.localSpecialSelected = localSpecialSelected;
    sortFilObj.eventDateSelected = eventDateSelected;
    sortFilObj.sortEventDateSelected = sortEventDateSelected;
    sortFilObj.alphabeticallySelected = alphabeticallySelected;
    sortFilObj.bandSelected =  bandSelected;
    sortFilObj.venueSelected = venueSelected;
    if (selectedDOB) {
        sortFilObj.evtDate = [[NSString alloc]initWithString:selectedDOB];
    }
    
    
    [defaults setValue:@"YES" forKey:@"isComingFromGroupingandSorting"];
    
    
    if ([module isEqualToString:@"Find All"] && inCategorySort) {
        [self request_sscatsearch:srchKey withObj:sortFilObj];
    }
    
    
        [self saveCustomObject:sortFilObj key:@"SortFilterObject"];
    
        [self.navigationController popViewControllerAnimated:YES];
    
    
}


- (void)saveCustomObject:(SortAndFilter *)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
    
}




-(void)showDatePicker{
    
    
    NSDate *displayDate=[[NSDate alloc]init];
    
    
    swipeTV.allowsSelection =NO;
    swipeTV.scrollEnabled = NO;
    
    UIBarButtonItem *done1 = [[UIBarButtonItem alloc]init];
    
    UIButton *nextBtn = [UtilityManager customizeBarButtonItem:@"Done"];
    // [nextBtn setTitleColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]] forState:UIControlStateNormal];
     [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextBtn addTarget:self action:@selector(dateSelected) forControlEvents:UIControlEventTouchUpInside];
    [[nextBtn layer] setBorderWidth:2.0f];
    [[nextBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
    [done1 setCustomView:nextBtn];
    
    //  [nextBtn ;
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:done1, nil];
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        dobToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-self.view.frame.size.height/3 - 40 + 65, self.view.frame.size.width, 40)];
    }
    else{
        
        dobToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-self.view.frame.size.height/3 - VARIABLE_HEIGHT(40) , self.view.frame.size.width, VARIABLE_HEIGHT(40))];
        
    }
    
    // [dobToolBar setBarTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]]];
    [dobToolBar setBarTintColor:[UIColor convertToHexString:@"#939393"]];
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {dobHolder = [[UIView alloc]initWithFrame:CGRectMake(0, dobToolBar.frame.origin.y + dobToolBar.frame.size.height,self.view.frame.size.width, self.view.frame.size.height/3)];
    }
    else
    {
        dobHolder = [[UIView alloc]initWithFrame:CGRectMake(0, 3*(self.view.frame.size.height/4) - 12,[[UIScreen mainScreen] bounds].size.width, 3*(self.view.frame.size.height/4) + 12)];
    }
    dobHolder.backgroundColor = [UIColor lightGrayColor];
    
    dobPicker = [[UIDatePicker alloc]init];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {dobPicker.bounds=dobHolder.bounds;
    }
    else
    {
        dobPicker.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, VARIABLE_HEIGHT(100));
    }
    
    dobPicker.datePickerMode= UIDatePickerModeDate;
  
    if (displayDate!=nil) {
        [dobPicker setDate:displayDate];
    }
    [dobHolder addSubview:dobPicker];
    [self.view addSubview:dobToolBar];
    [self.view addSubview:dobHolder];
    [dobToolBar setItems:toolbarItems];
    
   
    
}


-(void) hidePicker
{
    dobHolder.hidden=YES;
    dobPicker.hidden=YES;
    dobToolBar.hidden=YES;
    
    swipeTV.allowsSelection =YES;
    swipeTV.scrollEnabled = YES;
}

-(void)dateSelected
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeStyle:NSDateFormatterNoStyle];
    [df setDateStyle:NSDateFormatterMediumStyle];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [df setLocale:usLocale];
    
    
    selectedDOB = [[NSString alloc]initWithFormat:@"%@",
                   [df stringFromDate:dobPicker.date]];
    
    dobHolder.hidden=YES;
    dobPicker.hidden=YES;
    dobToolBar.hidden=YES;
    
    swipeTV.allowsSelection =YES;
    swipeTV.scrollEnabled = YES;
}




-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case SORT_FILTER:
            [self parseSortFilterList:response];
            break;
        default:
            break;
    }
}

-(void)request_sscatsearch:(NSString*)searchText withObj:(SortAndFilter*)sortFilterObj
{
    
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    //    if([defaults valueForKey:KEY_USERID])
    //    {
    //        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    //    }
    if (![categoryName isEqualToString:@""])
    {
        [parameters setValue:categoryName forKey:@"catName"];
    }
    [parameters setValue:@"0" forKey:@"lastVisitedNo"];
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length])
    {
        [parameters setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        [parameters setValue:[defaults valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        if ([defaults valueForKey:KEYZIPCODE]){
            [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"userPostalCode"];
        }
    }
    else
    {
        if ([defaults valueForKey:KEYZIPCODE]){
            [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"postalCode"];
        }
    }
    if([defaults valueForKey:KEY_MAINMENUID])
    {
        [parameters setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    }
    if (![defaults boolForKey:BottomButton])
    {
        if([defaults valueForKey:KEY_MITEMID])
        {
            [defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"findLocationMitemId"];
            [parameters setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
        }
        else if ([FindBottomButtonID count]>=1)
        {
            [parameters setValue:[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1] forKey:@"bottomBtnId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
            
        }
    }
    else
    {
        if ([defaults valueForKey:KEY_MITEMID]&& [[defaults valueForKey:KEY_BOTTOMBUTTONNAME] isEqualToString:@"subMenu"])
        {
            [defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"findLocationMitemId"];
            [parameters setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
        }
        [defaults setBool:YES forKey:BottomButton];
        if ([FindBottomButtonID count]>=1)
        {
            [parameters setValue:[FindBottomButtonID objectAtIndex:[FindBottomButtonID count]-1] forKey:@"bottomBtnId"];
            [parameters setValue:@"IOS" forKey:@"platform"];
        }
    }
    if ([searchText length])
    {
        [parameters setValue:searchText forKey:@"searchKey"];
    }
    [parameters setValue:@"ASC" forKey:@"sortOrder"];
    if(sortFilObj.distanceSelected || sortFilObj.alphabeticallySelected){
        if(sortFilObj.distanceSelected){
            [parameters setValue:@"distance" forKey:@"sortColumn"];
        }
        else{
            [parameters setValue:@"atoz" forKey:@"sortColumn"];
        }
    }
    else{
        if(segmentSelected==0){
            
            [parameters setValue:@"distance" forKey:@"sortColumn"];
            
        }
        else{
            
            [parameters setValue:@"atoz" forKey:@"sortColumn"];
            
        }
    }
    if (sortFilterObj.selectedCitiIds)
    {
        [parameters setValue:sortFilterObj.selectedCitiIds forKey:@"cityIds"];
    }
    else{
        if([RegionApp isEqualToString:@"1"] && [defaults valueForKey:@"sortedCitiIDs"])
        {
            [parameters setValue:[defaults valueForKey:@"sortedCitiIDs"] forKey:@"cityIds"];
            
        }
    }
    if(sortFilterObj.selectedOthersIds)
    {
        [parameters setValue:sortFilterObj.selectedOthersIds forKey:@"fValueId"];
    }
    if (sortFilterObj.selectedOptIds)
    {
        [parameters setValue:sortFilterObj.selectedOptIds forKey:@"filterId"];
    }
    if(sortFilterObj.selectedCatIds)
    {
        [parameters setValue:sortFilterObj.selectedCatIds forKey:@"subCatIds"];
    }
    if (sortFilterObj.selectedInterestsIds)
    {
        [parameters setValue:sortFilterObj.selectedInterestsIds forKey:@"interests"];
    }
    
    if (sortFilterObj.localSpecialSelected)
    {
        [parameters setValue:@"1" forKey:@"locSpecials"];
    }
    else
    {
        [parameters setValue:@"0" forKey:@"locSpecials"];
    }
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    
    
    if([defaults valueForKey:@"FindRadius"])
    {
        [parameters setValue:[defaults valueForKey:@"FindRadius"] forKey:@"radius"];
        
    }
    else
    {
        [parameters setValue:@"50" forKey:@"radius"];
        
    }
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [timeFormatter setTimeZone:timeZone];
    [timeFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *timeInUTC = [timeFormatter stringFromDate:currentTime];
    NSLog(@"Current time %@", timeInUTC);
    
    [parameters setValue:timeInUTC forKey:@"requestedTime"];
    
    DLog(@"parameter: %@",parameters);
    
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/sscatsearchjson",BASE_URL];
    DLog(@"Url: %@",urlString);
    
    NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
    
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    [defaults  setObject:responseData forKey:KEY_RESPONSEXML];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end


//
//  LoginViewController.m
//  HubCiti
//
//  Created by Anjana on 9/3/13.
//  Copyright (c) 2013 Anjana. All rights reserved.
//

#import "LoginViewController.h"
#import "FirstUse/RegistrationViewController.h"
#import "LoginDO.h"
#import "TBXML.h"
#import "HubCitiConstants.h"
#import "MainMenuViewController.h"
#import "RegistrationCompleteScreen.h"
#import "CityExperienceViewController.h"
#import "APNSListViewController.h"
#import "WebBrowserViewController.h"
#import "HTTPClient.h"
#import "UserInformationViewController.h"


extern BOOL isSameUser;
#pragma Variales for iPad conversion.

CGRect topImgRect,usernameLabelRect,txtField_userNameRect,pwdLabelRect,txtField_passwordRect,forgotPwdButtonRect,forgotPwdLabelRect,remMeButtonRect,remMeLabelRect,loginButtonRect,signUpButtonRect;
NSDictionary *viewsDictionary;

#pragma end


@interface LoginViewController () <HTTPClientDelegate>
{
    NSMutableArray *sortedCitiIDs;
}

@end

@implementation LoginViewController

@synthesize login_DO,okAction,tempPwdString,noEmailCountString;
@synthesize txtField_userName, txtField_password;
@synthesize guestUser,guestPassword;
@synthesize forgotEmailOrUser,resetPasswordString;

#pragma mark - View life cycle methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [self.view setAccessibilityLabel:@"LoginPage"];
    
    connectionManager = [[HubCitiConnectionManager alloc] init];
    
    //NSlog(@" view height %f",[[UIScreen mainScreen] bounds].size.height);
    
    [self designLoginView];
    
    /***** Logic to prevent View from expanding for iOS7 version *******/
    if([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // self.navigationController.navigationBar.hidden=YES;
    
    CustomizedNavController *cusNav =(CustomizedNavController *)self.navigationController;
    [cusNav hideBackButton:YES];
    [cusNav hideHambergerButton: YES];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    sortedCitiIDs=[[NSMutableArray alloc] init];
    //check if enabled "remember me"
    if ([defaults boolForKey:@"rememberMe"] == YES) {
        [remMeButton setImage:[UIImage imageNamed:@"checkboxOn.png"]  forState:UIControlStateNormal];
        isRemember = YES;
        txtField_userName.text = [defaults objectForKey:@"Username"];
        txtField_password.text = [defaults objectForKey:@"Password"];
        
    }
    else {
        [remMeButton setImage:[UIImage imageNamed:@"checkboxOff.png"]  forState:UIControlStateNormal];
        txtField_userName.text = @"";
        txtField_password.text = @"";
        if (![txtField_userName.text length] || ![txtField_password.text length]){
            [defaults setValue:nil forKey:@"Username"];
            [defaults setValue:nil forKey:@"Password"];
        }
    }
    
    if([defaults boolForKey:@"isMainMenuSignUp"]==YES)
    {
        [defaults setBool:NO forKey:@"isMainMenuSignUp"];
        
        RegistrationViewController *regView = [[RegistrationViewController alloc]initWithNibName:@"RegistrationViewController" bundle:[NSBundle mainBundle]];
        regView.signup_DO = login_DO;
        
        [self.navigationController pushViewController:regView animated:NO];
        ReleaseAndNilify(regView);
        
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    if (touch.tapCount>=1)
    {
        [txtField_userName resignFirstResponder];
        [txtField_password resignFirstResponder];
    }
    if (![txtField_userName.text length] && ![txtField_password.text length]){
        [defaults setValue:nil forKey:@"Username"];
        [defaults setValue:nil forKey:@"Password"];
    }
}

-(void)showSplash
{
    UIViewController *modalViewController = [[UIViewController alloc] init];
    if([defaults objectForKey:@"SplashImage"] != NULL)
    {
        SdImageView *splash1 = [[SdImageView alloc] initWithFrame:CGRectMake(0,0, VIEW_FRAME_WIDTH, VIEW_FRAME_HEIGHT)];
        splash1.contentMode = UIViewContentModeScaleToFill;
        splash1.backgroundColor = [UIColor clearColor];
        [splash1 loadImage:[defaults objectForKey:@"SplashImage"]];
        [splashScreen addSubview:splash1]; //WithImage:]];
        //[splash1 release];
    }
    else
    {
        UIImageView *splash1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, VIEW_FRAME_WIDTH, VIEW_FRAME_HEIGHT)];
        [splash1 setImage:[UIImage imageNamed:@"Default.png"]];
        [splashScreen addSubview:splash1]; //WithImage:]];
        // [splash1 release];
    }
    
    modalViewController.view = splashScreen;
    
    [self presentViewController:modalViewController animated:NO completion:nil];
    
    [self performSelector:@selector(hideSplash) withObject:nil afterDelay:5.0];
    // [modalViewController release];
}

- (void)hideSplash{
    [[self presentedViewController] dismissViewControllerAnimated:NO completion:nil];
}

/*
 *
 Method to render UI based on the service
 All UI controls should be generated dynamically
 *
 */
-(void)designLoginView
{
    int height_offset = 10;
    int width_offset=10;
    //NSlog(@"color %@",login_DO.backgroundColor);
    self.view.backgroundColor = [UIColor convertToHexString:login_DO.backgroundColor];
    /******* should use Asynimage view to load image from given url **************/
    float navHeight = self.navigationController.navigationBar.frame.size.height;
    
    topImageView = [[SdImageView alloc] init];
    
    //iPad conversion.
    if(IS_IPHONE5)
    {
        topImgRect = CGRectMake(X_OFFSET, navHeight, VIEW_FRAME_WIDTH-X_OFFSET*2, 120);
    }
    else
    {
        topImgRect = CGRectMake(X_OFFSET, X_OFFSET+width_offset, VIEW_FRAME_WIDTH-X_OFFSET*2, 80);
    }
    topImageView.frame=topImgRect;
    //topImageView.contentMode = UIViewContentModeScaleAspectFit;
    topImageView.backgroundColor = [UIColor clearColor];
    topImageView.layer.cornerRadius = 5.0f;
    topImageView.clipsToBounds = YES;
    
    if(login_DO.logoPath)
        [topImageView loadImage:[login_DO.logoPath stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
    
    [self.view addSubview:topImageView];
    /******************UNCOMMENT once service is ready***************************/
    
    usernameLabel = [self createLabel:NSLocalizedString(@"User Name or Email",@"User Name or Email") rect:CGRectZero];
    usernameLabel.textColor = [UIColor convertToHexString:login_DO.fontColor];
    usernameLabel.font=[UIFont boldSystemFontOfSize:15];
    
    //iPad conversion.
    usernameLabelRect = CGRectMake(X_OFFSET+2, topImgRect.origin.y+topImgRect.size.height+height_offset, 90, LABEL_HEIGHT);
    
    txtField_userName = [self createTxtField:NSLocalizedString(@"User Name or Email",@"User Name or Email") rect:CGRectZero];
    txtField_userName.tag = 1;
    txtField_userName.font=[UIFont systemFontOfSize:14];
    txtField_userName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtField_userName.autocorrectionType = UITextAutocorrectionTypeNo;
    [txtField_userName setAccessibilityLabel:@"userName"];
    txtField_userName.keyboardType = UIKeyboardTypeEmailAddress;
    txtField_userName.clearButtonMode = UITextFieldViewModeWhileEditing;
    txtField_userName.returnKeyType = UIReturnKeyNext;
    
    //iPad conversion.
    txtField_userNameRect = CGRectMake(X_OFFSET,usernameLabelRect.origin.y+usernameLabelRect.size.height+5,VIEW_FRAME_WIDTH-X_OFFSET*2,TEXTFIELD_HEIGHT);
    
    pwdLabel = [self createLabel:NSLocalizedString(@"Password",@"Password") rect:CGRectZero];
    pwdLabel.textColor = [UIColor convertToHexString:login_DO.fontColor];
    pwdLabel.font=[UIFont boldSystemFontOfSize:15];
    
    //iPad conversion.
    pwdLabelRect = CGRectMake(X_OFFSET+2,txtField_userNameRect.origin.y+txtField_userNameRect.size.height+height_offset,90,LABEL_HEIGHT);
    
    txtField_password = [self createTxtField:NSLocalizedString(@"password",@"password") rect:CGRectZero];
    txtField_password.tag = 2;
    txtField_password.secureTextEntry = YES;
    txtField_password.font=[UIFont systemFontOfSize:14];
    txtField_password.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtField_password.autocorrectionType = UITextAutocorrectionTypeNo;
    [txtField_password setAccessibilityLabel:@"password"];
    txtField_password.keyboardType = UIKeyboardTypeDefault;
    txtField_password.clearButtonMode = UITextFieldViewModeWhileEditing;
    txtField_password.returnKeyType = UIReturnKeyDone;
    
    //iPad conversion.
    txtField_passwordRect = CGRectMake(X_OFFSET,pwdLabelRect.origin.y+pwdLabelRect.size.height+5,VIEW_FRAME_WIDTH-X_OFFSET*2,TEXTFIELD_HEIGHT);
    
    /* forgot password UI control */
    forgotPwdButton  = [UIButton buttonWithType:UIButtonTypeCustom];
    forgotPwdButton.titleLabel.font=[UIFont boldSystemFontOfSize:12];
    [forgotPwdButton setTitle:NSLocalizedString(@"Forgot Password?",@"Forgot Password?") forState:UIControlStateNormal];
    forgotPwdButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [forgotPwdButton setTitleColor:[UIColor convertToHexString:login_DO.fontColor] forState:UIControlStateNormal];
    forgotPwdButton.backgroundColor =[UIColor clearColor];
    [forgotPwdButton setAccessibilityLabel:@"forgotPassword"];
    forgotPwdButton.showsTouchWhenHighlighted=YES;
    [forgotPwdButton addTarget:self action:@selector(forgotPwdClicked:) forControlEvents:UIControlEventTouchDown];
    
    //iPad conversion.
    forgotPwdButtonRect = CGRectMake(X_OFFSET,txtField_passwordRect.origin.y+txtField_passwordRect.size.height+height_offset,130,LABEL_HEIGHT);
    
    /* forgot password Label UI control */
    forgotPwdLabel = [[UILabel alloc]init];
    forgotPwdLabel.text = NSLocalizedString(@"___________________", @"___________________");
    forgotPwdLabel.textColor=[UIColor convertToHexString:login_DO.fontColor];
    forgotPwdLabel.font =[UIFont boldSystemFontOfSize:12];
    forgotPwdLabel.backgroundColor = [UIColor clearColor];
    
    /* Remember Me Button UI control - button image will change based on server response*/
    remMeButton  = [UIButton buttonWithType:UIButtonTypeCustom];
    [remMeButton setImage:[UIImage imageNamed:@"checkboxOff.png"]  forState:UIControlStateNormal];
    remMeButton.backgroundColor =[UIColor clearColor];
    [remMeButton setAccessibilityLabel:@"rememberMe"];
    [remMeButton addTarget:self action:@selector(rememberMeClicked) forControlEvents:UIControlEventTouchDown];
    
    //iPad conversion.
    remMeButtonRect = CGRectMake(X_OFFSET*2+forgotPwdButtonRect.size.width+18,txtField_passwordRect.origin.y+txtField_passwordRect.size.height+10,X_OFFSET,X_OFFSET);
    
    /* Remember Me Label UI control */
    remMeLabel = [[UILabel alloc]init];
    remMeLabel.text = NSLocalizedString(@"Remember me", @"Remember me");
    remMeLabel.textColor=[UIColor convertToHexString:login_DO.fontColor];
    remMeLabel.font =[UIFont boldSystemFontOfSize:12];
    remMeLabel.backgroundColor = [UIColor clearColor];
    
    //iPad conversion.
    remMeLabelRect = CGRectMake(X_OFFSET+remMeButtonRect.origin.x+width_offset,txtField_passwordRect.origin.y+txtField_passwordRect.size.height+height_offset,100,LABEL_HEIGHT);
    
    /*Login Button UI control */
    loginButton  = [UIButton buttonWithType:UIButtonTypeCustom];
    loginButton.layer.cornerRadius = 5.0f;
    loginButton.titleLabel.font=[UIFont boldSystemFontOfSize:14];
    loginButton.backgroundColor = [UIColor convertToHexString:login_DO.buttonColor];
    [loginButton setTitle:NSLocalizedString(@"Log In",@"Log In") forState:UIControlStateNormal];
    [loginButton setTitleColor:[UIColor convertToHexString:login_DO.buttonFontColor] forState:UIControlStateNormal];
    [loginButton setAccessibilityLabel:@"login"];
    [loginButton addTarget:self action:@selector(loginClicked:) forControlEvents:UIControlEventTouchDown];
    loginButton.showsTouchWhenHighlighted=YES;
    
    //iPad conversion.
    loginButtonRect = CGRectMake(X_OFFSET,forgotPwdButtonRect.origin.y+forgotPwdButtonRect.size.height+X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2,30);
    
    /* Sign up Button UI control */
    signUpButton  = [UIButton buttonWithType:UIButtonTypeCustom];
    signUpButton.layer.cornerRadius = 5.0f;
    signUpButton.backgroundColor = [UIColor convertToHexString:login_DO.buttonColor];
    signUpButton.titleLabel.font=[UIFont boldSystemFontOfSize:14];
    [signUpButton setTitle:NSLocalizedString(@"SignUp",@"SignUp") forState:UIControlStateNormal];
    [signUpButton setTitleColor:[UIColor convertToHexString:login_DO.buttonFontColor] forState:UIControlStateNormal];
    [signUpButton setAccessibilityLabel:@"signup"];
    [signUpButton addTarget:self action:@selector(signUpClicked:) forControlEvents:UIControlEventTouchUpInside];
    signUpButton.showsTouchWhenHighlighted=YES;
    
    //iPad conversion.
    signUpButtonRect = CGRectMake(X_OFFSET,loginButtonRect.origin.y+loginButtonRect.size.height+X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2,30);
    
    
    /*Guest Button*/
    guestButton = [UIButton buttonWithType:UIButtonTypeCustom];
    guestButton.layer.cornerRadius = 5.0f;
    guestButton.backgroundColor = [UIColor convertToHexString:login_DO.buttonColor];
    guestButton.titleLabel.font=[UIFont boldSystemFontOfSize:14];
    [guestButton setTitle:NSLocalizedString(@"Guest Login",@"Guest Login") forState:UIControlStateNormal];
    [guestButton setTag:2];
    [guestButton setTitleColor:[UIColor convertToHexString:login_DO.buttonFontColor] forState:UIControlStateNormal];
    [guestButton addTarget:self action:@selector(loginClicked:) forControlEvents:UIControlEventTouchDown];
    guestButton.showsTouchWhenHighlighted=YES;
    [guestButton setAccessibilityLabel:@"guest"];
    /* Powered By Image UI control */
    poweredByImg = [[SdImageView alloc]init];
    [poweredByImg setBackgroundColor:[UIColor clearColor]];
    [poweredByImg setContentMode:UIViewContentModeScaleAspectFit];
    
    if (login_DO.poweredByPath != NULL)
        [poweredByImg loadImage:login_DO.poweredByPath];//load image coming from server
    
    else
        [poweredByImg loadImagefrommainBundel:@"poweredByLogo.png"];//load default image
    
    
    txtField_userName.delegate = self;
    txtField_password.delegate = self;
    
    //App Version label
    versionLabel = [[UILabel alloc]init];
    [versionLabel setBackgroundColor:[UIColor clearColor]];
    [versionLabel setContentMode:UIViewContentModeScaleAspectFit];
    if(AppVersion != NULL)
        versionLabel.text = AppVersion;
    versionLabel.textColor=[UIColor convertToHexString:login_DO.fontColor];
    versionLabel.font =[UIFont boldSystemFontOfSize:12];
    
    
    
    [self.view addSubview:usernameLabel];
    [self.view addSubview:pwdLabel];
    [self.view addSubview:txtField_password];
    [self.view addSubview:txtField_userName];
    [self.view addSubview:forgotPwdButton];
    [self.view addSubview:remMeButton];
    [self.view addSubview:remMeLabel];
    [self.view addSubview:loginButton];
    [self.view addSubview:signUpButton];
    [self.view addSubview:guestButton];
    [self.view addSubview:poweredByImg];
    [self.view addSubview:forgotPwdLabel];
    [self.view addSubview:versionLabel];
    
    // Call this function to set constraints on views.
    [self setConstraints:height_offset width_offset:width_offset navHeight:navHeight];
    
    ReleaseAndNilify(versionLabel);
    ReleaseAndNilify(powerByLabel);
    ReleaseAndNilify(poweredByImg);
    ReleaseAndNilify(remMeLabel);
    ReleaseAndNilify(forgotPwdLabel);
}

/*
 Function for setting constraints on views using auto layout.
 */
-(void)setConstraints:(int)height_offset width_offset:(int)width_offset navHeight:(double)navHeight
{
    viewsDictionary =
    NSDictionaryOfVariableBindings(topImageView, usernameLabel,txtField_userName, pwdLabel, txtField_password, forgotPwdLabel, forgotPwdButton, remMeButton, remMeLabel,loginButton,signUpButton,guestButton,poweredByImg,versionLabel);
    
    // Set constraint property.
    topImageView.translatesAutoresizingMaskIntoConstraints = NO;
    usernameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    txtField_userName.translatesAutoresizingMaskIntoConstraints = NO;
    pwdLabel.translatesAutoresizingMaskIntoConstraints = NO;
    txtField_password.translatesAutoresizingMaskIntoConstraints = NO;
    forgotPwdLabel.translatesAutoresizingMaskIntoConstraints = NO;
    forgotPwdButton.translatesAutoresizingMaskIntoConstraints = NO;
    remMeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    remMeButton.translatesAutoresizingMaskIntoConstraints = NO;
    loginButton.translatesAutoresizingMaskIntoConstraints = NO;
    signUpButton.translatesAutoresizingMaskIntoConstraints = NO;
    guestButton.translatesAutoresizingMaskIntoConstraints = NO;
    poweredByImg.translatesAutoresizingMaskIntoConstraints = NO;
    versionLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        topImgRect = CGRectMake(X_OFFSET, navHeight, VIEW_FRAME_WIDTH-X_OFFSET*2, 240);
        
        // Call this function for setting constraints for iPad
        [self setIpadConstraints:height_offset width_offset:width_offset navHeight:navHeight];
        
    }
    else
    {
        // Call this function for setting constraints for iPhone
        [self setIphoneConstraints:height_offset width_offset:width_offset navHeight:navHeight];
    }
    
}

/*
 Function for setting constraints for iPad.
 */
-(void)setIpadConstraints:(int)height_offset width_offset:(int)width_offset navHeight:(double)navHeight
{
    // Vertical constrains
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[topImageView(240)]",navHeight+40] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[usernameLabel(%i)]",topImgRect.origin.y+topImgRect.size.height+height_offset+80,IPAD_LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[txtField_userName(%i)]",usernameLabelRect.origin.y+usernameLabelRect.size.height+275,IPAD_TEXTFIELD_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[pwdLabel(%i)]",txtField_userNameRect.origin.y+txtField_userNameRect.size.height+height_offset+280,IPAD_LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[txtField_password(%i)]",pwdLabelRect.origin.y+pwdLabelRect.size.height+290,IPAD_TEXTFIELD_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[loginButton(40)]",forgotPwdButtonRect.origin.y+forgotPwdButtonRect.size.height+X_OFFSET+400] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[signUpButton(40)]",loginButtonRect.origin.y+loginButtonRect.size.height+X_OFFSET+420] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[guestButton(40)]",signUpButtonRect.origin.y+signUpButtonRect.size.height+X_OFFSET+440] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[poweredByImg(60)]",VIEW_FRAME_HEIGHT-IPAD_LABEL_HEIGHT-STATUSBAR_HEIGHT-width_offset-65] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[versionLabel(40)]",VIEW_FRAME_HEIGHT-IPAD_LABEL_HEIGHT-STATUSBAR_HEIGHT-22] options:0 metrics:0 views:viewsDictionary]];
    
    // Horizontal constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[topImageView(%f)]|",X_OFFSET+85,VIEW_FRAME_WIDTH-X_OFFSET*10] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[usernameLabel(150)]",X_OFFSET+225] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[txtField_userName(%f)]",X_OFFSET+225,VIEW_FRAME_WIDTH-X_OFFSET*25+30] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[pwdLabel(90)]|",X_OFFSET+225] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[txtField_password(%f)]",X_OFFSET+225,VIEW_FRAME_WIDTH-X_OFFSET*25+30] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[loginButton(%f)]",X_OFFSET+225,VIEW_FRAME_WIDTH-X_OFFSET*25+30] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[signUpButton(%f)]",X_OFFSET+225,VIEW_FRAME_WIDTH-X_OFFSET*25+30] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[guestButton(%f)]",X_OFFSET+225,VIEW_FRAME_WIDTH-X_OFFSET*25+30] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[poweredByImg(80)]",X_OFFSET+115] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[versionLabel(150)]",X_OFFSET+115] options:0 metrics:0 views:viewsDictionary]];
    
    
    // Vertical
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[remMeButton(%i)]",txtField_passwordRect.origin.y+txtField_passwordRect.size.height+340,X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[remMeLabel(%i)]",txtField_passwordRect.origin.y+txtField_passwordRect.size.height+340,IPAD_LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[forgotPwdButton(%i)]",txtField_passwordRect.origin.y+txtField_passwordRect.size.height+height_offset+380,IPAD_LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    //        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[forgotPwdLabel(%i)]-|",txtField_passwordRect.origin.y+txtField_passwordRect.size.height+315,LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    forgotPwdLabel.hidden=YES;
    
    // Horizontal
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[remMeButton(%i)]",X_OFFSET+225,X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[remMeLabel(100)]",X_OFFSET+250] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[forgotPwdButton(130)]",X_OFFSET+225] options:0 metrics:0 views:viewsDictionary]];
    //[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[forgotPwdLabel(130)]-|",X_OFFSET+225] options:0 metrics:0 views:viewsDictionary]];
}

/*
 Function for setting constraints for iPhone.
 */
-(void)setIphoneConstraints:(int)height_offset width_offset:(int)width_offset navHeight:(double)navHeight
{
    
    
    if(IS_IPHONE5)
    {
        // Vertical constrains
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[topImageView(%f)]",navHeight,topImgRect.size.height] options:0 metrics:0 views:viewsDictionary]];
    }
    else
    {
        // Vertical constrains
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%i-[topImageView(%f)]",X_OFFSET+width_offset,topImgRect.size.height] options:0 metrics:0 views:viewsDictionary]];
    }
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[usernameLabel(%i)]",topImgRect.origin.y+topImgRect.size.height+height_offset,LABEL_HEIGHT] options:0 metrics:nil views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[txtField_userName(%i)]",usernameLabelRect.origin.y+usernameLabelRect.size.height+5,TEXTFIELD_HEIGHT] options:0 metrics:nil views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[pwdLabel(%i)]",txtField_userNameRect.origin.y+txtField_userNameRect.size.height+height_offset,LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[txtField_password(%i)]",pwdLabelRect.origin.y+pwdLabelRect.size.height+5,TEXTFIELD_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[loginButton(30)]",forgotPwdButtonRect.origin.y+forgotPwdButtonRect.size.height+X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[signUpButton(30)]",loginButtonRect.origin.y+loginButtonRect.size.height+X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[guestButton(30)]",signUpButtonRect.origin.y+signUpButtonRect.size.height+X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[poweredByImg(32)]",VIEW_FRAME_HEIGHT-LABEL_HEIGHT-STATUSBAR_HEIGHT-width_offset-5] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[versionLabel(25)]",VIEW_FRAME_HEIGHT-STATUSBAR_HEIGHT-5] options:0 metrics:0 views:viewsDictionary]];
    
    
    
    // Horizontal constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[topImageView(%f)]",X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[usernameLabel(150)]",X_OFFSET+2] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[txtField_userName(%f)]",X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[pwdLabel(90)]",X_OFFSET+2] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[txtField_password(%f)]",X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[loginButton(%f)]",X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[signUpButton(%f)]",X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[guestButton(%f)]",X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[poweredByImg(50)]",X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[versionLabel(100)]",X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    
    // Vertical
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[remMeButton(%i)]",txtField_passwordRect.origin.y+txtField_passwordRect.size.height+10,X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[remMeLabel(%i)]",txtField_passwordRect.origin.y+txtField_passwordRect.size.height+10,X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[forgotPwdButton(%i)]",txtField_passwordRect.origin.y+txtField_passwordRect.size.height+height_offset,LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[forgotPwdLabel(%i)]",txtField_passwordRect.origin.y+txtField_passwordRect.size.height+15,LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    // Horizontal
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%f-[remMeButton(90)]",X_OFFSET*2+forgotPwdButtonRect.size.width-18] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%f-[remMeLabel(100)]",X_OFFSET+remMeButtonRect.origin.x+width_offset] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[forgotPwdButton(130)]",X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[forgotPwdLabel(130)]",X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
}




/*
 *
 Common method to create the label
 *
 */
- (UILabel *)createLabel:(NSString*)title rect:(CGRect)rect{
    
    UILabel* label = [[UILabel alloc] initWithFrame:rect] ;
    //label.font = [UIFont boldSystemFontOfSize:MAIN_FONT_SIZE];
    //label.font=[UIFont fontWithName:@"GillSans-BoldItalic" size:14.0f];
    //label.adjustsFontSizeToFitWidth = YES;
    label.textColor = [UIColor blackColor];
    label.backgroundColor = [UIColor clearColor];
    label.text = title;
    return label;
}


/* Common method to create the textfield */
- (UITextField *)createTxtField:(NSString*)placeHolder rect:(CGRect)rect
{
    UITextField *txtField = [[UITextField alloc] initWithFrame:rect];
    txtField.textColor = [UIColor blackColor];
    txtField.borderStyle = UITextBorderStyleRoundedRect;
    txtField.keyboardType = UIKeyboardTypeDefault;
    txtField.placeholder = placeHolder;
    return txtField;
}




/*
 *
 Action for remember me button click
 *
 */
- (void)rememberMeClicked {
    
    if (isRemember) {
        [remMeButton setImage:[UIImage imageNamed:@"checkboxOff.png"]  forState:UIControlStateNormal];
        [remMeButton setAccessibilityValue:@"NO"];
        
        isRemember = NO;
        [defaults  setBool:NO forKey:@"rememberMe"];
    }
    else {
        [remMeButton setImage:[UIImage imageNamed:@"checkboxOn.png"]  forState:UIControlStateNormal];
        [remMeButton setAccessibilityValue:@"YES"];
        [defaults  setBool:YES forKey:@"rememberMe"];
        isRemember = YES;
        [defaults setObject:txtField_userName.text forKey:@"Username"];
        [defaults setObject:txtField_password.text forKey:@"Password"];
    }
}

/*
 *
 Action for forgot password button click
 *
 */

- (IBAction)forgotPwdClicked:(id)sender
{
    //    UIAlertController * alert=   [UIAlertController
    //                                  alertControllerWithTitle:@"Forgot Password"
    //                                  message:nil
    //                                  preferredStyle:UIAlertControllerStyleAlert];
    //
    //    UIAlertAction* ok = [UIAlertAction
    //                         actionWithTitle:@"Submit"
    //                         style:UIAlertActionStyleDefault
    //                         handler:^(UIAlertAction * action)
    //                         {
    //                             UITextField * textField1 = alert.textFields.firstObject;
    //                             forgotEmailOrUser = textField1.text;
    //                             forgotPasswordFlag = 1;
    //                            iWebRequestState = LOGIN_FORGOTPASSWORD;
    //                             [self requestForForgotPassword];
    //
    //
    //                         }];
    //    UIAlertAction* cancel = [UIAlertAction
    //                             actionWithTitle:@"Cancel"
    //                             style:UIAlertActionStyleDefault
    //                             handler:^(UIAlertAction * action)
    //                             {
    //                                 [alert dismissViewControllerAnimated:YES completion:nil];
    //
    //                             }];
    //
    //    [alert addAction:ok];
    //    [alert addAction:cancel];
    //
    //    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    //        textField.placeholder = @"Enter Email/User Name";
    //      //  [forgotEmailOrUser isEqualToString:textField.text];
    //
    //    }];
    //
    //    [self presentViewController:alert animated:YES completion:nil];
    
    
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Forgot Password"
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    self.okAction = [UIAlertAction
                     actionWithTitle:@"Submit"
                     style:UIAlertActionStyleDefault
                     handler:^(UIAlertAction * action)
                     {
                         UITextField * textField1 = alert.textFields.firstObject;
                         forgotEmailOrUser = textField1.text;
                         forgotPasswordFlag = 1;
                         iWebRequestState = LOGIN_FORGOTPASSWORD;
                         [self requestForForgotPassword];
                         
                         
                     }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    self.okAction.enabled = NO;
    [alert addAction:self.okAction];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.delegate = self;
        textField.placeholder = @"Enter Email/User Name";
        //  [forgotEmailOrUser isEqualToString:textField.text];
        
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self.okAction setEnabled:(finalString.length >= 1)];
    return YES;
}

/*
 *
 Method to send request for login authentication
 *
 */



-(void)requestForForgotPassword
{
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserRegistrationInfo><userName>%@</userName>",forgotEmailOrUser];
    [xmlStr appendFormat:@"<hubCitiId>%@</hubCitiId></UserRegistrationInfo>", [defaults valueForKey:KEY_HUBCITIID]];
    // [xmlStr appendFormat:@"<hubCitiId>93</hubCitiId></UserRegistrationInfo>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/forgotpwd",BASE_URL];
    
    // NSString *urlString = [NSString stringWithFormat:@"http://sdw2730:8080/HubCiti2.6/firstuse/v2/forgotpwd"];
    NSLog(@"XMl string %@",xmlStr);
    
    [connectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    ReleaseAndNilify(xmlStr);
    
}
//-(void)requestForForgotPassword
//{
//    NSMutableString *urlString = [BASE_URL mutableCopy];
//    [urlString appendFormat:@"firstuse/forgotpwd?username=%@&hubCitiId=%@",txtField_userName.text,[defaults valueForKey:KEY_HUBCITIID]];
//
//    [connectionManager establishConnectionForGet:urlString withDelegate:self];
//
//}



//-(void)parseForgotPassword : (NSString*)response
//{
//    if ([UtilityManager isNullOrEmptyString:response]) {
//        ReleaseAndNilify(response);
//        return;
//    }
//
//    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
//    TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
//    TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
//
//    if ([TBXML textForElement:responseCodeElement] != nil)
//    {
//
//        UIAlertController * alert=[UIAlertController alertControllerWithTitle:[TBXML textForElement:responseTextElement] message:nil preferredStyle:UIAlertControllerStyleAlert];
//
//
//        UIAlertAction* ok = [UIAlertAction
//                             actionWithTitle:@"OK"
//                             style:UIAlertActionStyleDefault
//                             handler:^(UIAlertAction * action)
//                             {
//                                 [alert dismissViewControllerAnimated:YES completion:nil];
//
//                             }];
//        [alert addAction:ok];
//
//        [self presentViewController:alert animated:YES completion:nil];
//
//        //[alert release];
//    }
//
//}

-(void)parseForgotPassword : (NSString*)response
{
    if ([UtilityManager isNullOrEmptyString:response]) {
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    
    if ([TBXML textForElement:responseCodeElement] != nil) {
       
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[TBXML textForElement:responseTextElement]
                                      message:nil
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 txtField_password.text = @"";
                                 [defaults  setObject:txtField_password.text forKey:@"Password"];
                                 [defaults  setBool:NO forKey:@"rememberMe"];
                                 [remMeButton setImage:[UIImage imageNamed:@"checkboxOff.png"]  forState:UIControlStateNormal];
                                 
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

/*
 *
 Action for login button click
 Perform validations before login
 *
 */
- (IBAction)loginClicked:(id)sender {
    if ([sender tag] == 2) {
        if([Network currentReachabilityStatus]==0)
        {
            [UtilityManager showAlert];
        }
        else
        {
            /* Have to be harcoded with whatever DB team gives */
            
            [defaults setBool:NO forKey:@"LoginUserNews"];
//            [SharedManager setGps_allow_flag:NO];
//            
//            [defaults setValue:@"" forKey:KEY_LATITUDE];//reset the values
//            
//            [defaults setValue:@"" forKey:KEY_LONGITUDE];
//            
//            
//            [defaults setValue:@"NO" forKey:@"allowingGPS"];
            guestUser = @"WelcomeScanSeeGuest";
            guestPassword = @":::We@Love!!ScanSee?{People}";
            [defaults  setBool:NO forKey:@"rememberMe"];
            //            [defaults  setValue:@"78654" forKey:KEYZIPCODE];
           // [defaults  setValue:@"NO" forKey:@"allowingGPS"];
            [SharedManager setGps_allow_flag:YES];
            [defaults setValue:@"YES" forKey:@"allowingGPS"];
            iWebRequestState = LOGIN_AUTHENTICATEUSER;
            userIdentifier = TRUE;
            //save the user email and password for "rememeber me"
            [self requestForLogin];
            
        }
    }
    else
    { loginButton.enabled = NO;
        [txtField_userName resignFirstResponder];
        [txtField_password resignFirstResponder];
        
        if([Network currentReachabilityStatus]==0)
        {
            [UtilityManager showAlert];
        }
        else {
            
            txtField_userName.text = [txtField_userName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            if (![txtField_userName.text length] || ![txtField_password.text length]) {
                
                [UtilityManager showAlert:nil msg:NSLocalizedString(@"Fields cannot be empty",@"Fields cannot be empty")];
                loginButton.enabled = YES;
            }
            else
            {
                //save the user email and password for "rememeber me"
                [defaults  setObject:txtField_userName.text forKey:@"Username"];
                [defaults  setObject:txtField_password.text forKey:@"Password"];
                
                if (isRemember)
                {
                    [defaults  setBool:YES forKey:@"rememberMe"];
                }
                else
                    
                {
                    [defaults  setBool:NO forKey:@"rememberMe"];
                    //txtField_userName.text = @"";
                    //txtField_password.text = @"";
                }
                //end
                userIdentifier=FALSE;
                
                [defaults setBool:YES forKey:@"LoginUserNews"];
                iWebRequestState = LOGIN_AUTHENTICATEUSER;
                [self requestForLogin];
                
            }
        }
    }
}


/*
 *
 Method to send request for login authentication
 *
 */
-(void)requestForLogin
{
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    if (userIdentifier==TRUE) {
        
        [xmlStr appendFormat:@"<UserDetails><userName><![CDATA[%@]]></userName>",guestUser];
        [xmlStr appendFormat:@"<password><![CDATA[%@]]></password>", guestPassword];
        
    }
    else{
        [xmlStr appendFormat:@"<UserDetails><userName><![CDATA[%@]]></userName>", txtField_userName.text];
        [xmlStr appendFormat:@"<password><![CDATA[%@]]></password>", txtField_password.text];
        
    }
    [xmlStr appendFormat:@"<appVersion>%@</appVersion>",AppVersion];
    [xmlStr appendFormat:@"<deviceId>%@</deviceId>",[defaults valueForKey:KEY_DEVICEID]];
    // [xmlStr appendFormat:@"<deviceId>3F0EAF9D-51F7-45DA-AC9D-554E8772B640</deviceId>"];
    [xmlStr appendFormat:@"<hubCitiKey>%@</hubCitiKey><platform>IOS</platform></UserDetails>",HUBCITIKEY];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/getuserlogin",BASE_URL];
    // NSString *urlString = [NSString stringWithFormat:@"http://sdw2730:8080/HubCiti2.6/firstuse/v2/getuserlogin"];
    
    [connectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    ReleaseAndNilify(xmlStr);
    
}


-(void) sortCitiId : (TBXML*) tbXml
{
    TBXMLElement *cityListElement = [TBXML childElementNamed:KEY_CITILIST parentElement:tbXml.rootXMLElement];
    NSLog(@"%@",[TBXML textForElement:tbXml.rootXMLElement]);
    
    NSLog(@"%@",[TBXML textForElement:cityListElement]);
    
    TBXMLElement *cityDetailsElement = [TBXML childElementNamed:@"City" parentElement:cityListElement];
    NSMutableArray* cityIDS = [[NSMutableArray alloc] init];
    
    while (cityDetailsElement!=nil) {
        
        TBXMLElement *cityIdElement = [TBXML childElementNamed:@"cityId" parentElement:cityDetailsElement];
        
        
        if (cityIdElement!=nil) {
            [cityIDS addObject: [TBXML textForElement:cityIdElement]];
            
        }
        
        
        cityDetailsElement = [TBXML nextSiblingNamed:@"City" searchFromElement: cityDetailsElement];
    }
    
    NSMutableString *cities=[[NSMutableString alloc]init];
    NSMutableString *sortCities=[[NSMutableString alloc]init];
    
    for (int i=0; i< [cityIDS count]; i++) {
        
        [cities appendFormat:@"%@,",[cityIDS objectAtIndex:i]];
        [sortedCitiIDs addObject:[cityIDS objectAtIndex:i]];
        
    }
    
    if ([cities hasSuffix:@","]) {
        [cities  setString:[cities substringToIndex:[cities length]-1]];
    }
    
    //changed for server caching
    
    [sortedCitiIDs sortUsingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
        return [str1 compare:str2 options:(NSNumericSearch)];
    }];
    for (int i=0; i<[sortedCitiIDs count]; i++) {
        [sortCities appendFormat:@"%@,",[sortedCitiIDs objectAtIndex:i]];
        
    }
    
    if ([sortCities hasSuffix:@","]) {
        [sortCities  setString:[sortCities substringToIndex:[sortCities length]-1]];
    }
    
    [defaults setObject:sortCities forKey:@"sortedCitiIDs"];
}


#pragma mark - Service Respone methods

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
 }


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        [NewsUtility pushViewFromHamberger:self];
        
    }
    else{
        [NewsUtility pushMainmenu:self];
    }
    NSLog(@"temp %@  email %@",tempPwdString,noEmailCountString);
    [self popUpForForgotEmail:tempPwdString :noEmailCountString];
    
}


-(void) request_updateDeviceDetails
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    if ([defaults valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    
    if ([defaults valueForKey:KEY_DEVICEID]) {
        [parameters setValue:[defaults valueForKey:KEY_DEVICEID] forKey:@"deviceId"];
    }
    
    [parameters setValue:@"IOS" forKey:@"platform"];
    
    if (AppVersion) {
        [parameters setValue:AppVersion forKey:@"appVersion"];
    }
    [parameters setValue:[[UIDevice currentDevice] systemVersion]forKey:@"osVersion"];
    
    DLog(@"parametr: %@",parameters);
    
    iWebRequestState = UPDATE_DEVICE;
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/updatedevicedetails",BASE_URL];
    
    DLog(@"Url: %@",urlString);
    [client sendRequest: parameters : urlString];
    
}

/*
 *
 Parsing logic for login screen
 *
 */
-(void)parseLoginScreenData:(NSString *) response
{
    DLog(@"response %@",response);
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
        return;
    
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    int responseCode = [[TBXML textForElement:saveResponseCode] intValue];
    if(responseCode == 10000)
    {
        TBXMLElement *userIdElement = [TBXML childElementNamed:KEY_USERID parentElement:tbXml.rootXMLElement];
        
        TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:KEY_HUBCITIID parentElement:tbXml.rootXMLElement];
        TBXMLElement *hubCitiNameElement = [TBXML childElementNamed:KEY_HUBCITINAME parentElement:tbXml.rootXMLElement];
        TBXMLElement * isTempUserElement = [TBXML childElementNamed:@"isTempUser" parentElement:tbXml.rootXMLElement];
        TBXMLElement * noEmailCountElement = [TBXML childElementNamed:@"noEmailCount" parentElement:tbXml.rootXMLElement];
        
        
        if ([RegionApp isEqualToString:@"1"]) {
            [self sortCitiId : tbXml];
        }
        
        if (![[TBXML textForElement:userIdElement] isEqualToString:[defaults objectForKey:KEY_USERID]]) {
            
            [defaults setBool:NO forKey: @"UpdateDeviceDetails"];
            
        }
        else{
            [defaults setBool:YES forKey: @"UpdateDeviceDetails"];
        }
        
        
        //  [defaults setObject:sortCities forKey:@"sortedCitiIDs"];
       
        [defaults  setObject:[TBXML textForElement:userIdElement] forKey:KEY_USERID];
        [defaults  setObject:[TBXML textForElement:hubCitiIdElement] forKey:KEY_HUBCITIID];
        [defaults  setObject:[TBXML textForElement:hubCitiNameElement] forKey:KEY_HUBCITINAME];
         [NewsUtility setUserSettings];
         if(!userIdentifier){
//             [SharedManager setGps_allow_flag:NO];
//             [defaults setValue:@"NO" forKey:@"allowingGPS"];
         }
        if ([defaults boolForKey:@"canDisplayAPNSListScreen"]==YES && userIdentifier==FALSE) {
            //[defaults setBool:YES forKey:@"hideBackBtn"];
            [defaults setBool:YES forKey:@"fromLogin"];
            [self sendResetBadgeRequest];
            APNSListViewController *pushotifyList = [[APNSListViewController alloc] initWithNibName:@"APNSListViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:pushotifyList animated:NO];
            //[pushotifyList release];
            [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
            [self popUpForForgotEmail:[TBXML textForElement:isTempUserElement] :[TBXML textForElement:noEmailCountElement]];
        }
        
        else if (userIdentifier==FALSE && [defaults boolForKey:@"fromShareLink"]==YES) {
            //[defaults setBool:YES forKey:@"hideBackBtn"];
            [defaults setBool:YES forKey:@"fromLogin"];
            [defaults setBool:NO forKey:@"fromShareLink"];
            WebBrowserViewController *webPage = [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:webPage animated:NO];
            [self popUpForForgotEmail:[TBXML textForElement:isTempUserElement] :[TBXML textForElement:noEmailCountElement]];
            // [webPage release];
        }
        
        else{
            //Navigate to main menu page
                        NSString* customerId = [defaults objectForKey:@"currentuserId"];
            if (customerId) {
                
                if (![customerId isEqualToString:[defaults valueForKey:KEY_USERID]]) {
                    
                    ReleaseAndNilify(cashedResponse);
                    [defaults setValue: nil forKey: @"currentuserId"];
                }
                else {
                    isSameUser = true;
                }
            }
            
            if (![defaults boolForKey:@"UpdateDeviceDetails"]) {
                [HubCitiAppDelegate showActivityIndicator];
                [defaults setBool:YES forKey: @"UpdateDeviceDetails"];
                NSLog(@"temp %@  email %@",[TBXML textForElement:isTempUserElement],[TBXML textForElement:noEmailCountElement]);
                tempPwdString = [TBXML textForElement:isTempUserElement];
                noEmailCountString = [TBXML textForElement:noEmailCountElement];
                [self request_updateDeviceDetails];
                
                
            }
            else
            {
                
                
                if([defaults boolForKey:@"newsTemplateExist"]){
                    
                    [NewsUtility pushViewFromHamberger:self];
                    
                }
                else{
                    
                   [NewsUtility pushMainmenu:self];
                }
                [self popUpForForgotEmail:[TBXML textForElement:isTempUserElement] :[TBXML textForElement:noEmailCountElement]];
                
            }
            
            if ([defaults boolForKey:@"canDisplayAPNSListScreen"]==YES && userIdentifier) {
                [UtilityManager showAlert:nil msg:@"Please Sign up to view the Notification" onView:self];
                if ([[UIApplication sharedApplication]applicationIconBadgeNumber])
                    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
                //  [self popUpForForgotEmail:[TBXML textForElement:isTempUserElement] :[TBXML textForElement:noEmailCountElement]];
                //[alert1 release];
            }
            
            else if (userIdentifier && [defaults boolForKey:@"fromShareLink"]==YES) {
                [UtilityManager showAlert:nil msg:@"Please Sign up to view the Share page" onView:self];
                // [self popUpForForgotEmail:[TBXML textForElement:isTempUserElement] :[TBXML textForElement:noEmailCountElement]];
                //  [alert1 release];
            }

        }
        
    }
    
    else
    {
        loginButton.enabled = YES;
        TBXMLElement *msgElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:msgElement]];
        return;
        
    }
    loginButton.enabled = YES;
}


-(void) sendResetBadgeRequest
{
    
    NSString* userid = [defaults valueForKey:KEY_USERID];
    NSString *urlString;
    if (userid.length > 0) {
        
        urlString = [NSString stringWithFormat:@"%@firstuse/resetbadge?hubCitiId=%@&deviceId=%@&userId=%@",BASE_URL,[defaults objectForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_USERID]];
    }
    else{
        urlString = [NSString stringWithFormat:@"%@firstuse/resetbadge?hubCitiId=%@&deviceId=%@",BASE_URL,[defaults objectForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_DEVICEID]];
        
    }
    
    
    
    NSString* responseJson = [ConnectionManager establishGetConnectionforJsonData:urlString];
    NSData *data = [responseJson dataUsingEncoding:NSUTF8StringEncoding];
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    
    if (responseCode  == 10000) {
        
        if ([responseText isEqualToString:@"Success"]) {
            
            [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
            NSLog(@"Success");
        }
    }
    
}



-(void) popUpForForgotEmail : (NSString*) tempUserFlag : (NSString *) emailCountFlag
{
    if ([tempUserFlag isEqualToString:@"1"] )
    {
        //            [[NSNotificationCenter defaultCenter]
        //             postNotificationName:@"tempPassword"
        //             object:self];
        
        NSNotification *notification;
        [NewsUtility tempPasswordDisplay: notification onViewController:[[[[UIApplication sharedApplication] delegate] window] rootViewController]];
    }
    if ([emailCountFlag isEqualToString:@"1"] )
    {
        //            [[NSNotificationCenter defaultCenter]
        //             postNotificationName:@"emailCount"
        //             object:self];
        
        NSNotification *notification;
        [NewsUtility emailCountDisplay: notification onViewController:[[[[UIApplication sharedApplication] delegate] window] rootViewController]];
    }
}

/*
 *
 asynchronous req/resp delegate method
 *
 */
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case LOGIN_AUTHENTICATEUSER:
            [self parseLoginScreenData:response];
            break;
            
        case LOGIN_FORGOTPASSWORD:
            [self parseForgotPassword:response];
            break;
            
        case RESETPASSWORD:
            [self parseResetPassword : response];
            break;
            
            
        default:
            break;
    }
}

/*
 *
 Logic for Sign up button click
 *
 */
-(void) request_ResetPassword
{
    
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserRegistrationInfo><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [xmlStr appendFormat:@"<hubCitiKey>%@</hubCitiKey>",[defaults valueForKey:HUBCITIKEY]];
    [xmlStr appendFormat:@"<password>%@</password></UserRegistrationInfo>", resetPasswordString];
    // NSString *urlString = [NSString stringWithFormat:@"http://sdw2730:8080/HubCiti2.6/firstuse/v2/resetpassword"];
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/resetpassword",BASE_URL];
    NSLog(@"XMl string %@",xmlStr);
    
    [connectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    ReleaseAndNilify(xmlStr);
    
}

-(void) parseResetPassword :(NSString *) response
{
    if ([UtilityManager isNullOrEmptyString:response]) {
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    
    if ([TBXML textForElement:responseCodeElement] != nil)
    {
        
        //        UIAlertController * alert=[UIAlertController alertControllerWithTitle:[TBXML textForElement:responseTextElement] message:nil preferredStyle:UIAlertControllerStyleAlert];
        //
        //
        //        UIAlertAction* ok = [UIAlertAction
        //                             actionWithTitle:@"OK"
        //                             style:UIAlertActionStyleDefault
        //                             handler:^(UIAlertAction * action)
        //                             {
        //                                 [alert dismissViewControllerAnimated:YES completion:nil];
        //
        //                             }];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        NSLog(@"Reset Password : %@",[TBXML textForElement:responseTextElement]);
        
        //[alert release];
    }
    
}
-(IBAction) signUpClicked:(id) sender{
    
    RegistrationViewController *regView = [[RegistrationViewController alloc]initWithNibName:@"RegistrationViewController" bundle:[NSBundle mainBundle]];
    userIdentifier=FALSE;
    
    regView.signup_DO = login_DO;
    [self.navigationController pushViewController:regView animated:NO];
    ReleaseAndNilify(regView);
    
}


#pragma mark - Textfield delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    NSInteger nextTag = textField.tag + 1;
    //find next responder
    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
    
    if (nextResponder){
        //if there is next responder, set it
        [nextResponder becomeFirstResponder];
    }
    
    else{
        [textField resignFirstResponder];
    }
    
    if (![txtField_userName.text length] && ![txtField_password.text length]){
        [defaults setValue:nil forKey:@"Username"];
        [defaults setValue:nil forKey:@"Password"];
    }
    /*if(textField.tag == 1){
     [txtField_userName resignFirstResponder];
     [txtField_password becomeFirstResponder];
     }
     else if (textField.tag == 2){
     [txtField_password resignFirstResponder];
     
     }*/
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

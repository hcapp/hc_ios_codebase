
//
//  CommonUtility.m
//  HubCiti
//
//  Created by Ajit on 9/4/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "CommonUtility.h"
#import <QuartzCore/QuartzCore.h>
#import "MainMenuViewController.h"
#import "UIImageView+AFNetworking.h"
#import "SpecialOffersViewController.h"
#import "AnythingPage.h"
#import "HTTPClient.h"
#import "AppDelegate.h"
#import "WebBrowserViewController.h"

static CommonUtility *sharedUtilityManager = nil;

@implementation CommonUtility
//Get the shared instance and create it if necessary
+(id)utilityManager{
    @synchronized(self){
        if (sharedUtilityManager == nil){
            
            sharedUtilityManager = [[super allocWithZone:NULL]init];
        }
    }
    return sharedUtilityManager;
}

//We can still have a regular init method, that will get called first time the singleton is used
-(id)init{
    
    self = [super init];
    if (self) {
        // Work your initialising magic here as you normally would
    }
    
    return self;
}

// Your dealloc method will never be called, as the singleton survives for the duration of your app.
// However, I like to include it so I know what memory I'm using (and incase, one day, I convert away from Singleton).
-(void)dealloc
{
    // I'm never called!
    //[super dealloc];
}

// We don't want to allocate a new instance, so return the current one.
//+ (id)allocWithZone:(NSZone *)zone {
//    return [[self utilityManager] retain];
//}

// Equally, we don't want to generate multiple copies of the singleton.
- (id)copyWithZone:(NSZone *)zone {
    return self;
}

// Once again - do nothing, as we don't have a retain counter for this object.
//- (id)retain {
//    return self;
//}

// Replace the retain counter so we can never release this object.
//- (unsigned long)retainCount {
//    return UINT_MAX; //denotes an object that cannot be released
//}

// This function is empty, as we don't want to let the user release this object.
//- (oneway void)release {
//    // never release
//}

//Do nothing, other than return the shared instance - as this is expected from autorelease.
//- (id)autorelease {
//    return self;
//}

//GLOBAL METHODS
-(void) dismiss :(id) alert {
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

-(void) alertControllerDismiss: (id) alert
{
    [alert dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)popBackToViewController:(Class)viewControllerClass inNavigationController:(UINavigationController*)navigationController
{
    NSArray *viewControllers = [navigationController viewControllers];
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    BOOL flag = NO;
    if([defaults boolForKey:@"newsTemplateExist"]){
        for(int i = 0 ; i < [viewControllers count] ; ++i) {
            id viewController = [viewControllers objectAtIndex:i];
            
            if([viewController isKindOfClass:viewControllerClass]) {
                [navigationController popToViewController:viewController animated:NO];
                // flag = YES;
                break;
            }
        }
        
    }
    else{
        for(int i = 0 ; i < [viewControllers count] ; ++i) {
            id viewController = [viewControllers objectAtIndex:i];
            
            if([viewController isKindOfClass:viewControllerClass]) {
                if([viewController isKindOfClass:[MainMenuViewController class]])
                {
                    [SharedManager setScrollIndicator:0];
                    [SharedManager setSubmenuScrollIndicator:0];
                    // [defaults setValue:nil forKey:@"popBack"];
                }
                [navigationController popToViewController:viewController animated:NO];
                flag = YES;
                break;
            }
        }
        
        if (flag == NO) {
            MainMenuViewController *mainScreen = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController" bundle:[NSBundle mainBundle]];
            [SharedManager setScrollIndicator:0];
            [SharedManager setSubmenuScrollIndicator:0];
            
            //  [defaults setValue:nil forKey:@"popBack"];
            [navigationController pushViewController:mainScreen animated:NO];
        }
    }
}

-(void) showAlert
{
    [self showAlert:nil msg:@"Network Failure. Please try later"];
    
}




-(void) showAlert:(NSString*)message
{
    NSString *messageBody = @"";
    if(message && [message length]>0)
        messageBody = message;
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:messageBody message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleCancel
                         handler:nil];
    [alert addAction:ok];
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
}

-(BOOL)isNullOrEmptyString:(NSString *)str {
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ((str == nil) || ([str isEqualToString:@""]))
        return YES;
    else
        return NO;
}

-(BOOL)isResponseXMLNullOrEmpty:(NSString *)str {
    
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ((str == nil) || ([str isEqualToString:@""]))
    {
        //[self showAlert];
        return YES;
    }
    else {
        return NO;
    }
}


-(void)requestTimedOut
{
    [self showAlert:nil msg:@"request timed out."];
    
    
}


- (void)showAlertOnWindow:(NSString*)aTitle msg:(NSString*)aMsg{
    
    __block UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    alertWindow.rootViewController =  [[UIViewController alloc] init];
    
    UIWindow *topWindow = [UIApplication sharedApplication].windows.lastObject;
    alertWindow.windowLevel = topWindow.windowLevel + 1;
    
    [alertWindow makeKeyAndVisible];
    
    
    UIAlertController * alert;
    if (aTitle==nil) {
        alert=[UIAlertController alertControllerWithTitle:aMsg message:nil preferredStyle:UIAlertControllerStyleAlert];
    }
    else{
        alert=[UIAlertController alertControllerWithTitle:aTitle message:aMsg preferredStyle:UIAlertControllerStyleAlert];
    }
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleCancel
                         handler:^(UIAlertAction * action){
                             
                             alertWindow.hidden = YES;
                             alertWindow = nil;
                             
                             
                         }];
    [alert addAction:ok];
    [alertWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    
}

- (void)showAlert:(NSString*)aTitle msg:(NSString*)aMsg  onView:(UIViewController*)viewController{
    UIAlertController * alert;
    if (aTitle==nil) {
        alert=[UIAlertController alertControllerWithTitle:aMsg message:nil preferredStyle:UIAlertControllerStyleAlert];
    }
    else{
        alert=[UIAlertController alertControllerWithTitle:aTitle message:aMsg preferredStyle:UIAlertControllerStyleAlert];
    }
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleCancel
                         handler:nil];
    [alert addAction:ok];
    [viewController presentViewController:alert animated:YES completion:nil];
    
}


- (void)showAlert:(NSString*)aTitle msg:(NSString*)aMsg {
    UIAlertController * alert;
    if (aTitle==nil) {
        alert=[UIAlertController alertControllerWithTitle:aMsg message:nil preferredStyle:UIAlertControllerStyleAlert];
    }
    else{
        alert=[UIAlertController alertControllerWithTitle:aTitle message:aMsg preferredStyle:UIAlertControllerStyleAlert];
    }
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleCancel
                         handler:nil];
    [alert addAction:ok];
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
    
}

-(UIButton*)customizeBarButtonItem:(NSString*)title{
    
    CGSize stringsize = [title sizeWithAttributes: @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:13.0f]}];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]] forState:UIControlStateNormal];
    //[button setTitleColor:[UIColor convertToHexString:@"#0091E6"] forState:UIControlStateHighlighted];
    button.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13.0f];
    [button.layer setCornerRadius:5.0f];
    [button.layer setMasksToBounds:YES];
    //[button.layer setBorderWidth:0.8f];
    //[button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [button setAccessibilityLabel:title];
    //button.frame=CGRectMake(100.0, 0.0, 78.0, 30.0);
    [button setFrame:CGRectMake(0,0,stringsize.width+20, 30.0)];
    return button;
}


-(UIButton*)customizeBackButton{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setAccessibilityLabel:@"Back"];
    button.frame=CGRectMake(0.0, 0.0, 30.0, 30.0);
    
    SdImageView *backImage= [[SdImageView alloc]initWithFrame:button.frame];
    [backImage loadImage:[[defaults valueForKey:@"bkImgPath"] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
    
    [button addSubview:backImage];
    
    // [button setBackgroundImage:[UIImage imageNamed:@"backBtn_up.png"] forState:UIControlStateHighlighted];
    //    [button setTitle:@"Back" forState:UIControlStateNormal];
    //    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    button.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
    return button;
}

-(UIButton*)customizeBackButtonSubCatColor{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame=CGRectMake(6.0, 8.0, 30.0, 30.0);
    return button;
}


-(void) setTransperentImageColor:(NSString*)imageColor forButton:(UIButton*)buttonTransperent{
    
    UIImage *image = [[UIImage imageNamed:@"back_icon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [buttonTransperent setImage:image forState:UIControlStateNormal];
    buttonTransperent.tintColor = [UIColor convertToHexString:imageColor];
}

-(void) showFormatedAlert: (NSString *) responseTextStr{
    
    
    UIAlertController * alert;
    
    alert=[UIAlertController alertControllerWithTitle:nil message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleCancel
                         handler:nil];
    [alert addAction:ok];
    
    
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 150)];
    
    
    
    NSArray *responseArray = [responseTextStr componentsSeparatedByString:@"."];
    
    NSString *firstString =[responseArray objectAtIndex:0];
    
    NSRange boldFirstRange = [firstString rangeOfString:firstString];
    
    NSMutableAttributedString *firstAttributedString = [[NSMutableAttributedString alloc] initWithString:firstString];
    
    
    
    [firstAttributedString addAttribute: NSFontAttributeName value:[UIFont boldSystemFontOfSize:16] range:boldFirstRange];
    
    
    
    
    
    
    
    NSString *secondString =[responseArray objectAtIndex:1];
    
    NSRange secondRange = [secondString rangeOfString:secondString];
    
    NSMutableAttributedString *secondAttributedString = [[NSMutableAttributedString alloc] initWithString:secondString];
    
    [secondAttributedString addAttribute: NSFontAttributeName value:[UIFont systemFontOfSize:16] range:secondRange];
    
    
    
    
    
    NSString *thirdString =[responseArray objectAtIndex:2];
    
    NSRange boldSecondRange = [thirdString rangeOfString:thirdString];
    
    NSMutableAttributedString *thirdAttributedString = [[NSMutableAttributedString alloc] initWithString:thirdString];
    
    
    
    [thirdAttributedString addAttribute: NSFontAttributeName value:[UIFont boldSystemFontOfSize:16] range:boldSecondRange];
    
    
    
    
    
    
    
    NSAttributedString *atrStr = [[NSAttributedString alloc] initWithString:@"\n"];
    
    NSAttributedString *dotStr = [[NSAttributedString alloc] initWithString:@"."];
    
    [firstAttributedString appendAttributedString:dotStr];
    
    [firstAttributedString appendAttributedString:atrStr];
    
    [firstAttributedString appendAttributedString:secondAttributedString];
    
    [firstAttributedString appendAttributedString:dotStr];
    
    [firstAttributedString appendAttributedString:atrStr];
    
    [firstAttributedString appendAttributedString:thirdAttributedString];
    
    [firstAttributedString appendAttributedString:dotStr];
    
    
    
    
    
    
    
    lbl.attributedText = firstAttributedString;
    
    lbl.numberOfLines=0;
    
    lbl.textAlignment = NSTextAlignmentCenter;
    
    [alert setValue:firstAttributedString forKey:@"attributedMessage"];
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
    //[alert show];
    
    //[alert release];
    
    
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation

{
    
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

- (BOOL)shouldAutorotate{
    
    return NO;
    
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    
    return UIInterfaceOrientationPortrait;
    
}



- (UIInterfaceOrientationMask)supportedInterfaceOrientations

{
    
    return UIInterfaceOrientationMaskPortrait;
    
}

-(void)customUrlLoad:(NSURL*)url{
    
    NSString *urlString =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:4]];
    if([urlString isEqualToString:@"id967334859"]){
        NSString *customURL = @"tyler://";
        
        if ([[UIApplication sharedApplication]
             canOpenURL:[NSURL URLWithString:customURL]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
        }
        else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/tyler-tx-news/id967334859?mt=8"]];
        }
    }
    
    else if([urlString isEqualToString:@"id850405764"]){
        NSString *customURL = @"marblefalls://";
        
        if ([[UIApplication sharedApplication]
             canOpenURL:[NSURL URLWithString:customURL]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
        }
        else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/marble-falls/id850405764?mt=8"]];
        }
    }
    else if([urlString isEqualToString:@"id878665448"]){
        NSString *customURL = @"rockwallArea://";
        
        if ([[UIApplication sharedApplication]
             canOpenURL:[NSURL URLWithString:customURL]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
        }
        else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/rockwall-area/id878665448?mt=8"]];
        }
    }
    else if([urlString isEqualToString:@"id975791074"]){
        NSString *customURL = @"addison://";
        
        if ([[UIApplication sharedApplication]
             canOpenURL:[NSURL URLWithString:customURL]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
        }
        else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/addison/id975791074?mt=8"]];
        }
    }
    else if([urlString isEqualToString:@"id1051296880"]){
        NSString *customURL = @"centex://";
        
        if ([[UIApplication sharedApplication]
             canOpenURL:[NSURL URLWithString:customURL]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
        }
        else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/central-tx-news/id1051296880?mt=8"]];
        }
    }
    
}


-(NSMutableCharacterSet *) getAllowedNSCharacterSet{
    
    NSMutableCharacterSet *allowedCharacters = [[NSMutableCharacterSet alloc] init];
    [allowedCharacters formUnionWithCharacterSet:[NSCharacterSet URLHostAllowedCharacterSet]];
    [allowedCharacters formUnionWithCharacterSet:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    [allowedCharacters formUnionWithCharacterSet:[NSCharacterSet URLPathAllowedCharacterSet]];
    [allowedCharacters formUnionWithCharacterSet:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    return allowedCharacters;
    
}

-(void) req_hubcitiAnyThingPage : (UIViewController*) viewCont
{
    desViewController = viewCont;
    NSMutableDictionary* param = [[NSMutableDictionary alloc] init];
    
     iWebRequestState = HubcitiAnythingInfo;
     
     NSString *urlString = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfojson",BASE_URL];
     NSLog(@"url = %@", urlString);
     [param setValue:[linkID objectAtIndex:[linkID count]-1] forKey:@"pageId"];
     [defaults setObject:[linkID objectAtIndex:[linkID count]-1]forKey:KEY_ANYPAGEID];
     
     [param setValue:@"IOS" forKey:@"platform"];
     [param setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
     
     if ([defaults valueForKey:KEY_MITEMID])
     [param setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
     if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
     [param setValue:[defaults valueForKey:KEY_BOTTOMBUTTONID] forKey:@"bottomBtnId"];
     
     
     [HubCitiAppDelegate showActivityIndicator];
     NSLog(@"param = %@", param);
     HTTPClient *client = [HTTPClient sharedHTTPClient];
     client.delegate = self;
     [client sendRequest:param :urlString];
    
    /*iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=TRUE;
    anythingShareFlag=YES;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    [defaults setObject:[linkID objectAtIndex:[linkID count]-1]forKey:KEY_ANYPAGEID];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];*/
    
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        
        
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        //        NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
        //         NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
        //        if([name isEqualToString:@"3000.htm"]){
        //            [HubCitiAppDelegate setIsLogistics:YES];
        //
        //        }
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        //        TBXMLElement *retailId = [TBXML childElementNamed:@"retailerId" parentElement:tbxml.rootXMLElement];
        //        if (retailId!=Nil) {
        //            [defaults setObject:[TBXML textForElement:retailId] forKey:KEY_RETAILERID];
        //            NSLog(@"The value is %@",[defaults valueForKey:KEY_RETAILERID]);
        //        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString] containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [desViewController performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [desViewController presentViewController:alert animated:YES completion:nil];
                    
                    
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [desViewController presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [desViewController.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc]init];
            [desViewController.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [desViewController.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}


-(void) Parse_AnyThingPage : (NSDictionary*) responseData
{
    
    DLog(@"%@",responseData);
    
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    
    if (responseCode != 10000) {
        [UtilityManager showAlert:nil msg:responseText];
        return;
    }
    else{
        [defaults  setValue:[responseData objectForKey:@"mainMenuId"] forKey:KEY_MAINMENUID];
        if (![[responseData objectForKey:@"pageLink"] isEqualToString:@"N/A"]) {
            NSURL *url = [NSURL URLWithString:(NSString*)[responseData objectForKey:@"pageLink"]];
            [defaults setValue:[responseData objectForKey:@"pageLink"] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [desViewController performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    
                    [desViewController presentViewController:alert animated:YES completion:nil];
                    
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                    if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [desViewController presentViewController:splOfferVC animated:YES completion:nil];
                }
                
                
            }
            else{
                [defaults setValue:[responseData objectForKey:@"pageLink"] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [desViewController.navigationController pushViewController:urlDetail animated:NO];
                
            }
            
        }
        
        else if ([[responseData objectForKey:@"mediaPath"] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:responseData forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc]init];
            [desViewController.navigationController pushViewController:anythingPage animated:NO];
            
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[responseData objectForKey:@"mediaPath"] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [desViewController.navigationController pushViewController:urlDetail animated:NO];
        }
    }
    [linkID removeLastObject];
}

#pragma mark - Service Respone methods

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
    
}


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    // Update Device Details successfully sent to the server
    [HubCitiAppDelegate removeActivityIndicator];
    [self responseData:responseObject];
    
}

-(void)responseData:(NSString *) response
{
    NSDictionary* resp = (NSDictionary*) response;
    switch (iWebRequestState) {
        case HubcitiAnythingInfo:
            
            [self Parse_AnyThingPage:resp];
            break;
            
        default:
            break;
    }
  
}


@end

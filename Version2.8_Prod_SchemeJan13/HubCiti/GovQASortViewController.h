//
//  GovQASortViewController.h
//  HubCiti
//
//  Created by Ashika on 9/2/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MakeRequestViewController.h"
#import "SortAndFilter.h"

@interface GovQASortViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    UITableView *tblGroupingNSorting;
    NSMutableArray *arrFilters;
    NSMutableArray *arrSorting;
    int filterSelectionVal;
    int sortSelectionval;
    BOOL sortAlphabet, sortType, sortName;

}

@property(nonatomic)BOOL sortAlphabet;
@property(nonatomic)BOOL sortType;
@property(nonatomic)BOOL sortName;
//@property(nonatomic,retain) MakeRequestViewController *makeObj;
@property(nonatomic,strong) SortAndFilter *sortObj;

@end

//
//  GovQAForgotPasswordViewController.m
//  HubCiti
//
//  Created by Ashika on 8/19/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "GovQAForgotPasswordViewController.h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"

#import "SingleCatRetailers.h"
#import "EventsListViewController.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "FindViewController.h"
#import "CityExperienceViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "SettingsViewController.h"
#import "MainMenuViewController.h"
#import "FilterListViewController.h"
#import "FilterRetailersList.h"
#import "CouponsViewController.h"
#import "FAQCategoryList.h"
#import "FundraiserListViewController.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "PreferredCategoriesScreen.h"
#import "WebBrowserViewController.h"
#import "AnythingPage.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "UserInformationViewController.h"
#import "CouponsInDealViewController.h"


@interface GovQAForgotPasswordViewController ()

@end
NSDictionary *viewsDictionary;
@implementation GovQAForgotPasswordViewController

@synthesize  txtField_emailId,anyVC,emailSendingVC;
@synthesize arrBottomButtonDO,activityIndicator;

#pragma mark - View life cycle methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) textFieldDidBeginEditing:(UITextField *)textField{
    
}
-(void) textFieldDidEndEditing:(UITextField *)textField{
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    
    self.navigationItem.title = @"Forgot Password";
    //ReleaseAndNilify(titleLabel);
    
    NSString *urlString = [NSString stringWithFormat:@"http://mygovhelp.com/ADDISONTX/_cs/ForgotPassword.aspx"];
    UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0 , SCREEN_WIDTH, SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT)];
    
    webview.delegate = self;
    webview.backgroundColor = [UIColor clearColor];
    webview.scalesPageToFit = YES;
    //self.automaticallyAdjustsScrollViewInsets = NO;
    //webview.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    webview.allowsInlineMediaPlayback = YES;
    webview.backgroundColor=[UIColor clearColor];
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame = CGRectMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2, 20, 20);
    [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    //[HubCitiAppDelegate showActivityIndicator];
    [self.view addSubview:webview];
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        
        [self activityIndicatorConstraints];
    }
    [self.view addSubview:activityIndicator];

//
//    [self designForgotPassword];
//    if([arrBottomButtonDO count] > 0)
//        [self setBottomBarMenu];

}
-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}
-(void)activityIndicatorConstraints
{
    NSDictionary *viewsDictionary = @{@"activity":activityIndicator };
    activityIndicator.translatesAutoresizingMaskIntoConstraints=NO;
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-512-[activity]" options:0 metrics:nil views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-384-[activity]" options:0 metrics:nil views:viewsDictionary]];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    [activityIndicator startAnimating];
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    //[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [activityIndicator stopAnimating];
    //[[NSURLCache sharedURLCache] removeAllCachedResponses];
  //  [HubCitiAppDelegate removeActivityIndicator];
    
}
- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [activityIndicator stopAnimating];
    // [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
  //  [HubCitiAppDelegate removeActivityIndicator];
}

-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}


- (void)dealloc
{
   
}


@end

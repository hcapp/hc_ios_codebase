//
//  GovQALoginPageViewController.m
//  HubCiti
//
//  Created by Ashika on 8/17/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "GovQALoginPageViewController.h"
#import "GovQAForgotPasswordViewController.h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "HubCitiConstants.h"
#import "AppDelegate.h"
#import "ReportProblemViewController.h"
#import "SpecialOffersViewController.h"
#import "SingleCatRetailers.h"
#import "EventsListViewController.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "FindViewController.h"
#import "CityExperienceViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "SettingsViewController.h"
#import "MainMenuViewController.h"
#import "FilterListViewController.h"
#import "FilterRetailersList.h"
#import "CouponsViewController.h"
#import "FAQCategoryList.h"
#import "FundraiserListViewController.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "PreferredCategoriesScreen.h"
#import "WebBrowserViewController.h"
#import "AnythingPage.h"
#import "CreateEditAccountViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "UserInformationViewController.h"
#import "HTTPClient.h"
#import "GetUserInfoResponse.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponsInDealViewController.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.9;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

@interface GovQALoginPageViewController (){
     int topHeight;
    int topsecondHeight;
    int topThirdHeight;
    BOOL isKeyBoradOpen;
   bottomButtonView *view;
}
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@end
CGRect topImgRect,topTextLabelRect,topThirdTextLabelRect,emailLabelRect,txtField_eamilAddressRect,pwdLabelRect,txtField_passwordRect,forgotPwdButtonRect,forgotPwdLabelRect,remMeButtonRect,remMeLabelRect,loginButtonRect,signUpButtonRect;//topsecondTextLabelRect,
NSDictionary *viewsDictionary;


@implementation GovQALoginPageViewController

@synthesize txtField_eamilAddress, txtField_password;
@synthesize guestUser,guestPassword;
//@synthesize arrBottomButtonDO,anyVC;
@synthesize infoResponse;
@synthesize arrBottomButtonDO,anyVC,emailSendingVC;

#pragma mark - View life cycle methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Service Respone methods



- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}





- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject

{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    
    
    
    [self responseData:responseObject];
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 360, 40)];
//    titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleLabel.textColor = [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]];
//    titleLabel.backgroundColor = [UIColor clearColor];
//    [titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
//    titleLabel.font = [UIFont boldSystemFontOfSize:16];
//    titleLabel.text = NSLocalizedString(@"GovQA Login",@"GovQA Login");
    self.navigationItem.title = @"GovQA Login";
   // ReleaseAndNilify(titleLabel);

    
    [self designLoginView];
    
    if([arrBottomButtonDO count] > 0)
        [self setBottomBarMenu];
    
    isRemember =  [defaults  boolForKey:@"govQARememberMe"];
    if (isRemember)
    {
        //save the user email and password for "rememeber me"
        txtField_eamilAddress.text = [defaults  objectForKey:@"govQAEmailId"];
        txtField_password.text = [defaults  objectForKey:@"govQAPassword"];
        [remMeButton setImage:[UIImage imageNamed:@"checkboxOn.png"]  forState:UIControlStateNormal];
    }

}
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HideKeyBard:) name:@"HIDE" object:nil];
    isRemember=[defaults boolForKey:@"govQARememberMe"];
    if ([defaults boolForKey:@"govQARememberMe"]==YES)
        
        {
        [remMeButton setImage:[UIImage imageNamed:@"checkboxOn.png"]  forState:UIControlStateNormal];
        isRemember = YES;
        txtField_eamilAddress.text = [defaults  objectForKey:@"govQAEmailId"];
        txtField_password.text = [defaults  objectForKey:@"govQAPassword"];
        
    }
    else {
        [remMeButton setImage:[UIImage imageNamed:@"checkboxOff.png"]  forState:UIControlStateNormal];
        txtField_eamilAddress.text = @"";
        txtField_password.text = @"";
        if (![txtField_eamilAddress.text length] || ![txtField_password.text length]){
            [defaults setValue:nil forKey:@"govQAEmailId"];
            [defaults setValue:nil forKey:@"govQAPassword"];
        }
    }
//    if (isRemember) {
//        
//        txtField_eamilAddress.text = [defaults  objectForKey:@"govQAEmailId"];
//        txtField_password.text = [defaults  objectForKey:@"govQAPassword"];
//        [remMeButton setImage:[UIImage imageNamed:@"checkboxOn.png"]  forState:UIControlStateNormal];
//    }
   
    
}
-(void)animateTextView: (BOOL)up
{
    
    const int movementDistance = -80;
    const float movementDuration = 0.3f;
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)deregisterForKeyboardNotifications {
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:@"HIDE" object:nil];
   // [center removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
  
        if (isKeyBoradOpen) {
            isKeyBoradOpen = false;
            [self animateTextView:NO];
        }
    
    [self deregisterForKeyboardNotifications];
}

// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    // Calculate Y position for View
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [arrBottomButtonDO count]; btnLoop++)
    {
        bottomButtonDO *obj_eventBottomDO = [arrBottomButtonDO objectAtIndex:btnLoop];
        
        view = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, 80,bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
            

            
        }
        else
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrBottomButtonDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrBottomButtonDO count],bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_eventBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
    }
}
- (void)rememberMeClicked {
    
    if (isRemember) {
        
        [remMeButton setImage:[UIImage imageNamed:@"checkboxOff.png"]  forState:UIControlStateNormal];
        [remMeButton setAccessibilityValue:@"NO"];
          [defaults  setBool:NO forKey:@"govQARememberMe"];
        isRemember = false;
    }
    else {
        
        [remMeButton setImage:[UIImage imageNamed:@"checkboxOn.png"]  forState:UIControlStateNormal];
        [remMeButton setAccessibilityValue:@"YES"];
        
        [defaults  setBool:YES forKey:@"govQARememberMe"];
        isRemember = YES;
        
    }
    
}

- (IBAction)submitClicked:(id)sender{
 [self dismissKeyBoard];
//[self animateTextView:NO];
    if([Network currentReachabilityStatus]==0){
        [UtilityManager showAlert];

    }
    else{
        
        txtField_eamilAddress.text=[txtField_eamilAddress.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if(![txtField_eamilAddress.text length] || ![txtField_password.text length]) {
            
            [UtilityManager showAlert:nil msg:NSLocalizedString(@"Fields cannot be empty",@"Fields cannot be empty")];
            loginButton.enabled = YES;
        }
        else
        {
            
            
            if (isRemember)
            {
                //save the user email and password for "rememeber me"
                [defaults  setObject:txtField_eamilAddress.text forKey:@"govQAEmailId"];
                [defaults  setObject:txtField_password.text forKey:@"govQAPassword"];
                [defaults  setBool:YES forKey:@"govQARememberMe"];
                
            }
            
            else
            {
                [defaults  setBool:NO forKey:@"govQARememberMe"];
            }
            //end
            [self requestForGovQALogin];

    }
    
    }
}
-(void) requestForGovQALogin{
    
    [HubCitiAppDelegate showActivityIndicator];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    NSDictionary *parameters = @{@"customerEmail":txtField_eamilAddress.text,@"password":txtField_password.text,@"authKey":@"ifF34jauK;"};
    NSDictionary *dicParameters=[[NSDictionary alloc] initWithObjectsAndKeys:parameters,@"customer",nil];
    DLog(@"dicParameters %@", dicParameters);
    NSString *baseURL=[NSString stringWithFormat:@"%@govqa/authenticateuser",BASE_URL];

    [manager POST:baseURL parameters:dicParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [HubCitiAppDelegate removeActivityIndicator];
        [self parseGovQALoginResponse:responseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [HubCitiAppDelegate removeActivityIndicator];

        NSLog(@"Error: %@", error);
    }];
    
    
}

-(void) parseGovQALoginResponse:(id)responseObject{
    
    NSDictionary *responseData=responseObject;
    NSDictionary *response=[responseData objectForKey:@"response"];
    NSInteger responseCode=[[response objectForKey:RESPONSECODE] integerValue];
   
    NSString *responseText=[response objectForKey:RESPONSETEXT];
    
    if(responseCode==10000)
    {
        
        [HubCitiAppDelegate showActivityIndicator];
        [SharedManager setSessionId:[response objectForKey:@"sessionId"]];
        NSString* session = [SharedManager sessionId];
        DLog(@"session id : %@",session);
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        NSString *baseURL=[NSString stringWithFormat:@"%@govqa/customerinfo",BASE_URL];
        [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        NSDictionary *parametersCustomerInfo = @{@"customerEmail":txtField_eamilAddress.text,@"authKey":@"ifF34jauK;"};
        NSDictionary *dicParametersCustomerInfo=[[NSDictionary alloc] initWithObjectsAndKeys:parametersCustomerInfo,@"customer",nil];
        DLog(@"dicParameters %@", dicParametersCustomerInfo);
        
        [manager POST:baseURL parameters:dicParametersCustomerInfo success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            [HubCitiAppDelegate removeActivityIndicator];
            [self parseGovQACustomerInfo:responseObject];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [HubCitiAppDelegate removeActivityIndicator];
            
            NSLog(@"Error: %@", error);
        }];

        
    }
    else
    {
        [UtilityManager showAlert:nil msg:responseText];
        [defaults  setBool:NO forKey:@"govQARememberMe"];
    }
}

-(void) parseGovQACustomerInfo:(id)responseObject{
    
    NSDictionary *responseData=responseObject;
    NSDictionary *response=[responseData objectForKey:@"response"];
    NSInteger responseCode=[[response objectForKey:RESPONSECODE] integerValue];
   // NSInteger customerID=[[[response objectForKey:@"customer"] objectForKey:@"id"] integerValue];
    
 //   NSString *responseText=[response objectForKey:RESPONSETEXT];
    
    if(responseCode==10000)
    {
        [defaults setValue:[[response objectForKey:@"customer"] objectForKey:@"id"] forKey:@"govqaCustomerID"];
        DLog(@"value %@",[defaults valueForKey:@"govqaCustomerID"]);
        [defaults  setObject:txtField_eamilAddress.text forKey:@"govQAEmailId"];
        [defaults  setObject:txtField_password.text forKey:@"govQAPassword"];

        ReportProblemViewController *reportObj = [[ReportProblemViewController alloc]initWithNibName:@"ReportProblemViewController" bundle:[NSBundle mainBundle]];
        reportObj.arrBottomBtnDO = arrBottomButtonDO;
        [self.navigationController pushViewController:reportObj animated:YES];
        [defaults setValue:txtField_eamilAddress.text forKey:@"emailID"];
    }
    else
    {
        
        [SharedManager setSessionId:nil];
        [defaults  setBool:NO forKey:@"govQARememberMe"];
        
        
    }
}

-(void)designLoginView
{
    int height_offset = 10;
    int width_offset=10;
     if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
         loginView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, -40, SCREEN_WIDTH, SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT)];
              }
    else
    {
    loginView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, -40, SCREEN_WIDTH, SCREEN_HEIGHT)];
       }
    
    [loginView setBackgroundColor:[UIColor whiteColor]];
    
    self.view.backgroundColor = [UIColor whiteColor];
   
    float navHeight = self.navigationController.navigationBar.frame.size.height;
    

    //topTextLabel
    topTextLabel=[self createLabel:NSLocalizedString(@"FIRST TIME USERS OF THE ADDISON SUPPORT CENTER NEED TO CREATE AN ACCOUNT. CLICK ON THE CREATE ACCOUNT BUTTON BELOW",@"FIRST TIME USERS OF THE ADDISON SUPPORT CENTER NEED TO CREATE AN ACCOUNT. CLICK ON THE CREATE ACCOUNT BUTTON BELOW") rect:CGRectZero];

    topTextLabel.textColor = [UIColor blackColor];
    
    topTextLabel.textAlignment= NSTextAlignmentJustified;
    topTextLabel.numberOfLines=0;
    topTextLabel.lineBreakMode= NSLineBreakByWordWrapping;
    topTextLabel.backgroundColor =[UIColor clearColor];
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        topTextLabel.font=[UIFont boldSystemFontOfSize:12];
        topHeight=[self calculateLabelHeight:@"FIRST TIME USERS OF THE ADDISON SUPPORT CENTER NEED TO CREATE AN ACCOUNT. CLICK ON THE CREATE ACCOUNT BUTTON BELOW" labelWidth:VIEW_FRAME_WIDTH-X_OFFSET*2 fontSize:12];
    }
    else
    {
        topTextLabel.font=[UIFont boldSystemFontOfSize:18];
        topHeight=[self calculateLabelHeight:@"FIRST TIME USERS OF THE ADDISON SUPPORT CENTER NEED TO CREATE AN ACCOUNT. CLICK ON THE CREATE ACCOUNT BUTTON BELOW" labelWidth:VIEW_FRAME_WIDTH-X_OFFSET*2 fontSize:18];
    }
    
    topTextLabelRect = CGRectMake(X_OFFSET, navHeight, VIEW_FRAME_WIDTH-X_OFFSET*2, topHeight);
    
    
    //topsecondTextLabel
//    topsecondTextLabel=[self createLabel:NSLocalizedString(@"Please log in to an existing account or create a new account (see create New Account) to access this feature",@"Please log in to an existing account or create a new account (see create New Account) to access this feature") rect:CGRectZero];
//    
//    topsecondTextLabel.textColor = [UIColor blackColor];
//    
//    topsecondTextLabel.textAlignment= NSTextAlignmentJustified;
//    topsecondTextLabel.numberOfLines=0;
//    topsecondTextLabel.lineBreakMode= NSLineBreakByWordWrapping;
//    topsecondTextLabel.backgroundColor =[UIColor clearColor];
//    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
//        
//        topsecondTextLabel.font=[UIFont systemFontOfSize:12];
//        topsecondHeight=[self calculateLabelHeight:@"Please log in to an existing account or create a new account (see create New Account) to access this feature" labelWidth:VIEW_FRAME_WIDTH-X_OFFSET*2 fontSize:12];
//    }
//    else
//    {
//        topsecondTextLabel.font=[UIFont systemFontOfSize:18];
//        topsecondHeight=[self calculateLabelHeight:@"Please log in to an existing account or create a new account (see create New Account) to access this feature" labelWidth:VIEW_FRAME_WIDTH-X_OFFSET*2 fontSize:18];
//    }
//
//    //iPad conversion.
//    topsecondTextLabelRect = CGRectMake(X_OFFSET, topTextLabelRect.origin.y+topTextLabelRect.size.height+height_offset,VIEW_FRAME_WIDTH-X_OFFSET*2, topsecondHeight);
    
    
    //topThirdTextLabel
    
    topThirdTextLabel=[self createLabel:NSLocalizedString(@"To keep your information secure and to ensure we can send you a response to your inquiry, we require you to log into the Addison Support Center.  Please use your existing account (or create a new account) to access this feature.",@"To keep your information secure and to ensure we can send you a response to your inquiry, we require you to log into the Addison Support Center.  Please use your existing account (or create a new account) to access this feature.") rect:CGRectZero];
    
    topThirdTextLabel.textColor = [UIColor blackColor];
    
    topThirdTextLabel.textAlignment= NSTextAlignmentJustified;
    topThirdTextLabel.numberOfLines=0;
    topThirdTextLabel.lineBreakMode= NSLineBreakByWordWrapping;
    topThirdTextLabel.backgroundColor =[UIColor clearColor];
    
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        
        topThirdTextLabel.font=[UIFont systemFontOfSize:12];
        topThirdHeight=[self calculateLabelHeight:@"To keep your information secure and to ensure we can send you a response to your inquiry, we require you to log into the Addison Support Center.  Please use your existing account (or create a new account) to access this feature." labelWidth:VIEW_FRAME_WIDTH-X_OFFSET*2 fontSize:12];
    }
    else
    {
        topThirdTextLabel.font=[UIFont systemFontOfSize:18];
        topThirdHeight=[self calculateLabelHeight:@"To keep your information secure and to ensure we can send you a response to your inquiry, we require you to log into the Addison Support Center.  Please use your existing account (or create a new account) to access this feature." labelWidth:VIEW_FRAME_WIDTH-X_OFFSET*2 fontSize:18];
    }

    //iPad conversion.
    topThirdTextLabelRect = CGRectMake(X_OFFSET, topTextLabelRect.origin.y+topTextLabelRect.size.height+height_offset,VIEW_FRAME_WIDTH-X_OFFSET*2, topThirdHeight);
    
    
    
    emailLabel = [self createLabel:NSLocalizedString(@"Email Address",@"Email Address") rect:CGRectZero];
    emailLabel.textColor = [UIColor blackColor];
    emailLabel.font=[UIFont boldSystemFontOfSize:15];
    
    //iPad conversion.
    emailLabelRect = CGRectMake(X_OFFSET+2, topThirdTextLabelRect.origin.y+topThirdTextLabelRect.size.height+height_offset, 90, LABEL_HEIGHT);
    
    txtField_eamilAddress = [self createTxtField:NSLocalizedString(@"Email Address",@"Email Address") rect:CGRectZero];
    txtField_eamilAddress.tag = 1;
    txtField_eamilAddress.font=[UIFont systemFontOfSize:14];
    txtField_eamilAddress.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtField_eamilAddress.autocorrectionType = UITextAutocorrectionTypeNo;
    txtField_eamilAddress.clearButtonMode = UITextFieldViewModeWhileEditing;
    //txtField_eamilAddress.returnKeyType = UIReturnKeyNext;
    
    //iPad conversion.
    txtField_eamilAddressRect = CGRectMake(X_OFFSET,emailLabelRect.origin.y+emailLabelRect.size.height+5,VIEW_FRAME_WIDTH-X_OFFSET*2,TEXTFIELD_HEIGHT);
    
    pwdLabel = [self createLabel:NSLocalizedString(@"Password",@"Password") rect:CGRectZero];
    pwdLabel.textColor = [UIColor blackColor];
    pwdLabel.font=[UIFont boldSystemFontOfSize:15];
    
    //iPad conversion.
    pwdLabelRect = CGRectMake(X_OFFSET+2,txtField_eamilAddressRect.origin.y+txtField_eamilAddressRect.size.height+height_offset,90,LABEL_HEIGHT);
    
    txtField_password = [self createTxtField:NSLocalizedString(@"password",@"password") rect:CGRectZero];
    txtField_password.tag = 2;
    txtField_password.secureTextEntry = YES;
    txtField_password.font=[UIFont systemFontOfSize:14];
    txtField_password.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtField_password.autocorrectionType = UITextAutocorrectionTypeNo;
    [txtField_password setAccessibilityLabel:@"password"];
    txtField_password.clearButtonMode = UITextFieldViewModeWhileEditing;
    txtField_password.returnKeyType = UIReturnKeyDone;
    
    //iPad conversion.
    txtField_passwordRect = CGRectMake(X_OFFSET,pwdLabelRect.origin.y+pwdLabelRect.size.height+5,VIEW_FRAME_WIDTH-X_OFFSET*2,TEXTFIELD_HEIGHT);
    
    /* forgot password UI control */
    forgotPwdButton  = [UIButton buttonWithType:UIButtonTypeCustom];
    forgotPwdButton.titleLabel.font=[UIFont boldSystemFontOfSize:12];
    [forgotPwdButton setTitle:NSLocalizedString(@"Forgot Password?",@"Forgot Password?") forState:UIControlStateNormal];
    forgotPwdButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [forgotPwdButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    forgotPwdButton.backgroundColor =[UIColor clearColor];
    [forgotPwdButton setAccessibilityLabel:@"forgotPassword"];
    forgotPwdButton.showsTouchWhenHighlighted=YES;
    [forgotPwdButton addTarget:self action:@selector(forgotPwdClicked:) forControlEvents:UIControlEventTouchDown];
    
    //iPad conversion.
    forgotPwdButtonRect = CGRectMake(X_OFFSET,txtField_passwordRect.origin.y+txtField_passwordRect.size.height+height_offset,130,LABEL_HEIGHT);
    
    /* forgot password Label UI control */
    forgotPwdLabel = [[UILabel alloc]init];
    forgotPwdLabel.text = NSLocalizedString(@"________________", @"________________");
    forgotPwdLabel.textColor=[UIColor blackColor];
    forgotPwdLabel.font =[UIFont boldSystemFontOfSize:12];
    forgotPwdLabel.backgroundColor = [UIColor clearColor];
    
    /* Remember Me Button UI control - button image will change based on server response*/
    remMeButton  = [UIButton buttonWithType:UIButtonTypeCustom];
    [remMeButton setImage:[UIImage imageNamed:@"checkboxOff.png"]  forState:UIControlStateNormal];
    remMeButton.backgroundColor =[UIColor clearColor];
    [remMeButton setAccessibilityLabel:@"rememberMe"];
    [remMeButton addTarget:self action:@selector(rememberMeClicked) forControlEvents:UIControlEventTouchDown];
    
    //iPad conversion.
    remMeButtonRect = CGRectMake(X_OFFSET*2+forgotPwdButtonRect.size.width+18,txtField_passwordRect.origin.y+txtField_passwordRect.size.height+10,X_OFFSET,X_OFFSET);
    
    /* Remember Me Label UI control */
    remMeLabel = [[UILabel alloc]init];
    remMeLabel.text = NSLocalizedString(@"Remember me", @"Remember me");
    remMeLabel.textColor=[UIColor blackColor];
    remMeLabel.font =[UIFont boldSystemFontOfSize:12];
    remMeLabel.backgroundColor = [UIColor clearColor];
    
    //iPad conversion.
    remMeLabelRect = CGRectMake(X_OFFSET+remMeButtonRect.origin.x+width_offset,txtField_passwordRect.origin.y+txtField_passwordRect.size.height+height_offset,100,LABEL_HEIGHT);
    
    /*Login Button UI control */
    loginButton  = [UIButton buttonWithType:UIButtonTypeCustom];
    loginButton.layer.cornerRadius = 5.0f;
    loginButton.titleLabel.font=[UIFont boldSystemFontOfSize:14];
    loginButton.backgroundColor = [UIColor convertToHexString:@"#AAA8AA"];
    [loginButton setTitle:NSLocalizedString(@"Submit",@"Submit") forState:UIControlStateNormal];
    [loginButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [loginButton setAccessibilityLabel:@"submit"];
    [loginButton addTarget:self action:@selector(submitClicked:) forControlEvents:UIControlEventTouchDown];
    loginButton.showsTouchWhenHighlighted=YES;
    
    //iPad conversion.
    loginButtonRect = CGRectMake(X_OFFSET,forgotPwdButtonRect.origin.y+forgotPwdButtonRect.size.height+X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2,30);
    
    /* Sign up Button UI control */
    signUpButton  = [UIButton buttonWithType:UIButtonTypeCustom];
    signUpButton.layer.cornerRadius = 5.0f;
    signUpButton.backgroundColor = [UIColor convertToHexString:@"#AAA8AA"];
    signUpButton.titleLabel.font=[UIFont boldSystemFontOfSize:14];
    [signUpButton setTitle:NSLocalizedString(@"Create Account",@"Create Account") forState:UIControlStateNormal];
    [signUpButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [signUpButton setAccessibilityLabel:@"createaccount"];
    [signUpButton addTarget:self action:@selector(signUpClicked:) forControlEvents:UIControlEventTouchUpInside];
    signUpButton.showsTouchWhenHighlighted=YES;
    
    //iPad conversion.
    signUpButtonRect = CGRectMake(X_OFFSET,loginButtonRect.origin.y+loginButtonRect.size.height+X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2,30);
    
  
    
//    /*Guest Button*/
//    guestButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    guestButton.layer.cornerRadius = 5.0f;
//    guestButton.backgroundColor = [UIColor blackColor];
//    guestButton.titleLabel.font=[UIFont boldSystemFontOfSize:14];
//    [guestButton setTitle:NSLocalizedString(@"Guest Login",@"Guest Login") forState:UIControlStateNormal];
//    [guestButton setTag:2];
//    [guestButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//    [guestButton addTarget:self action:@selector(loginClicked:) forControlEvents:UIControlEventTouchDown];
//    guestButton.showsTouchWhenHighlighted=YES;
//    [guestButton setAccessibilityLabel:@"guest"];
    /* Powered By Image UI control */
    
    
    poweredByImg = [[SdImageView alloc]init];
    [poweredByImg setBackgroundColor:[UIColor clearColor]];
    [poweredByImg setContentMode:UIViewContentModeScaleAspectFit];
    
   [poweredByImg loadImagefrommainBundel:@"govqa_logo.png"];//load default image
    
    
    txtField_eamilAddress.delegate = self;
    txtField_password.delegate = self;
    
    
    [loginView addSubview:topTextLabel];
   // [loginView addSubview:topsecondTextLabel];
    [loginView addSubview:topThirdTextLabel];
    [loginView addSubview:emailLabel];
    [loginView addSubview:pwdLabel];
    [loginView addSubview:txtField_password];
    [loginView addSubview:txtField_eamilAddress];
    [loginView addSubview:forgotPwdButton];
    [loginView addSubview:remMeButton];
    [loginView addSubview:remMeLabel];
    [loginView addSubview:loginButton];
    [loginView addSubview:signUpButton];
    [loginView addSubview:poweredByImg];
    [loginView addSubview:forgotPwdLabel];
    
    // Call this function to set constraints on views.
     [self setConstraints:height_offset width_offset:width_offset navHeight:navHeight];
     if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
    //loginView.showsVerticalScrollIndicator=YES;
    loginView.scrollEnabled=YES;
    loginView.userInteractionEnabled=YES;
     }
    [self.view addSubview:loginView];
    
    
    ReleaseAndNilify(powerByLabel);
    ReleaseAndNilify(poweredByImg);
    ReleaseAndNilify(remMeLabel);
    ReleaseAndNilify(forgotPwdLabel);
}

-(void) signUpClicked : (id) sender
{
    
    CreateEditAccountViewController* createAccount = [[CreateEditAccountViewController alloc]init] ;
     createAccount.arrayBottomButtonID = arrBottomButtonDO;
     createAccount.isEditScreen = false;
   
    [defaults setObject:nil forKey:@"password"];
    [defaults setObject:nil forKey:@"first"];
    [defaults setObject:nil forKey:@"last"];
    [defaults setObject:nil forKey:@"phone"];
    [defaults setBool:NO  forKey:@"govQARememberMe"];
    if(![txtField_eamilAddress.text length] && ![txtField_password.text length]) {
        
        [defaults setValue:nil forKey:@"govQAEmailId"];
        [defaults setValue:nil forKey:@"govQAPassword"];
        [defaults setBool:NO  forKey:@"govQARememberMe"];
        isRemember=[defaults boolForKey:@"govQARememberMe"];
    }

     [self.navigationController pushViewController:createAccount animated:YES];
    
}
- (void)viewDidLayoutSubviews {
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
    loginView.contentSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT+30);
    }
}


-(int)calculateLabelHeight:(NSString*)text labelWidth:(int)labelWidth fontSize:(int)fontSize
{
    CGSize constraint;
    CGSize size ;
    CGFloat height ;
    constraint = CGSizeMake(labelWidth, 20000.0f);
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    size = [text boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:fontSize], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    
    height = MAX(size.height+30, 40.0f);
    return height;
}


/*
 Function for setting constraints on views using auto layout.
 */
-(void)setConstraints:(int)height_offset width_offset:(int)width_offset navHeight:(double)navHeight
{
    viewsDictionary =
    NSDictionaryOfVariableBindings(topTextLabel,topThirdTextLabel, emailLabel,txtField_eamilAddress, pwdLabel, txtField_password, forgotPwdLabel, forgotPwdButton, remMeButton, remMeLabel,loginButton,signUpButton,poweredByImg);//topsecondTextLabel
    
    // Set constraint property.
    topTextLabel.translatesAutoresizingMaskIntoConstraints = NO;
   // topsecondTextLabel.translatesAutoresizingMaskIntoConstraints = NO;
    topThirdTextLabel.translatesAutoresizingMaskIntoConstraints = NO;
    emailLabel.translatesAutoresizingMaskIntoConstraints = NO;
    txtField_eamilAddress.translatesAutoresizingMaskIntoConstraints = NO;
    pwdLabel.translatesAutoresizingMaskIntoConstraints = NO;
    txtField_password.translatesAutoresizingMaskIntoConstraints = NO;
    forgotPwdLabel.translatesAutoresizingMaskIntoConstraints = NO;
    forgotPwdButton.translatesAutoresizingMaskIntoConstraints = NO;
    remMeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    remMeButton.translatesAutoresizingMaskIntoConstraints = NO;
    loginButton.translatesAutoresizingMaskIntoConstraints = NO;
    signUpButton.translatesAutoresizingMaskIntoConstraints = NO;
   
    poweredByImg.translatesAutoresizingMaskIntoConstraints = NO;
    
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
//        topImgRect = CGRectMake(X_OFFSET, navHeight, VIEW_FRAME_WIDTH-X_OFFSET*2, 240);
//        
        // Call this function for setting constraints for iPad
        [self setIpadConstraints:height_offset width_offset:width_offset navHeight:navHeight];
        
    }
    else
    {
        // Call this function for setting constraints for iPhone
        [self setIphoneConstraints:height_offset width_offset:width_offset navHeight:navHeight];
    }
    
}

/*
 Function for setting constraints for iPad.
 */
-(void)setIpadConstraints:(int)height_offset width_offset:(int)width_offset navHeight:(double)navHeight
{
    // Vertical constrains
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[topTextLabel(%i)]-|",navHeight+60,topHeight+10] options:0 metrics:0 views:viewsDictionary]];
    
//    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[topsecondTextLabel(%i)]-|",topTextLabelRect.origin.y+topTextLabelRect.size.height+height_offset+80,topsecondHeight] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[topThirdTextLabel(%i)]-|",topTextLabelRect.origin.y+topTextLabelRect.size.height+height_offset+80,topThirdHeight] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[emailLabel(%i)]-|",topThirdTextLabelRect.origin.y+topThirdTextLabelRect.size.height+height_offset+100,IPAD_LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[txtField_eamilAddress(%i)]-|",emailLabelRect.origin.y+emailLabelRect.size.height+105,IPAD_TEXTFIELD_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[pwdLabel(%i)]-|",txtField_eamilAddressRect.origin.y+txtField_eamilAddressRect.size.height+height_offset+115,IPAD_LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[txtField_password(%i)]-|",pwdLabelRect.origin.y+pwdLabelRect.size.height+120,IPAD_TEXTFIELD_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[loginButton(40)]-|",forgotPwdButtonRect.origin.y+forgotPwdButtonRect.size.height+X_OFFSET+200] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[signUpButton(40)]-|",loginButtonRect.origin.y+loginButtonRect.size.height+X_OFFSET+220] options:0 metrics:0 views:viewsDictionary]];
    
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[poweredByImg(60)]-|",signUpButtonRect.origin.y+signUpButtonRect.size.height+X_OFFSET+210] options:0 metrics:0 views:viewsDictionary]];
    
    // Horizontal constraints
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[topTextLabel(%f)]-|",X_OFFSET+85,VIEW_FRAME_WIDTH-X_OFFSET*10] options:0 metrics:0 views:viewsDictionary]];
    
//    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[topsecondTextLabel(%f)]-|",X_OFFSET+85,VIEW_FRAME_WIDTH-X_OFFSET*10] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[topThirdTextLabel(%f)]-|",X_OFFSET+85,VIEW_FRAME_WIDTH-X_OFFSET*10] options:0 metrics:0 views:viewsDictionary]];
    
      [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[emailLabel(130)]-|",X_OFFSET+225] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[txtField_eamilAddress(%f)]-|",X_OFFSET+225,VIEW_FRAME_WIDTH-X_OFFSET*25+30] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[pwdLabel(90)]-|",X_OFFSET+225] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[txtField_password(%f)]-|",X_OFFSET+225,VIEW_FRAME_WIDTH-X_OFFSET*25+30] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[loginButton(%f)]-|",X_OFFSET+225,VIEW_FRAME_WIDTH-X_OFFSET*25+30] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[signUpButton(%f)]-|",X_OFFSET+225,VIEW_FRAME_WIDTH-X_OFFSET*25+30] options:0 metrics:0 views:viewsDictionary]];
    
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[poweredByImg(80)]-|",X_OFFSET+115] options:0 metrics:0 views:viewsDictionary]];
    
    // Vertical
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[remMeButton(%i)]-|",txtField_passwordRect.origin.y+txtField_passwordRect.size.height+145,X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[remMeLabel(%i)]-|",txtField_passwordRect.origin.y+txtField_passwordRect.size.height+142,IPAD_LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[forgotPwdButton(%i)]-|",txtField_passwordRect.origin.y+txtField_passwordRect.size.height+height_offset+170,IPAD_LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    forgotPwdLabel.hidden=YES;
    
    // Horizontal
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[remMeButton(%i)]-|",X_OFFSET+225,X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[remMeLabel(100)]-|",X_OFFSET+250] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[forgotPwdButton(130)]-|",X_OFFSET+225] options:0 metrics:0 views:viewsDictionary]];
    //[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[forgotPwdLabel(130)]-|",X_OFFSET+225] options:0 metrics:0 views:viewsDictionary]];
}

/*
 Function for setting constraints for iPhone.
 */
-(void)setIphoneConstraints:(int)height_offset width_offset:(int)width_offset navHeight:(double)navHeight
{
    
     // Vertical constrains
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[topTextLabel(%i)]",navHeight,topHeight] options:0 metrics:0 views:viewsDictionary]];
       
    
//    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[topsecondTextLabel(%i)]-|",topTextLabelRect.origin.y+topTextLabelRect.size.height-5,topsecondHeight] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[topThirdTextLabel(%i)]",topTextLabelRect.origin.y+topTextLabelRect.size.height+5,topThirdHeight] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[emailLabel(%i)]",topThirdTextLabelRect.origin.y+topTextLabelRect.size.height+20,LABEL_HEIGHT] options:0 metrics:nil views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[txtField_eamilAddress(%i)]",emailLabelRect.origin.y+emailLabelRect.size.height-10,TEXTFIELD_HEIGHT] options:0 metrics:nil views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[pwdLabel(%i)]",txtField_eamilAddressRect.origin.y+txtField_eamilAddressRect.size.height-5,LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[txtField_password(%i)]",pwdLabelRect.origin.y+pwdLabelRect.size.height-5,TEXTFIELD_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[loginButton(30)]",forgotPwdButtonRect.origin.y+forgotPwdButtonRect.size.height+X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[signUpButton(30)]",loginButtonRect.origin.y+loginButtonRect.size.height+X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[poweredByImg(32)]",signUpButtonRect.origin.y+signUpButtonRect.size.height+X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    // Horizontal constraints
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[topTextLabel(%f)]",X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2] options:0 metrics:0 views:viewsDictionary]];
//    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[topsecondTextLabel(%f)]-|",X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[topThirdTextLabel(%f)]",X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[emailLabel(130)]",X_OFFSET+2] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[txtField_eamilAddress(%f)]",X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[pwdLabel(90)]",X_OFFSET+2] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[txtField_password(%f)]",X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[loginButton(%f)]",X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[signUpButton(%f)]",X_OFFSET,VIEW_FRAME_WIDTH-X_OFFSET*2] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[poweredByImg(50)]",X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    // Vertical
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[remMeButton(%i)]",txtField_passwordRect.origin.y+txtField_passwordRect.size.height,X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[remMeLabel(%i)]",txtField_passwordRect.origin.y+txtField_passwordRect.size.height,X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[forgotPwdButton(%i)]",txtField_passwordRect.origin.y+txtField_passwordRect.size.height,LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[forgotPwdLabel(%i)]",txtField_passwordRect.origin.y+txtField_passwordRect.size.height+5,LABEL_HEIGHT] options:0 metrics:0 views:viewsDictionary]];
    
    // Horizontal
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%f-[remMeButton(90)]",X_OFFSET*2+forgotPwdButtonRect.size.width-18] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%f-[remMeLabel(100)]",X_OFFSET+remMeButtonRect.origin.x+width_offset] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[forgotPwdButton(130)]",X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
    
    [loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%i-[forgotPwdLabel(130)]",X_OFFSET] options:0 metrics:0 views:viewsDictionary]];
}

- (IBAction)forgotPwdClicked:(id)sender{
    

    
    GovQAForgotPasswordViewController *svc = [[GovQAForgotPasswordViewController alloc]initWithNibName:@"GovQAForgotPasswordViewController" bundle:[NSBundle mainBundle]];
//    svc.arrBottomButtonDO=arrBottomButtonDO;
//    
//    [defaults setBool:NO  forKey:@"govQARememberMe"];
//    if(![txtField_eamilAddress.text length] && ![txtField_password.text length]) {
//        
//        [defaults setValue:nil forKey:@"govQAEmailId"];
//        [defaults setValue:nil forKey:@"govQAPassword"];
//        [defaults setBool:NO  forKey:@"govQARememberMe"];
//        isRemember=[defaults boolForKey:@"govQARememberMe"];
//    }

    
    [self.navigationController pushViewController:svc animated:NO];
    //[svc release];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    [HubCitiAppDelegate showActivityIndicator];
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    //[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    //[[NSURLCache sharedURLCache] removeAllCachedResponses];
    [HubCitiAppDelegate removeActivityIndicator];

}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
   [HubCitiAppDelegate removeActivityIndicator];
}


/*
 *
 Common method to create the label
 *
 */
- (UILabel *)createLabel:(NSString*)title rect:(CGRect)rect{
    
    UILabel* label = [[UILabel alloc] initWithFrame:rect] ;
    //label.font = [UIFont boldSystemFontOfSize:MAIN_FONT_SIZE];
    //label.font=[UIFont fontWithName:@"GillSans-BoldItalic" size:14.0f];
    //label.adjustsFontSizeToFitWidth = YES;
    label.textColor = [UIColor blackColor];
    label.backgroundColor = [UIColor clearColor];
    label.text = title;
    return label;
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    isKeyBoradOpen = false;
    //[self hidePickerView];
    [self animateTextView:NO];
    
    return YES;

}
-(BOOL)textView:(UITextView *)textView1 shouldChangeTextInRange:
(NSRange)range replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]) {
        
       // [self hidePickerView];
        [textView1 resignFirstResponder];
        isKeyBoradOpen = false;
        
    }
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    NSLog(@"Did begin editing");
    
   // [self hidePickerView];
    
    if (!isKeyBoradOpen) {
        isKeyBoradOpen = true;
        [self animateTextView:YES];
    }
    
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    if (isKeyBoradOpen) {
        isKeyBoradOpen = false;
        [self animateTextView:NO];
    }
    
   // [self hidePickerView];
    
    
    return YES;
}

// The callback for frame-changing of keyboard -
-(void)keyboardWillShow:(NSNotification *)notification
{
    //    NSDictionary* info = [notification userInfo];
    //    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect textFieldRect = [self.view.window convertRect:activeTextField.bounds fromView:activeTextField];
    CGRect viewRect = [self.view.window convertRect:loginView.bounds fromView:loginView];
    CGFloat midline = textFieldRect.origin.y + 0.8*textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION*viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator/denominator;
    if(heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if(heightFraction > 1.0)
    {
        heightFraction = 1.2;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if(orientation ==UIInterfaceOrientationPortrait||
       orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT*heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT*heightFraction);
    }
    CGRect viewFrame = loginView.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [loginView setFrame:viewFrame];
    [UIView commitAnimations];
    
    
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    CGRect viewFrame = loginView.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [loginView setFrame:viewFrame];
    [UIView commitAnimations];
    
}
-(void) HideKeyBard:(NSNotification *)noty
{
    if (isKeyBoradOpen) {
        isKeyBoradOpen = false;
        
           [activeTextField resignFirstResponder];
        
    }
    
}
-(void) dismissKeyBoard
{
    if (isKeyBoradOpen) {
        isKeyBoradOpen = false;
        [self animateTextView:NO];
        [activeTextField resignFirstResponder];
    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeTextField = nil;
}


- (IBAction)dismissKeyboard:(id)sender
{
    [activeTextField resignFirstResponder];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (!isKeyBoradOpen) {
        
        isKeyBoradOpen = true;
        [self animateTextView:YES];
    }

    activeTextField = textField;
    
}



/* Common method to create the textfield */
- (UITextField *)createTxtField:(NSString*)placeHolder rect:(CGRect)rect
{
    UITextField *txtField = [[UITextField alloc] initWithFrame:rect] ;
    txtField.textColor = [UIColor blackColor];
    txtField.borderStyle = UITextBorderStyleRoundedRect;
    txtField.keyboardType = UIKeyboardTypeDefault;
    txtField.placeholder = placeHolder;
    return txtField;
}
-(void)bottomButtonPressed:(id)sender
{
    [defaults setBool:NO forKey:@"ViewMore"];
    [defaults setBool:YES forKey:BottomButton];
    //   [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    //NSlog(@"The tag is %d",tag);
    bottomButtonDO *obj_bottomBottomDO = [arrBottomButtonDO  objectAtIndex:tag];
    
    [defaults setValue:obj_bottomBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_bottomBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_bottomBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_bottomBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_bottomBottomDO.btnLinkTypeName);
    if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    
    else{ //other menu types
        switch ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                //[hotDeals release];
                break;
                
            case 1://Scan Now
            {
            
            }
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }

                [self navigateToFindView];
            }
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
            {
                
            }
                
                break;
                
            case 8://Visitors Bureau
            {
                
            }
                
                break;
                
            case 9://transportation
            {
                
            }
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {
                    [self shareClicked:nil];
                }
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=TRUE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:obj_bottomBottomDO.btnLinkID];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                   // CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];;
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
             //   [dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }
                break;
            case 22:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{

                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }
                break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{

                iWebRequestState = GETFAVLOCATIONS;
                [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{

                if([Network currentReachabilityStatus]==0)
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                }
                else{
                    
                    UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:inform animated:NO];
                    //[inform release];
                }
                
                }
                
            }break;
            case 25:
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{

                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    }
                    else{
                        [SharedManager setUserInfoFromSignup:NO];
                        CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                        [self.navigationController pushViewController:citPref animated:NO];
                      //  [citPref release];
                    }
                }
                }
                
            }break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                //privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
            default:
                break;
        }
    }
    
}

-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}
-(void)parseGetUserData:(id)response
{
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}

-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
        isRemember = YES;
        
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}



-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}


-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}

// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case PARTNERRET:
            [self parse_PartnerRet:response];
            break;
            //        case gETPARTNER:
            //            [self parse_GetPartners:response];
            //            break;
            
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
            
   
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
            
        default:
            break;
            
            
            
    }
    
}

-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}

//Call Find Service and Class
-(void)navigateToFindView
{
    
    //    [defaults setValue:@"0" forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    
    navigatedFromMainMenu = FALSE;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }

    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
    [self.navigationController popToRootViewControllerAnimated:NO];
    }
}
-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}


-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=FALSE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    else if ([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}

#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
    }
}

-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}




-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}


//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                              [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}




- (void)dealloc {
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  RequestViewFiltersController.h
//  HubCiti
//
//  Created by Ashika on 1/4/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SortAndFilter.h"

@interface RequestViewFiltersController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    UITableView *requestViewFilter;
    NSMutableArray *arrFilters;
    NSString *selectedFilter;
    
}
@property(nonatomic,strong) SortAndFilter *sortObj;
@property(atomic,strong) NSString *selectedFilter;
@end

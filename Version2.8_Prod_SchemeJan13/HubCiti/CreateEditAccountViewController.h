//
//  CreateAccountViewController.h
//  HubCiti
//
//  Created by service on 9/23/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "CommonUtility.h"
@class AnyViewController;
@class EmailShareViewController;
@interface CreateEditAccountViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,UITextViewDelegate,UITextFieldDelegate,UITextViewDelegate,UIScrollViewDelegate,MFMessageComposeViewControllerDelegate,UIPickerViewDataSource, UIPickerViewDelegate,HTTPClientDelegate,CustomizedNavControllerDelegate>
{
    WebRequestState iWebRequestState;
    CommonUtility *common;
    BOOL isRemember;
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic, assign) BOOL isEditScreen;
@property (nonatomic, strong) NSMutableArray *arrayBottomButtonID,*arrBottomButtonViewContainer;
@end

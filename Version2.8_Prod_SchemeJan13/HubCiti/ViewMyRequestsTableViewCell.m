//
//  ViewMyRequestsTableViewCell.m
//  HubCiti
//
//  Created by Bindu M on 10/1/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "ViewMyRequestsTableViewCell.h"

@implementation ViewMyRequestsTableViewCell

@synthesize refNumField,statusField,summaryField,summary,status,refNum;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.refNum = [self createLabel];
        self.status = [self createLabel];
        self.summary = [self createLabel];
        self.refNumField = [self createLabel];
        self.statusField = [self createLabel];
        self.summaryField = [self createLabel];
        [self.contentView addSubview:self.refNumField];
        [self.contentView addSubview:self.statusField];
        [self.contentView addSubview:self.summaryField];
        [self.contentView addSubview:self.refNum];
        [self.contentView addSubview:self.status];
        [self.contentView addSubview:self.summary];

    }
    return self;
}


-(UILabel*) createLabel
{
    UILabel * labelView = [[UILabel alloc] init] ;
    labelView.translatesAutoresizingMaskIntoConstraints=YES;
    labelView.lineBreakMode = NSLineBreakByWordWrapping;
    labelView.numberOfLines = 0;
    labelView.backgroundColor = [UIColor clearColor];
    labelView.textColor = [UIColor blackColor];
    labelView.font = [UIFont systemFontOfSize:12];
    
    return labelView;
}


-(UIButton*)createButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    button.backgroundColor = [UIColor clearColor];
    return button;
}

@end

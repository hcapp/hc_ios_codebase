//
//  UpdateUserInfo.h
//  HubCiti
//
//  Created by Lakshmi H R on 1/11/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpdateUserInfo : NSObject

@property(nonatomic,retain) NSString * responseCode;
@property(nonatomic,retain) NSString * responseText;

@end

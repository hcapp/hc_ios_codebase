//
//  UserInfoFields.h
//  HubCiti
//
//  Created by Lakshmi H R on 1/8/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UserInfoFields;
@interface UserInfoFields : NSObject

@property(nonatomic,retain) NSString * position;
@property(nonatomic,retain) NSString * fieldName;
@property(nonatomic,retain) NSString * value;
//@property(nonatomic,retain) NSString * requiredField;
@property(nonatomic,assign) BOOL  requiredField;

@end

//
//  ArIncomeRangeList.h
//  HubCiti
//
//  Created by Lakshmi H R on 1/8/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol ArIncomeRangeList;
@interface ArIncomeRangeList : NSObject
@property(nonatomic,retain) NSString * icRangeId;
@property(nonatomic,retain) NSString * icRangeName;
@end

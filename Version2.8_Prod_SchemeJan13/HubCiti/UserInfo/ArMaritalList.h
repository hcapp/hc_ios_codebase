//
//  ArMaritalList.h
//  HubCiti
//
//  Created by Lakshmi H R on 1/8/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ArMaritalList;

@interface ArMaritalList : NSObject
@property(nonatomic,retain) NSString * mStatusId;
@property(nonatomic,retain) NSString * mStatusName;

@end

//
//  GetUserInfo.h
//  HubCiti
//
//  Created by Lakshmi H R on 1/8/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfoFields.h"
#import "ArEducationList.h"
#import "ArMaritalList.h"
#import "ArIncomeRangeList.h"

@interface GetUserInfoResponse : NSObject

@property (strong, nonatomic) NSArray<UserInfoFields>* listUserDetails;
@property (strong, nonatomic) NSArray<ArMaritalList>* arMaritalList;
@property (strong, nonatomic) NSArray<ArEducationList>* arEducationList;
@property (strong, nonatomic) NSArray<ArIncomeRangeList>* arIncomeRangeList;
@property(nonatomic,retain) NSString * responseCode;
@property(nonatomic,retain) NSString * responseText;
@end

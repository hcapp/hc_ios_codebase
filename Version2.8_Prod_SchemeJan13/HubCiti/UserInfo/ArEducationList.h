//
//  ArEducationList.h
//  HubCiti
//
//  Created by Lakshmi H R on 1/8/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol ArEducationList;

@interface ArEducationList : NSObject
@property(nonatomic,retain) NSString * educatLevelId;
@property(nonatomic,retain) NSString * educatLevelName;

@end

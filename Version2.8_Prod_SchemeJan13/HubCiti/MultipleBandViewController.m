//
//  MultipleBandViewController.m
//  HubCiti
//
//  Created by Nikitha on 9/14/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "MultipleBandViewController.h"
#import "ScrollTemplateViewController.h"
#import "MainMenuViewController.h"
#import "CombinationViewController.h"
#import "CustomizedNavController.h"
#import "AppDelegate.h"
#import "bottomButtonView.h"
#import "MultipleBandDetail.h"
#import "AlertsList.h"
#import "CityExperienceViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "FilterRetailersList.h"
#import "FilterListViewController.h"
#import "FAQCategoryList.h"
#import "FundraiserListViewController.h"
#import "UserSettingsController.h"
#import "CityPreferenceViewController.h"
#import "SingleCatRetailers.h"
#import "EventsListViewController.h"
#import "DealHotDealsList.h"
#import "RetailersListViewController.h"
#import "LocationDetailsViewController.h"
#import "CouponsViewController.h"
#import "EmailShareViewController.h"
#import "EventsListViewController.h"
#import "UserInformationViewController.h"
#import "PreferredCategoriesScreen.h"
#import "SpecialOffersViewController.h"
#import "WebBrowserViewController.h"
#import "AnythingPage.h"
#import "BandRetailerSummaryViewController.h"
#import "CouponsInDealViewController.h"
#import "CouponsMyAccountsViewController.h"
#import "MultipleBandTableViewCell.h"

NSDictionary * table_Constraint_Dict;

@interface MultipleBandViewController ()<CustomizedNavControllerDelegate,UITableViewDelegate,UITableViewDataSource,HTTPClientDelegate,MFMessageComposeViewControllerDelegate>
{
    int bottomBtn;
    WebRequestState iWebRequestState;
    NSMutableArray *arr_MultipleBandBottomBtn;
    NSMutableArray *arr_MultipleBandBottomBtnContainer;
    bottomButtonView *bottomBtn_View;
    CommonUtility *common;
    BOOL scrollViewEnabled,fetchNext;
    UIActivityIndicatorView *loading;
    UIView* containerView;
}


@end

@implementation MultipleBandViewController
@synthesize nextPage,obj_multipleBandResponse,array_MultipleBandList,sortObj,savedSortObj,sortFilObj,retailerListScreen,anyVC,emailSendingVC,iEventsListViewController,footer,infoResponse,navigationTitleText,retailerSummaryPage,multipleTableViewOutlet,bottomConstraintForTableView;

#pragma mark - Service Respone methods
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error
{
    if (fetchNext) {
        [self hideFooter];
    }
    
    [HubCitiAppDelegate removeActivityIndicator];
    scrollViewEnabled = TRUE;
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription] ];
    
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    if (fetchNext) {
        [self hideFooter];
    }
    [HubCitiAppDelegate removeActivityIndicator];
    DLog(@"parameter: %@",responseObject);
    scrollViewEnabled = TRUE;
    [self responseData:responseObject];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //memory allocation
    nextPage = [[NSNumber alloc]init];
    rowCountOfTotalCells = [[NSNumber alloc]initWithInt:0];
    array_MultipleBandList = [[NSMutableArray alloc]init];
    
    [multipleTableViewOutlet registerNib:[UINib nibWithNibName:@"MultipleBandTableViewCell" bundle:nil] forCellReuseIdentifier:@"multipleCell"];
    
    //default for pagination
    [defaults setBool:NO forKey:@"ViewMoreMultipleBand"];
    NSLog(@"Band ID %@",[defaults valueForKey:@"BandEventID"]);
    
    //Navigation bar title text, back button and home button
    self.navigationItem.hidesBackButton = YES;
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:navigationTitleText forView:self withHambergur:NO];


    //Home Button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    self.navigationItem.rightBarButtonItem = mainPage;
    
    //request for multiple bands list
    [self request_MultipleBandsList];
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    scrollViewEnabled = TRUE;
    
    //disable hamburger button
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark  scroll delegate for pagination

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (!scrollViewEnabled)
    {
        return;
    }
    CGSize contentSize = scrollView.contentSize;
    CGPoint contentOffset = scrollView.contentOffset;
    
    if (contentSize.height < SCREEN_HEIGHT && [nextPage intValue]  == 0) {
        return;
    }
    if ((contentOffset.y + scrollView.bounds.size.height > contentSize.height) && [nextPage intValue] == 1)
    {
        [self fetchNextMultipleBand];
    }
}
-(void)fetchNextMultipleBand
{
    scrollViewEnabled = FALSE;
    fetchNext = true;
    [HubCitiAppDelegate removeActivityIndicator];
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    loading.color = [UIColor blackColor];
    [loading startAnimating];
    CGFloat height = loading.frame.size.height + 6.f;
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, height)];
    containerView.backgroundColor = [UIColor clearColor];
    [containerView addSubview:loading];
    loading.center = CGPointMake(self.view.bounds.size.width / 2, height / 2.f);
    multipleTableViewOutlet.tableFooterView = containerView;
    multipleTableViewOutlet.userInteractionEnabled = false;
    [self request_MultipleBandsList];
}

-(void) hideFooter
{
    fetchNext = false;
    [loading stopAnimating];
    [containerView removeFromSuperview];
    scrollViewEnabled = FALSE;
    multipleTableViewOutlet.tableFooterView = nil;
    multipleTableViewOutlet.userInteractionEnabled = TRUE;
    [footer removeFromSuperview];
}

#pragma mark request response for multiple band listing

-(void) request_MultipleBandsList
{
    iWebRequestState = MULTIPLE_BANDS;
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    if ([defaults valueForKey:KEY_USERID])
    {
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID])
    {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    if([defaults valueForKey:@"BandEventID"])
    {
        [parameters setValue:[defaults valueForKey:@"BandEventID"] forKey:@"bEvtId"];
    }
    [parameters setValue:rowCountOfTotalCells forKey:@"lowerLimit"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@band/geteventbandslist",BASE_URL];
    
    DLog(@"parameter: %@",parameters);
    
    
    //If pagination is called
    if ([nextPage intValue] == 1)    {
        [defaults setBool:YES forKey:@"ViewMoreMultipleBand"];
        HTTPClient *client = [[HTTPClient alloc] init];
        client.delegate = self;
        DLog(@"Url: %@",urlString);
        [client sendRequest : parameters : urlString];
    }
    
    //if no pagination
    else
    {
        scrollViewEnabled = TRUE;
        HTTPClient *client = [[HTTPClient alloc] init];
        client.delegate = self;
        DLog(@"Url: %@",urlString);
        [client sendRequest : parameters : urlString];
        [HubCitiAppDelegate showActivityIndicator];
    }
}


-(void) parse_MultipleBandList : (id) responseData
{
    if (responseData == nil)
        return;
    
    if (obj_multipleBandResponse == nil) {
        obj_multipleBandResponse = [[MultipleBandResponse alloc] init];
    }
    [obj_multipleBandResponse setValuesForKeysWithDictionary:responseData];
    
    //Success
    if ([obj_multipleBandResponse.responseCode isEqualToString:@"10000"] )
    {
        if(obj_multipleBandResponse.nextPage)
        {
            nextPage = obj_multipleBandResponse.nextPage;
        }
        if(obj_multipleBandResponse.maxRowNum)
        {
            rowCountOfTotalCells  = obj_multipleBandResponse.maxRowNum;
        }
        if (obj_multipleBandResponse.retailerDetail!=nil)
        {
            for (int i = 0; i< obj_multipleBandResponse.retailerDetail.count; i++)
            {
                MultipleBandDetail * obj_multipleBandDetail = [[MultipleBandDetail alloc]init];
                NSDictionary * dict_MultipleBandDetail = obj_multipleBandResponse.retailerDetail[i];
                [obj_multipleBandDetail setValuesForKeysWithDictionary:dict_MultipleBandDetail];
                [array_MultipleBandList addObject:obj_multipleBandDetail];
            }
        }
        //if bottom button exist
        if([obj_multipleBandResponse.bottomBtn integerValue] == 1)
        {
            bottomBtn = [obj_multipleBandResponse.bottomBtn intValue];
            arr_MultipleBandBottomBtn=[[NSMutableArray alloc]init];
            for (int i=0; i < obj_multipleBandResponse.bottomBtnList.count; i++) {
                bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                NSDictionary* dictList = obj_multipleBandResponse.bottomBtnList[i];
                
                [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                
                if(obj_findBottomDO.bottomBtnName.length == 0)
                {
                    obj_findBottomDO.bottomBtnName = @" ";
                }
                if(obj_findBottomDO.bottomBtnImg.length == 0)
                {
                    obj_findBottomDO.bottomBtnImg = @" ";
                }
                if(obj_findBottomDO.bottomBtnImgOff.length == 0)
                {
                    obj_findBottomDO.bottomBtnImgOff = @" ";
                }
                if(obj_findBottomDO.btnLinkTypeName)
                {
                    if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"])
                    {//for filters
                        [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                    }
                }
                [arr_MultipleBandBottomBtn addObject:obj_findBottomDO];
                
                
            }
            //setting bottom button is not called when paginationis called
            
            if(![defaults boolForKey:@"ViewMoreMultipleBand"]){
                if([arr_MultipleBandBottomBtn count] > 0)
                {
                    // Set Bottom Menu Button after read from response data
                    [self setBottomBarMenu];
                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                }
                else
                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            }
        }
        //table view frame set
        [self setTableViewFrame];
    }
    
    //"no record found" result
    else if([obj_multipleBandResponse.responseCode isEqualToString:@"10002"])
    {
        nextPage=0;
        NSString *responseTextStr;
        if (obj_multipleBandResponse.responseText != nil) {
            responseTextStr = obj_multipleBandResponse.responseText;
        }
        else
            responseTextStr = @"No Records Found";
        
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        [self setTableViewFrame];
        [multipleTableViewOutlet setContentOffset:CGPointZero animated:YES];
    }
    
    else if ([obj_multipleBandResponse.responseCode isEqualToString:@"10005"])//when their is bottom button but no data in listing
    {
        if([obj_multipleBandResponse.bottomBtn integerValue] == 1)
        {
            bottomBtn = [obj_multipleBandResponse.bottomBtn intValue];
            arr_MultipleBandBottomBtn=[[NSMutableArray alloc]init];
            for (int i=0; i < obj_multipleBandResponse.bottomBtnList.count; i++) {
                bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                NSDictionary* dictList = obj_multipleBandResponse.bottomBtnList[i];
                [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                
                if(obj_findBottomDO.bottomBtnName.length == 0)
                {
                    obj_findBottomDO.bottomBtnName = @" ";
                }
                if(obj_findBottomDO.bottomBtnImg.length == 0)
                {
                    obj_findBottomDO.bottomBtnImg = @" ";
                }
                if(obj_findBottomDO.bottomBtnImgOff.length == 0)
                {
                    obj_findBottomDO.bottomBtnImgOff = @" ";
                }
                if(obj_findBottomDO.btnLinkTypeName)
                {
                    if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"])
                    {//for filters
                        [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                    }
                }
                [arr_MultipleBandBottomBtn addObject:obj_findBottomDO];
                
            }
            
            if(![defaults boolForKey:@"ViewMoreMultipleBand"]){
                if([arr_MultipleBandBottomBtn count] > 0)
                {
                    // Set Bottom Menu Button after read from response data
                    [self setBottomBarMenu];
                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                }
                else
                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
                
            }
            [self setTableViewFrame];
            [multipleTableViewOutlet setContentOffset:CGPointZero animated:YES];
            
        }
        nextPage=0;
        NSString *responseTextStr;
        if (obj_multipleBandResponse.responseText != nil) {
            responseTextStr = obj_multipleBandResponse.responseText;
        }
        else
            responseTextStr = @"No Records Found";
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
    }
    else {//show the error message in alert
        nextPage=0;
        
        if (obj_multipleBandResponse.responseText != nil) {
            [UtilityManager showAlert:nil msg:obj_multipleBandResponse.responseText];
            [loading stopAnimating];
        }
        else
        {
            [UtilityManager showAlert:nil msg:@"No Records found"];
            [loading stopAnimating];
        }
        [self setTableViewFrame];
        [multipleTableViewOutlet setContentOffset:CGPointZero animated:YES];
    }
    [defaults setBool:NO forKey:@"ViewMoreMultipleBand"];
    
}


#pragma mark back/home button functionality

-(void)popBackToPreviousPage
{
    [self.navigationController popViewControllerAnimated:NO];
    [defaults setBool:NO forKey:@"ViewMoreMultipleBand"];
}

-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"])
        {
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"])
        {
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


#pragma mark bottom button setup and functionality

-(void)bottomButtonPressed:(id)sender
{
    if([defaults boolForKey:@"ViewMore"])
        return;
    [defaults setBool:NO forKey:@"ViewMoreBand"];
    [defaults setBool:YES forKey:BottomButton];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    bottomButtonDO *obj_eventBottomDO = [arr_MultipleBandBottomBtn objectAtIndex:tag];
    
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=27||[[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=28)
    {
        [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    }
    [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_eventBottomDO.btnLinkID!=NULL)
        {
            [defaults setValue:obj_eventBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_eventBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_eventBottomDO.btnLinkTypeName);
    if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1)
        {
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1)
        {
            if (SingleEventCatID==Nil)
            {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewControllerVC = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            iEventsListViewControllerVC.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewControllerVC animated:NO];
        }
        else
            return;
    }
    
    
    else{ //other menu types
        switch ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                //[hotDeals release];
                break;
                
            case 1://Scan Now
            {
                
            }
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
                }
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                [self navigateToFindView];
            }
                
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
            {
                
            }
                
                break;
                
            case 8://Visitors Bureau
            {
                
            }
                
                break;
                
            case 9://transportation
            {
                
            }
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {
                    [self shareClicked:nil];
                }
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=TRUE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_eventBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
            
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0)
                {
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                }
                
                else if ([SharedManager filtersCount] == 1)
                { //navigate to
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];;
            
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:filters animated:NO];
                }
                
                else if ([SharedManager filtersCount] >1)
                { //navigate to city experience - multiple filters
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:filterList animated:NO];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                //   [dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                
            }
                break;
                
            case 21:
                
            {
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                
            }
                break;
            case 22:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }
                break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!")  msg:NSLocalizedString(@"Network is not available",@"Network is not available") ];
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                    }
                    
                }
                
            }break;
            case 25:
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                        }
                    }
                }
                
            }break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                //privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
            default:
                break;
        }
    }
    
}


-(void)setBottomBarMenu
{
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    // Calculate Y position for View
    arr_MultipleBandBottomBtnContainer = [[NSMutableArray alloc]init];
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-bottomBarButtonHeight - 44 - 20, SCREEN_WIDTH, bottomBarButtonHeight)];//-yVal
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [arr_MultipleBandBottomBtn count]; btnLoop++)
    {
        bottomButtonDO *obj_findBottomDO = [arr_MultipleBandBottomBtn objectAtIndex:btnLoop];
        
        bottomBtn_View = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            bottomBtn_View.contentMode = UIViewContentModeCenter;
            bottomBtn_View = [bottomButtonView initWithTitle:obj_findBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, (SCREEN_HEIGHT-bottomBarButtonHeight - 44 -20) , 80,bottomBarButtonHeight) imageName:obj_findBottomDO.bottomBtnImg img_off:obj_findBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];//-yVal
            
        }
        else
            bottomBtn_View = [bottomButtonView initWithTitle:obj_findBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arr_MultipleBandBottomBtn count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arr_MultipleBandBottomBtn count],bottomBarButtonHeight) imageName:obj_findBottomDO.bottomBtnImg img_off:obj_findBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [bottomBtn_View setAccessibilityLabel:obj_findBottomDO.btnLinkTypeName];
        [bottomBtn_View setAccessibilityValue:@"Bottom"];
        [arr_MultipleBandBottomBtnContainer addObject:bottomBtn_View];
        [self.view addSubview:bottomBtn_View];
        [self.view bringSubviewToFront:bottomBtn_View];
    }
    
}

-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewControllerVC = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    self.iEventsListViewController = iEventsListViewControllerVC;
    [self.navigationController pushViewController:iEventsListViewControllerVC animated:NO];
}
-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreenVC = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreenVC;
            [self.navigationController pushViewController:retailerListScreenVC animated:NO];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreenVC = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreenVC;
                [self.navigationController pushViewController:retailerListScreenVC animated:NO];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
            }
        }
    }
}


-(void)navigateToFindView
{
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    navigatedFromMainMenu=false;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
}

-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
}

-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
}

-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}

-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}


-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=FALSE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
               
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
            }
        }
        
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
        }
    }
    [linkID removeLastObject];
}


-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsviewc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsviewc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsviewc animated:NO];
    }
}



-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
}



-(void)parseGetUserData:(id)response
{
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
}


#pragma mark action sheet methods with message, mail delegates

-(void)shareClicked:(id)sender{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
        
    }
    
    
}
-(void) faceBookCliked
{//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
        else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
}

-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = bottomBtn_View;
        actionSheet.popoverPresentationController.sourceRect = bottomBtn_View.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}




- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode)
    {
        return NO;
    }
    return YES;
}


#pragma mark tableview setup with constraint and delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%lu",(unsigned long)array_MultipleBandList.count);
    return array_MultipleBandList.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        
        return 50.0;
    }
    else
    {
        return 70.0;
        
    }
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MultipleBandTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"multipleCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"right_arrow.png"]];
    [cell setDetailsInMultipleBandList:[[array_MultipleBandList objectAtIndex:indexPath.row] bandName] categoryName:[[array_MultipleBandList objectAtIndex:indexPath.row]catName] StartEndTime:[NSString stringWithFormat:@"%@-%@",[[array_MultipleBandList objectAtIndex:indexPath.row] sTime],[[array_MultipleBandList objectAtIndex:indexPath.row]eTime]] logoOfBand:[[array_MultipleBandList objectAtIndex:indexPath.row] bandImgPath]];
    
    return cell;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BandRetailerSummaryViewController* bandRetVC = [[BandRetailerSummaryViewController alloc] initWithNibName:@"BandRetailerSummaryViewController" bundle:nil];
    self.retailerSummaryPage = bandRetVC;
    bandRetVC.retailerBandId = [[[array_MultipleBandList objectAtIndex:indexPath.row] bandID] stringValue];
    [self.navigationController pushViewController:bandRetVC animated:YES];
    
    
}

-(void) setTableViewFrame
{
    if(bottomBtn == 1)
    {
        if (bottomConstraintForTableView.constant == 0)
        {
            bottomConstraintForTableView.constant = 50;
        }
    }
    else
    {
        bottomConstraintForTableView.constant = 0;
    }
    [self.multipleTableViewOutlet updateConstraints];
    [multipleTableViewOutlet setBackgroundColor:[UIColor whiteColor]];
    multipleTableViewOutlet.tableFooterView.hidden=YES;
    
}

#pragma mark logout pressed

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

#pragma mark response data for all request
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
            
            
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
            
        case MULTIPLE_BANDS:
            [self parse_MultipleBandList:response];
            [multipleTableViewOutlet reloadData];
            break;
            
        default:
            break;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end

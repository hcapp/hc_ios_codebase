//
//  GQSortAndFilterViewController.h
//  HubCiti
//
//  Created by Ashika on 10/1/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import "SortAndFilter.h"


@interface GQSortAndFilterViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource, UIPickerViewDelegate>{
    UITableView *sortAndFilterTable;
    NSMutableArray *arrFilters;
    NSMutableArray *arrSorting;
    int filterSelectionVal;
    int sortSelectionval;
    BOOL sortAlphabet;
    NSString *catPickerText,*subcatPickerText;
    UIButton *catButton;
    UIButton *subButton;
    UIPickerView *catSubPicker;
    NSMutableArray *categoryArray;
    NSMutableArray *subCatArray;
    UIView *doneView;
    
    
}

@property(nonatomic)BOOL sortAlphabet;
@property(nonatomic,strong)NSString *catPickerText;
@property(nonatomic,strong)NSString *subcatPickerText;
//@property(nonatomic,retain) MakeRequestViewController *makeObj;
@property(nonatomic,strong) SortAndFilter *sortObj;
@property(nonatomic,strong) NSMutableArray *categoryArray;

@end

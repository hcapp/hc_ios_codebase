//
//  SingleCatRetailers.m
//  HubCiti
//
//  Created by Ajit Nadig on 11/21/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//
#import "SpecialOffersViewController.h"
#import "SingleCatRetailers.h"
#import "WebBrowserViewController.h"
#import "RetailerSummaryViewController.h"
#import "SingleCatGroupAndSort.h"
#import "WebBrowserViewController.h"
#import "AppDelegate.h"
#import "SearchResultCount.h"
#import "FindOptionsViewController.h"
#import "HubCitiConstants.h"
#import "FindCategoryDO.h"
#import "GroupedProductList.h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "AnythingPage.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "CouponsMyAccountsViewController.h"
#import "AppDelegate.h"
//#import "ScanNowScreen.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FAQCategoryList.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "CityExperienceViewController.h"
#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "EventsListViewController.h"
#import "FilterListViewController.h"
#import "FundraiserListViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "UserInformationViewController.h"
#import "HTTPClient.h"
#import "GetUserInfoResponse.h"
#import "SingleCatRetResponse.h"
#import "SingleCatRetDetailsResponse.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "CouponsInDealViewController.h"

@implementation RetailerDetailsDO

@synthesize retId, retName, retLocnId,retCompleteAddress,retLat, retLong;
@synthesize logoImgPath, bannerAdImgPath, ribbonAdImgPath, ribbonAdUrl;
@synthesize retListId, distance,saleFlag;


@end

@implementation RetailerDO


@synthesize catId, catName;
@synthesize retDetailsObjArray;

@end

@interface SingleCatRetailers ()<HTTPClientDelegate>
{
    UIActivityIndicatorView *loading;
    NSDictionary *tableViewDictionary;
    bottomButtonView *view;
    float numberOfLines;
    UIFont *addressFont;
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property(nonatomic,strong) SingleCatRetResponse * singleCatResponse;
@end

@implementation SingleCatRetailers
@synthesize categoryName,catId;
//@synthesize sortFilObj,anyVC;
@synthesize infoResponse;
@synthesize sortFilObj,anyVC,emailSendingVC;
@synthesize singleCatResponse,oldSegmentedIndex;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - Service Respone methods



- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
    
}





- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject

{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    
    
    
    [self responseData:responseObject];
    
    
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
     [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    //    retObjArray = [[NSMutableArray alloc]init];
    retDetailsObjArray = [[NSMutableArray alloc]init];
    
    loading = nil;
    addressFont = [UIFont systemFontOfSize:(IPAD ? 15.0f:10.0f)];
//    UISwipeGestureRecognizer *mSwipeUpRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
//    [mSwipeUpRecognizer setDirection: UISwipeGestureRecognizerDirectionLeft];
//    [self.view addGestureRecognizer:mSwipeUpRecognizer];
//    
//    UISwipeGestureRecognizer *mSwipeUpRecognizerRht = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
//    [mSwipeUpRecognizerRht setDirection: UISwipeGestureRecognizerDirectionRight];
//    [self.view addGestureRecognizer:mSwipeUpRecognizerRht];
    
    ReleaseAndNilify(table_Dining);
    // Do any additional setup after loading the view from its nib.
    
    if (IOS7 == NO)
        search_Dining.tintColor = [UIColor blackColor];
    
    
    
    globalRetailerService=[[NSMutableArray alloc]init];
    retailerIDArray = [[NSMutableArray alloc]init];
    retailLocationIDArray = [[NSMutableArray alloc]init];
    if ([globalRetailerService count]>0) {
        [globalRetailerService removeAllObjects];
    }
    //    view_gSearch.hidden=YES;
    // Default value for Grouping and Sorting
    //    [defaults setValue:@"0" forKey:@"SubCategoryId"];
    //    [defaults setValue:@"distance" forKey:@"SortFindSingleCatBy"];//stores the sort criteria
    //    [defaults setValue:nil forKey:@"GroupFindSingleCatBy"];
    //    [defaults setValue:nil forKey:@"SelectedCityIds"];
    //    [defaults setValue:nil forKey:@"SelectedFilterId"];
    //    [defaults setValue:nil forKey:@"SelectedFilterValueIds"];
    //    [defaults setValue:nil forKeyPath:@"commaSeperatedFilterValueIds"];
    //    [defaults setValue:nil forKeyPath:@"commaSeperatedFilterIds"];
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
    //    [defaults setValue:nil forKey:@"afterFilterValueSelection"];
    //    [defaults setValue:nil forKey:@"afterFilterIdSelection"];
    
    sortFilObj =  [[SortAndFilter alloc]init];
    sortFilObj.distanceSelected = true;
    
   
    self.navigationItem.hidesBackButton = YES;
   
    //home button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    self.navigationItem.rightBarButtonItem = mainPage;

    
    [defaults setValue:nil forKey:@"findSingleMitemId"];
    [self customizedSegmentController];
    
    if ((([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) || ([[HubCitiManager sharedManager]gps_allow_flag] == NO)) && ![defaults valueForKey:KEYZIPCODE])
    {
        //        search_Dining.userInteractionEnabled=NO;
        //        self.navigationItem.rightBarButtonItem.enabled=NO;
       
        
        [UtilityManager showAlertOnWindow:nil msg:NSLocalizedString(@"\"Find\" uses your iPhone's Location Services or your zip code to find what's around you. Please either turn on Location Services and log in again or enter a zip code in the User Information page." , @"\"Find\" uses your iPhone's Location Services or your zip code to find what's around you. Please either turn on Location Services and log in again or enter a zip code in the User Information page.")];        //[alert release];
        
        //        [self request_singlecatretailers:0 subCatId:[defaults valueForKey:@"SubCategoryId"] sortBy:[defaults valueForKey:@"SortFindSingleCatBy"] searchKey:@"" citiesIds:[defaults valueForKeyPath:@"commaSeperatedCities"] fValueIds:[defaults valueForKeyPath:@"commaSeperatedFilterValueIds"]  filterIds:[defaults valueForKeyPath:@"commaSeperatedFilterIds"]];
        
        [self request_singlecatretailers:0 withObj:sortFilObj andsearchKey:@""];
        
    }
    else
        //        [self request_singlecatretailers:0 subCatId:[defaults valueForKey:@"SubCategoryId"] sortBy:[defaults valueForKey:@"SortFindSingleCatBy"] searchKey:@"" citiesIds:[defaults valueForKeyPath:@"commaSeperatedCities"] fValueIds:[defaults valueForKeyPath:@"commaSeperatedFilterValueIds"]  filterIds:[defaults valueForKeyPath:@"commaSeperatedFilterIds"]];
        [self request_singlecatretailers:0 withObj:sortFilObj andsearchKey:@""];
   
    
    
}
-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"])
        {
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"])
        {
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: NO];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    [super viewWillAppear:animated];
    
    DLog(@"%d",sortFilObj.distanceSelected);
    DLog(@"%@", sortFilObj.selectedCatIds);
    
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:categoryName forView:self withHambergur:YES];
    
    
    table_Dining.scrollEnabled = YES;
    
    table_Dining.allowsSelection =YES;
    if([segmentFindSingleController selectedSegmentIndex] == 2){
        
        [segmentFindSingleController setSelectedSegmentIndex:oldSegmentedIndex];
        [self setSegmentColor:segmentFindSingleController];
        
    }
    [table_Dining setContentOffset:CGPointZero];
    if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
        ReleaseAndNilify(table_Dining);
        
        nextPage = 0;
        if (retDetailsObjArray) {
            [retDetailsObjArray removeAllObjects];
        }
        if(globalRetailerService)
            [globalRetailerService removeAllObjects];
        
        sortFilObj = [[SortAndFilter alloc]initWithObject:[self loadCustomObjectWithKey:@"SortFilterObject"]];
        DLog(@"%d", sortFilObj.distanceSelected);
        if(sortFilObj.distanceSelected){
            
            [segmentFindSingleController setSelectedSegmentIndex:0];
            [self setSegmentColor:segmentFindSingleController];
            
        }
        else if(sortFilObj.alphabeticallySelected){
            
            [segmentFindSingleController setSelectedSegmentIndex:1];
            [self setSegmentColor:segmentFindSingleController];
            
        }
        [self request_singlecatretailers:0 withObj:sortFilObj andsearchKey:search_Dining.text];
        
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        [defaults setValue:nil forKey:@"SortFilterObject"];
    }
    
    
}
#pragma customized segment controller
-(void) customizedSegmentController{
    
    segmentFindSingleController = [[UISegmentedControl  alloc] initWithItems:[NSArray arrayWithObjects:@"Distance",@"Name",@"Map", nil]];
    [segmentFindSingleController setSelectedSegmentIndex:0];
    [segmentFindSingleController setBackgroundColor:[UIColor grayColor]];
    [segmentFindSingleController setSegmentedControlStyle:UISegmentedControlStylePlain];
    
    //set width to show divider in between
    [segmentFindSingleController setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:0];
    [segmentFindSingleController setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:1];
    [segmentFindSingleController setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:2];
    
    [self setSegmentColor:segmentFindSingleController]; //add customized color
    
    segmentFindSingleController.translatesAutoresizingMaskIntoConstraints = NO;
    [segmentFindSingleController addTarget:self action:@selector(viewFindSelectionChanged:) forControlEvents:UIControlEventValueChanged];
    
    search_Dining = [[UISearchBar alloc] init];
    search_Dining.showsCancelButton = YES;
    [search_Dining setBarTintColor:[UIColor darkGrayColor]];
    search_Dining.delegate = self;
    search_Dining.translatesAutoresizingMaskIntoConstraints = NO;
    search_Dining.searchBarStyle = UISearchBarStyleDefault;
    search_Dining.translucent = YES;
    search_Dining.layer.borderWidth = 1;
    search_Dining.tag = 0;
    search_Dining.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    
    viewDictionary = NSDictionaryOfVariableBindings(segmentFindSingleController,search_Dining);
    
    [self.view addSubview:segmentFindSingleController];
    [self.view addSubview:search_Dining];
    //set constraits for segment controller
    
    // Vertical constrains
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-2)-[segmentFindSingleController(48)]"] options:0 metrics:0 views:viewDictionary]];
    
    
    // Horizontal constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(-2)-[segmentFindSingleController(%f)]",SCREEN_WIDTH+4] options:0 metrics:0 views:viewDictionary]];
    
    
    // Vertical constrains
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(44)-[search_Dining(44)]"] options:0 metrics:0 views:viewDictionary]];
    
    
    // Horizontal constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[search_Dining(%f)]",SCREEN_WIDTH] options:0 metrics:0 views:viewDictionary]];
    
    
    
}

//set color to selected segment
-(void) setSegmentColor:(UISegmentedControl *) segmentSender
{
    
    for (int i=0; i<[segmentSender.subviews count]; i++)
    {
        if ([[segmentSender.subviews objectAtIndex:i]isSelected] )
        {
            UIColor *tintcolor=[UIColor colorWithRed:80/255.0 green:199/255.0 blue:228/255.0 alpha:1.0];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
            //break;
        }
        else {
            UIColor *tintcolor=[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
        }
    }
    for (int i=0; i<[segmentSender.subviews count]; i++)
    {
        if (![[segmentSender.subviews objectAtIndex:i]isSelected] )
        {
            UIColor *tintcolor=[UIColor darkGrayColor];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
            // break;
        }
        
    }
    
    float segmentFont = 14.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        segmentFont = 18.0;
    }
    
    // Text font color for selected segment
    NSDictionary *attributes = @{
                                 NSFontAttributeName : [UIFont boldSystemFontOfSize:segmentFont],
                                 NSForegroundColorAttributeName : [UIColor blackColor]};
    [segmentSender setTitleTextAttributes:attributes forState:UIControlStateSelected];
    
    NSDictionary *attributes2 = @{
                                  NSFontAttributeName : [UIFont boldSystemFontOfSize:segmentFont],
                                  NSForegroundColorAttributeName : [UIColor whiteColor]};
    [segmentSender setTitleTextAttributes:attributes2 forState:UIControlStateNormal];

    
}

-(void) viewFindSelectionChanged:(id)sender{
    
    switch ([segmentFindSingleController selectedSegmentIndex]) {
        case 0:
        {
            [search_Dining resignFirstResponder];
            oldSegmentedIndex = 0;
            [search_Dining resignFirstResponder];
            [self setSegmentColor:segmentFindSingleController];
            ReleaseAndNilify(table_Dining);
            
            nextPage = 0;
            if (retDetailsObjArray) {
                [retDetailsObjArray removeAllObjects];
            }
            if(globalRetailerService)
                [globalRetailerService removeAllObjects];
            
            sortFilObj.distanceSelected = TRUE;
            sortFilObj.alphabeticallySelected = FALSE;
            [self request_singlecatretailers:0 withObj:sortFilObj andsearchKey:search_Dining.text];
        }
            break;
        case 1:
        {
            [search_Dining resignFirstResponder];
            oldSegmentedIndex = 1;
            [search_Dining resignFirstResponder];

            [self setSegmentColor:segmentFindSingleController];
            ReleaseAndNilify(table_Dining);
            
            nextPage = 0;
            if (retDetailsObjArray) {
                [retDetailsObjArray removeAllObjects];
            }
            if(globalRetailerService)
                [globalRetailerService removeAllObjects];
            
            sortFilObj.distanceSelected = FALSE;
            sortFilObj.alphabeticallySelected = TRUE;
            
            [self request_singlecatretailers:0 withObj:sortFilObj andsearchKey:search_Dining.text];
            
        }
            break;
        case 2:{
            [search_Dining resignFirstResponder];
            [self setSegmentColor:segmentFindSingleController];
            
            
            for (int i=0; i<[retDetailsObjArray count]; i++) {
                [globalRetailerService addObject:[retDetailsObjArray objectAtIndex:i]];
            }
            if ([RegionApp isEqualToString:@"1"]){
                fromRegionFind = FALSE;//from region app
            }
            else
                fromFind=FALSE;
            
            fromFilters=FALSE;
            fromNearBy=FALSE;
            fromFindSingleCategory = TRUE;
            
            showMapFlag=TRUE;
            
            if ((/*fromFind==TRUE || fromRegionFind == TRUE ||*/ fromFindSingleCategory==TRUE) && [globalRetailerService count]>0) {
                
                //  [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
                [self showMap];
            }
            else{
                [segmentFindSingleController setSelectedSegmentIndex:oldSegmentedIndex];
                [self setSegmentColor:segmentFindSingleController];
                
                [UtilityManager showAlert:nil msg:NSLocalizedString(@"Retailers are not available to plot on map",@"Retailers are not available to plot on map")];
              
                
            }

        }
            break;
        default:
            break;
    }
    
}

- (SortAndFilter *)loadCustomObjectWithKey:(NSString *)key {
    
    NSData *encodedObject = [defaults objectForKey:key];
    SortAndFilter *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}


- (void)addItemViewController:(SwipesViewController *)controller didFinishEnteringItem:(SortAndFilter *)item
{
    sortFilObj = [[SortAndFilter alloc]initWithObject:item];
    
    NSLog(@"This was returned from ViewControllerB %@",item.selectedCatIds);
    for (UIView *addItemView in self.view.subviews)
        addItemView.userInteractionEnabled=YES;
    table_Dining.scrollEnabled = YES;
    
    table_Dining.allowsSelection =YES;
    iSwipeViewController= nil;
//    segmentFindSingleController.userInteractionEnabled = YES;
//    self.view.userInteractionEnabled = YES;
    if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
        nextPage = 0;
        if (retDetailsObjArray) {
            [retDetailsObjArray removeAllObjects];
        }
        if(globalRetailerService)
            [globalRetailerService removeAllObjects];
        if(sortFilObj.distanceSelected){
            
            [segmentFindSingleController setSelectedSegmentIndex:0];
            [self setSegmentColor:segmentFindSingleController];
            
        }
        else if(sortFilObj.alphabeticallySelected){
            
            [segmentFindSingleController setSelectedSegmentIndex:1];
            [self setSegmentColor:segmentFindSingleController];
            
        }

        
        [self request_singlecatretailers:0 withObj:item andsearchKey:search_Dining.text];
        
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        ReleaseAndNilify(table_Dining);
    }
    [table_Dining reloadData];
    
}


-(void)settableViewOnScreen
{
    
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    if (!table_Dining) {
        
        if(bottomBtn==1){
            table_Dining = [[UITableView alloc]initWithFrame:CGRectMake(0, 88, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-50-88) style:UITableViewStylePlain];
        }
        else{
            table_Dining = [[UITableView alloc]initWithFrame:CGRectMake(0, 88, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-88) style:UITableViewStylePlain];
        }
        
        table_Dining.dataSource=self;
        table_Dining.delegate=self;
        [table_Dining setBackgroundColor:[UIColor whiteColor]];
        table_Dining.tableFooterView.hidden=YES;
        table_Dining.translatesAutoresizingMaskIntoConstraints=NO;
        [self.view addSubview:table_Dining];
    }
    
    
}


#pragma mark navigation barbutton items actions
-(void)popBackToPreviousPage{
    [FindSingleBottomButtonID removeLastObject];
    [defaults setBool:NO forKey:@"ViewMore"];
    [defaults setValue:nil forKeyPath:@"commaSeperatedFilterValueIds"];
    [defaults setValue:nil forKeyPath:@"commaSeperatedFilterIds"];
    [defaults setValue:nil forKey:@"commaSeperatedCities"];
    [self.navigationController popViewControllerAnimated:NO];
    
}

-(void)navigateToSortPage
{
    
    if ([gsearch_Dining isFirstResponder]){
        
        gsearch_Dining.text = @"";
        [gsearch_Dining resignFirstResponder];
        [UIView beginAnimations:@"slideOut" context:NULL];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.3];
        
        //        view_gSearch.frame = viewFrame;
        [UIView commitAnimations];
        
    }
    else if ([search_Dining isFirstResponder]){
        
        search_Dining.text = @"";
        [search_Dining resignFirstResponder];
    }
    
    //    SingleCatGroupAndSort *iDinningGroupingAndSortingViewController = [[SingleCatGroupAndSort alloc]initWithNibName:@"SingleCatGroupAndSort" bundle:[NSBundle mainBundle]];
    //    iDinningGroupingAndSortingViewController.categoryName = categoryName;
    //    iDinningGroupingAndSortingViewController.catId = catId;
    //    iDinningGroupingAndSortingViewController.srchKey =  search_Dining.text;
    //    [self.navigationController pushViewController:iDinningGroupingAndSortingViewController animated:NO];
    //    [iDinningGroupingAndSortingViewController release];
    
    FindOptionsViewController *iFindOptionsListViewContoller = [[FindOptionsViewController alloc]initWithNibName:@"FindOptionsViewController" bundle:[NSBundle mainBundle]];
    iFindOptionsListViewContoller.typeStr = categoryName;
    iFindOptionsListViewContoller.catId = catId;
    iFindOptionsListViewContoller.srtFilterObj = sortFilObj;
    
    //        if (![catRetSearchKey isEqualToString:@""]){
    //            iFindOptionsListViewContoller.searchKey=catRetSearchKey;
    //        }
    
    if (![search_Dining.text isEqualToString:@""]){
        iFindOptionsListViewContoller.searchKey = search_Dining.text;
    }
    
    for (int i=0; i<[retDetailsObjArray count]; i++) {
        [globalRetailerService addObject:[retDetailsObjArray objectAtIndex:i]];
    }
    if ([RegionApp isEqualToString:@"1"]){
        fromRegionFind = FALSE;//from region app
    }
    else
        fromFind=FALSE;
    
    fromFilters=FALSE;
    fromNearBy=FALSE;
    fromFindSingleCategory = TRUE;
    
    [self.navigationController pushViewController:iFindOptionsListViewContoller animated:NO];
    
    
    
}


-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

-(void)bottomButtonPressed:(id)sender
{
    [defaults setBool:NO forKey:@"ViewMore"];
    //    [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    UIButton *btn = (UIButton*)sender;
    int tag = (int) btn.tag;
    
    bottomButtonDO *obj_findBottomDO = [arrFindSingleBottomButtonDO objectAtIndex:tag];
    if(!([[obj_findBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_findBottomDO.btnLinkTypeName]intValue]==26||[[obj_findBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_findBottomDO.btnLinkTypeName]intValue]==27||[[obj_findBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_findBottomDO.btnLinkTypeName]intValue]==28) )
    {
        [defaults setBool:YES forKey:BottomButton];
        
        [defaults setValue:obj_findBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
        
    }
    if ([[obj_findBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_findBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_findBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_findBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_findBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_findBottomDO.btnLinkTypeName);
    
    if ([obj_findBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_findBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    
    else if ([obj_findBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_findBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_findBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_findBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_findBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_findBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            self.iEventsListViewController = iEventsListViewController;
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    else{ //other menu types
        switch ([[obj_findBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_findBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                self.hotDeals = hotDeals;
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                // [hotDeals release];
                break;
                
            case 1://Scan Now
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_findBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_findBottomDO.bottomBtnID];
                }
                [EventsBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [NearbyBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                [self navigateToFindView];
            }
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                self.cityExp = citi;
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
                
                break;
                
            case 8://Visitors Bureau
                
                break;
                
            case 9://transportation
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];}
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
                
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=TRUE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_findBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    self.cityExp = cevc;
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    self.filters = filters;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    self.filterList = filterList;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                // [dvc release];
                
                break;
            }
                
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                self.faqList = faqCatList;
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                [FundraiserBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }break;
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        //[inform release];
                    }
                    
                }
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            //[citPref release];
                        }
                    }
                }
                
            }break;
                
            case 26://Map
                for (int i=0; i<[retDetailsObjArray count]; i++) {
                    [globalRetailerService addObject:[retDetailsObjArray objectAtIndex:i]];
                }
                if ([RegionApp isEqualToString:@"1"]){
                    fromRegionFind = FALSE;//from region app
                }
                else
                    fromFind=FALSE;
                
                fromFilters=FALSE;
                fromNearBy=FALSE;
                fromFindSingleCategory = TRUE;
                
                showMapFlag=TRUE;
                
                if ((/*fromFind==TRUE || fromRegionFind == TRUE ||*/ fromFindSingleCategory==TRUE) && [globalRetailerService count]>0) {
                    
                    //  [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
                    [self showMap];
                }
                else{
                    [UtilityManager showAlert:nil msg:NSLocalizedString(@"Retailers are not available to plot on map",@"Retailers are not available to plot on map")];
                }
                
                break;
                
            case 27:
            case 28://Sort/Filter
                //            if(iSwipeViewController)
                //                [iSwipeViewController release];
            {
                [defaults setValue:[defaults valueForKey:@"findSingCatMitemid"] forKey:KEY_MITEMID];
                SwipesViewController* iSwipeViewController1 = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                NSArray *viewControllers = [self.navigationController viewControllers];
                long vIndex=[viewControllers count]-2;
                iSwipeViewController1.delegate = [self.navigationController.viewControllers objectAtIndex:vIndex];
                iSwipeViewController1.module = @"Find Single";//Find All , Find Single , Events, CitiEXP
                
                iSwipeViewController1.categoryName = categoryName;
                iSwipeViewController1.catId = catId;
                iSwipeViewController1.sortFilObj = sortFilObj;
                if (![search_Dining.text isEqualToString:@""]){
                    iSwipeViewController1.srchKey = search_Dining.text;
                }
                [self.navigationController pushViewController:iSwipeViewController1 animated:NO];
            }
                
                break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                //privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
            default:
                break;
        }
    }
    //NSlog(@"%d",tag);
}

-(void)showMap
{
    showMapFlag=TRUE;
    if (/*fromFind==TRUE || fromRegionFind==TRUE || */fromFindSingleCategory == TRUE) {
        FindLocationServiceViewController *iShowMapController =[[FindLocationServiceViewController alloc]initWithNibName:@"FindLocationServiceViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:iShowMapController animated:NO];
        
    }
    //    else if (fromFilters==TRUE)
    //    {
    //        FilterRetailersList *iFindRetailersList = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
    //        self.filters = iFindRetailersList;
    //        [self.navigationController pushViewController:iFindRetailersList animated:NO];
    //        [iFindRetailersList release];
    //    }
    //    else if (fromNearBy==TRUE)
    //    {
    //        RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
    //        self.retailerListScreen = retailerListScreen;
    //        [self.navigationController pushViewController:retailerListScreen animated:NO];
    //        //[retailerListScreen release];
    //    }
    
}

//-(void)requestToGetUserData
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
//    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//    ReleaseAndNilify(xmlStr);
//}

-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
}
-(void)parseGetUserData:(id)response
{
    
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}
// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    // Calculate Y position for View
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [arrFindSingleBottomButtonDO count]; btnLoop++)
    {
        bottomButtonDO *obj_eventBottomDO = [arrFindSingleBottomButtonDO objectAtIndex:btnLoop];
        
        view = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, 80,bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrFindSingleBottomButtonDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrFindSingleBottomButtonDO count],bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_eventBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
    }
}









#pragma mark tableView delegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (![retDetailsObjArray count])return 1;
    
    //    RetailerDO *retObj = [retObjArray objectAtIndex:section];
    
    if ([defaults valueForKey:@"GroupFindSingleCatBy"]==nil){ //for distance sort
        
        if (nextPage == 1)
            return [retDetailsObjArray count]+1;
        else
            return [retDetailsObjArray count];
    }
    
    else if (nextPage == 1 && section == [retDetailsObjArray count]-1){ //for other than distance sort
        return [retDetailsObjArray count]+1;
    }
    
    else
        return [retDetailsObjArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    //    else
    //        return [retObjArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    float width = (IPAD ? 585.0f:200.0f);
    
    float noOflines = [self getLabelSize: [self retailerAddress:indexPath] withSize:width withFont:addressFont];
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        if(noOflines<2)
            return 50.0;
        else
            return 60.0;
    }
    else
    {
        return 85.0;
    }
    
    
}


-(CGFloat) getLabelSize : (NSString*) labelString withSize:(CGFloat) labelwidth withFont:(UIFont *) font
{
    
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    CGFloat noOflines = ceil(rect.size.height / font.lineHeight);
    
    
    return noOflines;
    
}

-(NSString *) retailerAddress:(NSIndexPath *)indexPath{
    NSString *address = @"";
    if([retDetailsObjArray count]){
        if (indexPath.row != [retDetailsObjArray count]){
            if((![[[retDetailsObjArray objectAtIndex:indexPath.row]retaileraddress2] isEqualToString:@"N/A"])&& [[retDetailsObjArray objectAtIndex:indexPath.row]retaileraddress2].length>0){
                address =  [NSString stringWithFormat:@"%@, %@",
                            [[retDetailsObjArray objectAtIndex:indexPath.row]retaileraddress1],[[retDetailsObjArray objectAtIndex:indexPath.row]retaileraddress2]];
            }
            else{
                address = [NSString stringWithFormat:@"%@",
                           [[retDetailsObjArray objectAtIndex:indexPath.row]retaileraddress1]];
            }
        }
    }
    return address;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (sortFilObj.distanceSelected==YES || (sortFilObj.alphabeticallySelected==YES && [retDetailsObjArray count]))
        return 25.0;
    else
        return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    float labelFont = 15.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        labelFont = 18.0;
    }
    
    
    UILabel *headerName = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 315, 26)];
    headerName.backgroundColor = [UIColor lightGrayColor];
    //        if([retObjArray count]>0)
    //        {
    //            RetailerDO *retObj = [retObjArray objectAtIndex:section];
    //            headerName.text = [NSString stringWithFormat:@" %@",retObj.catName];
    //        }
    //        else
    headerName.font = [UIFont boldSystemFontOfSize:labelFont];
    if([segmentFindSingleController selectedSegmentIndex]==0){
        headerName.text = [NSString stringWithFormat:@" Sorted by Distance"];
        return headerName ;
    }
    else if([segmentFindSingleController selectedSegmentIndex]==1) {
        headerName.text = [NSString stringWithFormat:@" Sorted by Name"];
        return headerName;
    }
    
    else{
        return NULL;
    }
    
    
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"Cell";
    
    SdImageView *asyncImageView = nil;
    UILabel *label = nil;
    UILabel *detailLabel = nil;
    UILabel *addressLabel = nil;
    UILabel *mileLabel = nil;
    UIImageView *specImage = nil;
    UILabel *hourFilterLabel = nil;
    
    UITableViewCell *cell = nil;
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] ;
    
    if ([retDetailsObjArray count]){
        
        
        if (indexPath.row != [retDetailsObjArray count] && indexPath.row<[retDetailsObjArray count]) {
            
            CGRect frame;
            if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
                frame.origin.x = 5;
                frame.origin.y = 5;
                frame.size.width = 40;
                frame.size.height = 40;
            }
            else
            {
                frame.origin.x = 5;
                frame.origin.y = 5;
                frame.size.width = 60;
                frame.size.height = 60;
            }
            
            asyncImageView = [[SdImageView alloc] initWithFrame:frame] ;
            asyncImageView.tag = ASYNC_IMAGE_TAG;
            // asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
            asyncImageView.backgroundColor = [UIColor clearColor];
            asyncImageView.layer.masksToBounds = YES;
            
            NSString *logoImageStr = [[retDetailsObjArray objectAtIndex:indexPath.row]logoImagePath];
            if(![logoImageStr isEqualToString:@"N/A"] )
            {
                [asyncImageView loadImage:logoImageStr];
                
                [cell.contentView addSubview:asyncImageView];
            }
            
            
            [cell.contentView addSubview:asyncImageView];
            
            label = [[UILabel alloc] init] ;
            if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                label.font = [UIFont boldSystemFontOfSize:19];
            else
                label.font = [UIFont boldSystemFontOfSize:14];
            label.textColor = [UIColor colorWithRGBInt:0x112e72];
            [cell.contentView addSubview:label];
            
            detailLabel = [[UILabel alloc] init] ;
            if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                detailLabel.font = [UIFont boldSystemFontOfSize:15];
            else
                detailLabel.font = [UIFont boldSystemFontOfSize:10];
            detailLabel.textColor = [UIColor darkGrayColor];
            detailLabel.numberOfLines = 1;
            detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            detailLabel.preferredMaxLayoutWidth = 150.0;
            [cell.contentView addSubview:detailLabel];
            
            addressLabel = [[UILabel alloc] init] ;
            if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
                addressLabel.font = [UIFont boldSystemFontOfSize:15];
                addressLabel.preferredMaxLayoutWidth = 350.0;
               // addressLabel.numberOfLines = 1;
                addressLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            }
            else
            {
                addressLabel.font = [UIFont boldSystemFontOfSize:10];
                addressLabel.preferredMaxLayoutWidth = 170.0;
                addressLabel.numberOfLines = 2;
                addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
                addressLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            }
            addressLabel.textColor = [UIColor darkGrayColor];
            
          
            
            [cell.contentView addSubview:addressLabel];
            
            
            mileLabel = [[UILabel alloc] init] ;
            if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                mileLabel.font = [UIFont boldSystemFontOfSize:15];
            else
                mileLabel.font = [UIFont boldSystemFontOfSize:10];
            mileLabel.textColor = [UIColor darkGrayColor];
            mileLabel.numberOfLines = 1;
            [cell.contentView addSubview:mileLabel];
            
            hourFilterLabel = [[UILabel alloc] init] ;
            if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                hourFilterLabel.font = [UIFont boldSystemFontOfSize:15];
            else
                hourFilterLabel.font = [UIFont boldSystemFontOfSize:10];
            hourFilterLabel.textColor = [UIColor darkGrayColor];
            hourFilterLabel.numberOfLines = 1;
            [cell.contentView addSubview:hourFilterLabel];
            
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
            
            
            
            
            if([retDetailsObjArray count] > indexPath.row)
            {
                SingleCatRetDetailsResponse *salesFlagObject=[retDetailsObjArray objectAtIndex:indexPath.row];

                label.text = [NSString stringWithFormat:@"%@", salesFlagObject.retailerName];
                
                //distance in miles
                mileLabel.text =[NSString stringWithFormat:@"%@", salesFlagObject.distance];
               
                //address1, address2 label
                
                if(!([salesFlagObject.retaileraddress2 isEqualToString:@"N/A"]) && salesFlagObject.retaileraddress2.length>0){
                    addressLabel.text =[NSString stringWithFormat:@"%@, %@",
                                        salesFlagObject.retaileraddress1,salesFlagObject.retaileraddress2];
                }
                else{
                    addressLabel.text =[NSString stringWithFormat:@"%@",
                                        salesFlagObject.retaileraddress1];
                }
//                addressLabel.text =[NSString stringWithFormat:@"%@, %@, %@, %@",
//                                                                salesFlagObject.retaileraddress1,salesFlagObject.retaileraddress1,salesFlagObject.retaileraddress1,salesFlagObject.retaileraddress1];

                //extracting city,state,zip from complete address
                
                detailLabel.text = [NSString stringWithFormat:@"%@,%@,%@",
                                    salesFlagObject.city,salesFlagObject.state, salesFlagObject.postalCode];
                
                
                                if (salesFlagObject.saleFlag) {
                                    specImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"retSpecialsIcon_o.png"]];
                                    [cell.contentView addSubview:specImage];
                    
                }
                if(![salesFlagObject.locationOpen isEqualToString:@"N/A"])
                hourFilterLabel.text =[NSString stringWithFormat:@"%@",salesFlagObject.locationOpen];
                else
                    hourFilterLabel.text = @"";
                float width = (IPAD ? 585.0f:200.0f);
                numberOfLines =[ self getLabelSize:addressLabel.text withSize:width withFont:addressFont];
                
                
                //[specImage release];
                // }
            }
            
        }
        
        //view more results...
        else if (indexPath.row == [retDetailsObjArray count] && nextPage == 1){
            
            loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            loading.center = cell.contentView.center;
            loading.color = [UIColor blackColor];
            
            CGRect frame;
            
            frame.origin.x = SCREEN_WIDTH/2;
            frame.origin.y = 5;
            frame.size.width = 40;
            frame.size.height = 40;
            
            if(DEVICE_TYPE == UIUserInterfaceIdiomPad)
                
                loading.frame = frame;
            [cell.contentView addSubview:loading];
            [loading startAnimating];
            cell.userInteractionEnabled = NO;
        }
        
        asyncImageView.translatesAutoresizingMaskIntoConstraints = NO;
        label.translatesAutoresizingMaskIntoConstraints = NO;
        detailLabel.translatesAutoresizingMaskIntoConstraints = NO;
        specImage.translatesAutoresizingMaskIntoConstraints = NO;
        addressLabel.translatesAutoresizingMaskIntoConstraints = NO;
        mileLabel.translatesAutoresizingMaskIntoConstraints = NO;
        hourFilterLabel.translatesAutoresizingMaskIntoConstraints = NO;
        if(specImage == nil && asyncImageView == nil && detailLabel == nil && label != nil)
            tableViewDictionary = NSDictionaryOfVariableBindings(label);
        else if(specImage == nil && detailLabel != nil)
            tableViewDictionary = NSDictionaryOfVariableBindings(asyncImageView,label,detailLabel,addressLabel,mileLabel,hourFilterLabel);
        else if(specImage != nil)
            tableViewDictionary = NSDictionaryOfVariableBindings(asyncImageView,label,detailLabel,specImage,addressLabel,mileLabel,hourFilterLabel);
        else
            tableViewDictionary = nil;
        [self setConstraints:cell];

    }
       return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.row == [retDetailsObjArray count] && nextPage == 1) {
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"] && lastRecord !=0) {
            search_Dining.userInteractionEnabled = NO;
            segmentFindSingleController.userInteractionEnabled = NO;
            
            [defaults setBool:YES forKey:@"ViewMore"];
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self request_singlecatretailers:lastRecord withObj:sortFilObj andsearchKey:search_Dining.text];
                dispatch_async(dispatch_get_main_queue(), ^{
                    // [table_Dining reloadData];
                    search_Dining.userInteractionEnabled = YES;
                   
                    segmentFindSingleController.userInteractionEnabled = YES;
                    if(iSwipeViewController){
                        for (UIView *displayView in self.view.subviews)
                            displayView.userInteractionEnabled=NO;
                         iSwipeViewController.view.userInteractionEnabled = YES;
                    }
                   

                });
            });
            
        }
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    RetailerDO *retObj = [retObjArray objectAtIndex:indexPath.section];
    //    [loading stopAnimating];
    //    ReleaseAndNilify(loading);
    [defaults setBool:NO forKey:@"ViewMore"];
    //    if (indexPath.row == [retDetailsObjArray count] && nextPage == 1) {
    //
    ////        [self request_singlecatretailers:lastRecord subCatId:[defaults valueForKey:@"SubCategoryId"] sortBy:[defaults valueForKey:@"SortFindSingleCatBy"] searchKey:search_Dining.text citiesIds:[defaults valueForKeyPath:@"commaSeperatedCities"] fValueIds:[defaults valueForKeyPath:@"commaSeperatedFilterValueIds"]  filterIds:[defaults valueForKeyPath:@"commaSeperatedFilterIds"]];
    //
    //        [self request_singlecatretailers:lastRecord withObj:sortFilObj andsearchKey:search_Dining.text];
    //
    //        return;
    //    }
    
    [defaults setObject:[[retDetailsObjArray objectAtIndex:indexPath.row]retListId]forKey:KEY_RLISTID];
    [defaults  setObject:[[retDetailsObjArray objectAtIndex:indexPath.row]ribbonAdImagePath]
                  forKey:@"ribbonAdImagePath"];
    
    selectedIndexPath = [indexPath copy];
    [self request_retsummary:indexPath];
    [table_Dining deselectRowAtIndexPath:[table_Dining indexPathForSelectedRow]  animated:YES];
}

-(void)setConstraints:(UITableViewCell*)cell{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
           if(tableViewDictionary != nil)
        {
            if (  DEVICE_TYPE==UIUserInterfaceIdiomPad )
            {
                if([tableViewDictionary objectForKey:@"asyncImageView"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[asyncImageView(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                if([tableViewDictionary objectForKey:@"label"] != nil)
                {
                    if(numberOfLines<2)
                        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(22)]-(1)-[addressLabel(30)]-(1)-[detailLabel(15)]"] options:0 metrics:0 views:tableViewDictionary]];
                    else
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(22)]-(1)-[addressLabel(30)]-(1)-[detailLabel(15)]"] options:0 metrics:0 views:tableViewDictionary]];
                     if([tableViewDictionary objectForKey:@"specImage"] == nil)
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[label(400)]"] options:0 metrics:0 views:tableViewDictionary]];
                    else
                      [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[label(350)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                if([tableViewDictionary objectForKey:@"detailLabel"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[detailLabel(585)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(65)-[addressLabel(585)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                
                
                if([tableViewDictionary objectForKey:@"specImage"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[specImage(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[mileLabel(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(600)-[specImage(50)]-10-[mileLabel(70)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                else{
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[mileLabel(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(660)-[mileLabel(70)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(24)-[hourFilterLabel(50)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(660)-[hourFilterLabel(50)]"] options:0 metrics:0 views:tableViewDictionary]];

                
            }
            else
            {
                if([tableViewDictionary objectForKey:@"asyncImageView"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[asyncImageView(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                if([tableViewDictionary objectForKey:@"label"] != nil)
                {
                    if(numberOfLines<2)
                        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(2)-[label(15)]-(1)-[addressLabel(15)]-(1)-[detailLabel(13)]"] options:0 metrics:0 views:tableViewDictionary]];
                    else
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(2)-[label(15)]-(2)-[addressLabel(25)]-(1)-[detailLabel(13)]"] options:0 metrics:0 views:tableViewDictionary]];
                     if([tableViewDictionary objectForKey:@"specImage"] == nil)
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[label(200)]"] options:0 metrics:0 views:tableViewDictionary]];
                    else
                         [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[label(150)]"] options:0 metrics:0 views:tableViewDictionary]];
                    
                }
                
                if([tableViewDictionary objectForKey:@"detailLabel"] != nil)
                {
                    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(24)-[addressLabel(20)]-30-[detailLabel(50)]-|"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[detailLabel]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[addressLabel]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                if([tableViewDictionary objectForKey:@"specImage"] != nil)
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[specImage(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[mileLabel(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(215)-[specImage(40)]-5-[mileLabel(60)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                else{
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-8)-[mileLabel(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(260)-[mileLabel(60)]"] options:0 metrics:0 views:tableViewDictionary]];
                }
                
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[hourFilterLabel(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(260)-[hourFilterLabel(40)]"] options:0 metrics:0 views:tableViewDictionary]];
                
            }
        }
    
}

#pragma mark request methods
-(void)request_singlecatretailers:(int)lastVisitedRecord withObj:(SortAndFilter *)sortFilOb andsearchKey:(NSString*)searchKey
{
    iWebRequestState = singlecatretailers;
    // [HubCitiAppDelegate showActivityIndicator];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    //[parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    NSLog(@"MItemId: %@",[defaults valueForKey:KEY_MITEMID]);
    
    [defaults setValue:[defaults valueForKey:@"findSingCatMitemid"] forKey:KEY_MITEMID];
    
    [parameters setValue:[NSString stringWithFormat:@"%d",lastVisitedRecord] forKey:@"lastVisitedNo"];
    if ([defaults valueForKey:KEY_MAINMENUID])
        [parameters setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    if(![defaults boolForKey:BottomButton])
    {
        if([defaults valueForKey:KEY_MITEMID])
            [parameters setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
    }
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID] && [defaults valueForKey:KEY_MITEMID])
    {
        [parameters setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
    }
    else{
        if ([FindSingleBottomButtonID count]>=1)
            [parameters setValue:[FindSingleBottomButtonID objectAtIndex:[FindSingleBottomButtonID count]-1] forKey:@"bottomBtnId"];
        
        else if([defaults valueForKey:KEY_BOTTOMBUTTONID])
            [parameters setValue:[defaults valueForKey:KEY_BOTTOMBUTTONID] forKey:@"bottomBtnId"];
    }
    [parameters setValue:@"ASC" forKey:@"sortOrder"];
    if ([segmentFindSingleController selectedSegmentIndex]==0)
        [parameters setValue:@"distance" forKey:@"sortColumn"];
    
 else  if ([segmentFindSingleController selectedSegmentIndex]==1)
        [parameters setValue:@"atoz" forKey:@"sortColumn"];
    
    if ([searchKey length]) {
        [parameters setValue:searchKey forKey:@"searchKey"];
    }
    NSLog(@"KEYZIPCODE:%@",[defaults valueForKey:KEYZIPCODE]);
    if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length])
    {
        [parameters setValue:[defaults  valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        [parameters setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        if ([[defaults valueForKey:KEYZIPCODE] length]){
            [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"userPostalCode"];
        }
        
    }
    
    else if ([[defaults valueForKey:KEYZIPCODE] length])
    {
        [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"postalCode"];
    }
    if (sortFilOb.selectedCitiIds)
    {
        [parameters setValue:sortFilOb.selectedCitiIds forKey:@"cityIds"];
    }
    else{
        if([RegionApp isEqualToString:@"1"] && [defaults valueForKey:@"sortedCitiIDs"])
        {
            [parameters setValue:[defaults valueForKey:@"sortedCitiIDs"] forKey:@"cityIds"];
            
        }
    }
    if (sortFilOb.selectedOthersIds) {
        [parameters setValue:sortFilOb.selectedOthersIds forKey:@"fValueId"];
    }
    
    if (sortFilOb.selectedOptIds) {
        [parameters setValue:sortFilOb.selectedOptIds forKey:@"filterId"];
    }
    
    if (sortFilOb.selectedInterestsIds) {
        [parameters setValue:sortFilOb.selectedInterestsIds forKey:@"interests"];
    }
    if (sortFilOb.selectedCatIds) {
        [parameters setValue:sortFilOb.selectedCatIds forKey:@"subCatIds"];
    }
    
    if (sortFilOb.localSpecialSelected) {
        [parameters setValue:@"1" forKey:@"locSpecials"];
    }
    else{
        [parameters setValue:@"0" forKey:@"locSpecials"];
    }
    
    
    if([defaults valueForKey:@"FindRadius"])
    {
        [parameters setValue:[defaults valueForKey:@"FindRadius"] forKey:@"radius"];
        
    }
    else
    {
        [parameters setValue:@"50" forKey:@"radius"];
        
    }
    
    
    [parameters setValue:categoryName forKey:@"catName"];
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [timeFormatter setTimeZone:timeZone];
    [timeFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *timeInUTC = [timeFormatter stringFromDate:currentTime];
    NSLog(@"Current time %@", timeInUTC);
    
    [parameters setValue:timeInUTC forKey:@"requestedTime"];
    
    DLog(@"parameter: %@",parameters);
    NSString *urlString = [NSString stringWithFormat:@"%@find/singlecatretailersjson",BASE_URL];
    //NSString *urlString = [NSString stringWithFormat:@"http://sdw2107:9990/HubCiti2.4.2/find/singlecatretailersjson"];
    DLog(@"Url: %@",urlString);
    if ([defaults boolForKey:@"ViewMore"])
    {
        NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
        id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        [self parse_SingleCatRetailers:responseData];
    }
    else{
        DLog(@"parameter: %@",parameters);
        [HubCitiAppDelegate showActivityIndicator];
        HTTPClient *client = [HTTPClient sharedHTTPClient];
        client.delegate = self;
        NSString *urlString = [NSString stringWithFormat:@"%@find/singlecatretailersjson",BASE_URL];
        //NSString *urlString = [NSString stringWithFormat:@"http://sdw2107:9990/HubCiti2.4.2/find/singlecatretailersjson"];
        DLog(@"Url: %@",urlString);
        [client sendRequest : parameters : urlString];
    }
    
    
    //
    //    iWebRequestState = singlecatretailers;
    //    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_USERID], [defaults valueForKey:KEY_HUBCITIID]];
    //     NSLog(@"MItemId: %@",[defaults valueForKey:KEY_MITEMID]);
    //
    ////    [reqStr appendFormat:@"<subCatId>%@</subCatId>",subCatId];
    //
    //    [reqStr appendFormat:@"<lastVisitedNo>%d</lastVisitedNo>",lastVisitedRecord];
    //
    //    if ([defaults valueForKey:KEY_MAINMENUID])
    //        [reqStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    //
    //    if(![defaults boolForKey:BottomButton])
    //    {
    //        if([defaults valueForKey:KEY_MITEMID])
    //            [reqStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];
    //    }
    //        else if ([defaults valueForKey:KEY_BOTTOMBUTTONID] && [defaults valueForKey:KEY_MITEMID])
    //    {
    //        [reqStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];
    //
    //    }
    //    else{
    //        if ([FindSingleBottomButtonID count]>=1)
    //            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[FindSingleBottomButtonID objectAtIndex:[FindSingleBottomButtonID count]-1]];
    //
    //
    //       else if([defaults valueForKey:KEY_BOTTOMBUTTONID])
    //            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    //    }
    //
    //    [reqStr appendFormat:@"<sortOrder>ASC</sortOrder>"];
    //
    //    if (sortFilOb.distanceSelected==true)
    //        [reqStr appendFormat:@"<sortColumn>distance</sortColumn>"];
    //
    //    if (sortFilOb.alphabeticallySelected==true)
    //        [reqStr appendFormat:@"<sortColumn>atoz</sortColumn>"];
    //
    //    if ([searchKey length]) {
    //        [reqStr appendFormat:@"<searchKey>%@</searchKey>",searchKey];
    //    }
    //
    //    if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorized) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
    //
    //        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
    //         [defaults  valueForKey:KEY_LATITUDE]];
    //
    //    }
    //    /* else if ([[defaults  valueForKey:KEYZIPCODE]length]){
    //
    //     [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    //     }*/
    //
    //
    //    if (sortFilOb.selectedCitiIds) {
    //        [reqStr appendFormat:@"<cityIds>%@</cityIds>",sortFilOb.selectedCitiIds];
    //        }
    //    //    <fValueIds>1,2</fValueIds>// New Input Request, Input value can be null.
    //    //    <filterIds>3,4</filterIds>
    //
    //    if (sortFilOb.selectedOthersIds) {
    //        [reqStr appendFormat:@"<fValueId>%@</fValueId>",sortFilOb.selectedOthersIds];
    //    }
    //
    //    if (sortFilOb.selectedOptIds) {
    //        [reqStr appendFormat:@"<filterId>%@</filterId>",sortFilOb.selectedOptIds];
    //    }
    //
    //    if (sortFilOb.selectedInterestsIds) {
    //        [reqStr appendFormat:@"<interests>%@</interests>",sortFilOb.selectedInterestsIds];
    //    }
    //
    //
    //    if (sortFilOb.selectedCatIds) {
    //        [reqStr appendFormat:@"<subCatIds>%@</subCatIds>",sortFilOb.selectedCatIds];
    //    }
    //
    //    if (sortFilOb.localSpecialSelected) {
    //        [reqStr appendFormat:@"<locSpecials>1</locSpecials>"];
    //    }
    //    else{
    //        [reqStr appendFormat:@"<locSpecials>0</locSpecials>"];
    //    }
    //
    //    [reqStr appendFormat:@"<catName>%@</catName></RetailerDetail>",categoryName];
    //
    //    NSString *urlStr = [NSString stringWithFormat:@"%@find/singlecatretailers",BASE_URL];
    //
    //    if ([defaults boolForKey:@"ViewMore"]) {
    //        NSString *response = [ConnectionManager establishPostConnection:urlStr withParam:reqStr];
    //
    //
    //        [self parse_SingleCatRetailers:response];
    //
    //    }
    //    else{
    //    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    //    }
}

//fetch the retailer summary of a ScanSee retailer
-(void)request_retsummary:(NSIndexPath*)indexPath
{
    iWebRequestState = RETSUMMARY;
    //    RetailerDO *retObj = [retObjArray objectAtIndex:indexPath.section];
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if([retDetailsObjArray count] > indexPath.row){
        
        [requestStr appendFormat:@"<retailerId>%@</retailerId>",[[retDetailsObjArray objectAtIndex:indexPath.row]retailerId]];
        [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[[retDetailsObjArray objectAtIndex:indexPath.row]retailLocationId]];
        [requestStr appendFormat:@"<retListId>%@</retListId>",[[retDetailsObjArray objectAtIndex:indexPath.row] retListId]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [requestStr appendFormat:@"<scanTypeId>%@</scanTypeId>",@"0"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    //[requestStr release];
    
}


-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=TRUE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    NSLog(@"mitemID: %@",[defaults valueForKey:KEY_MITEMID]);
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if (![defaults boolForKey:BottomButton]) {
        if([defaults valueForKey:KEY_MITEMID])
            [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    
    else{
        if([defaults valueForKey:KEY_BOTTOMBUTTONID])
            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}



-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}

//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if (![defaults boolForKey:BottomButton]) {
        if([defaults valueForKey:KEY_MITEMID])
            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    
    else{
        if([defaults valueForKey:KEY_BOTTOMBUTTONID])
            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}



#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        return;
    
    [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    [self showActionSheet];
    
    
}

-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
           
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
       
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}




- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
                }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}






#pragma mark parse methods
-(void)responseData:(NSString*)response{
    
    switch (iWebRequestState) {
        case singlecatretailers:
            [self parse_SingleCatRetailers:response];
            break;
        case RETSUMMARY:
            [self parse_RetSummary:response];
            break;
            
        case PARTNERRET:
            [self parse_PartnerRet:response];
            break;
            
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
            
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
            
            
        default:
            break;
    }
}



-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
           
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            self.cityExp = cevc;
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            self.filters = pvc;
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}

//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];                    //[alert release];
                    
                    //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage =[[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
       
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}

-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}


//-(void) navigateToScanNow
//{
//    [defaults setBool:YES forKey:@"KEY_SCANSPLASH"];
//    [defaults setValue:@"scanNow" forKey:KEY_SEARCHFROM];
//    [defaults setObject:@"YES" forKey:@"KEY_SCANNERCALLED"];
//
//    int menulevel = [[defaults valueForKey:KEY_MENULEVEL]integerValue];
//    if(menulevel > 1)
//        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
//    else
//        [defaults  setBool:NO forKey:@"DisplayBackBarButton"];
//
//
//    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
//    ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];
//    [self.navigationController pushViewController:viewScanNowScreen animated:NO];
//    [viewScanNowScreen release];
//
//}

-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
        
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}

//Call Find Service and Class
-(void)navigateToFindView
{
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    navigatedFromMainMenu=false;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}

-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    self.iEventsListViewController = iEventsListViewController;
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}


-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}




-(void)releaseAllocatedObjects
{
    NSArray *arrSubViews = [self.view subviews];
    for(int i=0; i<[arrSubViews count]; i++)
    {
        UIView *releaseObjView = (UIView*)[arrSubViews objectAtIndex:i];
        if(releaseObjView.tag == 99)
            [releaseObjView removeFromSuperview];
        if([releaseObjView isKindOfClass:[bottomButtonView class]])
        {
            [releaseObjView removeFromSuperview];
        }
    }
    
    if([arrFindSingleBottomButtonDO count]>0)
    {
        [arrFindSingleBottomButtonDO removeAllObjects];
        // [arrFindSingleBottomButtonDO release];
        arrFindSingleBottomButtonDO = nil;
    }
    if([arrBottomButtonViewContainer count]>0)
    {
        [arrBottomButtonViewContainer removeAllObjects];
        // [arrBottomButtonViewContainer release];
        arrBottomButtonViewContainer = nil;
    }
    
}




-(void)parse_SingleCatRetailers:(id)response
{
    if (response == nil)
        return;
    
    singleCatResponse = [[SingleCatRetResponse alloc] init];
    
    [singleCatResponse setValuesForKeysWithDictionary:response];
    if([singleCatResponse.responseCode isEqualToString:@"10000"])
    {
        if(singleCatResponse.nextPage)
        {
            nextPage = [singleCatResponse.nextPage intValue];
        }
        if(singleCatResponse.maxRowNum)
        {
            lastRecord = [singleCatResponse.maxRowNum intValue];
        }
        bottomBtn = [singleCatResponse.bottomBtn intValue];
        if(singleCatResponse.retailerDetail)
        {
            for(int i=0;i< singleCatResponse.retailerDetail.count;i++)
            {
                SingleCatRetDetailsResponse *retDetailsObj = [[SingleCatRetDetailsResponse alloc]init];
                NSDictionary* dictList = singleCatResponse.retailerDetail[i];
                [retDetailsObj setValuesForKeysWithDictionary:dictList];
                [retDetailsObjArray addObject:retDetailsObj];
                [retailerIDArray addObject:retDetailsObj.retailerId];
                
                [retailLocationIDArray addObject:retDetailsObj.retailLocationId];
            }
            
        }
        
        if(bottomBtn==1)
        {
            if (singleCatResponse.bottomBtnList)
            {
                arrFindSingleBottomButtonDO =[[NSMutableArray alloc]init];
                for(int i=0;i<singleCatResponse.bottomBtnList.count;i++)
                {
                    bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                    NSDictionary* dictList = singleCatResponse.bottomBtnList[i];
                    [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                    
                    
                    if(obj_findBottomDO.bottomBtnName)
                    {
                        
                    }
                    else
                    {
                        obj_findBottomDO.bottomBtnName = @" ";
                    }
                    
                    if(obj_findBottomDO.bottomBtnImg)
                    {
                        
                    }
                    else
                    {
                        obj_findBottomDO.bottomBtnImg = @" ";
                    }
                    
                    if(obj_findBottomDO.bottomBtnImgOff)
                    {
                        
                    }
                    else
                    {
                        obj_findBottomDO.bottomBtnImgOff = @" ";
                    }
                    
                    
                    if(obj_findBottomDO.btnLinkID){
                        if(obj_findBottomDO.btnLinkTypeName){
                            if ([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"]) //for filters
                                [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                        }
                    }
                    
                    [arrFindSingleBottomButtonDO addObject:obj_findBottomDO];
                }
                if([arrFindSingleBottomButtonDO count] > 0)
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self setBottomBarMenu];
                    });
                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                    
                }
                else
                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self settableViewOnScreen];
        });
        
    }
    
    else if([singleCatResponse.responseCode isEqualToString:@"10005"])
    {
        if(singleCatResponse.mainMenuId)
        {
            [defaults setValue:singleCatResponse.mainMenuId forKey:KEY_MAINMENUID];
        }
        
        
        bottomBtn = [singleCatResponse.bottomBtn intValue];
        if(singleCatResponse.nextPage)
        {
            nextPage = [singleCatResponse.nextPage intValue];
        }
        
        if(bottomBtn==1)
        {
            if (singleCatResponse.bottomBtnList)
            {
                arrFindSingleBottomButtonDO =[[NSMutableArray alloc]init];
                
                for(int i=0;i<singleCatResponse.bottomBtnList.count;i++)
                {
                    bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                    NSDictionary* dictList = singleCatResponse.bottomBtnList[i];
                    [obj_findBottomDO setValuesForKeysWithDictionary:dictList];
                    if(obj_findBottomDO.bottomBtnName)
                    {
                        
                    }
                    else{
                        obj_findBottomDO.bottomBtnName = @" ";
                    }
                    if(obj_findBottomDO.bottomBtnImg)
                    {
                        
                    }
                    else{
                        obj_findBottomDO.bottomBtnImg = @" ";
                    }
                    if(obj_findBottomDO.bottomBtnImgOff)
                    {
                        
                    }
                    else{
                        obj_findBottomDO.bottomBtnImgOff = @" ";
                    }
                    if(obj_findBottomDO.btnLinkID) {
                        if(obj_findBottomDO.btnLinkTypeName) {
                            if([obj_findBottomDO.btnLinkTypeName isEqualToString:@"Filters"])
                                [SharedManager setRetGroupId:[obj_findBottomDO.btnLinkID intValue]];
                        }
                    }
                    
                    [arrFindSingleBottomButtonDO addObject:obj_findBottomDO];
                }
                if([arrFindSingleBottomButtonDO count] > 0)
                {
                    // Set Bottom Menu Button after read from response data
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self setBottomBarMenu];
                    });
                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                    
                }
                else
                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            }
        }
        
        NSString *response=singleCatResponse.responseText;
        if([response isEqualToString:@"No Records Found."])
        {
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:response];
        }
        else{
            
            NSString *responseTextStr ;
            if(singleCatResponse.responseText.length == 0){
                responseTextStr = @"No Records found";
            }
            else{
                responseTextStr= [singleCatResponse.responseText stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
            }
            [UtilityManager showFormatedAlert:responseTextStr];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
       
            [self settableViewOnScreen];
        });
    }
    else
    {
        lastRecord = 0;
        nextPage = 0;
        NSString *responseTextStr = singleCatResponse.responseText;
        dispatch_async(dispatch_get_main_queue(), ^{
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        [loading stopAnimating];
        ReleaseAndNilify(loading);
        
        [self settableViewOnScreen];
         });
        //return;
        
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if(![defaults boolForKey:@"ViewMore"]){
            [table_Dining setContentOffset:CGPointZero];
        }
        [table_Dining reloadData];
        
        [defaults setBool:NO forKey:@"ViewMore"];
    });
    [self sendClicksNImpressions];
    //    dispatch_queue_t impressionQueue = dispatch_queue_create("impressionQueue", DISPATCH_QUEUE_CONCURRENT);
    //    dispatch_async(impressionQueue,^(void){
    //
    //    });
    
    
    
    
    //    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    //    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    //
    //    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]){
    //
    //
    //        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbxml.rootXMLElement];
    //        if(nextPageElement)
    //            nextPage = [[TBXML textForElement:nextPageElement]intValue];
    //
    //        TBXMLElement *maxRowNumElement = [TBXML childElementNamed:@"maxRowNum" parentElement:tbxml.rootXMLElement];
    //
    //        if(maxRowNumElement)
    //            lastRecord = [[TBXML textForElement:maxRowNumElement]intValue];
    //
    //        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbxml.rootXMLElement]]intValue];
    //
    //        //    TBXMLElement *catListElement = [TBXML childElementNamed:@"catList" parentElement:tbxml.rootXMLElement];
    //        //
    //        //    if(catListElement)
    //        //    {
    //        //        TBXMLElement *CategoryInfoElement = [TBXML childElementNamed:@"CategoryInfo" parentElement:catListElement];
    //        //
    //        //        RetailerDO *retObj;
    //        //
    //        //        //only in case of 'distance' sorting the category name should nbe considered - so bypassing that part
    //        //        if (CategoryInfoElement!=nil && [defaults valueForKey:@"GroupFindSingleCatBy"]==nil){
    //        //
    //        //            //for pagination to append existing list
    //        //            if (![retObjArray count]){
    //        //                retObj = [[RetailerDO alloc]init];
    //        //                retObj.retDetailsObjArray = [[NSMutableArray alloc]init];
    //        //            }
    //        //
    //        //            else{
    //        //                retObj = [retObjArray lastObject];
    //        //                retObj.retDetailsObjArray = [[NSMutableArray alloc]initWithArray:retObj.retDetailsObjArray];
    //        //            }
    //        //        }
    //        //
    //        //        while (CategoryInfoElement)
    //        //        {
    //        //
    //        //            //group name should not be displayed for 'distance' sort
    //        //            if ([defaults valueForKey:@"GroupFindSingleCatBy"]!=nil){
    //        //
    //        //                retObj = [[RetailerDO alloc]init];
    //        //                retObj.retDetailsObjArray = [[NSMutableArray alloc]init];
    //        //                BOOL isCatRepeted = NO;
    //        //                TBXMLElement *groupContentElement = [TBXML childElementNamed:@"groupContent" parentElement:CategoryInfoElement];
    //        //                if(groupContentElement)
    //        //                    retObj.catName = [TBXML textForElement:groupContentElement];
    //        //
    //        //                //if incoming group name is not continuation
    //        ////                if (![[[retObjArray lastObject]catName] isEqualToString:[TBXML textForElement:groupContentElement]])
    //        ////                {
    //        ////                    retObj.retDetailsObjArray = [[NSMutableArray alloc]init];
    //        ////                }
    //        ////                else
    //        ////                {
    //        ////                    // Copy Last Object Array in current AlertEventDO Object "if Same caegory Repeted"
    //        ////                    RetailerDO *repeatedRetObj = [retObjArray lastObject];
    //        ////                    retObj.retDetailsObjArray = [[NSMutableArray alloc]initWithArray:repeatedRetObj.retDetailsObjArray];
    //        ////                    [retObjArray removeLastObject];
    //        ////                }
    //        //                if([retObjArray count] > 0 && ![retObj.catName isEqualToString:@"N/A"])
    //        //                {
    //        //                    RetailerDO *iretObjrepted = [[[RetailerDO alloc]init];
    //        //
    //        //                    for(int i=0; i<[retObjArray count]; i++)
    //        //                    {
    //        //                        iretObjrepted = [retObjArray objectAtIndex:i];
    //        //                        isCatRepeted = NO;
    //        //                        if([iretObjrepted.catName isEqualToString:retObj.catName])
    //        //                        {
    //        //                            isCatRepeted = YES;
    //        //
    //        //                            [retObj.retDetailsObjArray addObjectsFromArray:iretObjrepted.retDetailsObjArray];
    //        //
    //        //                            [retObjArray replaceObjectAtIndex:i withObject:retObj];
    //        //                            break;
    //        //                        }
    //        //
    //        //                    }
    //        //                    if(isCatRepeted==NO)
    //        //                    {
    //        //                        [retObjArray addObject:retObj];
    //        //                    }
    //        //
    //        //                }
    //        //                else
    //        //                {
    //        //                    if (![retObj.catName isEqualToString:@""])
    //        //                        [retObjArray addObject:retObj];
    //        //
    //        //                }
    //        //            }
    //
    //        //            else{
    //        //                retObj.retDetailsObjArray = [[NSMutableArray alloc]init];
    //        //            }
    //
    //        TBXMLElement *retDetListElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbxml.rootXMLElement];
    //
    //        if(retDetListElement)
    //        {
    //            TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retDetListElement];
    //
    //            while (RetailerDetailElement)
    //            {
    //                RetailerDetailsDO *retDetailsObj = [[RetailerDetailsDO alloc]init];
    //
    //                TBXMLElement *retailerIdElement = [TBXML childElementNamed:@"retailerId" parentElement:RetailerDetailElement];
    //                TBXMLElement *retailerNameElement = [TBXML childElementNamed:@"retailerName" parentElement:RetailerDetailElement];
    //                TBXMLElement *retailLocationIdElement = [TBXML childElementNamed:@"retailLocationId" parentElement:RetailerDetailElement];
    //                TBXMLElement *completeAddressElement = [TBXML childElementNamed:@"completeAddress" parentElement:RetailerDetailElement];
    //                TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
    //                TBXMLElement *logoImagePathElement = [TBXML childElementNamed:@"logoImagePath" parentElement:RetailerDetailElement];
    //                TBXMLElement *bannerAdImagePathElement = [TBXML childElementNamed:@"bannerAdImagePath" parentElement:RetailerDetailElement];
    //                TBXMLElement *ribbonAdImagePathElement = [TBXML childElementNamed:@"ribbonAdImagePath" parentElement:RetailerDetailElement];
    //                TBXMLElement *ribbonAdURLElement = [TBXML childElementNamed:@"ribbonAdURL" parentElement:RetailerDetailElement];
    //                TBXMLElement *retListIdElement = [TBXML childElementNamed:@"retListId" parentElement:RetailerDetailElement];
    //                TBXMLElement *saleFlagElement = [TBXML childElementNamed:@"saleFlag" parentElement:RetailerDetailElement];
    //                TBXMLElement *retLatitudeElement = [TBXML childElementNamed:@"retLatitude" parentElement:RetailerDetailElement];
    //
    //                TBXMLElement *retLongitudeElement = [TBXML childElementNamed:@"retLongitude" parentElement:RetailerDetailElement];
    //                if(retLatitudeElement!=nil)
    //                    retDetailsObj.retLat = [TBXML textForElement:retLatitudeElement];
    //
    //                if(retLongitudeElement!=nil)
    //                    retDetailsObj.retLong = [TBXML textForElement:retLongitudeElement];
    //
    //                if (saleFlagElement)
    //                    retDetailsObj.saleFlag = [TBXML textForElement:saleFlagElement];
    //
    //                if(retailerIdElement)
    //                    retDetailsObj.retId = [TBXML textForElement:retailerIdElement];
    //
    //                if(retailerNameElement)
    //                    retDetailsObj.retName = [TBXML textForElement:retailerNameElement];
    //
    //                if(retailLocationIdElement)
    //                    retDetailsObj.retLocnId = [TBXML textForElement:retailLocationIdElement];
    //
    //                if(completeAddressElement)
    //                    retDetailsObj.retCompleteAddress = [TBXML textForElement:completeAddressElement];
    //
    //                if(distanceElement)
    //                    retDetailsObj.distance = [TBXML textForElement:distanceElement];
    //
    //                if(logoImagePathElement)
    //                    retDetailsObj.logoImgPath = [TBXML textForElement:logoImagePathElement];
    //
    //                if(bannerAdImagePathElement)
    //                    retDetailsObj.bannerAdImgPath = [TBXML textForElement:bannerAdImagePathElement];
    //
    //                if(ribbonAdImagePathElement)
    //                    retDetailsObj.ribbonAdImgPath = [TBXML textForElement:ribbonAdImagePathElement];
    //
    //                if(ribbonAdURLElement)
    //                    retDetailsObj.ribbonAdUrl = [TBXML textForElement:ribbonAdURLElement];
    //
    //                if(retListIdElement)
    //                    retDetailsObj.retListId = [TBXML textForElement:retListIdElement];
    //
    //                RetailerDetailElement = [TBXML nextSiblingNamed:@"RetailerDetail" searchFromElement:RetailerDetailElement];
    //
    //                [retDetailsObjArray addObject:retDetailsObj];
    //
    //                //                    [retObj.retDetailsObjArray addObject:retDetailsObj];
    //               // [retDetailsObj release];
    //            }
    //
    //        }
    //
    //        ///
    //        //only in case of 'distance' sorting the category name should nbe considered - so bypassing that part
    //        //            if (CategoryInfoElement!=nil && [defaults valueForKey:@"GroupFindSingleCatBy"]==nil){
    //        //
    //        //                //for pagination to append existing list
    //        //                if (![retObjArray count]){
    //        //                    [retObjArray addObject:retObj];
    //        //                }
    //        //
    //        //                else{
    //        ////                    [retObjArray removeLastObject];
    //        //                    [retObjArray addObject:retObj];
    //        //                }
    //        //            }
    //        //            else
    //        //            {
    //        //            [retObjArray addObject:retObj];
    //        //            //[retObj release];
    //        //            }
    //
    //        //            CategoryInfoElement = [TBXML nextSiblingNamed:@"CategoryInfo" searchFromElement:CategoryInfoElement];
    //        //        }
    //        //
    //        //    }
    //
    //        if(bottomBtn==1){
    //            TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbxml.rootXMLElement];
    //            if (bottomButtonListElement)
    //            {
    //                arrFindSingleBottomButtonDO =[[NSMutableArray alloc]init];
    //                TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
    //                while(BottomButtonElement)
    //                {
    //
    //                    bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
    //
    //
    //                    TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
    //                    TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
    //                    TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
    //                    TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
    //                    TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
    //                    TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
    //                    TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
    //                    TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
    //
    //
    //                    if(bottomBtnIDElement)
    //                        obj_findBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
    //
    //                    if(bottomBtnNameElement)
    //                        obj_findBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
    //                    else
    //                        obj_findBottomDO.bottomBtnName = @" ";
    //
    //                    if(bottomBtnImgElement)
    //                        obj_findBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
    //                    else
    //                        obj_findBottomDO.bottomBtnImg = @" ";
    //
    //                    if(bottomBtnImgOffElement)
    //                        obj_findBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
    //                    else
    //                        obj_findBottomDO.bottomBtnImgOff = @" ";
    //
    //                    if(btnLinkTypeIDElement)
    //                        obj_findBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
    //
    //                    if(btnLinkTypeNameElement)
    //                        obj_findBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
    //
    //                    if(btnLinkIDElement){
    //                        obj_findBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
    //                        if(btnLinkTypeNameElement){
    //                            if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
    //                                [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
    //                        }
    //                    }
    //
    //                    if(positionElement)
    //                        obj_findBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
    //
    //
    //
    //                    BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
    //                    [arrFindSingleBottomButtonDO addObject:obj_findBottomDO];
    //                    //[obj_findBottomDO release];
    //                }
    //                if([arrFindSingleBottomButtonDO count] > 0)
    //                {
    //                    // Set Bottom Menu Button after read from response data
    //                    [self setBottomBarMenu];
    //                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
    //
    //                }
    //                else
    //                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
    //            }
    //        }
    //        [self settableViewOnScreen];
    //
    //    }
    //
    //    else if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10005"])
    //    {
    //
    //        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
    //        if(mainMenuElement)
    //            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
    //        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbxml.rootXMLElement]]intValue];
    //
    //        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbxml.rootXMLElement];
    //        if(nextPageElement)
    //            nextPage = [[TBXML textForElement:nextPageElement]intValue];
    //
    //        if(bottomBtn==1){
    //            TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbxml.rootXMLElement];
    //            if (bottomButtonListElement)
    //            {
    //                arrFindSingleBottomButtonDO =[[NSMutableArray alloc]init];
    //
    //                TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
    //                while(BottomButtonElement)
    //                {
    //
    //                    bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
    //
    //
    //                    TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
    //                    TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
    //                    TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
    //                    TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
    //                    TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
    //                    TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
    //                    TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
    //                    TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
    //
    //
    //                    if(bottomBtnIDElement)
    //                        obj_findBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
    //
    //                    if(bottomBtnNameElement)
    //                        obj_findBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
    //                    else
    //                        obj_findBottomDO.bottomBtnName = @" ";
    //
    //                    if(bottomBtnImgElement)
    //                        obj_findBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
    //                    else
    //                        obj_findBottomDO.bottomBtnImg = @" ";
    //
    //                    if(bottomBtnImgOffElement)
    //                        obj_findBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
    //                    else
    //                        obj_findBottomDO.bottomBtnImgOff = @" ";
    //
    //                    if(btnLinkTypeIDElement)
    //                        obj_findBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
    //
    //                    if(btnLinkTypeNameElement)
    //                        obj_findBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
    //
    //                    if(btnLinkIDElement){
    //                        obj_findBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
    //                        if(btnLinkTypeNameElement){
    //                            if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
    //                                [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
    //                        }
    //                    }
    //
    //                    if(positionElement)
    //                        obj_findBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
    //
    //
    //
    //                    BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
    //                    [arrFindSingleBottomButtonDO addObject:obj_findBottomDO];
    //                    //[obj_findBottomDO release];
    //                }
    //                if([arrFindSingleBottomButtonDO count] > 0)
    //                {
    //                    // Set Bottom Menu Button after read from response data
    //                    [self setBottomBarMenu];
    //                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
    //
    //                }
    //                else
    //                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
    //            }
    //        }
    //        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    //        NSString *response=[TBXML textForElement:saveResponseText];
    //        if([response isEqualToString:@"No Records Found."])
    //        {
    //            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:response];
    //        }
    //        else{
    //            NSString *responseTextStr = [[TBXML textForElement:saveResponseText] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    //            [self showFormatedAlert:responseTextStr];
    //        }
    //
    //        [self settableViewOnScreen];
    //
    //    }
    //
    //
    //    else
    //    {
    //        lastRecord = 0;
    //        nextPage = 0;
    //        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    //        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
    //        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
    //        [loading stopAnimating];
    //        ReleaseAndNilify(loading);
    //        return;
    //
    //    }
    //     [defaults setBool:NO forKey:@"ViewMore"];
    //    [table_Dining reloadData];
}
-(void)sendClicksNImpressions
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    NSMutableString *retailerIDString = [[NSMutableString alloc]init];
    NSMutableString *retailLocationIDString = [[NSMutableString alloc]init];
    
    if([defaults valueForKey:KEY_MAINMENUID])
    {
        //[parameters setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
        [parameters setValue:@"138" forKey:@"mainMenuId"];
    }
    if (![categoryName isEqualToString:@""])
    {
        [parameters setValue:categoryName forKey:@"catName"];
    }
    if(retailerIDArray.count > 0)
    {
        for(int i=0;i<retailerIDArray.count;i++)
        {
            [retailerIDString appendString:[NSString stringWithFormat:@"%@,",retailerIDArray[i]]];
        }
        if ([retailerIDString hasSuffix:@","]) {
            [retailerIDString  setString:[retailerIDString substringToIndex:[retailerIDString length]-1]];
        }
        [parameters setValue:retailerIDString forKey:@"retailerId"];
        
    }
    if(retailLocationIDArray.count > 0)
    {
        for(int i=0;i<retailLocationIDArray.count;i++)
        {
            [retailLocationIDString appendString:[NSString stringWithFormat:@"%@,",retailLocationIDArray[i]]];
        }
        if ([retailLocationIDString hasSuffix:@","]) {
            [retailLocationIDString  setString:[retailLocationIDString substringToIndex:[retailLocationIDString length]-1]];
        }
        [parameters setValue:retailLocationIDString forKey:@"retailLocationId"];
    }
    DLog(@"parameter: %@",parameters);
    
    //NSString *urlString = [NSString stringWithFormat:@"http://sdw2107:8080/HubCiti2.4.3/hotdeals/retclickimpression"];
    NSString *urlString = [NSString stringWithFormat:@"%@hotdeals/retclickimpression",BASE_URL];
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    DLog(@"Url: %@",urlString);
    [client sendRequestAsync: parameters : urlString];
    
}




-(void)parse_RetSummary:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCodeElement!=nil && ![[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]){
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        return;
    }
    
    else{
        
        //        RetailerDO *retObj = [retObjArray objectAtIndex:selectedIndexPath.section];
        
        [defaults setValue:response forKey:KEY_RESPONSEXML];
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:nil];
        rsvc.distanceFromPreviousScreen = [[retDetailsObjArray objectAtIndex:selectedIndexPath.row]distance];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
        
    }
}

#pragma mark search bar delegate methods
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    if (searchBar.tag == 1){
        //        viewFrame = view_gSearch.frame;
        [UIView beginAnimations:@"slideIn" context:NULL];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        [UIView setAnimationDuration:0.3];
        //        view_gSearch.center = CGPointMake(160, 20);
        [UIView commitAnimations];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar resignFirstResponder];
    
    switch (searchBar.tag) {
        case 0:
        {
            lastRecord = 0;
            [globalRetailerService removeAllObjects];
            [retDetailsObjArray removeAllObjects];
            //            [retObjArray removeAllObjects];
            [table_Dining scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
            //            [self request_singlecatretailers:0 subCatId:[defaults valueForKey:@"SubCategoryId"] sortBy:[defaults valueForKey:@"SortFindSingleCatBy"] searchKey:search_Dining.text citiesIds:[defaults valueForKeyPath:@"commaSeperatedCities"] fValueIds:[defaults valueForKeyPath:@"commaSeperatedFilterValueIds"]  filterIds:[defaults valueForKeyPath:@"commaSeperatedFilterIds"]];
            [self request_singlecatretailers:0 withObj:sortFilObj andsearchKey:search_Dining.text];
        }
            break;
            
        case 1:
        {
            if (![searchBar.text length]) {
                return;
            }
            [defaults setValue:searchBar.text forKey:@"Google Search"];
            NSString *str = [NSString stringWithFormat:@"http://www.google.com/search?q=%@",searchBar.text/*[defaults valueForKey:@"Google Search"]*/];
            
            NSString *query = [str stringByReplacingOccurrencesOfString:@" " withString:@"+"];
            // NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.google.com/search?q=%@",query]];
            // str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [UIView beginAnimations:@"slideOut" context:NULL];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:0.3];
            //            view_gSearch.frame = viewFrame;
            [UIView commitAnimations];
            
            [defaults setValue:query forKey:KEY_URL];
            WebBrowserViewController *googleSearch = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:googleSearch animated:NO];
        }
            // [googleSearch release];
            break;
            
        default:
            break;
    }
    
    
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    
    switch (searchBar.tag) {
        case 0:
            [searchBar resignFirstResponder];
            break;
        case 1:
            [searchBar resignFirstResponder];
            [UIView beginAnimations:@"slideOut" context:NULL];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:0.3];
            
            //            view_gSearch.frame = viewFrame;
            [UIView commitAnimations];
            
            break;
            
        default:
            break;
    }
}

- (void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)searchText{
    
    if (searchBar.tag == 0 && ![searchText length]){
        [searchBar performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0.1];
        lastRecord = 0;
//        [retDetailsObjArray removeAllObjects];
//        //        [retObjArray removeAllObjects];
        [table_Dining scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        nextPage = 0;
        if (retDetailsObjArray) {
            [retDetailsObjArray removeAllObjects];
        }
        if(globalRetailerService)
            [globalRetailerService removeAllObjects];
        
        
        [self request_singlecatretailers:0 withObj:sortFilObj andsearchKey:search_Dining.text];

        //[table_Dining reloadData];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}


@end

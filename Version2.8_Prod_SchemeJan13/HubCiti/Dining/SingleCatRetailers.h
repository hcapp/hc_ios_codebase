//
//  SingleCatRetailers.h
//  HubCiti
//
//  Created by Ajit Nadig on 11/21/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import "SwipesViewController.h"
#import "SortAndFilter.h"
#import <MessageUI/MessageUI.h>
NSDictionary *viewDictionary;
@class RetailersListViewController;
@class DealHotDealsList;
@class FilterRetailersList;
@class CityExperienceViewController;
@class FundraiserListViewController;
@class FilterListViewController;
@class EventsListViewController;
@class FAQCategoryList;
@class AnyViewController;
@class EmailShareViewController;
BOOL fromFindSingleCategory;

@interface RetailerDetailsDO : NSObject{
    
    NSString *retId, *retName, *retLocnId, *retCompleteAddress,*retLat, *retLong;
    NSString *logoImgPath, *bannerAdImgPath, *ribbonAdImgPath, *ribbonAdUrl;
    NSString *retListId, *distance, *saleFlag;

}
@property (nonatomic, strong)NSString *retId, *retName, *retLocnId, *retCompleteAddress,*retLat, *retLong;
@property (nonatomic, strong)NSString *logoImgPath, *bannerAdImgPath, *ribbonAdImgPath, *ribbonAdUrl;
@property (nonatomic, strong)NSString *retListId, *distance, *saleFlag;

@end

@interface RetailerDO : NSObject
{
    NSString    *catId, *catName;
    NSMutableArray *retDetailsObjArray;
}
@property (nonatomic,strong) NSMutableArray *retDetailsObjArray;
@property (nonatomic, strong) NSString  *catId, *catName;

@end


@interface SingleCatRetailers : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,MFMessageComposeViewControllerDelegate,SwipesViewControllerDelegate,MFMailComposeViewControllerDelegate,CustomizedNavControllerDelegate>{
    
    
    SortAndFilter *sortFilObj;
    
    SwipesViewController *iSwipeViewController;
    UISegmentedControl *segmentFindSingleController;
    
    UITableView *table_Dining;
    UISearchBar *search_Dining;
    IBOutlet UISearchBar *gsearch_Dining;
    
    NSMutableArray *retObjArray,*retDetailsObjArray, *retailerIDArray, *retailLocationIDArray;
    
    int nextPage, lastRecord;
    WebRequestState iWebRequestState;
    NSMutableArray *arrFindSingleBottomButtonDO;
    NSMutableArray *arrBottomButtonViewContainer;
    NSString *androidDownloadLink;
    //google search related
//    IBOutlet UIView *view_gSearch;
    CGRect viewFrame;
    
    NSIndexPath *selectedIndexPath;
    NSString *categoryName,*catId;
    CommonUtility *common;
    
    int bottomBtn;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) FAQCategoryList *faqList;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,strong) EventsListViewController* iEventsListViewController;
@property(nonatomic,strong) FilterListViewController *filterList;
@property(nonatomic,strong) FundraiserListViewController *iFundraiserListViewController;
@property(nonatomic,strong) CityExperienceViewController *cityExp;
@property(nonatomic,strong) FilterRetailersList *filters;
@property(nonatomic,strong) RetailersListViewController* retailerListScreen;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *catId;
@property(nonatomic,strong) SortAndFilter *sortFilObj;
@property(nonatomic,strong) DealHotDealsList* hotDeals;
@property (nonatomic, assign) NSInteger oldSegmentedIndex;
@end

//
//  SingleCatGroupAndSort.h
//  HubCiti
//
//  Created by ionnor on 11/29/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleCatGroupAndSort : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UITableView *tblGroupingNSorting;
    NSMutableArray *arrGroups;
    NSMutableArray *arrSorting;
    int groupSelectionVal;
    int sortSelectionval;
    int cityFlag;
    UIImageView *checkButton;
    BOOL isExpanded;
    UIView *view_picker;
    UIPickerView *dinningTypePickerView;
    UIToolbar *pickerToolbar;
    NSMutableArray *arrDinningTypes;
    
}

@property(nonatomic,strong)NSDictionary *citiesDic;

@property(nonatomic,strong)NSMutableDictionary *OptionsDic;

@property(nonatomic,strong)NSMutableArray *arrCities;

@property(nonatomic,strong)NSMutableArray *arrCitiId;

@property(nonatomic,strong)NSMutableArray *selectedCitiIds,*selectedFilterValueIds,*selectedFilterId;

@property (nonatomic, strong) NSString *categoryName,*catId;

@property (nonatomic, strong) NSString *srchKey;

@property (nonatomic, strong)NSMutableArray *filterCount,*filterId;

@end

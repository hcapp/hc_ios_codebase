//
//  SingleCatGroupAndSort.m
//  HubCiti
//
//  Created by ionnor on 11/29/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "SingleCatGroupAndSort.h"
#import  "QuartzCore/QuartzCore.h"
#import "Multivalue.h"
#import "SingleCatRetailers.h"

@interface SingleCatGroupAndSort ()

@end

@implementation SingleCatGroupAndSort

@synthesize arrCitiId,arrCities,citiesDic,selectedCitiIds,selectedFilterValueIds,categoryName,srchKey,filterCount,OptionsDic,selectedFilterId,catId,filterId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    // Do any additional setup after loading the view from its nib.
    UIButton *btn = [UtilityManager customizeBarButtonItem:@"Done"];
    [btn addTarget:self action:@selector(doneButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = mainPage;
  //  //[mainPage release];
    
    UIButton *btncancel = [UtilityManager customizeBarButtonItem:NSLocalizedString(@"Cancel",@"Cancel")];
    [btncancel addTarget:self action:@selector(cancelButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* btncancelbar = [[UIBarButtonItem alloc] initWithCustomView:btncancel];
    self.navigationItem.rightBarButtonItem = btncancelbar;
   // [btncancelbar release];
    
    self.navigationItem.title = @"Group & Sort";
    
    filterCount = [[NSMutableArray alloc]init];
    filterId = [[NSMutableArray alloc]init];
    arrDinningTypes = [[NSMutableArray alloc]init];
    groupSelectionVal = -1;
     OptionsDic = [[NSMutableDictionary alloc]init];

    // [self setViewForGroupingNSorting];
    //call function to check if there are any cities selected in preferences
    
    if ([RegionApp isEqualToString:@"1"]) {
         [self fetchCityPreference];
    }
 
    [self Request_filter];
    [self Request_fetcheventdetail];
    
}

-(void)fetchCityPreference{
    /*
     <userId>3</userId>
     <hubCitiId>93</hubCitiId>
     <module>Find Single</module>// Find All,  Find Single,  Citi Exp, Events
     <mItemId>15214</mItemId>
     // <bottomBtnId>12345</bottomBtnId>// Input can be NULL.
     <radius>100</radius>
     <latitude>30.422401</latitude>
     <longitude>-97.796676</longitude>
     <catName>Dining</catName>
     //<srchKey></srchKey> // Input can be NULL.
     //<catIds>0</catIds>
     */
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId><userId>%@</userId><module>Find Single</module>", [defaults  valueForKey:KEY_HUBCITIID],[defaults  valueForKey:KEY_USERID]];
    
    if (![categoryName isEqualToString:@""]) {
        [xmlStr appendFormat:@"<catName>%@</catName>",categoryName];
    }
    
    if (![srchKey isEqualToString:@""]) {
        if (srchKey) {
            [xmlStr appendFormat:@"<srchKey>%@</srchKey>",srchKey];
        }
    }
    
    if([defaults valueForKey:KEY_MITEMID]){
        [xmlStr appendFormat:@" <mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    
    else{
        [xmlStr appendFormat:@" <bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [xmlStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        [xmlStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [xmlStr appendFormat:@"<subCatId>%@</subCatId>",[defaults valueForKey:@"SubCategoryId"]];
    
    [xmlStr appendFormat:@"</UserDetails>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercitypref",BASE_URL];
    
    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    [self parseCityPreference:responseXML];
    ReleaseAndNilify(xmlStr);
    
}

-(void)parseCityPreference:(NSString *)responseXml{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
    {
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        arrCities =[[NSMutableArray alloc]init];
        arrCitiId =[[NSMutableArray alloc]init];
        selectedCitiIds =[[NSMutableArray alloc]init];
        TBXMLElement *cityListElement = [TBXML childElementNamed:@"cityList" parentElement:tbXml.rootXMLElement];
        if(cityListElement)
        {
            cityFlag = 1;
            TBXMLElement *CityElement = [TBXML childElementNamed:@"City" parentElement:cityListElement];
            while (CityElement)
            {
                
                TBXMLElement *cityIdElement = [TBXML childElementNamed:@"cityId" parentElement:CityElement];
                TBXMLElement *cityNameElement = [TBXML childElementNamed:@"cityName" parentElement:CityElement];
                TBXMLElement *isCityElement = [TBXML childElementNamed:@"isCityChecked" parentElement:CityElement];
               
                
                if (cityIdElement) {
                    
                        [arrCitiId addObject:[TBXML textForElement:cityIdElement]];
                    
                }
                
                if (cityNameElement) {
                    
                        [arrCities addObject:[TBXML textForElement:cityNameElement]];
                    
                }
                
                if (isCityElement) {
                    if ([[TBXML textForElement:isCityElement] isEqualToString:@"1"]) {
                        [selectedCitiIds addObject:[TBXML textForElement:cityIdElement]];
                    }
                }
                CityElement = [TBXML nextSiblingNamed:@"City" searchFromElement:CityElement];
            }
        }
        
        
        if ([arrCities count]!=0) {
            citiesDic = [[NSDictionary alloc] initWithObjects:arrCitiId forKeys:arrCities];
        }
        
        
        if ([defaults valueForKey:@"SelectedCityIds"]==nil) {
            
            [defaults setValue:arrCitiId forKey:@"SelectedCityIds"];
        }
        
//        selectedCitiIds = [[NSMutableArray alloc]initWithArray:[defaults valueForKey:@"SelectedCityIds"]];
        else if([defaults valueForKey:@"SelectedCityIds"]!=nil && [defaults valueForKey:@"commaSeperatedCities"]!=nil)
        {
            [selectedCitiIds removeAllObjects];
            [selectedCitiIds addObjectsFromArray:[NSMutableArray arrayWithArray:[[defaults valueForKey:@"commaSeperatedCities"] componentsSeparatedByString:@","]]];
        }
    }
    

//    if ([defaults valueForKey:@"SelectedCityIds"]==nil) {
//        
//        [defaults setValue:arrCitiId forKey:@"SelectedCityIds"];
//    }
//    
//    selectedCitiIds = [[NSMutableArray alloc]initWithArray:[defaults valueForKey:@"SelectedCityIds"]];
    
}



-(void)cancelButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)doneButtonTouched:(id)sender
{
    [defaults setValue:@"YES" forKey:@"isComingFromGroupingandSorting"];
    
    if(groupSelectionVal == 0){
        [defaults setValue:@"0" forKey:@"SubCategoryId"];
        [defaults setValue:@"atoz" forKey:@"GroupFindSingleCatBy"];
    }
    
    else if(groupSelectionVal == 1){
         [defaults setValue:@"type" forKey:@"GroupFindSingleCatBy"];
    }
        
    
    else if(groupSelectionVal == -1){
        [defaults setValue:nil forKey:@"GroupFindSingleCatBy"];
        [defaults setValue:@"0" forKey:@"SubCategoryId"];
    }

    // Check the Selected Sorting Value
    if(sortSelectionval == 1){
        [defaults setValue:@"distance" forKey:@"SortFindSingleCatBy"];
        [defaults setValue:nil forKey:@"SelectedCityIds"];
    }
    
    else if(sortSelectionval == 0){
        [defaults setValue:@"name" forKey:@"SortFindSingleCatBy"];
        [defaults setValue:nil forKey:@"SelectedCityIds"];
    }
    
    else if(sortSelectionval == 2){
        [defaults setValue:@"City" forKey:@"SortFindSingleCatBy"];
        if ([selectedCitiIds count]==0) {
            [defaults setValue:nil forKey:@"SelectedCityIds"];
            [defaults setValue:nil forKey:@"commaSeperatedCities"];
        }
        else{
        
        [defaults setValue:selectedCitiIds forKey:@"SelectedCityIds"];
        NSMutableString *requestStr = [[NSMutableString alloc] init];
        for (int i=0; i<[selectedCitiIds count]; i++) {
            
            [requestStr appendFormat:@"%@,", selectedCitiIds[i]];
        }
        
        if ([requestStr hasSuffix:@","])
            [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
        [defaults setValue:requestStr forKey:@"commaSeperatedCities"];
        
    }
    }
    
    if ([selectedFilterValueIds count]==0) {
        [defaults setValue:nil forKey:@"SelectedFilterValueIds"];
        [defaults setValue:nil forKey:@"commaSeperatedFilterValueIds"];
        [defaults setValue:@"1" forKey:@"afterFilterValueSelection"];
    }
    
//    else if ([selectedFilterId count]== [filterId count]){
//        [defaults setValue:nil forKey:@"SelectedFilterValueIds"];
//        [defaults setValue:nil forKey:@"commaSeperatedFilterValueIds"];
//        [defaults setValue:@"1" forKey:@"afterFilterValueSelection"];
//    }
    
    
    else{
        [defaults setValue:nil forKey:@"afterFilterValueSelection"];
        [defaults setValue:selectedFilterValueIds forKey:@"SelectedFilterValueIds"];
        NSMutableString *requestStr = [[NSMutableString alloc] init];
        for (int i=0; i<[selectedFilterValueIds count]; i++) {
            
            [requestStr appendFormat:@"%@,", selectedFilterValueIds[i]];
        }
        
        if ([requestStr hasSuffix:@","])
            [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
        [defaults setValue:requestStr forKey:@"commaSeperatedFilterValueIds"];
        
    }
    
    
    if ([selectedFilterId count]==0) {
        [defaults setValue:nil forKey:@"SelectedFilterId"];
        [defaults setValue:nil forKey:@"commaSeperatedFilterIds"];
        [defaults setValue:@"1" forKey:@"afterFilterIdSelection"];
    }
    
//    else if ([selectedFilterId count]== [filterId count]){
//        [defaults setValue:nil forKey:@"SelectedFilterValueIds"];
//        [defaults setValue:nil forKey:@"commaSeperatedFilterValueIds"];
//        [defaults setValue:@"1" forKey:@"afterFilterValueSelection"];
//    }
    
    else{
        [defaults setValue:nil forKey:@"afterFilterIdSelection"];
        [defaults setValue:selectedFilterId forKey:@"SelectedFilterId"];
        NSMutableString *requestStr = [[NSMutableString alloc] init];
        for (int i=0; i<[selectedFilterId count]; i++) {
            
            [requestStr appendFormat:@"%@,", selectedFilterId[i]];
        }
        
        if ([requestStr hasSuffix:@","])
            [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
        [defaults setValue:requestStr forKey:@"commaSeperatedFilterIds"];
        
    }

    
    [UtilityManager popBackToViewController:[SingleCatRetailers class] inNavigationController:self.navigationController];

//    [self.navigationController popViewControllerAnimated:NO];
}

-(void)showpickerView
{
    tblGroupingNSorting.userInteractionEnabled = NO;
    
    NSMutableDictionary *dic = [arrDinningTypes objectAtIndex:0];
    [defaults setValue:[dic valueForKey:@"catId"] forKey:@"SubCategoryId"];
    if(dinningTypePickerView)
     {
     [dinningTypePickerView removeFromSuperview];
    // [dinningTypePickerView release];
     }
     float pickerHeight = 210;
     if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
     if (IOS7){
     dinningTypePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - pickerHeight, 320, pickerHeight)];//Y 221+70+44
     }
     else{
     dinningTypePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - pickerHeight, 320, pickerHeight)];//Y 221+44
     }
     }
     else
     {
     dinningTypePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 3*(SCREEN_HEIGHT/4), SCREEN_WIDTH, SCREEN_HEIGHT/4)];
     }
     
     dinningTypePickerView.delegate = self;
     dinningTypePickerView.dataSource = self;
     dinningTypePickerView.showsSelectionIndicator = YES;
     dinningTypePickerView.backgroundColor=[UIColor whiteColor];
     [self.view addSubview:dinningTypePickerView];
     
     if(pickerToolbar)
     {
     [pickerToolbar removeFromSuperview];
    // [pickerToolbar release];
     }
     
     if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
     if (IOS7){
     pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, dinningTypePickerView.frame.origin.y - 44, 320, 44)];//221+70
     }
     else{
     pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, dinningTypePickerView.frame.origin.y - 44, 320, 44)];
     }
     }
     else
     {
     pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,  3*(SCREEN_HEIGHT/4)-44, SCREEN_WIDTH, 44)];
     }
     
     pickerToolbar.barStyle = UIBarStyleBlackOpaque;
     [pickerToolbar sizeToFit];
     
     NSMutableArray *barItems = [[NSMutableArray alloc] init];
     
     UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
     [barItems addObject:flexSpace];
     //[flexSpace release];
     
     UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDone:)];
     [barItems addObject:doneBtn];
     //[doneBtn release];
     
     UILabel *lblTitle ;
     if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
     lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 320, 20)];
     lblTitle.font = [UIFont boldSystemFontOfSize:15];
     
     }
     else
     {
     lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 10,SCREEN_WIDTH, 40)];
     lblTitle.font = [UIFont boldSystemFontOfSize:20];
     
     }
     lblTitle.backgroundColor = [UIColor clearColor];
     lblTitle.text = @"Select Type";
     lblTitle.textColor = [UIColor whiteColor];
     lblTitle.textAlignment= NSTextAlignmentCenter;
     [pickerToolbar addSubview:lblTitle];
    // [lblTitle release];
     
     [pickerToolbar setItems:barItems animated:YES];
     //[barItems release];
     [self.view addSubview:pickerToolbar];
     
     [dinningTypePickerView reloadAllComponents];
     [self.view bringSubviewToFront:dinningTypePickerView];
    
   /* if(dinningTypePickerView)
    {
        [dinningTypePickerView removeFromSuperview];
        [dinningTypePickerView release];
    }
    
   float pickerHeight = 210;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        if (IOS7){
            dinningTypePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - pickerHeight, 320, pickerHeight)];//Y 221+70+44
        }
        else{
            dinningTypePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - pickerHeight, 320, pickerHeight)];//Y 221+44
        }
    }
    else
    {
        dinningTypePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 3*(SCREEN_HEIGHT/4), SCREEN_WIDTH, SCREEN_HEIGHT/4)];
    }
    dinningTypePickerView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    dinningTypePickerView.delegate = self;
    dinningTypePickerView.dataSource = self;
    dinningTypePickerView.showsSelectionIndicator = YES;
    [self.view addSubview:dinningTypePickerView];
    
    if(pickerToolbar)
    {
        [pickerToolbar removeFromSuperview];
        [pickerToolbar release];
    }
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        if (IOS7){
            pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, dinningTypePickerView.frame.origin.y - 44, 320, 44)];//221+70
        }
        else{
            pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, dinningTypePickerView.frame.origin.y - 44, 320, 44)];
        }
    }
    else
    {
        pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,  3*(SCREEN_HEIGHT/4)-44, SCREEN_WIDTH, 44)];
    }

    pickerToolbar.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    [flexSpace release];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDone:)];
    [barItems addObject:doneBtn];
    [doneBtn release];
    
    UILabel *lblTitle ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 320, 20)];
        lblTitle.font = [UIFont boldSystemFontOfSize:15];
        
    }
    else
    {
        lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 10,SCREEN_WIDTH, 40)];
        lblTitle.font = [UIFont boldSystemFontOfSize:20];
        
    }
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.text = @"Select Type";
    lblTitle.textColor = [UIColor whiteColor];
    //lblTitle.font = [UIFont boldSystemFontOfSize:15];
    lblTitle.textAlignment= NSTextAlignmentCenter;
    [pickerToolbar addSubview:lblTitle];
    [lblTitle release];
    
    [pickerToolbar setItems:barItems animated:YES];
    [barItems release];
    [self.view addSubview:pickerToolbar];*/
    //[self.view addSubview:view_picker];
    
//    [dinningTypePickerView reloadAllComponents];
    //[self.view bringSubviewToFront:dinningTypePickerView];
}

-(void)pickerDone:(id)sender
{
    tblGroupingNSorting.userInteractionEnabled = YES;
    
    
    [pickerToolbar removeFromSuperview];
    [dinningTypePickerView removeFromSuperview];
    [view_picker removeFromSuperview];
    
   // [pickerToolbar release];
    pickerToolbar = nil;
   // [dinningTypePickerView release];
    dinningTypePickerView = nil;
    
    ReleaseAndNilify(view_picker);
    
    // [arrDinningTypes removeAllObjects];
}


-(void)setViewForGroupingNSorting
{
    
    if(cityFlag ==1 && [RegionApp isEqualToString:@"1"])
    {
        arrSorting = [[NSMutableArray alloc]initWithObjects:@"Name",@"Distance",@"City",nil];
    }
    else{
        arrSorting = [[NSMutableArray alloc]initWithObjects:@"Name",@"Distance",nil];
    }

    
     
    if ([RegionApp isEqualToString:@"1"]){
     NSString *strG = [defaults valueForKey:@"GroupFindSingleCatBy"];
     
     if([strG isEqualToString:@"atoz"])
     groupSelectionVal = 0;
     else if([strG isEqualToString:@"type"])
     groupSelectionVal = 1;
    }
    
     else{
         if([defaults valueForKey:@"GroupFindSingleCatBy"]==nil){
              groupSelectionVal = -1;
         }
         else if([[defaults valueForKey:@"SubCategoryId"]isEqualToString:@"0"]){
             groupSelectionVal = 0;
         }
         else
             groupSelectionVal = 1;
     }
    
    if([[defaults valueForKey:@"SortFindSingleCatBy"]isEqualToString:@"name"])
        sortSelectionval = 0;
    else if([[defaults valueForKey:@"SortFindSingleCatBy"]isEqualToString:@"distance"])
        sortSelectionval = 1;
    
    else if([[defaults valueForKey:@"SortFindSingleCatBy"]isEqualToString:@"City"])
        sortSelectionval = 2;
    
    
    if(tblGroupingNSorting)
    {
        [tblGroupingNSorting removeFromSuperview];
       // [tblGroupingNSorting release];
    }
    tblGroupingNSorting = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,SCREEN_HEIGHT-80-EXCLUDE_BAR_HEIGHT) style:UITableViewStylePlain];
    tblGroupingNSorting.dataSource=self;
    tblGroupingNSorting.delegate=self;
    [tblGroupingNSorting setBackgroundColor:[UIColor clearColor]];
    //tblGroupingNSorting.tableFooterView.hidden=YES;
    
    // Set Border for tableView
    /* CALayer *layer = tblGroupingNSorting.layer;
     [layer setMasksToBounds:YES];
     [layer setCornerRadius: 4.0];
     [layer setBorderWidth:1.0];
     [layer setBorderColor:[[UIColor colorWithWhite: 0.8 alpha: 1.0] CGColor]];*/
    
    [self.view addSubview:tblGroupingNSorting];
    
    [tblGroupingNSorting reloadData];
    
}

-(void)Request_fetcheventdetail
{
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId><catId>%@</catId>",[defaults valueForKey:KEY_USERID] ,[defaults valueForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_LINKID]] ;//],[defaults valueForKey:KEY_LINKID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length])
    {
                            
    [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
                             [defaults  valueForKey:KEY_LATITUDE]];
                            
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
                            
    [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    if([defaults valueForKey:KEY_MITEMID]){
        [reqStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    
    else{
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    [reqStr appendFormat:@"</UserDetails>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/getsubcategory",BASE_URL];
    
    
    [ConnectionManager establishConnectionFor:reqStr base:urlString withDelegate:self];
    ReleaseAndNilify(reqStr);
    
}



-(void)Request_filter
{
    
    
//    <MenuItem>
//    *<userId>66</userId>
//    *<hubCitiId>93</hubCitiId>
//    <latitude>30.57228</latitude>
//    <longitude>-98.306992</longitude>
//    <searchKey>pizza</searchKey>
//    *<catId>11</catId>
//    <radius>100</radius>
//    <cityIds>1,2,3</cityIds>
//    </MenuItem>
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<MenuItem><userId>%@</userId><hubCitiId>%@</hubCitiId><catId>%@</catId>",[defaults valueForKey:KEY_USERID] ,[defaults valueForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_LINKID]] ;//],[defaults valueForKey:KEY_LINKID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length])
    {
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    if([defaults valueForKey:KEY_MITEMID]){
        [reqStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    
    else{
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    if (![srchKey isEqualToString:@""]) {
        if (srchKey) {
            [reqStr appendFormat:@"<searchKey>%@</searchKey>",srchKey];
        }
    }
    if([defaults valueForKey:@"SelectedCityIds"]!=nil && [defaults valueForKey:@"commaSeperatedCities"]!=nil)
    {
        [reqStr appendFormat:@"<cityIds>%@</cityIds>",[defaults valueForKey:@"commaSeperatedCities"]];
    }
    
    [reqStr appendFormat:@"</MenuItem>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/getfilterlist",BASE_URL];
    
    NSString *response = [ConnectionManager establishPostConnection:urlString withParam:reqStr];

    [self parsefilters:response];
    
    ReleaseAndNilify(reqStr);

    
    
}


-(void)parsefilters:(NSString*)response{
    
    selectedFilterValueIds = [[NSMutableArray alloc]init];
    selectedFilterId = [[NSMutableArray alloc]init];
    
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
    TBXMLElement *categoryListElement = [TBXML childElementNamed:@"categoryList" parentElement:tbxml.rootXMLElement];
    
    if(categoryListElement)
    {
        NSMutableDictionary *temp;
        TBXMLElement *CategoryInfoElement = [TBXML childElementNamed:@"CategoryInfo" parentElement:categoryListElement];
        while (CategoryInfoElement)
        {
            Multivalue *multipleValue = [[Multivalue alloc]init];
            multipleValue.valueNames = [[NSMutableArray alloc]init];
            multipleValue.valueIds = [[NSMutableArray alloc]init];
            multipleValue.filterId = [[NSMutableArray alloc]init];
            TBXMLElement *categoryIdElement = [TBXML childElementNamed:@"categoryId" parentElement:CategoryInfoElement];
            TBXMLElement *groupContentElement = [TBXML childElementNamed:@"groupContent" parentElement:CategoryInfoElement];
            
    

            
            if(groupContentElement)
                
                multipleValue.header = [TBXML textForElement:groupContentElement];

            
            TBXMLElement *eventListElement = [TBXML childElementNamed:@"filterList" parentElement:CategoryInfoElement];
            
            if(eventListElement)
            {
                TBXMLElement *EventDetailsElement = [TBXML childElementNamed:@"Filter" parentElement:eventListElement];
                
                while (EventDetailsElement)
                {
                    
                    if ([[TBXML textForElement:categoryIdElement] isEqualToString:@"0"]) {
                        TBXMLElement *filterIdElement = [TBXML childElementNamed:@"filterId" parentElement:EventDetailsElement];
                        TBXMLElement *filterNameElement = [TBXML childElementNamed:@"filterName" parentElement:EventDetailsElement];
                        
                        if(filterIdElement){
                            [multipleValue.filterId addObject:[TBXML textForElement:filterIdElement]];
                            [filterId addObject:[TBXML textForElement:filterIdElement]];
                        }
                        
                        if(filterNameElement){
                            [multipleValue.valueNames addObject:[TBXML textForElement:filterNameElement]];
                        }
                        
//                        [selectedFilterId addObject:[TBXML textForElement:filterIdElement]];
                         temp = [NSMutableDictionary dictionaryWithObjects:multipleValue.filterId forKeys:multipleValue.valueNames];
                    }
                    
                    else{
                    TBXMLElement *filterValueIdElement = [TBXML childElementNamed:@"fValueId" parentElement:EventDetailsElement];
                    TBXMLElement *filterValueNameElement = [TBXML childElementNamed:@"fValueName" parentElement:EventDetailsElement];
                        
                        if(filterValueIdElement)
                            [multipleValue.valueIds addObject:[TBXML textForElement:filterValueIdElement]];
                        
                        if(filterValueNameElement){
                            [multipleValue.valueNames addObject:[TBXML textForElement:filterValueNameElement]];
                        }
                        
                        if(categoryIdElement){
                            [multipleValue.filterId addObject:[TBXML textForElement:categoryIdElement]];
                            [filterId addObject:[TBXML textForElement:categoryIdElement]];
                        }
                        
                        if (![selectedFilterId containsObject:[TBXML textForElement:categoryIdElement]]) {
//                            [selectedFilterId addObject:[TBXML textForElement:categoryIdElement]];
                        }
//                        [selectedFilterValueIds addObject:[TBXML textForElement:filterValueIdElement]];
                         temp = [NSMutableDictionary dictionaryWithObjects:multipleValue.valueIds forKeys:multipleValue.valueNames];
                    }
                   

                    
                    [OptionsDic addEntriesFromDictionary:temp];
                    EventDetailsElement = [TBXML nextSiblingNamed:@"Filter" searchFromElement:EventDetailsElement];

                }
                
            }
            [filterCount addObject:multipleValue];
            CategoryInfoElement = [TBXML nextSiblingNamed:@"CategoryInfo" searchFromElement:CategoryInfoElement];
        }
        
    }

  
       if([defaults valueForKey:@"SelectedFilterId"]!=nil && [defaults valueForKey:@"commaSeperatedFilterIds"]!=nil)
        {
            [selectedFilterId removeAllObjects];
            [selectedFilterId addObjectsFromArray:[NSMutableArray arrayWithArray:[[defaults valueForKey:@"commaSeperatedFilterIds"] componentsSeparatedByString:@","]]];
        }
        
       else{
           if ([[defaults valueForKey:@"afterFilterIdSelection"] isEqualToString:@"1"]) {
                [selectedFilterId removeAllObjects];
           }
           else{
           [defaults setValue:selectedFilterId forKey:@"SelectedFilterId"];
           }
       }
        
        DLog(@"%@",[defaults valueForKey:@"SelectedFilterValueIds"]);
        
         if([defaults valueForKey:@"SelectedFilterValueIds"]!=nil && [[defaults valueForKey:@"commaSeperatedFilterValueIds"] componentsSeparatedByString:@","]!=nil)
        {
            [selectedFilterValueIds removeAllObjects];
            [selectedFilterValueIds addObjectsFromArray:[NSMutableArray arrayWithArray:[[defaults valueForKey:@"commaSeperatedFilterValueIds"] componentsSeparatedByString:@","]]];
        }
        
        else{
            if ([[defaults valueForKey:@"afterFilterValueSelection"] isEqualToString:@"1"]) {
                 [selectedFilterValueIds removeAllObjects];
            }
            else{
            [defaults setValue:selectedFilterValueIds forKey:@"SelectedFilterValueIds"];
            }
        }
            
        
        
    }
}

//for get method
-(void)responseData:(NSString*)response
{
    [self parseDinningCat:response];
    if([arrDinningTypes count] > 0)
    {
        arrGroups = [[NSMutableArray alloc]initWithObjects:@"Alphabetically",@"Type",nil];
    }
    else{
        arrGroups = [[NSMutableArray alloc]initWithObjects:@"Alphabetically",nil];
    }
    [self setViewForGroupingNSorting];
}

-(void)parseDinningCat:(NSString*)response
{
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        TBXMLElement *listCatDetailsElement = [TBXML childElementNamed:@"listCatDetails" parentElement:tbxml.rootXMLElement];
        if(listCatDetailsElement)
        {
            TBXMLElement *CategoryDetailsElement = [TBXML childElementNamed:@"CategoryDetails" parentElement:listCatDetailsElement];
            while (CategoryDetailsElement)
            {
                NSMutableDictionary *dicCat = [[NSMutableDictionary alloc]init];
                TBXMLElement *catIdElement = [TBXML childElementNamed:@"catId" parentElement:CategoryDetailsElement];
                TBXMLElement *catNameElement = [TBXML childElementNamed:@"catName" parentElement:CategoryDetailsElement];
                if(catIdElement)
                    [dicCat setObject:[TBXML textForElement:catIdElement] forKey:@"catId"];
                if(catNameElement)
                    [dicCat setObject:[TBXML textForElement:catNameElement] forKey:@"catName"];
                
                [arrDinningTypes addObject:dicCat];
               // [dicCat release];
                
                CategoryDetailsElement = [TBXML nextSiblingNamed:@"CategoryDetails" searchFromElement:CategoryDetailsElement];
            }
            
        }
    }
    else
    {
        //        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        //
        //		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        //		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
}

#pragma mark tableview delegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 44.0;
    }
    else
    {
        return 70.0;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 26.0;
    }
    else
    {
        return 36.0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

if([categoryName isEqualToString:@"Dining"] || [categoryName isEqualToString:@"Bars"]){
        return [filterCount count]+2;
    }
    else
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
        return [arrGroups count];
    else if(section==1)
        return [arrSorting count];
//    else if (section==2)
//        return [arrOptions count];
    else{
         Multivalue *multipleValue ;
        multipleValue = [filterCount objectAtIndex:section-2];
        if (multipleValue.isExpanded) {
            return [multipleValue.valueNames count];
        }
        else{
            return 0;
        }
    }
    
//    return 0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
     Multivalue *multipleValue ;
    UIButton *sectionHeader = [UIButton buttonWithType:UIButtonTypeCustom];
    sectionHeader.layer.borderColor = [UIColor blackColor].CGColor;
    sectionHeader.layer.borderWidth = 0.25;
   
   
    UILabel *lblCatName ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 18)];
    [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
	}
    else
    {
       
	   lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 25)];
    [lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
	}

    [lblCatName setTextColor:[UIColor blackColor]];
    
    if(section==0)
        lblCatName.text = @"Group Items by";
    else if(section==1)
        lblCatName.text = @"Sort Items by" ;

    
    else {
         multipleValue = [filterCount objectAtIndex:section-2];
        lblCatName.text = multipleValue.header ;
        sectionHeader.tag = section;
        UIImageView *caratImage =[[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-25, 7, 12, 13)];
        [sectionHeader addTarget:self action:@selector(clickedonHeader:) forControlEvents:UIControlEventTouchUpInside];
        [caratImage setBackgroundColor:[UIColor clearColor]];
        [sectionHeader addSubview:caratImage];
      
        if(multipleValue.isExpanded)
            [caratImage setImage:[UIImage imageNamed:@"carat-open"]];
        else
            [caratImage setImage:[UIImage imageNamed:@"carat"]];
    }

    [sectionHeader addSubview:lblCatName];
    [sectionHeader setBackgroundColor:[UIColor grayColor]];
    
    return sectionHeader;
}



-(void)clickedonHeader:(UIButton *)sender
{

    Multivalue *multipleValue ;
    multipleValue = [filterCount objectAtIndex:(sender.tag-2)];
//   multipleValue = [filterCount objectAtIndex:sender.tag];
    

    if (multipleValue.isExpanded==NO) {
        multipleValue.isExpanded=YES;
     
        [tblGroupingNSorting reloadData];
        
        NSIndexPath *selectedRowIndexPath = [NSIndexPath indexPathForRow:(sender.tag+1) inSection:sender.tag];
        
        if ([[tblGroupingNSorting visibleCells] containsObject:[tblGroupingNSorting cellForRowAtIndexPath:selectedRowIndexPath]]==NO) {
            NSIndexPath *tempIndexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
            [tblGroupingNSorting scrollToRowAtIndexPath:tempIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
    }
    else{
 
        multipleValue.isExpanded=NO;
        [tblGroupingNSorting reloadData];
    }
    
 
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellEventListView";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor =  [UIColor clearColor];
        
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }
        
    }
    
    
    
    if(indexPath.section == 0)
    {
        UILabel *lblEvtDate;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            lblEvtDate= [[UILabel alloc]initWithFrame:CGRectMake(10, 13, SCREEN_WIDTH - 20, 20)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        }
        else
        {
          lblEvtDate= [[UILabel alloc]initWithFrame:CGRectMake(10, 18, SCREEN_WIDTH - 20, 40)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
        }
        
        [lblEvtDate setText:[arrGroups objectAtIndex:indexPath.row]];
        [lblEvtDate setTextColor:[UIColor blackColor]];
        [lblEvtDate setBackgroundColor:[UIColor clearColor]];
        [cell.contentView setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:lblEvtDate];
        //[lblEvtDate release];
        
        if(indexPath.row == groupSelectionVal)
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else if(indexPath.section == 1)
    {
        UILabel *lblEvtDate;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            lblEvtDate= [[UILabel alloc]initWithFrame:CGRectMake(10, 13, SCREEN_WIDTH - 20, 20)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        }
        else
        {
            lblEvtDate= [[UILabel alloc]initWithFrame:CGRectMake(10, 18, SCREEN_WIDTH - 20, 40)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
        }

        [lblEvtDate setText:[arrSorting objectAtIndex:indexPath.row]];
        [lblEvtDate setTextColor:[UIColor blackColor]];
        [lblEvtDate setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:lblEvtDate];
        [cell.contentView setBackgroundColor:[UIColor clearColor]];
       // [lblEvtDate release];
        
        
        if(isExpanded==TRUE && indexPath.row>([arrSorting indexOfObject:@"City"])){
            
            UIButton *content ;
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                content = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 45)];
            }
            else
            {
               content = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
            }

            [content setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
            content.tag = indexPath.row;
            [content addTarget:self action:@selector(didSelectCity:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            if ([selectedCitiIds containsObject:[citiesDic valueForKey:[arrSorting objectAtIndex:content.tag]]]) {
                checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOn.png"]];
                if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                    checkButton.frame = CGRectMake(10, 15, 20, 20);

                }
                else
                {
                    checkButton.frame = CGRectMake(10, 20, 30, 30);

                }
                checkButton.backgroundColor =[UIColor clearColor];
            }
            
            else{
                checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOff.png"]];
                if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                    checkButton.frame = CGRectMake(10, 15, 20, 20);
                    
                }
                else
                {
                    checkButton.frame = CGRectMake(10, 20, 30, 30);
                    
                }
                checkButton.backgroundColor =[UIColor clearColor];
                
                
            }
            
            UILabel *lblCiti ;
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                lblCiti = [[UILabel alloc]initWithFrame:CGRectMake(40, 0, SCREEN_WIDTH, 45)];
                 [lblCiti setFont:[UIFont boldSystemFontOfSize:13]];
            }
            else
            {
                lblCiti = [[UILabel alloc]initWithFrame:CGRectMake(60, 0, SCREEN_WIDTH, 70)];
                 [lblCiti setFont:[UIFont boldSystemFontOfSize:18]];
            }
            [lblCiti setText:[arrSorting objectAtIndex:indexPath.row]];
            [lblCiti setTextColor:[UIColor blackColor]];
            [lblCiti setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
           
            
            [content addSubview:checkButton];
            [content addSubview:lblCiti];
            [cell.contentView addSubview:content];
           // [content release];
        }
        
        if(indexPath.row == sortSelectionval)
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
   
    
    else if(indexPath.section > 1){
        

         Multivalue *multipleValue ;
         multipleValue = [filterCount objectAtIndex:indexPath.section-2];
        
        if(multipleValue.isExpanded==YES)
        {
    
            if ([multipleValue.header isEqualToString:@"Options"]) {
                if([selectedFilterId containsObject:[OptionsDic valueForKey:[multipleValue.valueNames objectAtIndex:indexPath.row]]] ){
                    checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOn.png"]];
                    if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                        checkButton.frame = CGRectMake(10, 15, 20, 20);
                        
                    }
                    else
                    {
                        checkButton.frame = CGRectMake(10, 20, 30, 30);
                        
                    }
                    checkButton.backgroundColor =[UIColor clearColor];
                }
                
                else{
                    checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOff.png"]];
                    if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                        checkButton.frame = CGRectMake(10, 15, 20, 20);
                        
                    }
                    else
                    {
                        checkButton.frame = CGRectMake(10, 20, 30, 30);
                        
                    }
                    checkButton.backgroundColor =[UIColor clearColor];
                    
                }
            }

            else {
                
            if([selectedFilterValueIds containsObject:[OptionsDic valueForKey:[multipleValue.valueNames objectAtIndex:indexPath.row]]])
            {
                checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOn.png"]];
                if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                    checkButton.frame = CGRectMake(10, 15, 20, 20);
                    
                }
                else
                {
                    checkButton.frame = CGRectMake(10, 20, 30, 30);
                    
                }
                checkButton.backgroundColor =[UIColor clearColor];
            }
            
            else{
                checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOff.png"]];
                if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                    checkButton.frame = CGRectMake(10, 15, 20, 20);
                    
                }
                else
                {
                    checkButton.frame = CGRectMake(10, 20, 30, 30);
                    
                }
                checkButton.backgroundColor =[UIColor clearColor];
            }
            }
                      UILabel *lblCiti ;
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                lblCiti = [[UILabel alloc]initWithFrame:CGRectMake(40, 0, SCREEN_WIDTH, 45)];
                 [lblCiti setFont:[UIFont boldSystemFontOfSize:13]];
            }
            else
            {
                lblCiti = [[UILabel alloc]initWithFrame:CGRectMake(60, 0, SCREEN_WIDTH, 70)];
                 [lblCiti setFont:[UIFont boldSystemFontOfSize:18]];
            }
            [lblCiti setText:[multipleValue.valueNames objectAtIndex:indexPath.row]];
            [lblCiti setTextColor:[UIColor blackColor]];
            [lblCiti setBackgroundColor:[UIColor clearColor]];
            
            
//            [content addSubview:checkButton];
//            [content addSubview:lblCiti];
            [cell.contentView addSubview:checkButton];
            [cell.contentView addSubview:lblCiti];
            [cell.contentView setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
             cell.accessoryType = UITableViewCellAccessoryNone;
//            [content release];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0 && indexPath.row==1 && [arrDinningTypes count])
    {
        //[self Request_fetcheventdetail];
        [self showpickerView];
        groupSelectionVal = 1;
        
    }
    
    else if(indexPath.section==0){
        if (groupSelectionVal==-1 || groupSelectionVal==1) {
            groupSelectionVal = (int)indexPath.row;
        }
        else
            groupSelectionVal = -1;
    }
        //    else
//        sortSelectionval = indexPath.row;
    
    else if(indexPath.section==1 && cityFlag==0){
        sortSelectionval = (int)indexPath.row;
    }
    
    else{
        if(indexPath.section==1 && cityFlag==1){
        
        if(indexPath.row == [arrSorting indexOfObject:@"City"])
        {
            NSMutableArray *arrayOfIndexPaths = [[NSMutableArray alloc] init];
            if (isExpanded==FALSE )
            {
                isExpanded = TRUE;
                [arrSorting addObjectsFromArray:arrCities];
                for(int i = (int)[arrSorting indexOfObject:@"City"]+1 ; i < [arrSorting count] ; i++)
                {
                    NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:1];
                    [arrayOfIndexPaths addObject:path];
                }
                [tblGroupingNSorting beginUpdates];
                
                [tblGroupingNSorting insertRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation:UITableViewRowAnimationTop];
                
                [tblGroupingNSorting endUpdates];
            }
            
        }
        
        else{
            [arrSorting removeObjectsInArray:[citiesDic allKeys]];
            isExpanded = FALSE;
//            if ([selectedCitiIds count]>0) {
//                [selectedCitiIds removeAllObjects];
//            }
//            
//            [selectedCitiIds addObjectsFromArray:[citiesDic allValues]];
            
        }
         sortSelectionval = (int)indexPath.row;
    
        }
        
        else{
            
            [self didSelectFilters:indexPath];
    }
    
    }
    
    [tblGroupingNSorting reloadData];
}


-(void)didSelectCity:(id)sender
{
    
    if([[citiesDic allValues] count]>= 1)
    {
        UIButton *btnCity = (UIButton*)sender;
        
        
        if ([selectedCitiIds containsObject:[citiesDic valueForKey:[arrSorting objectAtIndex:btnCity.tag]]]) {
            //            if ([selectedCitiIds count]==1) {
            //                [selectedCitiIds removeObject:[citiesDic valueForKey:[arrGroups objectAtIndex:btnCity.tag]]];
            //                [arrGroups removeObjectsInArray:[citiesDic allKeys]];
            //                isExpanded = FALSE;
            //            }
            //            else
            [selectedCitiIds removeObject:[citiesDic valueForKey:[arrSorting objectAtIndex:btnCity.tag]]];
            
        }
        
        //        else if ([selectedCitiIds count]==0){
        //            [selectedCitiIds removeObject:[citiesDic valueForKey:[arrSorting objectAtIndex:btnCity.tag]]];
        //        }
        else{
            [selectedCitiIds addObject:[citiesDic valueForKey:[arrSorting objectAtIndex:btnCity.tag]]];
            
        }
        [tblGroupingNSorting reloadData];
    }
    
}



-(void)didSelectFilters:(NSIndexPath*)sender
{
    Multivalue *multipleValue ;
    DLog(@"%ld",(long)sender.section);
    multipleValue = [filterCount objectAtIndex:sender.section-2];
    
    if ([multipleValue.header isEqualToString:@"Options"]) {
        if ( ![selectedFilterId containsObject:[multipleValue.filterId objectAtIndex:sender.row]]) {
            [selectedFilterId addObject:[multipleValue.filterId objectAtIndex:sender.row]];
        }
        else{
            [selectedFilterId removeObject:[multipleValue.filterId objectAtIndex:sender.row]];
        }
      
    }
    else if([multipleValue.valueNames count]>= 1)
    {

    
         if ([selectedFilterValueIds containsObject:[OptionsDic valueForKey:[multipleValue.valueNames objectAtIndex:sender.row]]]) {
             BOOL containsValue=FALSE;
            [selectedFilterValueIds removeObject:[OptionsDic valueForKey:[multipleValue.valueNames objectAtIndex:sender.row]]];
            
             for (int i=0;i<[multipleValue.valueIds count];i++ ) {
                 if([selectedFilterValueIds containsObject:[multipleValue.valueIds objectAtIndex:i]])
                 {
                     containsValue=TRUE;
                 }
             }
             if (containsValue==FALSE) {
                  [selectedFilterId removeObject:[multipleValue.filterId objectAtIndex:sender.row]];
             }
            
        }
        
        else{

                if ( ![selectedFilterId containsObject:[multipleValue.filterId objectAtIndex:sender.row]]) {
                    [selectedFilterId addObject:[multipleValue.filterId objectAtIndex:sender.row]];
                }
    
                [selectedFilterValueIds addObject:[OptionsDic valueForKey:[multipleValue.valueNames objectAtIndex:sender.row]]];
        }
        
    }
     DLog(@"selectedFilterValueIds\n%@",selectedFilterValueIds);
      DLog(@"selectedFilterId\n%@",selectedFilterId);
    [tblGroupingNSorting reloadData];

}



#pragma mark PICKERVIEW
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    NSMutableDictionary *dic = [arrDinningTypes objectAtIndex:row];
    [defaults setValue:[dic valueForKey:@"catId"] forKey:@"SubCategoryId"];
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [arrDinningTypes count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
/*- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
 {
 NSMutableDictionary *dic = [arrDinningTypes objectAtIndex:row];
 return [dic valueForKey:@"catName"];
 }*/

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50.0;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel *label = [[UILabel alloc] init];
    
    if (IOS7){
        label.frame = CGRectMake(20, 0, 220, 40);
        //pickerView.backgroundColor = [UIColor colorWithRGBInt:0xEEEEEE];
        pickerView.layer.borderWidth = 5.0;
        pickerView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    }
    
    else
        label.frame = CGRectMake(0, 0, 200, 40);
    
    [label setBackgroundColor:[UIColor clearColor]];
    [label setNumberOfLines:2];
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    [label setTextAlignment:NSTextAlignmentLeft];
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    
    NSMutableDictionary *dic = [arrDinningTypes objectAtIndex:row];
    label.text = [dic valueForKey:@"catName"];
    
    return label;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

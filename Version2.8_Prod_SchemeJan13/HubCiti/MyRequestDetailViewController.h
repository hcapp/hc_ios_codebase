//
//  MyRequestDetailViewController.h
//  HubCiti
//
//  Created by Bindu M on 10/7/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "CommonUtility.h"
@class AnyViewController;
@class EmailShareViewController;
@interface ViewMyRequestDetailDO : NSObject{
    NSMutableArray *makeRequestDetailArray;
    NSString *fldName;
    NSNumber *fldNum;
    NSString *fldPrompt;
    BOOL fldRequired;
    
}

@property (nonatomic, strong) NSMutableArray *makeRequestDetailArray;
@property (nonatomic, strong) NSString *fldName;
@property (nonatomic, strong) NSNumber *fldNum;
@property (nonatomic, strong) NSString *fldPrompt;
@property (nonatomic, assign) BOOL fldRequired;



@end


@interface MyRequestDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate,UIScrollViewDelegate,UITextViewDelegate,MFMessageComposeViewControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>{
    WebRequestState iWebRequestState;
    CommonUtility *common;
    BOOL isRemember;
    
    NSMutableArray *ReqDetailArray;
    NSMutableDictionary *staticReqDictionary;
    UITableView *detailReqTblView;
    NSString *pageTitle;
    NSMutableString *descNote;
    //NSNumber *reqTypeID;
    NSMutableArray *arrBottomBtnDO;
    NSMutableArray *arrBottomButtonViewContainer;
    UIButton *submitBtn;
    UIButton *cancelBtn;
    UIPickerView *statePicker;
    UIButton *pickerBtn;
    UIView *doneView;
    NSMutableArray *pickerArrData;
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property (nonatomic,strong) NSMutableArray *arrBottomBtnDO;
@property (nonatomic,strong) NSMutableDictionary *staticReqDictionary;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic, strong) NSMutableArray *ReqDetailArray;
@property (nonatomic, strong) NSString *pageTitle;
@property (nonatomic, strong) NSString *referenceNum;
@property (nonatomic, strong) NSMutableString *descNote;
@property (nonatomic, strong) id customReqResponse;
//@property (nonatomic, retain) NSNumber *reqTypeID;

@end

//
//  SwipeViewController.m
//  SwipeGesture
//
//  Created by Bindu M on 3/18/14.
//  Copyright (c) 2014 Bindu M. All rights reserved.
//
#import "SGTableSection.h"
#import "SGTableRow.h"
#import "SGTableSubRow.h"
#import "SwipeViewController.h"

@interface SwipeViewController (){
   // BOOL isExpanded;
    BOOL isSelected;
    UIButton *expandRowBtn;
    UIImageView *caratImage;
   // UILabel *lblEvtDate;
    UIImageView *checkButtonImg;
    UILabel *lblRow;
    UIButton *checkBtn ;

}

#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)

@end

@implementation SwipeViewController



@synthesize  swipeTV, sortByItemArray, secHeaderData, activityIndicator, filterByItemArray, mainTableArr,done, dropDownBtn, catArray,dataArray, arrCitiId,citiesDic, selectedCitiIDs, optArray, cityArray,intArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    isSelected = FALSE;
    

    done = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [done addTarget:self action:@selector(sortingDone)forControlEvents:UIControlEventTouchUpInside];
    [done setTitle:@"Done" forState:UIControlStateNormal];
    done.backgroundColor = [UIColor darkGrayColor];
    [done.titleLabel setFont:[UIFont systemFontOfSize:15]];
   // buttonName.titleLabel.font = [UIFont fontWithName:@"LuzSans-Book" size:15];
    //done.tintColor = [UIColor grayColor];
    [done setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    done.frame = CGRectMake(self.view.frame.origin.x+165, self.view.frame.origin.y+swipeTV.frame.size.height+10, self.view.frame.size.width/2-10, 20);
    done.layer.cornerRadius = 10; // this value vary as per your desire
    done.clipsToBounds = YES;
    [self.view addSubview:done];
    done.hidden = YES;
    
    swipeTV.delegate = self;
    swipeTV.dataSource = self;

    
    secHeaderData = [NSMutableArray arrayWithObjects:@"Sort", @"Filter", nil];
    
    sortByItemArray = [NSMutableArray arrayWithObjects:@"Distance",nil];
    
    filterByItemArray = [NSMutableArray arrayWithObjects:@"Local Specials",@"Category", @"Options",@"City",@"Interests", nil];
    
    
    NSLog(@"RowData %@", filterByItemArray);
    catArray = [NSMutableArray arrayWithObjects:@"Test1",@"Test2", @"Test3", nil];
    
    optArray = [NSMutableArray arrayWithObjects:@"abc",@"xyz", @"pqr", nil];
    cityArray = [NSMutableArray arrayWithObjects:@"test",@"abc", @"lmn",nil];
    intArray = [NSMutableArray arrayWithObjects:@"Test1",@"Test2", nil];
    
    arrCitiId = [NSMutableArray arrayWithObjects:@"1", @"2", @"3", nil];
   // selectedCitiIDs = [NSMutableArray arrayWithObjects:@"2", @"1", nil];
    selectedCitiIDs = [[NSMutableArray alloc]init];
    
    
    
    
    /////////////////////////
    NSMutableArray *dataArr=[[NSMutableArray alloc] init];
    
    self.dataArray=dataArr;
    
    for(int i=0;i<[secHeaderData count];i++)
    {
        
        SGTableSection *sgsec=[SGTableSection alloc];
        
        sgsec.sectionName=[NSString stringWithFormat:@"%@",[secHeaderData objectAtIndex:i]];
        
        
        NSMutableArray *marr=[[NSMutableArray alloc] init];
        
        
        
        NSUInteger cnnt=0;
        
        NSArray *arrcnn=nil;
        
        if(i==0)
        {
            arrcnn=sortByItemArray;
            
        }
        else if(i==1)
        {
            arrcnn=filterByItemArray;
            
        }
        cnnt=arrcnn.count;
        
        for(int i=0;i<cnnt;i++)
        {

             SGTableRow *sgrow=[SGTableRow alloc];
            sgrow.isRow=1;
            sgrow.rowName= [NSString stringWithFormat:@"%@",[arrcnn objectAtIndex:i]];
            
            
            NSMutableArray *marr1=[[NSMutableArray alloc] init];
            
            for(int i=0;i< cityArray.count;i++)
            {
                SGTableSubRow *subrow=[SGTableSubRow alloc];
                subrow.isRow=0;
                subrow.subRowName=[NSString stringWithFormat:@"%@",[cityArray objectAtIndex:i]];
                
                
                [marr1 addObject:subrow];
            }
            
            
            if(marr1.count>1)
            {
                SGTableSubRow *subrow=[SGTableSubRow alloc];
                subrow.isRow=0;
                subrow.subRowName=@"All";
                
                subrow.isFirstRow=1;
                subrow.isFirstRowSelected=0;
                [marr1 insertObject:subrow atIndex:0];
            }
            
            
            sgrow.subrows=marr1;
            
            [marr addObject:sgrow];
            
        }
        sgsec.rows=marr;
        
        
        
       [dataArr addObject:sgsec];
    }
    
    //////////////////////////
    
    
    
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
   // [self.view addSubview:self.activityIndicator];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.view addSubview:activityIndicator];
    activityIndicator.hidden = YES;
    
}



-(void)removeFromSuperview{
    [activityIndicator stopAnimating];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == swipeTV) {
        return [dataArray count];
    }
    else
    
    return 1;
}


- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    view.tintColor = [UIColor grayColor];
    
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor blackColor]];
    
    // Another way to set the background color
    // Note: does not preserve gradient effect of original header
    // header.contentView.backgroundColor = [UIColor blackColor];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (tableView == swipeTV) {
        return [[dataArray objectAtIndex:section] sectionName];
    }
    
    return  @"";
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == swipeTV) {
        return [[[dataArray objectAtIndex:section] rows] count];
    }
   

    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    /////////////////////
    for(id vw in cell.contentView.subviews)
        [(UIView*)vw removeFromSuperview];
    
    
    /////////////////////
    
    [cell setBackgroundColor:[UIColor whiteColor]];
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    //NSMutableArray *mainArr = [[NSMutableArray alloc]init];
    [arr addObject:sortByItemArray];
    [arr addObject:filterByItemArray];
    NSLog(@"Arr %@", arr);
    
    if (tableView == swipeTV)
    {        
        if([[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isRow]){
       // cell.textLabel.text = [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName];
            
            lblRow= [[UILabel alloc]initWithFrame:CGRectMake(10, 5, SCREEN_WIDTH - 20, 40)];
            
            [lblRow setText:[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] rowName]];
            
            [lblRow setTextColor:[UIColor blackColor]];
            [lblRow setBackgroundColor:[UIColor clearColor]];
            [lblRow setFont:[UIFont boldSystemFontOfSize:18]];
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            expandRowBtn  = [UIButton buttonWithType:UIButtonTypeSystem];
            [expandRowBtn setTintColor:[UIColor blackColor]];
            expandRowBtn.frame = CGRectMake(self.swipeTV.frame.size.width/2+20,5, 80, 40);
            
            [expandRowBtn addTarget:self action:@selector(clickOnTableViewRow::)forControlEvents:UIControlEventTouchUpInside];
            if(!indexPath.section == 0){
                if(!(indexPath.section==1 &&  indexPath.row==0 && ([[filterByItemArray objectAtIndex:0]isEqualToString:@"Local Specials"] || [[filterByItemArray objectAtIndex:0]isEqualToString:@"Event Date"]))){
                if(![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isRowExpanded]){
//                    caratImage =[[UIImageView alloc]init];
//                    [caratImage setImage:[UIImage imageNamed:@"carat"]];
                    [expandRowBtn setImage:[UIImage imageNamed:@"carat.png"] forState:UIControlStateNormal];
                }
                else {
                   // [caratImage setImage:[UIImage imageNamed:@"carat-open"]];
                    [expandRowBtn setImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateNormal];
                }
                }
            }
            expandRowBtn.tag=10;
            
           // caratImage.frame = CGRectMake(10, 10, 20, 20);

           // [caratImage setBackgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:lblRow];
          //  [expandRowBtn addSubview:caratImage];
            [cell.contentView addSubview:expandRowBtn];
         }
        else{
            if(!indexPath.section == 0){
                
                UILabel *lblSubRow= [[UILabel alloc]initWithFrame:CGRectMake(50, 8, SCREEN_WIDTH - 20, 40)];

                [lblSubRow setText:[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] subRowName]];
                
                [lblSubRow setFont:[UIFont boldSystemFontOfSize:18]];
                [cell.contentView addSubview:lblSubRow];
                
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
                
                //UIButton *checkBtn ;
                checkBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 15, 20, 20)];//(0, 0, 20, 45)
                checkBtn.tag = indexPath.row;
                [checkBtn addTarget:self action:@selector(didSubRowClick:)forControlEvents:UIControlEventTouchUpInside];
                [checkBtn setBackgroundColor:[UIColor clearColor]];
                
              
               BOOL issel=   [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isSelected];
                
                BOOL isfstsel=   [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isFirstRowSelected];
                
                
                SGTableSubRow *sRow= [[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row];
                if([[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isFirstRow])
                {
                
                sRow.subRowsCount=0;
                }
                else
                {
                  SGTableSubRow *sRow1=  [[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)];
                    sRow.subRowsCount=sRow1.subRowsCount+1;
                }
                

                if (issel == TRUE || isfstsel==TRUE) {
                    //checkButtonImg =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOn.png"]];
                    
                    [checkBtn setImage:[UIImage imageNamed:@"checkboxOn.png"] forState:UIControlStateNormal];
                    
                    //checkButtonImg.frame = CGRectMake(0, 0, 20, 20);//(10, 15, 20, 20)
                    //checkButtonImg.backgroundColor =[UIColor clearColor];
                }
                
                else{
                    //checkButtonImg =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOff.png"]];
                    
                    [checkBtn setImage:[UIImage imageNamed:@"checkboxOff.png"] forState:UIControlStateNormal];
                
                    //checkButtonImg.frame = CGRectMake(0, 0, 20, 20);//(10, 15, 20, 20)
                        
                    //checkButtonImg.backgroundColor =[UIColor clearColor];
                }
                //checkButtonImg.userInteractionEnabled=YES;
                //checkButtonImg.tag=9;
                
                //[checkBtn addSubview:checkButtonImg];
                [cell.contentView addSubview:checkBtn];
                
            }
        }
        
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *tCell=[tableView cellForRowAtIndexPath:indexPath];

   UIButton *caratvw=(UIButton*) [tCell.contentView viewWithTag:10 ];
    //UIImageView *caratvw1=(UIImageView*)[(UIButton*) [tCell.contentView viewWithTag:indexPath.row ] viewWithTag:9];
    UIButton *caratvw1=(UIButton*) [tCell.contentView viewWithTag:indexPath.row ] ;
    
    if (tableView == swipeTV) {
        if (indexPath.section == 0 || (indexPath.section==1 &&  indexPath.row==0 && ([[filterByItemArray objectAtIndex:0]isEqualToString:@"Local Specials"] || [[filterByItemArray objectAtIndex:0]isEqualToString:@"Event Date"]))) {
                if([tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryCheckmark){
                [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
                }
                else{
                [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
                }
        }
        else{
        if([[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isRow])
        {
               NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] subrows] count];
               
               NSRange range=NSMakeRange((indexPath.row+1), [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] subrows] count]);
               
               NSMutableArray *mutarr=[[NSMutableArray alloc] init];
               @autoreleasepool {
                   
               NSIndexSet *indexSet=[NSIndexSet indexSetWithIndexesInRange:range];
               
                   for(int i=1;i<=cnt;i++)
                   {
                       NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                       
                       [mutarr addObject:indexPath1];
                   }
                   
                    [swipeTV beginUpdates];
                       if(! [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isRowExpanded])/////Add
                       {
                           [caratvw setImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateNormal];
                           //[caratvw setImage:[UIImage imageNamed:@"carat-open"]];
                           [[[dataArray objectAtIndex:indexPath.section] rows] insertObjects:[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] subrows] atIndexes:indexSet];
                           [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsRowExpanded:1];
                           
                           [swipeTV insertRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationTop];
                       }
                       else
                       {
                           [caratvw setImage:[UIImage imageNamed:@"carat.png"] forState:UIControlStateNormal];

                           //[caratvw setImage:[UIImage imageNamed:@"carat"]];
                            [[[dataArray objectAtIndex:indexPath.section] rows] removeObjectsAtIndexes:indexSet];
                           [swipeTV deleteRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationTop];
                           
                           [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsRowExpanded:0];
                        }
                    [swipeTV endUpdates];
               
               }
            
        }
        
        else
        {
            if(![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isFirstRow])
            {
                
                if(![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isSelected])
                {
                   // [caratvw1 setImage:[UIImage imageNamed:@"checkboxOn.png"]];
                    
                    [caratvw1 setImage:[UIImage imageNamed:@"checkboxOn.png"] forState:UIControlStateNormal];
                    
                    [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsSelected:1];
                    
                }
                else
                {
                   // [caratvw1 setImage:[UIImage imageNamed:@"checkboxOff.png"]];
                    
                     [caratvw1 setImage:[UIImage imageNamed:@"checkboxOff.png"] forState:UIControlStateNormal];
                    
                    SGTableSubRow *sRow= [[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row];
                    
                    [sRow setIsSelected:0];
                    NSMutableArray *mutarr=[[NSMutableArray alloc] init];

                    int16_t fstrwno=indexPath.row-sRow.subRowsCount;
            
                    @autoreleasepool {
                        
                        NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:fstrwno inSection:indexPath.section];
                        
                        [mutarr addObject:indexPath1];
                        
                        [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsFirstRowSelected:0];

                    }
                    
                    [tableView reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                    mutarr=nil;
                    
                }

            }
            else
            {
                BOOL selval=0;
 
                if(![[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] isFirstRowSelected])
                {
                   // [caratvw1 setImage:[UIImage imageNamed:@"checkboxOn.png"]];
                    
                    [caratvw1 setImage:[UIImage imageNamed:@"checkboxOn.png"] forState:UIControlStateNormal];
                    
                    [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsFirstRowSelected:1];
                    
                    selval=1;
                    
                }
                else
                {
                  //  [caratvw1 setImage:[UIImage imageNamed:@"checkboxOff.png"]];
                    
                      [caratvw1 setImage:[UIImage imageNamed:@"checkboxOff.png"] forState:UIControlStateNormal];
                    
                    [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath.row] setIsFirstRowSelected:0];
                    selval=0;
                }
                NSUInteger cnt= [[[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:(indexPath.row-1)] subrows] count];
                NSMutableArray *mutarr=[[NSMutableArray alloc] init];

                @autoreleasepool {
                    for(int i=1;i<cnt;i++)
                    {
                        NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:(indexPath.row+i) inSection:indexPath.section];
                        
                        [mutarr addObject:indexPath1];
                        
                        [[[[dataArray objectAtIndex:indexPath.section] rows] objectAtIndex:indexPath1.row] setIsSelected:selval];
                        
                    }
                }
                [tableView reloadRowsAtIndexPaths:mutarr withRowAnimation:UITableViewRowAnimationNone];
                mutarr=nil;
            }

        }
    
    }
    }
}


-(void)clickOnTableViewRow:(NSInteger) section :(NSInteger) row{
    
//    [expandRowBtn setImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateNormal];
    if (section == 1 && row == 3 && [[filterByItemArray objectAtIndex:row]isEqualToString:@"City"] ) {
        [expandRowBtn setImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateNormal];
        NSLog(@"Data %@", [filterByItemArray objectAtIndex:row]);
    }
    
}



-(void)didSubRowClick:(id)sender{
    
    if ([cityArray count]!=0) {
        citiesDic = [[NSDictionary alloc] initWithObjects:arrCitiId forKeys:cityArray];
    }
    UIButton *subRow = (UIButton *) sender;
    if(subRow.tag == 3){
    [selectedCitiIDs addObject:[cityArray objectAtIndex:subRow.tag]];
  //  [checkBtn setImage:[UIImage imageNamed:@"checkboxOn.png"] forState:UIControlStateNormal];
        if ([[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedCityIds"]==nil) {
            
            [[NSUserDefaults standardUserDefaults] setValue:arrCitiId forKey:@"SelectedCityIds"];
        }
        
        //        selectedCitiIds = [[NSMutableArray alloc]initWithArray:[defaults valueForKey:@"SelectedCityIds"]];
        else if([[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedCityIds"]!=nil && [[NSUserDefaults standardUserDefaults] valueForKey:@"commaSeperatedCities"]!=nil)
        {
            [selectedCitiIDs removeAllObjects];
            [selectedCitiIDs addObjectsFromArray:[NSMutableArray arrayWithArray:[[[NSUserDefaults standardUserDefaults] valueForKey:@"commaSeperatedCities"] componentsSeparatedByString:@","]]];
        }
    }
     NSLog(@"City Dic : %@", citiesDic);
    NSLog(@"Selected City Dic : %@", selectedCitiIDs);
    [swipeTV reloadData];
}


-(void)sortingDone{
    [activityIndicator startAnimating];
    activityIndicator.hidden = NO;
    swipeTV.hidden = YES;
    done.hidden = YES;
    [activityIndicator performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:3.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

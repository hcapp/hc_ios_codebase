//
//  CategoryList.h
//  HubCiti
//
//  Created by Nikitha on 2/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventList.h"
@protocol CategoryList;
@interface CategoryList : NSObject
@property(nonatomic,strong) NSNumber *categoryId;
@property(nonatomic, strong) NSString *groupContent;
@property(nonatomic,strong) NSMutableArray <EventList> * eventList;
@property (nonatomic, strong)NSMutableArray *catObjArray;
@property (nonatomic, strong)NSString *categoryImgPath;
@end

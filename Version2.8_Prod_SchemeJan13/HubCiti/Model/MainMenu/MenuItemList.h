//
//  MenuItemList.h
//  HubCiti
//
//  Created by service on 10/6/15.
//  Copyright © 2015 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MenuItemList;

@interface MenuItemList : NSObject

@property(nonatomic,assign) int  mItemId;
@property(nonatomic,strong) NSString * mItemName;
@property(nonatomic,assign) int  linkTypeId;
@property(nonatomic,strong) NSString* linkId;
@property(nonatomic,strong) NSString * linkTypeName;
@property(nonatomic,assign) int  position;
@property(nonatomic,strong) NSString * mItemImg;
@property(nonatomic,strong) NSString * mBtnColor;
@property(nonatomic,strong) NSString * mBtnFontColor;
@property(nonatomic,strong) NSString * smBtnFontColor;
@property(nonatomic,strong) NSString * smBtnColor;
@property(nonatomic,strong) NSString * mGrpBkgrdColor;
@property(nonatomic,strong) NSString * mGrpFntColor,*mItemTypeName,*departmentName;
@property(nonatomic,strong) NSString * mBkgrdColor,*mFontColor,*mShapeId;
@property(nonatomic,strong) NSString * mBkgrdImage,*smFontColor,*smBkgrdColor;
@property(nonatomic,strong) NSString *smGrpFntColor,*smGrpBkgrdColor,*mShapeName;
@property(nonatomic,strong) NSNumber *sortOrder;
@property(nonatomic,strong) NSNumber *flag;

@end

//
//  BottomItemList.m
//  HubCiti
//
//  Created by service on 10/6/15.
//  Copyright © 2015 Keshavakarthik S. All rights reserved.
//

#import "BottomItemList.h"

@implementation BottomItemList
@synthesize dict_linkTypeName_BottomButton;

- (void)initTheDictionary
{
     //[super init];
    // Custom initialization
    if (dict_linkTypeName_BottomButton == nil) {//assigning number to each bottom buttons - in order to
        dict_linkTypeName_BottomButton = [[NSMutableDictionary alloc]init];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:0] forKey:@"Hot Deals"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:1] forKey:@"Scan Now"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:2] forKey:@"Alerts"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:3] forKey:@"Events"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:4] forKey:@"Whats NearBy"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:5] forKey:@"Find"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:6] forKey:@"City Experience"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:7] forKey:@"City Services"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:8] forKey:@"Visitors Bureau"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:9] forKey:@"Transportation"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:10] forKey:@"Preference"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:11] forKey:@"About"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:12] forKey:@"Share"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:13] forKey:@"Settings"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:14] forKey:@"AnythingPage"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:15] forKey:@"AppSite"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:16] forKey:@"SubMenu"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:17] forKey:@"Filters"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:18] forKey:@"Coupon"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:19] forKey:@"Deals"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:20] forKey:@"FAQ"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:21] forKey:@"Fundraiser"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:22] forKey:@"User Information"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:23] forKey:@"Category Favorites"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:24] forKey:@"Location Preferences"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:25] forKey:@"City Favorites"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:26] forKey:@"Map"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:27] forKey:@"SortFilter"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:28] forKey:@"Filter"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:29] forKey:@"My Deals"];
        [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:30] forKey:@"Privacy Policy"];
        
    }
}

@end

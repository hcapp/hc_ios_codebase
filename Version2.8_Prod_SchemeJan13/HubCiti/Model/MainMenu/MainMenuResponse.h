//
//  MainMenuResponse.h
//  HubCiti
//
//  Created by service on 10/6/15.
//  Copyright © 2015 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MenuItemList.h"
#import "NewsTickerList.h"
#import "BottomItemList.h"
@protocol MainMenuResponse;
@interface MainMenuResponse : NSObject

@property(nonatomic,strong) NSString * menuId;
@property(nonatomic,strong) NSString * hubCitiId;
@property(nonatomic,strong) NSString * level;
@property(nonatomic,strong) NSString * templateName;
@property(nonatomic,assign) BOOL  departFlag;
@property(nonatomic,assign) BOOL  isTempChanged;
@property(nonatomic,strong) NSString * retGroupImg;
@property(nonatomic,strong) NSString * appIconImg;
@property(nonatomic,assign) int  retAffCount;
@property(nonatomic,assign) int  retAffId;
@property(nonatomic,strong) NSString * retAffName;
@property(nonatomic,assign) BOOL  isRegApp;
@property(nonatomic,assign) BOOL  typeFlag;
@property(nonatomic,strong) NSString * dateModified;
@property(nonatomic,strong) NSString * mBannerImg;
@property(nonatomic,strong) NSString * mBkgrdColor;
@property(nonatomic,strong) NSString * mBkgrdImage;
@property(nonatomic,strong) NSString * smBkgrdColor;
@property(nonatomic,strong) NSString * smBkgrdImage;
@property(nonatomic,strong) NSString * smFontColor;
@property(nonatomic,strong) NSString * menuName;
@property(nonatomic,strong) NSString * mFontColor;
@property(nonatomic,strong) NSString * responseCode;
@property(nonatomic,strong) NSString * responseText;
@property(nonatomic,strong) NSString * downLoadLink;
@property(nonatomic,strong) NSString * androidLink;
//@property(nonatomic,strong) NSString *listCatDetails;
//For Two Image Template
@property(nonatomic,strong) NSString *TempBkgrdImg;
@property(nonatomic,strong) NSNumber *isDisplayLabel;

@property(nonatomic,strong) NSString *mItemId;
@property(nonatomic,strong) NSString *linkTypeId;
@property(nonatomic,strong) NSString *linkId;
@property(nonatomic,strong) NSString *position;
@property(nonatomic,strong) NSString *mItemName;
@property(nonatomic,strong) NSString *mItemImg;
@property(nonatomic,strong) NSString *linkTypeName;

@property(nonatomic,strong) NSString *mBtnColor;
@property(nonatomic,strong) NSString *mBtnFontColor;

@property(nonatomic,strong) NSString *smBtnColor;
@property(nonatomic,strong) NSString *smBtnFontColor;

@property(nonatomic,strong) NSString *mGrpBkgrdColor;
@property(nonatomic,strong) NSString *mGrpFntColor;

@property(nonatomic,strong) NSString *smGrpBkgrdColor;
@property(nonatomic,strong) NSString *smGrpFntColor;


@property(nonatomic,strong) NSString *mShapeId;
//@property(nonatomic,retain) NSString *mShapeName;
@property(nonatomic,assign) int noOfColumns;

@property(nonatomic,strong) NSString *templateBgColor;
@property (strong, nonatomic) NSArray<MenuItemList>* arMItemList;
@property (strong, nonatomic) NSArray<BottomItemList>* arBottomBtnList;

@property(nonatomic,strong) NSString * btnBkgrdColor;
@property(nonatomic,strong) NSString * btnLblColor;

@property (nonatomic, strong) NSString *bkImgPath;
@property (nonatomic, strong) NSString *homeImgPath;
@property (nonatomic, strong) NSString *titleTxtColor;
@property (nonatomic, strong) NSString *titleBkGrdColor;
@property (nonatomic,strong) NSString *pgnColorActive;
@property (nonatomic,strong) NSString *pgnColorInActive;

@property (nonatomic, strong) NSString *hamburgerImg;
//•	"pgnColorActive":"#000000",
//•	"pgnColorInActive":"#808080"

// News Ticker:

@property (nonatomic, strong) NSString *isNewTicker;
@property (nonatomic, strong) NSString *tickerTxtColor;
@property (nonatomic, strong) NSString *tickerBkgrdColor;
@property (nonatomic, strong) NSString *tickerDirection;
@property (nonatomic,strong)  NSString *tickerMode;

@property (strong, nonatomic) NSArray<NewsTickerList>* arItemList;

@end

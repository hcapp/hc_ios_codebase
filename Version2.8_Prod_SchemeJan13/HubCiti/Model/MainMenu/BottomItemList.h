//
//  BottomItemList.h
//  HubCiti
//
//  Created by service on 10/6/15.
//  Copyright © 2015 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BottomItemList;
@interface BottomItemList : NSObject

@property (nonatomic, strong) NSString *bottomBtnID;
@property (nonatomic, strong) NSString *bottomBtnName;
@property (nonatomic, strong) NSString *bottomBtnImg;
@property (nonatomic, strong) NSString *bottomBtnImgOff;
@property (nonatomic, strong) NSString *btnLinkTypeID;
@property (nonatomic, strong) NSString *btnLinkTypeName;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, strong) NSString *btnLinkID;
@property (nonatomic, strong) NSMutableDictionary *dict_linkTypeName_BottomButton;
- (void)initTheDictionary;

@end


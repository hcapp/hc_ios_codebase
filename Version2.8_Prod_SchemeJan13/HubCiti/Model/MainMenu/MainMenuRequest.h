//
//  MainMenuRequest.h
//  HubCiti
//
//  Created by service on 10/6/15.
//  Copyright © 2015 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainMenuRequest : NSObject

@property(nonatomic,strong) NSString * userId;
@property(nonatomic,strong) NSString * hubCitiId;
@property(nonatomic,strong) NSString * level;
@property(nonatomic,strong) NSString * sortOrder;
@property(nonatomic,strong) NSString * departmentId;
@property(nonatomic,strong) NSString * typeId;
@property(nonatomic,strong) NSString * deviceId;
@property(nonatomic,strong) NSString * osVersion;
@property(nonatomic,strong) NSString * linkId;
@property(nonatomic,strong) NSString * platform;
@property(nonatomic,strong) NSString * cityIds;
@property(nonatomic,strong) NSString * dateCreation;



@end

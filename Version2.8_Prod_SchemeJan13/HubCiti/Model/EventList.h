//
//  EventList.h
//  HubCiti
//
//  Created by Nikitha on 2/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol EventList;
@interface EventList : NSObject
@property(nonatomic,strong) NSNumber * eventId;
@property(nonatomic,strong) NSString * eventName;
@property(nonatomic,strong) NSString * shortDes;
@property(nonatomic,strong) NSString * longDes;
@property(nonatomic,strong) NSNumber * hubCitiId;
@property(nonatomic,strong) NSString * imgPath;
@property(nonatomic,strong) NSNumber * busEvent;
@property(nonatomic,strong) NSNumber * pkgEvent;
@property(nonatomic,strong) NSString * startDate;
@property(nonatomic,strong) NSString * startTime;
@property(nonatomic,strong) NSNumber * mItemExist;
@property(nonatomic,strong) NSNumber * eventCatId;
@property(nonatomic,strong) NSString * eventCatName;
@property(nonatomic,strong) NSNumber * rowNum;
@property(nonatomic,strong) NSNumber * eventListId;
@property(nonatomic,strong) NSString * distance;
@property(nonatomic,strong) NSNumber * isOnGoing;
@property(nonatomic,strong) NSString * isOngoing;
@property(nonatomic,strong) NSString * city;
@property(nonatomic,strong) NSString * endTime;
@property(nonatomic,strong) NSString * endDate;

@property(nonatomic,strong) NSString * address;
@property(nonatomic,strong) NSString * latitude;
@property(nonatomic,strong) NSString * longitude;
@property(nonatomic,strong) NSString * pkgDes;
@property (nonatomic, strong)NSString *address1;
@property (nonatomic, strong)NSString *address2;
@property (nonatomic, strong)NSString *pkgTicketURL;
@property (nonatomic, strong)NSString *hotelFlag;
@property (nonatomic, strong)NSString *moreInfoURL;
@property (nonatomic, strong)NSString *recurringDays;
@property (nonatomic, strong)NSString * isAppSiteFlag;
@property (nonatomic, strong)NSString *logisticsURL;
@property (nonatomic,strong)NSString *location;
@property (nonatomic,strong) NSString *recurrencePattern;
@property (nonatomic,strong)NSString *isWeekDay;
@property (nonatomic) NSInteger recurrenceInterval;
@property (nonatomic)NSInteger dateOfMonth;
@property (nonatomic)NSInteger dayNumber;
@property (nonatomic,strong)NSArray *daysOfWeek;
@property (nonatomic,strong)NSArray *everyWeekDayMonth;
@property (nonatomic,strong) NSString *recurrencePatternName;
@property (nonatomic,strong) NSString *byDayNumber;

@end












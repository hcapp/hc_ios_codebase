//
//  EventListResponse.h
//  HubCiti
//
//  Created by Nikitha on 2/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoryList.h"
#import "EventList.h"
#import "bottomButtonDO.h"
//@protocol EventListResponse;
@interface EventListResponse : NSObject
@property(nonatomic, strong) NSString * responseCode;
@property(nonatomic, strong) NSString * responseText;
@property(nonatomic, strong) NSNumber * bottomBtn;
@property(nonatomic, strong) NSNumber * mainMenuId;
@property(nonatomic, strong) NSNumber * maxCount;
@property(nonatomic, strong) NSNumber * nextPage;
@property(nonatomic, strong) NSNumber * maxRowNum;
@property(nonatomic,strong) NSArray <CategoryList> * categoryList;
@property(nonatomic,strong) NSArray <bottomButtonDO> * bottomBtnList;
@end

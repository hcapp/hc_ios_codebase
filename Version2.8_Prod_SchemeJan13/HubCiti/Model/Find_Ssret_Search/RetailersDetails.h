//
//  RetailersDetails.h
//  HubCiti
//
//  Created by Lakshmi H R on 2/12/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RetailersInDetail.h"
#import "bottomButtonDO.h"
@interface RetailersDetails : NSObject

@property(nonatomic,retain) NSString * responseCode;
@property(nonatomic,retain) NSString * responseText;
@property(nonatomic,retain) NSNumber * bottomBtn;
@property (strong, nonatomic) NSArray<bottomButtonDO > *bottomBtnList;
@property(nonatomic,retain) NSNumber * maxCnt;
@property(nonatomic,retain) NSNumber * nextPage;
@property(nonatomic,retain) NSNumber * mainMenuId;
@property(nonatomic,retain) NSString * poweredby;
@property(nonatomic,retain) NSNumber * findRetSeaId;
@property(nonatomic,retain) NSNumber * maxRowNum;
@property (strong, nonatomic) NSArray<RetailersInDetail>* RetailerDetail;

@end


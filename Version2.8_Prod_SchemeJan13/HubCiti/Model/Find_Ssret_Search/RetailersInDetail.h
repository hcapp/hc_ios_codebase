//
//  RetailersInDetail.h
//  HubCiti
//
//  Created by Lakshmi H R on 2/12/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol RetailersInDetail;

@interface RetailersInDetail : NSObject

@property(nonatomic,retain) NSMutableString * retailerName;
@property(nonatomic,retain) NSNumber * retailLocationId;
@property(nonatomic,retain) NSMutableString * retaileraddress1;
@property(nonatomic,retain) NSNumber * nextPage;
@property(nonatomic,retain) NSMutableString * retaileraddress2;
@property(nonatomic,retain) NSMutableString * retaileraddress3;
@property(nonatomic,retain) NSMutableString * retaileraddress4;
@property(nonatomic,retain) NSMutableString * city;
@property(nonatomic,retain) NSString * state;
@property(nonatomic,retain) NSMutableString * postalCode;
@property(nonatomic,retain) NSString * distance;
@property(nonatomic,retain) NSMutableString * logoImagePath;
@property(nonatomic,retain) NSMutableString * bannerAdImagePath;
@property(nonatomic,retain) NSMutableString * ribbonAdImagePath;
@property(nonatomic,retain) NSMutableString * completeAddress;
@property(nonatomic,retain) NSMutableString * retLongitude;
@property(nonatomic,retain) NSString * retLatitude;
@property(nonatomic,retain) NSMutableString * ribbonAdURL;
@property(nonatomic,retain) NSString * advertisementId;
@property(nonatomic,retain) NSNumber * saleFlag;
@property(nonatomic,retain) NSString * splashAdId;
@property(nonatomic,retain) NSNumber * retListId;
@property(nonatomic,retain) NSNumber * retailerId;
@property(nonatomic,retain) NSNumber * rowNumber;
@property(nonatomic,retain) NSString * locationOpen;


@end



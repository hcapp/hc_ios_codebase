//
//  EventListDetails.h
//  HubCiti
//
//  Created by Lakshmi H R on 4/22/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

//"eventList": [
//              {
//                  address = "1000 Stone Mountain Dr";
//bandID = 1;
//bandName = "Emily Wolfe";
//distance = "6195 mi";
//eventCatId = 2;
//eventCatName = eventcategory2;
//eventId = 10;
//eventName = Events2;
//hubCitiId = 82;
//imgPath = "http://localhost:8080/Images/retailer/1/eventimage.png";
//latitude = "30.5834";
//longDes = " ";
//longitude = "-98.2711";
//moreInfoURL = "www.google.com";
//rowNum = 2;
//shortDes = " ";
//startDate = "04-22-2016";
//startTime = "11:39";
/////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

//"bandName": "Holiday Mountain",
//"bandID": 2,
//"eventId": 9,
//"eventName": "Holiday Events1",
//"shortDes": "Holiday short discription",
//"longDes": "<p>wel come</p> ",
//"hubCitiId": 82,
//"startDate": "04-22-2016",
//"startTime": "11:39",
//"startDate": "04-22-2016",
//"endDate": "04-22-2016",
//"startTime": "11:39",
//"endTime": "11:39",
//"eventCatId": 2,
//"eventCatName": "eventcategory2",
//"rowNum": 1,
//"distance": "0 mi",
//"latitude":"120.25",
//"longitude":"-99.82",
//"address":"austine some where",
//"moreInfoURL": "www.google.com",
//"imagePath": "http://localhost:8080/Images/retailer/2/eventimage.png"
//

#import <Foundation/Foundation.h>

@protocol EventListDetails;

@interface EventListDetails : NSObject

@property (nonatomic,strong) NSString  *bandName;
@property (nonatomic,strong) NSNumber *eventId;
@property (nonatomic,strong) NSString *eventName;
@property (nonatomic,strong) NSString *shortDes;
@property (nonatomic,strong) NSString *longDes;
@property (nonatomic,strong) NSNumber *hubCitiId;
@property (nonatomic,strong) NSString *startDate;
@property (nonatomic,strong) NSString *endDate;
@property (nonatomic,strong) NSString *startTime;
@property (nonatomic,strong) NSString *endTime;
@property (nonatomic,strong) NSNumber *eventCatId;
@property (nonatomic,strong) NSString *eventCatName;
@property (nonatomic,strong) NSNumber *rowNum;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSString *moreInfoURL;
@property (nonatomic,strong) NSString *imgPath;
@property (nonatomic,strong) NSString *retailName;
@property (nonatomic,strong) NSString *evtLocTitle;
@property (nonatomic,strong) NSNumber *retailId;
@property (nonatomic,strong) NSNumber *retailLocationId;
@property (nonatomic,strong) NSNumber *isAppSiteFlag;
@property (nonatomic,strong) NSString * recurringDays;
@property (nonatomic,strong) NSString *listingImgPath;
@property(nonatomic,strong) NSString * ticketUrl;
@property(nonatomic,strong) NSString * bandIds;
@property (nonatomic,strong) NSNumber *bandCntFlag;
@property(nonatomic,strong) NSString * popUpMsg;
@property (nonatomic,strong) NSNumber *bandID;

@end

//
//  BandCatList.h
//  HubCiti
//
//  Created by Ashika on 4/21/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol BandCatList;
@interface BandCatList : NSObject
@property (nonatomic,strong) NSNumber *bandID;
@property(nonatomic,strong) NSString *bandName;
@property(nonatomic,strong) NSString *bandImgPath;
@property(nonatomic,strong) NSNumber *catId;
@property(nonatomic,strong) NSString *catName;
@property (nonatomic,strong) NSNumber *rowNumber;
@property(nonatomic,strong) NSString *catIds;
@property(nonatomic,strong) NSString * bandIds;
@end

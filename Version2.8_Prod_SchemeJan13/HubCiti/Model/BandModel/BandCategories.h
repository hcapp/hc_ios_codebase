//
//  BandCategories.h
//  HubCiti
//
//  Created by Ashika on 4/21/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

//"catList":[
//           {
//               "groupContent":"HipHop",
//               "retDetList":[


#import <Foundation/Foundation.h>
#import "bottomButtonDO.h"
#import "BandCatList.h"
#import "BandGrouping.h"

@interface BandCategories : NSObject
@property(nonatomic, strong) NSString * responseCode;
@property(nonatomic, strong) NSString * responseText;
@property(nonatomic, strong) NSNumber * bottomBtn;
@property(nonatomic, strong) NSNumber * mainMenuId;
@property(nonatomic, strong) NSNumber * maxCnt;
@property(nonatomic, strong) NSNumber * nextPage;
@property(nonatomic, strong) NSNumber * maxRowNum;

//@property (nonatomic,strong) NSArray <BandCatList> *retailerDetail;
@property (nonatomic,strong) NSArray <BandGrouping> *catList;
@property(nonatomic,strong) NSArray <bottomButtonDO> *bottomBtnList;
@end

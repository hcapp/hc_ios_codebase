//
//  EventCatList.h
//  HubCiti
//
//  Created by Ashika on 4/21/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EventCatList;
@interface EventCatList : NSObject
@property (nonatomic,strong) NSString *evtTypeName;
@property (nonatomic,strong) NSNumber *evtTypeID;
@end

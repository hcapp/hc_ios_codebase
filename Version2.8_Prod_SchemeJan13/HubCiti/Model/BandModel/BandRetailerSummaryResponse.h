//
//  BandRetailerSummaryResponse.h
//  HubCiti
//
//  Created by Nikitha on 4/23/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RetailerDetailsResponse.h"
#import "RetailerCreatedPageResponse.h"

@interface BandRetailerSummaryResponse : NSObject
@property (nonatomic,strong) NSString * responseCode;
@property (nonatomic,strong) NSString * responseText;
@property (nonatomic,strong) NSNumber * eventExist;
@property (nonatomic,strong) NSNumber * fundExist;
@property (nonatomic,strong) NSArray <RetailerDetailsResponse> * retailerDetail;
@property (nonatomic,strong) NSArray <RetailerCreatedPageResponse> * retailerCreatedPageList;


@end


//
//  RetailerCreatedPageResponse.h
//  HubCiti
//
//  Created by Nikitha on 4/22/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol RetailerCreatedPageResponse;
@interface RetailerCreatedPageResponse : NSObject
@property(nonatomic,strong) NSString * pageLink;
@property(nonatomic,strong) NSString * pageTitle;
@property(nonatomic,strong) NSString * pageImage;
@property(nonatomic,strong) NSNumber * pageId;
@property(nonatomic,strong) NSNumber * retListId;
@property(nonatomic,strong) NSNumber * anythingPageListId;
@property(nonatomic,strong) NSString * pageTempLink;
@end












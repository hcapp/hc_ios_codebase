//
//  BandEventsList.h
//  HubCiti
//
//  Created by Lakshmi H R on 4/22/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//




//"responseCode": "10000",
//"responseText": "Success",
//"maxCount": 1,
//"nextPage": 0,
//"maxRowNum": 1,
//"eventList": [
//              {
//                  "bandName": "Holiday Mountain",
//                  "bandID": 2,
//                  "eventId": 9,
//                  "eventName": "Holiday Events1",
//                  "shortDes": "Holiday short discription",
//                  "longDes": "<p>wel come</p> ",
//                  "hubCitiId": 82,
//                  "startDate": "04-22-2016",
//                  "startTime": "11:39",
//                  "eventCatId": 2,
//                  "eventCatName": "eventcategory2",
//                  "rowNum": 1,
//                  "distance": "0 mi",
//                  "latitude":"120.25",
//                  "longitude":"-99.82",
//                  "address":"austine some where",
//                  "moreInfoURL": www.google.com
//                  "imagePath": "http://localhost:8080/Images/retailer/2/eventimage.png"
//


#import <Foundation/Foundation.h>
#import "EventListDetails.h"
#import "EventGroupListDetails.h"
#import "bottomButtonDO.h"

@interface BandEventsList : NSObject
@property(nonatomic,strong) NSString * responseCode;
@property(nonatomic,strong) NSString * responseText;
@property(nonatomic,strong) NSNumber * maxCount;
@property(nonatomic,strong) NSNumber * nextPage;
@property(nonatomic,strong) NSNumber * maxRowNum;
//to be changed
@property(nonatomic,strong) NSArray<EventGroupListDetails> * eventGroupList;
@property(nonatomic,retain) NSNumber * bottomBtn;
@property (strong, nonatomic) NSArray<bottomButtonDO> *bottomBtnList;


@end

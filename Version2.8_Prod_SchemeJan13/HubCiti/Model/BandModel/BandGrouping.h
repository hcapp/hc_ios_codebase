//
//  BandGrouping.h
//  HubCiti
//
//  Created by Ashika on 5/4/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BandCatList.h"
@protocol BandGrouping;

@interface BandGrouping : NSObject
@property(nonatomic, strong) NSString * groupContent;
@property (nonatomic,strong) NSArray <BandCatList> *retDetList;
@property (nonatomic,strong) NSMutableArray *bandArray;
@end

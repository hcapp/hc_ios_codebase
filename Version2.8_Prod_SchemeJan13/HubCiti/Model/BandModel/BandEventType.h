//
//  BandEventType.h
//  HubCiti
//
//  Created by Ashika on 4/21/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "bottomButtonDO.h"
#import "EventCatList.h"

@interface BandEventType : NSObject
@property(nonatomic, strong) NSString * responseCode;
@property(nonatomic, strong) NSString * responseText;
@property(nonatomic, strong) NSNumber * bottomBtn;
@property(nonatomic, strong) NSNumber * mainMenuId;

@property (nonatomic,strong) NSArray<EventCatList> *catList;
@property(nonatomic,strong) NSArray <bottomButtonDO> * bottomBtnList;
@end

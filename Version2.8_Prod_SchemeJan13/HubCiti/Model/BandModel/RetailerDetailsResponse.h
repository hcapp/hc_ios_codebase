//
//  RetailerDetailsResponse.h
//  HubCiti
//
//  Created by Nikitha on 4/22/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol RetailerDetailsResponse;
@interface RetailerDetailsResponse : NSObject
@property (nonatomic,strong) NSString * bandName;
@property (nonatomic,strong) NSString * bandImgPath;
@property (nonatomic,strong) NSNumber * bandID;

@property (nonatomic,strong) NSString * bandURL;
@property (nonatomic,strong) NSString * bandURLImgPath;
@property (nonatomic,strong) NSString * ribbonAdImagePath;
@property (nonatomic,strong) NSString * ribbonAdURL;
@property (nonatomic,strong) NSString * contactPhone;
@property (nonatomic,strong) NSString * phoneImg;
@property (nonatomic,strong) NSString * mail;
@property (nonatomic,strong) NSString * mailImg;
@property (nonatomic,strong) NSString * retailerPageQRURL;
@property (nonatomic,strong) NSString * appDownloadLink;
@property(nonatomic,strong) NSNumber * ribbonAdId;
@property(nonatomic,strong) NSString * Bandaddress1;


@end

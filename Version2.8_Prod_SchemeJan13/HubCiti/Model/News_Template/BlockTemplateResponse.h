//
//  BlockTemplateResponse.h
//  HubCiti
//
//  Created by Lakshmi H R on 6/24/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

//{
//    "responseCode":"10000",
//    "responseText":"Success",
//    "items":[
//             {
//                 "rowNum":1,
//                 "catId":1,
//                 "catName":"Blogues",
//                 "catImgPath":"image.png",
//                 "catColor":"#FFFFFF",
//                 "isSubcategory":0
//             },
//             {
//                 "rowNum":2,
//                 "catId":2,
//                 "catName":"Politics",
//                 "catImgPath":"image.png",
//                 "catColor":"#FFFFFF",
//                 "isSubcategory":1
//             },
//             {
//                 "rowNum":3,
//                 "catId":3,
//                 "catName":"Top Stories",
//                 "catImgPath":"image.png",
//                 "catColor":"#FFFFFF",
//                 "isSubcategory":0
//             },
//             .
//             .
//             .
//             ],
//    "maxCnt":6,
//    "nextPage":0,
//    "lowerLimitFlag":6,
//    "newtempName":"Combination News Template",
//    "templateChanged":0,
//    "modifiedDate":"Jun 23 2016 12:34PM"
//}

//
//For custom navigation :
//·         bkImgPath
//·         homeImgPath
//·         titleTxtColor
//·         titleBkGrdColor
//
//Weather URL :
//·         weatherURL

#import <Foundation/Foundation.h>
#import "BlockTemplateItemsList.h"

@interface BlockTemplateResponse : NSObject



@property (nonatomic,strong) NSString *responseCode;
@property (nonatomic,strong) NSString *responseText;
@property (nonatomic,strong) NSNumber *maxCnt;
@property (nonatomic,strong) NSNumber *nextPage;
@property(nonatomic,strong)  NSString * bannerImg;
@property (nonatomic,strong) NSNumber *lowerLimitFlag;
@property (nonatomic,strong) NSString *newtempName;
@property (nonatomic,assign) BOOL templateChanged;
@property (nonatomic,strong) NSString *modifiedDate;
@property (nonatomic,strong) NSArray<BlockTemplateItemsList> *items;
@property (nonatomic,strong) NSString *bkImgPath;
@property (nonatomic,strong) NSString *homeImgPath;
@property (nonatomic,strong) NSString *titleTxtColor;
@property (nonatomic,strong) NSString *titleBkGrdColor;
@property (nonatomic,strong) NSString *weatherURL;
@property (nonatomic,strong) NSString *hamburgerImg;


@end

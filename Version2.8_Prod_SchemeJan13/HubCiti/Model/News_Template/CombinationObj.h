//
//  CombinationObj.h
//  HubCiti
//
//  Created by service on 5/31/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoryListItem.h"

// Combination Main Model

@interface CombinationObj : NSObject

@property(nonatomic,strong) NSString* responseCode;
@property(nonatomic,strong) NSString* responseText;
@property(nonatomic,strong) NSString* bannerImg;
@property(nonatomic,strong) NSString* maxCnt;
@property(nonatomic,assign) int nextPage;
@property(nonatomic,strong) NSString* lowerLimitFlag;

@property(nonatomic,strong) NSString * modifiedDate;
@property(nonatomic,strong) NSString * newtempName;
@property(nonatomic,assign) BOOL templateChanged;
@property(nonatomic,strong) NSString *linkId;
@property(nonatomic,strong) NSString * level;
@property(nonatomic,strong) NSArray<CategoryListItem>* categoryList;
@property(nonatomic,strong) NSArray<CategoryListItem>*  bookMarkList;

@property (nonatomic, strong) NSString *bkImgPath;
@property (nonatomic, strong) NSString *homeImgPath;
@property (nonatomic, strong) NSString *titleTxtColor;
@property (nonatomic, strong) NSString *titleBkGrdColor;
@property (nonatomic, strong) NSString *weatherURL;
@property (nonatomic, strong) NSString *hamburgerImg;


@end




//
//  CategoryListDetails.h
//  HubCiti
//
//  Created by Lakshmi H R on 6/22/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

//
//{
//    "responseCode":"10000",
//    "responseText":"Success",
//    "listCatDetails":[
//                      {
//                          "parCatId":10,
//                          "parCatName":"Sports",
//                          "catColor":"#FFFFFF",
//                          "isSubCategory":0,
//                          "listCatDetails":[
//                                            {
//                                                "subCatId":1,
//                                                "subCatName":"Cricket",
//                                                "isSubCategory":1
//                                            },
//                                            {
//                                                "subCatId":2,
//                                                "subCatName":"Football",
//                                                "isSubCategory":1
//                                            },
//                                            {
//                                                "subCatId":3,
//                                                "subCatName":"Table Tennis",
//                                                "isSubCategory":1
//                                            }
//                                            ]
//                      }
//                      ]
//}
//
//2nd Type Response : without Sub Category Json Response.
//
//{  
//    "responseCode":"10000",
//    "responseText":"Success",
//    "listCatDetails":[  
//                      {
//                          "parCatId":1,
//                          "parCatName":"Blogues",
//                          "catColor":"#FFFFFF",
//                          "isSubCategory":0
//                      }
//                      ]
//}
//

#import <Foundation/Foundation.h>
#import "SubCategoryDetails.h"

@protocol CategoryListDetails;

@interface CategoryListDetails : NSObject

@property (nonatomic,strong) NSString *responseCode;
@property (nonatomic,strong) NSString *responseText;
@property (nonatomic, strong) NSNumber *parCatId;
@property (nonatomic,strong) NSString *parCatName;
@property (nonatomic,strong) NSString *catColor;
@property (nonatomic)     NSNumber *isSubCategory;
@property (nonatomic,strong) NSArray < SubCategoryDetails> *listCatDetails;



@end

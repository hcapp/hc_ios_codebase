//
//  SubCategoryDetails.h
//  HubCiti
//
//  Created by Lakshmi H R on 6/22/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

//                                                "subCatId":3,
//                                                "subCatName":"Table Tennis",
//                                                "isSubCategory":1

#import <Foundation/Foundation.h>

@protocol SubCategoryDetails;

@interface SubCategoryDetails : NSObject

@property (nonatomic,strong) NSString *subCatId;
@property (nonatomic,strong) NSString *subCatName;
@property (nonatomic) NSNumber *isSubCategory;

@end

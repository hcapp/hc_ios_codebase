//
//  CategoryList.h
//  HubCiti
//
//  Created by service on 6/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ItemList.h"

// Category List

@protocol CategoryListItem;

@interface CategoryListItem : NSObject

@property(nonatomic,strong) NSString* catName;
@property(nonatomic,strong) NSString* groupContent;
@property(nonatomic,strong) NSString *catColor;
@property(nonatomic,strong) NSString *catTxtColor;
@property(nonatomic,strong) NSString *nonfeedlink;
@property(nonatomic,strong) NSString *backButtonColor;
@property(nonatomic,strong) NSArray<ItemList>* itemList;


@end




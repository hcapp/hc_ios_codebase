//
//  NewsTickerList.h
//  HubCiti
//
//  Created by service on 10/6/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol NewsTickerList;

@interface NewsTickerList : NSObject

@property(nonatomic,strong) NSString* title;
@property(nonatomic,strong) NSString* catId;
@property(nonatomic,strong) NSString* catName ;
@property(nonatomic,strong) NSString* rowCount;

@end

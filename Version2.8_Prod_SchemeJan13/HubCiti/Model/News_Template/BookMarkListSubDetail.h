//
//  BookMarkListSubDetail.h
//  HubCiti
//
//  Created by Ashika on 6/24/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
//"subCatId": 1,
//"subCatName": "All",
//"isAdded": 0,
//"isBkMark": 0,
//"isSubCategory": 0,
//"sortOrder": "5"

@protocol BookMarkListSubDetail;
@interface BookMarkListSubDetail : NSObject
@property(strong,nonatomic) NSNumber *subCatId;
@property(strong,nonatomic) NSNumber *isAdded;
@property(strong,nonatomic) NSString *sortOrder;
@property(strong,nonatomic) NSString *subCatName;
@property(nonatomic) BOOL *isSubCategory;
@property(strong,nonatomic) NSNumber *isBkMark;

@end

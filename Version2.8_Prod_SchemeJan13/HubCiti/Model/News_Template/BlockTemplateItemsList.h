//
//  BlockTemplateItemsList.h
//  HubCiti
//
//  Created by Lakshmi H R on 6/24/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//


//"items":[
//         {
//             "rowNum":1,
//             "catId":1,
//             "catName":"Blogues",
//             "catImgPath":"image.png",
//             "catColor":"#FFFFFF",
//             "isSubcategory":0
//         },
//

@protocol BlockTemplateItemsList;

#import <Foundation/Foundation.h>

@interface BlockTemplateItemsList : NSObject

@property (nonatomic,strong) NSString *rowNum;
@property (nonatomic,strong) NSString *catId;
@property (nonatomic,strong) NSString *catName;
@property (nonatomic,strong) NSString *catTxtColor;
@property (nonatomic,strong) NSString *catImgPath;
@property (nonatomic,strong) NSString *catColor;
@property (nonatomic,strong) NSString *isSubcategory;
@property (nonatomic,strong) NSString *nonfeedlink;
@property (nonatomic,strong) NSString *backButtonColor;

@end

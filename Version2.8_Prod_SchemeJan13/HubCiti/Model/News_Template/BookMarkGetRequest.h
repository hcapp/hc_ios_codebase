//
//  BookMarkGetRequest.h
//  HubCiti
//
//  Created by Ashika on 5/30/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BookMarkListDetail.h"

@interface BookMarkGetRequest : NSObject
@property (nonatomic, strong) NSString *bkImgPath;
@property (nonatomic, strong) NSString *bannerImg;
@property (nonatomic, strong) NSString *homeImgPath;
@property (nonatomic, strong) NSString *titleBkGrdColor;
@property (nonatomic, strong) NSString *weatherURL;
@property (nonatomic, strong) NSString *hamburgerImg;
@property(nonatomic,strong) NSString *responseCode;
@property(nonatomic,strong) NSString *responseText;
@property(nonatomic,strong) NSString *backButtonColor;
@property(nonatomic,strong) NSArray<BookMarkListDetail> *bookMarkList;
@end

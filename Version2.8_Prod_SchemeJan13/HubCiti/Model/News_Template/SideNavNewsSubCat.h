//
//  SideNavNewsSubCat.h
//  HubCiti
//
//  Created by Ashika on 6/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol SideNavNewsSubCat;
@interface SideNavNewsSubCat : NSObject
@property(strong,nonatomic) NSString *subCatName;
@property(strong,nonatomic) NSNumber *subCatId;
@property(strong,nonatomic) NSNumber *sortOrder;
@property(strong,nonatomic) NSNumber *isSubCategory;
@end

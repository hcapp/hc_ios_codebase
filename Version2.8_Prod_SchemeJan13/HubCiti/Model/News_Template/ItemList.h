//
//  ItemList.h
//  HubCiti
//
//  Created by service on 6/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ItemList;

@interface ItemList : NSObject

@property(nonatomic,strong) NSString* itemID;
@property(nonatomic,strong) NSString* rowNum;
@property(nonatomic,strong) NSString* title;
@property(nonatomic,strong) NSString* link;
@property(nonatomic,strong) NSString* catName;
@property(nonatomic,strong) NSString* date;
@property(nonatomic,strong) NSString* Time;
@property(nonatomic,strong) NSString* displayType;
@property(nonatomic,strong) NSString* author;
@property(nonatomic,strong) NSString* image;
@property(nonatomic,strong) NSString* sDesc;
@property(nonatomic,strong) NSString* lDesc;
@property(nonatomic,strong) NSString* videoLink;


@end
//
//  SideNavigationResponse.h
//  HubCiti
//
//  Created by Ashika on 6/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SideNavigationList.h"

@interface SideNavigationResponse : NSObject
@property(nonatomic,strong) NSString *responseCode;
@property(nonatomic,strong) NSString *responseText;
@property(nonatomic,strong) NSArray <SideNavigationList> *listCatDetails;

@end

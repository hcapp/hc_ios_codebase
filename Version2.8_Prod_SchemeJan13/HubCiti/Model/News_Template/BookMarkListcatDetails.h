//
//  BookMarkListcatDetails.h
//  HubCiti
//
//  Created by Ashika on 5/30/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BookMarkListSubDetail.h"
@protocol BookMarkListcatDetails;
@interface BookMarkListcatDetails : NSObject
@property(strong,nonatomic) NSNumber *parCatId;
@property(strong,nonatomic) NSNumber *isAdded;
@property(strong,nonatomic) NSString *sortOrder;
@property(strong,nonatomic) NSString *parCatName;
@property(strong,nonatomic) NSNumber *isHubFunctn;
@property(strong,nonatomic) NSNumber *isBkMark;
@property(strong,nonatomic) NSString* catColor;
@property(strong,nonatomic) NSString *nonfeedlink;
@property(nonatomic,strong) NSString*backButtonColor;
@property (nonatomic,strong) NSArray <BookMarkListSubDetail> *listCatDetails;
@property(nonatomic) BOOL isSubCategory;
@end

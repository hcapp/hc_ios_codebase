//
//  ScrollTemplateResponse.h
//  HubCiti
//
//  Created by Nikitha on 5/31/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ScrollTemplateItems.h"
@protocol ScrollTemplateResponse;
@interface ScrollTemplateResponse : NSObject
@property (nonatomic,strong) NSString * responseCode;
@property(nonatomic,strong) NSString * responseText;
@property(nonatomic,strong) NSString * bannerImg;
@property(nonatomic,strong) NSNumber * maxCnt;
@property(nonatomic,strong) NSNumber * nextPage;
@property(nonatomic,strong) NSNumber * lowerLimitFlag;
@property(nonatomic,strong) NSString * modifiedDate;
@property(nonatomic,strong) NSString * newtempName;
@property(nonatomic,assign) BOOL templateChanged;
@property(nonatomic,strong) NSString *subPageName;
@property(nonatomic,strong) NSString *catColor;
@property(nonatomic,strong) NSString * catTxtColor;
@property(nonatomic,strong) NSNumber * isSubcategory;
@property(nonatomic,strong) NSString *backButtonColor;
@property (nonatomic, strong) NSString *bkImgPath;
@property (nonatomic, strong) NSString *homeImgPath;
@property (nonatomic, strong) NSString *titleTxtColor;
@property (nonatomic, strong) NSString *titleBkGrdColor;
@property (nonatomic,strong) NSString *weatherURL;
@property (nonatomic, strong) NSString *hamburgerImg;


@property(nonatomic,strong) NSArray <ScrollTemplateItems> * items;


@end

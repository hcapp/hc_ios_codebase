//
//  SideNavigationNewsCat.h
//  HubCiti
//
//  Created by Ashika on 6/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SideNavNewsSubCat.h"

@protocol SideNavigationNewsCat;
@interface SideNavigationNewsCat : NSObject
@property(strong,nonatomic) NSNumber *catId;
@property(strong,nonatomic) NSString *sortOrder;
@property(strong,nonatomic) NSString *catName;
@property(strong,nonatomic) NSString *catTxtColor;
@property(strong,nonatomic) NSNumber *flag;
@property(strong,nonatomic) NSArray<SideNavNewsSubCat> *mainCatList;
@property(strong,nonatomic) NSString* catColor;
@property(nonatomic) BOOL isSubCategory;

@end

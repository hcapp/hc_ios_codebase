//
//  SideNavigationList.h
//  HubCiti
//
//  Created by Ashika on 6/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SideNavigationNewsCat.h"
#import "MainMenuResponse.h"

@protocol SideNavigationList;

@interface SideNavigationList : NSObject
@property(nonatomic,strong) NSArray <SideNavigationNewsCat> *listMainCat;
@property (strong, nonatomic) NSArray <MainMenuResponse>* menuList;
@end

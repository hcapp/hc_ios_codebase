//
//  ScrollTemplateItems.h
//  HubCiti
//
//  Created by Nikitha on 5/31/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol ScrollTemplateItems;
@interface ScrollTemplateItems : NSObject
@property(nonatomic,strong) NSNumber * itemID;
@property(nonatomic,strong) NSNumber * rowNum;
@property(nonatomic,strong) NSString * title;
@property(nonatomic,strong) NSString * link;
@property(nonatomic,strong) NSString * catName;
@property(nonatomic,strong) NSString * date;
@property(nonatomic,strong) NSString * sDesc;
@property(nonatomic,strong) NSString * time;
@property(nonatomic,strong) NSString * imgPosition;
@property(nonatomic,strong) NSString * catColor;
@property(nonatomic,strong) NSNumber * userId;
@property(nonatomic,strong) NSNumber * catId;
@property(nonatomic,strong) NSString * catImgPath;



//not yet given need to change type
@property (nonatomic,strong) NSString * orderby;
@property (nonatomic,strong) NSString * image;
@property (nonatomic,strong) NSString * lDesc;
@property (nonatomic,strong) NSString * classification;
@property (nonatomic,strong) NSString * section;
@property (nonatomic,strong) NSString * adcopy;
@property (nonatomic,strong) NSString * hubCitiID;
@property (nonatomic,strong) NSString * author;
@property (nonatomic,strong) NSString * subcategory;
@property (nonatomic,strong) NSString * videoLink;
@property (nonatomic,strong) NSString * displayType;
@property(nonatomic,strong) NSString * apiPatnerName;
@property(nonatomic,strong) NSString * reason;
@property(nonatomic,strong) NSNumber * rowCount;
@property(nonatomic,strong) NSNumber * status;
@property(nonatomic,strong) NSString  * HubCitiName;
@property(nonatomic,strong) NSString * message;
@property(nonatomic,strong) NSString * searchKey;
@property(nonatomic,strong) NSString * templateType;
@property(nonatomic,strong) NSString * catTxtColor;
@property(nonatomic,strong) NSNumber * isSubcategory;


@end

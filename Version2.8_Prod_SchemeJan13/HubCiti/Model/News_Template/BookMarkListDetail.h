//
//  BookMarkListDetail.h
//  HubCiti
//
//  Created by Ashika on 5/30/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BookMarkListcatDetails.h"


@protocol BookMarkListDetail;
@interface BookMarkListDetail : NSObject
@property (nonatomic,strong) NSString *header;
@property (nonatomic,strong) NSArray <BookMarkListcatDetails> *listCatDetails;
@property (nonatomic,strong) NSMutableArray *bookMarkArray;
@property (nonatomic,strong) NSMutableArray *sectionsArray;

@end

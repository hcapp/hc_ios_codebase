//
//  Categories.h
//  HubCiti
//
//  Created by Lakshmi H R on 2/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CatList.h"
#import "bottomButtonDO.h"

@interface Categories : NSObject


@property(nonatomic,retain) NSString * responseCode;
@property(nonatomic,retain) NSString * responseText;
@property(nonatomic,retain) NSNumber * mainMenuId;
@property(nonatomic,retain) NSString * mColor;
@property(nonatomic,retain) NSString * mFontColor;
@property(nonatomic,retain) NSNumber * bottomBtn;
@property (strong, nonatomic) NSArray<CatList>* catList;
@property (strong, nonatomic) NSArray<bottomButtonDO> *bottomBtnList;

@end

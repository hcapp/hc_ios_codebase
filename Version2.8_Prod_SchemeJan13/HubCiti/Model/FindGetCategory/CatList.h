//
//  CatList.h
//  HubCiti
//
//  Created by Lakshmi H R on 2/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CatList;

@interface CatList : NSObject

@property(nonatomic,retain) NSString * CatDisName;
@property(nonatomic,retain) NSString * CatImgPth;
@property(nonatomic,retain) NSNumber * CatID;
@end

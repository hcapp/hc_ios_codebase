//
//  CreateAccountViewController.m
//  HubCiti
//
//  Created by service on 9/23/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "CreateEditAccountViewController.h"
#import "CreateEditAccountTableViewCell.h"
#import "TextFieldElement.h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "HubCitiConstants.h"
#import "AppDelegate.h"
#import <AFNetworking/AFNetworking.h>
#import "SingleCatRetailers.h"
#import "EventsListViewController.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "FindViewController.h"
#import "CityExperienceViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "SettingsViewController.h"
#import "MainMenuViewController.h"
#import "FilterListViewController.h"
#import "FilterRetailersList.h"
#import "CouponsViewController.h"
#import "FAQCategoryList.h"
#import "FundraiserListViewController.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "PreferredCategoriesScreen.h"
#import "WebBrowserViewController.h"
#import "AnythingPage.h"
#import "HubCitiConstants.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "UserInformationViewController.h"
#import "SpecialOffersViewController.h"
#include "HTTPClient.h"
#include "GetUserInfoResponse.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponsInDealViewController.h"


@interface CreateEditAccountViewController ()
{
 

    UIButton* submitButton,*cancelButton;
   
    BOOL isKeyBoradOpen;
   
    float fontSize;
    
    NSString *pickerStr;
    UIPickerView *statePicker;
   
    UIView *doneView;
    NSMutableArray *pickerArrData,*cellArray,*savedData;
    UIToolbar* numberToolbar;
    bottomButtonView *view;
    
}
@property (retain, nonatomic) UITableView *tableView;
@property (retain, nonatomic) NSMutableArray *formItems;
@property (retain, nonatomic) NSMutableArray *labelFieldName;
@property (retain, nonatomic) UIButton *pickerBtn;
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@end


@implementation CreateEditAccountViewController
//@synthesize pickerBtn,isEditScreen,anyVC;
@synthesize infoResponse;
@synthesize pickerBtn,isEditScreen,anyVC,emailSendingVC;

-(void) reqForAccountInfo
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    NSDictionary* parameters;
    [HubCitiAppDelegate showActivityIndicator];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];

    [dict setObject:@"ifF34jauK;" forKey:@"authKey"];
    [dict setObject:[defaults objectForKey: @"govQAEmailId"] forKey:@"customerEmail"];
    
    parameters=[[NSDictionary alloc] initWithObjectsAndKeys:dict,@"customer",nil];
    
    //@"http://66.228.143.28:8080/HubCiti_Perf/govqa/customerinfo"
    
    [manager POST:[NSString stringWithFormat:@"%@govqa/customerinfo",BASE_URL] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [HubCitiAppDelegate removeActivityIndicator];
        [self parseAccountInfo:responseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [HubCitiAppDelegate removeActivityIndicator];
        
        NSLog(@"Error: %@", error);
    }];


   
}

-(void)parseAccountInfo:(id)responseObj{
    
    
    DLog(@"responseObj %@", responseObj);
    NSDictionary *responseData=responseObj;
    NSDictionary *response=[responseData objectForKey:@"response"];
    NSInteger responseCode=[[response objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[response objectForKey:RESPONSETEXT];
    DLog(@"responseCode %ld", (long)responseCode);
    DLog(@"responsestr %@", responseText);
    
    
    if (responseCode == 10000) {
        
        [savedData insertObject: [[response objectForKey:@"customer"] objectForKey:@"email"] atIndex:0];
        [savedData insertObject: [defaults objectForKey: @"govQAPassword"]  atIndex:1];
        [savedData insertObject: [defaults objectForKey: @"govQAPassword"]  atIndex:2];
        
        [savedData insertObject: [NSString stringWithFormat:@"%@",[[response objectForKey:@"customer"] objectForKey:@"first"]] atIndex:3];
        [savedData insertObject: [NSString stringWithFormat:@"%@",[[response objectForKey:@"customer"] objectForKey:@"last"]] atIndex:4];
        [savedData insertObject:[NSString stringWithFormat:@"%@",[[response objectForKey:@"customer"] objectForKey:@"phone"]]  atIndex:5];
        submitButton.hidden = false;
        cancelButton.hidden = false;
        self.tableView.hidden = false;
        [self.tableView reloadData];
    }
    
    else if(responseCode == 10002)
    {
        [self showAlert:responseText];
    }
    
    else if(responseCode == 10004)
    {
        [self showAlert:responseText];
    }
    
    
    else{
        [UtilityManager showAlert:responseText msg:responseText];
        return;
    }
    
    
}
#pragma mark - Service Respone methods

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    [self responseData:responseObject];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    savedData= [[NSMutableArray alloc] init];
    cellArray = [[NSMutableArray alloc] init];
    numberToolbar = [[UIToolbar alloc]init];
    
    
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    
    if (self.isEditScreen) {
        self.title = @"Edit Account Information";
        [self reqForAccountInfo];
    }
    else
    {
        self.title = @"Create Account";
    }
    
    
    if (IPAD) {
        fontSize = 16;
    }
    else
    {
        fontSize = 10;
    }
    
    pickerBtn = [[UIButton alloc] init];
    
       
    
   
  /*  if (self.isEditScreen)
    {
        submitButton.hidden = true;
        cancelButton.hidden = true;
    }*/
    
    self.formItems = [[NSMutableArray alloc] initWithCapacity:10];
    
    if (self.isEditScreen) {
        
        self.labelFieldName = [[NSMutableArray alloc] initWithObjects:@"Email Address:", @"*Password:", @"*Confirm Password:", @"*First Name:", @"*Last Name:", @"*Phone:", nil];

    }
    else
    {
        self.labelFieldName = [[NSMutableArray alloc] initWithObjects:@"*Email Address:", @"*Password:", @"*Confirm Password:", @"*First Name:", @"*Last Name:",  @"Apt/Suite Number:", @"Home Address:", @"City:", @"State/Province:", @"Zip/Postal Code:", @"*Phone:", nil];
    }
   
    
    for (int i=0; i<self.labelFieldName.count; i++)
    {
        TextFieldElement *item = [[TextFieldElement alloc] init];
        item.label = [self.labelFieldName objectAtIndex:i];
        item.value = @"";
        [self.formItems insertObject:item atIndex:i];
    }
    
    
    [self settableViewOnScreen];
    
    if(([self.arrayBottomButtonID count] > 0))
    {
        [self setBottomBarMenu];
        
    }
    pickerArrData= [[NSMutableArray alloc] initWithObjects:@"AK",@"AL",@"AR",@"AS",
                    @"AZ",@"CA",@"CO",@"CT",@"DC",@"DE",@"FL",@"FM",
                    @"GA",@"HI",@"IA",@"ID",@"IL",@"IN",@"KS",@"KY",
                    @"LA",@"MA",@"MD",@"ME",@"MI",@"MN",@"MO",@"MS",
                    @"MT",@"NC",@"ND",@"NE",@"NH",@"NJ",@"NM",@"NV",
                    @"NY",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",
                    @"TN",@"TX",@"UT",@"VA", @"VT",@"WA",@"WI",@"WV",
                    @"WY",nil];

    
    pickerStr = @"TX";
    
   
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}

-(void)cancelNumberPad{
    
    for (int i = 0; i< self.formItems.count; i++) {
        
        CreateEditAccountTableViewCell *cell = [cellArray objectAtIndex:i];
        [cell.valueField resignFirstResponder];
       // cell.valueField.text = @"";
    }
   
    
}

-(void)doneWithNumberPad{
    
    
    for (int i = 0; i< self.formItems.count; i++) {
        
        CreateEditAccountTableViewCell *cell = [cellArray objectAtIndex:i];
        [cell.valueField resignFirstResponder];
    }

}

-(void)settableViewOnScreen
{
    if (!self.tableView) {
        self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - bottomBarButtonHeight) style:UITableViewStylePlain];
    }
    if (self.isEditScreen)
    {
        self.tableView.hidden = true;
    }
    else{
        self.tableView.hidden = false;
    }
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, VARIABLE_HEIGHT(150), 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.dataSource=self;
    self.tableView.delegate=self;
    self.tableView.allowsSelection = NO;
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    self.tableView.tableFooterView.hidden=YES;
    self.tableView.tableHeaderView.hidden=YES;
    self.tableView.translatesAutoresizingMaskIntoConstraints=YES;
    [self.view addSubview:self.tableView];
    
    
}

-(void)setBottomBarMenu
{
    
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    self.arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        ////[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [self.arrayBottomButtonID count]; btnLoop++)
    {
        bottomButtonDO *obj_eventBottomDO = [self.arrayBottomButtonID objectAtIndex:btnLoop];
        
        view = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, 80,bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[self.arrayBottomButtonID count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[self.arrayBottomButtonID count],bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_eventBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [self.arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
    }
}



-(UIButton*) addButton : (NSString*) string : (CGRect) frame
{
    UIButton* button;
    button  = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    button.layer.cornerRadius = 5.0f;
    button.titleLabel.font=[UIFont boldSystemFontOfSize:14];
    button.backgroundColor = [UIColor convertToHexString:@"#AAA8AA"];
    [button setTitle:string forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setAccessibilityLabel:string];
    [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
    button.showsTouchWhenHighlighted=YES;
    [self.view addSubview:button];
    
    return button;
}

#pragma mark - Email Id validation
-(BOOL) emailValidation : (NSString*) emailText
{
   
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = FALSE;
    isValid = [emailTest evaluateWithObject:emailText];
    if(isValid)
    {
        
        return FALSE;
    }
    else
    {
        return TRUE;
    }
    return FALSE;
}
#pragma mark - Mobile Number validation
- (BOOL)mobileNumberValidate:(NSString*)number
{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
        return TRUE;
    else
        return FALSE;
}

-(void) buttonClicked : (id) sender
{
    UIButton* btn = (UIButton*) sender;
    
    if (btn.tag == 1) {
       
        NSString* password,*confirmPassword;
   

        for (int i = 0; i< self.formItems.count; i++) {
            
            
            TextFieldElement *item = [self.formItems objectAtIndex:i];
            CreateEditAccountTableViewCell *cell = [cellArray objectAtIndex:i];
            
            
            if([item.label hasPrefix:@"*"]){
                
                if ([item.label isEqualToString:@"*Password:"])
                {
                    password = cell.valueField.text;
                }
                if ([item.label isEqualToString:@"*Confirm Password:"])
                {
                    confirmPassword = cell.valueField.text;
                }
                
                if ([item.label isEqualToString:@"*Email Address:"]) {
                    
                    if (cell.valueField.text.length == 0) {
                        
                        [self  showAlert :  @"Mandatory fields cannot be empty"];
                        return ;
                        
                    }
                    if ([self emailValidation:cell.valueField.text]) {
                        
                        [self  showAlert : @"The Email format is not valid"];
                        return;
                    }
                }
                if ([item.label isEqualToString:@"*Phone:"]) {
                    NSString* mobNo = @"";
                    
                    if ([cell.valueField.text length] == 0 ) {
                        
                        [self showAlert : @"Mandatory fields cannot be empty"];
                        return;
                    }
                    
                    if (cell.valueField.text ) {
                        mobNo = [cell.valueField.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
                    }
                    
                    if ([mobNo length] == 0 ) {
                        
                        [self showAlert : @"Mandatory fields cannot be empty"];
                        return;
                    }
                    
                    if (![self mobileNumberValidate:mobNo]) {
                        
                        [self showAlert : @"Mobile number is not valid formats."];
                        return;
                    }
                }
                
                
                NSLog(@"Value of text = %@", cell.valueField.text);
                
                if ([cell.valueField.text length] == 0 ) {
                    
                    [self showAlert : @"Mandatory fields cannot be empty"];
                    return;
                }
            }
        }
    if (![password isEqualToString:confirmPassword]) {
            
        [self showAlert : @"Password and confirm password mismatch"];
        return;
    }
  
    [self requestForCreateAccount];
 }
 else
 {
     [self.navigationController popViewControllerAnimated:NO];
 }
}


-(NSString*)trimPhoneNumber : (NSString*) phoneNumber
{
    NSMutableCharacterSet *characterSet =
    [NSMutableCharacterSet characterSetWithCharactersInString:@"-"];
    
    // Build array of components using specified characters as separtors
    NSArray *arrayOfComponents = [phoneNumber componentsSeparatedByCharactersInSet:characterSet];
    
    // Create string from the array components
    NSString *strOutput = [arrayOfComponents componentsJoinedByString:@""];
    
    return strOutput;
}

-(NSString*) getFormattedPhoneNumer : (NSString*) string
{
    
    NSMutableString *text = [NSMutableString stringWithString:[[string componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""]];
    
    for (int i = 0; i< string.length; i++) {
        
        if (i == 4) {
            [text insertString:@"-" atIndex:3];
            
        }
        if (i == 8) {
            [text insertString:@"-" atIndex:7];
            
        }
        if (i == 12) {
            text = [NSMutableString stringWithString:[text substringToIndex:11]];
        }
    }
    
    return text;
    
}

-(void) requestForCreateAccount{
    
    [self dismissKeyBoard];
    if([Network currentReachabilityStatus]==0){
        [UtilityManager showAlert];
        
    }
    else{
        
        NSString* email,*password,*firstName,*lastName,*phone,*address,*city,*state,*zip,*appSuitNumber;
        CreateEditAccountTableViewCell *cell;
        [HubCitiAppDelegate showActivityIndicator];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        
        cell = [cellArray objectAtIndex:0];
        email = cell.valueField.text;
        [defaults setObject:email forKey:@"govQAEmailId"];
        cell = [cellArray objectAtIndex:1];
        password = cell.valueField.text;
        [defaults setObject:password forKey:@"govQAPassword"];
        cell = [cellArray objectAtIndex:3];
        firstName = cell.valueField.text;
        [defaults setObject:firstName forKey:@"first"];
        cell = [cellArray objectAtIndex:4];
        lastName = cell.valueField.text;
        [defaults setObject:lastName forKey:@"last"];
        [defaults  setBool:NO forKey:@"govQARememberMe"];
        
        if (isEditScreen) {
            
            cell = [cellArray objectAtIndex:5];
            phone = [self trimPhoneNumber :cell.valueField.text];
           
        }
        else
        {
            cell = [cellArray objectAtIndex:5];
            appSuitNumber = cell.valueField.text;
            cell = [cellArray objectAtIndex:6];
            address = cell.valueViewField.text;
            cell = [cellArray objectAtIndex:7];
            city = cell.valueField.text;
            state = pickerStr;
            cell = [cellArray objectAtIndex:9];
            zip = cell.valueField.text;
            cell = [cellArray objectAtIndex:10];
            phone = [self trimPhoneNumber :cell.valueField.text];
            [defaults setObject:phone forKey:@"phone"];
        }
       
        NSDictionary *dicParameters ;
        NSDictionary* parameters ;
        
        
        if (self.isEditScreen) {
            
            
            parameters =  @{
                                          
                            @"authKey": @"ifF34jauK;",
                            @"loginEmail": email,
                            @"password": password,
                            @"firstName": firstName,
                            @"lastName": lastName,
                            @"phone": phone,
                            @"groupAccessName": @"",
                            @"custCFName1": @"",
                            @"custCFValue1": @"",
                            @"custCFName2": @"",
                            @"custCFValue2": @"",
                            @"custCFName3": @"",
                            @"custCFValue3": @"",
                            @"custCFName4": @"",
                            @"custCFValue4": @"",
                            @"custCFName5": @"",
                            @"custCFValue5": @""
                                          
                        };

            DLog(@"dicParameters %@",parameters);
            dicParameters=[[NSDictionary alloc] initWithObjectsAndKeys:parameters,@"customerInfo",nil];

        }
        else
        {
           parameters =  @{
                           @"authKey":@"ifF34jauK;",
                           @"customerEmail":email,
                           @"password": password,
                           @"customerFirstName":firstName,
                           @"customerLastName": lastName,
                           @"address1": address,
                           @"address2": appSuitNumber,
                           @"city": city,
                           @"state": state,
                           @"zip": zip,
                           @"phone": phone
                                    
                        };
            
            [defaults setValue:zip forKey:@"Zip"];
            DLog(@"dicParameters %@",parameters);
            dicParameters=[[NSDictionary alloc] initWithObjectsAndKeys:parameters,@"customer",nil];

        }
        
        
       
        NSString *baseUrl ;
        if (self.isEditScreen)
        {
            baseUrl= [NSString stringWithFormat:@"%@govqa/editaccountinfo", BASE_URL];
        }
        else
        {
            baseUrl= [NSString stringWithFormat:@"%@govqa/createaccount",BASE_URL];
        }
        
        
        [manager POST:baseUrl parameters:dicParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            [HubCitiAppDelegate removeActivityIndicator];
            [self parseCreateAccount:responseObject];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [HubCitiAppDelegate removeActivityIndicator];
            
            NSLog(@"Error: %@", error);
        }];
    }
}

-(void)parseCreateAccount:(id)responseObj{
    
    
    DLog(@"responseObj %@", responseObj);
    NSDictionary *responseData=responseObj;
    NSDictionary *response=[responseData objectForKey:@"response"];
    NSInteger responseCode=[[response objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[response objectForKey:RESPONSETEXT];
    DLog(@"responseCode %ld", (long)responseCode);
    DLog(@"responsestr %@", responseText);
    
    
    if (responseCode == 10000) {
        
        [self showAlertResponse:responseText];
    }
    
    else if(responseCode == 10002)
    {
        [self showAlert:responseText];
    }
    
    else if(responseCode == 10004)
    {
        [self showAlert:responseText];
    }

        
    else{
        [UtilityManager showAlert:responseText msg:responseText];
        return;
    }
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     if (self.formItems.count > indexPath.row) {
         TextFieldElement *item = [self.formItems objectAtIndex:indexPath.row];
    
         if ([item.label containsString:@"Home"]) {
             return VARIABLE_HEIGHT(70);
         }
     }
    
    return VARIABLE_HEIGHT(40);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.formItems.count + 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CreateEditAccountTableViewCell *cell = (CreateEditAccountTableViewCell*)[tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
    
    if(cell == nil)
    {
        cell = [[CreateEditAccountTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
        
        [cellArray insertObject:cell atIndex:indexPath.row];
        
    }
    
    if (indexPath.row > self.formItems.count - 1) {
        if (indexPath.row == self.formItems.count)
        {
            submitButton  = [UIButton buttonWithType:UIButtonTypeCustom];
            submitButton.frame = CGRectMake(VARIABLE_WIDTH(5) ,VARIABLE_HEIGHT(5), SCREEN_WIDTH - 2*VARIABLE_WIDTH(5), VARIABLE_HEIGHT(40));
            submitButton.tag = 1;
            submitButton.layer.cornerRadius = 5.0f;
            submitButton.titleLabel.font=[UIFont boldSystemFontOfSize: (IPAD ? 16.0 : 14.0)];
            submitButton.backgroundColor = [UIColor convertToHexString:@"#AAA8AA"];
            [submitButton setTitle:@"Submit" forState:UIControlStateNormal];
            [submitButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [submitButton setAccessibilityLabel:@"Submit"];
            [submitButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
            submitButton.showsTouchWhenHighlighted=YES;
            [cell.contentView addSubview:submitButton];
        }
        else{
            
            cancelButton  = [UIButton buttonWithType:UIButtonTypeCustom];
            cancelButton.frame = CGRectMake(VARIABLE_WIDTH(5) ,VARIABLE_HEIGHT(10), SCREEN_WIDTH - 2*VARIABLE_WIDTH(5), VARIABLE_HEIGHT(40));
            cancelButton.tag = 2;
            cancelButton.layer.cornerRadius = 5.0f;
            cancelButton.titleLabel.font=[UIFont boldSystemFontOfSize: (IPAD ? 16.0 : 14.0)];
            cancelButton.backgroundColor = [UIColor convertToHexString:@"#AAA8AA"];
            [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
            [cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [cancelButton setAccessibilityLabel:@"Cancel"];
            [cancelButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
            cancelButton.showsTouchWhenHighlighted=YES;

            [cell.contentView addSubview:cancelButton];
        }
    }
    else{
        TextFieldElement *item = [self.formItems objectAtIndex:indexPath.row];
        
        cell.labelField.frame = CGRectMake(VARIABLE_WIDTH(5), VARIABLE_HEIGHT(5), VARIABLE_WIDTH(120), VARIABLE_HEIGHT(30));
        cell.labelField.font = [UIFont boldSystemFontOfSize:fontSize];
        cell.valueViewField.font=[UIFont systemFontOfSize:fontSize];
        cell.valueField.font=[UIFont systemFontOfSize:fontSize];
        
        if ([item.label isEqualToString:@"*Password:"] || [item.label isEqualToString:@"*Confirm Password:"] )
        {
            cell.valueField.secureTextEntry = YES;
        }
        if ([item.label containsString:@"Home"]) {
            
            
            cell.valueViewField.frame = CGRectMake(cell.labelField.frame.origin.x + cell.labelField.frame.size.width + VARIABLE_WIDTH(10), VARIABLE_HEIGHT(5), VARIABLE_WIDTH(165), VARIABLE_HEIGHT(60));
            cell.valueViewField.delegate = self;
        }
        else{
            
            if (![item.label containsString:@"State"])
            {
                cell.valueField.frame = CGRectMake(cell.labelField.frame.origin.x + cell.labelField.frame.size.width + VARIABLE_WIDTH(10), VARIABLE_HEIGHT(5), VARIABLE_WIDTH(165), VARIABLE_HEIGHT(30));
                if ([item.label containsString:@"City"])
                    cell.valueField.text= cell.valueField.text.length ? cell.valueField.text : @"Addison";
            }
            
        }
        
        
        if ([item.label isEqualToString:@"*Phone:"] || [item.label isEqualToString:@"Zip/Postal Code:"])
        {
            if ([item.label isEqualToString:@"Zip/Postal Code:"]) {
                cell.valueField.tag = 101;
                cell.valueField.text= cell.valueField.text.length ? cell.valueField.text : @"75001";
            }
            else{
                cell.valueField.tag = 100;
            }
            cell.valueField.keyboardType = UIKeyboardTypeNumberPad;
            numberToolbar.frame = CGRectMake(0, 0, 320, 50);
            numberToolbar.barStyle = UIBarStyleBlackTranslucent;
            numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                                    [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                    [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
            [numberToolbar sizeToFit];
            
            cell.valueField.inputAccessoryView = numberToolbar;
            
        }
        if ([item.label containsString:@"State"]) {
            
            
            pickerBtn.frame = CGRectMake(cell.labelField.frame.origin.x + cell.labelField.frame.size.width  + VARIABLE_WIDTH(10), cell.labelField.frame.origin.y + VARIABLE_HEIGHT(3) , VARIABLE_WIDTH(165), VARIABLE_HEIGHT(27));
            
            if (IPAD) {
                [pickerBtn setBackgroundImage:[UIImage imageNamed:@"DropDownIconBigIpad.png"] forState:UIControlStateNormal];
            }
            else
            {
                [pickerBtn setBackgroundImage:[UIImage imageNamed:@"DropDownIconBig.png"] forState:UIControlStateNormal];
            }
            [pickerBtn setTitle:pickerStr forState:UIControlStateNormal];
            [pickerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            pickerBtn.titleLabel.font = [UIFont systemFontOfSize:14];
            [pickerBtn addTarget:self action:@selector(showStatePicker) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:pickerBtn];
            
        }
        if (self.isEditScreen) {
            
            if ([item.label isEqualToString:@"Email Address:"])
            {
                cell.valueField.layer.borderColor = [UIColor clearColor].CGColor;
                cell.valueField.font = [UIFont systemFontOfSize:fontSize];
                [cell.valueField setEnabled:NO];
            }
        }
        
        if([item.label hasPrefix:@"*"]){
            cell.labelField.attributedText = [self getTagColor:item.label];
        }
        else
            cell.labelField.text = item.label;
        
        
        cell.valueField.delegate = self;
        
        if (savedData.count > 0 && self.isEditScreen) {
            if (indexPath.row == 5) {
                
                cell.valueField.text = cell.valueField.text.length ? cell.valueField.text : [self getFormattedPhoneNumer:[savedData objectAtIndex:indexPath.row]];
            }
            else
                cell.valueField.text = cell.valueField.text.length ? cell.valueField.text : [savedData objectAtIndex:indexPath.row];
        }
    }
    
   
    return cell;
}


-(NSMutableAttributedString*) getTagColor : (NSString*) text
{
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:text];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
    
    return string;
}


-(void) showAlert : (NSString*) title
{
    [UtilityManager showAlert:nil msg:title];
}
-(void) showAlertResponse : (NSString*) title
{
    UIAlertController * alert;
    
    alert=[UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action){
                                 [self popBackToPreviousPage];
                             }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void) dismissKeyBoard
{
    if (isKeyBoradOpen) {
        isKeyBoradOpen = false;
        
        for (int i = 0; i< self.formItems.count; i++) {
            CreateEditAccountTableViewCell *cell = [cellArray objectAtIndex:i];
            [cell.valueField resignFirstResponder];
        }
    
    }
    
}

#pragma mark TextViewmethods
-(BOOL)textView:(UITextView *)textView1 shouldChangeTextInRange:
(NSRange)range replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]) {
        
        [self hidePickerView];
        [textView1 resignFirstResponder];
        isKeyBoradOpen = false;
        
    }
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    NSLog(@"Did begin editing");
    
    [self hidePickerView];
    
    if (!isKeyBoradOpen) {
        isKeyBoradOpen = true;
     
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    BOOL result = YES;
    
    
    if (textField.text.length>=5 && textField.tag == 101 && string.length != 0) {
        return NO;
    }
    
    if (string.length != 0 && textField.tag == 100) {
        NSMutableString *text = [NSMutableString stringWithString:[[textField.text componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""]];
       
        if (text.length > 3)
            [text insertString:@"-" atIndex:3];
        
        if (text.length > 7)
            [text insertString:@"-" atIndex:7];
        
        if (text.length > 11)
            text = [NSMutableString stringWithString:[text substringToIndex:11]];
        
      
        textField.text = text;
       
    }
    
    return result;
}

-(void)textViewDidChange:(UITextView *)textView{
    
    
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    
    
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    if (isKeyBoradOpen) {
        isKeyBoradOpen = false;
       
    }
    
    [self hidePickerView];
    
    
    return YES;
}

#pragma mark TextField methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self hidePickerView];
    if (!isKeyBoradOpen) {
        
        isKeyBoradOpen = true;
      
    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    isKeyBoradOpen = false;
    [self hidePickerView];
   
    
    return YES;
}


#pragma mark Picker view methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [pickerArrData count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [pickerArrData objectAtIndex:row];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel *labelPick = [[UILabel alloc] init];
    if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
        if (IOS7){
            labelPick.frame = CGRectMake(20, 0, 220, 30);
        }
        
        else
            labelPick.frame = CGRectMake(0, 0, 200, 30);
    }
    else{
        labelPick.frame = CGRectMake(0, 0, 220, 50);
    }
    
    
    [labelPick setBackgroundColor:[UIColor clearColor]];
    if (IPAD) {
        [labelPick setFont:[UIFont boldSystemFontOfSize:18]];
    }
    else
        [labelPick setFont:[UIFont boldSystemFontOfSize:16]];
    
    
    labelPick.text = [pickerArrData objectAtIndex:row];
    
    return labelPick ;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    pickerStr =[[pickerArrData objectAtIndex:row] copy];
    
}


- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 30.0;
}


-(void)showStatePicker{
    
    submitButton.hidden = true;
    cancelButton.hidden = true;
    if(!statePicker.superview){
    [self dismissKeyBoard];
    if (statePicker == nil) {
        if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
            statePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT/2+75, self.view.frame.size.width, 150)];
        }
        else
            statePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT/2+150, self.view.frame.size.width, 300)];
    }
    
    [self.view addSubview:statePicker];
    [statePicker setDataSource: self];
    [statePicker setDelegate: self];
    statePicker.hidden = NO;
    statePicker.showsSelectionIndicator = YES;
    statePicker.backgroundColor = [UIColor whiteColor];
    doneView = [[UIView alloc]initWithFrame:CGRectMake(0, statePicker.frame.origin.y-30, self.view.frame.size.width, 30)];
    doneView.backgroundColor = [UIColor lightGrayColor];
    UIButton *doneBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 0, 50, 30)];
    doneBtn.backgroundColor = [UIColor clearColor];
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    [doneBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(doneOnPicker) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn setTintColor:[UIColor blackColor]];
    [doneView addSubview:doneBtn];
    UIButton *cancelPickBtn = [[UIButton alloc]initWithFrame:CGRectMake(doneView.frame.size.width-70, 0, 60, 30)];
    cancelPickBtn.backgroundColor = [UIColor clearColor];
    [cancelPickBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelPickBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelPickBtn addTarget:self action:@selector(cancelOnPicker) forControlEvents:UIControlEventTouchUpInside];
    [cancelPickBtn setTintColor:[UIColor blackColor]];
    [doneView addSubview:cancelPickBtn];
    [self.view addSubview:doneView];
    }
}

-(void)doneOnPicker{
    submitButton.hidden = false;
    cancelButton.hidden = false;
    statePicker.hidden = YES;
    doneView.hidden = YES;
    if (pickerStr == nil) {
        [pickerBtn setTitle:@"TX" forState:UIControlStateNormal];
    }
    else
    {
        [pickerBtn setTitle:pickerStr forState:UIControlStateNormal];
    }
    [statePicker removeFromSuperview];
    [doneView removeFromSuperview];
    
}

-(void)cancelOnPicker{
    submitButton.hidden = false;
    cancelButton.hidden = false;
    statePicker.hidden = YES;
    doneView.hidden = YES;
    pickerStr = @"TX";
    [pickerBtn setTitle:@"TX" forState:UIControlStateNormal];
    [statePicker removeFromSuperview];
    [doneView removeFromSuperview];
    
    
}
-(void) hidePickerView
{
    submitButton.hidden = false;
    cancelButton.hidden = false;
    
    if (statePicker.hidden == NO) {
        statePicker.hidden = YES;
        doneView.hidden = YES;
    }
    
}

-(void)bottomButtonPressed:(id)sender
{
    [defaults setBool:NO forKey:@"ViewMore"];
    [defaults setBool:YES forKey:BottomButton];
    
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    //NSlog(@"The tag is %d",tag);
    bottomButtonDO *obj_bottomBottomDO = [_arrayBottomButtonID  objectAtIndex:tag];
    
    [defaults setValue:obj_bottomBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_bottomBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_bottomBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_bottomBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_bottomBottomDO.btnLinkTypeName);
    if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            ////[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    
    else{ //other menu types
        switch ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
               // [hotDeals release];
                break;
                
            case 1://Scan Now
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                ////[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                
                [self navigateToFindView];
            }
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
            {
                
            }
                
                break;
                
            case 8://Visitors Bureau
                
                break;
                
            case 9://transportation
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
            }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                ////[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                   [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    ////[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=FALSE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:obj_bottomBottomDO.btnLinkID];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                   // CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];;
                   // //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                   // //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    ////[filterList release];
                    
                    //[self request_GetPartners];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
               // [dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
               // //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                ////[iFundraiserListViewController release];
                
            }break;
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                   [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!")  msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                      //  [inform release];
                    }
                    
                }
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                           // [citPref release];
                        }
                    }
                }
                
            }
            break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
               // privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
            default:
                break;
        }
    }
    
}

//-(void)requestToGetUserData
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
//    
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
//    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//    ReleaseAndNilify(xmlStr);
//}
-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}



-(void)parseGetUserData:(id)response
{
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}


-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
        isRemember = YES;
        
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:retailerListScreen animated:NO];
           // //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:retailerListScreen animated:NO];
               // //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
               // //[locationDetailsScreen release];
            }
        }
    }
}



-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    ////[iCouponsViewController release];
}


-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
   // //[iMainMenuViewController release];
}

// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case PARTNERRET:
            [self parse_PartnerRet:response];
            break;
            //        case gETPARTNER:
            //            [self parse_GetPartners:response];
            //            break;
            
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
            
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
            
        default:
            break;
            
            
            
    }
    
}

-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    ////[iEventsListViewController release];;
}

//Call Find Service and Class
-(void)navigateToFindView
{
    
    //    [defaults setValue:@"0" forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    
    navigatedFromMainMenu = FALSE;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
   // //[mmvc release];
}
-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }

    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
    [self.navigationController popToRootViewControllerAnimated:NO];
    }
    
}
-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}


-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=FALSE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    else if ([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}


#pragma mark navigation barButtonItem actions
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
        
    }
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}

-(void) showActionSheet {
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}




-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
          //  //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            ////[pvc release];
        }
    }
}

//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    ////[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                             actionWithTitle:@"Sign up"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action){
                                                 [alert dismissViewControllerAnimated:NO completion:nil];
                                        [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                             }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                   
                    
                    
                   
                   // //[alert release];
                    
                    //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                ////[urlDetail release];
            }
        }
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            ////[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
           // //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        ////[rsvc release];
    }
}

-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }
}


@end

//
//  GQSortAndFilterViewController.m
//  HubCiti
//
//  Created by Ashika on 10/1/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "GQSortAndFilterViewController.h"
#import "HubCitiConstants.h"


@interface GQSortAndFilterViewController (){
    long catSubCheck;
    NSInteger catPosition;
    NSInteger subcatPosition;
        float fontSize;
    CustomizedNavController *cusNav;
}


@end

@implementation GQSortAndFilterViewController
@synthesize sortAlphabet,sortObj,catPickerText,subcatPickerText,categoryArray;

- (void)viewDidLoad {
    [super viewDidLoad];
   // int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    sortAndFilterTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, VARIABLE_HEIGHT(250)
                                                                      ) style:UITableViewStylePlain];
    sortAndFilterTable.dataSource=self;
    sortAndFilterTable.delegate=self;
    [sortAndFilterTable setBackgroundColor:[UIColor whiteColor]];
    
    
    [self.view addSubview:sortAndFilterTable];
    cusNav =(CustomizedNavController *) self.navigationController;
   
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    // Do any additional setup after loading the view from its nib.
    UIButton *button=[UtilityManager customizeBarButtonItem:@"Done"];
    [button addTarget:self action:@selector(doneButtonTouch:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mainPage=[[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem=mainPage;
    //[mainPage release];
    
    UIButton *btncancel = [UtilityManager customizeBarButtonItem:NSLocalizedString(@"Cancel",@"Cancel")];
    [btncancel addTarget:self action:@selector(cancelButtonTouch:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* btncancelbar = [[UIBarButtonItem alloc] initWithCustomView:btncancel];
    self.navigationItem.rightBarButtonItem = btncancelbar;
    //[btncancelbar release];
    
    self.navigationItem.title = @"Filter & Sort";
    
    arrSorting = [[NSMutableArray alloc]initWithObjects:@"Name", nil];
    arrFilters = [[NSMutableArray alloc]initWithObjects:@"Category",@"SubCategory",nil];
    
    sortAlphabet = sortObj.alphabetFindInfoGovQA;
    catPickerText = sortObj.categoryGovQA;
    subcatPickerText = sortObj.subCategoryGovQA;
    
    catButton=[[UIButton alloc] init];
    subButton=[[UIButton alloc] init];
    
    if (IPAD) {
        fontSize = 16;
    }
    else
    {
        fontSize = 12;
    }
    
    


}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    catButton.enabled=NO;
    if(!catPickerText)
   catPickerText=@"All Items";
    if(!subcatPickerText)
    subcatPickerText=@"All Items";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    NSDictionary *parameters = @{@"authKey":@"ifF34jauK;"};
  
     NSString *baseURL=[NSString stringWithFormat:@"%@govqa/filtera",BASE_URL];
    [manager POST:baseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
       
        NSDictionary *responseData=responseObject;
        
        NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
 
       if(responseCode==10000)
        {
            catButton.enabled=YES;
           // categoryDo* categoryObj = [[categoryDo alloc]init];
            categoryArray=[[responseData objectForKey:@"categories"] copy];
            NSLog(@"response: %@",categoryArray);
            NSLog(@"count: %lu", (unsigned long)[categoryArray count]);
            if([categoryArray count]>0)
            NSLog(@"des:%@",[[categoryArray objectAtIndex:0] objectForKey:@"description"]);
        }

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    
        NSLog(@"Error: %@", error);
    }];

   // [sortAndFilterTable reloadData];
    
}

-(void)doneButtonTouch:(id)sender{
    [defaults setValue:@"YES" forKey:@"isComingFromSorting"];
    if (!sortObj) {
        sortObj =[[SortAndFilter alloc]init];
    }
    sortObj.alphabetFindInfoGovQA = sortAlphabet;
    sortObj.categoryGovQA=catPickerText;
    sortObj.subCategoryGovQA=subcatPickerText;
    NSLog(@"value:%@",sortObj.categoryGovQA);
    [self saveCustomObject:sortObj key:@"SortObjectFind"];
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)saveCustomObject:(SortAndFilter *)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
    
}


-(void)cancelButtonTouch:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        if(indexPath.section==1){
            if(indexPath.row==0||indexPath.row==1)
       {
            return 88;
            }
            else
               return 44;
        }
        else
        return 44.0;
    }
    else
    {
        if(indexPath.section==1){
            if(indexPath.row==0||indexPath.row==1)
            {
                return 140;
            }
            else
                return 70;
        }
        else
        return 70.0;
    }
    
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 26.0;
    }
    else
    {
        return 36.0;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
        return [arrSorting count];
    else if(section==1)
        return [arrFilters count];
    
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *lblCatName ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 18)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
    }
    else
    {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 28)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
    }
    if(section==0)
        [lblCatName setText:@" Sort Items by"];
    else
        [lblCatName setText:@" Filter Items by"];
    
    [lblCatName setTextColor:[UIColor blackColor]];
    [lblCatName setBackgroundColor:[UIColor grayColor]];
    return lblCatName;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellSubMenuListView";
    
    UITableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor =  [UIColor clearColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }
        
    }
    
    if(indexPath.section == 0)
    {
        UILabel *lblEvtDate;
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 20)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        }
        else
        {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 30)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
        }
        [lblEvtDate setText:[arrSorting objectAtIndex:indexPath.row]];
        [lblEvtDate setTextColor:[UIColor blackColor]];
        [lblEvtDate setBackgroundColor:[UIColor clearColor]];
        
        [cell.contentView addSubview:lblEvtDate];
        //[lblEvtDate release];
        
        if(indexPath.row == 0){
            if(sortAlphabet == YES){
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else{
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        
    }
    else
    {
        
        UILabel *lblEvtDate;
        
       
        CGRect submitFrame = CGRectMake(VARIABLE_WIDTH(10), VARIABLE_HEIGHT(30) , SCREEN_WIDTH - VARIABLE_WIDTH(20), VARIABLE_HEIGHT(30));//+ (IPAD ? VARIABLE_HEIGHT(25) : VARIABLE_HEIGHT(0))
        if(indexPath.row==0)
        {
        [self setButton: catButton rowIndex:indexPath.row cgFrame:submitFrame];
            [cell.contentView addSubview:catButton];
            
        }
        else
        {
            [self setButton: subButton rowIndex:indexPath.row cgFrame:submitFrame];
            [cell.contentView addSubview:subButton];
        }

        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 20)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        }
        else
        {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 30)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
        }
        
        [lblEvtDate setText:[arrFilters objectAtIndex:indexPath.row]];
        [lblEvtDate setTextColor:[UIColor blackColor]];
        [lblEvtDate setBackgroundColor:[UIColor clearColor]];
        //[lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        [cell.contentView addSubview:lblEvtDate];
        
      //  [lblEvtDate release];
    

    }
    return cell;
}
-(void) setButton: (UIButton *)subbutton rowIndex:(long )row cgFrame:(CGRect)frame{
    subbutton.frame=frame;
    subbutton.backgroundColor=[UIColor convertToHexString:@"#bababa"];
    if(row==0)
    [subbutton setTitle:catPickerText forState:UIControlStateNormal];
    else
     [subbutton setTitle:subcatPickerText forState:UIControlStateNormal];
    
    subbutton.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    [subbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    subbutton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [subbutton setImage:[UIImage imageNamed:@"down_arrowblue"] forState:UIControlStateNormal];
    subbutton.imageEdgeInsets=UIEdgeInsetsMake(0, subbutton.frame.size.width-30, 0, 0);
    subbutton.tag=row;
    [subbutton addTarget:self action:@selector(categoryClicked:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void) categoryClicked : (id) sender{
    UIButton *clicked = (UIButton *) sender;
    catSubCheck=(long)clicked.tag;
    NSLog(@"%ld",(long)clicked.tag);
    NSLog(@"count: %lu", (unsigned long)[categoryArray count]);
    NSLog(@"des:%@",[[categoryArray objectAtIndex:0] objectForKey:@"description"]);
    
    if(catSubCheck==1){
        
        
        if(![catPickerText isEqualToString:@"All Items"]){
            
            if(!catSubPicker.superview)
            {
                
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                
                
                [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
                NSDictionary *parameters = @{@"authKey":@"ifF34jauK;",@"parentID":[[categoryArray objectAtIndex:[defaults integerForKey:@"catPosition"]] objectForKey:@"filterID"]};
                
                NSString *baseURL=[NSString stringWithFormat:@"%@govqa/filterb",BASE_URL];
                
                NSLog(@"para:%@",parameters);
                
                [manager POST:baseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    NSLog(@"JSON: %@", responseObject);
                    
                    NSDictionary *responseData=responseObject;
                    
                    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
                    
                    if(responseCode==10000)
                    {
                        // categoryDo* categoryObj = [[categoryDo alloc]init];
                        subCatArray=[[responseData objectForKey:@"categories"] copy];
                        NSLog(@"response: %@",subCatArray);
                        NSLog(@"count: %lu", (unsigned long)[subCatArray count]);
                        if([subCatArray count]>0)
                            NSLog(@"des:%@",[[subCatArray objectAtIndex:0] objectForKey:@"description"]);
                      if(!catSubPicker.superview){

                        [self showcatSubPicker];
                      }
                    }
                    
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    NSLog(@"Error: %@", error);
                }];
                
            }
        }
    }
    else
    {
        if(!catSubPicker.superview){
      

        [self showcatSubPicker];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *tCell=[tableView cellForRowAtIndexPath:indexPath];
   
    // Set value to show checkmark in table row
    if(indexPath.section==0){
        if(tCell.accessoryType == UITableViewCellAccessoryCheckmark){
            tCell.accessoryType=UITableViewCellAccessoryNone;
            sortAlphabet = NO;
            
        }
        else{
            tCell.accessoryType=UITableViewCellAccessoryCheckmark;

            sortAlphabet = YES;
        }
    }
}

#pragma mark Picker view methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(catSubCheck==0)
        return [categoryArray count]+1;
    
    else if(catSubCheck==1)
    {
        if([subCatArray count]>0){
            return [subCatArray count]+1;

        }
        else
            return 1;

    }
            else
        return 1;
    //return 0;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel *labelPick = [[UILabel alloc] init];
    if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
        if (IOS7){
            labelPick.frame = CGRectMake(20, 0, 220, 30);
        }
        
        else
            labelPick.frame = CGRectMake(0, 0, 200, 30);
    }
    else{
        labelPick.frame = CGRectMake(0, 0, 220, 50);
    }
    
    
    [labelPick setBackgroundColor:[UIColor clearColor]];
    if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
        [labelPick setFont:[UIFont boldSystemFontOfSize:12]];
    }
    else
        [labelPick setFont:[UIFont boldSystemFontOfSize:15]];
    if(row==0){
        labelPick.text=@"All Items";
    }
    else{
    if(catSubCheck==0)
    {
        if([categoryArray count]>0)
        {
            labelPick.text = [[categoryArray objectAtIndex:row-1] objectForKey:@"description"];
        }
        else
        {
            labelPick.text=@"All Items";
        }

    }
    else if(catSubCheck==1)
    {
        if([subCatArray count]>0)
        {
            
            labelPick.text = [[subCatArray objectAtIndex:row-1] objectForKey:@"description"];

        }
        else
        {
            labelPick.text=@"All Items";
        }

    }
    }
    NSLog(@"des:%@",[[categoryArray objectAtIndex:0] objectForKey:@"description"]);
    return labelPick;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(row==0){
        
        if(catSubCheck==0)
        {

        catPickerText=@"All Items";
        }
        if(catSubCheck==1){
        subcatPickerText=@"All Items";
        }
    }
    else{
        
    if(catSubCheck==0)
    {
        if([categoryArray count]>0)
        {
        catPosition=row-1;
            [defaults setInteger:catPosition forKey:@"catPosition"];
        catPickerText =[[categoryArray objectAtIndex:row-1] objectForKey:@"description"];
        }
        else
        {
            catPickerText=@"All Items";
        }
    }
    else if(catSubCheck==1)
    {
        if([subCatArray count]>0)
        {
            subcatPosition=row-1;
           [defaults setInteger:subcatPosition forKey:@"subcatPosition"];
        subcatPickerText =[[subCatArray objectAtIndex:row-1] objectForKey:@"description"];
        }
        else
        {
            subcatPickerText=@"All Items";
        }
    }
    }
    
}


- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 30.0;
}


-(void)showcatSubPicker{
    
    if (catSubPicker == nil) {
        if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
            catSubPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT/2+75, self.view.frame.size.width, 150)];
        }
        else
            catSubPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT/2+150, self.view.frame.size.width, 300)];
    }
    
    [self.view addSubview:catSubPicker];
    [catSubPicker setDataSource: self];
    [catSubPicker setDelegate: self];
    catSubPicker.hidden = NO;
    catSubPicker.showsSelectionIndicator = YES;
    catSubPicker.backgroundColor = [UIColor whiteColor];
    doneView = [[UIView alloc]initWithFrame:CGRectMake(0, catSubPicker.frame.origin.y-30, self.view.frame.size.width, 30)];
    doneView.backgroundColor = [UIColor lightGrayColor];
    UIButton *doneBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 0, 50, 30)];
    doneBtn.backgroundColor = [UIColor clearColor];
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    [doneBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(doneOnPicker) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn setTintColor:[UIColor blackColor]];
    [doneView addSubview:doneBtn];
    UIButton *cancelPickBtn = [[UIButton alloc]initWithFrame:CGRectMake(doneView.frame.size.width-70, 0, 60, 30)];
    cancelPickBtn.backgroundColor = [UIColor clearColor];
    [cancelPickBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelPickBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelPickBtn addTarget:self action:@selector(cancelOnPicker) forControlEvents:UIControlEventTouchUpInside];
    [cancelPickBtn setTintColor:[UIColor blackColor]];
    [doneView addSubview:cancelPickBtn];
    [self.view addSubview:doneView];
    [catSubPicker selectRow:0 inComponent:0 animated:NO];
    [catSubPicker selectRow:0 inComponent:0 animated:NO];
//    if(catSubCheck==0 && ![catButton.titleLabel.text isEqualToString:@"All Items"])
//    {
//        
//    [catSubPicker selectRow:[defaults integerForKey:@"catPosition"]+1 inComponent:0 animated:NO];
//    }
//    else if(catSubCheck==1)
//    {
//        if(![subButton.titleLabel.text isEqualToString:@"All Items"])
//      [catSubPicker selectRow:[defaults integerForKey:@"subcatPosition"]+1 inComponent:0 animated:NO];
//        else if([subButton.titleLabel.text isEqualToString:@"All Items"])
//            [catSubPicker selectRow:0 inComponent:0 animated:NO];
//
//    }
}

-(void)doneOnPicker{
    
    
     [catSubPicker removeFromSuperview];
    [doneView removeFromSuperview];
          if(catSubCheck==0)
        {
            if([categoryArray count]>0){
        [catButton setTitle:catPickerText forState:UIControlStateNormal];
            }else
            {
                catPickerText=@"All Items";
            }
            [subButton setTitle:@"All Items" forState:UIControlStateNormal];
            subcatPickerText=@"All Items";
        }
        else
        {
            if([subCatArray count]>0){
                [subButton setTitle:subcatPickerText forState:UIControlStateNormal];
            }else
            {
                subcatPickerText=@"All Items";
            }

        }
   
    
    
}

-(void)cancelOnPicker{
    [catSubPicker removeFromSuperview];
    [doneView removeFromSuperview];
    if(catSubCheck==0)
    {
//        catPickerText=@"All Items";
//        [catButton setTitle:catPickerText forState:UIControlStateNormal];
//        if(![subButton.titleLabel.text isEqualToString:@"All Items"])
//        {
//            subcatPickerText=@"All Items";
//            [subButton setTitle:subcatPickerText forState:UIControlStateNormal];
//
//        }
    }
    else if(catSubCheck==1)
    {
//        subcatPickerText=@"All Items";
//        [subButton setTitle:subcatPickerText forState:UIControlStateNormal];
    }
    
    
}
-(void) hidePickerView
{
    if (catSubPicker.hidden == NO) {
        catSubPicker.hidden = YES;
        doneView.hidden = YES;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  EventPackageDetailsViewController.h
//  HubCiti
//
//  Created by ionnor on 11/26/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventPackageDetailsViewController : UIViewController<CustomizedNavControllerDelegate>
{
    NSString *strPkgDes;
    NSString *strPkgName;
    UITextView *txtPkgDesc;
}
@property(nonatomic,strong) NSString *strPkgName;
@property(nonatomic,strong) NSString *strPkgDes;
@end

//
//  EventsListViewController.m
//  HubCiti
//
//  Created by ionnor on 11/22/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "EventsListViewController.h"
#import "EventsCategoryDO.h"
#import "EventDetailsDO.h"
#import "EventListDetailViewController.h"
#import "EventGroupingAndSortingViewController.h"
#import "bottomButtonView.h"
#import "bottomButtonDO.h"
#import "HubCitiConstants.h"
#import "SettingsViewController.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "CityExperienceViewController.h"
#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "FindViewController.h"
#import "AnythingPage.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "AppDelegate.h"
#import "SingleCatRetailers.h"
//#import "ScanNowScreen.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FAQCategoryList.h"
#import "FilterListViewController.h"
#import "FundraiserListViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import <QuartzCore/QuartzCore.h>
#import "SpecialOffersViewController.h"
#import "HTTPClient.h"
#import "GetUserInfoResponse.h"
#import "EventListResponse.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponsInDealViewController.h"

NSDictionary *viewDictionary;

@interface EventsListViewController ()<HTTPClientDelegate>


@property(nonatomic,strong) EventListResponse* eventResponse;
@end

@implementation EventsListViewController{
    UIActivityIndicatorView *loading;
    bottomButtonView *view;
}
//@synthesize categoryName,anyVC,retailId,retailLocationId,isRetailerEvent,fundId,sortFilObj,iSwipeViewController;
@synthesize infoResponse;
@synthesize categoryName,anyVC,retailId,retailLocationId,isRetailerEvent,fundId,sortFilObj,iSwipeViewController,emailSendingVC;
@synthesize eventResponse;
@synthesize bandEvent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - Service Respone methods
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
   
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    DLog(@"parameter: %@",responseObject);
    
    [self responseData:responseObject];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    [self.view setAccessibilityLabel:@"EventsPage"];
    loading = nil;
    arrEventsCategoryInfo = [[NSMutableArray alloc]init];
    
//    UISwipeGestureRecognizer *mSwipeUpRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
//    [mSwipeUpRecognizer setDirection: UISwipeGestureRecognizerDirectionLeft];
//    [self.view addGestureRecognizer:mSwipeUpRecognizer];
    
//    UISwipeGestureRecognizer *mSwipeUpRecognizerRht = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
//    [mSwipeUpRecognizerRht setDirection: UISwipeGestureRecognizerDirectionRight];
//    [self.view addGestureRecognizer:mSwipeUpRecognizerRht];
    
    // Default value for Grouping and Sorting
    
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
//    UIButton *btn = [UtilityManager customizeBarButtonItem:@"Filter"];
//    [btn addTarget:self action:@selector(sortEvents:) forControlEvents:UIControlEventTouchUpInside];
    
    // iPad conversion
//    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
//    {
//        btn.frame = CGRectMake(0, 0, 60, backBtn.frame.size.height);
//    }
//    
//    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
//    self.navigationItem.rightBarButtonItem = mainPage;
//    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"Filter"];
//    //[mainPage release];
    
    //home button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
    
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    

    
    sortFilObj =  [[SortAndFilter alloc]init];
    sortFilObj.sortEventDateSelected = YES;
    [defaults setValue:nil forKey:@"eventsMitemId"];
    [self Request_eventlistWithObj:sortFilObj];
   
    
    // Do any additional setup after loading the view from its nib.
}




-(void)setTitleView
{
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideBackButton:NO];
    [cusNav hideHambergerButton:NO];
    [cusNav setTitle:@"Events" forView:self withHambergur:YES];
   // self.navigationItem.title= @"Events";
    UIView *titleView = [[UIView alloc]init];
    
    UILabel *lblTitle = [[UILabel alloc]init];
    //    if (categoryName.length > 9){
    //        categoryName = [[categoryName substringToIndex:8] stringByAppendingString:@"..."];
    //    }
    //    [lblTitle setText:[NSString stringWithFormat:@"%@",@"Events"]];
    //    lblTitle.font =[UIFont boldSystemFontOfSize:16];
    //    lblTitle.numberOfLines=1;
    //    lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
    //    lblTitle.textColor = [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]];
    //    lblTitle.backgroundColor = [UIColor clearColor];
    //    lblTitle.textAlignment = NSTextAlignmentCenter;
    //    [titleView addSubview:lblTitle];
    [cusNav setTitle:@"Events" forView:self withHambergur:YES];

    //self.navigationItem.title= @"Events";
    // place a clickable button on top of everything
    
//    UIButton *backBtn = [UtilityManager customizeBackButton];
//    //    self.navigationItem.title = @"Events";
//    
//    [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    //self.navigationItem.leftBarButtonItem = back;
//    self.navigationItem.hidesBackButton = YES;
//    
//   
//    self.navigationItem.leftBarButtonItem = back;
    //self.navigationItem.titleView = titleView;
    
    
    // iPad conversion
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        //titleView.frame = CGRectMake(-120, 0, 200, 40);
        titleView.frame = CGRectMake(0, 0, 768, 44);
        lblTitle.frame = CGRectMake(270, 0, 80, 40);
        //[button setFrame:CGRectMake(60, 6, 100, 30)];
    }
    else
    {
        titleView.frame = CGRectMake(0, 0, 200, 40);
        lblTitle.frame = CGRectMake(105, 0, 80, 40);
       // [button setFrame:CGRectMake(0, 6, 100, 30)];
    }
    //[titleView release];
    
    
    
}
-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}
-(void)clickDistanceButton:(id)sender
{
    
    sortFilObj.distanceSelected = YES;
    sortFilObj.sortEventDateSelected = NO;
    sortFilObj.alphabeticallySelected = NO;
    rowCountOfTotalCells = 0;
    [arrEventsCategoryInfo removeAllObjects];
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
    [self Request_eventlistWithObj:sortFilObj];
    [tblEventList reloadData];
    
}
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];

    [self setTitleView];
    cusNav = (CustomizedNavController*) self.navigationController;
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
        if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
        
        ReleaseAndNilify(tblEventList);
        rowCountOfTotalCells = 0;
        [arrEventsCategoryInfo removeAllObjects];
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        sortFilObj = [[SortAndFilter alloc]initWithObject:[self loadCustomObjectWithKey:@"SortFilterObject"]];
        DLog(@"%d", sortFilObj.distanceSelected);
        [self Request_eventlistWithObj:sortFilObj];
        [defaults setValue:nil forKey:@"SortFilterObject"];
        
    }
    
}

-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}
- (SortAndFilter *)loadCustomObjectWithKey:(NSString *)key {
    
    NSData *encodedObject = [defaults objectForKey:key];
    SortAndFilter *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}

- (void)addItemViewController:(SwipesViewController *)controller didFinishEnteringItem:(SortAndFilter *)item
{
    if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
        ReleaseAndNilify(tblEventList);
        sortFilObj = [[SortAndFilter alloc]initWithObject:item];
        rowCountOfTotalCells = 0;
        [arrEventsCategoryInfo removeAllObjects];
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        [self Request_eventlistWithObj:item];
        
        
    }
}


-(void)popBackToPreviousPage
{
    [defaults setBool:NO forKey:@"ViewMore"];
    if (SingleCatFlag==TRUE) {
        if ([SingleEventCatID count]) {
            [SingleEventCatID removeLastObject];
            // [SingleEventCatID removeLastObject];
        }
    }
    
    else if (MultipleCatFlag==TRUE) {
        if ([MultipleEventCatID count]) {
            [MultipleEventCatID removeLastObject];
        }
    }
   [EventsBottomButtonID removeLastObject];
    [self.navigationController popViewControllerAnimated:NO];
    //    allEvents=FALSE;
}

-(void)sortEvents:(id)sender
{
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
//    EventGroupingAndSortingViewController *iEventGroupingAndSortingViewController = [[EventGroupingAndSortingViewController alloc]initWithNibName:@"EventGroupingAndSortingViewController" bundle:[NSBundle mainBundle]];
//    if(isRetailerEvent == TRUE)
//    {
//    iEventGroupingAndSortingViewController.isRetailerEvent = TRUE;
//    }
//    else if(fundId){
//        iEventGroupingAndSortingViewController.fundraiserId = fundId;
//    }
//    iEventGroupingAndSortingViewController.retailId = retailId ;
//    iEventGroupingAndSortingViewController.retailLocationId = retailLocationId ;
    
//    if(iSwipeViewController)
//        [iSwipeViewController release];
    
    iSwipeViewController = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
    iSwipeViewController.module = @"Events";//Find All , Find Single , Events, CitiEXP
    iSwipeViewController.delegate = self;
    if(isRetailerEvent == TRUE)
    {
        iSwipeViewController.isRetailerEvent = TRUE;
    }
    else if(fundId){
        iSwipeViewController.fundraiserId = fundId;
    }
    iSwipeViewController.retailId = retailId ;
    iSwipeViewController.retailLocationId = retailLocationId ;
    
    iSwipeViewController.sortFilObj = [[SortAndFilter alloc]initWithObject:sortFilObj];
    
    
    [self.navigationController pushViewController:iSwipeViewController animated:NO];
  //  [iSwipeViewController release];
}

-(void)settableViewOnScreen
{
    
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    if (!tblEventList) {
    if(bottomBtn==1){
        tblEventList = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-50) style:UITableViewStylePlain];
    }
    else{
        tblEventList = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-yVal) style:UITableViewStylePlain];
        }
    }
    tblEventList.dataSource=self;
    tblEventList.delegate=self;
    [tblEventList setBackgroundColor:[UIColor whiteColor]];
        [tblEventList setAccessibilityIdentifier:@"eventsList"];
    tblEventList.tableFooterView.hidden=YES;
    [self.view addSubview:tblEventList];
    
}



-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }

    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;

        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
    [self.navigationController popToRootViewControllerAnimated:NO];
    }
}



-(void)bottomButtonPressed:(id)sender
{
    
    [defaults setBool:NO forKey:@"ViewMore"];
    [defaults setBool:YES forKey:BottomButton];
    //    [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    UIButton *btn = (UIButton*)sender;
    int tag = (int) btn.tag;
    
    bottomButtonDO *obj_eventBottomDO = [arrEventBottomButtonDO objectAtIndex:tag];
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=27||[[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=28){
         [defaults setValue:obj_eventBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    }
    if ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_eventBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_eventBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_eventBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_eventBottomDO.btnLinkTypeName);
    if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];

            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    else if ([obj_eventBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_eventBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_eventBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_eventBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_eventBottomDO.bottomBtnID];
            }
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            self.iEventsListViewController = iEventsListViewController;
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
          //  //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    else{ //other menu types
        
        switch ([[obj_eventBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_eventBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                self.hotDeals = hotDeals;
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
              //  [hotDeals release];
                break;
                
            case 1://Scan Now
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
                //[self navigateToEventList];
                break;
            case 4://Whats NearBy
            {
                [NearbyBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];

                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:obj_eventBottomDO.bottomBtnID];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                [self navigateToFindView];
            }
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];

                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                self.cityExp = citi;
                [self.navigationController pushViewController:citi animated:NO];
            }
                //[citi release];
                break;
                
            case 7://City Services
                
                break;
                
            case 8://Visitors Bureau
                
                break;
                
            case 9://transportation
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];}
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
                
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=TRUE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_eventBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                

                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                

                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                   
                }
                
            else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                
                CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                self.cityExp = cevc;
                [SharedManager setRefreshAustinRetailers:YES];
                //[cevc release];

                
                FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                self.filters = filters;
                // [defaults setValue:retAffName forKey:@"Title"];
                [self.navigationController pushViewController:filters animated:NO];
                //[filters release];
            }
                
            else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                
                FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                self.filterList = filterList;
                // [defaults setValue:retAffName forKey:@"Title"];
                [self.navigationController pushViewController:filterList animated:NO];
                //[filterList release];
                
                //[self request_GetPartners];
            }
        }
            break;
               
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                //[dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                self.faqList = faqCatList;
                //[faqCatList release];
                
            }
                break;
            
            case 21:
                
            {
                [FundraiserBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];

                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }break;
             
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{

                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }
                break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{

                iWebRequestState = GETFAVLOCATIONS;
                [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{

                if([Network currentReachabilityStatus]==0)
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    
                }
                else{
                    
                    UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:inform animated:NO];
                   // [inform release];
                }
                
                }
                
            }
                break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{

                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                       
                    }
                    else{
                        [SharedManager setUserInfoFromSignup:NO];
                        CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                        [self.navigationController pushViewController:citPref animated:NO];
                        //[citPref release];
                    }
                }
            }
                
                break;
            
            case 27:
            case 28://Sort/Filter
            {
                [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
                
                [defaults setValue:[defaults valueForKey:@"eventsMitemId"] forKey:KEY_MITEMID];
                
               SwipesViewController *iSwipeViewController1 = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                iSwipeViewController1.module = @"Events";//Find All , Find Single , Events, CitiEXP
                iSwipeViewController1.delegate = self;
                if(isRetailerEvent == TRUE)
                {
                    iSwipeViewController1.isRetailerEvent = TRUE;
                }
                else if(fundId){
                    iSwipeViewController1.fundraiserId = fundId;
                }
                iSwipeViewController1.retailId = retailId ;
                iSwipeViewController1.retailLocationId = retailLocationId ;
                
                iSwipeViewController1.sortFilObj = [[SortAndFilter alloc]initWithObject:sortFilObj];
                
                
                [self.navigationController pushViewController:iSwipeViewController1 animated:NO];
                //[iSwipeViewController1 release];
                 //[EventsBottomButtonID addObject:obj_eventBottomDO.bottomBtnID];
            }
            break;
            case 29:
                {
                    CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:dvc animated:NO];
                }
                break;
            case 30:
                {
                    AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                    privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                  //  privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                    [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                    privacy.comingFromPrivacyScreen = YES;
                    [self.navigationController pushViewController:privacy animated:NO];
                }
                break;
            default:
                break;
        }
    }
    //NSlog(@"%d",tag);
   }
}


// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    // Calculate Y position for View
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
        
    }
    
    for(int btnLoop= 0; btnLoop < [arrEventBottomButtonDO count]; btnLoop++)
    {
        bottomButtonDO *obj_eventBottomDO = [arrEventBottomButtonDO objectAtIndex:btnLoop];
        
        view = [[bottomButtonView alloc]init];
        
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
                view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, (SCREEN_HEIGHT-yVal-bottomBarButtonHeight) , 80,bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrEventBottomButtonDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrEventBottomButtonDO count],bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_eventBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        //view.tag=99;
        [self.view bringSubviewToFront:view];
    }
}




#pragma mark Request Methods


-(void)Request_eventlistWithObj:(SortAndFilter *)sortFilOb
{
    iWebRequestState = EVENTSLIST;
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    [parameters setValue:@"ios" forKey:@"platform"];
    if([defaults valueForKey:KEY_MAINMENUID])
        [parameters setValue:[defaults valueForKey: KEY_MAINMENUID] forKey:@"mainMenuId"];
    
    if(isRetailerEvent == TRUE)
    {
        [parameters setValue:retailId forKey:@"retailId"];
        [parameters setValue:retailLocationId forKey:@"retailLocationId"];
        
    }
    else if(fundId){
        [parameters setValue:fundId forKey:@"fundId"];
    }
    else{
        if (SingleCatFlag==TRUE && SingleEventFromMainMenu==TRUE){
            if ([defaults valueForKey:KEY_MITEMID]) {
                [defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"eventsMitemId"];
                [parameters setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
            }
        }
        else if (SingleCatFlag==TRUE){
            if ([SingleEventCatID count]!=0) {
                [parameters setValue:[SingleEventCatID objectAtIndex:[SingleEventCatID count]-1]  forKey:@"bottomBtnId"];
            }
        }
        
        else if (MultipleCatFlag==TRUE){
            if ([MultipleEventCatID count]!=0)
            {
                [parameters setValue:[MultipleEventCatID objectAtIndex:[MultipleEventCatID count]-1] forKey:@"bottomBtnId"];
            }
            else{
                [parameters setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
            }
        }
        
        else if([defaults valueForKey:KEY_MITEMID]){
            [defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"eventsMitemId"];
            [parameters setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
        }
        
        else {
            if ([EventsBottomButtonID count]>=1)
                [parameters setValue:[EventsBottomButtonID objectAtIndex:[EventsBottomButtonID count]-1] forKey:@"bottomBtnId"];
            
            else if([defaults valueForKey:KEY_BOTTOMBUTTONID])
                [parameters setValue:[defaults valueForKey:KEY_BOTTOMBUTTONID] forKey:@"bottomBtnId"];
        }
    }
    
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length] && ([[HubCitiManager sharedManager]gps_allow_flag] == YES) &&([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse))
    {
        [parameters setValue:[defaults valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        [parameters setValue:[defaults valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        [parameters setValue:[defaults valueForKey:KEYZIPCODE] forKey:@"postalCode"];
    }
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    [parameters setValue:rowCountOfTotalCells forKey:@"lowerLimit"];
    
    if (sortFilOb.distanceSelected==TRUE) {
        [parameters setValue:@"distance" forKey:@"sortBy"];
    }
    
    
    if (sortFilOb.sortEventDateSelected==TRUE) {
        [parameters setValue:@"date" forKey:@"sortBy"];
    }
    
    if (sortFilOb.alphabeticallySelected==TRUE) {
        [parameters setValue:@"atoz" forKey:@"sortBy"];
    }
    
    if (sortFilOb.selectedCitiIds) {
        [parameters setValue:sortFilOb.selectedCitiIds forKey:@"cityIds"];
    }
    
    
    if (sortFilOb.eventDateSelected==TRUE && [sortFilOb.evtDate length]) {
        [parameters setValue:sortFilOb.evtDate forKey:@"evtDate"];
    }
    
    if (sortFilOb.selectedInterestsIds) {
        [parameters setValue:sortFilOb.selectedInterestsIds forKey:@"interests"];
    }
    
    
    if (sortFilOb.selectedCatIds) {
        [parameters setValue:sortFilOb.selectedCatIds forKey:@"catIds"];
    }
    
    
    
    if ([RegionApp isEqualToString:@"1"]) {
        [parameters setValue:@"1" forKey:@"isRegApp"];
    }
    
    else{
        [parameters setValue:@"0" forKey:@"isRegApp"];
    }
    
    if([bandEvent intValue]==1)
    [parameters setValue:@"Band" forKey:@"listType"];
    
    
//    {
//        "userId":"1",
//        "hubCitiId":"82",
//        "lowerLimit":"0",
//        "listType":"Band"
//    }
    
    
//    [parameters setValue:@"1" forKey:@"userId"];
//    [parameters setValue:@"82" forKey:@"hubCitiId"];
//    [parameters setValue:@"0" forKey:@"lowerLimit"];
//    [parameters setValue:@"Event" forKey:@"listType"];
    
    DLog(@"parameter: %@",parameters);
    NSString *urlString = [NSString stringWithFormat:@"%@alertevent/eventlist",BASE_URL];
    //  NSString *urlString = [NSString stringWithFormat:@"http://sdw2107:8080/HubCiti2.5/alertevent/eventlist"];
    DLog(@"Url: %@",urlString);
    if ([defaults boolForKey:@"ViewMore"])
    {
        NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
        id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        [HubCitiAppDelegate removeActivityIndicator];
        [self parse_eventList:responseData];
    }
    else{
        DLog(@"parameter: %@",parameters);
        
        HTTPClient *client = [HTTPClient sharedHTTPClient];
        client.delegate = self;
        DLog(@"Url: %@",urlString);
        [client sendRequest : parameters : urlString];
        [HubCitiAppDelegate showActivityIndicator];
    }
    
    
    /*
   iWebRequestState = EVENTSLIST;
    
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<MenuItem><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<platform>%@</platform>",@"ios"];
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if(isRetailerEvent == TRUE)
    {
        [requestStr appendFormat:@"<retailId>%@</retailId><retailLocationId>%@</retailLocationId>",retailId,retailLocationId];
    }
    
    else if(fundId){
        [requestStr appendFormat:@"<fundId>%@</fundId>",fundId];
    }
    
    else{
        if (SingleCatFlag==TRUE && SingleEventFromMainMenu==TRUE){
            if ([defaults valueForKey:KEY_MITEMID]) {
                [defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"eventsMitemId"];
                [requestStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];
            }
        }
        
        else if (SingleCatFlag==TRUE){
            if ([SingleEventCatID count]!=0) {
                [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[SingleEventCatID objectAtIndex:[SingleEventCatID count]-1]];
            }
        }
        else if (MultipleCatFlag==TRUE){
            if ([MultipleEventCatID count]!=0)
            {
                [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[MultipleEventCatID objectAtIndex:[MultipleEventCatID count]-1]];
            }
            else{
                [requestStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];
            }
        }
        else if([defaults valueForKey:KEY_MITEMID]){
             [defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"eventsMitemId"];
            [requestStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];
        }
        
        else {
            if ([EventsBottomButtonID count]>=1)
                [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[EventsBottomButtonID objectAtIndex:[EventsBottomButtonID count]-1]];

            else if([defaults valueForKey:KEY_BOTTOMBUTTONID])
                [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
    }
    
    
    if ([[defaults  valueForKey:KEY_LATITUDE]length] && [[defaults valueForKey:KEY_LONGITUDE]length] && ([[HubCitiManager sharedManager]gps_allow_flag] == YES) &&([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorized))
    {
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    [requestStr appendFormat:@"<lowerLimit>%d</lowerLimit>",rowCountOfTotalCells];
    
    if (sortFilOb.distanceSelected==TRUE) {
        [requestStr appendFormat:@"<sortBy>distance</sortBy>"];
    }
    
    
    if (sortFilOb.sortEventDateSelected==TRUE) {
        [requestStr appendFormat:@"<sortBy>date</sortBy>"];
    }
    
    if (sortFilOb.alphabeticallySelected==TRUE) {
        [requestStr appendFormat:@"<sortBy>atoz</sortBy>"];
    }
    
    if (sortFilOb.selectedCitiIds) {
        [requestStr appendFormat:@"<cityIds>%@</cityIds>",sortFilOb.selectedCitiIds];
    }
    
    
    if (sortFilOb.eventDateSelected==TRUE && [sortFilOb.evtDate length]) {
        [requestStr appendFormat:@"<evtDate>%@</evtDate>",sortFilOb.evtDate];
    }
    
    if (sortFilOb.selectedInterestsIds) {
        [requestStr appendFormat:@"<interests>%@</interests>",sortFilOb.selectedInterestsIds];
    }
    
    
    if (sortFilOb.selectedCatIds) {
        [requestStr appendFormat:@"<catIds>%@</catIds>",sortFilOb.selectedCatIds];
    }
    
    
    
    if ([RegionApp isEqualToString:@"1"]) {
        [requestStr appendFormat:@"<isRegApp>1</isRegApp>"];
    }
    
    else{
        [requestStr appendFormat:@"<isRegApp>0</isRegApp>"];
    }
    

    [requestStr appendFormat:@"</MenuItem>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@alertevent/eventlist",BASE_URL];
    
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
        
        
        [self parse_eventList:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    }

    
    //[ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    //[requestStr release];*/
    
}




-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=TRUE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if (![defaults boolForKey:BottomButton]){
        
        if([defaults valueForKey:KEY_MITEMID])
            [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else{
        if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if(![defaults boolForKey:BottomButton]){
        if ([defaults valueForKey:KEY_MITEMID])
            [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else {
        
        if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

//-(void)request_GetUserCat{
//    
//    iWebRequestState = GETFAVCATEGORIES;
//    if([Network currentReachabilityStatus]==0)
//    {
//        [common showAlert];
//    }
//    else {
//        
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//        
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//        
//    }
//}


-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}


//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if(![defaults boolForKey:BottomButton]){
        if ([defaults valueForKey:KEY_MITEMID])
            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else {
        
        if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}



#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        return;
    
    [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    [self showActionSheet];
	
	
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
       
        
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..")  msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
        
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}

-(void) showActionSheet {
  
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
       
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
            
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
	[self dismissViewControllerAnimated:YES completion:nil];
    
}








#pragma mark parse methods
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
       case EVENTSLIST:
            [self parse_eventList:response];
//            rowCountOfTotalCells = 0;
            [tblEventList reloadData];
        break;
            
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
        case CITIEXPRET:
            
            [self parse_CitiExpRet:response];
            
            break;
        case PARTNERRET:
            [self parse_PartnerRet:response];
            break;
//        case GETPARTNERS:
//            [self parse_GetPartners:response];
//            break;
            
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
            
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
//        case catsearch:
//            [self parse_sscatsearch:response];
//            break;
        default:
            break;
    }
}

-(void)parse_eventList:(id)responseString
{
  
    if (responseString == nil)
        return;
    if (eventResponse == nil) {
        eventResponse = [[EventListResponse alloc] init];
    }
    [eventResponse setValuesForKeysWithDictionary:responseString];
    if([eventResponse.responseCode isEqualToString:@"10000"])
    {
        if(eventResponse.mainMenuId)
        {
            [defaults setValue:eventResponse.mainMenuId forKey:KEY_MAINMENUID];
        }
        
        if(eventResponse.nextPage)
        {
            isNextPageAvailable = [eventResponse.nextPage boolValue];
        }
        if(eventResponse.maxRowNum)
        {
            rowCountOfTotalCells= eventResponse.maxRowNum;
        }
        bottomBtn = [eventResponse.bottomBtn intValue];
        if(eventResponse.categoryList)
        {
            
            
            //CategoryList *iEventsCategoryDOrepted = [[CategoryList alloc]init];
            
            for(int i = 0;i < eventResponse.categoryList.count; i++)
            {
                
                BOOL isCatRepeted = NO;
                arrEventBottomButtonDO =[[NSMutableArray alloc]init];
                CategoryList * iEventsCategoryDO = [[CategoryList alloc]init];
                
                iEventsCategoryDO.catObjArray = [[NSMutableArray alloc] init];
                
                
                NSDictionary* dictList = eventResponse.categoryList[i];
                [iEventsCategoryDO setValuesForKeysWithDictionary:dictList];
                if([[defaults valueForKey:@"GroupEventBy"]isEqualToString:@"type"])
                {
                    // Check if Last Object of Category and Incoming First Category both are same then append data
                    if([arrEventsCategoryInfo count] > 0)
                    {
                        
                        CategoryList *iEventsCategoryDOrepted ;
                        
                        for(int i=0; i< [arrEventsCategoryInfo count]; i++)
                        {
                            
                            iEventsCategoryDOrepted = [arrEventsCategoryInfo objectAtIndex:i];
                            isCatRepeted = NO;
                            if(iEventsCategoryDOrepted.categoryId == iEventsCategoryDO.categoryId)
                            {
                                isCatRepeted = YES;
                                
                                [iEventsCategoryDO.catObjArray addObjectsFromArray:iEventsCategoryDOrepted.catObjArray];
                                
                                [arrEventsCategoryInfo replaceObjectAtIndex:i withObject:iEventsCategoryDO];
                                break;
                            }
                            
                        }
                        if(isCatRepeted==NO)
                        {
                            [arrEventsCategoryInfo addObject:iEventsCategoryDO];
                        }
                        
                    }
                    
                    else
                    {
                        [arrEventsCategoryInfo addObject:iEventsCategoryDO];
                        
                    }
                }
                else
                {
                    // Check if Last Object of Category and Incoming First Category both are same then append data
                    if([arrEventsCategoryInfo count] > 0)
                    {
                        CategoryList *iEventsCategoryDOrepted ;
                        
                        for(int i=0;i<[arrEventsCategoryInfo count]; i++)
                        {
                            iEventsCategoryDOrepted = [arrEventsCategoryInfo objectAtIndex:i];
                            isCatRepeted=NO;
                            if([iEventsCategoryDOrepted.groupContent isEqualToString:iEventsCategoryDO.groupContent])
                            {
                                isCatRepeted = YES;
                                
                                [iEventsCategoryDO.catObjArray addObjectsFromArray:iEventsCategoryDOrepted.catObjArray];
                                
                                [arrEventsCategoryInfo replaceObjectAtIndex:i withObject:iEventsCategoryDO];
                                
                                break;
                                
                            }
                        }
                        if(isCatRepeted==NO)
                        {
                            [arrEventsCategoryInfo addObject:iEventsCategoryDO];
                        }
                        
                    }
                    else
                    {
                        [arrEventsCategoryInfo addObject:iEventsCategoryDO];
                        
                    }
                    
                }
                
                                if(iEventsCategoryDO.eventList)
                {
                    for(int i=0; i<[iEventsCategoryDO.eventList count]; i++)
                    {
                        EventList *iEventDetailsDO = [[EventList alloc]init];
                        
                        
                        NSDictionary* evntListDict = iEventsCategoryDO.eventList[i];
                        [iEventDetailsDO setValuesForKeysWithDictionary:evntListDict];
                        
                        if(iEventDetailsDO.isOnGoing)
                        {
                            if(iEventDetailsDO.isOnGoing == [NSNumber numberWithInt:1] )//isOnGoing is NSNumber
                            {
                                onGoingEventRows=onGoingEventRows+1;
                            }
                            
                        }
                        [iEventsCategoryDO.catObjArray addObject:iEventDetailsDO];
                        //[iEventDetailsDO release];
                    }
                    
                }
                
            }
            
        }
        
        if (bottomBtn==1) {
            
            for (int i=0;i<eventResponse.bottomBtnList.count;i++)
            {
                bottomButtonDO *obj_eventBottomDO= [[bottomButtonDO alloc]init];
                NSDictionary* dictList = eventResponse.bottomBtnList[i];
                [obj_eventBottomDO setValuesForKeysWithDictionary:dictList];
                if(obj_eventBottomDO.bottomBtnName)
                {
                    
                }
                else
                {
                    obj_eventBottomDO.bottomBtnName = @" ";
                }
                
                if(obj_eventBottomDO.bottomBtnImg)
                {
                    
                }
                else
                {
                    obj_eventBottomDO.bottomBtnImg = @" ";
                }
                
                if(obj_eventBottomDO.bottomBtnID)
                {
                    
                }
                
                else
                {
                    obj_eventBottomDO.bottomBtnID = @" ";
                }
                
                
                
                
                
                if(obj_eventBottomDO.bottomBtnImgOff)
                {
                    
                }
                
                else
                {
                    obj_eventBottomDO.bottomBtnImgOff = @" ";
                }
                if(obj_eventBottomDO.btnLinkID){
                    if(obj_eventBottomDO.btnLinkTypeName){
                        if ([obj_eventBottomDO.btnLinkTypeName isEqualToString:@"Filters"]) //for filters
                            [SharedManager setRetGroupId:[obj_eventBottomDO.btnLinkID intValue]];
                    }
                }
                
                
                if(obj_eventBottomDO.position)
                {
                    
                }
                
                else
                {
                    obj_eventBottomDO.position = @" ";
                }
                
                [arrEventBottomButtonDO addObject:obj_eventBottomDO];
                //[obj_eventBottomDO release];
            }
            if([arrEventBottomButtonDO count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                [self setBottomBarMenu];
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
        }
        
        [self settableViewOnScreen];
    }
    
    else if([eventResponse.responseCode isEqualToString:@"10005"])
    {
        
        if(eventResponse.mainMenuId)
        {
            [defaults setValue:eventResponse.mainMenuId forKey:KEY_MAINMENUID];
        }
        
        if(eventResponse.nextPage)
        {
            isNextPageAvailable = [eventResponse.nextPage boolValue];
        }
        bottomBtn = [eventResponse.bottomBtn intValue] ;
        
        if (bottomBtn==1) {
            
            if(eventResponse.bottomBtnList)
            {
                
                arrEventBottomButtonDO =[[NSMutableArray alloc]init];
                for (int i=0;i<eventResponse.bottomBtnList.count;i++)
                {
                    bottomButtonDO *obj_eventBottomDO = [[bottomButtonDO alloc]init];
                    NSDictionary* dictList = eventResponse.bottomBtnList[i];
                    [obj_eventBottomDO setValuesForKeysWithDictionary:dictList];
                    
                    if(obj_eventBottomDO.bottomBtnName)
                    {
                        
                    }
                    else
                    {
                        obj_eventBottomDO.bottomBtnName = @" ";
                    }
                    if(obj_eventBottomDO.bottomBtnImg)
                    {
                        
                    }
                    else
                    {
                        obj_eventBottomDO.bottomBtnImg = @" ";
                    }
                    
                    if(obj_eventBottomDO.bottomBtnImgOff)
                    {
                        
                    }
                    
                    else
                    {
                        obj_eventBottomDO.bottomBtnImgOff = @" ";
                    }
                    
                    if(obj_eventBottomDO.btnLinkID){
                        if(obj_eventBottomDO.btnLinkTypeName){
                            if ([obj_eventBottomDO.btnLinkTypeName isEqualToString:@"Filters"]) //for filters
                                [SharedManager setRetGroupId:[obj_eventBottomDO.btnLinkID intValue]];
                        }
                    }
                    [arrEventBottomButtonDO addObject:obj_eventBottomDO];
                }
                
                if([arrEventBottomButtonDO count] > 0)
                {
                    // Set Bottom Menu Button after read from response data
                    [self setBottomBarMenu];
                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                    
                }
                else
                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            }
        }
        
        NSString *responseTextStr = eventResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
        [self settableViewOnScreen];
    }
    
    else
    {
        NSString *responseTextStr = eventResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        [loading stopAnimating];
        ReleaseAndNilify(loading);
    }
    
    [defaults setBool:NO forKey:@"ViewMore"];

   /* if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
		return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbxml.rootXMLElement];
        if(nextPageElement)
            isNextPageAvailable = [[TBXML textForElement:nextPageElement]boolValue];
        
        TBXMLElement *maxRowNumElement = [TBXML childElementNamed:@"maxRowNum" parentElement:tbxml.rootXMLElement];
        if(maxRowNumElement)
            rowCountOfTotalCells =  [[TBXML textForElement:maxRowNumElement]intValue];
        
        TBXMLElement *categoryListElement = [TBXML childElementNamed:@"categoryList" parentElement:tbxml.rootXMLElement];
        
        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbxml.rootXMLElement]]intValue];
        
//        rowCountOfTotalCells = rowCountOfTotalCells+ (int)[iEventsCategoryDO.catObjArray count];
        
        if(categoryListElement)
        {
            TBXMLElement *CategoryInfoElement = [TBXML childElementNamed:@"CategoryInfo" parentElement:categoryListElement];
            while (CategoryInfoElement)
            {
                BOOL isCatRepeted = NO;
                arrEventBottomButtonDO =[[NSMutableArray alloc]init];
                TBXMLElement *categoryIdElement = [TBXML childElementNamed:@"categoryId" parentElement:CategoryInfoElement];
                TBXMLElement *groupContentElement = [TBXML childElementNamed:@"groupContent" parentElement:CategoryInfoElement];
                
                EventsCategoryDO *iEventsCategoryDO = [[EventsCategoryDO alloc]init];
                
                iEventsCategoryDO.catObjArray = [[NSMutableArray alloc]init];
                
                if(categoryIdElement)
                    iEventsCategoryDO.categoryId = [TBXML textForElement:categoryIdElement];
                if(groupContentElement)
                    iEventsCategoryDO.categoryName = [TBXML textForElement:groupContentElement];
                
                if([[defaults valueForKey:@"GroupEventBy"]isEqualToString:@"type"])
                {
                    // Check if Last Object of Category and Incoming First Category both are same then append data
                    if([arrEventsCategoryInfo count] > 0)
                    {
                        EventsCategoryDO *iEventsCategoryDOrepted = [[EventsCategoryDO alloc]init];
                        
                        for(int i=0; i<[arrEventsCategoryInfo count]; i++)
                        {
                            iEventsCategoryDOrepted = [arrEventsCategoryInfo objectAtIndex:i];
                            isCatRepeted = NO;
                            if([iEventsCategoryDOrepted.categoryId isEqualToString:iEventsCategoryDO.categoryId])
                            {
                                isCatRepeted = YES;
                                
                                [iEventsCategoryDO.catObjArray addObjectsFromArray:iEventsCategoryDOrepted.catObjArray];
                                
                                [arrEventsCategoryInfo replaceObjectAtIndex:i withObject:iEventsCategoryDO];
                                break;
                            }
                            
                        }
                        if(isCatRepeted==NO)
                        {
                            [arrEventsCategoryInfo addObject:iEventsCategoryDO];
                        }
                        
                    }
                    else
                    {
                        [arrEventsCategoryInfo addObject:iEventsCategoryDO];
                        
                    }
                }
                else
                {
                    // Check if Last Object of Category and Incoming First Category both are same then append data
                    if([arrEventsCategoryInfo count] > 0)
                    {
                        EventsCategoryDO *iEventsCategoryDOrepted = [[EventsCategoryDO alloc]init];
                        
                        for(int i=0;i<[arrEventsCategoryInfo count]; i++)
                        {
                            iEventsCategoryDOrepted = [arrEventsCategoryInfo objectAtIndex:i];
                            isCatRepeted=NO;
                            if([iEventsCategoryDOrepted.categoryName isEqualToString:iEventsCategoryDO.categoryName])
                            {
                                isCatRepeted = YES;
                                
                                [iEventsCategoryDO.catObjArray addObjectsFromArray:iEventsCategoryDOrepted.catObjArray];
                                
                                [arrEventsCategoryInfo replaceObjectAtIndex:i withObject:iEventsCategoryDO];
                                
                                break;
                                
                            }
                        }
                        if(isCatRepeted==NO)
                        {
                            [arrEventsCategoryInfo addObject:iEventsCategoryDO];
                        }
                        
                    }
                    else
                    {
                        [arrEventsCategoryInfo addObject:iEventsCategoryDO];
                        
                    }
                    
                }
                
                TBXMLElement *eventListElement = [TBXML childElementNamed:@"eventList" parentElement:CategoryInfoElement];
                
                if(eventListElement)
                {
                    TBXMLElement *EventDetailsElement = [TBXML childElementNamed:@"EventDetails" parentElement:eventListElement];
                    
                    while (EventDetailsElement)
                    {
                        EventDetailsDO *iEventDetailsDO = [[EventDetailsDO alloc]init];
                        
                        TBXMLElement *eventIdElement = [TBXML childElementNamed:@"eventId" parentElement:EventDetailsElement];
                        TBXMLElement *eventNameElement = [TBXML childElementNamed:@"eventName" parentElement:EventDetailsElement];
                        
                        TBXMLElement *imgPathElement = [TBXML childElementNamed:@"imgPath" parentElement:EventDetailsElement];
                        
                        TBXMLElement *startDateElement = [TBXML childElementNamed:@"startDate" parentElement:EventDetailsElement];
                        
                        TBXMLElement *startTimeElement = [TBXML childElementNamed:@"startTime" parentElement:EventDetailsElement];
                        
                        TBXMLElement *mItemExistElement = [TBXML childElementNamed:@"mItemExist" parentElement:EventDetailsElement];
                        TBXMLElement *eventListIdElement = [TBXML childElementNamed:@"eventListId" parentElement:EventDetailsElement];
                        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:EventDetailsElement];
                        TBXMLElement *isOnGoingElement=[TBXML childElementNamed:@"isOnGoing" parentElement:EventDetailsElement];
                        
                        
                        if(eventIdElement)
                            iEventDetailsDO.eventId = [TBXML textForElement:eventIdElement];
                        
                        if(eventNameElement)
                            iEventDetailsDO.eventName = [TBXML textForElement:eventNameElement];
                        
                        
                        if(imgPathElement)
                            iEventDetailsDO.imgPath = [TBXML textForElement:imgPathElement];
                        
                        
                        if(startDateElement)
                            iEventDetailsDO.startDate = [TBXML textForElement:startDateElement];
                        
                        
                        if(startTimeElement)
                            iEventDetailsDO.startTime = [TBXML textForElement:startTimeElement];
                        
                        
                        if(mItemExistElement)
                            iEventDetailsDO.mItemExist = [TBXML textForElement:mItemExistElement];
                        
                        if(eventListIdElement)
                            iEventDetailsDO.eventListId = [TBXML textForElement:eventListIdElement];
                        
                        if(distanceElement)
                            iEventDetailsDO.distance = [TBXML textForElement:distanceElement];
                        
                        if(isOnGoingElement)
                        {
                            iEventDetailsDO.isOnGoing = [TBXML textForElement:isOnGoingElement];
                            if([iEventDetailsDO.isOnGoing isEqualToString:@"1"])
                                {
                                    onGoingEventRows=onGoingEventRows+1;
                                }

                        }
                         EventDetailsElement = [TBXML nextSiblingNamed:@"EventDetails" searchFromElement:EventDetailsElement];
                        
                        
                        [iEventsCategoryDO.catObjArray addObject:iEventDetailsDO];
                        //[iEventDetailsDO release];
                    }
                    
                }
                //[iEventsCategoryDO release];
                
                CategoryInfoElement = [TBXML nextSiblingNamed:@"CategoryInfo" searchFromElement:CategoryInfoElement];
            }
            
        }
        
        if (bottomBtn==1) {
            
        TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbxml.rootXMLElement];
        TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
        while(BottomButtonElement)
        {
            
            bottomButtonDO *obj_eventBottomDO = [[bottomButtonDO alloc]init];
            
            
            TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
            TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
            TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
            TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
            TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
            TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
            TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
            TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
            
            
            if(bottomBtnIDElement)
                obj_eventBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
            
            if(bottomBtnNameElement)
                obj_eventBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
            else
                obj_eventBottomDO.bottomBtnName = @" ";
            
            if(bottomBtnImgElement)
                obj_eventBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
            else
                obj_eventBottomDO.bottomBtnImg = @" ";
            
            if(bottomBtnImgOffElement)
                obj_eventBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
            else
                obj_eventBottomDO.bottomBtnImgOff = @" ";
            
            if(btnLinkTypeIDElement)
                obj_eventBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
            
            if(btnLinkTypeNameElement)
                obj_eventBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
            
            if(btnLinkIDElement){
                obj_eventBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                if(btnLinkTypeNameElement){
                if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
                    [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
                }
            }
            
            if(positionElement)
                obj_eventBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
            
            
            
            BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
            [arrEventBottomButtonDO addObject:obj_eventBottomDO];
            //[obj_eventBottomDO release];
        }
        if([arrEventBottomButtonDO count] > 0)
        {
            // Set Bottom Menu Button after read from response data
            [self setBottomBarMenu];
            [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
            
        }
        else
            [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
        
    }
        [self settableViewOnScreen];
    }
    
    else if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10005"])
    {
        
            TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
            if(mainMenuElement)
                [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
            
            TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbxml.rootXMLElement];
            if(nextPageElement)
                isNextPageAvailable = [[TBXML textForElement:nextPageElement]boolValue];
            bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbxml.rootXMLElement]]intValue];
            
        if (bottomBtn==1) {
            
            TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbxml.rootXMLElement];
        if (bottomButtonListElement)
        {
            arrEventBottomButtonDO =[[NSMutableArray alloc]init];
        
            TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
            while(BottomButtonElement)
            {
                
                bottomButtonDO *obj_eventBottomDO = [[bottomButtonDO alloc]init];
                
                
                TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                
                
                if(bottomBtnIDElement)
                    obj_eventBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                
                if(bottomBtnNameElement)
                    obj_eventBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                else
                    obj_eventBottomDO.bottomBtnName = @" ";
                
                if(bottomBtnImgElement)
                    obj_eventBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                else
                    obj_eventBottomDO.bottomBtnImg = @" ";
                
                if(bottomBtnImgOffElement)
                    obj_eventBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                else
                    obj_eventBottomDO.bottomBtnImgOff = @" ";
                
                if(btnLinkTypeIDElement)
                    obj_eventBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                
                if(btnLinkTypeNameElement)
                    obj_eventBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                
                if(btnLinkIDElement){
                    obj_eventBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                    if(btnLinkTypeNameElement){
                    if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
                        [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
                    }
                }
                
                if(positionElement)
                    obj_eventBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                
                
                
                BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                [arrEventBottomButtonDO addObject:obj_eventBottomDO];
               
            }
            if([arrEventBottomButtonDO count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                [self setBottomBarMenu];
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
        }
        }
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];

        [self settableViewOnScreen];
    }
    
    
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        [loading stopAnimating];
        ReleaseAndNilify(loading);
        
    }
    
    [defaults setBool:NO forKey:@"ViewMore"];*/
    
}



-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
           
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            self.cityExp = cevc;
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            self.filters = pvc;
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}



-(void)parse_CitiExpRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [defaults setObject:nil forKey:@"Response_Austin"];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
           
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Austin"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            self.cityExp = cevc;
            [defaults setInteger:100 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
        }
    }
}

//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
       
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
      
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}

-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}



//-(void) navigateToScanNow
//{
//    [defaults setBool:YES forKey:@"KEY_SCANSPLASH"];
//    [defaults setValue:@"scanNow" forKey:KEY_SEARCHFROM];
//    [defaults setObject:@"YES" forKey:@"KEY_SCANNERCALLED"];
//    
//    int menulevel = [[defaults valueForKey:KEY_MENULEVEL]integerValue];
//    if(menulevel > 1)
//        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
//    else
//        [defaults  setBool:NO forKey:@"DisplayBackBarButton"];
//    
//    
//    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
//    ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];
//    [self.navigationController pushViewController:viewScanNowScreen animated:NO];
//    [viewScanNowScreen release];
//    
//}

    


-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
        
        
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
               
            }
		}
    }
}

//Call Find Service and Class
-(void)navigateToFindView
{
    [defaults setValue:nil forKey:KEY_MITEMID];
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    [defaults setBool:YES forKey:BottomButton];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    navigatedFromMainMenu=false;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];

    [self.navigationController pushViewController:mmvc animated:YES];
    
}

-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    self.iEventsListViewController = iEventsListViewController;
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
   
}


-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    
}




-(void)releaseAllocatedObjects
{
    NSArray *arrSubViews = [self.view subviews];
    for(int i=0; i<[arrSubViews count]; i++)
    {
        UIView *releaseObjView = (UIView*)[arrSubViews objectAtIndex:i];
        if(releaseObjView.tag == 99)
            [releaseObjView removeFromSuperview];
        if([releaseObjView isKindOfClass:[bottomButtonView class]])
        {
            [releaseObjView removeFromSuperview];
        }
    }
    
    if([arrEventBottomButtonDO count]>0)
    {
        [arrEventBottomButtonDO removeAllObjects];
        
        arrEventBottomButtonDO = nil;
    }
    if([arrBottomButtonViewContainer count]>0)
    {
        [arrBottomButtonViewContainer removeAllObjects];
      
        arrBottomButtonViewContainer = nil;
    }
    
}






#pragma mark tableView delegate and datasource methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // iPad conversion
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        return 70.0;
    }
    else
    {
        return 50.0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    // iPad conversion
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        return 30.0;
    }
    else
    {
        return 20.0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (arrEventsCategoryInfo!=0) {
        return [arrEventsCategoryInfo count];
    }
    else{
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (arrEventsCategoryInfo!=0) {
    CategoryList *iEventsCategoryDO = [arrEventsCategoryInfo objectAtIndex:section];
  //  EventsCategoryDO *iEventsCategoryDO = [arrEventsCategoryInfo objectAtIndex:section];
    if(isNextPageAvailable && section == [arrEventsCategoryInfo count]-1)
        return [iEventsCategoryDO.catObjArray count]+1;
    else
        return [iEventsCategoryDO.catObjArray count];
    }
    
    else{
        return 0;
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    float labelFont = 15.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        labelFont = 18.0;
    }
    CategoryList *iEventsCategoryDO = [arrEventsCategoryInfo objectAtIndex:section];
   // EventsCategoryDO *iEventsCategoryDO = [arrEventsCategoryInfo objectAtIndex:section];
    UILabel *lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH - 10, 18)];
    [lblCatName setTextColor:[UIColor blackColor]];
    [lblCatName setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
    [lblCatName setFont:[UIFont boldSystemFontOfSize:labelFont]];
    [lblCatName setText:[NSString stringWithFormat:@" %@",iEventsCategoryDO.groupContent]];
    return lblCatName;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellEventListView";
    static NSString *cellIdentifier1 = @"Cell";
    UITableViewCell *cell;
     CategoryList *iEventsCategoryDO = [arrEventsCategoryInfo objectAtIndex:indexPath.section];
    if(  isNextPageAvailable && indexPath.row == [iEventsCategoryDO.catObjArray count]){
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
            cell.backgroundColor =  [UIColor clearColor];
            
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        else
        {
            NSArray *arr = [cell.contentView subviews];
            for(int i=0; i<[arr count]; i++)
            {
                UIView *views = [arr objectAtIndex:i];
                [views removeFromSuperview];
            }
        }

    }
    else{
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.backgroundColor =  [UIColor clearColor];
        
		cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *views = [arr objectAtIndex:i];
            [views removeFromSuperview];
        }
    }
    }
    
    SdImageView *asyncImageView = nil;
    UILabel *lblDateTimeDistance = nil,*lblEvtName = nil;
    
    
  
   // EventsCategoryDO *iEventsCategoryDO = [arrEventsCategoryInfo objectAtIndex:indexPath.section];
    
    if(isNextPageAvailable && indexPath.row == [iEventsCategoryDO.catObjArray count] )
    {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
    }
    else
    {
        EventList *Evtdetail = [iEventsCategoryDO.catObjArray objectAtIndex:indexPath.row];
        // EventDetailsDO *Evtdetail = [iEventsCategoryDO.catObjArray objectAtIndex:indexPath.row];
        if(Evtdetail.imgPath)
        {
            CGRect frame;
            if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
                frame.origin.x = 5;
                frame.origin.y = 5;
                frame.size.width = 40;
                frame.size.height = 40;
            }
            else
            {
                frame.origin.x = 5;
                frame.origin.y = 5;
                frame.size.width = 60;
                frame.size.height = 60;
            }
            
            asyncImageView = [[SdImageView alloc] initWithFrame:frame] ;
           // asyncImageView = [[SdImageView alloc] init];
            asyncImageView.backgroundColor = [UIColor clearColor];
            asyncImageView.layer.cornerRadius = 5.0f;
            [asyncImageView loadImage:Evtdetail.imgPath];
            
            [cell.contentView addSubview:asyncImageView];
            //[asyncImageView release];
        }
        
        if(Evtdetail.startDate)
        {
            NSMutableString *strDateandTime = [[NSMutableString alloc]initWithString:Evtdetail.startDate];
            
            if(Evtdetail.startTime)
            {
                [strDateandTime appendString:@","];
                [strDateandTime appendString:Evtdetail.startTime];
            }
            if(Evtdetail.distance)
            {
                [strDateandTime appendString:@", "];
                [strDateandTime appendString:Evtdetail.distance];
            }
            
            lblDateTimeDistance = [[UILabel alloc]init];
            [lblDateTimeDistance setText:strDateandTime];
            [lblDateTimeDistance setTextColor:[UIColor blackColor]];
            [lblDateTimeDistance setBackgroundColor:[UIColor clearColor]];
            if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                [lblDateTimeDistance setFont:[UIFont systemFontOfSize:17]];
            else
                [lblDateTimeDistance setFont:[UIFont systemFontOfSize:12]];
            [cell.contentView addSubview:lblDateTimeDistance];
           // [lblDateTimeDistance release];
        }
 
        
        lblEvtName = [[UILabel alloc]init];
        [lblEvtName setText: Evtdetail.eventName];
        lblEvtName.numberOfLines=1;
        [lblEvtName setTextColor:[UIColor blackColor]];
        [lblEvtName setBackgroundColor:[UIColor clearColor]];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [lblEvtName setFont:[UIFont boldSystemFontOfSize:18]];
        else
            [lblEvtName setFont:[UIFont boldSystemFontOfSize:13]];
        [cell.contentView addSubview:lblEvtName];
        //[lblEvtName release];
        
        //            }
        
        asyncImageView.translatesAutoresizingMaskIntoConstraints = NO;
        lblDateTimeDistance.translatesAutoresizingMaskIntoConstraints = NO;
        lblEvtName.translatesAutoresizingMaskIntoConstraints = NO;
        viewDictionary = NSDictionaryOfVariableBindings(asyncImageView, lblDateTimeDistance,lblEvtName);
        
        [self setConstraints:cell];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
     CategoryList *iEventsCategoryDO = [arrEventsCategoryInfo objectAtIndex:indexPath.section];
    
     // EventsCategoryDO *iEventsCategoryDO = [arrEventsCategoryInfo objectAtIndex:indexPath.section];
    if(isNextPageAvailable && indexPath.row == [iEventsCategoryDO.catObjArray count] ) {
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"] && rowCountOfTotalCells !=0) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self Request_eventlistWithObj:sortFilObj];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tblEventList reloadData];
                });
            });
        
        }
    }
    
}



// Called after the user changes the selection.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryList *iEventsCategoryDO = [arrEventsCategoryInfo objectAtIndex:indexPath.section];
   //EventsCategoryDO *iEventsCategoryDO = [arrEventsCategoryInfo objectAtIndex:indexPath.section];
    [defaults setBool:NO forKey:@"ViewMore"];
    
    EventList *Evtdetail = [iEventsCategoryDO.catObjArray  objectAtIndex:indexPath.row];
    // EventDetailsDO *Evtdetail = [iEventsCategoryDO.catObjArray  objectAtIndex:indexPath.row];
    if(Evtdetail.eventId)
        [defaults setValue:Evtdetail.eventId forKey:EVENTID];
    if(Evtdetail.eventListId)
        [defaults setValue:Evtdetail.eventListId forKey:EVENTLISTID];
    
    EventListDetailViewController *iEventListDetailViewController = [[EventListDetailViewController alloc]initWithNibName:@"EventListDetailViewController" bundle:[NSBundle mainBundle]];
    if(Evtdetail.longDes)
        iEventListDetailViewController.strEventDescription = [[NSString alloc]initWithString:Evtdetail.longDes];
    else
        iEventListDetailViewController.strEventDescription = @" ";
    
    if([bandEvent intValue] == 1)
        iEventListDetailViewController.bandEvents = [NSNumber numberWithInt:1];

    
    [self.navigationController pushViewController:iEventListDetailViewController animated:NO];
    
    
    [tblEventList deselectRowAtIndexPath:[tblEventList indexPathForSelectedRow] animated:NO];
}

-(void)setConstraints:(UITableViewCell*)cell
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    int start_X;
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        start_X = 75;
        
        if([viewDictionary objectForKey:@"asyncImageView"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[asyncImageView(60)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(60)]"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblDateTimeDistance"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(33)-[lblDateTimeDistance(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[lblDateTimeDistance(%f)]",start_X,SCREEN_WIDTH-start_X] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblEvtName"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(13)-[lblEvtName(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[lblEvtName(%f)]",start_X,SCREEN_WIDTH-start_X-15] options:0 metrics:0 views:viewDictionary]];
        }
    }
    else
    {
        start_X = 55;
        
        if([viewDictionary objectForKey:@"asyncImageView"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(3)-[asyncImageView(44)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(44)]"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"lblDateTimeDistance"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(23)-[lblDateTimeDistance(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[lblDateTimeDistance(%f)]",start_X,SCREEN_WIDTH-start_X] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"lblEvtName"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(3)-[lblEvtName(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%i)-[lblEvtName(%f)]",start_X,SCREEN_WIDTH-start_X-15] options:0 metrics:0 views:viewDictionary]];
        }

    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//-(void)requestToGetUserData
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
//    
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
//    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//    ReleaseAndNilify(xmlStr);
//}
-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}
-(void)parseGetUserData:(id)response
{
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }
}

@end

//
//  EventsListViewController.h
//  HubCiti
//
//  Created by ionnor on 11/22/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "SwipesViewController.h"
#import "GetUserInfoResponse.h"


@class RetailersListViewController;
@class FilterRetailersList;
@class DealHotDealsList;
@class CityExperienceViewController;
@class FilterListViewController;
@class FundraiserListViewController;
@class FAQCategoryList;
@class AnyViewController;
@class EmailShareViewController;

@interface EventsListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMessageComposeViewControllerDelegate,SwipesViewControllerDelegate,MFMailComposeViewControllerDelegate,CustomizedNavControllerDelegate>
{
    WebRequestState iWebRequestState;
    UITableView *tblEventList;
    NSMutableArray *arrEventsCategoryInfo;
    NSMutableArray *arrEventBottomButtonDO;
    NSMutableArray *arrBottomButtonViewContainer;
    NSString *androidDownloadLink,*retailId,*retailLocationId;;
    BOOL isNextPageAvailable, isRetailerEvent;
//    BOOL allEvents;
    NSNumber *rowCountOfTotalCells;
	CommonUtility *common;
    int onGoingEventRows;
    NSString *categoryName;
    int bottomBtn;
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,retain) FAQCategoryList *faqList;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,retain) EventsListViewController *iEventsListViewController;
@property(nonatomic,retain) FilterListViewController *filterList;
@property(nonatomic,retain) FundraiserListViewController *iFundraiserListViewController;

@property(nonatomic,retain) CityExperienceViewController *cityExp;
@property(nonatomic,retain) FilterRetailersList *filters;
@property(nonatomic,retain) RetailersListViewController* retailerListScreen;
@property(nonatomic,retain) DealHotDealsList* hotDeals;
@property(nonatomic,retain) NSString *categoryName;
@property(nonatomic,retain) NSNumber *bandEvent;
@property(nonatomic,retain) NSString *retailId;
@property(nonatomic,retain) NSString *retailLocationId;
@property (nonatomic,retain) NSString *fundId;
@property(nonatomic) BOOL isRetailerEvent;
@property(nonatomic,retain) SortAndFilter *sortFilObj;
@property(nonatomic,retain) SwipesViewController *iSwipeViewController;

@end

//
//  EventHotelListViewController.h
//  HubCiti
//
//  Created by ionnor on 11/25/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum EventHotelStates
{
 GETEVENTHOTELDISPLAY
}eventHotelStates;
@interface EventHotelListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,CustomizedNavControllerDelegate>
{
    UITableView *tblEventHotelList;
    eventHotelStates *iWebRequestState;
    BOOL isNextPageAvailable;
    NSMutableArray *arreventHotelList;
}
@end

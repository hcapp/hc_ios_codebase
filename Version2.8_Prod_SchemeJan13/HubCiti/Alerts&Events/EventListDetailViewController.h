//
//  EventListDetailViewController.h
//  HubCiti
//
//  Created by ionnor on 11/25/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import <MessageUI/MessageUI.h>
#import "DWBubbleMenuButton.h"
@class AnyViewController;
@class EmailShareViewController;
//#import "EventLocationViewController.h"

typedef enum webServicesEventsDetails
{
    EVENTSDETAILLIST,
    EVENTADDRESSCALL,
	RETSUMMARYS,
    EVENT_SHAREINFO
}webServicesEventsDetailState;


static int countTapped;
BOOL firstTime;
@interface EventListDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, DWBubbleMenuViewDelegate,UIWebViewDelegate,CustomizedNavControllerDelegate>
{
    webServicesEventsDetailState iWebRequestState;
    NSString *strEventDescription;
    BOOL isHotelAvailable;
    BOOL isPackageAvailable;
    UITextView *txtEventDetails;
    UITextView *txtEventSortDesc;
    UILabel *lblOnGngDetails;
    UIButton *btnLocation;
    UIButton *btnCalendar;
    UIButton *btnPackage;
    UIButton *btnHotel,*btnLogistics;
    IBOutlet UIButton *btnShare;
    UIButton *btnPurchaseTicket;
    UIButton *btnMoreInfo;
    NSMutableArray *arrEventDetails;
    UIScrollView *eventDetailScrollView;
    CustomizedNavController *cusNav;
    //EventLocationViewController *iEventLocationViewController;
    
    NSMutableArray *arrRetailers;
    NSMutableArray *arrRetailerAddress;
    NSMutableArray *arrRetailerLogo;
    NSMutableArray *arrRetailerId;
    NSMutableArray *arrRetailerLocId;
    NSMutableArray *arrRetailerLat;
    NSMutableArray *arrRetailerLong;
    NSMutableArray *arrDistance;
    NSString *days;
    EKRecurrenceRule *rule;
    EKRecurrenceEnd *recurrenceEnd;
    //@Added by Keshava
    UITableView *tblEventLocations;
    UIButton    *cross_button;
    UIView  *tblEventHolder;
    
    NSArray *evtArrRetailers;
    NSArray *evtArrAddress;
    NSArray *evtArrAddress1;
    NSArray *evtArrAddress2;
    NSArray *evtArrRetailerLogo;
    NSArray *evtArrRetailerId;
    NSArray *evtArrRetailerLocId;
    NSArray *evtLatitude;
    NSArray *evtLongitude;
    NSString *evtStrEventName;
    NSString *evtLocation;
    NSMutableArray *evtArrRetailersAddress;

    NSMutableArray *locLatitude;
    NSMutableArray *locLongitude;
    NSMutableArray *eventID;
    NSMutableDictionary *weekDayNumber;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic, strong) NSString *strEventDescription;
@property (readwrite) BOOL isHotelAvailable;
@property (readwrite) BOOL isPackageAvailable;

@property(nonatomic,strong)NSMutableArray *evtArrRetailersAddress;
@property(nonatomic,strong)NSArray *evtArrRetailers;
@property(nonatomic,strong)NSArray *evtArrAddress;
@property(nonatomic,strong)NSArray *evtArrAddress1;
@property(nonatomic,strong)NSArray *evtArrAddress2;
@property(nonatomic,strong)NSArray *evtArrRetailerId;
@property(nonatomic,strong)NSArray *evtArrRetailerLocId;
@property(nonatomic,strong)NSArray *evtArrRetailerLogo;
@property(nonatomic,strong)NSString *evtStrEventName;
@property(nonatomic,strong)NSArray *evtLatitude;
@property(nonatomic,strong)NSArray *evtLongitude;
@property(nonatomic,strong)NSString *evtLocation;
@property(nonatomic,strong)NSNumber *bandEvents;
@property (nonatomic, strong) NSMutableDictionary *weekDayNumber;
@property (nonatomic,strong) EKEventStore *eventStore;

//@property (readwrite) BOOL isRetailerAvailable;
-(IBAction)DetailsButtonClicked:(id)sender;

@end

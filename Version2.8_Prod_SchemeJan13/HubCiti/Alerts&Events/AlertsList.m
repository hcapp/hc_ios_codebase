//
//  AlertsList.m
//  HubCiti
//
//  Created by Anjana on 9/6/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "AlertsList.h"
#import "AlertEventType.h"
#import "MainMenuViewController.h"
#import "AlertDetails.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

NSDictionary *viewDictionary;

@interface AlertsList ()

@end

/****** constant tags to distinguish for alert or event call ****/

@implementation AlertsList{
    UIActivityIndicatorView *loading;
}

static int tagValue;

#pragma mark - View Life cycle methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    loading = nil;
    
    //label to display screen title
    //    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 320, 40)];
    //    titleLabel.textColor = [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]];
    //    titleLabel.backgroundColor = [UIColor clearColor];
    //    titleLabel.font = [UIFont boldSystemFontOfSize:16];
    //    titleLabel.textAlignment = NSTextAlignmentCenter;
    //    titleLabel.text = @"Alerts";
    self.navigationItem.title = @"Alerts";
    // ReleaseAndNilify(titleLabel);
    
    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    //UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    
    ////[mainPage release];
    
    
    //array to hold alert or events objects.
    nextPage = 0;
    alertEventArray = [[NSMutableArray alloc] init];
    lastVisitedRecord = 0;
    
    [self request_AlertList:lastVisitedRecord];
    
    tagValue = 0;           // hardcoded tag value. will hav to set value based on call from previous screen for alerts or events.
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [tableView_Alert deselectRowAtIndexPath:[tableView_Alert indexPathForSelectedRow] animated:NO];
}

#pragma mark navigation barButtonItem actions
-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}



-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    
    else
    {
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


#pragma mark request methods
-(void)request_AlertList:(int)lastRecord{
    
    iWebRequestType = alertlist;
    NSString *urlStr = [NSString stringWithFormat:@"%@alertevent/alertlist",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<MenuItem><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId><platform>IOS</platform>",[defaults valueForKey:KEY_HUBCITIID]];
    
    //for user tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [reqStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    //either of mItemId or bottomBtnId is must
    else if([defaults  valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];
    
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    [reqStr appendFormat:@"<lowerLimit>%d</lowerLimit></MenuItem>",lastRecord];
    
    
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlStr withParam:reqStr];
        
        
        [self parse_AlertList:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    }
    
    
    
    
    //[ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    ReleaseAndNilify(reqStr);
}

#pragma mark parse methods
-(void)responseData:(NSString*)response{
    
    switch (iWebRequestType) {
        case alertlist:
            [self parse_AlertList:response];
            break;
            
        default:
            break;
    }
}

-(void) parse_AlertList : (NSString *) response
{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue]!=10000){
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];

        [loading stopAnimating];
        ReleaseAndNilify(loading);
        return;
    }
    
    TBXMLElement *yellowElement = [TBXML childElementNamed:@"yellow" parentElement:tbXml.rootXMLElement];
    TBXMLElement *greenElement = [TBXML childElementNamed:@"green" parentElement:tbXml.rootXMLElement];
    TBXMLElement *redElement = [TBXML childElementNamed:@"red" parentElement:tbXml.rootXMLElement];
    
    if (redElement!=nil){
        highSevImgPath = [[TBXML textForElement:redElement]copy];
    }
    if (yellowElement!=nil){
        medSevImgPath = [[TBXML textForElement:yellowElement]copy];
    }
    
    if (greenElement!=nil){
        lowSevImgPath = [[TBXML textForElement:greenElement]copy];
    }
    
    
    TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbXml.rootXMLElement];
    [defaults setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
    
    TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbXml.rootXMLElement];
    nextPage = [[TBXML textForElement:nextPageElement]intValue];
    
    TBXMLElement *maxRowNumElement = [TBXML childElementNamed:@"maxRowNum" parentElement:tbXml.rootXMLElement];
    lastVisitedRecord = [[TBXML textForElement:maxRowNumElement]intValue];
    
    TBXMLElement *categoryListElement = [TBXML childElementNamed:@"categoryList" parentElement:tbXml.rootXMLElement];
    
    TBXMLElement *CategoryInfoElement = [TBXML childElementNamed:@"CategoryInfo" parentElement:categoryListElement];
    
    
    while (CategoryInfoElement!=nil) {
        
        
        AlertEventDO *alertObj = [[AlertEventDO alloc]init];
        TBXMLElement *categoryIdElement = [TBXML childElementNamed:@"categoryId" parentElement:CategoryInfoElement];
        TBXMLElement *categoryNameElement = [TBXML childElementNamed:@"categoryName" parentElement:CategoryInfoElement];
        
        alertObj.catId = [TBXML textForElement:categoryIdElement];
        alertObj.catName = [TBXML textForElement:categoryNameElement];
        
        if (![[[alertEventArray lastObject]catId] isEqualToString:[TBXML textForElement:categoryIdElement]])
        {
            alertObj.alertDetailsArray = [[NSMutableArray alloc]init];
        }
        else
        {
            // Copy Last Object Array in current AlertEventDO Object "if Same caegory Repeted"
            AlertEventDO *alertObjRep = [alertEventArray lastObject];
            alertObj.alertDetailsArray = [[NSMutableArray alloc]initWithArray:alertObjRep.alertDetailsArray];
            [alertEventArray removeLastObject];
        }
        
        TBXMLElement *alertListElement = [TBXML childElementNamed:@"alertList" parentElement:CategoryInfoElement];
        TBXMLElement *AlertDetailsElement = [TBXML childElementNamed:@"AlertDetails" parentElement:alertListElement];
        
        //array to have alert details objects
        
        while (AlertDetailsElement!=nil) {
            
            //alert details object
            AlertEventDODetail *alertDetailsObj = [[AlertEventDODetail alloc]init];
            TBXMLElement *alertIdElement = [TBXML childElementNamed:@"alertId" parentElement:AlertDetailsElement];
            TBXMLElement *alertNameElement = [TBXML childElementNamed:@"alertName" parentElement:AlertDetailsElement];
            TBXMLElement *shortDesElement = [TBXML childElementNamed:@"shortDes" parentElement:AlertDetailsElement];
            TBXMLElement *severityIdElement = [TBXML childElementNamed:@"severityId" parentElement:AlertDetailsElement];
            TBXMLElement *startDateElement = [TBXML childElementNamed:@"startDate" parentElement:AlertDetailsElement];
            TBXMLElement *endDateElement = [TBXML childElementNamed:@"endDate" parentElement:AlertDetailsElement];
            TBXMLElement *startTimeElement = [TBXML childElementNamed:@"startTime" parentElement:AlertDetailsElement];
            TBXMLElement *endTimeElement = [TBXML childElementNamed:@"endTime" parentElement:AlertDetailsElement];
            TBXMLElement *alertListIdElement = [TBXML childElementNamed:@"alertListId" parentElement:AlertDetailsElement];
            TBXMLElement *sevImgPathElement = [TBXML childElementNamed:@"sevImgPath" parentElement:AlertDetailsElement];
            
            alertDetailsObj.alertId = [TBXML textForElement:alertIdElement];
            alertDetailsObj.alertName = [TBXML textForElement:alertNameElement];
            alertDetailsObj.sDesc = [TBXML textForElement:shortDesElement];
            alertDetailsObj.severityId = [TBXML textForElement:severityIdElement];
            alertDetailsObj.startDate = [TBXML textForElement:startDateElement];
            alertDetailsObj.endDate = [TBXML textForElement:endDateElement];
            alertDetailsObj.startTime = [TBXML textForElement:startTimeElement];
            alertDetailsObj.endTime = [TBXML textForElement:endTimeElement];
            alertDetailsObj.alertListId = [TBXML textForElement:alertListIdElement];
            alertDetailsObj.sevImgPath = [TBXML textForElement:sevImgPathElement];
            
            AlertDetailsElement = [TBXML nextSiblingNamed:@"AlertDetails" searchFromElement:AlertDetailsElement];
            
            [alertObj.alertDetailsArray addObject:alertDetailsObj];
            ReleaseAndNilify(alertDetailsObj);
        }
        
        CategoryInfoElement = [TBXML nextSiblingNamed:@"CategoryInfo" searchFromElement:CategoryInfoElement];
        [alertEventArray addObject:alertObj];
        
        ReleaseAndNilify(alertObj);
    }
    [tableView_Alert reloadData];
    [defaults setBool:NO forKey:@"ViewMore"];
    
    /*  if(alertElement != NULL) {
     
     TBXMLElement *categoryElemt = [TBXML childElementNamed:@"Category" parentElement:alertElement];
     while(categoryElemt ){
     
     TBXMLElement *dataElement = [TBXML childElementNamed:@"Data" parentElement:categoryElemt];
     AlertEventDO *aInfo = [[AlertEventDO alloc]init];
     aInfo.alertArray = [[NSMutableArray alloc]init];
     
     while (dataElement) {
     
     TBXMLElement *HeaderNameElemt = [TBXML childElementNamed:@"Header" parentElement:dataElement];
     aInfo.headerName = [TBXML textForElement:HeaderNameElemt];
     
     TBXMLElement *mainTitleElement = [TBXML childElementNamed:@"MainTitle" parentElement:dataElement];
     TBXMLElement *subTitleElement = [TBXML childElementNamed:@"SubTitle" parentElement:dataElement];
     
     AlertEventDODetail *alertObject = [[AlertEventDODetail alloc]init];
     
     if (mainTitleElement != NULL) {
     alertObject.mainTitle = [TBXML textForElement:mainTitleElement];
     }
     if (subTitleElement != NULL) {
     alertObject.subTitle = [TBXML textForElement:mainTitleElement];
     }
     
     //adding object in each category in array(represents objects in specific category).
     [aInfo.alertArray addObject:alertObject];
     ReleaseAndNilify(alertObject);
     dataElement = [TBXML nextSiblingNamed:@"Data" searchFromElement:dataElement];
     }
     //adding object for each category final array(represents category name along with objects in that category).
     [alertEventArray addObject:aInfo];
     ReleaseAndNilify(aInfo);
     ReleaseAndNilify(aInfo.alertArray);
     
     categoryElemt = [TBXML nextSiblingNamed:@"Category" searchFromElement:categoryElemt];
     }
     
     }*/
}

#pragma mark - Table view delegates and datasource methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        return 70;
    else
        return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    AlertEventDO *alertDO = [alertEventArray objectAtIndex:section];
    
    UILabel *headerName = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 315, 26)];
    headerName.backgroundColor = [UIColor lightGrayColor];
    headerName.text = [NSString stringWithFormat:@" %@",alertDO.catName];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        headerName.font = [UIFont fontWithName:@"Helvetica" size:20];
    else
        headerName.font = [UIFont fontWithName:@"Helvetica" size:15];
    return headerName ;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [alertEventArray count];
}

/*******************************************************************************
 AlertEventDO contains array(alertArray) to hold the objects for each category sent via server
 ******************************************************************************/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DLog(@"%lu %d",(unsigned long)[alertEventArray count],nextPage);
    AlertEventDO *alertDO = [alertEventArray objectAtIndex:section];
    
    if (nextPage == 1 && section == [alertEventArray count]-1)
        return [alertDO.alertDetailsArray count]+1;
    
    return [alertDO.alertDetailsArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"CellIdentifier1";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] ;
    
    AlertEventDO *alertType = [alertEventArray objectAtIndex:indexPath.section];
    UILabel *mainTitle = nil,*subTitle = nil;
    SdImageView *sevImg = nil;
    if (indexPath.row == [alertType.alertDetailsArray count] && nextPage == 1) {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
        //        CGRect frame;
        //        frame.origin.x = 0;
        //        frame.origin.y = 16;
        //        frame.size.width = SCREEN_WIDTH;
        //        frame.size.height = 24;
        //        label = [[UILabel alloc] init];
        //        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        //            label.font = [UIFont boldSystemFontOfSize:21];
        //        else
        //            label.font = [UIFont boldSystemFontOfSize:16];
        //        label.textAlignment = NSTextAlignmentCenter;
        //        label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
        //        [cell.contentView addSubview:label];
        //        cell.accessoryType = UITableViewCellAccessoryNone;
        //        label.translatesAutoresizingMaskIntoConstraints = NO;
        //        viewDictionary = NSDictionaryOfVariableBindings(label);
        //        ReleaseAndNilify(label);
        //return cell;
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    mainTitle = [[UILabel alloc] init];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        mainTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    else
        mainTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
    mainTitle.backgroundColor = [UIColor clearColor];
    mainTitle.text = [[alertType.alertDetailsArray objectAtIndex:indexPath.row] alertName];
    mainTitle.textColor = [UIColor colorWithRGBInt:0x112e72];
    
    subTitle = [[UILabel alloc] init];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        subTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    else
        subTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
    subTitle.backgroundColor = [UIColor clearColor];
    subTitle.text = [[alertType.alertDetailsArray objectAtIndex:indexPath.row] sDesc];
    subTitle.textColor = [UIColor grayColor];
    
    sevImg = [[SdImageView alloc]init];
    sevImg.backgroundColor = [UIColor clearColor];
    
    if ([[[alertType.alertDetailsArray objectAtIndex:indexPath.row] severityId] isEqualToString:@"1"]){
        
        [sevImg loadImage:lowSevImgPath];
    }
    
    else if ([[[alertType.alertDetailsArray objectAtIndex:indexPath.row] severityId] isEqualToString:@"2"]){
        
        [sevImg loadImage:medSevImgPath];
    }
    
    else if ([[[alertType.alertDetailsArray objectAtIndex:indexPath.row] severityId] isEqualToString:@"3"]){
        
        [sevImg loadImage:highSevImgPath];
    }
    
    [cell.contentView addSubview:sevImg];
    //[sevImg release];
    
    [cell.contentView addSubview:mainTitle];
    [cell.contentView addSubview:subTitle];
    
    mainTitle.translatesAutoresizingMaskIntoConstraints = NO;
    subTitle.translatesAutoresizingMaskIntoConstraints = NO;
    sevImg.translatesAutoresizingMaskIntoConstraints = NO;
    viewDictionary = NSDictionaryOfVariableBindings(mainTitle,subTitle,sevImg);
    [self setConstraints:cell];
    ReleaseAndNilify(mainTitle);
    ReleaseAndNilify(subTitle);
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    AlertEventDO *alertType = [alertEventArray objectAtIndex:indexPath.section];
    if (indexPath.row == [alertType.alertDetailsArray count] && nextPage == 1) {
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"] && lastVisitedRecord!=0) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self request_AlertList:lastVisitedRecord];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tableView_Alert reloadData];
                });
            });
            
        }
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AlertEventDO *alertType = [alertEventArray objectAtIndex:indexPath.section];
    
    [defaults setBool:NO forKey:@"ViewMore"];
    
    
    //    if (indexPath.row == [alertType.alertDetailsArray count] && nextPage == 1) {
    //
    //        [self request_AlertList:lastVisitedRecord];
    //        return;
    //    }
    
    // else{
    
    AlertEventDODetail *detailObj = [alertType.alertDetailsArray objectAtIndex:indexPath.row];
    
    switch ([[[alertType.alertDetailsArray objectAtIndex:indexPath.row] severityId]intValue]) {
        case 1:
            detailObj.sevImgPath = lowSevImgPath;
            break;
        case 2:
            detailObj.sevImgPath = medSevImgPath;
            break;
        case 3:
            detailObj.sevImgPath = highSevImgPath;
            break;
            
        default:
            break;
    }
    
    
    AlertDetails *alertDetails = [[AlertDetails alloc]initWithNibName:@"AlertDetails" bundle:nil];
    [alertDetails setAlertDetails:detailObj];
    [self.navigationController pushViewController:alertDetails animated:NO];
    // [alertDetails release];
    //}
    
    
}

-(void)setConstraints:(UITableViewCell*)cell
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        if([viewDictionary objectForKey:@"sevImg"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(18)-[sevImg(16)]-|"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(600)-[sevImg(48)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"mainTitle"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[mainTitle(20)]-|"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[mainTitle(220)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"subTitle"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(35)-[subTitle(20)]-|"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[subTitle(450)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"label"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(24)]-|"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(320)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
    }
    else
    {
        if([viewDictionary objectForKey:@"sevImg"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(18)-[sevImg(16)]-|"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(238)-[sevImg(48)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"mainTitle"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[mainTitle(20)]-|"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[mainTitle(220)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"subTitle"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(30)-[subTitle(20)]-|"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[subTitle(240)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"label"] != nil)
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(24)]-|"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(320)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

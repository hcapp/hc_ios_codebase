//
//  EventHotelListViewController.m
//  HubCiti
//
//  Created by ionnor on 11/25/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "EventHotelListViewController.h"
#import "MainMenuViewController.h"
#import "eventHotelListDO.h"
#import "RetailerSummaryViewController.h"
#import "HotelDetailViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

NSMutableDictionary *viewDictionary;

@interface EventHotelListViewController ()

@end

@implementation EventHotelListViewController{
    UIActivityIndicatorView *loading;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    loading = nil;
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    //UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0,30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    
    //[mainPage release];
    
    
    self.navigationItem.title = @"Hotels";
    
    arreventHotelList = [[NSMutableArray alloc]init];
    [self settableViewOnScreen];
    [self Request_geteventhoteldisplay];
    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillAppear:(BOOL)animated
{
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [super viewWillAppear:animated];
    if(tblEventHotelList)
        [tblEventHotelList deselectRowAtIndexPath:[tblEventHotelList indexPathForSelectedRow]  animated:YES];
}

-(void)popBackToPreviousPage
{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}




-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


-(void)settableViewOnScreen
{
    if(tblEventHotelList==nil){
        int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
        tblEventHotelList = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-yVal) style:UITableViewStylePlain];
        tblEventHotelList.dataSource=self;
        tblEventHotelList.delegate=self;
        [tblEventHotelList setBackgroundColor:[UIColor whiteColor]];
        tblEventHotelList.tableFooterView.hidden=YES;
        [self.view addSubview:tblEventHotelList];
    }
}

-(void)Request_geteventhoteldisplay
{
    iWebRequestState = GETEVENTHOTELDISPLAY;
    
    //    NSMutableString *requestStr = [[NSMutableString alloc] init];
    //    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>",@"3"];
    //    [requestStr appendFormat:@"<eventId>%@</eventId>",[defaults valueForKey:EVENTID]];
    //    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>",@"70"];
    //    [requestStr appendFormat:@"<lastVisitedRecord>%d</lastVisitedRecord>",[arreventHotelList count]];
    //    [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    //    [requestStr appendFormat:@"</ThisLocationRequest>"];
    //    NSString *urlString = [NSString stringWithFormat:@"%@alertevent/geteventhoteldisplay",@"http://10.11.202.220:8080/HubCiti1.0/"];
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<eventId>%@</eventId>",[defaults valueForKey:EVENTID]];
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    
    //@Keerthi added lat long or postal in req string
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    
    
    [requestStr appendFormat:@"<lastVisitedRecord>%lu</lastVisitedRecord>",(unsigned long)[arreventHotelList count]];
    [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    [requestStr appendFormat:@"</ThisLocationRequest>"];
    NSString *urlString = [NSString stringWithFormat:@"%@alertevent/geteventhoteldisplay",BASE_URL];
    
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
        
        
        [self parse_geteventhoteldisplay:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    }
    
    
    //[ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    //[requestStr release];
    
}




-(void)responseData:(NSString *) response
{
    if(iWebRequestState == GETEVENTHOTELDISPLAY)
    {
        [self parse_geteventhoteldisplay:response];
    }
    
    [tblEventHotelList reloadData];
}


-(void)parse_geteventhoteldisplay:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        isNextPageAvailable = NO;
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbxml.rootXMLElement];
        if(nextPageElement)
            isNextPageAvailable = [[TBXML textForElement:nextPageElement]boolValue];
        
        TBXMLElement *retailersElement = [TBXML childElementNamed:@"retailers" parentElement:tbxml.rootXMLElement];
        
        if(retailersElement)
        {
            TBXMLElement *RetailerElement = [TBXML childElementNamed:@"Retailer" parentElement:retailersElement];
            while (RetailerElement)
            {
                eventHotelListDO *ieventHotelListDO = [[eventHotelListDO alloc]init];
                
                TBXMLElement *retailerNameElement = [TBXML childElementNamed:@"retailerName" parentElement:RetailerElement];
                TBXMLElement *retailLocationIdElement = [TBXML childElementNamed:@"retailLocationId" parentElement:RetailerElement];
                TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerElement];
                TBXMLElement *hotelPriceElement = [TBXML childElementNamed:@"hotelPrice" parentElement:RetailerElement];
                TBXMLElement *ratingElement = [TBXML childElementNamed:@"rating" parentElement:RetailerElement];
                TBXMLElement *imgPathElement = [TBXML childElementNamed:@"logoImagePath" parentElement:RetailerElement];
                
                TBXMLElement *retListIdElement = [TBXML childElementNamed:@"retListId" parentElement:RetailerElement];
                TBXMLElement *retailerIdElement = [TBXML childElementNamed:@"retailerId" parentElement:RetailerElement];
                if(retListIdElement)
                    ieventHotelListDO.retListId = [TBXML textForElement:retListIdElement];
                if(retailerIdElement)
                    ieventHotelListDO.retailerId = [TBXML textForElement:retailerIdElement];
                if(retailerNameElement)
                    ieventHotelListDO.retailerName = [TBXML textForElement:retailerNameElement];
                if(retailLocationIdElement)
                    ieventHotelListDO.retailLocationId = [TBXML textForElement:retailLocationIdElement];
                if(distanceElement)
                    ieventHotelListDO.distance = [TBXML textForElement:distanceElement];
                if(hotelPriceElement)
                    ieventHotelListDO.hotelPrice = [TBXML textForElement:hotelPriceElement];
                if(ratingElement)
                    ieventHotelListDO.hotelrating = [TBXML textForElement:ratingElement];
                if(imgPathElement)
                    ieventHotelListDO.imgPath = [TBXML textForElement:imgPathElement];
                
                [arreventHotelList addObject:ieventHotelListDO];
                // [ieventHotelListDO release];
                
                RetailerElement = [TBXML nextSiblingNamed:@"Retailer" searchFromElement:RetailerElement];
            }
        }
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        [loading stopAnimating];
        ReleaseAndNilify(loading);
        
    }
    [tblEventHotelList reloadData];
    [defaults setBool:NO forKey:@"ViewMore"];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        return 120.0;
    else
        return 70.0;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isNextPageAvailable)
        return [arreventHotelList count]+1;
    else
        return [arreventHotelList count];
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellEventListView";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.backgroundColor =  [UIColor clearColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }
    }
    
    UILabel *label = nil;
    if(isNextPageAvailable && indexPath.row == [arreventHotelList count])
    {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
        //        label = [[[UILabel alloc] init] ;
        //        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        //            label.font = [UIFont boldSystemFontOfSize:21];
        //        else
        //            label.font = [UIFont boldSystemFontOfSize:16];
        //        label.textAlignment = NSTextAlignmentCenter;
        //        label.textColor = [UIColor colorWithRGBInt:0x112e72];
        //        label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
        //        [cell.contentView addSubview:label];
        //        cell.accessoryType = UITableViewCellAccessoryNone;
        //        return cell;
    }
    
    eventHotelListDO *ieventHotelListDO = [arreventHotelList objectAtIndex:indexPath.row];
    SdImageView *asyncImageView = nil;
    UILabel *lblRetailerName = nil;
    UILabel *lblhotelRating = nil;
    UILabel *lblRatingVal = nil;
    UILabel *lblHotelPriceTitle = nil;
    UILabel *lblPriceVal = nil;
    UILabel *lblHoteldistanceTitle = nil;
    UILabel *lblDistanceVal = nil;
    
    if(ieventHotelListDO.imgPath)
    {
        asyncImageView = [[SdImageView alloc] init];
        asyncImageView.backgroundColor = [UIColor clearColor];
        //asyncImageView.layer.cornerRadius = 5.0f;
        [asyncImageView loadImage:ieventHotelListDO.imgPath];
        
        [cell.contentView addSubview:asyncImageView];
        //[asyncImageView release];
        
    }
    
    int start_X = 55;
    int start_X_Title = 155;
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        start_X = 130;
        start_X_Title = 240;
    }
    
    if(ieventHotelListDO.retailerName)
    {
        lblRetailerName = [[UILabel alloc]init];
        [lblRetailerName setText:ieventHotelListDO.retailerName];
        [lblRetailerName setTextColor:[UIColor blackColor]];
        [lblRetailerName setBackgroundColor:[UIColor clearColor]];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [lblRetailerName setFont:[UIFont boldSystemFontOfSize:17]];
        else
            [lblRetailerName setFont:[UIFont boldSystemFontOfSize:12]];
        [cell.contentView addSubview:lblRetailerName];
        //[lblRetailerName release];
        
    }
    if(ieventHotelListDO.hotelrating)
    {
        lblhotelRating = [[UILabel alloc]init];
        [lblhotelRating setText:@"Rating: "];
        [lblhotelRating setTextColor:[UIColor blackColor]];
        [lblhotelRating setBackgroundColor:[UIColor clearColor]];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [lblhotelRating setFont:[UIFont systemFontOfSize:15]];
        else
            [lblhotelRating setFont:[UIFont systemFontOfSize:10]];
        [cell.contentView addSubview:lblhotelRating];
        //   [lblhotelRating release];
        
        
        lblRatingVal = [[UILabel alloc]init];
        [lblRatingVal setText:ieventHotelListDO.hotelrating];
        [lblRatingVal setTextColor:[UIColor blackColor]];
        [lblRatingVal setBackgroundColor:[UIColor clearColor]];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [lblRatingVal setFont:[UIFont boldSystemFontOfSize:17]];
        else
            [lblRatingVal setFont:[UIFont boldSystemFontOfSize:12]];
        [cell.contentView addSubview:lblRatingVal];
        // [lblRatingVal release];
    }
    if(ieventHotelListDO.hotelPrice)
    {
        lblHotelPriceTitle = [[UILabel alloc]init];
        [lblHotelPriceTitle setText:@"Price: "];
        [lblHotelPriceTitle setTextColor:[UIColor blackColor]];
        [lblHotelPriceTitle setBackgroundColor:[UIColor clearColor]];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [lblHotelPriceTitle setFont:[UIFont systemFontOfSize:15]];
        else
            [lblHotelPriceTitle setFont:[UIFont systemFontOfSize:10]];
        [cell.contentView addSubview:lblHotelPriceTitle];
        //[lblHotelPriceTitle release];
        
        
        lblPriceVal = [[UILabel alloc]init];
        [lblPriceVal setText:ieventHotelListDO.hotelPrice];
        [lblPriceVal setTextColor:[UIColor blackColor]];
        [lblPriceVal setBackgroundColor:[UIColor clearColor]];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [lblPriceVal setFont:[UIFont boldSystemFontOfSize:17]];
        else
            [lblPriceVal setFont:[UIFont boldSystemFontOfSize:12]];
        [cell.contentView addSubview:lblPriceVal];
        // [lblPriceVal release];
    }
    if(ieventHotelListDO.distance)
    {
        lblHoteldistanceTitle = [[UILabel alloc]init];
        [lblHoteldistanceTitle setText:@"Distance"];
        [lblHoteldistanceTitle setTextColor:[UIColor blackColor]];
        [lblHoteldistanceTitle setBackgroundColor:[UIColor clearColor]];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [lblHoteldistanceTitle setFont:[UIFont systemFontOfSize:15]];
        else
            [lblHoteldistanceTitle setFont:[UIFont systemFontOfSize:10]];
        [cell.contentView addSubview:lblHoteldistanceTitle];
        //[lblHoteldistanceTitle release];
        
        
        lblDistanceVal = [[UILabel alloc]init];
        [lblDistanceVal setText:ieventHotelListDO.distance];
        [lblDistanceVal setTextColor:[UIColor blackColor]];
        [lblDistanceVal setBackgroundColor:[UIColor clearColor]];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [lblDistanceVal setFont:[UIFont boldSystemFontOfSize:17]];
        else
            [lblDistanceVal setFont:[UIFont boldSystemFontOfSize:12]];
        [cell.contentView addSubview:lblDistanceVal];
        // [lblDistanceVal release];
    }
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        label.frame = CGRectMake(0, 26, SCREEN_WIDTH, 24);
        asyncImageView.frame = CGRectMake(10, 22, 100, 70);
        lblRetailerName.frame = CGRectMake(start_X, 15, SCREEN_WIDTH - start_X, 20);
        lblhotelRating.frame = CGRectMake(start_X, 35, 100, 20);
        lblRatingVal.frame = CGRectMake(start_X_Title, 35, SCREEN_WIDTH - start_X_Title, 20);
        lblHotelPriceTitle.frame = CGRectMake(start_X, 55, 100, 20);
        lblPriceVal.frame = CGRectMake(start_X_Title, 55, SCREEN_WIDTH - start_X_Title, 20);
        lblHoteldistanceTitle.frame = CGRectMake(start_X, 75, 100, 20);
        lblDistanceVal.frame = CGRectMake(start_X_Title, 75, SCREEN_WIDTH - start_X_Title, 20);
    }
    else
    {
        label.frame = CGRectMake(0, 16, 320, 24);
        asyncImageView.frame = CGRectMake(0, 0, 50, 50);
        lblRetailerName.frame = CGRectMake(start_X, 3, SCREEN_WIDTH - start_X, 20);
        lblhotelRating.frame = CGRectMake(start_X, 23, 100, 17);
        lblRatingVal.frame = CGRectMake(start_X_Title, 23, SCREEN_WIDTH - start_X_Title, 17);
        lblHotelPriceTitle.frame = CGRectMake(start_X, 40,100, 15);
        lblPriceVal.frame = CGRectMake(start_X_Title, 40, SCREEN_WIDTH - start_X_Title, 15);
        lblHoteldistanceTitle.frame = CGRectMake(start_X, 55, 100, 15);
        lblDistanceVal.frame = CGRectMake(start_X_Title, 55, SCREEN_WIDTH - start_X_Title, 15);
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(isNextPageAvailable && indexPath.row == [arreventHotelList count]){
        
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"] && isNextPageAvailable == YES) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self Request_geteventhoteldisplay];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tblEventHotelList reloadData];
                });
            });
            
        }
    }
    
}

// Called after the user changes the selection.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    [defaults setBool:NO forKey:@"ViewMore"];
    //    if(isNextPageAvailable && indexPath.row == [arreventHotelList count])
    //        [self Request_geteventhoteldisplay];
    // else
    // {
    eventHotelListDO *ieventHotelListDO = [arreventHotelList objectAtIndex:indexPath.row];
    [defaults setValue:ieventHotelListDO.retailerId forKey:@"HotelretailerId"];
    [defaults setValue:ieventHotelListDO.retailLocationId forKey:@"HotelretailLocationId"];
    [defaults setValue:ieventHotelListDO.retListId forKey:@"HotelretListId"];
    
    HotelDetailViewController *iHotelDetailViewController = [[HotelDetailViewController alloc]initWithNibName:@"HotelDetailViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iHotelDetailViewController animated:NO];
    //  [iHotelDetailViewController release];
    //}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

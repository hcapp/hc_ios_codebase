//
//  HotelDetailViewController.m
//  HubCiti
//
//  Created by ionnor on 11/27/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "HotelDetailViewController.h"
#import "eventHotelListDO.h"
#import "SpecialOffersViewController.h"

NSDictionary *viewDictionary;

@interface HotelDetailViewController ()

@end

@implementation HotelDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.navigationItem.title= @"Details";
    
  
    self.navigationItem.hidesBackButton = YES;
    //[back release];

    
    [self Request_fetcheventhoteldetail];
    // Do any additional setup after loading the view from its nib.
}
-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)Request_fetcheventhoteldetail
{
//    NSMutableString *requestStr = [[NSMutableString alloc] init];
//    [requestStr appendFormat:@"<Retailer><userId>%@</userId>",@"3"];
//    [requestStr appendFormat:@"<eventId>%@</eventId>",@"7"];
//    [requestStr appendFormat:@"<retListId>%@</retListId>",@"12530"];
//    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",@"6112"];
//    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>",@"70"];
//    
//    [requestStr appendFormat:@"</Retailer>"];
//    
//    NSString *urlString = [NSString stringWithFormat:@"%@alertevent/fetcheventhoteldetail",@"http://10.11.201.22:8080/HubCiti1.0/"];
    

    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<Retailer><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<eventId>%@</eventId>",[defaults valueForKey:EVENTID]];
    [requestStr appendFormat:@"<retListId>%@</retListId>",[defaults valueForKey:@"HotelretListId"]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[defaults valueForKey:@"HotelretailLocationId"]];
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    
    [requestStr appendFormat:@"</Retailer>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@alertevent/fetcheventhoteldetail",BASE_URL];

    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    //[requestStr release];
    
}
// handle server respobse Here
-(void)responseData:(NSString *) response
{
    [self Parse_fetcheventhoteldetail:response];
    
    if([arrHotelDetails count] > 0)
        [self setViewAccordingToData];
}


-(void)Parse_fetcheventhoteldetail:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
		return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        eventHotelListDO *ieventHotelListDO = [[eventHotelListDO alloc]init];
        
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *retailNameElement = [TBXML childElementNamed:@"retailName" parentElement:tbxml.rootXMLElement];
        TBXMLElement *priceElement = [TBXML childElementNamed:@"price" parentElement:tbxml.rootXMLElement];
        TBXMLElement *retailIdElement = [TBXML childElementNamed:@"retailId" parentElement:tbxml.rootXMLElement];

        TBXMLElement *addressElement = [TBXML childElementNamed:@"address" parentElement:tbxml.rootXMLElement];
        TBXMLElement *retailLocationIdElement = [TBXML childElementNamed:@"retailLocationId" parentElement:tbxml.rootXMLElement];
        TBXMLElement *ratingElement = [TBXML childElementNamed:@"rating" parentElement:tbxml.rootXMLElement];

        TBXMLElement *roomCheckUrlElement = [TBXML childElementNamed:@"roomCheckUrl" parentElement:tbxml.rootXMLElement];
        TBXMLElement *roomBookUrlElement = [TBXML childElementNamed:@"roomBookUrl" parentElement:tbxml.rootXMLElement];
        TBXMLElement *packagePriceElement = [TBXML childElementNamed:@"packagePrice" parentElement:tbxml.rootXMLElement];
        TBXMLElement *imagePathElement = [TBXML childElementNamed:@"imagePath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *DescriptionElement = [TBXML childElementNamed:@"description" parentElement:tbxml.rootXMLElement];

        
        if(DescriptionElement)
            ieventHotelListDO.description = [TBXML textForElement:DescriptionElement];

        if(retailNameElement)
            ieventHotelListDO.retailerName = [TBXML textForElement:retailNameElement];
        if(priceElement)
            ieventHotelListDO.hotelPrice = [TBXML textForElement:priceElement];
        if(retailIdElement)
            ieventHotelListDO.retailerId = [TBXML textForElement:retailIdElement];
        if(addressElement)
            ieventHotelListDO.address = [TBXML textForElement:addressElement];
        if(retailLocationIdElement)
            ieventHotelListDO.retailLocationId = [TBXML textForElement:retailLocationIdElement];
        if(ratingElement)
            ieventHotelListDO.hotelrating = [TBXML textForElement:ratingElement];
        if(roomCheckUrlElement)
            ieventHotelListDO.roomCheckUrl = [TBXML textForElement:roomCheckUrlElement];
        if(roomBookUrlElement)
            ieventHotelListDO.roomBookUrl = [TBXML textForElement:roomBookUrlElement];
        if(packagePriceElement)
            ieventHotelListDO.packagePrice = [TBXML textForElement:packagePriceElement];
        if(imagePathElement)
            ieventHotelListDO.imgPath = [TBXML textForElement:imagePathElement];

        arrHotelDetails = [[NSArray alloc]initWithObjects:ieventHotelListDO, nil];
        
       // [ieventHotelListDO release];

    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }

}

-(void)setViewAccordingToData
{
    
    eventHotelListDO *ieventHotelListDO = [arrHotelDetails objectAtIndex:0];
    
    // Add Event Image
    SdImageView *asyncImageView = [[SdImageView alloc] init];
    asyncImageView.backgroundColor = [UIColor clearColor];
    asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
   // asyncImageView.layer.cornerRadius = 5.0f;
    if(ieventHotelListDO.imgPath)
        [asyncImageView loadImage:ieventHotelListDO.imgPath];
    
    [self.view addSubview:asyncImageView];
    //[asyncImageView release];

    UILabel *lblHotelName = [[UILabel alloc]init];
    if(ieventHotelListDO.retailerName)
        [lblHotelName setText:ieventHotelListDO.retailerName];
    [lblHotelName setTextColor:[UIColor blackColor]];
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        lblHotelName.textAlignment = NSTextAlignmentCenter;
    [lblHotelName setBackgroundColor:[UIColor clearColor]];
    [lblHotelName setFont:[UIFont boldSystemFontOfSize:12]];
    [self.view addSubview:lblHotelName];
    
    UILabel *lblHotelRating = [[UILabel alloc]init];
    [lblHotelRating setText:@"Rating: "];
    [lblHotelRating setTextColor:[UIColor blackColor]];
    [lblHotelRating setBackgroundColor:[UIColor clearColor]];
    [lblHotelRating setFont:[UIFont systemFontOfSize:12]];
    [self.view addSubview:lblHotelRating];
    
    UILabel *lblHotelRatingVal = [[UILabel alloc]init];
    if(ieventHotelListDO.hotelrating)
        [lblHotelRatingVal setText:ieventHotelListDO.hotelrating];
    [lblHotelRatingVal setTextColor:[UIColor blackColor]];
    [lblHotelRatingVal setBackgroundColor:[UIColor clearColor]];
    [lblHotelRatingVal setFont:[UIFont boldSystemFontOfSize:12]];
    [self.view addSubview:lblHotelRatingVal];
    
    
    UILabel *lblHotelAddress = [[UILabel alloc]init];
    [lblHotelAddress setText:@"Address: "];
    [lblHotelAddress setTextColor:[UIColor blackColor]];
    [lblHotelAddress setBackgroundColor:[UIColor clearColor]];
    [lblHotelAddress setFont:[UIFont systemFontOfSize:12]];
    [self.view addSubview:lblHotelAddress];
    
    UILabel *lblHotelAddressVal = [[UILabel alloc]init];
    if(ieventHotelListDO.address)
        [lblHotelAddressVal setText:ieventHotelListDO.address];
    lblHotelAddressVal.numberOfLines=3;
    [lblHotelAddressVal setTextColor:[UIColor blackColor]];
    [lblHotelAddressVal setBackgroundColor:[UIColor clearColor]];
    [lblHotelAddressVal setFont:[UIFont boldSystemFontOfSize:12]];
    [self.view addSubview:lblHotelAddressVal];
    
    
    // Add Event Details
    txtEventDetails = [[UITextView alloc] init];
    [txtEventDetails.layer setBorderColor:[[[UIColor blackColor] colorWithAlphaComponent:0.5] CGColor]];
    
    if(ieventHotelListDO.description)
        txtEventDetails.text = ieventHotelListDO.description;
    txtEventDetails.editable= NO;
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
    [txtEventDetails.layer setBorderWidth:0.7];
    txtEventDetails.layer.cornerRadius = 10;
    }
    else{
        [txtEventDetails.layer setBorderWidth:0.5];
        //The rounded corner part, where you specify your view's corner radius:
        txtEventDetails.layer.cornerRadius = 5;
    }
    txtEventDetails.clipsToBounds = YES;
    [self.view addSubview:txtEventDetails];
    
   // int YPos = 320;
    if(ieventHotelListDO.roomCheckUrl)
    {
        btnAvailability = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        //btnAvailability.frame     = CGRectMake(10, YPos,SCREEN_WIDTH-20,40);
        [btnAvailability addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnAvailability.tag = 0;
        [btnAvailability setTitle:@"Check Availability" forState:UIControlStateNormal];
        [btnAvailability setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnAvailability setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        [btnAvailability setBackgroundColor:[UIColor lightGrayColor]];
        [self.view addSubview:btnAvailability];
       // YPos = YPos + 41;
    }

    if(ieventHotelListDO.roomBookUrl)
    {
        btnBookNow = [UIButton buttonWithType:UIButtonTypeRoundedRect];
       // btnBookNow.frame     = CGRectMake(10, YPos,SCREEN_WIDTH-20,40);
        [btnBookNow addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnBookNow.tag = 1;
        [btnBookNow setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnBookNow setTitle:@"Book Now" forState:UIControlStateNormal];
        [btnBookNow setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        [btnBookNow setBackgroundColor:[UIColor lightGrayColor]];
        [self.view addSubview:btnBookNow];
    }
    
    asyncImageView.translatesAutoresizingMaskIntoConstraints = NO;
    lblHotelName.translatesAutoresizingMaskIntoConstraints = NO;
    lblHotelRating.translatesAutoresizingMaskIntoConstraints = NO;
    lblHotelRatingVal.translatesAutoresizingMaskIntoConstraints = NO;
    lblHotelAddress.translatesAutoresizingMaskIntoConstraints = NO;
    lblHotelAddressVal.translatesAutoresizingMaskIntoConstraints = NO;
    txtEventDetails.translatesAutoresizingMaskIntoConstraints = NO;
    btnAvailability.translatesAutoresizingMaskIntoConstraints = NO;
    btnBookNow.translatesAutoresizingMaskIntoConstraints = NO;
    
    if(ieventHotelListDO.roomCheckUrl && ieventHotelListDO.roomBookUrl)
    {
        viewDictionary = NSDictionaryOfVariableBindings(asyncImageView,lblHotelName,lblHotelRating,lblHotelRatingVal,lblHotelAddress,lblHotelAddressVal,txtEventDetails,btnAvailability,btnBookNow);
    }
    else if(ieventHotelListDO.roomCheckUrl && !ieventHotelListDO.roomBookUrl)
    {
        viewDictionary = NSDictionaryOfVariableBindings(asyncImageView,lblHotelName,lblHotelRating,lblHotelRatingVal,lblHotelAddress,lblHotelAddressVal,txtEventDetails,btnAvailability);
    }
    else if(!ieventHotelListDO.roomCheckUrl && ieventHotelListDO.roomBookUrl)
    {
        viewDictionary = NSDictionaryOfVariableBindings(asyncImageView,lblHotelName,lblHotelRating,lblHotelRatingVal,lblHotelAddress,lblHotelAddressVal,txtEventDetails,btnBookNow);
    }
    else
    {
        viewDictionary = NSDictionaryOfVariableBindings(asyncImageView,lblHotelName,lblHotelRating,lblHotelRatingVal,lblHotelAddress,lblHotelAddressVal,txtEventDetails);
    }
    
    [self setConstraints];
}

-(void)setConstraints
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        if([viewDictionary objectForKey:@"asyncImageView"]!=nil)
        {
            // Vertical constrain
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(60)-[asyncImageView(250)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(150)-[asyncImageView(%f)]",SCREEN_WIDTH-280] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblHotelName"]!=nil)
        {
            // Vertical constrain
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(360)-[lblHotelName(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[lblHotelName(%f)]",SCREEN_WIDTH] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblHotelRating"]!=nil)
        {
            // Vertical constrain
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(400)-[lblHotelRating(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(330)-[lblHotelRating(50)]"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblHotelRatingVal"]!=nil)
        {
            // Vertical constrain
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(400)-[lblHotelRatingVal(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(400)-[lblHotelRatingVal(100)]"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblHotelAddress"]!=nil)
        {
            // Vertical constrain
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(450)-[lblHotelAddress(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(320)-[lblHotelAddress(50)]"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"lblHotelAddressVal"]!=nil)
        {
            // Vertical constrain
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(450)-[lblHotelAddressVal(30)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(400)-[lblHotelAddressVal(80)]"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"txtEventDetails"]!=nil)
        {
            // Vertical constrain
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(500)-[txtEventDetails(300)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(150)-[txtEventDetails(%f)]",SCREEN_WIDTH - 280] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"btnAvailability"]!=nil)
        {
            // Vertical constrain
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(820)-[btnAvailability(40)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(150)-[btnAvailability(%f)]",SCREEN_WIDTH - 280] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"btnBookNow"]!=nil)
        {
            // Vertical constrain
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(875)-[btnBookNow(40)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(150)-[btnBookNow(%f)]",SCREEN_WIDTH - 280] options:0 metrics:0 views:viewDictionary]];
        }
    }
    else
    {
        if([viewDictionary objectForKey:@"asyncImageView"]!=nil)
        {
            // Vertical constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[asyncImageView(150)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[asyncImageView(%f)]",SCREEN_WIDTH-20] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblHotelName"]!=nil)
        {
            // Vertical constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(155)-[lblHotelName(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[lblHotelName(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblHotelRating"]!=nil)
        {
            // Vertical constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(180)-[lblHotelRating(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[lblHotelRating(50)]"] options:0 metrics:0 views:viewDictionary]];
        }
        
        
        if([viewDictionary objectForKey:@"lblHotelRatingVal"]!=nil)
        {
            // Vertical constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(180)-[lblHotelRatingVal(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(50)-[lblHotelRatingVal(100)]"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblHotelAddress"]!=nil)
        {
            // Vertical constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(180)-[lblHotelAddress(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(150)-[lblHotelAddress(70)]"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"lblHotelAddressVal"]!=nil)
        {
            // Vertical constrain
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(180)-[lblHotelAddressVal(30)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(220)-[lblHotelAddressVal(80)]"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"txtEventDetails"]!=nil)
        {
            // Vertical constrain
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(211)-[txtEventDetails(100)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[txtEventDetails(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"btnAvailability"]!=nil)
        {
            // Vertical constrain
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(320)-[btnAvailability(40)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[btnAvailability(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"btnBookNow"]!=nil)
        {
            // Vertical constrain
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(361)-[btnBookNow(40)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[btnBookNow(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
        }

    }
}

-(void)DetailsButtonClicked:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    eventHotelListDO *ieventHotelListDO = [arrHotelDetails objectAtIndex:0];

    
    switch (btn.tag)
    {
        case 0:
        {
            [defaults setValue:ieventHotelListDO.roomCheckUrl forKey:KEY_URL];
            SpecialOffersViewController *splOfferVC = [[SpecialOffersViewController alloc]initWithNibName:@"SpecialOffersViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:splOfferVC animated:NO];
            //[splOfferVC release];

        }
            break;
        case 1:
        {
            [defaults setValue:ieventHotelListDO.roomBookUrl forKey:KEY_URL];
            SpecialOffersViewController *splOfferVC = [[SpecialOffersViewController alloc]initWithNibName:@"SpecialOffersViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:splOfferVC animated:NO];
            //[splOfferVC release];

        }
            break;
            
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  AlertEventType.m
//  HubCiti
//
//  Created by Anjana on 9/10/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "AlertEventType.h"

@implementation AlertEventDODetail

@synthesize alertId, alertName, sDesc;
@synthesize severityId, startDate, endDate, alertListId;
@synthesize startTime, endTime, sevImgPath;

@end


@implementation AlertEventDO

@synthesize alertDetailsArray;
@synthesize catId, catName;

@end


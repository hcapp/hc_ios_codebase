//
//  EventsMapViewController.h
//  HubCiti
//
//  Created by Keshava on 7/24/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>

@interface EventsMapViewController : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate,CustomizedNavControllerDelegate>
{
    MKMapView *mapView;
    NSArray *latitude;
    NSArray *longitude;
    NSArray *address1;
    NSArray *address2;
    NSMutableArray *annArray;
    UIWebView *googleMapView;
}

@property(nonatomic,strong)NSArray *longitude;
@property(nonatomic,strong)NSArray *latitude;
@property(nonatomic,strong)NSArray *address1;
@property(nonatomic,strong)NSArray *address2;

@end

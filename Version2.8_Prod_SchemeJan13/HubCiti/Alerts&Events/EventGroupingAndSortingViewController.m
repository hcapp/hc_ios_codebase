//
//  EventGroupingAndSortingViewController.m
//  HubCiti
//
//  Created by ionnor on 11/27/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "EventGroupingAndSortingViewController.h"
#import  "QuartzCore/QuartzCore.h"

NSDictionary *viewDictionary;
float tableHeight = 44.0, sectionHeight = 20.0;

@interface EventGroupingAndSortingViewController ()

@end

@implementation EventGroupingAndSortingViewController
@synthesize citiesDic,arrCities,arrCitiId,selectedCitiIds,retailId,retailLocationId,isRetailerEvent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setAccessibilityLabel:@"EventsSort"];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    UIButton *btn = [UtilityManager customizeBarButtonItem:@"Done"];
    [btn addTarget:self action:@selector(doneButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = mainPage;
    [self.navigationItem.leftBarButtonItem setAccessibilityLabel:@"Done"];
    //[mainPage release];
    
    UIButton *btncancel = [UtilityManager customizeBarButtonItem:NSLocalizedString(@"Cancel",@"Cancel")];
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        tableHeight = 60.0;
        sectionHeight = 30.0;
        btncancel.frame = CGRectMake(0, 0, 80, btn.frame.size.height);
    }
    
    [btncancel addTarget:self action:@selector(cancelButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* btncancelbar = [[UIBarButtonItem alloc] initWithCustomView:btncancel];
    self.navigationItem.rightBarButtonItem = btncancelbar;
    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"Cancel"];
   // [btncancelbar release];

    
    self.navigationItem.title = @"Group & Sort";
    
    
    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([RegionApp isEqualToString:@"1"]) {
    [self fetchCityPreference];
    }
    
    arrGroups = [[NSMutableArray alloc]initWithObjects:@"Event Date",@"Alphabetically",@"Type",nil];
   
    [self setViewForGroupingNSorting];
}



-(void)fetchCityPreference{
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId><userId>%@</userId><module>Events</module>", [defaults  valueForKey:KEY_HUBCITIID],[defaults  valueForKey:KEY_USERID]];
    
    if(isRetailerEvent == TRUE)
    {
        [xmlStr appendFormat:@"<retailId>%@</retailId><retailLocationId>%@</retailLocationId>",retailId,retailLocationId];
    }
    
    else{
        
        if([defaults valueForKey:KEY_MITEMID]){
            [xmlStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];}
    
        else{
            [xmlStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        }
    }
    
    [xmlStr appendFormat:@"</UserDetails>"];
    
 
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercitypref",BASE_URL];
    
    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    [self parseCityPreference:responseXML];
    ReleaseAndNilify(xmlStr);
    
}

-(void)parseCityPreference:(NSString *)responseXml{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
    {
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        arrCities =[[NSMutableArray alloc]init];
        arrCitiId =[[NSMutableArray alloc]init];
        TBXMLElement *cityListElement = [TBXML childElementNamed:@"cityList" parentElement:tbXml.rootXMLElement];
        if(cityListElement)
        {
            cityFlag = 1;
            TBXMLElement *CityElement = [TBXML childElementNamed:@"City" parentElement:cityListElement];
            while (CityElement)
            {
                
                TBXMLElement *cityIdElement = [TBXML childElementNamed:@"cityId" parentElement:CityElement];
                TBXMLElement *cityNameElement = [TBXML childElementNamed:@"cityName" parentElement:CityElement];
                
                if (cityIdElement) {
                    
                        [arrCitiId addObject:[TBXML textForElement:cityIdElement]];
                }
                
                if (cityNameElement) {
                    
                        [arrCities addObject:[TBXML textForElement:cityNameElement]];
                }
                CityElement = [TBXML nextSiblingNamed:@"City" searchFromElement:CityElement];            }
        }
        
        
        if ([arrCities count]!=0) {
            citiesDic = [[NSDictionary alloc] initWithObjects:arrCitiId forKeys:arrCities];
        }
        
        
        if ([defaults valueForKey:@"SelectedCityIds"]==nil) {
            
            [defaults setValue:arrCitiId forKey:@"SelectedCityIds"];
        }
        
        selectedCitiIds = [[NSMutableArray alloc]initWithArray:[defaults valueForKey:@"SelectedCityIds"]];
    }
    
    else
    {
                TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        cityFlag=0;
        
    }
//    arrCities =[NSMutableArray arrayWithObjects:@"Texas", @"Dalas", @"Rockwall", nil];
//    arrCitiId = [NSMutableArray arrayWithObjects:@"1", @"2", @"3", nil];
//    citiesDic = [[NSDictionary alloc] initWithObjects:arrCitiId forKeys:arrCities];
//    cityFlag = 1;
    if ([defaults valueForKey:@"SelectedCityIds"]==nil) {
        
        [defaults setValue:arrCitiId forKey:@"SelectedCityIds"];
    }
    
    selectedCitiIds = [[NSMutableArray alloc]initWithArray:[defaults valueForKey:@"SelectedCityIds"]];
}





-(void)cancelButtonTouched:(id)sender
{
     [self.navigationController popViewControllerAnimated:NO];
}

-(void)doneButtonTouched:(id)sender
{
    [defaults setValue:@"YES" forKey:@"isComingFromGroupingandSorting"];
    // Check the selected group
    if(groupSelectionVal == 0)
        [defaults setValue:@"date" forKey:@"GroupEventBy"];
    else if(groupSelectionVal == 1)
        [defaults setValue:@"atoz" forKey:@"GroupEventBy"];
    else if(groupSelectionVal == 2)
        [defaults setValue:@"type" forKey:@"GroupEventBy"];
    
    // Check the Selected Sorting Value
    if(sortSelectionval == 0){
        [defaults setValue:@"date" forKey:@"SortEventBy"];
        [defaults setValue:selectedDOB forKey:@"SelectedEventDate"];
        [defaults setValue:nil forKey:@"SelectedCityIds"];
    }
    else if(sortSelectionval == 1){
        [defaults setValue:@"name" forKey:@"SortEventBy"];
        [defaults setValue:nil forKey:@"SelectedCityIds"];
    }
    else if(sortSelectionval == 2){
        [defaults setValue:@"distance" forKey:@"SortEventBy"];
        [defaults setValue:nil forKey:@"SelectedCityIds"];
    }
    else if(sortSelectionval == 3){
        [defaults setValue:@"City" forKey:@"SortEventBy"];
        
        if ([selectedCitiIds count]==0) {
            [defaults setValue:nil forKey:@"SelectedCityIds"];
            [defaults setValue:nil forKey:@"commaSeperatedCities"];
        }
        else{
            [defaults setValue:selectedCitiIds forKey:@"SelectedCityIds"];
            NSMutableString *requestStr = [[NSMutableString alloc] init];
            for (int i=0; i<[selectedCitiIds count]; i++) {
                
                [requestStr appendFormat:@"%@,", selectedCitiIds[i]];
            }
            
            if ([requestStr hasSuffix:@","])
                [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
            [defaults setValue:requestStr forKey:@"commaSeperatedCities"];
        }
        
        
    }

    
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)setViewForGroupingNSorting
{
    
    if(cityFlag ==1 && [RegionApp isEqualToString:@"1"])
    {
        arrSorting = [[NSMutableArray alloc]initWithObjects:@"Date",@"Name",@"Distance",@"City",nil];
    }
    else{
        arrSorting = [[NSMutableArray alloc]initWithObjects:@"Date",@"Name",@"Distance",nil];
    }
    
    
    
    NSString *strG = [defaults valueForKey:@"GroupEventBy"];
    NSString *strS = [defaults valueForKey:@"SortEventBy"];
 
    if([strG isEqualToString:@"date"])
        groupSelectionVal = 0;
    else if([strG isEqualToString:@"atoz"])
        groupSelectionVal = 1;
    else if([strG isEqualToString:@"type"])
        groupSelectionVal = 2;
    
    if([strS isEqualToString:@"date"])
        sortSelectionval = 0;
    else if([strS isEqualToString:@"name"])
        sortSelectionval = 1;
    else if([strS isEqualToString:@"distance"])
        sortSelectionval = 2;
    else if([strS isEqualToString:@"City"])
        sortSelectionval = 3;
    
    //table height = 2 sections height + row height * arrays count
    tblGroupingNSorting = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,SCREEN_HEIGHT-60) style:UITableViewStylePlain];
    tblGroupingNSorting.dataSource=self;
    tblGroupingNSorting.delegate=self;
    [tblGroupingNSorting setAccessibilityLabel:@"sortEventsTable"];
    [tblGroupingNSorting setAccessibilityIdentifier:@"eventsTableSort"];
    [tblGroupingNSorting setBackgroundColor:[UIColor whiteColor]];
    tblGroupingNSorting.tableFooterView.hidden=YES;


    [self.view addSubview:tblGroupingNSorting];
    
    [tblGroupingNSorting reloadData];

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return tableHeight;
    }
    else
        return 70.0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return sectionHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
        return [arrGroups count];
    else
        return [arrSorting count];
    
    return 0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 18)];
    if(section==0)
        [lblCatName setText:@" Group Items by"];
    else
        [lblCatName setText:@" Sort Items by"];
    
    [lblCatName setTextColor:[UIColor blackColor]];
    [lblCatName setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        [lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
    else
        [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
    
    return lblCatName;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellEventListView";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.backgroundColor =  [UIColor clearColor];
        
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }

    }
    
        
    UILabel *lblEvtDate;
    if(indexPath.section == 0)
    {
        lblEvtDate = [[UILabel alloc]init];
        [lblEvtDate setText:[arrGroups objectAtIndex:indexPath.row]];
        [lblEvtDate setTextColor:[UIColor blackColor]];
        [lblEvtDate setBackgroundColor:[UIColor clearColor]];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
        else
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        [cell.contentView addSubview:lblEvtDate];
       // [lblEvtDate release];

        if(indexPath.row == groupSelectionVal)
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        lblEvtDate = [[UILabel alloc]init];
        [lblEvtDate setText:[arrSorting objectAtIndex:indexPath.row]];
        [lblEvtDate setTextColor:[UIColor blackColor]];
        [lblEvtDate setBackgroundColor:[UIColor clearColor]];
        float contentHeight = 45.0;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad) {
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
            contentHeight = 61.0;
        }
        else
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        [cell.contentView addSubview:lblEvtDate];
       // [lblEvtDate release];
        
        if(isExpanded==TRUE && indexPath.row>([arrSorting indexOfObject:@"City"])){
            
            UIButton *content ;
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                content = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, contentHeight)];
            }
            else
            {
                content = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
            }
            [content setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
            content.tag = indexPath.row;
            [content addTarget:self action:@selector(didSelectCity:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            if ([selectedCitiIds containsObject:[citiesDic valueForKey:[arrSorting objectAtIndex:content.tag]]]) {
                checkButton = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOn.png"]];
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    checkButton.frame = CGRectMake(10, 15, 20, 20);
                }
                else
                {
                    checkButton.frame = CGRectMake(10, 15, 30, 30);
                }
                
                checkButton.backgroundColor =[UIColor clearColor];
            }
            
            else{
                checkButton = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOff.png"]];
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    checkButton.frame = CGRectMake(10, 15, 20, 20);
                }
                else
                {
                    checkButton.frame = CGRectMake(10, 15, 30, 30);
                }

                checkButton.backgroundColor =[UIColor clearColor];
                
                
            }
            
            UILabel *lblCiti ;
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                lblCiti=[[UILabel alloc]initWithFrame:CGRectMake(40, 0, SCREEN_WIDTH, 45)];
                [lblCiti setFont:[UIFont boldSystemFontOfSize:13]];

            }
            else
            {
                lblCiti=[[UILabel alloc]initWithFrame:CGRectMake(50, 0, SCREEN_WIDTH, 70)];
                [lblCiti setFont:[UIFont boldSystemFontOfSize:18]];

            }
                        [lblCiti setText:[arrSorting objectAtIndex:indexPath.row]];
            [lblCiti setTextColor:[UIColor blackColor]];
            [lblCiti setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
            
            
            [content addSubview:checkButton];
            [content addSubview:lblCiti];
            [cell.contentView addSubview:content];
           // [content release];
        }
        
        
        
        if(indexPath.row == sortSelectionval)
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        else
            cell.accessoryType = UITableViewCellAccessoryNone;

    }
    
    lblEvtDate.translatesAutoresizingMaskIntoConstraints = NO;
    viewDictionary = NSDictionaryOfVariableBindings(lblEvtDate);
    [self setConstraints:cell];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
        groupSelectionVal = (int)indexPath.row;
    else{
        if(indexPath.section==1 && cityFlag==1){
            
            if(indexPath.row == [arrSorting indexOfObject:@"City"])
            {

                if (isExpanded==FALSE )
                {
                    isExpanded = TRUE;
                    [arrSorting addObjectsFromArray:arrCities];

                }
                
            }
            
            else{
                [arrSorting removeObjectsInArray:[citiesDic allKeys]];
                isExpanded = FALSE;
                if ([selectedCitiIds count]>0) {
                    [selectedCitiIds removeAllObjects];
                }
                
                [selectedCitiIds addObjectsFromArray:[citiesDic allValues]];
                
            }
            
        }
        sortSelectionval = (int)indexPath.row;
        if (sortSelectionval==0) {
            [self showDatePicker];
        }
    }
    
    [tblGroupingNSorting reloadData];
}


-(void)showDatePicker{

    
    int x_origin = 0;
    int y_origin ;
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        y_origin=((self.view.frame.size.height/2)-50);
    }
    else
    {
        y_origin=(3*(self.view.frame.size.height/4)-50);
    }
    
    NSDate *displayDate=[[NSDate alloc]init];
    

    
    
    UIBarButtonItem *done = [[UIBarButtonItem alloc]init];
    
    UIButton *nextBtn = [UtilityManager customizeBarButtonItem:@"Done"];
    [nextBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [nextBtn addTarget:self action:@selector(dateSelected) forControlEvents:UIControlEventTouchUpInside];
    [done setCustomView:nextBtn];
    //[nextBtn ;
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:done, nil];
    
    dobToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(x_origin, y_origin, [[UIScreen mainScreen] bounds].size.width, 50)];
    
    [dobToolBar setBackgroundColor:[UIColor blackColor]];
    [dobToolBar setBarTintColor:[UIColor blackColor]];
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {dobHolder = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/2,[[UIScreen mainScreen] bounds].size.width, self.view.frame.size.height/2)];
    }
    else
    {
        dobHolder = [[UIView alloc]initWithFrame:CGRectMake(0, 3*(self.view.frame.size.height/4),[[UIScreen mainScreen] bounds].size.width, 3*(self.view.frame.size.height/4))];
    }
    dobHolder.backgroundColor = [UIColor lightGrayColor];
    
    dobPicker = [[UIDatePicker alloc]init];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {dobPicker.bounds=dobHolder.bounds;
    }
    else
    {
        dobPicker.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, VARIABLE_HEIGHT(100));
    }
    
    dobPicker.datePickerMode= UIDatePickerModeDate;
    //    dobPicker.maximumDate = [NSDate date];
    if (displayDate!=nil) {
        [dobPicker setDate:displayDate];
    }
    [dobHolder addSubview:dobPicker];
    [self.view addSubview:dobToolBar];
    [self.view addSubview:dobHolder];
    [dobToolBar setItems:toolbarItems];
    
//    [dobToolBar release];
//    [dobPicker release];
//    [dobHolder release];
    
}




-(void)dateSelected
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeStyle:NSDateFormatterNoStyle];
    [df setDateStyle:NSDateFormatterMediumStyle];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [df setLocale:usLocale];
    
    
    selectedDOB = [[NSString alloc]initWithFormat:@"%@",
                   [df stringFromDate:dobPicker.date]];
    
    dobHolder.hidden=YES;
    dobPicker.hidden=YES;
    dobToolBar.hidden=YES;    
    
    
}

-(void)didSelectCity:(id)sender
{
    
    if([[citiesDic allValues] count]>= 1)
    {
        UIButton *btnCity = (UIButton*)sender;
        
        
        if ([selectedCitiIds containsObject:[citiesDic valueForKey:[arrSorting objectAtIndex:btnCity.tag]]]) {
            
            [selectedCitiIds removeObject:[citiesDic valueForKey:[arrSorting objectAtIndex:btnCity.tag]]];
            
        }
      
        else{
            [selectedCitiIds addObject:[citiesDic valueForKey:[arrSorting objectAtIndex:btnCity.tag]]];
            
        }
        [tblGroupingNSorting reloadData];
    }
    
}

-(void)setConstraints:(UITableViewCell*)cell
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        // Vertical constrains
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(20)-[lblEvtDate(20)]"] options:0 metrics:0 views:viewDictionary]];
        
        // Horizontal constraints
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[lblEvtDate(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
    }
    else
    {
        // Vertical constrains
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(12)-[lblEvtDate(20)]"] options:0 metrics:0 views:viewDictionary]];
        
        // Horizontal constraints
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[lblEvtDate(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
}

@end

//
//  EventGroupingAndSortingViewController.h
//  HubCiti
//
//  Created by ionnor on 11/27/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventGroupingAndSortingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *tblGroupingNSorting;
    NSMutableArray *arrGroups;
    NSMutableArray *arrSorting;
    int groupSelectionVal;
    int sortSelectionval;
    int cityFlag;
    UIImageView *checkButton;
    BOOL isExpanded;

    UILabel *dobLabel;
    UIView *dobHolder;
    UIDatePicker *dobPicker;
    NSString *selectedDOB,*serverDOB;
    UIToolbar *dobToolBar;

    
}
@property(nonatomic,strong)NSDictionary *citiesDic;

@property(nonatomic,strong)NSMutableArray *arrCities;

@property(nonatomic,strong)NSMutableArray *arrCitiId;

@property(nonatomic,strong)NSMutableArray *selectedCitiIds;

@property(nonatomic,strong) NSString *retailId;

@property(nonatomic,strong) NSString *retailLocationId;

@property(nonatomic) BOOL isRetailerEvent;

@property(nonatomic,strong) NSString *fundraiserId;

@end

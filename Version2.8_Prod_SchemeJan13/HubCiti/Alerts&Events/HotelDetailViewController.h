//
//  HotelDetailViewController.h
//  HubCiti
//
//  Created by ionnor on 11/27/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelDetailViewController : UIViewController<CustomizedNavControllerDelegate>
{
    UITextView *txtEventDetails;
    NSArray *arrHotelDetails;
    UIButton *btnAvailability;
    UIButton *btnBookNow;
}

-(void)DetailsButtonClicked:(id)sender;

@end

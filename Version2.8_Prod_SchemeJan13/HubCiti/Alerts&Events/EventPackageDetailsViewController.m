//
//  EventPackageDetailsViewController.m
//  HubCiti
//
//  Created by ionnor on 11/26/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "EventPackageDetailsViewController.h"

NSDictionary *viewDictionary;

@interface EventPackageDetailsViewController ()

@end

@implementation EventPackageDetailsViewController

@synthesize strPkgDes,strPkgName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
      
       self.navigationItem.hidesBackButton = YES;
    //[back release];

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)setViewforDescription
{
    // Add Event Details
    txtPkgDesc = [[UITextView alloc] init];
    
    
    //The rounded corner part, where you specify your view's corner radius:
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        [txtPkgDesc.layer setBorderColor:[[[UIColor blackColor] colorWithAlphaComponent:0.5] CGColor]];
        [txtPkgDesc.layer setBorderWidth:0.7];
        txtPkgDesc.layer.cornerRadius = 10;
    }
    else
    {
        [txtPkgDesc.layer setBorderColor:[[[UIColor blackColor] colorWithAlphaComponent:0.5] CGColor]];
        [txtPkgDesc.layer setBorderWidth:0.5];
        txtPkgDesc.layer.cornerRadius = 5;
    }
    
    txtPkgDesc.clipsToBounds = YES;
    txtPkgDesc.editable=NO;
    [self.view addSubview:txtPkgDesc];
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:strPkgName forView:self withHambergur:NO];

   
    txtPkgDesc.text= @"";
    txtPkgDesc.text = strPkgDes;
    txtPkgDesc.translatesAutoresizingMaskIntoConstraints = NO;
    viewDictionary = NSDictionaryOfVariableBindings(txtPkgDesc);
    [self setConstraints];
}

-(void)setConstraints
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        // Vertical constrain
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(80)-[txtPkgDesc(%f)]",SCREEN_HEIGHT-230] options:0 metrics:0 views:viewDictionary]];
        
        // Horizontal constraint
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(30)-[txtPkgDesc(%f)]",SCREEN_WIDTH-90] options:0 metrics:0 views:viewDictionary]];
    }
    else
    {
        // Vertical constrains
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(20)-[txtPkgDesc(%f)]",SCREEN_HEIGHT-100] options:0 metrics:0 views:viewDictionary]];
        
        // Horizontal constraint
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[txtPkgDesc(%f)]",SCREEN_WIDTH - 20] options:0 metrics:0 views:viewDictionary]];
    }

}

-(void)viewWillAppear:(BOOL)animated
{
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [super viewWillAppear:animated];
    [self setViewforDescription];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

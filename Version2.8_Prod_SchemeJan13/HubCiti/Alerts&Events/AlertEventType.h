//
//  AlertEventType.h
//  HubCiti
//
//  Created by Anjana on 9/10/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

/************************************************
Class to hold the details of alert and event types.
*************************************************/

@interface AlertEventDODetail : NSObject
{
    NSString    *alertId, *alertName, *sDesc;
    NSString    *severityId, *startDate, *endDate, *alertListId;
    NSString    *startTime, *endTime, *sevImgPath;
}
@property (nonatomic, strong) NSString *alertId, *alertName, *sDesc;
@property (nonatomic, strong) NSString *severityId, *startDate, *endDate, *alertListId;
@property (nonatomic, strong) NSString *startTime, *endTime, *sevImgPath;
@end


/************************************************
 Class to hold the alert and event data objects.
 header name which holds the name of each categorysent.
 alertArray holds the object of AlertEventDODetail.
 *************************************************/
@interface AlertEventDO : NSObject
{
    NSString    *catId, *catName;
    NSMutableArray *alertDetailsArray;
}
@property (nonatomic,strong) NSMutableArray *alertDetailsArray;
@property (nonatomic, strong) NSString  *catId, *catName;


@end


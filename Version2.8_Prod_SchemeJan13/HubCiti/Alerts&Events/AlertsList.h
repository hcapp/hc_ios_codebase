//
//  AlertsList.h
//  HubCiti
//
//  Created by Anjana on 9/6/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum webServicescall{
    alertlist
    
}WebRequests;

@interface AlertsList : UIViewController<UITableViewDataSource , UITableViewDelegate,CustomizedNavControllerDelegate>
{
    IBOutlet UITableView *tableView_Alert;
    
    NSMutableArray *alertEventArray;//Array to hold the Data objects for alerts&evnets.
    WebRequests iWebRequestType;
    int nextPage, lastVisitedRecord;
    
    NSString *highSevImgPath, *medSevImgPath, *lowSevImgPath;
    
}

@end

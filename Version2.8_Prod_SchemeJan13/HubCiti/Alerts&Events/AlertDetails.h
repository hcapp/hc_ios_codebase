//
//  AlertDetails.h
//  HubCiti
//
//  Created by Ajit Nadig on 11/21/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertEventType.h"
#import "SdImageView.h"

@interface AlertDetails : UIViewController<CustomizedNavControllerDelegate>
{
    AlertEventDODetail *alertDetails;
    SdImageView *severityImg;
     UILabel *startDate, *endDate;
     UITextView *sDesc;
     UILabel *startTime, *endTime;
}
@property (nonatomic, strong)AlertEventDODetail *alertDetails;

@end

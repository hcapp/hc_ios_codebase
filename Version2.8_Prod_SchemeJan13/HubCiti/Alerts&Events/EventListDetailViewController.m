//
//  EventListDetailViewController.m
//  HubCiti
//
//  Created by ionnor on 11/25/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "EventListDetailViewController.h"
#import "MainMenuViewController.h"
#import "EventHotelListViewController.h"
#import "EventDetailsDO.h"
#import "EventsCategoryDO.h"
#import "SpecialOffersViewController.h"
#import "EventPackageDetailsViewController.h"
#import "EventsMapViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "RetailerSummaryViewController.h"
#import "MainMenuViewController.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "AppDelegate.h"
#import "WebBrowserViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
//#import "EventDispalyViewController.h"



NSDictionary *viewDictionary;

@interface EventListDetailViewController (){
    int flagAppsite;
    NSMutableString *businessName ;
    
}

@end

@implementation EventListDetailViewController

static int objIndex = 0;

@synthesize strEventDescription,isHotelAvailable,isPackageAvailable;
@synthesize evtArrAddress,evtArrAddress1,evtArrAddress2,evtArrRetailerLogo,evtArrRetailers,evtArrRetailersAddress,evtLatitude,evtLongitude,evtStrEventName,evtArrRetailerId,evtArrRetailerLocId;
@synthesize weekDayNumber;
@synthesize eventStore;
@synthesize evtLocation,anyVC,emailSendingVC;
@synthesize bandEvents;
bool isAppSiteFlag;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        eventStore= [[EKEventStore alloc] init];
        if (weekDayNumber == nil) {//assigning number to each weekday - in order to
            weekDayNumber = [[NSMutableDictionary alloc]init];
            
            [weekDayNumber setObject:[NSNumber numberWithInt:1] forKey:@"Sunday"];
            [weekDayNumber setObject:[NSNumber numberWithInt:2] forKey:@"Monday"];
            [weekDayNumber setObject:[NSNumber numberWithInt:3] forKey:@"Tuesday"];
            [weekDayNumber setObject:[NSNumber numberWithInt:4] forKey:@"Wednesday"];
            [weekDayNumber setObject:[NSNumber numberWithInt:5] forKey:@"Thursday"];
            [weekDayNumber setObject:[NSNumber numberWithInt:6] forKey:@"Friday"];
            [weekDayNumber setObject:[NSNumber numberWithInt:7] forKey:@"Saturday"];
            
        }
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    // displaying share button
    
    
    
    
    //customize back button
    
    
    self.navigationItem.hidesBackButton = YES;
    // [shareButton release];
    //[back release];
    
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    //UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    
    //[mainPage release];
    
    btnLocation = [[UIButton alloc]init];
    btnCalendar=[[UIButton alloc ]init];
    btnHotel = [[UIButton alloc]init];
    btnMoreInfo = [[UIButton alloc]init];
    btnPackage = [[UIButton alloc]init];
    btnPurchaseTicket = [[UIButton alloc]init];
    btnLogistics = [[UIButton alloc]init];
    flagAppsite=1;
    [self Request_fetcheventdetail];
    businessName = [[NSMutableString alloc]init];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];

    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    //@added for appise req
    if (evtID==NULL) {
        evtID=[[NSMutableArray alloc]init];
    }
    [super viewWillAppear:animated];
}

-(void)addShareButton{
    // Create down menu button
    UIImageView *homeLabel;
    
    
    // Create up menu button
    homeLabel = [self createHomeButtonView];
    
    DWBubbleMenuButton *upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
                                                                                          self.view.frame.size.height - homeLabel.frame.size.height - 50.f,
                                                                                          homeLabel.frame.size.width,
                                                                                          homeLabel.frame.size.height)
                                                            expansionDirection:DirectionUp];
    upMenuView.homeButtonView = homeLabel;
    
    upMenuView.delegate = self;
    
    [upMenuView addButtons:[self createDemoButtonArray]];
    
    [self.view addSubview:upMenuView];
    
    [self.view bringSubviewToFront:upMenuView];
    
    upMenuView.onLogOutPressed =^(){
        
        [self logOutPressed];
    } ;
    
    
}




- (UIImageView *)createHomeButtonView {
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
    label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}

- (NSArray *)createDemoButtonArray {
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    
    int i = 0;
    for (NSString *title in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:title] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        
        button.clipsToBounds = YES;
        button.tag = i++;
        
        [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
}

- (void)test:(UIButton *)sender {
    
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
    [self shareClicked];
    
    switch (sender.tag) {
        case 0:{//facebook
            anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
            anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"EVENT_SHARE"];
            //[self.view addSubview:anyVC.view];
            [UIView transitionWithView:self.view duration:0.5
                               options:UIViewAnimationOptionTransitionCurlDown
                            animations:^ { [self.view addSubview:anyVC.view]; }
                            completion:nil];
        }
            break;
        case 1:{//twitter
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    if (result == SLComposeViewControllerResultCancelled) {
                        DLog(@"Twitter Result: canceled");
                    } else {
                        DLog(@"Twitter Result: sent");
                        NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><eventId>%@</eventId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:EVENTID]];
                        
                        [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
                    }
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        
                        [controller.view removeFromSuperview];
                    } else
                        [controller dismissViewControllerAnimated:YES completion:Nil];
                };
                controller.completionHandler =myBlock;
                
                //Adding the Text to the facebook post value from iOS
                [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                
                //Adding the URL to the facebook post value from iOS
                
                //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                
                //Adding the Image to the facebook post value from iOS
                
                [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    [self.view addSubview:controller.view];
                    [self.view bringSubviewToFront:controller.view];
                }else
                    [self presentViewController:controller animated:YES completion:Nil];
                
                
            }
            else {
                [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
               
            }
        }
            
            
            break;
        case 2:{//text
            
            if ([self hasCellularCoverage])
            {
                if ([MFMessageComposeViewController canSendText]) {
                    
                    MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
                    [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    smsComposerController.messageComposeDelegate = self;
                    [self presentViewController:smsComposerController animated:YES completion:nil];
                }
            }
            else
            {
                [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
            }
            
            
        }
            break;
        case 3:{//email
            //            [defaults setValue:EVENT_SHARE forKey:KEY_SHARE_TYPE];
            
            
            emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
            emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"EVENT_SHARE"];
            __typeof(self) __weak  obj = self;
            emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                
                [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
            } ;
            
            [emailSendingVC loadMail];
        }
            break;
            
        default:
            [defaults setBool:NO forKey:BottomButton];
            break;
    }
    
}

- (UIButton *)createButtonWithName:(NSString *)imageName {
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button sizeToFit];
    
    [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}



- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView{
    
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    
    if ([[[inRequest URL] scheme] isEqual:@"mailto"]) {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,
                                                                                                                   (CFStringRef)value,
                                                                                                                   CFSTR("")));
                    //[value ;
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            return NO;
            
        }
        
        else {
            
            [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
            
            
        }
        
        return NO;
        
    }
    
    
    //    if ([[inRequest URL] fragment]) {
    //
    //        return YES;
    //        [inWeb stringByEvaluatingJavaScriptFromString:@"window.location.hash='#hashtag'"];
    //
    //        return YES;
    //    }
    //
    else  if ([[[inRequest URL] scheme] isEqual:@"tel"]) {
        NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
        
        NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
        NSString *phoneString  = [[defaultRecipient componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        NSString *phoneNumber = [NSString stringWithFormat:@"tel:%@", phoneString];
        NSURL    *url= [NSURL URLWithString:phoneNumber];
        
        UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        webview.alpha = 0.0;
        
        [webview loadRequest:[NSURLRequest requestWithURL:url]];
        
        // Assume we are in a view controller and have access to self.view
        [self.view insertSubview:webview belowSubview:eventDetailScrollView];
        // [webview release];
        return NO;
        
    }
    
    else if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        
        NSString *url = [[inRequest URL]absoluteString];
        [defaults setValue:url forKey:KEY_URL];
        [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
        
        NSURL* urls = [NSURL URLWithString:[[inRequest URL]absoluteString]];
        
        NSString *urlString=[[inRequest URL]absoluteString];
        
        if ([[urls absoluteString] containsString:@"itunes.apple.com"]) {
            [UtilityManager customUrlLoad:urls];
        }
        
        else if ([urlString containsString:@"3000.htm"]) {
            //NSString *name =[NSString stringWithFormat:@"%@",[urls.pathComponents objectAtIndex:3]];
            
            
            
            if([urlString containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
                }
                else{
                    
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    splOfferVC.isEventLogisticsFlag = 1;
                    [HubCitiAppDelegate setIsLogistics:YES];                   

                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            else{
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        else{
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
            
        }
        return NO;
    }
    
    
    
    return YES;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];// ModalViewControllerAnimated:YES];
}

-(void)setViewAccordingToData
{
    EventDetailsDO *iEventDetailsDO;
    iEventDetailsDO = [arrEventDetails objectAtIndex:0];
    
    
    
    
    // Set Navigation Bar Title
    NSMutableString *strTitle = [[NSMutableString alloc]init];
    [strTitle appendFormat:@"%@",iEventDetailsDO.eventName];
    
    //CGRect frame = CGRectMake(50, 0, 200, self.navigationController.navigationBar.frame.size.height);
    //    UILabel *NavtitleViewlabel = [[UILabel alloc] initWithFrame:frame] ;
    //    NavtitleViewlabel.backgroundColor = [UIColor clearColor];
    //    NavtitleViewlabel.font = [UIFont boldSystemFontOfSize:16.0];
    //    NavtitleViewlabel.textAlignment = NSTextAlignmentCenter;
    //    NavtitleViewlabel.textColor = [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]];
    //    NavtitleViewlabel.numberOfLines=2;
    //    NavtitleViewlabel.lineBreakMode = NSLineBreakByWordWrapping;
    //    NavtitleViewlabel.text = strTitle;
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:strTitle forView:self withHambergur:NO];

    
    
    //[strTitle release];
    
    
    eventDetailScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [eventDetailScrollView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:eventDetailScrollView];
    
    float yPos = 2;
    float startX = 10;
    float width = SCREEN_WIDTH-20;
    
    // iPad conversion
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        yPos = 5;
        startX = 150;
        width = SCREEN_WIDTH-280;
    }
    
    if(iEventDetailsDO.imgPath)
    {
        // Add Event Image
        SdImageView *asyncImageView = [[SdImageView alloc] init];
        if ( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            asyncImageView.frame = CGRectMake(startX, yPos, width, 300);
        }
        else
        {
            asyncImageView.frame = CGRectMake(startX, yPos, width, 150);
        }
        asyncImageView.backgroundColor = [UIColor clearColor];
        // asyncImageView.layer.cornerRadius = 5.0f;
        [asyncImageView loadImage:iEventDetailsDO.imgPath];
        
        [eventDetailScrollView addSubview:asyncImageView];
        //[asyncImageView release];
        
        yPos = asyncImageView.frame.origin.y+asyncImageView.frame.size.height;
    }
    
    //Location
    {
        btnLocation.frame = CGRectMake(startX, yPos+5, width,40);
        btnLocation.clipsToBounds = YES;
        
        btnLocation.layer.cornerRadius = 5;
        
        btnLocation.titleLabel.font =[UIFont systemFontOfSize:17];
        
        [btnLocation setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [btnLocation setTitle:@"Location" forState:UIControlStateNormal];
        
        
        [btnLocation addTarget:self action:@selector(LocationButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [btnLocation setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        btnLocation.layer.borderWidth = 1.0;
        [btnLocation setBackgroundColor:[UIColor lightGrayColor]];
        btnLocation.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
        
        
        [eventDetailScrollView addSubview:btnLocation];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPad )
        {
            yPos = yPos + btnLocation.frame.size.height+11;
        }
        else
        {
            yPos = yPos + btnLocation.frame.size.height+5;
        }
    }
    
    //Calendar
    {
        btnCalendar.frame = CGRectMake(startX, yPos+5, width,40);
        btnCalendar.clipsToBounds = YES;
        
        btnCalendar.layer.cornerRadius = 5;
        
        btnCalendar.titleLabel.font =[UIFont systemFontOfSize:16];
        
        [btnCalendar setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [btnCalendar setTitle:@"Add to Calendar" forState:UIControlStateNormal];
        
        
        [btnCalendar addTarget:self action:@selector(calendarClicked) forControlEvents:UIControlEventTouchUpInside];
        [btnCalendar setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        btnCalendar.layer.borderWidth = 1.0;
        [btnCalendar setBackgroundColor:[UIColor lightGrayColor]];
        btnCalendar.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
        
        
        [eventDetailScrollView addSubview:btnCalendar];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPad )
        {
            yPos = yPos + btnCalendar.frame.size.height+11;
        }
        else
        {
            yPos = yPos + btnCalendar.frame.size.height+5;
        }
    }
    
    
    if(iEventDetailsDO.shortDes)
    {
        // Add Event Details
        
        
        UILabel *shortDesc = [[UILabel alloc]init];
        shortDesc.lineBreakMode = NSLineBreakByWordWrapping;
        shortDesc.numberOfLines = 0;
        shortDesc.font = [UIFont fontWithName:@"Helvetica" size:13];
        shortDesc.textAlignment = NSTextAlignmentLeft;
        shortDesc.backgroundColor = [UIColor colorWithRGBInt:0xE6E6E6];
        
        if(iEventDetailsDO.shortDes)
            shortDesc.text = [NSString stringWithFormat:@" %@",iEventDetailsDO.shortDes];
        
        [eventDetailScrollView addSubview:shortDesc];
        
        UILabel *verticalLine = [[UILabel alloc]init];
        verticalLine.backgroundColor = [UIColor lightGrayColor];
        [eventDetailScrollView addSubview:verticalLine];
        
        
        
        
        UILabel *horizontalLine = [[UILabel alloc]init];
        horizontalLine.backgroundColor = [UIColor lightGrayColor];
        [eventDetailScrollView addSubview:horizontalLine];
        
        int height = [self calculateLabelHeight:iEventDetailsDO.shortDes labelWidth:width fontSize:13];
        
        // iPad conversion
        if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        {
            shortDesc.frame = CGRectMake(startX, yPos + 5, width, height);//x-125 width+225
            verticalLine.frame = CGRectMake(startX-1, yPos + 5, 2.0, height);//startX-124
            yPos = shortDesc.frame.origin.y + shortDesc.frame.size.height;
            horizontalLine.frame = CGRectMake(startX-1, yPos , shortDesc.frame.size.width, 2.0);//startX-124
            yPos+=8;
        }
        else
        {
            shortDesc.frame = CGRectMake(startX+3, yPos + 5, width, height);
            verticalLine.frame = CGRectMake(startX, yPos + 5, 2.0, height);
            yPos = shortDesc.frame.origin.y + shortDesc.frame.size.height;
            horizontalLine.frame = CGRectMake(startX, yPos , shortDesc.frame.size.width, 2.0);
            yPos+=2;
        }
        
        ReleaseAndNilify(verticalLine);
        ReleaseAndNilify(horizontalLine);
        ReleaseAndNilify(shortDesc);
        
    }
    
    
    if(iEventDetailsDO.longDes && [iEventDetailsDO.longDes length])
    {
        
        
        UIWebView *longDesc = [[UIWebView alloc]init];
        longDesc.delegate = self;
        
        longDesc.backgroundColor = [UIColor colorWithRGBInt:0xE6E6E6];
        longDesc.layer.borderColor = [UIColor grayColor].CGColor;
        longDesc.layer.borderWidth = 1.0;
        if(iEventDetailsDO.longDes){
            //            longDesc.text = [NSString stringWithFormat:@" %@",iEventDetailsDO.longDes];
            [longDesc loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#000;word-wrap: break-word'>%@",iEventDetailsDO.longDes] baseURL:nil];
        }
        longDesc.layer.cornerRadius = 5.0;
        
        [eventDetailScrollView addSubview:longDesc];
        
        UILabel *verticalLineLongDesc = [[UILabel alloc]init];
        verticalLineLongDesc.backgroundColor = [UIColor lightGrayColor];
        [eventDetailScrollView addSubview:verticalLineLongDesc];
        
        
        
        
        UILabel *horizontalLineLongDescription = [[UILabel alloc]init];
        horizontalLineLongDescription.backgroundColor = [UIColor lightGrayColor];
        [eventDetailScrollView addSubview:horizontalLineLongDescription];
        
        int height = [self calculateLabelHeight:iEventDetailsDO.longDes labelWidth:width fontSize:13];
        
        // iPad conversion
        if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        {
            longDesc.frame = CGRectMake(startX, yPos + 5, width, height);//startX-125 width+225
            verticalLineLongDesc.frame = CGRectMake(startX-1, yPos + 5, 2.0, height);//startX-124
            yPos = longDesc.frame.origin.y + longDesc.frame.size.height;
            horizontalLineLongDescription.frame = CGRectMake(startX-1, yPos , longDesc.frame.size.width, 2.0);//startX-124
            yPos+=15;
        }
        else
        {
            longDesc.frame = CGRectMake(startX+3, yPos + 5, width, height);
            verticalLineLongDesc.frame = CGRectMake(startX, yPos + 5, 2.0, height);
            yPos = longDesc.frame.origin.y + longDesc.frame.size.height;
            horizontalLineLongDescription.frame = CGRectMake(startX, yPos , longDesc.frame.size.width, 2.0);
            yPos+=2;
        }
        
        ReleaseAndNilify(verticalLineLongDesc);
        ReleaseAndNilify(horizontalLineLongDescription);
        ReleaseAndNilify(longDesc);
    }
    
    
    if(iEventDetailsDO.startDate)
    {
        UILabel *lblstartDate = [[UILabel alloc]initWithFrame:CGRectMake(startX, yPos+5, width/2, 20)];
        [lblstartDate setBackgroundColor:[UIColor clearColor]];
        [lblstartDate setFont:[UIFont boldSystemFontOfSize:13]];
        [lblstartDate setTextColor:[UIColor blackColor]];
        
        UILabel *lblendDate = [[UILabel alloc]init];
        
        // iPad conversion
        if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        {
            lblendDate.frame = CGRectMake(width/2+250, yPos+5, width/2-2, 20);
        }
        else
        {
            lblendDate.frame = CGRectMake(width/2+2, yPos+5, width/2-2, 20);
        }
        [lblendDate setBackgroundColor:[UIColor clearColor]];
        [lblendDate setFont:[UIFont boldSystemFontOfSize:13]];
        [lblendDate setTextColor:[UIColor blackColor]];
        
        NSMutableString *strDate = [[NSMutableString alloc]initWithString:@"Start Date: "];
        NSMutableString *endDate = [[NSMutableString alloc]init];
        //Changes added by keerthi
        
        if ([iEventDetailsDO.endDate isEqualToString:@"N/A"]) {
            [strDate appendFormat:@"%@",iEventDetailsDO.startDate];
            //                 [endDate appendFormat:@"%@",iEventDetailsDO.endDate];
            
        }
        else
        {
            [strDate appendFormat:@"%@",iEventDetailsDO.startDate];
            [endDate appendFormat:@"  %@",@"End Date: "];
            [endDate appendFormat:@"%@",iEventDetailsDO.endDate];
            
            
        }
        
        //        [strDate appendFormat:@"%@",iEventDetailsDO.startDate];
        
        [lblstartDate setText:strDate];
        [lblendDate setText:endDate];
        
        
        [eventDetailScrollView addSubview:lblstartDate];
        [eventDetailScrollView addSubview:lblendDate];
        
        yPos = yPos + lblstartDate.frame.size.height+5;
        
        //        [strDate release];
        //        [endDate release];
        //        [lblstartDate release];
        //        [lblendDate release];
    }
    
    if(iEventDetailsDO.startTime)
    {
        UILabel *lblstartTime = [[UILabel alloc]initWithFrame:CGRectMake(startX, yPos+5, width/2, 20)];
        [lblstartTime setBackgroundColor:[UIColor clearColor]];
        [lblstartTime setFont:[UIFont boldSystemFontOfSize:13]];
        [lblstartTime setTextColor:[UIColor blackColor]];
        
        UILabel *lblendTime = [[UILabel alloc]init];
        
        // iPad conversion
        if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        {
            lblendTime.frame = CGRectMake(width/2+250, yPos+5, width/2-2, 20);
        }
        else
        {
            lblendTime.frame = CGRectMake(width/2+2, yPos+5, width/2-2, 20);
        }
        [lblendTime setBackgroundColor:[UIColor clearColor]];
        [lblendTime setFont:[UIFont boldSystemFontOfSize:13]];
        [lblendTime setTextColor:[UIColor blackColor]];
        
        NSMutableString *strTime = [[NSMutableString alloc]initWithString:@"Start Time: "];
        NSMutableString *endTime = [[NSMutableString alloc]init];
        ////NSlog(@"The ongoing flag is %@",iEventDetailsDO.isOnGoing);
        
        if ([iEventDetailsDO.endTime isEqualToString:@"N/A"]) {
            [strTime appendFormat:@"%@",iEventDetailsDO.startTime];
            //                 [endDate appendFormat:@"%@",iEventDetailsDO.endDate];
            
        }
        else
        {
            [strTime appendFormat:@"%@",iEventDetailsDO.startTime];
            [endTime appendFormat:@"  %@%@",@"End Time: ",iEventDetailsDO.endTime];
            
        }
        
        //            [strTime appendFormat:@"%@",iEventDetailsDO.startTime];
        
        [lblstartTime setText:strTime];
        [lblendTime setText:endTime];
        
        
        [eventDetailScrollView addSubview:lblstartTime];
        [eventDetailScrollView addSubview:lblendTime];
        
        yPos = yPos + lblstartTime.frame.size.height+5;
        
        //        [strTime release];
        //        [endTime release];
        //        [lblstartTime release];
        //        [lblendTime release];
        
        
        
    }
    
    if([iEventDetailsDO.isOnGoing isEqual:@"1"] && iEventDetailsDO.recurringDays)
    {
        if([iEventDetailsDO.recurringDays length]>40){
            lblOnGngDetails=[[UILabel alloc]initWithFrame:CGRectMake(startX,yPos+3,width,40)];
            lblOnGngDetails.numberOfLines=2;
        }
        else
            lblOnGngDetails=[[UILabel alloc]initWithFrame:CGRectMake(startX,yPos+5,width,20)];
        lblOnGngDetails.textAlignment = NSTextAlignmentLeft;
        lblOnGngDetails.clipsToBounds=YES;
        lblOnGngDetails.layer.cornerRadius=5;
        lblOnGngDetails.text = iEventDetailsDO.recurringDays;
        lblOnGngDetails.font=[UIFont boldSystemFontOfSize:13.0];
        [eventDetailScrollView addSubview:lblOnGngDetails];
        yPos = yPos + lblOnGngDetails.frame.size.height+5;
        
    }
    
    
    if(iEventDetailsDO.pkgDes && [iEventDetailsDO.pkgDes length]>0)
    {
        btnPackage.frame = CGRectMake(startX, yPos+5, width,40);
        btnPackage.clipsToBounds = YES;
        
        btnPackage.layer.cornerRadius = 5;
        
        btnPackage.titleLabel.font =[UIFont systemFontOfSize:17];
        
        [btnPackage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [btnPackage setTitle:@"Package Details" forState:UIControlStateNormal];
        
        
        [btnPackage addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnPackage.tag =0;
        [btnPackage setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        btnPackage.layer.borderWidth = 1.0;
        [btnPackage setBackgroundColor:[UIColor lightGrayColor]];
        btnPackage.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
        
        
        [eventDetailScrollView addSubview:btnPackage];
        
        yPos = yPos + btnPackage.frame.size.height+5;
    }
    
    
    if(iEventDetailsDO.hotelFlag && [iEventDetailsDO.hotelFlag intValue]==1)
    {
        btnHotel.frame = CGRectMake(startX, yPos+5, width, 40);
        
        btnHotel.clipsToBounds = YES;
        
        btnHotel.layer.cornerRadius = 5;
        
        btnHotel.titleLabel.font =[UIFont systemFontOfSize:17];
        
        [btnHotel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [btnHotel setTitle:@"Hotels" forState:UIControlStateNormal];
        
        [btnHotel addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnHotel.tag =1;
        [btnHotel setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        btnHotel.layer.borderWidth = 1.0;
        [btnHotel setBackgroundColor:[UIColor lightGrayColor]];
        btnHotel.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
        
        
        yPos = yPos + btnHotel.frame.size.height+5;
        
        
        [eventDetailScrollView addSubview:btnHotel];
    }
    
    
    if(![iEventDetailsDO.logisticsURL isEqualToString:@"N/A"] && iEventDetailsDO.logisticsURL)
    {
        btnLogistics.frame = CGRectMake(startX, yPos+5, width, 40);
        
        btnLogistics.clipsToBounds = YES;
        
        btnLogistics.layer.cornerRadius = 5;
        
        btnLogistics.titleLabel.font =[UIFont systemFontOfSize:17];
        
        [btnLogistics setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [btnLogistics setTitle:@"Maps/Event Logistics" forState:UIControlStateNormal];
        
        [btnLogistics addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnLogistics.tag =2;
        [btnLogistics setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        btnLogistics.layer.borderWidth = 1.0;
        [btnLogistics setBackgroundColor:[UIColor lightGrayColor]];
        btnLogistics.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
        
        
        yPos = yPos + btnLogistics.frame.size.height+5;
        
        
        [eventDetailScrollView addSubview:btnLogistics];
    }
    
    if(iEventDetailsDO.pkgTicketURL)
    {
        btnPurchaseTicket.frame = CGRectMake(startX, yPos+5, width, 40);
        
        btnPurchaseTicket.clipsToBounds = YES;
        
        btnPurchaseTicket.layer.cornerRadius = 5;
        
        btnPurchaseTicket.titleLabel.font =[UIFont systemFontOfSize:17];
        
        [btnPurchaseTicket setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [btnPurchaseTicket setTitle:@"Purchase Ticket" forState:UIControlStateNormal];
        
        [btnPurchaseTicket addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnPurchaseTicket.tag =3;
        [btnPurchaseTicket setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        btnPurchaseTicket.layer.borderWidth = 1.0;
        [btnPurchaseTicket setBackgroundColor:[UIColor lightGrayColor]];
        btnPurchaseTicket.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
        
        
        
        yPos = yPos + btnPurchaseTicket.frame.size.height+5;
        
        [eventDetailScrollView addSubview:btnPurchaseTicket];
        
    }
    
    
    if(iEventDetailsDO.moreInfoURL)
    {
        btnMoreInfo.frame = CGRectMake(startX, yPos+5, width, 40);
        
        btnMoreInfo.clipsToBounds = YES;
        
        btnMoreInfo.layer.cornerRadius = 5;
        
        btnMoreInfo.titleLabel.font =[UIFont systemFontOfSize:17];
        
        [btnMoreInfo setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [btnMoreInfo setTitle:@"More Information" forState:UIControlStateNormal];
        
        [btnMoreInfo addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnMoreInfo.tag =4;
        [btnMoreInfo setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        btnMoreInfo.layer.borderWidth = 1.0;
        [btnMoreInfo setBackgroundColor:[UIColor lightGrayColor]];
        btnMoreInfo.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
        
        
        
        yPos = yPos + btnMoreInfo.frame.size.height+5;
        
        [eventDetailScrollView addSubview:btnMoreInfo];
    }
    
    if([iEventDetailsDO.isAppSiteFlag isEqualToString:@"1"]) //If Event is associated to Retailer
    {
        flagAppsite=0;
        locLatitude=[[NSMutableArray alloc]initWithArray:[iEventDetailsDO.latitude componentsSeparatedByString:@"|"]];
        locLongitude=[[NSMutableArray alloc]initWithArray:[iEventDetailsDO.longitude componentsSeparatedByString:@"|"]];
        eventID=[[NSMutableArray alloc]initWithObjects:[defaults valueForKey:EVENTID], nil];
        
        
        [evtID addObject:[eventID objectAtIndex:0]];
        
        [self Request_eventappsiteloc];
        
        if(arrRetailers>0){
            int i;
            [businessName appendFormat:@"%@",[arrRetailers objectAtIndex:0]];
            for(i=1;i<arrRetailers.count;i++)
                [businessName appendFormat:@",%@",[arrRetailers objectAtIndex:i]];
        }
    }
    else{
        [businessName appendFormat:@"%@",iEventDetailsDO.address];
        
    }
    
    
    [eventDetailScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, yPos+80)];
    
    [self addShareButton];
    
}
-(int)calculateLabelHeight:(NSString*)text labelWidth:(int)labelWidth fontSize:(int)fontSize
{
    CGSize constraint;
    CGSize size ;
    CGFloat heightWebview ;
    constraint = CGSizeMake(labelWidth, 20000.0f);
    //    size = [text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:fontSizeText] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    size = [text boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:fontSize], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    
    heightWebview = MAX(size.height+20, 40.0f);
    return heightWebview;
}

-(void)Request_fetcheventdetail
{
    iWebRequestState= EVENTSDETAILLIST;
    NSString *urlString;
    if([bandEvents intValue] ==1 ){
        urlString = [NSString stringWithFormat:@"%@alertevent/fetcheventdetail?eventId=%@&hubCitiId=%@",BASE_URL,[defaults valueForKey:EVENTID],[defaults valueForKey:KEY_HUBCITIID]];
        
    }
    else{
        urlString = [NSString stringWithFormat:@"%@alertevent/fetcheventdetail?eventId=%@&eventsListId=%@&hubCitiId=%@",BASE_URL,[defaults valueForKey:EVENTID],[defaults valueForKey:EVENTLISTID],[defaults valueForKey:KEY_HUBCITIID]];
    }
    
    [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
}


-(void)loadRetailerAddressView
{
    
    EventDetailsDO *iEventDetailsDO;
    iEventDetailsDO = [arrEventDetails objectAtIndex:0];
    
    tblEventHolder=[[UIView alloc]init];
    
    
    if (IS_IPHONE5)
        tblEventHolder.frame = CGRectMake(10, 10, 300, 425);
    else if (DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        tblEventHolder.frame = CGRectMake(40, 80, 680, 750);
    }
    else
        tblEventHolder.frame = CGRectMake(10, 10, 300, 350);
    
    tblEventHolder.layer.backgroundColor = [UIColor whiteColor].CGColor;
    tblEventHolder.layer.cornerRadius = 20.0;
    tblEventHolder.layer.borderColor = [UIColor blackColor].CGColor;
    tblEventHolder.layer.shadowOffset = CGSizeMake(1, 0);
    tblEventHolder.layer.shadowColor = [[UIColor blackColor] CGColor];
    tblEventHolder.layer.shadowRadius = 5;
    tblEventHolder.layer.shadowOpacity = .25;
    
    
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        cross_button=[[UIButton alloc]initWithFrame:CGRectMake(620,-3, 75, 55)];
        [cross_button setImage:[UIImage imageNamed:@"cross_icon_ipad.png"] forState:UIControlStateNormal];
    }
    else
    {
        cross_button=[[UIButton alloc]initWithFrame:CGRectMake(250,-5, 65, 45)];
        [cross_button setImage:[UIImage imageNamed:@"cross_icon.png"] forState:UIControlStateNormal];
    }
    
    [cross_button addTarget:self action:@selector(crossClicked) forControlEvents:UIControlEventTouchDown];
    
    tblEventLocations=[[UITableView alloc]init];
    if (IS_IPHONE5)
        tblEventLocations = [[UITableView alloc]initWithFrame:CGRectMake(10, 30, 280, 350) style:UITableViewStylePlain];
    else if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        tblEventLocations = [[UITableView alloc]initWithFrame:CGRectMake(40, 100, 620, 650) style:UITableViewStylePlain];
    }
    else
        tblEventLocations = [[UITableView alloc]initWithFrame:CGRectMake(10, 30, 280, 270) style:UITableViewStylePlain];
    tblEventLocations.dataSource=self;
    tblEventLocations.delegate=self;
    [tblEventLocations setBackgroundColor:[UIColor clearColor]];
    
    evtStrEventName =[[NSString alloc]initWithString:iEventDetailsDO.eventName];
    
    evtArrRetailers = [[NSArray alloc]initWithArray:arrRetailers];
    evtArrRetailersAddress = [[NSMutableArray alloc]initWithArray:arrRetailerAddress];
    evtArrRetailerLogo = [[NSMutableArray alloc]initWithArray:arrRetailerLogo];
    evtArrRetailerId = [[NSMutableArray alloc]initWithArray:arrRetailerId];
    evtArrRetailerLocId=[[NSMutableArray alloc]initWithArray:arrRetailerLocId];
    evtLatitude = [[NSMutableArray alloc]initWithArray:arrRetailerLat];
    evtLongitude = [[NSMutableArray alloc]initWithArray:arrRetailerLong];
    
    [tblEventLocations reloadData];
    [tblEventHolder addSubview:cross_button];
    [tblEventHolder addSubview:tblEventLocations];
    [self.view addSubview:tblEventHolder];
    
    
}
-(void)Request_eventappsiteloc
{
    //iWebRequestState = EVENTADDRESSCALL;
    NSString *urlString = [NSString stringWithFormat:@"%@alertevent/eventappsiteloc",BASE_URL];
    NSMutableString *reqStr=[[NSMutableString alloc]initWithFormat:@"<Retailer><userId>%@</userId><eventId>%@</eventId><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_USERID],[evtID objectAtIndex:[evtID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [locLatitude count]>0){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[locLongitude objectAtIndex:0],
         [locLatitude objectAtIndex:0]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"</Retailer>"];
    
    // [ConnectionManager establishConnectionFor:reqStr base:urlString withDelegate:self];
    NSString *response=[ConnectionManager establishPostConnection:urlString withParam:reqStr];
    [self parse_eventappsiteloc:response];
    
}


-(void)responseData:(NSString *) response
{
    if(iWebRequestState==EVENTADDRESSCALL)
    {
        [self parse_eventappsiteloc:response];
    }
    else if(iWebRequestState==EVENTSDETAILLIST)
    {
        // [self parse_eventList:response];
        [self parsenew_eventList:response];
        if([arrEventDetails count] > 0)
            [self setViewAccordingToData];
    }
    else if(iWebRequestState == RETSUMMARYS )
    {
        [self parse_retSummary:response];
    }
    else if(iWebRequestState==EVENT_SHAREINFO)
    {
        [self parse_shareEvent:response];
    }
}

-(void)parsenew_eventList:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        if(arrEventDetails)
        {
            // [arrEventDetails release];
            arrEventDetails = nil;
        }
        
        arrEventDetails = [[NSMutableArray alloc]init];
        
        
        EventDetailsDO *iEventDetailsDO = [[EventDetailsDO alloc]init];
        
        
        TBXMLElement *eventNameElement = [TBXML childElementNamed:@"eventName" parentElement:tbxml.rootXMLElement];
        TBXMLElement *shortDesElement = [TBXML childElementNamed:@"shortDes" parentElement:tbxml.rootXMLElement];
        TBXMLElement *longDesElement = [TBXML childElementNamed:@"longDes" parentElement:tbxml.rootXMLElement];
        TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:@"hubCitiId" parentElement:tbxml.rootXMLElement];
        TBXMLElement *imgPathElement = [TBXML childElementNamed:@"imgPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *busEventElement = [TBXML childElementNamed:@"busEvent" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pkgEventElement = [TBXML childElementNamed:@"pkgEvent" parentElement:tbxml.rootXMLElement];
        TBXMLElement *startDateElement = [TBXML childElementNamed:@"startDate" parentElement:tbxml.rootXMLElement];
        TBXMLElement *startTimeElement = [TBXML childElementNamed:@"startTime" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mItemExistElement = [TBXML childElementNamed:@"mItemExist" parentElement:tbxml.rootXMLElement];
        //TBXMLElement *eventCatIdElement = [TBXML childElementNamed:@"eventCatId" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *latitudeElement = [TBXML childElementNamed:@"latitude" parentElement:tbxml.rootXMLElement];
        TBXMLElement *longitudeElement = [TBXML childElementNamed:@"longitude" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *pkgDesElement = [TBXML childElementNamed:@"pkgDes" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pkgTicketURLElement = [TBXML childElementNamed:@"pkgTicketURL" parentElement:tbxml.rootXMLElement];
        TBXMLElement *hotelFlagElement = [TBXML childElementNamed:@"hotelFlag" parentElement:tbxml.rootXMLElement];
        
        
        TBXMLElement *logisticsURLElement = [TBXML childElementNamed:@"eventLgSSQRPath" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *moreInfoURLElement = [TBXML childElementNamed:@"moreInfoURL" parentElement:tbxml.rootXMLElement];
        TBXMLElement *isOnGoingElement = [TBXML childElementNamed:@"isOnGoing" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *recurringDaysElement = [TBXML childElementNamed:@"recurringDays" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *isAppSiteFlagElement = [TBXML childElementNamed:@"isAppSiteFlag" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *addressElement = [TBXML childElementNamed:@"address" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *addressElement1 = [TBXML childElementNamed:@"address1" parentElement:tbxml.rootXMLElement];
        TBXMLElement *addressElement2 = [TBXML childElementNamed:@"address2" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *locationElement = [TBXML childElementNamed:@"evtLocTitle" parentElement:tbxml.rootXMLElement];
        
        if (locationElement) {
            iEventDetailsDO.location =[TBXML textForElement:locationElement];
        }
        if(addressElement)
            iEventDetailsDO.address = [TBXML textForElement:addressElement];
        if(addressElement1)
            iEventDetailsDO.address1 = [TBXML textForElement:addressElement1];
        if(addressElement2)
            iEventDetailsDO.address2 = [TBXML textForElement:addressElement2];
        if (latitudeElement) {
            iEventDetailsDO.latitude = [TBXML textForElement:latitudeElement];
        }
        if (latitudeElement) {
            iEventDetailsDO.longitude = [TBXML textForElement:longitudeElement];
        }
        
        if(isAppSiteFlagElement)
            iEventDetailsDO.isAppSiteFlag = [TBXML textForElement:isAppSiteFlagElement];
        
        if(moreInfoURLElement)
            iEventDetailsDO.moreInfoURL = [TBXML textForElement:moreInfoURLElement];
        if(hotelFlagElement)
            iEventDetailsDO.hotelFlag = [TBXML textForElement:hotelFlagElement];
        if(pkgEventElement)
            iEventDetailsDO.pkgEvent = [TBXML textForElement:pkgEventElement];
        if(busEventElement)
            iEventDetailsDO.busEvent = [TBXML textForElement:busEventElement];
        if(eventNameElement)
            iEventDetailsDO.eventName = [TBXML textForElement:eventNameElement];
        if(shortDesElement)
            iEventDetailsDO.shortDes = [TBXML textForElement:shortDesElement];
        if(longDesElement)
            iEventDetailsDO.longDes = [TBXML textForElement:longDesElement];
        if(imgPathElement)
            iEventDetailsDO.imgPath = [TBXML textForElement:imgPathElement];
        if(hubCitiIdElement)
            iEventDetailsDO.hubCitiId = [TBXML textForElement:hubCitiIdElement];
        if(pkgDesElement)
            iEventDetailsDO.pkgDes = [TBXML textForElement:pkgDesElement];
        if(pkgTicketURLElement)
            iEventDetailsDO.pkgTicketURL = [TBXML textForElement:pkgTicketURLElement];
        
        if(startDateElement)
            iEventDetailsDO.startDate = [TBXML textForElement:startDateElement];
        
        
        if(startTimeElement)
            iEventDetailsDO.startTime = [TBXML textForElement:startTimeElement];
        
        
        if(mItemExistElement)
            iEventDetailsDO.mItemExist = [TBXML textForElement:mItemExistElement];
        
        
        if(logisticsURLElement)
            iEventDetailsDO.logisticsURL = [TBXML textForElement:logisticsURLElement];
        
        
        
        if(isOnGoingElement)
        {
            
            iEventDetailsDO.isOnGoing=[TBXML textForElement:isOnGoingElement];
            // Changes added by Keerthi
            TBXMLElement *endDateElement = [TBXML childElementNamed:@"endDate" parentElement:tbxml.rootXMLElement];
            TBXMLElement *endTimeElement = [TBXML childElementNamed:@"endTime" parentElement:tbxml.rootXMLElement];
            
            if(endDateElement)
                iEventDetailsDO.endDate = [TBXML textForElement:endDateElement];
            
            
            if(endTimeElement)
                iEventDetailsDO.endTime = [TBXML textForElement:endTimeElement];
            
            
            if(recurringDaysElement && [iEventDetailsDO.isOnGoing isEqualToString:@"1"])
                
                iEventDetailsDO.recurringDays = [TBXML textForElement:recurringDaysElement];
            
            //event calendar...
            
            TBXMLElement *recurrencePatternNameElement= [TBXML childElementNamed:@"recurrencePatternName" parentElement:tbxml.rootXMLElement];
            if(recurrencePatternNameElement)
                iEventDetailsDO.recurrencePatternName=[TBXML textForElement:recurrencePatternNameElement];
            
            if([iEventDetailsDO.recurrencePatternName isEqualToString:@"Daily"]){
                
                TBXMLElement *isWeekDayElement = [TBXML childElementNamed:@"isWeekDay" parentElement:tbxml.rootXMLElement];
                if(isWeekDayElement)
                    iEventDetailsDO.isWeekDay=[TBXML textForElement:isWeekDayElement];
            }
            
            else if([iEventDetailsDO.recurrencePatternName isEqualToString:@"Weekly"]){
                TBXMLElement *daysElement=[TBXML childElementNamed:@"days" parentElement:tbxml.rootXMLElement];
                if(daysElement)
                    days=[TBXML textForElement:daysElement];
                iEventDetailsDO.daysOfWeek = [days componentsSeparatedByString:@","];
                
            }
            
            else if([iEventDetailsDO.recurrencePatternName isEqualToString:@"Monthly"]){
                
                TBXMLElement *byDayNumberElement=[TBXML childElementNamed:@"byDayNumber" parentElement:tbxml.rootXMLElement];
                if(byDayNumberElement)
                    iEventDetailsDO.byDayNumber=[TBXML textForElement:byDayNumberElement];
                if([iEventDetailsDO.byDayNumber isEqualToString:@"true"]){
                    
                    TBXMLElement *dateOfMonthElement=[TBXML childElementNamed:@"dateOfMonth" parentElement:tbxml.rootXMLElement];
                    if(dateOfMonthElement)
                        iEventDetailsDO.dateOfMonth=[[TBXML textForElement:dateOfMonthElement]integerValue];
                }
                else{
                    TBXMLElement *everyWeekDayMonthElement=[TBXML childElementNamed:@"everyWeekDayMonth" parentElement:tbxml.rootXMLElement];
                    if(everyWeekDayMonthElement){
                        NSString *dateOfWeekArray  =[TBXML textForElement:everyWeekDayMonthElement];
                        iEventDetailsDO.everyWeekDayMonth =[dateOfWeekArray componentsSeparatedByString:@","];
                        
                    }
                    TBXMLElement *dayNumberElement=[TBXML childElementNamed:@"dayNumber" parentElement:tbxml.rootXMLElement];
                    if(dayNumberElement)
                        iEventDetailsDO.dayNumber=[[TBXML textForElement:dayNumberElement]integerValue];
                    
                    
                    
                    
                    
                    
                }
                
            }
            TBXMLElement *recurrenceIntervalElement =[TBXML childElementNamed:@"recurrenceInterval" parentElement:tbxml.rootXMLElement];
            
            if(recurrenceIntervalElement)
                iEventDetailsDO.recurrenceInterval=[[TBXML textForElement:recurrenceIntervalElement]integerValue];
            
        }
        
        
        [arrEventDetails addObject:iEventDetailsDO];
        //[iEventDetailsDO release];
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
}

-(void)parse_eventappsiteloc:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *retListElement = [TBXML childElementNamed:@"retList" parentElement:tbxml.rootXMLElement];
        if(retListElement)
        {
            //            if(arrRetailers)
            //                [arrRetailers release];
            
            arrRetailers = [[NSMutableArray alloc]init];
            
            //            if(arrRetailerAddress)
            //                [arrRetailerAddress release];
            
            arrRetailerAddress = [[NSMutableArray alloc]init];
            
            //            if(arrRetailerLogo)
            //                [arrRetailerLogo release];
            
            arrRetailerLogo = [[NSMutableArray alloc]init];
            
            //            if (arrRetailerId) {
            //                [arrRetailerId release];
            //            }
            arrRetailerId = [[NSMutableArray alloc]init];
            
            //            if (arrRetailerLocId) {
            //                [arrRetailerLocId release];
            //            }
            arrRetailerLocId = [[NSMutableArray alloc]init];
            
            //            if (arrRetailerLat) {
            //                [arrRetailerLat release];
            //            }
            arrRetailerLat = [[NSMutableArray alloc]init];
            
            //            if (arrRetailerLong) {
            //                [arrRetailerLong release];
            //            }
            arrRetailerLong = [[NSMutableArray alloc]init];
            
            //            if (arrDistance) {
            //                [arrDistance release];
            //            }
            arrDistance = [[NSMutableArray alloc]init];
            
            
            TBXMLElement *RetailerElement = [TBXML childElementNamed:@"Retailer" parentElement:retListElement];
            while (RetailerElement)
            {
                TBXMLElement *appSiteNameElement = [TBXML childElementNamed:@"appSiteName" parentElement:RetailerElement];
                TBXMLElement *addressElement = [TBXML childElementNamed:@"address" parentElement:RetailerElement];
                TBXMLElement *logoImagePathElement = [TBXML childElementNamed:@"logoImagePath" parentElement:RetailerElement];
                TBXMLElement *retailerIdElement = [TBXML childElementNamed:@"retailerId" parentElement:RetailerElement];
                TBXMLElement *distanceElement = [TBXML childElementNamed:KEY_DISTANCE parentElement:RetailerElement];
                TBXMLElement *retailerLocElement = [TBXML childElementNamed:@"retailLocationId" parentElement:RetailerElement];
                TBXMLElement *retailerLatElement = [TBXML childElementNamed:@"latitude" parentElement:RetailerElement];
                TBXMLElement *retailerLongElement = [TBXML childElementNamed:@"longitude" parentElement:RetailerElement];
                
                if(appSiteNameElement)
                    [arrRetailers addObject:[TBXML textForElement:appSiteNameElement]];
                
                if(addressElement)
                    [arrRetailerAddress addObject:[TBXML textForElement:addressElement]];
                
                if(logoImagePathElement)
                    [arrRetailerLogo addObject:[TBXML textForElement:logoImagePathElement]];
                
                if (retailerIdElement) {
                    [arrRetailerId addObject:[TBXML textForElement:retailerIdElement]];
                }
                if (retailerLocElement) {
                    [arrRetailerLocId addObject:[TBXML textForElement:retailerLocElement]];
                }
                
                if (retailerLatElement) {
                    [arrRetailerLat addObject:[TBXML textForElement:retailerLatElement]];
                }
                
                if (retailerLongElement) {
                    [arrRetailerLong addObject:[TBXML textForElement:retailerLongElement]];
                }
                
                if (distanceElement) {
                    [arrDistance addObject:[TBXML textForElement:distanceElement]];
                }
                
                
                RetailerElement = [TBXML nextSiblingNamed:@"Retailer" searchFromElement:RetailerElement];
                
            }
            
            if([arrRetailers count]>0 && flagAppsite==1)
                [self loadRetailerAddressView];
            
        }
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        eventDetailScrollView.scrollEnabled=YES;
        
    }
    
}


-(void)popBackToPreviousPage
{
    [evtID removeLastObject];
    
    [self.navigationController popViewControllerAnimated:NO];
}



-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //   [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(void)crossClicked
{
    eventDetailScrollView.scrollEnabled=YES;
    
    [cross_button removeFromSuperview];
    [tblEventLocations removeFromSuperview];
    [tblEventHolder removeFromSuperview];
    
    //    [tblEventLocations release];
    //    [cross_button release];
    //    [tblEventHolder release];
}

-(void) calendarClicked{
    UIAlertController *actionSheet =[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *createEvent =[UIAlertAction actionWithTitle:@"Create Event" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self createshowEvent:0];
    }];
    UIAlertAction *copyEvent =[UIAlertAction actionWithTitle:@"Copy" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self copyEvent];
    }];
    
    UIAlertAction *cancel =[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:createEvent];
    [actionSheet addAction:copyEvent];
    [actionSheet addAction:cancel];
    
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = btnCalendar;
        actionSheet.popoverPresentationController.sourceRect =  btnCalendar.bounds;
    }
    [self presentViewController:actionSheet animated:YES completion:nil];
}

-(void) copyEvent{
    flagAppsite=0;
    
    EventDetailsDO *iEventDetailsDO;
    iEventDetailsDO = [arrEventDetails objectAtIndex:0];
    
    /* NSMutableString *businessName = [[NSMutableString alloc]initWithString:@"Location: "];
     
     if([iEventDetailsDO.isAppSiteFlag isEqualToString:@"1"]) //If Event is associated to Retailer
     {
     locLatitude=[[NSMutableArray alloc]initWithArray:[iEventDetailsDO.latitude componentsSeparatedByString:@"|"]];
     locLongitude=[[NSMutableArray alloc]initWithArray:[iEventDetailsDO.longitude componentsSeparatedByString:@"|"]];
     eventID=[[NSMutableArray alloc]initWithObjects:[defaults valueForKey:EVENTID], nil];
     
     
     [evtID addObject:[eventID objectAtIndex:0]];
     
     [self Request_eventappsiteloc];
     
     if(arrRetailers>0){
     int i;
     [businessName appendFormat:@"%@",[arrRetailers objectAtIndex:0]];
     for(i=1;i<arrRetailers.count;i++)
     [businessName appendFormat:@",%@",[arrRetailers objectAtIndex:i]];
     
     }
     }
     else{
     [businessName appendFormat:@"%@",iEventDetailsDO.address];
     
     }*/
    NSMutableString *strDate = [[NSMutableString alloc]initWithString:@"Start Date: "];
    [strDate appendFormat:@"%@",iEventDetailsDO.startDate];
    [strDate appendFormat:@" %@",iEventDetailsDO.startTime];
    NSMutableString *endDate = [[NSMutableString alloc]init];
    
    if (![iEventDetailsDO.endDate isEqualToString:@"N/A"]) {
        if([iEventDetailsDO.endTime isEqualToString:@"N/A"])
        {
            [endDate appendFormat:@"%@",@"End Date: "];
            
            [endDate appendFormat:@"%@",iEventDetailsDO.endDate];
        }
        else
        {
            [endDate appendFormat:@"%@",@"End Date: "];
            
            [endDate appendFormat:@"%@",iEventDetailsDO.endDate];
            [endDate appendFormat:@" %@",iEventDetailsDO.endTime];
        }
    }
    
    NSMutableString *copyText=[NSMutableString stringWithFormat:@"Title: %@\nShort Description: %@\nLocation: %@\n%@\n",iEventDetailsDO.eventName,iEventDetailsDO.shortDes,businessName,strDate];
    if (![iEventDetailsDO.endDate isEqualToString:@"N/A"]) {
        [copyText appendString:endDate];
    }
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string =copyText;
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Event is copied to clipboard"
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
}
-(NSDate *) getDateFromString:(NSString *)stringDate{
    
    NSString *dateString =stringDate;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    
    NSDate *dateFromString ;
    
    dateFromString = [dateFormatter dateFromString:dateString];
    return dateFromString;
    
}



-(NSDate *) getDateFromStringDate:(NSString *)stringDate{
    
    NSString *dateString =stringDate;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale:[NSLocale currentLocale]];
    
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    
    NSDate *dateFromString;
    
    dateFromString = [dateFormatter dateFromString:dateString];
    return dateFromString;
    
}

-(NSString *) getNextMonthDate: (NSDate *) date recurrence:(NSUInteger) recuuringMonth{
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSDateComponents *comps = [gregorian components:(NSCalendarUnitDay|NSCalendarUnitMonth) fromDate:date];
    NSUInteger month=[comps month];
    NSLog(@"%ld", (long)month);
    NSDateComponents *components = [NSDateComponents new];
    
    components.month=recuuringMonth;
    
    NSCalendar *calendars=[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDate* newDate2 = [calendars dateByAddingComponents:components toDate:date options:0];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSLog(@"%@",[dateFormatter stringFromDate:newDate2]);
    return [dateFormatter stringFromDate:newDate2];
}

-(NSUInteger) getTodayMonth:(NSDate *) date{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [gregorian components:(NSCalendarUnitDay|NSCalendarUnitMonth) fromDate:date];
    NSUInteger month=[comps month];
    return month;
}
-(NSUInteger) getTodayDay:(NSDate *) date{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [gregorian components:(NSCalendarUnitDay|NSCalendarUnitMonth) fromDate:date];
    NSUInteger day=[comps day];
    return day;
}

-(NSUInteger) checkifEventPresent: (EKEvent *) eventObject{
    
    NSPredicate *predicate;
    EventDetailsDO *iEventDetailsDO ;
    iEventDetailsDO = [arrEventDetails objectAtIndex:0];
    
    if([iEventDetailsDO.isOnGoing isEqualToString:@"1"]){
        
        if(eventObject.endDate==nil)
        {
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *oneYearFromNowComponents = [[NSDateComponents alloc] init];
            oneYearFromNowComponents.year = 1;
            NSDate *oneYearFromNow = [calendar dateByAddingComponents:oneYearFromNowComponents
                                                               toDate:eventObject.startDate
                                                              options:0];
            predicate = [self.eventStore predicateForEventsWithStartDate:eventObject.startDate
                                                                 endDate:oneYearFromNow
                                                               calendars:nil];
            
        }
        else
            predicate = [eventStore predicateForEventsWithStartDate:eventObject.startDate endDate:eventObject.endDate calendars:nil];
    }
    else{
        predicate = [eventStore predicateForEventsWithStartDate:eventObject.startDate endDate:eventObject.endDate calendars:nil];
    }
    NSArray *eventsOnDate = [eventStore eventsMatchingPredicate:predicate];
    NSUInteger eventIndex;
    if(eventsOnDate){
        eventIndex = [eventsOnDate indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            EKEvent *eventToCheck = (EKEvent*)obj;
            NSString *titleToCheck=[[eventToCheck.title componentsSeparatedByString:@"-"] objectAtIndex:0];
            
            return [iEventDetailsDO.eventName isEqualToString:titleToCheck] && [eventObject.startDate isEqualToDate:eventToCheck.startDate] && [eventObject.location isEqualToString:eventToCheck.location];
        }];
        return eventIndex;
    }
    else
        return NSNotFound;
}
-(NSUInteger) getCurrentWeekDay:(NSDate *) startDate{
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:startDate];
    NSInteger weekday = [comps weekday];
    return weekday;
}

-(NSString *) getNextDayOfWeek: (NSUInteger) day recurrenceWeek:(NSUInteger) recurWeek dateOfMonth: (NSDate *) date monthOccurence:(NSUInteger)month{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *weekdayComponents = [gregorian components:(NSCalendarUnitWeekday|NSCalendarUnitWeekdayOrdinal) fromDate:date];
    NSInteger todayWeekday = [weekdayComponents weekday];
    
    enum Weeks {
        SUNDAY = 1,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY
    };
    
    NSInteger moveDays=day-todayWeekday;
    if (moveDays<=0) {
        moveDays+=7;
    }
    
    NSDateComponents *components = [NSDateComponents new];
    if(month==0)
    {
        components.day=moveDays;
        NSCalendar *calendars=[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
        NSDate* newDate = [calendars dateByAddingComponents:components toDate:date options:0];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSLog(@"%@",[dateFormatter stringFromDate:newDate]);
        return [dateFormatter stringFromDate:newDate];
    }else
    {
        if(recurWeek==5)
            components.weekdayOrdinal=-1;
        else
            components.weekdayOrdinal=recurWeek;
        components.weekday=day;
        NSString *startDateString = [self getStringFromDateonly:date];
        
        NSArray *obj=[startDateString componentsSeparatedByString:@"/"];
        
        
        [components setMonth:[[obj objectAtIndex:0] integerValue]];
        [components setYear:[[obj objectAtIndex:2] integerValue]];
        //        components.month=12;
        //        components.year=2015;
        
        // NSCalendar *calendars=[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
        NSDate *newDate=[[NSCalendar currentCalendar] dateFromComponents:components];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSLog(@"%@",[dateFormatter stringFromDate:newDate]);
        NSString *newRecDate=[dateFormatter stringFromDate:newDate];
        NSArray *recDateArray=[newRecDate componentsSeparatedByString:@"/"];
        
        if([[obj objectAtIndex:1] integerValue]>[[recDateArray objectAtIndex:1] integerValue]){
            NSUInteger monthOfDay=([[obj objectAtIndex:0] integerValue]+ month);
            if(monthOfDay>12){
                monthOfDay=monthOfDay-12;
                [components setMonth:monthOfDay];
                [components setYear:([[obj objectAtIndex:2] integerValue]+1)];
            }
            else{
                [components setMonth:monthOfDay];
            }
        }
        NSDate *newDate2=[[NSCalendar currentCalendar] dateFromComponents:components];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        
        NSLog(@"%@",[dateFormatter stringFromDate:newDate2]);
        return [dateFormatter stringFromDate:newDate2];
    }
}

-(void) createshowEvent:(int) flag{
    
    
    
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (error)
            {
                NSLog(@"Could not access the calendar because an error ocurred.");
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Application uses Calendar to add event, turn on in the device settings"
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* noButton = [UIAlertAction
                                           actionWithTitle:@"OK"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action)
                                           {
                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                               
                                           }];
                
                [alert addAction:noButton];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else if (!granted)
            {
                NSLog(@"Could not access the calendar because permission was not granted.");
                NSLog(@"Could not access the calendar because an error ocurred.");
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Application uses Calendar to add event, turn on in the device settings"
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* noButton = [UIAlertAction
                                           actionWithTitle:@"OK"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action)
                                           {
                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                               
                                           }];
                
                [alert addAction:noButton];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else{
                if(flag==0)
                    [self checkForEvent];
                else if(flag==1)
                    [self showInCalendar];
            }
        });
    }];
    
}
-(void) showInCalendar{
    
    EventDetailsDO *iEventDetailsDO ;
    iEventDetailsDO = [arrEventDetails objectAtIndex:0];
    
    EKEvent *event=[self getEvent];
    NSUInteger eventIndex = [self checkifEventPresent:event];
    
    if(eventIndex == NSNotFound)
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Alert"
                                      message:@"Event doesnot exist in the personal calendar.Please add event to calendar."
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        
        if([iEventDetailsDO.isOnGoing isEqualToString:@"1"]){
            
            NSPredicate *predicate;
            if(recurrenceEnd!=nil){
                predicate = [self.eventStore predicateForEventsWithStartDate:event.startDate
                                                                     endDate:event.endDate
                                                                   calendars:nil];
            }
            else
            {
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDateComponents *oneYearFromNowComponents = [[NSDateComponents alloc] init];
                oneYearFromNowComponents.year = 1;
                NSDate *oneYearFromNow = [calendar dateByAddingComponents:oneYearFromNowComponents
                                                                   toDate:[NSDate date]
                                                                  options:0];
                predicate = [self.eventStore predicateForEventsWithStartDate:event.startDate
                                                                     endDate:oneYearFromNow
                                                                   calendars:nil];
                
            }
            
            
            NSMutableArray *eventArray=[[NSMutableArray alloc] init];
            
            [self.eventStore enumerateEventsMatchingPredicate:predicate
                                                   usingBlock:^(EKEvent *eventObj, BOOL *stop) {
                                                       
                                                       if([eventObj.title isEqualToString:event.title] && [eventObj.startDate isEqualToDate:event.startDate]){
                                                           [eventArray addObject:eventObj];
                                                       }
                                                   }];
            NSString *todaydate=[self getStringFromDateonly:[NSDate date]];
            NSDate *today=[self getDateFromStringDate:todaydate] ;
            NSArray *events =[NSArray arrayWithArray:eventArray];
            NSUInteger eventIndex = [events indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                EKEvent *eventToCheck = (EKEvent*)obj;
                NSComparisonResult result;
                NSDate *eventDate=[self getDateFromStringDate:[self getStringFromDateonly:eventToCheck.startDate]] ;
                result = [today compare:eventDate]; // comparing two dates
                
                return  (result == NSOrderedAscending || result ==NSOrderedSame);
            }];
            
            if (eventIndex == NSNotFound) {
                NSLog(@"Even not found");
            }
            else {
                NSLog(@"Event is found");
            }
            
            // Set its title to the cell's text label.
            EKEvent *eventAtIndex = [events objectAtIndex:eventIndex];
            
            EKEventViewController *controller = [[EKEventViewController alloc] init];
            controller.event = eventAtIndex;
            controller.allowsEditing = YES;
            controller.allowsCalendarPreview = YES;
            
            self.navigationItem.backBarButtonItem =
            [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                             style:UIBarButtonItemStylePlain
                                            target:nil
                                            action:nil];
            
            [self.navigationController pushViewController:controller
                                                 animated:YES];
            
            
        }
        else{
            
            NSPredicate *predicate;
            if ([iEventDetailsDO.endDate isEqualToString:@"N/A"]) {
                predicate = [self.eventStore predicateForEventsWithStartDate:event.startDate
                                                                     endDate:event.endDate
                                                                   calendars:nil];
                NSMutableArray *eventArray=[[NSMutableArray alloc] init];
                
                [self.eventStore enumerateEventsMatchingPredicate:predicate
                                                       usingBlock:^(EKEvent *eventObj, BOOL *stop) {
                                                           
                                                           if([eventObj.title isEqualToString:event.title] && [eventObj.startDate isEqualToDate:event.startDate]){
                                                               [eventArray addObject:eventObj];
                                                           }
                                                       }];
                EKEvent *eventAtIndex = [eventArray objectAtIndex:0];
                
                EKEventViewController *controller = [[EKEventViewController alloc] init];
                controller.event = eventAtIndex;
                controller.allowsEditing = YES;
                controller.allowsCalendarPreview = YES;
                
                self.navigationItem.backBarButtonItem =
                [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                 style:UIBarButtonItemStylePlain
                                                target:nil
                                                action:nil];
                
                [self.navigationController pushViewController:controller
                                                     animated:YES];
                
                
                
            }
            else{
                
                predicate = [self.eventStore predicateForEventsWithStartDate:event.startDate
                                                                     endDate:event.endDate
                                                                   calendars:nil];
                NSMutableArray *eventArray=[[NSMutableArray alloc] init];
                
                [self.eventStore enumerateEventsMatchingPredicate:predicate
                                                       usingBlock:^(EKEvent *eventObj, BOOL *stop) {
                                                           
                                                           if([eventObj.title isEqualToString:event.title] && [eventObj.startDate isEqualToDate:event.startDate]){
                                                               [eventArray addObject:eventObj];
                                                           }
                                                       }];
                
                // NSString *todaydate=[self getStringFromDateonly:[NSDate date]];
                // NSDate *today=[self getDateFromStringDate:todaydate] ;
                NSArray *events =[NSArray arrayWithArray:eventArray];
                
                EKEvent *eventAtIndex = [events objectAtIndex:0];
                
                EKEventViewController *controller = [[EKEventViewController alloc] init];
                controller.event = eventAtIndex;
                controller.allowsEditing = YES;
                controller.allowsCalendarPreview = YES;
                
                self.navigationItem.backBarButtonItem =
                [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                 style:UIBarButtonItemStylePlain
                                                target:nil
                                                action:nil];
                
                [self.navigationController pushViewController:controller
                                                     animated:YES];
                
                
            }
        }
    }
    
}
-(NSString *)getStringFromDateonly:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [dateFormatter stringFromDate:date];
    return stringFromDate;
}


-(EKEvent *) getEvent{
    
    EventDetailsDO *iEventDetailsDO ;
    iEventDetailsDO = [arrEventDetails objectAtIndex:0];
    
    EKEvent *events = [EKEvent eventWithEventStore:eventStore];
    NSString *startDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,iEventDetailsDO.startTime];
    NSString *eventTitle=[NSString stringWithFormat:@"%@-%@",iEventDetailsDO.eventName,iEventDetailsDO.shortDes];
    events.title = eventTitle;
    events.startDate =  [self getDateFromString:startDate];
    
    NSString *endDate;
    
    if([iEventDetailsDO.isOnGoing isEqualToString:@"1"]){
        
        //event end date will be same as start date but end time set as response.....
        if ([iEventDetailsDO.endDate isEqualToString:@"N/A"]) {
            endDate=nil;
            
            
        }
        else {
            if([iEventDetailsDO.endTime isEqualToString:@"12:00 AM"]){
                endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,@"11:59 PM"];
                
            }
            else{
                endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,iEventDetailsDO.endTime];
                
                
            }
            events.endDate = [self getDateFromString:endDate];
        }
        
        
        
    }
    else{
        if ([iEventDetailsDO.endDate isEqualToString:@"N/A"]) {
            if([iEventDetailsDO.endTime isEqualToString:@"N/A"])
                endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,@"11:59 PM"];
            else
                endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,iEventDetailsDO.endTime];
            
            
        }
        else {
            if([iEventDetailsDO.endTime isEqualToString:@"N/A"])
                endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.endDate,@"11:59 PM"];
            else{
                if([iEventDetailsDO.endTime isEqualToString:@"12:00 AM"]){
                    endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.endDate,@"11:59 PM"];
                    
                }
                else{
                    endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.endDate,iEventDetailsDO.endTime];
                }
            }
            
        }
        events.endDate = [self getDateFromString:endDate];
    }
    events.location=businessName;
    return events;
}

-(void) checkForEvent{
    
    EKEvent *event=[self getEvent];
    NSUInteger eventIndex = [self checkifEventPresent:event];
    
    if(eventIndex == NSNotFound)
    {
        [self addEventToCalender];
        
        
    }
    else{
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Event has already been added to your calendar. Add again?"
                                      message:nil
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [self addEventToCalender];
                                        
                                    }];
        
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

-(void) addEventToCalender{
    
    EventDetailsDO *iEventDetailsDO ;
    iEventDetailsDO = [arrEventDetails objectAtIndex:0];
    
    
    if(![iEventDetailsDO.byDayNumber isEqualToString:@"true"] && iEventDetailsDO.byDayNumber!=nil){
        NSMutableArray *daysOfTheWeekRecurrence=[[NSMutableArray alloc]init];
        NSUInteger daynumbers=iEventDetailsDO.dayNumber;
        
        //to find last week
        if(daynumbers==5)
            daynumbers=-1;
        
        for(int i=0;i<iEventDetailsDO.everyWeekDayMonth.count;i++){
            NSUInteger weekdayInt=[[weekDayNumber objectForKey:[iEventDetailsDO.everyWeekDayMonth objectAtIndex:i]] integerValue];
            [daysOfTheWeekRecurrence addObject:[EKRecurrenceDayOfWeek dayOfWeek:weekdayInt weekNumber:daynumbers]];
            
        }
        
        NSArray *myArray = [NSArray arrayWithArray:daysOfTheWeekRecurrence];
        int count=0;
        for(int i=0;i<iEventDetailsDO.everyWeekDayMonth.count;i++){
            EKEvent *event = [EKEvent eventWithEventStore:eventStore];
            NSString *startDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,iEventDetailsDO.startTime];
            NSString *eventTitle=[NSString stringWithFormat:@"%@-%@",iEventDetailsDO.eventName,iEventDetailsDO.shortDes];
            event.title = eventTitle;
            event.startDate =  [self getDateFromString:startDate];
            
            NSString *endDate;
            NSString *recurringEndDate;
            NSString *endTime;
           // int MonthCheck=1;
            NSMutableString *businessNames = [[NSMutableString alloc]init];
            
            if([iEventDetailsDO.isAppSiteFlag isEqualToString:@"1"]) //If Event is associated to Retailer
            {
                flagAppsite=0;
                locLatitude=[[NSMutableArray alloc]initWithArray:[iEventDetailsDO.latitude componentsSeparatedByString:@"|"]];
                locLongitude=[[NSMutableArray alloc]initWithArray:[iEventDetailsDO.longitude componentsSeparatedByString:@"|"]];
                eventID=[[NSMutableArray alloc]initWithObjects:[defaults valueForKey:EVENTID], nil];
                
                
                [evtID addObject:[eventID objectAtIndex:0]];
                
                [self Request_eventappsiteloc];
                
                if(arrRetailers>0){
                    int i;
                    [businessNames appendFormat:@"%@",[arrRetailers objectAtIndex:0]];
                    for(i=1;i<arrRetailers.count;i++)
                        [businessNames appendFormat:@",%@",[arrRetailers objectAtIndex:i]];
                    
                }
            }
            else{
                [businessNames appendFormat:@"%@",iEventDetailsDO.address];
                
            }
            event.location=businessNames;
            
            //For recurring event
            if([iEventDetailsDO.isOnGoing isEqualToString:@"1"]){
                
                //event end date will be same as start date but end time set as response.....
                
                if ([iEventDetailsDO.endTime isEqualToString:@"N/A"]) {
                    if([iEventDetailsDO.startTime isEqualToString:@"12:00 AM"])
                        event.allDay=true;
                    endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,@"11:59 PM"];
                    endTime=@"11:59 PM";
                }
                else
                {
                    if([iEventDetailsDO.startTime isEqualToString:@"12:00 AM"] && [iEventDetailsDO.endTime isEqualToString:@"12:00 AM"]){
                        event.allDay = true;
                    }
                    if([iEventDetailsDO.endTime isEqualToString:@"12:00 AM"]){
                        endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,@"11:59 PM"];
                        endTime=@"11:59 PM";
                        
                    }
                    else{
                        endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,iEventDetailsDO.endTime];
                        endTime=iEventDetailsDO.endTime;
                        
                    }
                    
                }
                
                //setting recurrence end date
                if ([iEventDetailsDO.endDate isEqualToString:@"N/A"]) {
                    recurringEndDate=nil;
                    recurrenceEnd = nil;
                }
                else{
                    recurringEndDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.endDate,endTime];
                    recurrenceEnd = [EKRecurrenceEnd recurrenceEndWithEndDate:[self getDateFromString:recurringEndDate]];
                }
                
                event.endDate = [self getDateFromString:endDate];
                
               // MonthCheck=0;
                //one case multiple event............
                int tempDayEqual=0;
                
                NSUInteger todayWeek=[self getCurrentWeekDay:[self getDateFromStringDate:iEventDetailsDO.startDate]];
                
                NSUInteger resultWeekDay;
                
                
                //     for(int i=0;i<myArray.count;i++){
                
                
                NSUInteger weekdayInt=[[weekDayNumber objectForKey:[iEventDetailsDO.everyWeekDayMonth objectAtIndex:i]] integerValue];
                if(weekdayInt==todayWeek){
                    tempDayEqual=1;
                   // resultWeekDay=weekdayInt;
                    startDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,iEventDetailsDO.startTime];
                }
                
                
                
                if(tempDayEqual==0){
                    resultWeekDay=[[weekDayNumber objectForKey:[iEventDetailsDO.everyWeekDayMonth objectAtIndex:i]] integerValue];
                    startDate=[NSString stringWithFormat:@"%@ %@",[self getNextDayOfWeek:resultWeekDay recurrenceWeek:iEventDetailsDO.dayNumber dateOfMonth:[self getDateFromStringDate:iEventDetailsDO.startDate] monthOccurence:iEventDetailsDO.recurrenceInterval],iEventDetailsDO.startTime];
                    
                    
                }
                if ([iEventDetailsDO.endTime isEqualToString:@"N/A"]) {
                    endDate=[NSString stringWithFormat:@"%@ %@",[self getStringFromDateonly:[self getDateFromString:startDate]],@"11:59 PM"];
                }
                else{
                    if([iEventDetailsDO.startTime isEqualToString:@"12:00 AM"] && [iEventDetailsDO.endTime isEqualToString:@"12:00 AM"]){
                        event.allDay = true;
                    }
                    if([iEventDetailsDO.endTime isEqualToString:@"12:00 AM"]){
                        endDate=[NSString stringWithFormat:@"%@ %@",[self getStringFromDateonly:[self getDateFromString:startDate]],@"11:59 PM"];
                        
                    }
                    else{
                        endDate=[NSString stringWithFormat:@"%@ %@",[self getStringFromDateonly:[self getDateFromString:startDate]],iEventDetailsDO.endTime];
                        //endTime=iEventDetailsDO.endTime;
                        
                    }
                    
                    
                }
                
                event.endDate=[self getDateFromString:endDate];
                event.startDate=[self getDateFromString:startDate];
                
                
                
                rule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyMonthly
                                                                    interval:iEventDetailsDO.recurrenceInterval
                                                               daysOfTheWeek:[NSArray arrayWithObjects:[myArray objectAtIndex:i],nil]
                                                              daysOfTheMonth:nil
                                                             monthsOfTheYear:nil
                                                              weeksOfTheYear:nil
                                                               daysOfTheYear:nil
                                                                setPositions:nil
                                                                         end:recurrenceEnd];
                
                
                event.recurrenceRules = @[rule];
                // NSUInteger eventIndex = [self checkifEventPresent:event];
                
                [event setCalendar:[eventStore defaultCalendarForNewEvents]];
                
                NSError *saveEventError;
                [eventStore saveEvent:event span:EKSpanThisEvent error: &saveEventError];
                
                if(saveEventError)
                {
                    NSLog(@"Could not remove event from the calendar because an error ocurred.");
                }
                else
                {
                    NSLog(@"The event was added from the calendar");
                    count++;
                    
                }
                //    }
            }
        }
        if(count==iEventDetailsDO.everyWeekDayMonth.count)
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Event is successfully added to calendar"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                           
                                       }];
            
            [alert addAction:noButton];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        
    }
    else{
        EKEvent *event = [EKEvent eventWithEventStore:eventStore];
        NSString *startDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,iEventDetailsDO.startTime];
        NSString *eventTitle=[NSString stringWithFormat:@"%@-%@",iEventDetailsDO.eventName,iEventDetailsDO.shortDes];
        event.title = eventTitle;
        event.startDate =  [self getDateFromString:startDate];
        
        NSString *endDate;
        NSString *recurringEndDate;
        NSString *endTime;
        int MonthCheck=1;
        /*   NSMutableString *businessName = [[NSMutableString alloc]init];
         
         if([iEventDetailsDO.isAppSiteFlag isEqualToString:@"1"]) //If Event is associated to Retailer
         {
         flagAppsite=0;
         locLatitude=[[NSMutableArray alloc]initWithArray:[iEventDetailsDO.latitude componentsSeparatedByString:@"|"]];
         locLongitude=[[NSMutableArray alloc]initWithArray:[iEventDetailsDO.longitude componentsSeparatedByString:@"|"]];
         eventID=[[NSMutableArray alloc]initWithObjects:[defaults valueForKey:EVENTID], nil];
         
         
         [evtID addObject:[eventID objectAtIndex:0]];
         
         [self Request_eventappsiteloc];
         
         if(arrRetailers>0){
         int i;
         [businessName appendFormat:@"%@",[arrRetailers objectAtIndex:0]];
         for(i=1;i<arrRetailers.count;i++)
         [businessName appendFormat:@",%@",[arrRetailers objectAtIndex:i]];        }
         }
         else{
         [businessName appendFormat:@"%@",iEventDetailsDO.address];
         
         }*/
        event.location=businessName;
        
        //For recurring event
        if([iEventDetailsDO.isOnGoing isEqualToString:@"1"]){
            
            //event end date will be same as start date but end time set as response.....
            
            if ([iEventDetailsDO.endTime isEqualToString:@"N/A"]) {
                if([iEventDetailsDO.startTime isEqualToString:@"12:00 AM"])
                    event.allDay=true;
                endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,@"11:59 PM"];
                endTime=@"11:59 PM";
            }
            else
            {
                if([iEventDetailsDO.startTime isEqualToString:@"12:00 AM"] && [iEventDetailsDO.endTime isEqualToString:@"12:00 AM"]){
                    event.allDay = true;
                }
                if([iEventDetailsDO.endTime isEqualToString:@"12:00 AM"]){
                    endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,@"11:59 PM"];
                    endTime=@"11:59 PM";
                    
                }
                else{
                    endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,iEventDetailsDO.endTime];
                    endTime=iEventDetailsDO.endTime;
                }
                
            }
            
            //setting recurrence end date
            if ([iEventDetailsDO.endDate isEqualToString:@"N/A"]) {
                recurringEndDate=nil;
                recurrenceEnd = nil;
            }
            else{
                recurringEndDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.endDate,endTime];
                recurrenceEnd = [EKRecurrenceEnd recurrenceEndWithEndDate:[self getDateFromString:recurringEndDate]];
            }
            
            event.endDate = [self getDateFromString:endDate];
            
            //recurring pattern.....
            
            if([iEventDetailsDO.recurrencePatternName isEqualToString:@"Daily"]){
                if([iEventDetailsDO.isWeekDay isEqualToString:@"false"]){
                    rule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyDaily interval:iEventDetailsDO.recurrenceInterval end:recurrenceEnd];
                    event.recurrenceRules=@[rule];
                    
                    
                    
                    
                }
                else{
                    
                    NSUInteger weekdayInt=[self getCurrentWeekDay:[self getDateFromStringDate:iEventDetailsDO.startDate]];
                    if(weekdayInt==1||weekdayInt==7){
                        startDate=[NSString stringWithFormat:@"%@ %@",[self getNextDayOfWeek:2 recurrenceWeek:0 dateOfMonth:[self getDateFromStringDate:iEventDetailsDO.startDate] monthOccurence:0],iEventDetailsDO.startTime];
                        event.startDate=[self getDateFromString:startDate];
                    }
                    
                    rule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyDaily
                                                                        interval:1
                                                                   daysOfTheWeek:[NSArray arrayWithObjects:
                                                                                  [EKRecurrenceDayOfWeek dayOfWeek:2],
                                                                                  [EKRecurrenceDayOfWeek dayOfWeek:3],
                                                                                  [EKRecurrenceDayOfWeek dayOfWeek:4],
                                                                                  [EKRecurrenceDayOfWeek dayOfWeek:5],
                                                                                  [EKRecurrenceDayOfWeek dayOfWeek:6],
                                                                                  nil]
                                                                  daysOfTheMonth:nil
                                                                 monthsOfTheYear:nil
                                                                  weeksOfTheYear:nil
                                                                   daysOfTheYear:nil
                                                                    setPositions:nil
                                                                             end:recurrenceEnd];
                    event.recurrenceRules=@[rule];
                }
            }
            else  if([iEventDetailsDO.recurrencePatternName isEqualToString:@"Weekly"]){
                
                int tempDayGreater=0,tempDayEqual=0;
                
                NSUInteger todayWeek=[self getCurrentWeekDay:[self getDateFromStringDate:iEventDetailsDO.startDate]];
                NSMutableArray *daysOfTheWeekRecurrence=[[NSMutableArray alloc]init];
                
                for(int i=0;i<iEventDetailsDO.daysOfWeek.count;i++){
                    NSUInteger weekdayInt=[[weekDayNumber objectForKey:[iEventDetailsDO.daysOfWeek objectAtIndex:i]] integerValue];
                    
                    //[daysOfTheWeekRecurrence addObject:[EKRecurrenceDayOfWeek dayOfWeek:weekdayInt]];
                    
                    if(weekdayInt>todayWeek){
                        tempDayGreater=1;
                        startDate=[NSString stringWithFormat:@"%@ %@",[self getNextDayOfWeek:weekdayInt recurrenceWeek:0 dateOfMonth:[self getDateFromStringDate:iEventDetailsDO.startDate] monthOccurence:0],iEventDetailsDO.startTime];
                        break;
                    }
                    if(weekdayInt==todayWeek){
                        tempDayEqual=1;
                        break;
                    }
                    
                }
                if(tempDayGreater==0 && tempDayEqual==0){
                    NSUInteger resultWeekDay=[[weekDayNumber objectForKey:[iEventDetailsDO.daysOfWeek objectAtIndex:0]] integerValue];
                    startDate=[NSString stringWithFormat:@"%@ %@",[self getNextDayOfWeek:resultWeekDay recurrenceWeek:iEventDetailsDO.recurrenceInterval dateOfMonth:[self getDateFromStringDate:iEventDetailsDO.startDate] monthOccurence:0],iEventDetailsDO.startTime];
                }
                
                event.startDate=[self getDateFromString:startDate];
                
                for(int i=0;i<iEventDetailsDO.daysOfWeek.count;i++){
                    NSUInteger weekdayInt=[[weekDayNumber objectForKey:[iEventDetailsDO.daysOfWeek objectAtIndex:i]] integerValue];
                    [daysOfTheWeekRecurrence addObject:[EKRecurrenceDayOfWeek dayOfWeek:weekdayInt]];
                }
                
                NSArray *myArray = [NSArray arrayWithArray:daysOfTheWeekRecurrence];
                rule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyWeekly
                                                                    interval:iEventDetailsDO.recurrenceInterval
                                                               daysOfTheWeek:myArray
                                                              daysOfTheMonth:nil
                                                             monthsOfTheYear:nil
                                                              weeksOfTheYear:nil
                                                               daysOfTheYear:nil
                                                                setPositions:nil
                                                                         end:recurrenceEnd];
                event.recurrenceRules=@[rule];
                
            }
            else  if([iEventDetailsDO.recurrencePatternName isEqualToString:@"Monthly"]){
                
                if([iEventDetailsDO.byDayNumber isEqualToString:@"true"]){
                    
                    NSUInteger startDateForevent;
                    NSUInteger occurDateForevent=iEventDetailsDO.dateOfMonth;
                    
                    //take start date as given occuring date....
                    NSMutableArray *array = [[iEventDetailsDO.startDate componentsSeparatedByString:@"/"] mutableCopy];
                    NSNumber *val = [NSNumber numberWithInteger:occurDateForevent];
                    [array replaceObjectAtIndex:1 withObject:(NSString *)val];
                    NSString *finalDate=[array componentsJoinedByString:@"/"];
                    startDateForevent=[self getTodayDay:[self getDateFromStringDate:finalDate]];
                    
                    if(occurDateForevent>startDateForevent){
                        startDate=[NSString stringWithFormat:@"%@ %@",finalDate,iEventDetailsDO.startTime];
                    }
                    else if(occurDateForevent<startDateForevent) {
                        startDate=[NSString stringWithFormat:@"%@ %@",[self getNextMonthDate:[self getDateFromStringDate:finalDate] recurrence:iEventDetailsDO.recurrenceInterval],iEventDetailsDO.startTime];
                    }
                    
                    event.startDate=[self getDateFromString:startDate];
                    
                    rule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyMonthly
                                                                        interval:iEventDetailsDO.recurrenceInterval
                                                                   daysOfTheWeek:nil
                                                                  daysOfTheMonth:[NSArray arrayWithObject:[NSNumber numberWithInt:(int)occurDateForevent]]
                                                                 monthsOfTheYear:nil
                                                                  weeksOfTheYear:nil
                                                                   daysOfTheYear:nil
                                                                    setPositions:nil
                                                                             end:recurrenceEnd];
                    
                    
                    event.recurrenceRules = @[rule];
                    
                }
                
            }
            if(MonthCheck!=0){
                [event setCalendar:[eventStore defaultCalendarForNewEvents]];
                
                NSError *saveEventError;
                [eventStore saveEvent:event span:EKSpanThisEvent error: &saveEventError];
                
                if(saveEventError)
                {
                    NSLog(@"Could not remove event from the calendar because an error ocurred.");
                }
                else
                {
                    
                    NSLog(@"The event was added from the calendar");
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@"Event is successfully added to calendar"
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* noButton = [UIAlertAction
                                               actionWithTitle:@"OK"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                   
                                               }];
                    
                    [alert addAction:noButton];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
                
                
            }
            
            
            
        }
        else{
            if ([iEventDetailsDO.endDate isEqualToString:@"N/A"]) {
                if([iEventDetailsDO.startTime isEqualToString:@"12:00 AM"]){
                    event.allDay = true;
                }
                if([iEventDetailsDO.endTime isEqualToString:@"N/A"])
                    endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,@"11:59 PM"];
                else
                    endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.startDate,iEventDetailsDO.endTime];
                
                
            }
            else {
                if([iEventDetailsDO.endTime isEqualToString:@"N/A"]){
                    endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.endDate,@"11:59 PM"];
                }
                else{
                    if([iEventDetailsDO.startTime isEqualToString:@"12:00 AM"] && [iEventDetailsDO.endTime isEqualToString:@"12:00 AM"]){
                        event.allDay = true;
                    }
                    if([iEventDetailsDO.endTime isEqualToString:@"12:00 AM"]){
                        endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.endDate,@"11:59 PM"];
                        
                    }
                    else{
                        endDate=[NSString stringWithFormat:@"%@ %@",iEventDetailsDO.endDate,iEventDetailsDO.endTime];
                    }
                    
                    
                }
                
            }
            event.endDate = [self getDateFromString:endDate];
            
            [event setCalendar:[eventStore defaultCalendarForNewEvents]];
            
            NSError *saveEventError;
            [eventStore saveEvent:event span:EKSpanThisEvent error: &saveEventError];
            
            if(saveEventError)
            {
                NSLog(@"Could not remove event from the calendar because an error ocurred.");
            }
            else
            {
                NSLog(@"The event was added from the calendar");
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Event is successfully added to calender"
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* noButton = [UIAlertAction
                                           actionWithTitle:@"OK"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action)
                                           {
                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                               
                                           }];
                
                [alert addAction:noButton];
                [self presentViewController:alert animated:YES completion:nil];
                
                
            }
            
            
        }
    }
    
}
-(NSString *)getStringFromDate:(NSDate *)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    [dateFormatter setDateFormat:@"dd-MM-yyyy, HH:mm"];
    NSString *stringFromDate = [dateFormatter stringFromDate:date];
    return stringFromDate;
}
-(void) LocationButtonClicked
{
    flagAppsite=1;
    
    eventDetailScrollView.scrollEnabled=NO;
    EventDetailsDO *iEventDetailsDO;
    iEventDetailsDO = [arrEventDetails objectAtIndex:0];
    
    if([iEventDetailsDO.isAppSiteFlag isEqualToString:@"0"]) // Event Associated to Address
    {
        
        isAppSiteFlag=FALSE;
        [locationTapped addObject:@"NO"];
        tblEventHolder=[[UIView alloc]init];
        
        if (IS_IPHONE5)
            tblEventHolder.frame = CGRectMake(10, 10, 300, 425);
        else if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
            tblEventHolder.frame = CGRectMake(40, 80, 680, 750);
        else
            tblEventHolder.frame = CGRectMake(10, 10, 300, 350);
        
        tblEventHolder.layer.backgroundColor = [UIColor whiteColor].CGColor;
        tblEventHolder.layer.cornerRadius = 20.0;
        tblEventHolder.layer.borderColor = [UIColor blackColor].CGColor;
        tblEventHolder.layer.shadowOffset = CGSizeMake(1, 0);
        tblEventHolder.layer.shadowColor = [[UIColor blackColor] CGColor];
        tblEventHolder.layer.shadowRadius = 5;
        tblEventHolder.layer.shadowOpacity = .25;
        
        
        
        
        if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        {
            cross_button=[[UIButton alloc]initWithFrame:CGRectMake(tblEventHolder.frame.size.width-75,-3, 75, 55)];
            [cross_button setImage:[UIImage imageNamed:@"cross_icon_ipad.png"] forState:UIControlStateNormal];
        }
        else
        {
            cross_button=[[UIButton alloc]initWithFrame:CGRectMake(250,-5, 65, 45)];
            [cross_button setImage:[UIImage imageNamed:@"cross_icon.png"] forState:UIControlStateNormal];
        }
        
        [cross_button addTarget:self action:@selector(crossClicked) forControlEvents:UIControlEventTouchDown];
        
        tblEventLocations=[[UITableView alloc]init];
        if (IS_IPHONE5)
            tblEventLocations = [[UITableView alloc]initWithFrame:CGRectMake(10, 30, 280, 350) style:UITableViewStylePlain];
        else if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
            tblEventLocations = [[UITableView alloc]initWithFrame:CGRectMake(40, 100, 620, 650) style:UITableViewStylePlain];
        else
            tblEventLocations = [[UITableView alloc]initWithFrame:CGRectMake(10, 30, 280, 270) style:UITableViewStylePlain];
        tblEventLocations.dataSource=self;
        tblEventLocations.delegate=self;
        [tblEventLocations setBackgroundColor:[UIColor clearColor]];
        
        
        evtStrEventName = [[NSString alloc]initWithString:iEventDetailsDO.eventName];
        evtLocation=[[NSString alloc]initWithString:iEventDetailsDO.location];
        evtArrAddress = [[NSArray alloc]initWithArray:[iEventDetailsDO.address componentsSeparatedByString:@"|"]];
        evtArrAddress1=[[NSArray alloc]initWithArray:[iEventDetailsDO.address1 componentsSeparatedByString:@"|"]];
        evtArrAddress2=[[NSArray alloc]initWithArray:[iEventDetailsDO.address2 componentsSeparatedByString:@"|"]];
        evtLatitude=[[NSArray alloc]initWithArray:[iEventDetailsDO.latitude componentsSeparatedByString:@"|"]];
        evtLongitude=[[NSArray alloc]initWithArray:[iEventDetailsDO.longitude componentsSeparatedByString:@"|"]];
        
        eventID=[[NSMutableArray alloc]initWithObjects:[defaults valueForKey:EVENTID], nil];
        
        
        [evtID addObject:[eventID objectAtIndex:0]];
        
        
        
        [tblEventLocations reloadData];
        [tblEventHolder addSubview:cross_button];
        [tblEventHolder addSubview:tblEventLocations];
        [self.view addSubview:tblEventHolder];
        
        
        
    }
    else if([iEventDetailsDO.isAppSiteFlag isEqualToString:@"1"]) //If Event is associated to Retailer
    {
        isAppSiteFlag=TRUE;
        [locationTapped addObject:@"YES"];
        locLatitude=[[NSMutableArray alloc]initWithArray:[iEventDetailsDO.latitude componentsSeparatedByString:@"|"]];
        locLongitude=[[NSMutableArray alloc]initWithArray:[iEventDetailsDO.longitude componentsSeparatedByString:@"|"]];
        eventID=[[NSMutableArray alloc]initWithObjects:[defaults valueForKey:EVENTID], nil];
        
        
        [evtID addObject:[eventID objectAtIndex:0]];
        
        countTapped++;
        [self Request_eventappsiteloc];
    }
    
}

-(IBAction)DetailsButtonClicked:(id)sender
{
    UIButton *btnEvent = (UIButton*)sender;
    
    EventDetailsDO *iEventDetailsDO ;
    iEventDetailsDO = [arrEventDetails objectAtIndex:0];
    
    switch (btnEvent.tag)
    {
        case 0: // Package
        {
            EventPackageDetailsViewController *iEventPackageDetailsViewController = [[EventPackageDetailsViewController alloc]initWithNibName:@"EventPackageDetailsViewController" bundle:[NSBundle mainBundle]];
            
            iEventPackageDetailsViewController.strPkgDes = iEventDetailsDO.pkgDes;
            iEventPackageDetailsViewController.strPkgName = iEventDetailsDO.eventName;
            
            [self.navigationController pushViewController:iEventPackageDetailsViewController animated:NO];
            //[iEventPackageDetailsViewController release];
        }
            break;
        case 1: // Hotel
        {
            EventHotelListViewController *iEventHotelListViewController = [[EventHotelListViewController alloc]initWithNibName:@"EventHotelListViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:iEventHotelListViewController animated:NO];
            //[iEventHotelListViewController release];
        }
            break;
            
        case 2: // Event Logistics
        {
            [defaults setValue:iEventDetailsDO.logisticsURL forKey:KEY_URL];
            
            
            BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
            if (!lsFlag) {
                
                UIAlertController * alert;
                
                alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action){
                                         [alert dismissViewControllerAnimated:NO completion:nil];
                                         [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                     }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
            }
            else{
                
                SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                 if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                [HubCitiAppDelegate setIsLogistics:YES];
                splOfferVC.isEventLogisticsFlag = 1;
                

                
                //            [splOfferVC setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
                [self presentViewController:splOfferVC animated:YES completion:nil];
            }
            
        }
            
            break;
            
        case 3: // Purchase
        {
            [defaults setValue:iEventDetailsDO.pkgTicketURL forKey:KEY_URL];
            SpecialOffersViewController *splOfferVC = [[SpecialOffersViewController alloc]initWithNibName:@"SpecialOffersViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:splOfferVC animated:NO];
            //[splOfferVC release];
            
        }
            break;
        case 4:
        {
            [defaults setValue:iEventDetailsDO.moreInfoURL forKey:KEY_URL];
            
            NSURL *url = [NSURL URLWithString:iEventDetailsDO.moreInfoURL];
            if([[url absoluteString] containsString:@"3000.htm"])
            {

                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
                }
                else{
                    
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    splOfferVC.isEventLogisticsFlag = 1;
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
            }else{
            
                
        SpecialOffersViewController *splOfferVC = [[SpecialOffersViewController alloc]initWithNibName:@"SpecialOffersViewController" bundle:[NSBundle mainBundle]];
            shareFlag=NO;
            [self.navigationController pushViewController:splOfferVC animated:NO];
            }
            
        }
            break;
            
        default:
            break;
    }
}





-(void)presentSpecialOffersAfterDismissAleert {
     if (self.isViewLoaded && self.view.window != nil) {
         SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
         
             splOfferVC.isEventLogisticsFlag = 1;
          if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
             [HubCitiAppDelegate setIsLogistics:YES];
         


         
    //    [splOfferVC setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
         [self presentViewController:splOfferVC animated:YES completion:nil];
     }
}


#pragma mark - Location Service Methods
-(void) updatedLatlongValue :(NSString *) latValue longitudeValue:(NSString *)longValue{
    
    [defaults setBool:YES forKey:@"gpsEnabled"];
    [defaults setObject:latValue forKey:KEY_LATITUDE];
    [defaults setObject:longValue forKey:KEY_LONGITUDE];
    
    //Added the setting for these keys because its what the Map is using.
    [defaults  setValue:latValue forKey:@"LatVal"];
    [defaults  setValue:longValue forKey:@"LongVal"];
}


-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}




- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    
    return UIInterfaceOrientationPortrait;
    
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table View Delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(isAppSiteFlag==TRUE)
        return [evtArrRetailersAddress count];
    else
    {
        DLog(@"arrAddress coun is %lu",(unsigned long)[evtArrAddress1 count]);
        return [evtArrAddress1 count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        return 90.0;
    else
        return 70.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //	UITableViewCell *cell2;
    static NSString *CellIdentifier = @"CellEventLocation";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i =0; i < [arr count]; i++)
        {
            UIView *v = (UIView*)[arr objectAtIndex:i];
            [v removeFromSuperview];
        }
    }
    
    SdImageView *asyncImageView = nil;
    UILabel *lblAppSiteName = nil;
    UILabel *lblLocation = nil;
    UILabel *lblLocation1 = nil;
    UILabel *lblLocation2 = nil;
    UILabel *eventLocation = nil;
    if(isAppSiteFlag==TRUE)
    {
        
        if([evtArrRetailerLogo count]>indexPath.row)
        {
            asyncImageView = [[SdImageView alloc] init];
            asyncImageView.backgroundColor = [UIColor clearColor];
            //asyncImageView.layer.cornerRadius = 5.0f;
            [asyncImageView loadImage:arrRetailerLogo[indexPath.row]];
            
            [cell.contentView addSubview:asyncImageView];
            //[asyncImageView release];
        }
        
        if([evtArrRetailers count]>indexPath.row)
        {
            lblAppSiteName = [[UILabel alloc]init];
            lblAppSiteName.backgroundColor = [UIColor clearColor];
            lblAppSiteName.textColor=[UIColor blueColor];
            if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                lblAppSiteName.font = [UIFont boldSystemFontOfSize:18];
            else
                lblAppSiteName.font = [UIFont boldSystemFontOfSize:13];
            //lblAppSiteName.numberOfLines = 2;
            lblAppSiteName.lineBreakMode=NSLineBreakByWordWrapping;
            lblAppSiteName.text =[NSString stringWithFormat:@"%@:",[evtArrRetailers objectAtIndex:indexPath.row]];
            [cell.contentView addSubview:lblAppSiteName];
        }
        
        lblLocation = [[UILabel alloc]init];
        lblLocation.backgroundColor = [UIColor clearColor];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            lblLocation.font = [UIFont systemFontOfSize:14];
        else
            lblLocation.font = [UIFont systemFontOfSize:9];
        // lblLocation.numberOfLines = 3;
        lblLocation.lineBreakMode=NSLineBreakByWordWrapping;
        if([evtArrRetailersAddress count]>indexPath.row)
            lblLocation.text =[evtArrRetailersAddress objectAtIndex:indexPath.row];
        [cell.contentView addSubview:lblLocation];
        if([evtArrRetailerLogo count]>indexPath.row && [evtArrRetailers count]>indexPath.row)
            viewDictionary = NSDictionaryOfVariableBindings(asyncImageView,lblAppSiteName,lblLocation);
        else if(![evtArrRetailerLogo count]>indexPath.row && [evtArrRetailers count]>indexPath.row)
            viewDictionary = NSDictionaryOfVariableBindings(lblAppSiteName,lblLocation);
        else
            viewDictionary = NSDictionaryOfVariableBindings(asyncImageView,lblLocation);
    }
    else
    {
        isAppSiteFlag=NO;
        
        if (![evtLocation isEqualToString:@"N/A"]) {
            if(DEVICE_TYPE ==UIUserInterfaceIdiomPad ){
                eventLocation = [[UILabel alloc]initWithFrame:CGRectMake(-65, -3, SCREEN_WIDTH-55, 20)];
            }
            else{
                eventLocation = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, SCREEN_WIDTH-55, 23)];
            }
            eventLocation.backgroundColor = [UIColor clearColor];
            if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                eventLocation.font = [UIFont systemFontOfSize:18];
            else
                eventLocation.font = [UIFont boldSystemFontOfSize:13];
            //lblLocation1.numberOfLines = 3;
            eventLocation.textAlignment=NSTextAlignmentCenter;
            eventLocation.lineBreakMode=NSLineBreakByWordWrapping;
            eventLocation.text= evtLocation;
            [cell.contentView addSubview:eventLocation];
        }
        
        
        lblLocation1 = [[UILabel alloc]init];
        lblLocation1.backgroundColor = [UIColor clearColor];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            lblLocation1.font = [UIFont systemFontOfSize:18];
        else
            lblLocation1.font = [UIFont systemFontOfSize:13];
        //lblLocation1.numberOfLines = 3;
        lblLocation1.textAlignment=NSTextAlignmentCenter;
        lblLocation1.lineBreakMode=NSLineBreakByWordWrapping;
        if([evtArrAddress1 count]>indexPath.row)
            lblLocation1.text =[evtArrAddress1 objectAtIndex:indexPath.row];
        [cell.contentView addSubview:lblLocation1];
        
        lblLocation2 = [[UILabel alloc]init];
        lblLocation2.backgroundColor = [UIColor clearColor];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            lblLocation2.font = [UIFont systemFontOfSize:18];
        else
            lblLocation2.font = [UIFont systemFontOfSize:13];
        //lblLocation1.numberOfLines = 3;
        lblLocation2.textAlignment=NSTextAlignmentCenter;
        lblLocation2.lineBreakMode=NSLineBreakByWordWrapping;
        if([evtArrAddress2 count]>indexPath.row)
            lblLocation2.text =[evtArrAddress2 objectAtIndex:indexPath.row];
        [cell.contentView addSubview:lblLocation2];
        viewDictionary = NSDictionaryOfVariableBindings(lblLocation1, lblLocation2);
    }
    
    asyncImageView.translatesAutoresizingMaskIntoConstraints = NO;
    lblAppSiteName.translatesAutoresizingMaskIntoConstraints = NO;
    lblLocation.translatesAutoresizingMaskIntoConstraints = NO;
    lblLocation1.translatesAutoresizingMaskIntoConstraints = NO;
    lblLocation2.translatesAutoresizingMaskIntoConstraints = NO;
    //    eventLocation.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self setConstraints:cell];
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (isAppSiteFlag==FALSE) {
        [self showMapAddress];
    }
    else
    {
        [linkID insertObject:@"0" atIndex:[linkID count]];
        [self pullAppSite:(int)indexPath.row];
    }
    [self crossClicked];
}

-(void)setConstraints:(UITableViewCell*)cell
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        
        if([viewDictionary objectForKey:@"asyncImageView"]!=nil)
        {
            // Vertical constrain
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[asyncImageView(60)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(60)]"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblAppSiteName"]!=nil)
        {
            // Vertical constrain
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[lblAppSiteName(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(75)-[lblAppSiteName(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
        }
        
        
        
        if([viewDictionary objectForKey:@"lblLocation"]!=nil)
        {
            // Vertical constrain
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(35)-[lblLocation(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(75)-[lblLocation(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblLocation1"]!=nil)
        {
            // Vertical constrain
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(18)-[lblLocation1(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(-65)-[lblLocation1(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblLocation2"]!=nil)
        {
            // Vertical constrain
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(43)-[lblLocation2(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(-65)-[lblLocation2(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
        }
    }
    else
    {
        if([viewDictionary objectForKey:@"asyncImageView"]!=nil)
        {
            // Vertical constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(3)-[asyncImageView(44)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[asyncImageView(44)]"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"lblAppSiteName"]!=nil)
        {
            // Vertical constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(3)-[lblAppSiteName(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(55)-[lblAppSiteName(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
        }
        
        
        if([viewDictionary objectForKey:@"lblLocation"]!=nil)
        {
            // Vertical constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(23)-[lblLocation(20)]"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraint
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(55)-[lblLocation(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
        }
        
        
        if ([evtLocation isEqualToString:@"N/A"]) {
            if([viewDictionary objectForKey:@"lblLocation1"]!=nil)
            {
                // Vertical constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(3)-[lblLocation1(20)]"] options:0 metrics:0 views:viewDictionary]];
                
                // Horizontal constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[lblLocation1(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
            }
            
            if([viewDictionary objectForKey:@"lblLocation2"]!=nil)
            {
                // Vertical constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(23)-[lblLocation2(20)]"] options:0 metrics:0 views:viewDictionary]];
                
                // Horizontal constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[lblLocation2(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
            }
            
        }
        else
        {
            
            if([viewDictionary objectForKey:@"lblLocation1"]!=nil)
            {
                // Vertical constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(23)-[lblLocation1(20)]"] options:0 metrics:0 views:viewDictionary]];
                
                // Horizontal constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[lblLocation1(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
            }
            
            if([viewDictionary objectForKey:@"lblLocation2"]!=nil)
            {
                // Vertical constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(43)-[lblLocation2(20)]"] options:0 metrics:0 views:viewDictionary]];
                
                // Horizontal constraint
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[lblLocation2(%f)]",SCREEN_WIDTH - 55] options:0 metrics:0 views:viewDictionary]];
            }
            
        }
        
    }
}

#pragma mark map show functionality

-(void)showMapAddress
{
    
    BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
    if (!lsFlag) {
        UIAlertController * alert;
        
            alert=[UIAlertController alertControllerWithTitle:nil message:@"Location uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
            
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action){
                                 EventsMapViewController *iMapViewController = [[EventsMapViewController alloc]initWithNibName:@"EventsMapViewController" bundle:[NSBundle mainBundle]];
                                 
                                 iMapViewController.latitude=[[NSArray alloc]initWithArray:evtLatitude];
                                 iMapViewController.longitude=[[NSArray alloc]initWithArray:evtLongitude];
                                 iMapViewController.address1=[[NSArray alloc]initWithArray:evtArrAddress1];
                                 iMapViewController.address2=[[NSArray alloc]initWithArray:evtArrAddress2];
                                 
                                 [self.navigationController pushViewController:iMapViewController animated:NO];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
               //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
    }
    else{
        
        if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse))
        {
            [defaults setValue:@"" forKey:KEY_LATITUDE];
            [defaults setValue:@"" forKey:KEY_LONGITUDE];
            
            [Location updateLocationinfo:self];
        }
        
        EventsMapViewController *iMapViewController = [[EventsMapViewController alloc]initWithNibName:@"EventsMapViewController" bundle:[NSBundle mainBundle]];
        
        iMapViewController.latitude=[[NSArray alloc]initWithArray:evtLatitude];
        iMapViewController.longitude=[[NSArray alloc]initWithArray:evtLongitude];
        iMapViewController.address1=[[NSArray alloc]initWithArray:evtArrAddress1];
        iMapViewController.address2=[[NSArray alloc]initWithArray:evtArrAddress2];
        
        [self.navigationController pushViewController:iMapViewController animated:NO];
    }
    
    
    
    
}

-(void) pullAppSite:(int)rowNumber
{
    objIndex = rowNumber;
    iWebRequestState = RETSUMMARYS;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[evtArrRetailerLocId objectAtIndex:rowNumber]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && evtLatitude){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[evtLongitude  objectAtIndex:rowNumber],[evtLatitude objectAtIndex:rowNumber]]
        ;
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",[evtArrRetailerId objectAtIndex:rowNumber]];
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    NSString *urlString =  [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);
}

-(void)parse_retSummary:(NSString *)response
{
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
      
        return;
    }
    else
    {
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [arrDistance objectAtIndex:objIndex];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
    
}
#pragma mark sender user tracking
-(void)userTracking:(NSString*)xmlStr
{
    
    NSMutableString *urlStr = [[NSMutableString alloc]init];
    [urlStr appendFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    
    NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:xmlStr]];
    DLog(@"%@",response);
    // [xmlStr release];
}
#pragma mark share events methods

-(void)shareClicked
{
    iWebRequestState = EVENT_SHAREINFO;
    
    //    [defaults setValue:retId forKey:KEY_RETAILERID];
    //    [defaults setValue:retLocId forKey:@"retailLocationID"];
    
    
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    
    
    [reqStr appendFormat:@"<ShareProductInfo><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [reqStr appendFormat:@"<eventId>%@</eventId>",[defaults valueForKey:EVENTID]];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/shareevent",BASE_URL];
    
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    
    [self responseData:response];
    
    //[reqStr release];
    //[self shareDetails];
    
}


-(void)shareDetails{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];        //[alert release];
    }
    else
    {
        [self shareClicked];
        
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        // [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
    }
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"EVENT_SHARE"];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
                NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><eventId>%@</eventId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:EVENTID]];
                
                [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
        
        
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        
       
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
        
    }
    
    
}

-(void) emailClick{//email
    //            [defaults setValue:EVENT_SHARE forKey:KEY_SHARE_TYPE];
    
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"EVENT_SHARE"];
    __typeof(self) __weak  obj = self;
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
}

-(void) showActionSheet {
   
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Event Details Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
//    if(IPAD){
//        actionSheet.popoverPresentationController.sourceView = view;
//        actionSheet.popoverPresentationController.sourceRect = view.bounds;
//    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
            NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><eventId>%@</eventId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:EVENTID]];
            
            NSMutableString *userUrlStr=[[NSMutableString alloc]initWithFormat:@"%@ratereview/sendersharetrack",BASE_URL];
            
            NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:userUrlStr withParam:userXmlStr]];
            DLog(@"%@",response);

        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
            
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)parse_shareEvent:(NSString*)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        //        TBXMLElement *eventDetails = [TBXML childElementNamed:@"EventDetails" parentElement:tbXml.rootXMLElement];
        
        
        TBXMLElement *shareText=[TBXML childElementNamed:@"eventName" parentElement:tbXml.rootXMLElement];
        TBXMLElement *emailURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *imagePath = [TBXML childElementNamed:@"imagePath" parentElement:tbXml.rootXMLElement];
        
        if (emailURL!=nil) {
            [defaults setValue:[TBXML textForElement:emailURL] forKey:KEY_HYPERLINK];
            
        }
        
        if (shareText!=nil) {
            NSMutableString *msgString = [[NSMutableString alloc]init];
            [msgString appendFormat:@"I found this Event in the %@ mobile app and thought you might be interested:",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [msgString appendFormat:@"%@",shareString];
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            
        }
        
        if (imagePath!=nil) {
            [defaults setObject:[TBXML textForElement:imagePath] forKey:KEY_PRODUCTIMGPATH];
            NSLog(@"The image path is %@",[defaults valueForKey:KEY_PRODUCTIMGPATH]);
        }
        
        
        
        
        
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
           
            return;
        }
        
    }
}

@end

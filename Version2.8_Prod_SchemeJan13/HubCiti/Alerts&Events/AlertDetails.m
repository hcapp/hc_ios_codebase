//
//  AlertDetails.m
//  HubCiti
//
//  Created by Ajit Nadig on 11/21/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "AlertDetails.h"
#import "MainMenuViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"


NSDictionary *viewDictionary;
CGRect sDescFrame, startDateFrame, startTimeFrame, endDateFrame, endTimeFrame;

@interface AlertDetails ()

@end

@implementation AlertDetails
@synthesize alertDetails;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    // //[back release];
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    //UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    
    ////[mainPage release];
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:alertDetails.alertName forView:self withHambergur:NO];
    
   
    
    UIScrollView *scroll = [[UIScrollView alloc]init];
    //scroll.frame = self.view.bounds;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        scroll.contentSize = CGSizeMake(768, 1024);
    else
        scroll.contentSize = CGSizeMake(320, 480);
    scroll.backgroundColor = [UIColor clearColor];
    scroll.showsVerticalScrollIndicator = NO;
    scroll.showsHorizontalScrollIndicator = NO;
    scroll.scrollEnabled = YES;
    scroll.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:scroll];
    
    //add alert severity image
    if (![alertDetails.sevImgPath isEqualToString:@"N/A"]){
        severityImg = [[SdImageView alloc]init];
        severityImg.backgroundColor = [UIColor clearColor];
        [severityImg loadImage:alertDetails.sevImgPath];
        severityImg.translatesAutoresizingMaskIntoConstraints = NO;
        //[self.view addSubview:severityImg];
        [scroll addSubview:severityImg];
        // [severityImg release];
    }
    
    //add alert description
    sDescFrame = CGRectMake(5, 60, 310, 146);
    sDesc = [[UITextView alloc]init];
    sDesc.backgroundColor = [UIColor colorWithRGBInt:0xE6E6E6];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        sDesc.font = [UIFont fontWithName:@"Helvetica" size:18];
    else
        sDesc.font = [UIFont fontWithName:@"Helvetica" size:13];
    sDesc.editable = NO;
    sDesc.textAlignment = NSTextAlignmentLeft;
    sDesc.layer.borderWidth = 2.0;
    sDesc.layer.borderColor = [UIColor lightGrayColor].CGColor;
    sDesc.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    sDesc.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    sDesc.layer.shadowOpacity = 1.0f;
    sDesc.layer.shadowRadius = 1.0f;
    sDesc.scrollEnabled = NO;
    sDesc.text = alertDetails.sDesc;
    
    CGSize textViewSize = [sDesc sizeThatFits:CGSizeMake(sDescFrame.size.width, FLT_MAX)];
    
    if (textViewSize.height > sDescFrame.size.height)
        sDescFrame = CGRectMake(sDescFrame.origin.x, sDescFrame.origin.y, 310, textViewSize.height);
    sDesc.translatesAutoresizingMaskIntoConstraints = NO;
    [scroll addSubview:sDesc];
    
    //add start date label
    startDate = [[UILabel alloc]init];
    startDate.backgroundColor = [UIColor clearColor];
    startDate.textAlignment = NSTextAlignmentLeft;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        startDate.font = [UIFont fontWithName:@"Helvetica" size:18];
    else
        startDate.font = [UIFont fontWithName:@"Helvetica" size:13];
    startDate.text = [NSString stringWithFormat:@"Start Date: %@",alertDetails.startDate];
    
    if ([startDate respondsToSelector:@selector(setAttributedText:)]){//ios6 and above
        
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:startDate.text];
        NSRange selectedRange = NSMakeRange(0, 11); // 11 characters, starting at index 0
        [string beginEditing];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [string addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:@"Helvetica-Bold" size:18.0]
                           range:selectedRange];
        else
            [string addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]
                           range:selectedRange];
        [string endEditing];
        startDate.attributedText = string;
        //[string release];
    }
    
    startDate.translatesAutoresizingMaskIntoConstraints = NO;
    [scroll addSubview:startDate];
    
    startDateFrame = CGRectMake(4, sDescFrame.origin.y + sDescFrame.size.height + 20, 296, 20);
    
    //start time
    startTime = [[UILabel alloc]init];
    startTime.backgroundColor = [UIColor clearColor];
    startTime.textAlignment = NSTextAlignmentLeft;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        startTime.font = [UIFont fontWithName:@"Helvetica" size:18];
    else
        startTime.font = [UIFont fontWithName:@"Helvetica" size:13];
    startTime.text = [NSString stringWithFormat:@"Start Time: %@",alertDetails.startTime];
    
    if ([startTime respondsToSelector:@selector(setAttributedText:)]){//ios6 and above
        NSMutableAttributedString *timeString = [[NSMutableAttributedString alloc] initWithString:startTime.text];
        NSRange timeSelectedRange = NSMakeRange(0, 11); // 11 characters, starting at index 0
        [timeString beginEditing];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [timeString addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:@"Helvetica-Bold" size:18.0]
                               range:timeSelectedRange];
        else
            [timeString addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]
                               range:timeSelectedRange];
        [timeString endEditing];
        startTime.attributedText = timeString;
        //[timeString release];
    }
    
    startTime.translatesAutoresizingMaskIntoConstraints = NO;
    [scroll addSubview:startTime];
    
    startTimeFrame = CGRectMake(4, startDateFrame.origin.y + startDateFrame.size.height + 20, 296, 20);
    
    //end date
    endDate = [[UILabel alloc]init];
    endDate.backgroundColor = [UIColor clearColor];
    endDate.textAlignment = NSTextAlignmentLeft;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        endDate.font = [UIFont fontWithName:@"Helvetica" size:18];
    else
        endDate.font = [UIFont fontWithName:@"Helvetica" size:13];
    endDate.text = [NSString stringWithFormat:@"End Date: %@",alertDetails.endDate];
    
    if ([endDate respondsToSelector:@selector(setAttributedText:)]){//ios6 and above
        NSMutableAttributedString *string1 = [[NSMutableAttributedString alloc] initWithString:endDate.text];
        NSRange selectedRange1 = NSMakeRange(0, 9); // 9 characters, starting at index 0
        [string1 beginEditing];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [string1 addAttribute:NSFontAttributeName
                            value:[UIFont fontWithName:@"Helvetica-Bold" size:18.0]
                            range:selectedRange1];
        else
            [string1 addAttribute:NSFontAttributeName
                            value:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]
                            range:selectedRange1];
        [string1 endEditing];
        endDate.attributedText = string1;
        //[string1 release];
    }
    
    endDate.translatesAutoresizingMaskIntoConstraints = NO;
    [scroll addSubview:endDate];
    
    endDateFrame = CGRectMake(4, startTimeFrame.origin.y + startTimeFrame.size.height + 20, 296, 20);
    
    //end time
    endTime = [[UILabel alloc]init];
    endTime.backgroundColor = [UIColor clearColor];
    endTime.textAlignment = NSTextAlignmentLeft;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
        endTime.font = [UIFont fontWithName:@"Helvetica" size:18];
    else
        endTime.font = [UIFont fontWithName:@"Helvetica" size:13];
    endTime.text = [NSString stringWithFormat:@"End Time: %@",alertDetails.endTime];
    
    if ([endTime respondsToSelector:@selector(setAttributedText:)]){//ios6 and above
        
        NSMutableAttributedString *endTimeString = [[NSMutableAttributedString alloc] initWithString:endTime.text];
        NSRange endTimeSelectedRange = NSMakeRange(0, 9); // 9 characters, starting at index 0
        
        [endTimeString beginEditing];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
            [endTimeString addAttribute:NSFontAttributeName
                                  value:[UIFont fontWithName:@"Helvetica-Bold" size:18.0]
                                  range:endTimeSelectedRange];
        else
            [endTimeString addAttribute:NSFontAttributeName
                                  value:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]
                                  range:endTimeSelectedRange];
        
        [endTimeString endEditing];
        
        endTime.attributedText = endTimeString;
        //[endTimeString release];
    }
    
    endTime.translatesAutoresizingMaskIntoConstraints = NO;
    [scroll addSubview:endTime];
    
    
    // Calculate scroll view size
    /* float sizeOfContent = 0;
     int i;
     NSArray *subviewsArray = [NSArray arrayWithArray:scroll.subviews];
     for (i = 0; i < [scroll.subviews count]; i++) {
     
     UIView *view =[scroll.subviews objectAtIndex:i];
     sizeOfContent += view.frame.size.height;
     }
     
     // Set content size for scroll view
     scroll.contentSize = CGSizeMake(scroll.frame.size.width, sizeOfContent+200);*/
    viewDictionary = NSDictionaryOfVariableBindings(scroll,severityImg,sDesc,startDate,startTime,endDate,endTime);
    [self setConstraintsWithView:scroll];
    [scroll addConstraint:[NSLayoutConstraint constraintWithItem:endTime attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:scroll attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-100]];
    //    [scroll addConstraint:[NSLayoutConstraint constraintWithItem:endTime attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:scroll attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    //    int y = CGRectGetMaxY(((UIView*)[scroll.subviews lastObject]).frame);
    float y = endTime.frame.origin.y + endTime.frame.size.height;
    [scroll setContentSize:(CGSizeMake(CGRectGetWidth(scroll.frame), y + 50))];//50 because including height of last control
    
    // Do any additional setup after loading the view from its nib.
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}
-(void)setConstraintsWithView:(UIScrollView *) scroll
{
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        if([viewDictionary objectForKey:@"scroll"] != nil)
        {
            // Vertical constrains
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[scroll(1024)]-|",self.view.bounds.origin.y] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[scroll(768)]-|",self.view.bounds.origin.x] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"severityImg"] != nil)
        {
            // Vertical constrains
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(20)-[severityImg(40)]-|"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(300)-[severityImg(170)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"sDesc"] != nil)
        {
            // Vertical constrains
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[sDesc(%f)]-|",sDescFrame.origin.y+30,sDescFrame.size.height] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(40)-[sDesc(688)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"startDate"] != nil)
        {
            // Vertical constrains
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[startDate(20)]-|",sDescFrame.origin.y + sDescFrame.size.height + 150] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(280)-[startDate(296)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"startTime"] != nil)
        {
            // Vertical constrains
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[startTime(20)]-|",startDateFrame.origin.y + startDateFrame.size.height + 150] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(280)-[startTime(296)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"endDate"] != nil)
        {
            // Vertical constrains
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[endDate(20)]-|",startTimeFrame.origin.y + startTimeFrame.size.height + 150] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(280)-[endDate(296)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"endTime"] != nil)
        {
            // Vertical constrains
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[endTime(20)]-|",endDateFrame.origin.y + endDateFrame.size.height + 150] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(280)-[endTime(296)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
    }
    else
    {
        if([viewDictionary objectForKey:@"scroll"] != nil)
        {
            // Vertical constrains
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[scroll(480)]-|",self.view.bounds.origin.y] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[scroll(320)]-|",self.view.bounds.origin.x] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"severityImg"] != nil)
        {
            // Vertical constrains
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(20)-[severityImg(16)]-|"] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(4)-[severityImg(48)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"sDesc"] != nil)
        {
            // Vertical constrains
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[sDesc(%f)]-|",sDescFrame.origin.y,sDescFrame.size.height] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[sDesc(%f)]-|",sDescFrame.origin.x,sDescFrame.size.width] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"startDate"] != nil)
        {
            // Vertical constrains
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[startDate(20)]-|",sDescFrame.origin.y + sDescFrame.size.height + 20] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(4)-[startDate(296)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        if([viewDictionary objectForKey:@"startTime"] != nil)
        {
            // Vertical constrains
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[startTime(20)]-|",startDateFrame.origin.y + startDateFrame.size.height + 20] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(4)-[startTime(296)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"endDate"] != nil)
        {
            // Vertical constrains
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[endDate(20)]-|",startTimeFrame.origin.y + startTimeFrame.size.height + 20] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(4)-[endDate(296)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
        
        if([viewDictionary objectForKey:@"endTime"] != nil)
        {
            // Vertical constrains
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[endTime(20)]-|",endDateFrame.origin.y + endDateFrame.size.height + 20] options:0 metrics:0 views:viewDictionary]];
            
            // Horizontal constraints
            [scroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(4)-[endTime(296)]-|"] options:0 metrics:0 views:viewDictionary]];
        }
    }
}


#pragma mark navigation barButtonItem actions
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  EventsMapViewController.m
//  HubCiti
//
//  Created by Keshava on 7/24/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import "EventsMapViewController.h"
#import "RetailerAnnotation.h"

@interface EventsMapViewController ()

@end

@implementation EventsMapViewController

@synthesize latitude,longitude,address1,address2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(BOOL)locationServicesOn
{
    if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) || ([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways))
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}

-(void)mapShow
{
    //    mapView = [[MKMapView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    //    //mapView.frame=
    //    mapView.hidden = NO;
    //    [mapView setMapType:MKMapTypeStandard];
    //    [mapView setZoomEnabled:YES];
    //    [mapView setScrollEnabled:YES];
    //    [mapView setDelegate:self];
    //    CLLocationCoordinate2D southWest;
    //    CLLocationCoordinate2D northEast;
    //
    //    CLLocationCoordinate2DMake([[latitude objectAtIndex:0]doubleValue],
    //                               [[longitude objectAtIndex:0] doubleValue]);
    //    northEast = southWest;
    //
    //    RetailerAnnotation *ann = [[RetailerAnnotation alloc] init];
    //    ann.coordinate = CLLocationCoordinate2DMake([[latitude objectAtIndex:0] doubleValue],
    //                                                [[longitude objectAtIndex:0] doubleValue]);
    //    [mapView addAnnotation:ann];
    //    [annArray addObject:ann];
    //    [ann setTitle:[address1 objectAtIndex:0]];
    //    [ann setSubtitle:[address2 objectAtIndex:0]];
    //    ReleaseAndNilify(ann);
    //
    //    MKMapRect zoomRect = MKMapRectNull;
    //    for (id <MKAnnotation> annotation in mapView.annotations)
    //    {
    //        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
    //        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
    //        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    //    }
    //    [mapView setVisibleMapRect:zoomRect animated:YES];
    //
    //    [self.view addSubview:mapView];
    
    /*
     
     Google API
     
     */
    
    
    DLog(@"%@",[address2 objectAtIndex:0]);
    //    [defaults  valueForKey:@"LongVal"];
    //    [defaults  valueForKey:@"LatVal"];
    
    googleMapView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-70)];
    
    [self.view addSubview:googleMapView];
    
    
    googleMapView.scalesPageToFit = YES;
    
    NSString* apiUrlStr =nil;

    
    if (![self locationServicesOn]) {
        
        DLog(@"%@",[latitude objectAtIndex:0]);
        if ([address2 objectAtIndex:0]==nil) {
            apiUrlStr = [[NSString stringWithFormat:@"http://www.google.com/maps/place/%@",[address1 objectAtIndex:0]] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
        }
        else{
            apiUrlStr = [[NSString stringWithFormat:@"http://www.google.com/maps/place/%@%@",[address1 objectAtIndex:0],[address2 objectAtIndex:0]] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
        }
        
        DLog(@"%@",apiUrlStr);
    }
    
    else{
//        if ([[defaults  valueForKey:@"LatVal"]doubleValue]!=0.0) {
//            apiUrlStr = [NSString stringWithFormat:@"http://www.google.com/maps/dir/%f,%f/%f,%f", [[defaults  valueForKey:@"LatVal"]doubleValue ], [[defaults  valueForKey:@"LongVal"] doubleValue],[[latitude objectAtIndex:0] doubleValue],[[longitude objectAtIndex:0] doubleValue]];
//        }
//        
//        else{
            if ([address2 objectAtIndex:0]==nil) {
                apiUrlStr = [[NSString stringWithFormat:@"http://www.google.com/maps/place/%@",[address1 objectAtIndex:0]] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
            }
            else{
                apiUrlStr = [[NSString stringWithFormat:@"http://www.google.com/maps/place/%@%@",[address1 objectAtIndex:0],[address2 objectAtIndex:0]] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
            }
            
       // }
    }
    
    
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    NSURLRequest* request = [NSURLRequest requestWithURL:apiUrl];
    [googleMapView loadRequest: request];
    
    
   // [googleMapView release];
    
    
    
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
	MKPinAnnotationView* pinView = nil;
	
	if ([annotation isKindOfClass:[RetailerAnnotation class]])
    {
        // try to dequeue an existing pin view first
        static NSString* RetailerAnnotationIdentifier = @"retailerAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:RetailerAnnotationIdentifier];
		
        if (!pinView)
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
												   initWithAnnotation:annotation reuseIdentifier:RetailerAnnotationIdentifier] ;
            //customPinView.pinColor = MKPinAnnotationColorPurple;
            customPinView.animatesDrop = YES;
            
            customPinView.canShowCallout = YES;
            //UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            //customPinView.rightCalloutAccessoryView = infoButton;
            
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
    }
	pinView.canShowCallout = NO;
	return pinView;
}

//- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
//{
//    
//}
-(void)popBackToPreviousPage{
//    [address1 release];
//    [address2 release];
//    [latitude release];
//    [longitude release];
    
    [self.navigationController popViewControllerAnimated:NO];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
     self.title=NSLocalizedString(@" Choose Your Location:", @"  Choose Your Location:");
    
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];

    
    [self mapShow];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

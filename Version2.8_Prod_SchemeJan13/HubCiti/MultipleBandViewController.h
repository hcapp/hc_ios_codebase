//
//  MultipleBandViewController.h
//  HubCiti
//
//  Created by Nikitha on 9/14/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MultipleBandResponse.h"
#import "SortAndFilter.h"
#import "RetailersListViewController.h"
#import "BandRetailerSummaryViewController.h"
@interface MultipleBandViewController : UIViewController
{
    
    NSMutableArray * array_MultipleBands;
    NSNumber *rowCountOfTotalCells;
}
@property(strong,nonatomic) NSNumber * nextPage;
@property(strong,nonatomic) MultipleBandResponse * obj_multipleBandResponse;
@property(strong,nonatomic) NSMutableArray * array_MultipleBandList;
@property(nonatomic,strong) SortAndFilter *sortObj,*savedSortObj;
@property(nonatomic,retain) SortAndFilter *sortFilObj;
@property(nonatomic,retain) RetailersListViewController *retailerListScreen;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,retain) EventsListViewController *iEventsListViewController;
@property(nonatomic,strong) UIView *footer;
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property(nonatomic,strong) NSString * navigationTitleText;
@property(nonatomic,strong)  BandRetailerSummaryViewController* retailerSummaryPage ;
@property (strong, nonatomic) IBOutlet UITableView *multipleTableViewOutlet;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintForTableView;
@end

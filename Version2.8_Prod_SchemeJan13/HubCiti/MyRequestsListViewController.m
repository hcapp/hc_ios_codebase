//
//  MyRequestsListViewController.m
//  HubCiti
//
//  Created by Bindu M on 9/29/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "MyRequestsListViewController.h"
#import "SpecialOffersViewController.h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "HubCitiConstants.h"
#import "AppDelegate.h"
#import "SingleCatRetailers.h"
#import "EventsListViewController.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "FindViewController.h"
#import "CityExperienceViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "SettingsViewController.h"
#import "MainMenuViewController.h"
#import "FilterListViewController.h"
#import "FilterRetailersList.h"
#import "CouponsViewController.h"
#import "FAQCategoryList.h"
#import "FundraiserListViewController.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "PreferredCategoriesScreen.h"
#import "WebBrowserViewController.h"
#import "AnythingPage.h"
#import "ViewMyRequestsTableViewCell.h"
#import "MyRequestDetailViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "UserInformationViewController.h"
#import "RequestViewFiltersController.h"
#import "HTTPClient.h"
#import "CouponsMyAccountsViewController.h"
#import "GetUserInfoResponse.h"
#import "CouponsInDealViewController.h"


@interface MyRequestsListViewController ()
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@end

@implementation MyRequestsListViewController{
    float fontSize;
    NSString *refString;
    NSString *statusString;
    NSString *summaryString;
    bottomButtonView *view;
}

@synthesize arrBottomBtnDO, ReqListArr,anyVC, MakeReqDetailObj,sortObj,infoResponse,emailSendingVC;
#pragma mark - Service Respone methods




- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
    
}





- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject

{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    
    
    
    [self responseData:responseObject];
    
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    refString = nil;
    statusString = nil;
    summaryString = nil;
    self.title = @"My Requests";
   
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *btn = [UtilityManager customizeBarButtonItem:@"Filter"];
    [btn addTarget:self action:@selector(SortPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *Sort = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = Sort;
   

    [self settableViewOnScreen];
    if([arrBottomBtnDO count] > 0)
        [self setBottomBarMenu];
    
    
    if (IPAD) {
        fontSize = 16;
    }
    else
    {
        fontSize = 12;
    }
    
    DLog(@"ReqListArr %@", ReqListArr);
    reqDetailPasstoNext = [[NSMutableDictionary alloc]init];
    sortObj =  [[SortAndFilter alloc]init];


}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    if([defaults valueForKey:@"isComingFromFilter"]){
        ReleaseAndNilify(myReqListTbl);
        sortObj = [[SortAndFilter alloc]initWithObject:[self loadCustomObjectWithKey:@"SortObject"]];
        DLog(@"%d", sortObj.alphabetSelectedGovQA);
        DLog(@"%d", sortObj.typeSelectedGovQA);
        DLog(@"%d", sortObj.nameSelectedGovQA);
        [ReqListArr removeAllObjects];
        [self requestOnSortMakeReq:sortObj];
        [defaults setValue:nil forKey:@"SortObject"];
        [defaults setValue:nil forKey:@"isComingFromFilter"];
    }
    
}
- (SortAndFilter *)loadCustomObjectWithKey:(NSString *)key {
    
    NSData *encodedObject = [defaults objectForKey:key];
    SortAndFilter *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}

-(void)SortPressed{
    RequestViewFiltersController *sortObjGovQA = [[RequestViewFiltersController alloc] init];
    sortObjGovQA.sortObj = [[SortAndFilter alloc]initWithObject:sortObj];
    
    [self.navigationController pushViewController:sortObjGovQA animated:YES];
}

-(void)requestOnSortMakeReq:(SortAndFilter *)sortedObj{
    if([Network currentReachabilityStatus]==0){
        [UtilityManager showAlert];
        
    }
    else{
        [HubCitiAppDelegate showActivityIndicator];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        
         NSString *baseURL=[NSString stringWithFormat:@"%@govqa/getallrequestsbycustmr",BASE_URL]; // @"http://10.10.222.223:8080/HubCiti2.3/govqa/makerequest"
        NSString *filterName=@"NONE";
        
        if(sortedObj.requestViewFilter){
            filterName=sortedObj.requestViewFilter;
        }
        
       // NSString *baseURL=[NSString stringWithFormat:@"http://sdw2107:8080/HubCiti2.3.2/govqa/getallrequestsbycustmr"];
        NSDictionary *parameters = @{@"email":[defaults stringForKey:@"emailID"],@"filterName":filterName};
        NSLog(@"JSON: %@", parameters);
        [manager POST:baseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            [HubCitiAppDelegate removeActivityIndicator];
            [self parseViewMyReq:responseObject];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [HubCitiAppDelegate removeActivityIndicator];
            
            NSLog(@"Error: %@", error);
        }];
    }
}

-(void)parseViewMyReq:(id)responseObj{
    
    DLog(@"responseObj %@", responseObj);
    NSDictionary *responseData=responseObj;
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    DLog(@"responseCode %ld", (long)responseCode);
    DLog(@"responsestr %@", responseText);
    NSMutableArray *requestObjArray = nil;
    if (responseCode == 10000) {
        requestObjArray = [responseData objectForKey:@"Table"];
        [ReqListArr removeAllObjects];
        for (int i=0; i<[requestObjArray count]; i++) {
            DLog(@"requestObjArray %@", [requestObjArray objectAtIndex:i]);
            [ReqListArr addObject:[requestObjArray objectAtIndex:i]];
        }
    }
    else{
        [UtilityManager showAlert:nil msg:responseText];
        
    }
    [self settableViewOnScreen];
    [myReqListTbl reloadData];
}

-(void)settableViewOnScreen
{
    
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    if (!myReqListTbl) {
        
        if([arrBottomBtnDO count] > 0){
            myReqListTbl = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-50) style:UITableViewStylePlain];
        }
        else{
            myReqListTbl = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-yVal) style:UITableViewStylePlain];
        }
    }
    myReqListTbl.dataSource=self;
    myReqListTbl.delegate=self;
    [myReqListTbl setBackgroundColor:[UIColor whiteColor]];
    myReqListTbl.tableFooterView.hidden=YES;
    myReqListTbl.translatesAutoresizingMaskIntoConstraints=YES;
    [self.view addSubview:myReqListTbl];
    myReqListTbl.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
}

-(float) getLabelSize : (NSString*) labelString
{
    float fontsize;
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
        fontsize=15;
    }
    else
    {
        fontsize=12;
    }
    CGSize constraint = CGSizeMake(SCREEN_WIDTH, CGFLOAT_MAX);
   
  
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    CGSize size = [labelString boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:fontsize], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    return size.height +VARIABLE_HEIGHT(8);
    
}

#pragma mark tableView delegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [ReqListArr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self getLabelSize:[[ReqListArr objectAtIndex:indexPath.row]objectForKey:@"description"]]+[self getLabelSize:[[ReqListArr objectAtIndex:indexPath.row]objectForKey:@"referNum"]]+[self getLabelSize:[[ReqListArr objectAtIndex:indexPath.row]objectForKey:@"status"]]+VARIABLE_HEIGHT(8);
    
//    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
//        return VARIABLE_HEIGHT(100);
//    }
//    else
//    {
//        return VARIABLE_HEIGHT(180);
//    }
//    
    
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
//        return 35.0;
//    }
//    else
//    {
//        return 36.0;
//    }
//}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    float labelFont = 15.0;
//    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
//        labelFont = 18.0;
//    }
//    UILabel *headerName = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 315, 26)];
//    headerName.backgroundColor = [UIColor grayColor];
//    headerName.font = [UIFont boldSystemFontOfSize:labelFont];
//    if (IPAD) {
//        [headerName setText:[NSString stringWithFormat:@"                   %@                                        %@", @"Ref. No.", @"Summary"]];
//    }
//    else{
//    [headerName setText:[NSString stringWithFormat:@"   %@                     %@", @"Ref. No.", @"Summary"]];
//    }
//    return [headerName ;
//}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    ViewMyRequestsTableViewCell *cell = (ViewMyRequestsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
    
    if(cell == nil)
    {
        cell = [[ViewMyRequestsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
        
    }
    
     cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    
    cell.refNum.frame = CGRectMake(10, 0, VARIABLE_WIDTH(60),  [self getLabelSize:@"Ref Num: "]);
    cell.status.frame = CGRectMake(10, cell.refNum.frame.origin.y+cell.refNum.frame.size.height, VARIABLE_WIDTH(60),  [self getLabelSize:@"Status: "]);
    cell.summary.frame = CGRectMake(10, cell.status.frame.origin.y+cell.status.frame.size.height, VARIABLE_WIDTH(60),  [self getLabelSize:@"Summary: "]+VARIABLE_HEIGHT(5));
   
    cell.refNum.text = [NSString stringWithFormat: @"Ref.No: "];
    cell.status.text = [NSString stringWithFormat: @"Status: "];
    cell.summary.text = [NSString stringWithFormat: @"Summary: "];
    cell.refNum.font = [UIFont boldSystemFontOfSize:fontSize];
    cell.summary.font = [UIFont boldSystemFontOfSize:fontSize];
     cell.status.font = [UIFont boldSystemFontOfSize:fontSize];

    cell.refNumField.frame = CGRectMake(cell.refNum.frame.origin.x+cell.refNum.frame.size.width+VARIABLE_WIDTH(10), 0, cell.contentView.frame.size.width/2, [self getLabelSize:[[ReqListArr objectAtIndex:indexPath.row]objectForKey:@"referNum"]]);
    cell.statusField.frame = CGRectMake(cell.status.frame.origin.x+cell.status.frame.size.width+VARIABLE_WIDTH(10), cell.refNum.frame.origin.y+cell.refNum.frame.size.height, cell.contentView.frame.size.width/2, [self getLabelSize:[[ReqListArr objectAtIndex:indexPath.row]objectForKey:@"status"]]);
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone){
    cell.summaryField.frame = CGRectMake(cell.summary.frame.origin.x+cell.summary.frame.size.width+VARIABLE_WIDTH(10), cell.status.frame.origin.y+cell.status.frame.size.height, cell.contentView.frame.size.width*3/4, [self getLabelSize:[[ReqListArr objectAtIndex:indexPath.row]objectForKey:@"description"]]+VARIABLE_HEIGHT(5));
    }
    else
    {
        cell.summaryField.frame = CGRectMake(cell.summary.frame.origin.x+cell.summary.frame.size.width+VARIABLE_WIDTH(10), cell.status.frame.origin.y+cell.status.frame.size.height, cell.contentView.frame.size.width+VARIABLE_HEIGHT(5), [self getLabelSize:[[ReqListArr objectAtIndex:indexPath.row]objectForKey:@"description"]]+VARIABLE_HEIGHT(5));
    }


    cell.refNumField.text = [[ReqListArr objectAtIndex:indexPath.row]objectForKey:@"referNum"];
    cell.statusField.text = [[ReqListArr objectAtIndex:indexPath.row]objectForKey:@"status"];
    cell.summaryField.text = [[ReqListArr objectAtIndex:indexPath.row]objectForKey:@"description"];
    cell.refNumField.font = [UIFont systemFontOfSize:fontSize];
    cell.summaryField.font = [UIFont systemFontOfSize:fontSize];
    cell.statusField.font = [UIFont systemFontOfSize:fontSize];


    
    
    
//    // making text property to underline text-
//    [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
//    // using text on button
//    [cell.btnField setAttributedTitle: titleString forState:UIControlStateNormal];
//    [cell.btnField addTarget:self action:@selector(clickOnRefNumBtn:) forControlEvents:UIControlEventTouchUpInside];
//    
//    frame = CGRectMake(cell.btnField.frame.origin.x + cell.btnField.frame.size.width + VARIABLE_WIDTH(25), cell.btnField.frame.origin.y , VARIABLE_WIDTH(185) - VARIABLE_WIDTH(40), cell.btnField.frame.size.height);
//    
//    cell.labelField.frame = frame;
//    cell.labelField.text = [[ReqListArr objectAtIndex:indexPath.row]objectForKey:@"description"];
//    cell.labelField.font = [UIFont systemFontOfSize:fontSize];
    
//    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(cell.btnField.frame.origin.x + cell.btnField.frame.size.width + VARIABLE_WIDTH(20), 0, 2, cell.contentView.bounds.size.height)];
//    lineView.backgroundColor = [UIColor darkGrayColor];
//    lineView.autoresizingMask = 0x3f;
//    [cell.contentView addSubview:lineView];
//    
    return cell;
    
}

#pragma mark on click of Reference num
-(void)clickOnRefNumBtn:(id) sender{
    
   CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:myReqListTbl];
    NSIndexPath *indexPath = [myReqListTbl indexPathForRowAtPoint:buttonPosition];
//  
   ViewMyRequestsTableViewCell *cell = ( ViewMyRequestsTableViewCell *)[myReqListTbl cellForRowAtIndexPath:indexPath];
//    NSLog(@"%@", cell.btnField.titleLabel.text);
    btnText = cell.refNumField.text;
   
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    //
    ViewMyRequestsTableViewCell *cell = ( ViewMyRequestsTableViewCell *)[myReqListTbl cellForRowAtIndexPath:indexPath];
    //    NSLog(@"%@", cell.btnField.titleLabel.text);
    btnText = cell.refNumField.text;
    reqDetailPasstoNext = [ReqListArr objectAtIndex:indexPath.row];
    DLog(@"reqDetailPasstoNext %@", reqDetailPasstoNext);

   [self requestForRefNumDetail];
}

-(void)requestForRefNumDetail{
    if([Network currentReachabilityStatus]==0){
        [UtilityManager showAlert];
        
    }
    else{
        [HubCitiAppDelegate showActivityIndicator];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        
        NSString *baseURL=[NSString stringWithFormat:@"%@govqa/getreqbyreferno",BASE_URL]; // @"http://10.10.222.223:8080/HubCiti2.3/govqa/makerequest"
        
        NSDictionary *parameters = @{@"referNum":btnText};
        [manager POST:baseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) { //sdw2107
            NSLog(@"JSON: %@", responseObject);
            [HubCitiAppDelegate removeActivityIndicator];
            [self parseForRefNumDetail:responseObject];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [HubCitiAppDelegate removeActivityIndicator];
            
            NSLog(@"Error: %@", error);
        }];
    }
}


-(void)parseForRefNumDetail:(id)responseObj{
    
    DLog(@"responseObj %@", responseObj);
    NSDictionary *responseData=responseObj;
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    DLog(@"responseCode %ld", (long)responseCode);
    DLog(@"responsestr %@", responseText);
    NSMutableArray *requestObjArray = nil;
    NSString *title = nil;
    NSString *refNum = nil;
    NSString *status = nil;
    if (responseCode == 10000) {
        requestObjArray = [responseData objectForKey:@"Table"];
        title = [NSString stringWithFormat:@"%@",[responseData objectForKey:@"description"]];
        refNum = [NSString stringWithFormat:@"%@",[responseData objectForKey:@"referNum"]];
        status = [NSString stringWithFormat:@"%@",[responseData objectForKey:@"status"]];
        DLog(@"%@ %@ %@", title, refNum, status );
    }
    else{
        [UtilityManager showAlert:nil msg:responseText];
      
        return;
    }
    
    MyRequestDetailViewController *myReq = [[MyRequestDetailViewController alloc]initWithNibName:@"MyRequestDetailViewController" bundle:[NSBundle mainBundle]];
    myReq.arrBottomBtnDO = arrBottomBtnDO;
    myReq.ReqDetailArray = requestObjArray;
    myReq.staticReqDictionary = reqDetailPasstoNext;
    myReq.pageTitle = title;
    myReq.referenceNum = refNum;
    [self.navigationController pushViewController:myReq animated:YES];
    
}

-(void)setBottomBarMenu
{
    // Calculate Y position for View
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [arrBottomBtnDO count]; btnLoop++)
    {
        bottomButtonDO *obj_eventBottomDO = [arrBottomBtnDO objectAtIndex:btnLoop];
        
       view = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, 80,bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            view = [bottomButtonView initWithTitle:obj_eventBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrBottomBtnDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrBottomBtnDO count],bottomBarButtonHeight) imageName:obj_eventBottomDO.bottomBtnImg img_off:obj_eventBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_eventBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
    }
}


-(void)bottomButtonPressed:(id)sender
{
    [defaults setBool:NO forKey:@"ViewMore"];
    [defaults setBool:YES forKey:BottomButton];
    //   [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    //NSlog(@"The tag is %d",tag);
    bottomButtonDO *obj_bottomBottomDO = [arrBottomBtnDO  objectAtIndex:tag];
    
    [defaults setValue:obj_bottomBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_bottomBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_bottomBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_bottomBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_bottomBottomDO.btnLinkTypeName);
    if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    
    else{ //other menu types
        switch ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
              //  [hotDeals release];
                break;
                
            case 1://Scan Now
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                [self navigateToEventList];
        }
                break;
            case 4://Whats NearBy
                [self navigateToWhatsNearBy];
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                
                
                [self navigateToFindView];
            }
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
                
                break;
                
            case 8://Visitors Bureau
                
                break;
                
            case 9://transportation
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
        }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=TRUE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:obj_bottomBottomDO.btnLinkID];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                    
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                   // CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];;
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
              //  [dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }break;
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                      //  [inform release];
                    }
                    
                }
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                           
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                          //  [citPref release];
                        }
                    }
                }
                
            }break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
            default:
                break;
        }
    }
    
}
-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}
-(void)parseGetUserData:(id)response
{
        
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
               isRemember = YES;
        
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}



-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}


-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}

// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case PARTNERRET:
            [self parse_PartnerRet:response];
            break;
            //        case gETPARTNER:
            //            [self parse_GetPartners:response];
            //            break;
            
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
            
        default:
            break;
            
            
            
    }
    
}

-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}

//Call Find Service and Class
-(void)navigateToFindView
{
    
    //    [defaults setValue:@"0" forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    
    navigatedFromMainMenu = FALSE;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}
-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }

    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;

        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
    [self.navigationController popToRootViewControllerAnimated:NO];
    }
}
-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}


-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=FALSE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    else if ([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}

#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
    }
}

-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
        
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}


-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
            
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}

//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
           // NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];                    //[alert release];
                    
                    //                @"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in both iPhone Settings and Application Settings"
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        

        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}


#pragma mark navigation barButtonItem actions
-(void)popBackToPrevPage{
     [self.navigationController popViewControllerAnimated:NO];
}

- (void)dealloc {
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

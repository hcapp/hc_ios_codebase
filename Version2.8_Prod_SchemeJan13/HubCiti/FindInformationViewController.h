//
//  FindInformationViewController.h
//  HubCiti
//
//  Created by Ashika on 10/9/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "CommonUtility.h"
#import "SortAndFilter.h"
@class AnyViewController;
@class EmailShareViewController;
@interface FindInformationViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,MFMessageComposeViewControllerDelegate,UISearchBarDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>{
    WebRequestState iWebRequestState;
    CommonUtility *common;
    BOOL isRemember;
    UITableView *reportProblemTbl;
    NSMutableArray *findInformationArray;
    NSMutableArray *arrBottomBtnDO;
    NSMutableArray *arrBottomButtonViewContainer;
    UIButton *cancelBtn;
    NSInteger nextLevel;
    IBOutlet UISearchBar *search;
    UITableView *findInfoTable;
    int lastVisitedRecord;
    NSString *searchkey;
    int totalRecord;
    CustomizedNavController *cusNav;

}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic,retain) NSMutableArray *arrBottomBtnDO;
@property(nonatomic,retain) SortAndFilter *sortObj;
@property(nonatomic,assign) NSInteger nextLevel;
@property (nonatomic,retain) NSMutableArray *findInformationArray;

@end

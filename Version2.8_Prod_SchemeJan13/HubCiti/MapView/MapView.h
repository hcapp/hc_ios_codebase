//
//  MapView.h
//  Scansee
//
//  Created by Sandeep S on 12/13/11.
//  Copyright 2011 span infotech india ptv. ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "RegexKitLite.h"
#import "Place.h"
#import "PlaceMark.h"
#import "DestMark.h"
#import <WebKit/WebKit.h>
@interface MapView : UIView<MKMapViewDelegate,WKNavigationDelegate> {
    
	MKMapView* mapView;
	UIImageView* routeView;
    WKWebView *googleMapView;
	NSArray* routes;
	
	UIColor* lineColor;
}

@property (nonatomic, strong) UIColor* lineColor;

-(void) showRouteFrom: (Place*) f to:(Place*) t;


@end

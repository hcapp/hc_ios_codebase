//
//  Place.m
//  Scansee
//
//  Created by Sandeep S on 12/13/11.
//  Copyright 2011 span infotech india ptv. ltd. All rights reserved.
//


#import "Place.h"


@implementation Place

@synthesize name;
@synthesize addr;
@synthesize latitude;
@synthesize longitude;


@end

//
//  Place.h
//  Scansee
//
//  Created by Sandeep S on 12/13/11.
//  Copyright 2011 span infotech india ptv. ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Place : NSObject {

	NSString* name;
	NSString* addr;
	double latitude;
	double longitude;
}

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* addr;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

@end

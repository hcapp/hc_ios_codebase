

#import "RetailerAnnotation.h"





@implementation RetailerAnnotation

@synthesize coordinate;

//- (CLLocationCoordinate2D)coordinate;
//{
//    CLLocationCoordinate2D theCoordinate;
//    theCoordinate.latitude = 37.810000;
//    theCoordinate.longitude = -122.477989;
//    return theCoordinate; 
//}
//
//// required if you set the MKPinAnnotationView's "canShowCallout" property to YES
//- (NSString *)title
//{
//    return @"Golden Gate Bridge";
//}
//
//// optional
//- (NSString *)subtitle
//{
//    return @"Opened: May 27, 1937";
//}

-init
{
    return self;
}

-initWithCoordinate:(CLLocationCoordinate2D)inCoord
{
    coordinate = inCoord;
    return self;
}
- (NSString *)subtitle{
    return mSubTitle;
}
- (NSString *)title{
    return mTitle;
}
-(NSString *)idString{
    return mIdString;
}

- (void) setTitle: (NSString *) title
{
    mTitle = title;
}

- (void) setSubtitle: (NSString *) subtitle
{
    mSubTitle = subtitle;
}
- (void) setIdString:(NSString *)idString
{
    mIdString = idString;
}


@end




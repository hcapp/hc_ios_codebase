//
//  DestMark.h
//  Scansee
//
//  Created by Stephanie Blackwell on 4/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Place.h"

@interface DestMark : NSObject <MKAnnotation> {
    
	CLLocationCoordinate2D coordinate;
	Place* place;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) Place* place;

-(id) initWithPlace: (Place*) p;

@end

//
//  PlaceMark.m
//  Scansee
//
//  Created by Sandeep S on 12/13/11.
//  Copyright 2011 span infotech india ptv. ltd. All rights reserved.
//

#import "PlaceMark.h"


@implementation PlaceMark

@synthesize coordinate;
@synthesize place;

-(id) initWithPlace: (Place*) p
{
	self = [super init];
	if (self != nil) {
		coordinate.latitude = p.latitude;
		coordinate.longitude = p.longitude;
		self.place = p;
	}
	return self;
}

- (NSString *)subtitle
{
	return self.place.addr;
}
- (NSString *)title
{
	return self.place.name;
}



@end

//
//  PlaceMark.h
//  Scansee
//
//  Created by Sandeep S on 12/13/11.
//  Copyright 2011 span infotech india ptv. ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Place.h"

@interface PlaceMark : NSObject <MKAnnotation> {

	CLLocationCoordinate2D coordinate;
	Place* place;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) Place* place;

-(id) initWithPlace: (Place*) p;

@end

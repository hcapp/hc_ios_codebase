//
//  MapView.m
//  Scansee
//
//  Created by Sandeep S on 12/13/11.
//  Copyright 2011 span infotech india ptv. ltd. All rights reserved.
//

#import "MapView.h"
///#import "FindNearByRetailerMap.h"
//#import "FindNearByRetailerDetails.h"

@interface MapView()

-(NSMutableArray *)decodePolyLine: (NSMutableString *)encoded;
-(void) updateRouteView;
-(void) calculateRoutesFrom:(CLLocationCoordinate2D) from to: (CLLocationCoordinate2D) to;
-(void) centerMap;

@end

@implementation MapView

@synthesize lineColor;

- (id) initWithFrame:(CGRect) frame
{
	self = [super initWithFrame:frame];
    
	if (self != nil) {
//		mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
//		mapView.showsUserLocation = YES;
        
//		[mapView setDelegate:self];
        
        
        /*
         
         Google API
         
         */
        
        googleMapView = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];

		[self addSubview:googleMapView];
        
        googleMapView.navigationDelegate = self;
        
       // googleMapView.scalesPageToFit = YES;
        
        
        
    
        
//        NSMutableString *urlString = [[[NSMutableString alloc]init];
//        urlString = [NSMutableString stringWithFormat:@"%@",[defaults valueForKey:KEY_URL]];
//        NSURL* url;
//        url = [NSURL URLWithString:urlString];
        
        
//        NSString* apiUrlStr = [NSString stringWithFormat:@"http://www.google.com/maps/dir/%f,%f/%f,%f", f.latitude,f.longitude, t.latitude, t.longitude];
//        NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
//        NSURLRequest* request = [NSURLRequest requestWithURL:apiUrl];
//        _request = [NSMutableURLRequest requestWithURL:url];
//        [googleMapView loadRequest: request];
//		routeView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, mapView.frame.size.width, mapView.frame.size.height)];
//		routeView.userInteractionEnabled = NO;
//		[mapView addSubview:routeView];
		
		self.lineColor = [UIColor colorWithWhite:0.2 alpha:0.5];
	}
	return self;
}

-(NSMutableArray *)decodePolyLine: (NSMutableString *)encoded {
	[encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\"
								options:NSLiteralSearch
								  range:NSMakeRange(0, [encoded length])];
	NSInteger len = [encoded length];
	NSInteger index = 0;
	NSMutableArray *array = [[NSMutableArray alloc] init];
	NSInteger lat=0;
	NSInteger lng=0;
	while (index < len) {
		NSInteger b;
		NSInteger shift = 0;
		NSInteger result = 0;
		do {
			b = [encoded characterAtIndex:index++] - 63;
			result |= (b & 0x1f) << shift;
			shift += 5;
		} while (b >= 0x20);
		NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
		lat += dlat;
		shift = 0;
		result = 0;
		do {
			b = [encoded characterAtIndex:index++] - 63;
			result |= (b & 0x1f) << shift;
			shift += 5;
		} while (b >= 0x20);
		NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
		lng += dlng;
		NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
		NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
		printf("[%f,", [latitude doubleValue]);
		printf("%f]", [longitude doubleValue]);
		CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
		[array addObject:loc];
	}
	
	return array;
}

-(void) calculateRoutesFrom:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t {
	//NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
	//NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
 
//	NSString* apiUrlStr = [NSString stringWithFormat:@"http://www.google.com/maps/dir/%f,%f/%f,%f", f.latitude,f.longitude, t.latitude, t.longitude];
//	NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
//	DLog(@"api url: %@", apiUrl);
//    NSError* error = nil;
//	NSString *apiResponse = [NSString stringWithContentsOfURL:apiUrl encoding:NSUTF8StringEncoding error:&error];
//	NSString* encodedPoints = [apiResponse stringByMatching:@"points:\\\"([^\\\"]*)\\\"" capture:1L];
//
//	return [self decodePolyLine:[encodedPoints mutableCopy]];
}

-(void) centerMap {
    
    @try
    {
        MKCoordinateRegion region;
        CLLocationDegrees maxLat = -90;
        CLLocationDegrees maxLon = -180;
        CLLocationDegrees minLat = 90;
        CLLocationDegrees minLon = 180;
        for(int idx = 0; idx < routes.count; idx++)
        {
            CLLocation* currentLocation = [routes objectAtIndex:idx];
            if(currentLocation.coordinate.latitude > maxLat)
                maxLat = currentLocation.coordinate.latitude;
            if(currentLocation.coordinate.latitude < minLat)
                minLat = currentLocation.coordinate.latitude;
            if(currentLocation.coordinate.longitude > maxLon)
                maxLon = currentLocation.coordinate.longitude;
            if(currentLocation.coordinate.longitude < minLon)
                minLon = currentLocation.coordinate.longitude;
        }
        region.center.latitude     = (maxLat + minLat) / 2;
        region.center.longitude    = (maxLon + minLon) / 2;
        region.span.latitudeDelta  = maxLat - minLat;
        region.span.longitudeDelta = maxLon - minLon;
        
        [mapView setRegion:region animated:YES];
    }
    @catch (NSException *e )
    {
        DLog(@"%@",e);
        
        [UtilityManager showAlert:NSLocalizedString(@"Sorry!",@"Sorry!") msg:NSLocalizedString(@"Could not calculate directions\n\n",@"Could not calculate directions\n\n")];
       
        
    }
    /*    @finally {
     
     DLog(@"finally");
     
     }*/
}

-(BOOL)locationServicesOn
{
    if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) || ([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways))
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}

-(void) showRouteFrom: (Place*) f to:(Place*) t {
	
	if(routes) {
		[mapView removeAnnotations:[mapView annotations]];
		//[routes release];
	}

	PlaceMark* from = [[PlaceMark alloc] initWithPlace:f];
	DestMark* to = [[DestMark alloc] initWithPlace:t];
	
	[mapView addAnnotation:from];
	[mapView addAnnotation:to];
    NSString* apiUrlStr =nil;
    
    if (![self locationServicesOn]) {
        
//        DLog(@"%@",[latitude objectAtIndex:0]);
        apiUrlStr = [[NSString stringWithFormat:@"http://www.google.com/maps/place/%@",t.addr] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
        
        DLog(@"%@",apiUrlStr);
    }
    
    else{
  
//        if (f.latitude==0.0) {
//            apiUrlStr = [[NSString stringWithFormat:@"http://www.google.com/maps/place/%@",t.addr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        }
//        else{
//    apiUrlStr = [NSString stringWithFormat:@"http://www.google.com/maps/dir/%f,%f/%f,%f", f.latitude,f.longitude, t.latitude, t.longitude];
//        }
        
         apiUrlStr = [[NSString stringWithFormat:@"http://www.google.com/maps/place/%@",t.addr] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
//        
    }
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    NSURLRequest* request = [NSURLRequest requestWithURL:apiUrl];
    [googleMapView loadRequest: request];
	
    
   // [googleMapView release];
 
 
 
    
    
  /*
    
    // Create an MKMapItem to pass to the Maps app
    
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:to.coordinate
                                                   addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:t.name];
    [mapItem setPhoneNumber:[defaults valueForKey:@"contactNumber"]];
    
    // Set the directions mode to "Driving"
    // Can use MKLaunchOptionsDirectionsModeDriving instead
    NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
    // Get the "Current User Location" MKMapItem
    MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
    // Pass the current location and destination map items to the Maps app
    // Set the direction mode in the launchOptions dictionary
    [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem]
                   launchOptions:launchOptions];
    */
    
//	routes =[self calculateRoutesFrom:from.coordinate to:to.coordinate];
	
//	[self updateRouteView];
//	[self centerMap];
}


-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
	MKPinAnnotationView* pinView = nil;
	
	if ([annotation isKindOfClass:[DestMark class]]) 
    {
        // try to dequeue an existing pin view first
        static NSString* RetailerAnnotationIdentifier = @"retailerAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:RetailerAnnotationIdentifier];
		
        if (!pinView)
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
												   initWithAnnotation:annotation reuseIdentifier:RetailerAnnotationIdentifier];
            customPinView.pinColor = MKPinAnnotationColorPurple;
            customPinView.animatesDrop = YES;
            customPinView.canShowCallout = YES;
            // UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            //customPinView.rightCalloutAccessoryView = infoButton;
            
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
    }
    if ([annotation isKindOfClass:[PlaceMark class]]) 
    {
        // try to dequeue an existing pin view first
        static NSString* RetailerAnnotationIdentifier = @"retailerAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:RetailerAnnotationIdentifier];
		
        if (!pinView)
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
												   initWithAnnotation:annotation reuseIdentifier:RetailerAnnotationIdentifier];
            customPinView.pinColor = MKPinAnnotationColorGreen;
            customPinView.animatesDrop = YES;
            customPinView.canShowCallout = YES;
            // UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            //customPinView.rightCalloutAccessoryView = infoButton;
            
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
    }
	pinView.canShowCallout = YES;
	return pinView;
}


-(void) updateRouteView {
    CGColorSpaceRef colourSpaceRef= CGColorSpaceCreateDeviceRGB();
	CGContextRef context = 	CGBitmapContextCreate(nil, 
												  routeView.frame.size.width, 
												  routeView.frame.size.height, 
												  8, 
												  4 * routeView.frame.size.width,
												  colourSpaceRef,
												  (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
	
	CGContextSetStrokeColorWithColor(context, lineColor.CGColor);
	CGContextSetRGBFillColor(context, 0.0, 0.0, 1.0, 1.0);
	CGContextSetLineWidth(context, 3.0);
	
	for(int i = 0; i < routes.count; i++) {
		CLLocation* location = [routes objectAtIndex:i];
		CGPoint point = [mapView convertCoordinate:location.coordinate toPointToView:routeView];
		
		if(i == 0) {
			CGContextMoveToPoint(context, point.x, routeView.frame.size.height - point.y);
		} else {
			CGContextAddLineToPoint(context, point.x, routeView.frame.size.height - point.y);
		}
	}
	
	CGContextStrokePath(context);
	
	CGImageRef image = CGBitmapContextCreateImage(context);
	UIImage* img = [UIImage imageWithCGImage:image];
    routeView.image = img;
    
    
    CGImageRelease(image);
    CGColorSpaceRelease(colourSpaceRef);
	CGContextRelease(context);
    
}

#pragma mark mapView delegate functions
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
	routeView.hidden = YES;
}



- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
	[self updateRouteView];
	routeView.hidden = NO;
	[routeView setNeedsDisplay];
}


@end

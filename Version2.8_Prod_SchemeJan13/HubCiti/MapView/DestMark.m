//
//  DestMark.m
//  Scansee
//
//  Created by Stephanie Blackwell on 4/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DestMark.h"

@implementation DestMark

@synthesize coordinate;
@synthesize place;

-(id) initWithPlace: (Place*) p
{
	self = [super init];
	if (self != nil) {
		coordinate.latitude = p.latitude;
		coordinate.longitude = p.longitude;
		self.place = p;
	}
	return self;
}

- (NSString *)subtitle
{
	return self.place.addr;
}
- (NSString *)title
{
	return self.place.name;
}



@end

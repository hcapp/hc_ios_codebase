

#import <MapKit/MapKit.h>
@interface RetailerAnnotation : NSObject <MKAnnotation>
{
	CLLocationCoordinate2D coordinate;
	
	NSString *mTitle;
	NSString *mSubTitle;
    NSString *mIdString;
    //FindNearByResultListMapViewController *find;
}

@property (nonatomic,strong) NSString *idString;
@property (nonatomic) CLLocationCoordinate2D coordinate;
- (void) setTitle: (NSString *) title;
- (void) setSubtitle: (NSString *) subtitle;
- (void) setIdString: (NSString *) idString;

@end
//
//  HttpClient.h
//  JSON_HelloWorld
//
//  Created by service on 10/5/15.
//  Copyright © 2015 Underplot ltd. All rights reserved.
//
#import "AFHTTPSessionManager.h"
#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@protocol HTTPClientDelegate;

@interface HTTPClient : AFHTTPSessionManager

@property(nonatomic,strong) AFHTTPRequestOperationManager *manager;

@property (nonatomic, weak) id<HTTPClientDelegate>delegate;

// Initialisation Methods
+ (HTTPClient *)sharedHTTPClient ;
- (instancetype)init;
-(void) sendRequest:(NSMutableDictionary *)parameters : (NSString*) url;
-(void) sendSyncRequest :(NSMutableDictionary *)parameters : (NSString*) url;
-(void) sendRequestAsync :(NSMutableDictionary *)parameters : (NSString*) url;
-(void) sendGetRequest : (NSMutableDictionary *)parameters : (NSString*) url;
@end

#pragma mark - Protocol Methods

@protocol HTTPClientDelegate <NSObject>

@optional

// RETDocumentsTableViewController
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject;

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error;
@end

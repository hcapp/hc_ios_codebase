//
//  HttpClient.m
//  JSON_HelloWorld
//
//  Created by service on 10/5/15.
//  Copyright © 2015 Underplot ltd. All rights reserved.
//

#import "HTTPClient.h"



@implementation HTTPClient

@synthesize manager;

+ (HTTPClient *)sharedHTTPClient
{
    static HTTPClient *_sharedHTTPClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedHTTPClient = [[self alloc] init];
    });
    
    return _sharedHTTPClient;
}

- (instancetype)init
{
    self.manager = [AFHTTPRequestOperationManager manager];
    [self.manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [self.manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [self.manager.requestSerializer setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    return self;
}



#pragma mark - AFNetworking Overriding


-(void) sendSyncRequest :(NSMutableDictionary *)parameters : (NSString*) url
{
    
    manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    [self.manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        dispatch_semaphore_signal(semaphore);
        if ([self.delegate respondsToSelector:@selector(HTTPClient:didSucceedWithResponse:)]) {
            [self.delegate HTTPClient:self didSucceedWithResponse:responseObject];
        }
        else{
            NSLog(@"Delegate do not response success service");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if ([self.delegate respondsToSelector:@selector(HTTPClient:didFailWithError:)]) {
            [self.delegate HTTPClient:self didFailWithError:error];
            dispatch_semaphore_signal(semaphore);
        }
        
        NSLog(@"Error: %@", error);
    }];
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
}

// Asynchronous Request
-(void) sendRequest:(NSMutableDictionary *)parameters : (NSString*) url
{
    
    [self.manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        if ([self.delegate respondsToSelector:@selector(HTTPClient:didSucceedWithResponse:)]) {
            [self.delegate HTTPClient:self didSucceedWithResponse:responseObject];
        }
        else{
            NSLog(@"Delegate do not response success service");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          if (error.code != NSURLErrorCancelled) {
        if ([self.delegate respondsToSelector:@selector(HTTPClient:didFailWithError:)]) {
            [self.delegate HTTPClient:self didFailWithError:error];
        }
          }
        NSLog(@"Error: %@", error);
    }];
    
}

-(void) sendGetRequest:(NSMutableDictionary *)parameters : (NSString*) url
{
    
    [self.manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
    
        NSLog(@"JSON: %@", responseObject);
        if ([self.delegate respondsToSelector:@selector(HTTPClient:didSucceedWithResponse:)]) {
            [self.delegate HTTPClient:self didSucceedWithResponse:responseObject];
        }
        else{
            NSLog(@"Delegate do not response success service");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if ([self.delegate respondsToSelector:@selector(HTTPClient:didFailWithError:)]) {
            [self.delegate HTTPClient:self didFailWithError:error];
        }
        NSLog(@"Error: %@", error);
    }];
    
}


-(void) sendRequestAsync:(NSMutableDictionary *)parameters : (NSString*) url
{
    
    [self.manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
    }];
    
}


- (NSString *)currentReachability {
    return [self reachabilityStringForStatus:self.reachabilityManager.networkReachabilityStatus];
}


- (NSString *)reachabilityStringForStatus:(AFNetworkReachabilityStatus)status {
    
    switch (status) {
        case AFNetworkReachabilityStatusNotReachable:       { return @"Not Reachable"; break; }
        case AFNetworkReachabilityStatusReachableViaWWAN:   { return @"ReachableViaWan"; break; }
        case AFNetworkReachabilityStatusReachableViaWiFi:   { return @"ReachableViaWifi"; break; }
        default:
            return @"UnknownResource";
            break;
    }
    
}
@end

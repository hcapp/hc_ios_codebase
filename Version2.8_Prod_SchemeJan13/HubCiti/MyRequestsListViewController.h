//
//  MyRequestsListViewController.h
//  HubCiti
//
//  Created by Bindu M on 9/29/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "CommonUtility.h"
#import "MakeRequestDetailViewController.h"
#import "SortAndFilter.h"
@class AnyViewController;
@class EmailShareViewController;
@interface MyRequestsListViewController : UIViewController<MFMessageComposeViewControllerDelegate, UITableViewDataSource, UITableViewDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>{
    NSMutableArray *arrBottomBtnDO;
    NSMutableArray *arrBottomButtonViewContainer;
    WebRequestState iWebRequestState;
    CommonUtility *common;
    BOOL isRemember;
    UITableView *myReqListTbl;
    NSString *btnText;
    NSMutableDictionary *reqDetailPasstoNext;
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic,strong) NSMutableArray *arrBottomBtnDO;
@property (nonatomic, strong) NSMutableArray *ReqListArr;
@property (nonatomic, strong) MakeRequestDetailViewController* MakeReqDetailObj;
@property(nonatomic,strong) SortAndFilter *sortObj;


@end

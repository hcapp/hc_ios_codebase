//
//  RequestDescriptionViewController.h
//  HubCiti
//
//  Created by Ashika on 9/29/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "CommonUtility.h"
#import <MessageUI/MFMailComposeViewController.h>
@class AnyViewController;
@class EmailShareViewController;

@interface RequestDescriptionViewController : UIViewController<UIWebViewDelegate,MFMessageComposeViewControllerDelegate,UITableViewDataSource, UITableViewDelegate,MFMailComposeViewControllerDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>{
    WebRequestState iWebRequestState;
    CommonUtility *common;
    BOOL isRemember;
    UITableView *reportProblemTbl;
    NSMutableDictionary *requestDetailDictionary;
    NSString *navTitle;
    NSMutableArray *arrBottomBtnDO;
    NSMutableArray *arrBottomButtonViewContainer;
    UIWebView *requestDescription,*question;
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic,retain) NSMutableArray *arrBottomBtnDO;
@property (nonatomic,retain) NSMutableDictionary *requestDetailDictionary;
@property (nonatomic,retain) NSString *navTitle;
@end

//
//  NewCouponDetailViewController.h
//  HubCiti
//
//  Created by Nikitha on 11/22/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomizedNavController.h"
#import "EmailShareViewController.h"
typedef enum WebServicesForCoupon
{
    REQUESTFORCOUPONDETAILS,
    CLIAMCOUPON,
    REDEEMCOUPON,
    SHARECOUPON,
    RETSUMMARYDATA,
    LOCATIONDETAILS
}WebServicesCouponState;

@interface NewCouponDetailViewController : UIViewController
{
    CustomizedNavController * cusNav;
    NSNumber *couponId;
    WebServicesCouponState iWebRequestState;
    BOOL canClaim;
    BOOL canRedeem;
    BOOL allDone;
}
@property (strong, nonatomic) IBOutlet UIButton *locationButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *termsAndConditionsButtonOutlet;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightFortermsDetailLabelConstraint;
@property (strong, nonatomic) IBOutlet UIView *termsConditionViewOutlet;
@property (strong, nonatomic) IBOutlet UILabel *termsDetailLabelOutlet;
@property (strong, nonatomic) IBOutlet UIButton *redeemButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *claimButtonOutlet;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *expiryLabelTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *termsLabelTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *termsLabelheightContraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *expirylabelHeightConstraint;
@property (strong, nonatomic) IBOutlet UIWebView *descriptionWebViewOutlet;

@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic) BOOL fromspecialCoupons;

- (IBAction)cliamButtonClicked:(id)sender;
- (IBAction)locationButtonClicked:(id)sender;
- (IBAction)termsAndCondButtonClicked:(id)sender;
- (IBAction)redeemButtonClicked:(id)sender;
- (IBAction)closeButtonPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *bannerNameOutlet;

@property (strong, nonatomic) IBOutlet UILabel *startDateOutlet;
@property (strong, nonatomic) IBOutlet UILabel *expireDateOutlet;
@property (strong, nonatomic) IBOutlet UIView *coverViewOutlet;
@property (strong, nonatomic) IBOutlet SdImageView *sdImageViewOutlet;
@property (strong, nonatomic) IBOutlet UITableView *productTableViewOutlet;
@property (strong, nonatomic) IBOutlet UIView *productAlertViewoutlet;

@property (nonatomic) BOOL isFromMapScreen;
@property (nonatomic,strong) NSString * setCouponId;
@property (nonatomic,strong) NSString * setCouponListId;



@end

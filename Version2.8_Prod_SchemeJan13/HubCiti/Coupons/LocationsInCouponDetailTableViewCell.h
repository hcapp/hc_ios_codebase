//
//  LocationsInCouponDetailTableViewCell.h
//  HubCiti
//
//  Created by Nikitha on 12/26/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationsInCouponDetailTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabelText;
@property (strong, nonatomic) IBOutlet UILabel *detailLabelText;
-(void) setDataForImageAndLabel : (NSString*) imagePath titleLabel:(NSString *) titleText detailLabel:(NSString*)detailText;
@property (strong, nonatomic) IBOutlet SdImageView *sdImageViewOutlet;
@end

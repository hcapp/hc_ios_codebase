//
//  CouponSpecialMapViewController.m
//  HubCiti
//
//  Created by Ashika on 3/9/17.
//  Copyright © 2017 Keshavakarthik S. All rights reserved.
//

#import "CouponSpecialMapViewController.h"
#import "RetailerAnnotation.h"
#import "CouponMapDetails.h"
#import "CouponMapResponse.h"
#import "AppDelegate.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "NewCouponDetailViewController.h"
#import "CouponsRetailerDetails.h"
#import "RetailerData.h"
#import "RetailerSummaryViewController.h"

@interface CouponSpecialMapViewController ()<MKMapViewDelegate,CustomizedNavControllerDelegate>
{
    NSMutableArray *mapDetailsArray,*annArray;
    WebRequestState iWebRequestState;
}


@end

@implementation CouponSpecialMapViewController
@synthesize globalRetailerService;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton = YES;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = mainPage;
    cusNav =(CustomizedNavController *) self.navigationController;
    [cusNav setTitle:@"Choose your location:" forView:self withHambergur:YES];
    [self showMap];
    // Do any additional setup after loading the view from its nib.
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)returnToMainPage:(id)sender
{
    
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(void)showMap
{
    mapView =[[MKMapView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    mapView.hidden = NO;
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    [mapView setDelegate:self];
    
    
    
    //--------------------->annotation
    for (int i=0; i<globalRetailerService.count; i++) {
        
        
        RetailerData *retailerData = [[RetailerData alloc] init];
        NSDictionary *dictList = globalRetailerService[i];
        [retailerData setRetailerDetailWithDictionary:dictList ];
        
         RetailerAnnotation *ann = [[RetailerAnnotation alloc] init];
        ann.coordinate = CLLocationCoordinate2DMake([[retailerData latitude] doubleValue],
                                                    [[retailerData longitude] doubleValue]);
        [mapView addAnnotation:ann];
        [annArray addObject:ann];
        [ann setTitle:[retailerData retailerName]];
        [ann setSubtitle:[retailerData completeAddress]];
        [ann setIdString:[NSString stringWithFormat:@"%@",[retailerData retListId]]];
        
    }
    
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in mapView.annotations){
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
        
    }
    UIEdgeInsets insets = UIEdgeInsetsMake(100, 50, 80, 50);
    
    
    MKMapRect biggerRect = [mapView mapRectThatFits:zoomRect edgePadding:insets];
    
    
    [mapView setVisibleMapRect:biggerRect animated:YES];
    
    [self.view addSubview:mapView];

}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    
    MKPinAnnotationView* pinView = nil;
    
    if([annotation isKindOfClass:[RetailerAnnotation class]]){
        static NSString* RetailerAnnotationIdentifier = @"retailerAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:RetailerAnnotationIdentifier];
        if (!pinView)
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
                                                  initWithAnnotation:annotation reuseIdentifier:RetailerAnnotationIdentifier] ;
            customPinView.animatesDrop = YES;
            
            customPinView.canShowCallout = YES;
            
            UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            
            customPinView.rightCalloutAccessoryView = infoButton;
            
            return customPinView;

            
        }
        else{
            pinView.annotation = annotation;

        }
    }
    pinView.canShowCallout = YES;
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    for (int i=0; i<globalRetailerService.count; i++) {
        
        
        RetailerData *retailerData = [[RetailerData alloc] init];
        NSDictionary *dictList = globalRetailerService[i];
        [retailerData setRetailerDetailWithDictionary:dictList ];
        
        
        RetailerAnnotation *couponAnnotation = (RetailerAnnotation *)view.annotation;
        
        if ([[retailerData retListId] isEqualToString: [couponAnnotation idString]])
        {
            [self request_retsummary:retailerData];
            return;
        }
        
    }
    
}


-(void)responseData:(NSString *) response
{
    
    [self parse_RetSummary:response];
    
}
-(void)request_retsummary:(RetailerData *) retData
{
    
    //iWebRequestState = SSRETSUMMARY;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    
        [requestStr appendFormat:@"<retailerId>%@</retailerId>",[retData retailerId]];
        
        [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[retData retailLocationId]];
        [requestStr appendFormat:@"<retListId>%@</retListId>",[retData retListId]];
   
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [requestStr appendFormat:@"<scanTypeId>%@</scanTypeId>",@"0"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}
-(void)parse_RetSummary:(NSString*)response
{
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else
    {
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
//        RetailersInDetail *distanceDic;
//        distanceDic = [findLocationServiceObjArray objectAtIndex:selectedindexRow];
//        rsvc.distanceFromPreviousScreen = distanceDic.distance;
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
       
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

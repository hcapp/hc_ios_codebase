//
//  QualifyingProductsList.m
//  Scansee
//
//  Created by Stephanie Blackwell on 5/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainMenuViewController.h"
#import "AboutAndPrivacyScreen.h"
//#import "RateShareViewController.h"
#import "WebBrowserViewController.h"
#import "QualifyingProductsList.h"
#import "ProductPage.h"
#import "RetailerSummaryViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@interface QualifyingProductsList ()

@end

@implementation QualifyingProductsList

@synthesize displayProductandLoc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)updateImageButtons
{
    if (numSelected) {
        [wishBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_wishlist_long.png"] forState:UIControlStateNormal];
        [wishBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_wishlist_long.png"] forState:UIControlStateHighlighted];
        
        [couponGalBtn setBackgroundImage:[UIImage imageNamed:@"tab_up_down_add_spngLst-02_long.png"] forState:UIControlStateNormal];
        [couponGalBtn setBackgroundImage:[UIImage imageNamed:@"tab_up_down_add_spngLst_long.png"] forState:UIControlStateHighlighted];
    }
    else {
        [wishBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_wishlist_long_g.png"] forState:UIControlStateNormal];
        [wishBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_wishlist_long_g.png"] forState:UIControlStateHighlighted];
        
        [couponGalBtn setBackgroundImage:[UIImage imageNamed:@"tab_up_down_add_spngLst_long_g.png"] forState:UIControlStateNormal];
        [couponGalBtn setBackgroundImage:[UIImage imageNamed:@"tab_up_down_add_spngLst_long_g.png"] forState:UIControlStateHighlighted];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    //float titleLabelFont = 16.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
      //  titleLabelFont = 21.0;
    }
    numSelected = 0;
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"Select Products To Add" forView:self withHambergur:NO];
    
    //ReleaseAndNilify(titleLabel);
    
    productNameArray = [[NSMutableArray alloc] init];
    productIdArray = [[NSMutableArray alloc] init];
    retailLocationIdArray = [[NSMutableArray alloc] init];
    prodImagePath = [[NSMutableArray alloc] init];
    prodDesc = [[NSMutableArray alloc] init];
    productChecked = [[NSMutableArray alloc] init];
    
    [wishBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_wishlist_long_g.png"] forState:UIControlStateNormal];
    [wishBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_wishlist_long_g.png"] forState:UIControlStateHighlighted];
    [wishBtn addTarget:self action:@selector(wishListClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [couponGalBtn setBackgroundImage:[UIImage imageNamed:@"tab_up_down_add_spngLst_long_g.png"] forState:UIControlStateNormal];
    [couponGalBtn setBackgroundImage:[UIImage imageNamed:@"tab_up_down_add_spngLst_long_g.png"] forState:UIControlStateHighlighted];
    [couponGalBtn addTarget:self action:@selector(addToList:) forControlEvents:UIControlEventTouchUpInside];
    
    [wishBtn setHidden:YES];
    [couponGalBtn setHidden:YES];
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    ////[mainPage release];
    
    
    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    if(displayProductandLoc == 2)
    {
        [self addsegementcontrolleronView];
        productsTable.frame = CGRectMake(productsTable.frame.origin.x,50-7,productsTable.frame.size.width,productsTable.frame.size.height-50);
        [self getProducts];
    }
    else if(displayProductandLoc == 1)
        [self getLocations];
    else
        [self getProducts];
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}
// If users location data is available then only will display segement otherwise only products will display on the screen
-(void)addsegementcontrolleronView
{
    if(segmentedControl)
    {
        [segmentedControl removeFromSuperview];
    }
    // Set the two segement one for Location and another for Product
    NSArray *items = [NSArray arrayWithObjects:@"Products",@"Locations", nil];
    segmentedControl = [[HubCitiSegmentedControl alloc] initWithItems:items];
    [segmentedControl setFrame:CGRectMake(-7, 0, SCREEN_WIDTH + 14, 44)];//width 334
    [segmentedControl setSelectedSegmentIndex:0];
    [segmentedControl setSegmentColor];
    [segmentedControl addTarget:self action:@selector(segmentChanged) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setBackgroundColor:[UIColor blackColor]];
    
    [self.view addSubview:segmentedControl];
    
}

- (void)segmentChanged
{
    [prodDesc removeAllObjects];
    [productIdArray removeAllObjects];
    [productNameArray removeAllObjects];
    [prodImagePath removeAllObjects];
    [productChecked removeAllObjects];
    
    switch(segmentedControl.selectedSegmentIndex)
    {
        case 0:
        {
            [self getProducts];
            break;
        }
        case 1:
        {
            [self getLocations];
            break;
        }
            
    }
}

-(void)getProducts
{
    iWebRequestState = GETPRODUCT;
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"Select Products To Add" forView:self withHambergur:NO];
//    UILabel *titleLabel = (UILabel*)self.navigationItem.titleView;
//    titleLabel.text = NSLocalizedString(@"Select Products To Add",@"Select Products To Add");
    
    currentDisplayingTab = 0;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendString:@"<CLRDetails>"];
    [requestStr appendFormat:@"<userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"%@",[defaults objectForKey:@"CouponRequest"] ];
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
    
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_HUBCITIID]];
    
    
    //@Deepak: Added couponListID or loyaltyListID for UserTracking
    if([defaults valueForKey:@"couponListID"])
        [requestStr appendFormat:@"<couponListId>%@</couponListId>",[defaults valueForKey:@"couponListID"]];
    
    [requestStr appendString:@"<type>product</type>"];
    
    [requestStr appendString:@"</CLRDetails>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@gallery/getcouponproductorlocation",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    
    //[requestStr release];
    
}


-(void)getLocations
{
    iWebRequestState = GETLOCATION;
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"Locations" forView:self withHambergur:NO];
//    UILabel *titleLabel = (UILabel*)self.navigationItem.titleView;
//    titleLabel.text = NSLocalizedString(@"Locations",@"Locations");
    currentDisplayingTab = 1;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendString:@"<CLRDetails>"];
    [requestStr appendFormat:@"<userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"%@",[defaults objectForKey:@"CouponRequest"] ];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
    
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_HUBCITIID]];
    
    //@Deepak: Added couponListID or loyaltyListID for UserTracking
    if([defaults valueForKey:@"couponListID"])
        [requestStr appendFormat:@"<couponListId>%@</couponListId>",[defaults valueForKey:@"couponListID"]];
    
    [requestStr appendString:@"<type>location</type>"];
    
    [requestStr appendString:@"</CLRDetails>"];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@gallery/getcouponproductorlocation",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    
    //[requestStr release];
    
}

-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case GETLOCATION:
            [self parseLocationDetails:response];
            break;
        case GETPRODUCT:
            [self parseProductsDetails:response];
            break;
            
        case RETSUMMARYS:
            [self parse_retSummary:response];
            break;
            
        default:
            break;
    }
    
    
    [productsTable reloadData];
}


-(void)parseProductsDetails:(NSString*)responseXml
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *ProductDetailElement = [TBXML childElementNamed:@"ProductDetail" parentElement:tbxml.rootXMLElement];
        if (ProductDetailElement!=nil)
        {
            while (ProductDetailElement!=nil)
            {
                
                TBXMLElement *productIdElement = [TBXML childElementNamed:@"productId" parentElement:ProductDetailElement];
                TBXMLElement *productNameElement = [TBXML childElementNamed:@"productName" parentElement:ProductDetailElement];
                
                TBXMLElement *imagePathElement = [TBXML childElementNamed:@"productImagePath" parentElement:ProductDetailElement];
                
                TBXMLElement *productDescriptionElement = [TBXML childElementNamed:@"productDescription" parentElement:ProductDetailElement];
                
                [productIdArray addObject:[TBXML textForElement:productIdElement]];
                [productNameArray addObject:[TBXML textForElement:productNameElement]];
                [prodImagePath addObject:[TBXML textForElement:imagePathElement]];
                [prodDesc addObject:[TBXML textForElement:productDescriptionElement]];
                [productChecked addObject:@"No"];
                
                ProductDetailElement = [TBXML nextSiblingNamed:@"ProductDetail" searchFromElement:ProductDetailElement];
            }
        }
    }
    else
    {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        if (responseTextElement!=nil)
        {
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        }
    }
    
}


-(void)parseLocationDetails:(NSString*)responseXml
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        [wishBtn setHidden:YES];
        [couponGalBtn setHidden:YES];
        
        TBXMLElement *ProductDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:tbxml.rootXMLElement];
        if (ProductDetailElement!=nil){
            while (ProductDetailElement!=nil) {
                
                
                TBXMLElement *productIdElement = [TBXML childElementNamed:@"retailerId" parentElement:ProductDetailElement];
                TBXMLElement *productNameElement = [TBXML childElementNamed:@"retailerName" parentElement:ProductDetailElement];
                TBXMLElement *retailLocationIdElement = [TBXML childElementNamed:@"retailLocationId" parentElement:ProductDetailElement];
                
                TBXMLElement *productDescriptionElement = [TBXML childElementNamed:@"completeAddress" parentElement:ProductDetailElement];
                
                TBXMLElement *imagePathElement = [TBXML childElementNamed:@"logoImagePath" parentElement:ProductDetailElement];
                
                [prodImagePath addObject:[TBXML textForElement:imagePathElement]];
                [retailLocationIdArray addObject:[TBXML textForElement:retailLocationIdElement]];
                [productIdArray addObject:[TBXML textForElement:productIdElement]];
                [productNameArray addObject:[TBXML textForElement:productNameElement]];
                [prodDesc addObject:[TBXML textForElement:productDescriptionElement]];
                [productChecked addObject:@"No"];
                
                ProductDetailElement = [TBXML nextSiblingNamed:@"RetailerDetail" searchFromElement:ProductDetailElement];
            }
        }
    }
    else
    {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        if (responseTextElement!=nil)
        {
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        }
        
    }
    
}



-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        return 70.0;
    }
    return 60.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell ; // = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    float tx_labelFont = 13.0, tx_label_DetailFont = 12.0;
    float tx_label_DetailHeight = 25;
    float tx_label_Detailheight_DisplayTab = 15.0, asyncImageViewWidth = 50.0, asyncImageViewHeight = 50.0;
    if (DEVICE_TYPE ==  UIUserInterfaceIdiomPad) {
        tx_labelFont = 18.0;
        tx_label_DetailFont = 17.0;
        tx_label_DetailHeight = 40.0;
        tx_label_Detailheight_DisplayTab = 25.0;
        asyncImageViewHeight = 60.0;
        asyncImageViewWidth = 60.0;
    }
    
    NSString *imagePathStr;
    // If displaying Product Tab
    if(currentDisplayingTab == 0)
    {
        
        UILabel *tx_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH - 20, 50)];//width 300
        tx_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:tx_labelFont];
        
        UILabel *tx_label_Detail = [[UILabel alloc]initWithFrame:CGRectMake(10, 42, SCREEN_WIDTH - 20, tx_label_DetailHeight)];//width 300
        tx_label_Detail.backgroundColor = [UIColor clearColor];
        tx_label_Detail.textColor = [UIColor grayColor];
        tx_label_Detail.font = [UIFont fontWithName:@"Helvetica-Bold" size:tx_label_DetailFont];
        tx_label_Detail.numberOfLines =2;
        tx_label_Detail.lineBreakMode = NSLineBreakByWordWrapping;
        
        tx_label.numberOfLines = 2;
        tx_label.frame = CGRectMake(10, 0, SCREEN_WIDTH - 120, 50);//width 200
        tx_label_Detail.frame = CGRectMake(10, 42, SCREEN_WIDTH - 120, tx_label_Detailheight_DisplayTab);//width 200
        
        
        //tx_label.lineBreakMode = YES;
        tx_label.lineBreakMode = NSLineBreakByWordWrapping;
        tx_label.text = [productNameArray objectAtIndex:indexPath.row];
        
        // Configure the cell...
        if (![[prodDesc objectAtIndex:indexPath.row]isEqualToString:@"N/A"])
            tx_label_Detail.text = [prodDesc objectAtIndex:indexPath.row];
        
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        
        imagePathStr = [prodImagePath objectAtIndex:indexPath.row];
        
        
        if (![imagePathStr isEqualToString:@"N/A"])
        {
            SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 102, 5, asyncImageViewWidth, asyncImageViewHeight)];//X value 218
            asyncImageView.backgroundColor = [UIColor clearColor];
            //[asyncImageView loadImageFromURL:url];
            [asyncImageView loadImage:imagePathStr];
            asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
            [cell addSubview:asyncImageView];
            //[asyncImageView release];
        }
        [cell addSubview:tx_label];
        [cell addSubview:tx_label_Detail];
        //[tx_label release];
        //[tx_label_Detail release];
    }
    else
    {
        imagePathStr = [prodImagePath objectAtIndex:indexPath.row];
        if (![imagePathStr isEqualToString:@"N/A"])
        {
            SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(5, 5, asyncImageViewWidth, asyncImageViewHeight)];//X value 218
            asyncImageView.backgroundColor = [UIColor clearColor];
            //[asyncImageView loadImageFromURL:url];
            [asyncImageView loadImage:imagePathStr];
            asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
            [cell addSubview:asyncImageView];
            //[asyncImageView release];
        }
        
        UILabel *tx_label = [[UILabel alloc] initWithFrame:CGRectMake(10+asyncImageViewWidth, 5,SCREEN_WIDTH - 20-asyncImageViewWidth, 50)];//width 300
        tx_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:tx_labelFont];
        
        tx_label.numberOfLines = 2;
        
        tx_label.lineBreakMode = NSLineBreakByWordWrapping;
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        tx_label.lineBreakMode = NSLineBreakByWordWrapping;
        tx_label.text = [prodDesc objectAtIndex:indexPath.row];
        [cell addSubview:tx_label];
        //[tx_label release];
    }
    
    
    return cell;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [productNameArray count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(currentDisplayingTab == 0)
    {
        [defaults setObject:@"TSL" forKey:@"couponFlag"];
        NSString *productId;
        
        productId = [productIdArray objectAtIndex:indexPath.row];
        
        [defaults  setObject:productId forKey:KEY_PRODUCTID];
        [defaults  setObject:nil forKey:KEY_PRODLISTID];
        
        if([Network currentReachabilityStatus]==0)
        {
            [UtilityManager showAlert];
        }else {
            if ([productId isEqualToString:@"0"]) {
                UIAlertController * alert;
                alert=[UIAlertController alertControllerWithTitle:@" " message:NSLocalizedString(@"Product details not available!",@"Product details not available!") preferredStyle:UIAlertControllerStyleAlert];
                [self presentViewController:alert animated:YES completion:nil];

                [UtilityManager performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:2];
            }
            else{
                
                [defaults setValue:@"0" forKey:KEY_SCANTYPEID];
                [[HubCitiManager sharedManager]setShareFromTL:NO];
                
                ProductPage *prodPage = [[ProductPage alloc]initWithNibName:@"ProductPage" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:prodPage animated:NO];
                //[prodPage release];
            }
        }
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    }
    else{
        [self pullAppSite:(int)indexPath.row];
    }
    
}


-(void) pullAppSite:(int)index
{
    
    iWebRequestState = RETSUMMARYS;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[retailLocationIdArray objectAtIndex:index]];
    
    
    //    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && latitude){
    //
    //        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",longitude,latitude]
    //        ;
    //
    //    }
    //    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
    //
    //        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    //    }
    
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",[productIdArray objectAtIndex:index]];
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    NSString *urlString =  [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);
}

-(void)parse_retSummary:(NSString *)response
{
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else
    {
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        //        rsvc.distanceFromPreviousScreen = [arrDistance objectAtIndex:objIndex];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
    
}




-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
    
}
-(IBAction)wishListClicked:(id)sender{
    if (numSelected) {
        
        
        NSMutableString *requestStr = [[NSMutableString alloc] init];
        
        [requestStr appendString:@"<ProductDetailsRequest>"];
        [requestStr appendFormat:@"<userId>%@</userId><productDetails>", [defaults  valueForKey:KEY_USERID]];
        for (int i =0 ; i<[productIdArray count]; i++) {
            if([[productChecked objectAtIndex:i] isEqualToString:@"Yes"])
            {
                [requestStr appendFormat:@"<ProductDetail><productId>%@</productId>", [productIdArray objectAtIndex:i]];
                [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName></ProductDetail>", [productNameArray objectAtIndex:i]];
            }
        }
        [requestStr appendString:@"</productDetails>"];
        
        if([defaults valueForKey:KEY_MAINMENUID])
            [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID>", [defaults  valueForKey:KEY_MAINMENUID]];
        [requestStr appendString:@"</ProductDetailsRequest>"];
        
        
        NSMutableString *urlString = [BASE_URL mutableCopy];
        [urlString appendFormat:@"wishlist/addWishListProd"];
        
        NSString *responseXml1 = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
        //[requestStr release];
        
        if ([UtilityManager isNullOrEmptyString:responseXml1]) {
            ReleaseAndNilify(responseXml1)
            return;
        }
        TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml1];
        
        TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        
        if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
            [SharedManager setAddToWL:YES];
        }
        
        //   [[ScanseeManager sharedManager] addToWL]
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"No Product Selected",@"No Product Selected") msg:NSLocalizedString(@"Please select one or more items to add them to your Wish List",@"Please select one or more items to add them to your Wish List")];
          }
}

-(IBAction)addToList:(id)sender{
    
    if (numSelected) {
        
        NSMutableString *requestStr = [[NSMutableString alloc] init];
        [requestStr appendString:@"<AddShoppingList>"];
        [requestStr appendFormat:@"<userId>%@</userId><productDetails>", [defaults  valueForKey:KEY_USERID]];
        
        for (int i =0 ; i<[productIdArray count]; i++) {
            if([[productChecked objectAtIndex:i] isEqualToString:@"Yes"])
            {
                [requestStr appendFormat:@"<ProductDetail><productId>%@</productId>", [productIdArray objectAtIndex:i]];
                [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName></ProductDetail>", [productNameArray objectAtIndex:i]];
            }
        }
        //@Deepak: Added MainMenuID in the request- userTracking for shoppinglist
        [requestStr appendFormat:@"</productDetails><mainMenuID>%@</mainMenuID>", [defaults  valueForKey:KEY_MAINMENUID]];
        [requestStr appendString:@"</AddShoppingList>"];
        
        NSMutableString *urlString = [BASE_URL mutableCopy];
        [urlString appendFormat:@"shoppingList/addtslbysearch"];
        
        DLog(@"Request:%@", requestStr);
        NSString *responseXml1 = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
        //[requestStr release];
        
        if ([UtilityManager isNullOrEmptyString:responseXml1]){
            ReleaseAndNilify(responseXml1);
            return;
        }
        
        //	[defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
        
        TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml1];
        //TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        //if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
        
        //}
        //	else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
         }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"No Product Selected",@"No Product Selected") msg:NSLocalizedString(@"Please select one or more items to add them to your Shopping List",@"Please select one or more items to add them to your Shopping List")];
          }
    
}
-(void)checkItem:(UIButton *)sender
{
    if ([[productChecked objectAtIndex:sender.tag] isEqualToString:@"Yes"]) {
        [productChecked replaceObjectAtIndex:sender.tag withObject:@"No"];
        DLog(@"Changed to No");
        numSelected--;
        DLog(@"Number - %d", numSelected);
        [self updateImageButtons];
        [productsTable reloadData];
        return;
    }
    
    if ([[productChecked objectAtIndex:sender.tag] isEqualToString:@"No"]) {
        [productChecked replaceObjectAtIndex:sender.tag withObject:@"Yes"];
        DLog(@"Changed to Yes");
        numSelected++;
        DLog(@"Number - %d", numSelected);
        [self updateImageButtons];
        [productsTable reloadData];
        return;
    }
    
}

@end

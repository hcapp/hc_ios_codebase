//
//  ProductCouponsList.h
//  Scansee
//
//  Created by ajit on 9/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"

@interface ProductCouponsList : UIViewController<UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource,CustomizedNavControllerDelegate> {

	IBOutlet UIView *modelView;
	IBOutlet UITableView *couponTableView;
    IBOutlet UIButton *listBtn;
    IBOutlet UIButton *wishBtn;
    IBOutlet UIButton *couponGalBtn;
    IBOutlet UIButton *prefBtn;
	
	NSMutableArray *myCLRHdrNameArray ;
	NSMutableArray *couponIdArray, *couponNameArray ,*couponExpireDateArray, *couponDiscountAmountArray,*couponUrlArray;
	NSMutableArray *couponImagePath ,*loyaltyImagePath ,*rebateImagePath ;
	NSMutableArray *loyaltyIdArray, *loyaltyNameArray, *loyaltyDiscountAmountArray , *loyaltyExpireDateArray;
	NSMutableArray *rebateIdArray, *rebateNameArray;
	NSMutableArray *rebateDiscountAmountArray , *rebateExpireDateArray;
	NSMutableArray *couponRowCount ,*loyaltyRowCount ,*rebateRowCount;
	NSMutableArray *couponAdded , *loyaltyAdded , *rebateAdded;
    NSMutableArray *viewableOnWebsiteArray,*couponListIDElementArray;
	BOOL couponsPresent, loyaltyPresent, rebatePresent;
	
	int nextPage;
	int lowerLimit;
	int rowNum ,rowNumSize;
	
	int clrC , clrL , clrR ;
}

-(IBAction)wishListClicked:(id)sender;
-(IBAction)couponListClicked:(id)sender;
-(IBAction)preferencesClicked:(id)sender;
-(IBAction) listClicked : (id) sender ;

-(void)returnToMainPage:(id)sender;
-(void)showSplash;
-(void)hideSplash;
-(void)parseCLRXml;
-(void)parseRebateXML;
-(void)parseLoyaltyXML;
-(void)parseCouponXML;


-(void) refreshDataStructure ;
-(void)fetchPreviousDeals:(id)sender;
-(void)fetchNextDeals:(id)sender;
-(void) addRemoveFromGallery : (UIButton *) sender;
-(void) resetStaticVariable;
-(void) removeAllObjectsInArray ;

@end



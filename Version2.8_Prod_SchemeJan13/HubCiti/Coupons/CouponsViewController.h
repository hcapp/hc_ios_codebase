//
//  CouponsViewController.h
//  Scansee
//
//  Created by ajit on 9/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"
#import "HubCitiSegmentedControl.h"
//#import "MyCouponPopulationCenterCities.h"

typedef enum CouponViewServices
{
    GETALLCOUPBYPROD,
    GETALLCOUPBYLOC,
    GETUSERCATCOUPON
    //addRemoveCoupon
}couponviewservices;

@interface CouponsViewController : UIViewController<UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate,UITextFieldDelegate,CustomizedNavControllerDelegate>
{
    
    couponviewservices iWebrequest;
    UIAlertAction* zipSave;
	IBOutlet UIView *modelView;
	IBOutlet UITableView *myGalleryTV;
	IBOutlet UIButton *couponGalleryBtn , *bycityBtn ,*searchBtn ,*preferanceBtn ;
    UIButton *addRemGalley;
  	int lowerLimitvalue;
    
    HubCitiSegmentedControl *segmentedControl;
   // MyCouponPopulationCenterCities *objMyCouponPopulationCenterCities;
    
    //search related variables
    UIView *searchView;    
    IBOutlet UIButton *radioBtn1;
    IBOutlet UIButton *radioBtn2;
    IBOutlet UIButton *catBtn;
    IBOutlet UILabel *catLabel;
    
    IBOutlet UIButton *retBtn;
    IBOutlet UILabel *retLabel;
    
    IBOutlet UITextField *searchTextField;
    IBOutlet UIPickerView *catPicker;
    IBOutlet UIToolbar *toolBar;
    int nextpageFlag;
    IBOutlet UILabel *visitAllCoupons;//to be hidden in case of Expired and All
    
    NSMutableArray *arrBusinessCatagoriesID;
    NSMutableArray *arrBusinessCatagoriesName;
    
    NSMutableArray *arrBusinessCatagoriesRetailerID;
    NSMutableArray *arrBusinessCatagoriesRetailerName;
    
    BOOL isLocationRadioButtonEnable;

    //Data set for storing the Product information
    NSMutableArray *arrCatagories;  
    NSMutableDictionary *dicProductsCoupon;
    
    //Data set for storing the Location information
    NSMutableDictionary *dicLocationCupon;
    NSMutableDictionary *dicLocationCategories;
    NSMutableArray *arrLocationRetailers;
    NSMutableArray *arrLocationCategoriesforRetailers;
    NSMutableArray *arrSectionArrayForRetailers;
    
    BOOL isCalledFromSegementChange;
    
    int selectedbusinesscatID;
    int selectedbusinesscatRetailerID;
    
    int indexpathRowCouponList;
    int indexpathSectionCouponList;

    BOOL isProductTabDisplaying; // If this is true that means currently active tab or displaying data is "Products"
    BOOL isDisplayBusinessCatagory; // In serach to identify if BusinessCategory is selected then only will dipsplay the retailers
    
    NSMutableDictionary *dictCoupon;
}

@property (nonatomic,strong) IBOutlet UIView *searchView;


-(void)returnToMainPage:(id)sender;
-(void)showSplash;
-(void)hideSplash;
-(void)segmentChanged;
-(IBAction)searchClicked:(id)sender;
-(IBAction)selectCategory:(id)sender;
-(IBAction)selectRetailer:(id)sender;
-(IBAction)toolbarDoneClicked:(id)sender;
-(IBAction)closeSearchView :(id)sender;
-(IBAction)radioButtonTapped:(id)sender;

@end



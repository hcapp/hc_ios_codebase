//
//  CouponGalleryDetail.h
//  Scansee
//
//  Created by developer_span on 15/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "DWBubbleMenuButton.h"
//#import <Twitter/Twitter.h>
//#import "WishListHomePage.h"
//#import "RateShareViewController.h"
//#import "UserSettings.h"
//#import "ProductCouponsList.h"
@class AnyViewController;
@class EmailShareViewController;
typedef enum webServicesCoupon
{
    GETCOUPONDETAILS,
    addCoupon,
    userredeemcoupon,
    COUPON_SHARE
}webServicesCouponState;

@interface CouponDetailViewController : UIViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,
UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate, DWBubbleMenuViewDelegate,CustomizedNavControllerDelegate> {
    
    webServicesCouponState iWebRequestState;
    
//    AnyViewController *anyVC;
    IBOutlet UITableView *openProductList;
    IBOutlet UIButton *rateBtn;
    IBOutlet UIButton *wishBtn;
    IBOutlet UIButton *couponGalBtn;
    IBOutlet UIButton *prefBtn;
    IBOutlet UIButton *markUsed;
    IBOutlet UIButton *clipCoupon;
    IBOutlet UILabel *titleLabel;
    IBOutlet UILabel *validDate;
    IBOutlet UITextView *descText;
    IBOutlet UIImageView *scissorsImageView;
    IBOutlet UIImageView *couponImageView;
    IBOutlet UIImageView *shadowImageView;
	NSMutableArray *prodNameArray ,*productIdArray;
	NSString *couponId,*couponName,*couponLongDesc,*couponUrl,*termsOfUse,*couponImagePath;
	NSString *couponStartDate,*couponEndDate;
	NSString *headerType;
	NSString *responseXml;
	CGSize strSize;
    BOOL usedFlag;
    int couponUsedVal;
    UIButton *redeemBtn;
    UIImageView *img;
    BOOL redeem, viewableOnWebsite;
    BOOL displayAddToGallery; //to decide whether to display Add To Gallery (or) Mark As Used
    BOOL isQLocationAvailable;
    BOOL isQProductsAvailable;
    
    UIView *termsNCondition;
    
    BOOL expiredFlag;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic, strong)NSString *couponId,*couponName,*couponLongDesc,*couponStartDate,*couponEndDate;
@property(readwrite) BOOL redeem;
@property (readwrite) BOOL viewableOnWebsite;
@property (readwrite) BOOL displayAddToGallery;

-(IBAction)addToGallery:(id)sender;
-(IBAction)redeemNowClicked:(id)sender;

-(void)updateData;

@end

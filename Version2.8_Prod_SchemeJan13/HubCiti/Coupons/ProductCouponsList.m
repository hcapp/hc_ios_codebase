//
// ProductCouponsList.m
//  Scansee
//
//  Created by ajit on 9/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ProductCouponsList.h"
#import "MainMenuViewController.h"
#import "NewCouponDetailViewController.h"
#import "WebBrowserViewController.h"
#import "ProductPage.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"


@implementation ProductCouponsList

//static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Coupon";
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    ////[mainPage release];
    
    
    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    //    [listBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_shpngLst.png"] forState:UIControlStateNormal];
    //    [listBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_shpngLst.png"] forState:UIControlStateHighlighted];
    //    [listBtn addTarget:self action:@selector(listClicked:) forControlEvents:UIControlEventTouchUpInside];
    //
    //
    //    [wishBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_goTowishlist.png"] forState:UIControlStateNormal];
    //    [wishBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_goTowishlist.png"] forState:UIControlStateHighlighted];
    //    [wishBtn addTarget:self action:@selector(wishListClicked:) forControlEvents:UIControlEventTouchUpInside];
    //
    //
    //    [couponGalBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_cpns.png"] forState:UIControlStateNormal];
    //    [couponGalBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_cpns.png"] forState:UIControlStateHighlighted];
    //    [couponGalBtn addTarget:self action:@selector(couponListClicked:) forControlEvents:UIControlEventTouchUpInside];
    //
    //
    //    [prefBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    //    [prefBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_preferences.png"] forState:UIControlStateHighlighted];
    //    [prefBtn addTarget:self action:@selector(preferencesClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    myCLRHdrNameArray = [[NSMutableArray alloc] init];
    
    couponIdArray = [[NSMutableArray alloc] init];
    couponNameArray = [[NSMutableArray alloc] init];
    couponExpireDateArray = [[NSMutableArray alloc] init];
    couponImagePath = [[NSMutableArray alloc] init];
    couponDiscountAmountArray = [[NSMutableArray alloc] init];
    couponUrlArray = [[NSMutableArray alloc] init];
    viewableOnWebsiteArray = [[NSMutableArray alloc] init];
    couponListIDElementArray = [[NSMutableArray alloc]init];
    
    loyaltyIdArray = [[NSMutableArray alloc] init];
    loyaltyNameArray = [[NSMutableArray alloc] init];
    loyaltyDiscountAmountArray = [[NSMutableArray alloc] init];
    loyaltyExpireDateArray = [[NSMutableArray alloc] init];
    loyaltyImagePath = [[NSMutableArray alloc] init];
    
    rebateIdArray = [[NSMutableArray alloc] init];
    rebateNameArray = [[NSMutableArray alloc] init];
    rebateDiscountAmountArray = [[NSMutableArray alloc] init];
    rebateExpireDateArray = [[NSMutableArray alloc] init];
    rebateImagePath = [[NSMutableArray alloc] init];
    
    couponRowCount = [[NSMutableArray alloc] init];
    loyaltyRowCount = [[NSMutableArray alloc] init];
    rebateRowCount = [[NSMutableArray alloc] init];
    
    couponAdded = [[NSMutableArray alloc] init];
    loyaltyAdded = [[NSMutableArray alloc] init];
    rebateAdded = [[NSMutableArray alloc] init];
    
    [self resetStaticVariable];
    [self refreshDataStructure];
}

-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void) resetStaticVariable {
    clrC = 0;
    clrL = 0;
    clrR = 0;
    lowerLimit = 0 ;
    rowNum = 0;
    nextPage = 0;
    rowNumSize = 0;
}
-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}

-(void) refreshDataStructure {
    
    [self removeAllObjectsInArray];
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<CLRDetails><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<productId>%@</productId>",[defaults  valueForKey:KEY_PRODUCTID]];
    if([[HubCitiManager sharedManager]shareFromTL] == YES)
    {
        [requestStr appendFormat:@"<retailerId>%@</retailerId>", [defaults  valueForKey:KEY_RETAILERID]];
    }
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_HUBCITIID]];
    
    [requestStr appendFormat:@"<lowerLimit>%i</lowerLimit></CLRDetails>",lowerLimit];
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/fetchproductclrinfo",BASE_URL];
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml]) {
        return;
    }
    [defaults  setObject:responseXml forKey:@"responseXmlCoupon"] ;
    
    //[self parseCLRXml];
    [self parseCouponXML];//since only coupons for now
    
    [couponTableView reloadData];
}
-(void) removeAllObjectsInArray {
    
    [couponIdArray removeAllObjects];
    [couponNameArray removeAllObjects];
    [couponImagePath removeAllObjects];
    [couponDiscountAmountArray removeAllObjects];
    [couponExpireDateArray removeAllObjects];
    [couponUrlArray removeAllObjects];
    [viewableOnWebsiteArray removeAllObjects];
    [couponListIDElementArray removeAllObjects];
    
    [loyaltyIdArray removeAllObjects];
    [loyaltyNameArray removeAllObjects];
    [loyaltyDiscountAmountArray removeAllObjects];
    [loyaltyExpireDateArray removeAllObjects];
    [loyaltyImagePath removeAllObjects];
    
    [rebateIdArray removeAllObjects];
    [rebateNameArray removeAllObjects];
    [rebateDiscountAmountArray removeAllObjects];
    [rebateExpireDateArray removeAllObjects];
    [rebateImagePath removeAllObjects];
    
    [couponRowCount removeAllObjects];
    [loyaltyRowCount removeAllObjects];
    [rebateRowCount removeAllObjects];
    [couponAdded removeAllObjects];
    [loyaltyAdded removeAllObjects];
    [rebateAdded removeAllObjects];
    
    [myCLRHdrNameArray removeAllObjects];
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
};

-(void)showSplash
{
    UIViewController *modalViewController = [[UIViewController alloc] init];
    modalViewController.view = modelView;
    [self presentViewController:modalViewController animated:NO completion:nil];
    [self performSelector:@selector(hideSplash) withObject:nil afterDelay:0.5];
    ReleaseAndNilify(modalViewController);
}

- (void)hideSplash{
    [[self presentedViewController] dismissViewControllerAnimated:NO completion:nil];
}

- (void)parseCouponXML{
    
    NSString *responseXml = [defaults  objectForKey:@"responseXmlCoupon"];
    
    DLog(@"Respo = %@", responseXml);
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    NSString *couponThereStr;
    TBXMLElement *isCouponthereElement = [TBXML childElementNamed:@"isCouponthere" parentElement:tbxml.rootXMLElement];
    if (isCouponthereElement != nil){
        couponThereStr = [TBXML textForElement:isCouponthereElement];
        if ([couponThereStr isEqualToString:@"true"] == NO) {
            couponsPresent = NO;
            return;
        }
        else {
            couponsPresent = YES;
            [myCLRHdrNameArray addObject:@"Coupons"];
        }
        TBXMLElement *couponPaginationElement = [TBXML childElementNamed:@"clrC" parentElement:tbxml.rootXMLElement];
        clrC = [[TBXML textForElement:couponPaginationElement] intValue];
        TBXMLElement *couponDetailsElement = [TBXML childElementNamed:@"couponDetails" parentElement:tbxml.rootXMLElement];
        if (couponDetailsElement != nil) {
            TBXMLElement *CouponDetailsElement = [TBXML childElementNamed:@"CouponDetails" parentElement:couponDetailsElement];
            
            while (CouponDetailsElement != nil) {
                
                TBXMLElement *viewableOnWebElement = [TBXML childElementNamed:@"viewableOnWeb" parentElement:CouponDetailsElement];
                TBXMLElement *couponIdElement = [TBXML childElementNamed:@"couponId" parentElement:CouponDetailsElement];
                TBXMLElement *couponNameElement = [TBXML childElementNamed:@"couponName" parentElement:CouponDetailsElement];
                TBXMLElement *couponDiscountAmountElement = [TBXML childElementNamed:@"couponDiscountAmount" parentElement:CouponDetailsElement];
                TBXMLElement *couponExpireDateElement = [TBXML childElementNamed:@"couponExpireDate" parentElement:CouponDetailsElement];
                TBXMLElement *couponImagePathElement = [TBXML childElementNamed:@"imagePath" parentElement:CouponDetailsElement];
                TBXMLElement *rowNumElement = [TBXML childElementNamed:@"rowNum" parentElement:CouponDetailsElement];
                TBXMLElement *addedElement = [TBXML childElementNamed:@"usage" parentElement:CouponDetailsElement];
                TBXMLElement *couponUrlElement = [TBXML childElementNamed:@"couponURL" parentElement:CouponDetailsElement];
                TBXMLElement *couponListIdElement = [TBXML childElementNamed:@"couponListId" parentElement:CouponDetailsElement];
                if(couponListIdElement != nil)
                {
                    [couponListIDElementArray addObject:[TBXML textForElement:couponListIdElement]];
                }
                
                if (viewableOnWebElement!=nil) {
                    
                    [viewableOnWebsiteArray addObject:[TBXML textForElement:viewableOnWebElement]];
                }
                if (couponUrlElement != nil) {
                    [couponUrlArray addObject:[TBXML textForElement:couponUrlElement]];
                }
                
                @try
                {
                    if (![TBXML textForElement:couponIdElement] || [TBXML textForElement:couponIdElement] == nil ) {
                        NSException *exception = [[NSException alloc] initWithName:@"TBXML Exception" reason:@"Invalid text for TBXML Element" userInfo:nil];
                        @throw exception;
                    }
                    else {
                        [couponIdArray addObject:[TBXML textForElement:couponIdElement]];
                    }
                    
                }
                @catch (NSException *ex)
                {
                    DLog(@"Invalid couponIdArray for TBXML Element");
                }
                
                @try
                {
                    if (![TBXML textForElement:couponNameElement] || [TBXML textForElement:couponNameElement] == nil ) {
                        NSException *exception = [[NSException alloc] initWithName:@"TBXML Exception" reason:@"Invalid text for TBXML Element" userInfo:nil];
                        @throw exception;
                    }
                    else {
                        [couponNameArray addObject:[TBXML textForElement:couponNameElement]];
                    }
                    
                }
                @catch (NSException *ex)
                {
                    DLog(@"Invalid couponNameArray for TBXML Element");
                }
                
                @try
                {
                    if (![TBXML textForElement:couponDiscountAmountElement] || [TBXML textForElement:couponDiscountAmountElement] == nil ) {
                        NSException *exception = [[NSException alloc] initWithName:@"TBXML Exception" reason:@"Invalid text for TBXML Element" userInfo:nil];
                        @throw exception;
                    }
                    else {
                        [couponDiscountAmountArray addObject:[TBXML textForElement:couponDiscountAmountElement]];
                    }
                    
                }
                @catch (NSException *ex)
                {
                    DLog(@"Invalid couponDiscountAmountArray for TBXML Element");
                }
                
                @try
                {
                    if (![TBXML textForElement:couponExpireDateElement] || [TBXML textForElement:couponExpireDateElement] == nil ) {
                        NSException *exception = [[NSException alloc] initWithName:@"TBXML Exception" reason:@"Invalid text for TBXML Element" userInfo:nil];
                        @throw exception;
                    }
                    else {
                        [couponExpireDateArray addObject:[TBXML textForElement:couponExpireDateElement]];
                    }
                    
                }
                @catch (NSException *ex)
                {
                    DLog(@"Invalid couponExpireDateArray for TBXML Element");
                }
                @try
                {
                    if (![TBXML textForElement:couponImagePathElement] || [TBXML textForElement:couponImagePathElement] == nil ) {
                        NSException *exception = [[NSException alloc] initWithName:@"TBXML Exception" reason:@"Invalid text for TBXML Element" userInfo:nil];
                        @throw exception;
                    }
                    else {
                        [couponImagePath addObject:[TBXML textForElement:couponImagePathElement]];
                    }
                    
                }
                @catch (NSException *ex)
                {
                    DLog(@"Invalid couponImagePath for TBXML Element");
                }
                @try
                {
                    if (![TBXML textForElement:rowNumElement] || [TBXML textForElement:rowNumElement] == nil ) {
                        NSException *exception = [[NSException alloc] initWithName:@"TBXML Exception" reason:@"Invalid text for TBXML Element" userInfo:nil];
                        @throw exception;
                    }
                    else {
                        [couponRowCount addObject:[TBXML textForElement:rowNumElement]];
                    }
                    
                }
                @catch (NSException *ex)
                {
                    DLog(@"Invalid couponRowCount for TBXML Element");
                }
                @try
                {
                    if (![TBXML textForElement:addedElement] || [TBXML textForElement:addedElement] == nil ) {
                        NSException *exception = [[NSException alloc] initWithName:@"TBXML Exception" reason:@"Invalid text for TBXML Element" userInfo:nil];
                        @throw exception;
                    }
                    else {
                        [couponAdded addObject:[TBXML textForElement:addedElement]];
                    }
                    
                }
                @catch (NSException *ex)
                {
                    DLog(@"Invalid couponAdded for TBXML Element");
                }
                
                CouponDetailsElement = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:CouponDetailsElement];
            }
        }
    }
    else{
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
    }
    //[tbxml release];
}

- (void)parseLoyaltyXML{
    
}

- (void)parseRebateXML{}

- (void)parseCLRXml {
    
    [self parseCouponXML];
    [self parseLoyaltyXML];
    [self parseRebateXML];
    
    DLog(@"CLrc = %i , ClrL = %i and ClrR = %i nextPage = %i",clrC ,clrL ,clrR ,nextPage);
}


-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    //if (tableView.tag == 3)
    return YES;
    //return NO;
}

-(void)fetchPreviousDeals:(id)sender{
    if (lowerLimit == 0) {
        return;
    }
    else {
        if (clrC == 1 || ([couponRowCount count]>0 && clrC == 0))
            rowNum =[[couponRowCount objectAtIndex:0] intValue] -rowNumSize -1;
        if (clrL == 1 || ([loyaltyRowCount count]>0 && clrL == 0))
            rowNum = [[loyaltyRowCount objectAtIndex:0] intValue] -rowNumSize -1;
        if (clrR == 1  || ( [rebateRowCount count]>0 && clrR == 0))
            rowNum = [[rebateRowCount objectAtIndex:0] intValue] - rowNumSize -1;
        //	1 ,3 ,5
        lowerLimit = rowNum;
        [self refreshDataStructure];
        
    }
    
}

-(void ) refreshRowNumber {
    
    if (clrC == 1) {
        rowNum =[[couponRowCount objectAtIndex:[couponRowCount count]-1] intValue];
        rowNumSize = (int)[couponRowCount count] ;
    }
    if (clrL == 1){
        rowNum = [[loyaltyRowCount objectAtIndex:[loyaltyRowCount count]-1] intValue];
        rowNumSize = (int)[loyaltyRowCount count];
    }
    if (clrR == 1){
        rowNum = [[rebateRowCount objectAtIndex:[rebateRowCount count]-1] intValue];
        rowNumSize = (int)[rebateRowCount count];
    }
}
-(void)fetchNextDeals:(id)sender{
    
    [self refreshRowNumber];
    if (nextPage == 1) {
        lowerLimit = rowNum;
        [self refreshDataStructure];
        
    }else {
        return;
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [myCLRHdrNameArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([[myCLRHdrNameArray objectAtIndex:section] isEqualToString:@"Coupons"] == YES){
        if ((section == [myCLRHdrNameArray count]-1 ) && (clrC == 1 || clrL == 1 || clrR == 1 || lowerLimit != 0))
            return [couponNameArray count]+1;
        else
            return [couponNameArray count];
    }
    //	return [rebateNameArray count];
    else
        return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    float tx_labelFont = 12.0, detailLabelFont = 12.0;
    float asyncImageWidth = 44.0, asyncImageHeight = 44.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        tx_labelFont = 17.0;
        detailLabelFont = 17.0;
        
        asyncImageWidth = 54.0;
        asyncImageHeight = 54.0;
    }
    UILabel *tx_label = [[UILabel alloc] initWithFrame:CGRectMake(110, 5, SCREEN_WIDTH - 130, 40)];//width 190
    UITableViewCell *cell ;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    tx_label.font = [UIFont boldSystemFontOfSize:tx_labelFont];
    tx_label.numberOfLines = 1;
    
    UILabel *detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 35, SCREEN_WIDTH - 120, 20)];//width 200
    detailLabel.backgroundColor = [UIColor clearColor];
    detailLabel.textColor = [UIColor colorWithRGBInt:0x4f81bd];
    
    detailLabel.numberOfLines = 1;
    detailLabel.font = [UIFont boldSystemFontOfSize:detailLabelFont];
    
    //  UIImageView *starImage  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 13, 34, 34)];
    UIButton *addRemGalley = [UIButton buttonWithType:UIButtonTypeCustom];
    addRemGalley.frame = CGRectMake(5, 13, 34, 34) ;
    [addRemGalley setImage:[UIImage imageNamed:@"WL_addCpn_live.png"] forState:UIControlStateNormal];
    [addRemGalley addTarget:self action:@selector(addRemoveFromGallery:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *imagePathStr = @"N/A";
    SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(50, 6, asyncImageWidth, asyncImageHeight)];
    asyncImageView.contentMode = UIViewContentModeScaleToFill;
    asyncImageView.backgroundColor = [UIColor clearColor];
    
    
    if ([[myCLRHdrNameArray objectAtIndex:indexPath.section] isEqualToString:@"Coupons"]) {
        if ((indexPath.section == [myCLRHdrNameArray count]-1 ) && (indexPath.row == [couponNameArray count])) {
            if (clrC == 1 || clrL == 1 || clrR == 1 ) {
                UIButton *next = [UIButton buttonWithType:UIButtonTypeCustom];
                next.frame = CGRectMake(SCREEN_WIDTH - 110, 7, 80, 25);//x 210
                [next setImage:[UIImage imageNamed:@"loginscreen_Nextbtn.png"] forState:UIControlStateNormal];
                [next addTarget:self action:@selector(fetchNextDeals:) forControlEvents:UIControlEventTouchUpInside];
                [cell addSubview:next];
            }
            if (!(lowerLimit == 0)) {
                UIButton *previous = [UIButton buttonWithType:UIButtonTypeCustom];
                previous.frame = CGRectMake(10, 7, 80, 25);
                [previous setImage:[UIImage imageNamed:@"loginscreen_Prevbtn.png"] forState:UIControlStateNormal];
                [previous addTarget:self action:@selector(fetchPreviousDeals:) forControlEvents:UIControlEventTouchUpInside];
                [cell addSubview:previous];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            ReleaseAndNilify(tx_label);
            ReleaseAndNilify(detailLabel);
            return cell;
            
        }
        else{
            if ([couponAdded count] >0) {
                if ([[couponAdded objectAtIndex:indexPath.row] isEqualToString:@"Green"])
                    [addRemGalley setImage:[UIImage imageNamed:@"WL_addCpn_Off.png"] forState:UIControlStateNormal];
            }
            tx_label.text = [couponNameArray objectAtIndex:indexPath.row];
            detailLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ off Expires %@",@"%@ off Expires %@"),
                                [couponDiscountAmountArray objectAtIndex:indexPath.row],
                                [couponExpireDateArray objectAtIndex:indexPath.row]];
            imagePathStr = [couponImagePath objectAtIndex:indexPath.row];
            
        }
    }
    
    
    if(![imagePathStr isEqualToString:@"N/A"])
    {
        //[asyncImageView loadImageFromURL:[NSURL URLWithString:imagePathStr]];
        [asyncImageView loadImage:imagePathStr];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    [cell addSubview:asyncImageView];
    [cell addSubview:tx_label];
    [cell addSubview:detailLabel];
    [cell addSubview:addRemGalley];
    //[detailLabel release];
    //[tx_label release];
    
    return cell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width,30.0)];
    float headerLabelFont = 15.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        headerLabelFont = 20.0;
    }
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, SCREEN_WIDTH - 10, 20)];//width 310
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:headerLabelFont];
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.text = [myCLRHdrNameArray objectAtIndex:section];
    [headerView setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
    [headerView addSubview:headerLabel];
    //[headerLabel release];
    return headerView;
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
 return [myCLRHdrNameArray objectAtIndex:section];
 }*/

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return nil;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([couponIdArray count] >indexPath.row)
        [defaults setValue:[couponIdArray objectAtIndex:indexPath.row] forKey:@"couponId"];
    [defaults  setValue:@"N/A" forKey:@"imgPath"];
    if([couponImagePath count] >indexPath.row)
        [defaults  setValue:[couponImagePath objectAtIndex:indexPath.row] forKey:@"imgPath"];
    
    NewCouponDetailViewController *obj_CouponGalleryDetail = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:obj_CouponGalleryDetail animated:NO];
    //[obj_CouponGalleryDetail release];
    
    [couponTableView deselectRowAtIndexPath:[couponTableView indexPathForSelectedRow] animated:YES];
}

-(void) addRemoveFromGallery : (UIButton *) sender {
    //	NSIndexPath *index = [couponTableView indexPathForCell:(UITableViewCell *)[sender superview]];
    //updated for iOS7 bug fixing
    CGPoint location = [sender.superview convertPoint:sender.center toView:couponTableView];
    
    
    NSIndexPath *index ;
    index = [couponTableView indexPathForRowAtPoint:location];
    
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    else {
        NSString *str = @"";
        NSMutableString *xmlStr = [[NSMutableString alloc] init];
        [xmlStr appendFormat:@"<CLRDetails><userId>%@</userId>",[defaults objectForKey:KEY_USERID]];
        
        if ([[myCLRHdrNameArray objectAtIndex:index.section] isEqualToString:@"Coupons"] == YES) {
            [xmlStr appendFormat:@"<couponId>%@</couponId>", [couponIdArray objectAtIndex:index.row]];
            // if([couponListIDElementArray count] > index.row)
            // [xmlStr appendFormat:@"<coupListID>%@</coupListID>",[couponListIDElementArray objectAtIndex:index.row]];
            if ([[couponAdded objectAtIndex:index.row] isEqualToString:@"Red"])
                str = @"add";
            else
                str = @"remove";
            [defaults  setValue:VALUE_COUPON forKey:KEY_CLR];
        }
        
        [xmlStr appendString:@"</CLRDetails>"];
        
        NSMutableString *urlString = [BASE_URL mutableCopy];
        [urlString appendFormat:@"gallery/%@%@", str,[defaults valueForKey:KEY_CLR]];
        
        
        if([Network currentReachabilityStatus]==0) {
            [UtilityManager showAlert];
            ReleaseAndNilify(xmlStr);
            return;
        }
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:xmlStr]];
        //[xmlStr release];
        
        if ([UtilityManager isNullOrEmptyString:responseXml]) {
            ReleaseAndNilify(responseXml);
            return;
        }
        TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
        
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        int responseCode = [[TBXML textForElement:responseCodeElement] intValue];
        if (responseCode == 10000) {
            //	[self removeAllObjectsInArray];
            
            if ([[[self.navigationController viewControllers] objectAtIndex:[[self.navigationController viewControllers] count]-2 ] isKindOfClass:[ProductPage class]]) {
                [defaults  setBool:YES forKey:@"refreshProd"];
            }
            [self refreshDataStructure];
            [SharedManager setRefreshList:YES];
            //	[self viewWillAppear:YES];
        }
        else {
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            }
        
        //[tbxml release];
        //[responseXml release];
        return;
    }
}


- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(  [[myCLRHdrNameArray objectAtIndex:indexPath.section] isEqualToString:@"Coupons"] && [[couponAdded objectAtIndex:indexPath.row] isEqualToString:@"Green"]){
        
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
    
}


- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Used";
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([myCLRHdrNameArray count] == 0)
        return ;
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
        if([Network currentReachabilityStatus]==0)
        {
            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
            }
        else {
            
            NSMutableString *urlString = [BASE_URL mutableCopy];
            
            NSMutableString *xmlStr = [[NSMutableString alloc] init];
            
            if(  [[myCLRHdrNameArray objectAtIndex:indexPath.section] isEqualToString:@"Coupons"]){
                
                [xmlStr appendFormat:@"<CouponDetails><userId>%@</userId>",[defaults objectForKey:KEY_USERID]];
                //@Deepak: Added couponListID for UserTracking
                if([couponListIDElementArray count] > indexPath.row)
                    [xmlStr appendFormat:@"<coupListID>%@</coupListID>",[couponListIDElementArray objectAtIndex:indexPath.row]];
                [xmlStr appendFormat:@"<couponId>%@</couponId></CouponDetails>", [couponIdArray objectAtIndex:indexPath.row]];
                [defaults  setValue:VALUE_COUPON forKey:KEY_CLR];
                [urlString appendFormat:@"mygallery/userredeemcoupon"];
            }
            DLog(@"request %@", xmlStr);
            NSString  *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:xmlStr]];
            ReleaseAndNilify(xmlStr);
            if ([UtilityManager isNullOrEmptyString:responseXml]) {
                ReleaseAndNilify(responseXml);
                return;
            }
            TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
            
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            return;
            
        }
        
        
    }
}


-(IBAction) listClicked : (id) sender {
    [SharedManager setRefreshList:YES];
    //[UtilityManager pushPopToList:self.navigationController];
}

-(IBAction)wishListClicked:(id)sender{
    
    //	WishListHomePage *wishListPage = [[WishListHomePage alloc]initWithNibName:@"WishListHomePage" bundle:[NSBundle mainBundle]];
    //	[self.navigationController pushViewController:wishListPage animated:NO];
    //	[wishListPage release];
}

-(IBAction)couponListClicked:(id)sender{
    
    // Set Bool when Not calling "MyCouponsViewController" from MainMenu
    [defaults  setBool:NO forKey:@"myGallerysplash"];
    [defaults setBool:NO forKey:@"CalledFromMainMenu"];
    //	MyCouponsViewController *myGal = [[MyCouponsViewController alloc]initWithNibName:@"MyCouponsViewController" bundle:[NSBundle mainBundle]];
    //	[self.navigationController pushViewController:myGal animated:NO];
    //	[myGal release];
}

-(IBAction)preferencesClicked:(id)sender{
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert];
    }
    else
    {
        
        //		SettingsViewController *settings = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
        //		[self.navigationController pushViewController:settings animated:NO];
        //		//[settings release];
        
    }
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

//- (void)viewDidUnload {
//    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
//    // For example: self.myOutlet = nil;
//}




@end





//
//  LocationsInCouponDetailTableViewCell.m
//  HubCiti
//
//  Created by Nikitha on 12/26/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "LocationsInCouponDetailTableViewCell.h"

@implementation LocationsInCouponDetailTableViewCell
@synthesize titleLabelText,detailLabelText,sdImageViewOutlet;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void) setDataForImageAndLabel : (NSString*) imagePath titleLabel:(NSString *) titleText detailLabel:(NSString*)detailText
{
    titleLabelText.text = titleText;
    detailLabelText.text = detailText;
    [sdImageViewOutlet loadImage:imagePath];
    

//    NSString *utf = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
//    imageViewOutlet.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:utf]]];
}
@end

//
//  QualifyingProductsList.h
//  Scansee
//
//  Created by Stephanie Blackwell on 5/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HubCitiSegmentedControl.h"

typedef enum webServicesQualifyingProducts
{
    GETLOCATION,
    GETPRODUCT,
    RETSUMMARYS
}webServicesQualifyingProductsState;

@interface QualifyingProductsList : UIViewController<UITableViewDelegate, UITableViewDataSource,CustomizedNavControllerDelegate>
{
    webServicesQualifyingProductsState iWebRequestState;
    NSMutableArray *productNameArray;
    NSMutableArray *productIdArray,*retailLocationIdArray;
    NSMutableArray *prodDesc;
    NSMutableArray *prodImagePath;
    NSMutableArray *productChecked;
    
    IBOutlet UITableView *productsTable;
    IBOutlet UIButton *rateBtn;
    IBOutlet UIButton *wishBtn;
    IBOutlet UIButton *couponGalBtn;
    IBOutlet UIButton *prefBtn;
    int numSelected;
    HubCitiSegmentedControl *segmentedControl;
    int displayProductandLoc;
    int currentDisplayingTab;
}
@property(nonatomic,readwrite)int displayProductandLoc;;
@end

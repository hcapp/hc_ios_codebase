//
//  CouponsViewController.m
//  Scansee
//
//  Created by ajit on 9/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CouponsViewController.h"
#import "MainMenuViewController.h"
#import "NewCouponDetailViewController.h"
#import "WebBrowserViewController.h"
#import "ProductPage.h"
#import "RetailerSummaryViewController.h"
//#import "AddRemLoyaltyCardViewController.h"
#import "PreferredCategoriesScreen.h"
//#import "MyCouponPopulationCenterCities.h"
//#import "GalleryList.h"
#import "LoginViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "AppDelegate.h"

@implementation CouponsViewController{
    UIActivityIndicatorView *loading;
}
@synthesize searchView;
//static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    lowerLimitvalue = 0;
    self.title = NSLocalizedString(@"Coupons",@"Coupons");
    [defaults setValue:nil forKey:@"couponzipcode"];
    
    //	if ([defaults  boolForKey:@"myGallerysplash"] == YES)
    //		[self showSplash];
    
    loading = nil;
    [defaults setValue:nil forKey:@"popCentId"];
    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    catPicker.hidden = YES;
    toolBar.hidden = YES;
    searchTextField.delegate = self;
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //
    
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    ////[mainPage release];
    
    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    ////[back release];
    
    [SharedManager setCouLoySearchActive:NO];
    [SharedManager setRefreshGallery:NO];
    searchView.hidden = YES;
    myGalleryTV.userInteractionEnabled = YES;
    myGalleryTV.backgroundColor = [UIColor whiteColor];
    segmentedControl.userInteractionEnabled = YES;
    couponGalleryBtn.userInteractionEnabled = YES;
    bycityBtn.userInteractionEnabled = YES;
    searchBtn.userInteractionEnabled = YES;
    preferanceBtn.userInteractionEnabled = YES;
    
    searchView.layer.cornerRadius = 10;
    [searchView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [searchView.layer setBorderWidth:1.5f];
    [searchView.layer setShadowColor:[UIColor blackColor].CGColor];
    [searchView.layer setShadowOpacity:0.8];
    [searchView.layer setShadowRadius:3.0];
    [searchView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    searchView.backgroundColor = [UIColor colorWithRed:95 green:183 blue:215 alpha:1.0];
    
    
    [self.view bringSubviewToFront:searchView];
    
    //    couponGalleryBtn.tag = 0;
    //	[couponGalleryBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateHighlighted];
    //    [couponGalleryBtn removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
    //    [couponGalleryBtn addTarget:self action:@selector(galleryTabClicked) forControlEvents:UIControlEventTouchUpInside];
    //
    //    bycityBtn.tag = 0;
    //	[bycityBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateHighlighted];
    //    [bycityBtn removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
    //    [bycityBtn addTarget:self action:@selector(byCityTabClicked) forControlEvents:UIControlEventTouchUpInside];
    //
    //	searchBtn.tag = 0;
    //    [searchBtn setImage:[UIImage imageNamed:@"tab_btn_down_srchTxt.png"] forState:UIControlStateHighlighted];
    //    [searchBtn removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
    //    [searchBtn addTarget:self action:@selector(searchTabClicked) forControlEvents:UIControlEventTouchUpInside];
    //
    //
    //    preferanceBtn.tag = 3;
    //    [SharedManager setEnableAddCoupon:NO];
    //    [preferanceBtn setImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    //    [preferanceBtn setImage:[UIImage imageNamed:@"tab_btn_down_preferences.png"] forState:UIControlStateHighlighted];
    //    [preferanceBtn removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
    //    [preferanceBtn addTarget:self action:@selector(preferanceTabClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    //myGalleryTV.tag = 3;
    //self.title = NSLocalizedString(@"My Coupons",@"My Coupons");
    [SharedManager setMyGallery:YES];
    
    arrCatagories = [[NSMutableArray alloc]init];
    dicProductsCoupon = [[NSMutableDictionary alloc]init];
    
    
    dicLocationCupon = [[NSMutableDictionary alloc]init];
    arrLocationCategoriesforRetailers = [[NSMutableArray alloc]init];
    arrSectionArrayForRetailers = [[NSMutableArray alloc]init];
    [arrSectionArrayForRetailers addObject:@"1"];
    
    
    // If user is coming from anywhere other then main menu "getMuCouponItemsforLocation" will call otherwise it will call from HideSplash
    [self getMuCouponItemsforLocation];
    
    
}

//To get Copuons based on retailers location if users location data is there otherwise display only products data
-(void)getMuCouponItemsforLocation
{
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE])
    {
        [self addsegementcontrolleronView];
        myGalleryTV.tag = 1;
        [self callLocationbasedCoupons];
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length])
    {
        myGalleryTV.tag = 1;
        [self addsegementcontrolleronView];
        [self callLocationbasedCoupons];
    }
    else
    {
        myGalleryTV.tag = 0;
        [self addsegementcontrolleronView];
        [self showZipEnter];
        
    }
}


// Display Enter ZipCode PopUp
-(void)showZipEnter
{
    
        // use UIAlertController
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Please enter your zip code"
                                   message:nil
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        zipSave = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       UITextField *textField = alert.textFields[0];
                                                       [defaults setValue:textField.text forKey:@"couponzipcode"];
                                                       myGalleryTV.tag = 0;
                                                       [self callLocationbasedCoupons];
                                                       
                                                       
                                                   }];
        zipSave.enabled = NO;
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           NSLog(@"cancel btn");
                                                           
                                                           segmentedControl.selectedSegmentIndex=1;
                                                           [self getMuCouponItemsforProducts];
                                                           
                                                       }];
        
        [alert addAction:cancel];
        [alert addAction:zipSave];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            [textField setTag:31];
            textField.delegate = self;
            textField.keyboardType = UIKeyboardTypeNumberPad;
            textField.font = [UIFont systemFontOfSize:16];
        }];
        
        [self presentViewController:alert animated:YES completion:nil];
        
   
   
}


//check the characters in zipcode textField
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // textField for ZipCode
    if(textField.tag == 31){
        NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        if (range.length == 1){
            NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [zipSave setEnabled:(finalString.length >= 5)];
            return YES;
        }
        else {
            for (int j = 0; j<[string length]; j++) {
                unichar c = [string characterAtIndex:j];
                if ([charSet characterIsMember:c]) {
                    NSUInteger textLength = [textField.text length] + [string length] - range.length;
                    [zipSave setEnabled:(textLength >= 5)];
                    return (textLength > 5)? NO : YES;
                }
                else {
                    return NO;
                }
            }
        }
    }
    
    return YES;
}



-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}



// If users location data is available then only will display segement otherwise only products will display on the screen
-(void)addsegementcontrolleronView
{
    //    if(segmentedControl)
    //    {
    //        [segmentedControl removeFromSuperview];
    //        [segmentedControl release];
    //    }
    // Set the two segement one for Location and another for Product
    NSArray *items = [NSArray arrayWithObjects:@"Locations",@"Products", nil];
    segmentedControl = [[HubCitiSegmentedControl alloc] initWithItems:items];
    [segmentedControl setFrame:CGRectMake(-7, 0, 334, 44)];
    [segmentedControl setSelectedSegmentIndex:0];
    [segmentedControl setSegmentColor];
    [segmentedControl addTarget:self action:@selector(segmentChanged) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setBackgroundColor:[UIColor blackColor]];
    
    [self.view addSubview:segmentedControl];
    
}


// Method to get the server data for the coupon based on retailers location and business catagory
-(void)callLocationbasedCoupons
{
    isProductTabDisplaying = NO;
    [myGalleryTV setHidden:NO];
    
    iWebrequest = GETALLCOUPBYLOC;
    
    NSMutableString *reqStr = [[NSMutableString alloc] init];
    [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    if([searchTextField.text length] > 0)
    {
        [reqStr appendFormat:@"<searchKey>%@</searchKey>",searchTextField.text];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    else if ([[defaults  valueForKey:@"couponzipcode"]length]){
        
        [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:@"couponzipcode"]];
    }
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [reqStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
    
    else if([defaults  valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<moduleId>%@</moduleId>", [defaults  valueForKey:KEY_MITEMID]];
    
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>", [defaults  valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<retailId>%d</retailId>", selectedbusinesscatRetailerID];
    
    [reqStr appendFormat:@"<busCatIds>%d</busCatIds>", selectedbusinesscatID];
    
    
    [reqStr appendFormat:@"<lowerLimit>%d</lowerLimit>", lowerLimitvalue];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults valueForKey:KEY_HUBCITIID]];
    [reqStr appendFormat:@"<platform>%@</platform>", @"IOS"];
    
    if([defaults  valueForKey:@"popCentId"])
        [reqStr appendFormat:@"<popCentId>%@</popCentId>", [defaults  valueForKey:@"popCentId"]];
    
    [reqStr appendString:@"</ProductDetailsRequest>"];
    
    NSMutableString *urlString1 = [BASE_URL mutableCopy] ;
    [urlString1 appendString:@"gallery/getallcoupbyloc"];
    
    
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString1 withParam:reqStr];
        
        
        [self parseMyCoupLocation:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:reqStr base:urlString1 withDelegate:self];
    }
    
    
    
    // [ConnectionManager establishConnectionFor:reqStr base:urlString1 withDelegate:self];
    
    
    //[reqStr release];
    
    selectedbusinesscatID = 0;
    selectedbusinesscatRetailerID = 0;
    searchTextField.text=@"";
    
    if (lowerLimitvalue == 0)
        [myGalleryTV setHidden:YES];
    
}


// Parse and get details for Retailers Location data
-(void)parseMyCoupLocation:(NSString*)responseString
{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        // Check if Next flag is '1' means need to show "View More result"
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPageFlag" parentElement:tbxml.rootXMLElement];
        
        nextpageFlag = (int)[[TBXML textForElement:nextPageElement]integerValue];
        
        // Parse and Store mainMenuID
        TBXMLElement *mainMenuIDElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if(mainMenuIDElement)
        {
            [defaults setObject:[TBXML textForElement:mainMenuIDElement] forKey:KEY_MAINMENUID];
        }
        
        TBXMLElement *retDetailsListElement = [TBXML childElementNamed:@"retDetailsList" parentElement:tbxml.rootXMLElement];
        
        // This is the main tag under which all catagory and coupons will come
        if(retDetailsListElement)
        {
            //parse catagory Info element
            TBXMLElement *RetailerDetailsElement1 = [TBXML childElementNamed:@"RetailersDetails" parentElement:retDetailsListElement];
            // Loop will run for all retailers, under this all retailers and category will comes
            while(RetailerDetailsElement1)
            {
                int iRepetedcategoryforSameretailers = 0;
                
                TBXMLElement *retIdElement = [TBXML childElementNamed:@"retId" parentElement:RetailerDetailsElement1];
                TBXMLElement *retNameElement = [TBXML childElementNamed:@"retName" parentElement:RetailerDetailsElement1];
                
                
                // Get the categories under each retailers
                TBXMLElement *retDetailsListElement = [TBXML childElementNamed:@"retDetailsList" parentElement:RetailerDetailsElement1];
                
                // Header tag for category under retailer
                if(retDetailsListElement)
                {
                    TBXMLElement *RetailersDetailsElement = [TBXML childElementNamed:@"RetailersDetails" parentElement:retDetailsListElement];
                    NSMutableArray *arrLocationCategories = [[NSMutableArray alloc]init];
                    
                    // Get all category under each retailers
                    // Loop will run for all catagories, under this all the coupons for each catagory will comes
                    while(RetailersDetailsElement)
                    {
                        
                        //parse categoryID element from catagory info
                        TBXMLElement *ccategoryIDElement = [TBXML childElementNamed:@"retLocId" parentElement:RetailersDetailsElement];
                        //parse categoryName element from catagory info
                        TBXMLElement *categoryNameElement = [TBXML childElementNamed:@"retailerAddress" parentElement:RetailersDetailsElement];
                        
                        TBXMLElement *cityElement = [TBXML childElementNamed:@"city" parentElement:RetailersDetailsElement];
                        
                        TBXMLElement *postalCodeElement = [TBXML childElementNamed:@"postalCode" parentElement:RetailersDetailsElement];
                        
                        NSMutableString *strAddress = [[NSMutableString alloc]init];
                        if(categoryNameElement)
                            [strAddress appendString:[TBXML textForElement:categoryNameElement]];
                        if(cityElement)
                            [strAddress appendFormat:@",%@",[TBXML textForElement:cityElement]];
                        if(postalCodeElement)
                            [strAddress appendFormat:@",%@",[TBXML textForElement:postalCodeElement]];
                        
                        // Saved the "retLocId" as categoryID named and "retailerAddress" as categoryName
                        NSMutableDictionary *dicCat = [[NSMutableDictionary alloc]init];
                        if(ccategoryIDElement)
                            [dicCat setValue:[TBXML textForElement:ccategoryIDElement] forKey:@"categoryId"];
                        if(categoryNameElement)
                            [dicCat setValue:strAddress forKey:@"categoryName"];
                        if(retIdElement)
                            [dicCat setValue:[TBXML textForElement:retIdElement] forKey:@"retId"];
                        if(retNameElement)
                            [dicCat setValue:[TBXML textForElement:retNameElement] forKey:@"retName"];
                        
                        //  [strAddress release];
                        
                        
                        // if last category and retailer value is same then will not add same category again
                        if([arrLocationCategoriesforRetailers count] > 0)
                        {
                            if(![[[arrLocationCategoriesforRetailers lastObject]valueForKey:@"retId"]isEqualToString:[TBXML textForElement:retIdElement]])
                            {
                                [arrSectionArrayForRetailers addObject:[NSString stringWithFormat:@"%u",(int)[arrLocationCategoriesforRetailers count]+1]];
                            }
                            BOOL isFound = NO;
                            for(int i=0; i<[arrLocationCategoriesforRetailers count]; i++)
                            {
                                NSDictionary *dic = [arrLocationCategoriesforRetailers objectAtIndex:i];
                                if(([[dic valueForKey:@"retId"]isEqualToString:[TBXML textForElement:retIdElement]] && [[dic valueForKey:@"categoryId"]isEqualToString:[TBXML textForElement:ccategoryIDElement]]))
                                {
                                    iRepetedcategoryforSameretailers+=1;
                                    isFound = YES;
                                    break;
                                }
                            }
                            
                            if(!isFound)
                                [arrLocationCategoriesforRetailers addObject:dicCat];
                        }
                        else
                            [arrLocationCategoriesforRetailers addObject:dicCat];
                        
                        [arrLocationCategories addObject:dicCat];
                        
                        // release local objects
                        // [dicCat release];
                        
                        
                        // Array to contain the all Coupons object for the particular category
                        NSMutableArray *arrCoupons = [[NSMutableArray alloc]init];
                        
                        //parse couponDetailsList element from catagory info
                        TBXMLElement *couponDetailsListElement = [TBXML childElementNamed:@"couponDetailsList" parentElement:RetailersDetailsElement];
                        
                        
                        // Under this all the coupon for particuler catagory will come
                        if(couponDetailsListElement)
                        {
                            //parse couponDetailsList element from catagory info
                            TBXMLElement *CouponDetailsElement = [TBXML childElementNamed:@"CouponDetails" parentElement:couponDetailsListElement];
                            
                            // Under this coupons' data will come
                            while(CouponDetailsElement)
                            {
                                NSMutableDictionary *dicCoupons = [[NSMutableDictionary alloc]init];
                                
                                //Parse and save Coupons data under catagory
                                TBXMLElement *couponIdElement = [TBXML childElementNamed:@"couponId" parentElement:CouponDetailsElement];
                                TBXMLElement *couponNameElement = [TBXML childElementNamed:@"couponName" parentElement:CouponDetailsElement];
                                TBXMLElement *couponStartDateElement = [TBXML childElementNamed:@"couponStartDate" parentElement:CouponDetailsElement];
                                TBXMLElement *couponExpireDateElement = [TBXML childElementNamed:@"couponExpireDate" parentElement:CouponDetailsElement];
                                TBXMLElement *couponImagePathElement = [TBXML childElementNamed:@"couponImagePath" parentElement:CouponDetailsElement];
                                TBXMLElement *coupDescElement = [TBXML childElementNamed:@"coupDesc" parentElement:CouponDetailsElement];
                                TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:CouponDetailsElement];
                                TBXMLElement *claimFlagElement = [TBXML childElementNamed:@"claimFlag" parentElement:CouponDetailsElement];
                                TBXMLElement *redeemFlagElement = [TBXML childElementNamed:@"redeemFlag" parentElement:CouponDetailsElement];
                                TBXMLElement *newFlagElement = [TBXML childElementNamed:@"newFlag" parentElement:CouponDetailsElement];
                                
                                //0 - Not yet added (Red), 1- cliped(Green)
                                TBXMLElement *usedElement = [TBXML childElementNamed:@"used" parentElement:CouponDetailsElement];
                                TBXMLElement *coupToIssueElement = [TBXML childElementNamed:@"coupToIssue" parentElement:CouponDetailsElement];
                                TBXMLElement *couponDiscountAmountElement = [TBXML childElementNamed:@"couponDiscountAmount" parentElement:CouponDetailsElement];
                                TBXMLElement *coupListIDElement = [TBXML childElementNamed:@"couponListId" parentElement:CouponDetailsElement];
                                
                                TBXMLElement *viewableonWebElement = [TBXML childElementNamed:@"viewableOnWeb" parentElement:CouponDetailsElement];
                                
                                if(viewableonWebElement)
                                    [dicCoupons setValue:[TBXML textForElement:viewableonWebElement] forKey:@"viewableOnWeb"];
                                
                                if(coupListIDElement)
                                    [dicCoupons setValue:[TBXML textForElement:coupListIDElement] forKey:@"couponListId"];
                                if(couponDiscountAmountElement)
                                    [dicCoupons setValue:[TBXML textForElement:couponDiscountAmountElement] forKey:@"couponDiscountAmount"];
                                if(couponIdElement)
                                    [dicCoupons setValue:[TBXML textForElement:couponIdElement] forKey:@"couponId"];
                                if(couponNameElement)
                                    [dicCoupons setValue:[TBXML textForElement:couponNameElement] forKey:@"couponName"];
                                if(couponStartDateElement)
                                    [dicCoupons setValue:[TBXML textForElement:couponStartDateElement] forKey:@"couponStartDate"];
                                if(couponExpireDateElement)
                                    [dicCoupons setValue:[TBXML textForElement:couponExpireDateElement] forKey:@"couponExpireDate"];
                                if(couponImagePathElement)
                                    [dicCoupons setValue:[TBXML textForElement:couponImagePathElement] forKey:@"couponImagePath"];
                                if(coupDescElement)
                                    [dicCoupons setValue:[TBXML textForElement:coupDescElement] forKey:@"coupDesc"];
                                if(distanceElement)
                                    [dicCoupons setValue:[TBXML textForElement:distanceElement] forKey:@"distance"];
                                if(claimFlagElement)
                                    [dicCoupons setValue:[TBXML textForElement:claimFlagElement] forKey:@"claimFlag"];
                                if(redeemFlagElement)
                                    [dicCoupons setValue:[TBXML textForElement:redeemFlagElement] forKey:@"redeemFlag"];
                                if(newFlagElement)
                                    [dicCoupons setValue:[TBXML textForElement:newFlagElement] forKey:@"newFlag"];
                                if(usedElement)
                                {
                                    // if Only Claim then 1, if redeemed then 2 else 0
                                    NSString *val = @"0";
                                    if([[TBXML textForElement:claimFlagElement]isEqualToString:@"1"])
                                        val = @"1";
                                    if([[TBXML textForElement:redeemFlagElement]isEqualToString:@"1"])
                                        val = @"2";
                                    
                                    [dicCoupons setValue:val forKey:@"used"];
                                    
                                }
                                if(coupToIssueElement)
                                    [dicCoupons setValue:[TBXML textForElement:coupToIssueElement] forKey:@"coupToIssue"];
                                
                                [arrCoupons addObject:dicCoupons];
                                // //[dicCoupons release];
                                
                                
                                // Get the next sibling of the "CouponDetails"
                                CouponDetailsElement = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:CouponDetailsElement];
                                
                            }
                            NSMutableString *str = [[NSMutableString alloc]initWithString:[TBXML textForElement:retIdElement]];
                            [str appendFormat:@"-%@",[TBXML textForElement:ccategoryIDElement]];
                            
                            if([dicLocationCupon count] > 0)
                            {
                                if([[dicLocationCupon allKeys]containsObject:str])
                                {
                                    NSMutableArray *arr = [[NSMutableArray alloc]initWithArray:[dicLocationCupon valueForKey:str]];
                                    [arr addObjectsFromArray:arrCoupons];
                                    [dicLocationCupon setValue:arr forKey:str];
                                    //[arr release];
                                }
                                else
                                    [dicLocationCupon setValue:arrCoupons forKey:str];
                            }
                            else
                                [dicLocationCupon setValue:arrCoupons forKey:str];
                            
                            
                            
                            // //[arrCoupons release];
                            // [str release];
                        }
                        
                        
                        
                        // Get the next sibling of the "CategoryInfo"
                        RetailersDetailsElement = [TBXML nextSiblingNamed:@"RetailersDetails" searchFromElement:RetailersDetailsElement];
                        
                    }
                    
                    
                    ////[arrLocationCategories release];
                }
                
                RetailerDetailsElement1 = [TBXML nextSiblingNamed:@"RetailersDetails" searchFromElement:RetailerDetailsElement1];
            }
            
        }
    }
    else
    {
        
        if (myGalleryTV.tag == 1){
            segmentedControl.selectedSegmentIndex=1;
            [self getMuCouponItemsforProducts];
            return;
        }
        
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        [loading stopAnimating];
        ReleaseAndNilify(loading);
        
    }
    
    if([arrLocationCategoriesforRetailers count] > 0)
    {
        [myGalleryTV setHidden:NO];
    }
    else
    {
        visitAllCoupons.font=[UIFont systemFontOfSize:14];
        visitAllCoupons.text = @"There are no coupons by Location";
    }
    [myGalleryTV reloadData];
    [defaults setBool:NO forKey:@"ViewMore"];
}



// Method to get the server data for the coupon based on products catagory
-(void)getMuCouponItemsforProducts
{
    isProductTabDisplaying = YES;
    
    
    iWebrequest = GETALLCOUPBYPROD;
    
    NSMutableString *reqStr = [[NSMutableString alloc] init];
    [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if([searchTextField.text length] > 0)
    {
        [reqStr appendFormat:@"<searchKey>%@</searchKey>",searchTextField.text];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    else if ([[defaults  valueForKey:@"couponzipcode"]length]){
        
        [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:@"couponzipcode"]];
    }
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [reqStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
    
    else if([defaults  valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<moduleId>%@</moduleId>", [defaults  valueForKey:KEY_MITEMID]];
    
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>", [defaults  valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<retailId>%d</retailId>", selectedbusinesscatRetailerID];
    
    [reqStr appendFormat:@"<busCatIds>%d</busCatIds>", selectedbusinesscatID];
    
    
    [reqStr appendFormat:@"<lowerLimit>%d</lowerLimit>", lowerLimitvalue];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults valueForKey:KEY_HUBCITIID]];
    [reqStr appendFormat:@"<platform>%@</platform>", @"IOS"];
    
    if([defaults  valueForKey:@"popCentId"])
        [reqStr appendFormat:@"<popCentId>%@</popCentId>", [defaults  valueForKey:@"popCentId"]];
    
    
    [reqStr appendString:@"</ProductDetailsRequest>"];
    
    NSMutableString *urlString1 = [BASE_URL mutableCopy] ;
    [urlString1 appendString:@"gallery/getallcoupbyprod"];
    
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString1 withParam:reqStr];
        
        
        [self parseMyCoupProduct:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:reqStr base:urlString1 withDelegate:self];
    }
    
    
    
    
    // [ConnectionManager establishConnectionFor:reqStr base:urlString1 withDelegate:self];
    
    //[reqStr release];
    
    selectedbusinesscatID = 0;
    selectedbusinesscatRetailerID = 0;
    searchTextField.text=@"";
    
    if (lowerLimitvalue == 0)
        [myGalleryTV setHidden:YES];
    
}

-(void)responseData:(NSString *) response
{
    if(isCalledFromSegementChange)
    {
        isCalledFromSegementChange = NO;
        [self setWhenCalledfromSegementChange];
    }
    switch (iWebrequest) {
        case GETALLCOUPBYPROD:
            [self parseMyCoupProduct:response];
            break;
        case GETALLCOUPBYLOC:
            [self parseMyCoupLocation:response];
            break;
        case GETUSERCATCOUPON:
            [self parseUserCategory:response];
            break;
            //        case addRemoveCoupon:
            //            [self parse_addRemoveCoupon:response];
            //            break;
            
        default:
            break;
    }
    
    [myGalleryTV reloadData];
}

-(void)parseUserCategory:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        [defaults  setObject:responseString forKey:KEY_RESPONSEXML];
        PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:settings animated:NO];
        // //[settings release];
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
}

// Parse and get the details for category data under products
-(void)parseMyCoupProduct:(NSString*)responseString
{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        // Check if Next flag is '1' means need to show "View More result"
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPageFlag" parentElement:tbxml.rootXMLElement];
        
        nextpageFlag = (int)[[TBXML textForElement:nextPageElement]integerValue];
        
        // Parse and Store mainMenuID
        TBXMLElement *mainMenuIDElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if(mainMenuIDElement)
        {
            [defaults setObject:[TBXML textForElement:mainMenuIDElement] forKey:KEY_MAINMENUID];
        }
        
        TBXMLElement *categoryInfoListElement = [TBXML childElementNamed:@"categoryInfoList" parentElement:tbxml.rootXMLElement];
        
        // This is the main tag under which all catagory and coupons will come
        if(categoryInfoListElement)
        {
            //parse catagory Info element
            TBXMLElement *CategoryInfoElement = [TBXML childElementNamed:@"CategoryInfo" parentElement:categoryInfoListElement];
            
            
            // Loop will run for all catagories, under this all the coupons for each catagory will comes
            while(CategoryInfoElement)
            {
                // Array to contain the all Coupons object for the particular category
                NSMutableArray *arrCoupons = [[NSMutableArray alloc]init];
                
                //parse couponDetailsList element from catagory info
                TBXMLElement *couponDetailsListElement = [TBXML childElementNamed:@"couponDetailsList" parentElement:CategoryInfoElement];
                
                
                // Under this all the coupon for particuler catagory will come
                if(couponDetailsListElement)
                {
                    //parse couponDetailsList element from catagory info
                    TBXMLElement *CouponDetailsElement = [TBXML childElementNamed:@"CouponDetails" parentElement:couponDetailsListElement];
                    
                    // Under this coupons' data will come
                    while(CouponDetailsElement)
                    {
                        NSMutableDictionary *dicCoupons = [[NSMutableDictionary alloc]init];
                        
                        //Parse and save Coupons data under catagory
                        TBXMLElement *couponIdElement = [TBXML childElementNamed:@"couponId" parentElement:CouponDetailsElement];
                        TBXMLElement *couponNameElement = [TBXML childElementNamed:@"couponName" parentElement:CouponDetailsElement];
                        TBXMLElement *couponStartDateElement = [TBXML childElementNamed:@"couponStartDate" parentElement:CouponDetailsElement];
                        TBXMLElement *couponExpireDateElement = [TBXML childElementNamed:@"couponExpireDate" parentElement:CouponDetailsElement];
                        TBXMLElement *couponImagePathElement = [TBXML childElementNamed:@"couponImagePath" parentElement:CouponDetailsElement];
                        TBXMLElement *coupDescElement = [TBXML childElementNamed:@"coupDesc" parentElement:CouponDetailsElement];
                        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:CouponDetailsElement];
                        TBXMLElement *claimFlagElement = [TBXML childElementNamed:@"claimFlag" parentElement:CouponDetailsElement];
                        TBXMLElement *redeemFlagElement = [TBXML childElementNamed:@"redeemFlag" parentElement:CouponDetailsElement];
                        TBXMLElement *newFlagElement = [TBXML childElementNamed:@"newFlag" parentElement:CouponDetailsElement];
                        
                        TBXMLElement *viewableonWebElement = [TBXML childElementNamed:@"viewableOnWeb" parentElement:CouponDetailsElement];
                        
                        
                        //0 - Not yet added (Red), 1- cliped(Green)
                        TBXMLElement *usedElement = [TBXML childElementNamed:@"used" parentElement:CouponDetailsElement];
                        TBXMLElement *coupToIssueElement = [TBXML childElementNamed:@"coupToIssue" parentElement:CouponDetailsElement];
                        TBXMLElement *couponDiscountAmountElement = [TBXML childElementNamed:@"couponDiscountAmount" parentElement:CouponDetailsElement];
                        TBXMLElement *coupListIDElement = [TBXML childElementNamed:@"couponListId" parentElement:CouponDetailsElement];
                        
                        if(coupListIDElement)
                            [dicCoupons setValue:[TBXML textForElement:coupListIDElement] forKey:@"couponListId"];
                        if(viewableonWebElement)
                            [dicCoupons setValue:[TBXML textForElement:viewableonWebElement] forKey:@"viewableOnWeb"];
                        if(couponDiscountAmountElement)
                            [dicCoupons setValue:[TBXML textForElement:couponDiscountAmountElement] forKey:@"couponDiscountAmount"];
                        if(couponIdElement)
                            [dicCoupons setValue:[TBXML textForElement:couponIdElement] forKey:@"couponId"];
                        if(couponNameElement)
                            [dicCoupons setValue:[TBXML textForElement:couponNameElement] forKey:@"couponName"];
                        if(couponStartDateElement)
                            [dicCoupons setValue:[TBXML textForElement:couponStartDateElement] forKey:@"couponStartDate"];
                        if(couponExpireDateElement)
                            [dicCoupons setValue:[TBXML textForElement:couponExpireDateElement] forKey:@"couponExpireDate"];
                        if(couponImagePathElement)
                            [dicCoupons setValue:[TBXML textForElement:couponImagePathElement] forKey:@"couponImagePath"];
                        if(coupDescElement)
                            [dicCoupons setValue:[TBXML textForElement:coupDescElement] forKey:@"coupDesc"];
                        if(distanceElement)
                            [dicCoupons setValue:[TBXML textForElement:distanceElement] forKey:@"distance"];
                        if(claimFlagElement)
                            [dicCoupons setValue:[TBXML textForElement:claimFlagElement] forKey:@"claimFlag"];
                        if(redeemFlagElement)
                            [dicCoupons setValue:[TBXML textForElement:redeemFlagElement] forKey:@"redeemFlag"];
                        if(newFlagElement)
                            [dicCoupons setValue:[TBXML textForElement:newFlagElement] forKey:@"newFlag"];
                        if(usedElement)
                        {
                            // if Only Claim then 1, if redeemed then 2 else 0
                            NSString *val = @"0";
                            if([[TBXML textForElement:claimFlagElement]isEqualToString:@"1"])
                                val = @"1";
                            if([[TBXML textForElement:redeemFlagElement]isEqualToString:@"1"])
                                val = @"2";
                            
                            [dicCoupons setValue:val forKey:@"used"];
                            
                        }
                        if(coupToIssueElement)
                            [dicCoupons setValue:[TBXML textForElement:coupToIssueElement] forKey:@"coupToIssue"];
                        
                        [arrCoupons addObject:dicCoupons];
                        ////[dicCoupons release];
                        
                        
                        // Get the next sibling of the "CouponDetails"
                        CouponDetailsElement = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:CouponDetailsElement];
                        
                    }
                    
                    
                }
                
                //parse categoryID element from catagory info
                TBXMLElement *ccategoryIDElement = [TBXML childElementNamed:@"categoryId" parentElement:CategoryInfoElement];
                //parse categoryName element from catagory info
                TBXMLElement *categoryNameElement = [TBXML childElementNamed:@"categoryName" parentElement:CategoryInfoElement];
                
                // Saved the categoryID & categoryName
                NSMutableDictionary *dicCat = [[NSMutableDictionary alloc]init];
                if(ccategoryIDElement)
                    [dicCat setValue:[TBXML textForElement:ccategoryIDElement] forKey:@"categoryId"];
                if(categoryNameElement)
                    [dicCat setValue:[TBXML textForElement:categoryNameElement] forKey:@"categoryName"];
                
                
                //If category is more the 1 then check the below conditions
                if([arrCatagories count] > 0)
                {
                    // Check if Last category value and current category value is same then append in the same array else add as new
                    
                    BOOL isAdded = NO;
                    for(int i_check = 0; i_check < [arrCatagories count]; i_check++)
                    {
                        if([[[arrCatagories objectAtIndex:i_check]valueForKey:@"categoryId"] isEqualToString:[TBXML textForElement:ccategoryIDElement]])
                        {
                            NSMutableArray *arr = [dicProductsCoupon valueForKey:[TBXML textForElement:ccategoryIDElement]];
                            [arr addObjectsFromArray:arrCoupons];
                            [dicProductsCoupon setObject:arr forKey:[TBXML textForElement:ccategoryIDElement]];
                            isAdded = YES;
                            break;
                        }
                    }
                    if(isAdded == NO)
                    {
                        // Storing Category objects in array
                        [arrCatagories addObject:dicCat];
                        // Storing all coupons object belongs to category
                        [dicProductsCoupon setObject:arrCoupons forKey:[TBXML textForElement:ccategoryIDElement]];
                        
                    }
                }
                else
                {
                    // Storing Category objects in array
                    [arrCatagories addObject:dicCat];
                    // Storing all coupons object belongs to category
                    [dicProductsCoupon setObject:arrCoupons forKey:[TBXML textForElement:ccategoryIDElement]];
                }
                
                // release local objects
                // [dicCat release];
                ////[arrCoupons release];
                
                // Get the next sibling of the "CategoryInfo"
                CategoryInfoElement = [TBXML nextSiblingNamed:@"CategoryInfo" searchFromElement:CategoryInfoElement];
                
            }
            
        }
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
    if([arrCatagories count] > 0)
    {
        [myGalleryTV setHidden:NO];
    }
    else
    {
        visitAllCoupons.font=[UIFont systemFontOfSize:14];
        visitAllCoupons.text = @"There are no coupons by product";
    }
}

// This method will call when tap on the gallery button at the bottom of the screen
- (void)galleryTabClicked
{
    
}

// This method will call when tap on the byCity button at the bottom of the screen
- (void)byCityTabClicked
{
    
}


// This method will call when tap on the Search button at the bottom of the screen
- (void)searchTabClicked
{
    // Update the Tab Image
    [couponGalleryBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    [bycityBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    [searchBtn setImage:[UIImage imageNamed:@"tab_btn_down_srchTxt.png"] forState:UIControlStateNormal];
    [preferanceBtn setImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    
    
    searchView.hidden = NO;
    selectedbusinesscatID = 0;
    selectedbusinesscatRetailerID = 0;
    
    // If product tab is active make product button enable else location button in search view
    if(isProductTabDisplaying)
    {
        isLocationRadioButtonEnable = NO;
        [radioBtn2 setImage:[UIImage imageNamed:@"Login_BlueSolid.png"] forState:UIControlStateNormal];
        [radioBtn1 setImage:[UIImage imageNamed:@"Login_BlueOutline.png"] forState:UIControlStateNormal];
        
        retBtn.hidden = YES;
        retLabel.text = @"";
        
        searchTextField.frame = CGRectMake(catBtn.frame.origin.x, retBtn.frame.origin.y, 231, 31);
        searchTextField.text=@"";
        catLabel.text = @"Category";
        
    }
    else
    {
        isLocationRadioButtonEnable = YES;
        [radioBtn2 setImage:[UIImage imageNamed:@"Login_BlueOutline.png"] forState:UIControlStateNormal];
        [radioBtn1 setImage:[UIImage imageNamed:@"Login_BlueSolid.png"] forState:UIControlStateNormal];
        
        retBtn.hidden=NO;
        searchTextField.frame = CGRectMake(catBtn.frame.origin.x, 165, 231, 31);
        searchTextField.text=@"";
        retLabel.text = @"Retailer";
        catLabel.text = @"Category";
        
        
    }
    
    [self.view bringSubviewToFront:searchView];
    
}


// This method will call when tap on the preferance button at the bottom of the screen
- (void)preferanceTabClicked
{
    iWebrequest = GETUSERCATCOUPON;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
    NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
    
    [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
}



-(void)showSplash
{
    UIViewController *modalViewController = [[UIViewController alloc] init];
    modelView.backgroundColor = [UIColor colorWithRGBInt:0x6fb540] ;
    modalViewController.view = modelView;
    [self presentViewController:modalViewController animated:NO completion:nil];
    [self performSelector:@selector(hideSplash) withObject:nil afterDelay:1.0];
    ReleaseAndNilify(modalViewController);
}

- (void)hideSplash
{
    [[self presentedViewController] dismissViewControllerAnimated:NO completion:nil];
    [defaults setBool:NO forKey:@"myGallerysplash"];
    [defaults setBool:NO forKey:@"CalledFromMainMenu"];
    [self getMuCouponItemsforLocation];
}



-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    searchTextField.text=@"";
    
    if([SharedManager refreshGallery])
    {
        [self refreshAlldatafromUI];
        [myGalleryTV setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    [SharedManager setRefreshGallery:NO];
    
    [myGalleryTV deselectRowAtIndexPath:[myGalleryTV indexPathForSelectedRow] animated:YES];
    [myGalleryTV reloadData];
}

-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}


-(void)viewWillDisappear:(BOOL)animated {
    
    //[self refreshArray];
    [super viewWillDisappear:YES];
}




-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


// No Delete Functionality required in All Coupon


-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}



-(IBAction)radioButtonTapped:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    [self toolbarDoneClicked:nil];
    catLabel.text = @"Category";
    selectedbusinesscatID = 0;
    selectedbusinesscatRetailerID = 0;
    searchTextField.text = @"";
    [searchTextField resignFirstResponder];
    
    if(btn == radioBtn1)
    {
        isLocationRadioButtonEnable = YES;
        retBtn.hidden = NO;
        retLabel.text = @"Retailer";
        searchTextField.frame = CGRectMake(catBtn.frame.origin.x, 165, 231, 31);
        [radioBtn2 setImage:[UIImage imageNamed:@"Login_BlueOutline.png"] forState:UIControlStateNormal];
        [radioBtn1 setImage:[UIImage imageNamed:@"Login_BlueSolid.png"] forState:UIControlStateNormal];
    }
    else if(btn == radioBtn2)
    {
        isLocationRadioButtonEnable = NO;
        retBtn.hidden = YES;
        retLabel.text = @"";
        searchTextField.frame = CGRectMake(catBtn.frame.origin.x, retBtn.frame.origin.y, 231, 31);
        [radioBtn2 setImage:[UIImage imageNamed:@"Login_BlueSolid.png"] forState:UIControlStateNormal];
        [radioBtn1 setImage:[UIImage imageNamed:@"Login_BlueOutline.png"] forState:UIControlStateNormal];
        
    }
    
}



-(IBAction)searchClicked:(id)sender{
    
    [self refreshAlldatafromUIforSearch];
    
}


- (void)refreshAlldatafromUIforSearch
{
    // remove all objects from Products Stored dataset
    [arrCatagories removeAllObjects];
    [dicProductsCoupon removeAllObjects];
    
    // remove all objects from Location Stored dataset
    [dicLocationCupon removeAllObjects];
    [arrLocationCategoriesforRetailers removeAllObjects];
    [arrSectionArrayForRetailers removeAllObjects];
    [arrSectionArrayForRetailers addObject:@"1"];
    lowerLimitvalue = 0;
    //If seach tab is active then hide the search view
    [self toolbarDoneClicked:nil];
    searchView.hidden = YES;
    
    // Set the bottom tab images
    [couponGalleryBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    [bycityBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    [searchBtn setImage:[UIImage imageNamed:@"tab_btn_up_srchTxt.png"] forState:UIControlStateNormal];
    [preferanceBtn setImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    
    if(!isLocationRadioButtonEnable)
    {
        segmentedControl.selectedSegmentIndex= 1;
        myGalleryTV.tag = 0;
        [self getMuCouponItemsforProducts];
    }
    else
    {
        segmentedControl.selectedSegmentIndex=0;
        myGalleryTV.tag = 0;
        [self callLocationbasedCoupons];
    }
}


- (void)refreshAlldatafromUI
{
    // remove all objects from Products Stored dataset
    [arrCatagories removeAllObjects];
    [dicProductsCoupon removeAllObjects];
    
    // remove all objects from Location Stored dataset
    [dicLocationCupon removeAllObjects];
    [arrLocationCategoriesforRetailers removeAllObjects];
    [arrSectionArrayForRetailers removeAllObjects];
    [arrSectionArrayForRetailers addObject:@"1"];
    lowerLimitvalue = 0;
    //If seach tab is active then hide the search view
    [self toolbarDoneClicked:nil];
    searchView.hidden = YES;
    
    // Set the bottom tab images
    [couponGalleryBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    [bycityBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    [searchBtn setImage:[UIImage imageNamed:@"tab_btn_up_srchTxt.png"] forState:UIControlStateNormal];
    [preferanceBtn setImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    
    if(isProductTabDisplaying)
    {
        segmentedControl.selectedSegmentIndex= 1;
        [self getMuCouponItemsforProducts];
    }
    else
    {
        segmentedControl.selectedSegmentIndex=0;
        myGalleryTV.tag = 0;
        [self callLocationbasedCoupons];
    }
}

-(IBAction)selectCategory:(id)sender
{
    selectedbusinesscatID = 0;
    selectedbusinesscatRetailerID = 0;
    isDisplayBusinessCatagory = YES;
    // NSMutableString *urlString = [[BASE_URL mutableCopy] ;
    NSMutableString *reqStr = [[NSMutableString alloc] init];
    NSMutableString *urlStr = [BASE_URL mutableCopy] ;
    if(!isLocationRadioButtonEnable)
    {
        
        [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
        
        if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE]){
            
            [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
             [defaults  valueForKey:KEY_LATITUDE]];
            
        }
        else if ([[defaults  valueForKey:KEYZIPCODE]length]){
            
            [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
        }
        else if ([[defaults  valueForKey:@"couponzipcode"]length]){
            
            [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:@"couponzipcode"]];
        }
        
        if([defaults  valueForKey:@"popCentId"])
            [reqStr appendFormat:@"<popCentId>%@</popCentId>", [defaults  valueForKey:@"popCentId"]];
        
        [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults valueForKey:KEY_HUBCITIID]];
        
        [reqStr appendString:@"</ProductDetailsRequest>"];
        
        
        [urlStr appendString:@"gallery/coupprodcat"];
    }
    else
    {
        [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
        
        if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE]){
            
            [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
             [defaults  valueForKey:KEY_LATITUDE]];
            
        }
        else if ([[defaults  valueForKey:KEYZIPCODE]length]){
            
            [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
        }
        else if ([[defaults  valueForKey:@"couponzipcode"]length]){
            
            [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:@"couponzipcode"]];
        }
        if([defaults  valueForKey:@"popCentId"])
            [reqStr appendFormat:@"<popCentId>%@</popCentId>", [defaults  valueForKey:@"popCentId"]];
        
        [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ProductDetailsRequest>", [defaults valueForKey:KEY_HUBCITIID]];
        
        [urlStr appendString:@"gallery/couplocbuscat"];
        
    }
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlStr withParam:reqStr]];
    
    ReleaseAndNilify(reqStr);
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
    {
        ReleaseAndNilify(responseXml);
        return;
    }
    
    if(arrBusinessCatagoriesID)
    {
        [arrBusinessCatagoriesID removeAllObjects];
        // [arrBusinessCatagoriesID release];
    }
    if(arrBusinessCatagoriesName)
    {
        [arrBusinessCatagoriesName removeAllObjects];
        //[arrBusinessCatagoriesName release];
    }
    
    arrBusinessCatagoriesID = [[NSMutableArray alloc]init];
    arrBusinessCatagoriesName = [[NSMutableArray alloc]init];
    
    
    [self parsecatagoryforsearchresponse:responseXml];
    //[responseXml release];
    if([arrBusinessCatagoriesID count] > 0)
    {
        [catPicker reloadAllComponents];
        catPicker.hidden = NO;
        toolBar.hidden = NO;
    }
}

-(void)parseRetailerforsearchresponse:(NSString *)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *couponDetailElement = [TBXML childElementNamed:@"couponDetail" parentElement:tbXml.rootXMLElement];
    
    
    if (couponDetailElement == nil) {
        TBXMLElement *respTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        if (respTextElement != nil){
            [UtilityManager showAlert:nil msg:[TBXML textForElement:respTextElement]];
            return;
        }
    }
    else
    {
        TBXMLElement *CouponDetailsElement = [TBXML childElementNamed:@"CouponDetails" parentElement:couponDetailElement];
        
        while (CouponDetailsElement != nil)
        {
            TBXMLElement *busCatIdElement = [TBXML childElementNamed:@"retId" parentElement:CouponDetailsElement];
            TBXMLElement *busCatNameElement = [TBXML childElementNamed:@"retName" parentElement:CouponDetailsElement];
            if (busCatIdElement!=nil)
            {
                [arrBusinessCatagoriesRetailerID addObject:[TBXML textForElement:busCatIdElement]];
                [arrBusinessCatagoriesRetailerName addObject:[TBXML textForElement:busCatNameElement]];
            }
            
            CouponDetailsElement = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:CouponDetailsElement];
        }
    }
}

-(void)parsecatagoryforsearchresponse:(NSString *)responseString
{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *CouponsDetailElement = [TBXML childElementNamed:@"couponDetail" parentElement:tbxml.rootXMLElement];
        
        
        TBXMLElement *CouponDetailsElement = [TBXML childElementNamed:@"CouponDetails" parentElement:CouponsDetailElement];
        
        
        if(!isLocationRadioButtonEnable)
        {
            
            while (CouponDetailsElement != nil)
            {
                TBXMLElement *busCatIdElement = [TBXML childElementNamed:@"catId" parentElement:CouponDetailsElement];
                TBXMLElement *busCatNameElement = [TBXML childElementNamed:@"catName" parentElement:CouponDetailsElement];
                if (busCatIdElement!=nil)
                {
                    [arrBusinessCatagoriesID addObject:[TBXML textForElement:busCatIdElement]];
                    [arrBusinessCatagoriesName addObject:[TBXML textForElement:busCatNameElement]];
                }
                
                CouponDetailsElement = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:CouponDetailsElement];
            }
            
        }
        else
        {
            while (CouponDetailsElement != nil)
            {
                TBXMLElement *busCatIdElement = [TBXML childElementNamed:@"busCatId" parentElement:CouponDetailsElement];
                TBXMLElement *busCatNameElement = [TBXML childElementNamed:@"busCatName" parentElement:CouponDetailsElement];
                if (busCatIdElement!=nil)
                {
                    [arrBusinessCatagoriesID addObject:[TBXML textForElement:busCatIdElement]];
                    [arrBusinessCatagoriesName addObject:[TBXML textForElement:busCatNameElement]];
                }
                
                CouponDetailsElement = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:CouponDetailsElement];
            }
        }
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
    
}

-(IBAction)selectRetailer:(id)sender
{
    if(selectedbusinesscatID > 0)
    {
        selectedbusinesscatRetailerID = 0;
        isDisplayBusinessCatagory = NO;
        
        NSMutableString *reqStr = [[NSMutableString alloc] init];
        NSMutableString *urlStr = [BASE_URL mutableCopy] ;
        [reqStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
        
        if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE]){
            
            [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
             [defaults  valueForKey:KEY_LATITUDE]];
            
        }
        else if ([[defaults  valueForKey:KEYZIPCODE]length]){
            
            [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:KEYZIPCODE]];
        }
        else if ([[defaults  valueForKey:@"couponzipcode"]length]){
            
            [reqStr appendFormat:@"<postalcode><![CDATA[%@]]></postalcode>", [defaults  valueForKey:@"couponzipcode"]];
        }
        
        if([defaults  valueForKey:@"popCentId"])
            [reqStr appendFormat:@"<popCentId>%@</popCentId>", [defaults  valueForKey:@"popCentId"]];
        
        [reqStr appendFormat:@"<busCatIds>%d</busCatIds>",selectedbusinesscatID];
        [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ProductDetailsRequest>",[defaults valueForKey:KEY_HUBCITIID]];
        
        [urlStr appendString:@"gallery/retforbuscat"];
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlStr withParam:reqStr]];
        ReleaseAndNilify(reqStr);
        
        if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]){
            ReleaseAndNilify(responseXml);
            return;
        }
        
        if(arrBusinessCatagoriesRetailerID)
        {
            [arrBusinessCatagoriesRetailerID removeAllObjects];
            // [arrBusinessCatagoriesRetailerID release];
        }
        if(arrBusinessCatagoriesRetailerName)
        {
            [arrBusinessCatagoriesRetailerName removeAllObjects];
            //[arrBusinessCatagoriesRetailerName release];
        }
        
        
        arrBusinessCatagoriesRetailerID = [[NSMutableArray alloc]init];
        arrBusinessCatagoriesRetailerName = [[NSMutableArray alloc]init];
        
        [self parseRetailerforsearchresponse:responseXml];
        //[responseXml release];
        if([arrBusinessCatagoriesRetailerID count] > 0)
        {
            [catPicker reloadAllComponents];
            catPicker.hidden = NO;
            toolBar.hidden = NO;
        }
    }
    
}


-(IBAction)toolbarDoneClicked:(id)sender{
    
    catPicker.hidden = YES;
    toolBar.hidden = YES;
}

#pragma mark Picker view methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(isDisplayBusinessCatagory)
        return [arrBusinessCatagoriesID count];
    else
        return [arrBusinessCatagoriesRetailerID count];
    
    return 0;
}

/*- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
 {
 if(isDisplayBusinessCatagory)
 {
 //catLabel.text = [arrBusinessCatagoriesName objectAtIndex:row];
 return [arrBusinessCatagoriesName objectAtIndex:row];
 }
 else
 {
 //retLabel.text = [arrBusinessCatagoriesRetailerName objectAtIndex:row];
 return [arrBusinessCatagoriesRetailerName objectAtIndex:row];
 }
 }*/

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel *label = [[UILabel alloc] init];
    
    if (IOS7){
        label.frame = CGRectMake(20, 0, 220, 40);
        //pickerView.backgroundColor = [UIColor colorWithRGBInt:0xEEEEEE];
        pickerView.layer.borderWidth = 5.0;
        pickerView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    }
    
    else
        label.frame = CGRectMake(0, 0, 200, 40);
    
    [label setBackgroundColor:[UIColor clearColor]];
    [label setNumberOfLines:2];
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    [label setTextAlignment:NSTextAlignmentLeft];
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    
    if(isDisplayBusinessCatagory)
        label.text = [arrBusinessCatagoriesName objectAtIndex:row];
    else
        label.text = [arrBusinessCatagoriesRetailerName objectAtIndex:row];
    
    
    return label;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(isDisplayBusinessCatagory)
    {
        DLog(@"%@",arrBusinessCatagoriesID);
        catLabel.text = [arrBusinessCatagoriesName objectAtIndex:row];
        selectedbusinesscatID = (int)[[arrBusinessCatagoriesID objectAtIndex:row]integerValue];
        DLog(@"%d",selectedbusinesscatID);
    }
    else
    {
        retLabel.text = [arrBusinessCatagoriesRetailerName objectAtIndex:row];
        selectedbusinesscatRetailerID = (int)[[arrBusinessCatagoriesRetailerID objectAtIndex:row]integerValue];
    }
    
}


- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50.0;
}

#pragma mark TextField methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    catPicker.hidden = YES;
    toolBar.hidden = YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)closeSearchView :(id)sender
{
    //If seach tab is active then hide the search view
    [self toolbarDoneClicked:nil];
    searchView.hidden = YES;
    searchTextField.text = 0;
    selectedbusinesscatID = 0;
    selectedbusinesscatRetailerID = 0;
    
    // Set the bottom tab images
    [couponGalleryBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    [bycityBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    [searchBtn setImage:[UIImage imageNamed:@"tab_btn_up_srchTxt.png"] forState:UIControlStateNormal];
    [preferanceBtn setImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}




- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *containerView = [[UIView alloc] init] ; //WithFrame:CGRectMake(10, 0, 320, 65)] ;
    UILabel *headerLabel1 = [[UILabel alloc] init] ;
    CGRect imageRect1 ;
    containerView.frame = CGRectMake(10, 0, 320, 26) ;
    headerLabel1.frame =  CGRectMake(20, 3, 300, 20) ;
    imageRect1 = CGRectMake(0.0f, 0.0f, 320.0f, 26.0f);
    
    if(isProductTabDisplaying)
    {
        if([arrCatagories count] > section)
            headerLabel1.text = [[arrCatagories objectAtIndex:section]valueForKey:@"categoryName"];
        else
            headerLabel1.text = @" ";
        headerLabel1.textColor = [UIColor whiteColor];
        headerLabel1.font = [UIFont boldSystemFontOfSize:14];
        headerLabel1.backgroundColor = [UIColor clearColor];
        
    }
    else
    {
        for (int i =0 ; i < [arrSectionArrayForRetailers count]; i++)
        {
            if (([[arrSectionArrayForRetailers objectAtIndex:i] intValue])-1 == section)
            {
                containerView.frame = CGRectMake(10, 0, 320, 60) ;
                imageRect1 = CGRectMake(0.0f, 30.0f, 320.0f, 30.0f);
                
                UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 300, 20)] ;
                
                NSString *strretName = [[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"retName"];
                NSString *str = [[NSString alloc]initWithString:strretName];
                //[str stringByAppendingString:[NSString stringWithFormat:@"- %@",[[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"address"]]];
                
                headerLabel.text = [NSString stringWithFormat:@"%@",str];
                headerLabel.textColor = [UIColor whiteColor];
                headerLabel.font = [UIFont boldSystemFontOfSize:16];
                headerLabel.backgroundColor = [UIColor clearColor];
                
                CGRect imageRect = CGRectMake(0.0f, 0.0f, 320.0f, 30.0f);
                UIImageView *headerImage = [[UIImageView alloc] initWithFrame:imageRect];
                UIImage *image = [UIImage imageNamed:@"hotDealsBg.png"];
                [headerImage setImage:image];
                headerImage.opaque = YES;
                [headerImage addSubview:headerLabel];
                
                [containerView addSubview:headerImage];
                //  //[headerImage release];
                
                break;
            }
        }
        
        if([arrLocationCategoriesforRetailers count] > section)
            headerLabel1.text = [NSString stringWithFormat:@"%@",[[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"categoryName"]];
        else
            headerLabel1.text = @" ";
        headerLabel1.textColor = [UIColor whiteColor];
        headerLabel1.font = [UIFont boldSystemFontOfSize:14];
        headerLabel1.backgroundColor = [UIColor clearColor];
    }
    
    UIImageView *headerImage1 = [[UIImageView alloc] initWithFrame:imageRect1];
    
    headerImage1.backgroundColor = [UIColor lightGrayColor];
    headerImage1.opaque = YES;
    [headerImage1 addSubview:headerLabel1];
    
    [containerView addSubview:headerImage1];
    ////[headerImage1 release];
    containerView.backgroundColor = [UIColor clearColor];
    return containerView;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isProductTabDisplaying)
    {
        NSMutableDictionary *dicLocal = [arrCatagories objectAtIndex:section];
        if(nextpageFlag == 1 && section == [arrCatagories count]-1)
        {
            int count = (int)[[dicProductsCoupon valueForKey:[dicLocal valueForKey:@"categoryId"]]count];
            lowerLimitvalue = lowerLimitvalue + count;
            return count + 1;
        }
        else
        {
            int count = (int)[[dicProductsCoupon valueForKey:[dicLocal valueForKey:@"categoryId"]]count];
            lowerLimitvalue = lowerLimitvalue + count;
            return count;
        }
    }
    else
    {
        if(nextpageFlag == 1 && section == [arrLocationCategoriesforRetailers count]-1)
        {
            NSMutableString *strId = [[NSMutableString alloc]initWithString:[[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"retId"]];
            [strId appendFormat:@"-%@",[[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"categoryId"]];
            lowerLimitvalue = lowerLimitvalue + (int)[[dicLocationCupon valueForKey:strId]count];
            return [[dicLocationCupon valueForKey:strId]count]+1;
        }
        else
        {
            NSMutableString *strId = [[NSMutableString alloc]initWithString:[[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"retId"]];
            [strId appendFormat:@"-%@",[[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"categoryId"]];
            lowerLimitvalue = lowerLimitvalue + (int)[[dicLocationCupon valueForKey:strId]count];
            return [[dicLocationCupon valueForKey:strId]count];
        }
    }
    return 0;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    lowerLimitvalue = 0;
    if(isProductTabDisplaying)
        return [arrCatagories count];
    else
    {
        return [arrLocationCategoriesforRetailers count];
    }
    return 0;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(isProductTabDisplaying)
    {
        if([arrCatagories count] > 0)
            return [[arrCatagories objectAtIndex:section]valueForKey:@"categoryName"];
    }
    //    else
    //    {
    //        return [[arrLocationCategoriesforRetailers objectAtIndex:section]valueForKey:@"categoryName"];
    //    }
    return NULL;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell ;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    NSMutableDictionary *dicLocal;
    NSString *catId;
    NSMutableArray *arr;
    NSMutableDictionary *dicCoupon;
    //float labelFont = 16.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        
       // labelFont = 20.0;
    }
    if(isProductTabDisplaying && [arrCatagories count])
    {
        if([arrCatagories count] > indexPath.section)
        {
            dicLocal = [arrCatagories objectAtIndex:indexPath.section];
            
            int count = (int)[[dicProductsCoupon valueForKey:[dicLocal valueForKey:@"categoryId"]]count];
            
            if(nextpageFlag == 1 && indexPath.section == [arrCatagories count]-1 && indexPath.row == count)
            {
                //            cell.accessoryType = UITableViewCellAccessoryNone;
                //            CGRect frame;
                //            frame.origin.x = 0;
                //            frame.origin.y = 10;
                //            frame.size.width = SCREEN_WIDTH;
                //            frame.size.height = 24;
                //            UILabel *label = [[[UILabel alloc] initWithFrame:frame] ;
                //            label.font = [UIFont boldSystemFontOfSize:labelFont];
                //            label.textAlignment = NSTextAlignmentCenter;
                //            label.textColor = [UIColor colorWithRGBInt:0x112e72];
                //            label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
                //            label.translatesAutoresizingMaskIntoConstraints = NO;
                //            [cell.contentView addSubview:label];
                //            return cell;
                
                loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                
                loading.center = cell.contentView.center;
                loading.color = [UIColor blackColor];
                
                
                [cell.contentView addSubview:loading];
                [loading startAnimating];
            }
            
            catId = [dicLocal valueForKey:@"categoryId"];
            arr = [dicProductsCoupon objectForKey:catId];
            dicCoupon = [arr objectAtIndex:indexPath.row];
            
            if(dicCoupon && [dicCoupon count]>0)
            {
                //claimFlag
                //@"redeemFlag"
                
                addRemGalley = [UIButton buttonWithType:UIButtonTypeCustom];
                addRemGalley.frame = CGRectMake(7, 13, 34, 34) ;
                [addRemGalley setImage:[UIImage imageNamed:@"WL_addCpn_live.png"] forState:UIControlStateNormal];
                [addRemGalley addTarget:self action:@selector(addRemoveFromGallery:) forControlEvents:UIControlEventTouchUpInside];
                
                if (![[dicCoupon valueForKey:@"used"] isEqualToString:@"0"])
                    [addRemGalley setImage:[UIImage imageNamed:@"WL_addCpn_Off.png"] forState:UIControlStateNormal];
                
                [cell addSubview:addRemGalley];
                
                // Display New Image Icon for the New Hot Deal
                if ([[dicCoupon valueForKey:@"newFlag"]isEqualToString:@"1"])
                {
                    AsyncImageView *asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 35)];
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    [asyncImageView loadImagefrommainBundel:@"new_ribbon_70.png"];
                    [cell addSubview:asyncImageView];
                    // //[asyncImageView release];
                }
                
                if([dicCoupon valueForKey:@"couponName"])
                {
                    UILabel *tx_label = [[UILabel alloc] initWithFrame:CGRectMake(104, 0, 200, 40)];
                    tx_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
                    tx_label.textColor = [UIColor colorWithRGBInt:0x112e72];
                    tx_label.numberOfLines = 2;
                    [tx_label setText:[dicCoupon valueForKey:@"couponName"]];
                    [tx_label setLineBreakMode:NSLineBreakByWordWrapping];
                    [cell addSubview:tx_label];
                    ReleaseAndNilify(tx_label);
                }
                
                if(![[dicCoupon valueForKey:@"couponDiscountAmount"]isEqualToString:@"N/A"])
                {
                    UILabel *detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(104, 40, 200, 20)];
                    detailLabel.backgroundColor = [UIColor clearColor];
                    detailLabel.textColor = [UIColor grayColor];
                    detailLabel.numberOfLines = 1;
                    [detailLabel setText: [NSString stringWithFormat:NSLocalizedString(@"%@ off Expires %@",@"%@ off Expires %@"),
                                           [dicCoupon valueForKey:@"couponDiscountAmount"],
                                           [dicCoupon valueForKey:@"couponExpireDate"]]];
                    
                    
                    detailLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
                    [cell addSubview:detailLabel];
                    ReleaseAndNilify(detailLabel);
                }
                
                // Coupon Image
                if([dicCoupon valueForKey:@"couponImagePath"])
                {
                    NSString *imagePathStr = [dicCoupon valueForKey:@"couponImagePath"];
                    SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(50, 7, 44, 44)];
                    asyncImageView.contentMode = UIViewContentModeScaleToFill;
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    [asyncImageView loadImage:imagePathStr];
                    [cell addSubview:asyncImageView];
                    ReleaseAndNilify(asyncImageView);
                }
                
            }
        }
    }
    else
    {
        if([arrLocationCategoriesforRetailers count]> indexPath.section)
        {
            NSMutableString *strId = [[NSMutableString alloc]initWithString:[[arrLocationCategoriesforRetailers objectAtIndex:indexPath.section]valueForKey:@"retId"]];
            [strId appendFormat:@"-%@",[[arrLocationCategoriesforRetailers objectAtIndex:indexPath.section]valueForKey:@"categoryId"]];
            int count =(int)[[dicLocationCupon valueForKey:strId]count];
            
            if(nextpageFlag == 1 && indexPath.section == [arrLocationCategoriesforRetailers count]-1 && indexPath.row == count)
            {
                //            cell.accessoryType = UITableViewCellAccessoryNone;
                //            CGRect frame;
                //            frame.origin.x = 0;
                //            frame.origin.y = 10;
                //            frame.size.width = SCREEN_WIDTH;
                //            frame.size.height = 24;
                //            UILabel *label = [[[UILabel alloc] initWithFrame:frame] ;
                //            label.font = [UIFont boldSystemFontOfSize:labelFont];
                //            label.textAlignment = NSTextAlignmentCenter;
                //            label.textColor = [UIColor colorWithRGBInt:0x112e72];
                //            label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
                //            label.translatesAutoresizingMaskIntoConstraints = NO;
                //            [cell.contentView addSubview:label];
                //            return cell;
                
                loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                
                loading.center = cell.contentView.center;
                loading.color = [UIColor blackColor];
                
                
                [cell.contentView addSubview:loading];
                cell.userInteractionEnabled = NO;
                [loading startAnimating];
            }
            
            
            arr = [dicLocationCupon valueForKey:strId];
            dicCoupon = [arr objectAtIndex:indexPath.row];
            
            if(dicCoupon && [dicCoupon count]>0)
            {
                //claimFlag
                //@"redeemFlag"
                addRemGalley = [UIButton buttonWithType:UIButtonTypeCustom];
                addRemGalley.frame = CGRectMake(7, 13, 34, 34) ;
                [addRemGalley setImage:[UIImage imageNamed:@"WL_addCpn_live.png"] forState:UIControlStateNormal];
                [addRemGalley addTarget:self action:@selector(addRemoveFromGallery:) forControlEvents:UIControlEventTouchUpInside];
                
                if (![[dicCoupon valueForKey:@"used"] isEqualToString:@"0"])
                    [addRemGalley setImage:[UIImage imageNamed:@"WL_addCpn_Off.png"] forState:UIControlStateNormal];
                [cell addSubview:addRemGalley];
                
                // Display New Image Icon for the New Hot Deal
                if ([[dicCoupon valueForKey:@"newFlag"]isEqualToString:@"1"])
                {
                    SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 35)];
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    [asyncImageView loadImagefrommainBundel:@"new_ribbon_70.png"];
                    [cell addSubview:asyncImageView];
                    ////[asyncImageView release];
                }
                
                if([dicCoupon valueForKey:@"couponName"])
                {
                    UILabel *tx_label = [[UILabel alloc] initWithFrame:CGRectMake(104, 0, 200, 40)];
                    tx_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
                    tx_label.textColor = [UIColor colorWithRGBInt:0x112e72];
                    tx_label.numberOfLines = 2;
                    [tx_label setText:[dicCoupon valueForKey:@"couponName"]];
                    [tx_label setLineBreakMode:NSLineBreakByWordWrapping];
                    [cell addSubview:tx_label];
                    ReleaseAndNilify(tx_label);
                }
                
                if(![[dicCoupon valueForKey:@"couponDiscountAmount"]isEqualToString:@"N/A"])
                {
                    UILabel *detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(104, 40, 200, 20)];
                    detailLabel.backgroundColor = [UIColor clearColor];
                    detailLabel.textColor = [UIColor grayColor];
                    detailLabel.numberOfLines = 1;
                    [detailLabel setText: [NSString stringWithFormat:NSLocalizedString(@"%@ off Expires %@",@"%@ off Expires %@"),
                                           [dicCoupon valueForKey:@"couponDiscountAmount"],
                                           [dicCoupon valueForKey:@"couponExpireDate"]]];
                    
                    
                    detailLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
                    [cell addSubview:detailLabel];
                    ReleaseAndNilify(detailLabel);
                }
                
                // Coupon Image
                if([dicCoupon valueForKey:@"couponImagePath"])
                {
                    NSString *imagePathStr = [dicCoupon valueForKey:@"couponImagePath"];
                    SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(50, 7, 44, 44)];
                    asyncImageView.contentMode = UIViewContentModeScaleToFill;
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    [asyncImageView loadImage:imagePathStr];
                    [cell addSubview:asyncImageView];
                    ReleaseAndNilify(asyncImageView);
                }
                
            }
        }
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // NSString *catId;
   // NSMutableArray *arr;
  //  NSMutableDictionary *dicCoupon;
    if(isProductTabDisplaying)
    {
        NSMutableDictionary *dicLocal = [arrCatagories objectAtIndex:indexPath.section];
        
        int count = (int)[[dicProductsCoupon valueForKey:[dicLocal valueForKey:@"categoryId"]]count];
        
        if(nextpageFlag == 1 && indexPath.section == [arrCatagories count]-1 && indexPath.row == count)
        {
            if (![defaults boolForKey:@"ViewMore"] && lowerLimitvalue!=0) {
                [defaults setBool:YES forKey:@"ViewMore"];
                dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
                dispatch_async(dispatchQueue, ^(void){
                    [self getMuCouponItemsforProducts];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [myGalleryTV reloadData];
                    });
                });
                
            }
            
            return;
        }
        
        //catId = [dicLocal valueForKey:@"categoryId"];
       // arr = [dicProductsCoupon objectForKey:catId];
       // dicCoupon = [arr objectAtIndex:indexPath.row];
    }
    else
    {
        NSMutableString *strId = [[NSMutableString alloc]initWithString:[[arrLocationCategoriesforRetailers objectAtIndex:indexPath.section]valueForKey:@"retId"]];
        [strId appendFormat:@"-%@",[[arrLocationCategoriesforRetailers objectAtIndex:indexPath.section]valueForKey:@"categoryId"]];
        
        int count =(int)[[dicLocationCupon valueForKey:strId]count];
        
        if(nextpageFlag == 1 && indexPath.section == [arrLocationCategoriesforRetailers count]-1 && indexPath.row == count)
        {
            myGalleryTV.tag = 0;
            if (![defaults boolForKey:@"ViewMore"]) {
                [defaults setBool:YES forKey:@"ViewMore"];
                
                dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
                dispatch_async(dispatchQueue, ^(void){
                    
                    [self callLocationbasedCoupons];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [myGalleryTV reloadData];
                    });
                });
                
            }
            
            return;
        }
        
       // arr = [dicLocationCupon valueForKey:strId];
       // dicCoupon = [arr objectAtIndex:indexPath.row];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

#pragma mark -
#pragma mark Table view delegate


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if(!isProductTabDisplaying)
    {
        for (int i =0 ; i < [arrSectionArrayForRetailers count]; i++)
        {
            if (([[arrSectionArrayForRetailers objectAtIndex:i] intValue])-1 == section)
            {
                return 60;
                break;
            }
            
        }
    }
    return 26;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    indexpathRowCouponList = (int)indexPath.row;
    indexpathSectionCouponList = (int)indexPath.section;
    
    
    
    [defaults setBool:NO forKey:@"ViewMore"];
    
    NSString *catId;
    NSMutableArray *arr;
    NSMutableDictionary *dicCoupon;
    if(isProductTabDisplaying)
    {
        NSMutableDictionary *dicLocal = [arrCatagories objectAtIndex:indexPath.section];
        
        int count = (int)[[dicProductsCoupon valueForKey:[dicLocal valueForKey:@"categoryId"]]count];
        
        if(nextpageFlag == 1 && indexPath.section == [arrCatagories count]-1 && indexPath.row == count)
        {
            // [self getMuCouponItemsforProducts];
            return;
        }
        
        catId = [dicLocal valueForKey:@"categoryId"];
        arr = [dicProductsCoupon objectForKey:catId];
        dicCoupon = [arr objectAtIndex:indexPath.row];
    }
    else
    {
        NSMutableString *strId = [[NSMutableString alloc]initWithString:[[arrLocationCategoriesforRetailers objectAtIndex:indexPath.section]valueForKey:@"retId"]];
        [strId appendFormat:@"-%@",[[arrLocationCategoriesforRetailers objectAtIndex:indexPath.section]valueForKey:@"categoryId"]];
        
        int count =(int)[[dicLocationCupon valueForKey:strId]count];
        
        if(nextpageFlag == 1 && indexPath.section == [arrLocationCategoriesforRetailers count]-1 && indexPath.row == count)
        {
            myGalleryTV.tag = 0;
            // [self callLocationbasedCoupons];
            return;
        }
        
        arr = [dicLocationCupon valueForKey:strId];
        dicCoupon = [arr objectAtIndex:indexPath.row];
    }
    
    [defaults setValue:[dicCoupon valueForKey:@"couponId"] forKey:@"couponId"];
    [defaults setValue:[dicCoupon valueForKey:@"couponListId"] forKey:@"couponListId"];
    [defaults  setValue:[dicCoupon valueForKey:@"couponImagePath"] forKey:@"imgPath"];
    
    NewCouponDetailViewController *obj_CouponGalleryDetail = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:obj_CouponGalleryDetail animated:NO];
    // //[obj_CouponGalleryDetail release];
    
    
    [myGalleryTV deselectRowAtIndexPath:[myGalleryTV indexPathForSelectedRow] animated:YES];
}


-(void) addRemoveFromGallery : (UIButton *) sender {
    //NSIndexPath *index = [myGalleryTV indexPathForCell:(UITableViewCell *)[sender superview]];
    //updated for iOS7 bug fixing
    CGPoint location = [sender.superview convertPoint:sender.center toView:myGalleryTV];
    
    
    NSIndexPath *index ;
    index = [myGalleryTV indexPathForRowAtPoint:location];
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    
    
    if(userIdentifier==TRUE)
    {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        
        NSString *catId;
        NSMutableArray *arr;
        //NSMutableDictionary *dictCoupon;
        if(isProductTabDisplaying)
        {
            catId = [[arrCatagories objectAtIndex:index.section]valueForKey:@"categoryId"];
            arr = [dicProductsCoupon objectForKey:catId];
            dictCoupon = [arr objectAtIndex:index.row];
        }
        else
        {
            NSMutableString *strId = [[NSMutableString alloc]initWithString:[[arrLocationCategoriesforRetailers objectAtIndex:index.section]valueForKey:@"retId"]];
            [strId appendFormat:@"-%@",[[arrLocationCategoriesforRetailers objectAtIndex:index.section]valueForKey:@"categoryId"]];
            
            arr = [dicLocationCupon valueForKey:strId];
            dictCoupon = [arr objectAtIndex:index.row];
        }
        
        NSString *str = @"";
        // iWebrequest = addRemoveCoupon;
        NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<CLRDetails><couponId>%@</couponId>",[dictCoupon valueForKey:@"couponId"]];
        [reqStr appendFormat:@"<userId>%@</userId></CLRDetails>",[defaults valueForKey:KEY_USERID]];
        
        if ([[dictCoupon valueForKey:@"used"]isEqualToString:@"0"])
            str = @"add";
        else
            str = @"remove";
        
        [defaults  setValue:VALUE_COUPON forKey:KEY_CLR];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@gallery/%@%@",BASE_URL,str,[defaults valueForKey:KEY_CLR]];
        
        //[ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
        NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:reqStr]];
        ReleaseAndNilify(reqStr);
        
        if ([UtilityManager isNullOrEmptyString:response]) {
            ReleaseAndNilify(response);
            return;
        }
        
        else{
            
            [self parse_addRemoveCoupon:response];
        }
    }
}

-(void)parse_addRemoveCoupon:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    int responseCode = [[TBXML textForElement:responseCodeElement] intValue];
    if (responseCode == 10000) {
        
        if ([[[self.navigationController viewControllers] objectAtIndex:[[self.navigationController viewControllers] count]-2 ] isKindOfClass:[ProductPage class]])
        {
            [defaults  setBool:YES forKey:@"refreshProd"];
        }
        
        if ([[dictCoupon valueForKey:@"used"]isEqualToString:@"0"])
            [dictCoupon setValue:@"1" forKey:@"used"];
        else
            [dictCoupon setValue:@"0" forKey:@"used"];
        
        //else if ([[dicCoupon valueForKey:@"used"]isEqualToString:@"1"])
        //[dicCoupon setValue:@"2" forKey:@"used"];
        
        [SharedManager setRefreshList:YES];
        [myGalleryTV reloadData];
    }
    else
    {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
    }
    ReleaseAndNilify(tbxml);
}

- (void)segmentChanged
{
    [arrSectionArrayForRetailers addObject:@"1"];
    
    lowerLimitvalue = 0;
    //If seach tab is active then hide the search view
    [self toolbarDoneClicked:nil];
    searchView.hidden = YES;
    
    // Set the bottom tab images
    [couponGalleryBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    [bycityBtn setImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    [searchBtn setImage:[UIImage imageNamed:@"tab_btn_up_srchTxt.png"] forState:UIControlStateNormal];
    [preferanceBtn setImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    
    switch(segmentedControl.selectedSegmentIndex)
    {
        case 0:
        {
            isCalledFromSegementChange = YES;
            myGalleryTV.tag = 0;
            [self callLocationbasedCoupons];
            
        }
            break;
        case 1:
        {
            isCalledFromSegementChange = YES;
            myGalleryTV.tag = 0;
            [self getMuCouponItemsforProducts];
        }
            break;
    }
}

-(void)setWhenCalledfromSegementChange
{
    // remove all objects from Products Stored dataset
    [arrCatagories removeAllObjects];
    [dicProductsCoupon removeAllObjects];
    
    // remove all objects from Location Stored dataset
    [dicLocationCupon removeAllObjects];
    [arrLocationCategoriesforRetailers removeAllObjects];
    [arrSectionArrayForRetailers removeAllObjects];
    
    
}

@end



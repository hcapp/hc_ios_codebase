//
//  CouponGalleryDetail.m
//  Scansee
//
//  Created by developer_span on 15/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "CouponDetailViewController.h"
#import "MainMenuViewController.h"
#import "CommonUtility.h"
#import "CurrentSpecials.h"
#import "CurrentSpecialsViewController.h"
#import "AppDelegate.h"
#import "Config.h"
#import "QuartzCore/QuartzCore.h"
#import "QualifyingProductsList.h"
//#import "ShoppingCartViewController.h"
#import "WebBrowserViewController.h"
//#import "WishListHomePage.h"
//#import "AddRemLoyaltyCardViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "LoginViewController.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "ProductPage.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"


#define kAlertViewEmail 10000

@implementation CouponDetailViewController
@synthesize couponId,couponName,couponLongDesc,couponStartDate,couponEndDate,redeem,viewableOnWebsite;
@synthesize displayAddToGallery,anyVC,emailSendingVC;


BOOL shareClikedFlag;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

-(int) checkWebAndViewable
{
    DLog(@"Coupon URL - %@", couponUrl);
    //checking whether the external coupon url has the "www.coupons.com"
    if (couponUrl !=nil && ![couponUrl isEqualToString:@"N/A"]){
        
        if (viewableOnWebsite == FALSE){
            return 0;
        }
        else {
            return 1;
        }
    }
    return 2;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Redeem",@"Redeem");
    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
   
    
    
    
    //customize back button
   
    self.navigationItem.hidesBackButton = YES;
    //[shareButton release];
    // //[back release];
    
    
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0,30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    // //[mainPage release];
    
    
    expiredFlag = NO;
    
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        markUsed.frame = CGRectMake((SCREEN_WIDTH - 250*2 - 7)/2, markUsed.frame.origin.y - 50.0, 250.0, 60.0);
        clipCoupon.frame = CGRectMake(markUsed.frame.origin.x + markUsed.frame.size.width + 7, clipCoupon.frame.origin.y - 50.0, 250.0, 60.0);
        [markUsed setBackgroundImage:[UIImage imageNamed:@"redeemCoupon_iPad.png"] forState:UIControlStateNormal];
        [clipCoupon setBackgroundImage:[UIImage imageNamed:@"claimCoupon_iPad.png"] forState:UIControlStateNormal];
        
        scissorsImageView.frame = CGRectMake(scissorsImageView.frame.origin.x - 10, scissorsImageView.frame.origin.y - 10, scissorsImageView.frame.size.width, scissorsImageView.frame.size.height);
        
        titleLabel.frame = CGRectMake(20, 0, SCREEN_WIDTH - 40, 38);
        titleLabel.font = [UIFont systemFontOfSize:22.0];
        
        couponImageView.frame = CGRectMake(20, 51, 150, 150);
        shadowImageView.frame = CGRectMake(5, 41, 180, 180);
        
        descText.frame = CGRectMake(SCREEN_WIDTH/2, 51, SCREEN_WIDTH/2 - 20, 150);
        descText.font = [UIFont systemFontOfSize:18.0];
        
        validDate.frame = CGRectMake(20, couponImageView.frame.size.height + couponImageView.frame.origin.y + 30, SCREEN_WIDTH - 40, 25);
        validDate.font = [UIFont systemFontOfSize:18.0];
        
        openProductList.frame = CGRectMake(20, validDate.frame.origin.y + validDate.frame.size.height + 30, self.view.frame.size.width - 40.0, 100);
    }
    //    [wishBtn setBackgroundImage:[UIImage imageNamed:@"btn_up_wishlist.png"] forState:UIControlStateNormal];
    //    [wishBtn setBackgroundImage:[UIImage imageNamed:@"btn_down_wishlist.png"] forState:UIControlStateHighlighted];
    //    [wishBtn addTarget:self action:@selector(wishListClicked:) forControlEvents:UIControlEventTouchUpInside];
    //
    //    [couponGalBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_shpngLst.png"] forState:UIControlStateNormal];
    //    [couponGalBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_shpngLst.png"] forState:UIControlStateHighlighted];
    //    [couponGalBtn addTarget:self action:@selector(addToList:) forControlEvents:UIControlEventTouchUpInside];
    //
    //    [prefBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_cpns.png"] forState:UIControlStateNormal];
    //    [prefBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_cpns.png"] forState:UIControlStateHighlighted];
    //    [prefBtn addTarget:self action:@selector(couponPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    prodNameArray = [[NSMutableArray alloc]init];
    productIdArray = [[NSMutableArray alloc] init];
    
    openProductList.layer.borderWidth = 1.0;
    openProductList.layer.borderColor = [UIColor grayColor].CGColor;
    
    [SharedManager setRefreshGallery:NO];
    
    [self request_getcoupondetails];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:NO];
    [self.navigationController setNavigationBarHidden:NO animated:NO];

    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    // Create down menu button
    UIImageView *homeLabel = [self createHomeButtonView];
    
    
    // Create up menu button
    // homeLabel = [self createHomeButtonView];
    
    DWBubbleMenuButton *upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
                                                                                          self.view.frame.size.height - homeLabel.frame.size.height - 10.f,
                                                                                          homeLabel.frame.size.width,
                                                                                          homeLabel.frame.size.height)
                                                            expansionDirection:DirectionUp];
    upMenuView.homeButtonView = homeLabel;
    
    upMenuView.delegate = self;
    
    [upMenuView addButtons:[self createDemoButtonArray]];
    
    [self.view addSubview:upMenuView];
    
    upMenuView.onLogOutPressed =^(){
        
        [self logOutPressed];
    } ;
    
}


- (UIImageView *)createHomeButtonView {
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
    label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}

- (NSArray *)createDemoButtonArray {
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    
    int i = 0;
    for (NSString *title in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:title] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        
        button.clipsToBounds = YES;
        button.tag = i++;
        
        [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
}

- (void)test:(UIButton *)sender {
    
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
    shareClikedFlag=TRUE;
    [self shareClicked];
    
    if (shareClikedFlag==TRUE) {
        
        
        switch (sender.tag) {
            case 0:{//facebook
                anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
                anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"HOT_DEAL_SHARE"];
                [UIView transitionWithView:self.view duration:0.5
                                   options:UIViewAnimationOptionTransitionCurlDown
                                animations:^ { [self.view addSubview:anyVC.view]; }
                                completion:nil];
            }
                break;
            case 1:{//twitter
                if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                    
                    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                    
                    SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                        if (result == SLComposeViewControllerResultCancelled) {
                            DLog(@"Twitter Result: canceled");
                        } else {
                            DLog(@"Twitter Result: sent");
                            NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><fundId>%@</fundId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:FUNDRAISERID]];
                            
                            [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
                        }
                        
                        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                            
                            [controller.view removeFromSuperview];
                        } else
                            [controller dismissViewControllerAnimated:YES completion:Nil];
                    };
                    controller.completionHandler =myBlock;
                    
                    //Adding the Text to the facebook post value from iOS
                    [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    
                    //Adding the URL to the facebook post value from iOS
                    
                    //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                    
                    //Adding the Image to the facebook post value from iOS
                    
                    [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        [self.view addSubview:controller.view];
                        [self.view bringSubviewToFront:controller.view];
                    }else
                        [self presentViewController:controller animated:YES completion:Nil];
                    
                    
                }
                else {
                    [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
                }
            }
                
                
                break;
            case 2:{//text
                
                if ([self hasCellularCoverage])
                {
                    if ([MFMessageComposeViewController canSendText]) {
                        
                        MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
                        [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                        smsComposerController.messageComposeDelegate = self;
                        [self presentViewController:smsComposerController animated:YES completion:nil];
                    }
                }
                else
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];  // //[alert release];
                }
                
            }
                break;
            case 3:{//email
                //[defaults setValue:FUNDRAISER_SHARE forKey:KEY_SHARE_TYPE];
                emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
                emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"COUPON_SHARE"];
                [defaults setValue:couponId forKey:@"couponID"];
                __typeof(self) __weak  obj = self;
                emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                    
                    [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
                } ;
                
                [emailSendingVC loadMail];
            }
                break;
                //            case 4:
                //                [actionSheet dismissWithClickedButtonIndex:4 animated:YES];
                //                break;
            default:
                shareClikedFlag=FALSE;
                [defaults setBool:NO forKey:BottomButton];
                break;
        }
    }
    else
    {
        switch (sender.tag) {
                //            case 4:
                //                [actionSheet dismissWithClickedButtonIndex:4 animated:YES];
                //                break;
                
            default:
                break;
        }
        
    }
    
}

- (UIButton *)createButtonWithName:(NSString *)imageName {
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button sizeToFit];
    
    [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}



- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView{
    
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}


// Request to get the details for Coupon
-(void)request_getcoupondetails
{
    
    iWebRequestState = GETCOUPONDETAILS;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<couponId>%@</couponId>",[defaults valueForKey:@"couponId"]];
    
    //For user tracking
    if ([defaults valueForKey:@"couponListId"])
        [requestStr appendFormat:@"<couponListId>%@</couponListId>",[defaults valueForKey:@"couponListId"]];
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getcoupondetails",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    ////[requestStr release];
    
}



-(void)parse_getcoupondetails:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *viewOnWebElement = [TBXML childElementNamed:@"viewOnWeb" parentElement:tbxml.rootXMLElement];
        TBXMLElement *usedFlagElement = [TBXML childElementNamed:@"usedFlag" parentElement:tbxml.rootXMLElement];
        TBXMLElement *couponURLElement = [TBXML childElementNamed:@"couponURL" parentElement:tbxml.rootXMLElement];
        TBXMLElement *couponIdElement = [TBXML childElementNamed:@"couponId" parentElement:tbxml.rootXMLElement];
        TBXMLElement *couponNameElement = [TBXML childElementNamed:@"couponName" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *couponImagePathElement = [TBXML childElementNamed:@"couponImagePath" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *couponLongDescriptionElement = [TBXML childElementNamed:@"longDescription" parentElement:tbxml.rootXMLElement];
        TBXMLElement *couponStartDateElement = [TBXML childElementNamed:@"couponStartDate" parentElement:tbxml.rootXMLElement];
        TBXMLElement *couponExpireDateElement = [TBXML childElementNamed:@"couponExpireDate" parentElement:tbxml.rootXMLElement];
        
        TBXMLElement *expireFlagElement = [TBXML childElementNamed:@"expireFlag" parentElement:tbxml.rootXMLElement];
        
        if (expireFlagElement!=nil)
            expiredFlag = [[TBXML textForElement:expireFlagElement]boolValue];
        else
            expiredFlag = NO;
        
        TBXMLElement *prodFlagElement = [TBXML childElementNamed:@"prodFlag" parentElement:tbxml.rootXMLElement];
        TBXMLElement *locatnFlagElement = [TBXML childElementNamed:@"locatnFlag" parentElement:tbxml.rootXMLElement];
        TBXMLElement *termAndConditionsElement = [TBXML childElementNamed:@"termAndConditions" parentElement:tbxml.rootXMLElement];
        
        if(viewOnWebElement)
            viewableOnWebsite = [[TBXML textForElement:viewOnWebElement]boolValue];
        
        if (couponURLElement!=nil) {
            
            couponUrl = [[TBXML textForElement:couponURLElement]copy];
        }
        
        if(prodFlagElement)
            isQProductsAvailable = [[TBXML textForElement:prodFlagElement]boolValue];
        
        if(locatnFlagElement)
            isQLocationAvailable = [[TBXML textForElement:locatnFlagElement]boolValue];
        
        
        if(couponIdElement)
            couponId = [[NSString alloc]initWithString:[TBXML textForElement:couponIdElement]];
        
        if(couponNameElement)
            couponName = [[NSString alloc]initWithString:[TBXML textForElement:couponNameElement]];
        
        if(couponImagePathElement)
            couponImagePath = [[NSString alloc]initWithString:[TBXML textForElement:couponImagePathElement]];
        
        if(couponLongDescriptionElement)
            couponLongDesc = [[NSString alloc]initWithString:[TBXML textForElement:couponLongDescriptionElement]];
        
        if(couponStartDateElement)
            couponStartDate = [[NSString alloc]initWithString:[TBXML textForElement:couponStartDateElement]];
        
        if(couponExpireDateElement)
            couponEndDate = [[NSString alloc]initWithString:[TBXML textForElement:couponExpireDateElement]];
        
        if(usedFlagElement)
            couponUsedVal = (int)[[[NSString alloc]initWithString:[TBXML textForElement:usedFlagElement]]integerValue];
        
        if (termAndConditionsElement)
            termsOfUse = [[TBXML textForElement:termAndConditionsElement]copy];
        else
            termsOfUse = @"";
        
        
        [self updateData];
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
}

// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case GETCOUPONDETAILS:
            [self parse_getcoupondetails:response];
            break;
        case addCoupon:
            [self parse_addCoupon:response];
            break;
            
        case userredeemcoupon:
            [self parse_userredeemcoupon:response];
            break;
        default:
            break;
    }
    
}


-(void)popBackToPreviousPage
{
    if (isFromProductPage) {
        //isFromProductPage = NO;
        [self.navigationController popViewControllerAnimated:NO];
    }
    else if (fromCurrentSpecials) {
        fromCurrentSpecials = FALSE;
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        for (UIViewController *aViewController in allViewControllers) {
            if ([aViewController isKindOfClass:[CurrentSpecialsViewController class]]) {
                [self.navigationController popToViewController:aViewController animated:NO];
            }
        }
    }
    else{
        
        if ([defaults boolForKey:@"detailsPage"]) {
            
            [defaults setBool:NO forKey:@"detailsPage"];
            [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:NO];
            
        }
        
    }
}
-(void)updateData
{
    float titleLabelFont = 16.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        titleLabelFont = 21.0;
    }
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.numberOfLines = 2;
    [titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    titleLabel.font = [UIFont boldSystemFontOfSize:titleLabelFont];
    if(couponName)
        titleLabel.text = [NSString stringWithFormat:@"%@",[couponName copy]];
    
    if(couponImagePath)
    {
        SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(15, 51, 128, 128)];
        asyncImageView.backgroundColor = [UIColor clearColor];
        [asyncImageView loadImage:couponImagePath];
        // asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
        asyncImageView.layer.cornerRadius = 10.0;
        asyncImageView.layer.masksToBounds = YES;
        [self.view addSubview:asyncImageView];
        // //[asyncImageView release];
    }
    
    if(couponLongDesc)
        descText.text = couponLongDesc;
    
    if(couponStartDate)
        validDate.text =[NSString stringWithFormat:NSLocalizedString(@"Valid From %@ to %@",@"Valid From %@ to %@"), couponStartDate, couponEndDate];
    
    
    if (expiredFlag){ //if expired, then disable both claim and redeem
        
        markUsed.enabled = NO;
        clipCoupon.enabled = NO;
        
    }
    
    else { //if not expired, then check for usedFlag
        
        switch (couponUsedVal) {
                
            case 0://yet to claim
                markUsed.enabled = NO;
                clipCoupon.enabled = YES;
                break;
                
            case 1://claimed but not used
                markUsed.enabled = YES;
                clipCoupon.enabled = NO;
                break;
                
            case 2://claimed and used
                markUsed.enabled = NO;
                clipCoupon.enabled = NO;
                break;
                
            default:
                markUsed.enabled = NO;
                clipCoupon.enabled = NO;
                break;
        }
    }
    
    [openProductList reloadData];
    if ([termsOfUse length] && [self checkWebAndViewable] != 2)
        openProductList.frame = CGRectMake(openProductList.frame.origin.x, openProductList.frame.origin.y, openProductList.frame.size.width, 36*3);
    else
        openProductList.frame = CGRectMake(openProductList.frame.origin.x, openProductList.frame.origin.y, openProductList.frame.size.width, 36*2);
    
}



-(void)returnToMainPage:(id)sender
{
    [defaults setBool:NO forKey: @"detailsPage"];
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(isQLocationAvailable || isQProductsAvailable)
    {
        if ([termsOfUse length] && [self checkWebAndViewable] != 2)
            return 3;
        else if([self checkWebAndViewable] != 2)
            return 2;
        else if ([termsOfUse length])
            return 2;
        else
            return 1;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float textlabelFont = 15.0, textlabel2Font = 13.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        textlabelFont = 20.0;
        textlabel2Font = 18.0;
    }
    if (indexPath.row == 0)
    {
        static NSString *CellIdentifier = @"cell";
        UITableViewCell *cell;
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        if(isQLocationAvailable && isQProductsAvailable)
            cell.textLabel.text = NSLocalizedString(@"Qualifying Products & Locations",@"Qualifying Products & Locations");
        
        else if(isQLocationAvailable)
            cell.textLabel.text = NSLocalizedString(@"Qualifying Locations",@"Qualifying Locations");
        
        else
            cell.textLabel.text = NSLocalizedString(@"Qualifying Products",@"Qualifying Products");
        
        cell.textLabel.font = [UIFont systemFontOfSize:textlabelFont];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    else {
        static NSString *CellIdentifier = @"cell";
        UITableViewCell *cell;
        
        if ([termsOfUse length] && [self checkWebAndViewable] != 2)
        {
            if(indexPath.row==1)
            {
                cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:textlabel2Font];
                cell.textLabel.text = NSLocalizedString(@"This is a web coupon. Click here to view.",@"This is a web coupon. Click here to view.");
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                return cell;
            }
            if(indexPath.row==2)
            {
                cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:textlabel2Font];
                cell.textLabel.text = NSLocalizedString(@"Terms and Conditions",@"Terms and Conditions");
                cell.accessoryType = UITableViewCellAccessoryNone;
                return cell;
            }
        }
        else if([termsOfUse length])
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:textlabel2Font];
            cell.textLabel.text = NSLocalizedString(@"Terms and Conditions",@"Terms and Conditions");
            cell.accessoryType = UITableViewCellAccessoryNone;
            return  cell;
        }
        else
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:textlabel2Font];
            cell.textLabel.text = NSLocalizedString(@"This is a web coupon. Click here to view.",@"This is a web coupon. Click here to view.");
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return cell;
        }
        
        
    }

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    float labelFont = 14.0, termsNConditionTextFont = 13.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        labelFont = 19.0;
        termsNConditionTextFont = 18.0;
    }
    if (indexPath.row == 0) {
        
        DLog(@"Going To Products");
        QualifyingProductsList *couponProductListV = [[QualifyingProductsList alloc]initWithNibName:@"QualifyingProductsList" bundle:[NSBundle mainBundle]];
        NSString *str;
        
        str = [NSString stringWithFormat:@"<couponId>%@</couponId>",couponId];
        
        
        [defaults setObject:str forKey:@"CouponRequest"];
        
        if(isQLocationAvailable && isQProductsAvailable)
            [couponProductListV setDisplayProductandLoc:2];
        else if(isQLocationAvailable)
            [couponProductListV setDisplayProductandLoc:1];
        else
            [couponProductListV setDisplayProductandLoc:0];
        
        
        [self.navigationController pushViewController:couponProductListV animated:NO];
        //[couponProductListV release];
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    }
    else {
        
        if ([termsOfUse length] && [self checkWebAndViewable] != 2)
        {
            if(indexPath.row==1)
            {
                if ([self checkWebAndViewable] == 1) {
                    [defaults setValue:couponUrl forKey:KEY_URL];
                    [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                    
                    WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:urlDetail animated:NO];
                    ////[urlDetail release];
                    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
                }
                else {
                    UIAlertController * alert;
                    alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"The provider of this coupon only allows this coupon to be printed through the ScanSee website. Using a non-mobile device, please visit http://www.scansee.com and view your My Coupons section to view or print",@"The provider of this coupon only allows this coupon to be printed through the ScanSee website. Using a non-mobile device, please visit http://www.scansee.com and view your My Coupons section to view or print") message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleCancel
                                         handler:nil];
                    [alert addAction:ok];
                    UIAlertAction* sendEmail = [UIAlertAction
                                             actionWithTitle:@"Email Coupon"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action){
                                                [self sendEmail];
                                             }];
                    [alert addAction:sendEmail];
                    [self presentViewController:alert animated:YES completion:nil];
                    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
                }
                
            }
            if(indexPath.row==2)
            {
                termsNCondition = [[UIView alloc]init];
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    
                    termsNCondition.frame = CGRectMake(10, 186, SCREEN_WIDTH - 20, 228);// width 301
                } else {
                    
                    if (IS_IPHONE5)
                        termsNCondition.frame = CGRectMake(10, 186, SCREEN_WIDTH - 20, 228);// width 301
                    else
                        termsNCondition.frame = CGRectMake(10, 186, SCREEN_WIDTH - 20, 148);// width 301
                }
                
                [termsNCondition setBackgroundColor:[UIColor colorWithRGBInt:0xE6E6E6]];
                termsNCondition.layer.borderWidth = 2.0;
                termsNCondition.layer.borderColor = [UIColor grayColor].CGColor;
                termsNCondition.layer.cornerRadius = 8.0;
                termsNCondition.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
                
                UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 6, SCREEN_WIDTH - 160, 20)];//WIDTH 160
                label.backgroundColor = [UIColor clearColor];
                label.text = @"Terms and Conditions:";
                label.font = [UIFont boldSystemFontOfSize:labelFont];
                [termsNCondition addSubview:label];
                // //[label release];
                
                UITextView *termsNConditionText = [[UITextView alloc]initWithFrame:CGRectMake(4, 25, SCREEN_WIDTH - 34, termsNCondition.frame.size.height-28)];//width 286
                termsNConditionText.backgroundColor = [UIColor clearColor];
                termsNConditionText.editable = NO;
                termsNConditionText.text = termsOfUse;
                termsNConditionText.font = [UIFont systemFontOfSize:termsNConditionTextFont];
                [termsNCondition addSubview:termsNConditionText];
                // [termsNConditionText release];
                
                UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                closeBtn.frame = CGRectMake(SCREEN_WIDTH - 44, 6, 20, 20);//X 276
                [closeBtn setImage:[UIImage imageNamed:@"Cpns_srch_close.png"] forState:UIControlStateNormal];
                [closeBtn addTarget:self action:@selector(closeTermsNCondition) forControlEvents:UIControlEventTouchUpInside];
                
                [termsNCondition addSubview:closeBtn];
                [self.view addSubview:termsNCondition];
                [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
                
            }
            
        }
        else if([termsOfUse length])
        {
            termsNCondition = [[UIView alloc]init];
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                termsNCondition.frame = CGRectMake(10, 186, SCREEN_WIDTH - 20, 228);// width 301
            } else {
                
                if (IS_IPHONE5)
                    termsNCondition.frame = CGRectMake(10, 186, SCREEN_WIDTH - 20, 228);//width 301
                else
                    termsNCondition.frame = CGRectMake(10, 186, SCREEN_WIDTH - 20, 148);//width 301
            }
            
            [termsNCondition setBackgroundColor:[UIColor colorWithRGBInt:0xE6E6E6]];
            termsNCondition.layer.borderWidth = 2.0;
            termsNCondition.layer.borderColor = [UIColor grayColor].CGColor;
            termsNCondition.layer.cornerRadius = 8.0;
            termsNCondition.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
            
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 6, SCREEN_WIDTH - 160, 20)];//width 160
            label.backgroundColor = [UIColor clearColor];
            label.text = @"Terms and Conditions:";
            label.font = [UIFont boldSystemFontOfSize:labelFont];
            [termsNCondition addSubview:label];
            //[label release];
            
            UITextView *termsNConditionText = [[UITextView alloc]initWithFrame:CGRectMake(4, 25, SCREEN_WIDTH - 34, termsNCondition.frame.size.height-28)];//width 286
            termsNConditionText.backgroundColor = [UIColor clearColor];
            termsNConditionText.editable = NO;
            termsNConditionText.text = termsOfUse;
            termsNConditionText.font = [UIFont systemFontOfSize:termsNConditionTextFont];
            [termsNCondition addSubview:termsNConditionText];
            //[termsNConditionText release];
            
            UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            closeBtn.frame = CGRectMake(SCREEN_WIDTH - 44, 6, 20, 20);//X value 276
            [closeBtn setImage:[UIImage imageNamed:@"Cpns_srch_close.png"] forState:UIControlStateNormal];
            [closeBtn addTarget:self action:@selector(closeTermsNCondition) forControlEvents:UIControlEventTouchUpInside];
            
            [termsNCondition addSubview:closeBtn];
            [self.view addSubview:termsNCondition];
            [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
            
        }
        else if([self checkWebAndViewable] != 2)
        {
            if ([self checkWebAndViewable] == 1) {
                [defaults setValue:couponUrl forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                ////[urlDetail release];
                [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
            }
            else {
                UIAlertController * alert;
                
                alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"The provider of this coupon only allows this coupon to be printed through the ScanSee website. Using a non-mobile device, please visit http://www.scansee.com and view your My Coupons section to view or print",@"The provider of this coupon only allows this coupon to be printed through the ScanSee website. Using a non-mobile device, please visit http://www.scansee.com and view your My Coupons section to view or print") message:nil preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleCancel
                                     handler:nil];
                [alert addAction:ok];
                UIAlertAction* sendEmail = [UIAlertAction
                                         actionWithTitle:@"Email Coupon"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [self sendEmail];
                                         }];
                [alert addAction:sendEmail];
                [self presentViewController:alert animated:YES completion:nil];
                [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
            }
            
        }
        
    }
}

-(void)closeTermsNCondition{
    
    termsNCondition.hidden = YES;
    
}

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

-(void)sendEmail
{
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
        //mailViewController.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
        //forKey:UITextAttributeTextColor];
        [mailViewController setSubject:[NSString stringWithFormat:NSLocalizedString(@"%@ - by ScanSee",@"%@ - by ScanSee"), couponName]];
        [mailViewController setMessageBody:[NSString stringWithFormat:@"%@ \n\n%@", couponLongDesc, couponUrl] isHTML:NO];
        
        [self presentViewController:mailViewController animated:YES completion:nil];
        //[mailViewController release];
        
    }
    
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
    }
    
}

-(IBAction)wishListClicked:(id)sender{
    
}

-(IBAction)addToList:(id)sender{
    
    
}

-(IBAction)rateShareClicked:(id)sender{
    
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        UIActionSheet *msg;
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            
            msg = [[UIActionSheet alloc]
                   initWithTitle:NSLocalizedString(@"Share Via",@"Share Via")
                   delegate:self
                   cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel")  destructiveButtonTitle:nil
                   otherButtonTitles:@"Facebook",@"Twitter",@"Text",@"Email",NSLocalizedString(@"Cancel",@"Cancel"), nil]
            ;
        } else {
            
            msg = [[UIActionSheet alloc]
                   initWithTitle:NSLocalizedString(@"Share Via",@"Share Via")
                   delegate:self
                   cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel")  destructiveButtonTitle:nil
                   otherButtonTitles:@"Facebook",@"Twitter",@"Text",@"Email", nil]
            ;
        }
        [msg showInView:self.view];
    }
    
}

//- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
//    float currentViewFont = 15.0;
//    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
//        currentViewFont = 20.0;
//    }
//    for (UIView *_currentView in actionSheet.subviews) {
//        if ([_currentView isKindOfClass:[UILabel class]]) {
//            [((UILabel *)_currentView) setFont:[UIFont boldSystemFontOfSize:currentViewFont]];
//        }
//    }
//}

//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    switch (buttonIndex) {
//        case 4:
//            [actionSheet dismissWithClickedButtonIndex:4 animated:YES];
//            break;
//
//        default:
//            break;
//    }
//}



- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

//- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
//{
//	[self dismissViewControllerAnimated:YES completion:nil];
//
//    if (result == MessageComposeResultSent) {
//
//        //For User Tracking
//        NSMutableString *reqStr = [[NSMutableString alloc] init];
//        [reqStr appendFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam>",[defaults valueForKey:KEY_MAINMENUID]];
//
//        [reqStr appendFormat:@"<coupId>%@</coupId></UserTrackingData>", couponId];
//
//
//        NSString *urlString = [NSString stringWithFormat:@"%@ratereview/updatesharetype",BASE_URL];
//         DLog(@"%@",urlString);
//        NSString *response = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
//        DLog(@"%@",response);
//        ReleaseAndNilify(response);
//        [reqStr release];
//        //For User Tracking
//    }
//}

//add to gallery
-(IBAction)addToGallery:(id)sender
{
    if(userIdentifier==TRUE)
    {
        [NewsUtility signupPopUp:self];
        
    }
    else
    {
        iWebRequestState = addCoupon;
        NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<CLRDetails><couponId>%@</couponId>",couponId];
        [reqStr appendFormat:@"<userId>%@</userId></CLRDetails>",[defaults valueForKey:KEY_USERID]];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@gallery/addCoupon",BASE_URL];
        
        [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
        ReleaseAndNilify(reqStr);
    }
}

-(void)parse_addCoupon:(NSString*)response{
    
    /*   <response>
     <responseCode>10000</responseCode>
     <responseText>Already Redeemed.</responseText>
     </response>*/
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]){
        markUsed.enabled = YES;
        clipCoupon.enabled = NO;
        [SharedManager setRefreshGallery:YES];
    }
    
    else{
        [SharedManager setRefreshGallery:NO];
        [UtilityManager showAlert:@"Claim" msg:[TBXML textForElement:responseTextElement]];
        return;
    }
}

//redeem the CLR
-(IBAction)redeemNowClicked:(id)sender
{
    /*<CouponDetails>
     <userId>3</userId>
     <couponId>1</couponId>
     </CouponDetails>*/
    if(userIdentifier==TRUE)
    {
        [NewsUtility signupPopUp:self];
        
    }
    else
    {
        iWebRequestState = userredeemcoupon;
        NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<CouponDetails><couponId>%@</couponId>",couponId];
        [reqStr appendFormat:@"<userId>%@</userId></CouponDetails>",[defaults valueForKey:KEY_USERID]];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@gallery/userredeemcoupon",BASE_URL];
        
        [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
        ReleaseAndNilify(reqStr);
    }
}

-(void)parse_userredeemcoupon:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseTextElement!=nil && [[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]){
        
        markUsed.enabled = NO;
        [SharedManager setRefreshGallery:YES];
        [UtilityManager showAlert:@"Redeem" msg:[TBXML textForElement:responseTextElement]];
        [UIView animateWithDuration:0.5
                         animations:^{
                             
                             [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.navigationController.view cache:NO];
                             
                         }
                         completion:^(BOOL finished){
                         }];
        
        
        [self.navigationController popViewControllerAnimated:NO];
    }
    
    else{
        [SharedManager setRefreshGallery:NO];
        [UtilityManager showAlert:@"Redeem" msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark share coupon methods

-(void)shareClicked
{
    //    iWebRequestState = COUPON_SHARE;
    
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    
    
    [reqStr appendFormat:@"<ShareProductInfo><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [reqStr appendFormat:@"<couponId>%@</couponId>",couponId];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharecoupon",BASE_URL];
    
    //    [ConnectionManager establishConnectionFor:reqStr base:urlString withDelegate:self];
    //    [HubCitiAppDelegate removeActivityIndicator];
    
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    
    [self parse_shareFundraiser:response];
    //[reqStr release];
    
}

#pragma mark Share Coupon

-(void)shareDetails{
    
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        shareClikedFlag=TRUE;
        [self shareClicked];
        
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        // [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        UIActionSheet *msg;
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            
            msg = [[UIActionSheet alloc]
                   initWithTitle:NSLocalizedString(@"Share Coupons Via",@"Share Coupons Via")
                   delegate:self
                   cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel")  destructiveButtonTitle:nil
                   otherButtonTitles:@"Facebook",@"Twitter",@"Text",@"Email",NSLocalizedString(@"Cancel",@"Cancel"), nil]
            ;
        } else {
            
            msg = [[UIActionSheet alloc]
                   initWithTitle:NSLocalizedString(@"Share Coupons Via",@"Share Coupons Via")
                   delegate:self
                   cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel")  destructiveButtonTitle:nil
                   otherButtonTitles:@"Facebook",@"Twitter",@"Text",@"Email", nil]
            ;
        }
        [msg showInView:self.view];
    }
    
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
    
    if (shareClikedFlag==TRUE) {
        for (UIView *_currentView in actionSheet.subviews) {
            if ([_currentView isKindOfClass:[UILabel class]]) {
                [((UILabel *)_currentView) setFont:[UIFont boldSystemFontOfSize:15.f]];
            }
        }
    }
    else
    {
        float currentViewFont = 15.0;
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            currentViewFont = 20.0;
        }
        for (UIView *_currentView in actionSheet.subviews) {
            if ([_currentView isKindOfClass:[UILabel class]]) {
                [((UILabel *)_currentView) setFont:[UIFont boldSystemFontOfSize:currentViewFont]];
            }
        }
    }
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (shareClikedFlag==TRUE) {
        
        
        switch (buttonIndex) {
            case 0:{//facebook
                anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
                anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"HOT_DEAL_SHARE"];
                [UIView transitionWithView:self.view duration:0.5
                                   options:UIViewAnimationOptionTransitionCurlDown
                                animations:^ { [self.view addSubview:anyVC.view]; }
                                completion:nil];
            }
                break;
            case 1:{//twitter
                if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                    
                    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                    
                    SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                        if (result == SLComposeViewControllerResultCancelled) {
                            DLog(@"Twitter Result: canceled");
                        } else {
                            DLog(@"Twitter Result: sent");
                            NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><fundId>%@</fundId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:FUNDRAISERID]];
                            
                            [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
                        }
                        
                        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                            
                            [controller.view removeFromSuperview];
                        } else
                            [controller dismissViewControllerAnimated:YES completion:Nil];
                    };
                    controller.completionHandler =myBlock;
                    
                    //Adding the Text to the facebook post value from iOS
                    [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    
                    //Adding the URL to the facebook post value from iOS
                    
                    //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                    
                    //Adding the Image to the facebook post value from iOS
                    
                    [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        [self.view addSubview:controller.view];
                        [self.view bringSubviewToFront:controller.view];
                    }else
                        [self presentViewController:controller animated:YES completion:Nil];
                    
                    
                }
                else {
                    [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
                }
            }
                
                
                break;
            case 2:{//text
                
                if ([self hasCellularCoverage])
                {
                    if ([MFMessageComposeViewController canSendText]) {
                        
                        MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
                        [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                        smsComposerController.messageComposeDelegate = self;
                        [self presentViewController:smsComposerController animated:YES completion:nil];
                    }
                }
                else
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
                }
                
            }
                break;
            case 3:{//email
                //[defaults setValue:FUNDRAISER_SHARE forKey:KEY_SHARE_TYPE];
                emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
                emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"COUPON_SHARE"];
                [defaults setValue:couponId forKey:@"couponID"];
                __typeof(self) __weak  obj = self;
                emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                    
                    [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
                } ;
                
                [emailSendingVC loadMail];
            }
                break;
            case 4:
                [actionSheet dismissWithClickedButtonIndex:4 animated:YES];
                break;
            default:
                shareClikedFlag=FALSE;
                [defaults setBool:NO forKey:BottomButton];
                break;
        }
    }
    else
    {
        switch (buttonIndex) {
            case 4:
                [actionSheet dismissWithClickedButtonIndex:4 animated:YES];
                break;
                
            default:
                break;
        }
        
    }
}



-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    if (shareClikedFlag==TRUE) {
        switch (result) {
            case MessageComposeResultCancelled:
                
                break;
                
            case MessageComposeResultSent:{
                [UtilityManager showAlert:nil msg:@"Message Sent"];
                
                NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><fundId>%@</fundId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:FUNDRAISERID]];
                
                [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
            }
                break;
            case MessageComposeResultFailed:{
                [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
            }
                break;
                
            default:
                break;
        }
        
        [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        
        if (result == MessageComposeResultSent) {
            
            //For User Tracking
            NSMutableString *reqStr = [[NSMutableString alloc] init];
            [reqStr appendFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam>",[defaults valueForKey:KEY_MAINMENUID]];
            
            [reqStr appendFormat:@"<coupId>%@</coupId></UserTrackingData>", couponId];
            
            
            NSString *urlString = [NSString stringWithFormat:@"%@ratereview/updatesharetype",BASE_URL];
            DLog(@"%@",urlString);
            NSString *response = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
            DLog(@"%@",response);
            ReleaseAndNilify(response);
            //[reqStr release];
            //For User Tracking
        }
    }
    
    
    
}


#pragma mark sender user tracking
-(void)userTracking:(NSString*)xmlStr
{
    
    NSMutableString *urlStr = [[NSMutableString alloc]init];
    [urlStr appendFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    DLog(@"%@",urlStr);
    NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:xmlStr]];
    DLog(@"%@",response);
    // [xmlStr release];
}



-(void)parse_shareFundraiser:(NSString*)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        
        TBXMLElement *shareText=[TBXML childElementNamed:@"couponName" parentElement:tbXml.rootXMLElement];
        TBXMLElement *qrURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *hotDealImagePath = [TBXML childElementNamed:@"couponImagePath" parentElement:tbXml.rootXMLElement];
        
        if (qrURL!=nil) {
            [defaults setValue:[TBXML textForElement:qrURL] forKey:KEY_HYPERLINK];
            NSLog(@"The link to be shared is %@",KEY_HYPERLINK);
        }
        
        if (shareText!=nil) {
            NSMutableString *msgString = [[NSMutableString alloc]initWithString:@"I found this Hot Deal @HubCiti and thought you might be interested:"];
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [msgString appendFormat:@"%@",shareString];
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            NSLog(@"The share message value is %@",KEY_SHAREMSG);
        }
        
        if (hotDealImagePath!=nil) {
            [defaults setObject:[TBXML textForElement:hotDealImagePath] forKey:KEY_PRODUCTIMGPATH];
            NSLog(@"The product image path is %@",KEY_PRODUCTIMGPATH);
        }
        
        
        
        
        
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            return;
        }
        
    }
}



@end

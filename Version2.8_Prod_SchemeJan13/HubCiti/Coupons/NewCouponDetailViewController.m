//
//  NewCouponDetailViewController.m
//  HubCiti
//
//  Created by Nikitha on 11/22/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "NewCouponDetailViewController.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "MainMenuViewController.h"
#import "BlockViewController.h"
#import "DWBubbleMenuButton.h"
#import "AnyViewController.h"
#import "ProductPage.h"
#import "CurrentSpecials.h"
#import "CurrentSpecialsViewController.h"
#import "QualifyingProductsList.h"
#import "CouponDetailResponse.h"
#import "WebBrowserViewController.h"
#import "SpecialOffersViewController.h"
#import "RetailerData.h"
#import "LocationsInCouponDetailTableViewCell.h"
#import "RetailerSummaryViewController.h"
#import "CouponSpecialMapViewController.h"

@interface NewCouponDetailViewController ()<CustomizedNavControllerDelegate,DWBubbleMenuViewDelegate,MFMessageComposeViewControllerDelegate,HTTPClientDelegate,MFMailComposeViewControllerDelegate>
{
    UIView *termsNCondition;
    UIActivityIndicatorView *activityIndicator;
    DWBubbleMenuButton *upMenuView;
    NSMutableArray * productArray;
    NSMutableArray * tableDetails;
}
@property(nonatomic,strong) CouponDetailResponse * obj_CouponDetailResponse;
@end

@implementation NewCouponDetailViewController
@synthesize  expiryLabelTopConstraint, termsLabelTopConstraint, expirylabelHeightConstraint, termsLabelheightContraint,emailSendingVC,anyVC, claimButtonOutlet,redeemButtonOutlet,termsDetailLabelOutlet,termsConditionViewOutlet,heightFortermsDetailLabelConstraint,descriptionWebViewOutlet,termsAndConditionsButtonOutlet,locationButtonOutlet,obj_CouponDetailResponse,startDateOutlet,expireDateOutlet,coverViewOutlet,productTableViewOutlet,productAlertViewoutlet,bannerNameOutlet,sdImageViewOutlet,isFromMapScreen,fromspecialCoupons;

BOOL shareClikedFlag;

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error
{
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription] ];
    
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    [self responseData : responseObject];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    coverViewOutlet.hidden = YES;
    productAlertViewoutlet.hidden = YES;
    
   
    tableDetails =[[NSMutableArray alloc]init];
    //set title of the screen
    cusNav =(CustomizedNavController *) self.navigationController;
    
    
    //customized home button navigation bar
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = mainPage;
    [productTableViewOutlet registerNib:[UINib nibWithNibName:@"LocationsInCouponDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    [SharedManager setRefreshGallery:NO];   // to refresh when a coupon is claimed
    
    termsConditionViewOutlet.hidden = YES;
    [self request_getcoupondetails];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
       //share button
    
    // Create down menu button
    UIImageView *homeLabel = [self createHomeButtonView];
    // Create up menu button
    upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
                                                                      self.view.frame.size.height - homeLabel.frame.size.height - 10.f,
                                                                      homeLabel.frame.size.width,
                                                                      homeLabel.frame.size.height)
                                        expansionDirection:DirectionUp];
    upMenuView.homeButtonView = homeLabel;
    upMenuView.delegate = self;
    [upMenuView addButtons:[self createDemoButtonArray]];
    [self.view addSubview:upMenuView];
    upMenuView.onLogOutPressed =^(){
        [self logOutPressed];
    } ;
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark floating share button

- (UIImageView *)createHomeButtonView
{
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
    label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}

- (NSArray *)createDemoButtonArray
{
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    int i = 0;
    for (NSString *title in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:title] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        button.clipsToBounds = YES;
        button.tag = i++;
        [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
}

- (void)test:(UIButton *)sender {
    
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
    shareClikedFlag=TRUE;
    [self shareClicked];
    
    if (shareClikedFlag==TRUE) {
        
        
        switch (sender.tag) {
            case 0:{//facebook
                anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
                anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"HOT_DEAL_SHARE"];
                [UIView transitionWithView:self.view duration:0.5
                                   options:UIViewAnimationOptionTransitionCurlDown
                                animations:^ { [self.view addSubview:anyVC.view]; }
                                completion:nil];
            }
                break;
            case 1:{//twitter
                if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                    
                    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                    
                    SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                        if (result == SLComposeViewControllerResultCancelled) {
                            DLog(@"Twitter Result: canceled");
                        } else {
                            DLog(@"Twitter Result: sent");
                            NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><fundId>%@</fundId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:FUNDRAISERID]];
                            
                            [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
                        }
                        
                        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                            
                            [controller.view removeFromSuperview];
                        } else
                            [controller dismissViewControllerAnimated:YES completion:Nil];
                    };
                    controller.completionHandler =myBlock;
                    
                    //Adding the Text to the facebook post value from iOS
                    [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    
                    //Adding the URL to the facebook post value from iOS
                    
                    //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                    
                    //Adding the Image to the facebook post value from iOS
                    
                    [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        [self.view addSubview:controller.view];
                        [self.view bringSubviewToFront:controller.view];
                    }else
                        [self presentViewController:controller animated:YES completion:Nil];
                    
                    
                }
                else {
                    [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
                }
            }
                
                
                break;
            case 2:{//text
                
                if ([self hasCellularCoverage])
                {
                    if ([MFMessageComposeViewController canSendText]) {
                        
                        MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
                        [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                        smsComposerController.messageComposeDelegate = self;
                        [self presentViewController:smsComposerController animated:YES completion:nil];
                    }
                }
                else
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];  // //[alert release];
                }
                
            }
                break;
            case 3:{//email
                //[defaults setValue:FUNDRAISER_SHARE forKey:KEY_SHARE_TYPE];
                emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
                emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"COUPON_SHARE"];
                [defaults setValue:couponId forKey:@"couponID"];
                __typeof(self) __weak  obj = self;
                emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                    
                    [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
                } ;
                
                [emailSendingVC loadMail];
            }
                break;
                
            default:
                shareClikedFlag=FALSE;
                [defaults setBool:NO forKey:BottomButton];
                break;
        }
    }
    else
    {
        switch (sender.tag)
        {
            default:
                break;
        }
        
    }
    
}
-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    if (shareClikedFlag==TRUE) {
        switch (result) {
            case MessageComposeResultCancelled:
                
                break;
                
            case MessageComposeResultSent:{
                [UtilityManager showAlert:nil msg:@"Message Sent"];
                
                NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><fundId>%@</fundId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:FUNDRAISERID]];
                
                [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
            }
                break;
            case MessageComposeResultFailed:{
                [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
            }
                break;
                
            default:
                break;
        }
        
        [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        
        if (result == MessageComposeResultSent) {
            
            //For User Tracking
            NSMutableString *reqStr = [[NSMutableString alloc] init];
            [reqStr appendFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam>",[defaults valueForKey:KEY_MAINMENUID]];
            
            [reqStr appendFormat:@"<coupId>%@</coupId></UserTrackingData>", couponId];
            
            
            NSString *urlString = [NSString stringWithFormat:@"%@ratereview/updatesharetype",BASE_URL];
            DLog(@"%@",urlString);
            NSString *response = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
            DLog(@"%@",response);
            ReleaseAndNilify(response);
            //[reqStr release];
            //For User Tracking
        }
    }
    
}

- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView
{
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}


-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

#pragma mark share coupon request response

-(void)shareClicked
{
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    [reqStr appendFormat:@"<ShareProductInfo><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [reqStr appendFormat:@"<couponId>%@</couponId>",couponId];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_HUBCITIID]];
    NSLog(@"request : %@",reqStr);
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharecoupon",BASE_URL];
    NSLog(@"URl : %@",urlString);
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    NSLog(@"response %@",response);
    [self parse_shareCoupons:response];
    
}

-(void)parse_shareCoupons:(NSString*)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        
        TBXMLElement *shareText=[TBXML childElementNamed:@"couponName" parentElement:tbXml.rootXMLElement];
        TBXMLElement *qrURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *hotDealImagePath = [TBXML childElementNamed:@"couponImagePath" parentElement:tbXml.rootXMLElement];
        
        if (qrURL!=nil) {
            [defaults setValue:[TBXML textForElement:qrURL] forKey:KEY_HYPERLINK];
            NSLog(@"The link to be shared is %@",KEY_HYPERLINK);
        }
        
        if (shareText!=nil) {
            NSMutableString *msgString = [[NSMutableString alloc]initWithString:@"I found this Hot Deal @HubCiti and thought you might be interested:"];
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [msgString appendFormat:@"%@",shareString];
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            NSLog(@"The share message value is %@",KEY_SHAREMSG);
        }
        
        if (hotDealImagePath!=nil) {
            [defaults setObject:[TBXML textForElement:hotDealImagePath] forKey:KEY_PRODUCTIMGPATH];
            NSLog(@"The product image path is %@",KEY_PRODUCTIMGPATH);
        }
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            return;
        }
        
    }
}

#pragma mark sender user tracking
-(void)userTracking:(NSString*)xmlStr
{
    NSMutableString *urlStr = [[NSMutableString alloc]init];
    [urlStr appendFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    DLog(@"%@",urlStr);
    NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:xmlStr]];
    DLog(@"%@",response);
    // [xmlStr release];
}




#pragma mark webview delegates
-(void) webViewDidFinishLoad:(UIWebView *)webView
{
    [activityIndicator stopAnimating];
    [descriptionWebViewOutlet.scrollView flashScrollIndicators];
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ([[[inRequest URL] scheme] isEqual:@"mailto"]) {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,(CFStringRef)value,
                                                                                                      CFSTR("")));
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            // [mailViewController release];
            return NO;
            
        }
        
        else {
            [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
            
            
        }
        
        return NO;
        
    }
    
    if ([[[inRequest URL] scheme] isEqual:@"tel"]) {
        NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
        
        NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
        NSString *phoneString  = [[defaultRecipient componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        NSString *phoneNumber = [NSString stringWithFormat:@"tel:%@", phoneString];
        NSURL    *url= [NSURL URLWithString:phoneNumber];
        
        UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        webview.alpha = 0.0;
        
        [webview loadRequest:[NSURLRequest requestWithURL:url]];
        
        // Assume we are in a view controller and have access to self.view
       
        [self.view insertSubview:webview belowSubview:descriptionWebViewOutlet];
        return NO;
    }
    
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        
        NSString *url = [[inRequest URL]absoluteString];
        [defaults setValue:url forKey:KEY_URL];
        //[defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
        
        WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:urlDetail animated:NO];
        //[urlDetail release];
        
        return NO;
    }
    return YES;
}




#pragma mark home page and back
-(void)returnToMainPage:(id)sender
{
    [defaults setBool:NO forKey: @"detailsPage"];
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }}

-(void)popBackToPreviousPage{
    
    if (isFromProductPage || isFromMapScreen) {
        //isFromProductPage = NO;
        [self.navigationController popViewControllerAnimated:NO];
    }
//    else if (fromCurrentSpecials) {
//        fromCurrentSpecials = FALSE;
//        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
//        for (UIViewController *aViewController in allViewControllers) {
//            if ([aViewController isKindOfClass:[CurrentSpecialsViewController class]]) {
//                [self.navigationController popToViewController:aViewController animated:NO];
//            }
//        }
//    }
    else{
        
        if ([defaults boolForKey:@"detailsPage"]) {
            
            [defaults setBool:NO forKey:@"detailsPage"];
            [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:NO];
            
        }
        
    }
}


#pragma mark coupon data

-(void)request_getcoupondetails
{
    
    iWebRequestState = REQUESTFORCOUPONDETAILS;
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];

    
    [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    [parameters setValue:[defaults valueForKey:@"couponId"] forKey:@"couponId"];
    
    //For user tracking
    if ([defaults valueForKey:@"couponListId"])
    {
        [parameters setValue:[defaults valueForKey:@"couponListId"] forKey:@"couponListId"];
    }
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getcoupondetailjson",BASE_URL];
    //NSString *urlString = [NSString stringWithFormat:@"http://10.10.221.155:9990/HubCiti2.8.2/thislocation/getcoupondetailjson"];
    
    NSLog(@"URL %@", urlString);
    NSLog(@"parameter %@",parameters);
    DLog(@"parameter: %@",parameters);
    HTTPClient *client = [[HTTPClient alloc] init];
    client.delegate = self;
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    [HubCitiAppDelegate showActivityIndicator];
    
    // [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}
-(void)parse_getcoupondetails:(id)responseString
{
    if (responseString == nil)
    {
        return;
    }
    if (obj_CouponDetailResponse == nil)
    {
        obj_CouponDetailResponse = [[CouponDetailResponse alloc]init];
    }
    [obj_CouponDetailResponse setValuesForKeysWithDictionary:responseString];
    if ([obj_CouponDetailResponse.responseCode isEqualToString:@"10000"])
    {
        if (obj_CouponDetailResponse.couponId)
        {
            couponId = obj_CouponDetailResponse.couponId;
        }
        [self resetConstraint];
        coverViewOutlet.hidden = NO;
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:obj_CouponDetailResponse.responseText];
        
    }
    
    
}
-(void) resetConstraint
{
    [cusNav setTitle:obj_CouponDetailResponse.couponName forView:self withHambergur:NO];
    bannerNameOutlet.text = obj_CouponDetailResponse.bannerName;
    startDateOutlet.text = obj_CouponDetailResponse.couponStartDate;
    [sdImageViewOutlet loadImage:obj_CouponDetailResponse.couponImagePath];
    
    [self.view layoutIfNeeded];
    if(!obj_CouponDetailResponse.couponExpireDate || [obj_CouponDetailResponse.couponExpireDate isEqualToString:@"N/A"])
    {
        expirylabelHeightConstraint.constant = 0;
        expiryLabelTopConstraint.constant = 0;
    }
    else
    {
        NSString *expireDateString = [NSString stringWithFormat:@"Expires On: %@",obj_CouponDetailResponse.couponExpireDate];
        expireDateOutlet.text = expireDateString;
    }
    if (!obj_CouponDetailResponse.termAndConditions || [obj_CouponDetailResponse.termAndConditions isEqualToString:@"N/A"])
    {
        termsLabelheightContraint.constant = 0;
        termsLabelTopConstraint.constant = 0;
        [termsAndConditionsButtonOutlet setTitle:@"" forState:UIControlStateNormal];
    }
    else
    {
        termsDetailLabelOutlet.text = obj_CouponDetailResponse.termAndConditions;
    }
    if(IPAD)
    {
        termsConditionViewOutlet.layer.borderWidth = 3;
    }
    termsConditionViewOutlet.layer.borderColor = [[UIColor grayColor]CGColor];
    
    //webview
    if (IPAD)
    {
        [descriptionWebViewOutlet loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:17px;font-family:Helvetica;text-align: center;color:#000000;word-wrap: break-word'>%@",obj_CouponDetailResponse.couponDesc] baseURL:nil];
    }
    else
    {
        [descriptionWebViewOutlet loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;text-align: center;color:#000000;word-wrap: break-word'>%@",obj_CouponDetailResponse.couponDesc] baseURL:nil];
    }
//    descriptionWebViewOutlet.layer.borderColor = [UIColor grayColor].CGColor;
//    descriptionWebViewOutlet.layer.borderWidth = 1.0;
    
    
    NSLog(@"%d",(int)obj_CouponDetailResponse.usedFlag);
    switch ([obj_CouponDetailResponse.usedFlag integerValue])
    {
            
        case 0://yet to claim
            redeemButtonOutlet.enabled = NO;
            redeemButtonOutlet.alpha = 0.5;
            claimButtonOutlet.enabled = YES;
            claimButtonOutlet.alpha = 1;
            canClaim = 1;
            canRedeem = 0;
            allDone = 0;
            break;
            
        case 1://claimed but not used
            redeemButtonOutlet.enabled = YES;
            redeemButtonOutlet.alpha = 1;
            claimButtonOutlet.enabled = NO;
            claimButtonOutlet.alpha = 0.5;
            canRedeem = 1;
            canClaim = 0;
            allDone = 0;
            break;
            
        case 2://claimed and used
            redeemButtonOutlet.enabled = NO;
            redeemButtonOutlet.alpha = 0.5;
            claimButtonOutlet.enabled = NO;
            claimButtonOutlet.alpha = 0.5;
            allDone = 1;
            canClaim = 0;
            canRedeem = 0;
            break;
            
        default:
            redeemButtonOutlet.enabled = NO;
            redeemButtonOutlet.alpha = 0.5;
            claimButtonOutlet.enabled = NO;
            claimButtonOutlet.alpha = 0.5;
            allDone = 1;
            canClaim = 0;
            canRedeem = 0;
            break;
    }
    if ([obj_CouponDetailResponse.expireFlag integerValue] == 1)
    {
        redeemButtonOutlet.enabled = NO;
        redeemButtonOutlet.alpha = 0.5;
        claimButtonOutlet.enabled = NO;
        claimButtonOutlet.alpha = 0.5;
        upMenuView.userInteractionEnabled = YES;
    }
    
    [self.view updateConstraints];
    //activity indicator
    activityIndicator = [[UIActivityIndicatorView alloc]
                         initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityIndicator.hidden = NO;
    activityIndicator.frame  = CGRectMake((descriptionWebViewOutlet.frame.size.width - (IPAD?30:20))/2, (descriptionWebViewOutlet.frame.size.height - (IPAD?30:20))/2, (IPAD?30:20), (IPAD?30:20));
    [descriptionWebViewOutlet addSubview:activityIndicator];
    [activityIndicator startAnimating];
}


// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case REQUESTFORCOUPONDETAILS:
            [self parse_getcoupondetails:response];
            break;
        case CLIAMCOUPON:
            [self parse_addCoupon:response];
            break;
            
        case REDEEMCOUPON:
            [self parse_userredeemcoupon:response];
            break;
        
        case RETSUMMARYDATA:
            [self parse_retSummary:response];
            break;
            
        case LOCATIONDETAILS:
            {
                if(fromspecialCoupons){
                    
                     [self parseLocationDetails :response];
                }
                else{
            dispatch_queue_t queue = dispatch_queue_create("com.example.MyQueue", NULL);
                dispatch_async(queue, ^{
                    [self parseLocationDetails :response];
                });
            }
            }
            break;
        default:
            break;
    }
    
}

#pragma mark claim, redeem, location, terms button

- (IBAction)cliamButtonClicked:(id)sender {
    if(userIdentifier==TRUE)
    {
        [NewsUtility signupPopUp:self];
        
    }
    else
    {
        iWebRequestState = CLIAMCOUPON;
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        [parameters setValue:couponId forKey:@"couponId"];
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
        
        //        NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<CLRDetails><couponId>%@</couponId>",couponId];
        //        [reqStr appendFormat:@"<userId>%@</userId></CLRDetails>",[defaults valueForKey:KEY_USERID]];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@gallery/addcouponjson",BASE_URL];
       // NSString *urlStr = [NSString stringWithFormat:@"http://10.10.221.155:9990/HubCiti2.8.2/gallery/addcouponjson"];
        NSLog(@"URL %@", urlStr);
        DLog(@"parameter: %@",parameters);
        HTTPClient *client = [[HTTPClient alloc] init];
        client.delegate = self;
        DLog(@"Url: %@",urlStr);
        [client sendRequest : parameters : urlStr];
        [HubCitiAppDelegate showActivityIndicator];
        //  [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
        // ReleaseAndNilify(reqStr);
    }
}

-(void)parse_addCoupon:(id)response
{
    if (response == nil)
        return;
    NSDictionary *responseData=response;
    NSString * responseCode = [responseData objectForKey:RESPONSECODE];
    NSString * responseText = [responseData objectForKey:RESPONSETEXT];
    
    //    if ([UtilityManager isResponseXMLNullOrEmpty:response])
    //        return;
    //
    //    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    //
    //    TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    //    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [responseCode isEqualToString:@"10000"])
    {
        redeemButtonOutlet.enabled = YES;
        redeemButtonOutlet.alpha = 1;
        claimButtonOutlet.enabled = NO;
        claimButtonOutlet.alpha = 0.5;
        canRedeem =1;
        canClaim =0;
        allDone = 0;
        [SharedManager setRefreshGallery:YES];
    }
    
    else{
        [SharedManager setRefreshGallery:NO];
        [UtilityManager showAlert:@"Claim" msg:responseText];
        return;
    }
}
- (IBAction)redeemButtonClicked:(id)sender {
    if(userIdentifier==TRUE)
    {
        [NewsUtility signupPopUp:self];
        
    }
    else
    {
        iWebRequestState = REDEEMCOUPON;
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        [parameters setValue:couponId forKey:@"couponId"];
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
        
        
        //        NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<CouponDetails><couponId>%@</couponId>",couponId];
        //        [reqStr appendFormat:@"<userId>%@</userId></CouponDetails>",[defaults valueForKey:KEY_USERID]];
        
            NSString *urlStr = [NSString stringWithFormat:@"%@gallery/userredeemcouponjson",BASE_URL];
        //
        //        [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
        //        ReleaseAndNilify(reqStr);
        
        //NSString *urlStr = [NSString stringWithFormat:@"http://10.10.221.155:9990/HubCiti2.8.2/gallery/userredeemcouponjson"];
        NSLog(@"URL %@", urlStr);
        DLog(@"parameter: %@",parameters);
        HTTPClient *client = [[HTTPClient alloc] init];
        client.delegate = self;
        DLog(@"Url: %@",urlStr);
        [client sendRequest : parameters : urlStr];
        [HubCitiAppDelegate showActivityIndicator];
    }
}


-(void)parse_userredeemcoupon:(id)response
{
    if (response == nil)
        return;
    NSDictionary *responseData=response;
    NSString * responseCode = [responseData objectForKey:RESPONSECODE];
    NSString * responseText = [responseData objectForKey:RESPONSETEXT];
    
    
    //    if ([UtilityManager isResponseXMLNullOrEmpty:response])
    //        return;
    //
    //    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    //
    //    TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    //    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    //
    if (responseText!=nil && [responseCode isEqualToString:@"10000"]){
        
        redeemButtonOutlet.enabled = NO;
        redeemButtonOutlet.alpha = 0.5;
        canClaim = 0;
        canRedeem =0;
        allDone = 1;
        [SharedManager setRefreshGallery:YES];
        [UtilityManager showAlert:@"Redeem" msg:responseText];
        [UIView animateWithDuration:0.5
                         animations:^{
                             
                             [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.navigationController.view cache:NO];
                             
                         }
                         completion:^(BOOL finished){
                         }];
        
        
        [self.navigationController popViewControllerAnimated:NO];
    }
    
    else{
        [SharedManager setRefreshGallery:NO];
        [UtilityManager showAlert:@"Redeem" msg:responseText];
        return;
    }
    
}

- (IBAction)locationButtonClicked:(id)sender
{
    
    if(!fromspecialCoupons){
    claimButtonOutlet.enabled = NO;
    redeemButtonOutlet.enabled = NO;
    termsAndConditionsButtonOutlet.enabled = NO;
    locationButtonOutlet.enabled = NO;
    upMenuView.userInteractionEnabled = NO;
    }
    NSString *str;
    str = [NSString stringWithFormat:@"<couponId>%@</couponId>",couponId];
    [defaults setObject:str forKey:@"CouponRequest"];
    [self getLocationDetails];
}

-(void) getLocationDetails
{
    
    iWebRequestState = LOCATIONDETAILS;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendString:@"<CLRDetails>"];
    [requestStr appendFormat:@"<userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"%@",[defaults objectForKey:@"CouponRequest"] ];
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
    
    if([defaults valueForKey:KEY_HUBCITIID])
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_HUBCITIID]];
    
    if([defaults valueForKey:@"couponListID"])
    [requestStr appendFormat:@"<couponListId>%@</couponListId>",[defaults valueForKey:@"couponListID"]];
    
    [requestStr appendString:@"<type>location</type>"];
    [requestStr appendString:@"</CLRDetails>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@gallery/getcouponproductorlocation",BASE_URL];
    

//    urlString = [NSString stringWithFormat:@"http://10.10.220.231:9990/HubCiti2.9/gallery/getcouponproductorlocation"];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    ReleaseAndNilify(requestStr);
}
-(void) parseLocationDetails :(NSString *)responseString
{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
    {
        [self updateButtonStatus];
         return;
    }
    


    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *locationDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:tbxml.rootXMLElement];
        if (locationDetailElement!=nil){
            if(!fromspecialCoupons){
            dispatch_async(dispatch_get_main_queue(), ^{
                productAlertViewoutlet.hidden = NO;
                [self.view bringSubviewToFront:productAlertViewoutlet];
            });
            }
            while (locationDetailElement!=nil) {
                
                
                TBXMLElement *locationIdElement = [TBXML childElementNamed:@"retailerId" parentElement:locationDetailElement];
                TBXMLElement *locationNameElement = [TBXML childElementNamed:@"retailerName" parentElement:locationDetailElement];
                TBXMLElement *retailLocationIdElement = [TBXML childElementNamed:@"retailLocationId" parentElement:locationDetailElement];
                
                TBXMLElement *locationDescriptionElement = [TBXML childElementNamed:@"completeAddress" parentElement:locationDetailElement];
                
                TBXMLElement *imagePathElement = [TBXML childElementNamed:@"logoImagePath" parentElement:locationDetailElement];
                
                TBXMLElement *reatilerlistIdElement = [TBXML childElementNamed:@"retListId" parentElement:locationDetailElement];
                
                TBXMLElement *latitudeElement = [TBXML childElementNamed:@"latitude" parentElement:locationDetailElement];

                TBXMLElement *longitudeElement = [TBXML childElementNamed:@"longitude" parentElement:locationDetailElement];

                
                RetailerData *iRetailData=[[RetailerData alloc]init];
                if (latitudeElement) {
                    iRetailData.latitude=[[NSString alloc]initWithString:[TBXML textForElement:latitudeElement]];
                }
                if (longitudeElement) {
                    iRetailData.longitude=[[NSString alloc]initWithString:[TBXML textForElement:longitudeElement]];
                }
                if (locationIdElement) {
                    iRetailData.retailerId=[[NSString alloc]initWithString:[TBXML textForElement:locationIdElement]];
                }
                if(reatilerlistIdElement){
                    iRetailData.retListId=[[NSString alloc]initWithString:[TBXML textForElement:reatilerlistIdElement]];
                }
                if (locationNameElement) {
                    iRetailData.retailerName=[[NSString alloc]initWithString:[TBXML textForElement:locationNameElement]];
                }
                
                if (retailLocationIdElement) {
                    iRetailData.retailLocationId=[[NSString alloc]initWithString:[TBXML textForElement:retailLocationIdElement]];
                }
                
                if (imagePathElement) {
                    iRetailData.logoImagePath=[[NSString alloc]initWithString:[TBXML textForElement:imagePathElement]];
                }
                
                if (locationDescriptionElement) {
                    iRetailData.completeAddress=[[NSString alloc]initWithString:[TBXML textForElement:locationDescriptionElement]];
                }
                
                [tableDetails addObject:iRetailData];

                 locationDetailElement = [TBXML nextSiblingNamed:@"RetailerDetail" searchFromElement:locationDetailElement];
            }
            if(fromspecialCoupons){
               
              CouponSpecialMapViewController   *couponDetailVC = [[CouponSpecialMapViewController alloc]initWithNibName:@"CouponSpecialMapViewController" bundle:[NSBundle mainBundle]];
                couponDetailVC.globalRetailerService = tableDetails;
                [self.navigationController pushViewController:couponDetailVC animated:NO];
                
            }
            else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [productTableViewOutlet reloadData];
            });
            }
        }
    }
    else
    {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        if (responseTextElement!=nil)
        {
            UIAlertController * alert;
           alert=[UIAlertController alertControllerWithTitle:nil message:[TBXML textForElement:responseTextElement] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction * action){
                                     [self updateButtonStatus];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
       
    }
    

}
-(void) updateButtonStatus
{
    descriptionWebViewOutlet.userInteractionEnabled = YES;
    if ([obj_CouponDetailResponse.expireFlag integerValue] == 1)
    {
        redeemButtonOutlet.enabled = NO;
        redeemButtonOutlet.alpha = 0.5;
        claimButtonOutlet.enabled = NO;
        claimButtonOutlet.alpha = 0.5;
        upMenuView.userInteractionEnabled = YES;
    }
    else
    {
        upMenuView.userInteractionEnabled = YES;
        if(canClaim == 1)
        {
            claimButtonOutlet.enabled = YES;
            claimButtonOutlet.alpha = 1;
            redeemButtonOutlet.enabled = NO;
            redeemButtonOutlet.alpha = 0.5;
        }
        else if(canRedeem == 1)
        {
            redeemButtonOutlet.enabled = YES;
            redeemButtonOutlet.alpha = 1;
            claimButtonOutlet.enabled = NO;
            claimButtonOutlet.alpha = 0.5;
        }
        else if(allDone == 1)
        {
            claimButtonOutlet.enabled = NO;
            claimButtonOutlet.alpha = 0.5;
            redeemButtonOutlet.enabled = NO;
            redeemButtonOutlet.alpha = 0.5;
        }
    }
    termsAndConditionsButtonOutlet.enabled = YES;
    locationButtonOutlet.enabled = YES;
    termsConditionViewOutlet.hidden = YES;
    productAlertViewoutlet.hidden = YES;
    
}
- (IBAction)termsAndCondButtonClicked:(id)sender
{
    descriptionWebViewOutlet.userInteractionEnabled = NO;
    claimButtonOutlet.enabled = NO;
    redeemButtonOutlet.enabled = NO;
    termsAndConditionsButtonOutlet.enabled = NO;
    locationButtonOutlet.enabled = NO;
    upMenuView.userInteractionEnabled = NO;
    
    
    termsConditionViewOutlet.hidden = NO;
    termsDetailLabelOutlet.numberOfLines = 0;
    [termsDetailLabelOutlet sizeToFit];
    [self.view bringSubviewToFront:termsConditionViewOutlet];
}

- (IBAction)closeButtonPressed:(id)sender
{
    [tableDetails removeAllObjects];
    [self updateButtonStatus];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"Number of rows %lu",(unsigned long)tableDetails.count);
    return tableDetails.count;
    //return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IPAD)
    {
        return 60;
    }
    else
        return 40;
    //ipad 80
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LocationsInCouponDetailTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    RetailerData *iData = [tableDetails objectAtIndex:indexPath.row];
    [cell setDataForImageAndLabel:iData.logoImagePath titleLabel:iData.retailerName detailLabel:iData.completeAddress];
      
//    UITableViewCell *cell ;
//    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"] ;
   // cell.textLabel.text = productArray[indexPath.row];
  //  cell.textLabel.font = [UIFont systemFontOfSize:(IPAD ?16:13)];
   // //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
  
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RetailerData *iData = [tableDetails objectAtIndex:indexPath.row];
    [self requestForRetSummary : (int)indexPath.row selecetdRetData : (RetailerData*) iData];
}
-(void) requestForRetSummary : (int) index selecetdRetData : (RetailerData*) iRetData
{
    iWebRequestState =RETSUMMARYDATA;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",iRetData.retailLocationId];
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",iRetData.retailerId];
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    NSString *urlString =  [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);

}

-(void)parse_retSummary:(NSString *)response
{
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else
    {
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        //        rsvc.distanceFromPreviousScreen = [arrDistance objectAtIndex:objIndex];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
    
}
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
//}
//- (BOOL)shouldAutorotate{
//    return NO;
//}
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//
//{
//    return UIInterfaceOrientationPortrait;
//}
//
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskPortrait;
//}

@end

//
//  CouponSpecialMapViewController.h
//  HubCiti
//
//  Created by Ashika on 3/9/17.
//  Copyright © 2017 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomizedNavController.h"
#import "Mapkit/MKMapView.h"

@interface CouponSpecialMapViewController : UIViewController
{
    CustomizedNavController * cusNav;
    MKMapView *mapView;
}
@property (nonatomic,strong) NSMutableArray *globalRetailerService;
@property (nonatomic,strong) NSString *couponIds,*coponmappostalcode;
@end


//
//  HubCitiConnectionManager.m
//  HubCiti
//
//  Created by Ajit on 9/4/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "HubCitiConnectionManager.h"
#import "CommonUtility.h"
#import "AppDelegate.h"
#import "SBJson5Writer.h"

static HubCitiConnectionManager *sharedConnectionManager = nil;

@implementation HubCitiConnectionManager

@synthesize respondeCode;

+ (id)connectionManager {
    @synchronized(self) {
        if(sharedConnectionManager == nil)
            sharedConnectionManager = [[super allocWithZone:NULL] init];
    }
    return sharedConnectionManager;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self connectionManager];
}
- (id)copyWithZone:(NSZone *)zone {
    return self;
}

//used for https connection
-(void)establishConnectionForSmartSearch:(NSString *) xmlStr base:(NSString *)baseUrl withDelegate:(id) _delegate
{
    self.hubcitiConnMgrDelegate = _delegate;
    [HubCitiAppDelegate showActivityIndicator];
    [defaults setValue:xmlStr forKey:@"xmlStr"];
    [defaults setValue:baseUrl forKey:@"baseUrl"];
    
    NSURL *url = [NSURL URLWithString:baseUrl];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[xmlStr length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    //[request setTimeoutInterval:20.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:HTTP_CONTLENGTH];
    [request setValue:@"text/xml;charset=utf-8" forHTTPHeaderField:HTTP_CONTTYPE];
    [request setHTTPBody:[xmlStr dataUsingEncoding:NSUTF8StringEncoding]];
    DLog(@"Service Name : %@\n Request : %@\n",baseUrl,xmlStr);
    
    if(connectionTask)
    {
        [connectionTask cancel];
        [session invalidateAndCancel];
        connectionTask = nil;
    }
    session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                          delegate:self
                                                     delegateQueue:nil];
    connectionTask = [session dataTaskWithRequest:request];
    [connectionTask resume];
    
    //connectionMain = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if( connectionTask )
    {
        if(webData)
        {
            //  [webData release];
            webData = nil;
        }
        //webData = [[NSMutableData data] retain];
    }
    
}
-(id) establishPostConnectionforJsonData:(NSString *) baseUrl withParam:(NSMutableDictionary *)dict
{
    NSURL *url = [[NSURL alloc]initWithString:baseUrl];
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc]initWithURL:url];
    // Create a simple dictionary with numbers.
    SBJson5Writer *writer = [[SBJson5Writer alloc] init];
    NSString *data1 = [writer stringWithObject: dict];
    
    //NSString* data1 = [dict JSONRepresentation];
    
    
    if(data1 != nil)
    {
        NSData *postData = [data1 dataUsingEncoding:NSUTF8StringEncoding];
        [req setHTTPBody:postData];
        
    }
    [req setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"]; //;charset=utf-8
    [req setHTTPMethod:@"POST"];
    __block NSString *responseData = @"";
    
    //Semaphore added to make it sychronous request
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    // Create a task.
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:req
                                                                 completionHandler:^(NSData *data,
                                                                                     NSURLResponse *response,
                                                                                     NSError *error)
                                  {
                                      if(data.bytes != 0)
                                      {
                                          responseData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                          
                                          DLog(@"Response:%@",response);
                                      }
                                      else if (!error && [error code] == -1001)
                                      {
                                          [UtilityManager requestTimedOut];
                                      }
                                      dispatch_semaphore_signal(semaphore);
                                      
                                  }];
    // Start the task.
    [task resume];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    return responseData ;
    
}
-(id) establishGetConnectionforJsonData:(NSString *) baseUrl
{
    
    //[HubCitiAppDelegate showActivityIndicator];
    
    
    NSURL *url = [[NSURL alloc ]initWithString:baseUrl];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    
    [req setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"]; //;charset=utf-8
    [req setHTTPMethod:@"GET"];
    
    __block NSString *responseData = @"";
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    // Create a task.
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:req
                                                                 completionHandler:^(NSData *data,
                                                                                     NSURLResponse *response,
                                                                                     NSError *error)
                                  {
                                      if(data.bytes != 0)
                                      {
                                          responseData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                          
                                          DLog(@"Response:%@",response);
                                          if(responseData.length == 0)
                                          {
                                              responseData= nil;
                                              
                                          }
                                      }
                                      else if (!error && [error code] == -1001)
                                      {
                                          [UtilityManager requestTimedOut];
                                      }
                                      dispatch_semaphore_signal(semaphore);
                                      
                                  }];
    // Start the task.
    [task resume];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    return responseData ;
    
}





-(void)establishConnectionFor:(NSString *) xmlStr base:(NSString *)baseUrl withDelegate:(id) _delegate{
    
    self.hubcitiConnMgrDelegate = _delegate;
    [HubCitiAppDelegate showActivityIndicator];
    [defaults setValue:xmlStr forKey:@"xmlStr"];
    [defaults setValue:baseUrl forKey:@"baseUrl"];
    
    NSURL *url = [NSURL URLWithString:baseUrl];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[xmlStr length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    //[request setTimeoutInterval:20.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:HTTP_CONTLENGTH];
    [request setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:HTTP_CONTTYPE];
    [request setHTTPBody:[xmlStr dataUsingEncoding:NSUTF8StringEncoding]];
    DLog(@"Service Name : %@\n Request : %@\n",baseUrl,xmlStr);
    //[xmlStr release];
    
    
    if(connectionTask)
    {
        [connectionTask cancel];
        [session invalidateAndCancel];
        connectionTask = nil;
    }
    session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                          delegate:self
                                                     delegateQueue:nil];
    connectionTask = [session dataTaskWithRequest:request];
    [connectionTask resume];
    
    
    if( connectionTask )
    {
        if(webData)
        {
            webData = nil;
        }
        webData = [NSMutableData data];
    }
}

//used for https connection
-(void)establishConnectionForGet:(NSString *) baseUrl withDelegate:(id) _delegate{
    
    self.hubcitiConnMgrDelegate = _delegate;
    
    [defaults setValue:nil forKey:@"xmlStr"];
    [defaults setValue:baseUrl forKey:@"baseUrl"];
    
    NSURL *url = [NSURL URLWithString:baseUrl];
    
    DLog(@"url string = %@\n" ,baseUrl);
    [HubCitiAppDelegate showActivityIndicator];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    
    
    [request setURL:url];
    [request setHTTPMethod:@"GET"];
    if(connectionTask)
    {
        [connectionTask cancel];
        [sessionForEstablishConnectionForGet invalidateAndCancel];
        connectionTask = nil;
    }
    sessionForEstablishConnectionForGet = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                          delegate:self
                                                     delegateQueue:nil];
    connectionTask = [sessionForEstablishConnectionForGet dataTaskWithRequest:request];
    [connectionTask resume];
    
    
    if( connectionTask )
    {
        if(webData)
        {
            webData = nil;
        }
        webData = [NSMutableData data];
    }
    
}

//Delegate method called when the connection receives responce

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    
    if ([httpResponse statusCode] == 200)
    {
        respondeCode = 200;
        [webData setLength: 0];
        // start recieving data
        //	DLog(@"Server Response :%d",[httpResponse statusCode],[NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]]);
       	DLog(@"Server Response :%ld",(long)[httpResponse statusCode]);
        NSMutableData *receivedData=nil;
        receivedData=[[NSMutableData alloc] init];
        [receivedData setLength:0];
        
        completionHandler(NSURLSessionResponseAllow);
        
    }
    else if ([httpResponse statusCode] >= 400)
    {
        // do error handling here
        DLog(@"remote url returned error %ld %@",(long)[httpResponse statusCode],[NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]]);
        NSString *errorMessage = [NSString stringWithFormat:@"%@ %ld:%@", @"remote url returned error",(long)[httpResponse statusCode],[NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]]];
        [UtilityManager showAlert:nil msg:errorMessage];
        respondeCode = (int)[httpResponse statusCode];
        return;
        
    }
    else
    {
        //need to handle it
        [webData setLength: 0];
        // start recieving data
        //	DLog(@"Server Response :%d",[httpResponse statusCode],[NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]]);
        DLog(@"Server Response :%ld",(long)[httpResponse statusCode]);
    }
}


//Delegate method called when the connection receives data
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    //webData = [[NSMutableData alloc]init];
    if(webData != nil)
    {
        [webData appendData:data];
    }
    
}

//Delegate method called when the connection finished loading all the data
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    if(error && error.code!=NSURLErrorCancelled)
    {
        NSDictionary *errorDic = [NSDictionary dictionaryWithObject:error forKey:@"error"];
        
        DLog(@"error = %@",errorDic);
        [HubCitiAppDelegate removeActivityIndicator];
        NSString *errorMessage = [NSString stringWithFormat:@"%@", @"Connection Failed"];
        
        UIAlertController * alert;
        
        alert=[UIAlertController alertControllerWithTitle:errorMessage message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleCancel
                             handler:nil];
        [alert addAction:ok];
        UIAlertAction* retry = [UIAlertAction
                                actionWithTitle:@"Retry"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action){
                                    if([defaults valueForKey:@"xmlStr"])
                                        [self establishConnectionFor:[defaults valueForKey:@"xmlStr"] base:[defaults valueForKey:@"baseUrl"] withDelegate:self.hubcitiConnMgrDelegate];
                                    else
                                        [self establishConnectionForGet:[defaults valueForKey:@"baseUrl"] withDelegate:self.hubcitiConnMgrDelegate];
                                    
                                }];
        [alert addAction:retry];
        
        [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
    }
 
    else if (task.error.code != NSURLErrorCancelled)
    {
        //    [webData appendDa ta:data2];
        dispatch_async(dispatch_get_main_queue(), ^{
            [HubCitiAppDelegate removeActivityIndicator];
        });
        
        [defaults setValue:nil forKey:@"xmlStr"];
        [defaults setValue:nil forKey:@"baseUrl"];
        
        NSMutableString *responseXml = [[NSMutableString alloc]initWithData:webData encoding:NSUTF8StringEncoding];
        
        DLog(@"Response xml = %@", responseXml);
        
        if(respondeCode == 200 )
        {
            //added in main thread as session internally does not call main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.hubcitiConnMgrDelegate responseData:responseXml];
            });
            
        }
        else
        {
            connectionMain = nil;
            ReleaseAndNilify(responseXml);
            return;
            
            
        }
        ReleaseAndNilify(responseXml);
        
    }
    
}



- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler{
    
        NSURLCredential * credential = [[NSURLCredential alloc] initWithTrust:[challenge protectionSpace].serverTrust];
    completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
}

//used for http connection
-(NSString *) establishPostConnection:(NSString *) baseUrl withParam:(NSString *)xmlStr{
    
    DLog(@"Req:%@",xmlStr);
    DLog(@"URL:%@",baseUrl);
    NSURL *url = [NSURL URLWithString:baseUrl];
    //[HubCitiAppDelegate showActivityIndicator];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[xmlStr length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setTimeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:HTTP_CONTLENGTH];
    [request setValue:@"text/xml; charset=UTF-8" forHTTPHeaderField:HTTP_CONTTYPE];
    [request setHTTPBody:[xmlStr dataUsingEncoding:NSUTF8StringEncoding]];
    //DLog(@"Login Request: %@ url =%@",xmlStr , baseUrl);
    
    //	NSError *error = nil;
    //	NSURLResponse *response;
    __block NSString *responseXml = @"";
    //Semaphore added to make it sychronous request
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    // Create a task.
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                                 completionHandler:^(NSData *data,
                                                                                     NSURLResponse *response,
                                                                                     NSError *error)
                                  {
                                      if(data.bytes != 0)
                                      {
                                          responseXml = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                          DLog(@"Response: %@",responseXml);
                                          
                                          if([UtilityManager isResponseXMLNullOrEmpty:responseXml])
                                          {
                                              ReleaseAndNilify(responseXml);
                                              
                                          }
                                          else{
                                              
                                              TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
                                              TBXMLElement *htmlTag = [TBXML childElementNamed:@"head" parentElement:tbXml.rootXMLElement];
                                              if (htmlTag != nil)
                                              {
                                                  [UtilityManager showAlert:NSLocalizedString(@"Technical Problem",@"Technical Problem") msg:NSLocalizedString(@"please try again later",@"please try again later")];
                                                  ReleaseAndNilify(responseXml);
                                              }
                                              
                                          }
                                      }
                                      else if (!error && [error code] == -1001)
                                      {
                                          [UtilityManager requestTimedOut];
                                      }
                                      dispatch_semaphore_signal(semaphore);
                                      
                                  }];
    // Start the task.
    [task resume];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    return responseXml;
}

//used for http connection
-(NSString *) establishGetConnection:(NSString *) baseUrl{
    
    NSURL *url = [NSURL URLWithString:baseUrl];
    
    DLog(@" url string = %@" ,baseUrl);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setTimeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    
    __block NSString *responseXml = @"";
    //Semaphore added to make it sychronous request
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    // Create a task.
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                                 completionHandler:^(NSData *data,
                                                                                     NSURLResponse *response,
                                                                                     NSError *error)
                                  {
                                      if(data.bytes != 0)
                                      {
                                          responseXml = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                          //DLog(@"Response: %@ ",responseXml);
                                          if([UtilityManager isResponseXMLNullOrEmpty:responseXml])
                                          {
                                              ReleaseAndNilify(responseXml);
                                              
                                          }
                                          else{
                                              responseXml = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                              if([UtilityManager isResponseXMLNullOrEmpty:responseXml])
                                              {
                                                  ReleaseAndNilify(responseXml);
                                                  
                                              }
                                              else{
                                                  TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
                                                  TBXMLElement *htmlTag = [TBXML childElementNamed:@"head" parentElement:tbXml.rootXMLElement];
                                                  if (htmlTag != nil)
                                                  {
                                                      [UtilityManager showAlert:NSLocalizedString(@"Technical Problem",@"Technical Problem") msg:NSLocalizedString(@"please try again later",@"please try again later")];
                                                      ReleaseAndNilify(responseXml);
                                                  }
                                                  
                                              }
                                          }
                                      }
                                      else if ( error != nil && [error code] == -1001 ) {
                                          DLog(@"Server timeout!%ld",(long)[error code] );
                                          [UtilityManager requestTimedOut];
                                          
                                      }
                                      dispatch_semaphore_signal(semaphore);
                                  }];
    // Start the task.
    [task resume];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    DLog(@"Response xml = %@", responseXml);
    return responseXml;
    
}
//let s = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
//if let url = NSURL(string: "http://my.url.request.com")
//{
//    download_requests.append(s)
//    s.dataTaskWithURL(url)
//    { (data, resp, error) -> Void in
//        // ....
//    }
//}
-(void)cancelSessionRequest : (NSString *) baseUrl
{

    [sessionForEstablishConnectionForGet getTasksWithCompletionHandler:^(NSArray<NSURLSessionDataTask *> * _Nonnull dataTasks, NSArray<NSURLSessionUploadTask *> * _Nonnull uploadTasks, NSArray<NSURLSessionDownloadTask *> * _Nonnull downloadTasks)
    {
    
        for (NSURLSessionDataTask * task in dataTasks)
        {
            if ([task.originalRequest.URL.absoluteString isEqualToString:baseUrl])
            {
                [task cancel];
                [sessionForEstablishConnectionForGet invalidateAndCancel];
            }
        }
    }
     ];
}


@end

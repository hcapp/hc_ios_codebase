//
//  AnythingPage.m
//  HubCiti
//
//  Created by Ajit Nadig on 11/13/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "AnythingPage.h"
#import "MainMenuViewController.h"
#import "EmailShareViewController.h"
#import "AnyViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "WebBrowserViewController.h"

@interface AnythingPage ()<UIScrollViewDelegate>
{
    float webviewHeight;
    UIActivityIndicatorView *activityIndicator;
}

@end

@implementation AnythingPage

@synthesize anyVC,emailSendingVC,titlelabel,imageViewOutlet,startDateOutlet,endDateOutlet,shortDescOutlet,longDescoutlet,heightConstraintForImage,bottomConstraintForImage,longWebViewHeight,shortWebViewHeight,scrollViewOutlet,detailContentViewOUTLET,stackViewOutlet;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    
    if ([defaults boolForKey:@"trainingSlides"]) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    else{
        self.navigationItem.rightBarButtonItem = mainPage;
    }
   
    self.navigationItem.hidesBackButton = YES;
    
    //hide scroll view untill web view is loaded.
    scrollViewOutlet.hidden = YES;
    shortDescOutlet.scrollView.scrollEnabled = NO;
    longDescoutlet.scrollView.scrollEnabled = NO;
    [self parse_AnythingPageInfo:[defaults valueForKey:KEY_RESPONSEXML]];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //customize back button
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [cusNav setTitle:@"Details" forView:self withHambergur:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    // Create down menu button
    UIImageView *homeLabel ;
    
    
    // Create up menu button
    homeLabel = [self createHomeButtonView];
    if ([defaults boolForKey:@"trainingSlides"]) {
        homeLabel.hidden = true;
    }
    else{
        homeLabel.hidden = false;
    }
    DWBubbleMenuButton *upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
                                                                                          self.view.frame.size.height - homeLabel.frame.size.height - 50.f,
                                                                                          homeLabel.frame.size.width,
                                                                                          homeLabel.frame.size.height)
                                                            expansionDirection:DirectionUp];
    upMenuView.homeButtonView = homeLabel;
    
    upMenuView.delegate = self;
    
    [upMenuView addButtons:[self createDemoButtonArray]];
    
    [self.view addSubview:upMenuView];
    
    upMenuView.onLogOutPressed =^(){
        
        [self logOutPressed];
    } ;
    
}


- (UIImageView *)createHomeButtonView {
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
    label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}

- (NSArray *)createDemoButtonArray {
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    
    int i = 0;
    for (NSString *title in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:title] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        
        button.clipsToBounds = YES;
        button.tag = i++;
        
        [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
}

- (void)test:(UIButton *)sender {
    
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
    [self shareClicked];
    
    switch (sender.tag) {
        case 0:{//facebook
            [self faceBookCliked];
        }
            break;
        case 1:{//twitter
            [self twitterClicked];
        }
            
            
            break;
        case 2:{//text
            
            [self textClicked];
            
        }
            break;
        case 3:{//email
            //[defaults setObject:PRODUCT_SHARE forKey:KEY_SHARE_TYPE];
            [self emailClick];
        }
            break;
            //        case 4:
            //            [actionSheet dismissWithClickedButtonIndex:4 animated:YES];
            //            break;
        default:
            [defaults setBool:NO forKey:BottomButton];
            break;
    }
    
    
}

- (UIButton *)createButtonWithName:(NSString *)imageName {
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button sizeToFit];
    
    [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}



- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView{
    
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}

#pragma mark navigation bar button actions
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}



-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

#pragma mark parse method
#pragma mark parse method
-(void)parse_AnythingPageInfo:(NSString*)response
{
    NSDictionary* responseData = (NSDictionary*) response;
    
    //activity indicator
    activityIndicator = [[UIActivityIndicatorView alloc]
                                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityIndicator.frame = CGRectMake(SCREEN_WIDTH/2 - VARIABLE_WIDTH(10), SCREEN_HEIGHT/2 - VARIABLE_HEIGHT(10), VARIABLE_WIDTH(20), VARIABLE_HEIGHT(20));
    [activityIndicator startAnimating];
    [self.view addSubview:activityIndicator];
   
    NSString* logoPath = [responseData objectForKey:@"logoImagePath"];
    if (![logoPath isEqualToString:@"N/A"]) {
        NSString *utf = [logoPath stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
        imageViewOutlet.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:utf]]];
    }
    else
    {
        heightConstraintForImage.constant = 0;
        bottomConstraintForImage.constant = 0;
    }
    NSString* pageTitle = [responseData objectForKey:@"pageTitle"];
    
    if (![pageTitle isEqualToString:@"N/A"])
    {
        titlelabel.text  = pageTitle;
        
    }
   
    NSString* startDate = [responseData objectForKey:@"startDate"];
    
    if (![startDate isEqualToString:@"N/A"])
    {
        startDateOutlet.text = [startDateOutlet.text stringByAppendingString:startDate];
        
    }
    else
    {
        startDateOutlet.hidden = YES;
    }
    NSString* endDate = [responseData objectForKey:@"endDate"];
    
    if (![endDate isEqualToString:@"N/A"])
    {
        endDateOutlet.text = [endDateOutlet.text stringByAppendingString:endDate];
        
    }
    else
    {
        endDateOutlet.hidden = YES;
    }
    
    NSString* sDescription = [responseData objectForKey:@"sDescription"];
    if (![sDescription isEqualToString:@"N/A"])
    {
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone)
        {
            [shortDescOutlet loadHTMLString:[NSString stringWithFormat:@"<body style='margin : 0; padding : 0;'><div style='font-size:12px;font-family:Helvetica;text-align:center;color:#000000;word-wrap: break-word'></body>%@",sDescription] baseURL:nil];
        }
        else
        {
            [shortDescOutlet loadHTMLString:[NSString stringWithFormat:@"<body style='margin : 0; padding : 0;'><div style='font-size:15px;font-family:Helvetica;text-align:center;color:#000000;word-wrap: break-word'></body>%@",sDescription] baseURL:nil];
        }
        
    }
    NSString* lDescription = [responseData objectForKey:@"longDescription"];
    if (![lDescription isEqualToString:@"N/A"])
    {
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone)
        {
            [longDescoutlet loadHTMLString:[NSString stringWithFormat:@"<body style='margin : 0; padding : 0;'><div id = 'foo' style='font-size:12px;font-family:Helvetica;text-align: center;color:#000000;margin : 0; padding : 0;word-wrap: break-word'></body>%@",lDescription] baseURL:nil];
        }
        else
        {
            [longDescoutlet loadHTMLString:[NSString stringWithFormat:@"<body style='margin : 0; padding : 0;'><div id = 'foo' style='font-size:15px;font-family:Helvetica;text-align: center;color:#000000;word-wrap: break-word'></body>%@",lDescription] baseURL:nil];
        }
        
    }

    
    
   /* TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *logoImagePathElement = [TBXML childElementNamed:@"logoImagePath" parentElement:tbxml.rootXMLElement];
    TBXMLElement *pageTitleElement = [TBXML childElementNamed:@"pageTitle" parentElement:tbxml.rootXMLElement];
    TBXMLElement *sDescriptionElement = [TBXML childElementNamed:@"sDescription" parentElement:tbxml.rootXMLElement];
    TBXMLElement *longDescriptionElement = [TBXML childElementNamed:@"longDescription" parentElement:tbxml.rootXMLElement];
    TBXMLElement *startDateElement = [TBXML childElementNamed:@"startDate" parentElement:tbxml.rootXMLElement];
    TBXMLElement *endDateElement = [TBXML childElementNamed:@"endDate" parentElement:tbxml.rootXMLElement];
    
    if (logoImagePathElement!=nil && ![[TBXML textForElement:logoImagePathElement]isEqualToString:@"N/A"])
    {
        NSString *utf = [[TBXML textForElement:logoImagePathElement] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
        imageViewOutlet.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:utf]]];

    }
    else
    {
        heightConstraintForImage.constant = 0;
        bottomConstraintForImage.constant = 0;
    }
    
    if (pageTitleElement!=nil && ![[TBXML textForElement:pageTitleElement]isEqualToString:@"N/A"])
    {
        titlelabel.text  = [TBXML textForElement:pageTitleElement];
       
    }
    if (startDateElement!=nil && ![[TBXML textForElement:startDateElement]isEqualToString:@"N/A"])
    {
        startDateOutlet.text = [startDateOutlet.text stringByAppendingString:[TBXML textForElement:startDateElement]];
        // sDate.attributedText = [self getTextStyle:sDateStr :@"Start Date"];
      
    }
    else
    {
        startDateOutlet.hidden = YES;
    }
    
    if (endDateElement!=nil && ![[TBXML textForElement:endDateElement]isEqualToString:@"N/A"])
    {
        endDateOutlet.text = [endDateOutlet.text stringByAppendingString:[TBXML textForElement:endDateElement]];
            // eDate.attributedText = [self getTextStyle:eDateStr :@"End Date"];
    }
    else
    {
        endDateOutlet.hidden = YES;
    }
    if (sDescriptionElement!=nil && ![[TBXML textForElement:sDescriptionElement]isEqualToString:@"N/A"])
    {
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone)
        {
            [shortDescOutlet loadHTMLString:[NSString stringWithFormat:@"<body style='margin : 0; padding : 0;'><div style='font-size:12px;font-family:Helvetica;text-align:center;color:#000000;word-wrap: break-word'></body>%@",[TBXML textForElement:sDescriptionElement]] baseURL:nil];
        }
        else
        {
             [shortDescOutlet loadHTMLString:[NSString stringWithFormat:@"<body style='margin : 0; padding : 0;'><div style='font-size:15px;font-family:Helvetica;text-align:center;color:#000000;word-wrap: break-word'></body>%@",[TBXML textForElement:sDescriptionElement]] baseURL:nil];
        }
        
    }

    
    if (longDescriptionElement!=nil && ![[TBXML textForElement:longDescriptionElement]isEqualToString:@"N/A"])
    {
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone)
        {
            [longDescoutlet loadHTMLString:[NSString stringWithFormat:@"<body style='margin : 0; padding : 0;'><div id = 'foo' style='font-size:12px;font-family:Helvetica;text-align: center;color:#000000;margin : 0; padding : 0;word-wrap: break-word'></body>%@",[TBXML textForElement:longDescriptionElement]] baseURL:nil];
        }
        else
        {
            [longDescoutlet loadHTMLString:[NSString stringWithFormat:@"<body style='margin : 0; padding : 0;'><div id = 'foo' style='font-size:15px;font-family:Helvetica;text-align: center;color:#000000;word-wrap: break-word'></body>%@",[TBXML textForElement:longDescriptionElement]] baseURL:nil];
        }

    }*/
}


//to get the attribute string for date

-(NSMutableAttributedString*) getTextStyle : (NSString*) completeText : (NSString*) editText
{
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:completeText];
    NSRange string_Range = [completeText rangeOfString:editText];
    [attributeStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:(IPAD ? 15.0f:12.0f)] range:string_Range];
    
    return attributeStr;
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark web view delegate
-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ([[[inRequest URL] scheme] isEqual:@"mailto"]) {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,(CFStringRef)value,
                                                                                                      CFSTR("")));
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            // [mailViewController release];
            return NO;
            
        }
        
        else {
            [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
            
            
        }
        
        return NO;
        
    }
    
    if ([[[inRequest URL] scheme] isEqual:@"tel"]) {
        NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
        
        NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
        NSString *phoneString  = [[defaultRecipient componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        NSString *phoneNumber = [NSString stringWithFormat:@"tel:%@", phoneString];
        NSURL    *url= [NSURL URLWithString:phoneNumber];
        
        UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        webview.alpha = 0.0;
        
        [webview loadRequest:[NSURLRequest requestWithURL:url]];
        
        // Assume we are in a view controller and have access to self.view
        if (inWeb.tag == 2)
        {
            [self.view insertSubview:webview belowSubview:longDescoutlet];
        }
        else
        {
            [self.view insertSubview:webview belowSubview:shortDescOutlet];
        }//ask ashika
        // [webview release];
        
        
        //        if ([[UIApplication sharedApplication] canOpenURL:[inRequest URL]])
        //        {
        //            NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
        //
        //            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
        //            NSString *phoneNumber = [@"tel://" stringByAppendingString:defaultRecipient];
        //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
        //
        //            return NO;
        //        }
        return NO;
    }
    
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        
        NSString *url = [[inRequest URL]absoluteString];
        [defaults setValue:url forKey:KEY_URL];
        //[defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
        
        WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:urlDetail animated:NO];
        //[urlDetail release];
        
        return NO;
    }
    return YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *output = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"];
    NSLog(@"height: %@", output);
    webviewHeight = [output integerValue];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.automaticallyAdjustsScrollViewInsets =  NO;
    if(webView.tag == 1)
    {
        shortWebViewHeight.constant = webviewHeight;
    }
    if(webView.tag == 2)
    {
       longWebViewHeight.constant = webviewHeight;
        [detailContentViewOUTLET updateConstraints];
       [activityIndicator stopAnimating];
        scrollViewOutlet.hidden = NO;
        
        
    }
}



#pragma mark sender user tracking
-(void)userTracking:(NSString*)xmlStr
{
    
    NSMutableString *urlStr = [[NSMutableString alloc]init];
    [urlStr appendFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    DLog(@"%@",urlStr);
    NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:xmlStr]];
    DLog(@"%@",response);
    // [xmlStr release];
}

#pragma mark share methods

-(void)shareClicked
{
    if (shareFlag==TRUE) {
        iWebRequestState = SPECIAL_OFFER_SHARE;
    }
    else if (anythingShareFlag ==TRUE)
    {
        iWebRequestState = ANYTHING_SHARE;
    }
    
    
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    [reqStr appendFormat:@"<ShareProductInfo>"];
    
    if (requestAnythingFromMainMenu==FALSE) {
        [reqStr appendFormat:@"<retailerId>%@</retailerId>",[defaults valueForKey:KEY_RETAILERID ]];
    }
    
    
    
    //    [reqStr appendFormat:@"<ShareProductInfo><retailerId>%@</retailerId>",[defaults valueForKey:KEY_RETAILERID ]];
    if (shareFlag==TRUE) {
        [reqStr appendFormat:@"<pageId>%@</pageId>",[defaults valueForKey:KEY_PAGEID]];
    }
    else if (anythingShareFlag==TRUE)
    {
        [reqStr appendFormat:@"<pageId>%@</pageId>",[defaults valueForKey:KEY_ANYPAGEID]];
    }
    
    [reqStr appendFormat:@"<userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharespecialoff",BASE_URL];
    DLog(@"%@",urlString);
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    
    [self responseData:response];
    
    //[reqStr release];
    
}


-(void)shareDetails{
    if (userIdentifier==TRUE) {
        UIAlertController * alert;
        
            alert=[UIAlertController alertControllerWithTitle:@"Sign up for free to use this feature" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Continue"
                             style:UIAlertActionStyleCancel
                             handler:nil];
        [alert addAction:ok];
        UIAlertAction* signup = [UIAlertAction
                             actionWithTitle:@"Sign up"
                             style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                      [self logOutPressed];
                                 }];
        [alert addAction:signup];

        [self presentViewController:alert animated:YES completion:nil];

    }
    else
    {
        [self shareClicked];
        
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        // [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
    }
}
-(void) faceBookCliked {
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    if (shareFlag==TRUE) {
        anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"ANYTHING_SHARE"];
    }
    else if(anythingShareFlag==TRUE)
    {
        anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
    }
    
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];

}


-(void) twitterClicked{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
                NSMutableString *userXmlStr;
                if (shareFlag==TRUE) {
                    userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><pageId>%@</pageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PAGEID]];
                }
                else {
                    if (requestAnythingFromMainMenu==FALSE) {
                        userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Email</shrTypNam><aPageId>%@</aPageId><retId>%@</retId><retLocId>%@</retLocId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_ANYPAGEID],[defaults valueForKey:KEY_RETAILERID],[defaults valueForKey:@"retailLocationID"]];
                    }
                    else{
                        userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><aPageId>%@</aPageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_ANYPAGEID]];
                    }
                }
                
                
                [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
        
        
    }
    else {
        [UtilityManager showAlert:@"Cannot Share" msg:@"Please Go To Settings and Set Up A Twitter Account"];
    }

    
}


-(void) textClicked{
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:@"Oops, SMS not supported.." msg:@"Looks like the sim card is missing or the device does not support texting"];

    }

}

-(void) emailClick{
    
    
    emailSendingVC = [[EmailShareViewController alloc]
                      initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    if (anythingShareFlag==true) {
        emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"ANYTHING_SHARE"];
    }
    else if(shareFlag==true)
    {
        emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
    }
    __typeof(self) __weak  obj = self;
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
}

-(void) showActionSheet {
    NSString *titleString;
    if (shareFlag==TRUE)
        titleString = @"Share Special Offer Via";
    else
        titleString =@"Share Anything Page Via";
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:titleString message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];

    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
//    if(IPAD){
//        actionSheet.popoverPresentationController.sourceView = btnCalendar;
//        actionSheet.popoverPresentationController.sourceRect =  btnCalendar.bounds;
//    }

    [self presentViewController:actionSheet animated:YES completion:nil];
    
}


-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
       
            
            NSMutableString *userXmlStr;
            if (shareFlag==TRUE) {
                userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><pageId>%@</pageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PAGEID]];
            }
            else {
                userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><aPageId>%@</aPageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_ANYPAGEID]];
            }
            
            
            [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];

        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}



#pragma mark parse method
-(void)responseData: (NSString*)response{
    
    switch (iWebRequestState) {
            
        case SPECIAL_OFFER_SHARE:
        case ANYTHING_SHARE:
            [self parse_SpecialOfferShare:response];
            break;
        default:
            break;
    }
    
    
}

-(void)parse_SpecialOfferShare:(NSString*)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        //        TBXMLElement *retailerDetails = [TBXML childElementNamed:@"RetailersDetails" parentElement:tbXml.rootXMLElement];
        
        //        if (retailerDetails!=nil) {
        TBXMLElement *shareText=[TBXML childElementNamed:@"pageTitle" parentElement:tbXml.rootXMLElement];
        TBXMLElement *emailURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *imagePath = [TBXML childElementNamed:@"retImage" parentElement:tbXml.rootXMLElement];
        
        if (emailURL!=nil) {
            [defaults setValue:[TBXML textForElement:emailURL] forKey:KEY_HYPERLINK];
            
        }
        
        if (shareText!=nil) {
            NSMutableString *msgString = [[NSMutableString alloc]init];
            if (shareFlag==YES) {
                [msgString appendFormat:@"I found this special offer page in the %@ mobile app and thought you might be interested:",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            }
            else
            {
                [msgString appendFormat:@"I found this anything page in the %@ mobile app and thought you might be interested:",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            }
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [msgString appendFormat:@"%@",shareString];
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            
        }
        
        if (imagePath!=nil) {
            [defaults setObject:[TBXML textForElement:imagePath] forKey:KEY_PRODUCTIMGPATH];
        }
        
        //        }
        
        
        
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];

            return;
        }
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

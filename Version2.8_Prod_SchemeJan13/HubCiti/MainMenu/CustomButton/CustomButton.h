//
//  CustomButton.h
//  ButtonDemo
//
//  Created by ionnor on 9/3/13.
//  Copyright (c) 2013 ionnor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomButton : UIButton
{
    
}
- (id)initWithFrame:(id)del frame:(CGRect)frame;
@end


// Create category for UIColor to convert the hexa value into UIColor
@interface UIColor (CreateMethods)

// wrapper for [UIColor colorWithRed:green:blue:alpha:]
// values must be in range 0 - 255
+ (UIColor*)colorWith8BitRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha;

// Creates color using hex representation
// hex - must be in format: #FF00CC
// alpha - must be in range 0.0 - 1.0
+ (UIColor*)colorWithHex:(NSString*)hex alpha:(CGFloat)alpha;

@end
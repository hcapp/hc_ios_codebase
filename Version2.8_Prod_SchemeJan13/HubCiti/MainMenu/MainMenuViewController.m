//
//  MainMenuViewController.m
//  HubCiti
//
//  Created by Anjana on 10/3/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "MainMenuViewController.h"
#import "SettingsViewController.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "ButtonSetupViewController.h"
#import "SESpringBoard.h"
#import "bottomButtonView.h"
#import "CityExperienceViewController.h"
#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "FindViewController.h"
#import "AnythingPage.h"
#import "HubCitiConstants.h"
#import "AlertsList.h"
#import "AppDelegate.h"
#import "SingleCatRetailers.h"
#import "ScanNowScreen.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "EventsListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FAQCategoryList.h"
#import "FilterListViewController.h"
#import "FundraiserListViewController.h"
#import "SwipesViewController.h"
#import "SpecialOffersViewController.h"
#import "GovQALoginPageViewController.h"
#import "ReportProblemViewController.h"
#import "HTTPClient.h"
#import "MainMenuRequest.h"
#import "MainMenuResponse.h"
#import "MenuItemList.h"
#import "BottomItemList.h"
#import "UserSettingsController.h"
#import "CityPreferenceViewController.h"
#import "GetUserInfoResponse.h"
#import "BandViewController.h"
#import "MFSideMenu.h"
#import "SideViewController.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "Marquee.h"
#import "BandsNearByEventsMapTable.h"
#import "NewsDetailViewController.h"
#import "Marquee.h"
#import "CouponsInDealViewController.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponsInDealViewController.h"

BOOL isSameUser ;
@interface MainMenuViewController () <HTTPClientDelegate,NewsTickerDelegate>
@property(nonatomic,retain) MainMenuResponse* menuResponse ;
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@end

@implementation MainMenuViewController{
    BOOL bottomBtn;
    NSString *menuItemName;
    bottomButtonView *viewButton;
    NSMutableArray *array_NewsTickerResponse;
    MarqueeType* marqType;
    CustomizedNavController *cusNav;
}
@synthesize menuResponse,iViewController,anyVC,infoResponse,emailSendingVC,bandViewPage,scrollView,combView,blockView;
//@synthesize menuResponse,iViewController,anyVC,emailSendingVC;
static NSMutableDictionary *dict_linkTypeName_BottomButton = nil;
static NSMutableDictionary *dict_linkTypeName_MenuButton = nil;

NSMutableString *previousMenuItem,*nextMenuItem;

#pragma mark - Service Respone methods

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    [self responseData:responseObject];
    
}

-(templetsMenuState) getTemplateName
{
    if([menuResponse.templateName isEqualToString:@"Grouped Tab"])
        iTempletsMenuId = GroupedTab;
    else if([menuResponse.templateName  isEqualToString:@"Single/Two Column Tab"])
        iTempletsMenuId = TwoColumnTab;
    else if([menuResponse.templateName  isEqualToString:@"List View"])
        iTempletsMenuId = ListView;
    else if ([menuResponse.templateName isEqualToString:@"Four Tile Template"])
        iTempletsMenuId = FourTile;
    else if ([menuResponse.templateName isEqualToString:@"Two Tile Template"])
        iTempletsMenuId = TwoTile;
    else if ([menuResponse.templateName isEqualToString:@"Two Tile Template With Banner"])
        iTempletsMenuId = TwoTileWithBanner;
    else if([menuResponse.templateName isEqualToString:@"Iconic Grid"] || [menuResponse.templateName isEqualToString:@"Square Grid"] || [menuResponse.templateName isEqualToString:@"Rectangular Grid"] || [menuResponse.templateName isEqualToString:@"4X4 Grid"]||[menuResponse.templateName isEqualToString:@"Two Image Template"]||[menuResponse.templateName isEqualToString:@"Square Image Template"]||[menuResponse.templateName isEqualToString:@"Iconic 3X3"] || [menuResponse.templateName isEqualToString:@"Square Grid  3x3"]){
        
        iTempletsMenuId = IconicGrid;
        if ([menuResponse.templateName isEqualToString:@"Square Grid"]) {
            [defaults setBool:YES forKey:@"SquareGrid"];
        }
        else if ([menuResponse.templateName isEqualToString:@"Rectangular Grid"]) {
            [defaults setBool:YES forKey:@"RectangularGrid"];
        }
        else if ([menuResponse.templateName isEqualToString:@"4X4 Grid"]) {
            [defaults setBool:YES forKey:@"4X4 Grid"];
        }
        else if([menuResponse.templateName isEqualToString:@"Two Image Template"]){
            [defaults setBool:YES forKey:@"TwoImageTemplate"];
        }
        else if([menuResponse.templateName isEqualToString:@"Square Image Template"]){
            [defaults setBool:YES forKey:@"Square Image Template"];
        }
        else if([menuResponse.templateName isEqualToString:@"Iconic 3X3"]){
            [defaults setBool:YES forKey:@"Iconic3X3"];
        }
        else if( [menuResponse.templateName isEqualToString:@"Square Grid  3x3"]){
            [defaults setBool:YES forKey:@"SquareGrid3x3"];
        }
        
    }
    else if([menuResponse.templateName  isEqualToString:@"Two Column Tab with Banner Ad"])
        iTempletsMenuId = TwoColumnTabWithBannerAd;
    else if([menuResponse.templateName  isEqualToString:@"Combo Template"])
        iTempletsMenuId = ComboTemplate;
    else if([menuResponse.templateName  isEqualToString:@"Grouped Tab With Image"])
        iTempletsMenuId = GroupedTabWithImage;
    
    else if([menuResponse.templateName  isEqualToString:@"List View with Banner Ad"])
        iTempletsMenuId = ListViewWithBannerAd;
    
    else
        iTempletsMenuId = GroupedTab;
    
    return iTempletsMenuId;
}

-(void) filetrMenuList
{
    arrMenuItems = [[NSMutableArray alloc] init];
    for( int i = 0; i< menuResponse.arMItemList.count ; i++) {
        
        MenuItemList* mainMenuDO = [[MenuItemList alloc] init];
        NSDictionary* dictList = menuResponse.arMItemList[i];
        [mainMenuDO setValuesForKeysWithDictionary:dictList];
        //label color for template
        if([menuResponse.templateName isEqualToString:@"Two Image Template"]||[menuResponse.templateName isEqualToString:@"Square Image Template"]){
            
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            
            if(menulevel > 1)
                [defaults setValue:mainMenuDO.smBtnFontColor forKey:@"menuBtnFontColor"];
            else
                [defaults setValue:mainMenuDO.mBtnFontColor forKey:@"menuBtnFontColor"];
            
        }
        
        if ([mainMenuDO.mShapeName isEqualToString:@"Square"] || [mainMenuDO.mShapeName isEqualToString:@"Circle"]) {
            mainMenuDO.smFontColor = menuResponse.smFontColor;
        }
        if(mainMenuDO.linkTypeName){
            
            [previousMenuItem setString:@""];
            [previousMenuItem appendString:nextMenuItem];
            [nextMenuItem setString:@""];
            [nextMenuItem appendString:mainMenuDO.linkTypeName];
            
        }
        // Added for Grouped tab / Grouped List View With Images
        if ([previousMenuItem isEqualToString:@"Text"]&&[nextMenuItem isEqualToString:@"Text"]) {
            
            [arrMenuItems removeLastObject];
            
        }
        
        [arrMenuItems addObject:mainMenuDO];
        
    }
    
    if ([arrMenuItems count]>1&&[nextMenuItem isEqualToString:@"Text"]) {
        [arrMenuItems removeLastObject];
    }
    
}
-(void) saveMenuDisplay
{
   
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
    {
        
        //self.navigationItem.title = @"Submenu";
        //customize home button
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:CGRectMake(0, 0, 30, 30)];
        SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
        [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
        NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
        
        [btn addSubview:homeImage];
        
        [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
        mainPage.style = UIBarButtonItemStylePlain;
        
        self.navigationItem.rightBarButtonItem = mainPage;
        self.navigationItem.hidesBackButton = YES;
        
        cusNav.customNavBardelegate = self;
        [cusNav hideBackButton:NO];
        [cusNav hideHambergerButton:NO];
        //customize back button
        //        UIButton *backBtn = [UtilityManager customizeBackButton];
        //        [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
        //
        //
        //        UIButton *hamburgerBtn = [NewsUtility customizedSideMenu:false];
        //
        //
        //        UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        //        UIBarButtonItem *hamburger = [[UIBarButtonItem alloc] initWithCustomView:hamburgerBtn];
        //
        //        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:back,hamburger, nil]];
        
        
    }
    else
    {
        
        [cusNav hideBackButton:YES];
        [cusNav hideHambergerButton:NO];
        //        UIButton *hamburger =  [NewsUtility customizedSideMenu:true];
        //        UIBarButtonItem *buttonLeft= [[UIBarButtonItem alloc] initWithCustomView:hamburger];
        //        
        //        self.navigationItem.leftBarButtonItem=buttonLeft;
        
    }
    
}

- (void)leftSideMenuButtonPressed:(id)sender
{
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
    
}

-(void) renderMenuDisplay : (id) responseObject
{
    // Added for push Notification
    if (userIdentifier==FALSE)
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {// iOS 8.0 and later
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        } else { // below iOS 8.0
            
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        }
    }
    
    
    [defaults setValue:Nil forKey:@"TopBannerImage"];
    arrMenuBottomButtonDO = [[NSMutableArray alloc]init];
    if (responseObject == nil)
        return;
    if (menuResponse == nil) {
        menuResponse = [[MainMenuResponse alloc] init];
    }
    [menuResponse setValuesForKeysWithDictionary:responseObject];
    
    
    NSLog(@"response = %@", menuResponse);
    
    NSLog(@"date = %@", menuResponse.dateModified);
    //Navigation bar
    if(self.menuResponse.titleBkGrdColor)
    {
        [defaults setValue:self.menuResponse.titleBkGrdColor forKey:@"titleBkGrdColor"];
    }
    if(self.menuResponse.titleTxtColor)
    {
        [defaults setValue:self.menuResponse.titleTxtColor forKey:@"titleTxtColor"];
    }
    if(self.menuResponse.homeImgPath)
    {
        [defaults setValue:self.menuResponse.homeImgPath forKey:@"homeImgPath"];
    }
    if(self.menuResponse.bkImgPath)
    {
      if (![[defaults valueForKey:@"bkImgPath"] isEqualToString:self.menuResponse.bkImgPath])
      {
          [cusNav changeBackImage:self.menuResponse.bkImgPath];
      }
        [defaults setValue:self.menuResponse.bkImgPath forKey:@"bkImgPath"];
    }
    if(self.menuResponse.hamburgerImg)
    {
        NSLog(@"ham image %@",[defaults valueForKey:@"hamburgerImg"]);
        if (![[defaults valueForKey:@"hamburgerImg"] isEqualToString:self.menuResponse.hamburgerImg])
        {
            [defaults setValue:self.menuResponse.hamburgerImg forKey:@"hamburgerImg"];
            [cusNav changeHambugerImage];
        }
        
        
        
    }
    if(self.menuResponse.pgnColorActive)
    {
        [defaults setValue:self.menuResponse.pgnColorActive forKey:@"pgnColorActive"];
    }
    if(self.menuResponse.pgnColorInActive)
    {
        [defaults setValue:self.menuResponse.pgnColorInActive forKey:@"pgnColorInActive"];
    }
    [self saveMenuDisplay];
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if ([menuResponse.responseCode isEqualToString:@"10000"] ) {
        
        NSLog(@"MAinmenu:%@",[defaults valueForKey:@"titleBkGrdColor"]);
        [HubCitiAppDelegate navigationColor];
        self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.navigationController.navigationBar setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:
          [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]],
          NSForegroundColorAttributeName,[UIFont fontWithName:@"Helvetica-Bold" size:17.0],
          NSFontAttributeName,nil]];
        /* to update all color and image changes*/
     
        
        if (menuResponse.isTempChanged) {
            
            [defaults setInteger:0 forKey:@"currentIndex"];
            [defaults setBool:NO forKey:@"scrolling"];
            [defaults setBool:NO forKey:@"PaginationCalled"];
            [defaults setValue:nil forKey:@"bannerImgScroll"];
            [defaults setInteger:0 forKey:@"currentIndexSubPage"];
            
            if([menuResponse.templateName isEqualToString:@"Scrolling News Template"] ){
                [cusNav hideBackButton:YES];
                [cusNav hideHambergerButton:YES];
                [defaults setValue:@"Scrolling News Template" forKey:@"selectedTemplateName"];
                NSLog(@"---->%@",[defaults valueForKey:KEY_HUBCITIID]);
                if(menulevel>1){
                [NewsUtility setNewsAsSubMenu:self:menulevel];
                [self skipMiddleScreen];
                }
                else{
                    [NewsUtility setNewsTemplateAsRoot];
                }
                return;
            }
            else if([menuResponse.templateName isEqualToString:@"News Tile Template"] )
            {
                [cusNav hideBackButton:YES];
                [cusNav hideHambergerButton:YES];
                [defaults setValue:@"News Tile Template" forKey:@"selectedTemplateName"];
                if(menulevel>1){
                   
                    [NewsUtility setNewsAsSubMenu:self: menulevel];
                    [self skipMiddleScreen];
                }
                else{
                    [NewsUtility setNewsTemplateAsRoot];
                }
                return;
                
            }
            
            else  if([menuResponse.templateName isEqualToString:@"Combination News Template"] ){
                [cusNav hideBackButton:YES];
                [cusNav hideHambergerButton:YES];
                [defaults setValue:@"Combination News Template" forKey:@"selectedTemplateName"];
                if(menulevel>1){
                    [NewsUtility setNewsAsSubMenu:self: menulevel];
                     [self skipMiddleScreen];
                }
                else{
                    [NewsUtility setNewsTemplateAsRoot];
                }
                return;
            }
            
            else{
                [self saveMenuData];
                if (menuResponse.dateModified) {
                    
                    [defaults setObject:menuResponse.dateModified forKey:@"DateModified"];
                    [dateCreated setValue:menuResponse.dateModified forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
                    
                    if (cashedResponse == nil) {
                        cashedResponse = [[NSMutableDictionary alloc] init];
                    }
                    
                    [cashedResponse setValue:responseObject forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
                    
                }
                if([menuResponse.arItemList count] > 0)
                {
                   
                    array_NewsTickerResponse = [[NSMutableArray alloc] init];
                   
                    for (int i = 0; i< menuResponse.arItemList.count; i++) {
                        
                        NewsTickerList *obj_newsTickerList = [[NewsTickerList alloc]init];
                        NSDictionary* dictList = menuResponse.arItemList[i];
                        [obj_newsTickerList setValuesForKeysWithDictionary:dictList];
                        [array_NewsTickerResponse addObject:obj_newsTickerList];
                    }
                    
                }

                
                if([menuResponse.arBottomBtnList count] > 0)
                {
                    for (int i = 0; i< menuResponse.arBottomBtnList.count; i++) {
                        
                        BottomItemList * arBItemList = [[BottomItemList alloc] init];
                        [arBItemList initTheDictionary];
                        NSDictionary* dictList = menuResponse.arBottomBtnList[i];
                        
                        [arBItemList setValuesForKeysWithDictionary:dictList];
                        
                        
                        [arrMenuBottomButtonDO addObject:arBItemList];
                    }
                    [self setBottomBarMenu];
                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                }
                else
                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
                
                iTempletsMenuId = [self getTemplateName];
                [self filetrMenuList];
                [self callTemplate: menuResponse.arMItemList Images:menuResponse.arMItemList mainMenuItemObjects:responseObject];
            }
        }
        
        else{
            [self releaseAllocatedObjects];
            
            [self renderMenuDisplay:[cashedResponse valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]]];
            
        }
        
        
    }
    
    else if([menuResponse.responseCode isEqualToString:@"10002"])
    {
        NSString *response= menuResponse.responseText;
        [UtilityManager showAlert:nil msg:response];
        return;
        
    }
    else if([menuResponse.responseCode isEqualToString:@"10005"])
    {
        
        NSString *response= menuResponse.responseText;
        
        if ([response isEqualToString:@"null"]) {
            
            response = @"No Records Found.";
            
        }
        if([response isEqualToString:@"No Records Found."])
        {
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:response];
        }
        else{
            
            NSString *responseTextStr = [menuResponse.responseText stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
            
            [UtilityManager showFormatedAlert:responseTextStr];
        }
    }
    else
    {
        NSString *response = menuResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:response];
    }
    
    
    
    if(menulevel > 1)
    {
        if(menuResponse.menuName)
            [cusNav setTitle:menuResponse.menuName forView:self withHambergur:YES];
        else
            [cusNav setTitle:[defaults valueForKey:KEY_MITEMNAME] forView:self withHambergur:YES];
        
    }
    
    if ([menuResponse.templateName isEqualToString:@"Rectangular Grid"] || [menuResponse.templateName isEqualToString:@"4X4 Grid"]|| [menuResponse.templateName isEqualToString:@"Two Image Template"]||[menuResponse.templateName isEqualToString:@"Square Image Template"]  ||[menuResponse.templateName isEqualToString:@"Four Tile Template"]||[menuResponse.templateName isEqualToString:@"Two Tile Template"]) {
        
        [defaults setBool:YES forKey:@"NameSortRectGrid"];
    }
    else
        [defaults setBool:NO forKey:@"NameSortRectGrid"];
    
}
-(void) skipMiddleScreen
{
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    
    // You can pass your index here
    NSInteger vcCount = [viewControllers count];
    
    for(NSInteger i = vcCount - 1  ; i >= 0 ;i--) {
        id viewController = [viewControllers objectAtIndex:i];
        if ([viewController isKindOfClass: [self class]]) {
            [viewControllers removeObjectAtIndex: i];
            break;
        }
        
        NSLog(@"viewc:%@",viewController);
    }
    
    self.navigationController.viewControllers = viewControllers;
}
-(void) addNewsTicker
{
    Marquee* marq = [[Marquee alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, MARQEE_HEIGHT)];
    marq.newsTickerDelegate = self;
    marq.textColor = self.menuResponse.tickerTxtColor;
    marq.tickerBkgrdColor = self.menuResponse.tickerBkgrdColor;
    
    if ([self.menuResponse.tickerMode isEqualToString:@"Scrolling"]) {
        
        if ([self.menuResponse.tickerDirection isEqualToString:@"Left"]) {
            [marq startAnimateTickerWithType:SCROLL_RIGHT_TO_LEFT  :array_NewsTickerResponse :self.view];
        }
        if ([self.menuResponse.tickerDirection isEqualToString:@"Right"]) {
            [marq startAnimateTickerWithType:SCROLL_LEFT_TO_RIGHT :array_NewsTickerResponse :self.view];
        }
        if ([self.menuResponse.tickerDirection isEqualToString:@"Up"]) {
            [marq startAnimateTickerWithType:SCROLL_DOWN_TO_UP :array_NewsTickerResponse :self.view];
        }
        if ([self.menuResponse.tickerDirection isEqualToString:@"Down"]) {
            [marq startAnimateTickerWithType:SCROLL_UP_TO_DOWN :array_NewsTickerResponse :self.view];
        }
        
    }
    else{
        if ([self.menuResponse.tickerDirection isEqualToString:@"Left"]) {
            [marq startAnimateTickerWithType:ROTATE_RIGHT_TO_LEFT :array_NewsTickerResponse :self.view];
        }
        if ([self.menuResponse.tickerDirection isEqualToString:@"Right"]) {
            [marq startAnimateTickerWithType: ROTATE_LEFT_TO_RIGHT:array_NewsTickerResponse :self.view];
        }
        if ([self.menuResponse.tickerDirection isEqualToString:@"Up"]) {
            [marq startAnimateTickerWithType:ROTATE_DOWN_TO_UP :array_NewsTickerResponse :self.view];
        }
        if ([self.menuResponse.tickerDirection isEqualToString:@"Down"]) {
            [marq startAnimateTickerWithType: ROTATE_UP_TO_DOWN :array_NewsTickerResponse :self.view];
        }
        
    }
    
}


-(void) saveMenuData
{
    if (self.menuResponse.downLoadLink){
        
        [defaults setValue:self.menuResponse.downLoadLink forKey:KEY_HYPERLINK];
    }
    if (self.menuResponse.androidLink){
        
        androidDownloadLink = self.menuResponse.androidLink;
    }
    else
        androidDownloadLink = @"";
    
    [defaults setValue:self.menuResponse.androidLink forKey:@"androidDownloadLink"];
    
    if (self.menuResponse.appIconImg){
        
        [defaults setValue:self.menuResponse.appIconImg forKey:KEY_PRODUCTIMGPATH];
    }
    
    if (self.menuResponse.menuName)
        [defaults setValue:self.menuResponse.menuName forKey:KEY_MENUNAME];
    
    [defaults setValue:@"0" forKey:@"noOfColumns"];
    if(self.menuResponse.noOfColumns)
        [defaults setValue:[NSString stringWithFormat:@"%d", self.menuResponse.noOfColumns]  forKey:@"noOfColumns"];
    if(self.menuResponse.retGroupImg){
        [defaults setValue:self.menuResponse.retGroupImg forKey:@"retGroupImg"];
        
        //add the highlighted imagepath
        NSString *path = [defaults valueForKey:@"retGroupImg"];
        NSString *extn = [path pathExtension]; // This is .png in this case
        NSString *finalPath = [[[path stringByDeletingPathExtension] stringByAppendingString:@"_On"] stringByAppendingPathExtension:extn];
        [defaults setValue:finalPath forKey:@"retGroupImg_On"];
    }
    else{
        [defaults setValue:nil forKey:@"retGroupImg"];
        [defaults setValue:nil forKey:@"retGroupImg_On"];
    }
    
    if(self.menuResponse.departFlag)
        [defaults setValue:[NSString stringWithFormat:@"%d", self.menuResponse.departFlag]  forKey:@"departFlag"];
    else
        [defaults setValue:@"0"  forKey:@"departFlag"];
    
    NSLog(@"dept: %@",[defaults valueForKey:@"departFlag"]);
    if(self.menuResponse.typeFlag)
        [defaults setValue:[NSString stringWithFormat:@"%d", self.menuResponse.typeFlag] forKey:@"typeFlag"];
    else
        [defaults setValue:@"0"  forKey:@"typeFlag"];
    
    if(self.menuResponse.menuId)
        [defaults setValue:self.menuResponse.menuId forKey:@"menuId"];
    if(self.menuResponse.isRegApp)
        [defaults setValue:[NSString stringWithFormat:@"%d", self.menuResponse.isRegApp] forKey:RegionApp];
    
    //[defaults setValue:self.menuResponse.hubCitiId forKey:KEY_HUBCITIID];
    
    if(self.menuResponse.mBannerImg)
        [defaults setValue:self.menuResponse.mBannerImg forKey:@"TopBannerImage"];
    else
        [defaults setValue:Nil forKey:@"TopBannerImage"];
    if([menuResponse.templateName isEqualToString:@"Two Image Template"]||[menuResponse.templateName isEqualToString:@"Square Image Template"]||[menuResponse.templateName isEqualToString:@"Four Tile Template"]||[menuResponse.templateName isEqualToString:@"Two Tile Template"]||[menuResponse.templateName isEqualToString:@"Two Tile Template With Banner"]){
        
        if([menuResponse.isDisplayLabel intValue]==1){
            [defaults setValue:@"1" forKey:@"isDisplayLabel"];
        }
    }
    else{
        [defaults setValue:nil forKey:@"isDisplayLabel"];
    }
    [SharedManager setFiltersCount:self.menuResponse.retAffCount];
    [defaults setValue:self.menuResponse.retAffName forKey:@"Title"];
    [SharedManager setRetAffId:self.menuResponse.retAffId];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if (dict_linkTypeName_BottomButton == nil) {//assigning number to each bottom buttons - in order to
            dict_linkTypeName_BottomButton = [[NSMutableDictionary alloc]init];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:0] forKey:@"Hot Deals"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:1] forKey:@"Scan Now"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:2] forKey:@"Alerts"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:3] forKey:@"Events"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:4] forKey:@"Whats NearBy"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:5] forKey:@"Find"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:6] forKey:@"City Experience"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:7] forKey:@"City Services"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:8] forKey:@"Visitors Bureau"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:9] forKey:@"Transportation"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:10] forKey:@"Preference"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:11] forKey:@"About"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:12] forKey:@"Share"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:13] forKey:@"Settings"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:14] forKey:@"AnythingPage"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:15] forKey:@"AppSite"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:16] forKey:@"SubMenu"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:17] forKey:@"Filters"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:18] forKey:@"Coupon"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:19] forKey:@"Deals"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:20] forKey:@"FAQ"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:21] forKey:@"Fundraisers"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:22] forKey:@"User Information"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:23] forKey:@"Category Favorites"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:24] forKey:@"Location Preferences"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:25] forKey:@"City Favorites"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:26] forKey:@"Map"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:27] forKey:@"SortFilter"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:28] forKey:@"Filter"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:29] forKey:@"My Deals"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:30] forKey:@"Privacy Policy"];
        }
        
        if (dict_linkTypeName_MenuButton == nil) {//assigning number to each menu buttons - in order to
            dict_linkTypeName_MenuButton = [[NSMutableDictionary alloc]init];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:0] forKey:@"City Experience"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:1] forKey:@"Coupon"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:2] forKey:@"AppSite"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:3] forKey:@"AnythingPage"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:4] forKey:@"Alerts"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:5] forKey:@"Deals"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:6] forKey:@"Find"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:7] forKey:@"Scan Now"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:8] forKey:@"Whats NearBy"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:9] forKey:@"SubMenu"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:10] forKey:@"Events"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:11] forKey:@"Preference"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:12] forKey:@"About"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:13] forKey:@"Settings"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:14] forKey:@"Share"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:15] forKey:@"Filters"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:16] forKey:@"FAQ"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:17] forKey:@"Fundraisers"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:18] forKey:@"User Information"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:19] forKey:@"Category Favorites"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:20] forKey:@"Location Preferences"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:21] forKey:@"City Favorites"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:10] forKey:@"Band Events"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:22] forKey:@"Playing Today"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:24] forKey:@"My Deals"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:25] forKey:@"Privacy Policy"];
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(showResetPasswordAlert:)
    //                                                 name:@"tempPassword"
    //                                               object:nil];
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(showEmailCountAlert:)
    //                                                 name:@"emailCount"
    //                                               object:nil];
    
    //[defaults setValue:nil forKey:KEY_MAINMENUID];
    NSLog(@"scrollindicator:%d",[SharedManager scrollIndicator]);
    
    // Do any additional setup after loading the view from its nib.
    [self.view setAccessibilityLabel:@"Mainmenu"];
    bottomBtn = 0;
    [defaults setBool:YES forKey:@"LoginSuccess"];
    
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    //    self.navigationItem.title = NSLocalizedString(@"Main Menu",@"Main Menu");
    self.navigationItem.hidesBackButton=YES;
    [defaults setBool:NO forKey:@"hideBackBtn"];
    [defaults setValue:@"1" forKey:@"CityPreferenceUpdated"];
    previousMenuItem = [[NSMutableString alloc]init];
    nextMenuItem = [[NSMutableString alloc]init];
    
    [previousMenuItem setString:@""];
    [nextMenuItem setString:@""];
    
    [defaults setValue:@"0" forKey:@"departmentId"];
    [defaults setValue:@"0" forKey:@"typeId"];
    [defaults setValue:@"NONE" forKey:@"sortOrder"];
    //    [defaults setValue:0 forKey:@"groupSelectionVal"];
    //    [defaults setValue:0 forKey:@"sortSelectionVal"];
    [defaults setValue:nil forKey:@"commaSeperatedCities"];
    
    [defaults setValue:nil forKey:@"isComingFromSubMenuGroupingAndSorting"];
    
    SingleCatFlag=FALSE;
    if (SingleEventCatID!=Nil) {
        [SingleEventCatID removeAllObjects];
    }
    
    // If called for Submenu
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
    {
        if([SharedManager submenuScrollIndicator]!=0){
            
            [SharedManager setSubmenuScrollIndicator:0];
        }
        
        
    }
    else
    {
        
        //modified scroll indicator
        if([SharedManager scrollIndicator]!=0){
            
            [SharedManager setScrollIndicator:0];
        }
        NSLog(@"scrollindicator:%d",[SharedManager scrollIndicator]);
        
        // //NSlog(@"The user type is %d",userIdentifier);
        if (userIdentifier==FALSE && ![defaults boolForKey:@"pushOnKill"]){
            [self checkLocationStatus];
            [self checkForRadius];
        }
        else if([defaults boolForKey:@"pushOnKill"]){
            [defaults setBool:NO forKey:@"pushOnKill"];
        }
        
    }
    
   
    
}
-(void) initMenuData
{
    //[defaults setInteger:0 forKey:@"CityExpTableTag"];
    //[tblMenuListView setUserInteractionEnabled:YES];
    [defaults setBool:NO forKey:@"ViewMore"];
    [defaults setBool:NO forKey:@"SquareGrid"];
    [defaults setBool:NO forKey:@"RectangularGrid"];
    [defaults setBool:NO forKey:@"4X4 Grid"];
    [defaults setBool:NO forKey:@"Iconic3X3"];
    [defaults setBool:NO forKey:@"SquareGrid3x3"];
    [defaults setValue:nil  forKey:KEY_BOTTOMBUTTONNAME];
    [defaults setValue:nil forKey:KEY_MITEMID];
    
    [defaults setValue:nil forKey:@"findFlag"];
    [defaults setValue:nil forKey:@"backgroundColor"];
    
    [defaults setBool:NO forKey:@"TwoImageTemplate"];
    [defaults setBool:NO forKey:@"Square Image Template"];
    [defaults setValue:nil forKey:@"isDisplayLabel"];
    
    menuItemName=nil;
    self.view.userInteractionEnabled =YES;
    [ self enableAllSubviewsOf:self.view];
    self.navigationItem.leftBarButtonItem.enabled = YES;
    [previousMenuItem setString:@""];
    [nextMenuItem setString:@""];
    [defaults setBool:NO forKey:@"canDisplayAPNSListScreen"];
    SingleCatFlag=FALSE;
    MultipleCatFlag=FALSE;
    SingleEventFromMainMenu=FALSE;
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if (menulevel==1) {
        
        [cusNav setTitle:@"Main Menu" forView:self withHambergur:NO];
       
        [defaults setValue:nil forKey:@"findCatMitemid"];
        [defaults setValue:nil forKey:@"findSingCatMitemid"];
    }
    if (FindBottomButtonID!=Nil && menulevel<=1)
    {
        [FindBottomButtonID removeAllObjects];
    }
    else
    {
        if (FindBottomButtonID==Nil) {
            FindBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (EventsBottomButtonID!=Nil && menulevel<=1)
    {
        [EventsBottomButtonID removeAllObjects];
    }
    else
    {
        if (EventsBottomButtonID==Nil) {
            EventsBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (FilterBottomButtonID!=Nil && menulevel<=1)
    {
        [FilterBottomButtonID removeAllObjects];
    }
    else
    {
        if (FilterBottomButtonID==Nil) {
            FilterBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    
    if (FindSingleBottomButtonID!=Nil && menulevel<=1)
    {
        [FindSingleBottomButtonID removeAllObjects];
    }
    else
    {
        if (FindSingleBottomButtonID==Nil) {
            FindSingleBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (CityExperienceBottomButtonID!=Nil && menulevel<=1)
    {
        [CityExperienceBottomButtonID removeAllObjects];
    }
    else
    {
        if (CityExperienceBottomButtonID==Nil) {
            CityExperienceBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (FundraiserBottomButtonID!=Nil && menulevel<=1)
    {
        [FundraiserBottomButtonID removeAllObjects];
    }
    else
    {
        if (FundraiserBottomButtonID==Nil) {
            FundraiserBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (NearbyBottomButtonID!=Nil && menulevel<=1)
    {
        [NearbyBottomButtonID removeAllObjects];
    }
    else
    {
        if (NearbyBottomButtonID==Nil) {
            NearbyBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    
    
    if (linkID==NULL) {
        linkID=[[NSMutableArray alloc]init];
    }
    else
    {
        if (menulevel<=1) {
            [linkID removeAllObjects];
        }
    }
    
    if (dateCreated==NULL) {
        dateCreated = [[NSMutableDictionary alloc]init];
    }
    
    if (cashedResponse==NULL) {
        cashedResponse = [[NSMutableDictionary alloc]init];
    }
    
    
    if (deptId==NULL) {
        deptId = [[NSMutableDictionary alloc]init];
    }
    
    if (typeId==NULL) {
        typeId = [[NSMutableDictionary alloc]init];
    }
    
    if (selectedSortOption==NULL) {
        selectedSortOption=[[NSMutableDictionary alloc]init];
    }
    
    if (selectedFilterOption==NULL) {
        selectedFilterOption=[[NSMutableDictionary alloc]init];
    }
    
    if (selectedSortCityDic==NULL) {
        selectedSortCityDic=[[NSMutableDictionary alloc]init];
        
    }
    [defaults setBool:NO forKey:BottomButton];
    
    if([defaults valueForKey:KEYZIPCODE] && [[defaults valueForKey:KEYZIPCODE]isEqualToString:@"N/A"])
    {
        [defaults setValue:nil forKey:KEYZIPCODE];
    }
    
    
}



-(void)returnToMainPage:(id)sender
{
    
    
    
    SideViewController *leftMenuViewController;
    if(IPAD){
        leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
    }
    else{
        leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
        
    }
    
    
    
    
    
    if([defaults boolForKey:@"newsTemplateExist"]){
        leftMenuViewController.isNewsTemplate = TRUE;
        [defaults setValue:@"1" forKey:KEY_MENULEVEL];
        
        //[self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
        //[self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [defaults setValue:@"1" forKey:KEY_MENULEVEL];
        
        
        leftMenuViewController.isNewsTemplate = FALSE;
        [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
        //[self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
    
    
    
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    [cusNav hideBackButton:YES];
    [cusNav hideHambergerButton:YES];
    
    
    [defaults setBool:NO forKey: @"Current Special Offer"];
    [defaults setBool:NO forKey: @"SpecialDetailsAvail"];
    [defaults setBool:NO forKey: @"SpecialListAvail"];
    [defaults setBool:NO forKey: @"Special Offer Retailer View"];
    [defaults setBool:NO forKey: @"detailsPage"];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    if(![defaults valueForKey:KEY_DONESORT])
        subMenuSortDone = NO;
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if (menulevel>1) {
        if(!isModalDismiss){
            if(![defaults boolForKey:@"newsTemplateExist"]){
                
                [defaults setInteger:0 forKey:@"currentIndex"];
                [defaults setBool:NO forKey:@"scrolling"];
                [defaults setBool:NO forKey:@"PaginationCalled"];
                [defaults setValue:nil forKey:@"bannerImgScroll"];
                [defaults setInteger:0 forKey:@"currentIndexSubPage"];
                [defaults setValue:nil forKey:@"ModifiedDate"];
                [defaults setValue:nil forKey:@"scrollModifiedDate"];
                [defaults setValue:nil forKey:@"blockModifiedDate"];
                
                SideViewController *leftMenuViewController;
                if(IPAD){
                    leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
                }
                else{
                    leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
                    
                }
                leftMenuViewController.isNewsTemplate = FALSE;
                [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
                
                
            }
        }
        else{
            isModalDismiss = NO;
        }
        
        
        
    }
    else{
        if(!isModalDismiss){
            
            
            SideViewController *leftMenuViewController;
            if(IPAD){
                leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
            }
            else{
                leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
                
            }
            leftMenuViewController.isNewsTemplate = FALSE;
            [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
        }
        else{
            isModalDismiss = NO;
        }
        
        
    }
    // If not coming back from Cancel Button of Sorting Page then load page again
    
    if(![defaults valueForKey:@"isComingFromSubMenuGroupingAndSorting"])
    {
        [self initMenuData];
        
        
        [self requestmenudisplay];
        
        
    }
    else{
        [self saveMenuDisplay];
    }
    
    
    [defaults setValue:nil forKey:@"isComingFromSubMenuGroupingAndSorting"];
    [defaults setValue:nil forKey:KEY_DONESORT];
    [defaults setValue:@"Distance" forKey:@"SortCitiExpBy"];
    [defaults setValue:nil forKey:@"GroupCitiExpBy"];
    [defaults setValue:@"Distance" forKey:@"NearBySort"];
    [defaults setValue:@"Distance" forKey:@"filtersSort"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}



#pragma mark textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField.tag == 31){
        NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        if (range.length == 1){
            NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [zipSave setEnabled:(finalString.length >= 5)];
            return YES;
        }
        else {
            for (int j = 0; j<[string length]; j++) {
                unichar c = [string characterAtIndex:j];
                if ([charSet characterIsMember:c]) {
                    NSUInteger textLength = [textField.text length] + [string length] - range.length;
                    [zipSave setEnabled:(textLength >= 5)];
                    return (textLength > 5)? NO : YES;
                }
                else {
                    return NO;
                }
            }
        }
        
    }
    return YES;
}

//Alert that shows a Zip Enter Box
-(void)showZipEnter
{
  
        // use UIAlertController
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Please enter your zip code"
                                   message:nil
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        zipSave = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       UITextField *textField = alert.textFields[0];
                                                       if ([textField.text length])
                                                           [self postZip:textField.text];
                                                       else
                                                           return;
                                                       
                                                   }];
        zipSave.enabled = NO;
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           NSLog(@"cancel btn");
                                                           
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                           [defaults setValue:nil forKey:KEYZIPCODE];
                                                           
                                                           [UtilityManager showAlert:nil msg:@"You have chosen not to search by your Zipcode. If you would like to search by Zipcode in the future, please go to the User Information screen on the bottom toolbar of the homepage."];
                                                          
                                                           
                                                       }];
        
        [alert addAction:cancel];
        [alert addAction:zipSave];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            [textField setTag:31];
            textField.delegate = self;
            textField.keyboardType = UIKeyboardTypeNumberPad;
            textField.font = [UIFont systemFontOfSize:16];
        }];
        
        [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark updated location
-(void) updatedLatlongValue :(NSString *) latValue longitudeValue:(NSString *)longValue{
    if (latValue == NULL || longValue == NULL) {
        return;
    }
    
    [defaults setObject:latValue forKey:KEY_LATITUDE];
    [defaults setObject:longValue forKey:KEY_LONGITUDE];
    
}

-(void)SortBtnPressed
{
    
    //    SubMenuGroupingAndSortingViewController *iSubMenuGroupingAndSortingViewController = [[SubMenuGroupingAndSortingViewController alloc]initWithNibName:@"SubMenuGroupingAndSortingViewController" bundle:[NSBundle mainBundle]];
    //
    //    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    //    if (bottomBtn) {
    ////        iSwipeViewController.bottomBtn = YES;
    //        iSubMenuGroupingAndSortingViewController.view.frame = CGRectMake(SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2, SCREEN_HEIGHT-yVal-50);
    //    }
    //    else{
    ////        iSwipeViewController.bottomBtn = NO;
    //        iSubMenuGroupingAndSortingViewController.view.frame = CGRectMake(SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2, SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT);
    //    }
    //    self.navigationItem.leftBarButtonItem.enabled = NO;
    //    //self.parentViewController.view.userInteractionEnabled = NO;
    //
    //    //tblMenuListView.allowsSelection =NO;
    //   iSubMenuGroupingAndSortingViewController.delegate = self;
    //    [self disableAllSubviewsOf:self.view];
    //
    //    [self.view addSubview:iSubMenuGroupingAndSortingViewController.view];
    //    iSubMenuGroupingAndSortingViewController.view.userInteractionEnabled = YES;
    //
    ////    [self.navigationController presentViewController:iSubMenuGroupingAndSortingViewController animated:NO completion:nil];
    //
    ////    [self.navigationController pushViewController:iSubMenuGroupingAndSortingViewController animated:YES];
    
    SubMenuGroupingAndSortingViewController *iSubMenuGroupingAndSortingViewController = [[SubMenuGroupingAndSortingViewController alloc]initWithNibName:@"SubMenuGroupingAndSortingViewController" bundle:[NSBundle mainBundle]];
    
    [self.navigationController pushViewController:iSubMenuGroupingAndSortingViewController animated:YES];
    
    
}

- (void)SubMenuGroupingAndSortingViewController:(SubMenuGroupingAndSortingViewController*)viewController{
    [self viewWillAppear:NO];
}


-(void) disableAllSubviewsOf:(UIView *) theView
{
    for(UIView* subview in [theView subviews])
    {
        [self disableAllSubviewsOf:subview];
        DLog(@"subview %@", subview);
        subview.userInteractionEnabled = NO;
    }
}

-(void) enableAllSubviewsOf:(UIView *) theView
{
    for(UIView* subview in [theView subviews])
    {
        [self enableAllSubviewsOf:subview];
        DLog(@"subview %@", subview);
        subview.userInteractionEnabled = YES;
    }
}


-(void)popBackToPreviousPage
{
    
    
    [defaults setBool:NO forKey:@"ViewMore"];
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    NSString *levelString;
    
    [[HubCitiAppDelegate getLinkIdArray]removeLastObject];
    
    if(menulevel > 1)
        levelString = [NSString stringWithFormat:@"%d", menulevel-1];
    else
        levelString = [NSString stringWithFormat:@"%d", menulevel];
    
    [defaults setValue:@"NONE" forKey:@"sortOrder"];
    
    [defaults setValue:levelString forKey:KEY_MENULEVEL];
    
    [defaults setValue:@"NONE" forKey:@"sortOrder"];
    
    [defaults setValue:nil forKey:@"commaSeperatedCities"];
    
    subMenuSortDone = NO;
    fromBack = YES;
    // [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
    NSLog(@"%@",self.navigationController.viewControllers);
//    NSArray *viewControllers = [self.navigationController viewControllers];
//    
//    
//    
//    id viewController = [viewControllers objectAtIndex:[viewControllers count]-2];
//    
//    if([viewController isKindOfClass:[ScrollTemplateViewController class]]||[viewController isKindOfClass:[CombinationViewController class]]||[viewController isKindOfClass:[BlockViewController class]] || [viewController isKindOfClass:[MainMenuViewController class]]) {
//        
//        SideViewController *leftMenuViewController;
//        if(IPAD){
//            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
//        }
//        else{
//            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
//            
//        }
//        leftMenuViewController.isNewsTemplate = TRUE;
//        if(! [viewController isKindOfClass:[MainMenuViewController class]])
//            leftMenuViewController.isSideMenu = YES;
//        [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
//        
//    }
//    
    
    
    
   // [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    [self.navigationController popViewControllerAnimated:NO];
}


#pragma mark Location Methods

// Checks if the user has a Zip Code associated with them
-(void)checkForZipCode
{
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/fetchuserlocationpoints?userId=%@",BASE_URL,[defaults objectForKey:KEY_USERID]];
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml] ;
    TBXMLElement *postalCodeElement = [TBXML childElementNamed:@"postalCode" parentElement:tbxml.rootXMLElement];
    
    if (postalCodeElement == nil) {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
        int responseCode = [[TBXML textForElement:[TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement]] intValue];
        if (responseCode == 10005) {
            
            
            //If The User Doesnt Have a Zip on file, Ask them if they want to enter one
            
            UIAlertController * alert; //enter a zipcode alert
           alert=[UIAlertController alertControllerWithTitle:nil message:@"Would you like to enter a zipcode for us to use instead?" preferredStyle:UIAlertControllerStyleAlert];
                      
            UIAlertAction* no = [UIAlertAction
                                 actionWithTitle:@"No"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     //Warn them that they are completely off the grid as far as ScanSee is concerned
                                     [defaults setValue:nil forKey:KEYZIPCODE];
                                     [UtilityManager showAlert:nil msg:@"You have chosen not to search by your Zipcode. If you would like to search by Zipcode in the future, please go to the User Information screen."];
                                 }];
            [alert addAction:no];
            UIAlertAction* yes = [UIAlertAction
                                 actionWithTitle:@"Yes"
                                 style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action){
                                       [self showZipEnter];
                                  }];
            [alert addAction:yes];

            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];

            
            return;
        }
        else {
            // Banner the message given by the server
            [UtilityManager showAlert:@"Info" msg:responseTextStr];
            //[tbxml release];
            ReleaseAndNilify(responseXml);
            return;
        }
    }
    
    NSString *postalCodeStr = [TBXML textForElement:postalCodeElement];
    
    //Sets the ZipCode to the key for use in other view controllers
    [defaults setValue:postalCodeStr forKey:KEYZIPCODE];
    
    //set for Austin Experience req when clicked from main menu
    [defaults setObject:postalCodeStr forKey:@"postalCode"];
    
    //[tbxml release];
    ReleaseAndNilify(responseXml);
    //[self getMainMenuItems];
    return;
}

//Posts Zip Code to User
-(void)postZip:(NSString *)zipCodeText
{
    DLog(@"Posting Zip");
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:@"Error!" msg:@"Network is not available"];
    }
    else {
        
        NSMutableString *urlString = [BASE_URL mutableCopy] ;
        [urlString appendFormat:@"thislocation/updateusrzipcode?userId=%@&zipcode=%@", [defaults valueForKey:KEY_USERID], zipCodeText];
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
        if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]) {
            ReleaseAndNilify(responseXml);
            return;
        }
        
        //NEED TO SET ERROR HANDLING OF RESPONSE. WILL BE ABLE TO WHEN CAN BE TESTED
        DLog(@"RESPONSE - %@",responseXml);
        [defaults setValue:zipCodeText forKey:KEYZIPCODE];
        // [self getMainMenuItems];
        
        // [responseXml release];
        
    }
}

-(void)checkLocationStatus{
    
    if([defaults integerForKey:@"AppCounter"] == 1)
    {
        // If Devive Level GPS and App Level GPS both ON
        if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [[defaults valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
        {
            //[SharedManager setGps_allow_flag:YES];
           // [defaults setValue:@"YES" forKey:@"allowingGPS"];
            [Location updateLocationinfo:self];
        }
        // IF Device Level GPS ON but Application Level GPS OFF
        else if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [[defaults valueForKey:@"allowingGPS"] isEqualToString:@"NO"])
        {
            
            
            
            [self gpsAllowDontAllowPopup];
        }
        // IF Application level GPS off
        else if ([[defaults valueForKey:@"allowingGPS"] isEqualToString:@"NO"]) {
            
            [self gpsAllowDontAllowPopup];
        }
    }
    
    else{
        
        [defaults setInteger:1 forKey:@"AppCounter"];
        if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [CLLocationManager locationServicesEnabled])
        {
//            [SharedManager setGps_allow_flag:YES];
//            [defaults setValue:@"YES" forKey:@"allowingGPS"];
            [Location updateLocationinfo:self];
        }
        else {
//            [SharedManager setGps_allow_flag:NO];
//            [defaults setValue:@"NO" forKey:@"allowingGPS"];
            [self checkForZipCode];
        }
    }
}

-(void) gpsAllowDontAllowPopup{
    
    
    UIAlertController * alert;
        alert=[UIAlertController alertControllerWithTitle:nil message:@"Application uses your current location to provide information about retailers and products near you. Do you wish to allow to access your location?" preferredStyle:UIAlertControllerStyleAlert];
   
    UIAlertAction* allow = [UIAlertAction
                         actionWithTitle:@"Allow"
                         style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action){
                                //Ajit's edit
                               // [SharedManager setGps_allow_flag:YES];
                                [NewsUtility setGPSAsAllowForUser];
                                //If LS Allowed, but LS are off
                                if ([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) {
                                    [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
                                    
                                }
                                [Location updateLocationinfo:self];
                                //[defaults setValue:@"YES" forKey:@"allowingGPS"];

                            }];
    [alert addAction:allow];
    
    UIAlertAction* dontAllow = [UIAlertAction
                            actionWithTitle:@"Don't Allow"
                            style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action){
                                   // [SharedManager setGps_allow_flag:NO];
                                    [defaults setValue:@"" forKey:KEY_LATITUDE];//reset the values
                                    [defaults setValue:@"" forKey:KEY_LONGITUDE];
                                    [self checkForZipCode];
                                    //[defaults setValue:@"NO" forKey:@"allowingGPS"];
                                    [alert dismissViewControllerAnimated:NO completion:nil];
                                }];
    [alert addAction:dontAllow];
    [self presentViewController:alert animated:YES completion:nil];
    

}

-(void)checkForRadius
{
    NSString *reqStr = [NSString stringWithFormat:@"<AuthenticateUser><deviceID>%@</deviceID><userId>%@</userId><hubCitiId>%@</hubCitiId></AuthenticateUser>",[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusersettings",BASE_URL];
     
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:reqStr];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
    if (responseCodeElement != nil && ![[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    
    TBXMLElement *localeRadiusElement = [TBXML childElementNamed:@"localeRadius" parentElement:tbxml.rootXMLElement];
    TBXMLElement *bLocServiceElement = [TBXML childElementNamed:@"bLocService" parentElement:tbxml.rootXMLElement];
    if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"false"])
    {
        [SharedManager setGps_allow_flag:NO];
        [defaults setValue:@"NO" forKey:@"allowingGPS"];
    }
    else if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"true"])
    {
        [SharedManager setGps_allow_flag:YES];
        [defaults setValue:@"YES" forKey:@"allowingGPS"];
    }
    
    NSString *userRadius = [TBXML textForElement:localeRadiusElement];
    if(userRadius.length > 0)
    {
        [defaults setValue:userRadius forKey:@"FindRadius"];
        NSLog(@"my radius is %@",[defaults valueForKey:@"FindRadius"]);
    }
    
    
}


-(void)setBackgroundColorandImage
{
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    int topbannerImageHeight = 0;
    int havingRectIndex = 0;
    MenuItemList* obj_mainMenuDO;
    
    if([defaults valueForKey:@"TopBannerImage"])
        topbannerImageHeight = 50;
    
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    
    for(int row=0; row < [arrMenuItems count]; row++)
    {
        obj_mainMenuDO = [arrMenuItems objectAtIndex:row];
        if([arrMenuItems count]>row)
        {
            
            // NSDictionary* dictList = arrMenuItems[row];
            //[obj_mainMenuDO setValuesForKeysWithDictionary:dictList];
            
            if([obj_mainMenuDO.mShapeName isEqualToString:@"Rectangle"]){
                havingRectIndex = row;
                break;
            }
        }
    }
    if(havingRectIndex > 0)
    {
        obj_mainMenuDO = [arrMenuItems objectAtIndex:havingRectIndex];
        // NSDictionary* dictList = menuResponse.arMItemList[havingRectIndex];
        // [obj_mainMenuDO setValuesForKeysWithDictionary:dictList];
        
        
    }
    else
    {
        if (arrMenuItems.count > 0) {
            obj_mainMenuDO = [arrMenuItems objectAtIndex:0];
        }
        
        // NSDictionary* dictList = menuResponse.arMItemList[0];
        // [obj_mainMenuDO setValuesForKeysWithDictionary:dictList];
    }
    
    
    // /Set the tableView Background image/color as per Main Menu BG color for List View
    if(iTempletsMenuId == ListView || iTempletsMenuId == ListViewWithBannerAd)
    {
        if(menulevel > 1)
        {
            if(self.menuResponse.smBkgrdImage && ![self.menuResponse.smBkgrdImage isEqualToString:@"N/A"])
            {
                SdImageView *asyncImageView;
                
                asyncImageView = [[SdImageView alloc] initWithFrame:tblMenuListView.frame];
                
                asyncImageView.backgroundColor = [UIColor clearColor];
                asyncImageView.layer.cornerRadius = 5.0f;
                if(IOS9)
                    [asyncImageView loadImage:[self.menuResponse.smBkgrdImage stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                else
                    [asyncImageView loadImage:[self.menuResponse.smBkgrdImage stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                asyncImageView.tag=99;
                [tblMenuListView setBackgroundView:asyncImageView];
                //[asyncImageView release];
                
                [defaults setValue:self.menuResponse.smBkgrdImage forKey:@"menuBkgrdImage"];
                [defaults setValue:nil forKey:@"menuBkgrdColor"];
                
                [defaults setValue:obj_mainMenuDO.smBtnColor forKey:@"menuBtnColor"];
                [defaults setValue:obj_mainMenuDO.smBtnFontColor forKey:@"menuBtnFontColor"];
                
                
            }
            else if(self.menuResponse.smBkgrdColor && ![self.menuResponse.smBkgrdColor isEqualToString:@"N/A"])
            {
                tblMenuListView.backgroundColor = [UIColor convertToHexString:self.menuResponse.smBkgrdColor];
                
                self.view.backgroundColor = [UIColor convertToHexString:self.menuResponse.smBkgrdColor];
                [defaults setValue:self.menuResponse.smBkgrdColor forKey:@"menuBkgrdColor"];
                [defaults setValue:nil forKey:@"menuBkgrdImage"];
                
                [defaults setValue:obj_mainMenuDO.smBtnColor forKey:@"menuBtnColor"];
                [defaults setValue:obj_mainMenuDO.smBtnFontColor forKey:@"menuBtnFontColor"];
                
                
            }
            else
                tblMenuListView.backgroundColor = [UIColor whiteColor];
            
        }
        else
        {
            if(self.menuResponse.mBkgrdImage && ![self.menuResponse.mBkgrdImage isEqualToString:@"N/A"])
            {
                SdImageView *asyncImageView;
                
                asyncImageView = [[SdImageView alloc] initWithFrame:tblMenuListView.frame];
                
                asyncImageView.backgroundColor = [UIColor clearColor];
                asyncImageView.layer.cornerRadius = 5.0f;
                if(IOS9)
                    [asyncImageView loadImage:[self.menuResponse.mBkgrdImage stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                else
                    [asyncImageView loadImage:[self.menuResponse.mBkgrdImage stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                asyncImageView.tag=99;
                [tblMenuListView setBackgroundView:asyncImageView];
                //[asyncImageView release];
                
                [defaults setValue:self.menuResponse.mBkgrdImage forKey:@"menuBkgrdImage"];
                [defaults setValue:nil forKey:@"menuBkgrdColor"];
                
                [defaults setValue:obj_mainMenuDO.mBtnColor forKey:@"menuBtnColor"];
                [defaults setValue:obj_mainMenuDO.mBtnFontColor forKey:@"menuBtnFontColor"];
                
                
                
            }
            else if(self.menuResponse.mBkgrdColor && ![self.menuResponse.mBkgrdColor isEqualToString:@"N/A"])
            {
                tblMenuListView.backgroundColor = [UIColor convertToHexString:self.menuResponse.mBkgrdColor];
                
                [defaults setValue:nil forKey:@"menuBkgrdImage"];
                [defaults setValue:self.menuResponse.mBkgrdColor forKey:@"menuBkgrdColor"];
                
                [defaults setValue:obj_mainMenuDO.mBtnColor forKey:@"menuBtnColor"];
                [defaults setValue:obj_mainMenuDO.mBtnFontColor forKey:@"menuBtnFontColor"];
                
                
            }
            else
                tblMenuListView.backgroundColor = [UIColor whiteColor];
            
            
        }
    }
    else
    {
        if(menulevel > 1)
        {
            if([self.menuResponse.templateName isEqualToString:@"Two Image Template"]||[menuResponse.templateName isEqualToString:@"Square Image Template"]){
                if(self.menuResponse.TempBkgrdImg && ![self.menuResponse.TempBkgrdImg isEqualToString:@"N/A"])
                {
                    
                    SdImageView *asyncImageView;
                    /* if([self.menuResponse.templateName isEqualToString:@"Two Image Template"]){
                     CGFloat backgroundHeight = (float)2384/(float)1440 * SCREEN_WIDTH;
                     
                     if([self.menuResponse.arBottomBtnList count] > 0)
                     asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, backgroundHeight)];
                     else
                     asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, backgroundHeight)];
                     }
                     else{*/
                    
                    if([self.menuResponse.arBottomBtnList count] > 0)
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight-topbannerImageHeight)];
                    else
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-topbannerImageHeight)];
                    //}
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    asyncImageView.layer.cornerRadius = 5.0f;
                    
                    [asyncImageView loadImage:[self.menuResponse.TempBkgrdImg stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                    asyncImageView.tag=99;
                    [self.view insertSubview:asyncImageView atIndex:0];
                    
                    [self.view sendSubviewToBack:asyncImageView];
                    //[asyncImageView release];
                    
                    [defaults setValue:self.menuResponse.TempBkgrdImg forKey:@"menuBkgrdImage"];
                    [defaults setValue:nil forKey:@"menuBkgrdColor"];
                    
                    [defaults setValue:obj_mainMenuDO.smBtnColor forKey:@"menuBtnColor"];
                    [defaults setValue:obj_mainMenuDO.smBtnFontColor forKey:@"menuBtnFontColor"];
                    
                }
            }
            else if([self.menuResponse.templateName isEqualToString:@"Rectangular Grid"]||[self.menuResponse.templateName isEqualToString:@"4X4 Grid"]){
                
                if(self.menuResponse.templateBgColor && ![self.menuResponse.templateBgColor isEqualToString:@"N/A"])
                {
                    
                    self.view.backgroundColor = [UIColor convertToHexString:self.menuResponse.templateBgColor];
                    [defaults setValue:self.menuResponse.templateBgColor forKey:@"menuBkgrdColor"];
                    [defaults setValue:nil forKey:@"menuBkgrdImage"];
                    
                }
                
                
            }
            else if ([self.menuResponse.templateName isEqualToString:@"Four Tile Template"])
            {
                self.view.backgroundColor = [UIColor whiteColor];
            }
            else{
                if([self.menuResponse.templateName isEqualToString:@"Two Tile Template"] && self.menuResponse.TempBkgrdImg && ![self.menuResponse.TempBkgrdImg isEqualToString:@"N/A"]){
                    SdImageView *asyncImageView;
                    if([self.menuResponse.arBottomBtnList count] > 0)
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight-topbannerImageHeight)];
                    else
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-topbannerImageHeight)];
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    asyncImageView.layer.cornerRadius = 5.0f;
                    
                    [asyncImageView loadImage:[self.menuResponse.TempBkgrdImg stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                    asyncImageView.tag=99;
                    [self.view insertSubview:asyncImageView atIndex:0];
                    
                    [self.view sendSubviewToBack:asyncImageView];
                    //[asyncImageView release];
                    
                    [defaults setValue:self.menuResponse.TempBkgrdImg forKey:@"menuBkgrdImage"];
                    [defaults setValue:nil forKey:@"menuBkgrdColor"];
                    
                    [defaults setValue:obj_mainMenuDO.smBtnColor forKey:@"menuBtnColor"];
                    [defaults setValue:obj_mainMenuDO.smBtnFontColor forKey:@"menuBtnFontColor"];
                }
                else if(self.menuResponse.smBkgrdImage && ![self.menuResponse.smBkgrdImage isEqualToString:@"N/A"])
                {
                    SdImageView *asyncImageView;
                    
                    if([self.menuResponse.arBottomBtnList count] > 0)
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight-topbannerImageHeight)];
                    else
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-topbannerImageHeight)];
                    
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    asyncImageView.layer.cornerRadius = 5.0f;
                    [asyncImageView loadImage:[self.menuResponse.smBkgrdImage stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                    asyncImageView.tag=99;
                    [self.view insertSubview:asyncImageView atIndex:0];
                    
                    [self.view sendSubviewToBack:asyncImageView];
                    //[asyncImageView release];
                    
                    [defaults setValue:self.menuResponse.smBkgrdImage forKey:@"menuBkgrdImage"];
                    [defaults setValue:nil forKey:@"menuBkgrdColor"];
                    
                    [defaults setValue:obj_mainMenuDO.smBtnColor forKey:@"menuBtnColor"];
                    [defaults setValue:obj_mainMenuDO.smBtnFontColor forKey:@"menuBtnFontColor"];
                    
                }
                else if(self.menuResponse.smBkgrdColor && ![self.menuResponse.smBkgrdColor isEqualToString:@"N/A"])
                {
                    self.view.backgroundColor = [UIColor convertToHexString:self.menuResponse.smBkgrdColor];
                    [defaults setValue:self.menuResponse.smBkgrdColor forKey:@"menuBkgrdColor"];
                    [defaults setValue:nil forKey:@"menuBkgrdImage"];
                    
                    [defaults setValue:obj_mainMenuDO.smBtnColor forKey:@"menuBtnColor"];
                    [defaults setValue:obj_mainMenuDO.smBtnFontColor forKey:@"menuBtnFontColor"];
                }
                
            }
        }
        else
        {
            if([self.menuResponse.templateName isEqualToString:@"Two Image Template"]||[menuResponse.templateName isEqualToString:@"Square Image Template"]){
                if(self.menuResponse.TempBkgrdImg && ![self.menuResponse.TempBkgrdImg isEqualToString:@"N/A"])
                {
                    SdImageView *asyncImageView;
                    /* if([self.menuResponse.templateName isEqualToString:@"Two Image Template"]){
                     CGFloat backgroundHeight = (float)2384/(float)1440 * SCREEN_WIDTH;
                     
                     if([self.menuResponse.arBottomBtnList count] > 0)
                     asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, backgroundHeight)];
                     else
                     asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, backgroundHeight)];
                     }
                     else{*/
                    
                    if([self.menuResponse.arBottomBtnList count] > 0)
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight-topbannerImageHeight)];
                    else
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-topbannerImageHeight)];
                    //}
                    
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    asyncImageView.layer.cornerRadius = 5.0f;
                    
                    [asyncImageView loadImage:[self.menuResponse.TempBkgrdImg stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                    asyncImageView.tag=99;
                    [self.view insertSubview:asyncImageView atIndex:0];
                    
                    [self.view sendSubviewToBack:asyncImageView];
                    //[asyncImageView release];
                    
                    [defaults setValue:self.menuResponse.TempBkgrdImg forKey:@"menuBkgrdImage"];
                    [defaults setValue:nil forKey:@"menuBkgrdColor"];
                    
                    [defaults setValue:obj_mainMenuDO.mBtnColor forKey:@"menuBtnColor"];
                    [defaults setValue:obj_mainMenuDO.mBtnFontColor forKey:@"menuBtnFontColor"];
                    
                    
                }
            }
            else if([self.menuResponse.templateName isEqualToString:@"Rectangular Grid"]||[self.menuResponse.templateName isEqualToString:@"4X4 Grid"]){
                
                
                if(self.menuResponse.templateBgColor && ![self.menuResponse.templateBgColor isEqualToString:@"N/A"])
                {
                    
                    self.view.backgroundColor = [UIColor convertToHexString:self.menuResponse.templateBgColor];
                    [defaults setValue:self.menuResponse.templateBgColor forKey:@"menuBkgrdColor"];
                    [defaults setValue:nil forKey:@"menuBkgrdImage"];
                    
                }
            }
            else if ( [self.menuResponse.templateName isEqualToString:@"Four Tile Template"])
            {
                self.view.backgroundColor = [UIColor whiteColor];
            }
            else{
                if([self.menuResponse.templateName isEqualToString:@"Two Tile Template"] && self.menuResponse.TempBkgrdImg && ![self.menuResponse.TempBkgrdImg isEqualToString:@"N/A"]){
                    SdImageView *asyncImageView;
                    if([self.menuResponse.arBottomBtnList count] > 0)
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight)];
                    else
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal)];
                    
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    asyncImageView.layer.cornerRadius = 5.0f;
                    
                    [asyncImageView loadImage:[self.menuResponse.TempBkgrdImg stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                    asyncImageView.tag=99;
                    [self.view insertSubview:asyncImageView atIndex:0];
                    
                    [self.view sendSubviewToBack:asyncImageView];
                    //[asyncImageView release];
                    
                    [defaults setValue:self.menuResponse.TempBkgrdImg forKey:@"menuBkgrdImage"];
                    [defaults setValue:nil forKey:@"menuBkgrdColor"];
                    
                    [defaults setValue:obj_mainMenuDO.mBtnColor forKey:@"menuBtnColor"];
                    [defaults setValue:obj_mainMenuDO.mBtnFontColor forKey:@"menuBtnFontColor"];
                    
                }
                
                else if(self.menuResponse.mBkgrdImage && ![self.menuResponse.mBkgrdImage isEqualToString:@"N/A"])
                {
                    
                    SdImageView *asyncImageView;
                    
                    if([self.menuResponse.arBottomBtnList count] > 0)
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight-topbannerImageHeight)];
                    else
                        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-topbannerImageHeight)];
                    
                    asyncImageView.backgroundColor = [UIColor clearColor];
                    asyncImageView.layer.cornerRadius = 5.0f;
                    //////
                    if(IOS9)
                        [asyncImageView loadImage:[self.menuResponse.mBkgrdImage stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                    else
                        [asyncImageView loadImage:[self.menuResponse.mBkgrdImage stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                    asyncImageView.tag=99;
                    [self.view insertSubview:asyncImageView atIndex:0];
                    
                    [self.view sendSubviewToBack:asyncImageView];
                    //[asyncImageView release];
                    
                    [defaults setValue:self.menuResponse.mBkgrdImage forKey:@"menuBkgrdImage"];
                    [defaults setValue:nil forKey:@"menuBkgrdColor"];
                    
                    [defaults setValue:obj_mainMenuDO.mBtnColor forKey:@"menuBtnColor"];
                    [defaults setValue:obj_mainMenuDO.mBtnFontColor forKey:@"menuBtnFontColor"];
                    
                }
                else if(self.menuResponse.mBkgrdColor && ![self.menuResponse.mBkgrdColor isEqualToString:@"N/A"])
                {
                    
                    self.view.backgroundColor = [UIColor convertToHexString:self.menuResponse.mBkgrdColor];
                    [defaults setValue:self.menuResponse.mBkgrdColor forKey:@"menuBkgrdColor"];
                    [defaults setValue:nil forKey:@"menuBkgrdImage"];
                    
                    [defaults setValue:obj_mainMenuDO.mBtnColor forKey:@"menuBtnColor"];
                    [defaults setValue:obj_mainMenuDO.mBtnFontColor forKey:@"menuBtnFontColor"];
                    
                }
                
            }
        }
    }
    
    
}


-(void)releaseAllocatedObjects
{
    NSArray *arrSubViews = [self.view subviews];
    for(int i=0; i<[arrSubViews count]; i++)
    {
        UIView *view = (UIView*)[arrSubViews objectAtIndex:i];
        if(view.tag == 99)
            [view removeFromSuperview];
        if([view isKindOfClass:[bottomButtonView class]])
        {
            [view removeFromSuperview];
        }
    }
    
    
    if([arrMenuBottomButtonDO count]>0)
    {
        [arrMenuBottomButtonDO removeAllObjects];
        // [arrMenuBottomButtonDO release];
        arrMenuBottomButtonDO = nil;
    }
    if([arrBottomButtonViewContainer count]>0)
    {
        [arrBottomButtonViewContainer removeAllObjects];
        // [arrBottomButtonViewContainer release];
        arrBottomButtonViewContainer = nil;
    }
    
}





#pragma mark - Menu Button & Bottom Button click Events
/******* Assuming the buttons would be About,UserAcct,Settings,More.. Navigate to corresponding screen *****/
-(IBAction)bottomBarButtonClick:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert];
    }
    else{
        switch (btn.tag) {
                
            case 0:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                [self.navigationController pushViewController:privacy animated:NO];
                //[privacy release];
            }
                break;
                
            case 1:
            {
                iWebRequestState = GETUSERINFO;;
                [self requestToGetUserData];
            }
                break;
                
            case 2:
            {
                SettingsViewController *inform = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:inform animated:NO];
                //[inform release];
            }
                break;
                
            case 3:
            {
                //should navigate to more screen
                
            }break;
                
            default:
                break;
        }
    }
}

/*
 *
 send request with data to get user details.
 request sent when navigated from settings screen.
 *
 */
//-(void)requestToGetUserData
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
//    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//    ReleaseAndNilify(xmlStr);
//}

-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}
-(void)parseGetUserData:(id)response
{
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}

// Set the custom templete for main menu
-(void)callTemplate:(NSArray*)arrTitles Images:(NSArray*)arrImages mainMenuItemObjects:(NSDictionary*)arrGropus
{
    switch (iTempletsMenuId)
    {
        case GroupedTab:
        case TwoColumnTab:
        case ComboTemplate:
        case GroupedTabWithImage:
        case FourTile :
        case TwoTile:
        case TwoTileWithBanner:
        {
            int topbannerImageHeight = 0;
            
            if ([self.menuResponse.isNewTicker boolValue]) {
                
                topbannerImageHeight = 50;
                
                [self addNewsTicker ];
                
            }
            
            else if([defaults valueForKey:@"TopBannerImage"] && ![self.menuResponse.isNewTicker boolValue])
            {
                topbannerImageHeight = 50;
                SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, topbannerImageHeight)];
                asyncImageView.backgroundColor = [UIColor clearColor];
                [asyncImageView loadImage:[defaults valueForKey:@"TopBannerImage"]];
                [self.view addSubview:asyncImageView];
                asyncImageView.userInteractionEnabled = NO;
                asyncImageView.tag = 99;
                //[asyncImageView release];
            }
            
            iViewController = [[ButtonSetupViewController alloc]initWithNibName:@"ButtonSetupViewController" bundle:nil];
            
            // Calculate Y position for View
            int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
            yVal = yVal+topbannerImageHeight;
            
            if ([menuResponse.templateName isEqualToString:@"Two Tile Template"]||[menuResponse.templateName isEqualToString:@"Two Tile Template With Banner"]||[menuResponse.templateName isEqualToString:@"Four Tile Template"]){
                iViewController.btnBkgrdColor = menuResponse.btnBkgrdColor;
                iViewController.btnLblColor = menuResponse.btnLblColor;
            }
            
            if([menuResponse.arBottomBtnList count] > 0)
                iViewController.view.frame=CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight);
            else
                iViewController.view.frame=CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal);
            
            [iViewController setbasicpropertyforMainMenu:@"#FF0000" Viewbackground:@"#C0C0C0" mainMenuItemObjects:arrGropus templetid:iTempletsMenuId setDelegate:self];
            iViewController.view.tag = 99;
            [self.view addSubview:iViewController.view];
            [self.view sendSubviewToBack:iViewController.view];
            
        }
            break;
            
        case ListView:
            [self CallTemplate_ListView];
            break;
        case IconicGrid:
        {
            NSMutableDictionary *dicItems = [[NSMutableDictionary alloc]init];
            /*[dicItems setObject:[arrImages copy] forKey:@"arrImages"];
             [dicItems setObject:[arrTitles copy] forKey:@"arrTitles"];*/
            [self callTemplate_IconicGrid:dicItems];
        }
            break;
        case TwoColumnTabWithBannerAd:
        {
            int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
            [defaults setValue:@"2" forKey:@"noOfColumns"];
            int topbannerImageHeight = 0;
            if ([self.menuResponse.isNewTicker boolValue]) {
                
                topbannerImageHeight = 50;
                
                [self addNewsTicker ];
                
            }
            if([defaults valueForKey:@"TopBannerImage"] && ![self.menuResponse.isNewTicker boolValue])
            {
                topbannerImageHeight = 50;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
                    topbannerImageHeight=150;
                }
                SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, topbannerImageHeight)];
                asyncImageView.backgroundColor = [UIColor clearColor];
                [asyncImageView loadImage:[defaults valueForKey:@"TopBannerImage"]];
                [self.view addSubview:asyncImageView];
                asyncImageView.userInteractionEnabled = NO;
                asyncImageView.tag = 99;
                //[asyncImageView release];
            }
            
            iViewController = [[ButtonSetupViewController alloc]initWithNibName:@"ButtonSetupViewController" bundle:nil];
            yVal = yVal + topbannerImageHeight;
            // Calculate Y position for View
            if([arrMenuBottomButtonDO count] > 0)
                iViewController.view.frame=CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight);
            else
                iViewController.view.frame=CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal);
            
            [iViewController setbasicpropertyforMainMenu:@"#FF0000" Viewbackground:@"#C0C0C0" mainMenuItemObjects:[arrGropus copy] templetid:2 setDelegate:self];
            iViewController.view.tag = 99;
            [self.view addSubview:iViewController.view];
            [self.view sendSubviewToBack:iViewController.view];
            
        }
            break;
        case ListViewWithBannerAd:
        {
            int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
            int topbannerImageHeight = 0;
            if ([self.menuResponse.isNewTicker boolValue]) {
                
                topbannerImageHeight = 50;
                
                [self addNewsTicker ];
                
            }

            if(self.menuResponse.mBannerImg && ![self.menuResponse.isNewTicker boolValue])
            {
                topbannerImageHeight = 50;
                
                SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, topbannerImageHeight)];
                asyncImageView.backgroundColor = [UIColor clearColor];
                
                [asyncImageView loadImage:self.menuResponse.mBannerImg];
                [self.view addSubview:asyncImageView];
                asyncImageView.userInteractionEnabled = NO;
                asyncImageView.tag = 99;
                //[asyncImageView release];
            }
            
            // Calculate Y position for View
            if([self.menuResponse.arBottomBtnList count] > 0)
                tblMenuListView = [[UITableView alloc]initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight - topbannerImageHeight) style:UITableViewStylePlain];
            else
                tblMenuListView = [[UITableView alloc]initWithFrame:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal - topbannerImageHeight) style:UITableViewStylePlain];
            
            tblMenuListView.delegate=self;
            tblMenuListView.dataSource=self;
            tblMenuListView.separatorStyle = UITableViewCellSeparatorStyleNone;
            tblMenuListView.tag=99;
            tblMenuListView.backgroundColor = [UIColor whiteColor];
            
            [self.view addSubview:tblMenuListView];
            
        }
            break;
            
        default:
            break;
    }
    
}

// Call Custom Class to set the view like Templete-4
-(void)callTemplate_IconicGrid:(NSMutableDictionary*)dicItems
{
    //  NSArray *arrTitles = [[NSArray alloc]initWithArray:[dicItems objectForKey:@"arrTitles"]];
    //  NSArray *arrImages =[[NSArray alloc]initWithArray:[dicItems objectForKey:@"arrImages"]];
    
    // Create an array of SEMenuItem objects
    NSMutableArray *items = [NSMutableArray array];
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    
    
    // Calculate Y position for View
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    int topbannerImageHeight = 0;
    
    if ([self.menuResponse.isNewTicker boolValue]) {
        
        topbannerImageHeight = 50;
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            if([defaults boolForKey:@"TwoImageTemplate"]||[defaults boolForKey:@"Square Image Template"])
                topbannerImageHeight=80;
            else
                topbannerImageHeight = 107;
        }
        [self addNewsTicker ];
        
    }
    
    else if(self.menuResponse.mBannerImg && ![self.menuResponse.isNewTicker boolValue])
    {
        topbannerImageHeight = 50;
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            if([defaults boolForKey:@"TwoImageTemplate"]||[defaults boolForKey:@"Square Image Template"])
                topbannerImageHeight=80;
            else
                topbannerImageHeight = 107;
        }
        //        if([defaults boolForKey:@"TwoImageTemplate"])
        //             topbannerImageHeight = (float)240/(float)1536 * SCREEN_WIDTH;
        
        SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, topbannerImageHeight)];
        asyncImageView.backgroundColor = [UIColor clearColor];
        [asyncImageView loadImage:[self.menuResponse.mBannerImg stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
        [self.view addSubview:asyncImageView];
        asyncImageView.userInteractionEnabled = NO;
        asyncImageView.tag = 99;
        //[asyncImageView release];
    }
    
    yVal = yVal + topbannerImageHeight;
    
    // Set the default column for Iconic templet with 3 and calculated height of item based on available view height
    float singleitemHeight = ((SCREEN_HEIGHT-yVal-bottomBarButtonHeight)-15)/3;
    float singleItemWidth;
    int offset;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        singleItemWidth = 170;
        singleitemHeight = 190;
    }
    else
        singleItemWidth = 80;
    
    if ([defaults boolForKey:@"4X4 Grid"]) {
        
        if(![defaults boolForKey:@"isBottomButtonAvailable"])
        {
            offset = VARIABLE_HEIGHT(25);
        }
        else
        {
            if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
                offset = bottomBarButtonHeight + VARIABLE_HEIGHT(15);// 15 for pagination
            }
            else
                offset = bottomBarButtonHeight_iPad ;
        }
        
        NSLog(@"Screen Width = %f",[[UIScreen mainScreen] bounds].size.width);
        singleItemWidth = (SCREEN_WIDTH - 6 )/4  ; // 2 px horizontal Gap
        singleitemHeight = ((SCREEN_HEIGHT - yVal - offset) - 6)/4 ; // 2 Px vertical Gap
        
    }
    //2 image template
    if ([defaults boolForKey:@"TwoImageTemplate"]) {
        
        if(![defaults boolForKey:@"isBottomButtonAvailable"])
        {
            //  offset = VARIABLE_HEIGHT(25);
        }
        else
        {
            if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
                //  offset = bottomBarButtonHeight;// 15 for pagination
            }
            else{
                //  offset = bottomBarButtonHeight_iPad ;
            }
        }
        
        NSLog(@"Screen Width = %f",[[UIScreen mainScreen] bounds].size.width);
        if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
            singleItemWidth = (SCREEN_WIDTH - 90)/2;
            singleitemHeight = singleItemWidth*1.02;
        }
        else{
            singleItemWidth = (SCREEN_WIDTH - 330)/2;
            singleitemHeight = singleItemWidth*1.02;
        }
        NSLog(@"Screen Width = %f,%f",singleItemWidth,singleitemHeight);
        
    }
    
    //square(4) image template
    if ([defaults boolForKey:@"Square Image Template"]) {
        
        if(![defaults boolForKey:@"isBottomButtonAvailable"])
        {
            // offset = VARIABLE_HEIGHT(25);
        }
        else
        {
            if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
                //  offset = bottomBarButtonHeight+ VARIABLE_HEIGHT(15);// 15 for pagination
            }
            else{
                // offset = bottomBarButtonHeight_iPad ;
            }
        }
        
        NSLog(@"Screen Width = %f",[[UIScreen mainScreen] bounds].size.width);
        if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
            singleItemWidth = (SCREEN_WIDTH - 90)/2;
            singleitemHeight = (SCREEN_WIDTH - 90)/2;
        }
        else{
            singleItemWidth = (SCREEN_WIDTH - 210)/2 ;
            singleitemHeight = (SCREEN_WIDTH - 210)/2 ;
        }
        NSLog(@"Screen Width = %f,%f",singleItemWidth,singleitemHeight);
        
    }
    
    
    // Set Image width and heught for Rectangular Grid ..
    if ([defaults boolForKey:@"RectangularGrid"]) {
        
        if(![defaults boolForKey:@"isBottomButtonAvailable"])
        {
            offset = VARIABLE_HEIGHT(25);
        }
        else
        {
            if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
                offset = bottomBarButtonHeight + VARIABLE_HEIGHT(15);// 15 for pagination
            }
            else
                offset = bottomBarButtonHeight_iPad ;
        }
        
        NSLog(@"Screen Width = %f",[[UIScreen mainScreen] bounds].size.width);
        singleItemWidth = (SCREEN_WIDTH - 15 )/4  ; // 5 px horizontal Gap
        singleitemHeight = ((SCREEN_HEIGHT - yVal - offset) - 10)/3 ; // 5 Px vertical Gap
        
        
    }
    
    
    [defaults setInteger:singleitemHeight forKey:@"singleitemHeight"];
    [defaults setInteger:singleItemWidth forKey:@"singleItemWidth"];
    // Add SEMenuItem object in array for number of icons required
    
    for(int i=0; i < [arrMenuItems count]; i++)
    {
        
        MenuItemList* mList = [arrMenuItems objectAtIndex:i];
        
        if(menulevel > 1)
            [items addObject:[SEMenuItem initWithTitle:mList.mItemName frame:CGRectMake(0, 0, singleItemWidth,singleitemHeight) imageName:mList.mItemImg fontColor:menuResponse.smFontColor removable:NO delegate:self]];
        else
            [items addObject:[SEMenuItem initWithTitle:mList.mItemName frame:CGRectMake(0, 0, singleItemWidth,singleitemHeight) imageName:mList.mItemImg fontColor:menuResponse.mFontColor removable:NO delegate:self]];
        
    }
    
    SESpringBoard *board;
    
    // SESpringBoard view will display on Silf.view, Set the Frame and Array of SEMenuItem objects in "SESpringBoard" class
    if([arrMenuItems count] > 0)
        board = [SESpringBoard initWithTitle:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight) items:items launcherImage:nil backgroundcolor:@"#FFFFFF"];
    else
        board = [SESpringBoard initWithTitle:CGRectMake(0, topbannerImageHeight, SCREEN_WIDTH, SCREEN_HEIGHT-yVal) items:items launcherImage:nil backgroundcolor:@"#FFFFFF"];
    
    
    board.tag = 99;
    
    [self.view addSubview:board];
    
    //[arrTitles release];
    //[arrImages release];
    
}

// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    // Calculate Y position for View
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [arrMenuBottomButtonDO count]; btnLoop++)
    {
        mainMenuBottomDO *obj_mainBottomDO = [arrMenuBottomButtonDO objectAtIndex:btnLoop];
        
        viewButton = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            viewButton.contentMode = UIViewContentModeCenter;
            viewButton = [bottomButtonView initWithTitle:obj_mainBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, 80,bottomBarButtonHeight) imageName:obj_mainBottomDO.bottomBtnImg img_off:obj_mainBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            viewButton = [bottomButtonView initWithTitle:obj_mainBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrMenuBottomButtonDO  count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrMenuBottomButtonDO  count],bottomBarButtonHeight) imageName:obj_mainBottomDO.bottomBtnImg img_off:obj_mainBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [viewButton setAccessibilityLabel:obj_mainBottomDO.btnLinkTypeName];
        [viewButton setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:viewButton];
        [self.view addSubview:viewButton];
        [self.view bringSubviewToFront:viewButton];
    }
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation

{
    
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

- (BOOL)shouldAutorotate{
    
    return NO;
    
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    
    return UIInterfaceOrientationPortrait;
    
}



- (UIInterfaceOrientationMask)supportedInterfaceOrientations

{
    
    return UIInterfaceOrientationMaskPortrait;
    
}


// To Display the cuome UI for templete 3
-(void)CallTemplate_ListView
{
    // Calculate Y position for View
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    if([self.menuResponse.arBottomBtnList count] > 0)
        tblMenuListView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-bottomBarButtonHeight) style:UITableViewStylePlain];
    else
        tblMenuListView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-yVal) style:UITableViewStylePlain];
    
    tblMenuListView.delegate=self;
    tblMenuListView.dataSource=self;
    tblMenuListView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tblMenuListView.tag=99;
    tblMenuListView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:tblMenuListView];
}

// This method will get call when Any Menu Item Button Will touched
-(void)clickonMenuItem:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    [self callSubClassAccordingToSelection:tag-1];
}

-(void)callSubClassAccordingToSelection:(int)Selectedtag
{
    fromBack = YES;
    //    [defaults setValue:nil forKey:KEY_MAINMENUID];
    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    
    MenuItemList* obj_mainMenuDO = [arrMenuItems objectAtIndex:Selectedtag];
    // NSDictionary* dictList = menuResponse.arMItemList[Selectedtag];
    // [obj_mainMenuDO setValuesForKeysWithDictionary:dictList];
    
    
    [defaults setValue:[NSString stringWithFormat:@"%d", obj_mainMenuDO.mItemId] forKey:KEY_MITEMID];
    [defaults setValue:obj_mainMenuDO.linkId forKey:KEY_LINKID];
    if ([defaults valueForKey:KEY_LINKID]!=NULL) {
        
        [linkID addObject:[defaults valueForKey:KEY_LINKID]];
    }
    
    [defaults setValue:obj_mainMenuDO.mItemName forKey:KEY_MITEMNAME];
    DLog(@"%@",[defaults valueForKey:KEY_MITEMNAME]);
    if([obj_mainMenuDO.linkTypeName isEqualToString:@"GovQA"]){
        
        BOOL isRemember = [defaults boolForKey:@"govQARememberMe"];
        DLog(@"session Id = %@", [SharedManager sessionId]);
        if ([SharedManager sessionId]) {
            if(isRemember){
                
                ReportProblemViewController *reportObj = [[ReportProblemViewController alloc]initWithNibName:@"ReportProblemViewController" bundle:[NSBundle mainBundle]] ;
                reportObj.arrBottomBtnDO = arrMenuBottomButtonDO;
                [self.navigationController pushViewController:reportObj animated:YES];
                
            }
            else
            {
                ReportProblemViewController *reportObj = [[ReportProblemViewController alloc]initWithNibName:@"ReportProblemViewController" bundle:[NSBundle mainBundle]] ;
                reportObj.arrBottomBtnDO = arrMenuBottomButtonDO;
                [self.navigationController pushViewController:reportObj animated:YES];
            }
            
        }
        else
        {
            GovQALoginPageViewController *svc = [[GovQALoginPageViewController alloc]initWithNibName:@"GovQALoginPageViewController" bundle:[NSBundle mainBundle]] ;
            svc.arrBottomButtonDO=arrMenuBottomButtonDO;
            [self.navigationController pushViewController:svc animated:NO];
        }
    }
    else if([obj_mainMenuDO.linkTypeName isEqualToString:@"Band"]){
        
        BandViewController *dvc = [[BandViewController alloc]initWithNibName:@"BandViewController" bundle:[NSBundle mainBundle]];
        self.bandViewPage = dvc;
        NSLog(@"BandMenuName %@", [defaults valueForKey:KEY_MITEMNAME]);
        dvc.bandPageName = [defaults valueForKey:KEY_MITEMNAME];
        [self.navigationController pushViewController:dvc animated:NO];
        
        
    }
    else if ([obj_mainMenuDO.linkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_mainMenuDO.linkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
             [defaults setValue:[NSString stringWithFormat:@"%d", obj_mainMenuDO.mItemId] forKey:@"findSingCatMitemid"];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            singleCat.catId = [[defaults valueForKey:KEY_LINKID]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
    }
    
    else if ([obj_mainMenuDO.linkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means event - single category
        
        NSArray *nameArray = [obj_mainMenuDO.linkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                SingleEventFromMainMenu = TRUE;
                //[SingleEventCatID addObject:obj_mainMenuDO.linkId];
                [SingleEventCatID addObject:[NSString stringWithFormat:@"%ld",(long)obj_mainMenuDO.linkId]];
            }
            else
            {
                SingleCatFlag = TRUE;
                SingleEventFromMainMenu = TRUE;
                //[SingleEventCatID addObject:obj_mainMenuDO.linkId];
                [SingleEventCatID addObject:[NSString stringWithFormat:@"%ld",(long)obj_mainMenuDO.linkId]];
            }
            
            //save the category name and navigate to Event single category screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            self.iEventsListViewController = iEventsListViewController;
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        
    }
    
    else{ //other menu types
        
        switch ([[dict_linkTypeName_MenuButton objectForKey:obj_mainMenuDO.linkTypeName]intValue]){
                
            case 0:{ //city experience
                
                
                CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                self.cityExp = cevc;
                [SharedManager setRefreshAustinRetailers:YES];
                [self.navigationController pushViewController:cevc animated:NO];
                //[cevc release];
                
            }
                break;
                
            case 1: //coupon
                [self navigateToCouponsViewController];
                break;
                
            case 2: //appsite
                [self request_appsitedetails];
                break;
                
            case 3: //anything page
                requestAnythingFromMainMenu=TRUE;
                anythingShareFlag=YES;
                [UtilityManager req_hubcitiAnyThingPage:self];
               
                break;
                
            case 4:{//alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
                
            }
                break;
                
            case 5:{//deals
                
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                //[dvc release];
                
                
                
                
            }
                break;
                
            case 6://find
            {   [defaults setValue:@"subMenu"  forKey:KEY_BOTTOMBUTTONNAME];
                [defaults setValue:@"true" forKey:@"findFlag"];
                [defaults setValue:[NSString stringWithFormat:@"%d", obj_mainMenuDO.mItemId] forKey:@"findCatMitemid"];
                menuItemName=obj_mainMenuDO.mItemName;
                [self navigateToFindView];
            }
                break;
                
            case 7://scan now
                [self navigateToScanNow];
                break;
                
            case 8://whats nearby
                [defaults setValue:@"distance" forKey:@"NearByRetDistance"];
                [self navigateToWhatsNearBy];
                break;
                
            case 9:{//submenu
                
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_mainMenuDO.linkId]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                
                [self navigateToSubMenu];
            }
                break;
                
            case 10://events
                //[self navigateToEventList];
            {
                EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
                self.iEventsListViewController = iEventsListViewController;
                if([obj_mainMenuDO.linkTypeName isEqualToString:@"Band Events"])
                    iEventsListViewController.bandEvent = [NSNumber numberWithInt:1];
                
                [self.navigationController pushViewController:iEventsListViewController animated:NO];
            }
                break;
                
            case 11://preference
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
                break;
                
            case 12:{//about
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 13:if (userIdentifier==TRUE) {
                [NewsUtility signupPopUp:self];
            }
            else
            {//settings
                SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:svc animated:NO];
                //[svc release];
            }
                break;
                
            case 14://share
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {[self shareClicked:nil];}
                break;
                
            case 15:{//filters
                
                if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    
                    CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    self.cityExp = cevc;
                    [SharedManager setRefreshAustinRetailers:YES];
                    //[cevc release];
                    
                    FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    self.filters = pvc;
                    pvc.isOnlyOnePartner = YES;
                    [self.navigationController pushViewController:pvc animated:NO];
                    //[pvc release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    self.filterList = filterList;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                }
            }
                break;
                
            case 16:
                
            {
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                self.faqList = faqCatList;
                //[faqCatList release];
                
                
            }break;
                
            case 17:
                
            {
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }break;
            case 18:
                
            {            if (userIdentifier==TRUE) {
                [NewsUtility signupPopUp:self];
            }
            else{
                iWebRequestState = GETUSERINFO;;
                [self requestToGetUserData];
            }
            }break;
            case 19:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else{
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                    
                }
            }break;
                
            case 20:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else{
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        
                       
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        //[inform release];
                    }
                }
                
                
            }break;
            case 21:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                    }
                    else
                    {
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                            
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            //    [citPref release];
                        }
                    }
                }
                
            }break;
            case 22:
            {
                BandsNearByEventsMapTable *eventList = [[BandsNearByEventsMapTable alloc] initWithNibName:@"BandsNearByEventsMapTable" bundle:nil];
                self.todayEventList = eventList;
                eventList.fromTodayEvents = TRUE;
                [self.navigationController pushViewController:eventList animated:YES];
            }
                break;
            
            case 24 :
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 25:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
              //  privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
            default:
                break;
        }
    }
}

// This method will call when any Bottom Button Will touched
-(void)bottomButtonPressed:(id)sender
{
    fromBack = YES;
    [defaults setBool:YES forKey:BottomButton];
    //  [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    BottomItemList* obj_mainMenuBottomDO = [[BottomItemList alloc] init];
    NSDictionary* dictList = menuResponse.arBottomBtnList[tag];
    [obj_mainMenuBottomDO setValuesForKeysWithDictionary:dictList];
    
    // mainMenuBottomDO *obj_mainMenuBottomDO = [arrMenuBottomButtonDO objectAtIndex:tag];
    
    [defaults setValue:obj_mainMenuBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[dict_linkTypeName_BottomButton objectForKey:obj_mainMenuBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_mainMenuBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_mainMenuBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_mainMenuBottomDO.bottomBtnName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_mainMenuBottomDO.btnLinkTypeName);
    
    if ([obj_mainMenuBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
        
        NSArray *nameArray = [obj_mainMenuBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            singleCat.catId = [[defaults valueForKey:KEY_LINKID]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        else
            return;
    }
    else if ([obj_mainMenuBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_mainMenuBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_mainMenuBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_mainMenuBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_mainMenuBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_mainMenuBottomDO.bottomBtnID];
            }
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            self.iEventsListViewController = iEventsListViewController;
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    else{ //other menu types
        
        switch ([[dict_linkTypeName_BottomButton objectForKey:obj_mainMenuBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                self.hotDeals = hotDeals;
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                //[hotDeals release];
                break;
                
            case 1://Scan Now
            {
                [self navigateToScanNow];
            }
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    [EventsBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_mainMenuBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_mainMenuBottomDO.bottomBtnID];
                    [EventsBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                }
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [defaults setValue:nil forKey:@"NearByRetDistance"];
                [NearbyBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                [self navigateToFindView];
            }
                break;
                
                
            case 6:{//City Experience
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                self.cityExp = cevc;
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                [SharedManager setRefreshAustinRetailers:YES];
                [self.navigationController pushViewController:cevc animated:NO];
                //[cevc release];
            }
                break;
                
            case 7://City Services
                
                break;
                
            case 8://Visitors Bureau
                
                break;
                
            case 9://transportation
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];}
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                [UtilityManager req_hubcitiAnyThingPage:self];
                
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_mainMenuBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                [self navigateToSubMenu];
                break;
                
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                    
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    
                    CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    self.cityExp = cevc;
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    self.filters = filters;
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    self.filterList = filterList;
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                // [dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                self.faqList = faqCatList;
                //[faqCatList release];
                
            }
                break;
                
            case 21://Fundraisers
                
            {
                [FundraiserBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                [FundraiserBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                //[iFundraiserListViewController release];
                
            }break;
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else{
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        //[inform release];
                    }
                }
                
                
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                    }
                    else{
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                            
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            //  [citPref release];
                        }
                    }
                }
                
            }break;
                
            case 27:
            case 28://Filters-sort
            {
                SubMenuGroupingAndSortingViewController *iSubMenuGroupingAndSortingViewController = [[SubMenuGroupingAndSortingViewController alloc]initWithNibName:@"SubMenuGroupingAndSortingViewController" bundle:[NSBundle mainBundle]];
                
                [self.navigationController pushViewController:iSubMenuGroupingAndSortingViewController animated:YES];
            }
                break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
               // privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
            default:
                break;
        }
    }
    //NSlog(@"%d",tag);
}

#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        return;
    [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    [self showActionSheet];
    
}

-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = viewButton;
        actionSheet.popoverPresentationController.sourceRect = viewButton.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
   
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}

-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
          
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark request methods


-(void)requestmenudisplay
{
    [self releaseAllocatedObjects];
    [HubCitiAppDelegate showActivityIndicator];
    
    MainMenuRequest* mainMenu = [[MainMenuRequest alloc] init];
    
    int level = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(level<=1)
    {
        
        [NewsUtility refreshLinkDeptArray];
        
    }
    
    if ([cashedResponse valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]] && subMenuSortDone==NO)
    {
        
        if (fromBack == YES ) {
            
            mainMenu.dateCreation = [dateCreated valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
            
            fromBack = NO;
        }
        else if (![[defaults valueForKey:@"CityPreferenceUpdated"] isEqualToString:@"1"]) {
            DLog(@"%@",[[HubCitiAppDelegate getLinkIdArray]lastObject]);
            
            mainMenu.dateCreation = [dateCreated valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
        }
        
        else{
            
            DLog(@"%@",[[HubCitiAppDelegate getLinkIdArray]lastObject]);
        }
    }
    else
    {
        mainMenu.dateCreation = @"";
    }
    DLog(@"%lu",(unsigned long)[[HubCitiAppDelegate getDeptIdArray]count]);
    mainMenu.sortOrder = [defaults valueForKey:@"sortOrder"];
    
    
    DLog(@"%@",[selectedSortCityDic valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]]);
    
    if(![defaults valueForKey:KEY_DONESORT] && [RegionApp isEqualToString:@"1"]){
        mainMenu.cityIds = [defaults valueForKey:@"sortedCitiIDs"];
    }
    
    if ([selectedSortCityDic valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]] != [NSNull null] && [selectedSortCityDic valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]] != NULL)
    {
        if([defaults valueForKey:KEY_DONESORT]){
            mainMenu.cityIds = [selectedSortCityDic valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
            
        }
        mainMenu.departmentId = @"0";
        mainMenu.typeId = @"0";
        
    }
    else{
        
        if ([deptId valueForKey:[[HubCitiAppDelegate getLinkIdArray] lastObject]]==NULL) {
            
            mainMenu.departmentId = @"0";
            mainMenu.typeId = @"0";
        }
        else{
            
            mainMenu.departmentId = [deptId valueForKey:[[HubCitiAppDelegate getLinkIdArray] lastObject]];
            mainMenu.typeId = [typeId valueForKey:[[HubCitiAppDelegate getLinkIdArray] lastObject]];
            
        }
    }
    
    
    
    mainMenu.deviceId = [defaults valueForKey:KEY_DEVICEID];
    
    mainMenu.osVersion = [[UIDevice currentDevice] systemVersion];
    
    if (level > 0) {
        
        mainMenu.linkId = [[HubCitiAppDelegate getLinkIdArray]objectAtIndex:level-1];
        mainMenu.platform = @"IOS";
    }
    else
    {
        mainMenu.linkId = @"0";
    }
    
    mainMenu.platform = @"IOS";
    
    mainMenu.hubCitiId = [defaults valueForKey:KEY_HUBCITIID];
    //  mainMenu.userId = [defaults valueForKey:KEY_USERID];
    
    
    //    mainMenu.hubCitiId = @"82";
    mainMenu.level = [defaults valueForKey:KEY_MENULEVEL];
    
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    if (mainMenu.linkId) {
        [parameters setValue:mainMenu.linkId forKey:@"linkId"];
    }
    if (mainMenu.hubCitiId) {
        [parameters setValue:mainMenu.hubCitiId forKey:@"hubCitiId"];
    }
    if (mainMenu.level) {
        [parameters setValue:mainMenu.level forKey:@"level"];
    }
    if (mainMenu.sortOrder) {
        [parameters setValue:mainMenu.sortOrder forKey:@"sortOrder"];
    }
    if (IPAD) {
        [parameters setValue:@"Ipad" forKey:@"devType"];
    }
    if(cashedResponse.count > 0 && isSameUser && level <= 1)
    {
        isSameUser = false;
        [parameters setValue:[defaults objectForKey :@"DateModified"] forKey:@"dateCreated"];
    }
    
    if (mainMenu.dateCreation.length > 0) {
        [parameters setValue:mainMenu.dateCreation forKey:@"dateCreated"];
    }
    if([RegionApp isEqualToString:@"1"] ) { // hubRegion
        
        //NSMutableString* citiId = [defaults objectForKey:@"sortedCitiIDs"];
        
        if (mainMenu.cityIds) {
            [parameters setValue:mainMenu.cityIds forKey:@"cityIds"];
        }
        else{
            [parameters setValue:[defaults objectForKey:@"sortedCitiIDs"] forKey:@"cityIds"];
        }
        
        if (mainMenu.departmentId) {
            [parameters setValue:mainMenu.departmentId forKey:@"departmentId"];
        }
        if (mainMenu.typeId) {
            [parameters setValue:mainMenu.typeId forKey:@"typeId"];
            
        }
        
    }
    else // hubciti
        
    {
        
        if (mainMenu.departmentId) {
            [parameters setValue:mainMenu.departmentId forKey:@"departmentId"];
        }
        if (mainMenu.typeId) {
            [parameters setValue:mainMenu.typeId forKey:@"typeId"];
            
        }
        
    }
    
    
    DLog(@"parametr: %@",parameters);
    iWebRequestState = MENUDISPLAY;
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = @"";
    if([RegionApp isEqualToString:@"1"]) {
        urlString = [NSString stringWithFormat:@"%@firstuse/hubregionmenudisplay",BASE_URL];
    }
    else
        urlString = [NSString stringWithFormat:@"%@firstuse/hubmenudisplay",BASE_URL];
    
    
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    
    /*  NSMutableString *xmlStr = [[NSMutableString alloc] init];
     [xmlStr appendFormat:@"<Menu><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
     [xmlStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
     [xmlStr appendFormat:@"<level>%@</level>",[defaults valueForKey:KEY_MENULEVEL]];
     
     int level = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
     
     // Delete all object and add array object to resolve cdition like if from other view directly main menu will get call
     if(level<=1)
     {
     
     [defaults setValue:@"NONE" forKey:@"sortOrder"];
     [defaults setValue:nil forKey:@"commaSeperatedCities"];
     
     [HubCitiAppDelegate refreshLinkIdArray];
     [[HubCitiAppDelegate getLinkIdArray]addObject:@"0"];
     
     
     DLog(@"%lu",(unsigned long)[[HubCitiAppDelegate getLinkIdArray]count]);
     
     [selectedFilterOption setValue:@"0" forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
     [selectedSortOption setValue:@"-1" forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
     DLog(@"%ld",(long)[[selectedSortOption valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]]integerValue]);
     
     [HubCitiAppDelegate refreshDeptIdArray];
     [[HubCitiAppDelegate getDeptIdArray]addObject:@"0"];
     [deptId setValue:@"0" forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
     
     DLog(@"%lu",(unsigned long)[[HubCitiAppDelegate getDeptIdArray]count]);
     
     DLog(@"%@",[deptId valueForKey:[[HubCitiAppDelegate getLinkIdArray] lastObject]]);
     
     [HubCitiAppDelegate refreshTypeIdArray];
     [[HubCitiAppDelegate getTypeIdArray]addObject:@"0"];
     [typeId setValue:@"0" forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
     
     [selectedSortCityDic setObject:[NSNull null] forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
     
     }
     
     
     DLog(@"%@",[defaults valueForKey:@"CityPreferenceUpdated"]);
     
     if ([cashedResponse valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]] && subMenuSortDone==NO) {
     
     if (fromBack == YES ) {
     [xmlStr appendFormat:@"<dateCreated>%@</dateCreated>",[dateCreated valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]]];
     fromBack = NO;
     }
     else if (![[defaults valueForKey:@"CityPreferenceUpdated"] isEqualToString:@"1"]) {
     DLog(@"%@",[[HubCitiAppDelegate getLinkIdArray]lastObject]);
     [xmlStr appendFormat:@"<dateCreated>%@</dateCreated>",[dateCreated valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]]];
     }
     
     else{
     DLog(@"%@",[[HubCitiAppDelegate getLinkIdArray]lastObject]);
     }
     }
     DLog(@"%lu",(unsigned long)[[HubCitiAppDelegate getDeptIdArray]count]);
     [xmlStr appendFormat:@"<sortOrder>%@</sortOrder>",[defaults valueForKey:@"sortOrder"]];
     
     
     DLog(@"%@",[selectedSortCityDic valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]]);
     
     if ([selectedSortCityDic valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]] != [NSNull null] && [selectedSortCityDic valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]] != NULL) {
     [xmlStr appendFormat:@"<cityIds>%@</cityIds>",[selectedSortCityDic valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]]];
     [xmlStr appendFormat:@"<departmentId>%@</departmentId>",@"0"];
     [xmlStr appendFormat:@"<typeId>%@</typeId>",@"0"];
     
     }
     else{
     
     if ([deptId valueForKey:[[HubCitiAppDelegate getLinkIdArray] lastObject]]==NULL) {
     [xmlStr appendFormat:@"<departmentId>%@</departmentId>",@"0"];
     [xmlStr appendFormat:@"<typeId>%@</typeId>",@"0"];
     }
     else{
     [xmlStr appendFormat:@"<departmentId>%@</departmentId>",[deptId valueForKey:[[HubCitiAppDelegate getLinkIdArray] lastObject]]];
     [xmlStr appendFormat:@"<typeId>%@</typeId>",[typeId valueForKey:[[HubCitiAppDelegate getLinkIdArray] lastObject]]];
     }
     }
     
     if([defaults valueForKey:KEY_DEVICEID])
     [xmlStr appendFormat:@"<deviceId>%@</deviceId>",[defaults valueForKey:KEY_DEVICEID]];
     
     [xmlStr appendFormat:@"<osVersion>%@</osVersion>",[[UIDevice currentDevice] systemVersion]];
     
     //"<sortOrder>None</sortOrder>" needs to be added when sorting implemented for submenus
     [xmlStr appendFormat:@"<linkId>%@</linkId><platform>IOS</platform></Menu>",[[HubCitiAppDelegate getLinkIdArray]objectAtIndex:level-1]];
     
     NSString *urlString = [NSString stringWithFormat:@"%@firstuse/menudisplay",BASE_URL];
     
     [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
     ReleaseAndNilify(xmlStr);*/
    
}

//get user favorite categories
//-(void)request_GetUserCat{
//
//    iWebRequestState = GETFAVCATEGORIES;
//    if([Network currentReachabilityStatus]==0)
//    {
//        [common showAlert];
//    }
//    else {
//
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//
//    }
//}

-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}


-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=TRUE;
    anythingShareFlag=YES;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    [defaults setObject:[linkID objectAtIndex:[linkID count]-1]forKey:KEY_ANYPAGEID];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

#pragma mark parse methods
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case UPDATE_DEVICE:
        {
            
        }
            break;
        case MENUDISPLAY:
        {
            [self renderMenuDisplay:response];
            [self setBackgroundColorandImage];
        }
            break;
        case MAINMENU_GETCUSTOMUI:
        {
            
        }
            break;
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
            
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
            
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
            //        case catsearch:
            //            [self parse_sscatsearch:response];
            //            break;
            
        default:
            break;
    }
}



/*
 *
 Parsing logic when response received for getting user details.
 Method called when navigated from Settings screen.
 *
 */

-(void)parsemenudisplay:(NSString*)responseXML
{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXML])
        return;
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXML];
    
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *isTempChangedElement = [TBXML childElementNamed:@"isTempChanged" parentElement:tbXml.rootXMLElement];
        if ([[TBXML textForElement:isTempChangedElement]isEqualToString:@"1"]) {
            //for share functionality
            TBXMLElement *downLoadLinkElement = [TBXML childElementNamed:@"downLoadLink" parentElement:tbXml.rootXMLElement];
            if (downLoadLinkElement!=nil){
                
                [defaults setValue:[TBXML textForElement:downLoadLinkElement] forKey:KEY_HYPERLINK];
            }
            
            TBXMLElement *androidLinkElement = [TBXML childElementNamed:@"androidLink" parentElement:tbXml.rootXMLElement];
            if (androidLinkElement!=nil){
                
                androidDownloadLink = [[TBXML textForElement:androidLinkElement]copy];
            }
            else
                androidDownloadLink = @"";
            
            [defaults setValue:androidDownloadLink forKey:@"androidDownloadLink"];
            
            TBXMLElement *appIconImgElement = [TBXML childElementNamed:@"appIconImg" parentElement:tbXml.rootXMLElement];
            if (appIconImgElement!=nil){
                
                [defaults setValue:[TBXML textForElement:appIconImgElement] forKey:KEY_PRODUCTIMGPATH];
            }
            
            TBXMLElement *menuNameElement = [TBXML childElementNamed:@"menuName" parentElement:tbXml.rootXMLElement];
            if (menuNameElement!=nil){
                
                [defaults setValue:[TBXML textForElement:menuNameElement] forKey:KEY_MENUNAME];
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                if(menulevel > 1)
                {
                    //                    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 320, 40)];
                    //                    titleLabel.textAlignment = NSTextAlignmentCenter;
                    //                    titleLabel.textColor = [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]];
                    //                    titleLabel.backgroundColor = [UIColor clearColor];
                    //                    titleLabel.numberOfLines = 2;
                    //                    [titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
                    //                    titleLabel.font = [UIFont boldSystemFontOfSize:16];
                    //                    titleLabel.text = [defaults valueForKey:KEY_MENUNAME];
                    self.navigationItem.title = [defaults valueForKey:KEY_MENUNAME];
                    //[titleLabel release];
                }
                
            }
            
            //For filters
            TBXMLElement *retAffCountElement = [TBXML childElementNamed:@"retAffCount" parentElement:tbXml.rootXMLElement];
            if (retAffCountElement)
                [SharedManager setFiltersCount:[[TBXML textForElement:retAffCountElement]intValue]];
            
            TBXMLElement *retAffIdElement = [TBXML childElementNamed:@"retAffId" parentElement:tbXml.rootXMLElement];
            if (retAffIdElement)
                [SharedManager setRetAffId:[[TBXML textForElement:retAffIdElement]intValue]];
            
            TBXMLElement *retAffNameElement = [TBXML childElementNamed:@"retAffName" parentElement:tbXml.rootXMLElement];
            if (retAffNameElement)
                [defaults setValue:[TBXML textForElement:retAffNameElement] forKey:@"Title"];
            
            
            TBXMLElement *dateCreatedElement = [TBXML childElementNamed:@"dateModified" parentElement:tbXml.rootXMLElement];
            
            TBXMLElement *menuIdElement = [TBXML childElementNamed:@"menuId" parentElement:tbXml.rootXMLElement];
            TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:@"hubCitiId" parentElement:tbXml.rootXMLElement];
            TBXMLElement *templateNameElement = [TBXML childElementNamed:@"templateName" parentElement:tbXml.rootXMLElement];
            TBXMLElement *mBannerImgmBannerImgElement = [TBXML childElementNamed:@"mBannerImg" parentElement:tbXml.rootXMLElement];
            
            TBXMLElement *mBkgrdColorElement = [TBXML childElementNamed:@"mBkgrdColor" parentElement:tbXml.rootXMLElement];
            TBXMLElement *mBkgrdImageElement = [TBXML childElementNamed:@"mBkgrdImage" parentElement:tbXml.rootXMLElement];
            TBXMLElement *smBkgrdColorElement = [TBXML childElementNamed:@"smBkgrdColor" parentElement:tbXml.rootXMLElement];
            TBXMLElement *smBkgrdImageElement = [TBXML childElementNamed:@"smBkgrdImage" parentElement:tbXml.rootXMLElement];
            
            TBXMLElement *departFlagElement = [TBXML childElementNamed:@"departFlag" parentElement:tbXml.rootXMLElement];
            TBXMLElement *typeFlagElement = [TBXML childElementNamed:@"typeFlag" parentElement:tbXml.rootXMLElement];
            
            TBXMLElement *noOfColumnsElement = [TBXML childElementNamed:@"noOfColumns" parentElement:tbXml.rootXMLElement];
            
            TBXMLElement *isRegAppElement = [TBXML childElementNamed:@"isRegApp" parentElement:tbXml.rootXMLElement];
            
            
            [defaults setValue:@"0" forKey:@"noOfColumns"];
            
            
            if (dateCreatedElement) {
                
                [dateCreated setValue:[TBXML textForElement:dateCreatedElement] forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
                [cashedResponse setValue:responseXML forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
            }
            
            if(noOfColumnsElement)
                [defaults setValue:[TBXML textForElement:noOfColumnsElement] forKey:@"noOfColumns"];
            
            
            //for city experience bottom bar image
            TBXMLElement *retGroupImgElement = [TBXML childElementNamed:@"retGroupImg" parentElement:tbXml.rootXMLElement];
            if(retGroupImgElement){
                [defaults setValue:[TBXML textForElement:retGroupImgElement] forKey:@"retGroupImg"];
                
                //add the highlighted imagepath
                NSString *path = [defaults valueForKey:@"retGroupImg"];
                NSString *extn = [path pathExtension]; // This is .png in this case
                NSString *finalPath = [[[path stringByDeletingPathExtension] stringByAppendingString:@"_On"] stringByAppendingPathExtension:extn];
                [defaults setValue:finalPath forKey:@"retGroupImg_On"];
            }
            else{
                [defaults setValue:nil forKey:@"retGroupImg"];
                [defaults setValue:nil forKey:@"retGroupImg_On"];
            }
            
            if(departFlagElement)
                [defaults setValue:[TBXML textForElement:departFlagElement] forKey:@"departFlag"];
            
            if(typeFlagElement)
                [defaults setValue:[TBXML textForElement:typeFlagElement] forKey:@"typeFlag"];
            
            
            if(mBkgrdColorElement)
                mBkgrdColor = [[NSString alloc]initWithString:[TBXML textForElement:mBkgrdColorElement]];
            if(mBkgrdImageElement)
                mBkgrdImage = [[NSString alloc]initWithString:[TBXML textForElement:mBkgrdImageElement]];
            
            if(smBkgrdColorElement)
                smBkgrdColor = [[NSString alloc]initWithString:[TBXML textForElement:smBkgrdColorElement]];
            if(smBkgrdImageElement)
                smBkgrdImage = [[NSString alloc]initWithString:[TBXML textForElement:smBkgrdImageElement]];
            if(menuIdElement)
                [defaults setValue:[TBXML textForElement:menuIdElement] forKey:@"menuId"];
            
            if(isRegAppElement)
                [defaults setValue:[TBXML textForElement:isRegAppElement] forKey:RegionApp];
            if(![mBkgrdColor isEqualToString:@"N/A"])
                [defaults setValue:mBkgrdColor forKey:@"backgroundColor"];
            else
                [defaults setValue:smBkgrdColor forKey:@"backgroundColor"];
            
            if(menuIdElement)
            {
                arrMenuDO = [[NSMutableArray alloc]init];
                arrMenuBottomButtonDO = [[NSMutableArray alloc]init];
                
                [defaults setValue:[TBXML textForElement:hubCitiIdElement] forKey:KEY_HUBCITIID];
                [defaults setValue:Nil forKey:@"TopBannerImage"];
                
                if(mBannerImgmBannerImgElement)
                    [defaults setValue:[TBXML textForElement:mBannerImgmBannerImgElement] forKey:@"TopBannerImage"];
                
                // Assign and ID for Templet as per Name of that
                if(templateNameElement)
                {
                    NSString *strName = [TBXML textForElement:templateNameElement];
                    if([strName isEqualToString:@"Grouped Tab"])
                        iTempletsMenuId = GroupedTab;
                    else if([strName isEqualToString:@"Single/Two Column Tab"])
                        iTempletsMenuId = TwoColumnTab;
                    else if([strName isEqualToString:@"List View"])
                        iTempletsMenuId = ListView;
                    else if([strName isEqualToString:@"Iconic Grid"] || [strName isEqualToString:@"Square Grid"] || [strName isEqualToString:@"Iconic 3X3"] || [strName isEqualToString:@"Square Grid  3x3"]){
                        iTempletsMenuId = IconicGrid;
                        if ([strName isEqualToString:@"Square Grid"]) {
                            [defaults setBool:YES forKey:@"SquareGrid"];
                        }
                        else if([strName isEqualToString:@"Iconic 3X3"]){
                            [defaults setBool:YES forKey:@"Iconic3X3"];
                        }
                        else if( [strName isEqualToString:@"Square Grid  3x3"]){
                            [defaults setBool:YES forKey:@"SquareGrid3x3"];
                        }

                    }
                    else if([strName isEqualToString:@"Two Column Tab with Banner Ad"])
                        iTempletsMenuId = TwoColumnTabWithBannerAd;
                    else if([strName isEqualToString:@"Combo Template"])
                        iTempletsMenuId = ComboTemplate;
                    else if([strName isEqualToString:@"Grouped Tab With Image"])
                        iTempletsMenuId = GroupedTabWithImage;
                    else if([strName isEqualToString:@"List View with Banner Ad"])
                        iTempletsMenuId = ListViewWithBannerAd;
                    
                    else
                        iTempletsMenuId = GroupedTab; // To show Default Templete if coming wrong from server
                }
                
                
            }
            
            NSMutableArray *arrTitles = [[NSMutableArray alloc]init];
            NSMutableArray *arrImages = [[NSMutableArray alloc]init];
            
            TBXMLElement *MenuFlagElement = [TBXML childElementNamed:@"MenuItem" parentElement:tbXml.rootXMLElement];
            while(MenuFlagElement)
            {
                mainMenuDO *obj_mainMenuDO = [[mainMenuDO alloc]init];
                
                TBXMLElement *mItemIdElement = [TBXML childElementNamed:@"mItemId" parentElement:MenuFlagElement];
                TBXMLElement *linkTypeIdElement = [TBXML childElementNamed:@"linkTypeId" parentElement:MenuFlagElement];
                TBXMLElement *linkIdElement = [TBXML childElementNamed:@"linkId" parentElement:MenuFlagElement];
                TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:MenuFlagElement];
                TBXMLElement *mItemNameElement = [TBXML childElementNamed:@"mItemName" parentElement:MenuFlagElement];
                TBXMLElement *mItemImgElement = [TBXML childElementNamed:@"mItemImg" parentElement:MenuFlagElement];
                TBXMLElement *linkTypeNameElement = [TBXML childElementNamed:@"linkTypeName" parentElement:MenuFlagElement];
                
                TBXMLElement *mBtnColorElement = [TBXML childElementNamed:@"mBtnColor" parentElement:MenuFlagElement];
                TBXMLElement *mBtnFontColorElement = [TBXML childElementNamed:@"mBtnFontColor" parentElement:MenuFlagElement];
                
                TBXMLElement *smBtnColorElement = [TBXML childElementNamed:@"smBtnColor" parentElement:MenuFlagElement];
                TBXMLElement *smBtnFontColorElement = [TBXML childElementNamed:@"smBtnFontColor" parentElement:MenuFlagElement];
                
                //Added for Grouped template and Combo template
                TBXMLElement *mGrpBkgrdColorElement = [TBXML childElementNamed:@"mGrpBkgrdColor" parentElement:MenuFlagElement];
                TBXMLElement *mGrpFntColorElement = [TBXML childElementNamed:@"mGrpFntColor" parentElement:MenuFlagElement];
                TBXMLElement *smGrpBkgrdColorElement = [TBXML childElementNamed:@"smGrpBkgrdColor" parentElement:MenuFlagElement];
                TBXMLElement *smGrpFntColorElement = [TBXML childElementNamed:@"smGrpFntColor" parentElement:MenuFlagElement];
                
                
                TBXMLElement *mShapeIdElement = [TBXML childElementNamed:@"mShapeId" parentElement:MenuFlagElement];
                TBXMLElement *mShapeNameElement = [TBXML childElementNamed:@"mShapeName" parentElement:MenuFlagElement];
                
                //Added for Iconic and Combo template
                if(iTempletsMenuId==ComboTemplate || iTempletsMenuId==IconicGrid)
                {
                    TBXMLElement *mFontColorElement = [TBXML childElementNamed:@"mFontColor" parentElement:tbXml.rootXMLElement];
                    TBXMLElement *smFontColorElement = [TBXML childElementNamed:@"smFontColor" parentElement:tbXml.rootXMLElement];
                    if (mFontColorElement) {
                        obj_mainMenuDO.mFontColor=[[NSString alloc]initWithString:[TBXML textForElement:mFontColorElement]];
                    }
                    
                    if (smFontColorElement) {
                        obj_mainMenuDO.smFontColor=[[NSString alloc]initWithString:[TBXML textForElement:smFontColorElement]];
                    }
                    
                }
                
                if(mShapeIdElement)
                    obj_mainMenuDO.mShapeId = [[NSString alloc]initWithString:[TBXML textForElement:mShapeIdElement]];
                
                if(mShapeNameElement)
                    obj_mainMenuDO.mShapeName = [[NSString alloc]initWithString:[TBXML textForElement:mShapeNameElement]];
                
                
                if(mGrpBkgrdColorElement)
                    obj_mainMenuDO.mGrpBkgrdColor = [[NSString alloc]initWithString:[TBXML textForElement:mGrpBkgrdColorElement]];
                
                if(mGrpFntColorElement)
                    obj_mainMenuDO.mGrpFntColor = [[NSString alloc]initWithString:[TBXML textForElement:mGrpFntColorElement]];
                
                if(smGrpBkgrdColorElement)
                    obj_mainMenuDO.smGrpBkgrdColor = [[NSString alloc]initWithString:[TBXML textForElement:smGrpBkgrdColorElement]];
                
                if(smGrpFntColorElement)
                    obj_mainMenuDO.smGrpFntColor = [[NSString alloc]initWithString:[TBXML textForElement:smGrpFntColorElement]];
                
                
                if(smBtnColorElement)
                    obj_mainMenuDO.smBtnColor = [[NSString alloc]initWithString:[TBXML textForElement:smBtnColorElement]];
                
                if(smBtnFontColorElement)
                    obj_mainMenuDO.smBtnFontColor = [[NSString alloc]initWithString:[TBXML textForElement:smBtnFontColorElement]];
                
                if(mBtnColorElement)
                    obj_mainMenuDO.mBtnColor = [[NSString alloc]initWithString:[TBXML textForElement:mBtnColorElement]];
                
                if(mBtnFontColorElement)
                    obj_mainMenuDO.mBtnFontColor = [[NSString alloc]initWithString:[TBXML textForElement:mBtnFontColorElement]];
                
                if(positionElement)
                    obj_mainMenuDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                
                if(linkTypeNameElement){
                    obj_mainMenuDO.linkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:linkTypeNameElement]];
                    
                    
                    
                    [previousMenuItem setString:@""];
                    [previousMenuItem appendString:nextMenuItem];
                    [nextMenuItem setString:@""];
                    [nextMenuItem appendString:obj_mainMenuDO.linkTypeName];
                    
                }
                
                if(mItemIdElement)
                    obj_mainMenuDO.mItemId = [[NSString alloc]initWithString:[TBXML textForElement:mItemIdElement]];
                
                if(linkTypeIdElement)
                    obj_mainMenuDO.linkTypeId = [[NSString alloc]initWithString:[TBXML textForElement:linkTypeIdElement]];
                
                
                if(linkIdElement)
                    obj_mainMenuDO.linkId = [[NSString alloc]initWithString:[TBXML textForElement:linkIdElement]];
                
                
                
                if(mItemNameElement)
                {
                    obj_mainMenuDO.mItemName = [[NSString alloc]initWithString:[TBXML textForElement:mItemNameElement]];
                    [arrTitles addObject:[TBXML textForElement:mItemNameElement]];
                }
                if(mItemImgElement)
                {
                    obj_mainMenuDO.mItemImg = [[NSString alloc]initWithString:[TBXML textForElement:mItemImgElement]];
                    [arrImages addObject:[TBXML textForElement:mItemImgElement]];
                }
                
                MenuFlagElement = [TBXML nextSiblingNamed:@"MenuItem" searchFromElement:MenuFlagElement];
                
                
                
                
                // Added for Grouped tab / Grouped List View With Images
                if ([previousMenuItem isEqualToString:@"Text"]&&[nextMenuItem isEqualToString:@"Text"]) {
                    [arrMenuDO removeLastObject];
                }
                
                
                
                
                //            if ([arrMenuDO count]==1 && [obj_mainMenuDO.linkTypeName isEqualToString:@"Text"] && MenuFlagElement == nil) {
                //                [arrMenuDO removeAllObjects];
                //            }
                
                [arrMenuDO addObject:obj_mainMenuDO];
                //[obj_mainMenuDO release];
            }
            
            if ([arrMenuDO count]>1&&[nextMenuItem isEqualToString:@"Text"]) {
                [arrMenuDO removeLastObject];
            }
            
            
            
            TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:tbXml.rootXMLElement];
            while(BottomButtonElement)
            {
                mainMenuBottomDO *obj_mainMenuBottomDO = [[mainMenuBottomDO alloc]init];
                //*********************//
                bottomBtn = YES;
                TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                
                
                if(bottomBtnIDElement)
                    obj_mainMenuBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                
                if(bottomBtnNameElement)
                    obj_mainMenuBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                else
                    obj_mainMenuBottomDO.bottomBtnName = @" ";
                
                if(bottomBtnImgElement)
                    obj_mainMenuBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                else
                    obj_mainMenuBottomDO.bottomBtnImg = @" ";
                
                if(bottomBtnImgOffElement)
                    obj_mainMenuBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                else
                    obj_mainMenuBottomDO.bottomBtnImgOff = @" ";
                
                if(btnLinkTypeIDElement)
                    obj_mainMenuBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                
                if(btnLinkTypeNameElement)
                    obj_mainMenuBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                
                if(btnLinkIDElement){
                    obj_mainMenuBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                    if(btnLinkTypeNameElement){
                        if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
                            [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
                    }
                }
                
                if(positionElement)
                    obj_mainMenuBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                
                
                
                BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                [arrMenuBottomButtonDO addObject:obj_mainMenuBottomDO];
                // [obj_mainMenuBottomDO release];
            }
            
            if([arrMenuBottomButtonDO count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                [self setBottomBarMenu];
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            
            [self callTemplate: arrTitles Images:arrImages mainMenuItemObjects:[arrMenuDO mutableCopy]];
            
            
        }
        else{
            [self releaseAllocatedObjects];
            [self parsemenudisplay:[cashedResponse valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]]];
        }
    }
    else if([[TBXML textForElement:saveResponseCode]isEqualToString:@"10005"])
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        NSString *response=[TBXML textForElement:saveResponseText];
        if([response isEqualToString:@"No Records Found."])
        {
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:response];
        }
        else{
            NSString *responseTextStr = [[TBXML textForElement:saveResponseText] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
            [UtilityManager showFormatedAlert:responseTextStr];
        }
        
        
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        NSString *response=[TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:response];
    }
    
    
    
}
-(void)presentSpecialOffersAfterDismissAleert {
    
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
        if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        splOfferVC.fromMainmenu = TRUE;
        splOfferVC.didDismiss = ^(NSString *data) {
            // this method gets called in MainVC when your SecondVC is dismissed
            isModalDismiss = YES;
            NSLog(@"Dismissed SecondViewController");
        };

        [self presentViewController:splOfferVC animated:YES completion:nil];
        
    }

}






//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        [UtilityManager showAlert:@"Error!" msg:@"Please try after sometime"];
        
       
        return;
    }
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCodeElement != nil && [[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {//means we have got proper response
        
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:settings animated:NO];
        //[settings release];
        
    }
    
    else {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    ReleaseAndNilify(tbxml);
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        
      
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        //        NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
        //         NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
        //        if([name isEqualToString:@"3000.htm"]){
        //            [HubCitiAppDelegate setIsLogistics:YES];
        //
        //        }
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        //        TBXMLElement *retailId = [TBXML childElementNamed:@"retailerId" parentElement:tbxml.rootXMLElement];
        //        if (retailId!=Nil) {
        //            [defaults setObject:[TBXML textForElement:retailId] forKey:KEY_RETAILERID];
        //            NSLog(@"The value is %@",[defaults valueForKey:KEY_RETAILERID]);
        //        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString] containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                             
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
             dispatch_async(dispatch_get_main_queue(), ^{
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
             });
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
            
            
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
       
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}

#pragma mark tableView delegate and datasource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrMenuItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 50.0;
    }
    else
    {
        return 80.0;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    // mainMenuDO *imainMenuDO = [arrMenuDO objectAtIndex:indexPath.row];
    MenuItemList* arMItemList = [arrMenuItems objectAtIndex:indexPath.row];
    // NSDictionary* dictList = menuResponse.arMItemList[indexPath.row];
    //[arMItemList setValuesForKeysWithDictionary:dictList];
    
    static NSString *CellIdentifier = @"CellMainListView";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = cell.frame;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        
        if (menulevel>1){
            //cell.backgroundColor = [UIColor convertToHexString:imainMenuDO.smBtnColor];
            // cell.backgroundColor = bView.backgroundColor;
            btn.backgroundColor = [UIColor convertToHexString:arMItemList.smBtnColor];
        }
        
        else{
            btn.backgroundColor = [UIColor convertToHexString:arMItemList.mBtnColor];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"right_arrow"]];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
        {
            [btn setImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        }
        else
        {
            [btn setImage:[UIImage imageNamed:@"menuGradientBig.png"] forState:UIControlStateNormal];
        }
        cell.backgroundView = btn; //[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menuBtnGradient"]] ;
        
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *mainmenuView = [arr objectAtIndex:i];
            [mainmenuView removeFromSuperview];
        }
    }
    
    
    SdImageView *asyncImageView ;
    UILabel *lblTitle ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        asyncImageView=[[SdImageView alloc] initWithFrame:CGRectMake(5, 5, 40, 40)];
        lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(55, 5, 240, 40)];
        lblTitle.font = [UIFont boldSystemFontOfSize:13];
    }
    else
    {
        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(5, 5, 70, 70)];
        lblTitle= [[UILabel alloc]initWithFrame:CGRectMake(85, 5, SCREEN_WIDTH-85, 70)];
        lblTitle.font = [UIFont boldSystemFontOfSize:18];
    }
    
    asyncImageView.backgroundColor = [UIColor clearColor];
    //asyncImageView.layer.cornerRadius = 5.0f;
    if(arMItemList.mItemImg)
        [asyncImageView loadImage:arMItemList.mItemImg];
    
    [cell.contentView addSubview:asyncImageView];
    //[asyncImageView release];
    
    
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    
    if (menulevel>1){
        
        lblTitle.textColor = [UIColor convertToHexString:arMItemList.smBtnFontColor];
    }
    else{
        
        lblTitle.textColor = [UIColor convertToHexString:arMItemList.mBtnFontColor];
    }
    lblTitle.numberOfLines = 2;
    
    lblTitle.text = arMItemList.mItemName;
    [cell.contentView addSubview:lblTitle];
    //[lblTitle release];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self callSubClassAccordingToSelection:(int)indexPath.row];
    
    [tblMenuListView deselectRowAtIndexPath:[tblMenuListView indexPathForSelectedRow]  animated:YES];
}

#pragma mark splash methods
-(void)showSplash{
    self.navigationController.navigationBarHidden = YES;
    imageViewBack = [[UIView alloc] initWithFrame:self.view.frame] ;
    UIImage *splashImage = [UIImage imageNamed:@"ThisLocationIcon.png"];
    
    splashImageView.frame = CGRectMake(55, 130, 200, 170);
    [splashImageView setImage:splashImage];
    
    imageViewBack.backgroundColor = [UIColor colorWithRGBInt:0x084178];
    [imageViewBack addSubview:splashImageView];
    
    UIViewController *modalViewController = [[UIViewController alloc] init];
    //modalViewController.view = self.view;
    [modalViewController.view addSubview:imageViewBack];
    [self presentViewController:modalViewController animated:NO completion:nil];
    ReleaseAndNilify(modalViewController);
    [self performSelector:@selector(removeSplashScreen) withObject:nil afterDelay:0.5];
}

- (void)removeSplashScreen {
    
    //[splashImageView removeFromSuperview];
    //[imageViewBack removeFromSuperview];
    ReleaseAndNilify(imageViewBack);
    self.navigationController.navigationBarHidden = NO;
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this value as it used to call "viewWillAppear" after this
    [[self presentedViewController] dismissViewControllerAnimated:NO completion:nil];
    
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
        
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:locationDetailsScreen animated:NO];
            //[locationDetailsScreen release];
            
        }
    }
}

#pragma mark navigate to viewcontroller

-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}

-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=TRUE;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
      
        
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}


-(void)navigateToEventList
{
    
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    self.iEventsListViewController = iEventsListViewController;
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}





-(void) navigateToScanNow
{
    [defaults setBool:YES forKey:@"KEY_SCANSPLASH"];
    [defaults setValue:@"scanNow" forKey:KEY_SEARCHFROM];
    [defaults setObject:@"YES" forKey:@"KEY_SCANNERCALLED"];
    
    int menulevel = [[defaults valueForKey:KEY_MENULEVEL] intValue];
    if(menulevel > 1)
        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
    else
        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
    
   
    [UIApplication sharedApplication].statusBarHidden = NO;

    //[[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];
    [self.navigationController pushViewController:viewScanNowScreen animated:NO];
    //[viewScanNowScreen release];
    
}


//Call CityExperiance Servie and Class
-(void) navigateToSubMenu
{
    
    
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}



//Call Find Service and Class
-(void)navigateToFindView
{
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    navigatedFromMainMenu = TRUE;
    // navigatedTOFindFromMainMenu = TRUE;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    if(menuItemName)
        mmvc.findTitle=menuItemName;
    
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}


-(void)request_utgetmainmenuid
{
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<MenuItem><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>", [defaults  valueForKey:KEY_BOTTOMBUTTONID]];
    
    if([defaults  valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId>", [defaults  valueForKey:KEY_MITEMID]];
    
    if([defaults  valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_HUBCITIID]];
    
    [requestStr appendFormat:@"<platform>%@</platform>",@"IOS"];
    
    [requestStr appendString:@"</MenuItem>"];
    
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendFormat:@"firstuse/utgetmainmenuid"];
    
    NSString *response=[ConnectionManager establishPostConnection:urlString withParam:requestStr];
    [self parse_utgetmainmenuid:response];
    
    //[requestStr release];
}

-(void)parse_utgetmainmenuid:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement !=nil)
        {
            [defaults setObject:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        }
    }
    
}

-(void)dealloc
{
    NSArray *arrViews = [self.view subviews];
    for(int i=0; i<[arrViews count]; i++)
    {
        UIView *deallocView = (UIView*)[arrViews objectAtIndex:i];
        [deallocView removeFromSuperview];
        
    }
   [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
    
}
//- (void)showResetPasswordAlert:(NSNotification *)notification {
//    [[CommonUtilityNews utilityManagers] tempPasswordDisplay:notification onViewController:self];
//
//}
//- (void)showEmailCountAlert:(NSNotification *)notification
//{
//    [[CommonUtilityNews utilityManagers] emailCountDisplay:notification onViewController:self];
//    
//}
-(void) navigateToNewsDetailScreen:(NSNumber *)newsID
{
         NewsDetailViewController *newsDetailVC = [[NewsDetailViewController alloc] initWithNibName:@"NewsDetailViewController" bundle:[NSBundle mainBundle]];
        newsDetailVC.newsID = newsID;
        [self.navigationController pushViewController:newsDetailVC animated:NO];
}



@end

//
//  AnythingPageDo.h
//  HubCiti
//
//  Created by Nikitha on 11/17/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnythingPageDo : NSObject
{
    NSString * logoImagePath;
    NSString * pageTitle;
    NSString * sDescription;
    NSString * longDescription;
    NSString * startDate;
    NSString * endDate;
}
@property(nonatomic,strong) NSString * logoImagePath;
@property(nonatomic,strong) NSString * pageTitle;
@property(nonatomic,strong) NSString * sDescription;
@property(nonatomic,strong) NSString * longDescription;
@property(nonatomic,strong) NSString * startDate;
@property(nonatomic,strong) NSString * endDate;
@end

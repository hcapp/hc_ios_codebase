//
//  SEMenuItem.h
//  SESpringBoardDemo
//
//  
//

#import <UIKit/UIKit.h>

@protocol MenuItemDelegate;
@interface SEMenuItem : UIView {
    NSString *image;
    NSString *titleText;
    NSString *fontText;
    UIViewController *vcToLoad;
    UIButton *removeButton;
    id __weak delegateView;
}
@property (nonatomic, weak)id delegateView;
@property (nonatomic,strong) NSString *titleText;
@property (nonatomic, assign) NSInteger tag;
@property BOOL isRemovable;
@property BOOL isInEditingMode;
@property (nonatomic, weak) id <MenuItemDelegate> delegate;

+ (id) initWithTitle:(NSString *)title frame:(CGRect)frame imageName:(NSString *)imageName fontColor:(NSString*)strColor removable:(BOOL)removable delegate:(id)del;

- (void) enableEditing;
- (void) disableEditing;
- (void) updateTag:(NSInteger) newTag;

@end

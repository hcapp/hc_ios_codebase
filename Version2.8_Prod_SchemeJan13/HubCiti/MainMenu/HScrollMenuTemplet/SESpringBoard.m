//
//  SESpringBoard.m
//  SESpringBoardDemo
//
//  
//

#import "SESpringBoard.h"
#import "CustomButton.h"
#import <QuartzCore/QuartzCore.h>


@implementation SESpringBoard

#define IPHONE_STANDARD_SCREEN_HEIGHT  480
#define NUMBER_OF_COLUMNS 4
#define NUMBER_OF_COLUMNS_3 3
@synthesize items, title, launcher, isInEditingMode, itemCounts;

- (IBAction) doneEditingButtonClicked {
    [self disableEditingMode];
}

- (id) initWithTitle:(CGRect)boardFrame items:(NSMutableArray *)menuItems image:(UIImage *) image backgroundcolor:(NSString*)strBackgroundColorHexValueForView
{
    self = [super initWithFrame:CGRectMake(boardFrame.origin.x, boardFrame.origin.y, boardFrame.size.width, boardFrame.size.height)];
    [self setUserInteractionEnabled:YES];
    if (self)
    {
        self.launcher = image;
        self.isInEditingMode = NO;
                        
        // create a container view to put the menu items inside
        if(IS_IPHONE5)
        {
            if([defaults boolForKey:@"isBottomButtonAvailable"]==YES)
            {
                if ([defaults boolForKey:@"RectangularGrid"] || [defaults boolForKey:@"4X4 Grid"]|| [defaults boolForKey:@"TwoImageTemplate"]||[defaults boolForKey:@"Square Image Template"]) {
                    itemsContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 5 , self.frame.size.width, self.frame.size.height-VARIABLE_HEIGHT(15))];
                   
                }
                else
                    itemsContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 30, self.frame.size.width-10, self.frame.size.height-40)];
            }
            else
            {
                if ([defaults boolForKey:@"RectangularGrid"] || [defaults boolForKey:@"4X4 Grid"]|| [defaults boolForKey:@"TwoImageTemplate"]||[defaults boolForKey:@"Square Image Template"]) {
                    itemsContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 5, self.frame.size.width , self.frame.size.height+VARIABLE_HEIGHT(30))];
                }
                else
                    itemsContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 50, self.frame.size.width-10, self.frame.size.height-40)];
            }
        }
        else
        {
            if([defaults boolForKey:@"isBottomButtonAvailable"])
            {
                if ([defaults boolForKey:@"RectangularGrid"] || [defaults boolForKey:@"4X4 Grid"]|| [defaults boolForKey:@"TwoImageTemplate"]||[defaults boolForKey:@"Square Image Template"]) {
                    itemsContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 5 , self.frame.size.width, self.frame.size.height-VARIABLE_HEIGHT(15))];
                }
                else
                    itemsContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 20, self.frame.size.width-10, self.frame.size.height-40)];
            }
            else
            {
                if ([defaults boolForKey:@"RectangularGrid"] || [defaults boolForKey:@"4X4 Grid"]|| [defaults boolForKey:@"TwoImageTemplate"]||[defaults boolForKey:@"Square Image Template"]) {
                    itemsContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 5, self.frame.size.width, self.frame.size.height+VARIABLE_HEIGHT(50))];
                }
                else
                    itemsContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 45, self.frame.size.width-10, self.frame.size.height-40)];
            }
        }
        
      
        itemsContainer.delegate = self;
        [itemsContainer setScrollEnabled:YES];
        [itemsContainer setPagingEnabled:YES];
        itemsContainer.showsHorizontalScrollIndicator = NO;
        [self addSubview:itemsContainer];
        
        self.items = menuItems;
        int itemInCol = 0; //change according to naming convension
        int counter = 0;
        int horGap = 0; //change
        int verGap = 0;
        int currentPage = 0;
        int horizontalGap = 0;
        int verticalGap = 0;
        int rowNumber = 3;
        
        int itemInRow = self.frame.size.width/([defaults integerForKey:@"singleItemWidth"]);  // calculate how many icons can come in each row
        if([defaults boolForKey:@"Iconic3X3"]||[defaults boolForKey:@"SquareGrid3x3"]){
            itemInRow = 3;
        }
        else
        if([defaults boolForKey:@"4X4 Grid"])
        {
            rowNumber = 4;
        }
        else if([defaults boolForKey:@"TwoImageTemplate"]){
            horizontalGap = 30;
        }
        else if([defaults boolForKey:@"Square Image Template"]){
            rowNumber=2;
            itemInRow =2;
            horGap=30;
              if([defaults boolForKey:@"isBottomButtonAvailable"]==YES){
            verGap=VARIABLE_HEIGHT(40);
              }
              else{
                  verGap=VARIABLE_HEIGHT(40)+bottomBarButtonHeight/4;

              }
        }
        
        // Setting Allignment for Rectangular Grid....
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            
            if ([defaults boolForKey:@"RectangularGrid"] || [defaults boolForKey:@"4X4 Grid"]) {
                
                horizontalGap = 0;
                verticalGap = 0;
                verGap = 0;
                horGap = horizontalGap;
                if ([defaults boolForKey:@"4X4 Grid"])
                {
                    rowNumber = 4;
                }
                else{
                    rowNumber = 3;
                }

            }
            else if([defaults boolForKey:@"TwoImageTemplate"]){
                horizontalGap = 110;
            }
            else if([defaults boolForKey:@"Square Image Template"]){
                horGap=70;
                verGap=80;
                

            }
            else{
                horizontalGap = 20;
                verticalGap = 70.0;
                verGap = 0;
                horGap = horizontalGap;
                rowNumber = 3;
            }
        }
        
      
        
        
        itemInCol = rowNumber;//self.frame.size.height/([defaults integerForKey:@"singleitemHeight"] + verticalGap); // calculate how many icons can come
        
        int itemsPerPage = itemInRow * itemInCol;   // calculate how many icons can come in each page
        
        int numberOfPages = (ceil((float)[menuItems count] / itemsPerPage)); // calculatehow manypages required to fit all icons
        
        if( ![defaults boolForKey:@"TwoImageTemplate"]){
        for (SEMenuItem *item in self.items) {
            currentPage = counter / itemsPerPage;
            item.tag = counter+1;
             UILabel* lbl ;
           
           
            
            
            if([defaults boolForKey:@"Square Image Template"]){
                 [item setFrame:CGRectMake(item.frame.origin.x + horGap +  (currentPage*itemsContainer.frame.size.width), item.frame.origin.y + verGap, item.frame.size.width, item.frame.size.width)];
                if([[defaults valueForKey:@"isDisplayLabel"] isEqualToString:@"1"])
                {
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad){
                    lbl = [[UILabel alloc] initWithFrame:CGRectMake(item.frame.origin.x ,verGap+ item.frame.size.width, item.frame.size.width, 80)];
                    lbl.font = [UIFont boldSystemFontOfSize:30];
                                    }
                else{
                    lbl = [[UILabel alloc] initWithFrame:CGRectMake(item.frame.origin.x ,verGap+ item.frame.size.width, item.frame.size.width, 40)];
                    lbl.font = [UIFont boldSystemFontOfSize:15];
                    
                }
                    
                    if([item.titleText length]<=7)
                        [lbl setText:[NSString stringWithFormat:@"  %@",item.titleText]];
                    else
                        [lbl setText:[NSString stringWithFormat:@"%@",item.titleText]];
                    
                    lbl.numberOfLines = 0;
                    
                    lbl.textColor= [UIColor convertToHexString:[defaults valueForKey:@"menuBtnFontColor"]];
                    lbl.lineBreakMode = NSLineBreakByWordWrapping;
                    lbl.backgroundColor = [UIColor clearColor];
                    lbl.textAlignment = NSTextAlignmentCenter;
                }
            }
            else{
                if([defaults boolForKey:@"Iconic3X3"]||[defaults boolForKey:@"SquareGrid3x3"]){
                    [item setFrame:CGRectMake(item.frame.origin.x + horGap +  (currentPage*itemsContainer.frame.size.width)+(IPAD? VARIABLE_WIDTH(11):VARIABLE_WIDTH(8)), item.frame.origin.y + verGap, item.frame.size.width, item.frame.size.height)];
                }
                else{
                    [item setFrame:CGRectMake(item.frame.origin.x + horGap +  (currentPage*itemsContainer.frame.size.width), item.frame.origin.y + verGap, item.frame.size.width, item.frame.size.height)];
                }
            }
            
            [itemsContainer addSubview:item];
            [itemsContainer addSubview:lbl];
            counter = counter + 1;
            
            
            if ([defaults boolForKey:@"RectangularGrid"] || [defaults boolForKey:@"4X4 Grid"]) {
                
                if ([defaults boolForKey:@"4X4 Grid"]) {
                     horGap = horGap + horizontalGap + item.frame.size.width + 2  ;
                }
               
                else
                {
                    horGap = horGap + horizontalGap + item.frame.size.width + 5  ;
                }
                
                
                
                if(counter % NUMBER_OF_COLUMNS == 0){
                    
                     if ([defaults boolForKey:@"4X4 Grid"]) {
                         verGap = verGap + verticalGap + item.frame.size.height + 2  ;
                     }
                     else{
                          verGap = verGap + verticalGap + item.frame.size.height + 5  ;
                     }
                    
                    horGap = 0;
                   
                }
                
                if (counter % itemsPerPage == 0) {
                    verGap = 0;
                    
                }
                
            }
            else if ([defaults boolForKey:@"Square Image Template"]){
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad)
                    horGap = horGap + horizontalGap + item.frame.size.width +70;
                else
                    horGap = horGap + horizontalGap + item.frame.size.width +30;
                
                if(counter % 2 == 0){
                    //vergap = vergap + verticalGap + item.frame.size.height + 2  ;
                    verGap = verGap + verticalGap + item.frame.size.width+VARIABLE_HEIGHT(40) ;
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad)
                        horGap=70;
                    else
                        horGap = 30;
                }
                
                if (counter % itemsPerPage == 0) {
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad)
                        verGap=80;
                    else
                        verGap = VARIABLE_HEIGHT(40);
                    
                }
                
                
            }
            else{
                int numberOfCol;
                if([defaults boolForKey:@"Iconic3X3"]||[defaults boolForKey:@"SquareGrid3x3"]){
                    horGap = horGap + horizontalGap + item.frame.size.width+VARIABLE_WIDTH(25);
                    numberOfCol = NUMBER_OF_COLUMNS_3;
                }
                else{
                    horGap = horGap + horizontalGap + item.frame.size.width;
                    numberOfCol = NUMBER_OF_COLUMNS;
                }
                
                if(counter % numberOfCol == 0){
                    verGap = verGap + verticalGap + item.frame.size.height-5;
                    horGap = 0;
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        horGap = horizontalGap;
                    }
                }
                if (counter % itemsPerPage == 0) {
                    verGap = 0;
                }
                
            }
            
        }
        }
        else{
            int i=0;
            for (SEMenuItem *item in self.items) {
                if([defaults boolForKey:@"isBottomButtonAvailable"]==YES){
                    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                        [item setFrame:CGRectMake(horizontalGap + i*item.frame.size.width +  horizontalGap*i, (SCREEN_HEIGHT - itemsContainer.frame.size.height+160+bottomBarButtonHeight_iPad)/2, item.frame.size.width, item.frame.size.height)];
                    else
                        [item setFrame:CGRectMake(horizontalGap + i*item.frame.size.width +  horizontalGap*i, (SCREEN_HEIGHT - itemsContainer.frame.size.height+50)/2, item.frame.size.width, item.frame.size.height)];
                }
                else{
                    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
                        [item setFrame:CGRectMake(horizontalGap + i*item.frame.size.width +  horizontalGap*i, (SCREEN_HEIGHT - itemsContainer.frame.size.height+320+bottomBarButtonHeight_iPad)/2, item.frame.size.width, item.frame.size.height)];
                    else
                        [item setFrame:CGRectMake(horizontalGap + i*item.frame.size.width +  horizontalGap*i, (SCREEN_HEIGHT - itemsContainer.frame.size.height+70+bottomBarButtonHeight)/2, item.frame.size.width, item.frame.size.height)];
                }
                NSLog(@"Screen Width = %f,%f",item.frame.size.width,item.frame.size.height);
                NSLog(@"Screen Width = %i",horizontalGap);
                NSLog(@"Screen Width = %f",horizontalGap + i*item.frame.size.width +  horizontalGap*i);
                [itemsContainer addSubview:item];
                NSLog(@"Screen Width = %@",[defaults valueForKey:@"menuBtnFontColor"]);

                if([[defaults valueForKey:@"isDisplayLabel"] isEqualToString:@"1"])
                {
                    
                    UILabel *lblTitle = nil;
                    
                    if([defaults boolForKey:@"isBottomButtonAvailable"]==YES){
                        if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
                            lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(item.frame.origin.x-5, item.frame.size.height+((SCREEN_HEIGHT - itemsContainer.frame.size.height+160+bottomBarButtonHeight_iPad)/2), item.frame.size.width+20, 90)];
                            lblTitle.font =[UIFont boldSystemFontOfSize:30];
                        }
                        else{
                            lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(item.frame.origin.x, item.frame.size.height+((SCREEN_HEIGHT - itemsContainer.frame.size.height+35)/2), item.frame.size.width+5, 60)];
                            lblTitle.font =[UIFont boldSystemFontOfSize:15];
                        }
                    }
                    else{
                        if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
                            lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(item.frame.origin.x-5, item.frame.size.height+((SCREEN_HEIGHT - itemsContainer.frame.size.height+320+bottomBarButtonHeight_iPad)/2), item.frame.size.width+20, 90)];
                            lblTitle.font =[UIFont boldSystemFontOfSize:30];
                        }
                        else{
                            lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(item.frame.origin.x, item.frame.size.height+((SCREEN_HEIGHT - itemsContainer.frame.size.height+55+bottomBarButtonHeight)/2), item.frame.size.width+5, 60)];
                            lblTitle.font =[UIFont boldSystemFontOfSize:15];
                        }
                    }
                    
                    if([item.titleText length]<=7)
                        [lblTitle setText:[NSString stringWithFormat:@"  %@",item.titleText]];
                    else
                        [lblTitle setText:[NSString stringWithFormat:@"%@",item.titleText]];
                    
                    
                    lblTitle.numberOfLines=2;
                    lblTitle.textColor= [UIColor convertToHexString:[defaults valueForKey:@"menuBtnFontColor"]];
                    
                    lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
                    lblTitle.backgroundColor = [UIColor clearColor];
                    lblTitle.textAlignment = NSTextAlignmentCenter;
                    [self addSubview:lblTitle];
                }
                i++;
                item.tag = i;
            }
        }
        // record the item counts for each page
        self.itemCounts = [NSMutableArray array];
        int totalNumberOfItems = (int)[self.items count];
        int numberOfFullPages = totalNumberOfItems % itemsPerPage;
        int lastPageItemCount = totalNumberOfItems - numberOfFullPages%itemsPerPage;
        for (int i=0; i<numberOfFullPages; i++)
            [self.itemCounts addObject:[NSNumber numberWithInteger:itemsPerPage]];
        if (lastPageItemCount != 0)
            [self.itemCounts addObject:[NSNumber numberWithInteger:lastPageItemCount]];
        
        [itemsContainer setContentSize:CGSizeMake(numberOfPages*itemsContainer.frame.size.width, itemsContainer.frame.size.height)];
       // [itemsContainer release];
        
        float pageControlY;
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            
            if([defaults boolForKey:@"isBottomButtonAvailable"]) {
                pageControlY = self.frame.size.height-40+VARIABLE_HEIGHT(10);
            }
            else {
                pageControlY = self.frame.size.height-20 + 40;
            }
        }
        else {
            
            // add a page control representing the page the scrollview controls
            if(IS_IPHONE5)
            {
                if([defaults boolForKey:@"isBottomButtonAvailable"]) {
                     pageControlY = self.frame.size.height-40+VARIABLE_HEIGHT(19);
                }
                else {
                    pageControlY = self.frame.size.height-20 + 50;
                }
            }
            else
            {
                if([defaults boolForKey:@"isBottomButtonAvailable"]) {
                     pageControlY = self.frame.size.height-20+VARIABLE_HEIGHT(19);
                }
                else {
                    pageControlY = self.frame.size.height-10;
                }
            }
        }
        if ([defaults boolForKey:@"RectangularGrid"] || [defaults boolForKey:@"4X4 Grid"]) {
            
            if(![defaults boolForKey:@"isBottomButtonAvailable"])
            {
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad)
                {
                     pageControlY = self.frame.size.height + VARIABLE_HEIGHT(7);
                }
                else
                    pageControlY = self.frame.size.height + VARIABLE_HEIGHT(22);
            }
            else{
                if (DEVICE_TYPE == UIUserInterfaceIdiomPhone)
                {
                    pageControlY = self.frame.size.height - VARIABLE_HEIGHT(15);
                }
                else{
                    pageControlY = self.frame.size.height - VARIABLE_HEIGHT(11);
                }
            }
        }
        
        
        pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, pageControlY, self.frame.size.width, 20)];

        if (numberOfPages > 1) {
            pageControl.numberOfPages = numberOfPages;
           NSLog(@"%d",[SharedManager scrollIndicator]);
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel>1){
                pageControl.currentPage=[SharedManager submenuScrollIndicator];
                [self pageTrack:[SharedManager submenuScrollIndicator]];
            }
            else{
                pageControl.currentPage=[SharedManager scrollIndicator];
                [self pageTrack:[SharedManager scrollIndicator]];
            
            }
           
            pageControl.pageIndicatorTintColor = [UIColor convertToHexString:[defaults valueForKey:@"pgnColorInActive"]];
            pageControl.currentPageIndicatorTintColor = [UIColor convertToHexString:[defaults valueForKey:@"pgnColorActive"]];
//            if ([defaults boolForKey:@"RectangularGrid"] || [defaults boolForKey:@"4X4 Grid"]) {
//                if( DEVICE_TYPE == UIUserInterfaceIdiomPhone)
//                {
//                    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, pageControlY+5, SCREEN_WIDTH, 13)];
//                }
//                else
//                {
//                    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, pageControlY, SCREEN_WIDTH, VARIABLE_HEIGHT(11))];
//                }
//            }
//            else{
//                bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, pageControlY, SCREEN_WIDTH, IPAD?VARIABLE_HEIGHT(10):VARIABLE_HEIGHT(15))];
//            }
//            bottomView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
//            // bottomView.layer.cornerRadius = 4.0f;
//            [self addSubview:bottomView];
            [self addSubview:pageControl];
        }
        [pageControl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];
       // [self pageTurn:pageControl];
        // add listener to detect close view events
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeViewEventHandler:) name:@"closeView" object:nil];
               // allow background color to be set
        
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}
- (void) pageTrack: (int) whichPage
{
   
    pageControl.currentPage = whichPage;
    
    
    CGFloat pageWidth = itemsContainer.frame.size.width;
    [itemsContainer setContentOffset:CGPointMake(pageWidth*whichPage, itemsContainer.contentOffset.y)];
}


- (void) pageTurn: (UIPageControl *) aPageControl
{
    int whichPage = (int)aPageControl.currentPage;
    // int whichPage=1;
    pageControl.currentPage = whichPage;
   
    
    CGFloat pageWidth = itemsContainer.frame.size.width;
    [itemsContainer setContentOffset:CGPointMake(pageWidth*whichPage, itemsContainer.contentOffset.y)];
}

+ (id) initWithTitle:(CGRect)boardFrame items:(NSMutableArray *)menuItems launcherImage:(UIImage *)image backgroundcolor:(NSString*)bgColor
{
    SESpringBoard *tmpInstance = [[SESpringBoard alloc] initWithTitle:boardFrame items:menuItems image:image backgroundcolor:bgColor];
	return tmpInstance;
};


// transition animation function required for the springboard look & feel
- (CGAffineTransform)offscreenQuadrantTransformForView:(UIView *)theView {
    CGPoint parentMidpoint = CGPointMake(CGRectGetMidX(theView.superview.bounds), CGRectGetMidY(theView.superview.bounds));
    CGFloat xSign = (theView.center.x < parentMidpoint.x) ? -1.f : 1.f;
    CGFloat ySign = (theView.center.y < parentMidpoint.y) ? -1.f : 1.f;
    return CGAffineTransformMakeTranslation(xSign * parentMidpoint.x, ySign * parentMidpoint.y);
}



#pragma mark - UIScrollView Delegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    CGFloat pageWidth = itemsContainer.frame.size.width;
    int page = floor((itemsContainer.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
   
    pageControl.currentPage = page;
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel>1){
    [SharedManager setSubmenuScrollIndicator:page];
        }
     else
    [SharedManager setScrollIndicator:page];
}

#pragma mark - Custom Methods

- (void) disableEditingMode {
    // loop thu all the items of the board and disable each's editing mode
    for (SEMenuItem *item in items)
        [item disableEditing];
    
    [doneEditingButton setHidden:YES];
    self.isInEditingMode = NO;
}

- (void) enableEditingMode {
    
    for (SEMenuItem *item in items)
        [item enableEditing];
    
    // show the done editing button
    [doneEditingButton setHidden:NO];
    self.isInEditingMode = YES;
}

@end

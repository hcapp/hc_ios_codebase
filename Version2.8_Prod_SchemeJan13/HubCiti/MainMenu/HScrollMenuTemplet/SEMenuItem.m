//
//  SEMenuItem.m
//  SESpringBoardDemo
//
//  
//

#import "SEMenuItem.h"
#import "SESpringBoard.h"
#import <QuartzCore/QuartzCore.h>
#include <stdlib.h>
#include "AsyncImageView.h"
#import "SdImageView.h"

@implementation SEMenuItem

@synthesize tag, delegate, isRemovable, isInEditingMode,delegateView,titleText;


#pragma mark - Custom Methods

- (void) enableEditing
{
    if (self.isInEditingMode == YES)
        return;
    
    // put item in editing mode
    self.isInEditingMode = YES;
    
    // make the remove button visible
    [removeButton setHidden:NO];
    
    // start the wiggling animation
    CATransform3D transform;
    
    if (arc4random() % 2 == 1)
        transform = CATransform3DMakeRotation(-0.08, 0, 0, 1.0);
    else
        transform = CATransform3DMakeRotation(0.08, 0, 0, 1.0);
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.toValue = [NSValue valueWithCATransform3D:transform];
    animation.autoreverses = YES;
    animation.duration = 0.1;
    animation.repeatCount = 10000;
    animation.delegate = self;
    [[self layer] addAnimation:animation forKey:@"wiggleAnimation"];
    
    // inform the springboard that the menu items are now editable so that the springboard
    // will place a done button on the navigationbar
    [(SESpringBoard *)self.delegate enableEditingMode];
    
}

- (void) disableEditing {
    [[self layer] removeAllAnimations];
    [removeButton setHidden:YES];
    self.isInEditingMode = NO;
}

- (void) updateTag:(NSInteger) newTag {
    self.tag = newTag;
    removeButton.tag = newTag;
}

#pragma mark - Initialization

- (id) initWithTitle:(NSString *)title frame:(CGRect)frame imageName:(NSString *)imageName fontColor:(NSString*)strColor canBeRemoved:(BOOL)removable delegate:(id)del
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        image = imageName;
        if(title)
        titleText = [[NSString alloc]initWithString:title];;
        if(strColor){
            if([defaults boolForKey:@"TwoImageTemplate"]||[defaults boolForKey:@"Square Image Template"])
                fontText=[[NSString alloc]initWithString:[defaults valueForKey:@"menuBtnFontColor"]];
            else
        fontText = [[NSString alloc]initWithString:strColor];
        }
        [self setAccessibilityLabel:title];
        [self setAccessibilityValue:@"Menu"];
        //NSlog(@"The accessibility label is %@ and value is %@",[self accessibilityLabel],[self accessibilityValue]);
        self.isInEditingMode = NO;
        self.delegateView = del;
        self.isRemovable = removable;
    }
    return self;
}

+ (id) initWithTitle:(NSString *)title frame:(CGRect)frame imageName:(NSString *)imageName fontColor:(NSString*)strColor removable:(BOOL)removable delegate:(id)del  {
	SEMenuItem *tmpInstance = [[SEMenuItem alloc] initWithTitle:title frame:frame imageName:imageName fontColor:strColor canBeRemoved:removable delegate:del];
	return tmpInstance;
}

-(NSString *) description
{
    return [NSString stringWithFormat:@"<%@: %p FRAME:(%f, %f, %f, %f) TAG: %ld IMAGE: %@ TITLE_TEXT: %@ VIEW_CONTROLLER_TO_LOAD: %@ DELEGATE: %@ REMOVE_BUTTON:%@ IS_REMOVABLE?: %@ IS_IN_EDITING_MODE?: %@>", @"SEMenuItem", self, self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height, self.tag, image, titleText, vcToLoad, delegate, removeButton, (isRemovable ? @"YES" : @"NO"), (isInEditingMode ? @"YES" : @"NO")];
}

# pragma mark - Overriding UiView Methods

- (void) removeFromSuperview {
    
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 0.0;
        [self setFrame:CGRectMake(self.frame.origin.x+50, self.frame.origin.y+50, 0, 0)];
        [removeButton setFrame:CGRectMake(0, 0, 0, 0)];
        
        UIView *spinny = [self viewWithTag:8989];
        [spinny removeFromSuperview];

    }];
    
//completion:^(BOOL finished) {
 //   [super removeFromSuperview];
//}
}

# pragma mark - Drawing

- (void) drawRect:(CGRect)rect
{
    float asyncImageWidth, asyncimageHeight, lblHeight, buttonWidth, buttonHeight;
    float lblFont, cornerRadius, borderWidth;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        asyncImageWidth = 160.0;
        asyncimageHeight = 160.0;
        if([defaults boolForKey:@"Iconic3X3"]||[defaults boolForKey:@"SquareGrid3x3"]){
            asyncImageWidth = 170.0;
            asyncimageHeight = 170.0;
        }
        lblHeight = 30.0;
        buttonWidth = asyncImageWidth + 10;
        buttonHeight = asyncimageHeight + lblHeight + 10;
        lblFont = 15;
        cornerRadius = asyncimageHeight/2.0f;
        borderWidth = 2.0;
       
    }
    
    else {
        asyncImageWidth = 63.0;
        asyncimageHeight = 63.0;
        if([defaults boolForKey:@"Iconic3X3"]||[defaults boolForKey:@"SquareGrid3x3"]){
            asyncImageWidth = 75.0;
            asyncimageHeight = 75.0;
        }
        lblHeight = 27.0;
        buttonWidth = 80.0;
        buttonHeight = 90.0;
        lblFont = 10;
        cornerRadius = asyncimageHeight/2.0f;
        borderWidth = 1.0;
       
    }
    
    // Setting frame for containg Image for rectangular grid....
    if ([defaults boolForKey:@"RectangularGrid"] || [defaults boolForKey:@"4X4 Grid"]|| [defaults boolForKey:@"TwoImageTemplate"]||[defaults boolForKey:@"Square Image Template"]) {
        
        
        asyncImageWidth = [defaults integerForKey:@"singleItemWidth"] ;
        asyncimageHeight = [defaults integerForKey:@"singleitemHeight"];
      
        borderWidth = 0.0;
        lblHeight = 0.0;
        lblFont = 0;
        cornerRadius = 0;
        
    }

    // draw the icon image
    SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0.0, 0.0,
                                                                                      asyncImageWidth, asyncimageHeight)];
    asyncImageView.backgroundColor = [UIColor clearColor];
    
    if ([defaults boolForKey:@"4X4 Grid"])
    {
        asyncImageView.frame = CGRectMake(1.0, 0.0,asyncImageWidth, asyncimageHeight);
    }
   
   /* if ([defaults boolForKey:@"4X4 Grid"])
    {
        NSString* imageName = @"" ;
        asyncImageView.frame = CGRectMake(1.0, 0.0,
                                         asyncImageWidth, asyncimageHeight);
        if((counttest >= 0 && counttest <=3) || (counttest >= 8 && counttest <= 11)) {
            
            if (counttest%2 == 0) {
                
                imageName = @"ipad_new.png";
            }
            else {
                imageName = @"ipad_new2.png";
                
            }
        }
        else {
            if (counttest%2 == 0) {
                
                imageName = @"ipad_new2.png";
            }
            else {
                imageName = @"ipad_new.png";
                
            }
        }
        
       
        
        counttest = counttest + 1;
        imageName = @"ipad_new.png";
        
        UIImageView *imageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]] ;
        imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.frame = asyncImageView.frame;
        //imageView.layer.borderColor = [UIColor blackColor].CGColor;
       // imageView.layer.borderWidth = 0.5;
        imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:imageView];
    }*/
    
        [asyncImageView loadImage:[image stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
        asyncImageView.tag = 8989;
        [self addSubview:asyncImageView];


    // To make the UIView rounded/Circular
    
    if (![HUBCITIKEY isEqualToString:@"Austin49"] && [defaults boolForKey:@"SquareGrid"] == NO && [defaults boolForKey:@"SquareGrid3x3"] == NO && ![defaults boolForKey:@"RectangularGrid"] && ![defaults boolForKey:@"4X4 Grid"]&& ![defaults boolForKey:@"TwoImageTemplate"] && ![defaults boolForKey:@"Square Image Template"]) {
        asyncImageView.layer.cornerRadius = cornerRadius;
        asyncImageView.layer.masksToBounds = YES;
        asyncImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        asyncImageView.layer.borderWidth = borderWidth;
    }
     if(![defaults boolForKey:@"TwoImageTemplate"]||![defaults boolForKey:@"Square Image Template"]){
    UILabel *lblTitle = nil;
//    UILabel *lblTitle = [[[UILabel alloc]initWithFrame:CGRectMake(asyncImageView.bounds.origin.x, asyncimageHeight, asyncImageView.bounds.size.width, lblHeight)];
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(asyncImageView.bounds.origin.x, asyncimageHeight, asyncImageView.bounds.size.width+5, lblHeight)];
    }
    else{
        lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(asyncImageView.bounds.origin.x, asyncimageHeight, asyncImageView.bounds.size.width+4, lblHeight)];
    }
    if([titleText length]<=7)
        [lblTitle setText:[NSString stringWithFormat:@"  %@",titleText]];
    else
        [lblTitle setText:[NSString stringWithFormat:@"%@",titleText]];
    
    lblTitle.font =[UIFont boldSystemFontOfSize:lblFont];
    lblTitle.numberOfLines=2;
    
    //to achieve vertical alignment of label in case of 1 line
//    CGRect beforeFrame = lblTitle.frame;
//    [lblTitle sizeToFit];
//    lblTitle.frame = CGRectMake(beforeFrame.origin.x, 60, beforeFrame.size.width, lblTitle.frame.size.height);
    
    lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
    lblTitle.textColor = [UIColor convertToHexString:fontText];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.textAlignment = NSTextAlignmentCenter;

    [self addSubview:lblTitle];
     }
    //[asyncImageView release];
    if([defaults boolForKey:@"TwoImageTemplate"]||[defaults boolForKey:@"Square Image Template"]){
        asyncImageView.layer.cornerRadius = 15;
        //  asyncImageView.layer.borderWidth = 2;
        asyncImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        asyncImageView.clipsToBounds = YES;
        buttonWidth = asyncImageWidth;
        buttonHeight = asyncimageHeight;
        
        
    }
    // place a clickable button on top of everything
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, buttonWidth, buttonHeight)];
    button.backgroundColor = [UIColor clearColor];
    [button addTarget:self.delegateView action:@selector(clickonMenuItem:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = tag;
    [self addSubview:button];
    
}

@end
//
//  SESpringBoard.h
//  SESpringBoardDemo
//
//  
//

#import <UIKit/UIKit.h>
#import "SEMenuItem.h"

@interface SESpringBoard : UIView <UIScrollViewDelegate> {
    UINavigationController *nav;
    UIScrollView *itemsContainer;
    UIPageControl *pageControl;
    UIButton *doneEditingButton;
}

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIImage *launcher;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *itemCounts;    // holds how many items there are in each page
@property BOOL isInEditingMode;

+ (id) initWithTitle:(CGRect)boardFrame items:(NSMutableArray *)menuItems launcherImage:(UIImage *)image backgroundcolor:(NSString*)bgColor;

- (void) disableEditingMode;
- (void) enableEditingMode;

@end

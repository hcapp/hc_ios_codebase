//
//  AnythingPage.h
//  HubCiti
//
//  Created by Ajit Nadig on 11/13/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import "DWBubbleMenuButton.h"
#import "AnythingPageDo.h"
@class AnyViewController;
@class EmailShareViewController;
BOOL shareFlag;
BOOL anythingShareFlag;


@interface AnythingPage : UIViewController<MFMailComposeViewControllerDelegate,UIWebViewDelegate,MFMessageComposeViewControllerDelegate, DWBubbleMenuViewDelegate,CustomizedNavControllerDelegate>{
    WebRequestState iWebRequestState;
    AnythingPageDo * anythingPageDO;
}
@property(nonatomic,strong) AnyViewController * anyVC;
@property (strong, nonatomic) IBOutlet UILabel *titlelabel;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property (strong, nonatomic) IBOutlet UIWebView *longDescoutlet;
@property (strong, nonatomic) IBOutlet UILabel *endDateOutlet;
@property (strong, nonatomic) IBOutlet UIWebView *shortDescOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewOutlet;
@property (strong, nonatomic) IBOutlet UILabel *startDateOutlet;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintForImage;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintForImage;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *shortWebViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *longWebViewHeight;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewOutlet;
@property (strong, nonatomic) IBOutlet UIView *detailContentViewOUTLET;
@property (strong, nonatomic) IBOutlet UIStackView *stackViewOutlet;
@end

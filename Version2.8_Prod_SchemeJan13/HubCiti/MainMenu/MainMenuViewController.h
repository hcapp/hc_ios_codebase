//
//  MainMenuViewController.h
//  HubCiti
//
//  Created by Anjana on 10/3/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mainMenuDO.h"
#import "LocationManager.h"
#import <MessageUI/MessageUI.h>
#import "FindLocationServiceViewController.h"
#import <Social/Social.h>
#import "SubMenuGroupingAndSortingViewController.h"
@class DealHotDealsList;
@class FilterRetailersList;
@class RetailersListViewController;
@class CityExperienceViewController;
@class EventsListViewController;
@class FundraiserListViewController;
@class FilterListViewController;
@class FAQCategoryList;
@class ButtonSetupViewController;
@class AnyViewController;
@class EmailShareViewController;
@class BandViewController;
@class CombinationViewController;
@class ScrollTemplateViewController;
@class BlockViewController;
@class BandsNearByEventsMapTable;
//Used to track navigation from main menu
BOOL navigatedFromMainMenu,subMenuSortDone,fromBack;
BOOL requestAnythingFromMainMenu;
BOOL navigatedTOFindFromMainMenu;
NSMutableArray *FindBottomButtonID;
NSMutableArray *EventsBottomButtonID;
NSMutableArray *FilterBottomButtonID;
NSMutableArray *FindSingleBottomButtonID;
NSMutableArray *FundraiserBottomButtonID;
NSMutableArray *CityExperienceBottomButtonID;

NSMutableArray *NearbyBottomButtonID;
NSMutableArray *SingleEventCatID,*MultipleEventCatID;
BOOL SingleCatFlag,MultipleCatFlag,SingleEventFromMainMenu;
NSMutableArray *evtID,*locationTapped;
NSMutableArray *linkID;
NSMutableDictionary *selectedSortOption,*selectedFilterOption;
NSMutableDictionary *selectedSortCityDic;
NSMutableDictionary *cashedResponse,*dateCreated,*typeId,*deptId;
NSMutableArray* arrMenuItems;

@class WebBrowserViewController;

typedef enum templetsMenu
{
    GroupedTab=1,
    TwoColumnTab,
    ListView,
    IconicGrid,
    TwoColumnTabWithBannerAd,
    ComboTemplate,
    GroupedTabWithImage,
    ListViewWithBannerAd,
    RectangularGrid,
    FourTile,
    TwoTile,
    TwoTileWithBanner
}templetsMenuState;

@interface MainMenuViewController : UIViewController<HubCitiConnectionManagerDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,LocationManagerDelegate,MFMessageComposeViewControllerDelegate,SubMenuGroupingAndSortingViewControllerDelegate,CustomizedNavControllerDelegate>
{
    templetsMenuState iTempletsMenuId;
    
    IBOutlet UIButton *btn_About;
    IBOutlet UIButton *btn_UserAct;
    IBOutlet UIButton *btn_Settings;
    IBOutlet UIButton *btn_More;
    
    IBOutlet UILabel *label_One;
    IBOutlet UILabel *label_Two;
    IBOutlet UILabel *label_Three;
    IBOutlet UILabel *label_Four;
    
    WebRequestState iWebRequestState;  //create enum
    
    NSMutableArray *arrMenuDO;
    NSMutableArray *arrMenuBottomButtonDO;
    NSMutableArray *arrBottomButtonViewContainer;
    
    UITableView *tblMenuListView;
    CommonUtility *common;
    
    NSString *mBkgrdColor;
    NSString *mBkgrdImage;
    NSString *smBkgrdColor;
    NSString *smBkgrdImage;
    BOOL isModalDismiss;
    //splash for WNB
    UIImageView *splashImageView;
    UIView *imageViewBack;
    
    FindLocationServiceViewController *locVC;
    
    NSString *androidDownloadLink;
    NSString *retAffName;
    UIAlertAction* zipSave;
    
}
@property(nonatomic,strong) WebBrowserViewController *urlDetail;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,strong) ButtonSetupViewController* iViewController;
@property(nonatomic,strong) FAQCategoryList *faqList;

@property(nonatomic,strong) FilterListViewController *filterList;
@property(nonatomic,strong) FundraiserListViewController *iFundraiserListViewController;
@property(nonatomic,strong) EventsListViewController* iEventsListViewController;
@property(nonatomic,strong) CityExperienceViewController *cityExp;
@property(nonatomic,strong) FilterRetailersList *filters;
@property(nonatomic,strong) RetailersListViewController* retailerListScreen;
@property(nonatomic,strong) DealHotDealsList* hotDeals;
@property(nonatomic,strong) BandViewController *bandViewPage;
@property(nonatomic,strong) CombinationViewController* combView;
@property(nonatomic,strong) ScrollTemplateViewController* scrollView;
@property(nonatomic,strong) BlockViewController *blockView;
@property(nonatomic,strong) BandsNearByEventsMapTable *todayEventList;

-(IBAction)bottomBarButtonClick:(id)sender;

-(void) saveMenuDisplay;

@end


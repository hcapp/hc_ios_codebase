//
//  ViewController.m
//  ButtonDemo
//
//  Created by ionnor on 9/3/13.
//  Copyright (c) 2013 ionnor. All rights reserved.
//

#import "ButtonSetupViewController.h"
#import "CustomButton.h"
#import <QuartzCore/QuartzCore.h>
#import "mainMenuDO.h"
#import "MainMenuResponse.h"
#import "MainMenuViewController.h"


@interface ButtonSetupViewController ()
@property(nonatomic,strong) NSDictionary* menuResponse;
@end

@implementation ButtonSetupViewController

@synthesize delegateView;
@synthesize btnBkgrdColor;
@synthesize btnLblColor;

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
-(void)addScrollViewonView
{
    [mainmenuScrollView setBackgroundColor:[UIColor clearColor]];
    mainmenuScrollView = [[UIScrollView alloc]init];
}

// Set the Property values for Buttons in Main menu
-(void)setbasicpropertyforMainMenu:(NSString*)buttonbg Viewbackground:(NSString*)viewbg mainMenuItemObjects:(NSDictionary*)arrGroups templetid:(int)tempid setDelegate:(id)delegate
{
    self.menuResponse = arrGroups;
    strBackgroundColorHexValueForButton = [[NSString alloc]initWithString:buttonbg];
    strBackgroundColorHexValueForView = [[NSString alloc]initWithString:viewbg];
    //arrMainMenuItemsObjects = [[NSArray alloc]initWithArray:arrGroups];
    self.delegateView = delegate;
    if(tempid==1)
        [self setButtonViewTemp1:YES];
    else if(tempid==2)
        [self setButtonViewTemp2:NO];
    else if(tempid==6)
        [self setButtonViewforCustomTemplet];
    else if(tempid==7)
        [self setButtonViewTemp4:YES];
    else if(tempid == 10)
        [self setButtonViewTemp4Tile];
    else if(tempid == 11)
        [self setButtonViewTwoTile:NO];
    else if(tempid == 12)
        [self setButtonViewTwoTile:YES];
}


-(void)setBottombuttonOfPage:(NSString*)buttonBGColor titles:(NSArray*)arrTitles btnImages:(NSArray*)arrImages setDelegate:(id)delegate
{
    strBackgroundColorHexValueForButton = [[NSString alloc]initWithString:buttonBGColor];
    self.delegateView = delegate;
    int xCoord =0;
    int yCoord = 0;
    for(int i=0; i<[arrTitles count]; i++)
    {
        //UIButton *FirstButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        UIButton *FirstButton = [UIButton buttonWithType:UIButtonTypeCustom];
        FirstButton.frame = CGRectMake(xCoord, yCoord,80,40 );
        [FirstButton addTarget:self.delegateView action:@selector(clickonMenuItem:) forControlEvents:UIControlEventTouchUpInside];
        [FirstButton setTitle:[arrTitles objectAtIndex:i] forState:UIControlStateNormal];
        [FirstButton setBackgroundImage:[UIImage imageNamed:[arrImages objectAtIndex:i]] forState:UIControlStateNormal];
        [mainmenuScrollView addSubview:FirstButton];
        xCoord = xCoord + 80;
    }
    [self.view setBackgroundColor:[UIColor clearColor]];
}


// get the RoundOff value for the number of Rows in Main Menu
-(NSInteger)divideAndRoundUp:(NSInteger)a with:(NSInteger)b
{
    if( a % b != 0 )
    {
        return a / b + 1;
    }
    return a / b;
}


// Convert uicolor to uiimage
- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

//grouped template
-(void)setButtonViewTemp1:(BOOL)isBtnTransprent
{
    mainmenuScrollView.frame=self.view.bounds;
    [mainmenuScrollView setBackgroundColor:[UIColor clearColor]];
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    int totalScreenWidth = self.view.bounds.size.width;
    int yCoord=0;
    int buttonHeight=46;
    int LabelHeight=26;
    int DiffBetweenButtonsInWidth = 0;
    int DiffBetweenButtonsInHight = 0;
    
    //Added by Keshava for iPad Conversion.
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        buttonHeight=80;
        LabelHeight=40;
    }
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    //MainMenuResponse* menu = [[MainMenuResponse alloc] init];
    //[menu setValuesForKeysWithDictionary:self.menuResponse];
    
    
    for(int row=0; row < [arrMenuItems count]; row++)
    {
        int xCoord=0;
    
        if([arrMenuItems count]>row)
        {
            MenuItemList* imainMenuDO = [arrMenuItems objectAtIndex:row];
           // NSDictionary* dictList = menu.arMItemList[row];
           // [imainMenuDO setValuesForKeysWithDictionary:dictList];
            
            NSString *strTitle = imainMenuDO.mItemName;
            NSString *strgroup = imainMenuDO.linkTypeName;
            
            if(![strgroup isEqualToString:@"Text"])
            {
                UIButton *FirstButton = [UIButton buttonWithType:UIButtonTypeCustom];
                FirstButton.frame = CGRectMake(xCoord, yCoord,totalScreenWidth+DiffBetweenButtonsInWidth,buttonHeight );
                [FirstButton addTarget:self.delegateView action:@selector(clickonMenuItem:) forControlEvents:UIControlEventTouchUpInside];
                FirstButton.tag = row+1;
                [FirstButton setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
                FirstButton.layer.borderWidth = 1.0;
                FirstButton.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
                
                [FirstButton setTitle:strTitle forState:UIControlStateNormal];
                if(menulevel > 1)
                {
                    [FirstButton setTitleColor:[UIColor convertToHexString:imainMenuDO.smBtnFontColor] forState:UIControlStateNormal];
                    FirstButton.backgroundColor = [UIColor convertToHexString:imainMenuDO.smBtnColor];
                }
                else
                {
                    [FirstButton setTitleColor:[UIColor convertToHexString:imainMenuDO.mBtnFontColor] forState:UIControlStateNormal];
                    FirstButton.backgroundColor = [UIColor convertToHexString:imainMenuDO.mBtnColor];
                }
                
                [FirstButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
                
                
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                   FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
                }
                else
                {
                   FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
                    
                }
                
                FirstButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                
                FirstButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
                
                UIImageView *arrowImage ;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    arrowImage = [[UIImageView alloc]initWithFrame:CGRectMake(totalScreenWidth-20, 17, 9, 13)];
                }
                else
                {
                    arrowImage = [[UIImageView alloc]initWithFrame:CGRectMake(totalScreenWidth-20, 35, 9, 13)];
                    
                }
                [arrowImage setBackgroundColor:[UIColor clearColor]];
                [arrowImage setImage:[UIImage imageNamed:@"right_arrow"]];
                [FirstButton addSubview:arrowImage];
                ReleaseAndNilify(arrowImage);
                
                [mainmenuScrollView addSubview:FirstButton];
                
                
                yCoord += buttonHeight + DiffBetweenButtonsInHight;
            }
            else
            {
                NSLog(@"Count = %u ",(int)[arrMenuItems count]-1);
                if([arrMenuItems count]!=1 ||row !=[arrMenuItems count]-1) {
                    
                UILabel *lblGroup = [[UILabel alloc]initWithFrame:CGRectMake(xCoord, yCoord,totalScreenWidth,LabelHeight )];
                lblGroup.tag = row + 1;
                lblGroup.userInteractionEnabled=NO;
                [lblGroup setText:[NSString stringWithFormat:@"  %@",strTitle]];
                
                if(menulevel > 1)
                {
                    lblGroup.textColor=[UIColor convertToHexString:imainMenuDO.smGrpFntColor];
                    lblGroup.backgroundColor = [UIColor convertToHexString:imainMenuDO.smGrpBkgrdColor];
                }
                else
                {
                    lblGroup.textColor=[UIColor convertToHexString:imainMenuDO.mGrpFntColor];
                    lblGroup.backgroundColor = [UIColor convertToHexString:imainMenuDO.mGrpBkgrdColor];
                }
                
               
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                         lblGroup.font =[UIFont boldSystemFontOfSize:13];
                }
                else
                {
                      lblGroup.font =[UIFont boldSystemFontOfSize:18];
                        
                }
                
                lblGroup.textAlignment = NSTextAlignmentLeft;
                [mainmenuScrollView addSubview:lblGroup];
                yCoord += LabelHeight + DiffBetweenButtonsInHight;
            }
            }
        }
        
    }
    
    [mainmenuScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, yCoord+buttonHeight)];

}

-(void)setButtonViewTemp4:(BOOL)isBtnTransperent
{
    mainmenuScrollView.frame=self.view.bounds;
    [mainmenuScrollView setBackgroundColor:[UIColor clearColor]];
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    int totalScreenWidth = self.view.bounds.size.width;
    int yCoord=0;
    int buttonHeight=46;
    int LabelHeight=26;
    int DiffBetweenButtonsInWidth = 0;
    int DiffBetweenButtonsInHight = 0;
    int imageWidth = 40;
    int imageHeight = 40;
    
    //Added by Keshava for iPad Conversion.
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        buttonHeight=80;
        LabelHeight=40;
        imageHeight=65;
        imageWidth=65;
    }
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
   // MainMenuResponse* menu = [[MainMenuResponse alloc] init];
    //[menu setValuesForKeysWithDictionary:self.menuResponse];
    
    for(int row=0; row < [arrMenuItems count]; row++)
    {
      
        
        int xCoord=0;
        
        if([arrMenuItems count]>row)
        {
            MenuItemList* imainMenuDO = [arrMenuItems objectAtIndex:row];
           // NSDictionary* dictList = menu.arMItemList[row];
           // [imainMenuDO setValuesForKeysWithDictionary:dictList];
            
            NSString *strTitle = imainMenuDO.mItemName;
            NSString *strgroup = imainMenuDO.linkTypeName;
            
            if(![strgroup isEqualToString:@"Text"])
            {
                UIButton *FirstButton = [UIButton buttonWithType:UIButtonTypeCustom];
                FirstButton.frame = CGRectMake(xCoord, yCoord,totalScreenWidth+DiffBetweenButtonsInWidth,buttonHeight );
                [FirstButton addTarget:self.delegateView action:@selector(clickonMenuItem:) forControlEvents:UIControlEventTouchUpInside];
                FirstButton.tag = row+1;
                
                if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
                {
                     //[FirstButton setImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
                     FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
                }
                else
                {
                    //[FirstButton setImage:[UIImage imageNamed:@"menuGradientBig.png"] forState:UIControlStateNormal];
                     FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
                }
                FirstButton.layer.borderWidth = 1.0;
                FirstButton.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
                
                
               if(menulevel > 1)
                {
                    [FirstButton setTitleColor:[UIColor convertToHexString:imainMenuDO.smBtnFontColor] forState:UIControlStateNormal];
                    FirstButton.backgroundColor = [UIColor convertToHexString:imainMenuDO.smBtnColor];
                }
                else
                {
                    [FirstButton setTitleColor:[UIColor convertToHexString:imainMenuDO.mBtnFontColor] forState:UIControlStateNormal];
                    FirstButton.backgroundColor = [UIColor convertToHexString:imainMenuDO.mBtnColor];
                    
                }
               
                
                FirstButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                FirstButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
                [FirstButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
               
                
                UIImageView *arrowImage ;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    arrowImage = [[UIImageView alloc]initWithFrame:CGRectMake(totalScreenWidth-20, 17, 9, 13)];
                }
                else
                {
                   arrowImage = [[UIImageView alloc]initWithFrame:CGRectMake(totalScreenWidth-20, 35, 9, 13)];
                    
                }
                [arrowImage setBackgroundColor:[UIColor clearColor]];
                [arrowImage setImage:[UIImage imageNamed:@"right_arrow"]];
               //
                
                SdImageView *asyncImageView ;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(5, 3, imageWidth, imageHeight)];
                }
                else
                {
                   asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(7, 7, imageWidth, imageHeight)];
                    
                }

                asyncImageView.backgroundColor = [UIColor clearColor];
                [asyncImageView loadImage:imainMenuDO.mItemImg];
                [FirstButton addSubview:asyncImageView];
                asyncImageView.userInteractionEnabled = NO;
                asyncImageView.tag = 99;
                ////[asyncImageView release];
                
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    [FirstButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
                }
                else
                {
                    [FirstButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 80, 0, 0)];
                    
                }
                
                [FirstButton setTitle:strTitle forState:UIControlStateNormal];
                [FirstButton addSubview:arrowImage];
                [mainmenuScrollView addSubview:FirstButton];
                ReleaseAndNilify(arrowImage);
                
                yCoord += buttonHeight + DiffBetweenButtonsInHight;
            }
            else
            {
                if([arrMenuItems count]!=1 ||row !=[arrMenuItems count]-1) {
                UILabel *lblGroup = [[UILabel alloc]initWithFrame:CGRectMake(xCoord, yCoord,totalScreenWidth,LabelHeight )];
                lblGroup.tag = row + 1;
                lblGroup.userInteractionEnabled=NO;
                
                [lblGroup setText:[NSString stringWithFormat:@"  %@",strTitle]];
                
                if(menulevel > 1)
                {
                    
                    lblGroup.textColor=[UIColor convertToHexString:imainMenuDO.smGrpFntColor];
                    lblGroup.backgroundColor = [UIColor convertToHexString:imainMenuDO.smGrpBkgrdColor];
                }
                else
                {
                    lblGroup.textColor=[UIColor convertToHexString:imainMenuDO.mGrpFntColor];
                    lblGroup.backgroundColor = [UIColor convertToHexString:imainMenuDO.mGrpBkgrdColor];
                }
                    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                        lblGroup.font =[UIFont boldSystemFontOfSize:13];
                    }
                    else
                    {
                        lblGroup.font =[UIFont boldSystemFontOfSize:18];
                    }
                
                
                lblGroup.textAlignment = NSTextAlignmentLeft;
                [mainmenuScrollView addSubview:lblGroup];
                yCoord += LabelHeight + DiffBetweenButtonsInHight;
            }
            }
        }
        
    }
    
    [mainmenuScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, yCoord+buttonHeight)];
}

//2 column template
-(void)setButtonViewTemp2:(BOOL)isBtnTransprent
{
    mainmenuScrollView.frame=self.view.bounds;
    [self.view setBackgroundColor:[UIColor clearColor]];
    [mainmenuScrollView setBackgroundColor:[UIColor clearColor]];
    float totalScreenWidth = self.view.bounds.size.width;
    float yCoord=0;
    float buttonWidth=(self.view.bounds.size.width/2);
    float buttonHeight=46;
    float DiffBetweenButtonsInWidth = 0.25;
    float DiffBetweenButtonsInHight = 0;
    float xCoord=0;
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        buttonHeight=80;
        DiffBetweenButtonsInWidth = 0.50;
    }
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    //MainMenuResponse* menu = [[MainMenuResponse alloc] init];
    //[menu setValuesForKeysWithDictionary:self.menuResponse];
    
    for(int row=0; row < [arrMenuItems count]; row++)
    {
        MenuItemList* imainMenuDO = [arrMenuItems objectAtIndex:row];
        //NSDictionary* dictList = menu.arMItemList[row];
       // [imainMenuDO setValuesForKeysWithDictionary:dictList];
        
        NSString *strTitle = imainMenuDO.mItemName;
        NSString *strlinkTypeName = imainMenuDO.linkTypeName;
        
        if([[defaults valueForKey:@"noOfColumns"]isEqualToString:@"2"])
        {
            if([strlinkTypeName isEqualToString:@"City Experience"])
            {
                //UIButton *FirstButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                UIButton *FirstButton = [UIButton buttonWithType:UIButtonTypeCustom];
                FirstButton.frame = CGRectMake(0, yCoord,totalScreenWidth,buttonHeight );
                [FirstButton addTarget:self.delegateView action:@selector(clickonMenuItem:) forControlEvents:UIControlEventTouchUpInside];
                [FirstButton setAccessibilityLabel:imainMenuDO.mItemName];
                FirstButton.tag = row+1;
                [FirstButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 5.0, 5.0)];
                [FirstButton setTitle:strTitle forState:UIControlStateNormal];
                [FirstButton setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    
                     FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
                }
                else
                {
                    //[FirstButton setBackgroundImage:[UIImage imageNamed:@"menuGradientBig.png"] forState:UIControlStateNormal];
                    FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
                }
                
                if(menulevel > 1){
                    [FirstButton setTitleColor:[UIColor convertToHexString:imainMenuDO.smBtnFontColor] forState:UIControlStateNormal];
                    FirstButton.backgroundColor = [UIColor convertToHexString:imainMenuDO.smBtnColor];
                }
                else{
                    [FirstButton setTitleColor:[UIColor convertToHexString:imainMenuDO.mBtnFontColor] forState:UIControlStateNormal];
                    FirstButton.backgroundColor = [UIColor convertToHexString:imainMenuDO.mBtnColor];
                }
                
                [FirstButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
               
                
                [mainmenuScrollView addSubview:FirstButton];
                
                
                yCoord += buttonHeight + DiffBetweenButtonsInHight;
                xCoord = 0;
                
            }
            else
            {
                //UIButton *FirstButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                UIButton *FirstButton = [UIButton buttonWithType:UIButtonTypeCustom];
                FirstButton.frame     = CGRectMake(xCoord, yCoord,buttonWidth,buttonHeight );
                [FirstButton addTarget:self.delegateView action:@selector(clickonMenuItem:) forControlEvents:UIControlEventTouchUpInside];
                [FirstButton setAccessibilityLabel:imainMenuDO.mItemName];
                FirstButton.tag = row+1;
                [FirstButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 5.0, 5.0)];
                FirstButton.titleLabel.lineBreakMode=NSLineBreakByWordWrapping | NSLineBreakByTruncatingTail;
                [FirstButton setTitle:strTitle forState:UIControlStateNormal];
                [FirstButton setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                   
                    FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
                }
                else
                {
                   
                    FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
                }
                FirstButton.titleLabel.numberOfLines=2;
                if(menulevel > 1){
                    [FirstButton setTitleColor:[UIColor convertToHexString:imainMenuDO.smBtnFontColor] forState:UIControlStateNormal];
                    FirstButton.backgroundColor = [UIColor convertToHexString:imainMenuDO.smBtnColor];
                }
                
                else{
                    [FirstButton setTitleColor:[UIColor convertToHexString:imainMenuDO.mBtnFontColor] forState:UIControlStateNormal];
                    FirstButton.backgroundColor = [UIColor convertToHexString:imainMenuDO.mBtnColor];
                }
                
                [FirstButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
                //FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
                
                [mainmenuScrollView addSubview:FirstButton];
                
                if(xCoord > (SCREEN_WIDTH)/2)
                {
                    yCoord += buttonHeight + DiffBetweenButtonsInHight;
                    xCoord = 0;
                }
                else
                    // Set the x position for next button in the same row
                    xCoord = xCoord + buttonWidth + DiffBetweenButtonsInWidth;
            }
        }
        else
        {
            UIButton *FirstButton = [UIButton buttonWithType:UIButtonTypeCustom];
            FirstButton.frame = CGRectMake(xCoord, yCoord,totalScreenWidth+DiffBetweenButtonsInWidth,buttonHeight );
            [FirstButton addTarget:self.delegateView action:@selector(clickonMenuItem:) forControlEvents:UIControlEventTouchUpInside];
            [FirstButton setAccessibilityLabel:imainMenuDO.mItemName];
            FirstButton.tag = row+1;
            [FirstButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 5.0, 5.0)];
            [FirstButton setTitle:strTitle forState:UIControlStateNormal];
             FirstButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
            [FirstButton setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                
                FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
            }
            else
            {
               
                FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
            }
            
            
            if(menulevel > 1){
                [FirstButton setTitleColor:[UIColor convertToHexString:imainMenuDO.smBtnFontColor] forState:UIControlStateNormal];
                FirstButton.backgroundColor = [UIColor convertToHexString:imainMenuDO.smBtnColor];
            }
            
            else{
                [FirstButton setTitleColor:[UIColor convertToHexString:imainMenuDO.mBtnFontColor] forState:UIControlStateNormal];
                FirstButton.backgroundColor = [UIColor convertToHexString:imainMenuDO.mBtnColor];
            }
            
            [FirstButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
           
            
            [mainmenuScrollView addSubview:FirstButton];
            
            
            yCoord += buttonHeight + DiffBetweenButtonsInHight;
            
        }
        
        
    }
    
    [mainmenuScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, yCoord+buttonHeight)];
    
}


//4 tile template
-(void)setButtonViewTemp4Tile
{
    mainmenuScrollView.frame=self.view.bounds;
    [self.view setBackgroundColor:[UIColor clearColor]];
    [mainmenuScrollView setBackgroundColor:[UIColor clearColor]];
    [mainmenuScrollView setBackgroundColor:[UIColor whiteColor]];
    float X = 0;
    float Y = 0 ;
    //int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    float yVal = 64;
    float imageHeight;
    float imageWidth = (SCREEN_WIDTH);//90:iphone, ipad:360 if same image is used, else 240
    if(![defaults boolForKey:@"isBottomButtonAvailable"])
    {
        imageHeight = ((SCREEN_HEIGHT- yVal))/4;
    }
    else
    {
        imageHeight = ((SCREEN_HEIGHT- yVal - bottomBarButtonHeight))/4;
    }
    
    NSLog(@"image Dimension = %f , %f", imageWidth,imageHeight);
    
    for(int row=0; row < [arrMenuItems count]; row++)
    {
        MenuItemList* imainMenuDO = [arrMenuItems objectAtIndex:row];
        NSString *strTitle = imainMenuDO.mItemName;
       
        
        UIButton *FirstButton = [UIButton buttonWithType:UIButtonTypeCustom];
        FirstButton.frame = CGRectMake(X,Y, imageWidth,imageHeight);
        [FirstButton addTarget:self.delegateView action:@selector(clickonMenuItem:) forControlEvents:UIControlEventTouchUpInside];
        [FirstButton setAccessibilityLabel:imainMenuDO.mItemName];
        FirstButton.tag = row+1;
        [FirstButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 5.0, 5.0)];
        
        
        SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0.0,0.0, imageWidth, imageHeight)];
        asyncImageView.backgroundColor = [UIColor clearColor];
        [asyncImageView loadImage:[imainMenuDO.mItemImg stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
        //[asyncImageView loadImagefrommainBundel:@"768by226_1.png"];
        
        asyncImageView.userInteractionEnabled=NO;
        
        UIView *messageView = [[UIView alloc] init];
        messageView.frame = CGRectMake(X, FirstButton.frame.size.height/4*3, imageWidth, imageHeight/4);
        messageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        messageView.backgroundColor = [UIColor colorWithHex:btnBkgrdColor alpha:0.6];
        
        messageView.layer.shadowOffset = CGSizeZero;
        messageView.layer.shadowOpacity = 0.5;
         UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(X+5,FirstButton.frame.size.height/4*3, imageWidth, imageHeight/4)];
        if([[defaults valueForKey:@"isDisplayLabel"] isEqualToString:@"1"])
        {
           
        lbl.font = [UIFont systemFontOfSize:15];
        lbl.text = strTitle;
        lbl.textAlignment = NSTextAlignmentLeft;
        lbl.textColor = [UIColor convertToHexString:btnLblColor];
        lbl.numberOfLines = 0;
        
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            
            lbl.font = [UIFont boldSystemFontOfSize:13];
        }
        else
        {
            
            lbl.font = [UIFont boldSystemFontOfSize:18];
        }
        }
        
        
        [FirstButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        
        [mainmenuScrollView addSubview:FirstButton];
        [FirstButton addSubview:asyncImageView];
        [asyncImageView addSubview:messageView];
        [asyncImageView addSubview:lbl];
        
        Y += imageHeight;
        
    }
    [mainmenuScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, Y)];
    
}

//two tile
-(void)setButtonViewTwoTile:(BOOL)isTopBanner{
    
    mainmenuScrollView.frame=self.view.bounds;
    [mainmenuScrollView setBackgroundColor:[UIColor clearColor]];
    
    [self.view setBackgroundColor:[UIColor clearColor]];

    float X=(DEVICE_TYPE==UIUserInterfaceIdiomPhone)?VARIABLE_HEIGHT(30):VARIABLE_HEIGHT(50);
    
    float Y = VARIABLE_HEIGHT(40);
    float space_y = VARIABLE_HEIGHT(40);
    float imageWidth = (SCREEN_WIDTH - 2*X);//90:iphone, ipad:360 if same image is used, else 240
    float offset=0;
    

    if(isTopBanner)
    {
        
        if(![defaults boolForKey:@"isBottomButtonAvailable"]){
            offset = VARIABLE_HEIGHT(85);
        }
        else{
            offset = bottomBarButtonHeight+VARIABLE_HEIGHT(85);
        }
        
    }
    else
    {
        
        if(![defaults boolForKey:@"isBottomButtonAvailable"]){
            offset = (DEVICE_TYPE==UIUserInterfaceIdiomPhone)?VARIABLE_HEIGHT(50):VARIABLE_HEIGHT(30);
        }
        else{
            offset = (DEVICE_TYPE==UIUserInterfaceIdiomPhone)?bottomBarButtonHeight+VARIABLE_HEIGHT(50):bottomBarButtonHeight+VARIABLE_HEIGHT(30);
        }
    }
    
    float imageHeight = ((SCREEN_HEIGHT- Y-offset) - 2*space_y)/2;
    
    
    for(int row=0; row < [arrMenuItems count]; row++)
    {
        MenuItemList* imainMenuDO = [arrMenuItems objectAtIndex:row];
        
        NSString *strTitle = imainMenuDO.mItemName;
        UIButton *FirstButton = [UIButton buttonWithType:UIButtonTypeCustom];
        FirstButton.frame = CGRectMake(X, Y,imageWidth,imageHeight );
        [FirstButton addTarget:self.delegateView action:@selector(clickonMenuItem:) forControlEvents:UIControlEventTouchUpInside];
        FirstButton.tag = row+1;
        
        [FirstButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 5.0, 5.0)];
        FirstButton.layer.cornerRadius = 20;
        FirstButton.clipsToBounds=YES;
        [FirstButton setBackgroundColor:[UIColor clearColor]];
        
        
        SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(X, Y,
                                                                                    imageWidth, imageHeight)];
        asyncImageView.backgroundColor = [UIColor clearColor];
        asyncImageView.backgroundColor = [UIColor clearColor];
        [asyncImageView loadImage:imainMenuDO.mItemImg];
        asyncImageView.layer.cornerRadius = 20;
        asyncImageView.clipsToBounds =YES;
        
        UILabel *lblTitle = nil;
        
        if([[defaults valueForKey:@"isDisplayLabel"] isEqualToString:@"1"])
        {
      
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    
                    
                    lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0.0, asyncImageView.frame.size.height/4*3+VARIABLE_HEIGHT(10), imageWidth, 60)];
                    lblTitle.font =[UIFont boldSystemFontOfSize:45];
                }
                else{
                    lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0.0, asyncImageView.frame.size.height/4*3+VARIABLE_HEIGHT(5), imageWidth, 30)];
                    lblTitle.font =[UIFont boldSystemFontOfSize:23];
                }
            
            
                    if([strTitle length]<=7)
            [lblTitle setText:[NSString stringWithFormat:@"  %@",strTitle]];
        else
            [lblTitle setText:[NSString stringWithFormat:@"%@",strTitle]];
        
              lblTitle.numberOfLines=2;
        
        lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
        lblTitle.textColor = [UIColor convertToHexString: btnLblColor];
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        
        }
        [mainmenuScrollView addSubview:FirstButton];
        [mainmenuScrollView addSubview:asyncImageView];
        [asyncImageView addSubview:lblTitle];
        
        Y += imageHeight + space_y;
        
    }
    [mainmenuScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, Y)];
}



// Method to draw Custom and Single Column Templet

-(void)setButtonViewforCustomTemplet
{
    mainmenuScrollView.frame=self.view.bounds;
    [mainmenuScrollView setBackgroundColor:[UIColor clearColor]];
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    int totalScreenWidth = self.view.bounds.size.width;
    int buttonHeight;
    int LabelHeight;
    int DiffBetweenButtonsInWidth = 0;
    int DiffBetweenButtonsInHight = 0;
    
    int circularButtonWeidth;
    int circularButtonHeight;
    
    
    
    int SquareButtonWeidth;
    int SquareButtonHeight;
    
    int horizontalGap, initialHorizantalGap;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        circularButtonWeidth = 160;
        circularButtonHeight = 200;
        
        SquareButtonWeidth = 160;
        SquareButtonHeight = 200;
        initialHorizantalGap = 25.6;
        horizontalGap = initialHorizantalGap - 5;
        buttonHeight = 80;
        LabelHeight = 40;
    } else {
        circularButtonWeidth = 80;
        circularButtonHeight = 90;
        
        SquareButtonWeidth = 80;
        SquareButtonHeight = 90;
        horizontalGap = 0;
        initialHorizantalGap = 2;
        buttonHeight = 46;
        LabelHeight = 26;
    }
    int xCoord=initialHorizantalGap;
    int yCoord=0;

    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    
    //MainMenuResponse* menu = [[MainMenuResponse alloc] init];
   // [menu setValuesForKeysWithDictionary:self.menuResponse];
    
    for(int row=0; row < [arrMenuItems count]; row++)
    {
        if([arrMenuItems count]>row)
        {
            
            MenuItemList* imainMenuDO = [arrMenuItems objectAtIndex:row];
            //NSDictionary* dictList = menu.arMItemList[row];
           // [imainMenuDO setValuesForKeysWithDictionary:dictList];
            
            NSString *strTitle = imainMenuDO.mItemName;
            NSString *strShap = imainMenuDO.mShapeName;
            NSString *strgroup = imainMenuDO.linkTypeName;
            
            NSString *current = imainMenuDO.mShapeName;
            if(!current)
                current = strgroup;
            
            if(row > 0)
            {
                MenuItemList* imainMenuDOPrevious = [arrMenuItems objectAtIndex:row - 1];
               // NSDictionary* dictList = menu.arMItemList[row - 1];
               // [imainMenuDOPrevious setValuesForKeysWithDictionary:dictList];
                NSString *previous = imainMenuDOPrevious.mShapeName;
                if(!previous)
                    previous = imainMenuDOPrevious.linkTypeName;
                
                if([current isEqualToString:previous])
                {
                    if([previous isEqualToString:@"Square"])
                    {
                        if(xCoord >= SquareButtonWeidth*3 + initialHorizantalGap*3 +1)
                        {
                            xCoord = initialHorizantalGap;
                            yCoord= yCoord+SquareButtonHeight;
                        }
                        else
                        {
                            xCoord = xCoord + SquareButtonWeidth+5 + horizontalGap;
                        }

                    }
                    else if([previous isEqualToString:@"Circle"])
                    {
                        if(xCoord >= circularButtonWeidth*3 + initialHorizantalGap*3 +1)
                        {
                            xCoord = initialHorizantalGap;
                            yCoord= yCoord+circularButtonHeight;
                        }
                        else
                        {
                            xCoord = xCoord + circularButtonWeidth+5 + horizontalGap;
                        }
 
                    }
                }
                else
                {
                    xCoord = initialHorizantalGap;
                    if([previous isEqualToString:@"Circle"])
                        yCoord= yCoord+circularButtonHeight;
                    else if([previous isEqualToString:@"Square"])
                        yCoord= yCoord+SquareButtonHeight;
                }
            }
            
            
            float asyncImageWidth, asyncImageHeight, lblHeight, lblFont, arrowY, cornerRadius, squareCornerRadius, borderWidth;
            int titleFont;
            NSString *menuBtnGradientName;
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                asyncImageWidth = 160;
                asyncImageHeight = 160;
                lblHeight = 30;
                lblFont = 14;
                arrowY = 33.5;
                titleFont = 18;
                cornerRadius = 80;
                squareCornerRadius = 12;
                borderWidth = 2.0;
                menuBtnGradientName = @"menuGradientBig";
            } else {
                asyncImageWidth = 60;
                asyncImageHeight = 60;
                lblHeight = 25;
                lblFont = 9;
                arrowY = 17;
                titleFont = 13;
                cornerRadius = 30;
                squareCornerRadius = 5;
                borderWidth = 1.0;
                menuBtnGradientName = @"menuBtnGradient";
            }

            
            if([strShap isEqualToString:@"Rectangle"])
            {
                xCoord = 0;
                yCoord = yCoord-3;
                UIButton *FirstButton = [UIButton buttonWithType:UIButtonTypeCustom];
                FirstButton.frame = CGRectMake(xCoord, yCoord,totalScreenWidth+DiffBetweenButtonsInWidth,buttonHeight );
                [FirstButton addTarget:self.delegateView action:@selector(clickonMenuItem:) forControlEvents:UIControlEventTouchUpInside];
                FirstButton.tag = row+1;
                [FirstButton setBackgroundImage:[UIImage imageNamed:menuBtnGradientName] forState:UIControlStateNormal];
                FirstButton.layer.borderWidth = 1.0;
                FirstButton.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
                
                [FirstButton setTitle:strTitle forState:UIControlStateNormal];
                if(menulevel > 1)
                {
                    [FirstButton setTitleColor:[UIColor convertToHexString:imainMenuDO.smBtnFontColor] forState:UIControlStateNormal];
                    FirstButton.backgroundColor = [UIColor convertToHexString:imainMenuDO.smBtnColor];
                }
                else
                {
                    [FirstButton setTitleColor:[UIColor convertToHexString:imainMenuDO.mBtnFontColor] forState:UIControlStateNormal];
                    FirstButton.backgroundColor = [UIColor convertToHexString:imainMenuDO.mBtnColor];
                }
                
                [FirstButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
                FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:titleFont];
                
                FirstButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                
                FirstButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
                
                UIImageView *arrowImage = [[UIImageView alloc]initWithFrame:CGRectMake(totalScreenWidth-20, arrowY, 9, 13)];
                [arrowImage setBackgroundColor:[UIColor clearColor]];
                [arrowImage setImage:[UIImage imageNamed:@"right_arrow"]];
                [FirstButton addSubview:arrowImage];

                
                [mainmenuScrollView addSubview:FirstButton];
                
                
                
                yCoord += buttonHeight + DiffBetweenButtonsInHight;
            }
            else if([strgroup isEqualToString:@"Label"] || [strgroup isEqualToString:@"Text"])
            {
                if([arrMenuItems count]>1){ xCoord = 0;
                UILabel *lblGroup = [[UILabel alloc]initWithFrame:CGRectMake(xCoord, yCoord,totalScreenWidth,LabelHeight )];
                lblGroup.tag = row + 1;
                lblGroup.userInteractionEnabled=NO;
                [lblGroup setText:[NSString stringWithFormat:@"  %@",strTitle]];
                
                lblGroup.backgroundColor = [UIColor clearColor];
                if(menulevel > 1)
                {
                    lblGroup.textColor=[UIColor convertToHexString:imainMenuDO.smGrpFntColor];
                    if([strgroup isEqualToString:@"Text"]) // Group & "Label - used for Title"
                        lblGroup.backgroundColor = [UIColor convertToHexString:imainMenuDO.smGrpBkgrdColor];
                }
                else
                {
                    lblGroup.textColor=[UIColor convertToHexString:imainMenuDO.mGrpFntColor];
                    if([strgroup isEqualToString:@"Text"])
                        lblGroup.backgroundColor = [UIColor convertToHexString:imainMenuDO.mGrpBkgrdColor];
                }
                
                lblGroup.font =[UIFont boldSystemFontOfSize:titleFont];
                
                lblGroup.textAlignment = NSTextAlignmentLeft;
                
                [mainmenuScrollView addSubview:lblGroup];
                yCoord += LabelHeight + DiffBetweenButtonsInHight+3;
                }
                
            }
            else if([strShap isEqualToString:@"Circle"])
            {
                // draw the icon image
                SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(xCoord, yCoord, asyncImageWidth, asyncImageHeight)];
                asyncImageView.backgroundColor = [UIColor clearColor];
                [asyncImageView loadImage:[imainMenuDO.mItemImg stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
                asyncImageView.tag = 8989;
                [mainmenuScrollView addSubview:asyncImageView];
                
                // To make the UIView rounded/Circular
                asyncImageView.layer.cornerRadius = cornerRadius;
                asyncImageView.layer.masksToBounds = YES;
                asyncImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
                asyncImageView.layer.borderWidth = borderWidth;
                
                UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(xCoord, yCoord+asyncImageHeight, asyncImageView.bounds.size.width, lblHeight)];
                if([strTitle length]<=7)
                    [lblTitle setText:[NSString stringWithFormat:@"  %@",strTitle]];
                else
                    [lblTitle setText:[NSString stringWithFormat:@"%@",strTitle]];
                
                lblTitle.font =[UIFont boldSystemFontOfSize:lblFont];
                lblTitle.numberOfLines=2;
                lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
                if(menulevel > 1)
                {
                    lblTitle.textColor=[UIColor convertToHexString:imainMenuDO.smFontColor];
                    lblTitle.backgroundColor = [UIColor convertToHexString:imainMenuDO.smBtnColor];
                }
                else
                {
                    lblTitle.textColor=[UIColor convertToHexString:imainMenuDO.mFontColor];
                    lblTitle.backgroundColor = [UIColor convertToHexString:imainMenuDO.mBtnColor];
                }

                
                
                lblTitle.backgroundColor = [UIColor clearColor];
                lblTitle.textAlignment = NSTextAlignmentCenter;
                //lblTitle.textAlignment = NSTextAlignmentLeft
                [mainmenuScrollView addSubview:lblTitle];
                
              //  //[asyncImageView release];
                
                // place a clickable button on top of everything
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setFrame:CGRectMake(xCoord, yCoord, circularButtonWeidth, circularButtonHeight)];
//                button.backgroundColor = [UIColor clearColor];
                [button addTarget:self.delegateView action:@selector(clickonMenuItem:) forControlEvents:UIControlEventTouchUpInside];
                button.tag = row+1;
                [button setBackgroundColor:[UIColor clearColor]];
                [mainmenuScrollView addSubview:button];
                
                
            }
            else if([strShap isEqualToString:@"Square"])
            {
               
                // draw the icon image
                SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(xCoord, yCoord, asyncImageWidth, asyncImageHeight)];
                asyncImageView.backgroundColor = [UIColor clearColor];
               // [asyncImageView loadImage:[imainMenuDO.mItemImg stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                [asyncImageView loadImage:imainMenuDO.mItemImg];
                asyncImageView.tag = 8989;
                [mainmenuScrollView addSubview:asyncImageView];
                // To make the UIView rounded/Circular
                asyncImageView.layer.cornerRadius = squareCornerRadius;
                asyncImageView.layer.masksToBounds = YES;
                asyncImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
                asyncImageView.layer.borderWidth = borderWidth;
                asyncImageView.userInteractionEnabled=NO;

                
                UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(xCoord,yCoord+asyncImageHeight, asyncImageView.bounds.size.width, lblHeight)];
                if([strTitle length]<=7)
                    [lblTitle setText:[NSString stringWithFormat:@"  %@",strTitle]];
                else
                    [lblTitle setText:[NSString stringWithFormat:@"%@",strTitle]];
                
                lblTitle.font =[UIFont boldSystemFontOfSize:lblFont];
                lblTitle.numberOfLines=2;
                lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
                //lblTitle.textAlignment = NSTextAlignmentCenter;
                
                if(menulevel > 1)
                {
                    lblTitle.textColor=[UIColor convertToHexString:imainMenuDO.smFontColor];
                    lblTitle.backgroundColor = [UIColor convertToHexString:imainMenuDO.smBtnColor];
                }
                else
                {
                    lblTitle.textColor=[UIColor convertToHexString:imainMenuDO.mBtnColor];
                    lblTitle.backgroundColor = [UIColor convertToHexString:imainMenuDO.mBtnColor];
                }

                lblTitle.backgroundColor = [UIColor clearColor];
                lblTitle.textAlignment = NSTextAlignmentCenter;
                [mainmenuScrollView addSubview:lblTitle];
               // //[asyncImageView release];
                // place a clickable button on top of everything
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setFrame:CGRectMake(xCoord, yCoord, SquareButtonWeidth, SquareButtonHeight-0.5)];
                [button addTarget:self.delegateView action:@selector(clickonMenuItem:) forControlEvents:UIControlEventTouchUpInside];
                button.tag = row+1;
                
                if(menulevel > 1)
                {
                    button.backgroundColor = [UIColor convertToHexString:imainMenuDO.smBtnColor];
                }
                else
                {
                    button.backgroundColor = [UIColor convertToHexString:imainMenuDO.mBtnColor];
                }
                button.backgroundColor = [UIColor clearColor];
                
                [mainmenuScrollView addSubview:button];
                
            }
        }
    }

    [mainmenuScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, yCoord + circularButtonHeight)];
}

-(void)setgloseryEffect:(UIButton*)FirstButton
{
    // To set the button glosery effect
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = FirstButton.bounds;
    gradient.colors = [NSArray arrayWithObjects: (id)[[UIColor colorWithHex:strBackgroundColorHexValueForButton alpha:1.0] CGColor],(id)[[UIColor colorWithHex:strBackgroundColorHexValueForButton alpha:0.5] CGColor], nil];
    [FirstButton.layer insertSublayer:gradient atIndex:0];
    
}

- (void)setbackgroundcolorAndTitleColor:(UIButton*)newButton transparent:(BOOL)isTransprent
{
    // Set background color and Tttle Color for Button
    float val;
    if(isTransprent)
        val = 0.4;
    else
        val = 1.0;
    
    UIImage *image = [self imageWithColor:[UIColor colorWithHex:strBackgroundColorHexValueForButton alpha:val]];
    [newButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [newButton setBackgroundImage:image forState:UIControlStateNormal];
    newButton.layer.cornerRadius = 6.0;
    newButton.layer.borderWidth = 1.0;
    newButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    newButton.clipsToBounds = YES;
}


-(void)ButtonClicked:(id)sender
{
    [delegateView ButtonClicked:sender];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end

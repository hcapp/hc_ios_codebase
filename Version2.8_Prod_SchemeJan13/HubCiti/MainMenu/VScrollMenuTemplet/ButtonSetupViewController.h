//
//  ViewController.h
//  ButtonDemo
//
//  Created by ionnor on 9/3/13.
//  Copyright (c) 2013 ionnor. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ButtonSetupViewController : UIViewController<UIScrollViewDelegate>
{
    NSArray *arrMainMenuItemsObjects;
    NSString *strBackgroundColorHexValueForButton;
    NSString *strBackgroundColorHexValueForView;
    int totalNumberofButtons;
    id __weak delegateView;
    IBOutlet UIScrollView *mainmenuScrollView;
}
@property (nonatomic, weak)id delegateView;
@property(nonatomic,strong) NSString * btnBkgrdColor;
@property(nonatomic,strong) NSString * btnLblColor;

-(void)setbasicpropertyforMainMenu:(NSString*)buttonbg Viewbackground:(NSString*)viewbg mainMenuItemObjects:(NSDictionary*)arrGroups templetid:(int)tempid setDelegate:(id)delegate;
-(void)setBottombuttonOfPage:(NSString*)buttonBGColor titles:(NSArray*)arrTitles btnImages:(NSArray*)arrImages setDelegate:(id)delegate;
- (void)setbackgroundcolorAndTitleColor:(UIButton*)newButton transparent:(BOOL)isTransprent;
@end

@protocol mainmenuButtonTouch<NSObject>

-(void)mainButtonClicked:(id)sender;



@end
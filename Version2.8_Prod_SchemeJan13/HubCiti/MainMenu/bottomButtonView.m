//
//  bottomButtonView.m
//  HubCiti
//
//  Created by ionnor on 10/22/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "bottomButtonView.h"
#import "SdImageView.h"

@implementation bottomButtonView

@synthesize delegateView,tag, titleText,imageName;

- (id) initWithTitle:(NSString *)title frame:(CGRect)frame imageName:(NSString *)imagetext img_off:(NSString*)imagetext_off delegate:(id)del tag:(NSInteger)tagVal
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        imageName = imagetext;
        imageNameOff = imagetext_off;
        titleText = title;
        self.delegateView = del;
        tag = tagVal;
    }
    return self;

}

+ (id) initWithTitle:(NSString *)title frame:(CGRect)frame imageName:(NSString *)imageName img_off:(NSString*)imageNameOff delegate:(id)del tag:(NSInteger)tagVal
{
    bottomButtonView *tmpInstance = [[bottomButtonView alloc] initWithTitle:title frame:frame imageName:imageName img_off:imageNameOff delegate:del tag:tagVal] ;
	return tmpInstance;
}


- (void)drawRect:(CGRect)rect
{
    SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, bottomBarButtonHeight)];
    asyncImageView.backgroundColor = [UIColor clearColor];
    [asyncImageView loadImage:imageName];
    [self addSubview:asyncImageView];
    
    ////[asyncImageView release];
    
   /* UIImage *btnImg = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageName]]];
    UIImage *btnImg_Off = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageNameOff]]];*/
    // Currently Title will not come from server, only image will required.
    
//    // draw the menu item title shadow
//    NSString* shadowText = titleText;
//    [[UIColor whiteColor] set];
//    UIFont *bold14 = [UIFont fontWithName:@"Helvetica-Bold" size:14];
//	[shadowText drawInRect:CGRectMake(0.0, 52.0, self.frame.size.width, 20.0) withFont:bold14 lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentCenter];
    
    
    // place a clickable button on top of everything
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:self.bounds];
    button.backgroundColor = [UIColor clearColor];
    button.adjustsImageWhenHighlighted = YES;
   // [button setBackgroundImage:btnImg forState:UIControlStateNormal];
   // [button setBackgroundImage:btnImg_Off forState:UIControlStateHighlighted];
    [button addTarget:self.delegateView action:@selector(bottomButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = tag;
    [self addSubview:button];
}


@end

//
//  SubMenuGroupingAndSortingViewController.h
//  HubCiti
//
//  Created by ionnor on 12/3/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SubMenuGroupingAndSortingViewControllerDelegate;
@interface SubMenuGroupingAndSortingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UITableView *tblGroupingNSorting;
    NSMutableArray *arrFilters;
    NSMutableArray *arrSorting;
    NSMutableArray *arrDepartMenu;
    NSMutableArray *arrTypesMenu;
    
    int filterSelectionVal,cityFlag;
    int sortSelectionval;
    UIPickerView *dinningTypePickerView;
    UIToolbar *pickerToolbar;
    int displayValue;
    UIImageView *checkButton;
    BOOL isExpanded,selectedFlag;

}
@property (strong, nonatomic) UIButton *doneBtn;
@property (strong, nonatomic) UIButton *cancelBtn;

@property(nonatomic,strong)NSDictionary *citiesDic;

@property(nonatomic,strong)NSMutableArray *arrCities;

@property(nonatomic,strong)NSMutableArray *arrCitiId;

@property(nonatomic,strong)NSMutableArray *selectedCitiIds;

@property (weak) id <SubMenuGroupingAndSortingViewControllerDelegate> delegate;


@end

@protocol SubMenuGroupingAndSortingViewControllerDelegate <NSObject>

- (void)SubMenuGroupingAndSortingViewController:(SubMenuGroupingAndSortingViewController*)viewController;

@end

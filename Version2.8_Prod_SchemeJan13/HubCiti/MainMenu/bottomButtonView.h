//
//  bottomButtonView.h
//  HubCiti
//
//  Created by ionnor on 10/22/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface bottomButtonView : UIView
{
    NSString *imageName;
    NSString *imageNameOff;
    NSString *titleText;
    id __weak delegateView;
    NSInteger tag;
}
+ (id) initWithTitle:(NSString *)title frame:(CGRect)frame imageName:(NSString *)imageName img_off:(NSString*)imageNameOff delegate:(id)del tag:(NSInteger)tagVal;

@property(nonatomic, copy) NSString *imageName;
@property(nonatomic, copy) NSString *titleText;
@property (nonatomic, weak)id delegateView;
@property (nonatomic,assign) NSInteger tag;
@end

//
//  SubMenuGroupingAndSortingViewController.m
//  HubCiti
//
//  Created by ionnor on 12/3/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "SubMenuGroupingAndSortingViewController.h"
#import "AppDelegate.h"


@interface SubMenuGroupingAndSortingViewController ()

@end

@implementation SubMenuGroupingAndSortingViewController
@synthesize arrCities,arrCitiId,citiesDic,selectedCitiIds, doneBtn, cancelBtn;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    UIButton *btn = [UtilityManager customizeBarButtonItem:@"Done"];
    [btn addTarget:self action:@selector(doneButtonTouch:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = mainPage;
   // //[mainPage release];
    
    UIButton *btncancel = [UtilityManager customizeBarButtonItem:NSLocalizedString(@"Cancel",@"Cancel")];
    [btncancel addTarget:self action:@selector(cancelButtonTouch:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* btncancelbar = [[UIBarButtonItem alloc] initWithCustomView:btncancel];
    self.navigationItem.rightBarButtonItem = btncancelbar;
    //[btncancelbar release];

    
    self.navigationItem.title = @"Filter & Sort";
    
    
    arrFilters = [[NSMutableArray alloc]init];
    if([defaults boolForKey:@"NameSortRectGrid"])
         arrSorting = [[NSMutableArray alloc]init];
        else
    arrSorting = [[NSMutableArray alloc]initWithObjects:@"Name",nil];
    
    if ([RegionApp isEqualToString:@"1"]) {
        [self fetchCityPreference];
    }
    
    arrDepartMenu = [[NSMutableArray alloc]init];
    arrTypesMenu = [[NSMutableArray alloc]init];
    
//    [arrGroups addObject:@"Alphabetically"];

    if([[defaults valueForKey:@"departFlag"]isEqualToString:@"1"])
        [arrFilters addObject:@"Department"];

    if([[defaults valueForKey:@"typeFlag"]isEqualToString:@"1"])
        [arrFilters addObject:@"Type"];
    
//    groupSelectionVal = -1;
//    sortSelectionval = 0;

    
    [self setViewForGroupingNSorting];
    // Do any additional setup after loading the view from its nib.
}


-(void)fetchCityPreference{
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    
//<UserDetails>
//    <userId>6</userId>
//    <hubCitiId>4173</hubCitiId>
//    <module>SubMenu</module> // Find All,  Find Single,  Citi Exp, Events, SubMenu
//    <linkId>9081</linkId>
//    <typeId>0</typeId> // Input can be NULL.
//    <departmentId>0</departmentId> // Input can be NULL.
//    <level>0</level> // Input can be NULL.
//    </UserDetails>


    [xmlStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId><userId>%@</userId><module>SubMenu</module>", [defaults  valueForKey:KEY_HUBCITIID],[defaults  valueForKey:KEY_USERID]];
    
    [xmlStr appendFormat:@"<level>%@</level>",[defaults valueForKey:KEY_MENULEVEL]];
    
    int level = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    
    if ([defaults valueForKeyPath:@"commaSeperatedCities"]) {
        [xmlStr appendFormat:@"<departmentId>%@</departmentId>",@"0"];
        [xmlStr appendFormat:@"<typeId>%@</typeId>",@"0"];
        
    }
    else{
//        [xmlStr appendFormat:@"<departmentId>%@</departmentId>",[[HubCitiAppDelegate getDeptIdArray]objectAtIndex:level-1]];
//        [xmlStr appendFormat:@"<typeId>%@</typeId>",[[HubCitiAppDelegate getTypeIdArray]objectAtIndex:level-1]];
        
//        [xmlStr appendFormat:@"<departmentId>%@</departmentId>",[deptId valueForKey:[[HubCitiAppDelegate getDeptIdArray] lastObject]]];// [[HubCitiAppDelegate getDeptIdArray]objectAtIndex:level-1]];
//        [xmlStr appendFormat:@"<typeId>%@</typeId>",[typeId valueForKey:[[HubCitiAppDelegate getDeptIdArray] lastObject]]];
        
    }
    
    [xmlStr appendFormat:@"<linkId>%@</linkId>",[[HubCitiAppDelegate getLinkIdArray]objectAtIndex:level-1]];

    
    [xmlStr appendFormat:@"</UserDetails>"];
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercitypref",BASE_URL];
    
    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    [self parseCityPreference:responseXML];
    ReleaseAndNilify(xmlStr);
    
}

-(void)parseCityPreference:(NSString *)responseXml{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
    {
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        arrCities =[[NSMutableArray alloc]init];
        arrCitiId =[[NSMutableArray alloc]init];
        selectedCitiIds =[[NSMutableArray alloc]init];
        TBXMLElement *cityListElement = [TBXML childElementNamed:@"cityList" parentElement:tbXml.rootXMLElement];
        if(cityListElement)
        {
            cityFlag = 1;
            TBXMLElement *CityElement = [TBXML childElementNamed:@"City" parentElement:cityListElement];
            while (CityElement)
            {
                
                TBXMLElement *cityIdElement = [TBXML childElementNamed:@"cityId" parentElement:CityElement];
                TBXMLElement *cityNameElement = [TBXML childElementNamed:@"cityName" parentElement:CityElement];
                TBXMLElement *isCityElement = [TBXML childElementNamed:@"isCityChecked" parentElement:CityElement];

                
                if (cityIdElement) {
                    
                    [arrCitiId addObject:[TBXML textForElement:cityIdElement]];
                    
                }
                
                if (cityNameElement) {
                    
                    [arrCities addObject:[TBXML textForElement:cityNameElement]];
                    
                }
                
                if (isCityElement) {
                    if ([[TBXML textForElement:isCityElement] isEqualToString:@"1"]) {
                        [selectedCitiIds addObject:[TBXML textForElement:cityIdElement]];
                    }
                }
                
                CityElement = [TBXML nextSiblingNamed:@"City" searchFromElement:CityElement];
            }
        }
        
        
        if ([arrCities count]!=0) {
            citiesDic = [[NSDictionary alloc] initWithObjects:arrCitiId forKeys:arrCities];
        }
        
        
        if ([defaults valueForKey:@"SelectedCityIds"]==nil) {
            
            [defaults setValue:selectedCitiIds forKey:@"SelectedCityIds"];
        }
        DLog(@"%@",[selectedSortCityDic valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]]);
        
        if ([selectedSortCityDic valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]] != [NSNull null] && [selectedSortCityDic valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]] != NULL) {
            [selectedCitiIds removeAllObjects];
            [selectedCitiIds addObjectsFromArray:[NSMutableArray arrayWithArray:[[selectedSortCityDic valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]] componentsSeparatedByString:@","]]];
        }
//        else{
//        selectedCitiIds = [[NSMutableArray alloc]initWithArray:arrCitiId];//
//        }
    }
    
//     [selectedCitiIds componentsSeparatedByString:@","];
    
    //    else
    //    {
    //        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    //
    //		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
    //		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
    //        cityFlag=0;
    //
    //    }
    //    arrCities =[NSMutableArray arrayWithObjects:@"Texas", @"Dalas", @"Rockwall", nil];
    //    arrCitiId = [NSMutableArray arrayWithObjects:@"1", @"2", @"3", nil];
    //    citiesDic = [[NSDictionary alloc] initWithObjects:arrCitiId forKeys:arrCities];
    //    cityFlag = 1;
//    if ([defaults valueForKey:@"SelectedCityIds"]==nil) {
//        
//        [defaults setValue:arrCitiId forKey:@"SelectedCityIds"];
//    }
//    
//    selectedCitiIds = [[NSMutableArray alloc]initWithArray:[defaults valueForKey:@"SelectedCityIds"]];
    
    
}




-(void)cancelButtonTouch:(id)sender
{
    [defaults setValue:@"yes" forKey:@"isComingFromSubMenuGroupingAndSorting"];
    //[self.delegate SubMenuGroupingAndSortingViewController:self];
    [self.navigationController popViewControllerAnimated:NO];
    //[self.view removeFromSuperview];
}

-(void)request_deptandmenutype:(NSString*)option
{
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<MenuItem><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [xmlStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    [xmlStr appendFormat:@"<menuId>%@</menuId>",[defaults valueForKey:@"menuId"]];
    [xmlStr appendFormat:@"<fName>%@</fName>",option];
    [xmlStr appendFormat:@"</MenuItem>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/deptandmenutype",BASE_URL];
    
    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    ReleaseAndNilify(xmlStr);

}

-(void)responseData:(NSString *) response
{
    [self parse_deptandmenutype:response];
}


-(void)parse_deptandmenutype:(NSString*)responseXML
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXML])
		return;
    
   
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXML];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mItemList = [TBXML childElementNamed:@"mItemList" parentElement:tbXml.rootXMLElement];
        if(mItemList)
        {
            TBXMLElement *MenuItemElement = [TBXML childElementNamed:@"MenuItem" parentElement:mItemList];
            while (MenuItemElement)
            {
                NSMutableDictionary *dicType = [[NSMutableDictionary alloc]init];
                NSMutableDictionary *dicdepart = [[NSMutableDictionary alloc]init];
                
                

                // Add Department
                if (displayValue == 1) {
                    TBXMLElement *departmentIdElement = [TBXML childElementNamed:@"departmentId" parentElement:MenuItemElement];
                    TBXMLElement *departmentNameElement = [TBXML childElementNamed:@"departmentName" parentElement:MenuItemElement];
                    
                    if(departmentIdElement)
                        [dicdepart setValue:[TBXML textForElement:departmentIdElement] forKey:@"departmentId"];
                    if(departmentNameElement)
                        [dicdepart setValue:[TBXML textForElement:departmentNameElement] forKey:@"departmentName"];
                    if([dicdepart count] > 0)
                        [arrDepartMenu addObject:dicdepart];
                    
                    //[dicdepart release];
                }
                else if (displayValue==2){
                    // Add Types
                    TBXMLElement *mItemTypeIdElement = [TBXML childElementNamed:@"mItemTypeId" parentElement:MenuItemElement];
                    TBXMLElement *mItemTypeNameElement = [TBXML childElementNamed:@"mItemTypeName" parentElement:MenuItemElement];
                    if(mItemTypeIdElement)
                        [dicType setValue:[TBXML textForElement:mItemTypeIdElement] forKey:@"mItemTypeId"];
                    if(mItemTypeNameElement)
                        [dicType setValue:[TBXML textForElement:mItemTypeNameElement] forKey:@"mItemTypeName"];
                    if([dicType count] > 0)
                        [arrTypesMenu addObject:dicType];
                    
                    //[dicType release];
                }

              
                MenuItemElement = [TBXML nextSiblingNamed:@"MenuItem" searchFromElement:MenuItemElement];
            }

        }
        
        if([arrTypesMenu count]>0 || [arrDepartMenu count] > 0)
            [self showpickerView];
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
    }

}

-(void)showpickerView
{
    tblGroupingNSorting.userInteractionEnabled = NO;
    
    // By Default selected first value
    if(displayValue == 1 && [arrDepartMenu count] > 0)
    {
        NSMutableDictionary *dic = [arrDepartMenu objectAtIndex:0];
        [defaults setValue:[dic valueForKey:@"departmentId"] forKey:@"departmentId"];
        [defaults setValue:@"0" forKey:@"typeId"];
        
    }
    else if(displayValue == 2 && [arrTypesMenu count] > 0)
    {
        NSMutableDictionary *dic = [arrTypesMenu objectAtIndex:0];
        [defaults setValue:[dic valueForKey:@"mItemTypeId"] forKey:@"typeId"];
        [defaults setValue:@"0" forKey:@"departmentId"];
    }
    
    
    if(dinningTypePickerView)
    {
        [dinningTypePickerView removeFromSuperview];
       // [dinningTypePickerView release];
    }
    float pickerHeight = 210;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        if (IOS7){
            dinningTypePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - pickerHeight, 320, pickerHeight)];//Y 221+70+44
        }
        else{
            dinningTypePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - pickerHeight, 320, pickerHeight)];//Y 221+44
        }
    }
    else
    {
         dinningTypePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 3*(SCREEN_HEIGHT/4), SCREEN_WIDTH, SCREEN_HEIGHT/4)];
    }
   
    dinningTypePickerView.delegate = self;
    dinningTypePickerView.dataSource = self;
    dinningTypePickerView.showsSelectionIndicator = YES;
    dinningTypePickerView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:dinningTypePickerView];
    
    if(pickerToolbar)
    {
        [pickerToolbar removeFromSuperview];
       // [pickerToolbar release];
    }
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        if (IOS7){
            pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, dinningTypePickerView.frame.origin.y - 44, 320, 44)];//221+70
        }
        else{
            pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, dinningTypePickerView.frame.origin.y - 44, 320, 44)];
        }
    }
    else
    {
        pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,  3*(SCREEN_HEIGHT/4)-44, SCREEN_WIDTH, 44)];
    }
   
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar setBarTintColor:[UIColor blackColor]];
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
   // [flexSpace release];
    
    UIBarButtonItem *pickerDoneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDone:)];
    [barItems addObject:pickerDoneBtn];
  //  [doneBtn release];
    
    UILabel *lblTitle ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 320, 20)];
        lblTitle.font = [UIFont boldSystemFontOfSize:15];

    }
    else
    {
        lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 10,SCREEN_WIDTH, 40)];
        lblTitle.font = [UIFont boldSystemFontOfSize:20];

    }
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.text = @"Select Type";
    lblTitle.textColor = [UIColor whiteColor];
        lblTitle.textAlignment= NSTextAlignmentCenter;
    [pickerToolbar addSubview:lblTitle];
   // [lblTitle release];
    
    [pickerToolbar setItems:barItems animated:YES];
    //[barItems release];
    [self.view addSubview:pickerToolbar];
    
    [dinningTypePickerView reloadAllComponents];
    [self.view bringSubviewToFront:dinningTypePickerView];
}


-(void)pickerDone:(id)sender
{
//    displayValue = 0;
    tblGroupingNSorting.userInteractionEnabled = YES;
    [pickerToolbar removeFromSuperview];
    [dinningTypePickerView removeFromSuperview];
    
    //[pickerToolbar release];
    pickerToolbar = nil;
    //[dinningTypePickerView release];
    dinningTypePickerView = nil;
}


-(void)doneButtonTouch:(id)sender
{
    [defaults setValue:@"yes" forKey:KEY_DONESORT];
    [defaults setValue:nil forKey:@"isComingFromSubMenuGroupingAndSorting"];
    subMenuSortDone = YES;
    NSNumber *selectedFilter = [NSNumber numberWithInt:filterSelectionVal];
     NSNumber *selectedSort = [NSNumber numberWithInt:sortSelectionval];
//    [defaults setInteger:groupSelectionVal forKey:@"groupSelectionVal"];
     [selectedFilterOption setValue:selectedFilter forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
    [selectedSortOption setValue:selectedSort forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
//    [selectedSortOption removeLastObject];
//    [selectedSortOption addObject: [defaults valueForKey:@"sortSelectionval"]];
   
    [defaults setValue:@"NONE" forKey:@"sortOrder"];
    // If alphabetical selcted the set 0
    if(displayValue==3 )
    {
        if (sortSelectionval==-1) {
            [defaults setValue:@"NONE" forKey:@"sortOrder"];
        }
        else{
        [defaults setValue:@"ASC" forKey:@"sortOrder"];
        }
        [defaults setValue:@"0" forKey:@"departmentId"];
        [defaults setValue:@"0" forKey:@"typeId"];
        [[HubCitiAppDelegate getDeptIdArray]removeLastObject];
        [[HubCitiAppDelegate getDeptIdArray]addObject:[defaults valueForKey:@"departmentId"]];
        [[HubCitiAppDelegate getTypeIdArray]removeLastObject];
        [[HubCitiAppDelegate getTypeIdArray]addObject:[defaults valueForKey:@"typeId"]];
        [deptId setValue:[defaults valueForKey:@"departmentId"] forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
        [typeId setValue:[defaults valueForKey:@"typeId"] forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];

        if ([selectedCitiIds count]==0) {
            [defaults setValue:nil forKey:@"SelectedCityIds"];
            [defaults setValue:nil forKey:@"commaSeperatedCities"];
            [selectedSortCityDic setObject:[NSNull null] forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
        }
        else{
            //            [defaults setValue:selectedCitiIds forKey:@"SelectedCityIds"];
            NSMutableString *requestStr = [[NSMutableString alloc] init];
            //changed to send sorted array
            [selectedCitiIds sortUsingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
                return [str1 compare:str2 options:(NSNumericSearch)];
            }];
            
            for (int i=0; i<[selectedCitiIds count]; i++) {
                
                [requestStr appendFormat:@"%@,", selectedCitiIds[i]];
            }
            
            if ([requestStr hasSuffix:@","])
                [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
            [defaults setValue:requestStr forKey:@"commaSeperatedCities"];
            [selectedSortCityDic setObject:[defaults valueForKey:@"commaSeperatedCities"] forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
        }
    }
    else if(displayValue==1 || displayValue==2)
    {
        
        if (sortSelectionval==-1) {
            [defaults setValue:@"NONE" forKey:@"sortOrder"];
        }
        else{
            [defaults setValue:@"ASC" forKey:@"sortOrder"];
        }
         [selectedSortCityDic setObject:[NSNull null] forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
        DLog(@"%@",[defaults valueForKey:@"departmentId"]);
        DLog(@"%@",[defaults valueForKey:@"typeId"]);
        [[HubCitiAppDelegate getDeptIdArray]removeLastObject];
        [[HubCitiAppDelegate getDeptIdArray]addObject:[defaults valueForKey:@"departmentId"]];
        [[HubCitiAppDelegate getTypeIdArray]removeLastObject];
        [[HubCitiAppDelegate getTypeIdArray]addObject:[defaults valueForKey:@"typeId"]];
        [deptId setValue:[defaults valueForKey:@"departmentId"] forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
        [typeId setValue:[defaults valueForKey:@"typeId"] forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
    }
    
    // Check the Selected Sorting Value
//    if(sortSelectionval == 0){
//        [defaults setValue:@"name" forKey:@"SortMenuBy"];
//        [defaults setValue:nil forKey:@"SelectedCityIds"];
//    }
//
//    else
     else if(displayValue == 4)
    {

        if (sortSelectionval==-1) {
            [defaults setValue:@"NONE" forKey:@"sortOrder"];
        }
        else{
            [defaults setValue:@"ASC" forKey:@"sortOrder"];
        }
        
        if ([selectedCitiIds count]==0) {
            [defaults setValue:nil forKey:@"SelectedCityIds"];
            [defaults setValue:nil forKey:@"commaSeperatedCities"];
            [selectedSortCityDic setObject:[NSNull null] forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
        }
        else{
//            [defaults setValue:selectedCitiIds forKey:@"SelectedCityIds"];
            NSMutableString *requestStr = [[NSMutableString alloc] init];
            
            [selectedCitiIds sortUsingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
                return [str1 compare:str2 options:(NSNumericSearch)];
            }];
            for (int i=0; i<[selectedCitiIds count]; i++) {
                
                [requestStr appendFormat:@"%@,", selectedCitiIds[i]];
            }
            
            if ([requestStr hasSuffix:@","])
                [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
            [defaults setValue:requestStr forKey:@"commaSeperatedCities"];
            [selectedSortCityDic setObject:[defaults valueForKey:@"commaSeperatedCities"] forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
        }
    }
    //[self.delegate SubMenuGroupingAndSortingViewController:self];
    // Assign self as the delegate for the child view controller
    
    [self.navigationController popViewControllerAnimated:NO];
   // [self.view removeFromSuperview];
}

-(void)setViewForGroupingNSorting
{

    if ([selectedSortOption valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]] == NULL) {
        sortSelectionval=-1;
    }
    else{
        sortSelectionval = (int)[[selectedSortOption valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]]integerValue];
    }
     DLog(@"%d",sortSelectionval);
    DLog(@"%@",[selectedSortOption valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]]);
    
    if ([selectedFilterOption valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]] == NULL) {
        filterSelectionVal=-1;
    }
    else{
    filterSelectionVal = (int)[[selectedFilterOption valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]]integerValue];
    }
    DLog(@"%d",filterSelectionVal);
    
    if(cityFlag ==1 && [RegionApp isEqualToString:@"1"])
    {
        [arrFilters addObject:@"City"];
    }
    
    tblGroupingNSorting = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-60) style:UITableViewStylePlain];
    tblGroupingNSorting.dataSource=self;
    tblGroupingNSorting.delegate=self;
    [tblGroupingNSorting setBackgroundColor:[UIColor whiteColor]];
    
      
    [self.view addSubview:tblGroupingNSorting];
    
    [tblGroupingNSorting reloadData];
    
//    doneBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [doneBtn addTarget:self action:@selector(doneButtonTouch:)forControlEvents:UIControlEventTouchUpInside];
//    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
//    doneBtn.backgroundColor = [UIColor darkGrayColor];
//    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
//        [doneBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];
//    }
//    else{
//        [doneBtn.titleLabel setFont:[UIFont systemFontOfSize:20]];
//    }
//    [doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    
//    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
//        doneBtn.frame = CGRectMake(5,tblGroupingNSorting.frame.size.height-62, SCREEN_WIDTH/2-10, 20);
//        doneBtn.layer.cornerRadius = 10;
//    }
//    else{
//        doneBtn.frame = CGRectMake(5,tblGroupingNSorting.frame.size.height, SCREEN_WIDTH/2-10, 30);
//        doneBtn.layer.cornerRadius = 15;
//    }
//    doneBtn.clipsToBounds = YES;
//    [tblGroupingNSorting addSubview:doneBtn];
//    
//    cancelBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [cancelBtn addTarget:self action:@selector(cancelButtonTouch:)forControlEvents:UIControlEventTouchUpInside];
//    [cancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
//    cancelBtn.backgroundColor = [UIColor darkGrayColor];
//    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
//        [cancelBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];
//    }
//    else{
//        [cancelBtn.titleLabel setFont:[UIFont systemFontOfSize:20]];
//    }
//    [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    
//    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
//        cancelBtn.frame = CGRectMake(5,tblGroupingNSorting.frame.size.height-30, SCREEN_WIDTH/2-10, 20);
//        cancelBtn.layer.cornerRadius = 10;
//    }
//    else{
//        cancelBtn.frame = CGRectMake(5,tblGroupingNSorting.frame.size.height, SCREEN_WIDTH/2-10, 30);
//        cancelBtn.layer.cornerRadius = 15;
//    }
//    cancelBtn.clipsToBounds = YES;
//    [tblGroupingNSorting addSubview:cancelBtn];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
         return 44.0;
    }
    else
    {
         return 70.0;
    }
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 26.0;
    }
    else
    {
        return 36.0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([arrFilters count] == 0 || [arrSorting count] == 0) {
        return 1;
    }
    else
        return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0 && [arrSorting count] != 0)
        return [arrSorting count];
    else
        return [arrFilters count];
    
    return 0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *lblCatName ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 18)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
    }
    else
    {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 28)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
    }
    lblCatName.text = [self tableView:tableView titleForHeaderInSection:section];
    [lblCatName setTextColor:[UIColor blackColor]];
    [lblCatName setBackgroundColor:[UIColor grayColor]];
    return lblCatName;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellSubMenuListView";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor =  [UIColor clearColor];
        
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }
        
    }
    
    if(indexPath.section == 0 && [arrSorting count] != 0)
    {
        UILabel *lblEvtDate;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 20)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        }
        else
        {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 30)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
        }
        [lblEvtDate setText:[arrSorting objectAtIndex:indexPath.row]];
        [lblEvtDate setTextColor:[UIColor blackColor]];
        [lblEvtDate setBackgroundColor:[UIColor clearColor]];
        
        [cell.contentView addSubview:lblEvtDate];
        //[lblEvtDate release];
        
        if(indexPath.row == sortSelectionval)
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        
        UILabel *lblEvtDate;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 20)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        }
        else
        {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 30)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
        }

        [lblEvtDate setText:[arrFilters objectAtIndex:indexPath.row]];
        [lblEvtDate setTextColor:[UIColor blackColor]];
        [lblEvtDate setBackgroundColor:[UIColor clearColor]];
        //[lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        [cell.contentView addSubview:lblEvtDate];
        //[lblEvtDate release];
        
        
        if(isExpanded==TRUE && indexPath.row>([arrFilters indexOfObject:@"City"])){
            
            UIButton *content ;
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                content = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 45)];
            }
            else
            {
                content = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
            }

            [content setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
            content.tag = indexPath.row;
            [content addTarget:self action:@selector(didSelectCity:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            if ([selectedCitiIds containsObject:[citiesDic valueForKey:[arrFilters objectAtIndex:content.tag]]]) {
                checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOn.png"]];
                if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                    checkButton.frame = CGRectMake(10, 15, 20, 20);
                    
                }
                else
                {
                    checkButton.frame = CGRectMake(10, 20, 30, 30);
                    
                }
                checkButton.backgroundColor =[UIColor clearColor];
            }
            
            else{
                
                if ([[arrFilters objectAtIndex:content.tag] isEqualToString:@"All"] ) {
                    
                    if ([selectedCitiIds count] == [[citiesDic allValues]count]) {
                        checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOn.png"]];
                        if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                            checkButton.frame = CGRectMake(10, 15, 20, 20);
                            
                        }
                        else
                        {
                            checkButton.frame = CGRectMake(10, 20, 30, 30);
                            
                        }
                        checkButton.backgroundColor =[UIColor clearColor];
                    }
                    
                   
                    
                    else{
                        checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOff.png"]];
                        if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                            checkButton.frame = CGRectMake(10, 15, 20, 20);
                            
                        }
                        else
                        {
                            checkButton.frame = CGRectMake(10, 20, 30, 30);
                            
                        }
                        checkButton.backgroundColor =[UIColor clearColor];
                    }
                   
                }
                
                else{
                checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOff.png"]];
                if (DEVICE_TYPE== UIUserInterfaceIdiomPhone) {
                    checkButton.frame = CGRectMake(10, 15, 20, 20);
                    
                }
                else
                {
                    checkButton.frame = CGRectMake(10, 20, 30, 30);
                    
                }
                checkButton.backgroundColor =[UIColor clearColor];
                }
                
            }
            
            UILabel *lblCiti ;
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                lblCiti = [[UILabel alloc]initWithFrame:CGRectMake(40, 0, SCREEN_WIDTH, 45)];
                [lblCiti setFont:[UIFont boldSystemFontOfSize:13]];
            }
            else
            {
                lblCiti = [[UILabel alloc]initWithFrame:CGRectMake(50, 0, SCREEN_WIDTH, 70)];
                [lblCiti setFont:[UIFont boldSystemFontOfSize:18]];
            }

            [lblCiti setText:[arrFilters objectAtIndex:indexPath.row]];
            [lblCiti setTextColor:[UIColor blackColor]];
            [lblCiti setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
            //[lblCiti setFont:[UIFont boldSystemFontOfSize:13]];
            
            [content addSubview:checkButton];
            [content addSubview:lblCiti];
            [cell.contentView addSubview:content];
            //[content release];
        }
        
        
        if(indexPath.row == filterSelectionVal){

            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0 && [arrSorting count] != 0)
      return @" Sort Items by";
    else
        return @" Filter Items by";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//  int secVal;
//    if(arrSorting.count == 0)
//        secVal = 0;
//    else
//        secVal = 1;
//
    NSString *titleForHeader = [self tableView:tableView titleForHeaderInSection:indexPath.section];
    NSLog(@"%@",titleForHeader);
    
   if([titleForHeader isEqualToString:@" Filter Items by"] )
    {
        
        if(indexPath.row == [arrFilters indexOfObject:@"City"])
        {
            filterSelectionVal = (int)indexPath.row;

            NSMutableArray *arrayOfIndexPaths = [[NSMutableArray alloc] init];
            if (isExpanded==FALSE )
            {
                isExpanded = TRUE;
                if([arrCities count]>1){
                    [arrCities insertObject:@"All" atIndex:0];
                    [arrFilters addObjectsFromArray:arrCities];
                }
                else{
                [arrFilters addObjectsFromArray:arrCities];
                }
                for(int i = (int)[arrFilters indexOfObject:@"City"]+1 ; i < [arrFilters count] ; i++)
                {
                    NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:indexPath.section];
                    [arrayOfIndexPaths addObject:path];
                }
                [tblGroupingNSorting beginUpdates];
                
                [tblGroupingNSorting insertRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation:UITableViewRowAnimationTop];
                
                [tblGroupingNSorting endUpdates];
            }
             displayValue = 4;
        }
        else
        {
            if([arrCities count]>1){
                [arrCities removeObjectAtIndex:0];
                 [arrFilters removeObjectsInArray:[citiesDic allKeys]];
                if ([[arrFilters lastObject]isEqualToString:@"All"]) {
                     [arrFilters removeLastObject];
                }
                
            }
            else{
            [arrFilters removeObjectsInArray:[citiesDic allKeys]];
            }
            isExpanded = FALSE;
//            if ([selectedCitiIds count]>0) {
//                [selectedCitiIds removeAllObjects];
//            }
//            
//            [selectedCitiIds addObjectsFromArray:[citiesDic allValues]];
            
            if( [titleForHeader isEqualToString:@" Filter Items by"] && indexPath.row==[arrFilters indexOfObject:@"Department"] && [[defaults valueForKey:@"departFlag"]isEqualToString:@"1"])
            {
                filterSelectionVal = (int)indexPath.row;

                displayValue = 1;
                if([arrDepartMenu count] <=0)
                {
                    [self request_deptandmenutype:@"dept"];
                }
                else
                    [self showpickerView];
            }
            
            else if([titleForHeader isEqualToString:@" Filter Items by"] && indexPath.row ==[arrFilters indexOfObject:@"Type"] && [[defaults valueForKey:@"typeFlag"]isEqualToString:@"1"])
            {
                filterSelectionVal = (int)indexPath.row;

                displayValue = 2;
                if([arrTypesMenu count] <= 0)
                {
                    [self request_deptandmenutype:@"type"];
                }
                else
                    [self showpickerView];
            }
            
            else
                displayValue = 3;
        }
       
        
    }
 
    else
    {
        displayValue = 3;
//        if(tCell.accessoryType == UITableViewCellAccessoryCheckmark){
//            tCell.accessoryType = UITableViewCellAccessoryNone;
//        }
//        else{
//            tCell.accessoryType = UITableViewCellAccessoryCheckmark;
//        }
        //Set value to show checkmark in table row
        if([arrSorting count] != 0){
                if (sortSelectionval==-1) {
                        sortSelectionval = (int)indexPath.row;
            
                    }
                else
                    sortSelectionval = -1;
        }

    }


    
//    // Set value to show checkmark in table row
//    if([arrSorting count] != 0){
//        if (sortSelectionval==-1) {
//            sortSelectionval = (int)indexPath.row;
//
//        }
//        else
//        sortSelectionval = -1;
//    }
//    
//    else
//        filterSelectionVal = (int)indexPath.row;
//    
  //  DLog(@"%d",filterSelectionVal);
    
    [tblGroupingNSorting reloadData];
}


-(void)didSelectCity:(id)sender
{
    
    if([[citiesDic allValues] count]>= 1)
    {
        UIButton *btnCity = (UIButton*)sender;
        
        if ([[arrFilters objectAtIndex:btnCity.tag] isEqualToString:@"All"]) {
            if ([selectedCitiIds count]==[[citiesDic allValues]count]) {
                [selectedCitiIds removeAllObjects];
            }
            else{
                [selectedCitiIds removeAllObjects];
                [selectedCitiIds addObjectsFromArray:[citiesDic allValues]];
            }
        }
        
        else if ([selectedCitiIds containsObject:[citiesDic valueForKey:[arrFilters objectAtIndex:btnCity.tag]]]) {
            [selectedCitiIds removeObject:[citiesDic valueForKey:[arrFilters objectAtIndex:btnCity.tag]]];
            
        }
        
       
        else{
            [selectedCitiIds addObject:[citiesDic valueForKey:[arrFilters objectAtIndex:btnCity.tag]]];
            
        }
        [tblGroupingNSorting reloadData];
    }
    
}


#pragma PICKERVIEW
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 44.0;
    }
    else
    {
        return 60.0;
    }
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    if(displayValue == 1)
    {
        NSMutableDictionary *dic = [arrDepartMenu objectAtIndex:row];
        [defaults setValue:[dic valueForKey:@"departmentId"] forKey:@"departmentId"];
        [defaults setValue:@"0" forKey:@"typeId"];
        
    }
    else if(displayValue == 2)
    {
        NSMutableDictionary *dic = [arrTypesMenu objectAtIndex:row];
       [defaults setValue:[dic valueForKey:@"mItemTypeId"] forKey:@"typeId"];
        [defaults setValue:@"0" forKey:@"departmentId"];
    }

}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(displayValue == 1)
        return [arrDepartMenu count];
    else
        return [arrTypesMenu count];
    
    return 0;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(displayValue == 1)
    {
        NSMutableDictionary *dic = [arrDepartMenu objectAtIndex:row];
        return [dic valueForKey:@"departmentName"];
    }
    else if(displayValue == 2)
    {
        NSMutableDictionary *dic = [arrTypesMenu objectAtIndex:row];
        return [dic valueForKey:@"mItemTypeName"];
    }
    return nil;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end

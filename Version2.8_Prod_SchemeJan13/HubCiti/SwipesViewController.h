//
//  SwipesViewController.h
//  HubCiti
//
//  Created by Kitty on 19/05/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SortAndFilter.h"
#import "CommonUtility.h"

@class SingleCatRetailers;
@class EventsListViewController;
@class FindLocationServiceViewController;
@class CityExperienceViewController;
@class SwipesViewController;

@protocol SwipesViewControllerDelegate <NSObject>
- (void)addItemViewController:(SwipesViewController *)controller didFinishEnteringItem:(SortAndFilter *)item;
@end

@interface SwipesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *expandedRowData;
    int cityFlag;
    SingleCatRetailers *iSingleCatRetailers;
    EventsListViewController *iEventsListViewController;
    FindLocationServiceViewController *iFindLocationServiceViewController;
    CityExperienceViewController *iCityExperienceViewController;
    WebRequestState iWebRequestState;
    UILabel *dobLabel;
    UIView *dobHolder;
    UIDatePicker *dobPicker;
    NSString *selectedDOB,*serverDOB;
    UIToolbar *dobToolBar;
}


@property (strong, nonatomic) UITableView *swipeTV;
@property (nonatomic) BOOL showDone,bottomBtn;
@property (strong, nonatomic) UIButton *done;

@property(strong, nonatomic) NSMutableArray *filterByItemArray;
@property(strong, nonatomic) NSMutableArray *sortByItemArray;
@property(strong, nonatomic) NSMutableArray *secHeaderData;
@property(strong, nonatomic) NSMutableArray *mainTableArr;
@property(strong, nonatomic) NSMutableArray *catArray;
@property(strong, nonatomic) NSMutableArray *optArray;
@property(strong, nonatomic) NSMutableArray *cityArray;
@property(strong, nonatomic) NSMutableArray *intArray;
@property(strong, nonatomic) NSMutableArray *othersArray;

@property(strong, nonatomic) NSMutableArray *dataArray;
@property(strong, nonatomic) UIActivityIndicatorView *activityIndicator;

////////////////// Changes on 23rd may////////////////
@property(nonatomic,strong) NSMutableArray *arrCategoryId,*arrCategoryNames,*arrSelectedCatIds,*arrSelectedCatNames;
@property(nonatomic,strong) NSMutableArray *arrOptionsId,*arrOptionsNames,*arrSelectedOptionsIds,*arrSelectedOptionsNames;
@property(nonatomic,strong) NSMutableArray *arrCitiId,*arrCitiesNames,*arrSelectedCitiIds,*arrSelectedCitiNames;
@property(nonatomic,strong) NSMutableArray *arrInterestsId,*arrInterestsNames,*arrSelectedInterestsIds,*arrSelectedInterestsNames;
@property(nonatomic,strong) NSMutableArray *arrOthersId,*arrOthersNames,*arrSelectedOthersIds,*arrSelectedOthersNames;

@property(nonatomic,strong)NSMutableDictionary *categoryDic;
@property(nonatomic,strong)NSMutableDictionary *optionsDic;
@property(nonatomic,strong)NSMutableDictionary *citiesDic;
@property(nonatomic,strong)NSMutableDictionary *interestsDic;
@property(nonatomic,strong)NSMutableDictionary *othersDic;



@property(nonatomic,strong)NSString *module;
@property (nonatomic, strong) NSString *categoryName,*catId;

@property (nonatomic, strong) NSString *srchKey,*srchKeyBand;
@property (nonatomic, strong) NSNumber *evtTypeID;
@property (nonatomic, strong) NSString *bandId;


@property(nonatomic,strong) NSString *retailId, *cityExpId;

@property (readwrite) int filterID;

@property(nonatomic,strong) NSString *retailLocationId;

@property(nonatomic) BOOL isRetailerEvent;

@property(nonatomic,strong) NSString *fundraiserId;
@property(nonatomic,strong) NSString *couponsPostalCodeSort;
@property(nonatomic,assign) NSInteger segmentSelected;

@property(nonatomic) BOOL distanceSelected,localSpecialSelected,eventDateSelected,sortEventDateSelected, alphabeticallySelected, bandSelected, venueSelected;

@property(nonatomic,strong) SortAndFilter *sortFilObj;

@property (weak) id <SwipesViewControllerDelegate> delegate;


@end

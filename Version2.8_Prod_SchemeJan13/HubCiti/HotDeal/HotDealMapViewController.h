//
//  HotDealMapViewController.h
//  HubCiti
//
//  Created by Keshava on 4/16/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>

@interface HotDealMapViewController : UIViewController<CLLocationManagerDelegate,
MKMapViewDelegate,CustomizedNavControllerDelegate>
{
    NSString *strRetailerName;
    NSArray *arrLocations;
    
    NSString *retailerLatitude;
    NSString *retailerLongitude;
    MKMapView *mapView;
}
@property(nonatomic,strong)NSString *retailerLatitude;
@property(nonatomic,strong)NSString *retailerLongitude;
@property(nonatomic,strong)NSString *strRetailerName;
@property(nonatomic,strong)NSArray *arrLocations;
@end

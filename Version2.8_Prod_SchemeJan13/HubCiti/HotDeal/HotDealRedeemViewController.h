//
//  HotDealRedeemViewController.h
//  Scansee
//
//  Created by ionnor on 6/19/13.
//
//

#import <UIKit/UIKit.h>

@interface HotDealRedeemViewController : UIViewController
{

    
    IBOutlet UILabel *lblStartDate;
    IBOutlet UILabel *lblEndDate;
    IBOutlet UILabel *lblExpiration;
    IBOutlet UILabel *lblCode;
    IBOutlet UIButton *btnRedeem;
    IBOutlet UIWebView *txtDescription;
    
    NSString *strhotDealId;
    NSString *strStartDate;
    NSString *strEndDate;
    NSString *strDescription;
    NSString *strCode;
    NSString *strExpiration;
    NSString *strUserName;
    id objClass;
}
@property(nonatomic,strong)id objClass;
@property(nonatomic,strong)NSString *strhotDealId;
@property(nonatomic,strong)NSString *strStartDate;
@property(nonatomic,strong)NSString *strEndDate;
@property(nonatomic,strong)NSString *strDescription;
@property(nonatomic,strong)NSString *strCode;
@property(nonatomic,strong)NSString *strExpiration;
@property(nonatomic,strong)NSString *strUserName;


@property (strong, nonatomic) IBOutlet UILabel *StartDate;

@property (strong, nonatomic) IBOutlet UILabel *EndDate;

@property (strong, nonatomic) IBOutlet UILabel *Expiration;

@property (strong, nonatomic) IBOutlet UILabel *Code;

- (IBAction)redeemButtonClicked:(id)sender;
- (IBAction)crossButtonPressed:(id)sender;
- (void)redeemHotDeal;
@end

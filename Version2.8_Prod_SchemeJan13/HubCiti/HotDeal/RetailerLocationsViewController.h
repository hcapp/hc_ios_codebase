//
//  RetailerLocationsViewController.h
//  Scansee
//
//  Created by ionnor on 6/20/13.
//
//

#import <UIKit/UIKit.h>

@interface RetailerLocationsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UILabel *lblRetailerName;
    IBOutlet UILabel *lblDealName;
    IBOutlet UITableView *tblRetailsLocation;
    
    NSString *strDealName;
    NSString *strRetailerName;
    NSArray *arrLocations;
}

@property(nonatomic,strong)NSString *strDealName;
@property(nonatomic,strong)NSString *strRetailerName;
@property(nonatomic,strong)NSArray *arrLocations;

- (IBAction)crossButtonPressed:(id)sender;
@end

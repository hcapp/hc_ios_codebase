//
//  HotDealsList.m
//  Scansee
//
//  Created by Dhananjaya C H on 22/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HotDealsList.h"
#import "MainMenuViewController.h"
#import "PreferredCategoriesScreen.h"
#import "WebBrowserViewController.h"
#import "HotDealDetails.h"
//#import "PopulationCenterCities.h"
#import <CoreLocation/CoreLocation.h>
#import "CLLocation (Strings).h"
//#import "GalleryList.h"
#import "QuartzCore/QuartzCore.h"


@implementation HotDealDO
@synthesize apiPartnerId, apiPartnerName;
@synthesize catId, catName;
@synthesize hotDealDetailsObjArray;
@end

@implementation HotDealDetailsDO
@synthesize hdName, hdSDesc, hdImgPath, hdListId;
@synthesize hdId, hdPrice, hdSalePrice;
@synthesize hdUrl, hdNewFlag;
@end

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;

@implementation HotDealsList{
    UIActivityIndicatorView *loading;

}

@synthesize m_Button;

static int count = 0;
static int pickerRow = 0;
static int tempLastVisitedRecord = 0;
static int tempCount = 0;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization.
//		//isFirstButtonClicked = YES;
//    }
//    return self;
//}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    loading = nil;
    if (IOS7 == NO)
        m_searchBar.tintColor = [UIColor blackColor];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    lastvalueforLoop = 0; //@PaginationChange: Assigned with "0" to start
	categoryIdValue = 0;
    m_Button.hidden = YES;
    hotDealIconImg.hidden = YES;
    hotDealFoundTxt.hidden = YES;
    
    // if (IS_IPHONE5)
    // m_searchBar.frame = CGRectMake(0,-40, self.view.frame.size.width, 44);
    
    m_searchBar.frame = CGRectMake(0,110, self.view.frame.size.width, 44);
    
	customView.hidden = YES;
	customView.tag = 0;
	self.title = @"Hot Deals";
	self.navigationItem.hidesBackButton = YES;
	self.navigationController.navigationBar.hidden = NO;
    
    //customize righbarbuttonitem
    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = mainPage;
    [mainPage release];
	
    // If coming from SubMenu then Show BackButton
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
    {
        UIButton *backBtn = [UtilityManager customizeBackButton];
        [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        self.navigationItem.leftBarButtonItem = back;
        self.navigationItem.hidesBackButton = YES;
        [back release];
    }
    
	[byCategory setImage:[UIImage imageNamed:@"tab_btn_down_byctgry.png"] forState:UIControlStateHighlighted];
//	[galleryBtn setImage:[UIImage imageNamed:@"claimed_on.png"] forState:UIControlStateHighlighted];
	[preferenceBtn setImage:[UIImage imageNamed:@"tab_btn_down_preferences.png"] forState:UIControlStateHighlighted];
	
    hotDealObjArray = [[NSMutableArray alloc]init];
    
	if ([defaults  boolForKey:@"HotDealsplash"] == YES) {
		//[self showSplash];
        byCategory.enabled = YES;
        [SharedManager setRefreshCategories:YES];
        [self request_getHotDealProds];
		[defaults  setBool:NO forKey:@"HotDealsplash"];
	}
	
	lastVisitedRecord = 0;
    
}

-(void) viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:YES];
    
	[m_searchBar resignFirstResponder];
	customView.hidden = YES;
    
	modelView.backgroundColor = [UIColor colorWithRGBInt:0xe8ab29];
    
	if ([SharedManager refreshList] == YES) {
        
        lastVisitedRecord = 0 ;
        categoryIdValue = 0;
        count = 0;
        tempLastVisitedRecord = 0;
        tempCount = 0;
        lastvalueforLoop = 0;
        [self removeAllObjectsinArray];
        [self request_getHotDealProds];
        [hotDealTable setContentOffset:CGPointMake(0, 0) animated:YES];
        [SharedManager setRefreshList:NO];
	}
 	  
    [self resizeViews];
}


-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)resizeViews{
    
    if (favCat == 1){//means favorite categories not set
        m_Button.hidden = NO;
        m_Button.frame = CGRectMake(0, 55, 320, 60);
        m_searchBar.frame = CGRectMake(0, 115, 320, 44);
        
        float y_pos = m_searchBar.frame.origin.y + m_searchBar.frame.size.height;
        
        hotDealTable.frame = CGRectMake(0, y_pos, SCREEN_WIDTH, SCREEN_HEIGHT-(y_pos+50));
    }
    
    else{//means favorite categories set
        
        m_Button.hidden = YES;
        m_searchBar.frame = CGRectMake(0, 55, 320, 44);
        float y_pos = m_searchBar.frame.origin.y + m_searchBar.frame.size.height;
        
        hotDealTable.frame = CGRectMake(0, y_pos, SCREEN_WIDTH, SCREEN_HEIGHT-(y_pos+50));    }
}

#pragma mark navigation barbuttonitem action
-(void)returnToMainPage:(id)sender
{
	[UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
}

#pragma mark request methods
-(void) request_getHotDealProds{
	
	if([Network currentReachabilityStatus]==0)
	{
		UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error!",@"Error!") message:NSLocalizedString(@"Network is not available",@"Network is not available") delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
	else
	{
        iWebRequestType = getHotDealProds;
		NSMutableString *requestStr = [[NSMutableString alloc] init];
		[requestStr appendFormat:@"<HotDealsListRequest><userId>%@</userId><hubCitiId>%@</hubCitiId><category>%d</category>",[defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID],categoryIdValue];
        
		NSString * searchStr = m_searchBar.text;
		searchStr = [searchStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		if ([searchStr length]>0)
			[requestStr appendFormat:@"<searchItem><![CDATA[%@]]></searchItem>", m_searchBar.text];
		
        if([defaults  valueForKey:KEY_MAINMENUID])
            [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>", [defaults  valueForKey:KEY_MAINMENUID]];
        
        else if([defaults  valueForKey:KEY_MITEMID])
            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults  objectForKey:KEY_MITEMID]];
        
        else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
        
        if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorized) && [SharedManager gps_allow_flag] == YES && [defaults  valueForKey:KEY_LATITUDE]){
            
            [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
             [defaults  valueForKey:KEY_LATITUDE]];
            
        }
        else if ([[defaults  valueForKey:KEYZIPCODE]length]){
            
            [requestStr appendFormat:@"<zipCode><![CDATA[%@]]></zipCode>", [defaults  valueForKey:KEYZIPCODE]];
        }
        
        [requestStr appendFormat:@"<lastVisitedProductNo>%d</lastVisitedProductNo></HotDealsListRequest>",lastVisitedRecord];
		
		NSString *urlString = [NSString stringWithFormat:@"%@hotdeals/gethotdealprods",BASE_URL];
		
        
        if ([defaults boolForKey:@"ViewMore"]) {
            NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
            
            
            [self parse_getHotDealProds:response];
            
        }
        else{
            [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
        }

        
	    //[ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    }
}

//get user favorite categories
-(void)request_GetUserCat{
    
    iWebRequestType = getusercat;
    if([Network currentReachabilityStatus]==0)
    {
        CommonUtility *common = [[CommonUtility alloc]init];
        [common showAlert];
        [common release];
    }
    else {
        
        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
        
        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
        
    }
}

-(IBAction) bannerPressed : (id) sender {
    
    [self request_GetUserCat];
}


#pragma mark parse methods
-(void)responseData:(NSString*)response{
    
    switch (iWebRequestType) {
            
        case getHotDealProds:
            [self parse_getHotDealProds:response];
            break;
            
        case hddetail:
            [self parse_HdDetail:response];
            break;
            
        case getusercat:
            [self parse_GetUserCat:response];
            break;
            
        default:
            break;
    }
    [hotDealTable reloadData];
}

-(void) parse_getHotDealProds:(NSString *)response {
    
    if ([UtilityManager isNullOrEmptyString:response]) {
        byCategory.enabled = NO;
        [loading stopAnimating];
        ReleaseAndNilify(loading);
        return;
    }
    
	TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    
	TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbXml.rootXMLElement];
	
	if (nextPageElement == nil) {
		
        UIAlertView *alert;
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
		
        if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10005"]) {
            
            TBXMLElement *FavCatFlagElement = [TBXML childElementNamed:@"FavCatFlag" parentElement:tbXml.rootXMLElement];
            favCat = [[TBXML textForElement:FavCatFlagElement]intValue];
            [self resizeViews];
            
            byCategory.enabled = YES;
            
            [categoryArray removeAllObjects];
            [categoryId removeAllObjects];
            [categoryName removeAllObjects];
            [hotDealObjArray removeAllObjects];
            
            
            [categoryArray addObject:@"All"];
            [categoryId addObject:@"0"];
            
            pickerRow = 0;
            
            alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Hot Deals Found For Your Current Location",@"No Hot Deals Found For Your Current Location")
                                              message:nil
                                             delegate:self
                                    cancelButtonTitle:NSLocalizedString(@"OK",@"OK")
                                    otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        
        else{ //any server response code other than 10005
            
            byCategory.enabled = NO;
            
            // Banner the message given by the server
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            alert = [[UIAlertView alloc]initWithTitle:[TBXML textForElement:responseTextElement]
                                              message:nil
                                             delegate:self
                                    cancelButtonTitle:NSLocalizedString(@"OK",@"OK")
                                    otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        
        hotDealIconImg.hidden = NO;
        hotDealFoundTxt.hidden = NO;
        hotDealFoundTxt.text = @"No Hot Deals Found";
        return;
	}
	
	NSString *nextPageStr = [TBXML textForElement:nextPageElement];
	nextPage = [nextPageStr intValue];
	
    TBXMLElement *maxRowNumElement = [TBXML childElementNamed:@"maxRowNum" parentElement:tbXml.rootXMLElement];
    lastVisitedRecord = [[TBXML textForElement:maxRowNumElement]intValue];
    
	TBXMLElement *hdAPIResultElement = [TBXML childElementNamed:@"hdAPIResult" parentElement:tbXml.rootXMLElement];
    
    //@Deepak: Parsed and saved MainMenuID in User Default list for userTracking
    TBXMLElement *mainMenuIDElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbXml.rootXMLElement];
    
    if(mainMenuIDElement !=nil)
    {
        [defaults setObject:[TBXML textForElement:mainMenuIDElement] forKey:KEY_MAINMENUID];
    }
    
    TBXMLElement *favCatElement = [TBXML childElementNamed:@"FavCat" parentElement:tbXml.rootXMLElement];
    if (favCatElement != nil) {
        favCat = [[TBXML textForElement:favCatElement] intValue];
        [self resizeViews];
    }
	
    //@PaginationChange: Assigned the i and J value with paginaion variable
	int j =lastvalueforLoop ;
	int i= lastvalueforLoop;
    
    
    
	TBXMLElement *HotDealAPIResultSetElement = [TBXML childElementNamed:@"HotDealAPIResultSet" parentElement:hdAPIResultElement];
	while (HotDealAPIResultSetElement != nil)
    {
        hotDealIconImg.hidden = NO;
        hotDealFoundTxt.hidden = NO;
        hotDealTable.hidden = NO;
        
		TBXMLElement *apiNameElement = [TBXML childElementNamed:@"apiPartnerName" parentElement:HotDealAPIResultSetElement];
        
        TBXMLElement *apiIdElement = [TBXML childElementNamed:@"apiPartnerId" parentElement:HotDealAPIResultSetElement];
		
		TBXMLElement *hdCatInfoListElement = [TBXML childElementNamed:@"hdCatInfoList" parentElement:HotDealAPIResultSetElement];
        
        TBXMLElement *HotDealsCategoryInfoElement = [TBXML childElementNamed:@"HotDealsCategoryInfo" parentElement:hdCatInfoListElement];
		
		while (HotDealsCategoryInfoElement != nil) {
            HotDealDO *hotDealObj = [[HotDealDO alloc]init];
            
			TBXMLElement *categoryNameElement = [TBXML childElementNamed:@"categoryName" parentElement:HotDealsCategoryInfoElement];
            TBXMLElement *categoryIdElement = [TBXML childElementNamed:@"categoryId" parentElement:HotDealsCategoryInfoElement];
            
            hotDealObj.apiPartnerName = [TBXML textForElement:apiNameElement];
            hotDealObj.apiPartnerId = [TBXML textForElement:apiIdElement];
            
            hotDealObj.catName = [TBXML textForElement:categoryNameElement];
            if ((![[categoryName lastObject] isEqualToString:@"Uncategorized"]) && (categoryIdElement!= nil)){ //for uncategorized we wont get any categoryId
                hotDealObj.catId = [TBXML textForElement:categoryIdElement];
            }
            else{
                hotDealObj.catId = @"-1";
            }
            
            //[sectionArry addObject:[NSString stringWithFormat:@"%i",j]];
            
            
            if (![[[hotDealObjArray lastObject]catId] isEqualToString:[TBXML textForElement:categoryIdElement]])
            {
                hotDealObj.hotDealDetailsObjArray = [[NSMutableArray alloc]init];
            }
            else
            {
                // Copy Last Object Array in current AlertEventDO Object "if Same caegory Repeted"
                HotDealDO *hotDealObjRep = [hotDealObjArray lastObject];
                hotDealObj.hotDealDetailsObjArray = [[NSMutableArray alloc]initWithArray:hotDealObjRep.hotDealDetailsObjArray];
                [hotDealObjArray removeLastObject];
            }
            
			
			TBXMLElement *hotDealsDetailsArrayLstElement = [TBXML childElementNamed:@"hotDealsDetailsArrayLst" parentElement:HotDealsCategoryInfoElement];
			
			if (hotDealsDetailsArrayLstElement != nil) {
				
				TBXMLElement *HotDealsDetails = [TBXML childElementNamed:@"HotDealsDetails" parentElement:hotDealsDetailsArrayLstElement];

                //hotDealObj.hotDealDetailsObjArray = [[NSMutableArray alloc]init];
                
                //not to be included in while loop as it has to execute only once - only first city name
                if (HotDealsDetails != nil) {
                    
                        hotDealFoundTxt.frame = CGRectMake(52, 3, 248, 48);
                        hotDealFoundTxt.font = [UIFont fontWithName:@"Helvetica" size:15.0];
                        hotDealFoundTxt.text = NSLocalizedString(@"Great News!!\nMany Hot Deals Found Around You",@"Great News!!\nMany Hot Deals Found Around You");
                        hotDealIconImg.hidden = NO;
                }
				
				while (HotDealsDetails != nil) {
                    
					HotDealDetailsDO *hdDetailObj = [[HotDealDetailsDO alloc]init];
					TBXMLElement *hotDealNameElement = [TBXML childElementNamed:@"hotDealName" parentElement:HotDealsDetails];
					TBXMLElement *hotDealIdElement = [TBXML childElementNamed:@"hotDealId" parentElement:HotDealsDetails];
					TBXMLElement *hotDealImagePathElement = [TBXML childElementNamed:@"hotDealImagePath" parentElement:HotDealsDetails];
					TBXMLElement *hDshortDescriptionElement = [TBXML childElementNamed:@"hDshortDescription" parentElement:HotDealsDetails];
					TBXMLElement *hDPriceElement = [TBXML childElementNamed:@"hDPrice" parentElement:HotDealsDetails];
					TBXMLElement *hDSalePriceElement = [TBXML childElementNamed:@"hDSalePrice" parentElement:HotDealsDetails];
					TBXMLElement *hDURLElement = [TBXML childElementNamed:@"hdURL" parentElement:HotDealsDetails];
                    TBXMLElement *hnewFlagElement = [TBXML childElementNamed:@"newFlag" parentElement:HotDealsDetails];
                    
                    //@Deepak: HotDeal parsing for UserTracking
                    TBXMLElement *hDealListIDElement = [TBXML childElementNamed:@"hotDealListId" parentElement:HotDealsDetails];
                    if(hDealListIDElement !=nil)
                    {
                        hdDetailObj.hdListId = [TBXML textForElement:hDealListIDElement];
                    }
                    
                    if (hnewFlagElement!= nil)
                        hdDetailObj.hdNewFlag = [TBXML textForElement:hnewFlagElement];
                    
					if (hotDealNameElement!= nil)
                        hdDetailObj.hdName = [TBXML textForElement:hotDealNameElement];
                    
					if (hDshortDescriptionElement!= nil)
                        hdDetailObj.hdSDesc = [TBXML textForElement:hDshortDescriptionElement];
                        
					if (hotDealImagePathElement!= nil)
                        hdDetailObj.hdImgPath = [TBXML textForElement:hotDealImagePathElement];
                    
					if (hDPriceElement!= nil)
                        hdDetailObj.hdPrice = [TBXML textForElement:hDPriceElement];
                    
					if (hotDealIdElement!= nil)
                        hdDetailObj.hdId = [TBXML textForElement:hotDealIdElement];
                    
					if (hDSalePriceElement!= nil)
                        hdDetailObj.hdSalePrice = [TBXML textForElement:hDSalePriceElement];
                    
					if (hDURLElement!= nil)
                        hdDetailObj.hdUrl = [TBXML textForElement:hDURLElement];
					
					HotDealsDetails = [TBXML nextSiblingNamed:@"HotDealsDetails" searchFromElement:HotDealsDetails];

					[hotDealObj.hotDealDetailsObjArray addObject:hdDetailObj];
                    ReleaseAndNilify(hdDetailObj);
				}
			}
			i++;
			j++;
            lastvalueforLoop++; //@PaginationChange: Increment the value with loop
			HotDealsCategoryInfoElement = [TBXML nextSiblingNamed:@"HotDealsCategoryInfo" searchFromElement:HotDealsCategoryInfoElement];
            
            [hotDealObjArray addObject:hotDealObj];
		}
        
		HotDealAPIResultSetElement = [TBXML nextSiblingNamed:@"HotDealAPIResultSet" searchFromElement:HotDealAPIResultSetElement];
	}
    
    if ([SharedManager refreshCategories] == YES && [categoryName count]) {
        
        byCategory.enabled = YES;
        [categoryArray removeAllObjects];
        [categoryId removeAllObjects];
        
        //Add "All" as a default category
        [categoryArray addObject:@"All"];
        [categoryId addObject:@"0"];
        
        // Parse the list of category from the response and add in the "categoryArray" list to display as byCategory
        TBXMLElement *categoryElement = [TBXML childElementNamed:@"category" parentElement:tbXml.rootXMLElement];
        if(categoryElement!=nil)
        {
            TBXMLElement *categoryElementName = [TBXML childElementNamed:@"Category" parentElement:categoryElement];
            while(categoryElementName!=nil)
            {
                TBXMLElement *CatNameElement = [TBXML childElementNamed:@"category" parentElement:categoryElementName];
                TBXMLElement *CatIdElement = [TBXML childElementNamed:@"catId" parentElement:categoryElementName];
                
                if(CatNameElement!=nil)
                    [categoryArray addObject:[TBXML textForElement:CatNameElement]];
                
                if(CatIdElement!=nil)
                    [categoryId addObject:[TBXML textForElement:CatIdElement]];
                
                categoryElementName = [TBXML nextSiblingNamed:@"Category" searchFromElement:categoryElementName];
            }
        }
        
        pickerRow = 0;
        [data_picker reloadAllComponents];
        [data_picker selectRow:pickerRow inComponent:0 animated:NO];
    }
    else if([categoryName count] == 0)
        byCategory.enabled = NO;
    [defaults setBool:NO forKey:@"ViewMore"];
}

-(void)parse_HdDetail:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    HotDealDetails *hdd = [[HotDealDetails alloc] initWithNibName:@"HotDealDetails" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:hdd animated:NO];
    [hdd release];
}

//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    [settings release];
}

#pragma mark pickerView delegate methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	
    return [categoryArray count];
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	
    if ([categoryArray count] >0) {
		
		categoryValue = [categoryArray objectAtIndex:row];
		//categoryIdValue = [[categoryId objectAtIndex:row] intValue];
        pickerRow = (int)row;
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *label = [[UILabel alloc] init];
    
    if (IOS7){
        label.frame = CGRectMake(20, 0, 220, 40);
       // pickerView.backgroundColor = [UIColor colorWithRGBInt:0xEEEEEE];
        pickerView.layer.borderWidth = 5.0;
        pickerView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    }
    
    else
        label.frame = CGRectMake(0, 0, 200, 40);
    
    [label setText:[NSString stringWithFormat:@"%@", [categoryArray objectAtIndex:row]]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setNumberOfLines:2];
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    [label setTextAlignment:NSTextAlignmentLeft];
    return [label autorelease];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 60.0;
}
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

#pragma mark splash methods
-(void)showSplash
{
    UIViewController *modalViewController = [[UIViewController alloc] init];
    modalViewController.view = modelView;
	modelView.backgroundColor = [UIColor colorWithRGBInt:0xf2b32e];
    [self presentViewController:modalViewController animated:NO completion:nil];
    [modalViewController release];
    [self performSelector:@selector(hideSplash) withObject:nil afterDelay:1.0];
}

- (void)hideSplash{
    
    [[self presentedViewController] dismissViewControllerAnimated:NO completion:nil];
    byCategory.enabled = YES;
    [SharedManager setRefreshCategories:YES];
    [self request_getHotDealProds];
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    index = indexPath;
    HotDealDO *hotDealObj = [hotDealObjArray objectAtIndex:indexPath.section];
    
    if (indexPath.section == ([hotDealObjArray count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count]){
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"] && lastVisitedRecord !=0) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self viewMoreResults];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hotDealTable reloadData];
                });
            });
            
        }
    }
    
}


#pragma mark Table view delegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    index = indexPath;
	HotDealDO *hotDealObj = [hotDealObjArray objectAtIndex:indexPath.section];
    

    [defaults setBool:NO forKey:@"ViewMore"];
//    if (indexPath.section == ([hotDealObjArray count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count]){
//        
//		[self viewMoreResults]; // If cliecked "View More Result request to load next result
//	}
    
   // else {
		
		if([Network currentReachabilityStatus]==0)
		{
			UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error!",@"Error!") message:NSLocalizedString(@"Network is not available",@"Network is not available") delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
			[alert show];
			[alert release];
		}
		
		else {
			
			if([hotDealObj.apiPartnerId isEqualToString:@"4"])
				//If selected cell is dealmap
			{
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
				[defaults setValue:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdUrl] forKey:KEY_URL];
				WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
				[self.navigationController pushViewController:urlDetail animated:NO];
				[urlDetail release];
				
			}
			else
			{
                [defaults setObject:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdId] forKey:@"HotDealId"];
                [defaults setObject:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdName] forKey:@"HotDealProductName"];
                [defaults setObject:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdUrl] forKey:KEY_URL];
                [defaults setObject:hotDealObj.apiPartnerName forKey:@"apiName"];
                
                //@Deepak: adding hotDealListID to use in getDeal Button click for usertracking
				[defaults  setObject:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdListId] forKey:@"hotDealListID"];
				[defaults setObject:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdName] forKey:@"HotDealName"];
				[defaults setObject:[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdSalePrice] forKey:@"saleProductName"];
                
                iWebRequestType = hddetail;
				NSString *urlString = [NSString stringWithFormat:@"%@hotdeals/hddetail",BASE_URL];
                NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<HotDealsDetails><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
                [reqStr appendFormat:@"<hotDealId>%@</hotDealId><hotDealListId>%@</hotDealListId></HotDealsDetails>",[defaults valueForKey:@"HotDealId"] ,[defaults valueForKey:@"hotDealListID"]];
                
				[ConnectionManager establishConnectionFor:reqStr base:urlString withDelegate:self];
                [reqStr autorelease];
            }
            [hotDealTable deselectRowAtIndexPath:[hotDealTable indexPathForSelectedRow] animated:YES];
		}
	//}
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return [hotDealObjArray count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    HotDealDO *hotDealObj = [hotDealObjArray objectAtIndex:section];
    
    if (nextPage == 1 && section == ([hotDealObjArray count] - 1))
		return ([hotDealObj.hotDealDetailsObjArray count] + 1);
	else
		return [hotDealObj.hotDealDetailsObjArray count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell ; //= [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
	
    HotDealDO *hotDealObj = [hotDealObjArray objectAtIndex:indexPath.section];
    
    //to avoid a crash upon clicking mainmenu or back while scrolling
    /*if ([hotDealObj.hotDealDetailsObjArray[indexPath.row] count] == 0) {
		static NSString *CellIdentifier1 = @"Cell";
		UITableViewCell *cell1 ; //= [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
		cell1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
		return cell1;
	}*/
    
    float viewMoreLabelFont = 16.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        viewMoreLabelFont = 20.0;
    }
    
    //normal flow
	if (indexPath.section == ([hotDealObjArray count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count] ){
        if (nextPage == 1) {
            loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            loading.center = cell.contentView.center;
            loading.color = [UIColor blackColor];
            
            
            [cell.contentView addSubview:loading];
            [loading startAnimating];
            cell.userInteractionEnabled = NO;
//            CGRect frame;
//            frame.origin.x = 0;
//            frame.origin.y = 10;
//            frame.size.width = SCREEN_WIDTH;
//            frame.size.height = 24;
//            UILabel *label = [[UILabel alloc] initWithFrame:frame];
//            label.font = [UIFont boldSystemFontOfSize:viewMoreLabelFont];
//            label.textAlignment = NSTextAlignmentCenter;
//            //label.textColor = [UIColor colorWithRGBInt:0x112e72];
//            label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
//            [cell.contentView addSubview:label];
//            ReleaseAndNilify(label);
		}
		
		return cell;
    }
    
    else {
		
		UILabel *hdNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(75.0, 3, 230.0, 50.0)];
		
        if ([hotDealObjArray count] == 0) {
            ReleaseAndNilify(hdNameLabel);
            return NULL;
        }
        
        hdNameLabel.text = [[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdName];
		hdNameLabel.numberOfLines = 3;
        [hdNameLabel setLineBreakMode:NSLineBreakByWordWrapping];
		hdNameLabel.textColor = [UIColor colorWithRGBInt:0x112e72];
		hdNameLabel.font = [UIFont boldSystemFontOfSize:13];
		hdNameLabel.backgroundColor = [UIColor clearColor];
		[cell.contentView addSubview:hdNameLabel];
		ReleaseAndNilify(hdNameLabel);
        
        NSString *salePriceStr = [[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdSalePrice];
		if (![salePriceStr isEqualToString:@"N/A"] && ![salePriceStr isEqualToString:@"$0.00"])
        {
            
            UILabel *salePrice = [[UILabel alloc] initWithFrame:CGRectMake(170, 55, 150, 30)];
            salePrice.text = [NSString stringWithFormat:NSLocalizedString(@"Sale Price %@ ",@"Sale Price %@ "),salePriceStr];
            salePrice.textColor = [UIColor redColor];
            salePrice.font = [UIFont boldSystemFontOfSize:13];
            [cell.contentView addSubview:salePrice];
            ReleaseAndNilify(salePrice);
        }
		
		NSString *imgPath = [[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdImgPath];
		if (![imgPath isEqualToString:@"N/A"])
        {
            AsyncImageView *asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(5, 13, 60, 60)];
            asyncImageView.backgroundColor = [UIColor clearColor];
            [asyncImageView loadImage:imgPath];
            [cell addSubview:asyncImageView];
            ReleaseAndNilify(asyncImageView);
		}
        
        // Display New Image Icon for the New Hot Deal
		if ([[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdNewFlag] isEqualToString:@"1"])
        {
            AsyncImageView *asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
            asyncImageView.backgroundColor = [UIColor clearColor];
            [asyncImageView loadImagefrommainBundel:@"new_ribbon_70.png"];
            [cell addSubview:asyncImageView];
            ReleaseAndNilify(asyncImageView);
		}
		cell.backgroundColor = [UIColor whiteColor];
		cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        
        NSString *priceStr = [[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdPrice];
        if (![priceStr isEqualToString:@"N/A"] && ![priceStr isEqualToString:@"$0.00"])
        {
            UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(75, 55, 90, 30)];
            priceLabel.text = priceStr;
            priceLabel.textColor = [UIColor darkGrayColor];
            priceLabel.font = [UIFont boldSystemFontOfSize:13];
            priceLabel.backgroundColor = [UIColor clearColor];
            [cell addSubview:priceLabel];
            ReleaseAndNilify(priceLabel);
        }
		return cell;
	}
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HotDealDO *hotDealObj = [hotDealObjArray objectAtIndex:indexPath.section];
    if (indexPath.section == ([hotDealObjArray count] - 1) && indexPath.row == [hotDealObj.hotDealDetailsObjArray count] && nextPage == 1){
        return 60;
    }
    return 90;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if ([hotDealObjArray count] == 0 && (nextPage == 1 || lastVisitedRecord!=0)) {
        return 0.0;
    }
	//CGFloat val = 26;
	
	//for (int i =0 ; i < [sectionArry count]; i++) {
		//if ([[sectionArry objectAtIndex:i] intValue] == section)
			//val = 60 ;
	//}
	//return val;
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	
    if ([hotDealObjArray count] == 0 && (nextPage == 1 || lastVisitedRecord!=0)) {
        return NULL;
    }
	UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(10, 0, 320, 26)] autorelease];
	UILabel *catNameLabel = [[[UILabel alloc] initWithFrame:CGRectMake(20, 3, 300, 20)] autorelease];
	CGRect imageRect1 = CGRectMake(0.0f, 0.0f, 320.0f, 26.0f);
	
    HotDealDO *hdObj = [hotDealObjArray objectAtIndex:section];

    //	DLog(@"category = %@  ,section array = %@",categoryName ,sectionArry );
	
    //	for (int i =0 ; i < [sectionArry count]; i++)
    {
        //		if ([[sectionArry objectAtIndex:i] intValue] == section)
        {
            
			headerView.frame = CGRectMake(10, 0, 320, 60) ;
			imageRect1 = CGRectMake(0.0f, 30.0f, 320.0f, 30.0f);
			
			UILabel *apiNameLabel = [[[UILabel alloc] initWithFrame:CGRectMake(20, 5, 300, 20)] autorelease];
            //DLog(@"category = %@  ,section array = %@",categoryName ,sectionArry );
			apiNameLabel.text = [NSString stringWithFormat:@"%@",hdObj.apiPartnerName];
			apiNameLabel.textColor = [UIColor whiteColor];
			apiNameLabel.font = [UIFont boldSystemFontOfSize:16];
			apiNameLabel.backgroundColor = [UIColor clearColor];
			
			CGRect imageRect = CGRectMake(0.0f, 0.0f, 320.0f, 30.0f);
			UIImageView *headerImage = [[UIImageView alloc] initWithFrame:imageRect];
			UIImage *image = [UIImage imageNamed:@"hotDealsBg.png"];
			[headerImage setImage:image];
			headerImage.opaque = YES;
			[headerImage addSubview:apiNameLabel];
			
			[headerView addSubview:headerImage];
            [headerImage release];
		}
	}
	
	catNameLabel.text = [NSString stringWithFormat:@"%@",hdObj.catName];
	catNameLabel.textColor = [UIColor whiteColor];
	catNameLabel.font = [UIFont boldSystemFontOfSize:14];
	catNameLabel.backgroundColor = [UIColor lightGrayColor];
	
	UIImageView *headerImage1 = [[UIImageView alloc] initWithFrame:imageRect1];
	headerImage1.backgroundColor = [UIColor lightGrayColor];
	headerImage1.opaque = YES;
	[headerImage1 addSubview:catNameLabel];
	
	[headerView addSubview:headerImage1];
    [headerImage1 release];
	headerView.backgroundColor = [UIColor clearColor];
	return headerView;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    //	[super setEditing:YES animated:animated];
    
    /*   if (index.row == [rowNumberArray count])
     [hotDealTable setEditing:NO animated:YES];
     else
     [hotDealTable setEditing:editing animated:YES]; */
    
    HotDealDO *hotDealObj = [hotDealObjArray objectAtIndex:index.section];
    if ((index.section == [hotDealObjArray count] -1) && (index.row == [hotDealObj.hotDealDetailsObjArray count])) {
        [hotDealTable setEditing:NO animated:YES];
    }
    else
        [hotDealTable setEditing:YES animated:YES];
}

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    int sections = (int)[tableView numberOfSections];
    
    int rows = 0;
    
    for(int i=0; i < sections; i++)
    {
        rows += [tableView numberOfRowsInSection:i];
        DLog(@"%d",rows);
    }
    DLog(@"%d",rows);
    DLog(@"%ld",(long)indexPath.row);
    
    HotDealDO *hotDealObj = [hotDealObjArray objectAtIndex:indexPath.section];
    
    if((indexPath.section == [hotDealObjArray count] -1) && (indexPath.row == [hotDealObj.hotDealDetailsObjArray count])) {
        // any row that returns UITableViewCellEditingStyleNone will NOT support delete (in your case, the first row is returning this)
        return UITableViewCellEditingStyleNone;
    }
    
    // any row that returns UITableViewCellEditingStyleDelete will support delete (in your case, all but the first row is returning this)
    return UITableViewCellEditingStyleDelete;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    /*if (index.row == [rowNumberArray count]) {
     return;
     }  */
    
    HotDealDO *hotDealObj = [hotDealObjArray objectAtIndex:indexPath.section];
    
	if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSMutableString *xmlString = [[NSMutableString alloc] init];
        [xmlString appendFormat:@"<HotDealsListRequest><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        [xmlString appendFormat:@"<hotDealId>%@</hotDealId><hDInterested>0</hDInterested></HotDealsListRequest>",[[hotDealObj.hotDealDetailsObjArray objectAtIndex:indexPath.row]hdId]];
        
        NSString *urlString = [NSString stringWithFormat:@"%@hotdeals/removehdprod",BASE_URL];
        
        NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:xmlString];
        
        [xmlString release];
        
        if ([UtilityManager isNullOrEmptyString:responseXml]){
            return;
        }
        
        TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
        
        TBXMLElement *RTE = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        TBXMLElement *RCE = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        
        if([[TBXML textForElement:RCE] isEqualToString:@"10000"]){
            
            [hotDealObj.hotDealDetailsObjArray removeObjectAtIndex:indexPath.row];
            
            if([hotDealObj.hotDealDetailsObjArray count]<=0)
            {
                [hotDealObjArray removeObjectAtIndex:indexPath.section];
            }
            
            [hotDealTable reloadData];
        }
        else {
            NSString *resTxtStr = [TBXML textForElement:RTE];
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:resTxtStr
                                                           message:nil
                                                          delegate:self
                                                 cancelButtonTitle:NSLocalizedString(@"OK",@"OK")
                                                 otherButtonTitles:nil];
            
            [alert show];
            [alert release];
            return;
            
        }
	}
}

-(IBAction) categorybtnPressed : (id) sender {
    
    //if(favCat==1)
      //  m_Button.frame = CGRectMake(0, m_searchBar.frame.origin.y , m_Button.frame.size.width, m_Button.frame.size.height);
    
	[m_searchBar resignFirstResponder];
	
	if (customView.tag == 0) {
		customView.tag = 1 ;
		customView.hidden = NO;
	}else {
		customView.hidden = !customView.hidden ;
	}
	customView.tag = 1 ;
	[data_picker reloadAllComponents];
    [data_picker selectRow:pickerRow inComponent:0 animated:NO];
}


-(IBAction) galleryPressed : (id) sender
{
   /* GalleryList *galleryList = [[GalleryList alloc] initWithNibName:@"GalleryList" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:galleryList animated:NO];
    [galleryList release];*/
}

#pragma mark searchBar delegate methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    lastvalueforLoop = 0;
    [searchBar resignFirstResponder];
	lastVisitedRecord = 0;
    count = 0;
    tempLastVisitedRecord = 0;
    tempCount = 0;
    
    [SharedManager setRefreshCategories:YES];
    [self removeAllObjectsinArray];
	[self request_getHotDealProds];
    m_searchBar.text = @"";
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar;{
    
	[searchBar resignFirstResponder];
}

-(IBAction)doneClicked : (id) sender {
    
    if ([categoryArray count]>1)
        [SharedManager setRefreshCategories:NO];
    else
        [SharedManager setRefreshCategories:YES];
    //lastVisitedRecord = 0;
	customView.hidden = YES ;
    
    //count = 0;
    lastvalueforLoop = 0;
    DLog(@"%d,%@",pickerRow,[categoryId description]);
    categoryIdValue = [[categoryId objectAtIndex:pickerRow]intValue];
    
    // Removed all Objects, apart from Catagoryarray and catId
    [categoryName removeAllObjects];
    //[sectionArry removeAllObjects];
    [categoryIdHotdeals removeAllObjects];
	
	//remove all the objects from main array
    [hotDealObjArray removeAllObjects];
    
    
    if (!(categoryIdValue == 0)) {
        lastVisitedRecord = 0;
        count = 0;
    }
    else{
        lastVisitedRecord = tempLastVisitedRecord;
        count = tempCount;
    }
    
	[self request_getHotDealProds];
}

-(IBAction) favCategories : (id) sender {
    
    [self request_GetUserCat];
}


-(void)viewMoreResults{
    
    [SharedManager setRefreshCategories:YES];
    
    //means if the category is All then only capture the LVR and count value of it
	if (categoryIdValue == 0){
        tempLastVisitedRecord = lastVisitedRecord;
        tempCount = count;
    }
    
	[self request_getHotDealProds];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
	[super viewDidUnload];
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)dealloc {
    
    [self removeAllObjectsinArray];
    ReleaseAndNilify(hotDealObjArray);
    ReleaseAndNilify(categoryName);
    //ReleaseAndNilify(sectionArry);
    ReleaseAndNilify(categoryIdHotdeals);
    ReleaseAndNilify(categoryArray);
    ReleaseAndNilify(categoryId);
	[super dealloc];
}

-(void) removeAllObjectsinArray {
    [categoryId removeAllObjects];
    [categoryArray removeAllObjects];
    [categoryName removeAllObjects];
    //[sectionArry removeAllObjects];
    [categoryIdHotdeals removeAllObjects];
	[hotDealObjArray removeAllObjects];
}

-(void) viewWillDisappear:(BOOL)animated {
    
	[super viewWillDisappear:YES];
}

@end

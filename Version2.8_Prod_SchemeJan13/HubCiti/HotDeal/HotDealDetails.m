//
//  HotDealDetails.m
//  Scansee
//
//  Created by Dhananjaya C H on 22/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HotDealDetails.h"
#import "WebBrowserViewController.h"
#import "MainMenuViewController.h"
#import "RateShareViewController.h"
#import "EmailShareViewController.h"
#import "AppDelegate.h"
//#import "Config.h"
//#import "FTShare.h"
#import "AnyViewController.h"
#import <Twitter/Twitter.h>
//#import "HotDealRedeemViewController.h"


#define kAlertViewEmail 10000

@implementation HotDealDetails

@synthesize customView,url_Button,claimnredeemBuutonValue,btnGetDeal;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;

    startDate.font = [UIFont boldSystemFontOfSize:10];
    endDate.font = [UIFont boldSystemFontOfSize:10];
    
    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = mainPage;
    [mainPage release];
	
    //customize back button
    UIButton *backBtn = [UtilityManager customizeBackButton];
    
    [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = back;
    self.navigationItem.hidesBackButton = YES;
    [back release];
	   
//	[faceBookBtn setImage:[UIImage imageNamed:@"tab_btn_down_facebook.png"] forState:UIControlStateHighlighted];
//	[twitterBtn setImage:[UIImage imageNamed:@"tab_btn_down_twitter.png"] forState:UIControlStateHighlighted];
//	[sendText setImage:[UIImage imageNamed:@"tab_btn_down_sendtxt.png"] forState:UIControlStateHighlighted];
//	[sendMail setImage:[UIImage imageNamed:@"tab_btn_down_sendmail.png"] forState:UIControlStateHighlighted];
	
	hotDealDescription.delegate=self;
	prodIndex = 0;
	hotDealDescription.font = [UIFont systemFontOfSize:14];
	termsConditions.font = [UIFont systemFontOfSize:14];
    
    
    if (IS_IPHONE5)
    {
        lblPowerBy.frame = CGRectMake(lblPowerBy.frame.origin.x, lblPowerBy.frame.origin.y + 80, lblPowerBy.frame.size.width, lblPowerBy.frame.size.height);
        
        btnGetDeal.frame = CGRectMake(btnGetDeal.frame.origin.x, btnGetDeal.frame.origin.y + 80, btnGetDeal.frame.size.width, btnGetDeal.frame.size.height);
        
        url_Label.frame = CGRectMake(url_Label.frame.origin.x, url_Label.frame.origin.y + 80, url_Label.frame.size.width, url_Label.frame.size.height);
    
        hD_Desc.frame = CGRectMake(hD_Desc.frame.origin.x, hD_Desc.frame.origin.y, hD_Desc.frame.size.width, hD_Desc.frame.size.height+80);
        
    }
    
    [self request_hddetail];
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        [self setiPadView];
    }
}



-(void) viewWillAppear:(BOOL)animated{
	[super viewWillAppear:YES];
    
    if(claimnredeemBuutonValue==3)
    {
        btnGetDeal.enabled=NO;
    }
	if ([[HubCitiManager sharedManager] hotDealToolBar]== YES) {
		customView.hidden = NO;
        //	self.view.backgroundColor = [UIColor colorWithRGBInt:0xf57929];
	}else {
		customView.hidden = YES;
	}
	
}

-(void)setiPadView
{
    hotDealName.frame=CGRectMake(SCREEN_WIDTH/4, 30, SCREEN_WIDTH/2, 60);
    hotDealName.font = [UIFont systemFontOfSize:20];
    hotDealName.textAlignment=NSTextAlignmentCenter;
    
    deal.frame = CGRectMake((SCREEN_WIDTH/2)-40, 100, SCREEN_WIDTH/8, 40);
    regularPrice.frame = CGRectMake((SCREEN_WIDTH/2)-40, 150, SCREEN_WIDTH/8, 40);
    when.frame=CGRectMake((SCREEN_WIDTH/2)-40, 200, SCREEN_WIDTH/8, 40);
    deal.font=[UIFont systemFontOfSize:20];
    when.font=[UIFont systemFontOfSize:20];
    regularPrice.font=[UIFont systemFontOfSize:20];
    
    salePrice.frame = CGRectMake(3*(SCREEN_WIDTH/4)-40, 100, SCREEN_WIDTH/8, 40);
    price.frame=CGRectMake(3*(SCREEN_WIDTH/4)-40, 150, SCREEN_WIDTH/8, 40);
    startDate.frame = CGRectMake(3*(SCREEN_WIDTH/4)-40, 200, SCREEN_WIDTH/8, 40);
    endDate.frame = CGRectMake(3*(SCREEN_WIDTH/4)-40+(SCREEN_WIDTH/8)+40, 200, SCREEN_WIDTH/8, 40);
    seprator.frame=CGRectMake(3*(SCREEN_WIDTH/4)+(SCREEN_WIDTH/8)+8-40, 200, 24, 40);
    salePrice.font=[UIFont systemFontOfSize:15];
    price.font=[UIFont systemFontOfSize:15];
    startDate.font=[UIFont systemFontOfSize:15];
    endDate.font=[UIFont systemFontOfSize:15];
    seprator.font=[UIFont systemFontOfSize:15];
    
    details.frame=CGRectMake(30, 370, SCREEN_WIDTH/4, 40);
    details.font=[UIFont systemFontOfSize:20];
    
    btnLocation.frame=CGRectMake(3*(SCREEN_WIDTH/4)-40, 370, (SCREEN_WIDTH/4)-10, 40);
    [btnLocation setImage:[UIImage imageNamed:@"locations_iPad"] forState:UIControlStateNormal];
    hD_Desc.frame=CGRectMake(30,450, SCREEN_WIDTH-60, SCREEN_HEIGHT/4);
    
    
    btnGetDeal.frame=CGRectMake(30, 450+(SCREEN_HEIGHT/4)+10,SCREEN_WIDTH/4, 40);
    [btnGetDeal setImage:[UIImage imageNamed:@"getdeal_iPad"] forState:UIControlStateNormal];
    lblPowerBy.frame=CGRectMake(SCREEN_WIDTH/2, 450+(SCREEN_HEIGHT/4)+10, 100, 40);
    poweredBy.frame=CGRectMake(SCREEN_WIDTH/2, 450+(SCREEN_HEIGHT/4)+10, 100, 40);
    lblPowerBy.font =[UIFont systemFontOfSize:20];
    poweredBy.font=[UIFont systemFontOfSize:20];
    
    url_Label.frame=CGRectMake(SCREEN_WIDTH/2+110, 450+(SCREEN_HEIGHT/4)+10, 200, 40);
    
    

    
    
}

-(void)request_hddetail
{
//    iWebRequestState = HDDETAIL;
    
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<HotDealsDetails><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<hotDealId>%@</hotDealId>",[defaults valueForKey:@"HotDealId"]];
    if([defaults valueForKey:@"hotDealListID"])
        [requestStr appendFormat:@"<hotDealListId>%@</hotDealListId>",[defaults valueForKey:@"hotDealListID"]];
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></HotDealsDetails>",[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@hotdeals/hddetail",BASE_URL];
    
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    [requestStr release];

}

// Parse Hot Deal Details
-(void)parse_hddetail:(NSString *)responseXml
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
		return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];

        TBXMLElement *hotDealsDetailsArrayLstElement = [TBXML childElementNamed:@"hotDealsDetailsArrayLst" parentElement:tbxml.rootXMLElement];
         TBXMLElement *hotDealDetailElement = [TBXML childElementNamed:@"HotDealsDetails" parentElement:hotDealsDetailsArrayLstElement];
        if(hotDealDetailElement)
        {
            TBXMLElement *apiLogoElement = [TBXML childElementNamed:@"apiPartnerImagePath" parentElement:hotDealDetailElement];
            TBXMLElement *productHotDealIDElement = [TBXML childElementNamed:@"productHotDealId" parentElement:hotDealDetailElement];
            
            TBXMLElement *cityElement = [TBXML childElementNamed:@"city" parentElement:hotDealDetailElement];
            
            TBXMLElement *hotDealNameElement = [TBXML childElementNamed:@"hotDealName" parentElement:hotDealDetailElement];
            TBXMLElement *hotDealLongDescriptionElement = [TBXML childElementNamed:@"hDLognDescription" parentElement:hotDealDetailElement];
            TBXMLElement *imagePathElement = [TBXML childElementNamed:@"hotDealImagePath" parentElement:hotDealDetailElement];
            TBXMLElement *hotDealSaleElement = [TBXML childElementNamed:@"hDSalePrice" parentElement:hotDealDetailElement];
            TBXMLElement *hotDealSalePriceElement = [TBXML childElementNamed:@"hDPrice" parentElement:hotDealDetailElement];
            TBXMLElement *hotDealStartDateElement = [TBXML childElementNamed:@"hDStartDate" parentElement:hotDealDetailElement];
            
            
            TBXMLElement *hotDealEndDateElement = [TBXML childElementNamed:@"hDEndDate" parentElement:hotDealDetailElement];
            TBXMLElement *hotDealUrlElement = [TBXML childElementNamed:@"hdURL" parentElement:hotDealDetailElement];
            
            // New Tags for Promotions
            TBXMLElement *hotDealextFlagElement = [TBXML childElementNamed:@"extFlag" parentElement:hotDealDetailElement];
            
            TBXMLElement *hotDealclaimFlagElement = [TBXML childElementNamed:@"claimFlag" parentElement:hotDealDetailElement];
            
            TBXMLElement *hotDealredeemFlagElement = [TBXML childElementNamed:@"redeemFlag" parentElement:hotDealDetailElement];
            
            TBXMLElement *hotDealuserNameElement = [TBXML childElementNamed:@"userName" parentElement:hotDealDetailElement];
            
            TBXMLElement *hotDealretLocIDsElement = [TBXML childElementNamed:@"retLocIDs" parentElement:hotDealDetailElement];
            
            TBXMLElement *hotDealretLocsElement = [TBXML childElementNamed:@"retLocs" parentElement:hotDealDetailElement];
            
            TBXMLElement *hDExpDateElement = [TBXML childElementNamed:@"hDExpDate" parentElement:hotDealDetailElement];
            
            TBXMLElement *hDcoupCodeElement = [TBXML childElementNamed:@"coupCode" parentElement:hotDealDetailElement];
            
            TBXMLElement *hretNameElement = [TBXML childElementNamed:@"retName" parentElement:hotDealDetailElement];
            
            claimnredeemBuutonValue = 0;
            // Internal Deal
            if (hotDealextFlagElement!=nil && [[TBXML textForElement:hotDealextFlagElement] isEqualToString:@"0"])
            {
                // If no location associated to deal/retailer then will not show location button
                if(hotDealretLocsElement)
                    btnLocation.hidden = NO;
                else
                    btnLocation.hidden = YES;
                
                if ([[TBXML textForElement:hotDealclaimFlagElement] isEqualToString:@"1"] && [[TBXML textForElement:hotDealredeemFlagElement] isEqualToString:@"1"])
                {
                    // Redeem button will be deactive as its claimed and reemed both
                    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                        [btnGetDeal setImage:[UIImage imageNamed:@"redeem.png"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [btnGetDeal setImage:[UIImage imageNamed:@"redeemiPad.png"] forState:UIControlStateNormal];
                    }
                    claimnredeemBuutonValue = 3;
                    btnGetDeal.enabled=NO;
                }
                else if ([[TBXML textForElement:hotDealclaimFlagElement] isEqualToString:@"1"] && [[TBXML textForElement:hotDealredeemFlagElement] isEqualToString:@"0"])
                {
                    // Redeem button will be active as its claimed but not redeemed
                    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                        [btnGetDeal setImage:[UIImage imageNamed:@"redeem.png"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [btnGetDeal setImage:[UIImage imageNamed:@"redeemiPad.png"] forState:UIControlStateNormal];
                    }
                    
                    claimnredeemBuutonValue = 2;
                }
                else if ([[TBXML textForElement:hotDealclaimFlagElement] isEqualToString:@"0"] && [[TBXML textForElement:hotDealredeemFlagElement] isEqualToString:@"0"])
                {
                    // Claim button will be active as its not claimed and not redeemed too
                    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                        [btnGetDeal setImage:[UIImage imageNamed:@"claim.png"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [btnGetDeal setImage:[UIImage imageNamed:@"claimiPad.png"] forState:UIControlStateNormal];
                    }
                    
                    claimnredeemBuutonValue = 1;
                }
                
            }
            else // External Deal
                btnLocation.hidden = YES;
            
            //hide the get deal button for expired deals
            TBXMLElement *expiredElement = [TBXML childElementNamed:@"expired" parentElement:hotDealDetailElement];
            
            if (expiredElement!=nil && [[TBXML textForElement:expiredElement]boolValue] == YES){
                
                btnGetDeal.hidden = YES;
            }
            else
                btnGetDeal.hidden = NO;
            
            if(hotDealretLocIDsElement)
                retLocIDs = [[TBXML textForElement:hotDealretLocIDsElement]copy];
            else
                retLocIDs = [[NSString alloc]initWithString:@"N/A"];
            
            
            if(hretNameElement)
                retName = [[TBXML textForElement:hretNameElement]copy];
            else
                retName = [[NSString alloc]initWithString:@"N/A"];
            
            if(hotDealretLocsElement)
                retLocs = [[TBXML textForElement:hotDealretLocsElement]copy];
            else
                retLocs = [[NSString alloc]initWithString:@"N/A"];
            
            if(hotDealextFlagElement)
                extFlagValue = [[TBXML textForElement:hotDealextFlagElement]copy];
            else
                extFlagValue = [[NSString alloc]initWithString:@"N/A"];
            
            if(hotDealredeemFlagElement)
                redeemFlagValue = [[TBXML textForElement:hotDealredeemFlagElement]copy];
            else
                redeemFlagValue = [[NSString alloc]initWithString:@"N/A"];
            
            if(hotDealclaimFlagElement)
                claimFlagValue = [[TBXML textForElement:hotDealclaimFlagElement]copy];
            else
                claimFlagValue = [[NSString alloc]initWithString:@"N/A"];
            
            if(hotDealuserNameElement)
                username = [[TBXML textForElement:hotDealuserNameElement]copy];
            else
                username = [[NSString alloc]initWithString:@"N/A"];
            
            if(hDExpDateElement)
                hDExpDate = [[TBXML textForElement:hDExpDateElement]copy];
            else
                hDExpDate = [[NSString alloc]initWithString:@"N/A"];
            
            if(hDcoupCodeElement)
                coupCode = [[TBXML textForElement:hDcoupCodeElement]copy];
            else
                coupCode = [[NSString alloc]initWithString:@"N/A"];
            
            if(hotDealStartDateElement)
                startDateString = [[TBXML textForElement:hotDealStartDateElement]copy];
            else
                startDateString = [[NSString alloc]initWithString:@"N/A"];
            
            if(hotDealEndDateElement)
                endDateString = [[TBXML textForElement:hotDealEndDateElement]copy];
            else
                endDateString = [[NSString alloc]initWithString:@"N/A"];
            
            if(hotDealLongDescriptionElement)
                description = [[TBXML textForElement:hotDealLongDescriptionElement]copy];
            
            if (hotDealUrlElement)
                url = [[TBXML textForElement:hotDealUrlElement]copy];
            
            if (apiLogoElement != nil)
                apiLogoUrl = [[TBXML textForElement:apiLogoElement] copy];
            DLog(@"url:%@", url);
            [defaults setObject:url forKey:KEY_HYPERLINK];
            
            if (cityElement!=nil && ![[TBXML textForElement:cityElement]isEqualToString:@"N/A"])
            {
                UILabel *cityLabel = [[UILabel alloc]initWithFrame:CGRectMake(135.0, 160.0, 185.0, 20.0)];
                cityLabel.textAlignment = NSTextAlignmentLeft;
                cityLabel.textColor = [UIColor blackColor];
                cityLabel.backgroundColor = [UIColor clearColor];
                //cityLabel.numberOfLines = 2;
                cityLabel.font = [UIFont boldSystemFontOfSize:13];
                cityLabel.adjustsFontSizeToFitWidth = YES;
                cityLabel.minimumScaleFactor = 10.0;
                cityLabel.text = [NSString stringWithFormat:@"City: %@",[TBXML textForElement:cityElement]];
                [self.view addSubview:cityLabel];
                ReleaseAndNilify(cityLabel);
            }
            
            
            if (hotDealNameElement && ![[TBXML textForElement:hotDealNameElement] isEqualToString:@"N/A"]){
                hotDealName.text = [[[TBXML textForElement:hotDealNameElement] copy] autorelease];
                hotDealName.textColor = [UIColor colorWithRGBInt:0x112e72];
            }
            if (hotDealSaleElement && ![[TBXML textForElement:hotDealSaleElement] isEqualToString:@"N/A"])
                salePrice.text = [[[TBXML textForElement:hotDealSaleElement] copy] autorelease];
            if (hotDealSalePriceElement && ![[TBXML textForElement:hotDealSalePriceElement] isEqualToString:@"N/A"])
                price.text = [[[TBXML textForElement:hotDealSalePriceElement] copy] autorelease];
            
            NSString *str = @"N/A";
            if(hotDealLongDescriptionElement)
                str = [[[TBXML textForElement:hotDealLongDescriptionElement] copy] autorelease];
            
            if (![startDateString isEqualToString:@"N/A"])
                startDate.text = [[startDateString copy] autorelease];
            if (![endDateString isEqualToString:@"N/A"])
            {
                seprator.hidden=NO;
                endDate.text = [[endDateString copy] autorelease];
            }
            else
                seprator.hidden=YES;
            
            imageStringValue= @"N/A";
            if(imagePathElement)
                imageStringValue = [[TBXML textForElement:imagePathElement] copy];
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                  [hD_Desc loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#0000;'>%@",str] baseURL:nil];
            }
            else
            {
                  [hD_Desc loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:20px;font-family:Helvetica;color:#0000;'>%@",str] baseURL:nil];
            }
          
            hD_Desc.backgroundColor = [UIColor clearColor];
            hD_Desc.layer.cornerRadius = 5.0;
            hD_Desc.layer.borderWidth = 1.0f;
            hD_Desc.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
            [[hD_Desc.subviews objectAtIndex:0] setBounces:NO];
            
            hotDealId = 0;
            if(productHotDealIDElement)
                hotDealId = [[TBXML textForElement:productHotDealIDElement]copy];
            
            //customize the title
            UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 320, 40)];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.textColor = [UIColor whiteColor];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.numberOfLines = 2;
            [titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
            titleLabel.font = [UIFont boldSystemFontOfSize:16];
            titleLabel.text = [NSString stringWithFormat:@"%@",hotDealName.text];
            self.navigationItem.titleView = titleLabel;
            [titleLabel release];
            
            //frame the image
            CGRect frame;
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                frame.origin.x = 10;
                frame.origin.y = 70;
                frame.size.width = 108;
                frame.size.height = 100;
            }
            else
            {
                frame.origin.x = 30;
                frame.origin.y = 100;
                frame.size.width = 2*SCREEN_WIDTH/6;
                frame.size.height =  2*SCREEN_WIDTH/6;
            }
           
            
            DLog(@" image path = %@ ",imageStringValue);
            //NSURL *imageUrl = [NSURL URLWithString:imageStringValue];
            AsyncImageView *asyncImageView = [[AsyncImageView alloc] initWithFrame:frame ];
            asyncImageView.backgroundColor = [UIColor clearColor];
            asyncImageView.layer.shadowColor = [UIColor blackColor].CGColor;
            asyncImageView.layer.shadowOffset = CGSizeMake(0, 1);
            asyncImageView.layer.shadowOpacity = 1;
            asyncImageView.layer.shadowRadius = 5.0;
            asyncImageView.clipsToBounds = NO;
            [asyncImageView loadImage:imageStringValue];
            [self.view addSubview:asyncImageView];
            [asyncImageView release];
            
            if (![apiLogoUrl isEqualToString:@"N/A"]) {
                
                url_Label.hidden = YES;
                
                CGRect logoframe = CGRectMake(url_Label.frame.origin.x, url_Label.frame.origin.y, url_Label.frame.size.width, url_Label.frame.size.height);
                AsyncImageView *asyncImageView1 = [[AsyncImageView alloc] initWithFrame:logoframe ];
                asyncImageView1.backgroundColor = [UIColor clearColor];
                [asyncImageView1 loadImage:apiLogoUrl];
                [self.view addSubview:asyncImageView1];
                [asyncImageView1 release];
            }
            else
            {
                if ([defaults  objectForKey:@"apiName"]) {
                    url_Label.text = [NSString stringWithFormat:@"%@",[defaults valueForKey:@"apiName"]];
                    url_Label.hidden = NO;
                }
                else{
                    
                    poweredBy.hidden = YES;
                    url_Label.hidden = YES;
                }
            }
            
            
        }
            
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }

}

// handle server respobse Here
-(void)responseData:(NSString *) response
{
//    switch (iWebRequestState)
//    {
//            case HDDETAIL:
            [self parse_hddetail:response];
//            break;
//    }
}


-(IBAction)locationButtonClicked:(id)sender
{
    if(iRetailerLocationsViewController)
    {
        [iRetailerLocationsViewController release];
        iRetailerLocationsViewController = nil;
    }
    
    iRetailerLocationsViewController = [[RetailerLocationsViewController alloc]initWithNibName:@"RetailerLocationsViewController" bundle:[NSBundle mainBundle]];
    if (IS_IPHONE5)
        iRetailerLocationsViewController.view.frame = CGRectMake(10, 10, 300, 425);
    else
        iRetailerLocationsViewController.view.frame = CGRectMake(10, 10, 300, 350);
    
    iRetailerLocationsViewController.view.layer.backgroundColor = [UIColor whiteColor].CGColor;
    iRetailerLocationsViewController.view.layer.cornerRadius = 20.0;
    iRetailerLocationsViewController.view.layer.borderColor = [UIColor blackColor].CGColor;
    iRetailerLocationsViewController.view.layer.shadowOffset = CGSizeMake(1, 0);
    iRetailerLocationsViewController.view.layer.shadowColor = [[UIColor blackColor] CGColor];
    iRetailerLocationsViewController.view.layer.shadowRadius = 5;
    iRetailerLocationsViewController.view.layer.shadowOpacity = .25;
    
    iRetailerLocationsViewController.strDealName = [[NSString alloc]initWithString:hotDealName.text];
    iRetailerLocationsViewController.arrLocations = [[NSArray alloc]initWithArray:[retLocs componentsSeparatedByString:@"|"]];
    iRetailerLocationsViewController.strRetailerName = [[NSString alloc]initWithString:retName];
    
    [self.view addSubview:iRetailerLocationsViewController.view];
    
}

-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

/*
-(void) parseHotDealDetailXml {
    
	NSString *responseXml = [NSString stringWithFormat:@"%@", [defaults  valueForKey:KEY_RESPONSEXML]]; //modified by pkm
	
	TBXML *tbxml = [[TBXML tbxmlWithXMLString:responseXml] retain];
	TBXMLElement *hotDealDetailElement = [TBXML childElementNamed:@"HotDealsDetails" parentElement:tbxml.rootXMLElement];
	if (hotDealDetailElement == nil) {
		TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
		NSString *responseTextStr = [TBXML textForElement:responseTextElement];
		
		// Banner the message given by the server
		UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil 
													   message:responseTextStr 
													  delegate:self 
											 cancelButtonTitle:NSLocalizedString(@"OK",@"OK")
											 otherButtonTitles:nil];
		[alert show];
		[alert release];
		
		[tbxml release];	
		return;
	}
	else {
		TBXMLElement *apiLogoElement = [TBXML childElementNamed:@"apiPartnerImagePath" parentElement:hotDealDetailElement];
		TBXMLElement *productHotDealIDElement = [TBXML childElementNamed:@"productHotDealID" parentElement:hotDealDetailElement];

        TBXMLElement *cityElement = [TBXML childElementNamed:@"city" parentElement:hotDealDetailElement];

		TBXMLElement *hotDealNameElement = [TBXML childElementNamed:@"hotDealName" parentElement:hotDealDetailElement];
		TBXMLElement *hotDealLongDescriptionElement = [TBXML childElementNamed:@"hDLognDescription" parentElement:hotDealDetailElement];
		TBXMLElement *imagePathElement = [TBXML childElementNamed:@"hotDealImagePath" parentElement:hotDealDetailElement];
		TBXMLElement *hotDealSaleElement = [TBXML childElementNamed:@"hDSalePrice" parentElement:hotDealDetailElement];
		TBXMLElement *hotDealSalePriceElement = [TBXML childElementNamed:@"hDPrice" parentElement:hotDealDetailElement];
		TBXMLElement *hotDealStartDateElement = [TBXML childElementNamed:@"hDStartDate" parentElement:hotDealDetailElement];
		
		
		TBXMLElement *hotDealEndDateElement = [TBXML childElementNamed:@"hDEndDate" parentElement:hotDealDetailElement];
		TBXMLElement *hotDealUrlElement = [TBXML childElementNamed:@"hdURL" parentElement:hotDealDetailElement];
        
        // New Tags for Promotions
        TBXMLElement *hotDealextFlagElement = [TBXML childElementNamed:@"extFlag" parentElement:hotDealDetailElement];
        
        TBXMLElement *hotDealclaimFlagElement = [TBXML childElementNamed:@"claimFlag" parentElement:hotDealDetailElement];
        
        TBXMLElement *hotDealredeemFlagElement = [TBXML childElementNamed:@"redeemFlag" parentElement:hotDealDetailElement];
        
        TBXMLElement *hotDealuserNameElement = [TBXML childElementNamed:@"userName" parentElement:hotDealDetailElement];
        
        TBXMLElement *hotDealretLocIDsElement = [TBXML childElementNamed:@"retLocIDs" parentElement:hotDealDetailElement];
        
        TBXMLElement *hotDealretLocsElement = [TBXML childElementNamed:@"retLocs" parentElement:hotDealDetailElement];
        
        TBXMLElement *hDExpDateElement = [TBXML childElementNamed:@"hDExpDate" parentElement:hotDealDetailElement];
        
        TBXMLElement *hDcoupCodeElement = [TBXML childElementNamed:@"coupCode" parentElement:hotDealDetailElement];
        
        TBXMLElement *hretNameElement = [TBXML childElementNamed:@"retName" parentElement:hotDealDetailElement];
        
        
        claimnredeemBuutonValue = 0;
        // Internal Deal
        if (hotDealextFlagElement!=nil && [[TBXML textForElement:hotDealextFlagElement] isEqualToString:@"0"])
        {
            // If no location associated to deal/retailer then will not show location button
            if(hotDealretLocsElement)
                btnLocation.hidden = NO;
            else
                btnLocation.hidden = YES;
            
            if ([[TBXML textForElement:hotDealclaimFlagElement] isEqualToString:@"1"] && [[TBXML textForElement:hotDealredeemFlagElement] isEqualToString:@"1"])
            {
                // Redeem button will be deactive as its claimed and reemed both
                [btnGetDeal setImage:[UIImage imageNamed:@"redeem.png"] forState:UIControlStateNormal];
                claimnredeemBuutonValue = 3;
                btnGetDeal.enabled=NO;
            }
            else if ([[TBXML textForElement:hotDealclaimFlagElement] isEqualToString:@"1"] && [[TBXML textForElement:hotDealredeemFlagElement] isEqualToString:@"0"])
            {
                // Redeem button will be active as its claimed but not redeemed
                [btnGetDeal setImage:[UIImage imageNamed:@"redeem.png"] forState:UIControlStateNormal];
                claimnredeemBuutonValue = 2;
            }
            else if ([[TBXML textForElement:hotDealclaimFlagElement] isEqualToString:@"0"] && [[TBXML textForElement:hotDealredeemFlagElement] isEqualToString:@"0"])
            {
                // Claim button will be active as its not claimed and not redeemed too
                [btnGetDeal setImage:[UIImage imageNamed:@"claim.png"] forState:UIControlStateNormal];
                claimnredeemBuutonValue = 1;
            }

        }
        else // External Deal
            btnLocation.hidden = YES;
        
        if(hotDealretLocIDsElement)
            retLocIDs = [[TBXML textForElement:hotDealretLocIDsElement]copy];
        else
            retLocIDs = [[NSString alloc]initWithString:@"N/A"];

        
        if(hretNameElement)
            retName = [[TBXML textForElement:hretNameElement]copy];
        else
            retName = [[NSString alloc]initWithString:@"N/A"];
        
        if(hotDealretLocsElement)
            retLocs = [[TBXML textForElement:hotDealretLocsElement]copy];
        else
            retLocs = [[NSString alloc]initWithString:@"N/A"];
        
        if(hotDealextFlagElement)
            extFlagValue = [[TBXML textForElement:hotDealextFlagElement]copy];
        else
            extFlagValue = [[NSString alloc]initWithString:@"N/A"];
        
        if(hotDealredeemFlagElement)
            redeemFlagValue = [[TBXML textForElement:hotDealredeemFlagElement]copy];
        else
            redeemFlagValue = [[NSString alloc]initWithString:@"N/A"];
        
        if(hotDealclaimFlagElement)
            claimFlagValue = [[TBXML textForElement:hotDealclaimFlagElement]copy];
        else
            claimFlagValue = [[NSString alloc]initWithString:@"N/A"];
        
        if(hotDealuserNameElement)
            username = [[TBXML textForElement:hotDealuserNameElement]copy];
        else
            username = [[NSString alloc]initWithString:@"N/A"];
        
        if(hDExpDateElement)
            hDExpDate = [[TBXML textForElement:hDExpDateElement]copy];
        else
            hDExpDate = [[NSString alloc]initWithString:@"N/A"];
        
        if(hDcoupCodeElement)
            coupCode = [[TBXML textForElement:hDcoupCodeElement]copy];
        else
            coupCode = [[NSString alloc]initWithString:@"N/A"];
        
        if(hotDealStartDateElement)
            startDateString = [[TBXML textForElement:hotDealStartDateElement]copy];
        else
            startDateString = [[NSString alloc]initWithString:@"N/A"];
        
        if(hotDealEndDateElement)
            endDateString = [[TBXML textForElement:hotDealEndDateElement]copy];
        else
            endDateString = [[NSString alloc]initWithString:@"N/A"];
        
        if(hotDealLongDescriptionElement)
            description = [[TBXML textForElement:hotDealLongDescriptionElement]copy];
        
        if (hotDealUrlElement)
		url = [[TBXML textForElement:hotDealUrlElement]copy];
        
		if (apiLogoElement != nil)
			apiLogoUrl = [[TBXML textForElement:apiLogoElement] copy];
		DLog(@"url:%@", url);
		[defaults setObject:url forKey:KEY_HYPERLINK];
        
        if (cityElement!=nil && ![[TBXML textForElement:cityElement]isEqualToString:@"N/A"])
        {
            UILabel *cityLabel = [[UILabel alloc]initWithFrame:CGRectMake(135.0, 160.0, 185.0, 20.0)];
            cityLabel.textAlignment = NSTextAlignmentLeft;
            cityLabel.textColor = [UIColor blackColor];
            cityLabel.backgroundColor = [UIColor clearColor];
            //cityLabel.numberOfLines = 2;
            cityLabel.font = [UIFont boldSystemFontOfSize:13];
            cityLabel.adjustsFontSizeToFitWidth = YES;
            cityLabel.minimumScaleFactor = 10.0;
            cityLabel.text = [NSString stringWithFormat:@"City: %@",[TBXML textForElement:cityElement]];
            [self.view addSubview:cityLabel];
            ReleaseAndNilify(cityLabel);
        }
            
        
        if (![[TBXML textForElement:hotDealNameElement] isEqualToString:@"N/A"]){
		hotDealName.text = [[[TBXML textForElement:hotDealNameElement] copy] autorelease];
            hotDealName.textColor = [UIColor colorWithRGBInt:0x112e72];
        }
        if (![[TBXML textForElement:hotDealSaleElement] isEqualToString:@"N/A"])
		salePrice.text = [[[TBXML textForElement:hotDealSaleElement] copy] autorelease];
        if (![[TBXML textForElement:hotDealSalePriceElement] isEqualToString:@"N/A"])
		price.text = [[[TBXML textForElement:hotDealSalePriceElement] copy] autorelease];
		
        NSString *str = [[[TBXML textForElement:hotDealLongDescriptionElement] copy] autorelease];
        
        if (![startDateString isEqualToString:@"N/A"])
            startDate.text = [[startDateString copy] autorelease];
         if (![endDateString isEqualToString:@"N/A"])
         {
             seprator.hidden=NO;
             endDate.text = [[endDateString copy] autorelease];
         }
        else
            seprator.hidden=YES;
		
        //imageStringValue = [[[TBXML textForElement:imagePathElement] copy] autorelease];
        imageStringValue = [[TBXML textForElement:imagePathElement] copy];
		//	url_Label.text = [NSString stringWithFormat:@"%@",[defaults valueForKey:@"apiName"]];
        
		[hD_Desc loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#0000;'>%@",str] baseURL:nil];
		hD_Desc.backgroundColor = [UIColor clearColor];
        hD_Desc.layer.cornerRadius = 5.0;
        hD_Desc.layer.borderWidth = 1.0f;
        hD_Desc.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
		[[hD_Desc.subviews objectAtIndex:0] setBounces:NO];
	//	DLog(@"productDescription = %@ ",[prodDescStringValue copy]);
//		[prodDescStringValue release];
		
		hotDealId = [[TBXML textForElement:productHotDealIDElement]copy];
		
       //customize the title 
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 320, 40)];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.numberOfLines = 2;
        [titleLabel setLineBreakMode:NSLineBreakModeWordWrap];
        titleLabel.font = [UIFont boldSystemFontOfSize:16];
        titleLabel.text = [NSString stringWithFormat:@"%@",hotDealName.text];
        self.navigationItem.titleView = titleLabel;
        [titleLabel release];
        
        //frame the image 
		CGRect frame;
		frame.origin.x = 10;
		frame.origin.y = 70;
		frame.size.width = 108;
		frame.size.height = 100;
		
		DLog(@" image path = %@ ",imageStringValue);
		//NSURL *imageUrl = [NSURL URLWithString:imageStringValue];
		AsyncImageView *asyncImageView = [[AsyncImageView alloc] initWithFrame:frame ];
		asyncImageView.backgroundColor = [UIColor clearColor];
        asyncImageView.layer.shadowColor = [UIColor blackColor].CGColor;
        asyncImageView.layer.shadowOffset = CGSizeMake(0, 1);
        asyncImageView.layer.shadowOpacity = 1;
        asyncImageView.layer.shadowRadius = 5.0;
        asyncImageView.clipsToBounds = NO;
        [asyncImageView loadImage:imageStringValue];
		[self.view addSubview:asyncImageView];
		[asyncImageView release];
		
		if (![apiLogoUrl isEqualToString:@"N/A"]) {
            
            url_Label.hidden = YES;
            
			CGRect logoframe = CGRectMake(url_Label.frame.origin.x, url_Label.frame.origin.y, url_Label.frame.size.width, url_Label.frame.size.height);
			AsyncImageView *asyncImageView1 = [[AsyncImageView alloc] initWithFrame:logoframe ];
			asyncImageView1.backgroundColor = [UIColor clearColor];
            [asyncImageView1 loadImage:apiLogoUrl];
			[self.view addSubview:asyncImageView1];
			[asyncImageView1 release];
		}
		else {
            if ([defaults  objectForKey:@"apiName"]) {
                url_Label.text = [NSString stringWithFormat:@"%@",[defaults valueForKey:@"apiName"]];
                url_Label.hidden = NO;
            }
            else{
                
                poweredBy.hidden = YES;
                url_Label.hidden = YES;
            }
		//	url_Label.text = [NSString stringWithFormat:@"%@",[defaults valueForKey:@"apiName"]];
		}
		
		[tbxml release];	
	}
}
*/

- (void)facebookButtonPressed:(id)sender {
    
    /*[SharedManager  setShareEmailCoupon:NO];
    [SharedManager setShareEmailHotDeal:YES];
    [SharedManager setShareEmailSpecialOffer:NO];
    [SharedManager setShareEmailSpecialOfferUrl:NO];
    [SharedManager setShareAppsite:NO];//for UT*/
    
	NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharehotdealinfo?hotdealId=%@",BASE_URL,hotDealId];    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];

	if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]){
        ReleaseAndNilify(responseXml);
        return;
	}
	TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
	
	TBXMLElement *HotDealsDetailsElement = [TBXML childElementNamed:@"HotDealsDetails" parentElement:tbXml.rootXMLElement];
	if (HotDealsDetailsElement != nil) {
		TBXMLElement *titleElement1 = [TBXML childElementNamed:@"titleText" parentElement:HotDealsDetailsElement];
		TBXMLElement *titleElement2 = [TBXML childElementNamed:@"titleText2" parentElement:HotDealsDetailsElement];
		TBXMLElement *hotDealNameElement = [TBXML childElementNamed:@"hotDealName" parentElement:HotDealsDetailsElement];
		TBXMLElement *hdURLElement = [TBXML childElementNamed:@"hdURL" parentElement:HotDealsDetailsElement];
		
		
		NSMutableString *shareContent = [NSMutableString stringWithFormat:@"%@\n%@\n",[TBXML textForElement:titleElement1],[TBXML textForElement:titleElement2]];
		[shareContent appendFormat:@"%@\n",[[[TBXML textForElement:hotDealNameElement]copy] autorelease]];
		
//		if (hdURLElement != nil){
//			if (![[[[TBXML textForElement:hdURLElement]copy] autorelease]isEqualToString:@"N/A"])
//				[shareContent appendFormat:@"URL:%@\n",[[[TBXML textForElement:hdURLElement]copy] autorelease]];
//		}
		
		NSString *productImagePath = [NSString stringWithFormat:@"%@",imageStringValue];
		DLog(@"Share Content:%@",shareContent);
		DLog(@"Product Image Path:%@",productImagePath);
		
		[defaults   setObject:shareContent forKey:KEY_SHAREMSG];
		[defaults   setObject:productImagePath forKey:KEY_PRODUCTIMGPATH];
        [defaults   setObject:[TBXML textForElement:hdURLElement] forKey:KEY_HYPERLINK];
        
		//[[ScanseeManager sharedManager]setShareFromTL:NO];
    // [self.navigationController pushViewController:[[[AnyViewController alloc] initWithNibName:nil bundle:nil] autorelease] animated:YES];
        AnyViewController *anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
        //[self.view addSubview:anyVC.view];
        [UIView transitionWithView:self.view duration:0.5
                           options:UIViewAnimationOptionTransitionCurlDown
                        animations:^ { [self.view addSubview:anyVC.view]; }
                        completion:nil];
	}
	
	else {
		TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbXml.rootXMLElement];
		int respCode = [[TBXML textForElement:responseCodeElement] intValue];
		if ((responseCodeElement != nil) && (respCode == 10001)) {
			TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbXml.rootXMLElement];
			NSString *str = [[[TBXML textForElement:responseTextElement]copy]autorelease];
			[UtilityManager showAlert:@"" msg:str];
		}
	}
    ReleaseAndNilify(responseXml);
}

- (void)twitterButtonPressed:(id)sender
{
	
   /* [SharedManager  setShareEmailCoupon:NO];
    [SharedManager setShareEmailHotDeal:YES];
    [SharedManager setShareEmailSpecialOffer:NO];
    [SharedManager setShareEmailSpecialOfferUrl:NO];
    [SharedManager setShareAppsite:NO];//for UT*/
    
	NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharehotdealinfo?hotdealId=%@",BASE_URL,hotDealId];
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];

	if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]){
        ReleaseAndNilify(responseXml);
        return;
	}
	TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
	
	TBXMLElement *HotDealsDetailsElement = [TBXML childElementNamed:@"HotDealsDetails" parentElement:tbXml.rootXMLElement];
	if (HotDealsDetailsElement != nil) {
		
		TBXMLElement *titleElement1 = [TBXML childElementNamed:@"titleText" parentElement:HotDealsDetailsElement];
		TBXMLElement *titleElement2 = [TBXML childElementNamed:@"titleText2" parentElement:HotDealsDetailsElement];
		TBXMLElement *hotDealNameElement = [TBXML childElementNamed:@"hotDealName" parentElement:HotDealsDetailsElement];
	//	TBXMLElement *hdURLElement = [TBXML childElementNamed:@"hdURL" parentElement:HotDealsDetailsElement];
		
		
		NSMutableString *shareContent = [NSMutableString stringWithFormat:@"%@\n%@\n",[TBXML textForElement:titleElement1],[TBXML textForElement:titleElement2]];

        NSString *str = [[[TBXML textForElement:hotDealNameElement]copy] autorelease];
		[shareContent appendFormat:@"%@\n",str];
		
	/*	if (hdURLElement != nil){
            NSString *str = [TBXML textForElement:hdURLElement];
			if (![str isEqualToString:@"N/A"])
				[shareContent appendFormat:@"Visit:%@\n",str];
		} */
    
		NSString *productImagePath = [NSString stringWithFormat:@"%@",imageStringValue];
		DLog(@"Share Content:%@",shareContent);
		DLog(@"Product Image Path:%@",productImagePath);
		
		[defaults   setObject:shareContent forKey:KEY_SHAREMSG];
		[defaults   setObject:productImagePath forKey:KEY_PRODUCTIMGPATH];
        //twitter
            
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    if (result == SLComposeViewControllerResultCancelled) {
                        DLog(@"Twitter Result: canceled");
                    } else {
                        DLog(@"Twitter Result: sent");
                    }
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        
                        [controller.view removeFromSuperview];
                    } else
                        [controller dismissViewControllerAnimated:YES completion:Nil];
                };
                controller.completionHandler =myBlock;
                
                //Adding the Text to the facebook post value from iOS
                [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                
                //Adding the URL to the facebook post value from iOS
                
                //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                
                //Adding the Image to the facebook post value from iOS
                
                [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    [self.view addSubview:controller.view];
                    [self.view bringSubviewToFront:controller.view];
                }else
                    [self presentViewController:controller animated:YES completion:Nil];
            }
            /*if ([TWTweetComposeViewController canSendTweet])
             {
             TWTweetComposeViewController *tweetSheet =
             [[[TWTweetComposeViewController alloc] init]autorelease];
             
             UIImage *img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]];
             [tweetSheet addImage:img];
             //[tweetSheet addURL:[NSURL URLWithString:androidDownloadLink]];
             [tweetSheet setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
             
             [self presentViewController:tweetSheet animated:YES];
             
             TWTweetComposeViewControllerCompletionHandler
             completionHandler = ^(TWTweetComposeViewControllerResult result)
             {
             switch (result)
             {
             case TWTweetComposeViewControllerResultCancelled:
             DLog(@"Twitter Result: canceled");
             break;
             case TWTweetComposeViewControllerResultDone:{
             DLog(@"Twitter Result: sent");
             }
             break;
             default:
             DLog(@"Twitter Result: default");
             break;
             }
             [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
             [self dismissModalViewControllerAnimated:YES];
             };
             [tweetSheet setCompletionHandler:completionHandler];
             }
             */
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cannot Share",@"Cannot Share")
                                                                message:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")
                                                               delegate:nil
                                                      cancelButtonTitle:NSLocalizedString(@"OK",@"OK")
                                                      otherButtonTitles: nil];
                [alert show];
                [alert release];
            }
        
	}
	
	else {
		TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbXml.rootXMLElement];
		int respCode = [[TBXML textForElement:responseCodeElement] intValue];
		if ((responseCodeElement != nil) && (respCode == 10001)) {
			TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbXml.rootXMLElement];
			
            NSString *str = [[[TBXML textForElement:responseTextElement]copy] autorelease];
			[UtilityManager showAlert:@"" msg:str];
		}
	}
    ReleaseAndNilify(responseXml);
}



- (IBAction)sendMailPressed:(id)sender
{
    [SharedManager  setShareEmailCoupon:NO];
    [SharedManager setShareEmailHotDeal:YES];
    [SharedManager setShareEmailSpecialOffer:NO];
    [SharedManager setShareEmailSpecialOfferUrl:NO];
    [SharedManager setShareAppsite:NO];
    [defaults setValue:hotDealId forKey:@"emailHotDealId"];
    
	EmailShareViewController *emailShareVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailShareVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [self presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailShareVC loadMail];
	
}

- (IBAction)sendTextPressed:(id)sender
{
    [SharedManager  setShareEmailCoupon:NO];
    [SharedManager setShareEmailHotDeal:YES];
    [SharedManager setShareEmailSpecialOffer:NO];
    [SharedManager setShareEmailSpecialOfferUrl:NO];
    [SharedManager setShareAppsite:NO];//for UT
    
	NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharehotdealinfo?hotdealId=%@",BASE_URL,hotDealId];
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];

	if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]) {
        ReleaseAndNilify(responseXml);
        return;
	}
	TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
	
	TBXMLElement *HotDealsDetailsElement = [TBXML childElementNamed:@"HotDealsDetails" parentElement:tbXml.rootXMLElement];
	if (HotDealsDetailsElement != nil) {
		
		TBXMLElement *titleElement1 = [TBXML childElementNamed:@"titleText" parentElement:HotDealsDetailsElement];
		TBXMLElement *titleElement2 = [TBXML childElementNamed:@"titleText2" parentElement:HotDealsDetailsElement];
		TBXMLElement *hotDealNameElement = [TBXML childElementNamed:@"hotDealName" parentElement:HotDealsDetailsElement];
		TBXMLElement *hdURLElement = [TBXML childElementNamed:@"hdURL" parentElement:HotDealsDetailsElement];
		
		
		NSMutableString *shareContent = [NSMutableString stringWithFormat:@"%@\n%@\n",[TBXML textForElement:titleElement1],[TBXML textForElement:titleElement2]];
        NSString *str = [[[TBXML textForElement:hotDealNameElement]copy] autorelease];
		[shareContent appendFormat:@"%@\n",str];
		
		if (hdURLElement != nil){
        
            NSString *str = [[[TBXML textForElement:hdURLElement]copy]autorelease];
			if (![str isEqualToString:@"N/A"])
				[shareContent appendFormat:@"URL:%@\n",str];
		}
		
		NSString *productImagePath = [NSString stringWithFormat:@"%@",imageStringValue];
		DLog(@"Share Content:%@",shareContent);
		DLog(@"Product Image Path:%@",productImagePath);
		
		[defaults   setObject:shareContent forKey:KEY_SHAREMSG];
		[defaults   setObject:productImagePath forKey:KEY_PRODUCTIMGPATH];
        
		
		// sending the sms
		
		//NSString* body = [defaults  objectForKey:KEY_SHAREMSG];
            
            if ([self hasCellularCoverage])
            {
                if ([MFMessageComposeViewController canSendText]) {
                    
                    MFMessageComposeViewController *smsComposerController = [[[MFMessageComposeViewController alloc] init] autorelease];
                    [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    smsComposerController.messageComposeDelegate = self;
                    [self presentViewController:smsComposerController animated:YES completion:nil];
                }
            }
            else
            {
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") message:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
            
            
        
	}
	else {
		TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbXml.rootXMLElement];
		int respCode = [[TBXML textForElement:responseCodeElement] intValue];
		if ((responseCodeElement != nil) && (respCode == 10001)) {
			TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbXml.rootXMLElement];
			NSString *str = [[[TBXML textForElement:responseTextElement]copy]autorelease];
			[UtilityManager showAlert:@"" msg:str];
		}
	}
    ReleaseAndNilify(responseXml);
}


-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	[controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
	[self dismissViewControllerAnimated:YES completion:nil];
    if (result == MessageComposeResultSent) {
        
        //For User Tracking
        NSMutableString *reqStr = [[NSMutableString alloc] init];
        [reqStr appendFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><hotDealId>%@</hotDealId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],hotDealId];
        
        
        
        NSMutableString *urlString1 = [[BASE_URL mutableCopy] autorelease];
        [urlString1 appendString:@"ratereview/updatesharetype"];
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString1 withParam:reqStr]];

        DLog(@"%@",responseXml);
        ReleaseAndNilify(responseXml);
        [reqStr release];
        //For User Tracking
    }
}

-(void)returnToMainPage:(id)sender
{
	[UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
}


-(IBAction)urlButtonClicked:(id)sender{
	
    switch (claimnredeemBuutonValue)
    {
        case 0: //Get Deal
        {
            NSMutableString *urlString = [[BASE_URL mutableCopy] autorelease];
            if([defaults valueForKey:@"hotDealListID"])
            //@Deepak: Added code for UserTracking for get Deal
            [urlString appendFormat:@"hotdeals/utgethotdealclick?userId=%@&hotDealId=%@&hotDealListId=%@",[defaults valueForKey:KEY_USERID],hotDealId,[defaults valueForKey:@"hotDealListID"]];
            else
                //@Deepak: Added code for UserTracking for get Deal
                [urlString appendFormat:@"hotdeals/utgethotdealclick?userId=%@&hotDealId=%@",[defaults valueForKey:KEY_USERID],hotDealId];
            
            
            NSString *responseXml = [ConnectionManager establishGetConnection:urlString];
            DLog(@"%@",responseXml);
            
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            [defaults setValue:url forKey:KEY_URL];
            WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            [urlDetail release];

        }
        break;
            
        case 1: // Claim
        {
            NSMutableString *reqStr = [[NSMutableString alloc] init];
            [reqStr appendFormat:@"<HotDealsListRequest><userId>%@</userId><hotDealId>%@</hotDealId>",[defaults valueForKey:KEY_USERID],hotDealId];
            if ([defaults valueForKey:KEY_MAINMENUID]) {
            [reqStr appendFormat:@"<mainMenuId>%@</mainMenuId></HotDealsListRequest>",[defaults valueForKey:KEY_MAINMENUID]];
            }else{
                [reqStr appendFormat:@"</HotDealsListRequest>"];
            }
            NSMutableString *urlString1 = [[BASE_URL mutableCopy] autorelease];
            [urlString1 appendString:@"hotdeals/hotdealclaim"];
            
            NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString1 withParam:reqStr]];
            
            TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
            
            ReleaseAndNilify(responseXml);
            TBXMLElement *responseTextElement= [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
            
            if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"])
            {
                claimnredeemBuutonValue = 2;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    [btnGetDeal setImage:[UIImage imageNamed:@"redeem.png"] forState:UIControlStateNormal];
                }
                else
                {
                    [btnGetDeal setImage:[UIImage imageNamed:@"redeemiPad.png"] forState:UIControlStateNormal];
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[TBXML textForElement:responseTextElement] message:nil
                                                 delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
                [alert show];
                [alert release];
            }

            
            [reqStr release];

        }
        break;
            
        case 2: // redeem
        {
            if(iHotDealRedeemViewController)
                [iHotDealRedeemViewController release];
            
            iHotDealRedeemViewController = [[HotDealRedeemViewController alloc]initWithNibName:@"HotDealRedeemViewController" bundle:[NSBundle mainBundle]];
            
            if (IS_IPHONE5)
                iHotDealRedeemViewController.view.frame = CGRectMake(10, 10, 300, 425);
            else
                iHotDealRedeemViewController.view.frame = CGRectMake(10, 10, 300, 350);

            
            iHotDealRedeemViewController.view.layer.backgroundColor = [UIColor whiteColor].CGColor;
            iHotDealRedeemViewController.view.layer.cornerRadius = 20.0;
            iHotDealRedeemViewController.view.layer.borderColor = [UIColor blackColor].CGColor;
            iHotDealRedeemViewController.view.layer.shadowOffset = CGSizeMake(1, 0);
            iHotDealRedeemViewController.view.layer.shadowColor = [[UIColor blackColor] CGColor];
            iHotDealRedeemViewController.view.layer.shadowRadius = 5;
            iHotDealRedeemViewController.view.layer.shadowOpacity = .25;
            
           
            if (![startDateString isEqualToString:@"N/A"]) {
                iHotDealRedeemViewController.strStartDate = [[NSString alloc]initWithString:startDateString];
                iHotDealRedeemViewController.StartDate.text = @"Start Date :";
            }
            else{
                
                if (![endDateString isEqualToString:@"N/A"]){
                    iHotDealRedeemViewController.strStartDate = [[NSString alloc]initWithString:endDateString];
                    iHotDealRedeemViewController.StartDate.text = @"End Date :";
                }
                
                else if (![hDExpDate isEqualToString:@"N/A"]){
                    iHotDealRedeemViewController.strStartDate = [[NSString alloc]initWithString:hDExpDate];
                    iHotDealRedeemViewController.StartDate.text = @"Expiration :";
                }
                
                else if (![coupCode isEqualToString:@"N/A"]){
                    iHotDealRedeemViewController.strStartDate = [[NSString alloc]initWithString:coupCode];
                    iHotDealRedeemViewController.StartDate.text = @"Code :";
                }
            }
            
            
            
            
            if (![endDateString isEqualToString:@"N/A"]) {
                iHotDealRedeemViewController.strEndDate = [[NSString alloc]initWithString:endDateString];
                iHotDealRedeemViewController.EndDate.text = @"End Date :";
            }
            else{
                if (![hDExpDate isEqualToString:@"N/A"]){
                    iHotDealRedeemViewController.strEndDate = [[NSString alloc]initWithString:hDExpDate];
                     iHotDealRedeemViewController.EndDate.text = @"Expiration :";
                }
                
                else if (![coupCode isEqualToString:@"N/A"]){
                    iHotDealRedeemViewController.strEndDate = [[NSString alloc]initWithString:coupCode];
                     iHotDealRedeemViewController.EndDate.text = @"Code :";
                }

            }
            
            
            
            
            if (![hDExpDate isEqualToString:@"N/A"]) {
                iHotDealRedeemViewController.strExpiration = [[NSString alloc]initWithString:hDExpDate];
                iHotDealRedeemViewController.Expiration.text = @"Expiration :";
            }
            else{
                if (![coupCode isEqualToString:@"N/A"]){
                    iHotDealRedeemViewController.strExpiration = [[NSString alloc]initWithString:coupCode];
                    iHotDealRedeemViewController.Expiration.text = @"Code :";
                }
            }
            
            
            
            
            if (![coupCode isEqualToString:@"N/A"]) {
                iHotDealRedeemViewController.strCode = [[NSString alloc]initWithString:coupCode];
                iHotDealRedeemViewController.Code.text = @"Code :";
            }
            
//            iHotDealRedeemViewController.strEndDate = [[NSString alloc]initWithString:endDateString];
            iHotDealRedeemViewController.strDescription =[[NSString alloc]initWithString:description];
//            iHotDealRedeemViewController.strExpiration = [[NSString alloc]initWithString:hDExpDate];
//            iHotDealRedeemViewController.strCode = [[NSString alloc]initWithString:coupCode];
            iHotDealRedeemViewController.strUserName = [[NSString alloc]initWithString:username];
            iHotDealRedeemViewController.strhotDealId = [[NSString alloc]initWithString:hotDealId];
            iHotDealRedeemViewController.objClass = self;
            
            [self.view addSubview:iHotDealRedeemViewController.view];
         
                                                            
        }
        break;
            
        case 3: // Already Redeemed
        {
        
        }
        break;
            
        default:
            break;
    }
}

-(IBAction)rateShareButtonClicked:(id)sender{
	[defaults setObject:[defaults valueForKey:@"HotDealId"] forKey:KEY_PRODUCTID];
	RateShareViewController *rateShareVC = [[RateShareViewController alloc] initWithNibName:@"RateShareViewController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:rateShareVC animated:NO];
	[rateShareVC release];
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];

    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)dealloc
{
	[customView release];
    if(iRetailerLocationsViewController)
        [iRetailerLocationsViewController release];
    [super dealloc];
}

@end

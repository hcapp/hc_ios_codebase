//
//  HotDealDetails.h
//  Scansee
//
//  Created by Dhananjaya C H on 22/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "RetailerLocationsViewController.h"
#import "HotDealRedeemViewController.h"

//typedef enum webServicesHotDeal
//{
//    HDDETAIL
//}webServicesHotDealState;

@interface HotDealDetails : UIViewController<UITextViewDelegate , MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UIActionSheetDelegate>
{
//    webServicesHotDealState iWebRequestState;
    RetailerLocationsViewController *iRetailerLocationsViewController;
    HotDealRedeemViewController *iHotDealRedeemViewController;
	IBOutlet UITextView *hotDealDescription, *termsConditions;
    
	IBOutlet UILabel *hotDealName;
	IBOutlet UILabel *price;
	IBOutlet UILabel *salePrice;
	IBOutlet UILabel *startDate, *endDate,*seprator;
	IBOutlet UIImageView *imgView;
    IBOutlet UIWebView *hD_Desc;
    IBOutlet UILabel *url_Label;
    IBOutlet UILabel *lblPowerBy;
    IBOutlet UIButton *btnGetDeal;
    IBOutlet UIButton *btnLocation;
    IBOutlet UILabel *deal;
    IBOutlet UILabel *regularPrice;
    IBOutlet UILabel *when;
    IBOutlet UILabel *details;
    
    
    IBOutlet UIButton *url_Button;
	NSMutableString *productId;
	int prodIndex;

    IBOutlet UILabel *poweredBy;
    
	NSString *prodDescStringValue;
	NSString *url;
	NSString *imageStringValue;
	NSString *startDateString, *endDateString;
	NSString *apiLogoUrl;
	NSString *hotDealId;
    NSString *extFlagValue;
    NSString *claimFlagValue;
    NSString *redeemFlagValue;
    NSString *username;
    NSString *hDExpDate;
    NSString *coupCode;
    NSString *description;
    NSString *retLocIDs;
    NSString *retLocs;
    NSString *retName;
    
    int claimnredeemBuutonValue;

	IBOutlet UIButton *urlButton;

    IBOutlet UIView *customView;
	
	IBOutlet UIButton *faceBookBtn ,*twitterBtn , *sendText , *sendMail ;

}
@property(nonatomic,readwrite)int claimnredeemBuutonValue;
@property (nonatomic , retain) IBOutlet UIView *customView;
@property (nonatomic , retain) IBOutlet UIButton *url_Button;
@property (nonatomic , retain) IBOutlet UIButton *btnGetDeal;

- (IBAction)facebookButtonPressed:(id)sender;
- (IBAction)twitterButtonPressed:(id)sender;
- (IBAction)sendTextPressed:(id)sender;
- (IBAction)sendMailPressed:(id)sender;
-(IBAction)locationButtonClicked:(id)sender;
-(void)returnToMainPage:(id)sender;
-(IBAction)urlButtonClicked:(id)sender;

@end

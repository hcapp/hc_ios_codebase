//
// HotDealsList.h   
//  Scansee
//
//  Created by Dhananjaya C H on 22/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"
#import <CoreLocation/CoreLocation.h>

typedef enum webServicescalls{
    getHotDealProds,
    hddetail,
    getusercat
}WebRequestType;

@interface HotDealDO : NSObject{
    
    NSString *catId, *catName;
    NSString *apiPartnerId, *apiPartnerName;
    NSMutableArray *hotDealDetailsObjArray;//array of hot deal details objects
}
@property (nonatomic, retain)NSString *catId, *catName;
@property (nonatomic, retain)NSString *apiPartnerId, *apiPartnerName;
@property (nonatomic, retain)NSMutableArray *hotDealDetailsObjArray;
@end

@interface HotDealDetailsDO : NSObject{
    
    NSString *hdName, *hdSDesc, *hdImgPath, *hdListId;
    NSString *hdId, *hdPrice, *hdSalePrice;
    NSString *hdUrl, *hdNewFlag;
}

@property (nonatomic, retain)NSString *hdName, *hdSDesc, *hdImgPath, *hdListId;
@property (nonatomic, retain)NSString *hdId, *hdPrice, *hdSalePrice;
@property (nonatomic, retain)NSString *hdUrl, *hdNewFlag;

@end

@class ScanseeManager;

@interface HotDealsList : UIViewController<CLLocationManagerDelegate, UIPickerViewDelegate,UIAlertViewDelegate , UITableViewDelegate , UITableViewDataSource , UISearchBarDelegate> {

	IBOutlet UIView *modelView;
	IBOutlet UIPickerView *data_picker;
	IBOutlet UIView *customView;
	IBOutlet UISearchBar *m_searchBar;
	IBOutlet UITableView *hotDealTable;
    IBOutlet UIButton *m_Button;
    IBOutlet UIImageView *hotDealIconImg;
    IBOutlet UILabel *hotDealFoundTxt;
	IBOutlet UIButton *byCategory , *galleryBtn ,*preferenceBtn ;
	
	NSInteger favCat;
	NSMutableArray *categoryName ,*categoryIdHotdeals ;
    
    int lastvalueforLoop;  //@PaginationChange: Added Variable to maintain the pagination count

	//NSMutableArray *sectionArry;
	
	NSMutableArray *categoryArray , *categoryId;
	NSMutableString *categoryValue ;
	int categoryIdValue;
	
	int nextPage,lastVisitedRecord;
    NSIndexPath *index;
    
    WebRequestType iWebRequestType;
    NSMutableArray *hotDealObjArray;//main array having hot deal objects
    
} 
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) NSMutableArray *locationMeasurements;
@property (nonatomic, retain) CLLocation *bestEffortAtLocation;
@property (nonatomic, retain) IBOutlet UIButton *m_Button;

-(IBAction)doneClicked : (id) sender ;

-(void) removeAllObjectsinArray ;
-(IBAction) categorybtnPressed : (id) sender ;
-(IBAction) galleryPressed : (id) sender ;
-(IBAction) bannerPressed : (id) sender ;
-(IBAction) favCategories : (id) sender ;

-(void)showSplash;
- (void)hideSplash;

@end

//
//  HotDealDetailViewController.m
//  HubCiti
//
//  Created by Kitty on 01/04/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "HotDealDetailViewController.h"
#import "MainMenuViewController.h"
#import "WebBrowserViewController.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "HotDealMapViewController.h"
#import "DealHotDealsList.h"
#import "RetailerSummaryViewController.h"
#import "CurrentSpecials.h"
#import "CurrentSpecialsViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "CouponsInDealViewController.h"

#define LOCATION_TAG 1
#define DETAIL_TAG  2

@interface HotDealDetailViewController ()

@end

@implementation HotDealDetailViewController
@synthesize claimnredeemBuutonValue,fundraiserPurchaseProductslink,anyVC,emailSendingVC;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"Deals";
    //    fundraiserView = [[UIScrollView alloc]init];
    title = [[NSString alloc]init];
    fundraiserPurchaseProductslink = [[UIButton alloc]init];
    // displaying share button
    
       
    
    //customize back button
   
    self.navigationItem.hidesBackButton = YES;
    //[shareButton release];
    //[back release];
    
    
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    
    [self request_hddetail];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];

    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    //    [self addShareButton];
}



-(void)addShareButton{
    // Create down menu button
    UIImageView *homeLabel;
    
    
    // Create up menu button
    homeLabel = [self createHomeButtonView];
    
    DWBubbleMenuButton *upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
                                                                                          self.view.frame.size.height - homeLabel.frame.size.height - 20.f,
                                                                                          homeLabel.frame.size.width,
                                                                                          homeLabel.frame.size.height)
                                                            expansionDirection:DirectionUp];
    upMenuView.homeButtonView = homeLabel;
    
    upMenuView.delegate = self;
    
    [upMenuView addButtons:[self createDemoButtonArray]];
    
    [self.view addSubview:upMenuView];
    
    [self.view bringSubviewToFront:upMenuView];
    
    upMenuView.onLogOutPressed =^(){
        
        [self logOutPressed];
    } ;
    
    
}




- (UIImageView *)createHomeButtonView {
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
    label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}

- (NSArray *)createDemoButtonArray {
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    
    int i = 0;
    for (NSString *titleButton in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:titleButton] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        
        button.clipsToBounds = YES;
        button.tag = i++;
        
        [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
}

- (void)test:(UIButton *)sender {
    
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
    [self shareClicked];
    switch (sender.tag) {
        case 0:{//facebook
            anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
            anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"HOT_DEAL_SHARE"];
            [UIView transitionWithView:self.view duration:0.5
                               options:UIViewAnimationOptionTransitionCurlDown
                            animations:^ { [self.view addSubview:anyVC.view]; }
                            completion:nil];
        }
            break;
        case 1:{//twitter
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    if (result == SLComposeViewControllerResultCancelled) {
                        DLog(@"Twitter Result: canceled");
                    } else {
                        DLog(@"Twitter Result: sent");
                        NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><fundId>%@</fundId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:FUNDRAISERID]];
                        
                        [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
                    }
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        
                        [controller.view removeFromSuperview];
                    } else
                        [controller dismissViewControllerAnimated:YES completion:Nil];
                };
                controller.completionHandler =myBlock;
                
                //Adding the Text to the facebook post value from iOS
                [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                
                //Adding the URL to the facebook post value from iOS
                
                //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                
                //Adding the Image to the facebook post value from iOS
                
                [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    [self.view addSubview:controller.view];
                    [self.view bringSubviewToFront:controller.view];
                }else
                    [self presentViewController:controller animated:YES completion:Nil];
                
                
            }
            else {
                [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
                }
        }
            
            
            break;
        case 2:{//text
            
            if ([self hasCellularCoverage])
            {
                if ([MFMessageComposeViewController canSendText]) {
                    
                    MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
                    [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    smsComposerController.messageComposeDelegate = self;
                    [self presentViewController:smsComposerController animated:YES completion:nil];
                }
            }
            else
            {
                [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
            }
            
            
        }
            break;
        case 3:{//email
            //[defaults setValue:FUNDRAISER_SHARE forKey:KEY_SHARE_TYPE];
            __typeof(self) __weak  obj = self;
            emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
            emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"HOT_DEAL_SHARE"];
            emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                
                [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
            } ;
            
            [emailSendingVC loadMail];
        }
            break;
            
        default:
            [defaults setBool:NO forKey:BottomButton];
            break;
    }
    
}

- (UIButton *)createButtonWithName:(NSString *)imageName {
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button sizeToFit];
    
    [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}



- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView{
    
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark button action methods

-(void)popBackToPreviousPage
{
    if (maxCountFlag==TRUE) {
        maxCountFlag=FALSE;
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        for (UIViewController *aViewController in allViewControllers) {
            if ([aViewController isKindOfClass:[CouponsInDealViewController class]]) {
                [self.navigationController popToViewController:aViewController animated:NO];
            }
        }
    }
    
    else if(fromCurrentSpecials){
        fromCurrentSpecials =FALSE;
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        for (UIViewController *aViewController in allViewControllers) {
            if ([aViewController isKindOfClass:[CurrentSpecialsViewController class]]) {
                [self.navigationController popToViewController:aViewController animated:NO];
            }
        }
    }
    else
    {
        if ([defaults boolForKey:@"detailsPage"]) {
            
            [defaults setBool:NO forKey:@"detailsPage"];
            [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:NO];
            
        }
    }
}



-(void)returnToMainPage:(id)sender
{
    [defaults setBool:NO forKey: @"detailsPage"];
    maxCountFlag=FALSE;
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

#pragma mark view set methods

-(void) setiPadView
{
    
    
    fundraiserView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, -20, SCREEN_WIDTH, SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT)];
    [fundraiserView setBackgroundColor:[UIColor clearColor]];
    
    int xPos=10;
    int yPos=-10;
    int gap=10;
    int height = 0;
    int width = SCREEN_WIDTH-20;
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        gap=15;
    }
    
    //Set the tile lable
    if (title) {
        fundraiserTitle=[[UILabel alloc]init];
        fundraiserTitle.textAlignment = NSTextAlignmentCenter;
        fundraiserTitle.textColor = [UIColor whiteColor];
        fundraiserTitle.backgroundColor= [UIColor lightGrayColor];
        fundraiserTitle.numberOfLines=2;
        fundraiserTitle.lineBreakMode = NSLineBreakByWordWrapping;
        fundraiserTitle.text=title;
        fundraiserTitle.translatesAutoresizingMaskIntoConstraints=NO;
        
    }
    
    if (startDateString) {
        fundraiserStartDate=[[UILabel alloc]init];
        //        NSString *sdate = @"Start Date:";
        //        NSString *fundStartDate = [sdate stringByAppendingString:fundDetails.startDate];
        fundraiserStartDate.font =[UIFont fontWithName:@"Helvetica" size:13];
        fundraiserStartDate.numberOfLines=1;
        fundraiserStartDate.textColor = [UIColor whiteColor];
        fundraiserStartDate.lineBreakMode= NSLineBreakByWordWrapping;
        fundraiserStartDate.text=startDateString;
        fundraiserStartDate.translatesAutoresizingMaskIntoConstraints=NO;
        
    }
    
    if (![endDateString isEqualToString:@"N/A"]) {
        NSString *edate = @" - ";
        NSString *fundEndDate = [edate stringByAppendingString:endDateString];
        fundraiserEndDate =[[UILabel alloc]init];
        fundraiserEndDate.font =[UIFont fontWithName:@"Helvetica" size:13];
        fundraiserEndDate.textAlignment= NSTextAlignmentLeft;
        fundraiserEndDate.numberOfLines=1;
        fundraiserEndDate.textColor = [UIColor whiteColor];
        fundraiserEndDate.lineBreakMode= NSLineBreakByWordWrapping;
        fundraiserEndDate.text=fundEndDate;
        fundraiserEndDate.translatesAutoresizingMaskIntoConstraints=NO;
        [fundraiserTitle addSubview:fundraiserEndDate];
    }
    
    [fundraiserTitle addSubview:fundraiserStartDate];
    [fundraiserView addSubview:fundraiserTitle];
    
    height = 0.1*SCREEN_HEIGHT;
    NSDictionary *dictonaryOne;
    
    if (![endDateString isEqualToString:@"N/A"]){
        dictonaryOne = @{@"title":fundraiserTitle,@"start":fundraiserStartDate,@"end":fundraiserEndDate};
    }
    else
    {
        dictonaryOne = @{@"title":fundraiserTitle,@"start":fundraiserStartDate};
    }
    
    [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[title(%d)]",height] options:0 metrics:nil views:dictonaryOne]];
    [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[title(%f)]",SCREEN_WIDTH] options:0 metrics:nil views:dictonaryOne]];
    fundraiserTitle.font = [UIFont boldSystemFontOfSize:19.0];
    if ([endDateString isEqualToString:@"N/A"])
    {
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[start(%d)]",(height/2),(height/2)] options:0 metrics:nil views:dictonaryOne]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[start(%f)]",SCREEN_WIDTH] options:0 metrics:nil views:dictonaryOne]];
        fundraiserStartDate.textAlignment= NSTextAlignmentCenter;
    }
    else
    {
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[start(%d)]",(height/2),(height/2)] options:0 metrics:nil views:dictonaryOne]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[start(%f)]",SCREEN_WIDTH/2] options:0 metrics:nil views:dictonaryOne]];
        fundraiserStartDate.textAlignment= NSTextAlignmentRight;
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[end(%d)]",(height/2),(height/2)] options:0 metrics:nil views:dictonaryOne]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[end(%f)]",SCREEN_WIDTH/2,SCREEN_WIDTH/2] options:0 metrics:nil views:dictonaryOne]];
        fundraiserEndDate.textAlignment= NSTextAlignmentLeft;
    }
    fundraiserStartDate.font =[UIFont fontWithName:@"Helvetica" size:18];
    fundraiserEndDate.font =[UIFont fontWithName:@"Helvetica" size:18];
    
    
    yPos = height+ gap;
    
    
    if (![shortDesc isEqualToString:@"N/A"] && shortDesc) {
        
        fundraiserShortDescription = [[UILabel alloc]init ];//WithFrame:CGRectMake(xPos,yPos,width, 40)];
        fundraiserShortDescription.font =[UIFont fontWithName:@"Helvetica" size:13];
        fundraiserShortDescription.textAlignment= NSTextAlignmentCenter;
        fundraiserShortDescription.numberOfLines=0;
        fundraiserShortDescription.lineBreakMode= NSLineBreakByWordWrapping;
        fundraiserShortDescription.textColor = [UIColor blackColor];
        fundraiserShortDescription.backgroundColor =[UIColor clearColor];
        fundraiserShortDescription.text=shortDesc;
        height=[self calculateLabelHeight:fundraiserShortDescription.text labelWidth:width-25 fontSize:13];
        fundraiserShortDescription.frame = CGRectMake(xPos+15, yPos, width-25, height);
        [fundraiserView addSubview:fundraiserShortDescription];
        yPos = yPos + height+gap;
    }
    
    if (![imageStringValue isEqualToString:@"N/A"] && imageStringValue) {
        if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
        {
            height=0.35*SCREEN_HEIGHT;
        }
        else
        {
            height=0.25*SCREEN_HEIGHT;
        }
        
        
        
        SdImageView *asyncImageView = [[SdImageView alloc] init];
        asyncImageView.backgroundColor = [UIColor clearColor];
        asyncImageView.layer.cornerRadius = 5.0f;
        [asyncImageView loadImage:imageStringValue];
        [asyncImageView setBackgroundColor:[UIColor grayColor]];
        asyncImageView.translatesAutoresizingMaskIntoConstraints=NO;
        [fundraiserView addSubview:asyncImageView];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            
        }
        else
        {
            NSDictionary *imageDic=@{@"image":asyncImageView};
            
            [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[image(%d)]",yPos,height] options:0 metrics:nil views:imageDic]];
            [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%d)-[image(%d)]",width/8,(6*width)/8] options:0 metrics:nil views:imageDic]];
            yPos=yPos+height +gap;
            
        }
        
        //[asyncImageView release];
    }
    
    
    height=0.04*SCREEN_HEIGHT;
    //    fundraiserPurchaseProductslink.frame = CGRectMake(xPos+30.0, yPos, width-60.0, 30);
    fundraiserPurchaseProductslink.layer.cornerRadius = 5.0;
    fundraiserPurchaseProductslink.titleLabel.font = [UIFont boldSystemFontOfSize:17.0];
    
    
    [fundraiserPurchaseProductslink setBackgroundColor:[UIColor grayColor]];
    [fundraiserPurchaseProductslink addTarget:self action:@selector(urlButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    fundraiserPurchaseProductslink.translatesAutoresizingMaskIntoConstraints=NO;
    
    [fundraiserView addSubview:fundraiserPurchaseProductslink];
    
    
    NSDictionary *productDictionary=@{@"link":fundraiserPurchaseProductslink};
    
    
    [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[link(%d)]",yPos,height] options:0 metrics:nil views:productDictionary]];
    [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[link(%f)]",(width/3.5),width/2.5] options:0 metrics:nil views:productDictionary]];
    
    yPos = yPos + height +gap;
    
    if (claimnredeemBuutonValue) {
        
        switch (claimnredeemBuutonValue) {
            case 3:
                // Redeem button will be deactive as its claimed and reemed both
                
                [fundraiserPurchaseProductslink setTitle:@"Redeem" forState:UIControlStateNormal];
                fundraiserPurchaseProductslink.enabled=NO;
                break;
            case 2:
                // Redeem button will be active as its claimed but not redeemed
                
                [fundraiserPurchaseProductslink setTitle:@"Redeem" forState:UIControlStateNormal];
                
                break;
                
            case 1:
                // Claim button will be active as its not claimed and not redeemed t
                
                [fundraiserPurchaseProductslink setTitle:@"Claim" forState:UIControlStateNormal];
                break;
                
            default:
                break;
        }
        
        if (isExpiredFlag) {
            fundraiserPurchaseProductslink.hidden=YES;
        }
        else{
            fundraiserPurchaseProductslink.hidden=NO;
            [fundraiserView addSubview:fundraiserPurchaseProductslink];
        }
        DLog(@"%d",yPos);
        yPos = yPos +  height+gap;
        DLog(@"%d",yPos);
    }
    
    else{
        [fundraiserPurchaseProductslink setTitle:@"Get Deal" forState:UIControlStateNormal];
        [fundraiserView addSubview:fundraiserPurchaseProductslink];
        DLog(@"%d",yPos);
        yPos = yPos +  height+gap;
        DLog(@"%d",yPos);
    }
    
    
    
    
    
    if(![description isEqualToString:@"N/A"] && description)
    {
        
        moreInformation=[[UIButton alloc]init];
        //        [defaults setValue:fundDetails.moreInfoURL forKey:KEY_URL];
        moreInformation.clipsToBounds = YES;
        
        moreInformation.titleLabel.font =[UIFont boldSystemFontOfSize:28];
        
        [moreInformation setTitleColor:[UIColor colorWithRGBInt:0x112e72] forState:UIControlStateNormal];
        
        [moreInformation setTitle:@"More Information" forState:UIControlStateNormal];
        
        [moreInformation addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        moreInformation.tag =1;
        
        moreInformation.layer.borderWidth = 2.0;
        moreInformation.layer.cornerRadius=10;
        moreInformation.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
        moreInformation.translatesAutoresizingMaskIntoConstraints=NO;
        
        UIImage *btnImage = [UIImage imageNamed:@"info.png"];
        UIImageView *buttonImage =[[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 60, 60)];
        [buttonImage setImage:btnImage];
        
        
        [moreInformation addSubview:buttonImage];
        [fundraiserView addSubview:moreInformation];
        
        NSDictionary *moreDictionary = @{@"more":moreInformation,@"image":btnImage};
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[more(70)]",yPos] options:0 metrics:nil views:moreDictionary]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[more(%f)]",SCREEN_WIDTH/4,SCREEN_WIDTH/2] options:0 metrics:nil views:moreDictionary]];
        yPos=yPos+70+gap;
        
        UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake((moreInformation.frame.size.width-15),35, 10,10)];
        [arrow setImage:[UIImage imageNamed:@"right_arrow"]];
        [moreInformation addSubview:arrow];
        [fundraiserView addSubview:moreInformation];
    }
    
    if(isLocationFlag)
    {
        
        eventInformation =[[UIButton alloc]init];
        eventInformation.clipsToBounds = YES;
        eventInformation.titleLabel.font =[UIFont boldSystemFontOfSize:28];
        [eventInformation setTitleColor:[UIColor colorWithRGBInt:0x112e72] forState:UIControlStateNormal];
        [eventInformation setTitle:@"Location(s)" forState:UIControlStateNormal];
        [eventInformation addTarget:self action:@selector(locationButtonClicked:) forControlEvents:UIControlEventTouchDown];
        eventInformation.tag =2;
        eventInformation.layer.borderWidth = 2.0;
        eventInformation.layer.cornerRadius=10;
        eventInformation.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
        eventInformation.translatesAutoresizingMaskIntoConstraints=NO;
        
        
        UIImage *btnImage = [UIImage imageNamed:@"location"];
        UIImageView *buttonImage =[[UIImageView alloc]initWithFrame:CGRectMake(10, 5 , 60, 60)];
        [buttonImage setImage:btnImage];
        
        [eventInformation addSubview:buttonImage];
        [fundraiserView addSubview:eventInformation];
        
        NSDictionary *eventDictionary=@{@"event":eventInformation};
        
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%d)-[event(70)]",yPos] options:0 metrics:nil views:eventDictionary]];
        [fundraiserView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[event(%f)]",SCREEN_WIDTH/4,SCREEN_WIDTH/2] options:0 metrics:nil views:eventDictionary]];
        
    }
    
    int addFactor;
    if (yPos>(SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT)) {
        addFactor=yPos+100-(SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT);
    }
    else
    {
        addFactor=0;
    }
    
    
    fundraiserView.contentSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT+100+addFactor);
    fundraiserView.scrollEnabled=YES;
    [self.view addSubview:fundraiserView];
    [self addShareButton];
}

-(void) setFundraiserView
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        [self setiPadView];
    }
    else
    {
        fundraiserView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, -10, SCREEN_WIDTH, SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT)];
        [fundraiserView setBackgroundColor:[UIColor clearColor]];
        
        int xPos=10;
        int yPos=-10;
        int gap= 10;
        int height = 0;
        int width = SCREEN_WIDTH-20;
        
        //Set the tile lable
        if (title) {
            fundraiserTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
            fundraiserTitle.font = [UIFont boldSystemFontOfSize:14.0];
            fundraiserTitle.textAlignment = NSTextAlignmentCenter;
            fundraiserTitle.textColor = [UIColor whiteColor];
            fundraiserTitle.backgroundColor= [UIColor lightGrayColor];
            fundraiserTitle.numberOfLines=2;
            fundraiserTitle.lineBreakMode = NSLineBreakByWordWrapping;
            fundraiserTitle.text=title;
            
        }
        
        if (startDateString) {
            
            if ([endDateString isEqualToString:@"N/A"]) {
                fundraiserStartDate=[[UILabel alloc]initWithFrame:CGRectMake(0, 40, SCREEN_WIDTH, 40)];
                fundraiserStartDate.textAlignment= NSTextAlignmentCenter;
            }
            else{
                fundraiserStartDate=[[UILabel alloc]initWithFrame:CGRectMake(0, 40, SCREEN_WIDTH/2, 40)];
                fundraiserStartDate.textAlignment= NSTextAlignmentRight;
            }
            
            fundraiserStartDate.font =[UIFont fontWithName:@"Helvetica" size:11];
            fundraiserStartDate.numberOfLines=1;
            fundraiserStartDate.textColor = [UIColor whiteColor];
            fundraiserStartDate.lineBreakMode= NSLineBreakByWordWrapping;
            fundraiserStartDate.text=startDateString;
            
        }
        
        if (![endDateString isEqualToString:@"N/A"]) {
            NSString *edate = @" - ";
            NSString *dealEndDate = [edate stringByAppendingString:endDateString];
            fundraiserEndDate =[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2, 40, SCREEN_WIDTH/2, 40)];
            fundraiserEndDate.font =[UIFont fontWithName:@"Helvetica" size:11];
            fundraiserEndDate.textAlignment= NSTextAlignmentLeft;
            fundraiserEndDate.numberOfLines=1;
            fundraiserEndDate.textColor = [UIColor whiteColor];
            fundraiserEndDate.lineBreakMode= NSLineBreakByWordWrapping;
            fundraiserEndDate.text=dealEndDate;
            [fundraiserTitle addSubview:fundraiserEndDate];
        }
        
        [fundraiserTitle addSubview:fundraiserStartDate];
        [fundraiserView addSubview:fundraiserTitle];
        yPos = yPos+fundraiserTitle.frame.size.height + gap+10;
        
        if (![shortDesc isEqualToString:@"N/A"] && shortDesc) {
            
            fundraiserShortDescription = [[UILabel alloc]init ];//WithFrame:CGRectMake(xPos,yPos,width, 40)];
            fundraiserShortDescription.font =[UIFont fontWithName:@"Helvetica" size:13];
            fundraiserShortDescription.textAlignment= NSTextAlignmentCenter;
            fundraiserShortDescription.numberOfLines=0;
            fundraiserShortDescription.lineBreakMode= NSLineBreakByWordWrapping;
            fundraiserShortDescription.textColor = [UIColor blackColor];
            fundraiserShortDescription.backgroundColor =[UIColor clearColor];
            fundraiserShortDescription.text=shortDesc;
            height=[self calculateLabelHeight:fundraiserShortDescription.text labelWidth:width-25 fontSize:13];
            fundraiserShortDescription.frame = CGRectMake(xPos+15, yPos, width-25, height);
            [fundraiserView addSubview:fundraiserShortDescription];
            yPos = yPos + height+gap;
        }
        
        if (![imageStringValue isEqualToString:@"N/A"] && imageStringValue) {
            
            SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(xPos+25, yPos, width-50, 150)];
            asyncImageView.layer.cornerRadius = 5.0f;
            [asyncImageView loadImage:imageStringValue];
            [asyncImageView setBackgroundColor:[UIColor clearColor]];
            
            [fundraiserView addSubview:asyncImageView];
            yPos=yPos+asyncImageView.frame.size.height +gap;
            //[asyncImageView release];
        }
        
        fundraiserPurchaseProductslink.frame = CGRectMake(xPos+30.0, yPos, width-60.0, 30);
        fundraiserPurchaseProductslink.layer.cornerRadius = 5.0;
        fundraiserPurchaseProductslink.titleLabel.font = [UIFont boldSystemFontOfSize:17.0];
        
        DLog(@"%f",fundraiserPurchaseProductslink.frame.size.width);
        
        [fundraiserPurchaseProductslink setBackgroundColor:[UIColor grayColor]];
        
        
        [fundraiserPurchaseProductslink addTarget:self action:@selector(urlButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if (claimnredeemBuutonValue) {
            
            switch (claimnredeemBuutonValue) {
                case 3:
                    // Redeem button will be deactive as its claimed and reemed both
                    [fundraiserPurchaseProductslink setTitle:@"Redeem" forState:UIControlStateNormal];
                    fundraiserPurchaseProductslink.enabled=NO;
                    break;
                case 2:
                    // Redeem button will be active as its claimed but not redeemed
                    [fundraiserPurchaseProductslink setTitle:@"Redeem" forState:UIControlStateNormal];
                    
                    
                    break;
                    
                case 1:
                    // Claim button will be active as its not claimed and not redeemed too
                    [fundraiserPurchaseProductslink setTitle:@"Claim" forState:UIControlStateNormal];
                    
                    
                    
                    break;
                    
                default:
                    break;
            }
            
            if (isExpiredFlag) {
                fundraiserPurchaseProductslink.hidden=YES;
            }
            else{
                fundraiserPurchaseProductslink.hidden=NO;
                [fundraiserView addSubview:fundraiserPurchaseProductslink];
            }
            
        }
        
        else{
            [fundraiserPurchaseProductslink setTitle:@"Get Deal" forState:UIControlStateNormal];
            [fundraiserView addSubview:fundraiserPurchaseProductslink];
        }
        DLog(@"%d",yPos);
        yPos = yPos +  fundraiserPurchaseProductslink.frame.size.height+gap;
        DLog(@"%d",yPos);
        
        
        if(![description isEqualToString:@"N/A"] && description)
        {
            //        moreInformation.frame = CGRectMake(startX, yPos+5, width, 40);
            
            moreInformation =[[UIButton alloc]initWithFrame:CGRectMake(0, yPos, SCREEN_WIDTH, 50)];
            //            [defaults setValue:fundDetails.moreInfoURL forKey:KEY_URL];
            moreInformation.clipsToBounds = YES;
            
            moreInformation.titleLabel.font =[UIFont boldSystemFontOfSize:17];
            
            [moreInformation setTitleColor:[UIColor colorWithRGBInt:0x112e72] forState:UIControlStateNormal];
            
            [moreInformation setTitle:@"More Information" forState:UIControlStateNormal];
            
            [moreInformation addTarget:self action:@selector(DetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            moreInformation.tag =1;
            
            moreInformation.layer.borderWidth = 1.0;
            moreInformation.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
            UIImageView *icon = [[UIImageView alloc]initWithFrame:CGRectMake(15, 5, 40, 40)];
            [icon setImage:[UIImage imageNamed:@"info.png"]];
            
            [moreInformation addSubview:icon];
            
            UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake(width, moreInformation.frame.size.height/2-2, 7, 7)];
            [arrow setImage:[UIImage imageNamed:@"right_arrow"]];
            
            yPos = yPos + moreInformation.frame.size.height;
            
            // [moreInformation addSubview:icon];
            [moreInformation addSubview:arrow];
            [fundraiserView addSubview:moreInformation];
        }
        
        if(isLocationFlag)
        {
            //        eventInformation.frame = CGRectMake(startX, yPos+5, width, 40);
            
            eventInformation =[[UIButton alloc]initWithFrame:CGRectMake(0, yPos, SCREEN_WIDTH, 50)];
            
            eventInformation.clipsToBounds = YES;
            
            eventInformation.titleLabel.font =[UIFont boldSystemFontOfSize:17];
            
            [eventInformation setTitleColor:[UIColor colorWithRGBInt:0x112e72] forState:UIControlStateNormal];
            
            [eventInformation setTitle:@"Location(s)" forState:UIControlStateNormal];
            
            [eventInformation addTarget:self action:@selector(locationsClicked) forControlEvents:UIControlEventTouchUpInside];
            eventInformation.tag =2;
            
            eventInformation.layer.borderWidth = 1.0;
            
            eventInformation.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
            
            UIImageView *icon1 = [[UIImageView alloc]initWithFrame:CGRectMake(15, 5, 40, 40)];
            [icon1 setImage:[UIImage imageNamed:@"location.png"]];
            
            [eventInformation addSubview:icon1];
            
            UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake(width, eventInformation.frame.size.height/2-2, 7, 7)];
            [arrow setImage:[UIImage imageNamed:@"right_arrow"]];
            
          //  yPos = yPos + eventInformation.frame.size.height+5;
            
            
            [eventInformation addSubview:arrow];
            
            [fundraiserView addSubview:eventInformation];
        }
        
        fundraiserView.contentSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT+140);
        [self.view addSubview:fundraiserView];
    }
    
    [self addShareButton];
    
}

#pragma deatils buttons

-(void)locationsClicked
{
    tblEventHolder=[[UIView alloc]init];
    
    if (IS_IPHONE5)
        tblEventHolder.frame = CGRectMake(10, 10, 300, 425);
    else if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        tblEventHolder.frame = CGRectMake(80, 60, 600, 700);
    else
        tblEventHolder.frame = CGRectMake(10, 10, 300, 350);
    
    tblEventHolder.layer.backgroundColor = [UIColor whiteColor].CGColor;
    tblEventHolder.layer.cornerRadius = 20.0;
    tblEventHolder.layer.borderColor = [UIColor blackColor].CGColor;
    tblEventHolder.layer.shadowOffset = CGSizeMake(1, 0);
    tblEventHolder.layer.shadowColor = [[UIColor blackColor] CGColor];
    tblEventHolder.layer.shadowRadius = 5;
    tblEventHolder.layer.shadowOpacity = .25;
    
    
    
    
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        cross_button=[[UIButton alloc]initWithFrame:CGRectMake(tblEventHolder.frame.size.width-75,-3, 75, 55)];
        [cross_button setImage:[UIImage imageNamed:@"cross_icon_ipad.png"] forState:UIControlStateNormal];
    }
    else
    {
        cross_button=[[UIButton alloc]initWithFrame:CGRectMake(250,-5, 65, 45)];
        [cross_button setImage:[UIImage imageNamed:@"cross_icon.png"] forState:UIControlStateNormal];
    }
    
    [cross_button addTarget:self action:@selector(crossClicked) forControlEvents:UIControlEventTouchDown];
    [tblEventHolder addSubview:cross_button];
    
    tblLocations=[[UITableView alloc]init];
    if (IS_IPHONE5)
        tblLocations = [[UITableView alloc]initWithFrame:CGRectMake(10, 135, 280, 255) style:UITableViewStylePlain];
    else if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        tblLocations = [[UITableView alloc]initWithFrame:CGRectMake(10, 200, 580, 500) style:UITableViewStylePlain];
    else
        tblLocations = [[UITableView alloc]initWithFrame:CGRectMake(10, 135, 280, 210) style:UITableViewStylePlain];
    tblLocations.dataSource=self;
    tblLocations.delegate=self;
    [tblLocations setBackgroundColor:[UIColor clearColor]];
    
    
    strDealName = [[NSString alloc]initWithString:fundraiserTitle.text];
    arrLocations = [[NSArray alloc]initWithArray:[retLocs componentsSeparatedByString:@"|"]];
    strRetailerName = [[NSString alloc]initWithString:retName];
    if(![retLocImgLogo isEqualToString:@"N/A"] && retLocImgLogo)
        imagePath=[[NSString alloc]initWithString:retLocImgLogo];
    
    
    UILabel *retailerLabel ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        retailerLabel= [[UILabel alloc]initWithFrame:CGRectMake(20, 25, 150, 50)];
        retailerLabel.font=[UIFont boldSystemFontOfSize:24.0];
    }
    else
    {
        retailerLabel= [[UILabel alloc]initWithFrame:CGRectMake(20, 25, 76, 21)];
        retailerLabel.font=[UIFont boldSystemFontOfSize:17.0];
    }
    
    retailerLabel.text=@"Retailer:";
    [tblEventHolder addSubview:retailerLabel];
    
    
    UILabel *retNameLabel;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        retNameLabel =[[UILabel alloc]initWithFrame:CGRectMake(20, 80, 400, 50)];
        retNameLabel.font=[UIFont systemFontOfSize:24.0];
    }
    else
    {
        retNameLabel =[[UILabel alloc]initWithFrame:CGRectMake(20, 50, 226, 21)];
        retNameLabel.font=[UIFont systemFontOfSize:17.0];
    }
    
    retNameLabel.text=strRetailerName;
    
    [tblEventHolder addSubview:retNameLabel];
    
    UILabel *locLabel;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        locLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 135, 220, 50)];
        locLabel.font=[UIFont boldSystemFontOfSize:24.0];
    }
    else
    {
        locLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 80, 150, 21)];
        locLabel.font=[UIFont boldSystemFontOfSize:17.0];
    }
    
    locLabel.text= strDealName; //@"Locations :"
    [tblEventHolder addSubview:locLabel];
    
    [tblEventHolder addSubview:tblLocations];
    
    [self.view addSubview:tblEventHolder];
    
}
#pragma mark request methods

-(void)request_hddetail
{
    iWebRequestState = HDDETAIL;
    
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<HotDealsDetails><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<hotDealId>%@</hotDealId>",[defaults valueForKey:@"HotDealId"]];
    if([defaults valueForKey:@"hotDealListID"])
        [requestStr appendFormat:@"<hotDealListId>%@</hotDealListId>",[defaults valueForKey:@"hotDealListID"]];
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></HotDealsDetails>",[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@hotdeals/hddetail",BASE_URL];
    
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    //[requestStr release];
    
}

// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case HDDETAIL:
            [self parse_hddetail:response];
            [self setFundraiserView];
            break;
        case HOT_DEAL_SHARE:
            [self parse_shareFundraiser:response];
            break;
            
        case HotRetSummary:
            [self parse_retSummary:response];
            break;
    }
    
}

// Parse Hot Deal Details
-(void)parse_hddetail:(NSString *)responseXml
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *hotDealsDetailsArrayLstElement = [TBXML childElementNamed:@"hotDealsDetailsArrayLst" parentElement:tbxml.rootXMLElement];
        TBXMLElement *hotDealDetailElement = [TBXML childElementNamed:@"HotDealsDetails" parentElement:hotDealsDetailsArrayLstElement];
        if(hotDealDetailElement)
        {
            //            TBXMLElement *apiLogoElement = [TBXML childElementNamed:@"apiPartnerImagePath" parentElement:hotDealDetailElement];
            TBXMLElement *productHotDealIDElement = [TBXML childElementNamed:@"productHotDealId" parentElement:hotDealDetailElement];
            
            //            TBXMLElement *cityElement = [TBXML childElementNamed:@"city" parentElement:hotDealDetailElement];
            
            TBXMLElement *hotDealNameElement = [TBXML childElementNamed:@"hotDealName" parentElement:hotDealDetailElement];
            TBXMLElement *hotDealLongDescriptionElement = [TBXML childElementNamed:@"hDLognDescription" parentElement:hotDealDetailElement];
            TBXMLElement *hotDealShortDescriptionElement = [TBXML childElementNamed:@"hDDesc" parentElement:hotDealDetailElement];
            TBXMLElement *imagePathElement = [TBXML childElementNamed:@"hotDealImagePath" parentElement:hotDealDetailElement];
            //            TBXMLElement *hotDealSaleElement = [TBXML childElementNamed:@"hDSalePrice" parentElement:hotDealDetailElement];
            //            TBXMLElement *hotDealSalePriceElement = [TBXML childElementNamed:@"hDPrice" parentElement:hotDealDetailElement];
            TBXMLElement *hotDealStartDateElement = [TBXML childElementNamed:@"hDStartDate" parentElement:hotDealDetailElement];
            
            
            TBXMLElement *hotDealEndDateElement = [TBXML childElementNamed:@"hDEndDate" parentElement:hotDealDetailElement];
            TBXMLElement *hotDealUrlElement = [TBXML childElementNamed:@"hdURL" parentElement:hotDealDetailElement];
            
            // New Tags for Promotions
            TBXMLElement *hotDealextFlagElement = [TBXML childElementNamed:@"extFlag" parentElement:hotDealDetailElement];
            
            TBXMLElement *hotDealclaimFlagElement = [TBXML childElementNamed:@"claimFlag" parentElement:hotDealDetailElement];
            
            TBXMLElement *hotDealredeemFlagElement = [TBXML childElementNamed:@"redeemFlag" parentElement:hotDealDetailElement];
            
            TBXMLElement *hotDealuserNameElement = [TBXML childElementNamed:@"userName" parentElement:hotDealDetailElement];
            
            TBXMLElement *hotDealretLocIDsElement = [TBXML childElementNamed:@"retLocIDs" parentElement:hotDealDetailElement];
            
            TBXMLElement *hotDealretLocsElement = [TBXML childElementNamed:@"retLocs" parentElement:hotDealDetailElement];
            
            TBXMLElement *hDExpDateElement = [TBXML childElementNamed:@"hDExpDate" parentElement:hotDealDetailElement];
            
            TBXMLElement *hDcoupCodeElement = [TBXML childElementNamed:@"coupCode" parentElement:hotDealDetailElement];
            
            TBXMLElement *hretNameElement = [TBXML childElementNamed:@"retName" parentElement:hotDealDetailElement];
            
            claimnredeemBuutonValue = 0;
            // Internal Deal
            if (hotDealextFlagElement!=nil && [[TBXML textForElement:hotDealextFlagElement] isEqualToString:@"0"])
            {
                // If no location associated to deal/retailer then will not show location button
                if(hotDealretLocsElement)
                    isLocationFlag = YES;
                else
                    isLocationFlag = NO;
                
                if ([[TBXML textForElement:hotDealclaimFlagElement] isEqualToString:@"1"] && [[TBXML textForElement:hotDealredeemFlagElement] isEqualToString:@"1"])
                {
                    // Redeem button will be deactive as its claimed and reemed both
                    
                    claimnredeemBuutonValue = 3;
                    
                }
                else if ([[TBXML textForElement:hotDealclaimFlagElement] isEqualToString:@"1"] && [[TBXML textForElement:hotDealredeemFlagElement] isEqualToString:@"0"])
                {
                    
                    claimnredeemBuutonValue = 2;
                }
                else if ([[TBXML textForElement:hotDealclaimFlagElement] isEqualToString:@"0"] && [[TBXML textForElement:hotDealredeemFlagElement] isEqualToString:@"0"])
                {
                    
                    claimnredeemBuutonValue = 1;
                }
                
            }
            else // External Deal
                isLocationFlag = NO;
            
            //hide the get deal button for expired deals
            TBXMLElement *expiredElement = [TBXML childElementNamed:@"expired" parentElement:hotDealDetailElement];
            
            if (expiredElement!=nil && [[TBXML textForElement:expiredElement]boolValue] == YES){
                
                isExpiredFlag = YES;
                //                btnGetDeal.hidden = YES;
            }
            else{
                isExpiredFlag = NO;
                //                btnGetDeal.hidden = NO;
            }
            
            
            if(hotDealretLocIDsElement)
                retLocIDs = [[TBXML textForElement:hotDealretLocIDsElement]copy];
            else
                retLocIDs = @"N/A";
            
            
            if(hretNameElement)
                retName = [[TBXML textForElement:hretNameElement]copy];
            else
                retName = @"N/A";
            
            if(hotDealretLocsElement)
                retLocs = [[TBXML textForElement:hotDealretLocsElement]copy];
            else
                retLocs = @"N/A";
            
            if(hotDealextFlagElement)
                extFlagValue = [[TBXML textForElement:hotDealextFlagElement]copy];
            else
                extFlagValue = @"N/A";
            
            if(hotDealredeemFlagElement)
                redeemFlagValue = [[TBXML textForElement:hotDealredeemFlagElement]copy];
            else
                redeemFlagValue = @"N/A";
            
            if(hotDealclaimFlagElement)
                claimFlagValue = [[TBXML textForElement:hotDealclaimFlagElement]copy];
            else
                claimFlagValue = @"N/A";
            
            if(hotDealuserNameElement)
                username = [[TBXML textForElement:hotDealuserNameElement]copy];
            else
                username = @"N/A";
            TBXMLElement *latitudeElement = [TBXML childElementNamed:@"latitude" parentElement:hotDealDetailElement];
            TBXMLElement *longitudeElement = [TBXML childElementNamed:@"longitude" parentElement:hotDealDetailElement];
            
            TBXMLElement *retailIdElement = [TBXML childElementNamed:@"retailId" parentElement:hotDealDetailElement];
            TBXMLElement *retLocImgLogoElement = [TBXML childElementNamed:@"retLocImgLogo" parentElement:hotDealDetailElement];
            
            TBXMLElement *retLocIdElement = [TBXML childElementNamed:@"retLocId" parentElement:hotDealDetailElement];
            
            if (latitudeElement) {
                latitude=[[NSString alloc]initWithString:[TBXML textForElement:latitudeElement]];
            }
            
            if (longitudeElement) {
                longitude = [[NSString alloc]initWithString:[TBXML textForElement:longitudeElement]];
            }
            
            
            if (retailIdElement) {
                retailId=[[NSString alloc]initWithString:[TBXML textForElement:retailIdElement]];
            }
            
            if (retLocImgLogoElement) {
                retLocImgLogo = [[NSString alloc]initWithString:[TBXML textForElement:retLocImgLogoElement]];
            }
            
            if (retLocIdElement) {
                retLocId=[[NSString alloc]initWithString:[TBXML textForElement:retLocIdElement]];
            }
            
            
            if(hDExpDateElement)
                hDExpDate = [[TBXML textForElement:hDExpDateElement]copy];
            else
                hDExpDate = @"N/A";
            
            if(hDcoupCodeElement){
                if ([coupCode length] >0) {
                    coupCode = [[TBXML textForElement:hDcoupCodeElement]copy];
                }
                
                else
                    coupCode = @"N/A";
            }
            
            if(hotDealStartDateElement)
                startDateString = [[TBXML textForElement:hotDealStartDateElement]copy];
            else
                startDateString = [[NSMutableString alloc]initWithString:@"N/A"];
            
            if(hotDealEndDateElement)
                endDateString = [[TBXML textForElement:hotDealEndDateElement]copy];
            else
                endDateString = [[NSMutableString alloc]initWithString:@"N/A"];
            
            if(hotDealLongDescriptionElement)
                description = [[TBXML textForElement:hotDealLongDescriptionElement]copy];
            
            if(hotDealShortDescriptionElement)
                shortDesc = [[TBXML textForElement:hotDealShortDescriptionElement]copy];
            
            if (hotDealUrlElement)
                url = [[TBXML textForElement:hotDealUrlElement]copy];
            
            //            if (apiLogoElement != nil)
            //                apiLogoUrl = [[TBXML textForElement:apiLogoElement] copy];
            DLog(@"url:%@", url);
            [defaults setObject:url forKey:KEY_HYPERLINK];
            
            
            
            if (hotDealNameElement && ![[TBXML textForElement:hotDealNameElement] isEqualToString:@"N/A"]){
                title = [[TBXML textForElement:hotDealNameElement] copy];
                //                hotDealName.textColor = [UIColor colorWithRGBInt:0x112e72];
            }
            
            
            imageStringValue= @"N/A";
            if(imagePathElement)
                imageStringValue = [[TBXML textForElement:imagePathElement] copy];
            
            
            hotDealId = 0;
            if(productHotDealIDElement)
                hotDealId = [[TBXML textForElement:productHotDealIDElement]copy];
            
            
            
            
        }
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
}



-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}



#pragma mark parse methods





-(void)urlButtonClicked:(id)sender{
    
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else{
        switch (claimnredeemBuutonValue)
        {
            case 0: //Get Deal
            {
                NSMutableString *urlString = [BASE_URL mutableCopy];
                if([defaults valueForKey:@"hotDealListID"])
                    //@Deepak: Added code for UserTracking for get Deal
                    [urlString appendFormat:@"hotdeals/utgethotdealclick?userId=%@&hotDealId=%@&hotDealListId=%@",[defaults valueForKey:KEY_USERID],hotDealId,[defaults valueForKey:@"hotDealListID"]];
                else
                    //@Deepak: Added code for UserTracking for get Deal
                    [urlString appendFormat:@"hotdeals/utgethotdealclick?userId=%@&hotDealId=%@",[defaults valueForKey:KEY_USERID],hotDealId];
                
                
                NSString *responseXml = [ConnectionManager establishGetConnection:urlString];
                DLog(@"%@",responseXml);
                
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                [defaults setValue:url forKey:KEY_URL];
                WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
                
            }
                break;
                
            case 1: // Claim
            {
                NSMutableString *reqStr = [[NSMutableString alloc] init];
                [reqStr appendFormat:@"<HotDealsListRequest><userId>%@</userId><hotDealId>%@</hotDealId>",[defaults valueForKey:KEY_USERID],hotDealId];
                if ([defaults valueForKey:KEY_MAINMENUID]) {
                    [reqStr appendFormat:@"<mainMenuId>%@</mainMenuId></HotDealsListRequest>",[defaults valueForKey:KEY_MAINMENUID]];
                }else{
                    [reqStr appendFormat:@"</HotDealsListRequest>"];
                }
                NSMutableString *urlString1 = [BASE_URL mutableCopy];
                [urlString1 appendString:@"hotdeals/hotdealclaim"];
                
                NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString1 withParam:reqStr]];
                
                TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
                
                ReleaseAndNilify(responseXml);
                TBXMLElement *responseTextElement= [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
                TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
                
                if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"])
                {
                    claimnredeemBuutonValue = 2;
                    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                        [fundraiserPurchaseProductslink setTitle:@"Redeem" forState:UIControlStateNormal];
                    }
                    else
                    {
                        [fundraiserPurchaseProductslink setTitle:@"Redeem" forState:UIControlStateNormal];
                    }
                }
                else
                {
                    [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
                }
                
                
                //[reqStr release];
                
            }
                break;
                
            case 2: // redeem
            {
                //if(iHotDealRedeemViewController)
                //   [iHotDealRedeemViewController release];
                
                iHotDealRedeemViewController = [[HotDealRedeemViewController alloc]initWithNibName:@"HotDealRedeemViewController" bundle:[NSBundle mainBundle]];
                
                if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
                    iHotDealRedeemViewController.view.frame = CGRectMake(SCREEN_WIDTH/4,SCREEN_HEIGHT/4,SCREEN_WIDTH/2,SCREEN_HEIGHT/2);
                }
                else
                {
                    
                    if (IS_IPHONE5)
                        iHotDealRedeemViewController.view.frame = CGRectMake(10, 10, 300, 425);
                    else
                        iHotDealRedeemViewController.view.frame = CGRectMake(10, 10, 300, 350);
                }
                
                
                iHotDealRedeemViewController.view.layer.backgroundColor = [UIColor whiteColor].CGColor;
                iHotDealRedeemViewController.view.layer.cornerRadius = 20.0;
                iHotDealRedeemViewController.view.layer.borderColor = [UIColor blackColor].CGColor;
                iHotDealRedeemViewController.view.layer.shadowOffset = CGSizeMake(1, 0);
                iHotDealRedeemViewController.view.layer.shadowColor = [[UIColor blackColor] CGColor];
                iHotDealRedeemViewController.view.layer.shadowRadius = 5;
                iHotDealRedeemViewController.view.layer.shadowOpacity = .25;
                
                
                
                if (![startDateString isEqualToString:@"N/A"]) {
                    iHotDealRedeemViewController.strStartDate = [[NSString alloc]initWithString:startDateString];
                    iHotDealRedeemViewController.StartDate.text = @"Start Date :";
                }
                else{
                    
                    if (![endDateString isEqualToString:@"N/A"]){
                        iHotDealRedeemViewController.strStartDate = [[NSString alloc]initWithString:endDateString];
                        iHotDealRedeemViewController.StartDate.text = @"End Date :";
                    }
                    
                    else if (![hDExpDate isEqualToString:@"N/A"]){
                        iHotDealRedeemViewController.strStartDate = [[NSString alloc]initWithString:hDExpDate];
                        iHotDealRedeemViewController.StartDate.text = @"Expiration :";
                    }
                    
                    else if (![coupCode isEqualToString:@"N/A"]){
                        iHotDealRedeemViewController.strStartDate = [[NSString alloc]initWithString:coupCode];
                        iHotDealRedeemViewController.StartDate.text = @"Code :";
                    }
                }
                
                
                
                
                if (![endDateString isEqualToString:@"N/A"]) {
                    iHotDealRedeemViewController.strEndDate = [[NSString alloc]initWithString:endDateString];
                    iHotDealRedeemViewController.EndDate.text = @"End Date :";
                }
                else{
                    if (![hDExpDate isEqualToString:@"N/A"]){
                        iHotDealRedeemViewController.strEndDate = [[NSString alloc]initWithString:hDExpDate];
                        iHotDealRedeemViewController.EndDate.text = @"Expiration :";
                    }
                    
                    else if (![coupCode isEqualToString:@"N/A"]){
                        iHotDealRedeemViewController.strEndDate = [[NSString alloc]initWithString:coupCode];
                        iHotDealRedeemViewController.EndDate.text = @"Code :";
                    }
                    
                }
                
                
                
                
                if (![hDExpDate isEqualToString:@"N/A"] && ![endDateString isEqualToString:@"N/A"]) {
                    iHotDealRedeemViewController.strExpiration = [[NSString alloc]initWithString:hDExpDate];
                    iHotDealRedeemViewController.Expiration.text = @"Expiration :";
                }
                else{
                    if (![coupCode isEqualToString:@"N/A"] && ![hDExpDate isEqualToString:@"N/A"]){
                        iHotDealRedeemViewController.strExpiration = [[NSString alloc]initWithString:coupCode];
                        iHotDealRedeemViewController.Expiration.text = @"Code :";
                    }
                }
                
                
                
                
                if (![coupCode isEqualToString:@"N/A"]) {
                    iHotDealRedeemViewController.strCode = [[NSString alloc]initWithString:coupCode];
                    iHotDealRedeemViewController.Code.text = @"Code :";
                }
                
                else{
                    
                    
                }
                
                
                //            iHotDealRedeemViewController.strStartDate = [[NSString alloc]initWithString:startDateString];
                //            iHotDealRedeemViewController.strEndDate = [[NSString alloc]initWithString:endDateString];;
                iHotDealRedeemViewController.strDescription =[[NSString alloc]initWithString:description];
                //            iHotDealRedeemViewController.strExpiration = [[NSString alloc]initWithString:hDExpDate];
                //            iHotDealRedeemViewController.strCode = [[NSString alloc]initWithString:coupCode];
                iHotDealRedeemViewController.strUserName = [[NSString alloc]initWithString:username];
                iHotDealRedeemViewController.strhotDealId = [[NSString alloc]initWithString:hotDealId];
                iHotDealRedeemViewController.objClass = self;
                
                [self.view addSubview:iHotDealRedeemViewController.view];
                
                
            }
                break;
                
            case 3: // Already Redeemed
            {
                
            }
                break;
                
            default:
                break;
        }
    }
}


-(void)locationButtonClicked:(id)sender
{
    
    
    tblEventHolder=[[UIView alloc]init];
    if (IS_IPHONE5)
        tblEventHolder.frame = CGRectMake(10, 10, 300, 425);
    else if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        tblEventHolder.frame = CGRectMake(80, 60, 600, 700);
    else
        tblEventHolder.frame = CGRectMake(10, 10, 300, 350);
    
    tblEventHolder.layer.backgroundColor = [UIColor whiteColor].CGColor;
    tblEventHolder.layer.cornerRadius = 20.0;
    tblEventHolder.layer.borderColor = [UIColor blackColor].CGColor;
    tblEventHolder.layer.shadowOffset = CGSizeMake(1, 0);
    tblEventHolder.layer.shadowColor = [[UIColor blackColor] CGColor];
    tblEventHolder.layer.shadowRadius = 5;
    tblEventHolder.layer.shadowOpacity = .25;
    
    
    
    
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
    {
        cross_button=[[UIButton alloc]initWithFrame:CGRectMake(tblEventHolder.frame.size.width-75,-3, 75, 55)];
        [cross_button setImage:[UIImage imageNamed:@"cross_icon_ipad.png"] forState:UIControlStateNormal];
    }
    else
    {
        cross_button=[[UIButton alloc]initWithFrame:CGRectMake(250,-5, 65, 45)];
        [cross_button setImage:[UIImage imageNamed:@"cross_icon.png"] forState:UIControlStateNormal];
    }
    
    [cross_button addTarget:self action:@selector(crossClicked) forControlEvents:UIControlEventTouchDown];
    
    strDealName = [[NSString alloc]initWithString:title];
    arrLocations = [[NSArray alloc]initWithArray:[retLocs componentsSeparatedByString:@"|"]];
    strRetailerName = [[NSString alloc]initWithString:retName];
    
    if(![retLocImgLogo isEqualToString:@"N/A"] && retLocImgLogo)
        imagePath=[[NSString alloc]initWithString:retLocImgLogo];
    
    tblRetailsLocation=[[UITableView alloc]init];
    if (IS_IPHONE5)
        tblRetailsLocation = [[UITableView alloc]initWithFrame:CGRectMake(20 , 106, 280, 236) style:UITableViewStylePlain];
    else if ( DEVICE_TYPE==UIUserInterfaceIdiomPad )
        tblRetailsLocation = [[UITableView alloc]initWithFrame:CGRectMake(10, 200, 580, 600) style:UITableViewStylePlain]; //80 @bindu
    else
        tblRetailsLocation = [[UITableView alloc]initWithFrame:CGRectMake(10, 30, 280, 270) style:UITableViewStylePlain];
    
    int gap=0;
    
    if ( DEVICE_TYPE==UIUserInterfaceIdiomPad ){
        gap=15;
        lblRetailerName = [[UILabel alloc]initWithFrame:CGRectMake(15,tblEventHolder.frame.origin.y+cross_button.frame.size.height-20, 550, 40)];//-20 by bindu
        lblDealName = [[UILabel alloc]initWithFrame:CGRectMake(15, lblRetailerName.frame.origin.y+lblRetailerName.frame.size.height+gap, 550, 40)];
    }
    else{
        gap=10;
        lblRetailerName = [[UILabel alloc]initWithFrame:CGRectMake(25,tblEventHolder.frame.origin.y+cross_button.frame.size.height, 250, 40)];
        lblDealName = [[UILabel alloc]initWithFrame:CGRectMake(10, lblRetailerName.frame.origin.y+lblRetailerName.frame.size.height+gap, 250, 40)];
    }
    
    NSString *str= [[NSString alloc]initWithFormat:@"Retailer : %@",strRetailerName];
    
    [lblRetailerName setText:str];
    [lblDealName setText:strDealName];
    tblRetailsLocation.dataSource=self;
    tblRetailsLocation.delegate=self;
    [tblRetailsLocation setBackgroundColor:[UIColor clearColor]];
    [tblEventHolder addSubview:lblRetailerName];
    [tblEventHolder addSubview:lblDealName];
    [tblEventHolder addSubview:cross_button];
    [tblEventHolder addSubview:tblRetailsLocation];
    [self.view addSubview:tblEventHolder];
    
    
    
}

- (void)crossClicked:(id)sender
{
    [self.view removeFromSuperview];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrLocations count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //	UITableViewCell *cell2;
    static NSString *CellIdentifier = @"CellRetailerLocation";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i =0; i < [arr count]; i++)
        {
            UIView *v = (UIView*)[arr objectAtIndex:i];
            [v removeFromSuperview];
        }
    }
    
    SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
    asyncImageView.backgroundColor = [UIColor clearColor];
    [asyncImageView loadImage:imagePath];
    [asyncImageView setBackgroundColor:[UIColor grayColor]];
    asyncImageView.translatesAutoresizingMaskIntoConstraints=NO;
    [cell.contentView  addSubview:asyncImageView];
    
    UILabel *lblLocation = [[UILabel alloc]initWithFrame:CGRectMake(50, 0, 170, 50)];
    lblLocation.backgroundColor = [UIColor clearColor];
    lblLocation.font = [UIFont systemFontOfSize:12];
    lblLocation.numberOfLines = 3;
    lblLocation.lineBreakMode=NSLineBreakByCharWrapping;
    lblLocation.text =[arrLocations objectAtIndex:indexPath.row];
    [cell.contentView addSubview:lblLocation];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (IBAction)crossButtonPressed:(id)sender
{
    [self.view removeFromSuperview];
}

#pragma mark webview delegate methods



//- (void)webViewDidFinishLoad:(UIWebView *)webView {
//    int height = [[fundraiserLongDescription stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"] integerValue];
//
//        DLog(@"%d",height);
//}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    
    if ([[[inRequest URL] scheme] isEqual:@"mailto"]) {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,
                                                                                                                   (CFStringRef)value,
                                                                                                                   CFSTR("")));
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            //[mailViewController release];
            return NO;
            
        }
        
        else {
            [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
            }
        
        return NO;
        
    }
    
    
    if ([[inRequest URL] fragment]) {
        
        return YES;
        [inWeb stringByEvaluatingJavaScriptFromString:@"window.location.hash='#hashtag'"];
        
        return YES;
    }
    
    
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        
       // NSString *url = [[inRequest URL]absoluteString];
        //        [defaults setValue:url forKey:KEY_URL];
        [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
        
        WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:urlDetail animated:NO];
        //[urlDetail release];
        
        return NO;
    }
    
    return YES;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    latitude=@"30.578245";
    //    longitude=@"-98.272800";
    
    //    HotDealMapViewController *mapViewController =[[HotDealMapViewController alloc]initWithNibName:@"HotDealMapViewController" bundle:[NSBundle mainBundle]];
    //    mapViewController.retLocId=[[NSString alloc]initWithString:retLocId];
    //    mapViewController.retailId=[[NSString alloc]initWithString:retailId];
    //    mapViewController.arrLocations = [[NSArray alloc]initWithArray:arrLocations];
    //    mapViewController.strRetailerName = [[NSString alloc]initWithString:strRetailerName];
    //    [self.navigationController pushViewController:mapViewController animated:NO];
    //    [mapViewController release];
    
    [self pullAppSite];
}


-(void) pullAppSite
{
    
    iWebRequestState = HotRetSummary;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",retLocId];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && latitude){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",longitude,latitude]
        ;
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",retailId];
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    NSString *urlString =  [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);
}

-(void)parse_retSummary:(NSString *)response
{
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else
    {
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        //        rsvc.distanceFromPreviousScreen = [arrDistance objectAtIndex:objIndex];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
    
}


-(void)crossClicked
{
    [tblEventHolder removeFromSuperview];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];// ModalViewControllerAnimated:YES];
}



-(int)calculateLabelHeight:(NSString*)text labelWidth:(int)labelWidth fontSize:(int)fontSize
{
    CGSize constraint;
    CGSize size ;
    CGFloat height ;
    constraint = CGSizeMake(labelWidth, 20000.0f);
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    size = [text boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:fontSize], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    
    height = MAX(size.height+20, 40.0f);
    return height;
}



-(IBAction)DetailsButtonClicked:(id)sender
{
    UIButton *btnEvent = (UIButton*)sender;
    
    //    FundraiserDeatilsDO *iEventDetailsDO = [[FundraiserDeatilsDO alloc]init];
    //    iEventDetailsDO = [arrFundraisingDetails objectAtIndex:0];
    
    switch (btnEvent.tag)
    {
            
        case 1: // More Info
        {
            WebBrowserViewController *iWebBrowserViewController = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            iWebBrowserViewController.hDLongDesc = [[NSString alloc]initWithString:description];
            iWebBrowserViewController.title = [[NSString alloc]initWithString:title];
            [self.navigationController pushViewController:iWebBrowserViewController animated:NO];
            // [iWebBrowserViewController release];
        }
            break;
            
            
        case 3: // More Info
        {
            WebBrowserViewController *iWebBrowserViewController = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            //            [defaults setValue:iEventDetailsDO.purchasedProducts forKey:KEY_URL];
            [self.navigationController pushViewController:iWebBrowserViewController animated:NO];
            // [iWebBrowserViewController release];
        }
            break;
            
            
        default:
            break;
    }
}

#pragma mark sender user tracking
-(void)userTracking:(NSString*)xmlStr
{
    
    NSMutableString *urlStr = [[NSMutableString alloc]init];
    [urlStr appendFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    
    NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:xmlStr]];
    DLog(@"%@",response);
    //[xmlStr release];
}



#pragma mark share events methods

-(void)shareClicked
{
    iWebRequestState = HOT_DEAL_SHARE;
    
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    
    
    [reqStr appendFormat:@"<ShareProductInfo><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [reqStr appendFormat:@"<hotdealId>%@</hotdealId>",[defaults valueForKey:@"HotDealId"]];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharehotdeal",BASE_URL];
    
    //  [ConnectionManager establishConnectionFor:reqStr base:urlString withDelegate:self];
    
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    
    [self responseData:response];
    // [reqStr release];
    
}


-(void)shareDetails{
    
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        
        [self shareClicked];
        
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        // [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
    }
    
}

-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Hot Deal Details Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    //    if(IPAD){
    //        actionSheet.popoverPresentationController.sourceView = btnCalendar;
    //        actionSheet.popoverPresentationController.sourceRect =  btnCalendar.bounds;
    //    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"HOT_DEAL_SHARE"];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
                NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><fundId>%@</fundId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:FUNDRAISERID]];
                
                [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
        
        
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    //[defaults setValue:FUNDRAISER_SHARE forKey:KEY_SHARE_TYPE];
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"HOT_DEAL_SHARE"];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
}




-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}
- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
            NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><fundId>%@</fundId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:FUNDRAISERID]];
            
            [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
            }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)parse_shareFundraiser:(NSString*)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        
        TBXMLElement *shareText=[TBXML childElementNamed:@"hotDealName" parentElement:tbXml.rootXMLElement];
        TBXMLElement *qrURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *hotDealImagePath = [TBXML childElementNamed:@"hotDealImagePath" parentElement:tbXml.rootXMLElement];
        
        if (qrURL!=nil) {
            [defaults setValue:[TBXML textForElement:qrURL] forKey:KEY_HYPERLINK];
            NSLog(@"The link to be shared is %@",KEY_HYPERLINK);
        }
        
        if (shareText!=nil) {
            NSMutableString *msgString = [[NSMutableString alloc]initWithString:@"I found this Hot Deal @HubCiti and thought you might be interested:"];
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [msgString appendFormat:@"%@",shareString];
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            NSLog(@"The share message value is %@",KEY_SHAREMSG);
        }
        
        if (hotDealImagePath!=nil) {
            [defaults setObject:[TBXML textForElement:hotDealImagePath] forKey:KEY_PRODUCTIMGPATH];
            NSLog(@"The product image path is %@",KEY_PRODUCTIMGPATH);
        }
        
        
        
        
        
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            return;
        }
        
    }
}


@end


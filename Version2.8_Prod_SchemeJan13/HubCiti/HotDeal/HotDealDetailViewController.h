//
//  HotDealDetailViewController.h
//  HubCiti
//
//  Created by Kitty on 01/04/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "RetailerLocationsViewController.h"
#import "HotDealRedeemViewController.h"
#import "DWBubbleMenuButton.h"
@class AnyViewController;
@class EmailShareViewController;
typedef enum webServicesHotDeal
{
    HDDETAIL,
    HOT_DEAL_SHARE,
    HotRetSummary
}webServicesHotDealState;


@interface HotDealDetailViewController : UIViewController<UITextViewDelegate , MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,DWBubbleMenuViewDelegate,CustomizedNavControllerDelegate>
{
    webServicesHotDealState iWebRequestState;
    RetailerLocationsViewController *iRetailerLocationsViewController;
    HotDealRedeemViewController *iHotDealRedeemViewController;
    UIScrollView *fundraiserView;
    
    UIImage *fundraiserImagePath;
    
    UILabel *fundraiserTitle;
    UILabel *fundraiserStartDate;
    UILabel *fundraiserEndDate;
    UILabel *fundraiserShortDescription;
    UIWebView *fundraiserLongDescription;
    UILabel *fundraiserCurrentLevel;
    UILabel *fundraiserPurchaseProducts;
    
    UILabel *fundraiserGoal;
    
    UIButton *btnGetDeal;
    
    UIButton *moreInformation;
    UIButton *eventInformation;
    
    NSMutableArray *arrFundraisingDetails;
    
    NSMutableString *productId;
    int prodIndex;
    NSString *prodDescStringValue;
    NSString *url;
    NSString *imageStringValue;
    NSMutableString *startDateString, *endDateString;
    NSString *apiLogoUrl;
    NSString *hotDealId;
    NSString *extFlagValue;
    NSString *claimFlagValue;
    NSString *redeemFlagValue;
    NSString *username;
    NSString *hDExpDate;
    NSString *coupCode;
    NSString *description,*shortDesc;
    NSString *retLocIDs;
    NSString *retLocs;
    NSString *retName,*title;
    
    NSString *latitude,*longitude;
    
    NSString *retailId, *retLocImgLogo, *retLocId;
    
    UIButton    *cross_button;
    UIView  *tblEventHolder;
    
    NSString *strDealName;
    NSString *strRetailerName;
    NSArray *arrLocations;
    
    UILabel *lblRetailerName;
    UILabel *lblDealName;
    UITableView *tblRetailsLocation;
    
    UITableView *tblLocations;
     NSString *imagePath;
    int isLocationFlag,isExpiredFlag;

}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic,readwrite)int claimnredeemBuutonValue;
@property (nonatomic , strong)UIButton *fundraiserPurchaseProductslink;
@end

//
//  RetailerLocationsViewController.m
//  Scansee
//
//  Created by ionnor on 6/20/13.
//
//

#import "RetailerLocationsViewController.h"

@interface RetailerLocationsViewController ()

@end

@implementation RetailerLocationsViewController

@synthesize strDealName;
@synthesize strRetailerName;
@synthesize arrLocations;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    lblDealName.text =strDealName;
    lblDealName.font = [UIFont systemFontOfSize:12];
    lblRetailerName.text = strRetailerName;
    lblRetailerName.font = [UIFont systemFontOfSize:12];
    tblRetailsLocation.frame = CGRectMake(20, 150, 250, 200);
    [tblRetailsLocation reloadData];

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrLocations count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //	UITableViewCell *cell2;
    static NSString *CellIdentifier = @"CellRetailerLocation";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i =0; i < [arr count]; i++)
        {
            UIView *v = (UIView*)[arr objectAtIndex:i];
            [v removeFromSuperview];
        }
    }
    
    
    UILabel *lblLocation = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 250, 50)];
    lblLocation.backgroundColor = [UIColor clearColor];
    lblLocation.font = [UIFont systemFontOfSize:12];
    lblLocation.numberOfLines = 3;
    lblLocation.lineBreakMode=NSLineBreakByCharWrapping;
    lblLocation.text =[arrLocations objectAtIndex:indexPath.row];
    [cell.contentView addSubview:lblLocation];
    

    return cell;
}

- (IBAction)crossButtonPressed:(id)sender
{
    [self.view removeFromSuperview];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

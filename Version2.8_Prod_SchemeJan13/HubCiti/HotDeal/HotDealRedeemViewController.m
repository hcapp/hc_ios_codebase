//
//  HotDealRedeemViewController.m
//  Scansee
//
//  Created by ionnor on 6/19/13.
//
//

#import "HotDealRedeemViewController.h"
#import "HotDealDetailViewController.h"

@interface HotDealRedeemViewController ()

@end

@implementation HotDealRedeemViewController


@synthesize strStartDate;
@synthesize strEndDate;
@synthesize strDescription;
@synthesize strCode;
@synthesize strExpiration;
@synthesize strUserName;
@synthesize strhotDealId;
@synthesize objClass;

@synthesize StartDate,EndDate,Expiration,Code;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    lblStartDate.font = [UIFont systemFontOfSize:12];
    lblEndDate.font = [UIFont systemFontOfSize:12];

    lblExpiration.font = [UIFont systemFontOfSize:12];
    lblCode.font = [UIFont systemFontOfSize:12];
    
    lblStartDate.text = strStartDate;
    lblEndDate.text = strEndDate;
    
    [txtDescription loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#000;'>%@",strDescription] baseURL:nil];
    txtDescription.backgroundColor = [UIColor clearColor];
    txtDescription.layer.cornerRadius = 5.0;
    txtDescription.layer.borderWidth = 1.0f;
    txtDescription.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [[txtDescription.subviews objectAtIndex:0] setBounces:NO];
    
    
    lblExpiration.text = strExpiration;
    lblCode.text = strCode;
}

-(IBAction)redeemButtonClicked:(id)sende
{
    UIAlertController * alert;
    
    alert=[UIAlertController alertControllerWithTitle:@"Confirmation" message:NSLocalizedString(@"Are you sure you want to redeem?",@"Are you sure you want to redeem?") preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* no = [UIAlertAction
                         actionWithTitle:@"No"
                         style:UIAlertActionStyleCancel
                         handler:nil];
    [alert addAction:no];
    UIAlertAction* yes = [UIAlertAction
                             actionWithTitle:@"Yes"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action){
                                  [self redeemHotDeal];
                             }];
    [alert addAction:yes];
    
    [self presentViewController:alert animated:YES completion:nil];
}



// Method to call the redeem web service and redeem the deal
-(void)redeemHotDeal
{
     NSMutableString *reqStr = [[NSMutableString alloc] init];
     [reqStr appendFormat:@"<HotDealsListRequest><hotDealId>%@</hotDealId><userId>%@</userId>",strhotDealId,[defaults valueForKey:KEY_USERID]];
    if ([defaults valueForKey:KEY_MAINMENUID]) {
        [reqStr appendFormat:@"<mainMenuId>%@</mainMenuId></HotDealsListRequest>",[defaults valueForKey:KEY_MAINMENUID]];
    }else{
        [reqStr appendFormat:@"</HotDealsListRequest>"];
    }
     NSMutableString *urlString1 = [BASE_URL mutableCopy];
     [urlString1 appendString:@"hotdeals/hotdealredeem"];
     
     NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString1 withParam:reqStr]];
     
     TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    ReleaseAndNilify(responseXml);
     
     TBXMLElement *responseTextElement= [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
     TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    
     if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"])
     {
         // Success
         //to check for refreshing the list when coming from gallery
         [SharedManager setRefreshGallery:YES];
         
         HotDealDetailViewController *iHotDealsProductDetailDealMapViewController = (HotDealDetailViewController*)objClass;
         iHotDealsProductDetailDealMapViewController.claimnredeemBuutonValue = 3;
         iHotDealsProductDetailDealMapViewController.fundraiserPurchaseProductslink.enabled=NO;
         [self.view removeFromSuperview];
         //[self.navigationController popViewControllerAnimated:NO];
     }
     else
     {
         [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
              //  alert.tag = 99;
     }
     
   //  [reqStr release];

}



- (IBAction)crossButtonPressed:(id)sender
{
    [self.view removeFromSuperview];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

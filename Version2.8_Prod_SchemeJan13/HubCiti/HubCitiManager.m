//
//  HubCitiManager.m
//  HubCiti
//
//  Created by Anjana on 9/13/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "HubCitiManager.h"

static HubCitiManager *sharedHubCitiManager = nil;

@implementation HubCitiManager

@synthesize responseXmlString;
@synthesize wishList, list, termsOfUse, gps_allow_flag ,hotDealToolBar ,wishListProdInfo,Wishcolor,shareFromTL ,addToWL, HotDealsplash;
@synthesize refreshProdPage,userInfoFromSignup,myGallery,refreshList,timeFlag;
@synthesize shareEmailCoupon,shareEmailHotDeal,enableAddCoupon,refreshCategories,shareEmailSpecialOffer,shareEmailSpecialOfferUrl,shareAppsite,refMngLoyTable;
@synthesize firstTimeCancel,smartSearchStatus,fromSL,callScanner,addRemLoyaltyCard,enableRedeemCoupon,couLoySearchActive,refreshGallery;
@synthesize filtersCount,refreshAustinRetailers;
@synthesize segmentIndex,typeIndex;
@synthesize dealsCouponDisplay, dealsHotDealDisplay;
@synthesize retAffId, retAffId_Cat, retGroupId, retGroupId_Cat;
@synthesize localSpecilas, sessionId,scrollIndicator,submenuScrollIndicator;

#pragma mark Singleton Methods

+ (id)sharedManager {
    @synchronized(self) {
        if(sharedHubCitiManager == nil)
            sharedHubCitiManager = [[super allocWithZone:NULL] init];
    }
    return sharedHubCitiManager;
}
+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedManager] retain];
}
- (id)copyWithZone:(NSZone *)zone {
    return self;
}
- (id)retain {
    return self;
}
- (unsigned long)retainCount {
    return UINT_MAX; //denotes an object that cannot be released
}
- (oneway void)release {
    // never release
}
- (id)autorelease {
    return self;
}
- (id)init {
    if (self = [super init]) {
        
        userInfoFromSignup = NO;
        filtersCount = 0;
        refreshAustinRetailers = NO;
        gps_allow_flag = NO;
        refreshCategories = NO;
        segmentIndex = 0;//default is coupons
        typeIndex = 0; //default is claimed
        
        retAffId = 0;
        retAffId_Cat = 0;
        retGroupId = 0;
        retGroupId_Cat = 0;
        localSpecilas=0;
    }
    return self;
}

- (void)dealloc {
    [super dealloc];
}


@end

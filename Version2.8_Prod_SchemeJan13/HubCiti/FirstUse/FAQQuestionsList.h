//
//  FAQQuestionsList.h
//  HubCiti
//
//  Created by Ajit Nadig on 3/20/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FAQCategoryList.h"
#import "FaqDetailsDo.h"
#define EXTENDED_HEIGHT 144
#define NORMAL_HEIGHT 60
#define WIDTH_OFFSET  15
#define TEXTVIEW_HEIGHT  80
#define LABEL_HEIGHT_OFFSET 20


typedef enum webServices{
    faqdisplay
}WebServiceType;

@interface FAQDO : NSObject{
    
    int faqId;
    NSString *question;
}
@property (readwrite) int faqId;
@property (nonatomic, strong) NSString *question;

@end

@interface FAQQuestionsList : UIViewController<UITableViewDelegate,UITableViewDataSource,CustomizedNavControllerDelegate>{
    
   IBOutlet UITableView *faqTableView;
    UITextView *cellTextView;
    NSMutableArray *faqObjArray;
    NSString *answer;
    int nextPage, lastRecord,selectedRow,selectedfaqId;
    WebServiceType iWebServiceType;
}

@property (nonatomic,strong ) NSMutableArray *answerArray;

@end

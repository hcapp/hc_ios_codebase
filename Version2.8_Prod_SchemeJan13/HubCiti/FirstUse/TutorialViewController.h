//
//  TutorialViewController.h
//  HubCiti
//
//  Created by service on 8/17/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController<UIPageViewControllerDataSource>
{
    LoginDO *login_DO;
    SignupDO *signupDO;
}
@property (strong, nonatomic) UIPageViewController *pageController;
@property (assign, nonatomic) NSString* viewType;
@end

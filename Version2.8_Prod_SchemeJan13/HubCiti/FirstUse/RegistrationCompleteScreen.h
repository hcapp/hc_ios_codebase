//
//  RegistrationCompleteScreen.h
//  HubCiti
//
//  Created by Anjana on 9/12/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

/*****************************************************
 Class designed to display registration success message
*****************************************************/

#import <UIKit/UIKit.h>

@interface RegistrationCompleteScreen : UIViewController<UIWebViewDelegate>
{
    UIWebView *webView;
    UIActivityIndicatorView *activityIndicator;
    UIButton *getStarted;
    NSMutableArray *arrImages;
    UIToolbar *bottomToolBar;
}

@end

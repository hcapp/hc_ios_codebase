//
//  PreferredCategoriesScreen.m
//  HubCiti
//
//  Created by Anjana on 9/12/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "PreferredCategoriesScreen.h"
#import "RegistrationCompleteScreen.h"
#import "MainMenuViewController.h"
#import "HubCitiManager.h"
#import "DealHotDealsList.h"
#import "DealCouponsViewController.h"
#import "CityPreferenceViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@interface PreferredCategoriesScreen ()
{
    /* anjana */
    HubCitiSegmentedControl *viewSelectionControl;
}

@end

@implementation PreferredCategoriesScreen
@synthesize categoryList;


#pragma mark- View lifecycle methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    [defaults setBool:FALSE forKey:@"PopbackRemoveAlert"];
    
    [categoryList setAccessibilityLabel:@"categoryTable"];
    [categoryList setAccessibilityIdentifier:@"categoryList"];
    
    /***** label to be displayed on right of navigatin bar*******/
    UILabel *titleLabel ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 320, 40)];
        titleLabel.font = [UIFont boldSystemFontOfSize:16];
    }
    else
    {
        titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, SCREEN_WIDTH, 60)];
        titleLabel.font = [UIFont boldSystemFontOfSize:20];
    }
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"Tell Us What You Like" forView:self withHambergur:NO];
    
  
    ReleaseAndNilify(titleLabel);
    
    /* Anjana start */
    NSArray *segmentItems = [NSArray arrayWithObjects:@"Categories",@"Products", nil];
    viewSelectionControl = [[HubCitiSegmentedControl alloc] initWithItems:segmentItems];
    [viewSelectionControl setSelectedSegmentIndex:0];
    [viewSelectionControl setSegmentColor];
    [viewSelectionControl addTarget:self action:@selector(viewSelectionChanged:) forControlEvents:UIControlEventValueChanged];
    [viewSelectionControl setBackgroundColor:[UIColor blackColor]];
    viewSelectionControl.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:viewSelectionControl];
    [self setConstraints:nil tag:1 heightIphone:0 heightIpad:0];
    
    /*Anjana end */
    
    //Create mainCategoryNameArray to hold the categories from server
    mainCategoryNameArray = [[NSMutableArray alloc]init];
    parCatIdArray = [[NSMutableArray alloc] init];
    
    //add save (or) next barButonItem
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    flexibleSpace.width = 247.0;
    customItem = [[UIBarButtonItem alloc] init];
    customItem.style = UIBarButtonItemStylePlain;
    NSArray *items;
    
    //customize back button
   
    
    
    //toolbar.barStyle = UIBarStyleDefault;
    //[toolbar setBackgroundColor:[UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]]];
    // toolbar.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    toolbar.barTintColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    NSLog(@"%@",[defaults valueForKey:@"titleBkGrdColor"]);
    if ([SharedManager userInfoFromSignup] == NO){
        
        
       

        
        //        UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
        //        [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
        //        UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
        //        self.navigationItem.rightBarButtonItem = mainPage;
        //        [ self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"MainMenu"];
        //        ReleaseAndNilify(mainPage);
        // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:CGRectMake(0, 0, 30, 30)];
        SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
        [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
        [btn addSubview:homeImage];
        
        
        [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
        mainPage.style = UIBarButtonItemStylePlain;
        
        self.navigationItem.rightBarButtonItem = mainPage;
        ////[mainPage release];
        
        
         self.navigationItem.hidesBackButton = YES;
        
        
        UIButton *saveBtn = [UtilityManager customizeBarButtonItem:@"Save"];
        [saveBtn setAccessibilityLabel:@"save"];
        [saveBtn addTarget:self action:@selector(savePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [customItem setCustomView:saveBtn];
        
        items = [NSArray arrayWithObjects: flexibleSpace, customItem, nil];
        /************ parse response got from previous screen **************/
        [self parseGetPreferredLocations:[defaults objectForKey:KEY_RESPONSEXML]];
        
        
    }
    else {
        
        UIButton *nextBtn = [UtilityManager customizeBarButtonItem:@"Next"];
        [nextBtn addTarget:self action:@selector(navigateToNextPage:) forControlEvents:UIControlEventTouchUpInside];
        
        [customItem setCustomView:nextBtn];
        self.navigationItem.hidesBackButton = YES;
        
       
        [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
        self.navigationItem.leftBarButtonItem = nil;
        //customize bottom back button
        UIButton *backBottomBtn = [UtilityManager customizeBarButtonItem:@"Back"];
        
        [backBottomBtn addTarget:self action:@selector(popBackfromBottom) forControlEvents:UIControlEventTouchUpInside];
        //        else if([RegionApp isEqualToString:@"0"])
        //             [backBottomBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* backBottomBarBtn = [[UIBarButtonItem alloc] initWithCustomView:backBottomBtn];
        
        
        
        items = [NSArray arrayWithObjects:backBottomBarBtn,flexibleSpace,  customItem, nil];
        /************ send request to get the user details from server **************/
        iWebRequestState = GETFAVLOCATIONS;
        [self requestToGetPreferredLocations];
        ReleaseAndNilify(backBottomBarBtn);
        
    }
    
    [toolbar setItems:items animated:NO];
    ReleaseAndNilify(flexibleSpace);
    ReleaseAndNilify(customItem );
    
    
    
    
    /***** Logic to prevent View from expanding for iOS7 version *******/
    if([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
}


-(void)viewWillAppear:(BOOL)animated{
    //[super viewWillAppear:animated];
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    if ([SharedManager userInfoFromSignup] == NO){
        
        
        cusNav.customNavBardelegate = self;
        [cusNav hideHambergerButton: YES];
        [cusNav hideBackButton:NO];
        
        [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
        
        
        
    }
    else {
        
        
        [cusNav hideBackButton:YES];
        [cusNav hideHambergerButton:YES];
        [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
        self.navigationItem.leftBarButtonItem = nil;
        //customize bottom back button
        
    }

    self.navigationController.navigationBar.hidden = NO;
    
}


/*Anjana start */
-(void)setConstraints:(UITableViewCell*)cell tag:(int)tag heightIphone:(float)heigtIphone heightIpad:(float)heightIpad
{
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    NSDictionary *viewDictionary = NSDictionaryOfVariableBindings(categoryList, viewSelectionControl,toolbar);
    
    [categoryList setTranslatesAutoresizingMaskIntoConstraints:NO];
    [viewSelectionControl setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[viewSelectionControl]|"] options:0 metrics:0 views:viewDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[categoryList]|"] options:0 metrics:0 views:viewDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[viewSelectionControl(44)][categoryList][toolbar]|"] options:0 metrics:0 views:viewDictionary]];
    
    
}


-(void)viewSelectionChanged:(id)sender
{
    
    switch ([viewSelectionControl selectedSegmentIndex])
    {
        case 1:
        {
            iWebRequestState=GETFAVCATEGORIES;
            [self requestToGetPreferredCategories];
            
            //categoryTable.tag = 2;
            
        }
            break;
            
        case 0:{
            iWebRequestState = GETFAVLOCATIONS;
            [self requestToGetPreferredLocations];
            
            //  categoryTable.tag = 1;
            
        }
            break;
            
            
        default:
            break;
    }
}
/* Anjana end */


-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
    
    //    switch ([viewSelectionControl selectedSegmentIndex])
    //    {
    //        case 1:
    //        {
    //            iWebRequestState=CATEGORIES_SET;
    //            [self requestToSetPreferredCategories];
    //
    //            //categoryTable.tag = 2;
    //
    //        }
    //            break;
    //
    //        case 0:{
    //            iWebRequestState = LOCATIONS_SET;
    //            [self requestToSetPreferredLocations];
    //
    //            //  categoryTable.tag = 1;
    //
    //        }
    //            break;
    //
    //
    //        default:
    //            break;
    //}
    
    
}

//- (void)backViewController {
//    NSArray * stack = self.navigationController.viewControllers;
//
//    for (int i=stack.count-1; i > 0; --i)
//        if (stack[i] == self && i==stack.count-1)
//            [self.navigationController popToViewController:stack[i-1] animated:NO];
//
//
//
//}

-(void)popBackfromBottom
{
    NSLog(@"%@",self.navigationController.viewControllers);
    NSLog(@"%@",[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3]);
    [defaults setBool:TRUE forKey:@"PopbackRemoveAlert"];
    
    NSLog(@"%@",viewSelectionControl);
    
    switch ([viewSelectionControl selectedSegmentIndex])
    {
        case 1:
        {
            iWebRequestState=CATEGORIES_SET;
            [self requestToSetPreferredCategories];
            //categoryTable.tag = 2;
            
        }
            break;
            
        case 0:{
            iWebRequestState = LOCATIONS_SET;
            [self requestToSetPreferredLocations];
            //  categoryTable.tag = 1;
        }
            break;
            
            
        default:
            break;
    }
    if([RegionApp isEqualToString:@"1"]){
        [UtilityManager popBackToViewController:[CityPreferenceViewController class] inNavigationController:self.navigationController];
    }
    else if([RegionApp isEqualToString:@"0"])
        [self.navigationController popViewControllerAnimated:NO];
}
#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
-(NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Check if its a Main Category Cell.
    if ([VPPDropDown tableView:tableView isRootCellAtIndexPath:indexPath] ) {
        return 4;
    }
    else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //need number of rows in the elements list
    int rows = (int)[VPPDropDown tableView:tableView numberOfExpandedRowsInSection:section];
    //have to add one for the parent row itself
    rows +=[DropDownsDDArray count];
    return rows;
    
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = nil;
    if (indexPath.row == 0) {
        cell.textLabel.text = [NSString stringWithFormat:@"   %@",NSLocalizedString(@"All",@"All")];
        
    }
    
    //Create A DropDown Cell If It is a Dropdown Cell
    if ([VPPDropDown tableView:tableView dropdownsContainIndexPath:indexPath]) {
        return [VPPDropDown tableView:tableView cellForRowAtIndexPath:indexPath];
        
    }
    
    return cell;
}

- (UITableViewCell *) dropDown:(VPPDropDown *)dropDown rootCellAtGlobalIndexPath:(NSIndexPath *)globalIndexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [categoryList dequeueReusableCellWithIdentifier:CellIdentifier];
    UIButton *checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [checkButton setAccessibilityLabel:@"checkBox"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone)
        {checkButton.frame = CGRectMake(3, 3, 40, 40);
        }
        else
        {
            checkButton.frame = CGRectMake(5, 5, 60, 60);
        }
        checkButton.tag = 2;
        [cell.contentView addSubview:checkButton];
        
    }
    else {
        checkButton = (UIButton *) [cell viewWithTag:2];
    }
    BOOL shouldExpand = NO;
    cell.accessoryView = nil;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if ([dropDown.elements count] > 1) {
        shouldExpand = YES;
    }
    
    if (dropDown.isExpanded) {
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"SectionButtonTop.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
        }
        else
        {
            cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"SectionButtonTop@2x.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:0.5] ];
        }
        UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"UITableContract_"]];
        cell.accessoryView = imView;
        ReleaseAndNilify(imView);
    }
    else {
        
        // DLog(@" %d Elements at index %d", [dropDown.elements count], globalIndexPath.row);
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"SectionButton.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
        }
        else
        {
            cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"SectionButton@2x.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:0.5] ];
        }
        
        if (shouldExpand) {
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"UITableExpand_"]];
            cell.accessoryView = imView;
            ReleaseAndNilify(imView);
        }
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"   %@",dropDown.title];
    DLog(@"%@",dropDown.title);
    cell.textLabel.backgroundColor = [UIColor clearColor];
    checkButton.titleLabel.text = dropDown.title;
    switch ([self checkChildFlags:(int)[mainCategoryNameArray indexOfObject:dropDown.title]]) {
        case 0:
            [checkButton setImage:[UIImage imageNamed:@"CheckMarkUnCheck.png"] forState:UIControlStateNormal];
            [checkButton removeTarget:self action:@selector(deselectAllChildren:) forControlEvents:UIControlEventTouchUpInside];
            [checkButton addTarget:self
                            action:@selector(selectAllChildren:) forControlEvents:UIControlEventTouchUpInside];
            break;
        case 1:
            [checkButton removeTarget:self action:@selector(selectAllChildren:) forControlEvents:UIControlEventTouchUpInside];
            [checkButton setImage:[UIImage imageNamed:@"CheckMarkSettings.png"] forState:UIControlStateNormal];
            [checkButton addTarget:self
                            action:@selector(deselectAllChildren:) forControlEvents:UIControlEventTouchUpInside];
            break;
        case 2:
            [checkButton removeTarget:self action:@selector(deselectAllChildren:) forControlEvents:UIControlEventTouchUpInside];
            [checkButton setImage:[UIImage imageNamed:@"CheckMarkPart.png"] forState:UIControlStateNormal];
            [checkButton addTarget:self
                            action:@selector(selectAllChildren:) forControlEvents:UIControlEventTouchUpInside];
            break;
            
        default:
            
            break;
    };
    return cell;
    
}

- (UITableViewCell *) dropDown:(VPPDropDown *)dropDown cellForElement:(VPPDropDownElement *)element atGlobalIndexPath:(NSIndexPath *)globalIndexPath {
    static NSString *cellIdentifier = @"CustomDropDownCell";
    
    UITableViewCell *cell = [categoryList dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    //Set Up Cell Data
    cell.textLabel.text = element.title;
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.text = dropDown.title;
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    
    //Check the Element Flag To See If We Need To Have The Check Box On on Off
    if (element.checkFlag) {
        cell.textLabel.textColor = [UIColor colorWithRed:51.0f/255.0f green:102.0f/255.0f blue:153.0f/255.0f alpha:1.0f];
        ;
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    if ([dropDown.elements count] - 1 == [dropDown.elements indexOfObject:element] ) {
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"SectionButtonBottom.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
        }
        else
        {
            cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"SectionButtonBottomiPad.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
        }
        
    }
    else {
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"SectionMiddle.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
        }
        else
        {
            cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"SectionMiddle.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
        }
        
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {return 50;
    }
    else
    {
        return 70;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    customItem.enabled = YES;
    
    //If the Cell is a DropDown Cell, Do The DropDown
    if ([VPPDropDown tableView:tableView dropdownsContainIndexPath:indexPath]) {
        [VPPDropDown tableView:tableView didSelectRowAtIndexPath:indexPath];
        return;
    }
}

- (void) dropDown:(VPPDropDown *)dropDown elementSelected:(VPPDropDownElement *)element atGlobalIndexPath:(NSIndexPath *)indexPath {
    
    //Call Method To Flip Flag
    [self flipCheckFlag:element.title mainTitle:dropDown.title];
    [categoryList deselectRowAtIndexPath:indexPath animated:YES];
}



/*
 *
 Navigate back to Main Menu screen page
 *
 */-(void)returnToMainPage:(id)sender
{
    //return to main page
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


/*
 *
 save/set user selected preferred categories.
 Method called when navigated from Settings screen to view user account details
 *
 */
-(IBAction)savePressed:(id)sender
{
    
    switch (viewSelectionControl.selectedSegmentIndex) {
        case 0:{
            iWebRequestState = LOCATIONS_SET;
            [self requestToSetPreferredLocations];
        }
            break;
            
        case 1:{
            iWebRequestState = CATEGORIES_SET;
            [self requestToSetPreferredCategories];
        }
            break;
            
        default:
            break;
    }
    //send request to server
    // iWebRequestState = CATEGORIES_SET;
    // [self requestToSetPreferredCategories];
}


/*
 *
 save/set user preferred categories
 Method called when navigated from user signup screen while registration.
 *
 */
-(IBAction)navigateToNextPage:(id)sender{
    
    //send request to server and navigate to next page based on response
    
    
    switch ([viewSelectionControl selectedSegmentIndex])
    {
        case 1:
        {
            iWebRequestState=CATEGORIES_SET;
            [self requestToSetPreferredCategories];
            
            //categoryTable.tag = 2;
            
        }
            break;
            
        case 0:{
            iWebRequestState = LOCATIONS_SET;
            [self requestToSetPreferredLocations];
            
            //  categoryTable.tag = 1;
            
        }
            break;
            
            
        default:
            break;
    }
}


#pragma mark - Request/Response methods
/*
 *
 send request to set user preferred categories.
 *
 */
-(void)requestToSetPreferredCategories
{
    //send request to server
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendString:@"<CategoryDetails>"];
    [requestStr appendFormat:@"<userId>%@</userId><catIds>", [defaults  valueForKey:KEY_USERID]];
    //[requestStr appendFormat:@""];
    
    /******** loop through the preferred categories and append catid's(comma separated) of those selected in the request ******/
    for (int i=1; i<[mainCategoryNameArray count]; i++) {
        VPPDropDown *currentItem = [DropDownsDDArray objectAtIndex:i];
        for (int j = 0; j < [currentItem.elements count]; j++) {
            VPPDropDownElement *currentElement = [currentItem.elements objectAtIndex:j];
            
            if (currentElement.checkFlag == NO)
                continue;
            else
            {
                [requestStr appendFormat:@"%@,", currentElement.categoryID];
            }
            
        }
        
    }
    if ([requestStr hasSuffix:@","])
        [requestStr  setString:[requestStr substringToIndex:[requestStr length]-1]];    // remove the comma after the last value appended
    [requestStr appendFormat:@"</catIds>"];
    [requestStr appendString:@"</CategoryDetails>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/setusercat",BASE_URL];
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    //[requestStr release];
    [defaults  setObject:@"0" forKey:@"Request"];
    
}

/*
 *
 Parsing logic when response received for setting user preferred categories.
 *
 */
-(void)parseSetPreferredCategories:(NSString *)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    int responseCode = [[TBXML textForElement:responseCodeElement]intValue];
    
    //if success navigate to success screen
    if (responseCode == 10000) {
        
        if ([SharedManager userInfoFromSignup] == YES){
            [SharedManager setUserInfoFromSignup:YES];
            RegistrationCompleteScreen *setupFlow = [[RegistrationCompleteScreen alloc]initWithNibName:@"RegistrationCompleteScreen" bundle:[NSBundle mainBundle]];
            //setupFlow.responseXml =[NSMutableString stringWithFormat:@"%@Registration_successtext.html",B_URL];
            [self.navigationController pushViewController:setupFlow animated:NO];
            // [setupFlow release];
        }
        else
        {
            UIAlertController * alert;
            alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Saved Successfully",@"Saved Successfully") message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     if ([SharedManager userInfoFromSignup] == YES)
                                     {
                                         [self.navigationController popViewControllerAnimated:NO];
                                     }
                                     else
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }
                                 }];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            //used to check whether coming from Hot Deals - to refresh the page upon going back
            int count = (int)[[self.navigationController viewControllers]count];
            if (count>2){
                id rootVC = [[self.navigationController viewControllers] objectAtIndex:count-2];
                if ([rootVC isKindOfClass:[DealHotDealsList class]]) {
                    
                    [SharedManager setRefreshCategories:YES];
                    [SharedManager setRefreshList:YES];
                }
                if ([rootVC isKindOfClass:[DealCouponsViewController class]])
                {
                    [SharedManager setRefreshGallery:YES];
                }
            }
        }
    }
    
    else if(responseCode == 10001)
    {
        TBXMLElement *msgElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        UIAlertController * alert;
        
        alert=[UIAlertController alertControllerWithTitle:[TBXML textForElement:msgElement] message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action){
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 if ([SharedManager userInfoFromSignup] == YES)
                                 {
                                     if(viewSelectionControl.selectedSegmentIndex==0){
                                         [viewSelectionControl setSelectedSegmentIndex:1];
                                         [self viewSelectionChanged:0];
                                     }
                                 }
                                 else
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }
                                 
                                 
                             }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        return;
        
    }
    
}



/*
 *
 send request with data to get user preferred categories to display.
 Request sent when navigated from Settings flow while viewing user accounts.
 *
 */
-(void)requestToGetPreferredCategories
{
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId></UserDetails>", [defaults  valueForKey:KEY_USERID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    ReleaseAndNilify(xmlStr);
}



/*
 *
 Parsing logic after getting preferred categories.
 Request sent when navigated from Settings flow while viewing user accounts.
 *
 */
-(void)parseGetPreferredCategories:(NSString *)response
{
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *MainCategoryDetailElement = [TBXML childElementNamed:@"MainCategory" parentElement:tbxml.rootXMLElement];
    
    if([mainCategoryNameArray count])
        [mainCategoryNameArray removeAllObjects];
    [mainCategoryNameArray addObject:@"All"];
    subCategoryNameArray[[mainCategoryNameArray count] - 1] = [[NSMutableArray alloc]init];
    subCategoryIdArray[[mainCategoryNameArray count] - 1] = [[NSMutableArray alloc]init];
    displayedArray[[mainCategoryNameArray count] - 1] = [[NSMutableArray alloc]init];
    categoryIdArray[[mainCategoryNameArray count] - 1] = [[NSMutableArray alloc]init];
    
    [subCategoryNameArray[[mainCategoryNameArray count] - 1] addObject:@""];
    [subCategoryIdArray[[mainCategoryNameArray count] - 1] addObject:@""];
    [displayedArray[[mainCategoryNameArray count] - 1] addObject:@""];
    [categoryIdArray[[mainCategoryNameArray count] - 1] addObject:@""];
    
    int i = 1;
    
    //Create Appropriate Arrays for SubCatecories, et al, and fill them.
    while (MainCategoryDetailElement !=nil)
    {
        
        subCategoryNameArray[i] = [[NSMutableArray alloc]init];
        subCategoryIdArray[i] = [[NSMutableArray alloc]init];
        displayedArray[i] = [[NSMutableArray alloc]init];
        categoryIdArray[i] = [[NSMutableArray alloc]init];
        
        TBXMLElement *mainCategoryNameElement = [TBXML childElementNamed:@"parCatName" parentElement:MainCategoryDetailElement];
        
        if (mainCategoryNameElement != nil) {
            [mainCategoryNameArray addObject:[TBXML textForElement:mainCategoryNameElement]];
            
        }
        
        TBXMLElement *SubCategoryDetailElement = [TBXML childElementNamed:@"SubCategory" parentElement:MainCategoryDetailElement];
        
        while (SubCategoryDetailElement !=nil) {
            TBXMLElement *subCategoryIdElement = [TBXML childElementNamed:@"subCatId" parentElement:SubCategoryDetailElement];
            TBXMLElement *subCategoryNameElement = [TBXML childElementNamed:@"subCatName" parentElement:SubCategoryDetailElement];
            TBXMLElement *displayedElement = [TBXML childElementNamed:@"isAdded" parentElement:SubCategoryDetailElement];
            TBXMLElement *categoryIdElement = [TBXML childElementNamed:@"catId" parentElement:SubCategoryDetailElement];
            
            
            if (subCategoryIdElement != nil) {
                [subCategoryIdArray[i] addObject:[TBXML textForElement:subCategoryIdElement]];
            }
            
            if (subCategoryNameElement != nil) {
                [subCategoryNameArray[i] addObject:[TBXML textForElement:subCategoryNameElement]];
            }
            
            if (displayedElement != nil) {
                [displayedArray[i] addObject:[TBXML textForElement:displayedElement]];
            }
            
            if (categoryIdElement != nil) {
                [categoryIdArray[i] addObject:[TBXML textForElement:categoryIdElement]];
            }
            
            SubCategoryDetailElement = [TBXML nextSiblingNamed:@"SubCategory" searchFromElement:SubCategoryDetailElement];
        }
        i++;
        
        MainCategoryDetailElement = [TBXML nextSiblingNamed:@"MainCategory" searchFromElement:MainCategoryDetailElement];
    }
    
    //[tbxml release];
    [self updateAllArrays];
}


/*
 *
 Method called after parsing the preferred categories received from server.
 Update and sort all the arrays with the categories received to display.
 *
 */
-(void)updateAllArrays
{
    //Declare Our Arrays For the DropDown Cells
    SubCategoriesDDArray = [[NSMutableArray alloc] init];
    DropDownsDDArray = [[NSMutableArray alloc] init];
    displayedDDArray = [[NSMutableArray alloc] init];
    categoryIDDDArray = [[NSMutableArray alloc] init];
    
    
    
    //For Each Section In Main, We Need Names,Checks, and ID Arrays For The Elements
    for (int i = 0; i < [mainCategoryNameArray count]; i++) {
        
        [SubCategoriesDDArray addObject:subCategoryNameArray[i]];
        
        [displayedDDArray addObject:displayedArray[i]];
        [categoryIDDDArray addObject:categoryIdArray[i]];
    }
    
    //For Each Section In Main, We Need to Break down into those Arrays We Just Created To Make Each Individual Element To Place in the DropDown Objects
    
    NSArray *currentElements ;
    NSArray *currentDisplayFlag ;
    NSArray *currentIDArray ;
    
    for (int i = 0; i < [mainCategoryNameArray count]; i++) {
        
        //Grab Associated Element Names and Display Flags
        currentElements = [SubCategoriesDDArray objectAtIndex:i];
        currentDisplayFlag = [displayedDDArray objectAtIndex:i];
        currentIDArray = [categoryIDDDArray objectAtIndex:i];
        
        //Make a Mutable Array of Elements That we can start adding to
        NSMutableArray *elts = [NSMutableArray array];
        
        //For every item in currentElements, we need to create a VPPDropDownElement, give it info, and stick it in the list.
        for (int j = 0; j < [currentElements count]; j++) {
            VPPDropDownElement *e = [[VPPDropDownElement alloc] initWithTitle:[NSString stringWithFormat:@"%@",[currentElements objectAtIndex:j]] andObject:[NSNumber numberWithInt:j] categID:[currentIDArray objectAtIndex:j]];
            if ([[currentDisplayFlag objectAtIndex:j] intValue] == 1) {
                e.checkFlag = YES;
            }
            [elts addObject:e];
            // [e release];
        }
        
        //For Each Main Category, We Need a DropDown Item To Put into the list using the Elements We Just Created and the MainCategoryNameArray.
        
        DropDownItem = [[VPPDropDown alloc] initWithTitle:[mainCategoryNameArray objectAtIndex:i]
                                                     type:VPPDropDownTypeCustom
                                                tableView:categoryList
                                                indexPath:[NSIndexPath indexPathForRow:i inSection:0]
                                                 elements:elts
                                                      tag:i
                                                 delegate:self];
        
        [DropDownsDDArray addObject:DropDownItem];
        // [DropDownItem release];
        
    }
    
    /*[currentElements release];
     [currentDisplayFlag release];
     [currentIDArray release];
     
     for (int i = 0; i < [mainCategoryNameArray count]; i++) {
     
     [subCategoryNameArray[i] release];
     [subCategoryIdArray[i] release];
     [displayedArray[i] release];
     [categoryIdArray[i] release];
     
     
     }*/
    dispatch_async(dispatch_get_main_queue(), ^{
        [categoryList reloadData];
    });
    
    
}

/********** Anjana start ******************/

/*
 *
 
 send request with data to get locations to display.
 // http://10.11.200.98:8080/HubCiti1.6/firstuse/getuserloccat?userId=7517&hubCitiId=4173
 *
 */
-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    
    NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    
    [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    
}



/*
 *
 Parsing logic after getting preferred categories.
 Request sent when navigated from Settings flow while viewing user accounts.
 *
 */
-(void)parseGetPreferredLocations:(NSString *)response
{
    
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *MainCategoryDetailElement = [TBXML childElementNamed:@"MainCategory" parentElement:tbxml.rootXMLElement];
    
    if([mainCategoryNameArray count])
        [mainCategoryNameArray removeAllObjects];
    if([parCatIdArray count])
        [parCatIdArray removeAllObjects];
    
    [mainCategoryNameArray addObject:@"All"];
    [parCatIdArray addObject:@""];
    subCategoryNameArray[[mainCategoryNameArray count] - 1] = [[NSMutableArray alloc]init];
    subCategoryIdArray[[mainCategoryNameArray count] - 1] = [[NSMutableArray alloc]init];
    displayedArray[[mainCategoryNameArray count] - 1] = [[NSMutableArray alloc]init];
    categoryIdArray[[mainCategoryNameArray count] - 1] = [[NSMutableArray alloc]init];
    
    [subCategoryNameArray[[mainCategoryNameArray count] - 1] addObject:@""];
    [subCategoryIdArray[[mainCategoryNameArray count] - 1] addObject:@""];
    [displayedArray[[mainCategoryNameArray count] - 1] addObject:@""];
    [categoryIdArray[[mainCategoryNameArray count] - 1] addObject:@""];
    
    int i = 1;
    
    //Create Appropriate Arrays for SubCatecories, et al, and fill them.
    while (MainCategoryDetailElement !=nil)
    {
        
        subCategoryNameArray[i] = [[NSMutableArray alloc]init];
        subCategoryIdArray[i] = [[NSMutableArray alloc]init];
        displayedArray[i] = [[NSMutableArray alloc]init];
        categoryIdArray[i] = [[NSMutableArray alloc]init];
        
        TBXMLElement *mainCategoryNameElement = [TBXML childElementNamed:@"parCatName" parentElement:MainCategoryDetailElement];
        TBXMLElement *parentIdElement = [TBXML childElementNamed:@"parCatId" parentElement:MainCategoryDetailElement];
        
        if (mainCategoryNameElement != nil) {
            [mainCategoryNameArray addObject:[TBXML textForElement:mainCategoryNameElement]];
            
        }
        if (parentIdElement != nil) {
            [parCatIdArray addObject:[TBXML textForElement:parentIdElement]];
        }
        
        
        TBXMLElement *SubCategoryDetailElement = [TBXML childElementNamed:@"SubCategory" parentElement:MainCategoryDetailElement];
        
        while (SubCategoryDetailElement !=nil) {
            TBXMLElement *subCategoryIdElement = [TBXML childElementNamed:@"subCatId" parentElement:SubCategoryDetailElement];
            TBXMLElement *subCategoryNameElement = [TBXML childElementNamed:@"subCatName" parentElement:SubCategoryDetailElement];
            TBXMLElement *displayedElement = [TBXML childElementNamed:@"isAdded" parentElement:SubCategoryDetailElement];
            
            
            if (subCategoryIdElement != nil) {
                [subCategoryIdArray[i] addObject:[TBXML textForElement:subCategoryIdElement]];
            }
            
            if (subCategoryNameElement != nil) {
                [subCategoryNameArray[i] addObject:[TBXML textForElement:subCategoryNameElement]];
            }
            
            if (displayedElement != nil) {
                [displayedArray[i] addObject:[TBXML textForElement:displayedElement]];
            }
            
            
            SubCategoryDetailElement = [TBXML nextSiblingNamed:@"SubCategory" searchFromElement:SubCategoryDetailElement];
        }
        i++;
        
        MainCategoryDetailElement = [TBXML nextSiblingNamed:@"MainCategory" searchFromElement:MainCategoryDetailElement];
    }
    
    // [tbxml release];
    [self updateLocationArrays];
}


-(void)updateLocationArrays
{
    //Declare Our Arrays For the DropDown Cells
    SubCategoriesDDArray = [[NSMutableArray alloc] init];
    DropDownsDDArray = [[NSMutableArray alloc] init];
    displayedDDArray = [[NSMutableArray alloc] init];
    categoryIDDDArray = [[NSMutableArray alloc] init];
    
    
    
    //For Each Section In Main, We Need Names,Checks, and ID Arrays For The Elements
    for (int i = 0; i < [mainCategoryNameArray count]; i++) {
        
        [SubCategoriesDDArray addObject:subCategoryNameArray[i]];
        [displayedDDArray addObject:displayedArray[i]];
        [categoryIDDDArray addObject:subCategoryIdArray[i]];
    }
    
    //For Each Section In Main, We Need to Break down into those Arrays We Just Created To Make Each Individual Element To Place in the DropDown Objects
    
    NSArray *currentElements ;
    NSArray *currentDisplayFlag ;
    NSArray *currentIDArray ;
    for (int i = 0; i < [mainCategoryNameArray count]; i++) {
        
        //Grab Associated Element Names and Display Flags
        currentElements = [SubCategoriesDDArray objectAtIndex:i];
        currentDisplayFlag = [displayedDDArray objectAtIndex:i];
        currentIDArray = [categoryIDDDArray objectAtIndex:i];
        
        //Make a Mutable Array of Elements That we can start adding to
        NSMutableArray *elts = [NSMutableArray array];
        
        //For every item in currentElements, we need to create a VPPDropDownElement, give it info, and stick it in the list.
        for (int j = 0; j < [currentElements count]; j++) {
            VPPDropDownElement *e = [[VPPDropDownElement alloc] initWithTitle:[NSString stringWithFormat:@"%@",[currentElements objectAtIndex:j]] andObject:[NSNumber numberWithInt:j] categID:[currentIDArray objectAtIndex:j]];
            if ([[currentDisplayFlag objectAtIndex:j] intValue] == 1) {
                e.checkFlag = YES;
            }
            [elts addObject:e];
            //[e release];
        }
        
        //For Each Main Category, We Need a DropDown Item To Put into the list using the Elements We Just Created and the MainCategoryNameArray.
        DropDownItem = [[VPPDropDown alloc] initWithTitle:[mainCategoryNameArray objectAtIndex:i]
                                                     type:VPPDropDownTypeCustom
                                                tableView:categoryList
                                                indexPath:[NSIndexPath indexPathForRow:i inSection:0]
                                                 elements:elts
                                                      tag:-1
                                                 delegate:self];
        
        [DropDownsDDArray addObject:DropDownItem];
        // [DropDownItem release];
        
    }
    
    /*[currentElements release];
     [currentDisplayFlag release];
     [currentIDArray release];
     
     for (int i = 0; i < [mainCategoryNameArray count]; i++) {
     
     [subCategoryNameArray[i] release];
     [subCategoryIdArray[i] release];
     [displayedArray[i] release];
     [categoryIdArray[i] release];
     
     
     }*/
    dispatch_async(dispatch_get_main_queue(), ^{
        [categoryList reloadData];
    });
}

-(void)requestToSetPreferredLocations
{
    //send request to server
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    
    NSMutableString *parIds = [[NSMutableString alloc]init];
    NSMutableString *subIds = [[NSMutableString alloc]init];
    BOOL isChecked = NO;
    
    [requestStr appendString:@"<CategoryDetails>"];
    [requestStr appendFormat:@"<userId>%@</userId><hubCitiId>%@</hubCitiId><catIds>", [defaults  valueForKey:KEY_USERID],[defaults  valueForKey:KEY_HUBCITIID]];
    /******** loop through the preferred categories and append catid's(comma separated) of those selected in the request ******/
    for (int i=1; i<[mainCategoryNameArray count]; i++) {
        VPPDropDown *currentItem = [DropDownsDDArray objectAtIndex:i];
        for (int j = 0; j < [currentItem.elements count]; j++) {
            VPPDropDownElement *currentElement = [currentItem.elements objectAtIndex:j];
            
            if (currentElement.checkFlag == NO)
                continue;
            else
            {
                isChecked = YES;
                if([currentElement.title isEqualToString:@"All"])
                    [subIds appendFormat:@"NULL"];
                else
                    [subIds appendFormat:@"%@,",currentElement.categoryID];
            }
            
            
        }
        if ([subIds hasSuffix:@","])
            [subIds setString:[subIds substringToIndex:[subIds length]-1]];
        
        if (![subIds hasSuffix:@"|"])
            [subIds appendFormat:@"|"];
        
        if(isChecked){
            [parIds appendFormat:@"%@,",[parCatIdArray objectAtIndex:i]];
            isChecked = NO;
        }
        
    }
    if ([parIds hasSuffix:@","])
        [parIds  setString:[parIds substringToIndex:[parIds length]-1]];
    
    if ([subIds hasPrefix:@"|"])
        [subIds  setString:[subIds substringToIndex:[subIds length]-1]];
    
    if ([subIds hasSuffix:@"|"] ||  [subIds hasSuffix:@","])
        [subIds  setString:[subIds substringToIndex:[subIds length]-1]];
    
    
    [requestStr appendFormat:@"%@</catIds><subCatIds>%@</subCatIds>", parIds,subIds];
    [requestStr appendString:@"</CategoryDetails>"];
    NSLog(@"request = %@", requestStr);
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/setuserloccat",BASE_URL];
    //    NSString *urlString = [NSString stringWithFormat:@"http://10.11.200.199:8080/HubCiti1.6/firstuse/setuserloccat"];
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    //[requestStr release];
    [defaults  setObject:@"0" forKey:@"Request"];
    
}

/*
 *
 Parsing logic when response received for setting user preferred categories.
 *
 */
-(void)parseSetPreferredLocations:(NSString *)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    int responseCode = [[TBXML textForElement:responseCodeElement]intValue];
    
    //if success navigate to success screen
    if (responseCode == 10000) {
        
        if ([SharedManager userInfoFromSignup] == YES && [defaults boolForKey:@"PopbackRemoveAlert"]){
            [SharedManager setUserInfoFromSignup:YES];
            //            RegistrationCompleteScreen *setupFlow = [[RegistrationCompleteScreen alloc]initWithNibName:@"RegistrationCompleteScreen" bundle:[NSBundle mainBundle]];
            //            //setupFlow.responseXml =[NSMutableString stringWithFormat:@"%@Registration_successtext.html",B_URL];
            //            [self.navigationController pushViewController:setupFlow animated:NO];
            //            [setupFlow release];
            if(viewSelectionControl.selectedSegmentIndex==0){
                [viewSelectionControl setSelectedSegmentIndex:1];
                [self viewSelectionChanged:0];
                [defaults setBool:FALSE forKey:@"PopbackRemoveAlert"];
            }
        }
        else
        {
            UIAlertController * alert;
            
            alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Saved Successfully",@"Saved Successfully") message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     if ([SharedManager userInfoFromSignup] == YES)
                                     {
                                         if(viewSelectionControl.selectedSegmentIndex==0){
                                             [viewSelectionControl setSelectedSegmentIndex:1];
                                             [self viewSelectionChanged:0];
                                         }
                                     }
                                     else
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }
                                     
                                     
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            //used to check whether coming from Hot Deals - to refresh the page upon going back
            int count = (int)[[self.navigationController viewControllers]count];
            if (count>2){
                id rootVC = [[self.navigationController viewControllers] objectAtIndex:count-2];
                if ([rootVC isKindOfClass:[DealHotDealsList class]]) {
                    
                    [SharedManager setRefreshCategories:YES];
                    [SharedManager setRefreshList:YES];
                }
                if ([rootVC isKindOfClass:[DealCouponsViewController class]])
                {
                    [SharedManager setRefreshGallery:YES];
                }
            }
        }
    }
    
    if(responseCode == 10001)
    {
        TBXMLElement *msgElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        UIAlertController * alert;
        
        alert=[UIAlertController alertControllerWithTitle:[TBXML textForElement:msgElement] message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action){
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 if ([SharedManager userInfoFromSignup] == YES)
                                 {
                                     if(viewSelectionControl.selectedSegmentIndex==0){
                                         [viewSelectionControl setSelectedSegmentIndex:1];
                                         [self viewSelectionChanged:0];
                                     }
                                 }
                                 else
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }
                                 
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        return;
        
    }
    
}



/************* Anjana end *****************/

/*
 *
 asynchronous req/resp delegate method
 *
 */
-(void)responseData:(NSString *)response
{
    DLog(@"response %@",response);
    switch (iWebRequestState)
    {
        case GETFAVCATEGORIES:
            [self parseGetPreferredCategories:response];
            break;
            
        case CATEGORIES_SET:
            [self parseSetPreferredCategories:response];
            break;
            
        case GETFAVLOCATIONS:
            [self parseGetPreferredLocations:response];
            break;
            
        case LOCATIONS_SET:
            [self parseSetPreferredLocations:response];
            break;
            
        default:
            break;
    }
}


-(void)flipCheckFlag:(NSString*)subCatTitle mainTitle:(NSString *)mainTitle
{
    //Sets the CheckFlag of the individual DropDown Cells.
    
    //Find index of Which Section Was Called
    int titleIndex = (int)[mainCategoryNameArray indexOfObject:mainTitle];
    
    //Find Index of SubCagetory Selected
    
    
    //Create local Instance Of DropDown Item
    VPPDropDown *currentItem = [DropDownsDDArray objectAtIndex:titleIndex];
    
    // int subCatIndex =0;
    // VPPDropDownElement *currentElement = [[VPPDropDownElement alloc] init];
    for (int i = 0; i < [currentItem.elements count]; i++)
    {
        VPPDropDownElement *currentElement = [currentItem.elements objectAtIndex:i];
        if (currentElement.title == subCatTitle) {
            currentElement.checkFlag = !currentElement.checkFlag;
        }
    }
    
    //Reload Table
    dispatch_async(dispatch_get_main_queue(), ^{
        [categoryList reloadData];
    });
    // [currentSubcategory release];
}


-(void)selectAllChildren:(UIButton *)sender
{
    customItem.enabled = TRUE;
    int titleIndex = (int)[mainCategoryNameArray indexOfObject:sender.titleLabel.text];
    
    if (titleIndex == 0) {
        [self selectAllPreferences];
    }
    else {
        
        VPPDropDown *currentItem = [DropDownsDDArray objectAtIndex:titleIndex];
        
        //Create Local Instance Of Element Item
        for (int i = 0; i < [currentItem.elements count]; i++)
        {
            VPPDropDownElement *currentElement = [currentItem.elements objectAtIndex:i];
            
            currentElement.checkFlag = YES;
            
        }
    }
    //Reload Table
    dispatch_async(dispatch_get_main_queue(), ^{
        [categoryList reloadData];
    });
}


-(void)deselectAllChildren:(UIButton *)sender
{
    customItem.enabled = TRUE;
    
    int titleIndex = (int)[mainCategoryNameArray indexOfObject:sender.titleLabel.text];
    
    if (titleIndex == 0) {
        [self deselectAllPreferences];
    }
    else {
        
        VPPDropDown *currentItem = [DropDownsDDArray objectAtIndex:titleIndex];
        
        //Create Local Instance Of Element Item
        for (int i = 0; i < [currentItem.elements count]; i++)
        {
            VPPDropDownElement *currentElement = [currentItem.elements objectAtIndex:i];
            
            currentElement.checkFlag = NO;
            
        }
    }
    //Reload Table
    dispatch_async(dispatch_get_main_queue(), ^{
        [categoryList reloadData];
    });
}


-(int) checkChildFlags:(int) titleIndex
{
    int numFlagsOn = 0;
    int numAllItems = 0;
    int currentItemElements = 0;
    
    if(titleIndex == -1){
        return 0;
    }
    
    if (titleIndex == 0) {
        for (int i = 0; i < [DropDownsDDArray count]; i++) {
            
            VPPDropDown *currentItem = [DropDownsDDArray objectAtIndex:i];
            numAllItems +=[currentItem.elements count];
            //Create Local Instance Of Element Item
            for (int j = 0; j < [currentItem.elements count]; j++)
            {
                VPPDropDownElement *currentElement = [currentItem.elements objectAtIndex:j];
                
                if (currentElement.checkFlag) {
                    numFlagsOn++;
                }
                
            }
        }
    }
    else {
        
        //Create local Instance Of DropDown Item
        VPPDropDown *currentItem = [DropDownsDDArray objectAtIndex:titleIndex];
        currentItemElements = (int)[currentItem.elements count];
        //Create Local Instance Of Element Item
        for (int i = 0; i < [currentItem.elements count]; i++)
        {
            VPPDropDownElement *currentElement = [currentItem.elements objectAtIndex:i];
            
            if (currentElement.checkFlag) {
                numFlagsOn++;
            }
            
        }
    }
    if (numFlagsOn == 0) {
        
        return 0;
    }
    else if (numFlagsOn == currentItemElements) {
        
        return 1;
    }
    else {
        if (numAllItems == numFlagsOn) {
            
            return 1;
        }
        else {
            
            return 2;
        }
    }
}


-(void)selectAllPreferences
{
    
    for (int i = 0; i < [DropDownsDDArray count]; i++) {
        
        VPPDropDown *currentItem = [DropDownsDDArray objectAtIndex:i];
        
        //Create Local Instance Of Element Item
        for (int j = 0; j < [currentItem.elements count]; j++)
        {
            VPPDropDownElement *currentElement = [currentItem.elements objectAtIndex:j];
            
            currentElement.checkFlag = YES;
            
        }
    }
    //Reload Table
    dispatch_async(dispatch_get_main_queue(), ^{
        [categoryList reloadData];
    });
}


-(void)deselectAllPreferences
{
    for (int i = 0; i < [DropDownsDDArray count]; i++) {
        
        VPPDropDown *currentItem = [DropDownsDDArray objectAtIndex:i];
        
        //Create Local Instance Of Element Item
        for (int j = 0; j < [currentItem.elements count]; j++)
        {
            VPPDropDownElement *currentElement = [currentItem.elements objectAtIndex:j];
            
            currentElement.checkFlag = NO;
        }
    }
    //Reload Table 
    dispatch_async(dispatch_get_main_queue(), ^{
        [categoryList reloadData];
    });}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

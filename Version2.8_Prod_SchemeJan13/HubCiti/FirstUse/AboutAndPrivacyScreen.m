//
//  AboutAndPrivacyScreen.m
//  Demo
//
//  Created by ajit on 5/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AboutAndPrivacyScreen.h"
#import "MainMenuViewController.h"
#import "SdImageView.h"
#import "CombinationViewController.h"
#import "ScrollTemplateViewController.h"
#import "BlockViewController.h"
//#import "RootViewController.h"


@implementation AboutAndPrivacyScreen

@synthesize responseXml, backButton ,screenName,comingFromPrivacyScreen;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
   	[self.navigationController setNavigationBarHidden:NO ];
	self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;

    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    ////[back release];
    
    self.title=@"Title";
       
    UILabel *AboutAndPrivacy ;
    if (comingFromPrivacyScreen)
    {
       AboutAndPrivacy = [self aboutAndPrivacyButton:@"Privacy"];
    }
    else
    {
        AboutAndPrivacy = [self aboutAndPrivacyButton:@"About"];
    }
    UIBarButtonItem *AboutAndPrivacyBarButton = [[UIBarButtonItem alloc] initWithCustomView:AboutAndPrivacy];
    self.navigationItem.rightBarButtonItem = AboutAndPrivacyBarButton;

    
       /*
     *ADD THE LOGO IMAGE HERE
     *
     */
    //if (![[defaults valueForKey:HC_LOGOIMGPATH] isEqualToString:@"N/A"]){
        
        SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(10, 0, 86, 35)];
        asyncImageView.backgroundColor = [UIColor clearColor];
        //asyncImageView.layer.cornerRadius = 5.0f;
        asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
        [asyncImageView loadImage:[defaults valueForKey:HC_LOGOIMGPATH]];
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 86, 35)];
    asyncImageView.frame = titleView.bounds;
    [titleView addSubview:asyncImageView];
   
    [self.navigationController.navigationBar.topItem setTitleView:titleView];
      //  //[asyncImageView release];

    
    //webView.frame = [[UIScreen mainScreen]bounds];
    [webView scalesPageToFit];
    webView.backgroundColor = [UIColor clearColor];
    [webView clearsContextBeforeDrawing];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
    activityIndicator.hidesWhenStopped = YES;
    [webView addSubview:activityIndicator];
}
-(UILabel*)aboutAndPrivacyButton:(NSString*)title
{
    CGSize stringsize = [title sizeWithAttributes: @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:13.0f]}];
    UILabel * titlelabel = [[UILabel alloc]initWithFrame:CGRectMake(0,0,stringsize.width, 30.0)];
    [titlelabel setText:title];
    [titlelabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
    [titlelabel setTextColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
    return titlelabel;
}
-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(void)returnToMainMenu {
    
	[UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:YES];
	[self.navigationController setNavigationBarHidden:NO ];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
	NSURL *url = [NSURL URLWithString:responseXml];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	[webView loadRequest:request];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    [activityIndicator startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [activityIndicator stopAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [activityIndicator stopAnimating];
    DLog(@"Error");
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}
@end

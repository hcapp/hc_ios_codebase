//
//  TutorialViewController.m
//  HubCiti
//
//  Created by service on 8/17/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "TutorialViewController.h"
#import "TutorialChildViewController.h"
#import "MainMenuViewController.h"
#import "RegistrationViewController.h"
#import "HTTPClient.h"
#import "AppDelegate.h"
#import "WebBrowserViewController.h"
#import "CommonUtility.h"
#import "SpecialOffersViewController.h"

@interface TutorialViewController () < HTTPClientDelegate>
{
    UIButton* skip,*signup;
    NSMutableArray* imageArr,*typeArr,*isAnythingPageOrWeblink,*isAnythingPageOrWeblinkValue;
    UIPageControl *pageControl;
    
}
@end

@implementation TutorialViewController

@synthesize pageController,viewType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void) setup{
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    
    // Sign up
    signup = [UIButton buttonWithType:UIButtonTypeCustom];
    signup.frame = CGRectMake(5, SCREEN_HEIGHT - 35 , 100, 30);
    [signup setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [signup addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [signup setTitle:@"Sign Up Now" forState:UIControlStateNormal];
    signup.titleLabel.font = [UIFont systemFontOfSize:16];
    signup.tag = 0;
    signup.hidden = TRUE;
    
    // Skip
    
    skip = [UIButton buttonWithType:UIButtonTypeCustom];
    skip.frame = CGRectMake(SCREEN_WIDTH - 55, SCREEN_HEIGHT - 35, 50, 30);
    [skip setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [skip addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [skip setTitle:@"Skip" forState:UIControlStateNormal];
    skip.titleLabel.font = [UIFont systemFontOfSize:16];
    skip.tag = 1;
    skip.hidden = TRUE;

    
    TutorialChildViewController *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    
    
    [self.pageController.view addSubview:signup];

    
    [self.pageController.view addSubview:skip];
    
    if (imageArr.count == 1) {
        
        pageControl = [UIPageControl appearance];
        pageControl.pageIndicatorTintColor = [UIColor blackColor];
        pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
        pageControl.backgroundColor = [UIColor blackColor];
    }

    
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    pageControl.backgroundColor = [UIColor clearColor];
    //pageControl.enabled = NO;
}
-(void) buttonPressed : (id) sender
{
    UIButton *btn = (UIButton*) sender;
    [defaults setBool:NO forKey:@"trainingSlides"];
        
        if (btn.tag == 0) {
            
            self.viewType = @"SignUp";
            [[NSNotificationCenter defaultCenter] postNotificationName:@"tutorial"
                                                                object:self];
        }
        else{
            self.viewType = @"MainMenu";
            [[NSNotificationCenter defaultCenter] postNotificationName:@"tutorial"
                                                                object:self];
        }
    
}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        splOfferVC.didDismiss = ^(NSString *data) {
            // this method gets called in MainVC when your SecondVC is dismissed
           
            NSLog(@"Dismissed SecondViewController");
        };
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }
    
}

#pragma mark - Service Respone methods

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [self Parse_AnyThingPage:responseObject];
    [HubCitiAppDelegate removeActivityIndicator];
    
}
-(void)responseData:(NSString *) response
{
    [self parse_HubcitiAnythingInfo : response];
    
    //[self parseDataCustomLoginUI:response];
}

-(void) Parse_AnyThingPage : (NSString*) response
{
    NSDictionary* responseData = (NSDictionary*) response;
    
    DLog(@"%@",responseData);
    
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    
    if (responseCode != 10000) {
  
        [UtilityManager showAlert:nil msg:responseText];
    }
    else{
        
        [defaults  setValue:[responseData objectForKey:@"mainMenuId"] forKey:KEY_MAINMENUID];
        if (![[responseData objectForKey:@"pageLink"] isEqualToString:@"N/A"]) {
            NSURL *url = [NSURL URLWithString:(NSString*)[responseData objectForKey:@"pageLink"]];
            [defaults setValue:[responseData objectForKey:@"pageLink"] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
                else{
                    [[self navigationController] setNavigationBarHidden:NO animated:NO];
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
                
            }
            else{
                [defaults setValue:[responseData objectForKey:@"pageLink"] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                [[self navigationController] setNavigationBarHidden:NO animated:NO];
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                
            }
            
        }
        
        else if ([[responseData objectForKey:@"mediaPath"] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:responseData forKey:KEY_RESPONSEXML];
            [[self navigationController] setNavigationBarHidden:NO animated:NO];
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[responseData objectForKey:@"mediaPath"] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            [[self navigationController] setNavigationBarHidden:NO animated:NO];
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
        }
    }
 
}


-(void) showAnyThingPage : (NSNotification *)notification
{
    [defaults setBool:YES forKey:@"trainingSlides"];
    
    NSMutableDictionary* param = [[NSMutableDictionary alloc] init];
    
    // NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfojson",BASE_URL];
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfojson",BASE_URL];
    
    NSLog(@"url = %@", urlString);
    
    NSString* pageId =  [defaults objectForKey:@"pageid"];
    
    [param setValue:pageId forKey:@"pageId"];
    [defaults setObject: pageId forKey:KEY_ANYPAGEID];
    
    [param setValue:@"IOS" forKey:@"platform"];
    
    [param setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [param setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
    
    if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [param setValue:[defaults valueForKey:KEY_BOTTOMBUTTONID] forKey:@"bottomBtnId"];
    
    NSLog(@"param: %@", param);
   
    [HubCitiAppDelegate showActivityIndicator];
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    [client sendRequest:param :urlString];
   
   /* NSString* pageId =  [defaults objectForKey:@"pageid"];
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",pageId];
    [defaults setObject:pageId forKey:KEY_ANYPAGEID];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];*/
  
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        
        
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
      
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [[self navigationController] setNavigationBarHidden:NO animated:NO];
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString] containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                }
                else{
                    [[self navigationController] setNavigationBarHidden:NO animated:NO];
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                [[self navigationController] setNavigationBarHidden:NO animated:NO];
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            [[self navigationController] setNavigationBarHidden:NO animated:NO];
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            [[self navigationController] setNavigationBarHidden:NO animated:NO];
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    
}


-(void) showWebPage : (NSNotification *)notification
{
    [defaults setBool:YES forKey:@"trainingSlides"];
   
    NSString* url = [defaults valueForKey: KEY_URL];
    
    if([url containsString:@"3000.htm"]){
        
         [defaults setValue:url forKey:KEY_URL];
        
        
        BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
        if (!lsFlag) {
            
            UIAlertController * alert;
            
            alert=[UIAlertController alertControllerWithTitle:nil message:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
        else{
            [[self navigationController] setNavigationBarHidden:NO animated:NO];
            SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
            
            splOfferVC.isAppsiteLogisticsFlag = 1;
             if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
            [HubCitiAppDelegate setIsLogistics:YES];
            
            [self presentViewController:splOfferVC animated:YES completion:nil];
        }
        
    }
    else{
        [defaults setValue:url forKey:KEY_URL];
        [[self navigationController] setNavigationBarHidden:NO animated:NO];
        WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:urlDetail animated:NO];
        
    }
  
}
- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [defaults setBool:NO forKey:@"trainingSlides"];
    [defaults setObject:nil forKey:@"pageid"];
    imageArr = [[NSMutableArray alloc] init];
    typeArr =  [[NSMutableArray alloc] init];
    isAnythingPageOrWeblink =  [[NSMutableArray alloc] init];
    isAnythingPageOrWeblinkValue =  [[NSMutableArray alloc] init];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                  selector:@selector(showAnyThingPage:)
                                                      name:@"AnyThing"
                                                    object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showWebPage:)
                                                 name:@"WebPage"
                                               object:nil];
    
   
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getslides?hubCitiKey=%@",BASE_URL, HUBCITIKEY];
    NSLog(@"url %@ ",urlString);
    NSString* responseJson = [ConnectionManager establishGetConnectionforJsonData:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
    NSData *data = [responseJson dataUsingEncoding:NSUTF8StringEncoding];
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    NSLog(@"tutorial response %@",responseData);
    if (responseCode  == 10000) {
        [defaults setBool:YES forKey:@"isFirstTimeTutorial"];
        
        
        if ([responseText isEqualToString:@"Success"]) {
            self.view.backgroundColor = [UIColor blackColor];
            NSArray* slideArr = [responseData objectForKey:@"slideImages"];
            for (int i = 0; i<slideArr.count; i++) {
                NSString* url =[slideArr[i] objectForKey:@"link"];
                if ([url containsString:@" "]) {
                    url = [url stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                }
                
                [imageArr addObject:url];
                [typeArr addObject:[[slideArr[i] objectForKey:@"Type"] stringValue]];
                if ([slideArr[i] valueForKey:@"isAnythingPageOrWebLink"]  != nil) {
                     [isAnythingPageOrWeblink addObject:[slideArr[i] objectForKey:@"isAnythingPageOrWebLink"]];
                }
                else{
                    [isAnythingPageOrWeblink addObject:@"N/A"];
                }
                if ([slideArr[i] valueForKey:@"isAnythingPageOrWebLinkValue"] != nil) {
                    NSString* url = [slideArr[i] objectForKey:@"isAnythingPageOrWebLinkValue"];
                    if ([url containsString:@" "]) {
                        url = [url stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                    }
                    [isAnythingPageOrWeblinkValue addObject:url];
                }
                else{
                    [isAnythingPageOrWeblinkValue addObject:@"N/A"];
                }
               
            }
            if (slideArr.count == 0) {
                NSString* url = [responseData objectForKey:@"logoPath"];
                if ([url containsString:@" "]) {
                    url = [url stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                }
                [imageArr addObject:url];
                [typeArr addObject:@"1"];
                
            }
            
            
            
            NSLog(@"Success");
        }
    }
    
    else{
        
        self.view.backgroundColor = [UIColor whiteColor];
        [UtilityManager showAlert:nil msg:responseText];
       
        return;
    }
    
    [self setup];
    
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
   
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(TutorialChildViewController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
    
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(TutorialChildViewController *)viewController index];
    
    index++;
   
    
    if (index == imageArr.count) {
        return nil;
    }
        return [self viewControllerAtIndex:index];
    
}

- (TutorialChildViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    TutorialChildViewController *childViewController = [[TutorialChildViewController alloc] initWithNibName:@"TutorialChildViewController" bundle:nil];
    childViewController.index = index;
    childViewController.typeArr = [typeArr copy];
    childViewController.isAnythingPageOrWeblink = [isAnythingPageOrWeblink copy];
    childViewController.isAnythingPageOrWeblinkValue = [isAnythingPageOrWeblinkValue copy];
    childViewController.skip = skip;
    childViewController.signup = signup;
    childViewController.imageArray = [imageArr copy];
   
    
    [self.navigationController pushViewController:childViewController animated:NO];
    return childViewController;
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return imageArr.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation

{
    
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

- (BOOL)shouldAutorotate{
    
    return NO;
    
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    
    return UIInterfaceOrientationPortrait;
    
}



- (UIInterfaceOrientationMask)supportedInterfaceOrientations

{
    
    return UIInterfaceOrientationMaskPortrait;
    
}



@end

//
//  UserSettingsController.m
//  HubCiti
//
//  Created by Anjana on 10/1/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "UserSettingsController.h"
#import "MainMenuViewController.h"
#import "CommonUtility.h"
#import "HubCitiManager.h"
#import "LocationManager.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@interface UserSettingsController ()

@end

@implementation UserSettingsController

#pragma mark - View lifecycle methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addConstraints];
    
    self.title = NSLocalizedString(@"Settings",@"Settings");
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    [locServSwitch setAccessibilityLabel:@"location"];
    [radius setAccessibilityLabel:@"radius"];
    if  ([[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        DLog(@"Setting On");
        [locServSwitch setOn:YES];
    }
    else {
        DLog(@"Setting Off");
        [locServSwitch setOn:NO];
    }
    
    [self getUserSettings];
    
    //customize back button and main menu button displayes on the navigation bar.
        self.navigationItem.hidesBackButton = YES;
    // //[back release];
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"MainMenu"];
    //    //[mainPage release];
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    ////[mainPage release];
    
    
    radius.delegate = self;
    radius.autocorrectionType = UITextAutocorrectionTypeNo;
    [radius setClearButtonMode:UITextFieldViewModeWhileEditing];
    
    UIButton *saveBtn = [UtilityManager customizeBarButtonItem:@"Save"];
    [saveBtn setAccessibilityLabel:@"save"];
    [saveBtn addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [save setCustomView:saveBtn];
    
    [bottomToolBar setBarTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]]];
    
    /***** Logic to prevent View from expanding for iOS7 version *******/
    if([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];

    //save.enabled = FALSE;
    
}
// Added by Keshava on 6/11/2014 for iPad Conversion.
// This will add auto layout constraints for the iPad
-(void)addConstraints{
    
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        
        headerLabel.translatesAutoresizingMaskIntoConstraints=NO;
        switchLabel.translatesAutoresizingMaskIntoConstraints=NO;
        pushLabel.translatesAutoresizingMaskIntoConstraints=NO;
        radiusLabel.translatesAutoresizingMaskIntoConstraints=NO;
        milesLabel.translatesAutoresizingMaskIntoConstraints=NO;
        locServSwitch.translatesAutoresizingMaskIntoConstraints=NO;
        pushNotifnSwitch.translatesAutoresizingMaskIntoConstraints=NO;
        radius.translatesAutoresizingMaskIntoConstraints=NO;
        
        NSDictionary *viewsDictionary = @{@"header":headerLabel,@"slabel":switchLabel,@"rlabel":radiusLabel,@"mlabel":milesLabel,@"lSwitch":locServSwitch,@"rfield":radius,@"plabel":pushLabel,@"pSwitch":pushNotifnSwitch};
        
        NSArray *headerLabel_VConstraint = [NSLayoutConstraint
                                            constraintsWithVisualFormat:@"V:|-100-[header]"                                                                                   options:0                                                                                   metrics:nil                                                                                     views:viewsDictionary];
        
        NSArray *headerLabel_HConstaint = [NSLayoutConstraint
                                           constraintsWithVisualFormat:@"H:|-250-[header(350)]-200-|"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary];
        
        headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:22.0];
        
        
        NSArray *switchLabel_VConstarint = [NSLayoutConstraint
                                            constraintsWithVisualFormat:@"V:|-200-[slabel]"                                                                                   options:0                                                                                   metrics:nil                                                                                     views:viewsDictionary];
        
        
        switchLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20.0];
        
        NSArray *pushLabel_VConstarint = [NSLayoutConstraint
                                          constraintsWithVisualFormat:@"V:|-300-[plabel]"                                                                                   options:0                                                                                   metrics:nil                                                                                     views:viewsDictionary];
        
        
        pushLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20.0];
        
        
        NSArray *radiusLabel_VConstraint = [NSLayoutConstraint
                                            constraintsWithVisualFormat:@"V:|-400-[rlabel]"                                                                                   options:0                                                                                   metrics:nil                                                                                     views:viewsDictionary];
        
        
        radiusLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20.0];
        
        
        NSArray *locServer_VConstraint = [NSLayoutConstraint
                                          constraintsWithVisualFormat:@"V:|-200-[lSwitch]"                                                                                   options:0                                                                                   metrics:nil                                                                                     views:viewsDictionary];
        
        NSArray *locServer_HConstraint = [NSLayoutConstraint
                                          constraintsWithVisualFormat:@"H:|-130-[slabel(350)]-70-[lSwitch]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary];
        
        NSArray *push_VConstraint = [NSLayoutConstraint
                                     constraintsWithVisualFormat:@"V:|-300-[pSwitch]"                                                                                   options:0                                                                                   metrics:nil                                                                                     views:viewsDictionary];
        
        NSArray *push_HConstraint = [NSLayoutConstraint
                                     constraintsWithVisualFormat:@"H:|-130-[plabel(350)]-70-[pSwitch]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary];
        
        NSArray *radius_VConstraint = [NSLayoutConstraint
                                       constraintsWithVisualFormat:@"V:|-400-[rfield]"                                                                                   options:0                                                                                   metrics:nil                                                                                     views:viewsDictionary];
        radius.font = [UIFont fontWithName:@"HelveticaNeue" size:20.0];
        
        NSArray *miles_VConstraint = [NSLayoutConstraint
                                      constraintsWithVisualFormat:@"V:|-405-[mlabel]"                                                                                   options:0                                                                                   metrics:nil                                                                                     views:viewsDictionary];
        
        NSArray *radiusLabel_HConstarint = [NSLayoutConstraint
                                            constraintsWithVisualFormat:@"H:|-130-[rlabel(350)]-70-[rfield(100)]-10-[mlabel]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary];
        
        
        
        
        [self.view addConstraints:headerLabel_VConstraint];
        [self.view addConstraints:headerLabel_HConstaint];
        [self.view addConstraints:switchLabel_VConstarint];
        [self.view addConstraints:locServer_VConstraint];
        [self.view addConstraints:locServer_HConstraint];
        [self.view addConstraints:pushLabel_VConstarint];
        [self.view addConstraints:push_VConstraint];
        [self.view addConstraints:push_HConstraint];
        [self.view addConstraints:radiusLabel_VConstraint];
        [self.view addConstraints:radius_VConstraint];
        [self.view addConstraints:miles_VConstraint];
        [self.view addConstraints:radiusLabel_HConstarint];
        
    }
}

/*
 *
 Navigate back to previous page
 *
 */
-(void)popBackToPreviousPage{
    
    [self.navigationController popViewControllerAnimated:NO];
}


/*
 *
 Navigate back to Main Menu screen page
 *
 */-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(void) getUserSettings
{
    
    NSString *reqStr = [NSString stringWithFormat:@"<AuthenticateUser><deviceID>%@</deviceID><userId>%@</userId><hubCitiId>%@</hubCitiId></AuthenticateUser>",[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusersettings",BASE_URL];
   
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:reqStr];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
    if (responseCodeElement != nil && ![[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
        
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        return;
    }
    
    TBXMLElement *localeRadiusElement = [TBXML childElementNamed:@"localeRadius" parentElement:tbxml.rootXMLElement];
    TBXMLElement *bLocServiceElement = [TBXML childElementNamed:@"bLocService" parentElement:tbxml.rootXMLElement];
    if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"false"])
    {
        [SharedManager setGps_allow_flag:NO];
        [defaults setValue:@"NO" forKey:@"allowingGPS"];
        [locServSwitch setOn:NO];
    }
    else if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"true"])
    {
        [SharedManager setGps_allow_flag:YES];
        [defaults setValue:@"YES" forKey:@"allowingGPS"];
        [locServSwitch setOn:YES];
    }
    TBXMLElement *pushNotifyElement = [TBXML childElementNamed:@"pushNotify" parentElement:tbxml.rootXMLElement];
    radius.text = [TBXML textForElement:localeRadiusElement];
    [defaults setValue:radius.text forKey:@"FindRadius"];
    
    if ([[TBXML textForElement:pushNotifyElement] isEqualToString:@"true"]) {
        [pushNotifnSwitch setOn:YES];
    }
    else {
        [pushNotifnSwitch setOn:NO];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    save.enabled = TRUE;
}

//limit the number of digits in locale radius to 2
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    BOOL returnValue = YES;
    NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    if (range.length == 1){
        returnValue = YES;
    }
    else {
        
        for (int j = 0; j<[string length]; j++) {
            unichar c = [string characterAtIndex:j];
            if ([charSet characterIsMember:c]) {
                NSUInteger textLength = [textField.text length] + [string length] - range.length;
                returnValue = (textLength > 2)? NO : YES;
            }
            else {
                returnValue = NO;
            }
            
        }
    }
    
    return returnValue;
}

/*
 *
 Call service to send request to save the changes
 *
 */
-(IBAction)saveButtonClicked:(id)sender{
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
       
    }
    else {
		      
        [self requestforUserSettings];
    }
}

/*
 *
 send request with data to save user settings
 *
 */
-(void)requestforUserSettings
{
    NSString *pushNotifyString;
    if (pushNotifnSwitch.on) {
        pushNotifyString = @"true";
    }
    else {
        pushNotifyString = @"false";
    }
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<UserSettings><userId>%@</userId><deviceId>%@</deviceId>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_DEVICEID]];
    [requestStr appendFormat:@"<pushNotify>%@</pushNotify><hubCitiId>%@</hubCitiId>",pushNotifyString,[defaults valueForKey:KEY_HUBCITIID]];
    if (locServSwitch.isOn)
    {
        [requestStr appendFormat:@"<bLocService>true</bLocService>"];
        [SharedManager setGps_allow_flag:YES];
        [defaults setValue:@"YES" forKey:@"allowingGPS"];
    }
    else
    {
        [requestStr appendFormat:@"<bLocService>false</bLocService>"];
        [SharedManager setGps_allow_flag:NO];
        [defaults setValue:@"NO" forKey:@"allowingGPS"];
    }
    radius.text = [radius.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [defaults setValue:radius.text forKey:@"FindRadius"];
    if([radius.text length] <=0)
        radius.text = @"0";
    [requestStr appendFormat:@"<localeRadius>%@</localeRadius></UserSettings>",radius.text];

   NSString *urlString = [NSString stringWithFormat:@"%@firstuse/setusersettings",BASE_URL];
    
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    ReleaseAndNilify(requestStr);
    ReleaseAndNilify(pushNotifyString);
}


/*
 *
 Parsing logic when response received for user settings.
 *
 */
-(void)parseUserSettingsResponseData:(NSString *) responseXml
{
    //parse the response
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]) {
        
        ReleaseAndNilify(responseXml);
        return;
    }
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]){
        
        if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && ![[defaults  valueForKey:KEY_LATITUDE] length]){
            
            [Location updateLocationinfo:self];
            //sleep(4);
        }
        
        else if ([SharedManager gps_allow_flag] == NO){//reset the values on switching off
            
            [defaults setValue:@"" forKey:KEY_LATITUDE];
            [defaults setValue:@"" forKey:KEY_LONGITUDE];
            
            [self saveAlert:@"Saved"];
            
        }
        
        else
        {//device level off but in app settings its on
            [self saveAlert:@"Saved"];
          
            
        }
    }
    else{
        [self saveAlert:[TBXML textForElement:responseTextElement]];
        
       
        
    }
}
-(void) saveAlert:(NSString *)message{
    
    UIAlertController * alert;
    
        alert=[UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action){
                                 [self popBackToPreviousPage];
                             }];
    [alert addAction:ok];
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];

    
}


/*
 *
 asynchronous req/resp delegate method
 *
 */
-(void)responseData:(NSString *) responseXml
{
    [self parseUserSettingsResponseData:responseXml];
}

-(IBAction)flippedLocServSwitch:(id)sender
{
    save.enabled = TRUE;
    DLog(@"flipped");
//    if (![[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"]) {
//        [SharedManager setGps_allow_flag:YES];
//        [defaults  setValue:@"YES" forKey:@"allowingGPS"];
//        DLog(@"Setting to ON");
//    }
//    else
//    {
//        DLog(@"Setting To OFF");
//        [SharedManager setGps_allow_flag:NO];
//        [defaults  setValue:@"NO" forKey:@"allowingGPS"];
//        
//    }
    
}
/*
 *
 methos called when an action on push notification switch
 *
 */
-(IBAction)flippedPushNotSwitch:(id)sender
{
    save.enabled = TRUE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) updatedLatlongValue :(NSString *) latValue longitudeValue:(NSString *)longValue{
    
    [defaults setObject:latValue forKey:KEY_LATITUDE];
    [defaults setObject:longValue forKey:KEY_LONGITUDE];
    
    // Banner the message given by the server
    [self saveAlert:@"Saved"];
   
    
    
}

@end

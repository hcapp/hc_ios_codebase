//
//  SettingsViewController.h
//  HubCiti
//
//  Created by Anjana on 10/1/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

/*****************************************************************
 Class designed to display various settings tools in the application
 *****************************************************************/

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@class FAQCategoryList;
@interface SettingsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CustomizedNavControllerDelegate> {
    
	IBOutlet UITableView *table_settings;
    NSIndexPath *selectedIndexPath;
    
    WebRequestState iWebRequestState;
    int faqExist;
    CommonUtility *common;
    AppDelegate *appDelegate;
}
@property(nonatomic,strong) FAQCategoryList *faqList;
@property(strong,nonatomic) NSMutableArray *rssFeedListDOArr;
@property(assign) BOOL fromNewsSideSetting;

@end

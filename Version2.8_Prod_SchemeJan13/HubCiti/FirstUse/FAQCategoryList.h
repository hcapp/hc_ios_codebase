//
//  FAQCategoryList.h
//  HubCiti
//
//  Created by Keerthi on 28/02/14.
//  Copyright (c) 2014 span infotech india ptv. ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FaqDetailsDo.h"
#import "NSString+URLEncoding.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
@class EventsListViewController;
@class FundraiserListViewController;
@class FilterListViewController;
@class DealHotDealsList;
@class FilterRetailersList;
@class RetailersListViewController;
@class CityExperienceViewController;
@class AnyViewController;
@class EmailShareViewController;
@interface FAQCategoryList : UIViewController<UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate,UISearchBarDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>
{
    UITableView *faqTableView;
    NSIndexPath *selectedIndexPath;
    FaqDetailsDo *faqDetailsObj;
    NSMutableArray *faqDetailsArray;
    int nextPage,selectedCatId;
    NSString *title;
    NSMutableArray *arrFAQBottomButtonDO;
    NSMutableArray *arrBottomButtonViewContainer;
    NSString *androidDownloadLink;
    int selectedRow;
    WebRequestState iWebRequestState;
    CommonUtility *common;
    
    IBOutlet UISearchBar *searchFAQ;
    UIButton *cancelBtn;
    int bottomBtn;
    NSString *searchResult;
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,retain) CityExperienceViewController *cityExp;
@property(nonatomic,retain)NSString *searchResult;
@property(nonatomic,retain) FilterRetailersList *filters;
@property(nonatomic,retain) RetailersListViewController* retailerListScreen;
@property(nonatomic,retain) DealHotDealsList* hotDeals;
@property(nonatomic,retain) FilterListViewController *filterList;
@property(nonatomic,retain) FundraiserListViewController *iFundraiserListViewController;
@property(nonatomic,retain) EventsListViewController* iEventsListViewController;
@end

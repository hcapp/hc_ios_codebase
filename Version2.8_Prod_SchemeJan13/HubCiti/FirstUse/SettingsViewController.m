//
//  SettingsViewController.m
//  HubCiti
//
//  Created by Anjana on 10/1/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "SettingsViewController.h"
#import "UserInformationViewController.h"
#import "PreferredCategoriesScreen.h"
#import "UserSettingsController.h"
#import "MainMenuViewController.h"
#import "HubCitiManager.h"
#import "FAQCategoryList.h"
#import "CityPreferenceViewController.h"
#import "APNSListViewController.h"
#include "GetUserInfoResponse.h"
#import "WebBrowserViewController.h"
#import "HTTPClient.h"
#import "HotDealDetailViewController.h"
#import "NewCouponDetailViewController.h"
#import "RetailerSpecialOffersViewController.h"
#import "NativeSpecialOfferViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"


@interface SettingsViewController ()<HTTPClientDelegate>
{
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;

@end

@implementation SettingsViewController
@synthesize infoResponse;
@synthesize rssFeedListDOArr,fromNewsSideSetting;

#pragma mark - View lifecycle methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - Service Respone methods

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
   
}


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    [self responseData:responseObject];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setAccessibilityLabel:@"SettingsPage"];
    [table_settings setAccessibilityLabel:@"SettingsTable"];
    [table_settings setAccessibilityIdentifier:@"GeneralSettings"];
    
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    //    if(DEVICE_TYPE==UIUserInterfaceIdiomPad)
    //    {self.navigationController.navigationBar.frame=CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, 60);
    //        [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:18], NSFontAttributeName, nil]];
    //    }
    //customize back button and main menu button displayes on the navigation bar.
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"MainMenu"];
    //    //[mainPage release];
    //
    
    //notification
     [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    // //[mainPage release];
    
    //title
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    [cusNav setTitle:@"App Settings" forView:self withHambergur:YES];
    
    //self.navigationItem.title = NSLocalizedString(@"User Settings",@"User Settings");
    [self.navigationController setToolbarHidden:YES];
    
    /***** Logic to prevent View from expanding for iOS7 version *******/
    dispatch_async(dispatch_get_main_queue(), ^{
        if([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    });
    
    
    [self req_faqCheck];
   
    
}

-(void)req_faqCheck{
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getfaqsetting",BASE_URL];
    NSMutableString *reqString = [[NSMutableString alloc]initWithFormat:@"<UserDetails><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    [reqString appendFormat:@"<hubCitiId>%@</hubCitiId></UserDetails>",[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:reqString];
    [self parsefaqCheck:responseXML];
    ReleaseAndNilify(reqString);
    
}

-(void)parsefaqCheck:(NSString *)responseXml{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
    {
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        
        TBXMLElement *isFaqExistElement = [TBXML childElementNamed:@"isFaqExist" parentElement:tbXml.rootXMLElement];
        
        if (isFaqExistElement) {
            
            faqExist = (int)[[TBXML textForElement:isFaqExistElement]integerValue];
        }
    }
    
}

-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: NO];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
}



/*
 *
 Navigate back to Main Menu screen page
 *
 */-(void)returnToMainPage:(id)sender
{
    //return to main page
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

/*
 *
 Navigate back to previous page
 *
 */
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - Tableview datasource and delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([RegionApp isEqualToString:@"1"])
    {
        if (faqExist){
            if ([defaults valueForKey:@"PushNotifyResponse"]==nil) {
                return 5;
            }
            else{
                return 6;
            }
        }
        
        else{
            if ([defaults valueForKey:@"PushNotifyResponse"]==nil) {
                return 4;
            }
            else{
                return 5;
            }
        }
        
    }
    else{
        
        if (faqExist){
            if ([defaults valueForKey:@"PushNotifyResponse"]==nil) {
                return 4;
            }
            else{
                return 5;
            }
        }
        
        else{
            if ([defaults valueForKey:@"PushNotifyResponse"]==nil) {
                return 3;
            }
            else{
                return 4;
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {return 55.0;
    }
    else
    {
        return 85.0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    int x_origin;
    
    int height ;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        x_origin=60;
        height = 55;
    }
    else
    {
        x_origin=80;
        height = 85;
    }
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UILabel *itemsLabel = [[UILabel alloc]initWithFrame:CGRectMake(x_origin, 0, VIEW_FRAME_WIDTH-x_origin,height)];
    itemsLabel.backgroundColor = [UIColor clearColor];
    itemsLabel.numberOfLines = 2;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        itemsLabel.font = [UIFont boldSystemFontOfSize:16];
    }
    else
    {
        itemsLabel.font = [UIFont boldSystemFontOfSize:22];
    }
    [cell addSubview:itemsLabel];
    //[itemsLabel release];
    
    if ([RegionApp isEqualToString:@"1"]) {
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            switch (indexPath.row) {
                    
                case 0:
                    cell.imageView.image = [UIImage imageNamed:@"UserInfoIcon.png"];
                    itemsLabel.text = NSLocalizedString(@"User Information",@"User Information");
                    break;
                    
                case 1:
                    cell.imageView.image = [UIImage imageNamed:@"SettingsIcon.png"];
                    itemsLabel.text =  NSLocalizedString(@"Location Preferences",@"Location Preferences");
                    break;
                    
                case 2:
                    cell.imageView.image = [UIImage imageNamed:@"citysettings.png"];
                    itemsLabel.text =  NSLocalizedString(@"City Favorites",@"City Favorites");
                    break;
                    
                case 3:
                    cell.imageView.image = [UIImage imageNamed:@"PreferencesIcon.png"];
                    itemsLabel.text =  NSLocalizedString(@"Category Favorites","Category Favorites");
                    break;
                    
                case 4:
                    if (faqExist==1) {
                        cell.imageView.image=[UIImage imageNamed:@"FAQ_Icon.png"];
                        itemsLabel.text=NSLocalizedString(@"FAQ's","FAQ's");
                    }
                    else{
                        cell.imageView.image=[UIImage imageNamed:@"Notification.png"];
                        itemsLabel.text=NSLocalizedString(@"Notifications","Notifications");
                    }
                    break;
                case 5:{
                    cell.imageView.image=[UIImage imageNamed:@"Notification.png"];
                    itemsLabel.text=NSLocalizedString(@"Notifications","Notifications");
                }
                    break;
                    
                    
                default:
                    break;
            }
            
        }
        else
        {
            switch (indexPath.row) {
                    
                case 0:
                    cell.imageView.image = [UIImage imageNamed:@"userIpad.png"];
                    itemsLabel.text = NSLocalizedString(@"User Information",@"User Information");
                    break;
                    
                case 1:
                    cell.imageView.image = [UIImage imageNamed:@"settingsIpad.png"];
                    itemsLabel.text =  NSLocalizedString(@"Location Preferences",@"Location Preferences");
                    break;
                    
                case 2:
                    cell.imageView.image = [UIImage imageNamed:@"citysettingsiPad.png"];
                    itemsLabel.text =  NSLocalizedString(@"City Favorites",@"City Favorites");
                    break;
                    
                case 3:
                    cell.imageView.image = [UIImage imageNamed:@"preferencesIpad.png"];
                    itemsLabel.text =  NSLocalizedString(@"Category Favorites","Category Favorites");
                    break;
                    
                case 4:
                    if (faqExist==1) {
                        cell.imageView.image=[UIImage imageNamed:@"faqIpad.png"];
                        itemsLabel.text=NSLocalizedString(@"FAQ's","FAQ's");
                    }
                    else{
                        cell.imageView.image=[UIImage imageNamed:@"Notificationipad.png"];
                        itemsLabel.text=NSLocalizedString(@"Notifications","Notifications");
                    }
                    break;
                case 5:{
                    cell.imageView.image=[UIImage imageNamed:@"Notificationipad.png"];
                    itemsLabel.text=NSLocalizedString(@"Notifications","Notifications");
                }
                    break;
                    
                default:
                    break;
            }
            
        }
        
        
    }
    else
    {
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            switch (indexPath.row) {
                    
                case 0:
                    cell.imageView.image = [UIImage imageNamed:@"UserInfoIcon.png"];
                    itemsLabel.text = NSLocalizedString(@"User Information",@"User Information");
                    break;
                    
                case 1:
                    cell.imageView.image = [UIImage imageNamed:@"SettingsIcon.png"];
                    itemsLabel.text =  NSLocalizedString(@"Location Preferences",@"Location Preferences");
                    break;
                    
                case 2:
                    cell.imageView.image = [UIImage imageNamed:@"PreferencesIcon.png"];
                    itemsLabel.text =  NSLocalizedString(@"Category Favorites","Category Favorites");
                    break;
                    
                case 3:
                    if (faqExist==1) {
                        cell.imageView.image=[UIImage imageNamed:@"FAQ_Icon.png"];
                        itemsLabel.text=NSLocalizedString(@"FAQ's","FAQ's");
                    }
                    else{
                        cell.imageView.image=[UIImage imageNamed:@"Notification.png"];
                        itemsLabel.text=NSLocalizedString(@"Notifications","Notifications");
                    }
                    break;
                case 4:{
                    cell.imageView.image=[UIImage imageNamed:@"Notification.png"];
                    itemsLabel.text=NSLocalizedString(@"Notifications","Notifications");
                }
                    break;
                    
                default:
                    break;
            }
            
        }
        else
        {
            switch (indexPath.row) {
                    
                case 0:
                    cell.imageView.image = [UIImage imageNamed:@"userIpad.png"];
                    itemsLabel.text = NSLocalizedString(@"User Information",@"User Information");
                    break;
                    
                case 1:
                    cell.imageView.image = [UIImage imageNamed:@"settingsIpad.png"];
                    itemsLabel.text =  NSLocalizedString(@"Location Preferences",@"Location Preferences");
                    break;
                    
                    
                case 2:
                    cell.imageView.image = [UIImage imageNamed:@"preferencesIpad.png"];
                    itemsLabel.text =  NSLocalizedString(@"Category Favorites","Category Favorites");
                    break;
                    
                case 3:if (faqExist==1) {
                    cell.imageView.image=[UIImage imageNamed:@"faqIpad.png"];
                    itemsLabel.text=NSLocalizedString(@"FAQ's","FAQ's");
                }
                else{
                    cell.imageView.image=[UIImage imageNamed:@"Notificationipad.png"];
                    itemsLabel.text=NSLocalizedString(@"Notifications","Notifications");
                }
                    break;
                case 4:{
                    cell.imageView.image=[UIImage imageNamed:@"Notificationipad.png"];
                    itemsLabel.text=NSLocalizedString(@"Notifications","Notifications");
                }
                    break;
                    
                    
                default:
                    break;
            }
            
        }
        
        
    }
    
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedIndexPath = indexPath;
    if ([RegionApp isEqualToString:@"1"]) {
        switch (indexPath.row) {
            case 0: {
                
                if([Network currentReachabilityStatus]==0)
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    
                    //[alert release];
                }
                else
                {
                    iWebRequestState = GETUSERINFO;;
                    [self requestToGetUserData];
                }
            }
                break;
                
            case 1: {
                
                if([Network currentReachabilityStatus]==0)
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                }
                else{
                    
                    UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:inform animated:NO];
                    //[inform release];
                }
            }
                break;
                
            case 2: {
                
                if([Network currentReachabilityStatus]==0)
                {
                    
                    [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                }
                else{
                    [SharedManager setUserInfoFromSignup:NO];
                    CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                    if(self.fromNewsSideSetting)
                    {
                        citPref.isNewsSideCityPref = TRUE;
                    }
                    else
                    {
                        citPref.isNewsSideCityPref = FALSE;
                    }
                    [self.navigationController pushViewController:citPref animated:NO];
                    // [citPref release];
                }
            }
                break;
                
            case 3: {
                
                if([Network currentReachabilityStatus]==0)
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    
                }
                else
                {
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                    
                }
            }
                break;
                
            case 4: {
                if (faqExist==1) {
                    
                    FAQCategoryList *faqlist = [[FAQCategoryList alloc] initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:faqlist animated:NO];
                    self.faqList = faqlist;
                    //  [faqlist release];
                }
                else{
                    [self sendResetBadgeRequest];
                    rssFeedListDOArr = [[NSMutableArray alloc]init];
                    if ([defaults valueForKey:@"PushNotifyResponse"]) {
                        
                        NSMutableArray* notificationMsgList = [defaults valueForKey:@"PushNotifyResponse"];
                        // NSArray* uniqueArray = [self removeDuplicate : notificationMsgList];
                        
                        [self parsePushNotify:notificationMsgList];
                    }
                    
                    // [pushotifyList release];
                    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
                }
                break;
            }
            case 5:{
                [self sendResetBadgeRequest];
                rssFeedListDOArr = [[NSMutableArray alloc]init];
                if ([defaults valueForKey:@"PushNotifyResponse"]) {
                    
                    NSMutableArray* notificationMsgList = [defaults valueForKey:@"PushNotifyResponse"];
                    // NSArray* uniqueArray = [self removeDuplicate : notificationMsgList];
                    
                    [self parsePushNotify:notificationMsgList];
                }
                // [pushotifyList release];
                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
            }
                break;
                
        }
        
        
    }
    else
    {
        switch (indexPath.row) {
            case 0: {
                
                if([Network currentReachabilityStatus]==0)
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                }
                else
                {
                    iWebRequestState = GETUSERINFO;;
                    [self requestToGetUserData];
                }
            }
                break;
                
            case 1: {
                
                if([Network currentReachabilityStatus]==0)
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    
                }
                else{
                    
                    UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:inform animated:NO];
                    // [inform release];
                }
            }
                break;
                
                
            case 2: {
                
                if([Network currentReachabilityStatus]==0)
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    
                }
                else
                {
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                    
                }
            }
                break;
                
            case 3: {
                if (faqExist==1) {
                    
                    FAQCategoryList *faqlist = [[FAQCategoryList alloc] initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:faqlist animated:NO];
                    self.faqList = faqlist;
                    //[faqlist release];
                }
                else{
                    [self sendResetBadgeRequest];
                    rssFeedListDOArr = [[NSMutableArray alloc]init];
                    if ([defaults valueForKey:@"PushNotifyResponse"]) {
                        
                        NSMutableArray* notificationMsgList = [defaults valueForKey:@"PushNotifyResponse"];
                        //NSArray* uniqueArray = [self removeDuplicate : notificationMsgList];
                        
                        [self parsePushNotify:notificationMsgList];
                    }
                    
                    //[pushotifyList release];
                    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
                }
                break;
            }
            case 4:{
                [self sendResetBadgeRequest];
                rssFeedListDOArr = [[NSMutableArray alloc]init];
                if ([defaults valueForKey:@"PushNotifyResponse"]) {
                    
                    NSMutableArray* notificationMsgList = [defaults valueForKey:@"PushNotifyResponse"];
                    // NSArray* uniqueArray = [self removeDuplicate : notificationMsgList];
                    
                    [self parsePushNotify:notificationMsgList];
                }
                
                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
            }
                break;
        }
    }
    
    [table_settings deselectRowAtIndexPath:selectedIndexPath animated:YES];
}


-(void) sendResetBadgeRequest
{
    
    NSString* userid = [defaults valueForKey:KEY_USERID];
    NSString *urlString;
    if (userid.length > 0) {
        
        urlString = [NSString stringWithFormat:@"%@firstuse/resetbadge?hubCitiId=%@&deviceId=%@&userId=%@",BASE_URL,[defaults objectForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_USERID]];
    }
    else{
        urlString = [NSString stringWithFormat:@"%@firstuse/resetbadge?hubCitiId=%@&deviceId=%@",BASE_URL,[defaults objectForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_DEVICEID]];
        
    }
    
    
    
    NSString* responseJson = [ConnectionManager establishGetConnectionforJsonData:urlString];
    NSData *data = [responseJson dataUsingEncoding:NSUTF8StringEncoding];
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    
    if (responseCode  == 10000) {
        
        if ([responseText isEqualToString:@"Success"]) {
            
            [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
            NSLog(@"Success");
        }
    }
    }



-(int) checkForExpiry : (NSString*) endDate : (NSString*) endTime
{
    NSString* dateString;
    if (endDate.length > 0  && endTime.length > 0) {
        
        dateString = [NSString stringWithFormat:@"%@ %@", endDate, endTime];
    }
    NSDateFormatter *datePickerFormat = [[NSDateFormatter alloc] init];
    [datePickerFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    datePickerFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    
    
    NSString *today = [datePickerFormat stringFromDate:[NSDate date]];
    NSDate *currentDate = [datePickerFormat dateFromString:today];
    NSDate *serverDate = [datePickerFormat dateFromString:dateString];
    
    NSComparisonResult result;
    
    result = [currentDate compare:serverDate]; // comparing two dates
    
    if(result == NSOrderedAscending)
    {
        NSLog(@"current date is less than server date");
        return -1;
        
    }
    else if(result == NSOrderedDescending)
    {
        NSLog(@"current date is greater than server date ");
        return 1;
    }
    
    else if(result == NSOrderedSame)
    {
        NSLog(@"both are equal");
        return 0;
    }
    return 0;
    
}


-(void)parsePushNotify:(NSMutableArray*)responseObj{
    
    //  NSArray * responseObj = [self compareExpDate : responseObject];
    
    
    // TBXML *tbxml = [[TBXML tbxmlWithXMLString:str] ;
    //    NSError *error;
    //
    //    //NSDictionary *pushData = responseObj;
    //
    //
    //    NSData *data = [responseObj dataUsingEncoding:NSUTF8StringEncoding];
    //
    //    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];//response object is your response from server as NSData
    NSMutableArray *notificationExpireFilter = [responseObj mutableCopy];
    NSMutableArray *indexes = [[NSMutableArray alloc] init];
    for (int i = 0; i < [responseObj count]; i++) {
        
        NSDictionary *dic=[responseObj objectAtIndex:i];
        
        NSArray *rssFeedArr = nil; //[[NSArray alloc]init];
        NSArray *dealArr = nil;//[[NSArray alloc]init];
        
        if ([dic valueForKey:@"rssFeedList"]) {
            rssFeedArr = [dic valueForKey:@"rssFeedList"];
            RssFeedListDO *obj_rssFeedListDO = [[RssFeedListDO alloc]init];
            for (NSDictionary *dictionary in rssFeedArr) {
                //Staff *staff = [[Staff alloc] init];
                obj_rssFeedListDO.title = [dictionary objectForKey:@"title"];
                obj_rssFeedListDO.link = [dictionary objectForKey:@"link"];
                
            }
            
            [rssFeedListDOArr addObject:obj_rssFeedListDO];
        }
        if ([dic valueForKey:@"dealList"]) {
            dealArr = [dic valueForKey:@"dealList"];
            RssFeedListDO *obj_rssFeedListDO = [[RssFeedListDO alloc]init];
            for (NSDictionary *dictionary in dealArr) {
                //Staff *staff = [[Staff alloc] init];
                obj_rssFeedListDO.dealName  = [dictionary objectForKey:@"dealName"];
                obj_rssFeedListDO.type  = [dictionary objectForKey:@"type"];
                obj_rssFeedListDO.splUrl = [dictionary objectForKey:@"splUrl"];
                obj_rssFeedListDO.dealId = [dictionary objectForKey:@"dealId"];
                obj_rssFeedListDO.retailerId = [dictionary objectForKey:@"retailerId"];
                obj_rssFeedListDO.retailLocationId = [dictionary objectForKey:@"retailLocationId"];
                obj_rssFeedListDO.endDate = [dictionary objectForKey:@"endDate"];
                obj_rssFeedListDO.endTime = [dictionary objectForKey:@"endTime"];
            }
            
            if (![HUBCITIKEY isEqualToString:@"Tyler19"])
            {
                
                
                int compare = [self checkForExpiry : obj_rssFeedListDO.endDate : obj_rssFeedListDO.endTime];
                
                if (compare > 0) {
                    
                    NSLog(@"don't add expired coupon");
                    [indexes addObject:[NSNumber numberWithInt:i]];
                    
                    
                    
                }
                else
                {
                    [rssFeedListDOArr addObject:obj_rssFeedListDO];
                }
            }
            else{
                
                [rssFeedListDOArr addObject:obj_rssFeedListDO];
            }
            
            
        }
    }
    
    DLog(@"rssFeedListDOArr %@", rssFeedListDOArr);
    if(indexes.count>0)
    {
        for(int i=0;i<indexes.count;i++){
            NSInteger indexDel= [[indexes objectAtIndex:i] integerValue];
            
            if(notificationExpireFilter.count > 0)
                [notificationExpireFilter removeObjectAtIndex:indexDel-i];
            
            // remove expired deals from global array
            appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            if(appDelegate.notificationListArr.count > 0)
                [appDelegate.notificationListArr removeObjectAtIndex:indexDel-i];
        }
    }
    
    if (rssFeedListDOArr.count == 0) {
        
        [UtilityManager showAlert:nil msg:@"Deals has expired"];
       
        
        return;
    }
    
    if(responseObj.count > rssFeedListDOArr.count)
    {
        [UtilityManager showAlert:nil msg:@"Some Deals are expired"];
        [defaults setValue:notificationExpireFilter forKey:@"PushNotifyResponse"];
        DLog(@"PUSHNOTI:%@",[defaults valueForKey:@"PushNotifyResponse"]);
       
        
    }
    
    if ([rssFeedListDOArr count] < 2) {
        
        RssFeedListDO *obj_rssFeedListDO ;
        obj_rssFeedListDO = [rssFeedListDOArr objectAtIndex:0];
        
        //[defaults setBool:YES forKey:@"detailsPage"];
        
        if (obj_rssFeedListDO.title == nil) {
            
            
            if ([obj_rssFeedListDO.type isEqualToString:@"Hotdeals"]) {
                [defaults setValue:obj_rssFeedListDO.dealId forKey:@"HotDealId"];
                HotDealDetailViewController *hdd = [[HotDealDetailViewController alloc] initWithNibName:@"HotDealDetailViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:hdd animated:NO];
                // [hdd release];
            }
            
            else if([obj_rssFeedListDO.type isEqualToString:@"Coupons"]){
                [defaults setValue:obj_rssFeedListDO.dealId forKey:@"couponId"];
                NewCouponDetailViewController *obj_CouponGalleryDetail = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:obj_CouponGalleryDetail animated:NO];
                // //[obj_CouponGalleryDetail release];
            }
            
            else if ([obj_rssFeedListDO.type isEqualToString:@"SpecialOffers"]){
                
                if (obj_rssFeedListDO.splUrl == nil) {
                    [defaults setValue:obj_rssFeedListDO.dealId forKey:@"nativePageId"];
                    [defaults setValue:obj_rssFeedListDO.retailLocationId forKey:@"retailLocationID"];
                    [defaults setValue:obj_rssFeedListDO.retailerId forKey:@"retailerId"];
                    NativeSpecialOfferViewController *splOfferVC = [[NativeSpecialOfferViewController alloc]initWithNibName:@"NativeSpecialOfferViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:splOfferVC animated:NO];
                    // //[splOfferVC release];
                }
                
                else{
                    
                    [defaults setValue:obj_rssFeedListDO.splUrl forKey:KEY_URL];
                    
                    WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:urlDetail animated:NO];
                    ////[urlDetail release];
                }
            }
        }
        else{
            [defaults setValue:obj_rssFeedListDO.link forKey:KEY_URL];
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            ////[urlDetail release];
        }
    }
    else{
        APNSListViewController *pushotifyList = [[APNSListViewController alloc] initWithNibName:@"APNSListViewController" bundle:[NSBundle mainBundle]];
        pushotifyList.rssFeedListDOArr = [[NSMutableArray alloc]init];
        pushotifyList.rssFeedListDOArr = rssFeedListDOArr;
        [self.navigationController pushViewController:pushotifyList animated:NO];
    }
    
    
    
    
    
    //    TBXMLElement *rssFeedList = [TBXML childElementNamed:@"rssFeedList" parentElement:tbxml.rootXMLElement];
    //    if(rssFeedList){
    //
    //    TBXMLElement *RSSFeedMessage = [TBXML childElementNamed:@"RSSFeedMessage" parentElement:rssFeedList];
    //
    //
    //    while (RSSFeedMessage!=nil) {
    //        RssFeedListDO *obj_rssFeedListDO = [[RssFeedListDO alloc]init];
    //        TBXMLElement *titleElement = [TBXML childElementNamed:@"title" parentElement:RSSFeedMessage];
    //        TBXMLElement *linkElement = [TBXML childElementNamed:@"link" parentElement:RSSFeedMessage];
    //        if (linkElement) {
    //            obj_rssFeedListDO.link = [[NSString alloc]initWithString:[TBXML textForElement:linkElement]];
    //        }
    //        if(titleElement)
    //            obj_rssFeedListDO.title = [[NSString alloc]initWithString:[TBXML textForElement:titleElement]];
    //        RSSFeedMessage = [TBXML nextSiblingNamed:@"RSSFeedMessage" searchFromElement:RSSFeedMessage];
    //        [rssFeedListDOArr addObject:obj_rssFeedListDO];
    //    }
    //    }
    //
    //    TBXMLElement *dealList = [TBXML childElementNamed:@"dealList" parentElement:tbxml.rootXMLElement];
    //    if (dealList) {
    //        TBXMLElement *dealElement = [TBXML childElementNamed:@"Deal" parentElement:dealList];
    //        while (dealElement!=nil) {
    //            RssFeedListDO *obj_rssFeedListDO = [[RssFeedListDO alloc]init];
    //
    //            TBXMLElement *dealIdElement = [TBXML childElementNamed:@"dealId" parentElement:dealElement];
    //            TBXMLElement *dealNameElement = [TBXML childElementNamed:@"dealName" parentElement:dealElement];
    //            TBXMLElement *dealDescElement = [TBXML childElementNamed:@"dealDesc" parentElement:dealElement];
    //            TBXMLElement *typeElement = [TBXML childElementNamed:@"type" parentElement:dealElement];
    //            TBXMLElement *splUrlElement = [TBXML childElementNamed:@"splUrl" parentElement:dealElement];
    //
    //
    //            if(dealIdElement)
    //                obj_rssFeedListDO.dealId = [[NSString alloc]initWithString:[TBXML textForElement:dealIdElement]];
    //
    //            if(dealNameElement)
    //                obj_rssFeedListDO.dealName = [[NSString alloc]initWithString:[TBXML textForElement:dealNameElement]];
    //
    //            if(dealDescElement)
    //                obj_rssFeedListDO.dealDesc = [[NSString alloc]initWithString:[TBXML textForElement:dealDescElement]];
    //
    //            if(typeElement)
    //                obj_rssFeedListDO.type = [[NSString alloc]initWithString:[TBXML textForElement:typeElement]];
    //
    //            if(splUrlElement)
    //                obj_rssFeedListDO.splUrl = [[NSString alloc]initWithString:[TBXML textForElement:splUrlElement]];
    //
    //            dealElement = [TBXML nextSiblingNamed:@"Deal" searchFromElement:dealElement];
    //            [rssFeedListDOArr addObject:obj_rssFeedListDO];
    //        }
    //
    //    }
}






-(NSArray*) removeDuplicate : (NSMutableArray*) responseObj
{
    NSSet *set = [NSSet setWithArray:responseObj];
    NSArray *uniqueArray = [set allObjects];
    return uniqueArray;
}






-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}

-(void)parseGetPreferredCategories:(NSString*)response
{
    if ([UtilityManager isNullOrEmptyString:response]){
        [UtilityManager showAlert:@"Error!" msg:@"Please try after sometime"];
        
        return;
    }
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCodeElement != nil && [[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {//means we have got proper response
        
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:settings animated:NO];
        //[settings release];
        
    }
    
    else {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    ReleaseAndNilify(tbxml);
}

/*
 *
 send request with data to get user details.
 request sent when navigated from settings screen.
 *
 */
-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}


/*
 *
 Parsing logic when response received for getting user details.
 Method called when navigated from Settings screen.
 *
 */
-(void)parseGetUserData:(id)response
{
    
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}

/*
 *
 asynchronous req/resp delegate method
 *
 */
-(void)responseData:(NSString *)response
{
    DLog(@"response %@",response);
    switch (iWebRequestState)
    {
        case GETFAVLOCATIONS:
            [self parseGetPreferredCategories:response];
            break;
            
        case GETUSERINFO:
            [self parseGetUserData:response];
            break;
            
        default:
            break;
    }
}
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  RegistrationViewController.h
//  HubCiti
//
//  Created by Anjana on 9/11/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

/*********************************************************
 Class is designed for user registration or signup screen
 Includes validations for all the fields on registering.
 Class is called form Login Screen on SignUp.
 *********************************************************/

#import <UIKit/UIKit.h>
#import "LoginDO.h"
#import "SignupDO.h"

@interface RegistrationViewController : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,CustomizedNavControllerDelegate>
{
    /* IBOutlet controls */
    IBOutlet UITableView *table_Signup;
    IBOutlet UIImageView *background_Signup;
    IBOutlet UIButton *button_Save;
    IBOutlet UIButton *button_SetPref;
    IBOutlet UIButton *button_GetStarted;
    
    UITextField *txtField_email;
    UITextField *txtField_password;
    UITextField *txtField_confirmPwd;
    UITextField * textField_emailAddress;
    
    LoginDO *signup_DO;
    SignupDO *regSuccessUI_DO;
    
    WebRequestState iWebRequestState;
}

@property(nonatomic,strong)LoginDO *signup_DO;


-(IBAction)saveBtnPressed:(id)sender;
-(IBAction)setPrefPressed:(id)sender;
-(IBAction)getStartedPressed:(id)sender;
-(BOOL)validateEmailAddress;
-(void)configureEmailCell:(UITableViewCell *)cell;
-(void)configurePasswordCell:(UITableViewCell *)cell;
-(void)configureConfirmPasswordCell:(UITableViewCell *)cell;
@end

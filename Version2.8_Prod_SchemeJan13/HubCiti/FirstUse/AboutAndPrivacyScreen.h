//
//  AboutAndPrivacyScreen.h
//  Demo
//
//  Created by ajit on 5/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutAndPrivacyScreen : UIViewController<UIWebViewDelegate,CustomizedNavControllerDelegate> {
	UILabel *screenName;	
	IBOutlet UIWebView *webView;
    UIActivityIndicatorView *activityIndicator;
}

@property(nonatomic,strong) NSMutableString *responseXml;
@property(nonatomic,strong) UIButton *backButton;
@property(nonatomic, strong) UILabel *screenName;
@property(nonatomic,assign) BOOL comingFromPrivacyScreen;

@end

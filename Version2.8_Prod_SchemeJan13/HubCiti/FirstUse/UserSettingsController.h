//
//  UserSettingsController.h
//  HubCiti
//
//  Created by Anjana on 10/1/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

/*****************************************************************
 Class designed to set various user specific settings 
 Class is called form Main Settings page. 
 It is basicallly used to enable/disble push notification service in the app 
 *****************************************************************/

#import <UIKit/UIKit.h>

@interface UserSettingsController : UIViewController<UITextFieldDelegate,CustomizedNavControllerDelegate>
{
    IBOutlet UIToolbar *bottomToolBar;
    IBOutlet UISwitch *pushNotifnSwitch;
	IBOutlet UIBarButtonItem *save;
    IBOutlet UISwitch *locServSwitch;
    IBOutlet UITextField *radius;
    
    IBOutlet UILabel *milesLabel;
    IBOutlet UILabel *switchLabel,*pushLabel;
    IBOutlet UILabel *headerLabel;
    IBOutlet UILabel *radiusLabel;
    
}
-(IBAction)saveButtonClicked:(id)sender;
-(IBAction)flippedPushNotSwitch:(id)sender;
-(IBAction)flippedLocServSwitch:(id)sender;
@end

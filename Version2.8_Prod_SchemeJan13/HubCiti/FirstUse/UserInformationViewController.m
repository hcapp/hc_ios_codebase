//
//  UserInformationViewController.m
//  HubCiti
//
//  Created by Anjana on 9/11/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "PreferredCategoriesScreen.h"
#import "CityPreferenceViewController.h"
#import "MainMenuViewController.h"
#import "HubCitiManager.h"
#import <QuartzCore/QuartzCore.h>
#import "HTTPClient.h"
#import "GetUserInfoResponse.h"
#import "UserInfoFields.h"
#import "ArIncomeRangeList.h"
#import "ArEducationList.h"
#import "ArMaritalList.h"
#include "AppDelegate.h"
#include "UpdateUserInfo.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"


#define k_first 1000
#define k_last 2000
#define k_email 3000
#define k_zip 4000
#define k_mobile 5000
#define k_pwd 6
#define k_confirmpwd 7
#define k_dob 8000

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.9;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

#define LEGAL	@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define NUMBERS	@"0123456789"

static __weak id currentFirstResponder;

@implementation UIResponder (FirstResponder)

+(id)currentFirstResponder {
    currentFirstResponder = nil;
    [[UIApplication sharedApplication] sendAction:@selector(findFirstResponder:) to:nil from:nil forEvent:nil];
    return currentFirstResponder;
}

-(void)findFirstResponder:(id)sender {
    currentFirstResponder = self;
}

@end

@implementation UserInfoDO

@synthesize position, fieldName, value;

@end

@interface UserInformationViewController ()<HTTPClientDelegate>
@property(nonatomic,retain) GetUserInfoResponse* infoResponse ;
@property(nonatomic,retain) UpdateUserInfo* updateResponse ;

@end

@implementation UserInformationViewController
@synthesize infoResponse,updateResponse;

static int i;
static NSMutableDictionary *dict_fieldName = nil;
static int elPickerIndex = 0;
static int msPickerIndex = 0;
static int irPickerIndex = 0;

bool dateFromServerFlag,dobShowFlag;

#pragma mark - View Lifecycle methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        if (dict_fieldName == nil) {//assigning number to each bottom buttons - in order to
            dict_fieldName = [[NSMutableDictionary alloc]init];
            [dict_fieldName setObject:[NSNumber numberWithInt:0] forKey:@"Below"];
            [dict_fieldName setObject:[NSNumber numberWithInt:1] forKey:@"First Name"];
            [dict_fieldName setObject:[NSNumber numberWithInt:2] forKey:@"Last Name"];
            [dict_fieldName setObject:[NSNumber numberWithInt:3] forKey:@"Email"];
            [dict_fieldName setObject:[NSNumber numberWithInt:4] forKey:@"Date Of Birth"];
            [dict_fieldName setObject:[NSNumber numberWithInt:5] forKey:@"Zip Code"];
            [dict_fieldName setObject:[NSNumber numberWithInt:6] forKey:@"Mobile"];
            [dict_fieldName setObject:[NSNumber numberWithInt:7] forKey:@"Gender"];
            [dict_fieldName setObject:[NSNumber numberWithInt:8] forKey:@"Education"];
            [dict_fieldName setObject:[NSNumber numberWithInt:9] forKey:@"Marital Status"];
            [dict_fieldName setObject:[NSNumber numberWithInt:10] forKey:@"Image"];
            [dict_fieldName setObject:[NSNumber numberWithInt:11] forKey:@"Income Range"];
            [dict_fieldName setObject:[NSNumber numberWithInt:12] forKey:@"Imagepath"];
        }
    }
    return self;
}
#pragma mark - Service Respone methods

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
  }


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    [self responseData:responseObject];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    dateFromServerFlag=TRUE;
    
    newFirstName=[[NSMutableString alloc]init];
    newLastName = [[NSMutableString alloc]init];
    newMobile = [[NSMutableString alloc]init];
    newZipCode = [[NSMutableString alloc]init];
    newEmail = [[NSMutableString alloc]init];
    
    firstNameFlag=lastNameFlag=mobileFlag=zipFlag=emailFlag=FALSE;
    
    table_UserInfo.backgroundColor = [UIColor clearColor];
    table_UserInfo.scrollEnabled = YES;
    table_UserInfo.tag = 0;
    table_UserInfo.userInteractionEnabled = YES;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        marStatusLabel = [[UILabel alloc]initWithFrame:CGRectMake(134,14,164,20)];
        marStatusLabel.font = [UIFont fontWithName:@"Helvetica" size:13];
        marStatusLabel.backgroundColor = [UIColor clearColor];
        
        eduLevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(134,14,164,20)];
        eduLevelLabel.font = [UIFont fontWithName:@"Helvetica" size:13];
        eduLevelLabel.backgroundColor = [UIColor clearColor];
        
        incRangeLabel = [[UILabel alloc]initWithFrame:CGRectMake(134,14,164,20)];
        incRangeLabel.font = [UIFont fontWithName:@"Helvetica" size:13];
        incRangeLabel.backgroundColor = [UIColor clearColor];
        
        dobLabel = [[UILabel alloc]initWithFrame:CGRectMake(134,14,164,20)];
        dobLabel.font = [UIFont fontWithName:@"Helvetica" size:13];
        dobLabel.backgroundColor = [UIColor clearColor];
        
    }
    else
    {
        marStatusLabel = [[UILabel alloc]initWithFrame:CGRectMake(214,14,754,30)];
        marStatusLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
        marStatusLabel.backgroundColor = [UIColor clearColor];
        
        eduLevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(214,14,754,30)];
        eduLevelLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
        eduLevelLabel.backgroundColor = [UIColor clearColor];
        
        incRangeLabel = [[UILabel alloc]initWithFrame:CGRectMake(214,14,754,30)];
        incRangeLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
        incRangeLabel.backgroundColor = [UIColor clearColor];
        
        dobLabel = [[UILabel alloc]initWithFrame:CGRectMake(214,14,754,30)];
        dobLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
        dobLabel.backgroundColor = [UIColor clearColor];
        
    }
    
    
    
    
    
    
    
    [defaults  setObject:@"1" forKey:@"Gender"];
    
    /************** label to be displayed on right of navigation bar**************/
    //    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 320, 40)];
    //    titleLabel.textColor = [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]];
    //    titleLabel.backgroundColor = [UIColor clearColor];
    //    titleLabel.font = [UIFont boldSystemFontOfSize:16];
    //    titleLabel.textAlignment = NSTextAlignmentCenter;
    //    titleLabel.text = NSLocalizedString(@"Tell Us About You",@"Tell Us About You");
    self.navigationItem.title = @"Tell Us About You";
    // ReleaseAndNilify(titleLabel);
    
    cpasswordView.hidden = YES;
    cpasswordView.layer.cornerRadius = 10;
    [cpasswordView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [cpasswordView.layer setBorderWidth:1.5f];
    [cpasswordView.layer setShadowColor:[UIColor blackColor].CGColor];
    [cpasswordView.layer setShadowOpacity:0.8];
    [cpasswordView.layer setShadowRadius:3.0];
    [cpasswordView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    txtField_ChangePwd.delegate = self;
    txtField_ChangePwd.tag = k_pwd;
    txtField_ConfirmPwd.delegate = self;
    txtField_ConfirmPwd.tag = k_confirmpwd;
    
  
    
    /***** Logic to prevent View from expanding for iOS7 version *******/
    if([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

/*
 *
 Method to set the bottom tool bar along with right and left bar buttons
 *
 */
-(void)setBottomToolbar
{
    CGRect rect;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        rect = CGRectMake(0, self.view.frame.size.height-44, [[UIScreen mainScreen] bounds].size.width, 44);
    }
    else
    {
        NSLog(@"Self =%f",self.view.frame.size.height);
        rect = CGRectMake(0, SCREEN_HEIGHT-88-20, SCREEN_WIDTH, 44);
    }
    
    bottomToolBar = [[UIToolbar alloc]initWithFrame:rect];
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        [bottomToolBar setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
        [bottomToolBar sizeToFit];
        
    }
    // bottomToolBar.barStyle = UIBarStyleBlackOpaque;
    bottomToolBar.barTintColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    
    [self.view addSubview:bottomToolBar];
    
    /********* set leftbar back button ***************/
    
    self.navigationItem.hidesBackButton = YES;
    
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    flexibleSpace.width = 247.0;
    
    NSArray *items;
    
    /******* set right bar button based on screen navigation(wheather from signup or settings *********/
    UIButton *privacyPolicyBtn = [UtilityManager customizeBarButtonItem:@"Privacy Policy"];
    [privacyPolicyBtn addTarget:self action:@selector(privacyPolicyClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *privacyBarBtn = [[UIBarButtonItem alloc] initWithCustomView:privacyPolicyBtn];
    
    customBarItem = [[UIBarButtonItem alloc] init];
    
    if ([SharedManager userInfoFromSignup] == NO){
        
        cusNav =(CustomizedNavController *) self.navigationController;
        cusNav.customNavBardelegate = self;
        [cusNav hideHambergerButton:YES];
        [cusNav hideBackButton:NO];
        [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
        
        //        UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
        //        [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
        //        UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
        //        self.navigationItem.rightBarButtonItem = mainPage;
        //        ReleaseAndNilify(mainPage);
        
        //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:CGRectMake(0, 0, 30, 30)];
        SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
        [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
        [btn addSubview:homeImage];
        
        
        [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
        mainPage.style = UIBarButtonItemStylePlain;
        
        self.navigationItem.rightBarButtonItem = mainPage;
        // //[mainPage release];
        
        
        UIButton *saveBtn = [UtilityManager customizeBarButtonItem:@"Save"];
        [saveBtn addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [customBarItem setCustomView:saveBtn];
        
        /************ parse response got from previous screen **************/
        [self parse_GetUserInfo:[defaults objectForKey:KEY_RESPONSEXML]];
        
        for(int i = 0; i < [userInfoObjArray count]; i++)
        {
            UserInfoDO *checkingInfo = [userInfoObjArray objectAtIndex:i];
            
            if([checkingInfo.fieldName isEqualToString:@"First Name"]){
                newFirstName = [checkingInfo.value mutableCopy];
            }
            if([checkingInfo.fieldName isEqualToString:@"Last Name"]){
                newLastName = [checkingInfo.value mutableCopy];
            }
            
            if([checkingInfo.fieldName isEqualToString:@"Mobile"]){
                newMobile = [checkingInfo.value mutableCopy];
            }
            if([checkingInfo.fieldName isEqualToString:@"Email"]&& newEmail.length == 0){
                newEmail = [checkingInfo.value mutableCopy];
                
            }
            
            if ([checkingInfo.fieldName isEqualToString:@"Zip Code"])
            {
                newZipCode = [checkingInfo.value mutableCopy];
                
            }
            if ([checkingInfo.fieldName isEqualToString:@"Marital Status"])
            {
                marStatusLabel.text = [checkingInfo.value mutableCopy];
                
            }
            if ([checkingInfo.fieldName isEqualToString:@"Education"])
            {
                eduLevelLabel.text = [checkingInfo.value mutableCopy];
                
            }
            if ([checkingInfo.fieldName isEqualToString:@"Income Range"])
            {
                incRangeLabel.text = [checkingInfo.value mutableCopy];
                
            }
            
        }
        
        
        
    }
    else if ([SharedManager userInfoFromSignup] == YES){
        cusNav =(CustomizedNavController *) self.navigationController;
        [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
        self.navigationItem.leftBarButtonItem = nil;//hide left button
       [cusNav hideBackButton:YES];
        [cusNav hideHambergerButton:YES];
        UIButton *nextBtn = [UtilityManager customizeBarButtonItem:@"Next"];
        [nextBtn addTarget:self action:@selector(navigateToNextPage:) forControlEvents:UIControlEventTouchUpInside];
        [customBarItem setCustomView:nextBtn];
        /************ parse response got from previous screen **************/
        [self parse_GetUserInfo:[defaults objectForKey:KEY_RESPONSEXML]];
        
    }
    items = [NSArray arrayWithObjects:privacyBarBtn,flexibleSpace, customBarItem, nil];
    [bottomToolBar setItems:items animated:NO];
    [self.view bringSubviewToFront:bottomToolBar];
    ReleaseAndNilify(flexibleSpace);
    ReleaseAndNilify(privacyBarBtn);
    ReleaseAndNilify(customBarItem);
    
}

-(void)viewWillAppear:(BOOL)animated{
    //table_UserInfo.scrollEnabled=NO;
    
    [super viewWillAppear:YES];
 
    self.navigationController.navigationBar.hidden = NO;
      [self setBottomToolbar];
    view_Picker.hidden = YES;
    
    if ([SharedManager userInfoFromSignup] == YES)
        customBarItem.enabled = TRUE;
    else
        customBarItem.enabled = FALSE;
    
}
/*
 *
 release various allocated objects here
 *
 */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark navigation barbutton actions
/*
 *
 Navigate back to Main Menu screen page
 *
 */-(void)returnToMainPage:(id)sender
{
    //return to main page
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - Table view delegates and datasource methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserInfoDO *userInfoObj = [userInfoObjArray objectAtIndex:indexPath.row];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        if ([userInfoObj.fieldName isEqualToString:@"Email"]){
            
            return 70;
        }
        else if ([userInfoObj.fieldName isEqualToString:@"Image"]){
            
            return 175;
        }
        else
            return 55;
    }
    else
    {
        if ([userInfoObj.fieldName isEqualToString:@"Email"]){
            
            return 85;
        }
        else if ([userInfoObj.fieldName isEqualToString:@"Image"]){
            
            return 215;
        }
        else
            return 70;
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([userInfoObjArray count])
        return [userInfoObjArray count];
    else
        return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int x_origin ;
    int y_origin,button_yorigin;
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        x_origin = 10;
    }
    else
    {
        x_origin= 50;
    }
    
    static NSString *cellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] ;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        y_origin = (cell.frame.size.height - LABEL_HEIGHT)/2;
        button_yorigin = (cell.frame.size.height - X_OFFSET)/2;
    }
    else
    {
        y_origin = (cell.frame.size.height - IPAD_LABEL_HEIGHT)/2;
        button_yorigin = (cell.frame.size.height - IPAD_X_OFFSET)/2;
    }
    
    UserInfoFields *userInfoObj = [userInfoObjArray objectAtIndex:indexPath.row];
    UILabel *label;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        label = [[UILabel alloc]initWithFrame:CGRectMake(x_origin, y_origin, 100, LABEL_HEIGHT)];
    }
    else
    {
        label = [[UILabel alloc]initWithFrame:CGRectMake(x_origin, y_origin, 100, IPAD_LABEL_HEIGHT)];
    }
    label.font =[UIFont fontWithName:@"Helvetica" size:14];
    label.backgroundColor = [UIColor clearColor];
    
    UITextField *txtField;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        txtField =  [[UITextField alloc] initWithFrame:CGRectMake(x_origin*2+label.frame.size.width, y_origin, VIEW_FRAME_WIDTH-label.frame.size.width-x_origin*3, TEXTFIELD_HEIGHT)];
        txtField.autocorrectionType = UITextAutocorrectionTypeNo;
    }
    else
    {
        txtField =  [[UITextField alloc] initWithFrame:CGRectMake(x_origin*2+label.frame.size.width, y_origin, VIEW_FRAME_WIDTH-label.frame.size.width-x_origin*3-20, IPAD_TEXTFIELD_HEIGHT)];
        txtField.autocorrectionType = UITextAutocorrectionTypeNo;
    }
    
    [txtField setClearButtonMode:UITextFieldViewModeWhileEditing];
    txtField.font = [UIFont fontWithName:@"Helvetica" size:13];
    txtField.borderStyle = UITextBorderStyleRoundedRect;
    txtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtField.delegate = self;
    
    
    switch ([[dict_fieldName objectForKey:userInfoObj.fieldName]intValue]) {
            
            //        case 0:{
            //
            //            UILabel *labelInfo;
            //            if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
            //            {
            //             labelInfo=[[UILabel alloc]initWithFrame:CGRectMake(x_origin, y_origin, 300, LABEL_HEIGHT)];
            //            }
            //            else
            //            {
            //               labelInfo=[[UILabel alloc]initWithFrame:CGRectMake(x_origin, y_origin, 300, IPAD_LABEL_HEIGHT)];
            //            }
            //            labelInfo.font =[UIFont systemFontOfSize:14];
            //            labelInfo.backgroundColor = [UIColor clearColor];
            //            labelInfo.text=@"Below Fields are Optional.";
            //            //labelInfo.text = userInfoObj.value;
            //
            //            [cell addSubview:labelInfo];
            //
            //
            //            ReleaseAndNilify(labelInfo);
            //            ReleaseAndNilify(label);
            //            ReleaseAndNilify(txtField);}
            //            break;
            
        case 1://firstname
        {
            if(userInfoObj.requiredField ){
                label.text = NSLocalizedString(@"First Name*", @"First Name");
                label.attributedText = [self getTagColor:label.text];
            }
            else
                label.text = NSLocalizedString(@"First Name", @"First Name");
            txtField.keyboardType = UIKeyboardTypeDefault;
            if (firstNameFlag==TRUE) {
                txtField.text=newFirstName;
            }
            else
            {
                txtField.text = userInfoObj.value;
            }
            txtField.tag = k_first;
            cell.tag = k_first;
            
            [cell addSubview:label];
            [cell addSubview:txtField];
            ReleaseAndNilify(label);
            ReleaseAndNilify(txtField);
        }
            break;
            
        case 2://lastname
        {
            if(userInfoObj.requiredField){
                label.text = NSLocalizedString(@"Last Name*", @"Last Name");
                label.attributedText = [self getTagColor:label.text];
            }
            
            
            else
                
                label.text = NSLocalizedString(@"Last Name", @"Last Name");
            txtField.keyboardType = UIKeyboardTypeDefault;
            if (lastNameFlag==TRUE) {
                txtField.text=newLastName;
            }
            else
            {
                txtField.text = userInfoObj.value;
            }
            txtField.tag = k_last;
            
            [cell addSubview:label];
            [cell addSubview:txtField];
            ReleaseAndNilify(label);
            ReleaseAndNilify(txtField);
        }
            break;
            
        case 3://email
        {
            if(userInfoObj.requiredField){
                label.text = NSLocalizedString(@"Email*", @"Email");
                
                label.attributedText = [self getTagColor:label.text];
                
            }
            else
                label.text = NSLocalizedString(@"Email", @"Email");
            txtField.placeholder=NSLocalizedString(@"abc@abc.com", @"abc@abc.com");
            txtField.keyboardType = UIKeyboardTypeEmailAddress;
            if (emailFlag==TRUE) {
                txtField.text = newEmail;
            }
            else
            {
                txtField.text = userInfoObj.value;
            }
            txtField.tag = k_email;
            
            /***** Includes "Change Password" action which displays another view *****/
            UIButton *cpwdButton = [UIButton buttonWithType:UIButtonTypeCustom];
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                cpwdButton.frame = CGRectMake(175, 45, 150, 20);
                cpwdButton.titleLabel.font=[UIFont fontWithName:@"Helvetica" size:13];
            }
            else
            {
                cpwdButton.frame = CGRectMake(499, 45, 150, 30);
                cpwdButton.titleLabel.font=[UIFont fontWithName:@"Helvetica" size:15];
            }
            
            
            [cpwdButton setTitle:NSLocalizedString(@"Change Password",@"Change Password") forState:UIControlStateNormal];
            [cpwdButton setTitleColor:[UIColor colorWithRGBInt:0x58A5CD] forState:UIControlStateNormal];
            cpwdButton.backgroundColor =[UIColor clearColor];
            [cpwdButton addTarget:self action:@selector(changePassword:) forControlEvents:UIControlEventTouchDown];
            [cell addSubview:cpwdButton];
            
            [cell addSubview:label];
            [cell addSubview:txtField];
            ReleaseAndNilify(label);
            ReleaseAndNilify(txtField);
        }
            break;
            
        case 4://DOB
        {
            if(userInfoObj.requiredField){
                label.text = NSLocalizedString(@"Date of Birth*", @"Date of Birth");
                label.attributedText = [self getTagColor:label.text];
            }
            else
                label.text = NSLocalizedString(@"Date of Birth", @"Date of Birth");
            
            UIButton *dropDownBtn5 = [UIButton buttonWithType:UIButtonTypeCustom];
            dropDownBtn5.frame = txtField.frame;
            dropDownBtn5.tag = 1;
            if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
            {
                [dropDownBtn5 setImage:[UIImage imageNamed:@"DropDownIconBig.png"] forState:UIControlStateNormal];
            }
            else
            {
                [dropDownBtn5 setImage:[UIImage imageNamed:@"DropDownIconBigIpad.png"] forState:UIControlStateNormal];
            }
            [dropDownBtn5 addTarget:self action:@selector(showDatePicker) forControlEvents:UIControlEventTouchUpInside];
            
            //            UIButton *dropDownBtn4 = [UIButton buttonWithType:UIButtonTypeCustom];
            //            CGRect rect = CGRectMake((txtField.frame.origin.x+160),txtField.frame.origin.y, 30, 30);
            //            dropDownBtn4.frame = rect;
            //            dropDownBtn4.tag = 4;
            //            [dropDownBtn4 setImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateNormal];
            //            [dropDownBtn4 addTarget:self action:@selector(showDatePicker) forControlEvents:UIControlEventTouchUpInside];
            //
            //            txtField.placeholder=NSLocalizedString(@"Date of Birth", @"Date of Birth");
            //            //txtField.keyboardType = UIKeyboardTypeEmailAddress;
            //            CGRect  textRect = CGRectMake(x_origin*2+label.frame.size.width, y_origin, VIEW_FRAME_WIDTH-label.frame.size.width-x_origin*3-30, TEXTFIELD_HEIGHT);
            //            txtField.frame=textRect;
            //            //txtField.text = userInfoObj.value;
            //            txtField.tag = k_dob;
            if (selectedDOB!=NULL) {
                dateFromServerFlag=FALSE;
                dobLabel.text=selectedDOB;
                //userInfoObj.value=selectedDOB;
                //[self textFieldDidEndEditing:txtField];
            }
            else
            {
                if ([userInfoObj.value length]) {
                    replyFromServer = TRUE;
                }
                dobLabel.text = userInfoObj.value;
                selectedDOB=userInfoObj.value;
                customBarItem.enabled=YES;
                
                
            }
            
            [cell addSubview:label];
            [cell addSubview:dropDownBtn5];
            [cell addSubview:dobLabel];
            ReleaseAndNilify(label);
            ReleaseAndNilify(txtField);
        }
            break;
            
        case 5://zipcode
        {
            if(userInfoObj.requiredField){
                label.text = NSLocalizedString(@"Zip*", @"Zip");
                label.attributedText = [self getTagColor:label.text];
            }
            else
                label.text = NSLocalizedString(@"Zip", @"Zip");
            txtField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            txtField.placeholder=NSLocalizedString(@"Should be of 5 digits", @"Should be of 5 digits");
            if (zipFlag==TRUE) {
                txtField.text = newZipCode;
            }
            else
            {
                txtField.text = userInfoObj.value;
            }
            txtField.tag = k_zip;
            
            
            [cell addSubview:label];
            [cell addSubview:txtField];
            ReleaseAndNilify(label);
            ReleaseAndNilify(txtField);
        }
            break;
            
        case 6://mobile
        {
            if(userInfoObj.requiredField){
                label.text = NSLocalizedString(@"Mobile #*", @"Mobile #");
                label.attributedText = [self getTagColor:label.text];
            }
            else
                label.text = NSLocalizedString(@"Mobile #", @"Mobile #");
            txtField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            txtField.placeholder=NSLocalizedString(@"Should be of 10 digits", @"Should be of 10 digits");
            if (mobileFlag==TRUE) {
                txtField.text = newMobile;
            }
            else
            {
                txtField.text = userInfoObj.value;
            }
            txtField.tag = k_mobile;
            
            [cell addSubview:label];
            [cell addSubview:txtField];
            ReleaseAndNilify(label);
            ReleaseAndNilify(txtField);
        }
            break;
            
        case 7:{//gender
            if(userInfoObj.requiredField){
                label.text = NSLocalizedString(@"Gender*", @"Gender");
                label.attributedText = [self getTagColor:label.text];
            }
            else
                label.text = NSLocalizedString(@"Gender", @"Gender");
            
            maleButton = [UIButton buttonWithType:UIButtonTypeCustom];
            maleButton.frame = CGRectMake(x_origin*2+label.frame.size.width, button_yorigin, X_OFFSET, X_OFFSET);
            [maleButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            maleButton.tag=0;
            maleButton.backgroundColor =[UIColor clearColor];
            maleButton.adjustsImageWhenHighlighted = NO;
            [maleButton addTarget:self action:@selector(selectGender:) forControlEvents:UIControlEventTouchDown];
            
            UILabel *maleLabel = [[UILabel alloc]initWithFrame:CGRectMake(maleButton.frame.origin.x+maleButton.frame.size.width+x_origin,y_origin,50, LABEL_HEIGHT)];
            maleLabel.text = NSLocalizedString(@"Male", @"Male");
            maleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
            maleLabel.backgroundColor = [UIColor clearColor];
            
            femaleButton = [UIButton buttonWithType:UIButtonTypeCustom];
            femaleButton.frame = CGRectMake(maleLabel.frame.origin.x+maleLabel.frame.size.width, button_yorigin, X_OFFSET, X_OFFSET);
            [femaleButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            femaleButton.backgroundColor =[UIColor clearColor];
            femaleButton.tag=1;
            femaleButton.adjustsImageWhenHighlighted = NO;
            [femaleButton addTarget:self action:@selector(selectGender:) forControlEvents:UIControlEventTouchDown];
            
            UILabel *femaleLabel = [[UILabel alloc]initWithFrame:CGRectMake(femaleButton.frame.origin.x+femaleButton.frame.size.width+x_origin, y_origin, 70, LABEL_HEIGHT)];
            femaleLabel.text = NSLocalizedString(@"Female", @"Female");
            femaleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
            femaleLabel.backgroundColor = [UIColor clearColor];
            
            if([[defaults valueForKey:@"Gender"] intValue] == 1)
            {
                [maleButton setImage:[UIImage imageNamed:@"RBE.png"] forState:UIControlStateNormal];
                [femaleButton setImage:[UIImage imageNamed:@"RBD.png"] forState:UIControlStateNormal];
            }
            else
            {
                [maleButton setImage:[UIImage imageNamed:@"RBD.png"] forState:UIControlStateNormal];
                [femaleButton setImage:[UIImage imageNamed:@"RBE.png"] forState:UIControlStateNormal];
            }
            
            [cell addSubview:maleButton];
            [cell addSubview:maleLabel];
            [cell addSubview:femaleLabel];
            [cell addSubview:femaleButton];
            [cell addSubview:label];
            
            ReleaseAndNilify(maleLabel);
            ReleaseAndNilify(femaleLabel);
            ReleaseAndNilify(label);
            ReleaseAndNilify(txtField);
            //return cell;
        }
            break;
            
        case 8://education
        {
            if(userInfoObj.requiredField){
                label.text = NSLocalizedString(@"Education*", @"Education");
                label.attributedText = [self getTagColor:label.text];
            }
            else
                label.text = NSLocalizedString(@"Education", @"Education");
            UIButton *dropDownBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            dropDownBtn1.frame = txtField.frame;
            dropDownBtn1.tag = 1;
            if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
            {
                [dropDownBtn1 setImage:[UIImage imageNamed:@"DropDownIconBig.png"] forState:UIControlStateNormal];
            }
            else
            {
                [dropDownBtn1 setImage:[UIImage imageNamed:@"DropDownIconBigIpad.png"] forState:UIControlStateNormal];
            }
            [dropDownBtn1 addTarget:self action:@selector(showPicker:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell addSubview:label];
            [cell addSubview:dropDownBtn1];
            [cell addSubview:eduLevelLabel];
            ReleaseAndNilify(label);
        }
            break;
            
        case 9://marital status
        {
            if(userInfoObj.requiredField){
                label.text = NSLocalizedString(@"Marital Status*", @"Marital Status");
                label.attributedText = [self getTagColor:label.text];
            }
            else
                label.text = NSLocalizedString(@"Marital Status", @"Marital Status");
            
            UIButton *dropDownBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
            dropDownBtn2.frame = txtField.frame;
            dropDownBtn2.tag = 0;
            if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
            {
                [dropDownBtn2 setImage:[UIImage imageNamed:@"DropDownIconBig.png"] forState:UIControlStateNormal];
            }
            else
            {
                [dropDownBtn2 setImage:[UIImage imageNamed:@"DropDownIconBigIpad.png"] forState:UIControlStateNormal];
            }
            [dropDownBtn2 addTarget:self action:@selector(showPicker:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell addSubview:label];
            [cell addSubview:dropDownBtn2];
            [cell addSubview:marStatusLabel];
            
            ReleaseAndNilify(label);
        }
            break;
            
        case 10:{//image
            SdImageView *image;
            if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
            {
                image = [[SdImageView alloc]initWithFrame:CGRectMake(10, 0, 300, 116)];
            }
            else
            {
                image = [[SdImageView alloc]initWithFrame:CGRectMake(40, 0, 680, 150)];
            }
            image.backgroundColor = [UIColor clearColor];
            [image loadImage:[userInfoObj.value stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
            [cell addSubview:image];
            // [image release];
            ReleaseAndNilify(label);
            //return cell;
        }
            break;
            
        case 11://income range
        {
            if(userInfoObj.requiredField){
                label.text = NSLocalizedString(@"Income Range*", @"Income Range");
                label.attributedText = [self getTagColor:label.text];
            }
            else
                label.text = NSLocalizedString(@"Income Range", @"Income Range");
            UIButton *dropDownBtn3 = [UIButton buttonWithType:UIButtonTypeCustom];
            dropDownBtn3.frame = txtField.frame;
            dropDownBtn3.tag = 2;
            if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
            {
                [dropDownBtn3 setImage:[UIImage imageNamed:@"DropDownIconBig.png"] forState:UIControlStateNormal];
            }
            else
            {
                [dropDownBtn3 setImage:[UIImage imageNamed:@"DropDownIconBigIpad.png"] forState:UIControlStateNormal];
            }
            [dropDownBtn3 addTarget:self action:@selector(showPicker:) forControlEvents:UIControlEventTouchUpInside];
            
            //cell.tag = indexPath.row;
            [cell addSubview:label];
            [cell addSubview:dropDownBtn3];
            [cell addSubview:incRangeLabel];
            ReleaseAndNilify(label);
        }
            break;
            
        case 12://imagepath
            
            break;
            
        default:
            break;
    }
    
    return cell;
}

#pragma mark Date picker methods

//Added by Keshava - This method Parses the date formate sent by server to ios compatible
-(NSString *) parseDate:dateText
{
    NSArray *chunks = [dateText componentsSeparatedByString:@"-"];
    
    NSString *parsedString = [[NSString alloc]initWithString:[chunks objectAtIndex:0]];
    parsedString = [parsedString stringByAppendingFormat:@" %@",[chunks objectAtIndex:1]];
    parsedString = [parsedString stringByAppendingFormat:@",%@",[chunks objectAtIndex:2]];
    
    return parsedString;
}

//Added by Keshava - This mehod creates date from string and set it to GMT Timezone
-(NSDate *)setDateFormatter:(NSString *)dateText{
    NSString *date;
    
    date = [self parseDate:dateText];
    
    NSDateFormatter *inFormat = [[NSDateFormatter alloc] init];
    [inFormat setDateFormat:@"MMM dd,yyyy"];
    NSDate *showDate = [inFormat dateFromString:date];
    NSCalendar *cal =[NSCalendar currentCalendar];
    unsigned int intFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents *components = [cal components:intFlags fromDate:showDate];
    [components setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *newShowDate = [cal dateFromComponents:components];
    
    return newShowDate;
}

// Added by Keshava - Selected Date is added to the Textfield in Table view
-(void)dateSelected
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeStyle:NSDateFormatterNoStyle];
    [df setDateStyle:NSDateFormatterMediumStyle];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [df setLocale:usLocale];
    
    
    selectedDOB = [[NSString alloc]initWithFormat:@"%@",
                   [df stringFromDate:dobPicker.date]];
    
    dobHolder.hidden=YES;
    dobPicker.hidden=YES;
    dobToolBar.hidden=YES;
    [table_UserInfo reloadData];
    
    
    
}
//Added by Keshava - This mehod displays the date picker with the date sent by the
//server highlighted
-(void)showDatePicker{
    if ([[UIResponder currentFirstResponder] isKindOfClass:[UITextField class]]){
        
        [[UIResponder currentFirstResponder] resignFirstResponder];
    }
    
    
    int x_origin = 0;
    int y_origin ;
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        y_origin=((self.view.frame.size.height/2)-50);
    }
    else
    {
        y_origin=(3*(self.view.frame.size.height/4)-50);
    }
    
    NSDate *displayDate=[[NSDate alloc]init];
    
    
    
    if ([selectedDOB length] && replyFromServer==TRUE) {
        replyFromServer=FALSE;
        displayDate = [self setDateFormatter:selectedDOB];
        //NSlog(@"The display date value is %@",displayDate);
    }
    
    
    UIBarButtonItem *done = [[UIBarButtonItem alloc]init];
    
    UIButton *nextBtn = [UtilityManager customizeBarButtonItem:@"Done"];
    [nextBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [nextBtn addTarget:self action:@selector(dateSelected) forControlEvents:UIControlEventTouchUpInside];
    [done setCustomView:nextBtn];
    //[nextBtn ;
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:done, nil];
    
    dobToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(x_origin, y_origin, [[UIScreen mainScreen] bounds].size.width, 50)];
    
    [dobToolBar setBackgroundColor:[UIColor lightGrayColor]];
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {dobHolder = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/2,[[UIScreen mainScreen] bounds].size.width, self.view.frame.size.height/2)];
    }
    else
    {
        dobHolder = [[UIView alloc]initWithFrame:CGRectMake(0, 3*(self.view.frame.size.height/4),[[UIScreen mainScreen] bounds].size.width, 3*(self.view.frame.size.height/4))];
    }
    dobHolder.backgroundColor = [UIColor lightGrayColor];
    
    dobPicker = [[UIDatePicker alloc]init];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {dobPicker.bounds=dobHolder.bounds;
    }
    else
    {
        dobPicker.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, VARIABLE_HEIGHT(100));
    }
    
    dobPicker.datePickerMode= UIDatePickerModeDate;
    //    dobPicker.maximumDate = [NSDate date];
    if (displayDate!=nil) {
        [dobPicker setDate:displayDate];
    }
    dobShowFlag=TRUE;
    [dobHolder addSubview:dobPicker];
    [self.view addSubview:dobToolBar];
    [self.view addSubview:dobHolder];
    [dobToolBar setItems:toolbarItems];
    
    //    [dobToolBar release];
    //    [dobPicker release];
    //    [dobHolder release];
    
}


#pragma mark picker methods
-(void)showPicker:(id)sender{ //tag = 0 for marital status, 1 for education level and 2 for income range which is set for button in cellForRowAtIndexPath
    
    if ([[UIResponder currentFirstResponder] isKindOfClass:[UITextField class]]){
        
        [[UIResponder currentFirstResponder] resignFirstResponder];
    }
    
    UIButton *btn = (UIButton*)sender;
    view_Picker.hidden = NO;
    picker.tag = btn.tag;
    
    switch (btn.tag) {
        case 0:
            if ([marStatusLabel.text isEqualToString:@"Select"]){
                msPickerIndex = 0;
                marStatusLabel.text = [marStatusNameArray objectAtIndex:0];
            }
            else
                marStatusLabel.text = [marStatusNameArray objectAtIndex:msPickerIndex];
            
            [picker reloadAllComponents];
            [picker selectRow:msPickerIndex inComponent:0 animated:NO];
            break;
            
        case 1:
            if ([eduLevelLabel.text isEqualToString:@"Select"]){
                elPickerIndex = 0;
                eduLevelLabel.text = [eduLevelNameArray objectAtIndex:0];
            }
            else
                eduLevelLabel.text = [eduLevelNameArray objectAtIndex:elPickerIndex];
            
            [picker reloadAllComponents];
            [picker selectRow:elPickerIndex inComponent:0 animated:NO];
            break;
            
        case 2:
            if ([incRangeLabel.text isEqualToString:@"Select"]){
                irPickerIndex = 0;
                incRangeLabel.text = [incRangeNameArray objectAtIndex:0];
            }
            else
                incRangeLabel.text = [incRangeNameArray objectAtIndex:irPickerIndex];
            
            [picker reloadAllComponents];
            [picker selectRow:irPickerIndex inComponent:0 animated:NO];
            break;
            
        default:
            break;
    }
    
    
    
    table_UserInfo.scrollEnabled = NO;
    
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (pickerView.tag) {
        case 0://marital status
            return [marStatusIdArray count]? [marStatusIdArray count]:1;
            break;
            
        case 1://education level
            return [eduLevelIdArray count]? [eduLevelIdArray count]:1;
            break;
            
        case 2://income range
            return [incRangeIdArray count]? [incRangeIdArray count]:1;
            break;
            
        default:
            break;
    }
    
    return 0;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel *label = [[UILabel alloc] init];
    
    if (IOS7){
        
        
        label.frame = CGRectMake(20, 0, 220, 40);
        //pickerView.backgroundColor = [UIColor colorWithRGBInt:0xEEEEEE];
        pickerView.layer.borderWidth = 5.0;
        pickerView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    }
    
    else
        label.frame = CGRectMake(0, 0, 200, 40);
    //label.frame = CGRectMake(0, 3*(self.view.frame.size.height/4),[[UIScreen mainScreen] bounds].size.width, 3*(self.view.frame.size.height/4));
    
    [label setBackgroundColor:[UIColor clearColor]];
    [label setNumberOfLines:2];
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    [label setTextAlignment:NSTextAlignmentLeft];
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    if(IPAD){
        doneSpaceBar.width = 20;
    }
    switch (pickerView.tag) {
        case 0://marital status
            
            if ([marStatusNameArray count]>row)
                label.text = [marStatusNameArray objectAtIndex:row];
            else
                label.text = @"Not Available";
            break;
            
        case 1://education level
            
            if ([eduLevelNameArray count]>row)
                label.text = [eduLevelNameArray objectAtIndex:row];
            else
                label.text = @"Not Available";
            break;
            
        case 2://income range
            
            if ([incRangeNameArray count]>row)
                label.text = [incRangeNameArray objectAtIndex:row];
            else
                label.text = @"Not Available";
            break;
            
        default:
            break;
    }
    
    return label ;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    switch (pickerView.tag) {
        case 0://marital status
            
            marStatusLabel.text = [marStatusNameArray objectAtIndex:row];
            selMarStatusId = [[marStatusIdArray objectAtIndex:row]intValue];
            msPickerIndex = (int)row;
            
            break;
            
        case 1://education level
            
            eduLevelLabel.text = [eduLevelNameArray objectAtIndex:row];
            selEduLevelId = [[eduLevelIdArray objectAtIndex:row]intValue];
            elPickerIndex = (int)row;
            
            break;
            
        case 2://income range
            
            incRangeLabel.text = [incRangeNameArray objectAtIndex:row];
            selIncRangeId = [[incRangeIdArray objectAtIndex:row]intValue];
            irPickerIndex =(int)row;
            break;
            
        default:
            break;
    }
}


- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {return 50.0;
    }
    else
    {
        return 75.0;
    }
}

-(IBAction)pickerDoneTapped:(id)sender{
    
    if (customBarItem.enabled == FALSE) {
        customBarItem.enabled = TRUE;
    }
    view_Picker.hidden = YES;
    table_UserInfo.scrollEnabled = YES;
    
}
/*
 *
 unhide the change passowrd view on click of "Change password"
 *
 */
-(IBAction)changePassword:(id)sender{
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        cpasswordView.hidden = NO;
    }
    else
    {
        
        cpasswordView.frame = CGRectMake((SCREEN_WIDTH-300)/2, (SCREEN_HEIGHT-400)/2, 300, 200);
        cpasswordView.hidden = NO;
        updateButton.frame=CGRectMake((cpasswordView.frame.size.width/2)-62,(cpasswordView.frame.size.height)-50,124,30);
    }
    
    txtField_ChangePwd.text = @"";
    txtField_ConfirmPwd.text = @"";
    customBarItem.enabled ? (i = 1):(i = 0);
    customBarItem.enabled = FALSE;
    errorText.text = @"";
    errorText.numberOfLines = 2;
    
    table_UserInfo.alpha = 0.5;
    table_UserInfo.userInteractionEnabled = NO;
}


/*
 *
 Handle the Gender selection logic here
 *
 */
-(IBAction)selectGender:(id)sender{
    
    if (customBarItem.enabled == FALSE) {
        customBarItem.enabled = TRUE;
    }
    
    if (cpasswordView) {
        
        cpasswordView.hidden = YES;
    }
    switch ([sender tag]) {
        case 0:
            [maleButton setImage:[UIImage imageNamed:@"RBE.png"] forState:UIControlStateNormal];
            [femaleButton setImage:[UIImage imageNamed:@"RBD.png"] forState:UIControlStateNormal];
            [defaults  setObject:@"1" forKey:@"Gender"];
            break;
            
        case 1:
            [femaleButton setImage:[UIImage imageNamed:@"RBE.png"] forState:UIControlStateNormal];
            [maleButton setImage:[UIImage imageNamed:@"RBD.png"] forState:UIControlStateNormal];
            [defaults  setObject:@"0" forKey:@"Gender"];
            break;
    }
}


/*
 *
 close the change password view
 *
 */
-(IBAction)closeView:(id)sender{
    
    if (txtField_ChangePwd)
        [txtField_ChangePwd resignFirstResponder];
    
    if(txtField_ConfirmPwd)
        [txtField_ConfirmPwd resignFirstResponder];
    
    i = 1? (customBarItem.enabled = TRUE) : (customBarItem.enabled = FALSE);
    cpasswordView.hidden = YES;
    errorText.text = @"";
    
    table_UserInfo.alpha = 1.0;
    table_UserInfo.userInteractionEnabled = YES;
}


/*
 *
 Update button clicked in ChangePassword view
 Validate the fields for different scenarios before save.
 *
 */

-(IBAction)updateClicked:(id)sender
{
    if([Network currentReachabilityStatus]==0){
        
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    else {
        txtField_ChangePwd.text = [txtField_ChangePwd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        txtField_ConfirmPwd.text = [txtField_ConfirmPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if (![txtField_ChangePwd.text length]) {
            errorText.text = NSLocalizedString(@"Please enter the password",@"Please enter the password");
            updateButton.enabled = NO;
        }
        else if ([txtField_ChangePwd.text length] < 6){
            
            errorText.text = NSLocalizedString(@"Password should contain minimum of 6 characters",@"Password should contain minimum of 6 characters");
            updateButton.enabled = NO;
        }
        else if(![txtField_ConfirmPwd.text length]){
            errorText.text = NSLocalizedString(@"Please confirm your password",@"Please confirm your password");
            updateButton.enabled = NO;
        }
        else if(![txtField_ChangePwd.text isEqualToString:txtField_ConfirmPwd.text]){
            
            errorText.text = NSLocalizedString(@"Password doesnt match",@"Password doesnt match");
            updateButton.enabled = NO;
            
        }
        else {
            iWebRequestState = CHANGE_PASSWORD;
            [self requestToChangePassword];
            //            [txtField_ChangePwd resignFirstResponder];
            //            [txtField_ConfirmPwd resignFirstResponder];
            //            cpasswordView.hidden = YES;
            //			errorText.text = @"";
        }
    }
}

#pragma mark bottom toolbar items action
/*
 *
 Navigate to Privacy policy screen.
 *
 */
-(void)privacyPolicyClicked:(id)sender{
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    else {
        //navigate to privacy screen based on servser response
        AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
       privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
        
        [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
        privacy.comingFromPrivacyScreen = YES;
        [self.navigationController pushViewController:privacy animated:NO];
        //[privacy release];
        
    }
}
- (BOOL)myMobileNumberValidate:(NSString*)number
{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
        return TRUE;
    else
        return FALSE;
}
- (BOOL)myZipcodeValidate:(NSString*)number
{
    NSString *numberRegEx = @"[0-9]{5}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
        return TRUE;
    else
        return FALSE;
}
/*
 *
 save/update the user details
 Method called when navigated from Settings screen to view user account details
 *
 */
-(void) showAlertPopup:(NSString*) message{
    UIAlertController * alert;
            alert=[UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleCancel
                         handler:nil];
    [alert addAction:ok];
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
    [UtilityManager performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:1];
}

-(IBAction)saveButtonClicked:(id)sender{
    BOOL requiredFilled;
    requiredFilled = FALSE;
    BOOL validEmail = NO;
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    else {
        for(int i = 0; i < [userInfoObjArray count]; i++)
        {
            UserInfoFields *checkingInfo = [userInfoObjArray objectAtIndex:i];
            if(checkingInfo.requiredField){
                if([checkingInfo.fieldName isEqualToString:@"First Name"]&& newFirstName.length == 0){
                    requiredFilled = TRUE;
                    break;
                }
                if([checkingInfo.fieldName isEqualToString:@"Last Name"]&& newLastName.length == 0){
                    requiredFilled = TRUE;
                    break;
                }
                
                if([checkingInfo.fieldName isEqualToString:@"Mobile"]&& newMobile.length == 0){
                    requiredFilled = TRUE;
                    break;
                }
                if([checkingInfo.fieldName isEqualToString:@"Email"]&& newEmail.length == 0){
                    requiredFilled = TRUE;
                    break;
                }
                if ([checkingInfo.fieldName isEqualToString:@"Date Of Birth"]&& (selectedDOB == 0)&&(serverDOB.length == 0)){
                    requiredFilled = TRUE;
                    break;
                }
                
                if ([checkingInfo.fieldName isEqualToString:@"Zip Code"]&& newZipCode.length == 0)
                {
                    requiredFilled = TRUE;
                    break;
                }
                if ([checkingInfo.fieldName isEqualToString:@"Marital Status"] && ([marStatusLabel.text  isEqualToString:@"Select"])){
                    requiredFilled = TRUE;
                    break;
                }
                if ([checkingInfo.fieldName isEqualToString:@"Education"] && ([eduLevelLabel.text isEqualToString:@"Select"])){
                    requiredFilled = TRUE;
                    break;
                }
                if ([checkingInfo.fieldName isEqualToString:@"Income Range"] && ([incRangeLabel.text  isEqualToString:@"Select"])){
                    requiredFilled = TRUE;
                    break;
                }
                
                
            }
            if ([checkingInfo.fieldName isEqualToString:@"Email"]&&[checkingInfo.value length]) {
                
                NSString *emailRegex = @"[a-zA-Z0-9.\\-_]{1,32}@[a-zA-Z0-9.\\-_]{2,32}\\.[A-Za-z]{2,4}";
                NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                validEmail = [regExPredicate evaluateWithObject:checkingInfo.value];
                
                if(validEmail == 0){
                    [self showAlertPopup:NSLocalizedString(@"Please enter valid email id", @"Please enter valid email id")];
                    return;
                }
            }
            if(![self myMobileNumberValidate:newMobile]&& newMobile.length!=0){
                [self showAlertPopup:NSLocalizedString(@"Please enter valid mobile number", @"Please enter valid mobile number")];
                return;
                
            }
            if(![self myZipcodeValidate:newZipCode] && newZipCode.length!=0){
                [self showAlertPopup:NSLocalizedString(@"Please enter valid Zipcode", @"Please enter valid Zipcode")];
                return;
                
            }
            
        }
        if(requiredFilled == TRUE)
        {
           // requiredFilled = FALSE;
            [UtilityManager showAlert:nil msg:NSLocalizedString(@"Mandatory fields cannot be empty", @"Mandatory fields cannot be empty")];
            return;
        }
        
        //email is mandatory and always be at the index 2
        
        //UserInfoDO *userInfoObj = [userInfoObjArray objectAtIndex:3];
        
        
        if ([newEmail length] && emailFlag==TRUE) {
            NSString *emailRegex = @"[a-zA-Z0-9.\\-_]{1,32}@[a-zA-Z0-9.\\-_]{2,32}\\.[A-Za-z]{2,4}";
            NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
            validEmail = [regExPredicate evaluateWithObject:newEmail];
            
            if(validEmail == 0){
                
                
                [self showAlertPopup:NSLocalizedString(@"Please enter valid email id", @"Please enter valid email id")];
                
                return;
            }
        }
        
        
        iWebRequestState = SIGNUP_UPDATEUSER;
        [self request_UpdateUserInfo];
    }
}
/*
 *
 save user details and navigate to preferred categories screen on success
 Method called when navigated from user signup screen while registration.
 *
 */
-(void)navigateToNextPage:(id)sender{
    
    BOOL validEmail, requiredFilled;
    requiredFilled = FALSE;
    if([Network currentReachabilityStatus]==0){
        
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    else {
        
        for(int i = 0; i < [userInfoObjArray count]; i++)
        {
            if(![self myMobileNumberValidate:newMobile] && newMobile.length!=0){
                [self showAlertPopup:NSLocalizedString(@"Please enter valid mobile number", @"Please enter valid mobile number")];
               
                return;
                
            }
            if(![self myZipcodeValidate:newZipCode] && newZipCode.length!=0){
                [self showAlertPopup:NSLocalizedString(@"Please enter valid Zipcode", @"Please enter valid Zipcode")];
               
                return;
                
            }
            
            
            UserInfoFields *checkingInfo = [userInfoObjArray objectAtIndex:i];
            if(checkingInfo.requiredField){
                if([checkingInfo.fieldName isEqualToString:@"First Name"]&& newFirstName.length == 0){
                    requiredFilled = TRUE;
                    break;
                }
                if([checkingInfo.fieldName isEqualToString:@"Last Name"]&& newLastName.length == 0){
                    requiredFilled = TRUE;
                    break;
                }
                
                if([checkingInfo.fieldName isEqualToString:@"Mobile"]&& newMobile.length == 0){
                    requiredFilled = TRUE;
                    break;
                }
                if([checkingInfo.fieldName isEqualToString:@"Email"]&& newEmail.length == 0){
                    requiredFilled = TRUE;
                    break;
                }
                if ([checkingInfo.fieldName isEqualToString:@"Date Of Birth"]&& ((selectedDOB.length == 0)&&(serverDOB.length == 0))){
                    requiredFilled = TRUE;
                    break;
                }
                
                if ([checkingInfo.fieldName isEqualToString:@"Zip Code"]&& newZipCode.length == 0)
                {
                    requiredFilled = TRUE;
                    break;
                }
                if ([checkingInfo.fieldName isEqualToString:@"Marital Status"] && ([marStatusLabel.text  isEqualToString:@"Select"])){
                    requiredFilled = TRUE;
                    break;
                }
                if ([checkingInfo.fieldName isEqualToString:@"Education"] && ([eduLevelLabel.text isEqualToString:@"Select"])){
                    requiredFilled = TRUE;
                    break;
                }
                if ([checkingInfo.fieldName isEqualToString:@"Income Range"] && ([incRangeLabel.text  isEqualToString:@"Select"])){
                    requiredFilled = TRUE;
                    break;
                }
            }
            if ([checkingInfo.fieldName isEqualToString:@"Email"]&&[checkingInfo.value length]) {
                
                NSString *emailRegex = @"[a-zA-Z0-9.\\-_]{1,32}@[a-zA-Z0-9.\\-_]{2,32}\\.[A-Za-z]{2,4}";
                NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                validEmail = [regExPredicate evaluateWithObject:checkingInfo.value];
                
                if(validEmail == 0){
                    [self showAlertPopup:NSLocalizedString(@"Please enter valid email id", @"Please enter valid email id")];
                   
                    return;
                }
                
                
            }
            
            
            //email is mandatory and always be at the index 2
            // UserInfoDO *userInfoObj = [userInfoObjArray objectAtIndex:3];
            
            
            
            if ([newEmail length] && emailFlag==TRUE) {
                NSString *emailRegex = @"[a-zA-Z0-9.\\-_]{1,32}@[a-zA-Z0-9.\\-_]{2,32}\\.[A-Za-z]{2,4}";
                NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                validEmail = [regExPredicate evaluateWithObject:newEmail];
                
                if(validEmail == 0){
                    
                    [self showAlertPopup:NSLocalizedString(@"Please enter valid email id", @"Please enter valid email id")];
                    return;
                }
            }
            
        }
        if(requiredFilled == TRUE)
        {
           // requiredFilled = FALSE;
            [UtilityManager showAlert:nil msg:NSLocalizedString(@"Mandatory fields cannot be empty", @"Mandatory fields cannot be empty")];
           
            return;
        }
        
        iWebRequestState = SIGNUP_UPDATEUSER;
        [self request_UpdateUserInfo];
    }
}




#pragma mark request methods
/*
 *
 send request with data to get user details.
 request sent when navigated from settings screen.
 *
 */
//-(void)request_GetUserInfo
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//  //[xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
////    [xmlStr appendFormat:@"<UserDetails><userId>%d</userId><hubCitiId>%@</hubCitiId></UserDetails>", 3,[defaults valueForKey:KEY_HUBCITIID]];
//     [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><deviceId>%@</deviceId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_HUBCITIID]];
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
//    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//    ReleaseAndNilify(xmlStr);
//}

/*
 *
 send request with data to save/update user details.
 Method called when request sent updating user details while registration.
 *
 */
-(void)request_GetUserInfo
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    
    //NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //  //[xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    ////    [xmlStr appendFormat:@"<UserDetails><userId>%d</userId><hubCitiId>%@</hubCitiId></UserDetails>", 3,[defaults valueForKey:KEY_HUBCITIID]];
    //     [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><deviceId>%@</deviceId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_HUBCITIID]];
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //ReleaseAndNilify(xmlStr);
}

/*
 *
 send request with data to save/update user details.
 Method called when request sent updating user details while registration.
 *
 */
-(void)request_UpdateUserInfo
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_DEVICEID]) {
        [parameters setValue:[defaults  valueForKey:KEY_DEVICEID] forKey:@"deviceId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults  valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    for (int i = 0; i < [userInfoObjArray count]; i++){
        UserInfoFields *userInfoObj = [userInfoObjArray objectAtIndex:i];
        switch ([[dict_fieldName objectForKey:userInfoObj.fieldName]intValue]) {
                DLog(@"%@",userInfoObj.value);
            case 1://firstname
                if (firstNameFlag==TRUE) {
                    if([newFirstName length]){
                        [parameters setValue:newFirstName forKey:@"firstName"];
                    }
                    else
                    {
                        [parameters setValue:newFirstName forKey:@"firstName"];
                    }
                    // firstNameFlag=FALSE;
                }
                else
                {
                    if ([userInfoObj.value length]) {
                        [parameters setValue:userInfoObj.value forKey:@"firstName"];
                    }
                    
                }
                break;
                
            case 2://lastname
                if (lastNameFlag==TRUE) {
                    
                    if([newLastName length]){
                        [parameters setValue:newLastName forKey:@"lastName"];
                    }
                    else if(newLastName)
                    {
                        [parameters setValue:newLastName forKey:@"lastName"];
                    }
                    //lastNameFlag=FALSE;
                }
                else
                {
                    if ([userInfoObj.value length]) {
                        [parameters setValue:userInfoObj.value forKey:@"lastName"];
                    }
                }
                break;
                
            case 3://email
                if (emailFlag==TRUE) {
                    if([newEmail length]){
                        [parameters setValue:newEmail forKey:@"email"];
                    }
                    else if(newEmail)
                    {
                        [parameters setValue:newEmail forKey:@"email"];
                    }
                    //emailFlag=FALSE;
                }
                
                else
                {
                    if ([userInfoObj.value length]) {
                        
                        [parameters setValue:userInfoObj.value forKey:@"email"];
                        
                    }
                }
                break;
            case 4 ://DOB
                //NSlog(@"The selected date is %@",serverDOB);
                if (selectedDOB && dateFromServerFlag==FALSE) {
                    
                    [parameters setValue:selectedDOB forKey:@"dob"];
                }
                else if(serverDOB)
                {
                    
                    [parameters setValue:serverDOB forKey:@"dob"];
                }
                break;
            case 5://zipcode
                if (zipFlag==TRUE) {
                    if([newZipCode length]){
                        
                        [parameters setValue:newZipCode forKey:@"postalCode"];
                        [defaults setValue:newZipCode forKey:KEYZIPCODE];
                    }
                    else if(newZipCode)
                    {
                        [parameters setValue:newZipCode forKey:@"postalCode"];
                        [defaults setValue:nil forKey:KEYZIPCODE];
                    }
                    //zipFlag=FALSE;
                }
                
                else if([userInfoObj.value length])
                {
                    [parameters setValue:userInfoObj.value forKey:@"postalCode"];
                    [defaults setValue:userInfoObj.value forKey:KEYZIPCODE];
                }
                else{
                    [defaults setValue:nil forKey:KEYZIPCODE];
                }
                break;
                
            case 6://mobile
                if (mobileFlag==TRUE) {
                    if([newMobile  length]){
                        
                        [parameters setValue:newMobile forKey:@"phoneNum"];
                    }
                    else if(newMobile)
                    {
                        [parameters setValue:newMobile forKey:@"phoneNum"];
                    }
                    //mobileFlag=FALSE;
                }
                
                
                else
                {
                    if (([userInfoObj.value length])) {
                        
                        [parameters setValue:userInfoObj.value forKey:@"phoneNum"];
                    }
                }
                
                break;
                
            case 7://gender
                
                
                [parameters setValue:[defaults valueForKey:@"Gender"] forKey:@"gender"];
                
                break;
                
            case 8://education
                
                if ([eduLevelIdArray count] && ![eduLevelLabel.text isEqualToString:@"Select"]){
                    if([eduLevelIdArray objectAtIndex:elPickerIndex])
                        
                        if (eduLevelIdArray.count > elPickerIndex) {
                            
                            [parameters setValue:[eduLevelIdArray objectAtIndex:elPickerIndex] forKey:@"educatonLevelId"];
                        }
                    
                }
                
                
                
                break;
                
            case 9://marital status
                
                if ([marStatusIdArray count] && ![marStatusLabel.text isEqualToString:@"Select"]){
                    if([marStatusIdArray objectAtIndex:msPickerIndex])
                        
                        if (marStatusIdArray.count > msPickerIndex)
                        {
                            [parameters setValue:[marStatusIdArray objectAtIndex:msPickerIndex]forKey:@"martialStatusId"];
                        }
                }
                
                break;
                
            case 10://image
                break;
                
            case 11://income range
                if ([incRangeIdArray count] && ![incRangeLabel.text isEqualToString:@"Select"]){
                    if([incRangeIdArray objectAtIndex:irPickerIndex])
                        
                        if (incRangeIdArray.count > irPickerIndex)
                        {
                            [parameters setValue:[incRangeIdArray objectAtIndex:irPickerIndex]forKey:@"incomeRangeId"];
                        }
                }
                
                break;
                
            case 12://imagepath
                break;
        }
        
        
    }
    
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/updateuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];

    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><deviceId>%@</deviceId>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_DEVICEID]];
    //
    //    for (int i = 0; i < [userInfoObjArray count]; i++) {
    //
    //        UserInfoDO *userInfoObj = [userInfoObjArray objectAtIndex:i];
    //
    //        switch ([[dict_fieldName objectForKey:userInfoObj.fieldName]intValue]) {
    //
    //                DLog(@"%@",userInfoObj.value);
    //            case 1://firstname
    //                if (firstNameFlag==TRUE) {
    //                    if([newFirstName length]){
    //                    [xmlStr appendFormat:@"<firstName><![CDATA[%@]]></firstName>",newFirstName];
    //                }
    //                    else
    //                    {
    //                       [xmlStr appendFormat:@"<firstName><![CDATA[%@]]></firstName>",newFirstName];
    //                    }
    //                   // firstNameFlag=FALSE;
    //                }
    //                else
    //                {
    //                    if ([userInfoObj.value length]) {
    //                        [xmlStr appendFormat:@"<firstName><![CDATA[%@]]></firstName>",userInfoObj.value];
    //                    }
    //
    //                }
    //                break;
    //
    //            case 2://lastname
    //                if (lastNameFlag==TRUE) {
    //
    //                    if([newLastName length]){
    //                    [xmlStr appendFormat:@"<lastName><![CDATA[%@]]></lastName>",newLastName];
    //                    }
    //                    else if(newLastName)
    //                    {
    //                   [xmlStr appendFormat:@"<lastName><![CDATA[%@]]></lastName>",newLastName];
    //                    }
    //                    //lastNameFlag=FALSE;
    //                }
    //                else
    //                {
    //                    if ([userInfoObj.value length]) {
    //                        [xmlStr appendFormat:@"<lastName><![CDATA[%@]]></lastName>",userInfoObj.value];
    //                    }
    //                }
    //                break;
    //
    //            case 3://email
    //                if (emailFlag==TRUE) {
    //                    if([newEmail length]){
    //                        [xmlStr appendFormat:@"<email><![CDATA[%@]]></email>",newEmail];
    //                    }
    //                    else if(newEmail)
    //                    {
    //                        [xmlStr appendFormat:@"<email><![CDATA[%@]]></email>",newEmail];
    //                    }
    //                    //emailFlag=FALSE;
    //                }
    //
    //                else
    //                {
    //                    if ([userInfoObj.value length]) {
    //                        [xmlStr appendFormat:@"<email><![CDATA[%@]]></email>",userInfoObj.value];
    //                    }
    //                }
    //                break;
    //            case 4 ://DOB
    //                //NSlog(@"The selected date is %@",serverDOB);
    //                if (selectedDOB && dateFromServerFlag==FALSE) {
    //                    [xmlStr appendFormat:@"<dob><![CDATA[%@]]></dob>",selectedDOB];
    //                }
    //                else if(serverDOB)
    //                {
    //                    [xmlStr appendFormat:@"<dob><![CDATA[%@]]></dob>",serverDOB];
    //                }
    //                break;
    //            case 5://zipcode
    //                if (zipFlag==TRUE) {
    //                    if([newZipCode length]){
    //
    //                        [xmlStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>",newZipCode];
    //                        [defaults setValue:newZipCode forKey:KEYZIPCODE];
    //                    }
    //                    else if(newZipCode)
    //                    {
    //                        [xmlStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>",newZipCode];
    //                        [defaults setValue:nil forKey:KEYZIPCODE];
    //                    }
    //                    //zipFlag=FALSE;
    //                }
    //
    //                else if([userInfoObj.value length])
    //                {
    //                    [xmlStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>",userInfoObj.value];
    //                    [defaults setValue:userInfoObj.value forKey:KEYZIPCODE];
    //                }
    //                else{
    //                    [defaults setValue:nil forKey:KEYZIPCODE];
    //                }
    //                break;
    //
    //            case 6://mobile
    //                if (mobileFlag==TRUE) {
    //                    if([newMobile  length]){
    //                        [xmlStr appendFormat:@"<phoneNum><![CDATA[%@]]></phoneNum>",newMobile];
    //                    }
    //                    else if(newMobile)
    //                    {
    //                       [xmlStr appendFormat:@"<phoneNum><![CDATA[%@]]></phoneNum>",newMobile];
    //                    }
    //                    //mobileFlag=FALSE;
    //                }
    //
    //
    //                else
    //                {
    //                    if (([userInfoObj.value length])) {
    //                        [xmlStr appendFormat:@"<phoneNum><![CDATA[%@]]></phoneNum>",userInfoObj.value];
    //                    }
    //                }
    //
    //                break;
    //
    //            case 7://gender
    //
    //                [xmlStr appendFormat:@"<gender>%@</gender>",[defaults valueForKey:@"Gender"]];
    //
    //                break;
    //
    //            case 8://education
    //
    //                if ([eduLevelIdArray count] && ![eduLevelLabel.text isEqualToString:@"Select"]){
    //                    if([eduLevelIdArray objectAtIndex:elPickerIndex])
    //                        [xmlStr appendFormat:@"<educatonLevelId>%@</educatonLevelId>",[[eduLevelIdArray objectAtIndex:elPickerIndex] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    //                }
    //
    //
    //
    //                break;
    //
    //            case 9://marital status
    //
    //                if ([marStatusIdArray count] && ![marStatusLabel.text isEqualToString:@"Select"]){
    //                    if([marStatusIdArray objectAtIndex:msPickerIndex])
    //                        [xmlStr appendFormat:@"<martialStatusId>%@</martialStatusId>",[[marStatusIdArray objectAtIndex:msPickerIndex] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    //                }
    //
    //                break;
    //
    //            case 10://image
    //                break;
    //
    //            case 11://income range
    //                if ([incRangeIdArray count] && ![incRangeLabel.text isEqualToString:@"Select"]){
    //                    if([incRangeIdArray objectAtIndex:irPickerIndex])
    //                        [xmlStr appendFormat:@"<incomeRangeId>%@</incomeRangeId>",[[incRangeIdArray objectAtIndex:irPickerIndex] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    //                }
    //
    //                break;
    //
    //            case 12://imagepath
    //                break;
    //        }
    
    //    }
    //
    //    [xmlStr appendString:@"</UserDetails>"];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/updateuserinfo",BASE_URL];
    //
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}

/*
 *
 send request with data to get change pwd.
 *
 */
-(void)requestToChangePassword
{
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserRegistrationInfo><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [xmlStr appendFormat:@"<hubCitiKey>%@</hubCitiKey>",HUBCITIKEY];
    NSLog(@"%@",HUBCITIKEY);
    [xmlStr appendFormat:@"<password>%@</password></UserRegistrationInfo>", txtField_ChangePwd.text];
    //  NSString *urlString = [NSString stringWithFormat:@"http://sdw2730:8080/HubCiti2.6/firstuse/v2/resetpassword"];
    // NSString *urlString = [NSString stringWithFormat:@"http://sdw2730:8080/HubCiti2.6/firstuse/v2/resetpassword"];
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/resetpassword",BASE_URL];
    NSLog(@"XMl string %@",xmlStr);
    
    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    ReleaseAndNilify(xmlStr);
}
//-(void)requestToChangePassword
//{
//
//        NSMutableString *xmlStr = [[NSMutableString alloc] init];
//        [xmlStr appendFormat:@"<UserRegistrationInfo><userId>7730</userId>"];
//        // [xmlStr appendFormat:@"<hubCitiId>%@</hubCitiId></UserRegistrationInfo>", [defaults valueForKey:KEY_HUBCITIID]];
//        [xmlStr appendFormat:@"<hubCitiKey>spanqa.hubciti693</hubCitiKey>"];
//        [xmlStr appendFormat:@"<password>%@</password></UserRegistrationInfo>", txtField_ChangePwd.text];
//
//        // NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/getuserlogin",BASE_URL];
//
//        NSString *urlString = [NSString stringWithFormat:@"http://sdw2730:8080/HubCiti2.6/firstuse/v2/resetpassword"];
//        NSLog(@"XMl string %@",xmlStr);
//
//        [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//        ReleaseAndNilify(xmlStr);
//
//    }

#pragma mark parse methods
/*
 *
 asynchronous req/resp delegate method
 *
 */
-(void)responseData:(NSString *)response
{
    switch (iWebRequestState)
    {
        case SIGNUP_UPDATEUSER:
            [self parse_UpdateUserInfo:response];;
            break;
            
        case GETUSERINFO:
            [self parse_GetUserInfo:response];;
            break;
            
        case CHANGE_PASSWORD:
            [self parseChangePasword:response];;
            break;
            
        default:
            break;
    }
}

///*
// *
// Parsing logic when response received for getting user details.
// Method called when navigated from Settings screen.
// *
// */
//
//-(void)responseData:(NSString *)response
//{
//    switch (iWebRequestState)
//    {
//        case SIGNUP_UPDATEUSER:
//            [self parse_UpdateUserInfo:response];
//            break;
//
//        case GETUSERINFO:
//            [self parse_GetUserInfo:response];
//            break;
//
//        case CHANGE_PASSWORD:
//            [self parseChangePasword:response];
//            break;
//
//        default:
//            break;
//    }
//}

/*
 *
 Parsing logic when response received for getting user details.
 Method called when navigated from Settings screen.
 *
 */

-(void)parse_GetUserInfo:(id)response
{
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){//success response
        if(infoResponse.listUserDetails) {
            userInfoObjArray = [[NSMutableArray alloc]init];
            for(int i = 0;i < infoResponse.listUserDetails.count; i++){
                UserInfoFields * userInfoObj = [[UserInfoFields alloc] init];
                
                NSDictionary* dictList = infoResponse.listUserDetails[i];
                
                [userInfoObj setValuesForKeysWithDictionary:dictList];
                
                
                
                // [userInfoObjArray addObject:userInfoObj];
                
                if(userInfoObj.value == NULL){
                    userInfoObj.value = [@"" copy];
                }
                if ([userInfoObj.fieldName isEqualToString:@"Date Of Birth"])
                {
                    if ([userInfoObj.value isEqualToString:@""]) {
                        serverDOB=[[NSString alloc]init];
                    }
                    else
                    {
                        NSDateFormatter *df = [[NSDateFormatter alloc] init];
                        [df setTimeStyle:NSDateFormatterNoStyle];
                        [df setDateStyle:NSDateFormatterMediumStyle];
                        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
                        [df setLocale:usLocale];
                        NSDate *dateFromServer = [self setDateFormatter:userInfoObj.value];
                        NSString *parsedDateFromServer = [[NSString alloc]initWithFormat:@"%@",
                                                          [df stringFromDate:dateFromServer]];
                        
                        serverDOB=[[NSString alloc]initWithString:parsedDateFromServer];
                        dateFromServerFlag=TRUE;
                    }
                }
                
                if ([userInfoObj.fieldName isEqualToString:@"Gender"]){
                    
                    if ([userInfoObj.value intValue] == 1) {
                        [defaults  setObject:@"1" forKey:@"Gender"];
                    }
                    else {
                        [defaults  setObject:@"0" forKey:@"Gender"];
                    }
                }
                
                
                //update Zip Code value in defaults
                if ([userInfoObj.fieldName isEqualToString:@"Zip Code"]){
                    
                    [defaults setValue:userInfoObj.value forKey:KEYZIPCODE];
                }
                
                //populate marital status drop down values
                if ([userInfoObj.fieldName isEqualToString:@"Marital Status"] && ![marStatusIdArray count]){
                    
                    marStatusIdArray = [[NSMutableArray alloc]init];
                    marStatusNameArray = [[NSMutableArray alloc]init];
                    
                    for(int i = 0; i < infoResponse.arMaritalList.count;i++){
                        NSDictionary* maritalDictList = infoResponse.arMaritalList[i];
                        ArMaritalList* carMaritalStatus = [[ArMaritalList alloc]init];
                        
                        [carMaritalStatus setValuesForKeysWithDictionary:maritalDictList];
                        if(carMaritalStatus!=NULL) {
                            [marStatusIdArray addObject:carMaritalStatus.mStatusId];
                            [marStatusNameArray addObject:carMaritalStatus.mStatusName];
                        }
                        
                    }
                    if ([userInfoObj.value isEqualToString:@"Select"]) {
                        
                        msPickerIndex = 0;
                        marStatusLabel.text = @"Select";
                    }
                    else{
                        
                        for (int i = 0; i<[marStatusNameArray count]; i++) {
                            
                            if ([userInfoObj.value isEqualToString:[marStatusNameArray objectAtIndex:i]]) {
                                NSString *str = [marStatusNameArray objectAtIndex:i];
                                str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                marStatusLabel.text = str;
                                msPickerIndex = i;//to highlight the particular index value in picker
                            }
                        }
                    }
                    
                }
                
                //populate education level drop down values
                else if ([userInfoObj.fieldName isEqualToString:@"Education"] && ![eduLevelIdArray count]){
                    
                    eduLevelIdArray = [[NSMutableArray alloc]init];
                    eduLevelNameArray = [[NSMutableArray alloc]init];
                    
                    for(int i = 0; i < infoResponse.arEducationList.count;i++){
                        NSDictionary* eduDictList = infoResponse.arEducationList[i];
                        ArEducationList* carEduStatus = [[ArEducationList alloc]init];
                        
                        [carEduStatus setValuesForKeysWithDictionary:eduDictList];
                        if(carEduStatus!=NULL) {
                            [eduLevelIdArray addObject:carEduStatus.educatLevelId];
                            [eduLevelNameArray addObject:carEduStatus.educatLevelName];
                        }
                    }
                    
                    if ([userInfoObj.value isEqualToString:@"Select"]) {
                        
                        elPickerIndex = 0;
                        eduLevelLabel.text = @"Select";
                    }
                    
                    else{
                        
                        for (int i = 0; i<[eduLevelNameArray count]; i++) {
                            
                            if ([userInfoObj.value isEqualToString:[eduLevelNameArray objectAtIndex:i]]) {
                                NSString *str = [eduLevelNameArray objectAtIndex:i];
                                str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                eduLevelLabel.text = str;
                                elPickerIndex = i;//to highlight the particular index value in picker
                            }
                        }
                    }
                }
                
                
                //populate income range drop down values
                else if ([userInfoObj.fieldName isEqualToString:@"Income Range"] && ![incRangeIdArray count]){
                    
                    incRangeIdArray = [[NSMutableArray alloc]init];
                    incRangeNameArray = [[NSMutableArray alloc]init];
                    
                    
                    for(int i = 0; i < infoResponse.arIncomeRangeList.count;i++){
                        NSDictionary* incomeDictList = infoResponse.arIncomeRangeList[i];
                        ArIncomeRangeList* carIncomeStatus = [[ArIncomeRangeList alloc]init];
                        
                        [carIncomeStatus setValuesForKeysWithDictionary:incomeDictList];
                        if(carIncomeStatus!=NULL) {
                            [incRangeIdArray addObject:carIncomeStatus.icRangeId];
                            [incRangeNameArray addObject:carIncomeStatus.icRangeName];
                        }
                        
                    }
                    if ([userInfoObj.value isEqualToString:@"Select"]) {
                        
                        irPickerIndex = 0;
                        incRangeLabel.text = @"Select";
                    }
                    
                    else{
                        
                        for (int i = 0; i<[incRangeNameArray count]; i++) {
                            
                            if ([userInfoObj.value isEqualToString:[incRangeNameArray objectAtIndex:i]]) {
                                NSString *str = [incRangeNameArray objectAtIndex:i];
                                str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                incRangeLabel.text = str;
                                irPickerIndex = i;//to highlight the particular index value in picker
                                //break;
                            }
                        }
                    }
                }
                
                [userInfoObjArray addObject:userInfoObj];
            }
        }
    }
    if ([infoResponse.responseCode isEqualToString:@"10001"] ){
        NSString *response= infoResponse.responseText;
        [UtilityManager showAlert:nil msg:response];
        return;
        
    }
    if ([infoResponse.responseCode isEqualToString:@"10002"] ){
        NSString *response= infoResponse.responseText;
        [UtilityManager showAlert:nil msg:response];
        return;
        
    }
    if ([infoResponse.responseCode isEqualToString:@"10005"] ){
        NSString *response= infoResponse.responseText;
        [UtilityManager showAlert:nil msg:response];
        return;
        
    }
    
    
    
}


/*
 *
 Parsing logic when response received while updating user details.
 Navigate to preferres categories on success response.
 *
 */
-(void)parse_UpdateUserInfo:(id)response{
    if (response == nil)
        return;
    if (updateResponse == nil) {
        updateResponse = [[UpdateUserInfo alloc] init];
    }
    
    
    [updateResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", updateResponse);
    if ([updateResponse.responseCode isEqualToString:@"10000"] ){
        if ([SharedManager userInfoFromSignup] == YES)
        {
            [SharedManager setUserInfoFromSignup:YES];
            if ([RegionApp isEqualToString:@"1"]) {
                CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                [self.navigationController pushViewController:citPref animated:NO];
                //[citPref release];
            }
            else
            {
                PreferredCategoriesScreen *preferredCategories = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:preferredCategories animated:NO];
                //[preferredCategories release];
            }
        }
        else
        {
            
            UIAlertController * alert;
           
                alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Saved Successfully",@"Saved Successfully") message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     [self.navigationController popViewControllerAnimated:NO];
                                 }];
            [alert addAction:ok];
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];

            
            
        }
    }
    else
    {
        
        [UtilityManager showAlert:nil msg:updateResponse.responseText];
       
        return;
        
    }
    
    
}

/*
 *
 Parsing logic when response received for getting user details.
 Method called when navigated from Settings screen.
 *
 */

-(void)parseChangePasword:(NSString *)response
{
    DLog(@"response %@",response);
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    int responseCode = [[TBXML textForElement:responseCodeElement]intValue];
    if (responseCode == 10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
                [defaults setValue:txtField_ConfirmPwd.text forKey:@"Password"];
        NSLog(@"password after changing new one %@",[defaults valueForKey:@"Password"]);
        ////[alert release];
        if (txtField_ChangePwd)
            [txtField_ChangePwd resignFirstResponder];
        
        if(txtField_ConfirmPwd)
            [txtField_ConfirmPwd resignFirstResponder];
        
        i = 1? (customBarItem.enabled = TRUE) : (customBarItem.enabled = FALSE);
        cpasswordView.hidden = YES;
        table_UserInfo.alpha = 1.0;
        table_UserInfo.userInteractionEnabled = YES;
        errorText.text = @"";
    }
    else
    {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
               ReleaseAndNilify(tbxml);
    }
}

#pragma mark - TextField view delegates and datasource methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:LEGAL];
    NSCharacterSet *numCharSet = [NSCharacterSet characterSetWithCharactersInString:NUMBERS];
    
    switch (textField.tag) {
            
        case 1000:{//fNameField
            
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++) {
                    unichar c = [string characterAtIndex:j];
                    if ([charSet characterIsMember:c]) {
                        NSUInteger textLength = [textField.text length] + [string length] - range.length;
                        return (textLength > 30)? NO : YES;
                    }
                    else {
                        return NO;
                    }
                    
                }
            }
        }
            break;
            
        case 2000:{//lNameField
            
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++) {
                    unichar c = [string characterAtIndex:j];
                    if ([charSet characterIsMember:c]) {
                        NSUInteger textLength = [textField.text length] + [string length] - range.length;
                        return (textLength > 30)? NO : YES;
                    }
                    else {
                        return NO;
                    }
                    
                }
            }
        }
            break;
            
        case 3000:{//emailField
            NSUInteger textLength = [textField.text length] + [string length] - range.length;
            return (textLength > 100)? NO : YES;
        }
            break;
            
        case 4000:{//zipField
            
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++) {
                    unichar c = [string characterAtIndex:j];
                    if ([numCharSet characterIsMember:c]) {
                        NSUInteger textLength = [textField.text length] + [string length] - range.length;
                        return (textLength > 5)? NO : YES;
                    }
                    else {
                        return NO;
                    }
                }
            }
        }
            break;
            
        case 5000:{//mobileField
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++) {
                    unichar c = [string characterAtIndex:j];
                    if ([numCharSet characterIsMember:c]) {
                        NSUInteger textLength = [textField.text length] + [string length] - range.length;
                        return (textLength > 10)? NO : YES;
                    }
                    else {
                        return NO;
                    }
                    
                }
            }
        }
            break;
            
        default:
            return YES;
            break;
    }
    
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    // table_UserInfo.scrollEnabled=NO;
    
    if (dobShowFlag==TRUE) {
        dobShowFlag=FALSE;
        dobHolder.hidden=YES;
        dobPicker.hidden=YES;
        dobToolBar.hidden=YES;
    }
    
    if (view_Picker.hidden == NO)
        view_Picker.hidden = YES;
    
    if (customBarItem.enabled == FALSE) {
        customBarItem.enabled = TRUE;
    }
    
    UITableViewCell* myCell;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        myCell = (UITableViewCell *) textField.superview;
        
    }
    else
    {
        myCell = (UITableViewCell *) textField.superview.superview;
    }
    // UITableViewCell* myCell = (UITableViewCell*)textField.superview.superview ;
    NSIndexPath *indexPath = [table_UserInfo indexPathForCell:myCell];
    index_Path = indexPath;
    
    switch (textField.tag) {
        case 3000:
        case 2000:
        case 4000:
        case 5000:{
            
            CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
            CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
            CGFloat midline = textFieldRect.origin.y + 0.8*textFieldRect.size.height;
            CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION*viewRect.size.height;
            CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
            CGFloat heightFraction = numerator/denominator;
            if(heightFraction < 0.0)
            {
                heightFraction = 0.0;
            }
            else if(heightFraction > 1.0)
            {
                heightFraction = 1.2;
            }
            UIInterfaceOrientation orientation =
            [[UIApplication sharedApplication] statusBarOrientation];
            if(orientation ==UIInterfaceOrientationPortrait||
               orientation == UIInterfaceOrientationPortraitUpsideDown)
            {
                animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT*heightFraction);
            }
            else
            {
                animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT*heightFraction);
            }
            CGRect viewFrame = self.view.frame;
            viewFrame.origin.y -= animatedDistance;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
            [self.view setFrame:viewFrame];
            [UIView commitAnimations];
        }
            break;
            
        case 8000:
            [self textFieldShouldReturn:textField];
            [self showDatePicker];
            [self textFieldDidEndEditing:textField];
            break;
            
        case 6:
        case 7:
            errorText.text = @"";
            updateButton.enabled = YES;
            break;
            
        default:
            break;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    table_UserInfo.scrollEnabled=YES;
    if (customBarItem.enabled == FALSE) {
        customBarItem.enabled = TRUE;
    }
    
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    UITableViewCell* myCell;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        myCell = (UITableViewCell *) textField.superview;
        
    }
    else
    {
        myCell = (UITableViewCell *) textField.superview.superview;
    }
    
    //UITableViewCell* myCell = (UITableViewCell*)textField.superview.superview;
    NSIndexPath *indexPath = [table_UserInfo indexPathForCell:myCell];
    index_Path = indexPath;
    DLog(@"%ld",(long)indexPath.row);
    DLog(@"%ld",(long)textField.tag);
    
    //    UserInfoDO *userInfoObj = [userInfoObjArray objectAtIndex:index_Path.row];
    
    switch (textField.tag) {
            
        case 1000:[newFirstName setString: @""];
            NSLog(@"The lenght of new name = %lu",(unsigned long)[newFirstName length]);
            [newFirstName appendString:textField.text];
            firstNameFlag=TRUE;
            
            break;
            
        case 2000:{
            
            CGRect viewFrame = self.view.frame;
            viewFrame.origin.y += animatedDistance;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
            [self.view setFrame:viewFrame];
            [UIView commitAnimations];
            [newLastName setString: @""];
            [newLastName appendString:textField.text];
            
            
            lastNameFlag=TRUE;
        }
            break;
            
        case 3000: {
            
            CGRect viewFrame = self.view.frame;
            viewFrame.origin.y += animatedDistance;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
            [self.view setFrame:viewFrame];
            [UIView commitAnimations];
            [newEmail setString: @""];
            [newEmail appendString:textField.text];
            emailFlag=TRUE;
        }
            
            break;
            
        case 4000:{
            
            CGRect viewFrame = self.view.frame;
            viewFrame.origin.y += animatedDistance;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
            [self.view setFrame:viewFrame];
            [UIView commitAnimations];
            [newZipCode setString: @""];
            
            [newZipCode appendString:textField.text];
            zipFlag=TRUE;
        }
            break;
            
        case 5000:{
            
            CGRect viewFrame = self.view.frame;
            viewFrame.origin.y += animatedDistance;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
            [self.view setFrame:viewFrame];
            [UIView commitAnimations];
            [newMobile setString: @""];
            
            [newMobile appendString:textField.text];
            mobileFlag=TRUE;
        }
            break;
            
        case 8000:
            // userInfoObj.value=selectedDOB;
            
        case 6:
        case 7:
            errorText.text = @"";
            updateButton.enabled = YES;
            break;
        default:
            break;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(NSMutableAttributedString*) getTagColor : (NSString*) text
{
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:text];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(text.length-1,1)];
    
    return string;
}

@end

//
//  PreferredCategoriesScreen.h
//  HubCiti
//
//  Created by Anjana on 9/12/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

/*********************************************************
 Class is designed selection of Preferred categories 
 Called while user registration / Sign up.
 *********************************************************/

#import <UIKit/UIKit.h>
#import "VPPDropDown.h"
#import "VPPDropDownDelegate.h"

@interface PreferredCategoriesScreen : UIViewController<VPPDropDownDelegate,HubCitiConnectionManagerDelegate,CustomizedNavControllerDelegate> {
    

	IBOutlet UITableView *categoryList;
	IBOutlet UIToolbar *toolbar;
	UIBarButtonItem *customItem;
    
	NSMutableArray *mainCategoryNameArray;
    NSMutableArray *parCatIdArray;
	NSMutableArray *subCategoryNameArray[NUM_CATEGORIES];
	NSMutableArray *subCategoryIdArray[NUM_CATEGORIES];
	NSMutableArray *displayedArray[NUM_CATEGORIES];
	NSMutableArray *categoryIdArray[NUM_CATEGORIES];
    
    //DropDown Edits
    VPPDropDown *DropDownItem;
    NSMutableArray *SubCategoriesDDArray;
    NSMutableArray *DropDownsDDArray;
    NSMutableArray *displayedDDArray;
    NSMutableArray *categoryIDDDArray;
    
    WebRequestState iWebRequestState;  //create enum
    
}

@property (nonatomic, strong) UITableView *categoryList;

-(IBAction)savePressed:(id)sender;


@end

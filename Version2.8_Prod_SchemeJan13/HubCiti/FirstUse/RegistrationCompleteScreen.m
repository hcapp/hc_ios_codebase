//
//  RegistrationCompleteScreen.m
//  HubCiti
//
//  Created by Anjana on 9/12/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "RegistrationCompleteScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "UserInformationViewController.h"
#import "HubCitiManager.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "SignupDO.h"
#import "MainMenuViewController.h"


extern BOOL isSameUser;
@interface RegistrationCompleteScreen ()

@end

@implementation RegistrationCompleteScreen

#pragma mark - View lifecycle methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBar.hidden = NO;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        if (IS_IPHONE5)
            webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 320, 416+88)];
        else
            webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 320, 416)];
    }
    else
    {
        webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-44)];
    }
    
    
    webView.userInteractionEnabled = NO;
    webView.delegate = self;
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    activityIndicator.hidesWhenStopped = YES;
    [webView addSubview:activityIndicator];
    
    [self.view addSubview:webView];
    
    self.navigationItem.hidesBackButton=YES;
       [self setBottomToolbar];
    
    /*********** temporary addition. should be removed later*********/
    UILabel *titleLabel = [[UILabel alloc]init];
    //WithFrame:CGRectMake(60, 40, 200, 40)];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = NSLocalizedString(@"Congratulations let's get started!",@"Congratulations let's get started!");
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 2;
    titleLabel.translatesAutoresizingMaskIntoConstraints=NO;
    [self.view addSubview:titleLabel];
    
    NSDictionary *labelDictionary=@{@"label":titleLabel};
    
    if (DEVICE_TYPE == UIUserInterfaceIdiomPhone)
    {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(40)-[label(40)]" options:0 metrics:nil views:labelDictionary]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(60)-[label(200)]" options:0 metrics:nil views:labelDictionary]];
        titleLabel.font = [UIFont boldSystemFontOfSize:16];
        
        
    }
    else
    {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:  [NSString stringWithFormat:@"V:|-(%f)-[label(%f)]",(SCREEN_HEIGHT/4),(SCREEN_HEIGHT/12)] options:0 metrics:nil views:labelDictionary]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[label(%f)]",(SCREEN_WIDTH/4),(SCREEN_WIDTH/2)] options:0 metrics:nil views:labelDictionary]];
        titleLabel.font = [UIFont boldSystemFontOfSize:24];
        
        
    }
    
    
    ReleaseAndNilify(titleLabel);
    
    self.navigationItem.leftBarButtonItem = nil;
    
    /***** Logic to prevent View from expanding for iOS7 version *******/
    if([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
}
-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];

}

/*
 *
 Method to set the bottom tool bar along with buttons
 *
 */
-(void)setBottomToolbar
{
    if ([SharedManager userInfoFromSignup] == NO){
        CGRect rect = CGRectMake(0, self.view.frame.size.height-44, [[UIScreen mainScreen] bounds].size.width, 44);
        bottomToolBar = [[UIToolbar alloc]initWithFrame:rect];
        [bottomToolBar setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
        //bottomToolBar.barStyle = UIBarStyleBlackOpaque;
        [bottomToolBar setBarTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]]];
        [bottomToolBar sizeToFit];
        
        UIBarButtonItem *next = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Next",@"Next") style:UIBarButtonItemStylePlain target:self action:@selector(nextClicked:)];
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *privacyPolicy = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Privacy Policy",@"Privacy Policy") style:UIBarButtonItemStylePlain target:self action:@selector(privacyPolicyClicked:)];
        
        NSArray *items = [NSArray arrayWithObjects: privacyPolicy, flexibleSpace,next, nil];
        [bottomToolBar setItems:items animated:NO];
        [self.view addSubview:bottomToolBar];
        
    }
    else {
        
        getStarted = [UIButton buttonWithType:UIButtonTypeCustom];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            getStarted.frame = CGRectMake(9.0, 250.0, 302.0, 41.0);
        }
        else
        {
            getStarted.frame = CGRectMake(SCREEN_WIDTH/4, 3*(SCREEN_HEIGHT/4), SCREEN_WIDTH/2, 60);
        }
        getStarted.backgroundColor = [UIColor redColor];
        getStarted.layer.cornerRadius = 5.0f;
        [getStarted setTitle:NSLocalizedString(@"Get Started",@"Get Started") forState:UIControlStateNormal];
        [getStarted setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
        [getStarted setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //to handle the city preference back bottom button
        
        [getStarted addTarget:self action:@selector(getStartedClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:getStarted];
        
        getStarted.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"KEY_BUTTONCOLOR"]];
        [getStarted setTitleColor:[UIColor convertToHexString:[defaults valueForKey:@"KEY_BUTTONFONTCOLOR"]] forState:UIControlStateNormal];
        
        
    }
}


-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)nextClicked:(id)sender{
    // Navigate to user info screen based on server reponse
}


-(void)privacyPolicyClicked:(id)sender{
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert];
    }
    else {
        AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
        [self.navigationController pushViewController:privacy animated:NO];
        //[privacy release];
    }
}


/*
 *
 will call class displaying tutorial videos/slides
 *
 */
-(void)getStartedClicked:(id)sender {
    
    [SharedManager setUserInfoFromSignup:NO];
    [NewsUtility setUserSettings];
    NSString* customerId = [defaults objectForKey:@"currentuserId"];
    if (customerId) {
        
        if (![customerId isEqualToString:[defaults valueForKey:KEY_USERID]]) {
            
            ReleaseAndNilify(cashedResponse);
            [defaults setValue: nil forKey: @"currentuserId"];
        }
        else {
            isSameUser = true;
        }
    }
    [defaults setBool:YES forKey:@"LoginUserNews"];
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        [NewsUtility pushViewFromHamberger:self];
        
        
    }
    else{
        
        MainMenuViewController *imainPageViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:imainPageViewController animated:NO];
    }
    
    
}




#pragma mark - WebView delegate methods
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    [activityIndicator startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [activityIndicator stopAnimating];
    //if([responseXml isEqualToString:[NSString stringWithFormat:@"%@Registration_successtext.html", B_URL]])
    getStarted.hidden = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [activityIndicator stopAnimating];
    DLog(@"Error");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 *
 release various allocated objects here
 *
 */

@end

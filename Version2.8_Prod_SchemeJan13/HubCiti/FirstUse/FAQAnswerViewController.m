//
//  FAQAnswerViewController.m
//  HubCiti
//
//  Created by deepak.agarwal on 4/11/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import "FAQAnswerViewController.h"
#import "WebBrowserViewController.h"
#import "SpecialOffersViewController.h"
#import "AppDelegate.h"

@interface FAQAnswerViewController ()

@end

@implementation FAQAnswerViewController

@synthesize strQuestion;
@synthesize strAnswer;
@synthesize strHeaderTitle;
@synthesize arrQuestions;
@synthesize currentPage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.navigationItem.hidesBackButton = YES;
   
    
    [self setAnswerandQuestiononView];
    // Do any additional setup after loading the view from its nib.
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
}
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

-(int)calculateLabelHeight:(NSString*)text labelWidth:(int)labelWidth fontSize:(int)fontSize
{
    CGSize constraint;
    CGSize size ;
    CGFloat heightWebview ;
    constraint = CGSizeMake(labelWidth, 20000.0f);
    //    size = [text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:fontSizeText] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    size = [text boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:fontSize], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    
    heightWebview = MAX(size.height+20, 40.0f);
    return heightWebview;

}


// Create and Set view for Question and Answer display on ScrollView.
// Also add the left and Right Arrow button if Questions for cat are more then 1
-(void)setAnswerandQuestiononView
{
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:strHeaderTitle forView:self withHambergur:NO];

    
    
    //Add Arrow Image for Left Right Question Navigations
//    if([arrQuestions count]==1){
//        
//    }
    
    if([arrQuestions count]>=1)
    {
        btnLeftArrow = [UIButton buttonWithType:UIButtonTypeCustom];
       // btnLeftArrow.frame = CGRectMake(0, 180, 25, 60);
        
        [btnLeftArrow addTarget:self action:@selector(clickonLeftArrow:) forControlEvents:UIControlEventTouchUpInside];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
         btnLeftArrow.frame = CGRectMake(5, SCREEN_HEIGHT-130, 60, 60);
         [btnLeftArrow setBackgroundImage:[UIImage imageNamed:@"arrow_left_black"] forState:UIControlStateNormal];
        }
        else
        {
            btnLeftArrow.frame = CGRectMake(5, SCREEN_HEIGHT-190, 120, 120);
            [btnLeftArrow setBackgroundImage:[UIImage imageNamed:@"arrow_left_black"] forState:UIControlStateNormal];
        }
        
        [btnLeftArrow setBackgroundColor:[UIColor clearColor]];
        [btnLeftArrow setAccessibilityLabel:@"leftArrow"];
        [self.view addSubview:btnLeftArrow];
        
        btnRightArrow = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [btnRightArrow addTarget:self action:@selector(clickonRightArrow:) forControlEvents:UIControlEventTouchUpInside];
         if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
             btnRightArrow.frame = CGRectMake(SCREEN_WIDTH-65,SCREEN_HEIGHT-130, 60, 60);
        [btnRightArrow setBackgroundImage:[UIImage imageNamed:@"arrow_right_black"] forState:UIControlStateNormal];
         }
        else
        {
             btnRightArrow.frame = CGRectMake(SCREEN_WIDTH-130,SCREEN_HEIGHT-190, 120, 120);
             [btnRightArrow setBackgroundImage:[UIImage imageNamed:@"arrow_right_black"] forState:UIControlStateNormal];
        }
        [btnRightArrow setBackgroundColor:[UIColor clearColor]];
        [btnRightArrow setAccessibilityLabel:@"rightArrow"];
        [self.view addSubview:btnRightArrow];
        
        [self setLeftandRightArrowStatus];

    }
    
    [self setquestionAnswer];
    
}

// Set the Enable/Disable State of Left and Right Arrow as per current page and total questions avaialble to display
-(void)setLeftandRightArrowStatus
{
    if ([arrQuestions count]==1) {
        btnLeftArrow.enabled=NO;
        btnRightArrow.enabled=NO;
    }
    if(currentPage > 0)
        btnLeftArrow.enabled=YES;
    else
        btnLeftArrow.enabled=NO;
    
    if([arrQuestions count]>currentPage+1)
        btnRightArrow.enabled=YES;
    else
        btnRightArrow.enabled=NO;

}

// Set the Lable for Answer and Question text on ScrollView
-(void)setquestionAnswer
{
    NSArray *arrSubView = [self.view subviews];
    
    for(int subview=0; subview <[arrSubView count]; subview++)
    {
        UIView *view = [arrSubView objectAtIndex:subview];
        if(view.tag==100)
        {
            [view removeFromSuperview];
        }
    }
    

    float xPos = 15.0;
    float yPOS = 5.0;
    int width = SCREEN_WIDTH-30-2;
    int height = [self calculateLabelHeight:strQuestion labelWidth:width fontSize:13];
    
    // Label to display Question
    UILabel *questions = [[UILabel alloc]initWithFrame:CGRectMake(xPos, yPOS, width, height)];
    questions.lineBreakMode = NSLineBreakByWordWrapping;
    questions.numberOfLines = 0;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        questions.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
    }
    else
    {
        questions.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
    }
    questions.tag=100;
    questions.textAlignment = NSTextAlignmentLeft;
    questions.backgroundColor = [UIColor colorWithRGBInt:0xE6E6E6];
	[questions setText:strQuestion];
    
	// UIWebView *questions = [[UIWebView alloc]initWithFrame:CGRectMake(xPos, yPOS, width, SCREEN_HEIGHT/4)];
	//questions.layer.cornerRadius = 5.0;
 	//questions.layer.borderColor = [UIColor grayColor].CGColor;
 	//[questions loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#0000;'>%@",strQuestion]baseURL:nil];
    [self.view addSubview:questions];
    
    yPOS = yPOS + height;
    
    
    // UIWebView to display Answer of Question

    UIWebView *AnswerDesc ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
         AnswerDesc =[[UIWebView alloc]initWithFrame:CGRectMake(xPos, yPOS + 5, width, SCREEN_HEIGHT-130-12-height)];
    }
    else
    {
         AnswerDesc =[[UIWebView alloc]initWithFrame:CGRectMake(xPos, yPOS + 5, width, SCREEN_HEIGHT-210-12-height)];
    }
   
    AnswerDesc.delegate=self;
    AnswerDesc.layer.cornerRadius = 5.0;
    AnswerDesc.layer.borderColor = [UIColor grayColor].CGColor;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
    [AnswerDesc loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#000;'>%@",strAnswer] baseURL:nil];
    }
    else
    {
       [AnswerDesc loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:17px;font-family:Helvetica;color:#000;'>%@",strAnswer] baseURL:nil];  
    }
    
    [self.view addSubview:AnswerDesc];
    
    //yPOS = yPOS + height;
    
//    [AnswerDesc release];
//    [questions release];

}





-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil) {
    
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        
            splOfferVC.isEventLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
            [HubCitiAppDelegate setIsLogistics:YES];
        
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
     }
}


-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    
    if ([[[inRequest URL] scheme] isEqual:@"mailto"]) {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,
                                                                                                 (CFStringRef)value,
                                                                                                 CFSTR("")));
                   // [value autorelease];
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
           // [mailViewController release];
            return NO;
            
        }
        
        else {
            
            [UtilityManager showAlert:@"Cannot Send" msg:@"Please go to the Mail applications and set up an email account"];
            }
        
        return NO;
        
    }
    
    
    if ([[inRequest URL] fragment]) {

        return YES;
        [inWeb stringByEvaluatingJavaScriptFromString:@"window.location.hash='#hashtag'"];
        
        return YES;
    }
    
    
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {

        NSString *url = [[inRequest URL]absoluteString];
        [defaults setValue:url forKey:KEY_URL];
        [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
        
        NSURL* urls = [NSURL URLWithString:[[inRequest URL]absoluteString]];
        
        NSString *urlString=[[inRequest URL]absoluteString];
        
        if ([[urls absoluteString] containsString:@"itunes.apple.com"]) {
            [UtilityManager customUrlLoad:urls];
        }

        else if ([urlString containsString:@"3000.htm"]) {
          //  NSString *name =[NSString stringWithFormat:@"%@",[urls.pathComponents objectAtIndex:3]];
            
            if([urlString isEqualToString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    
                    UIAlertController * alert;
                   
                        alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
                else{
                   
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    splOfferVC.isEventLogisticsFlag = 1;                    
                   
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            else{
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
                
            }
        }
        else{
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
            
        }
        return NO;
    }
    
    return YES;
}




- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];// ModalViewControllerAnimated:YES];
}



// Handle the Action when Left arrow will click to get the previou page question & Answer
-(void)clickonLeftArrow:(id)sender
{
    if(currentPage>0)
        currentPage = currentPage -1;
    
    [self setLeftandRightArrowStatus];

    NSMutableDictionary *dic = [arrQuestions objectAtIndex:currentPage];
    
    [self request_faqdetails:(int)[[dic valueForKey:@"faqId"]integerValue]];

}

// Handle the Action when Right arrow will click to get the Next page question & Answer
-(void)clickonRightArrow:(id)sender
{
    if([arrQuestions count]>currentPage)
        currentPage = currentPage+1;
    
    [self setLeftandRightArrowStatus];

    NSMutableDictionary *dic = [arrQuestions objectAtIndex:currentPage];
    [self request_faqdetails:(int)[[dic valueForKey:@"faqId"]integerValue]];

}


// Get the Answer for the Questions
-(void)request_faqdetails:(int)selfaqId
{
    
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<FAQDetails><hubCitiId>%@</hubCitiId><faqId>%d</faqId>",[defaults valueForKey:KEY_HUBCITIID],selfaqId];
    
    [reqStr appendFormat:@"</FAQDetails>"];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@firstuse/faqdetails",BASE_URL];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}


// Handle Response
-(void)responseData:(NSString*)response
{
    [self parsefaqAnswer:response];
}

// Parse and Display the Answer for the Questions on View
-(void)parsefaqAnswer:(NSString*)response
{
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        TBXMLElement *answerElement = [TBXML childElementNamed:@"answer" parentElement:tbxml.rootXMLElement];
        
        if(answerElement)
            strAnswer = [[NSString alloc]initWithString:[TBXML textForElement:answerElement]];
        
        NSMutableDictionary *dic = [arrQuestions objectAtIndex:currentPage];
        if([dic valueForKey:@"question"]){
//            if(strQuestion)
//                [strQuestion release];
            strQuestion = [[NSMutableString alloc]initWithString:[dic valueForKey:@"question"]];
        }

        
        [self setquestionAnswer];
     }
    
    else{
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

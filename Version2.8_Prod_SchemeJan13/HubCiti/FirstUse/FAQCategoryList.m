//
//  FAQCategoryList.m
//  HubCiti
//
//  Created by Keerthi on 28/02/14.
//  Copyright (c) 2014 span infotech india ptv. ltd. All rights reserved.
//
#import "SpecialOffersViewController.h"
#import "FAQCategoryList.h"
#import "FaqDetailsDo.h"
#import "MainMenuViewController.h"
#import "FAQQuestionsList.h"
#import "FAQAnswerViewController.h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "SettingsViewController.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "ButtonSetupViewController.h"
#import "SESpringBoard.h"
#import "CityExperienceViewController.h"
#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "FindViewController.h"
#import "AnythingPage.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "AppDelegate.h"
#import "SingleCatRetailers.h"
//#import "ScanNowScreen.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "EventsListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FilterListViewController.h"
#import "FundraiserListViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "HTTPClient.h"
#import "GetUserInfoResponse.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponsInDealViewController.h"


@interface FAQCategoryList ()
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@end
static int lastVisitedRecord = 0;
//static int animate = 1;
@implementation FAQCategoryList{
    bottomButtonView *view;
}
//@synthesize searchResult,anyVC;
@synthesize infoResponse;
@synthesize searchResult,anyVC,emailSendingVC;

#pragma mark - View lifecycle methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject

{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    
    
    
    [self responseData:responseObject];
    
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setAccessibilityLabel:@"faqCategory"];
    selectedRow = -1;
    // Do any additional setup after loading the view from its nib.
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"FAQ's" forView:self withHambergur:YES];
    //self.title=NSLocalizedString(@"FAQ's", @"FAQ's");
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.barStyle=UIBarStyleBlackOpaque;
    faqDetailsArray = [[NSMutableArray alloc]init];
    
    [faqTableView setAccessibilityLabel:@"faqs"];
    [faqTableView setAccessibilityIdentifier:@"faqTable"];
    
    //customize back button and main menu button displayes on the navigation bar.
   
    self.navigationItem.hidesBackButton = YES;
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"MainMenu"];
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    //notification
     [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    
    [self request_utgetmainmenuid];
    [self request_GetFaqList:0 searchKey:@""];
    
    //added for searchable FAQ
    if (IOS7 == NO){
        searchFAQ.tintColor = [UIColor blackColor];
        
    }
    
    for(id subview in [searchFAQ subviews])
    {
        if ([subview isKindOfClass:[UIButton class]]) {
            
            cancelBtn = (UIButton*)subview;
            cancelBtn.tag = 100;
        }
    }
    
    searchResult=@"";
    /***** Logic to prevent View from expanding for iOS7 version *******/
    dispatch_async(dispatch_get_main_queue(), ^{
        if([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    });
 
    
   
    
}
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    //     [faqTableView reloadData];
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideBackButton:NO];
    [cusNav hideHambergerButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    [searchFAQ resignFirstResponder];
    self.navigationController.navigationBar.hidden = NO;
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}

-(void)settableViewOnScreen
{
    if(faqTableView==nil){
        int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
        if(bottomBtn==1){
            faqTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-50-44) style:UITableViewStylePlain];
        }
        else{
            faqTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-44) style:UITableViewStylePlain];
        }
        faqTableView.dataSource=self;
        faqTableView.delegate=self;
        [faqTableView setBackgroundColor:[UIColor whiteColor]];
        faqTableView.tableFooterView.hidden=YES;
        [self.view addSubview:faqTableView];
    }
}

#pragma mark navigation barbutton actions



/*
 *
 Navigate back to Main Menu screen page
 *
 */
-(void)returnToMainPage:(id)sender
{
    //return to main page
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

/*
 Navigate back to previous page
 */
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}


-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}





-(void)bottomButtonPressed:(id)sender
{
    [defaults setBool:YES forKey:BottomButton];
    //    [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int) btn.tag;
    
    bottomButtonDO *obj_faqBottomDO = [arrFAQBottomButtonDO objectAtIndex:tag];
    
    [defaults setValue:obj_faqBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_faqBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_faqBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_faqBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_faqBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_faqBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_faqBottomDO.btnLinkTypeName);
    if ([obj_faqBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_faqBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
            
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    else if ([obj_faqBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_faqBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_eventBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_faqBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                //[SingleEventCatID addObject:obj_eventBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_faqBottomDO.bottomBtnID];
            }
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            self.iEventsListViewController = iEventsListViewController;
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            ////[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    else{ //other menu types
        
        switch ([[obj_faqBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_faqBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                self.hotDeals=hotDeals;
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                //[hotDeals release];
                break;
                
            case 1://Scan Now
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                [EventsBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [NearbyBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:obj_faqBottomDO.bottomBtnID];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                [self navigateToFindView];
            }
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                self.cityExp = citi;
                [self.navigationController pushViewController:citi animated:NO];
            }
                //[citi release];
                break;
                
            case 7://City Services
                
                break;
                
            case 8://Visitors Bureau
                
                break;
                
            case 9://transportation
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];}
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
            }
                break;
                
            case 13:{
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=TRUE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_faqBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    
                    CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    
                    self.cityExp=cevc;
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    self.filters=filters;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    self.filterList = filterList;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                //  [dvc release];
                
                break;
            }
                
            case 20:
            {
                
                //                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                //                [self.navigationController pushViewController:faqCatList animated:NO];
                //                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                [FundraiserBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }break;
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        // [inform release];
                    }
                    
                }
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            //[citPref release];
                        }
                    }
                }
                
            }
                break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
              //  privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
                
            default:
                break;
        }
    }
    //NSlog(@"%d",tag);
}


// To Display the cuome UI for Bottom Bar Menu
-(void)setBottomBarMenu
{
    // Calculate Y position for View
    arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH, bottomBarButtonHeight)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [arrFAQBottomButtonDO count]; btnLoop++)
    {
        bottomButtonDO *obj_faqBottomDO = [arrFAQBottomButtonDO  objectAtIndex:btnLoop];
        
        view = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_faqBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-bottomBarButtonHeight, 80,bottomBarButtonHeight) imageName:obj_faqBottomDO.bottomBtnImg img_off:obj_faqBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            view = [bottomButtonView initWithTitle:obj_faqBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[arrFAQBottomButtonDO count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[arrFAQBottomButtonDO count],bottomBarButtonHeight) imageName:obj_faqBottomDO.bottomBtnImg img_off:obj_faqBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_faqBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
    }
}




#pragma mark Request Methods


-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=TRUE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if (![defaults boolForKey:BottomButton]){
        
        if([defaults valueForKey:KEY_MITEMID])
            [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else{
        if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if(![defaults boolForKey:BottomButton]){
        if ([defaults valueForKey:KEY_MITEMID])
            [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else {
        
        if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

//-(void)request_GetUserCat{
//
//    iWebRequestState = GETFAVCATEGORIES;
//    if([Network currentReachabilityStatus]==0)
//    {
//        [common showAlert];
//    }
//    else {
//
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//
//    }
//}


-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}

//in case of only 1 partner
-(void)request_PartnerRet{
    
    iWebRequestState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if(![defaults boolForKey:BottomButton]){
        if ([defaults valueForKey:KEY_MITEMID])
            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else {
        
        if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
    
    NSMutableString *urlString = [BASE_URL mutableCopy];
    [urlString appendString:@"thislocation/partnerret"];
    
    //iWebRequestState = GETRETFORPARTNER;
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
}


#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        return;
    
    [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    [self showActionSheet];
    
}


-(void) showActionSheet {
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }

    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
        
    }
    
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    __typeof(self) __weak  obj = self;
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
    
}


-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}








#pragma mark parse methods
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case GETUSERINFOM:
        case GETUSERINFO:
            [self parseGetUserData:response];
            break;
        case CITIEXPRET:
            
            [self parse_CitiExpRet:response];
            
            break;
        case PARTNERRETS:
            [self parse_PartnerRet:response];
            break;
            
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
            
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
        case FAQ:
            [self parse_faqdisplay:response];
            break;
            
        default:
            break;
    }
    
    
    [faqTableView reloadData];
    
}



-(void)parse_PartnerRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            self.cityExp=cevc;
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
            
            FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            self.filters=pvc;
            pvc.isOnlyOnePartner = YES;
            [self.navigationController pushViewController:pvc animated:NO];
            //[pvc release];
        }
    }
}




-(void)parse_CitiExpRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [defaults setObject:nil forKey:@"Response_Austin"];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Austin"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            self.cityExp=cevc;
            [defaults setInteger:100 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
        }
    }
}

//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            // NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                              [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}

-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}


//-(void) navigateToScanNow
//{
//    [defaults setBool:YES forKey:@"KEY_SCANSPLASH"];
//    [defaults setValue:@"scanNow" forKey:KEY_SEARCHFROM];
//    [defaults setObject:@"YES" forKey:@"KEY_SCANNERCALLED"];
//
//    int menulevel = [[defaults valueForKey:KEY_MENULEVEL]integerValue];
//    if(menulevel > 1)
//        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
//    else
//        [defaults  setBool:NO forKey:@"DisplayBackBarButton"];
//
//
//    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
//    ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];
//    [self.navigationController pushViewController:viewScanNowScreen animated:NO];
//    [viewScanNowScreen release];
//
//}


-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}

//Call Find Service and Class
-(void)navigateToFindView
{
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    [defaults setBool:YES forKey:BottomButton];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    navigatedFromMainMenu=false;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}

-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    self.iEventsListViewController = iEventsListViewController;
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //  //[iEventsListViewController release];;
}


-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}




-(void)releaseAllocatedObjects
{
    NSArray *arrSubViews = [self.view subviews];
    for(int i=0; i<[arrSubViews count]; i++)
    {
        UIView *releaseObjView = (UIView*)[arrSubViews objectAtIndex:i];
        if(releaseObjView.tag == 99)
            [releaseObjView removeFromSuperview];
        if([releaseObjView isKindOfClass:[bottomButtonView class]])
        {
            [releaseObjView removeFromSuperview];
        }
    }
    
    if([arrFAQBottomButtonDO count]>0)
    {
        [arrFAQBottomButtonDO removeAllObjects];
        //[arrFAQBottomButtonDO release];
        arrFAQBottomButtonDO = nil;
    }
    if([arrBottomButtonViewContainer count]>0)
    {
        [arrBottomButtonViewContainer removeAllObjects];
        //[arrBottomButtonViewContainer release];
        arrBottomButtonViewContainer = nil;
    }
    
}



#pragma mark - Request method
-(void)request_utgetmainmenuid
{
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<MenuItem><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>", [defaults  valueForKey:KEY_BOTTOMBUTTONID]];
    
    if([defaults  valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId>", [defaults  valueForKey:KEY_MITEMID]];
    
    if([defaults  valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_HUBCITIID]];
    
    [requestStr appendFormat:@"<platform>%@</platform>",@"IOS"];
    
    [requestStr appendString:@"</MenuItem>"];
    
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendFormat:@"firstuse/utgetmainmenuid"];
    
    NSString *response=[ConnectionManager establishPostConnection:urlString withParam:requestStr];
    [self parse_utgetmainmenuid:response];
    
    //[requestStr release];
}


- (void) request_GetFaqList:(int)lastRecord searchKey:(NSString *)searchKey{
    
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/faqcategory?hubCitiId=%@&userId=%@",BASE_URL,[defaults valueForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_USERID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/faqcategory?hubCitiId=%@&lowerLimit=0&userId=%@&platform=%@&searchKey=%@",BASE_URL,[defaults valueForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_USERID],@"IOS",searchKey];
    
    NSString *responseXml = [ConnectionManager establishGetConnection:urlString];
    
    [self reponse_ParseFaqList:responseXml];
    
}


#pragma mark - Parse method
-(void)parse_utgetmainmenuid:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement !=nil)
        {
            [defaults setObject:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        }
    }
    
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
}




-(void)reponse_ParseFaqList:(NSString *)response{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    
    //check for response code - 10000 if so failure display the alert with message as response text
    //else parse other elements
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
       // TBXMLElement *maxCountElement = [TBXML childElementNamed:@"maxCnt" parentElement:tbXml.rootXMLElement];
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbXml.rootXMLElement];
        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbXml.rootXMLElement]]intValue];
        if(nextPageElement !=nil)
        {
            nextPage=[[TBXML textForElement:nextPageElement]intValue];
        }
        TBXMLElement *maxRowNumElement = [TBXML childElementNamed:@"maxRowNum" parentElement:tbXml.rootXMLElement];
        if(maxRowNumElement !=nil)
        {
            lastVisitedRecord=[[TBXML textForElement:maxRowNumElement]intValue];
        }
        TBXMLElement *faqCatListElement = [TBXML childElementNamed:@"faqCatList" parentElement:tbXml.rootXMLElement];
        TBXMLElement *faqDetailsElement = [TBXML childElementNamed:@"FAQDetails" parentElement:faqCatListElement];
        
        
        while ( faqDetailsElement!= nil)
        {
            
            faqDetailsObj= [[FaqDetailsDo alloc]init];
            TBXMLElement *categotyElementId = [TBXML childElementNamed:@"categoryId" parentElement:faqDetailsElement];
            TBXMLElement *categotyElementName = [TBXML childElementNamed:@"categoryName" parentElement:faqDetailsElement];
            if (categotyElementId!=nil)
            {
                faqDetailsObj.categoryId = [TBXML textForElement:categotyElementId];
                faqDetailsObj.categoryName =[TBXML textForElement:categotyElementName];
            }
            
            faqDetailsElement= [TBXML nextSiblingNamed:@"FAQDetails" searchFromElement:faqDetailsElement];
            [faqDetailsArray addObject:faqDetailsObj];
        }
        
        if (bottomBtn==1) {
            
            TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbXml.rootXMLElement];
            arrFAQBottomButtonDO=[[NSMutableArray alloc]init];
            TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
            while(BottomButtonElement)
            {
                
                bottomButtonDO *obj_faqBottomDO = [[bottomButtonDO alloc]init];
                
                
                TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                
                
                if(bottomBtnIDElement)
                    obj_faqBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                
                if(bottomBtnNameElement)
                    obj_faqBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                else
                    obj_faqBottomDO.bottomBtnName = @" ";
                
                if(bottomBtnImgElement)
                    obj_faqBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                else
                    obj_faqBottomDO.bottomBtnImg = @" ";
                
                if(bottomBtnImgOffElement)
                    obj_faqBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                else
                    obj_faqBottomDO.bottomBtnImgOff = @" ";
                
                if(btnLinkTypeIDElement)
                    obj_faqBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                
                if(btnLinkTypeNameElement)
                    obj_faqBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                
                if(btnLinkIDElement){
                    obj_faqBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                    if(btnLinkTypeNameElement){
                        if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
                            [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
                    }
                }
                
                if(positionElement)
                    obj_faqBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                
                
                
                BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                [arrFAQBottomButtonDO addObject:obj_faqBottomDO];
                //[obj_faqBottomDO release];
            }
            if([arrFAQBottomButtonDO count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                [self setBottomBarMenu];
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self settableViewOnScreen];
        });
        
        
        if([faqDetailsArray count]<=1)
            [self getanddisplayQuestiondirectly];
        
    }
    
    
    
    
    else if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10005"])
    {
        //@Deepak: Parsed MainMenuID for userTracking
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbXml.rootXMLElement];
        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbXml.rootXMLElement]]intValue];
        if(mainMenuIdElement !=nil)
        {
            [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if(bottomBtn==1)
        {
            TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbXml.rootXMLElement];
            if(bottomButtonListElement !=nil)
            {
                TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
                arrFAQBottomButtonDO=[[NSMutableArray alloc]init];
                while(BottomButtonElement)
                {
                    
                    bottomButtonDO *obj_faqBottomDO = [[bottomButtonDO alloc]init];
                    
                    
                    TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                    TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                    
                    
                    if(bottomBtnIDElement)
                        obj_faqBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                    
                    if(bottomBtnNameElement)
                        obj_faqBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                    else
                        obj_faqBottomDO.bottomBtnName = @" ";
                    
                    if(bottomBtnImgElement)
                        obj_faqBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                    else
                        obj_faqBottomDO.bottomBtnImg = @" ";
                    
                    if(bottomBtnImgOffElement)
                        obj_faqBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                    else
                        obj_faqBottomDO.bottomBtnImgOff = @" ";
                    
                    if(btnLinkTypeIDElement)
                        obj_faqBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                    
                    if(btnLinkTypeNameElement)
                        obj_faqBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                    
                    if(btnLinkIDElement){
                        obj_faqBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                        if(btnLinkTypeNameElement){
                            if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
                                [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
                        }
                    }
                    
                    if(positionElement)
                        obj_faqBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                    
                    
                    
                    BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                    [arrFAQBottomButtonDO addObject:obj_faqBottomDO];
                    //[obj_faqBottomDO release];
                }
                if([arrFAQBottomButtonDO count] > 0)
                {
                    // Set Bottom Menu Button after read from response data
                    [self setBottomBarMenu];
                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                }
                else
                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            }
        }
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self settableViewOnScreen];
        });
    }
    
    
    
    else
    {
        
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
    [faqTableView reloadData];
    
}




#pragma mark - Tableview datasource and delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    if ((nextPage == 1)|| (lastVisitedRecord != 0)){
    //        if(nextPage==0)
    //            return [faqDetailsArray count];
    //        else
    //            return ([faqDetailsArray count]+1);
    //    }
    //    else
    return [faqDetailsArray count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    FaqDetailsDo *FaqDO ;
    UIButton *FirstButton = [UIButton buttonWithType:UIButtonTypeCustom];
    FirstButton.frame = CGRectMake(5, 0, 315, 26);
    [FirstButton addTarget:self action:@selector(clickedonCategory:) forControlEvents:UIControlEventTouchUpInside];
    [FirstButton setBackgroundImage:[UIImage imageNamed:@"menuBtnGradient"] forState:UIControlStateNormal];
    FirstButton.layer.borderWidth = 1.0;
    FirstButton.layer.borderColor = [UIColor colorWithRGBInt:0x8C8C8C].CGColor;
    FirstButton.tag = section;
    [FirstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    FirstButton.backgroundColor = [UIColor lightGrayColor];
    [FirstButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    }
    else
    {
        FirstButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    }
    FirstButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    FirstButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    //    if (section == [faqDetailsArray count] && nextPage == 1) {
    //
    //        [FirstButton setTitle:NSLocalizedString(@"                          View More Results...",@"                            View More Results...") forState:UIControlStateNormal];
    //    }
    
    //    else
    //    {
    if(section!=[faqDetailsArray count])
    {
        FaqDO = [faqDetailsArray objectAtIndex:section];
        [FirstButton setTitle:FaqDO.categoryName forState:UIControlStateNormal];
        UIImageView *arrowImage;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
        {
            arrowImage = [[UIImageView alloc]initWithFrame:CGRectMake(300, 17, 12, 13)];
        }
        else
        {
            arrowImage = [[UIImageView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-20, 31, 12, 13)];
        }
        [arrowImage setBackgroundColor:[UIColor clearColor]];
        [FirstButton addSubview:arrowImage];
        if([faqDetailsArray count]==1){
            
        }
        else if(FaqDO.isExpanded)
            [arrowImage setImage:[UIImage imageNamed:@"arrow_down"]];
        else
            [arrowImage setImage:[UIImage imageNamed:@"arrow_right"]];
    }
    //    }
    
    return FirstButton;
}

-(void)getanddisplayQuestiondirectly
{
    selectedRow = 0;
    iWebRequestState=FAQ;
    FaqDetailsDo *faqdo = [faqDetailsArray objectAtIndex:0];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<FAQDetails><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    if(searchResult==NULL){
        [reqStr appendFormat:@"<categoryId>%@</categoryId><platform>ISO</platform><searchKey><![CDATA[%@]]></searchKey><mainMenuId>%@</mainMenuId></FAQDetails>",faqdo.categoryId,@"",[defaults valueForKey:KEY_MAINMENUID]];
    }
    else
    {
        [reqStr appendFormat:@"<categoryId>%@</categoryId><platform>ISO</platform><searchKey><![CDATA[%@]]></searchKey><mainMenuId>%@</mainMenuId></FAQDetails>",faqdo.categoryId,searchResult,[defaults valueForKey:KEY_MAINMENUID]];
        
    }
    //<lowerLimit>0</lowerLimit>/
    
    NSString *urlStr = [NSString stringWithFormat:@"%@firstuse/faqdisplay",BASE_URL];
    
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
    
}

-(void)clickedonCategory:(id)sender
{
    
    if([faqDetailsArray count]> 1)
    {
        UIButton *btnCat = (UIButton*)sender;
        
        selectedRow = (int) btnCat.tag;
        if(selectedRow!=[faqDetailsArray count]){
            FaqDetailsDo *faqdo = [faqDetailsArray objectAtIndex:selectedRow];
            
            if(faqdo.isExpanded)
            {
                if(selectedRow >=0)
                {
                    FaqDetailsDo *faqdo = [faqDetailsArray objectAtIndex:selectedRow];
                    if(faqdo.arrQuestions)
                    {
                        if([faqdo.arrQuestions count]>0)
                            [faqdo.arrQuestions removeAllObjects];
                        
                        // [faqdo.arrQuestions release];
                    }
                }
                faqdo.isExpanded = FALSE;
                [faqTableView reloadData];
                return;
            }
            iWebRequestState=FAQ;
            //        NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<FAQDetails><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
            //        [reqStr appendFormat:@"<categoryId>%@</categoryId></FAQDetails>", faqdo.categoryId];
            //<lowerLimit>0</lowerLimit>
            
            NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<FAQDetails><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
            [reqStr appendFormat:@"<categoryId>%@</categoryId><platform>ISO</platform><searchKey><![CDATA[%@]]></searchKey></FAQDetails>", faqdo.categoryId,searchResult];
            
            
            NSString *urlStr = [NSString stringWithFormat:@"%@firstuse/faqdisplay",BASE_URL];
            
            [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
        }
        
        else{
            [self request_GetFaqList:lastVisitedRecord searchKey:@""];
        }
    }
    
}

#pragma mark - request methods
-(void)request_faqdetails:(int)selfaqId{
    
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<FAQDetails><hubCitiId>%@</hubCitiId><faqId>%d</faqId>",[defaults valueForKey:KEY_HUBCITIID],selfaqId];
    
    [reqStr appendFormat:@"</FAQDetails>"];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@firstuse/faqdetails",BASE_URL];
    
    NSString *responseXml=[ConnectionManager establishPostConnection:urlStr withParam:reqStr];
    [self parsefaqAnswer:responseXml];
    // Need to Parse the Answer and Display in next detail View
    
}

-(void)parsefaqAnswer:(NSString*)response
{
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        TBXMLElement *answerElement = [TBXML childElementNamed:@"answer" parentElement:tbxml.rootXMLElement];
        
        FaqDetailsDo *faqdo = [faqDetailsArray objectAtIndex:selectedIndexPath.section];
        
        FAQAnswerViewController *iFAQAnswerViewController = [[FAQAnswerViewController alloc]initWithNibName:@"FAQAnswerViewController" bundle:[NSBundle mainBundle]];
        if(answerElement)
            iFAQAnswerViewController.strAnswer = [[NSString alloc]initWithString:[TBXML textForElement:answerElement]];
        
        NSMutableDictionary *dic = [faqdo.arrQuestions objectAtIndex:selectedIndexPath.row];
        if([dic valueForKey:@"question"])
            iFAQAnswerViewController.strQuestion = [[NSMutableString alloc]initWithString:[dic valueForKey:@"question"]];
        
        if(faqdo.arrQuestions)
            iFAQAnswerViewController.arrQuestions = [[NSMutableArray alloc]initWithArray:faqdo.arrQuestions];
        
        iFAQAnswerViewController.currentPage=(int)selectedIndexPath.row;
        
        iFAQAnswerViewController.strHeaderTitle = [[NSString alloc]initWithString:faqdo.categoryName];
        
        [self.navigationController pushViewController:iFAQAnswerViewController animated:NO];
        
        
    }
    
    else{
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        }
    
}



-(void)parse_faqdisplay:(NSString*)response{
    
    FaqDetailsDo *faqdo = [faqDetailsArray objectAtIndex:selectedRow];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    
    
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        
       // TBXMLElement *nextPageFlagElement = [TBXML childElementNamed:@"nextPageFlag" parentElement:tbxml.rootXMLElement];
        //        nextPage = [[TBXML textForElement:nextPageFlagElement]intValue];
        
       // TBXMLElement *maxRowNumElement = [TBXML childElementNamed:@"maxRowNum" parentElement:tbxml.rootXMLElement];
        
        
        TBXMLElement *FAQDetailsElement = [TBXML childElementNamed:@"FAQDetails" parentElement:tbxml.rootXMLElement];
        
        faqdo.arrQuestions = [[NSMutableArray alloc]init];
        
        while (FAQDetailsElement!=nil)
        {
            NSMutableDictionary *dicQuestions = [[NSMutableDictionary alloc]init];
            
            TBXMLElement *faqIdElement = [TBXML childElementNamed:@"faqId" parentElement:FAQDetailsElement];
            TBXMLElement *questionElement = [TBXML childElementNamed:@"question" parentElement:FAQDetailsElement];
            TBXMLElement *answerElement = [TBXML childElementNamed:@"answer" parentElement:FAQDetailsElement];
            
            if(faqIdElement)
                [dicQuestions setValue:[TBXML textForElement:faqIdElement] forKey:@"faqId"];
            
            if(questionElement)
                [dicQuestions setValue:[TBXML textForElement:questionElement] forKey:@"question"];
            
            if(answerElement)
                [dicQuestions setValue:[TBXML textForElement:answerElement] forKey:@"answer"];
            
            [faqdo.arrQuestions addObject:dicQuestions];
            
            // [dicQuestions release];
            FAQDetailsElement = [TBXML nextSiblingNamed:@"FAQDetails" searchFromElement:FAQDetailsElement];
            
        }
        
        if([faqdo.arrQuestions count]>0)
            faqdo.isExpanded = TRUE;
        
        [faqTableView reloadData];
        
        //        if(selectedRow >= [faqDetailsArray count]-1)
        //        {
        //            NSIndexPath *tempIndexPath = [NSIndexPath indexPathForRow:0 inSection:selectedRow];
        //            [faqTableView scrollToRowAtIndexPath:tempIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        //}
        //Added By Keshava
        NSIndexPath *selectedRowIndexPath = [NSIndexPath indexPathForRow:(selectedRow+1) inSection:selectedRow];
        
        if ([[faqTableView visibleCells] containsObject:[faqTableView cellForRowAtIndexPath:selectedRowIndexPath]]==NO) {
            NSIndexPath *tempIndexPath = [NSIndexPath indexPathForRow:0 inSection:selectedRow];
            [faqTableView scrollToRowAtIndexPath:tempIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
        
        
    }
    
    
    else{
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section!=[faqDetailsArray count]) {
        FaqDetailsDo *faqdo = [faqDetailsArray objectAtIndex:section];
        if([faqdo.arrQuestions count]>0)
            return [faqdo.arrQuestions count];
    }
    return 0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {return 50;
    }
    else
    {
        return 70;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {return 44.0;
    }
    else
    {
        return 65.0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.backgroundColor =  [UIColor clearColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    FaqDetailsDo *faqdo = [faqDetailsArray objectAtIndex:indexPath.section];
    
    if(([faqdo.arrQuestions count]>indexPath.row))
    {
        NSMutableDictionary *dic =[faqdo.arrQuestions objectAtIndex:indexPath.row];
        UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 4, SCREEN_WIDTH - 50, 40)];
        lbl.numberOfLines = 2;
        [lbl setText:[dic valueForKey:@"question"]];
        [lbl setBackgroundColor:[UIColor clearColor]];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
        {
            [lbl setFont:[UIFont boldSystemFontOfSize:12.0]];
        }
        else
        {
            [lbl setFont:[UIFont boldSystemFontOfSize:16.0]];
        }
        [lbl setLineBreakMode:NSLineBreakByWordWrapping];
        [cell.contentView addSubview:lbl];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedIndexPath=indexPath;
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    else
    {
        // Get the Answer for the Question
        FaqDetailsDo *faqdo = [faqDetailsArray objectAtIndex:indexPath.section];
        if([faqdo.arrQuestions count]>0)
        {
            NSMutableDictionary *dic = [faqdo.arrQuestions objectAtIndex:indexPath.row];
            [self request_faqdetails:(int)[[dic valueForKey:@"faqId"]integerValue]];
        }
        
    }
    
    [faqTableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark searchbar delegate methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    cancelBtn.tag = 100;
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar resignFirstResponder];
    [faqDetailsArray removeAllObjects];
    
    //NSString *searchString = [[[NSString alloc] initWithFormat:@"%@",searchBar.text] ;
    //   NSError *error = nil;
    //    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
    //    NSString *trimmedString = [regex stringByReplacingMatchesInString:[searchBar.text copy] options:0 range:NSMakeRange(0, [searchBar.text length]) withTemplate:@" "];
    //    searchResult=[[trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] copy];
    //
    //     NSRegularExpression *regexExp = [NSRegularExpression regularExpressionWithPattern:@" " options:NSRegularExpressionCaseInsensitive error:&error];
    //   NSString *trimmedspaceString = [regexExp stringByReplacingMatchesInString:trimmedString options:0 range:NSMakeRange(0, [trimmedString length]) withTemplate:@"%20"];
    //
    //
    //    NSRegularExpression *regexAndExp = [NSRegularExpression regularExpressionWithPattern:@"&" options:NSRegularExpressionCaseInsensitive error:&error];
    //
    //    trimmedspaceString = [regexAndExp stringByReplacingMatchesInString:trimmedspaceString options:0 range:NSMakeRange(0, [trimmedspaceString length]) withTemplate:@"%26"];
    //     //   DLog(@"%@",trimmedspaceString);
    //    searchBar.text=trimmedString;
    searchResult=[searchBar.text copy];
    
    NSString *encodedStrng=[searchBar.text  urlEncodeUsingEncoding:NSUTF8StringEncoding];
    
    
    [self request_GetFaqList:0 searchKey:encodedStrng];
    
}


- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    
    [searchBar resignFirstResponder];
    searchResult=@"";
}

- (void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)searchText{
    
    if (searchBar.tag == 0 && ![searchText length]){
        [faqDetailsArray removeAllObjects];
        [searchBar resignFirstResponder];
        searchResult=@"";
        [self request_GetFaqList:0 searchKey:@""];
    }
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//-(void)requestToGetUserData
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
//    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//    ReleaseAndNilify(xmlStr);
//}
//-(void)presentSpecialOffersAfterDismissAleert {
//   if (self.isViewLoaded && self.view.window != nil) {
//    [HubCitiAppDelegate setIsLogistics:YES];
//    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
//    splOfferVC.isAppsiteLogisticsFlag = 1;
//    // [splOfferVC setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
//    [self presentViewController:splOfferVC animated:YES completion:nil];
// }
//}

-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }
}

-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];

}
-(void)parseGetUserData:(id)response
{
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}
@end

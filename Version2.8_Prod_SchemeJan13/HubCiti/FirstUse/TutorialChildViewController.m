//
//  TutorialChildViewController.m
//  HubCiti
//
//  Created by service on 8/17/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "TutorialChildViewController.h"
#import "HubCitiConstants.h"
#import "AppDelegate.h"
#import "WebBrowserViewController.h"
#import "SpecialOffersViewController.h"
@interface TutorialChildViewController ()
{
    WebBrowserViewController *urlDetail;
    SdImageView *splash;
    UITapGestureRecognizer* tapRecognizer;
    UIImageView* linkButtonImage;
}
@end

@implementation TutorialChildViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    splash= [[SdImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(20),VARIABLE_HEIGHT(60), SCREEN_WIDTH - 2*VARIABLE_WIDTH(20), VARIABLE_HEIGHT(150))];
    
    splash.hidden = true;
    splash.backgroundColor = [UIColor clearColor];
    [self.view addSubview:splash];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    
   [super viewWillAppear:animated];
   [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
}
-(void) viewWillDisappear:(BOOL)animated
{
   [super viewWillDisappear:animated];
   
}

-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [urlDetail.view removeFromSuperview];
    
}
- (UIImage*)resizeImageWithImage:(UIImage*)image toSize:(CGSize)newSize
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // draw in new context, with the new size
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    return newImage;
}
-(void) viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    splash.userInteractionEnabled = true;
    [linkButtonImage removeFromSuperview];
    
        self.skip.hidden = FALSE;
        self.signup.hidden = FALSE;
    
        if([[self.typeArr objectAtIndex:self.index] isEqualToString:@"1"] ) {
            
            splash.hidden = false;
            //splash.contentMode = UIViewContentModeScaleToFill;
            [HubCitiAppDelegate showActivityIndicator];
            [splash sd_setImageWithURL:[self.imageArray objectAtIndex:self.index] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                UIImage* imag = [self resizeImageWithImage:image toSize:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT - 35)];
                
                splash.frame = CGRectMake(0, 0, SCREEN_WIDTH, imag.size.height );
                
                [HubCitiAppDelegate removeActivityIndicator];
                
                if (self.isAnythingPageOrWeblink.count > 0 && ![[self.isAnythingPageOrWeblink objectAtIndex:self.index] isEqualToString:@"N/A"]) {
                    
                    
                    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(linkButtonPressed:)];
                    [tapRecognizer setNumberOfTapsRequired:1];

                    linkButtonImage = [[UIImageView alloc] init];
                    linkButtonImage.frame = CGRectMake((SCREEN_WIDTH - 57)/2, (SCREEN_HEIGHT - 57)/2, 70, 70);
                    linkButtonImage.backgroundColor = [UIColor whiteColor];
                    linkButtonImage.layer.borderColor = [UIColor blackColor].CGColor;
                    linkButtonImage.layer.cornerRadius = 70/2.0f;
                    linkButtonImage.layer.opacity = 0.7;
                    linkButtonImage.image = [UIImage imageNamed:@"tap-black.png"];
                    [splash addSubview:linkButtonImage];
                    [splash addGestureRecognizer:tapRecognizer];
                    
                    
                }
            }];
            
        }
        
        else{
            
            [HubCitiAppDelegate removeActivityIndicator];
            
            urlDetail = [[WebBrowserViewController alloc]init];
            [self.view addSubview:urlDetail.view];
            
            [defaults setValue:[self.imageArray objectAtIndex:self.index] forKey:KEY_URL];
            
            urlDetail.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.view.frame.size.height - 20 );
        }
    
   
}

-(void) linkButtonPressed : (UITapGestureRecognizer*)sender
{
    
        if(self.isAnythingPageOrWeblink.count > 0  && [[self.isAnythingPageOrWeblink objectAtIndex:self.index] isEqualToString:@"WebLink"] ) {
            
            
            [defaults setValue:[self.isAnythingPageOrWeblinkValue objectAtIndex:self.index] forKey:KEY_URL];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"WebPage" object:nil];
         
        }
        else{
            
            if (self.isAnythingPageOrWeblinkValue.count > 0 ) {
                NSString* pageId = [self.isAnythingPageOrWeblinkValue objectAtIndex:self.index] ;
                [defaults setObject:pageId forKey:@"pageid"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"AnyThing" object:nil];
            }
           
            
        }

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation

{
    
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

- (BOOL)shouldAutorotate{
    
    return NO;
    
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    
    return UIInterfaceOrientationPortrait;
    
}



- (UIInterfaceOrientationMask)supportedInterfaceOrientations

{
    
    return UIInterfaceOrientationMaskPortrait;
    
}

@end

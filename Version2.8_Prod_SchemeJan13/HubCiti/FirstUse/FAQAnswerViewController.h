//
//  FAQAnswerViewController.h
//  HubCiti
//
//  Created by deepak.agarwal on 4/11/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface FAQAnswerViewController : UIViewController<UIWebViewDelegate,MFMailComposeViewControllerDelegate,CustomizedNavControllerDelegate>
{
    NSString *strQuestion;
    NSString*strAnswer;
    NSString*strHeaderTitle;
    
    UIButton *btnLeftArrow;
    UIButton *btnRightArrow;
    
    int currentPage;
    NSMutableArray *arrQuestions;
}
@property(readwrite)int currentPage;
@property(nonatomic,strong)NSMutableArray *arrQuestions;
@property(nonatomic,strong)NSString *strHeaderTitle;
@property(nonatomic,strong)NSString *strQuestion;
@property(nonatomic,strong)NSString *strAnswer;
@end

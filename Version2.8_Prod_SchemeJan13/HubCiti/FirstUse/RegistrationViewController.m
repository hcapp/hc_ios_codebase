//
//  RegistrationViewController.m
//  HubCiti
//
//  Created by Anjana on 9/11/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//


#import "RegistrationViewController.h"
#import "UserInformationViewController.h"
#import "HubCitiManager.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "LoginDO.h"
#import "MainMenuViewController.h"
#import "LoginViewController.h"
#include "HTTPClient.h"
#include "AppDelegate.h"
#import "GetUserInfoResponse.h"
#import "UserInfoFields.h"
#import "ArIncomeRangeList.h"
#import "ArEducationList.h"
#import "ArMaritalList.h"



extern BOOL isSameUser;
static const int x_origin = 10;
@interface RegistrationViewController ()<HTTPClientDelegate>
@property(nonatomic,retain) GetUserInfoResponse* infoResponse ;
@end

@implementation RegistrationViewController
@synthesize signup_DO;
@synthesize infoResponse;

#pragma mark - View lifecycle methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) addConstraintsUI
{
    table_Signup.translatesAutoresizingMaskIntoConstraints = NO;
    button_SetPref.translatesAutoresizingMaskIntoConstraints=NO;
    button_Save.translatesAutoresizingMaskIntoConstraints = NO;
    button_GetStarted.translatesAutoresizingMaskIntoConstraints=NO;
    
    NSDictionary *viewsDictionary = @{@"signUP":table_Signup,@"save":button_Save,@"start":button_GetStarted,@"pref":button_SetPref};
    
    NSArray *signUP_HConstaint = [NSLayoutConstraint
                                  constraintsWithVisualFormat:@"H:|-150-[signUP(468)]-150-|"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary];
    
    NSArray *save_HConstraint = [NSLayoutConstraint
                                 constraintsWithVisualFormat:@"H:|-200-[save(368)]-200-|"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary];
    
    NSArray *start_HConstraint = [NSLayoutConstraint
                                  constraintsWithVisualFormat:@"H:|-200-[pref(368)]-200-|"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary];
    
    NSArray *pref_HConstraint = [NSLayoutConstraint
                                 constraintsWithVisualFormat:@"H:|-200-[start(368)]-200-|"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary];
    
    NSArray *vConstarint = [NSLayoutConstraint
                            constraintsWithVisualFormat:@"V:|-[signUP(300)]-50-[save(50)]-30-[pref(50)]-30-[start(50)]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary];
    
    [self.view addConstraints:signUP_HConstaint];
    [self.view addConstraints:save_HConstraint];
    [self.view addConstraints:start_HConstraint];
    [self.view addConstraints:pref_HConstraint];
    [self.view addConstraints:vConstarint];
    
    
    
}
- (void)dealloc {
    
    [table_Signup removeObserver:self forKeyPath:@"contentSize"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setAccessibilityLabel:@"Registration"];
    // Do any additional setup after loading the view from its nib.
    table_Signup.layer.cornerRadius = 5.0f;
    
    [table_Signup addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        [self addConstraintsUI];
    }
    /***** Logic to prevent View from expanding for iOS7 version *******/
    if([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
   
    [cusNav hideHambergerButton:YES];
    
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    DLog(@"%@--navigation",self.navigationController);
    NSLog(@"%@-->%@",self.navigationController.viewControllers,self.navigationController.parentViewController);
    /****************************************************
     image to be displayed on navigation bar
     dynamic image based on city-
     will be same as login screen nav header image
     *****************************************************/
    //    UIImageView *navLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
    //	self.navigationItem.titleView = navLogo;
    //	[navLogo release];
    
    
    /************** label to be displayed on right of navigation bar**************/
    //	UILabel *navRightLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 80, 25)];
    //	navRightLabel.text = NSLocalizedString(@"  Registration",@"  Registration");
    //	navRightLabel.backgroundColor = [UIColor clearColor];
    //	navRightLabel.textColor = [UIColor whiteColor];
    //	navRightLabel.font = [UIFont systemFontOfSize:12];
    //	UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:navRightLabel];
    //	self.navigationItem.rightBarButtonItem = barButton;
    //	ReleaseAndNilify(navRightLabel);
    //	ReleaseAndNilify(barButton);
    self.navigationItem.title = NSLocalizedString(@"  Registration",@"  Registration");
    
    if(userIdentifier==TRUE)
    {
        [cusNav hideBackButton:YES];
        UIButton *backBtn = [UtilityManager customizeBarButtonItem:@"Login"];
        [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        self.navigationItem.leftBarButtonItem = back;
        self.navigationItem.hidesBackButton = YES;
        ReleaseAndNilify(back);
        
        
    }
    else
    {
        //customize back button
        cusNav.customNavBardelegate = self;
        [cusNav hideBackButton:NO];
        self.navigationItem.hidesBackButton = YES;
       // ReleaseAndNilify(back);
    }
    userIdentifier=FALSE;
    button_SetPref.enabled = NO;
    button_GetStarted.enabled = NO;
    
    
    //Assign Custom UI values
    button_Save.layer.cornerRadius = 5.0f;
    button_Save.backgroundColor = [UIColor convertToHexString:signup_DO.buttonColor];
    [button_Save setTitleColor:[UIColor convertToHexString:signup_DO.buttonFontColor] forState:UIControlStateNormal];
    [button_Save setAccessibilityLabel:@"save"];
    button_Save.alpha = 1.0;
    
    button_GetStarted.layer.cornerRadius = 5.0f;
    button_GetStarted.backgroundColor = [UIColor convertToHexString:signup_DO.buttonColor];
    [button_GetStarted setTitleColor:[UIColor convertToHexString:signup_DO.buttonFontColor] forState:UIControlStateNormal];
    [button_GetStarted setAccessibilityLabel:@"getstarted"];
    button_GetStarted.alpha = 0.5;
    
    button_SetPref.layer.cornerRadius = 5.0f;
    button_SetPref.backgroundColor = [UIColor convertToHexString:signup_DO.buttonColor];
    [button_SetPref setTitleColor:[UIColor convertToHexString:signup_DO.buttonFontColor] forState:UIControlStateNormal];
    [button_SetPref setAccessibilityLabel:@"setpreferences"];
    button_SetPref.alpha = 0.5;
    
    self.view.backgroundColor = [UIColor convertToHexString:signup_DO.backgroundColor];
    
    //    if(signup_DO.smallLogoPath)
    //    {
    //        AsyncImageView *asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(10, 0, 86, 35)];
    //        asyncImageView.backgroundColor = [UIColor clearColor];
    //        asyncImageView.layer.cornerRadius = 5.0f;
    //        [asyncImageView loadImage:signup_DO.smallLogoPath];
    //
    //        [self.navigationController.navigationBar.topItem setTitleView:asyncImageView];
    //        //[asyncImageView release];
    //    }
    //    else
    //    {
    //        self.navigationItem.title = @"Signup";
    //    }
    
}


#pragma mark - Table view delegates and datasource methods

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    CGRect frame = table_Signup.frame;
    frame.size = table_Signup.contentSize;
    table_Signup.frame = frame;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1)
    {
        return 65;
    }
    
    
    return 55;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        switch (indexPath.row) {
            case 0:{
                [self configureEmailCell:cell];
            }
                break;
            case 1:{
                [self configureEmailAddressCell : cell];
            }
                break;
            case 2:{
                [self configurePasswordCell:cell];
                
                
            }
                break;
            case 3:{
                [self configureConfirmPasswordCell:cell];
                
            }
                break;
            default:{
                assert(FALSE);
            }
                break;
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


/*
 *
 Method to configure first row of table view - Email Address
 *
 */
-(void)configureEmailCell:(UITableViewCell *)cell
{
    int y_origin = (cell.contentView.frame.size.height - LABEL_HEIGHT)/2;
    UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(x_origin, y_origin, 110, LABEL_HEIGHT+x_origin)];
    emailLabel.text = NSLocalizedString(@"User Name", @"User Name");
    emailLabel.font =[UIFont boldSystemFontOfSize:12];
    emailLabel.textColor = [UIColor blackColor];
    emailLabel.backgroundColor = [UIColor clearColor];
    //text for username condition
    UILabel * usernameCondition= [[UILabel alloc] initWithFrame:CGRectMake(x_origin, y_origin + LABEL_HEIGHT+ x_origin - 5 , 110 + VIEW_FRAME_WIDTH-emailLabel.frame.size.width-X_OFFSET*2-5, LABEL_HEIGHT+x_origin - 10)];
    usernameCondition.text = @"User name is not case sensitive.";
    usernameCondition.font = [UIFont boldSystemFontOfSize:10];
    usernameCondition.textColor = [UIColor blackColor];
    usernameCondition.backgroundColor = [UIColor clearColor];
    
    
    txtField_email = [[UITextField alloc] initWithFrame:CGRectMake(x_origin+emailLabel.frame.size.width+X_OFFSET, y_origin, VIEW_FRAME_WIDTH-emailLabel.frame.size.width-X_OFFSET*2-5, TEXTFIELD_HEIGHT)];
    txtField_email.tag = 1;
    txtField_email.autocorrectionType = UITextAutocorrectionTypeNo;
    txtField_email.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtField_email.placeholder = NSLocalizedString(@"User Name",@"User Name");
    [txtField_email setClearButtonMode:UITextFieldViewModeWhileEditing];
    txtField_email.keyboardType = UIKeyboardTypeEmailAddress;
    txtField_email.font = [UIFont fontWithName:@"Helvetica" size:12];
    txtField_email.delegate=self;
    [txtField_email setAccessibilityLabel:@"username"];
    txtField_email.returnKeyType = UIReturnKeyNext;
    [cell.contentView addSubview:emailLabel];
    [cell.contentView addSubview:txtField_email];
    [cell.contentView addSubview:usernameCondition];
    ReleaseAndNilify(emailLabel);
}

/*
 *
 Method to configure second row of table view - Password
 *
 */
-(void)configurePasswordCell:(UITableViewCell *)cell
{
    int y_origin = (cell.contentView.frame.size.height - LABEL_HEIGHT)/2;
    UILabel *pwdLabel = [[UILabel alloc] initWithFrame:CGRectMake(x_origin, y_origin, 110, LABEL_HEIGHT+x_origin)];
    pwdLabel.text = NSLocalizedString(@"Password",@"Password");
    pwdLabel.font = [UIFont boldSystemFontOfSize:12];
    pwdLabel.textColor = [UIColor blackColor];
    pwdLabel.backgroundColor = [UIColor clearColor];
    
    txtField_password = [[UITextField alloc] initWithFrame:CGRectMake(x_origin+pwdLabel.frame.size.width+X_OFFSET, y_origin, VIEW_FRAME_WIDTH-pwdLabel.frame.size.width-X_OFFSET*2-5, TEXTFIELD_HEIGHT)];
    txtField_password.tag = 3;
    txtField_password.secureTextEntry = YES;
    txtField_password.autocorrectionType = UITextAutocorrectionTypeNo;
    txtField_password.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtField_password.placeholder = NSLocalizedString(@"Minimum 6 characters", @"Minimum 6 characters");
    [txtField_password setClearButtonMode:UITextFieldViewModeWhileEditing];
    txtField_password.font = [UIFont fontWithName:@"Helvetica" size:12];
    txtField_password.delegate=self;
    [txtField_password setAccessibilityLabel:@"password"];
    txtField_password.returnKeyType = UIReturnKeyNext;
    [cell.contentView addSubview:pwdLabel];
    [cell.contentView addSubview:txtField_password];
    ReleaseAndNilify(pwdLabel);
}

/*
 *
 Method to configure first row of table view - Confirm Password
 *
 */
-(void)configureConfirmPasswordCell:(UITableViewCell *)cell
{
    int y_origin = (cell.contentView.frame.size.height - LABEL_HEIGHT)/2;
    UILabel *cpwdLabel = [[UILabel alloc] initWithFrame:CGRectMake(x_origin, y_origin, 110, LABEL_HEIGHT+x_origin)];
    cpwdLabel.text = NSLocalizedString(@"Confirm Password",@"Confirm Password");
    cpwdLabel.font = [UIFont boldSystemFontOfSize:12];
    cpwdLabel.textColor = [UIColor blackColor];
    cpwdLabel.backgroundColor = [UIColor clearColor];
    
    txtField_confirmPwd = [[UITextField alloc] initWithFrame:CGRectMake(x_origin+cpwdLabel.frame.size.width+X_OFFSET, y_origin, VIEW_FRAME_WIDTH-cpwdLabel.frame.size.width-X_OFFSET*2-5, TEXTFIELD_HEIGHT)];
    txtField_confirmPwd.tag = 4;
    txtField_confirmPwd.secureTextEntry = YES;
    txtField_confirmPwd.autocorrectionType = UITextAutocorrectionTypeNo;
    txtField_confirmPwd.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtField_confirmPwd.placeholder = NSLocalizedString(@"Retype Password", @"Retype Password");
    [txtField_confirmPwd setClearButtonMode:UITextFieldViewModeWhileEditing];
    txtField_confirmPwd.font = [UIFont fontWithName:@"Helvetica" size:12];
    txtField_confirmPwd.delegate=self;
    txtField_confirmPwd.returnKeyType = UIReturnKeyDone;
    [txtField_confirmPwd setAccessibilityLabel:@"confirmpassword"];
    [cell.contentView addSubview:cpwdLabel];
    [cell.contentView addSubview:txtField_confirmPwd];
    ReleaseAndNilify(cpwdLabel);
}
-(void)configureEmailAddressCell:(UITableViewCell *)cell
{
    int y_origin = (cell.contentView.frame.size.height - LABEL_HEIGHT)/2;
    UILabel *emailAddressLabel = [[UILabel alloc] initWithFrame:CGRectMake(x_origin, y_origin, 110, LABEL_HEIGHT+x_origin)];
    emailAddressLabel.text = NSLocalizedString(@"Email Address", @"Email Address");
    emailAddressLabel.font =[UIFont boldSystemFontOfSize:12];
    emailAddressLabel.textColor = [UIColor blackColor];
    emailAddressLabel.backgroundColor = [UIColor clearColor];
    
    UILabel * emailCondition= [[UILabel alloc] initWithFrame:CGRectMake(x_origin, y_origin + LABEL_HEIGHT, 110 + VIEW_FRAME_WIDTH-emailAddressLabel.frame.size.width-X_OFFSET*2-5, (2*LABEL_HEIGHT)+x_origin - 10)];
    emailCondition.text = @"If you forget your password, we will send you an email to reset your password";
    emailCondition.font = [UIFont boldSystemFontOfSize:10];
    emailCondition.numberOfLines = 2;
    emailCondition.textColor = [UIColor blackColor];
    emailCondition.backgroundColor = [UIColor clearColor];
    
    textField_emailAddress = [[UITextField alloc] initWithFrame:CGRectMake(x_origin+emailAddressLabel.frame.size.width+X_OFFSET, y_origin, VIEW_FRAME_WIDTH-emailAddressLabel.frame.size.width-X_OFFSET*2-5, TEXTFIELD_HEIGHT)];
    textField_emailAddress.tag = 2;
    textField_emailAddress.autocorrectionType = UITextAutocorrectionTypeNo;
    textField_emailAddress.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField_emailAddress.placeholder = NSLocalizedString(@"Optional",@"Optional");
    [textField_emailAddress setClearButtonMode:UITextFieldViewModeWhileEditing];
    textField_emailAddress.keyboardType = UIKeyboardTypeEmailAddress;
    textField_emailAddress.font = [UIFont fontWithName:@"Helvetica" size:12];
    textField_emailAddress.delegate=self;
    [textField_emailAddress setAccessibilityLabel:@"email"];
    textField_emailAddress.returnKeyType = UIReturnKeyNext;
    [cell.contentView addSubview:emailAddressLabel];
    [cell.contentView addSubview:textField_emailAddress];
    [cell.contentView addSubview:emailCondition];
    ReleaseAndNilify(emailAddressLabel);
}


/*
 *
 Perform various validations before saving he details.
 *
 */
-(IBAction)saveBtnPressed:(id)sender
{
    BOOL validEmail = NO;
    
    txtField_email.text = [txtField_email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtField_password.text = [txtField_password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtField_confirmPwd.text = [txtField_confirmPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    textField_emailAddress.text = [textField_emailAddress.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    /************** validate if no fields are blank ****************/
    if (![txtField_email.text length] || ![txtField_password.text length] || ![txtField_confirmPwd.text length])
    {
        [UtilityManager showAlert:nil msg:NSLocalizedString(@"Please enter missing fields", @"Please enter missing fields")];
        return;
    }
    /************** validate if passowrd entered contains atleast 6 characters ****************/
    else if ([txtField_password.text length] < 6)
    {
        [UtilityManager showAlert:nil msg:NSLocalizedString(@"Password must be minimum of 6 characters", @"Password must be minimum of 6 characters")];
        return;
    }
    /************** validate if passowrd and confirm password are same ****************/
    else if(![txtField_password.text isEqualToString:txtField_confirmPwd.text])
    {
        txtField_password.text = @"";
        txtField_confirmPwd.text = @"";
        [UtilityManager showAlert:nil msg:NSLocalizedString(@"Password Mismatch", @"Password Mismatch")];
        return;
    }
    
    
    /************** Continue if all validations are passed ****************/
    else if([txtField_email.text length])
    {
        if ([textField_emailAddress.text length])
        {
            NSString *emailRegex = @"[a-zA-Z0-9.\\-_]{1,32}@[a-zA-Z0-9.\\-_]{2,32}\\.[A-Za-z]{2,4}";
            NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
            validEmail = [regExPredicate evaluateWithObject:textField_emailAddress.text];
            
            if(validEmail == 0){
                [UtilityManager showAlert:nil msg:@"Please enter valid email id"];
                return;
                
            }
        }
        if([Network currentReachabilityStatus]==0){
            
        }
        else {
            
            
            [txtField_password resignFirstResponder];
            [txtField_email resignFirstResponder];
            [txtField_confirmPwd resignFirstResponder];
            [textField_emailAddress resignFirstResponder];
            
            iWebRequestState = SIGNUP_SUCCESSRESPONSE;
            [self requestforSignup];
        }
        
    }
    else
    {
        //show alert from response - registration not successful
    }
    
    
}

-(void)requestforSignup
{
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserDetails><userName><![CDATA[%@]]></userName>",txtField_email.text];
    [xmlStr appendFormat:@"<password><![CDATA[%@]]></password>", txtField_password.text];
    if (textField_emailAddress.text.length!=0)
    {
        [xmlStr appendFormat:@"<email><![CDATA[%@]]></email>",textField_emailAddress.text];
    }
    [xmlStr appendFormat:@"<deviceId>%@</deviceId>", [defaults valueForKey:KEY_DEVICEID]];
    [xmlStr appendFormat:@"<hubCitiKey>%@</hubCitiKey>",HUBCITIKEY];
    
    if(!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [defaults valueForKey:@"LatValue"])
    {
        [xmlStr appendFormat:@"<latitude>%@</latitude>",[defaults valueForKey:@"LatValue"]];
        [xmlStr appendFormat:@"<longitude>%@</longitude>",[defaults valueForKey:@"LongValue"]];
    }
    
    [xmlStr appendFormat:@"<appVersion>%@</appVersion></UserDetails>",AppVersion];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/signup",BASE_URL];
    
    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    
    ReleaseAndNilify(xmlStr);
    
}

-(void)parseRegistrationResponseData:(NSString *) responseXml
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    int responseCode = [[TBXML textForElement:responseCodeElement]intValue];
    
    if (responseCode == 10000)
    {
        TBXMLElement *userIdElement = [TBXML childElementNamed:KEY_USERID parentElement:tbXml.rootXMLElement];
        TBXMLElement *hubCitiNameElement = [TBXML childElementNamed:KEY_HUBCITINAME parentElement:tbXml.rootXMLElement];
        TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:KEY_HUBCITIID parentElement:tbXml.rootXMLElement];
        
        if(hubCitiIdElement)
            [defaults  setObject:[TBXML textForElement:hubCitiIdElement] forKey:KEY_HUBCITIID];
        if(userIdElement)
            [defaults  setObject:[TBXML textForElement:userIdElement] forKey:KEY_USERID];
        if(hubCitiNameElement)
            [defaults  setObject:[TBXML textForElement:hubCitiNameElement] forKey:KEY_HUBCITINAME];
        
        regSuccessUI_DO = [[SignupDO alloc]init];
        
        // Set Default value if No Data will come from Server it will display the given UI Colors
        regSuccessUI_DO.fontColor = @"#ffffff";
        regSuccessUI_DO.backgroundColor  = @"#C0C0C0";
        regSuccessUI_DO.buttonFontColor = @"#ffffff";
        regSuccessUI_DO.buttonColor = @"#000000";
        
        TBXMLElement *regUIElement = [TBXML childElementNamed:@"regSuccessUI" parentElement:tbXml.rootXMLElement];
        if(regUIElement)
        {
            TBXMLElement *fontColorElement = [TBXML childElementNamed:@"fontColor" parentElement:regUIElement];
            TBXMLElement *bkgndColorElement = [TBXML childElementNamed:@"bkgndColor" parentElement:regUIElement];
            TBXMLElement *buttonFontColorElement = [TBXML childElementNamed:@"btnFontColor" parentElement:regUIElement];
            TBXMLElement *buttonColorElement = [TBXML childElementNamed:@"btnColor" parentElement:regUIElement];
            TBXMLElement *logoImagePathElement = [TBXML childElementNamed:@"logoImg" parentElement:regUIElement];
            TBXMLElement *descriptionElement = [TBXML childElementNamed:@"description" parentElement:regUIElement];
            TBXMLElement *TitlePathElement = [TBXML childElementNamed:@"title" parentElement:regUIElement];
            
            if (fontColorElement != NULL) {
                regSuccessUI_DO.fontColor = [TBXML textForElement:fontColorElement];
            }
            if (bkgndColorElement != NULL) {
                regSuccessUI_DO.backgroundColor = [TBXML textForElement:bkgndColorElement];
            }
            if (buttonFontColorElement != NULL) {
                regSuccessUI_DO.buttonFontColor = [TBXML textForElement:buttonFontColorElement];
            }
            if (buttonColorElement != NULL) {
                regSuccessUI_DO.buttonColor = [TBXML textForElement:buttonColorElement];
            }
            if (logoImagePathElement != NULL) {
                regSuccessUI_DO.logoImagePath = [TBXML textForElement:logoImagePathElement];
            }
            if (descriptionElement != NULL) {
                regSuccessUI_DO.description = [TBXML textForElement:descriptionElement];
            }
            if(TitlePathElement)
                [self displaySuccessView:regSuccessUI_DO title:[TBXML textForElement:TitlePathElement]];
            else
                [self displaySuccessView:regSuccessUI_DO title:@" "];
            
            //after registering new user, replace stored username and password for earlier user with new user
            [defaults setValue:txtField_email.text forKey:@"Username"];
            [defaults setValue:txtField_password.text forKey:@"Password"];
            
            LoginViewController *lvc = [[LoginViewController alloc]init];
            lvc.txtField_userName.text = txtField_email.text;
            lvc.txtField_password.text = txtField_password.text;
            
            [defaults setBool:YES forKey:@"rememberMe"];
            
            //also reset user location settings so that on going to main menu it will ask popup
//            [SharedManager setGps_allow_flag:NO];
//            [defaults setValue:@"NO" forKey:@"allowingGPS"];
            
            [defaults setValue:nil forKey:KEYZIPCODE];
            
        }
        else
            [self displaySuccessView:regSuccessUI_DO title:@" "];
    }
    else
    {
        TBXMLElement *responseTextElement= [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
    }
}

/*
 *
 Display registration Success message on successful registration
 Hide the table view on displaying the message.
 *
 */
-(void)displaySuccessView:(SignupDO*)successUIDO title:(NSString*)successTitle
{
    
    table_Signup.hidden = YES;
    self.navigationItem.leftBarButtonItem = nil;
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
   // cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];

    //added a space to leftBarButtonItem in order to display logo at center
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    NSArray *items = [NSArray arrayWithObjects:fixedSpace, nil];
    self.navigationItem.leftBarButtonItems = items;
    // [fixedSpace release];
    
    
    CGRect labelFrame;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        labelFrame = CGRectMake(X_OFFSET,X_OFFSET*2,VIEW_FRAME_WIDTH-(X_OFFSET*2), LABEL_HEIGHT*2);
    }
    else
    {
        labelFrame = CGRectMake(X_OFFSET,X_OFFSET*2,VIEW_FRAME_WIDTH-(X_OFFSET*2), IPAD_LABEL_HEIGHT*2);
        
    }
    
    
    UILabel *lblRegSuccess = [[UILabel alloc]initWithFrame:labelFrame];
    lblRegSuccess.backgroundColor = [UIColor clearColor];
    lblRegSuccess.textAlignment = NSTextAlignmentCenter;
    lblRegSuccess.textColor = [UIColor convertToHexString:successUIDO.fontColor];
    lblRegSuccess.font = [UIFont systemFontOfSize:20];
    lblRegSuccess.text = successTitle;
    lblRegSuccess.shadowColor = [UIColor colorWithWhite:0.7 alpha:1];
    lblRegSuccess.shadowOffset = CGSizeMake(1, 2);
    
    UITextView *descpView ;
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        descpView= [[UITextView alloc]initWithFrame:CGRectMake(X_OFFSET,X_OFFSET*3+lblRegSuccess.frame.size.height,lblRegSuccess.frame.size.width, 150)];
    }
    else
    {
        // descpView= [[UITextView alloc]initWithFrame:CGRectMake(X_OFFSET,X_OFFSET*3+lblRegSuccess.frame.size.height,lblRegSuccess.frame.size.width, 250)];
        descpView= [[UITextView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/4,SCREEN_HEIGHT/8,SCREEN_WIDTH/2, 250)];
    }
    
    
    
    descpView.font = [UIFont systemFontOfSize:14];
    descpView.backgroundColor = [UIColor clearColor];
    descpView.editable = NO;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        descpView.textAlignment=NSTextAlignmentCenter;
    }
    
    descpView.textColor = [UIColor convertToHexString:successUIDO.fontColor];
    descpView.text = successUIDO.description;
    
    [self.view addSubview:lblRegSuccess];
    [self.view addSubview:descpView];
    // [self.view addSubview:viewRegSuccess];
    
    
    ReleaseAndNilify(lblRegSuccess);
    ReleaseAndNilify(descpView);
    
    button_SetPref.enabled = YES;
    button_GetStarted.enabled = YES;
    [button_Save removeFromSuperview];
    
    button_GetStarted.alpha = 1.0;
    button_GetStarted.backgroundColor = [UIColor convertToHexString:successUIDO.buttonColor];
    [button_GetStarted setTitleColor:[UIColor convertToHexString:successUIDO.buttonFontColor] forState:UIControlStateNormal];
    
    button_SetPref.alpha = 1.0;
    button_SetPref.backgroundColor = [UIColor convertToHexString:successUIDO.buttonColor];
    [button_SetPref setTitleColor:[UIColor convertToHexString:successUIDO.buttonFontColor] forState:UIControlStateNormal];
    
    self.view.backgroundColor = [UIColor convertToHexString:successUIDO.backgroundColor];
    
    [defaults setValue:signup_DO.buttonColor forKey:@"KEY_BUTTONCOLOR"];
    [defaults setValue:signup_DO.buttonFontColor forKey:@"KEY_BUTTONFONTCOLOR"];
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        [self addSuccessConstraints];
    }
}

-(void)addSuccessConstraints
{
    button_SetPref.translatesAutoresizingMaskIntoConstraints=NO;
    button_GetStarted.translatesAutoresizingMaskIntoConstraints=NO;
    
    NSDictionary *viewsDictionary = @{@"start":button_GetStarted,@"pref":button_SetPref};
    
    [self.view addConstraints: [NSLayoutConstraint
                                constraintsWithVisualFormat:@"V:|-450-[pref]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary]];
    
    [self.view addConstraints: [NSLayoutConstraint
                                constraintsWithVisualFormat:@"V:|-600-[start]"                                                                                  options:0                                                                                  metrics:nil                                                                                    views:viewsDictionary]];
    
}
-(void)responseData:(NSString *)response
{
    switch (iWebRequestState)
    {
        case SIGNUP_SUCCESSRESPONSE:
            [self parseRegistrationResponseData:response];
            break;
            
        case GETUSERINFO:
            [self parseGetUserData:response];
            break;
        case FetchCityPreferences:
            [self parse_FetchCityPreferences:response];
            break;
        default:
            break;
    }
    ;
}



/*
 *
 validation method for email address format
 *
 */
-(BOOL)validateEmailAddress
{
    BOOL validEmail = NO;
    if ([txtField_email.text length]) {
        
        NSString *emailRegex = @"[a-zA-Z0-9.\\-_]{1,32}@[a-zA-Z0-9.\\-_]{2,32}\\.[A-Za-z]{2,4}";
        NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        validEmail = [regExPredicate evaluateWithObject:txtField_email.text];
    }
    return validEmail;
}


-(IBAction)setPrefPressed:(id)sender
{
    [SharedManager setUserInfoFromSignup:YES];
    
    //call view to enter user information based on server response
    iWebRequestState = GETUSERINFO;;
    [self requestToGetUserData];
}

/*
 *
 send request with data to get user details.
 request sent when navigated from settings screen.
 *
 */
-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    //   // [xmlStr appendFormat:@"<UserDetails><userId>%d</userId><hubCitiId>%d</hubCitiId></UserDetails>", 17075,9257];
    //     //NSString *urlString = [NSString stringWithFormat:@"http://10.10.222.62:8080/HubCiti2.3.3/firstuse/getuserinfo"];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}


/*
 *
 Parsing logic when response received for getting user details.
 Method called when navigated from Settings screen.
 *
 */
-(void)parseGetUserData:(id)response
{
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        [SharedManager setUserInfoFromSignup:YES];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
    
    
    //    TBXML *tbxml = [[TBXML tbxmlWithXMLString:response] retain];
    //	TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    //    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    //    {
    //		[defaults  setObject:response forKey:KEY_RESPONSEXML];
    //        [SharedManager setUserInfoFromSignup:YES];
    //		UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
    //		[self.navigationController pushViewController:inform animated:NO];
    //		[inform release];
    //	}
    //	else {
    //        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    //
    //		NSString *responseTextStr = [TBXML textForElement:saveResponseText];
    //		[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
    //
    //	}
    //
    //    ReleaseAndNilify(tbxml);
}


-(void)parse_FetchCityPreferences : (NSString *)response
{
    NSMutableArray*cityIDS = [[NSMutableArray alloc] init];
    NSMutableArray*sortedCitiIDs = [[NSMutableArray alloc] init];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]){
        
        TBXMLElement *cityListElement = [TBXML childElementNamed:@"cityList" parentElement:tbxml.rootXMLElement];
        
        NSLog(@"%@",[TBXML textForElement:tbxml.rootXMLElement]);
        
        NSLog(@"%@",[TBXML textForElement:cityListElement]);
        
        TBXMLElement *cityDetailsElement = [TBXML childElementNamed:@"City" parentElement:cityListElement];
        
        while (cityDetailsElement!=nil) {
            
            TBXMLElement *cityIdElement = [TBXML childElementNamed:@"cityId" parentElement:cityDetailsElement];
            
            if (cityIdElement!=nil) {
                [cityIDS addObject: [TBXML textForElement:cityIdElement]];
                
            }
            
            
            cityDetailsElement = [TBXML nextSiblingNamed:@"City" searchFromElement: cityDetailsElement];
        }
        
        NSMutableString *cities=[[NSMutableString alloc]init];
        NSMutableString *sortCities=[[NSMutableString alloc]init];
        
        for (int i=0; i< [cityIDS count]; i++) {
            
            [cities appendFormat:@"%@,",[cityIDS objectAtIndex:i]];
            [sortedCitiIDs addObject:[cityIDS objectAtIndex:i]];
            
        }
        
        if ([cities hasSuffix:@","]) {
            [cities  setString:[cities substringToIndex:[cities length]-1]];
        }
        
        //changed for server caching
        
        [sortedCitiIDs sortUsingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
            return [str1 compare:str2 options:(NSNumericSearch)];
        }];
        for (int i=0; i<[sortedCitiIDs count]; i++) {
            [sortCities appendFormat:@"%@,",[sortedCitiIDs objectAtIndex:i]];
            
        }
        
        if ([sortCities hasSuffix:@","]) {
            [sortCities  setString:[sortCities substringToIndex:[sortCities length]-1]];
        }
        
        [defaults setObject:sortCities forKey:@"sortedCitiIDs"];
        
        // call main menu
        [SharedManager setUserInfoFromSignup:NO];
        NSString* customerId = [defaults objectForKey:@"currentuserId"];
        if (customerId) {
            
            if (![customerId isEqualToString:[defaults valueForKey:KEY_USERID]]) {
                
                ReleaseAndNilify(cashedResponse);
                [defaults setValue: nil forKey: @"currentuserId"];
            }
            else {
                isSameUser = true;
            }
        }
        [defaults setBool:YES forKey:@"LoginUserNews"];
        
        if([defaults boolForKey:@"newsTemplateExist"]){
            
            [NewsUtility pushViewFromHamberger:self];
            
            
        }
        else{
            
           [NewsUtility pushMainmenu:self];
            
        }
    }
    else
    {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    
    
}


-(void)requestCityPrefrences
{
    iWebRequestState = FetchCityPreferences;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    if ([SharedManager userInfoFromSignup] == YES)
    {
        [requestStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    }
    else
    {
        [requestStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId><userId>%@</userId>",[defaults valueForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_USERID]];
    }
    [requestStr appendFormat:@"</UserDetails>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getcitypref",BASE_URL];
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    
}


-(IBAction)getStartedPressed:(id)sender
{
    [NewsUtility setUserSettings];
    if ([RegionApp isEqualToString:@"1"]) {
        [self requestCityPrefrences];
    }
    else{
        [SharedManager setUserInfoFromSignup:NO];
        
        NSString* customerId = [defaults objectForKey:@"currentuserId"];
        if (customerId) {
            
            if (![customerId isEqualToString:[defaults valueForKey:KEY_USERID]]) {
                
                ReleaseAndNilify(cashedResponse);
                [defaults setValue: nil forKey: @"currentuserId"];
            }
            else {
                isSameUser = true;
            }
        }
        [defaults setBool:YES forKey:@"LoginUserNews"];
        if([defaults boolForKey:@"newsTemplateExist"]){
            
            [NewsUtility pushViewFromHamberger:self];
            
            
        }
        else{
            
            [NewsUtility pushMainmenu:self];
        }
        
    }
    
    //[imainPageViewController release];
}

/************** Navigate back to previous page *************/
-(void)popBackToPreviousPage{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    
    [self.navigationController popViewControllerAnimated:NO];
    
    
}
#pragma mark - Service Respone methods
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    [self responseData:responseObject];
    
}

#pragma mark - Textfield delegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    //[textField resignFirstResponder];
    
    NSInteger nextTag = textField.tag + 1;
    //find next responder
    UIResponder *nextResponder = [table_Signup viewWithTag:nextTag];
    
    if (nextResponder){
        //if there is next responder, set it
        [nextResponder becomeFirstResponder];
    }
    
    else{
        [textField resignFirstResponder];
    }
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  FAQQuestionsList.m
//  HubCiti
//
//  Created by Ajit Nadig on 3/20/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import "FAQQuestionsList.h"
#import "FAQCategoryList.h"
#import "FaqDetailsDo.h"
#import "MainMenuViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

static bool selectedFlag=TRUE;//to keep track of selected row

@implementation FAQDO

@synthesize faqId;
@synthesize  question;

@end

@interface FAQQuestionsList ()

@end

@implementation FAQQuestionsList{
    UIActivityIndicatorView *loading;
}
@synthesize answerArray;


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=[defaults valueForKey:@"title"];
    [self request_faqdisplay:0];
    
    faqObjArray = [[NSMutableArray alloc]init];
    answerArray=[[NSMutableArray alloc]init];
    loading = nil;
    
    selectedRow=0;
    
    //customize back button and main menu button displayes on the navigation bar.
   
    self.navigationItem.hidesBackButton = YES;
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    
    /***** Logic to prevent View from expanding for iOS7 version *******/
    if([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [faqTableView reloadData];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
 *
 Navigate back to Main Menu screen page
 *
 */-(void)returnToMainPage:(id)sender
{
    //return to main page
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


/*
 Navigate back to previous page
 */
-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self.navigationController popViewControllerAnimated:NO];
}



#pragma mark - request methods
-(void)request_faqdetails:(int)selfaqId{
    
    
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<FAQDetails><hubCitiId>%@</hubCitiId><faqId>%d</faqId>",[defaults valueForKey:KEY_HUBCITIID],selfaqId];
    
    [reqStr appendFormat:@"</FAQDetails>"];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@firstuse/faqdetails",BASE_URL];
    
    NSString *responseXml=[ConnectionManager establishPostConnection:urlStr withParam:reqStr];
    
    [self parse_faqdetails:responseXml];
}

-(void)request_faqdisplay:(int)lastRecord{
    
    
    iWebServiceType = faqdisplay;
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<FAQDetails><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    [reqStr appendFormat:@"<categoryId>%@</categoryId></FAQDetails>",[defaults valueForKey:@"faqCatId"]];
    //<lowerLimit>0</lowerLimit>
    //search is not reqd as per customer req - if reqd add search bar
    /* if ([searchBar.text length]){
     [reqStr appendFormat:@"<searchKey>%@</searchKey>",searchBar.text];
     }*/
    
    NSString *urlStr = [NSString stringWithFormat:@"%@firstuse/faqdisplay",BASE_URL];
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlStr withParam:reqStr];
        
        
        [self parse_faqdisplay:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    }
    
    //[ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

#pragma mark - parse methods

-(void)responseData:(NSString*)response{
    
    switch (iWebServiceType) {
        case faqdisplay:
            [self parse_faqdisplay:response];
            break;
        default:
            break;
    }
}

-(void)parse_faqdetails:(NSString*)response{
    
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    
    
    
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        
        TBXMLElement *answerElement = [TBXML childElementNamed:@"answer" parentElement:tbxml.rootXMLElement];
        answer =[TBXML textForElement:answerElement];
        DLog(@"%@",[answerArray objectAtIndex:0]);
        [answerArray removeObjectAtIndex:0];
        [answerArray insertObject:answer atIndex:0];
        DLog(@"%@",[answerArray objectAtIndex:0]);
    }
    
    else{
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
    }
    
    [faqTableView reloadData];
}

-(void)parse_faqdisplay:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    
    
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        
        TBXMLElement *nextPageFlagElement = [TBXML childElementNamed:@"nextPageFlag" parentElement:tbxml.rootXMLElement];
        nextPage = [[TBXML textForElement:nextPageFlagElement]intValue];
        
        TBXMLElement *maxRowNumElement = [TBXML childElementNamed:@"maxRowNum" parentElement:tbxml.rootXMLElement];
        lastRecord = [[TBXML textForElement:maxRowNumElement]intValue];
        
        TBXMLElement *FAQDetailsElement = [TBXML childElementNamed:@"FAQDetails" parentElement:tbxml.rootXMLElement];
        
        while (FAQDetailsElement!=nil)
        {
            
            FAQDO *faqObj = [[FAQDO alloc]init];
            
            TBXMLElement *faqIdElement = [TBXML childElementNamed:@"faqId" parentElement:FAQDetailsElement];
            TBXMLElement *questionElement = [TBXML childElementNamed:@"question" parentElement:FAQDetailsElement];
            TBXMLElement *answerElement = [TBXML childElementNamed:@"answer" parentElement:FAQDetailsElement];
            if(answerElement!=nil){
                answer=[TBXML textForElement:answerElement];
                if(selectedRow==0){
                    [answerArray insertObject:answer atIndex:0];
                    DLog(@"%@",[answerArray objectAtIndex:0]);
                }
            }
            
            faqObj.faqId = [[TBXML textForElement:faqIdElement]intValue];
            faqObj.question = [TBXML textForElement:questionElement];
            
            [faqObjArray addObject:faqObj];
            //  [faqObj release];
            
            FAQDetailsElement = [TBXML nextSiblingNamed:@"FAQDetails" searchFromElement:FAQDetailsElement];
            
        }
    }
    
    
    else{
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        ReleaseAndNilify(loading);
        
    }
    [faqTableView reloadData];
    [defaults setBool:NO forKey:@"ViewMore"];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


//used to calculate the number of lines in question,such that no of character in a line is 40.
-(int)calculateNoOfLabelTextLines:(NSUInteger)rowNumber
{
    int x;
    FAQDO *faqObjForIndexPath=[faqObjArray objectAtIndex:rowNumber];
    int characters = (int)[faqObjForIndexPath.question length];
    
    x= characters/40;
    if ((characters%40)==0) {
        return x;
    }
    else
    {
        return x+1;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int noOfLines = [self calculateNoOfLabelTextLines:indexPath.row];
    
    if (selectedFlag == TRUE && indexPath.row ==selectedRow)
    {
        if (noOfLines <=3) {
            return EXTENDED_HEIGHT;
        }
        else
        {
            return ((LABEL_HEIGHT_OFFSET* noOfLines)+TEXTVIEW_HEIGHT+3);
        }
    }
    else
    {
        if (noOfLines <=3) {
            return NORMAL_HEIGHT;
        }
        else
        {
            return (LABEL_HEIGHT_OFFSET * noOfLines);
        }
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]init];
    
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if((nextPage == 0) && (lastRecord != 0))
    {
        return [faqObjArray count];
    }
    
    else if(((nextPage == 1)|| (lastRecord != 0)))
    {
        DLog(@"%lu",(unsigned long)[faqObjArray count]);
        return [faqObjArray count]+1;
        
    }
    
    else
        return [faqObjArray count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.backgroundColor =  [UIColor clearColor];
   // float labelFont = 16.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
       // labelFont = 20.0;
    }
    if(nextPage==1 && indexPath.row == [faqObjArray count] )
    {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
        //        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 16, SCREEN_WIDTH, 24)];
        //        label.font = [UIFont boldSystemFontOfSize:labelFont];
        //        label.textAlignment = NSTextAlignmentCenter;
        //        label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
        //        [cell.contentView addSubview:label];
        //        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    else if(([faqObjArray count]>indexPath.row))
    {
        
        if(selectedFlag == true && selectedRow == indexPath.row)
        {
            
            FAQDO *faqObjForIndexPath=[faqObjArray objectAtIndex:indexPath.row];
            int noOfLines = [self calculateNoOfLabelTextLines:indexPath.row];//returns num of lines in question
            UILabel *lbl;
            if (noOfLines <=3) {
                lbl = [[UILabel alloc]initWithFrame:CGRectMake(8, 2, tableView.frame.size.width-WIDTH_OFFSET, NORMAL_HEIGHT)];
            }
            else
            {
                lbl = [[UILabel alloc]initWithFrame:CGRectMake(8, 2, tableView.frame.size.width-WIDTH_OFFSET, (LABEL_HEIGHT_OFFSET*noOfLines))];
            }
            lbl.numberOfLines = noOfLines;
            [lbl setText:faqObjForIndexPath.question];
            [lbl setBackgroundColor:[UIColor clearColor]];
            [lbl setFont:[UIFont boldSystemFontOfSize:16.0]];
            [lbl setLineBreakMode:NSLineBreakByWordWrapping];
            [cell.contentView addSubview:lbl];
            
            cellTextView = [[UITextView alloc]initWithFrame:CGRectMake(8, lbl.frame.origin.y+lbl.frame.size.height,tableView.frame.size.width-15.0 ,TEXTVIEW_HEIGHT)];
            cellTextView.font = [UIFont systemFontOfSize:16.0];
            [cellTextView setBackgroundColor:[UIColor clearColor]];
            cellTextView.textAlignment = NSTextAlignmentJustified;
            DLog(@"%@",[answerArray objectAtIndex:0]);
            cellTextView.text = [answerArray objectAtIndex:0];
            cellTextView.editable = NO;
            [cell addSubview:cellTextView];
            
        }
        
        else {
            FAQDO *faqObjForIndexPath=[faqObjArray objectAtIndex:indexPath.row];
            int noOfLines = [self calculateNoOfLabelTextLines:indexPath.row];
            UILabel *lbl;
            if (noOfLines <=3) {
                lbl = [[UILabel alloc]initWithFrame:CGRectMake(8, 2, tableView.frame.size.width-WIDTH_OFFSET, NORMAL_HEIGHT)];
            }
            else
            {
                lbl = [[UILabel alloc]initWithFrame:CGRectMake(8, 2, tableView.frame.size.width-WIDTH_OFFSET, (LABEL_HEIGHT_OFFSET*noOfLines))];
            }
            lbl.numberOfLines = noOfLines;
            [lbl setText:faqObjForIndexPath.question];
            [lbl setBackgroundColor:[UIColor clearColor]];
            [lbl setFont:[UIFont boldSystemFontOfSize:16.0]];
            [lbl setLineBreakMode:NSLineBreakByWordWrapping];
            [cell.contentView addSubview:lbl];
        }
        
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(nextPage==1 && indexPath.row == [faqObjArray count]){
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"] && lastRecord!=0) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self request_faqdisplay:lastRecord];
                [faqTableView deselectRowAtIndexPath:[faqTableView indexPathForSelectedRow] animated:YES];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [faqTableView reloadData];
                });
            });
            
        }
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [defaults setBool:NO forKey:@"ViewMore"];
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    
    else
    {
        
        //        if(nextPage==1 && indexPath.row == [faqObjArray count])
        //        {
        //            [self request_faqdisplay:lastRecord];
        //            [faqTableView deselectRowAtIndexPath:[faqTableView indexPathForSelectedRow] animated:YES];
        //            return;
        //        }
        
        //else
        //{
        
        selectedFlag = TRUE;
        selectedRow=(int)indexPath.row;
        FAQDO *faqObjForIndexPath=[faqObjArray objectAtIndex:indexPath.row];
        selectedfaqId=faqObjForIndexPath.faqId;
        DLog(@"%d",selectedfaqId);
        [self request_faqdetails:selectedfaqId];
        
        //}
    }
    
    [faqTableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end


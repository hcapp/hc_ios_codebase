//
//  TutorialChildViewController.h
//  HubCiti
//
//  Created by service on 8/17/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnythingPage.h"
@interface TutorialChildViewController : UIViewController
@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) NSMutableArray *imageArray,*typeArr,*isAnythingPageOrWeblink,*isAnythingPageOrWeblinkValue;
@property (strong, nonatomic) UIButton* skip,*signup;
@end

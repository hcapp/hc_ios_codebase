//
//  UserInformationViewController.h
//  HubCiti
//
//  Created by Anjana on 9/11/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

/*********************************************************
 Class is for user to enter his details while registration
 Includes various validations for all the fields .
 Class can be called while Signup or from App Settings.
 *********************************************************/

#import <UIKit/UIKit.h>

@interface UserInfoDO : NSObject{
    
    NSString *position, *fieldName, *value;
   
}
@property (nonatomic, strong)NSString *position, *fieldName, *value;

@end

@interface UIResponder (FirstResponder)

+(id)currentFirstResponder;
-(void)findFirstResponder:(id)sender;

@end

@interface UserInformationViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,HubCitiConnectionManagerDelegate,UIPickerViewDelegate,CustomizedNavControllerDelegate>
{
    IBOutlet UITableView *table_UserInfo;
    
    //UI controls for Change Password view
    IBOutlet UIView *cpasswordView;
    IBOutlet UITextField *txtField_ChangePwd,*txtField_ConfirmPwd;
    IBOutlet UILabel *errorText;
    IBOutlet UIButton *updateButton;
    
    IBOutlet UIBarButtonItem *doneSpaceBar;
    UIButton *maleButton;
    UIButton *femaleButton;
    UIBarButtonItem *customBarItem;
    UIToolbar *bottomToolBar;
    
    NSIndexPath *index_Path;            //To keep the selected row to resize the table
    
    WebRequestState iWebRequestState;  //create enum
    
    NSMutableArray *userInfoObjArray; //to hold user info screen fields
    NSMutableArray *eduLevelIdArray, *eduLevelNameArray;
    NSMutableArray *marStatusIdArray, *marStatusNameArray;
    NSMutableArray *incRangeIdArray, *incRangeNameArray;
    CustomizedNavController *cusNav;
    
    IBOutlet UIView *view_Picker;
    IBOutlet UIPickerView *picker;
    IBOutlet UIToolbar *pickerToolbar;
    
    
    //in case of marital status field
    int selMarStatusId;
    UILabel *marStatusLabel;
    
    //in case of education level field
    int selEduLevelId;
    UILabel *eduLevelLabel;
    
    //in case of income range field
    int selIncRangeId;
    UILabel *incRangeLabel;
    
    //added by Keshava.
    //added for UIDatePicker
    UILabel *dobLabel;
    UIView *dobHolder;
    UIDatePicker *dobPicker;
    NSString *selectedDOB,*serverDOB;
    UIToolbar *dobToolBar;
    
    
    
    BOOL replyFromServer;
    //    IBOutlet UIView  *view_Date;
    //   IBOutlet UIDatePicker *dobPicker;
    //    IBOutlet UIToolbar *dateToolBar;
    
    NSMutableString *newFirstName;
    NSMutableString *newLastName;
    NSMutableString *newEmail;
    NSMutableString *newZipCode;
    NSMutableString *newMobile;
    
    BOOL firstNameFlag,lastNameFlag,emailFlag,zipFlag,mobileFlag;
    
    
    CGFloat animatedDistance;
}

-(IBAction)changePassword:(id)sender;
-(IBAction)closeView:(id)sender;
-(IBAction)updateClicked:(id)sender;
-(IBAction)pickerDoneTapped:(id)sender;
@end

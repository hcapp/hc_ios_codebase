//
//  MakeRequestDetailViewController.h
//  HubCiti
//
//  Created by Bindu M on 9/2/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "CommonUtility.h"
@class AnyViewController;
@class EmailShareViewController;
@interface makeRequestDetailDO : NSObject{
    NSMutableArray *makeRequestDetailArray;
    NSString *fldName;
    NSNumber *fldNum;
    NSString *fldPrompt;
    BOOL fldRequired;
    
}

@property (nonatomic, strong) NSMutableArray *makeRequestDetailArray;
@property (nonatomic, strong) NSString *fldName;
@property (nonatomic, strong) NSNumber *fldNum;
@property (nonatomic, strong) NSString *fldPrompt;
@property (nonatomic, assign) BOOL fldRequired;


@end



@interface MakeRequestDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate,UIScrollViewDelegate,UITextViewDelegate,MFMessageComposeViewControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>{
    CustomizedNavController *cusNav;
    WebRequestState iWebRequestState;
    CommonUtility *common;
    BOOL isRemember;
    
    NSMutableArray *ReqDetailArray;
    UITableView *detailReqTblView;
    NSString *pageTitle;
    NSMutableString *descNote;
    NSNumber *reqTypeID;
    NSMutableArray *arrayBottomButtonID;
     NSMutableArray *arrBottomButtonViewContainer;
    UIButton *submitBtn;
    UIButton *cancelBtn;
    UIPickerView *statePicker;
    UIButton *pickerBtn;
    UIView *doneView;
    NSMutableArray *pickerArrData;
    
   
 
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic, strong) NSMutableArray *ReqDetailArray;
@property (nonatomic, strong) NSString *pageTitle;
@property (nonatomic, strong) NSMutableString *descNote;
@property (nonatomic, strong) id customReqResponse;
@property (nonatomic, strong) NSNumber *reqTypeID;
@property (nonatomic, strong) NSMutableArray *arrayBottomButtonID;
@property (nonatomic, strong) NSString *fName;
@property (nonatomic, strong) NSString *lName;
@property (nonatomic, strong) NSString *phoneNum;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *zip;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *customerAddress;



@end

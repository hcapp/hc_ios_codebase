//
//  FindNearByRetailerDetails.m
//  Scansee
//
//  Created by Sandeep S on 11/17/11.
//  Copyright 2011 span infotech india ptv. ltd. All rights reserved.
//

#import "FindNearByRetailerDetails.h"
#import "MainMenuViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "RateShareViewController.h"
//Chance's Edits

//End Chance's Edits
#import "AppDelegate.h"
#import "WebBrowserViewController.h"
#import "FindNearByRetailerMap.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@implementation FindNearByRetailerDetails

//Chance's Edits
-(void) drawImageInfo
{
    float lablel1Font = 14.0, lable2Font = 12.0;;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        lablel1Font = 19.0;
        lable2Font = 17.0;
    }
    SdImageView *asyncImageView = [[SdImageView alloc] init] ;
    asyncImageView.contentMode = UIViewContentModeScaleToFill;
    asyncImageView.backgroundColor = [UIColor clearColor];
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(65, 10, SCREEN_WIDTH - 70, 40)] ;//250 width
    label1.backgroundColor = [UIColor clearColor];
    label1.textColor = [UIColor blackColor];
    label1.font = [UIFont systemFontOfSize:lablel1Font];
    label1.numberOfLines = 2;
    [label1 setLineBreakMode:NSLineBreakByWordWrapping];
    
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(65, 47, SCREEN_WIDTH - 70, 15)] ;//250 width
    label2.backgroundColor = [UIColor clearColor];
    label2.textColor = [UIColor grayColor];
    label2.font = [UIFont systemFontOfSize:lable2Font];
    label2.numberOfLines = 2;
    [label2 setLineBreakMode:NSLineBreakByWordWrapping];
    
    
    label1.text = [defaults  objectForKey:KEY_PRODUCTNAME];
    label2.text = [defaults  objectForKey:@"salePrice"];
    [self.view addSubview:label1];
    
    if (![label2.text isEqualToString:@"N/A"])
        [self.view addSubview:label2];
    
    if([[HubCitiManager sharedManager]shareFromTL] == YES){
        NSString *str =[NSString stringWithFormat:@"%@",[defaults valueForKey:@"PRODIMG"]];
        asyncImageView.frame = CGRectMake(10, 10, 50, 50);
        [asyncImageView loadImage:str];
        [self.view addSubview:asyncImageView];
    }
    
}
//End Chance's Edits

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    
    //Resetting GPS flag if user has turned it on
    
    //if ([CLLocationManager  authorizationStatus] != kCLAuthorizationStatusDenied) {
    //    [defaults  setObject:@"YES" forKey:@"allowingGPS"];
    //}
    //Chance's Edits
    //[self drawImageInfo];
    //end Chance's Edits
    
    float retailerNameFont = 22.0, distanceLabelFont = 12.0, minDistanceLabelFont = 9.0;
    float tableViewHeight = 380.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        
        retailerNameFont = 27.0;
        distanceLabelFont = 17.0;
        minDistanceLabelFont = 14.0;
        tableViewHeight = SCREEN_HEIGHT - 60.0 - NAVIGATION_HEIGHT - STATUSBAR_HEIGHT;
    }
    
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:[defaults valueForKey:KEY_RTLNAME] forView:self withHambergur:NO];
    
    
    UILabel *retailerName = [[UILabel alloc]initWithFrame:CGRectMake(50.0, 0.0, SCREEN_WIDTH - 120, 60.0)];//200 width
    retailerName.font = [UIFont boldSystemFontOfSize:retailerNameFont];
    retailerName.textAlignment = NSTextAlignmentCenter;
    [retailerName setBackgroundColor:[UIColor clearColor]];
    retailerName.shadowColor = [UIColor grayColor];
    retailerName.shadowOffset = CGSizeMake(1,1);
    retailerName.lineBreakMode = NSLineBreakByWordWrapping;
    retailerName.numberOfLines = 2;
    retailerName.text = [defaults valueForKey:KEY_RTLNAME];
    [self.view addSubview:retailerName];
    
    UILabel *lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0, retailerName.frame.origin.y+retailerName.frame.size.height+1, SCREEN_WIDTH, 1.0)];
    lineLabel.backgroundColor = [UIColor blackColor];
    [self.view addSubview:lineLabel];
    //  [lineLabel release];
    
    UIImageView *pinImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 30, 0.0, 32.0, 39.0)];//290 Xvalue
    pinImage.image = [UIImage imageNamed:@"PinDown.png"];
    pinImage.contentMode = UIViewContentModeCenter;
    pinImage.backgroundColor = [UIColor clearColor];
    [self.view addSubview:pinImage];
    
    UILabel *distLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 70, pinImage.frame.origin.y+pinImage.frame.size.height, 70.0, 20.0)];//280.0 Xvalue
    distLabel.backgroundColor = [UIColor clearColor];
    if([[HubCitiAppDelegate selIndexArrayFind]count]>10)
        distLabel.text = [NSString stringWithFormat:@"%@ miles",[[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :10]];
    else
        distLabel.text = @"N/A";
    
    distLabel.font = [UIFont systemFontOfSize:distanceLabelFont];
    distLabel.adjustsFontSizeToFitWidth = YES;
    distLabel.minimumScaleFactor = minDistanceLabelFont;
    [self.view addSubview:distLabel];
    //  [distLabel release];
    //[pinImage release];
    
    retDetailsTV = [[UITableView alloc] initWithFrame:CGRectMake(0.0, retailerName.frame.origin.y+retailerName.frame.size.height+2, SCREEN_WIDTH, tableViewHeight) style:UITableViewStylePlain];
    retDetailsTV.delegate = self;
    retDetailsTV.dataSource = self;
    [self.view addSubview:retDetailsTV];
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    ReleaseAndNilify(retailerName);
    
    /*UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithTitle:BACKMENU style:UIBarButtonItemStylePlain target:nil action:nil];
     self.navigationItem.backBarButtonItem = back;
     //[back release];*/
    
    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    if([[HubCitiAppDelegate selIndexArrayFind] count] > 0)
        address = [[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :0];
    else
        address = @"N/A";
    
    if([[HubCitiAppDelegate selIndexArrayFind] count] > 1)
        phoneNum = [[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :1];
    else
        phoneNum = @"N/A";
    
    if([[HubCitiAppDelegate selIndexArrayFind] count] > 2)
        price = [[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :2];
    else
        price = @"N/A";
    
    if([[HubCitiAppDelegate selIndexArrayFind] count] > 3)
        RetailerURL = [[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :3];
    else
        RetailerURL = @"N/A";
    
    if([[HubCitiAppDelegate selIndexArrayFind] count] > 4)
        headDirection = [[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :4];
    else
        headDirection = @"N/A";
    
    if([[HubCitiAppDelegate selIndexArrayFind] count] > 5)
        headWeb = [[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :5];
    else
        headWeb = @"N/A";
    
    if([[HubCitiAppDelegate selIndexArrayFind] count] > 6)
        imgPathDir = [[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :6];
    else
        imgPathDir = @"N/A";
    
    if([[HubCitiAppDelegate selIndexArrayFind] count] > 7)
        imgPathWeb = [[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :7];
    else
        imgPathWeb = @"N/A";
    
    if([[HubCitiAppDelegate selIndexArrayFind] count] > 8)
        latitude = [[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :8];
    else
        latitude = @"N/A";
    
    if([[HubCitiAppDelegate selIndexArrayFind] count] > 9)
        longitude = [[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :9];
    else
        longitude = @"N/A";
    
    
    //latitude = @"12.910373302582069";
    //longitude = @"77.64381408691406";
    m_lat = [defaults valueForKey:@"LatVal"];
    m_long = [defaults valueForKey:@"LongVal"];
    //m_lat = @"30.546456";
    //m_long = @"96.21546";
    //phoneNum = @"415-4676787";
    
}

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}

-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

//Chance's Edits
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        
        if (indexPath.row == 0)
        {
            return 110;
        }
        else {
            return 90;
        }
    }
    if (indexPath.row == 0)
    {
        return 80;
    }
    else {
        return 60;
    }
}
//End Chance's Edits


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    //Chance's Edits
    return [[HubCitiAppDelegate selIndexArrayFind] count]-9;
    //return [[AppDelegate selIndexArrayFind] count]-6;
    //End Chance's Edits
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell ;
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
    
    // Configure the cell...
    float titleLabelFont = 14.0, headerLabelFont = 18.0, minimumScaleFactor = 10.0;
    float labelXValue = 60.0;
    float callStoreHeight = 60.0;
    float asyncImageViewWidth = 44.0, asyncImageViewHeight = 44.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        
        titleLabelFont = 19.0;
        headerLabelFont = 23.0;
        minimumScaleFactor = 15.0;
        
        callStoreHeight = 90.0;
        labelXValue = 70.0;
        asyncImageViewWidth = 54.0;
        asyncImageViewHeight = 54.0;
    }
    //Chance's Edits
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(labelXValue, 38, SCREEN_WIDTH - 120, 14)] ;//width 200
    titleLabel.backgroundColor = [UIColor clearColor];
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(labelXValue, 8, SCREEN_WIDTH - 120, 18)] ;//width 200
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont systemFontOfSize:titleLabelFont];
    headerLabel.font = [UIFont boldSystemFontOfSize:headerLabelFont];
    headerLabel.textColor = [UIColor colorWithRGBInt:0x112e72];
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.minimumScaleFactor = minimumScaleFactor;
    //End Chance's Edits
    
    switch (indexPath.row) {
        case 0:
        {
            
            //Chance's Edits
            titleLabel.text = address;
            titleLabel.frame = CGRectMake(labelXValue, 32, SCREEN_WIDTH - 120, 40);//width 200
            titleLabel.numberOfLines = 2;
            [titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.minimumScaleFactor = minimumScaleFactor;
            headerLabel.text = @"Get Directions";
            
            //NSURL *asyncImgUrl = [NSURL URLWithString:imgPathDir];
            SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(10, 23, asyncImageViewWidth, asyncImageViewWidth)];
            asyncImageView.backgroundColor = [UIColor clearColor];
            //[asyncImageView loadImageFromURL:asyncImgUrl];
            [asyncImageView loadImage:imgPathDir];
            [cell addSubview: asyncImageView];
            [cell addSubview:headerLabel];
            //[asyncImageView release];
            
            /* UILabel *tx_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 280, 40)];
             tx_label.font = [UIFont fontWithName:@"Helvetica" size:16];
             tx_label.numberOfLines = 2;
             tx_label.lineBreakMode = YES;
             tx_label.backgroundColor = [UIColor clearColor];
             tx_label.text  = address;
             UILabel *txDetail_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 250, 20)];
             txDetail_label.font = [UIFont systemFontOfSize:12];
             txDetail_label.text = phoneNum;
             txDetail_label.backgroundColor = [UIColor clearColor];
             cell.selectionStyle = UITableViewCellSelectionStyleNone;
             [cell addSubview:tx_label];
             ReleaseAndNilify(tx_label);
             if(![phoneNum isEqualToString: @"N/A" ])
             [cell addSubview:txDetail_label];
             ReleaseAndNilify(txDetail_label);*/
            //End Chance's Edits
        }
            break;
        case 1:
        {
            
            //Chance's Edits
            titleLabel.text = phoneNum;
            headerLabel.text = NSLocalizedString(@"Call Location",@"Call Location");
            cell.selectionStyle = UITableViewCellEditingStyleNone;
            cell.accessoryType = UITableViewCellEditingStyleNone;
            callStore = [UIButton buttonWithType:UIButtonTypeCustom];
            callStore.tag = indexPath.row;
            [callStore setEnabled:YES];
            callStore.frame = CGRectMake(0, 0, SCREEN_WIDTH, callStoreHeight);
            [callStore setBackgroundColor:[UIColor clearColor]];
            [callStore setShowsTouchWhenHighlighted:YES];
            [callStore addTarget:self action:@selector(callPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:callStore];
            
            UIImageView *callImageView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, asyncImageViewWidth, asyncImageViewHeight)];
            callImageView.image = [UIImage imageNamed:@"phone_icon.png"];
            [cell addSubview:callImageView];
            
            
            if([phoneNum isEqualToString: @"N/A" ])
            {
                titleLabel.textColor = [UIColor grayColor];
                titleLabel.text = NSLocalizedString(@"No Phone Number",@"No Phone Number");
                callStore.enabled = NO;
                callImageView.image = [UIImage imageNamed:@"phone_iconDisabled.png"];
                callStore.titleLabel.textColor = [UIColor lightGrayColor];
            }
            // [callImageView release];
            /*
             UILabel *tx_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 15, 100, 30)];
             tx_label.font = [UIFont fontWithName:@"Helvetica" size:16];
             tx_label.text  = price;
             callbtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
             callbtn.frame = CGRectMake(200, 5, 100, 50);
             [callbtn  addTarget:self action: @selector(callPressed:) forControlEvents:UIControlEventTouchUpInside];
             cell.backgroundColor = [UIColor clearColor];
             callbtn.titleLabel.textColor = [UIColor blackColor];
             callbtn.backgroundColor = [UIColor clearColor];
             [callbtn setTitle:@"Call Location" forState:UIControlStateNormal];
             if([phoneNum isEqualToString: @"N/A" ])
             {
             callbtn.enabled = NO;
             callbtn.titleLabel.textColor = [UIColor grayColor];
             }
             if(![price isEqualToString: @"N/A" ])
             [cell addSubview:tx_label];
             ReleaseAndNilify(tx_label);
             cell.selectionStyle = UITableViewCellSelectionStyleNone;
             [cell addSubview:callbtn]; */
            
            //End Chance's Edits
        }
            break;
        case 2:
        {
            //Chance's Edits
            if (![RetailerURL isEqualToString:@"N/A"]) {
                
                //NSURL *asyncImgUrl = [NSURL URLWithString:imgPathWeb];
                SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(10, 8, asyncImageViewWidth, asyncImageViewHeight)];
                asyncImageView.backgroundColor = [UIColor clearColor];
                //[asyncImageView loadImageFromURL:asyncImgUrl];
                [asyncImageView loadImage:imgPathWeb];
                [cell addSubview: asyncImageView];
                titleLabel.text = NSLocalizedString(@"Browse Website",@"Browse Website");
                headerLabel.text = NSLocalizedString(@"Website",@"Website");
                //titleLabel.frame = CGRectMake(10, 2, 160, 80);
                //UIButton *websiteText = [UIButton buttonWithType:UIButtonTypeCustom];
                //[websiteText setEnabled:YES];
                //[websiteText.titleLabel sizeToFit];
                //websiteText.frame =  CGRectMake(210, 58, 100, 20);
                //[websiteText setTitle:@"Website" forState:UIControlStateNormal];
                //[websiteText setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                //[cell addSubview:websiteText];
                //[websiteText release];
                //[asyncImageView release];
                website = [UIButton buttonWithType:UIButtonTypeCustom];
                website.tag = indexPath.row;
                [website setEnabled:YES];
                website.frame = CGRectMake(SCREEN_WIDTH - 120, 2, 116, 76);//202 Xvalue
                [website setBackgroundColor:[UIColor clearColor]];
                [website setShowsTouchWhenHighlighted:YES];
                [website addTarget:self action:@selector(websitePressed) forControlEvents:UIControlEventTouchUpInside];
                [cell addSubview:website];
            }
            else {
                cell.selectionStyle = UITableViewCellEditingStyleNone;
                cell.accessoryType = UITableViewCellEditingStyleNone;
            }
            /*
             UILabel *tx_label = [[UILabel alloc] initWithFrame:CGRectMake(70, 15, 250, 30)];
             tx_label.text = headDirection;
             tx_label.font = [UIFont fontWithName:@"Helvetica" size:16];
             NSURL *asyncImgUrl = [NSURL URLWithString:imgPathDir];
             AsyncImageView *asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(5, 5, 50, 50)];
             asyncImageView.backgroundColor = [UIColor clearColor];
             [asyncImageView loadImageFromURL:asyncImgUrl];
             DLog(@"%@",m_lat);
             if(m_lat && [m_lat length]>0 && m_long && [m_long length]>0)
             {
             [cell addSubview:asyncImageView];
             [cell addSubview:tx_label];
             }
             //[asyncImageView release];
             ReleaseAndNilify(tx_label);
             
             }
             break;
             
             case 3:
             {
             UILabel *tx_label = [[UILabel alloc] initWithFrame:CGRectMake(70, 15, 250, 30)];
             tx_label.text = headWeb;
             tx_label.font = [UIFont fontWithName:@"Helvetica" size:16];
             NSURL *asyncImgUrl = [NSURL URLWithString:imgPathWeb];
             AsyncImageView *asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(5, 5, 50, 50)];
             asyncImageView.backgroundColor = [UIColor clearColor];
             [asyncImageView loadImageFromURL:asyncImgUrl];
             if(![RetailerURL isEqualToString: @"N/A" ])
             {
             [cell addSubview:asyncImageView];
             [cell addSubview:tx_label];
             }
             //[asyncImageView release];
             ReleaseAndNilify(tx_label);
             */
            //End Chance's Edits
        }
            break;
        default:
            break;
    }
    [cell addSubview:titleLabel];
    [cell addSubview:headerLabel];
    return cell;
}

-(void) websitePressed {
    //DLog(@"%@",RetailerURL);
    [defaults  setObject:RetailerURL forKey:KEY_URL];
    [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
    WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:urlDetail animated:NO];
    //[urlDetail release];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    switch (indexPath.row) {
            
            //Chance's Edits
        case 2:
        {
            if(![RetailerURL isEqualToString: @"N/A" ])
                
            {
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                [defaults setValue:RetailerURL forKey:@"URL"];
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
            break;
            
        case 0:
        {
            DLog(@"CHECKING ZIP AND LOCATION - %d", [HubCitiAppDelegate checkZipAndLocation]);
            if ([HubCitiAppDelegate checkZipAndLocation] != 0) {
                DLog(@"Setting to Zero");
                [defaults setValue:@"" forKey:@"Directions"];
            }
            else {
                if (![HubCitiAppDelegate locationServicesOn]) {
                    DLog(@"Setting to Zero");
                    [defaults setValue:@"" forKey:@"Directions"];
                }
                else {
                    DLog(@"Setting to Directions");
                    [defaults setValue:@"Directions" forKey:@"Directions"];
                }
            }
            
            FindNearByRetailerMap *urlDetail= [[FindNearByRetailerMap alloc] initWithNibName:@"FindNearByRetailerMap" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
            /*
             case 3:
             {
             if(![RetailerURL isEqualToString: @"N/A" ])
             
             {
             [defaults setValue:RetailerURL forKey:@"URL"];
             UrlDetailViewController *urlDetail= [[UrlDetailViewController alloc] initWithNibName:@"UrlDetailViewController" bundle:[NSBundle mainBundle]];
             [self.navigationController pushViewController:urlDetail animated:YES];
             //[urlDetail release];
             }
             }
             break;
             
             case 2:
             {
             [defaults setValue:@"Directions" forKey:@"Directions"];
             FindNearByRetailerMap *urlDetail= [[FindNearByRetailerMap alloc] initWithNibName:@"FindNearByRetailerMap" bundle:[NSBundle mainBundle]];
             [self.navigationController pushViewController:urlDetail animated:YES];
             //[urlDetail release];
             }         */
            //End Chance's Edits
    }
}
//Chance's Edits
/*
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	switch (indexPath.row) {
 case 0:
 
 return 70.0;
 break;
 
 case 1:
 return 60.0;
 break;
 
 case 2:
 
 if(m_lat && [m_lat length]>0 && m_long && [m_long length]>0)
 return 60.0;
 else
 return 0.0;
 
 break;
 
 case 3:
 DLog(@"%@",RetailerURL);
 if( [RetailerURL isEqualToString:@"N/A"])
 return 0.0;
 else
 return 60.0;
 break;
 
 default:
 return 60.0;
 break;
	}
 } */
-(void) callPressed: (id)sender {
    
    NSString *phoneString  = [[phoneNum componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
    NSString *phoneNumber = [NSString stringWithFormat:@"tel:%@", phoneString];
    NSURL    *url= [NSURL URLWithString:phoneNumber];
    
    UIWebView *webview = [[UIWebView alloc] initWithFrame:[callStore frame]];
    webview.alpha = 0.0;
    
    [webview loadRequest:[NSURLRequest requestWithURL:url]];
    
    // Assume we are in a view controller and have access to self.view
    [self.view insertSubview:webview belowSubview:callStore];
    //[webview release];
}



-(void)returnToMainPage:(id)sender{
    [defaults  removeObjectForKey:@"AdUrl"];
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // [website release];
    // [callStore release];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

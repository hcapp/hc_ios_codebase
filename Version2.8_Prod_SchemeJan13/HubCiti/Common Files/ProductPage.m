//
//  ProductPage.m
//  Scansee
//
//  Created by ajit on 9/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "ProductPage.h"
#import "ProductPageCustomCell1.h"
#import "MainMenuViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "RateShareViewController.h"
#import "FindNearbyOnlineStoresList.h"
#import "FindNearByResultListMapViewController.h"
#import "ProductCouponsList.h"
#import "WebBrowserViewController.h"
#import "UserSettingsController.h"
#import "ProductInformation.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "CurrentSpecials.h"
#import "CurrentSpecialsViewController.h"
#import "NewCouponDetailViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

BOOL couponsFlag;

@implementation StockHeader

@synthesize m_StockHeader;
@synthesize m_StockType;
@end

@implementation ProductPage
@synthesize productPageTable , adRibbonBtn;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    if ([defaults  objectForKey:@"refreshProd"] != nil)
        [defaults  removeObjectForKey:@"refreshProd"];
    
    prodName = @"";
    isFromProductPage = NO;
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    // //[mainPage release];
    
    
    //customize back button
   

    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    //    [aboutBtn setBackgroundImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [aboutBtn setBackgroundImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateHighlighted];
    //    //[aboutBtn addTarget:self action:@selector(aboutClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    aboutBtn.userInteractionEnabled = NO;
    //
    //    [wishBtn setBackgroundImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [wishBtn setBackgroundImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateHighlighted];
    //    //[wishBtn addTarget:self action:@selector(wishListClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    wishBtn.userInteractionEnabled = NO;
    //
    //    [prefBtn setBackgroundImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [prefBtn setBackgroundImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateHighlighted];
    //    //[prefBtn addTarget:self action:@selector(preferences:) forControlEvents:UIControlEventTouchUpInside];
    //    prefBtn.userInteractionEnabled = NO;
    //
    //    [rateBtn setBackgroundImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateNormal];
    //    [rateBtn setBackgroundImage:[UIImage imageNamed:@"BlankBottomButton.png"] forState:UIControlStateHighlighted];
    //    //[rateBtn addTarget:self action:@selector(rateShareClicked:) forControlEvents:UIControlEventTouchUpInside];
    //	rateBtn.userInteractionEnabled = NO;
    
    float salePriceFont = 13.0;
    float retailerNameLabelFont = 16.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        salePriceFont = 18.0;
        retailerNameLabelFont = 21.0;
    }
    //added 3rd condn as soetimes the ribbonAdImagePath coming as N/A even though AdUrl is there
    if (([[defaults valueForKey:@"AdUrl"]length]>0) && ([[HubCitiManager sharedManager]shareFromTL] == YES)
        && ![[defaults  objectForKey:@"ribbonAdImagePath"]isEqualToString:@"N/A"]) {
        
        productPageTable = [[UITableView alloc]initWithFrame:CGRectMake(0.0, 71.0, SCREEN_WIDTH, 299.0) style:UITableViewStylePlain];
        
        
        NSString *ribbonAdImagePathStr = [[defaults  objectForKey:@"ribbonAdImagePath"] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
        
        
        adRibbonBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //adRibbonBtn.backgroundColor = [UIColor whiteColor];
        [adRibbonBtn addTarget:self action:@selector(ribbonAdBtnPressed)forControlEvents:UIControlEventTouchUpInside];
        adRibbonBtn.frame = CGRectMake(0.0,0.0, self.view.frame.size.width, 50.0);
        
        UILabel *saleprice = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 20)];
        saleprice.font = [UIFont fontWithName:@"Helvetica" size:salePriceFont];
        saleprice.textAlignment = NSTextAlignmentCenter;
        saleprice.backgroundColor = [UIColor whiteColor];
        saleprice.text = [NSString stringWithFormat:@"Sale Price : %@   ",[defaults  objectForKey:@"salePrice"]];
        [self.view addSubview:saleprice];
        ReleaseAndNilify(saleprice);
        
        NSURL *ribbonAdImageURL = [NSURL URLWithString:ribbonAdImagePathStr];
        UIImage *ribbonAdImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:ribbonAdImageURL]];
        [adRibbonBtn setImage:ribbonAdImage forState:UIControlStateNormal];
        
        [self.view addSubview:adRibbonBtn];
    }
    else if(!([[defaults valueForKey:@"AdUrl"]length]>0) && ([[HubCitiManager sharedManager]shareFromTL] == YES)){
        
        UILabel *retailerNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 50.0)];//320
        retailerNameLabel.backgroundColor = [UIColor whiteColor];
        retailerNameLabel.textColor = [UIColor blackColor];
        retailerNameLabel.font = [UIFont boldSystemFontOfSize:retailerNameLabelFont];
        retailerNameLabel.textAlignment = NSTextAlignmentCenter;
        retailerNameLabel.numberOfLines = 2;
        [retailerNameLabel setLineBreakMode:NSLineBreakByWordWrapping];
        retailerNameLabel.text = [NSString stringWithFormat:@"%@",[defaults valueForKey:KEY_RTLNAME]];
        [self.view addSubview:retailerNameLabel];
        ReleaseAndNilify(retailerNameLabel);
        
        UILabel *saleprice = [[UILabel alloc] initWithFrame:CGRectMake(00, 40, self.view.frame.size.width, 30)];//320
        saleprice.font = [UIFont fontWithName:@"Helvetica" size:salePriceFont];
        saleprice.backgroundColor = [UIColor whiteColor];
        saleprice.textAlignment = NSTextAlignmentCenter;
        saleprice.text = [NSString stringWithFormat:@"Sale Price : %@   ",[defaults  objectForKey:@"salePrice"]];
        [self.view addSubview:saleprice];
        ReleaseAndNilify(saleprice);
        productPageTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 71, SCREEN_WIDTH, SCREEN_HEIGHT - 120) style:UITableViewStylePlain];//320 height 360
    }
    
    else {
        productPageTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 120) style:UITableViewStylePlain];//320 height 360
    }
    
    productPageTable.delegate = self;
    productPageTable.dataSource = self;
    [self.view addSubview:productPageTable];
    [self.view bringSubviewToFront:productPageTable];
    
    productPageTable.hidden = YES;
    
    stockHeaderArray = [[NSMutableArray alloc]init];
    stockImagePathArray = [[NSMutableArray alloc]init];
    labelValueArray = [[NSMutableArray alloc]init];
    detailLabelValueArray = [[NSMutableArray alloc]init];
    retIdArray = [[NSMutableArray alloc]init];
    retLocIdArray = [[NSMutableArray alloc]init];
    retListIDArray = [[NSMutableArray alloc]init];
    couponsFlag = 0;
    
    if([CLLocationManager  locationServicesEnabled] && !([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) &&[[HubCitiManager sharedManager]gps_allow_flag] == YES){
        
        [Location updateLocationinfo:self];
    }
    
    else
    {
        [self request_getproductsummary];
    }
}


-(void)request_fetchproudctattributes
{
    iWebRequestState = FETCHPRODUCTATTRIBUTES;
    NSString *urlString = [NSString stringWithFormat:@"%@find/fetchproudctattributes?userId=%@&productId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],prodId];
    [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    
}

-(void)request_getproductsummary
{
    iWebRequestState = GETPRODUCTSUMMARY;
    
    NSMutableString *xmlString = [[NSMutableString alloc] init];
    [xmlString appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults  objectForKey:KEY_USERID]];
    [xmlString appendFormat:@"<productId>%@</productId>",[defaults  objectForKey:KEY_PRODUCTID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[HubCitiManager sharedManager]gps_allow_flag] == YES && [[defaults valueForKey:KEY_LATITUDE] length])
        
        [xmlString appendFormat:@"<latitude>%@</latitude><longitude>%@</longitude>", [defaults valueForKey:KEY_LATITUDE],[defaults valueForKey:KEY_LONGITUDE]];
    
    else if ([[defaults objectForKey:@"ZipCode"] length])
        [xmlString appendFormat:@"<postalcode>%@</postalcode>", [defaults objectForKey:@"ZipCode"]] ;
    
    if ([[HubCitiManager sharedManager]shareFromTL] == YES)
        [xmlString appendFormat:@"<retailId>%@</retailId>",[defaults objectForKey:KEY_RETAILERID]];
    if([defaults  valueForKey:KEY_MAINMENUID])
        [xmlString appendFormat:@"<mainMenuId>%@</mainMenuId><scanTypeId>%@</scanTypeId>",[defaults  valueForKey:KEY_MAINMENUID],[defaults  valueForKey:KEY_SCANTYPEID]];
    if([defaults  valueForKey:@"saleListID"])
        [xmlString appendFormat:@"<saleListId>%@</saleListId>",[defaults  valueForKey:@"saleListID"]];
    if([defaults  valueForKey:KEY_PRODLISTID])
        [xmlString appendFormat:@"<prodListId>%@</prodListId>",[defaults  valueForKey:KEY_PRODLISTID]];
    if([defaults  valueForKey:KEY_HUBCITIID])
        [xmlString appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults  valueForKey:KEY_HUBCITIID]];
    
    
    [xmlString appendString:@"</ProductDetailsRequest>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@find/getproductsummary",BASE_URL];
    
    [ConnectionManager establishConnectionFor:xmlString base:urlString withDelegate:self];
    
    //[xmlString release];
    
}

// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case GETPRODUCTSUMMARY:
            [self parse_getproductsummary:response];
            break;
        case FETCHPRODUCTATTRIBUTES:
            [self parse_fetchproductarrributes:response];
            break;
        default:
            break;
    }
    
}


-(void)parse_fetchproductarrributes:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        [defaults setObject:responseString forKey:@"responseXml1"];
        ProductInformation *prodInfo = [[ProductInformation alloc]initWithNibName:@"ProductInformation" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:prodInfo animated:NO];
        //[prodInfo release];
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
}

-(void)parse_getproductsummary:(NSString*)responseString
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        [defaults setValue:responseString forKey:KEY_RESPONSEXML];
        [self parseTheXml:responseString];
      //  float titlelabelFont = 16.0;
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
           // titlelabelFont = 21.0;
        }
        
        
        CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
        [cus setTitle:[NSString stringWithFormat:@"%@",prodName] forView:self withHambergur:NO];
        
        
        //ReleaseAndNilify(titleLabel);
        
        [productPageTable setHidden:NO];
        [productPageTable reloadData];
        
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
}

-(void)popBackToPreviousPage{
    if (fromCurrentSpecials) {
        fromCurrentSpecials = FALSE;
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        for (UIViewController *aViewController in allViewControllers) {
            if ([aViewController isKindOfClass:[CurrentSpecialsViewController class]]) {
                [self.navigationController popToViewController:aViewController animated:NO];
            }
        }
    }
    else{
        [self.navigationController popViewControllerAnimated:NO];
    }
}

- (NSString *)removeHtmlTags:(NSString *)string {
    
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString:string];
    
    while ([theScanner isAtEnd] == NO) {
        
        [theScanner scanUpToString:@"<" intoString:NULL] ;
        
        [theScanner scanUpToString:@">" intoString:&text] ;
        
        string = [string stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return string;
}

- (void) animateCellToButton: (UIButton *)sender
{
    SdImageView *asyncImageView = [[SdImageView alloc] init];
    asyncImageView.contentMode = UIViewContentModeScaleToFill;
    asyncImageView.backgroundColor = [UIColor clearColor];
    asyncImageView.frame = CGRectMake(5, 5, 50, 50);
    
    float label1Font = 14, label2font = 12;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        label1Font = 19.0;
        label2font = 17.0;
    }
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, SCREEN_WIDTH - 90, 30)];//width230
    label1.backgroundColor = [UIColor clearColor];
    label1.textColor = [UIColor blackColor];
    label1.font = [UIFont systemFontOfSize:label1Font];
    label1.numberOfLines = 1;
    
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(60, 35, SCREEN_WIDTH - 90, 15)];//width 230
    label2.backgroundColor = [UIColor clearColor];
    label2.textColor = [UIColor grayColor];
    label2.font = [UIFont systemFontOfSize:label2font];
    label2.numberOfLines = 1;
    
    UIView *duplicateCell = [[UIView alloc] initWithFrame:CGRectMake(productPageTable.frame.origin.x, productPageTable.frame.origin.y, productPageTable.frame.size.width, productPageTable.rowHeight)];
    [duplicateCell setBackgroundColor:[UIColor whiteColor]];
    
    label1.text = prodName;
    label2.text = prodDesc;
    [duplicateCell addSubview:label1];
    
    if (![label2.text isEqualToString:@"N/A"])
        [duplicateCell addSubview:label2];
    
    if([[HubCitiManager sharedManager]shareFromTL] == YES)
        [asyncImageView loadImage:[NSString stringWithFormat:@"%@",[defaults valueForKey:@"PRODIMG"]]];
    
    else
        [asyncImageView loadImage:prodImage];
    
    [duplicateCell addSubview:asyncImageView];
    //    [label1 release];
    //    [label2 release];
    //[asyncImageView release];
    
    [self.view addSubview:duplicateCell];
    [self scaleButtonUp:sender];
    [self.view bringSubviewToFront:sender];
    // [self.view addSubview:sender];
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        CGAffineTransform scale = CGAffineTransformMakeScale(0.1,0.1);
        CGAffineTransform translate = CGAffineTransformMakeTranslation((sender.frame.origin.x + (sender.frame.size.width / 2)) - (duplicateCell.frame.size.width / 2), sender.frame.origin.y + (sender.frame.size.height / 2));
        duplicateCell.transform = CGAffineTransformConcat(scale, translate);
    }completion:^(BOOL finished) {
        
        if (finished)
        {
            [duplicateCell removeFromSuperview];
            [self scaleButtonDown:sender];
            //[duplicateCell release];
        }
    }
     ];
}

-(void)scaleButtonUp:(UIButton *)sender
{
    UIImage *img = [sender backgroundImageForState:UIControlStateHighlighted];
    UIImage *img2 = [sender backgroundImageForState:UIControlStateNormal];
    [sender setImage:img forState:UIControlStateNormal];
    [sender setImage:img2 forState:UIControlStateHighlighted];
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        CGAffineTransform scale = CGAffineTransformMakeScale(1.3,1.3);
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0,0);
        sender.transform = CGAffineTransformConcat(scale, translate);
    }completion:^(BOOL finished) {
        
        if (finished)
        {
        }
    }
     ];
}
-(void)scaleButtonDown:(UIButton *)sender
{
    UIImage *img = [sender backgroundImageForState:UIControlStateHighlighted];
    UIImage *img2 = [sender backgroundImageForState:UIControlStateNormal];
    
    [sender setImage:img2 forState:UIControlStateNormal];
    [sender setImage:img forState:UIControlStateHighlighted];
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        CGAffineTransform scale = CGAffineTransformMakeScale(1.0,1.0);
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0,0);
        sender.transform = CGAffineTransformConcat(scale, translate);
    }completion:^(BOOL finished) {
        
        if (finished)
        {
        }
    }
     ];
}



-(void) addToList:(UIButton *) sender
{
    [[HubCitiManager sharedManager]setRefreshList:YES];
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<AddShoppingList><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", prodId];
    [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName>", prodName];
    
    [requestStr appendString:@"</ProductDetail></productDetails>"];
    //@Deepak: Added MainMenuID in the request- userTracking for shoppinglist
    [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID></AddShoppingList>", [defaults  valueForKey:KEY_MAINMENUID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@shoppingList/addtslbysearch",BASE_URL];
    
    NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:response]) {
        return;
    }
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    
    if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
        [self animateCellToButton:sender];
    }
    else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        }
}

- (void)ribbonAdBtnPressed {
    NSString *url = [defaults  objectForKey:@"ribbonAdURL"];
    [defaults  setObject:url forKey:KEY_URL];
    DLog(@"url value:%@",[defaults valueForKey:KEY_URL]);
    [[HubCitiManager sharedManager]setShareFromTL:YES];
    [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
    WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:urlDetail animated:NO];
    //[urlDetail release];
}

-(void) updatedLatlongValue :(NSString *) latValue longitudeValue:(NSString *)longValue
{
    if (latValue == NULL || longValue == NULL) {
        return;
    }
    
    [defaults setObject:latValue forKey:KEY_LATITUDE];
    [defaults setObject:longValue forKey:KEY_LONGITUDE];
    
    [self request_getproductsummary];
   // float titleLabelFont = 16;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        //titleLabelFont = 21.0;
    }
    //	UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 40)];//320
    //	titleLabel.textAlignment = NSTextAlignmentCenter;
    //	titleLabel.textColor = [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]];
    //	titleLabel.backgroundColor = [UIColor clearColor];
    //	titleLabel.numberOfLines = 2;
    //    [titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    //	titleLabel.font = [UIFont boldSystemFontOfSize:titleLabelFont];
    //	titleLabel.text = [NSString stringWithFormat:@"%@",prodName];
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:[NSString stringWithFormat:@"%@",prodName] forView:self withHambergur:NO];
    
    //[titleLabel release];
    
    [productPageTable setHidden:NO];
    [productPageTable reloadData];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    isFromProductPage = NO;
    [self refresh];
    if ([[HubCitiManager sharedManager]refreshProdPage] == YES)
    {
        [[HubCitiManager sharedManager]setRefreshProdPage:NO];
        responseXml = [[NSString alloc]initWithString:[defaults valueForKey:KEY_RESPONSEXML]];
        
        aboutBtn.userInteractionEnabled = NO;
        wishBtn.userInteractionEnabled = NO;
        prefBtn.userInteractionEnabled = NO;
        rateBtn.userInteractionEnabled = NO;
        [self parseTheXml:responseXml];
        [productPageTable reloadData];
    }
    //[productPageTable deselectRowAtIndexPath:selectedIndex animated:YES];
}

// refresh product Summary coming after adding/ deleting clr to/from gallery
-(void) refresh
{
    if ([defaults  objectForKey:@"refreshProd"] != nil)
    {
        if ([defaults  boolForKey:@"refreshProd"] == YES) {
            
            [self request_getproductsummary];
            
            aboutBtn.userInteractionEnabled = NO;
            wishBtn.userInteractionEnabled = NO;
            prefBtn.userInteractionEnabled = NO;
            rateBtn.userInteractionEnabled = NO;
            
            [productPageTable reloadData];
            [defaults  removeObjectForKey:@"refreshProd"];
        }
    }
}

-(void)parseTheXml:(NSString *)response{
    
    [stockHeaderArray removeAllObjects];
    [stockImagePathArray removeAllObjects];
    [labelValueArray removeAllObjects];
    [detailLabelValueArray removeAllObjects];
    couponsFlag = 0;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    //Online Store part
    TBXMLElement *OSE = [TBXML childElementNamed:@"OnlineStores" parentElement:tbxml.rootXMLElement];
    if(OSE != nil){
        TBXMLElement *PIPE = [TBXML childElementNamed:@"stockImagePath" parentElement:OSE];
        
        if (PIPE != nil)
            [stockImagePathArray addObject:[TBXML textForElement:PIPE]];
        
        TBXMLElement *SHE = [TBXML childElementNamed:@"stockHeader" parentElement:OSE];
        NSString *stockHeaderStr = @"N/A";
        if(SHE)
            stockHeaderStr = [TBXML textForElement:SHE];
        
        
        StockHeader *stockHdr = [[StockHeader alloc] init];
        stockHdr.m_StockHeader = [stockHeaderStr copy];
        stockHdr.m_StockType = STOCK_ONLINE;
        [stockHeaderArray addObject:stockHdr];
        
        // chaitra
        
        TBXMLElement *OSMDE = [TBXML childElementNamed:@"onlineStoresMetaData" parentElement:OSE];
        if (OSMDE != nil) {
            
            TBXMLElement *MPE = [TBXML childElementNamed:@"minPrice" parentElement:OSMDE];
            NSString *minPriceStr = @"0";
            if(MPE)
                minPriceStr = [TBXML textForElement:MPE];
            
            DLog(@"MinPrice = %@",minPriceStr);
            [labelValueArray addObject:minPriceStr];
            
            
            TBXMLElement *TSE = [TBXML childElementNamed:@"totalStores" parentElement:OSMDE];
            NSString *totalStoresStr = @"0";
            if(TSE)
                totalStoresStr = [NSString stringWithFormat:@"%@ Stores", [TBXML textForElement:TSE]];
            
            DLog(@"totalStores = %@",totalStoresStr);
            [detailLabelValueArray addObject:totalStoresStr];
        }
        else {
            [labelValueArray addObject:@" "];
            [detailLabelValueArray addObject:@""];
        }
    }//end Online Store part
    
    //Find Nearby part
    TBXMLElement *FNDE = [TBXML childElementNamed:@"FindNearByDetailsResultSet" parentElement:tbxml.rootXMLElement];
    
    if (FNDE != nil) {
        
        TBXMLElement *PIPE = [TBXML childElementNamed:@"stockImagePath" parentElement:FNDE];
        if (PIPE != nil)
            [stockImagePathArray addObject:[TBXML textForElement:PIPE]];
        
        TBXMLElement *SHE = [TBXML childElementNamed:@"stockHeader" parentElement:FNDE];
        NSString *stockHeaderStr = @"N/A";
        if(SHE)
            stockHeaderStr = [TBXML textForElement:SHE];
        
        
        StockHeader *stockHdr = [[StockHeader alloc] init];
        stockHdr.m_StockHeader = [stockHeaderStr copy];
        stockHdr.m_StockType = STOCK_NEARBY;
        [stockHeaderArray addObject:stockHdr];
        
        TBXMLElement *FNMDE = [TBXML childElementNamed:@"findNearByMetaData" parentElement:FNDE];
        
        if(FNMDE)
        {
            TBXMLElement *TLE = [TBXML childElementNamed:@"totalLocations" parentElement:FNMDE];
            NSString *totalLocationsStr = @"N/A";
            if(TLE)
                totalLocationsStr = [TBXML textForElement:TLE];
            
            [detailLabelValueArray addObject:totalLocationsStr];
        }
        
        //For ScanSee Data
        TBXMLElement *FindNearbyDetailsElement = [TBXML childElementNamed:@"FindNearByDetails" parentElement:FNDE];
        TBXMLElement *retLocIdElement = [TBXML childElementNamed:@"retLocId" parentElement:FindNearbyDetailsElement];
        if (retLocIdElement!=nil) {
            
            while (FindNearbyDetailsElement!=nil){
                
                TBXMLElement *retIdElement = [TBXML childElementNamed:@"retailerId" parentElement:FindNearbyDetailsElement];
                
                TBXMLElement *retListIDElement = [TBXML childElementNamed:KEY_RLISTID parentElement:FindNearbyDetailsElement];
                if(retListIDElement != nil)
                    [retListIDArray addObject:[TBXML textForElement:retIdElement]];
                
                if(retIdElement)
                    [retIdArray addObject:[TBXML textForElement:retIdElement]];
                if(retLocIdElement)
                    [retLocIdArray addObject:[TBXML textForElement:retLocIdElement]];
                
                FindNearbyDetailsElement = [TBXML nextSiblingNamed:@"FindNearByDetails" searchFromElement:FindNearbyDetailsElement];
                
            }
        }
        
        TBXMLElement *LPE = [TBXML childElementNamed:@"lowestPrice" parentElement:FNMDE];
        NSString *lowestPriceStr = @"N/A";
        if(LPE)
            lowestPriceStr = [TBXML textForElement:LPE];
        
        if ([lowestPriceStr isEqualToString:@"N/A"] == NO)
            [labelValueArray addObject:lowestPriceStr];
        else
            [labelValueArray addObject:@" "];
    }//end Find Nearby part
    
    //Reviews part
    TBXMLElement *ProdRatingReviewElement = [TBXML childElementNamed:@"ProductRatingReview" parentElement:tbxml.rootXMLElement];
    
    if (ProdRatingReviewElement != nil) {
        
        TBXMLElement *TotalReviewsElement = [TBXML childElementNamed:@"totalReviews" parentElement:ProdRatingReviewElement];
        NSString *totalReviewsStr= @"N/A";
        if(TotalReviewsElement)
            totalReviewsStr = [TBXML textForElement:TotalReviewsElement];
        
        TBXMLElement *stockImgPathElement = [TBXML childElementNamed:@"stockImagePath" parentElement:ProdRatingReviewElement];
        if (stockImgPathElement != nil)
            [stockImagePathArray addObject:[TBXML textForElement:stockImgPathElement]];
        
        TBXMLElement *StockHeaderElement = [TBXML childElementNamed:@"stockHeader" parentElement:ProdRatingReviewElement];
        NSString *stockHeaderStr = @"N/A";
        if(StockHeaderElement)
            stockHeaderStr = [TBXML textForElement:StockHeaderElement];
        
        StockHeader *stockHdr = [[StockHeader alloc] init];
        stockHdr.m_StockHeader = [stockHeaderStr copy];
        stockHdr.m_StockType = STOCK_REVIEWS;
        [stockHeaderArray addObject:stockHdr];
        
        TBXMLElement *UserRatingInfoElement = [TBXML childElementNamed:@"userRatingInfo" parentElement:ProdRatingReviewElement];
        
        int userCount = 0;
        if (UserRatingInfoElement != nil) {
            
            TBXMLElement *AvgRatingElement = [TBXML childElementNamed:@"avgRating" parentElement:UserRatingInfoElement];
            NSString *avgRatingStr = [TBXML textForElement:AvgRatingElement];
            rating = [avgRatingStr copy] ;
            ReleaseAndNilify(avgRatingStr);
            
            TBXMLElement *noOfUsersRatedElement = [TBXML childElementNamed:@"noOfUsersRated" parentElement:UserRatingInfoElement];
            if(noOfUsersRatedElement)
                userCount = [[TBXML textForElement:noOfUsersRatedElement] intValue];
        }
        
        [detailLabelValueArray addObject:[NSString stringWithFormat:@"%@                              %i Ratings",totalReviewsStr,userCount]];
        [labelValueArray addObject:@" "];
    }//end Reviews part
    
    //Product Info part
    TBXMLElement *PDE = [TBXML childElementNamed:KEY_PRODUCTDETAIL parentElement:tbxml.rootXMLElement];
    if (PDE != nil) {
        
        TBXMLElement *SIPE = [TBXML childElementNamed:@"stockImagePath" parentElement:PDE];
        if(SIPE!=nil)
            [stockImagePathArray addObject:[TBXML textForElement:SIPE]];
        
        TBXMLElement *PNE = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:PDE];
        NSString *prodNameStr = [TBXML textForElement:PNE];
        prodName = [prodNameStr copy];
        [defaults  setObject:prodName forKey:KEY_PRODUCTNAME];
        
        TBXMLElement *PIDE = [TBXML childElementNamed:KEY_PRODUCTID parentElement:PDE];
        NSString *prodIdStr = [TBXML textForElement:PIDE];
        prodId = [prodIdStr copy];
        
        
        TBXMLElement *PLDE = [TBXML childElementNamed:@"productLongDescription" parentElement:PDE];
        TBXMLElement *ProductShortDescription = [TBXML childElementNamed:@"productShortDescription" parentElement:PDE];
        if(ProductShortDescription != nil && ![[TBXML textForElement:ProductShortDescription]isEqualToString:@"N/A"]){
            NSString *prodShortDescStr = [TBXML textForElement:ProductShortDescription];
            prodDesc = [prodShortDescStr copy];
        }
        else {
            NSString *prodLongDescStr = [TBXML textForElement:PLDE];
            prodDesc = [prodLongDescStr copy];
        }
        TBXMLElement *IPE = [TBXML childElementNamed:TAG_IMGPATH parentElement:PDE];
        NSString *imagePathStr= @"N/A";
        if(IPE)
            imagePathStr = [TBXML textForElement:IPE];
        prodImage = [imagePathStr copy];
        
        TBXMLElement *CLRFE = [TBXML childElementNamed:KEY_CLRFLAG parentElement:PDE];
        NSString *clrFlagStr = @"N/A";
        if(CLRFE)
            clrFlagStr = [TBXML textForElement:CLRFE];
        clrFlag = [clrFlagStr boolValue];
        
        [labelValueArray addObject:@" "];
        [detailLabelValueArray addObject:@" "];
        
    }//end Product Info part
    
    //CLR part
    TBXMLElement *CLRIE = [TBXML childElementNamed:@"CLRInfo" parentElement:tbxml.rootXMLElement];
    if (CLRIE != nil) {
        couponsFlag = 1;
        TBXMLElement *IPE = [TBXML childElementNamed:@"stockImagePath" parentElement:CLRIE];
        
        if(IPE != nil)
            [stockImagePathArray addObject:[TBXML textForElement:IPE]];
        
        TBXMLElement *SHE = [TBXML childElementNamed:@"stockHeader" parentElement:CLRIE];
        NSString *stockHeaderStr= @"N/A";
        if(SHE)
            stockHeaderStr = [TBXML textForElement:SHE];
        
        StockHeader *stockHdr = [[StockHeader alloc] init];
        stockHdr.m_StockHeader = stockHeaderStr;
        stockHdr.m_StockType = STOCK_COUPONS;
        [stockHeaderArray addObject:stockHdr];
        
        TBXMLElement *CE = [TBXML childElementNamed:@"couponFlag" parentElement:CLRIE];
        if(CE)
            couponStatus = [TBXML textForElement:CE];
        
        TBXMLElement *LE = [TBXML childElementNamed:@"loyaltyFlag" parentElement:CLRIE];
        if(LE)
            loyaltyStatus = [TBXML textForElement:LE];
        
        TBXMLElement *REBE = [TBXML childElementNamed:@"rebateFlag" parentElement:CLRIE];
        if(REBE)
            rebateStatus = [TBXML textForElement:REBE];
        
        [detailLabelValueArray addObject:@" "];
        [labelValueArray addObject:@" "];
    }//end CLR part
    
    //[tbxml release];
    
    CGFloat y_Axis = productPageTable.frame.origin.y;
    DLog(@" y = %g ((([stockHeaderArray count] + 1) * 60.0) - productPageTable.frame.origin.y-50) = %f",y_Axis,((([stockHeaderArray count] + 1) * 60.0) - productPageTable.frame.origin.y-50));
    
    CGRect newTVframe;
    float rowHeight = 60.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        
        rowHeight = 70.0;
    }
    if (((([stockHeaderArray count] + 1) * rowHeight) + productPageTable.frame.origin.y) > (SCREEN_HEIGHT - 114) ) {
        newTVframe = CGRectMake(productPageTable.frame.origin.x,
                                productPageTable.frame.origin.y,
                                productPageTable.frame.size.width,
                                (SCREEN_HEIGHT - 114) - productPageTable.frame.origin.y );
    }else{
        
        newTVframe = CGRectMake(productPageTable.frame.origin.x,
                                productPageTable.frame.origin.y,
                                productPageTable.frame.size.width,
                                ([stockHeaderArray count] + 1) * rowHeight);
    }
    
    productPageTable.frame = newTVframe;
    
    aboutBtn.userInteractionEnabled = YES;
    wishBtn.userInteractionEnabled = YES;
    prefBtn.userInteractionEnabled = YES;
    rateBtn.userInteractionEnabled = YES;
    
    DLog(@"%@",prodDesc);
    prodDesc = [[self removeHtmlTags:prodDesc]copy];
    DLog(@"%@",prodDesc);
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [stockHeaderArray count]+1;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        return 70.0;
    }
    return 60.0;
}


-(void) preferences : (id) sender{
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert];
    }
    else {
        UserSettingsController *settings = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:settings animated:NO];
        //[settings release];
        
    }
}
/*
 - (void)singleTap:(UIButton *)sender {
 // single tap does nothing for now
 
 }
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    productPageTable.backgroundColor = [UIColor whiteColor];
    
    float label1Font = 14.0, label2Font = 12.0, label1XValue = 60.0, label2XValue = 60.0, asyncImageViewWidth = 50.0, asyncImageViewHeight = 50.0, asyncImageViewSubWidth = 40.0, asyncImageViewSubHeight = 40.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        label1Font = 19.0;
        label2Font = 17.0;
        
        label1XValue = 70.0;
        label2XValue = 70.0;
        asyncImageViewWidth = 60.0;
        asyncImageViewHeight = 60.0;
        asyncImageViewSubWidth = 50.0;
        asyncImageViewSubHeight = 50.0;
    }
    
    SdImageView *asyncImageView = [[SdImageView alloc] init];
    asyncImageView.contentMode = UIViewContentModeScaleToFill;
    asyncImageView.backgroundColor = [UIColor clearColor];
    asyncImageView.frame = CGRectMake(5, 5, asyncImageViewWidth, asyncImageViewHeight);
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(label1XValue, 5, self.view.frame.size.width - label1XValue - 30, 30)];//width 230
    label1.backgroundColor = [UIColor clearColor];
    label1.textColor = [UIColor blackColor];
    label1.font = [UIFont boldSystemFontOfSize:label1Font];
    label1.numberOfLines = 1;
    
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(label2XValue, 35, self.view.frame.size.width - label2XValue - 30, 20)];//width 230
    label2.backgroundColor = [UIColor clearColor];
    label2.textColor = [UIColor grayColor];
    label2.font = [UIFont boldSystemFontOfSize:label2Font];
    label2.numberOfLines = 1;
    
    
    if (indexPath.row == 0 && ![prodName isEqualToString:@""])
    {
        static NSString *CellIdentifier = @"Cell";
        
        //taken for prod image zoom feature
        //UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        
        label1.text = prodName;
        label2.text = prodDesc;
        [cell addSubview:label1];
        
        if (![label2.text isEqualToString:@"N/A"])
            [cell addSubview:label2];
        
        if([[HubCitiManager sharedManager]shareFromTL] == YES)
            [asyncImageView loadImage:[NSString stringWithFormat:@"%@",[defaults valueForKey:@"PRODIMG"]]];
        
        else
            [asyncImageView loadImage:prodImage];
        
        [cell addSubview:asyncImageView];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    
    else if(indexPath.row!=0 && [stockHeaderArray count]) {
        static NSString *CellIdentifier1 = @"Cell1";
        
        ProductPageCustomCell1 *cell1 ;//= (ProductPageCustomCell1*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        // if (cell1 == nil){
        cell1 = [[ProductPageCustomCell1 alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier1];
        
        // }
        
        if([stockHeaderArray count]>indexPath.row - 1)
        {
            StockHeader *stkHdr = [stockHeaderArray objectAtIndex:indexPath.row - 1];
            DLog(@"8888888888888888888888 %@", stkHdr.m_StockHeader);
            
            label1.text = stkHdr.m_StockHeader;
            
            label2.text = [detailLabelValueArray objectAtIndex:indexPath.row - 1];
            [cell1 addSubview:label1];
            
            if (![label2.text isEqualToString:@"N/A"])
                [cell1 addSubview:label2];
            
            if([stockImagePathArray count] > indexPath.row - 1)
            {
                asyncImageView.frame = CGRectMake(5, 10, asyncImageViewSubWidth, asyncImageViewSubHeight);
                [asyncImageView loadImage:[stockImagePathArray objectAtIndex:indexPath.row - 1]];
                [cell1 addSubview:asyncImageView];
            }
            
            if([labelValueArray count] > indexPath.row - 1)
                cell1.label.text = [labelValueArray objectAtIndex:indexPath.row - 1];
            
            cell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            if (couponsFlag == 1 && indexPath.row == [stockHeaderArray count] && [stkHdr.m_StockHeader isEqualToString:@"Discount"]) {
                
                UIImageView * couponImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 46, 3, 14, 14)];//x 274
                UIImageView * loyaltyImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 46, 21, 14, 14)];//x 274
                UIImageView * rebateImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 46, 39, 14, 14)];//x 274
                
                couponImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"C-%@.png", couponStatus]];
                loyaltyImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"L-%@.png", loyaltyStatus]];
                rebateImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"R-%@.png", rebateStatus]];
                
                [cell1 addSubview:couponImage];
                [cell1 addSubview:loyaltyImage];
                [cell1 addSubview:rebateImage];
                
            }
            
            if ([stkHdr.m_StockHeader isEqualToString:@"Reviews"]) {
                
                float avgRating_f = [rating floatValue];
                int avgRating_i = [rating intValue];
                int i;
                i =0;
                do{
                    UIImageView *image1 = [[UIImageView alloc] init]; //WithFrame:CGRectMake(200, 14, 21, 21)];
                    image1.frame = CGRectMake((self.view.frame.size.width - 140 +(i*23)) , 10, 21, 21);//x (180+(i*23))
                    if (i < avgRating_i)
                        image1.image = [UIImage imageNamed:@"Red_fullstar.png"];
                    else
                        image1.image = [UIImage imageNamed:@"greyStar.png"];
                    
                    [cell1 addSubview:image1];
                    i++;
                } while (i < 5);
                
                float diff =avgRating_f - (float)avgRating_i;
                if (diff >= 0.5) {
                    UIImageView *image2 = [[UIImageView alloc] init]; //WithFrame:CGRectMake(200, 14, 21, 21)];
                    image2.frame = CGRectMake((self.view.frame.size.width - 140+(avgRating_i*23)) , 10, 21, 21);//x (180+(avgRating_i*23))
                    image2.image = [UIImage imageNamed:@"Red_Halfstar.png"];
                    
                    [cell1 addSubview:image2];
                }
                
            }
            
            return cell1;
        }
        
    }
    else {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell ;
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        return cell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    selectedIndex = indexPath;
    
    if(indexPath.row == 0){
        {
            // Launch respective view controller
            
            if ([prodId isEqualToString:@"0"]) {
                
                UIAlertController * alert;
                alert=[UIAlertController alertControllerWithTitle:@" " message:NSLocalizedString(@"Product details not available!",@"Product details not available!") preferredStyle:UIAlertControllerStyleAlert];
                [self presentViewController:alert animated:YES completion:nil];
                
                [UtilityManager performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:2];
               
            }
            else
            {
                [self request_fetchproudctattributes];
            }
        }
    }
    
    if (indexPath.row > 0){
        
        StockHeader *stkHdr = [stockHeaderArray objectAtIndex:indexPath.row - 1];
        switch (stkHdr.m_StockType) {
            case STOCK_ONLINE:
            {
                // Launch respective view controller
                //[defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
                [defaults  setObject:prodId forKey:KEY_PRODUCTID];
                FindNearbyOnlineStoresList *onlineStores = [[FindNearbyOnlineStoresList alloc]initWithNibName:@"FindNearbyOnlineStoresList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:onlineStores animated:NO];
                //[onlineStores release];
                
            }
                break;
            case STOCK_NEARBY: {
                // Launch respective view controller
                // Launch respective view controller
                FindNearByResultListMapViewController *localStores = [[FindNearByResultListMapViewController alloc]initWithNibName:@"FindNearByResultListMapViewController" bundle:[NSBundle mainBundle]];
                
                [HubCitiAppDelegate RefreshRetIdArray];
                [HubCitiAppDelegate RefreshRetLocIdArray];
                if ([retLocIdArray count]){//means only in case of scansee data
                    [[HubCitiAppDelegate getRetIdArray]addObjectsFromArray:retIdArray];
                    [[HubCitiAppDelegate getRetLocIdArray]addObjectsFromArray:retLocIdArray];
                }
                [self.navigationController pushViewController:localStores animated:NO];
                //[localStores release];
                
                
            }
                break;
            case STOCK_REVIEWS: {
                // Launch respective view controller
                RateShareViewController *rateShare = [[RateShareViewController alloc]initWithNibName:@"RateShareViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:rateShare animated:NO];
                //[rateShare release];
                
            }
                break;
                
            case STOCK_PRODUCTINFO: {
                // Launch respective view controller
                
                if ([prodId isEqualToString:@"0"]) {
                    UIAlertController * alert;
                    alert=[UIAlertController alertControllerWithTitle:@" " message:NSLocalizedString(@"Network appears to be down. Please try later",@"Network appears to be down. Please try later") preferredStyle:UIAlertControllerStyleAlert];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    [UtilityManager performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:2];
                }
                else
                {
                    [self request_fetchproudctattributes];
                }
                DLog(@"Productinfo");
            }
                break;
                
            case STOCK_COUPONS: {
                
                // Launch respective view controller
                if (clrFlag == 0 ) {
                    UIAlertController * alert;
                    alert=[UIAlertController alertControllerWithTitle:@" " message:@"No C/L/R available" preferredStyle:UIAlertControllerStyleAlert];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                  [UtilityManager performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:2];
                    
                }
                else {
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert];
                    }
                    else {
                        ProductCouponsList *couponsScreen = [[ProductCouponsList alloc]initWithNibName:@"ProductCouponsList" bundle:[NSBundle mainBundle]];
                        isFromProductPage = YES;
                        [self.navigationController pushViewController:couponsScreen animated:NO];
                        //[couponsScreen release];
                    }
                }
                DLog(@"Coupons");
            }
                break;
            default:
                break;
        }
    }
    [productPageTable deselectRowAtIndexPath:selectedIndex animated:YES];
}



-(void)returnToMainPage:(id)sender{
    [defaults  removeObjectForKey:@"AdUrl"];
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
    
}
-(void)aboutClicked:(id)sender{
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert];
    }else {
        AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
        privacy.responseXml =[NSMutableString stringWithFormat:@"%@About_ScanSee.html", BASE_URL];
        [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
        privacy.comingFromPrivacyScreen = NO;
        [self.navigationController pushViewController:privacy animated:NO];
        //[privacy release];
    }
}
-(void)wishListClicked:(UIButton *)sender{
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", prodId];
    [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName></ProductDetail></productDetails>", prodName];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID>", [defaults  valueForKey:KEY_MAINMENUID]];
    [requestStr appendString:@"</ProductDetailsRequest>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@wishlist/addWishListProd",BASE_URL];
    NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
        
        [self animateCellToButton:sender];
        
        [defaults  removeObjectForKey:KEY_SEARCHFROM];
    }
    else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        }
}

-(void)viewListClicked:(id)sender{
}
-(void)rateShareClicked:(id)sender{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        
        if([Network currentReachabilityStatus]==0)
        {
            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
        }else {
            
            [[HubCitiManager sharedManager]setHotDealToolBar:NO];
            DLog(@"Product ID:%@", prodId);
            [defaults  setObject:prodId forKey:KEY_PRODUCTID];
        }
        //		RateShareViewController *rateShareVC = [[RateShareViewController alloc] initWithNibName:@"RateShareViewController" bundle:[NSBundle mainBundle]];
        //		[self.navigationController pushViewController:rateShareVC animated:NO];
        //		[rateShareVC release];
    }
}




-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}



- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end

//
//  FindNearbyOnlineStoresList.h
//  Scansee
//
//  Created by ajit on 9/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"

@interface FindNearbyOnlineStoresList : UITableViewController<CustomizedNavControllerDelegate> {

	NSString *minPrice;
	NSMutableArray *urlArray,*merchantNameArray,*salePriceArray,*shipAmountArray;
	int nextPage,pageSize,pageStart;
	IBOutlet UITableView *merchantTable;
	NSString *apiUrl, *vendorName;
	NSString *poweredByStr;

    NSMutableArray *retailerName ,*retailerId ,*commissionSalePrice ,*commissionPrice ,*shipmentCost ,*buyUrlArray;
}
@property (nonatomic, strong)UITableView *merchantTable;

-(void)parseOnlineResults:(NSString *)response;
-(void)returnToMainPage:(id)sender;
-(void)updateRetailersList;

@end

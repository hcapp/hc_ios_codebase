//
//  FindNearbyResultCustomCell.m
//  Scansee
//
//  Created by ajit on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FindNearbyResultCustomCell.h"


@implementation FindNearbyResultCustomCell
@synthesize priceLabel,titleLabel, subTitleLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code.
		
		//we need a view to place our labels
		UIView *myContentView = self.contentView;
		priceLabel = [[UILabel alloc]init];
		priceLabel.textAlignment = NSTextAlignmentRight;
		[myContentView addSubview:priceLabel];
		titleLabel = [[UILabel alloc]init];
		titleLabel.textAlignment = NSTextAlignmentLeft; 
		[myContentView addSubview:titleLabel];
		subTitleLabel = [[UILabel alloc]init];
		subTitleLabel.textAlignment = NSTextAlignmentLeft; 
		[myContentView addSubview:subTitleLabel];

	}
		
    
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}

-(void)setData:(NSDictionary *)dict{
	subTitleLabel.text = [dict objectForKey:@"SubTitle"];
	titleLabel.text = [dict objectForKey:@"Title"];
	priceLabel.text = [dict objectForKey:@"Price"];
}
- (void)layoutSubviews {
	
    [super layoutSubviews];
	
	// getting the cell size
    CGRect contentRect = self.contentView.bounds;
    float priceLabelXvalue = 180;
    float subTitleFont = 12;
    float extraYValue_iPad = 0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        priceLabelXvalue = SCREEN_WIDTH - 140;
        subTitleFont = 17;
        extraYValue_iPad = 5.0;
    }
	// In this example we will never be editing, but this illustrates the appropriate pattern
    if (!self.editing) {
		
		// get the X pixel spot
        CGFloat originX = contentRect.origin.x;
		CGFloat originY = contentRect.origin.y;
		CGRect frame;
		
        /*
		 Place the title label.
		 place the label whatever the current X is plus 10 pixels from the left
		 place the label 4 pixels from the top
		 make the label 200 pixels wide
		 make the label 20 pixels high
		 */
		
		frame = CGRectMake(originX + 5+10, originY + 5 + extraYValue_iPad, SCREEN_WIDTH - 120, 20);//width 200
		titleLabel.frame = frame;
		
		
		
		frame = CGRectMake(originX + 5+10, originY + 5 + 22 + extraYValue_iPad, SCREEN_WIDTH - 160, 20);// width 160
		subTitleLabel.frame = frame;
		subTitleLabel.font=[UIFont systemFontOfSize:subTitleFont];
		
		
		frame = CGRectMake(priceLabelXvalue, originY + 22, 80, 20);
		priceLabel.frame = frame;

	}
}



@end

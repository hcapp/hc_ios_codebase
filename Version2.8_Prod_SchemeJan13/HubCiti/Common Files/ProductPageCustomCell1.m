//
//  ProductPageCustomCell1.m
//  Scansee
//
//  Created by ajit on 9/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ProductPageCustomCell1.h"


@implementation ProductPageCustomCell1
@synthesize label;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code.
        float labelFont = 14.0;
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            labelFont = 19.0;
        }
		UIView *myContentView = self.contentView;
		label = [[UILabel alloc]init];
		label.textAlignment = NSTextAlignmentRight;
		label.textColor = [UIColor blackColor];
		label.font = [UIFont systemFontOfSize:labelFont];
		label.backgroundColor = [UIColor clearColor]; 
		[myContentView addSubview:label];
		
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}

- (void)layoutSubviews {
	
    [super layoutSubviews];
	
	// getting the cell size
    CGRect contentRect = self.contentView.bounds;
	
	// In this example we will never be editing, but this illustrates the appropriate pattern
    if (!self.editing) {
		
		CGFloat originY = contentRect.origin.y;
		CGRect frame;
		frame = CGRectMake(SCREEN_WIDTH - 120, originY + 15, 80, 20);
		label.frame = frame;
		
	}
}





@end

//
//  FSImageViewController.h
//  Scansee
//
//  Created by Chance Ivey on 7/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FSImageViewController : UIViewController<UIScrollViewDelegate>
{
    
    NSString *imgUrlString;
}
@property(nonatomic, assign) IBOutlet UIScrollView *scrollView;
@property(nonatomic, strong)  UIImageView *imageView;
@property(nonatomic,retain) NSString *imgUrlString;
@property(nonatomic, strong) UITapGestureRecognizer *singleTap; 
@property(nonatomic, strong) UITapGestureRecognizer *doubleTap; 
@property(nonatomic, strong) UITapGestureRecognizer *twoFingerTap;
@end

//
//  FindNearbyOnlineStoresList.m
//  Scansee
//
//  Created by ajit on 9/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FindNearbyOnlineStoresList.h"
#import "MainMenuViewController.h"
#import "WebBrowserViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@implementation FindNearbyOnlineStoresList
@synthesize merchantTable;

#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
    urlArray = [[NSMutableArray alloc]init];
    merchantNameArray = [[NSMutableArray alloc]init];
    salePriceArray = [[NSMutableArray alloc]init];
    shipAmountArray = [[NSMutableArray alloc] init];
    
    retailerId = [[NSMutableArray alloc] init];
    retailerName = [[NSMutableArray alloc] init];
    commissionPrice = [[NSMutableArray alloc] init];
    commissionSalePrice = [[NSMutableArray alloc] init];
    shipmentCost = [[NSMutableArray alloc] init];
    buyUrlArray = [[NSMutableArray alloc] init];
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.title = @"Online Stores";
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0,30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    
    //customize back button
   
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        merchantTable.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
    NSString *responseXml = [[NSString alloc] initWithString:[defaults  valueForKey:KEY_RESPONSEXML]];
    
    [self parseOnlineResults:responseXml];
    //[responseXml release];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}

-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)parseOnlineResults:(NSString *)response{
    
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *OnlineStoresElement = [TBXML childElementNamed:@"OnlineStores" parentElement:tbxml.rootXMLElement];
    
    TBXMLElement *onlineStoresMetaDataElement = [TBXML childElementNamed:@"onlineStoresMetaData" parentElement:OnlineStoresElement];
    
    if (onlineStoresMetaDataElement != nil) {
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:onlineStoresMetaDataElement];
        
        if (nextPageElement == nil) {
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
            NSString *responseTextStr = [TBXML textForElement:responseTextElement];
            
            // Banner the message given by the server
            [UtilityManager showAlert:nil msg:responseTextStr];
            return;
        }
        
        //NSString *nextPageStr = [TBXML textForElement:nextPageElement];
        //extra tags are needed from server end for pagination - for 1.4 version. So pagination not supported
        nextPage = 0; //= [nextPageStr intValue];
        
        if (nextPage == 1){
            TBXMLElement *apiURLElement = [TBXML childElementNamed:@"apiURL" parentElement:onlineStoresMetaDataElement];
            apiUrl = [[TBXML textForElement:apiURLElement]copy];
            DLog(@"API url: %@",apiUrl);
            
            TBXMLElement *pageSizeElement = [TBXML childElementNamed:@"pageSize" parentElement:onlineStoresMetaDataElement];
            pageSize = [[TBXML textForElement:pageSizeElement] intValue];
            
            TBXMLElement *pageStartElement = [TBXML childElementNamed:@"pageStart" parentElement:onlineStoresMetaDataElement];
            pageStart = [[TBXML textForElement:pageStartElement] intValue];
            
            TBXMLElement *vendorNameElement = [TBXML childElementNamed:@"vendorName" parentElement:onlineStoresMetaDataElement];
            vendorName = [[TBXML textForElement:vendorNameElement]copy];
            
        }
        
        TBXMLElement *offersElement = [TBXML childElementNamed:@"offers" parentElement:OnlineStoresElement];
        
        while (offersElement != nil) {
            
            TBXMLElement *OfferElement = [TBXML childElementNamed:@"Offer" parentElement:offersElement];
            
            while (OfferElement != nil){
                TBXMLElement *urlElement = [TBXML childElementNamed:@"url" parentElement:OfferElement];
                TBXMLElement *merchantNameElement = [TBXML childElementNamed:@"merchantName" parentElement:OfferElement];
                TBXMLElement *salePriceElement = [TBXML childElementNamed:@"salePrice" parentElement:OfferElement];
                TBXMLElement *poweredByElement = [TBXML childElementNamed:@"poweredBy" parentElement:OfferElement];
                TBXMLElement *shipAmountElement = [TBXML childElementNamed:@"shipAmount" parentElement:OfferElement];
                
                if (urlElement != nil) {
                    [urlArray addObject:[TBXML textForElement:urlElement]];
                }
                
                if (merchantNameElement != nil) {
                    [merchantNameArray addObject:[TBXML textForElement:merchantNameElement]];
                }
                
                if (salePriceElement != nil) {
                    
                    [salePriceArray addObject: [TBXML textForElement:salePriceElement]];
                }
                
                if (poweredByElement != nil) {
                    //taken as a string since currently we are fetching only from shopzilla api
                    poweredByStr = [[TBXML textForElement:poweredByElement]copy];
                }
                if (shipAmountElement != nil) {
                    //taken as a string since currently we are fetching only from shopzilla api
                    [shipAmountArray addObject:[TBXML textForElement:shipAmountElement]];
                }
                OfferElement = [TBXML nextSiblingNamed:@"Offer" searchFromElement:OfferElement];
                
            }
            offersElement = [TBXML nextSiblingNamed:@"offers" searchFromElement:offersElement];
        }
        DLog(@"merchantNameArray:%@",[merchantNameArray description]);
    }
    
    
    TBXMLElement *CommissionJunctionDataE = [TBXML childElementNamed:@"CommissionJunctionData" parentElement:OnlineStoresElement];
    if (CommissionJunctionDataE != nil) {
        TBXMLElement *RetailerDetailEle = [TBXML childElementNamed:@"RetailerDetail" parentElement:CommissionJunctionDataE];
        while (RetailerDetailEle != nil) {
            TBXMLElement *retailerIdElement = [TBXML childElementNamed:@"retailerId" parentElement:RetailerDetailEle];
            TBXMLElement *retailerNameElement = [TBXML childElementNamed:@"retailerName" parentElement:RetailerDetailEle];
            TBXMLElement *priceElement = [TBXML childElementNamed:@"price" parentElement:RetailerDetailEle];
            TBXMLElement *salePriceElement = [TBXML childElementNamed:@"salePrice" parentElement:RetailerDetailEle];
            TBXMLElement *shipmentCostElement = [TBXML childElementNamed:@"shipmentCost" parentElement:RetailerDetailEle];
            TBXMLElement *buyURLElement = [TBXML childElementNamed:@"buyURL" parentElement:RetailerDetailEle];
            
            if (retailerIdElement != nil)
                [retailerId addObject:[TBXML textForElement:retailerIdElement]];
            if(retailerNameElement != nil)
                [retailerName addObject:[TBXML textForElement:retailerNameElement]];
            if(priceElement)
                [commissionPrice addObject:[TBXML textForElement:priceElement]];
            if(salePriceElement)
                [commissionSalePrice addObject:[TBXML textForElement:salePriceElement]];
            if(shipmentCostElement)
                [shipmentCost addObject:[TBXML textForElement:shipmentCostElement]];
            if(buyURLElement)
                [buyUrlArray addObject:[TBXML textForElement:buyURLElement]];
            
            RetailerDetailEle = [TBXML nextSiblingNamed:@"RetailerDetail" searchFromElement:RetailerDetailEle];
        }
    }
    
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    int i = 0 ;
    if ([retailerId count]>0) {
        i++;
    }
    if ([merchantNameArray count] > 0) {
        i++ ;
    }
    return i;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 0 && [retailerId count]>0) {
        return [retailerId count];
    }else {
        if (nextPage == 1) {
            return ([merchantNameArray count] + 1);
        }
        else
            return [merchantNameArray count];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 0 && [retailerId count] > 0) {
        return 0.0;
    }
    else
        return 22.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 0 && [retailerId count] > 0) {
        return nil;
    }
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width,30.0)];
    [headerView setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
    
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 310, 20)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:15.0];
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.text = poweredByStr;
    [headerView addSubview:headerLabel];
    // [headerLabel release];
    return headerView;
}

/*
 - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
 if (section == 0 && [retailerId count] > 0) {
 return nil;
 }
	return poweredByStr;
 }*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        return 70.0;
    }
    return 50.0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    // UITableViewCell *cell ;//= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //if (cell == nil) {
    // cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    //}
    
    // Configure the cell...
    float textLabel1Font = 16.0;
    float textLabel2Font = 14.0, nameLabelFont = 14.0, salePriceFont = 12.0, shippingFont = 12.0;
    float shippingXValue = 150.0;
    float topLabelsYValue = 5.0, bottomLabelYValue = 25.0;;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        textLabel1Font = 21.0;
        textLabel2Font = 19.0;
        nameLabelFont = 19.0;
        salePriceFont = 17.0;
        shippingFont = 17.0;
        shippingXValue = 470.0;
        
        topLabelsYValue = 10.0;
        bottomLabelYValue = 30.0;
    }
    if (indexPath.row == [merchantNameArray count] && !(indexPath.section == 0 && [retailerId count]>0) ) {
        
        static NSString *CellIdentifier1 = @"Cell1";
        UITableViewCell *cell1 ;
        cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] ;
        
        cell1.textLabel.text = NSLocalizedString(@"Show more Retailers...",@"Show more Retailers...");
        cell1.textLabel.font = [UIFont fontWithName:@"Helvetica" size:textLabel1Font];
        cell1.textLabel.textColor = [UIColor purpleColor];
        return cell1;
    }
    else {
        
        //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UITableViewCell *cell ;
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        
        cell.textLabel.textColor = [UIColor colorWithRGBInt:0x112e72];
        cell.textLabel.font = [UIFont systemFontOfSize:textLabel2Font];
        
        UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, topLabelsYValue, SCREEN_WIDTH - 90.0, 25)];//width 230
        nameLabel.textColor = [UIColor colorWithRGBInt:0x112e72];
        nameLabel.font = [UIFont boldSystemFontOfSize:nameLabelFont];
        nameLabel.backgroundColor = [UIColor clearColor];
        
        UILabel *salePrice = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 80, topLabelsYValue, 60, 20)];//X 240
        salePrice.textAlignment = NSTextAlignmentRight;
        salePrice.textColor = [UIColor blackColor];
        salePrice.font = [UIFont boldSystemFontOfSize:salePriceFont];
        salePrice.adjustsFontSizeToFitWidth = YES;
        salePrice.backgroundColor = [UIColor clearColor];
        
        UILabel *shipping = [[UILabel alloc]initWithFrame:CGRectMake(shippingXValue, bottomLabelYValue, SCREEN_WIDTH - (shippingXValue), 20)];//X 150 width 150
        shipping.textAlignment = NSTextAlignmentRight;
        shipping.textColor = [UIColor darkGrayColor];
        shipping.font = [UIFont boldSystemFontOfSize:shippingFont];
        shipping.adjustsFontSizeToFitWidth = YES;
        shipping.backgroundColor = [UIColor clearColor];
        
        if (indexPath.section == 0 && [retailerId count]>0) {
            //  cell.selectionStyle = UITableViewCellSelectionStyleNone;
            //cell.textLabel.text = [retailerName objectAtIndex:indexPath.row];
            nameLabel.text = [retailerName objectAtIndex:indexPath.row];
            salePrice.text =[NSString stringWithFormat:@"%@", [commissionSalePrice objectAtIndex:indexPath.row]];
            if ([[shipmentCost objectAtIndex:indexPath.row] isEqualToString:@"$0.00"]) {
                shipping.text = NSLocalizedString(@"Free shipping",@"Free shipping");
            }
            else {
                shipping.text = [NSString stringWithFormat:@"%@ ",[shipmentCost objectAtIndex:indexPath.row]];
            }
        }
        else {
            //cell.textLabel.text = [merchantNameArray objectAtIndex:indexPath.row];
            nameLabel.text = [merchantNameArray objectAtIndex:indexPath.row];
            salePrice.text = [salePriceArray objectAtIndex:indexPath.row];
            if ([[shipAmountArray objectAtIndex:indexPath.row] isEqualToString:@"$0.00"]) {
                shipping.text = NSLocalizedString(@"Free shipping",@"Free shipping");
            }else {
                shipping.text = [NSString stringWithFormat:@"+ %@ shipping",[shipAmountArray objectAtIndex:indexPath.row]];
            }
        }
        [cell addSubview:nameLabel];
        [cell addSubview:shipping];
        [cell addSubview:salePrice];
        ReleaseAndNilify(nameLabel);
        ReleaseAndNilify(salePrice);
        ReleaseAndNilify(shipping);
        return cell;
        
    }
}


-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //   [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
    
}


-(void)updateRetailersList{
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<OnlineStoresRequest><pageStart>%d</pageStart>",pageStart];
    
    DLog(@"Api Url: %@",apiUrl);
    DLog(@"page size: %d",pageSize);
    DLog(@"Vender Name: %@",vendorName);
    
    NSMutableString *str = [[NSMutableString alloc]init];
    [str appendString:@"<![CDATA["];
    [str appendFormat:@"%@",apiUrl];
    [str appendString:@"]]>"];
    
    [requestStr appendFormat:@"<apiURL>%@</apiURL><vendorName>%@</vendorName>",str,vendorName];
    ReleaseAndNilify(str);
    [requestStr appendFormat:@"<pageSize>%d</pageSize></OnlineStoresRequest>",pageSize];
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"shoppingList/findOnlineStores"];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml]) {
        ReleaseAndNilify(responseXml);
        return;
    }
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    //TBXMLElement *OnlineStoresElement = [TBXML childElementNamed:@"OnlineStores" parentElement:tbxml.rootXMLElement];
    TBXMLElement *onlineStoresMetaDataElement = [TBXML childElementNamed:@"onlineStoresMetaData" parentElement:tbxml.rootXMLElement];
    
    TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:onlineStoresMetaDataElement];
    
    if (nextPageElement == nil) {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
        
        // Banner the message given by the server
        [UtilityManager showAlert:nil msg:responseTextStr];
        return;
    }
    
    NSString *nextPageStr = [TBXML textForElement:nextPageElement];
    nextPage = [nextPageStr intValue];
    
    if (nextPage == 1){
        TBXMLElement *apiURLElement = [TBXML childElementNamed:@"apiURL" parentElement:onlineStoresMetaDataElement];
        apiUrl = [[TBXML textForElement:apiURLElement]copy];
        
        TBXMLElement *pageSizeElement = [TBXML childElementNamed:@"pageSize" parentElement:onlineStoresMetaDataElement];
        pageSize = [[TBXML textForElement:pageSizeElement]intValue];
        
        TBXMLElement *vendorNameElement = [TBXML childElementNamed:@"vendorName" parentElement:onlineStoresMetaDataElement];
        vendorName = [[TBXML textForElement:vendorNameElement]copy];
    }
    
    
    TBXMLElement *offersElement = [TBXML childElementNamed:@"offers" parentElement:tbxml.rootXMLElement];
    
    if (offersElement != nil) {
        
        TBXMLElement *OfferElement = [TBXML childElementNamed:@"Offer" parentElement:offersElement];
        
        while (OfferElement != nil){
            TBXMLElement *urlElement = [TBXML childElementNamed:@"url" parentElement:OfferElement];
            TBXMLElement *merchantNameElement = [TBXML childElementNamed:@"merchantName" parentElement:OfferElement];
            TBXMLElement *salePriceElement = [TBXML childElementNamed:@"salePrice" parentElement:OfferElement];
            TBXMLElement *poweredByElement = [TBXML childElementNamed:@"poweredBy" parentElement:OfferElement];
            
            if (urlElement != nil) {
                [urlArray addObject:[TBXML textForElement:urlElement]];
            }
            
            if (merchantNameElement != nil) {
                [merchantNameArray addObject:[TBXML textForElement:merchantNameElement]];
            }
            
            if (salePriceElement != nil) {
                
                [salePriceArray addObject:[TBXML textForElement:salePriceElement]];
            }
            if (poweredByElement != nil) {
                
                poweredByStr = [[TBXML textForElement:poweredByElement]copy];
            }
            OfferElement = [TBXML nextSiblingNamed:@"Offer" searchFromElement:OfferElement];
            
        }
        //offersElement = [TBXML nextSiblingNamed:@"offers" searchFromElement:offersElement];
    }
    //[tbxml release];
    //[responseXml release];
    
    DLog(@"Value of nextPage: %d",nextPage);
    DLog(@"No. of Merchants (new) = %lu", (unsigned long)[merchantNameArray count]);
    [merchantTable reloadData];
    
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //For User Tracking
    /*NSMutableString *reqStr = [[NSMutableString alloc] init];
     [reqStr appendFormat:@"<AddSLHistoryItems><productID>%@</productID><mainMenuID>%@</mainMenuID>",[defaults valueForKey:KEY_PRODUCTID],[defaults valueForKey:KEY_MAINMENUID]];
     
     NSMutableString *urlString = [[BASE_URL mutableCopy] ;
     [urlString appendString:@"shoppingList/utonstoclick"];*/
    
    //For User Tracking
    
    if (indexPath.section == 0 && [retailerId count]>0 ) {
        if ([buyUrlArray count] == 0) {
            return;
        }
        
        //For User Tracking
        /*[reqStr appendFormat:@"<retailerName>%@</retailerName></AddSLHistoryItems>",[retailerName objectAtIndex:indexPath.row]];
         
         NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];*/
        //For User Tracking
        
        //navigating to WebBrowserViewController which is under Hot Deals
        [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
        [defaults setObject:[buyUrlArray objectAtIndex:indexPath.row] forKey:KEY_URL];
        WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:urlDetail animated:NO];
        //[urlDetail release];
        
        return;
    }
    
    if (indexPath.row == [merchantNameArray count]) { // More records to be shown...pagination
        [self updateRetailersList];
        return;
    }
    else {
        
        //For User Tracking
        /*[reqStr appendFormat:@"<retailerName>%@</retailerName></AddSLHistoryItems>",[merchantNameArray objectAtIndex:indexPath.row]];
         
         NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
         NSData *urlData = [responseXml dataUsingEncoding:NSUTF8StringEncoding];
         
         DLog(@"%@",urlData);*/
        //For User Tracking
        
        //navigating to WebBrowserViewController which is under Hot Deals
        [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
        [defaults setObject:[urlArray objectAtIndex:indexPath.row] forKey:KEY_URL];
        WebBrowserViewController *urlDetails = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:urlDetails animated:NO];
        //[urlDetails release];
    }
    
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


@end


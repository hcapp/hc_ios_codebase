//
//  FSImageViewController.m
//  Scansee
//
//  Created by Chance Ivey on 7/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FSImageViewController.h"
#define ZOOM_STEP 1.5
@interface FSImageViewController (UtilityMethods)
- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center;
@end

@implementation FSImageViewController
@synthesize scrollView, imageView;
@synthesize singleTap, doubleTap, twoFingerTap;
@synthesize imgUrlString;



- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
// Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)doneAction:(id)sender
{
[self dismissViewControllerAnimated:YES completion:nil];

}

- (void)viewDidLoad
{
[super viewDidLoad];

// Do any additional setup after loading the view, typically from a nib.


/*UIToolbar *toolBar;
 toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
 toolBar.frame = CGRectMake(0, 0, 320, 50);
 toolBar.barStyle = UIBarStyleBlackOpaque;
 [toolBar sizeToFit];      
 UIBarButtonItem *flexibleSpace = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
 UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"DONE" style:UIBarButtonItemStyleBordered target:self action:@selector(doneAction:)];
 
 
 NSArray *barButton  =   [[NSArray alloc] initWithObjects:flexibleSpace,doneButton,nil];
 [toolBar setItems:barButton];
 
 [self.view addSubview:toolBar];
 [toolBar release];
 [barButton release];
 [doneButton release];
 barButton = nil;*/

scrollView.bouncesZoom = YES;
scrollView.delegate = self;
scrollView.clipsToBounds = YES;
scrollView.showsHorizontalScrollIndicator = NO;
scrollView.showsVerticalScrollIndicator = NO;
NSURL *url = [NSURL URLWithString:imgUrlString];
NSData *imgData = [NSData dataWithContentsOfURL:url];
DLog(@"%@", imgUrlString);
imageView = [[[UIImageView alloc]initWithImage:[UIImage imageWithData:imgData]] retain];

imageView.contentMode = UIViewContentModeScaleToFill;
imageView.userInteractionEnabled = YES;
imageView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth );
imageView.contentMode = UIViewContentModeScaleAspectFit;
[scrollView addSubview:imageView];


// add gesture recognizers to the image view
//singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
//doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
//twoFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoFingerTap:)];

//[doubleTap setNumberOfTapsRequired:2];
//[twoFingerTap setNumberOfTouchesRequired:2];

//[imageView addGestureRecognizer:singleTap];
//[imageView addGestureRecognizer:doubleTap];
//[imageView addGestureRecognizer:twoFingerTap];



// calculate minimum scale to perfectly fit image width, and begin at that scale
float minimumScale = [scrollView frame].size.width  / [imageView frame].size.width;
scrollView.maximumZoomScale = 1.0;
scrollView.minimumZoomScale = minimumScale;
scrollView.zoomScale = minimumScale;



}

#pragma mark UIScrollViewDelegate methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
return imageView;
}

#pragma mark TapDetectingImageViewDelegate methods

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {

// zoom in
float newScale = [scrollView zoomScale] * ZOOM_STEP;
CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
[scrollView zoomToRect:zoomRect animated:YES];
}

- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {

}

- (void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer {
// two-finger tap zooms out
float newScale = [scrollView zoomScale] / ZOOM_STEP;
CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
[scrollView zoomToRect:zoomRect animated:YES];
}

#pragma mark Utility methods

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {

CGRect zoomRect;

// the zoom rect is in the content view's coordinates. 
//    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
//    As the zoom scale decreases, so more content is visible, the size of the rect grows.
zoomRect.size.height = [scrollView frame].size.height / scale;
zoomRect.size.width  = [scrollView frame].size.width  / scale;

// choose an origin so as to get the right center.
zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);

return zoomRect;
}

- (void)viewDidUnload
{
[super viewDidUnload];
//[imageView release];
// Release any retained subviews of the main view.
// e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    DLog(@"scroll Frame %@", scrollView);
    DLog(@"Imagev Frame %@", imageView);
    float imageViewXvalue = 0;
    float imageViewYvalue = 0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        imageViewXvalue = 210.0;
        imageViewYvalue = 250.0;
    }
    imageView.frame = CGRectMake(imageViewXvalue,imageViewYvalue, 260, 360);
}

- (void)viewWillDisappear:(BOOL)animated
{
[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
// Return YES for supported orientations
return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
- (void)dealloc
{
    [doubleTap release];
    [imageView release];
    [imgUrlString release];
    [singleTap release];
    [twoFingerTap release];
    [super dealloc];
}
@end

//
//  FindNearByResultListMapViewController.h
//  Scansee
//
//  Created by Chaitra on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MKMapView.h>
#import "CommonUtility.h"
#import <CoreLocation/CoreLocation.h>
#import "HubCitiSegmentedControl.h"

@class DisplayMap;

@interface FindNearByResultListMapViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,CLLocationManagerDelegate,MKMapViewDelegate,CustomizedNavControllerDelegate> {
    
    NSMutableArray *locationMeasurements;
    CLLocation *bestEffortAtLocation;
    CLLocationManager *locationManager;
	NSMutableArray *retailersArray,*retListIDArray;
	NSMutableArray *distanceArray;
	NSMutableArray *priceArray;
	NSMutableArray *latitudeArray, *longitudeArray;
	NSMutableArray *annArray;
	NSMutableArray *poweredByArray;
    NSMutableArray *phone;
    NSMutableArray *address;
    NSMutableArray *retailerUrl;
    NSMutableArray *bannerAdImgPath;
    
	IBOutlet UITableView *listTableView;
	
    NSString *elemName, *elemName1, *elemName2, *elemName3, *elemName4, *elemName5;
    NSString *imgPathDir,*imgPathWeb;
    NSString *stockHeaderDir;
    NSString *stockHeaderWeb;
	NSMutableString *currentStringValue, *currentStringValue1, *currentStringValue2, *currentStringValue3, *currentStringValue4,*currentStringValue5;
    NSString *latitudeValue;
    NSString *longitudeValue;
	
	IBOutlet MKMapView *mapView;
	//IBOutlet UISegmentedControl *segmentedControl;
    IBOutlet UIButton *aboutBtn;
    IBOutlet UIButton *wishBtn;
    IBOutlet UIButton *prefBtn;
    IBOutlet UIButton *rateBtn;
	NSIndexPath *selectedIndex;
    HubCitiSegmentedControl *segmentedControl;
}

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSMutableArray *locationMeasurements;
@property (nonatomic, strong) CLLocation *bestEffortAtLocation;

@property(nonatomic,strong) UITableView *listTableView;

@property(nonatomic,strong)IBOutlet MKMapView *mapView;
//@property(nonatomic, retain) UISegmentedControl *segmentedControl;
@property (nonatomic,strong) NSString *stockHeaderDir;
@property (nonatomic,strong) NSString *stockHeaderWeb;
@property (nonatomic,strong) NSString *imgPathDir;
@property (nonatomic,strong) NSString *imgPathWeb;

@property (nonatomic, strong) NSMutableArray *retIdCopyArray,*retLocIdCopyArray;

-(IBAction)rateShareClicked:(id)sender;
-(IBAction)wishListClicked:(id)sender;
-(IBAction)aboutClicked:(id)sender;
-(IBAction) preferences : (id) sender;

@end

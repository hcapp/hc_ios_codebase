//
//  FindNearByResultListMapViewController.m
//  Scansee
//
//  Created by Chaitra on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FindNearByResultListMapViewController.h"
#import "AppDelegate.h"
#import "RetailerAnnotation.h"
#import "FindNearbyResultCustomCell.h"
#import "MainMenuViewController.h"
#import "FindNearByRetailerDetails.h"
#import "AboutAndPrivacyScreen.h"
#import "RateShareViewController.h"
#import "SettingsViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "CLLocation (Strings).h"
#import "RetailerSummaryViewController.h"
#import "LoginViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@implementation FindNearByResultListMapViewController

//@synthesize segmentedControl;
@synthesize listTableView;
@synthesize mapView,stockHeaderDir,stockHeaderWeb,imgPathDir,imgPathWeb;
@synthesize locationManager;
@synthesize locationMeasurements;
@synthesize bestEffortAtLocation;
@synthesize retIdCopyArray,retLocIdCopyArray;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

#pragma mark View Lifecycle methods
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    [defaults setValue:nil forKey:@"productpageresponse"];
    mapView.hidden = YES;
    annArray = [[NSMutableArray alloc] init];
    
    self.title = NSLocalizedString(@"Local Stores",@"Local Stores");
    listTableView.backgroundColor = [UIColor clearColor];
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    listTableView.backgroundColor = [UIColor clearColor];
    listTableView.delegate = self;
    listTableView.dataSource = self;
    mapView.delegate = self;
    
    [self addsegementcontrolleronView];
    
    [aboutBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_about.png"] forState:UIControlStateNormal];
    [aboutBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_about.png"] forState:UIControlStateHighlighted];
    [aboutBtn addTarget:self action:@selector(aboutClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [wishBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_wishlist.png"] forState:UIControlStateNormal];
    [wishBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_wishlist.png"] forState:UIControlStateHighlighted];
    [wishBtn addTarget:self action:@selector(wishListClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [prefBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    [prefBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_preferences.png"] forState:UIControlStateHighlighted];
    [prefBtn addTarget:self action:@selector(preferences:) forControlEvents:UIControlEventTouchUpInside];
    
    [rateBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_rateshare.png"] forState:UIControlStateNormal];
    [rateBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_rateshare.png"] forState:UIControlStateHighlighted];
    [rateBtn addTarget:self action:@selector(rateShareClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self allocateArrays];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [listTableView deselectRowAtIndexPath:[listTableView indexPathForSelectedRow] animated:YES];
    
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [SharedManager gps_allow_flag] == YES)
    {
        [defaults setValue:@"" forKey:KEY_LATITUDE];
        [defaults setValue:@"" forKey:KEY_LONGITUDE];
        
        [Location updateLocationinfo:self];
    }
    
    if([defaults valueForKey:@"productpageresponse"])
    {
        [defaults setValue:[defaults valueForKey:@"productpageresponse"] forKey:KEY_RESPONSEXML];
        [defaults setValue:nil forKey:@"productpageresponse"];
    }
    
    [self parse_FindNearByDetails:[defaults  valueForKey:KEY_RESPONSEXML]];
    
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)allocateArrays{
    
    if(retListIDArray == nil)
        retListIDArray = [[NSMutableArray alloc]init];
    
    if (retailersArray == nil)
        retailersArray = [[NSMutableArray alloc]init];
    
    if (distanceArray == nil)
        distanceArray = [[NSMutableArray alloc]init];
    
    if (priceArray == nil)
        priceArray = [[NSMutableArray alloc]init];
    
    if (latitudeArray == nil)
        latitudeArray = [[NSMutableArray alloc]init];
    
    if (longitudeArray == nil)
        longitudeArray = [[NSMutableArray alloc]init];
    
    if (poweredByArray == nil)
        poweredByArray = [[NSMutableArray alloc]init];
    
    if (retailerUrl == nil)
        retailerUrl = [[NSMutableArray alloc]init];
    
    if (phone == nil)
        phone = [[NSMutableArray alloc]init];
    
    if (address == nil)
        address = [[NSMutableArray alloc]init];
    
    if (bannerAdImgPath == nil)
        bannerAdImgPath = [[NSMutableArray alloc]init];
}

-(void)releaseArrays{
    
    /*[retListIDArray release];
     [retailersArray release];
     [distanceArray release];
     [priceArray release];
     [latitudeArray release];
     [longitudeArray release];
     [poweredByArray release];
     [mapView release];
     [phone release];
     [address release];
     [retailerUrl release];
     [bannerAdImgPath release];*/
}

#pragma mark - Parse Method
-(void)parse_FindNearByDetails:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *FindNearByDetailsResultSetElement = [TBXML childElementNamed:@"FindNearByDetailsResultSet" parentElement:tbxml.rootXMLElement];
    
    if (FindNearByDetailsResultSetElement != nil) {
        
        TBXMLElement *imgPathDirectionEle = [TBXML childElementNamed:@"stockImgPathDir" parentElement:FindNearByDetailsResultSetElement];
        NSString * imgDir = @"N/A";
        if(imgPathDirectionEle)
            imgDir = [TBXML textForElement:imgPathDirectionEle];
        imgPathDir = [imgDir copy];
        
        
        TBXMLElement *stockHeaderDirEle = [TBXML childElementNamed:@"stockHeaderDir" parentElement:FindNearByDetailsResultSetElement];
        NSString *sthd = @"N/A";
        if(stockHeaderDirEle)
            sthd = [TBXML textForElement:stockHeaderDirEle];
        stockHeaderDir = [sthd copy];
        
        
        TBXMLElement *stockImgWebEle = [TBXML childElementNamed:@"stockImgPathWebSite" parentElement:FindNearByDetailsResultSetElement];
        NSString *imgWeb = @"N/A";
        if(stockImgWebEle)
            imgWeb = [TBXML textForElement:stockImgWebEle];
        
        imgPathWeb = [imgWeb copy];
        
        
        TBXMLElement *stockHeaderWebEle = [TBXML childElementNamed:@"stockHeaderWebSite" parentElement:FindNearByDetailsResultSetElement];
        NSString * sth = @"N/A";
        if(stockHeaderWebEle)
            sth = [TBXML textForElement:stockHeaderWebEle];
        stockHeaderWeb = [sth copy];
        
        
        TBXMLElement *FindNearByDetailsElement = [TBXML childElementNamed:@"FindNearByDetails" parentElement:FindNearByDetailsResultSetElement];
        
        if (retListIDArray!=nil && retListIDArray.count > 0 ) {
            [retListIDArray removeAllObjects];
        }
        if (retailersArray!=nil && retailersArray.count > 0 ) {
            [retailersArray removeAllObjects];
        }
        if (distanceArray!=nil && distanceArray.count > 0 ) {
            [distanceArray removeAllObjects];
        }
        if (priceArray!=nil && priceArray.count > 0 ) {
            [priceArray removeAllObjects];
        }
        if (latitudeArray!=nil && latitudeArray.count > 0 ) {
            [latitudeArray removeAllObjects];
        }
        if (poweredByArray!=nil && poweredByArray.count > 0 ) {
            [poweredByArray removeAllObjects];
        }
        if (phone!=nil && phone.count > 0 ) {
            [phone removeAllObjects];
        }
        if (address!=nil && address.count > 0 ) {
            [address removeAllObjects];
        }
        if (retailerUrl!=nil && retailerUrl.count > 0 ) {
            [retailerUrl removeAllObjects];
        }
        if (bannerAdImgPath!=nil && bannerAdImgPath.count > 0 ) {
            [bannerAdImgPath removeAllObjects];
        }
        
        while (FindNearByDetailsElement != nil) {
            TBXMLElement *retailerNameElement = [TBXML childElementNamed:KEY_RTLNAME parentElement:FindNearByDetailsElement];
            TBXMLElement *distanceElement = [TBXML childElementNamed:KEY_DISTANCE parentElement:FindNearByDetailsElement];
            TBXMLElement *productPriceElement = [TBXML childElementNamed:KEY_PRODUCTPRICE parentElement:FindNearByDetailsElement];
            TBXMLElement *latitudeElement = [TBXML childElementNamed:@"latitude" parentElement:FindNearByDetailsElement];
            TBXMLElement *longitudeElement = [TBXML childElementNamed:@"longitude" parentElement:FindNearByDetailsElement];
            TBXMLElement *poweredbyElement = [TBXML childElementNamed:@"poweredby" parentElement:FindNearByDetailsElement];
            TBXMLElement *phoneElement = [TBXML childElementNamed:@"phone" parentElement:FindNearByDetailsElement];
            TBXMLElement *addressElementElement = [TBXML childElementNamed:@"address" parentElement:FindNearByDetailsElement];
            TBXMLElement *retailerURLElement = [TBXML childElementNamed:@"retailerUrl" parentElement:FindNearByDetailsElement];
            TBXMLElement *retListIDElement = [TBXML childElementNamed:KEY_RLISTID parentElement:FindNearByDetailsElement];
            
            TBXMLElement *bannerAdImageElement = [TBXML childElementNamed:@"bannerAdImagePath" parentElement:FindNearByDetailsElement];
            
            if(retListIDElement != nil)
                [retListIDArray addObject:[TBXML textForElement:retListIDElement]];
            
            if (retailerNameElement != nil) {
                [retailersArray addObject:[TBXML textForElement:retailerNameElement]];
            }
            
            if (distanceElement != nil) {
                [distanceArray addObject:[TBXML textForElement:distanceElement]];
            }
            
            if (productPriceElement != nil) {
                [priceArray addObject:[TBXML textForElement:productPriceElement ]];
            }
            
            if (latitudeElement != nil) {
                [latitudeArray addObject:[TBXML textForElement:latitudeElement]];
            }
            
            if (longitudeElement != nil) {
                [longitudeArray addObject:[TBXML textForElement:longitudeElement]];
            }
            
            if (poweredbyElement != nil) {
                [poweredByArray addObject:[TBXML textForElement:poweredbyElement]];
            }
            else {
                [poweredByArray addObject:@"ScanSee"];//in case of scansee data
            }
            
            if (phoneElement != nil) {
                [phone addObject:[TBXML textForElement:phoneElement]];
            }
            if (addressElementElement != nil) {
                [address addObject:[TBXML textForElement:addressElementElement]];
            }
            if (retailerURLElement != nil) {
                [retailerUrl addObject:[TBXML textForElement:retailerURLElement]];
            }
            
            if (bannerAdImageElement)
                [bannerAdImgPath addObject:[TBXML textForElement:bannerAdImageElement]];
            else
                [bannerAdImgPath addObject:@"N/A"];
            
            FindNearByDetailsElement = [TBXML nextSiblingNamed:@"FindNearByDetails" searchFromElement:FindNearByDetailsElement];
        }
        
    }
    
    //[tbxml release];
}

#pragma mark - segmented control methods
-(void)addsegementcontrolleronView
{
    float listTableViewHeight = 471-50;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        listTableViewHeight = SCREEN_HEIGHT - 100;
    }
    if(segmentedControl)
    {
        [segmentedControl removeFromSuperview];
        //[segmentedControl release];
    }
    // Set the two segement one for Location and another for Product
    NSArray *items = [NSArray arrayWithObjects:@"List",@"Map", nil];
    segmentedControl = [[HubCitiSegmentedControl alloc] initWithItems:items];
    [segmentedControl setFrame:CGRectMake(-7, 0, SCREEN_WIDTH + 14, 44)];//width 334
    [segmentedControl setSelectedSegmentIndex:0];
    [segmentedControl setSegmentColor];
    [segmentedControl addTarget:self action:@selector(changeSegment:) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setBackgroundColor:[UIColor blackColor]];
    
    [self.view addSubview:segmentedControl];
    
    if (IOS7 == NO)
        listTableView.frame = CGRectMake(0, segmentedControl.frame.origin.y+segmentedControl.frame.size.height, SCREEN_WIDTH, listTableViewHeight);
    
}

-(void) changeSegment:(id)sender{
    
    switch (segmentedControl.selectedSegmentIndex) {
        case 0:
        {
            mapView.hidden = YES;
            listTableView.hidden = NO;
            [mapView removeAnnotations:annArray];
        }
            break;
        case 1:
        {
            listTableView.hidden = YES;
            mapView.hidden = NO;
            [mapView setMapType:MKMapTypeStandard];
            [mapView setZoomEnabled:YES];
            [mapView setScrollEnabled:YES];
            [mapView setDelegate:self];
            
            
            if ([latitudeArray count] == 0 || latitudeArray== nil || [longitudeArray count] == 0 || longitudeArray == nil) {
                return;
            }
            CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake([[latitudeArray objectAtIndex:0] doubleValue],
                                                                          [[longitudeArray objectAtIndex:0] doubleValue]);
            
            CLLocationCoordinate2D northEast = southWest;
            for(int i=0; i<[latitudeArray count]; i++) {
                RetailerAnnotation *ann = [[RetailerAnnotation alloc] init];
                ann.coordinate = CLLocationCoordinate2DMake([[latitudeArray objectAtIndex:i] doubleValue],
                                                            [[longitudeArray objectAtIndex:i] doubleValue]);
                [mapView addAnnotation:ann];
                [annArray addObject:ann];
                [ann setTitle:[retailersArray objectAtIndex:i]];
                [ann setSubtitle:[address objectAtIndex:i]];
                
                southWest.latitude = MIN(southWest.latitude, ann.coordinate.latitude);
                southWest.longitude = MIN(southWest.longitude, ann.coordinate.longitude);
                
                northEast.latitude = MAX(northEast.latitude, ann.coordinate.latitude);
                northEast.longitude = MAX(northEast.longitude, ann.coordinate.longitude);
                ReleaseAndNilify(ann);
                
            }
            
            CLLocation *locSouthWest = [[CLLocation alloc] initWithLatitude:southWest.latitude longitude:southWest.longitude];
            CLLocation *locNorthEast = [[CLLocation alloc] initWithLatitude:northEast.latitude longitude:northEast.longitude];
            
            // This is a diag distance (if you wanted tighter you could do NE-NW or NE-SE)
            CLLocationDistance meters = [locSouthWest distanceFromLocation:locNorthEast];
            
            MKCoordinateRegion region;
            region.center.latitude = (southWest.latitude + northEast.latitude) / 2.0;
            region.center.longitude = (southWest.longitude + northEast.longitude) / 2.0;
            region.span.latitudeDelta = meters / 111319.5;
            region.span.longitudeDelta = 0.0;
            
            //Zoom out a little if only one location
            if ([latitudeArray count] == 1) {
                region.span.latitudeDelta = 0.0125;
            }
            MKCoordinateRegion savedRegion = [mapView regionThatFits:region];
            [mapView setRegion:savedRegion animated:YES];
            
            //[locSouthWest release];
            //[locNorthEast release];
        }
            break;
        default:
            break;
    }
}

#pragma mark - Navigation Bar methods
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}



-(void)returnToMainPage:(id)sender{
    
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
    
}

#pragma mark - tableview delegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        return 70.0;
    }
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedIndex = indexPath;
    
    if([defaults valueForKey:KEY_RESPONSEXML])
    {
        [defaults setValue:[defaults valueForKey:KEY_RESPONSEXML] forKey:@"productpageresponse"];
    }
    
    //In case of Retelligence data
    if (![[poweredByArray objectAtIndex:indexPath.row] isEqualToString:@"ScanSee"]){
        if([[HubCitiAppDelegate selIndexArrayFind] count])
            [HubCitiAppDelegate  RefreshIndexFindArray];
        
        if([address count] > indexPath.row)
            [[HubCitiAppDelegate selIndexArrayFind] addObject:[address objectAtIndex:indexPath.row]];
        
        if([phone count] > indexPath.row)
            [[HubCitiAppDelegate selIndexArrayFind] addObject:[phone objectAtIndex:indexPath.row]];
        
        if([priceArray count] > indexPath.row)
            [[HubCitiAppDelegate selIndexArrayFind] addObject:[priceArray objectAtIndex:indexPath.row]];
        
        if([retailerUrl count] > indexPath.row)
            [[HubCitiAppDelegate selIndexArrayFind] addObject:[retailerUrl objectAtIndex:indexPath.row]];
        
        if(stockHeaderDir)
            [[HubCitiAppDelegate selIndexArrayFind] addObject:stockHeaderDir];
        
        if(stockHeaderWeb)
            [[HubCitiAppDelegate selIndexArrayFind] addObject:stockHeaderWeb];
        
        if(imgPathDir)
            [[HubCitiAppDelegate selIndexArrayFind] addObject:imgPathDir];
        
        if(imgPathWeb)
            [[HubCitiAppDelegate selIndexArrayFind] addObject:imgPathWeb];
        
        if([latitudeArray count] > indexPath.row)
            [[HubCitiAppDelegate selIndexArrayFind] addObject:[latitudeArray objectAtIndex:indexPath.row]];
        
        if([longitudeArray count] > indexPath.row)
            [[HubCitiAppDelegate selIndexArrayFind] addObject:[longitudeArray objectAtIndex:indexPath.row]];
        
        if([distanceArray count] > indexPath.row)
            [[HubCitiAppDelegate selIndexArrayFind] addObject:[distanceArray objectAtIndex:indexPath.row]];
        
        if ([bannerAdImgPath count] > indexPath.row)
            [[HubCitiAppDelegate selIndexArrayFind] addObject:[bannerAdImgPath objectAtIndex:indexPath.row]];
        
        DLog(@"%@",[[HubCitiAppDelegate selIndexArrayFind] description]);
        
        if([retailersArray count] > indexPath.row)
            [defaults setObject:[retailersArray objectAtIndex:indexPath.row] forKey:KEY_RTLNAME];
        
        FindNearByRetailerDetails *findBestBuy = [[FindNearByRetailerDetails alloc]initWithNibName:@"FindNearByRetailerDetails" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:findBestBuy animated:NO];
        //  [findBestBuy release];
        [listTableView deselectRowAtIndexPath:selectedIndex animated:YES];
        
    }
    
    //in case of ScanSee data
    else {
        
        NSMutableString *requestStr = [[NSMutableString alloc] init];
        [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
        [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[[HubCitiAppDelegate getRetLocIdArray]objectAtIndex:indexPath.row]];
        
        if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [SharedManager gps_allow_flag] == YES && [[defaults valueForKey:KEY_LATITUDE]length]){
            [requestStr appendFormat:@"<latitude>%@</latitude><longitude>%@</longitude>",[defaults  valueForKey:KEY_LATITUDE],
             [defaults  valueForKey:KEY_LONGITUDE]];
        }
        
        else if ([defaults  valueForKey:@"ZipCode"])
            [requestStr appendFormat:@"<postalCode>%@</postalCode>",[defaults  valueForKey:@"ZipCode"]];
        
        // @Deepak: Added this code for "getretsummary" in UserTracking
        [requestStr appendFormat:@"<scanTypeId>%@</scanTypeId>",@"0"];
        if([defaults valueForKey:KEY_MAINMENUID])
            [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
        if([retListIDArray count] > indexPath.row)
        {
            [requestStr appendFormat:@"<retListId>%@</retListId>",[retListIDArray objectAtIndex:indexPath.row]];
            [defaults setObject:[retListIDArray objectAtIndex:indexPath.row] forKey:KEY_RLISTID];
        }
        
        [requestStr appendFormat:@"<retailerId>%@</retailerId>",[[HubCitiAppDelegate getRetIdArray]objectAtIndex:indexPath.row]];
        if([defaults valueForKey:KEY_HUBCITIID])
            [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
        NSMutableString *urlString = [BASE_URL mutableCopy] ;
        [urlString appendString:@"thislocation/retsummary"];
        
        DLog(@"requestStr = %@",requestStr);
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
        ReleaseAndNilify(requestStr);
        
        if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]){
            ReleaseAndNilify(responseXml);
            return;
        }
        
        TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil && ![[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            ReleaseAndNilify(responseXml);
            return;
        }
        else {
            
            [defaults  setObject:responseXml forKey:KEY_RESPONSEXML] ;
            ReleaseAndNilify(responseXml);
            RetailerSummaryViewController *retailerSummaryPage = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
            
            if([[[HubCitiAppDelegate navigationController]viewControllers]count] > 0)
                [[HubCitiAppDelegate navigationController] pushViewController:retailerSummaryPage animated:NO];
            else
                [self.navigationController pushViewController:retailerSummaryPage animated:NO];
            
            //  [retailerSummaryPage release];
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [retailersArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *MyIdentifier = @"Cell";
    
    FindNearbyResultCustomCell *cell = (FindNearbyResultCustomCell *) [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[FindNearbyResultCustomCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MyIdentifier] ;
    }
    
    if(indexPath.row<[retailersArray count])
        cell.titleLabel.text = [retailersArray objectAtIndex:indexPath.row];
    cell.titleLabel.backgroundColor = [UIColor clearColor];
    
    if(indexPath.row<[distanceArray count]){
        cell.subTitleLabel.text = [NSString stringWithFormat:@"%@ %@", [distanceArray objectAtIndex:indexPath.row],@"miles"];
        cell.subTitleLabel.font = [UIFont systemFontOfSize:14];
        cell.subTitleLabel.backgroundColor = [UIColor clearColor];
    }
    if(indexPath.row<[priceArray count]){
        if(![[priceArray objectAtIndex:indexPath.row] isEqualToString:@"N/A"])
            cell.priceLabel.text = [NSString stringWithFormat:@"%@",[priceArray objectAtIndex:indexPath.row]];
        cell.priceLabel.backgroundColor = [UIColor clearColor];
    }
    
    if(indexPath.row<[poweredByArray count] && ![[poweredByArray objectAtIndex:indexPath.row]isEqualToString:@"ScanSee"]){
        UILabel *poweredBy = [[UILabel alloc]initWithFrame:CGRectMake(20, 50, SCREEN_WIDTH - 120, 10)];//width 200
        poweredBy.font = [UIFont systemFontOfSize:10];
        poweredBy.backgroundColor = [UIColor clearColor];
        poweredBy.textColor = [UIColor grayColor];
        NSString *str = NSLocalizedString(@"Powered by:",@"Powered by:");
        str = [str stringByAppendingString:[poweredByArray objectAtIndex:indexPath.row]];
        poweredBy.text = str;
        
        [cell addSubview:poweredBy];
        ReleaseAndNilify(poweredBy);
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

#pragma mark - mapview delegate methods
-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    MKPinAnnotationView* pinView = nil;
    
    if ([annotation isKindOfClass:[RetailerAnnotation class]])
    {
        // try to dequeue an existing pin view first
        static NSString* RetailerAnnotationIdentifier = @"retailerAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:RetailerAnnotationIdentifier];
        
        if (!pinView)
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
                                                  initWithAnnotation:annotation reuseIdentifier:RetailerAnnotationIdentifier] ;
            customPinView.pinColor = MKPinAnnotationColorPurple;
            customPinView.animatesDrop = YES;
            customPinView.canShowCallout = YES;
            // UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            //customPinView.rightCalloutAccessoryView = infoButton;
            
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
    }
    pinView.canShowCallout = YES;
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    DLog(@"calloutAccessoryControlTapped:");
}

#pragma mark - Botton Buttons action
-(IBAction)preferences : (id) sender{
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert];
    }
    else {
        SettingsViewController *settings = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:settings animated:NO];
        //[settings release];
        
    }
}

-(IBAction)aboutClicked:(id)sender{
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert];
    }else {
        AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
        privacy.responseXml =[NSMutableString stringWithFormat:@"%@About_ScanSee.html",BASE_URL];//[responseXml copy];
        [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
        privacy.comingFromPrivacyScreen = NO;
        [self.navigationController pushViewController:privacy animated:NO];
        //	[privacy release];
    }
}
-(IBAction)wishListClicked:(id)sender{
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", [defaults  valueForKey:KEY_PRODUCTID]];
    [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName>", [defaults  valueForKey:KEY_PRODUCTNAME]];
    [requestStr appendString:@"</ProductDetail></productDetails>"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID>", [defaults  valueForKey:KEY_MAINMENUID]];
    [requestStr appendString:@"</ProductDetailsRequest>"];
    
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendFormat:@"wishlist/addWishListProd"];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
    //[requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:responseXml]){
        ReleaseAndNilify(responseXml);
        return;
    }
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
        
        UIAlertController * alert;
        alert=[UIAlertController alertControllerWithTitle:@" " message:[TBXML textForElement:responseTextElement] preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:nil];
        [UtilityManager performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:1.5];
        [defaults  removeObjectForKey:KEY_SEARCHFROM];
    }
    else {
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        }
    
    //[responseXml release];
}


-(IBAction)rateShareClicked:(id)sender{
    
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        if([Network currentReachabilityStatus]==0)
        {
            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
        }else {
            
            //[[ScanseeManager sharedManager]setHotDealToolBar:NO];
            //[defaults  setObject:prodId forKey:KEY_PRODUCTID];
            RateShareViewController *rateShareVC = [[RateShareViewController alloc] initWithNibName:@"RateShareViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:rateShareVC animated:NO];
            //[rateShareVC release];
        }
    }
}



-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}




#pragma mark - Location Service Methods
-(void) updatedLatlongValue :(NSString *) latValue longitudeValue:(NSString *)longValue{
    
    [defaults setBool:YES forKey:@"gpsEnabled"];
    [defaults setObject:latValue forKey:KEY_LATITUDE];
    [defaults setObject:longValue forKey:KEY_LONGITUDE];
    
    //Added the setting for these keys because its what the Map is using.
    [defaults  setValue:latValue forKey:@"LatVal"];
    [defaults  setValue:longValue forKey:@"LongVal"];
}

@end

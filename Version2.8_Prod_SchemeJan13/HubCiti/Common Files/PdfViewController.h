//
//  PdfViewController.h
//  Scansee
//
//  Created by ajit on 8/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"

@interface PdfViewController : UIViewController<UIWebViewDelegate> {
	IBOutlet UIActivityIndicatorView *activityIndicator;

}
@property(nonatomic,strong)UIActivityIndicatorView *activityIndicator;
@end

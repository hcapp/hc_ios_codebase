//
//  NSString+RemoveHtml.h
//  HubCiti
//
//  Created by Ajit Nadig on 1/29/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RemoveHtml)
+(NSString*)removeHtmlTags:(NSString*)string;

@end

//
//  FindNearByRetailerDetails.h
//  Scansee
//
//  Created by Sandeep S on 11/17/11.
//  Copyright 2011 span infotech india ptv. ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MKMapView.h>
#import "CommonUtility.h"
#import <CoreLocation/CoreLocation.h>

@interface FindNearByRetailerDetails : UIViewController<UITableViewDelegate,UITableViewDataSource,CustomizedNavControllerDelegate>
{
    
    IBOutlet UIButton *callbtn;
    //Chance's Edits
    
    IBOutlet UIButton *getDirections;
    IBOutlet UIButton *callStore;
    IBOutlet UIButton *website;

    //End Chance's Edits
    
    NSString *address;
    NSString *phoneNum;
    NSString *price;
    NSString *RetailerURL;
    NSString *headDirection;
    NSString *headWeb;
    NSString *imgPathDir;
    NSString *imgPathWeb;
    NSString *latitude;
    NSString *longitude;
    NSString *m_lat;
    NSString *m_long;
    UITableView *retDetailsTV;
     
}
-(void) callPressed: (id)sender;

@end

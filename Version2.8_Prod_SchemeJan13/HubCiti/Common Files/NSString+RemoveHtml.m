//
//  NSString+RemoveHtml.m
//  HubCiti
//
//  Created by Ajit Nadig on 1/29/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import "NSString+RemoveHtml.h"

@implementation NSString (RemoveHtml)
+(NSString*)removeHtmlTags:(NSString*)string{
    
    string = [string stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    
    string = [string stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    
    string = [string stringByReplacingOccurrencesOfString:@"&gt" withString:@">"];
    
    string = [string stringByReplacingOccurrencesOfString:@"&amp" withString:@"&"];
    
    string = [string stringByReplacingOccurrencesOfString:@"&cent" withString:@"¢"];
    
    string = [string stringByReplacingOccurrencesOfString:@"&pound;" withString:@"£"];
    
    string = [string stringByReplacingOccurrencesOfString:@"&yen;" withString:@"¥"];
    
    string = [string stringByReplacingOccurrencesOfString:@"&euro;" withString:@"€"];
    
    string = [string stringByReplacingOccurrencesOfString:@"&copy;" withString:@"©"];
    
    string = [string stringByReplacingOccurrencesOfString:@"&reg;" withString:@"®"];

    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:string];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        string = [string stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return string;
}


@end

//
//  AudioVideoList.m
//  ScanSee
//
//  Created by ajit on 5/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AudioVideoList.h"
#import "PdfViewController.h"
#import "AudioPlayer.h"
#import "MainMenuViewController.h"
#import "WebBrowserViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
@implementation AudioVideoList

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    if ([[defaults valueForKey:@"Display"]isEqualToString:@"Videos"]) {
        self.title = NSLocalizedString(@"Product Videos",@"Product Videos");
    }
    else if([[defaults valueForKey:@"Display"]isEqualToString:@"Audios"]) {
        self.title = NSLocalizedString(@"Product Audios",@"Product Audios");
    }
    else{
        self.title = NSLocalizedString(@"Other Info",@"Other Info");
    }
    
    //customize back button
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];

    self.navigationItem.hidesBackButton = YES;
    // //[back release];
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    // //[mainPage release];
    
    
    mediaPathArray = [[NSMutableArray alloc]init];
    mediaArray = [[NSMutableArray alloc]init];
    pmLstIDArray = [[NSMutableArray alloc]init];
    
    NSString *responseXml = [[NSString alloc] initWithFormat:@"%@",[defaults objectForKey:KEY_RESPONSEXML]];
    [self parseResponseXml:responseXml];
    ReleaseAndNilify(responseXml);
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
}

-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}
#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    return [mediaPathArray count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    }
    if([mediaArray count] > indexPath.row){
        cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
        // if (![[mediaPathArray objectAtIndex:indexPath.row] isEqualToString:@"N/A"])
        // cell.textLabel.text = [mediaArray objectAtIndex:indexPath.row];
        // else
        //cell.textLabel.text = [mediaPathArray objectAtIndex:indexPath.row];
        
        NSString *list = [mediaPathArray objectAtIndex:indexPath.row];
        listItems = [list componentsSeparatedByString:@"/"];
        cell.textLabel.text = [listItems objectAtIndex:[listItems count]-1];
    }
    return cell;
}



-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DLog(@"Medianame array:%@", mediaArray);
    DLog(@"MediaPath array:%@", mediaPathArray);
    
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    else {
        
        //@Deepak: added new service call for usertracking
        
        NSMutableString *urlString = [BASE_URL mutableCopy] ;
        [urlString appendFormat:@"find/utpmclick?pmListID=%@",[pmLstIDArray objectAtIndex:indexPath.row]];
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
        DLog(@"%@",responseXml);
        ReleaseAndNilify(responseXml);
        
        if ([[defaults valueForKey:@"Display"]isEqualToString:@"Videos"]) {
            
            NSString *videoURLString = [mediaPathArray objectAtIndex:indexPath.row];
            //NSString *videoURLString = @"http://122.181.128.152:8080/Images/manufacturer/13932/Video_Buttons_Under_Construction.mp4";
            videoURLString = [videoURLString stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
            // NSURL *videoURL = [NSURL URLWithString:videoURLString];
            DLog(@"VIDEO URL:%@", videoURLString);
            //            moviePlayerView = [[[MPMoviePlayerViewController alloc] initWithContentURL:videoURL] ;
            //            [moviePlayerView setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            //            [self presentViewController:moviePlayerView animated:NO];
            [defaults setValue:videoURLString forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            ////[urlDetail release];
            
        }
        else if([[defaults valueForKey:@"Display"]isEqualToString:@"Audios"]) {
            
            [defaults setValue:[mediaPathArray objectAtIndex:indexPath.row] forKey:@"MediaPath"];
            NSString *list = [mediaPathArray objectAtIndex:indexPath.row];
            listItems = [list componentsSeparatedByString:@"/"];
            [defaults setValue:[listItems objectAtIndex:[listItems count]-1] forKey:@"MediaName"];
            AudioPlayer *audioPlay = [[AudioPlayer alloc]initWithNibName:@"AudioPlayer" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:audioPlay animated:NO];
            //[audioPlay release];
        }
        else{
            
            [defaults setValue:[mediaPathArray objectAtIndex:indexPath.row] forKey:@"MediaPath"];
            [defaults setValue:[mediaArray objectAtIndex:indexPath.row] forKey:@"MediaName"];
            PdfViewController *pdfInfo = [[PdfViewController alloc]initWithNibName:@"PdfViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:pdfInfo animated:NO];
            // [pdfInfo release];
            
        }
    }
    [tv deselectRowAtIndexPath:[tv indexPathForSelectedRow] animated:YES];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}



-(void) parseResponseXml : (NSString *) response

{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *productsElement = [TBXML childElementNamed:KEY_PRODUCTDETAIL parentElement:tbXml.rootXMLElement];
    
    while (productsElement) {
        TBXMLElement *productMediaPathElement = [TBXML childElementNamed:@"productMediaPath" parentElement:productsElement];
        TBXMLElement *productMediaNameElement = [TBXML childElementNamed:@"productMediaName" parentElement:productsElement];
        TBXMLElement *pmLstIDElement = [TBXML childElementNamed:@"pmLstId" parentElement:productsElement];
        
        if(productMediaPathElement)
            [mediaPathArray addObject:[TBXML textForElement:productMediaPathElement]];
        if(productMediaNameElement)
            [mediaArray addObject:[TBXML textForElement:productMediaNameElement]];
        if(pmLstIDElement)
            [pmLstIDArray addObject:[TBXML textForElement:pmLstIDElement]];
        
        productsElement = [TBXML nextSiblingNamed:KEY_PRODUCTDETAIL searchFromElement:productsElement];
        
    }
}

@end


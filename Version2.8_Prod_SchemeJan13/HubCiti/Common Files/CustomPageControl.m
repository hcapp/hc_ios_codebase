//
//  CustomPageControl.m
//  Scansee
//
//  Created by Chaitra on 03/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomPageControl.h"

@implementation CustomPageControl

@synthesize activeImage , inactiveImage, view;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        activeImage = [UIImage imageNamed:@"disc_img_dark.png"];
        inactiveImage = [UIImage imageNamed:@"disc_img_light.png"];
//        self.backgroundColor = [UIColor clearColor];
//        self.dotColorCurrentPage = [UIColor blueColor];
//        self.dotColorOtherPage = [UIColor redColor];
    }
    return self;
}
-(void)updateDots
{
    for (int i = 0; i < [self.subviews count]; i++)
    {
        DLog(@"%@,%d",self.subviews,i);
        view = [self.subviews objectAtIndex:i];
       //UIImageView* dot = [self.subviews objectAtIndex:i];
        UIImageView* dotImage = [[UIImageView alloc]initWithImage:inactiveImage];
        if (i == self.currentPage){
            dotImage.image = activeImage;
            
        }
       // else{
           // dotImage.image = inactiveImage;
       // }
        [view addSubview:dotImage];
    }
}

-(void)setCurrentPage:(NSInteger)page
{
    [super setCurrentPage:page];
    [self updateDots];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

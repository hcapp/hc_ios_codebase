//
//  ProductInformation.h
//  Scansee
//
//  Created by ajit on 10/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"
#import "CustomPageControl.h"
#import "FSImageViewController.h"
#import <MessageUI/MessageUI.h>
#import "DWBubbleMenuButton.h"
@class AnyViewController;
@class EmailShareViewController;
@interface ProductInformation : UIViewController<UITableViewDelegate,UITableViewDataSource ,UIScrollViewDelegate,UITextViewDelegate,UIWebViewDelegate,MFMessageComposeViewControllerDelegate,DWBubbleMenuViewDelegate,CustomizedNavControllerDelegate> {
    
	IBOutlet UITableView *attributTable;
	IBOutlet UIScrollView *scrollView ,*imageScroll;
	NSMutableArray *attributeArray;
	NSMutableArray *valueArray;
    
	NSString *productIdStr, *productNameStr, *productLongDescriptionStr,*imagePathStr ,*prodShortDescription ;
	NSString *videoFlagStr, *audioFlagStr,*fileFlagStr ;
	IBOutlet UIButton *audio, *video,*otherInfo;
	CGSize strSize , shortDescSize,warrantyInfoSize;
    NSTimer *timer;
    NSMutableArray *productImageArray;
    int repeatCount;
    CustomPageControl *imagePgcontrol;
    
    UIButton *scrollLeftImg;
    UIButton *scrollRightImg;
    FSImageViewController *fsIVC;
    
    IBOutlet UIToolbar *bottomToolBar;
    
     NSString *androidDownloadLink;
    
    WebRequestState iWebRequestState;
   
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic , strong) UIScrollView *imageScroll;
-(void)parseProductDetailXml;
-(void)renderProductDetailScreen;
-(IBAction)audiosButtonClicked:(id)sender;
-(IBAction)videosButtonClicked:(id)sender;
-(IBAction)otherInfoClicked:(id)sender;
-(void) loadImage;

-(IBAction)scrollImageRight:(id)sender;
-(IBAction)scrollImageLeft:(id)sender;

@end

//
//  ProductInformation.m
//  Scansee
//
//  Created by ajit on 10/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ProductInformation.h"
#import "AudioVideoList.h"
#import "MainMenuViewController.h"
#import "FSImageViewController.h"
#import "UAModalPanel.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@implementation ProductInformation

@synthesize imageScroll,anyVC,emailSendingVC;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    repeatCount = 0;
    attributeArray = [[NSMutableArray alloc]init];
    valueArray = [[NSMutableArray alloc]init];
    productImageArray = [[NSMutableArray alloc] init];
    
    attributTable.delegate = self;
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    // //[mainPage release];
    
    
    //Customizing share button
   
    
    
    bottomToolBar.barTintColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    //customize back button
   

    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    audio.enabled = NO;
    [audio setImage:[UIImage imageNamed:@"Audio-Off"] forState:UIControlStateNormal];
    [audio setImage:[UIImage imageNamed:@"Audio-On"] forState:UIControlStateHighlighted];
    
    video.enabled = NO;
    [video setImage:[UIImage imageNamed:@"Video-Off"] forState:UIControlStateNormal];
    [video setImage:[UIImage imageNamed:@"Video-On"] forState:UIControlStateHighlighted];
    
    otherInfo.enabled = NO;
    [otherInfo setImage:[UIImage imageNamed:@"Otherinfo-Off"] forState:UIControlStateNormal];
    [otherInfo setImage:[UIImage imageNamed:@"Otherinfo-On"] forState:UIControlStateHighlighted];
    
    [scrollView becomeFirstResponder];
    [self parseProductDetailXml];
    [self renderProductDetailScreen];
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    NSInteger height ;
    height = 60 * [attributeArray count];
    
    attributTable.frame = CGRectMake(attributTable.frame.origin.x, attributTable.frame.origin.y, self.view.frame.size.height, height);//320.0
    
    scrollView.backgroundColor = [UIColor clearColor];
    float sizeOfContent = 0;
    int i;
    for (i = 0; i < [scrollView.subviews count]; i++) {
        UIView *view =[scrollView.subviews objectAtIndex:i];
        sizeOfContent += view.frame.size.height;
    }
    
    // Set content size for scroll view
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, sizeOfContent-120);
    
    [self addShareButton];
    
}



-(void)addShareButton{
    // Create down menu button
    UIImageView *homeLabel ;
    
    
    // Create up menu button
    homeLabel = [self createHomeButtonView];
    
    DWBubbleMenuButton *upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
                                                                                          self.view.frame.size.height - homeLabel.frame.size.height - 60.f,
                                                                                          homeLabel.frame.size.width,
                                                                                          homeLabel.frame.size.height)
                                                            expansionDirection:DirectionUp];
    upMenuView.homeButtonView = homeLabel;
    
    upMenuView.delegate = self;
    
    [upMenuView addButtons:[self createDemoButtonArray]];
    
    [self.view addSubview:upMenuView];
    
    [self.view bringSubviewToFront:upMenuView];
    
    upMenuView.onLogOutPressed =^(){
        
        [self logOutPressed];
    } ;
    
    
}




- (UIImageView *)createHomeButtonView {
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
    label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}

- (NSArray *)createDemoButtonArray {
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    
    int i = 0;
    for (NSString *title in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:title] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        
        button.clipsToBounds = YES;
        button.tag = i++;
        
        [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
}

- (void)test:(UIButton *)sender {
    
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
    [self shareClicked];
    
    switch (sender.tag) {
        case 0:{//facebook
            anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
            anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"PRODUCT_SHARE"];
            //[self.view addSubview:anyVC.view];
            [UIView transitionWithView:self.view duration:0.5
                               options:UIViewAnimationOptionTransitionCurlDown
                            animations:^ { [self.view addSubview:anyVC.view]; }
                            completion:nil];
        }
            break;
        case 1:{//twitter
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    if (result == SLComposeViewControllerResultCancelled) {
                        DLog(@"Twitter Result: canceled");
                    } else {
                        DLog(@"Twitter Result: sent");
                        NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><prodId>%@</prodId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PRODUCTID]];
                        
                        [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
                    }
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        
                        [controller.view removeFromSuperview];
                    } else
                        [controller dismissViewControllerAnimated:YES completion:Nil];
                };
                controller.completionHandler =myBlock;
                
                //Adding the Text to the facebook post value from iOS
                [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                
                //Adding the URL to the facebook post value from iOS
                
                //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                
                //Adding the Image to the facebook post value from iOS
                
                [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    [self.view addSubview:controller.view];
                    [self.view bringSubviewToFront:controller.view];
                }else
                    [self presentViewController:controller animated:YES completion:Nil];
                
                
            }
            else {
                [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
                }
        }
            
            
            break;
        case 2:{//text
            
            if ([self hasCellularCoverage])
            {
                if ([MFMessageComposeViewController canSendText]) {
                    
                    MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
                    [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    smsComposerController.messageComposeDelegate = self;
                    [self presentViewController:smsComposerController animated:YES completion:nil];
                }
            }
            else
            {
                [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
            }
            
            
        }
            break;
        case 3:{//email
            //[defaults setValue:PRODUCT_SHARE forKey:KEY_SHARE_TYPE];
            __typeof(self) __weak  obj = self;
            emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
            emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"PRODUCT_SHARE"];
            emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                
                [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
            } ;
            
            [emailSendingVC loadMail];
        }
            break;
        default:
            [defaults setBool:NO forKey:BottomButton];
            break;
    }
    
}

- (UIButton *)createButtonWithName:(NSString *)imageName {
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button sizeToFit];
    
    [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}



- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView{
    
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}






#pragma mark product share parse

-(void)parseProductShare:(NSString*)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        TBXMLElement *productInfo = [TBXML childElementNamed:@"productDetail" parentElement:tbXml.rootXMLElement];
        
        
        TBXMLElement *productName=[TBXML childElementNamed:@"productName" parentElement:productInfo];
        TBXMLElement *emailURL = [TBXML childElementNamed:@"qrUrl" parentElement:productInfo];
        TBXMLElement *imagePath = [TBXML childElementNamed:@"imagePath" parentElement:productInfo];
        
        if (emailURL!=nil) {
            [defaults setObject:[TBXML textForElement:emailURL] forKey:KEY_HYPERLINK];
            NSLog(@"The link to be shared is %@",[defaults objectForKey:KEY_HYPERLINK]);
        }
        
        if (productName!=nil) {
            
            NSMutableString *msgString = [[NSMutableString alloc]init];
            [msgString appendFormat:@"I found this product in the %@ mobile app and thought you might be interested:",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:productName]];
            [msgString appendFormat:@"%@",shareString];
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            NSLog(@"The share message value is %@",[defaults objectForKey:KEY_SHAREMSG]);
        }
        
        if (imagePath!=nil) {
            [defaults setObject:[TBXML textForElement:imagePath] forKey:KEY_PRODUCTIMGPATH];
            NSLog(@"The product image path is %@",[defaults objectForKey:KEY_PRODUCTIMGPATH]);
        }
        
        
        
        
        
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            return;
        }
        
    }
}


#pragma parse response
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case PRODUCT_SHARE: [self parseProductShare:response];
            break;
        case CATEGORIES_SET:
            break;
        case SIGNUP_UPDATEUSER:
            break;
        case GETUSERINFO:
            break;
            case SIGNUP_SUCCESSRESPONSE:
            break;
            case MENUDISPLAY:
            break;
            case MAINMENU_GETCUSTOMUI:
            break;
            case GETPARTNERS:
            break;
            case CITIEXPRET:
            break;
            case SEARCH_LIST:
            break;
            case RETAILER_DETAIL:
            break;
            case CATEGORIES_DISPLAY:
            break;
            case FILTERS_RETAILERLIST:
            break;
            case LOGIN_FORGOTPASSWORD:
            break;
            case LOGIN_AUTHENTICATEUSER:
            break;
            case CHANGE_PASSWORD:
            break;
            case RETSUMMARY:
            break;
            case PARTNERRET:
            break;
            case GETFAVLOCATIONS:
            break;
            case GETFAVCATEGORIES:
            break;
            case HubcitiAnythingInfo:
            break;
            case appsitedetails:
            break;
            case catsearch:
            break;
        case EVENTSLIST:
            break;
            case EVENT_SHARE:
            break;
            case EVENTCATLIST:
            break;
            case singlecatretailers:
            break;
            case FundraisersList:
            break;
            case FUNDRAISER_SHARE:
            break;
            case FUNDRAISER_DETAILS:
            break;
            case FAQ:
            break;
            case FetchCityPreferences:
            break;
            case APPSITE_SHARE:
            break;
            case SPECIALS_REQUEST:
            break;
            case SPECIAL_OFFER_SHARE:
            break;
            case ANYTHING_SHARE:
            break;
            case HOTDEAL_SHARE:
            break;
            case COUPONS_SHARE:
            break;
            case BAND_SHARE:
            break;
            case BAND_CAT_TYPE:
            break;
            case BAND_EVENT_SHARE:
            break;
            case shareapplink:
            break;
            case MAPLOGISTICS:
            break;
            case LOCATIONS_SET:
            break;
            case RESETPASSWORD:
            break;
            case DEALS_REQUEST:
            break;
            case FUNDRAISER_LOCATION:
            break;
            case COMB_TEMP_REQ:
            break;
            case FUNDRAISER_REQUEST:
            break;
            case SORT_FILTER:
            break;
            case BANDS_EVENTS_INFO:
            break;
            case COUPONS_REQUEST:
            break;
            case UPDATE_DEVICE:
            break;
            default:
            break;
          
           
            
    }
    
}
#pragma mark sender user tracking
-(void)userTracking:(NSString*)xmlStr
{
    
    NSMutableString *urlStr = [[NSMutableString alloc]init];
    [urlStr appendFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    DLog(@"%@",urlStr);
    NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:xmlStr]];
    DLog(@"%@",response);
    // [xmlStr release];
}




-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}





#pragma mark action sheet methods
-(void)shareClicked
{
    iWebRequestState = PRODUCT_SHARE;
    
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    
    [reqStr appendFormat:@"<ShareProductInfo><productId>%@</productId>",[defaults valueForKey:KEY_PRODUCTID ]];
    [reqStr appendFormat:@"<userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/shareproductretailerinfo",BASE_URL];
    DLog(@"%@",urlString);
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    
    [self responseData:response];
    
    
}


-(void)shareDetails{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        [self shareClicked];
        
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        // [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
    }
}

-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Product Information Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    //    if(IPAD){
    //        actionSheet.popoverPresentationController.sourceView = btnCalendar;
    //        actionSheet.popoverPresentationController.sourceRect =  btnCalendar.bounds;
    //    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"PRODUCT_SHARE"];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
                NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><prodId>%@</prodId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PRODUCTID]];
                
                [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
        
        
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    //[defaults setValue:PRODUCT_SHARE forKey:KEY_SHARE_TYPE];
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
}


-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
            NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><prodId>%@</prodId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PRODUCTID]];
            
            [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
            
            
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void) loadImage{
    DLog(@"timer called = %lu att arr = %lu val arr= %lu repeatCount = %i",(unsigned long)[productImageArray count],(unsigned long)[attributeArray count] , (unsigned long)[valueArray count],repeatCount);
    if ([productImageArray count] >0) {
        
        if (![[productImageArray objectAtIndex:repeatCount] isEqualToString:@"N/A"]) {
            for(UIView *i_view in [self.view subviews]){
                if ([i_view isKindOfClass:[UIScrollView class]]) {
                    for(UIView *j_view in [i_view subviews]){
                        if ([j_view isKindOfClass:[AsyncImageView class]]) {
                            [j_view removeFromSuperview];
                        }
                    }
                }
            }
        }
        
        AsyncImageView *asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(5, 10, 120, 120)];
        asyncImageView.backgroundColor = [UIColor clearColor];
        [asyncImageView loadImage:[NSString stringWithFormat:@"%@",[productImageArray objectAtIndex:repeatCount]]];
        [scrollView addSubview:asyncImageView];
        
        repeatCount++;
        if (repeatCount == [productImageArray count]) {
            repeatCount = 0;
        }
        //[asyncImageView release];
    }
}
-(void) loadImageToScroll{
    // float x = 0;
    if ([productImageArray count] >0) {
        
        //   if (![[productImageArray objectAtIndex:repeatCount] isEqualToString:@"N/A"]) {
        for(UIView *i_view in [self.view subviews]){
            if ([i_view isKindOfClass:[UIScrollView class]]) {
                for(UIView *j_view in [i_view subviews]){
                    if ([j_view isKindOfClass:[AsyncImageView class]]) {
                        [j_view removeFromSuperview];
                    }
                }
            }
            //      }
        }
        
        //NSURL *url;
        for (int i = 0; i< [productImageArray count]; i++) {
            CGRect frame;
            frame.origin.x = self.imageScroll.frame.size.width * i;
            frame.origin.y = 10;
            frame.size = self.imageScroll.frame.size;
            
            AsyncImageView *asyncImageView = [[AsyncImageView alloc] initWithFrame:frame];
            asyncImageView.backgroundColor = [UIColor clearColor];
            
            //url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[productImageArray objectAtIndex:i]]];
            //[asyncImageView loadImageFromURL:url];
            [asyncImageView loadImage:[NSString stringWithFormat:@"%@",[productImageArray objectAtIndex:i]]];
            [self.imageScroll addSubview:asyncImageView];
            //[asyncImageView release];
            
            UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
            imageButton.frame = frame;
            imageButton.tag = i;
            [imageButton addTarget:self action:@selector(makeImageFullScreen:) forControlEvents:UIControlEventTouchUpInside];
            [self.imageScroll addSubview:imageButton];
            
            
            // x += 120;
        }
        
        
        //            repeatCount++;
        //            if (repeatCount == [productImageArray count]) {
        //                repeatCount = 0;
        //            }
        [self.imageScroll setContentSize:CGSizeMake([self.imageScroll bounds].size.width * imagePgcontrol.numberOfPages,[self.imageScroll bounds].size.height)];
        //  imageScroll.contentSize = CGSizeMake(((120* [productImageArray count]) +5), imageScroll.frame.size.height);
    }
    
    
}

-(void)parseProductDetailXml{
    
    NSString *responseXml = [NSString stringWithFormat:@"%@", [defaults  valueForKey:@"responseXml1"]];
    DLog(@"responseXml1 = %@",responseXml);
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement *productIdElement = [TBXML childElementNamed:KEY_PRODUCTID parentElement:tbxml.rootXMLElement];
    
    if (productIdElement == nil) {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
        
        // Banner the message given by the server
        [UtilityManager showAlert:nil msg:responseTextStr];
        return;
    }
    [defaults  setObject:[TBXML textForElement:productIdElement] forKey:KEY_PRODUCTID];
    TBXMLElement *productNameElement = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:tbxml.rootXMLElement];
    TBXMLElement *productLongDescriptionElement = [TBXML childElementNamed:@"productLongDescription" parentElement:tbxml.rootXMLElement];
    TBXMLElement *productShortDescriptionElement = [TBXML childElementNamed:@"productShortDescription" parentElement:tbxml.rootXMLElement];
    TBXMLElement *imagePathElement = [TBXML childElementNamed:KEY_PRODUCTIMGPATH parentElement:tbxml.rootXMLElement];
    TBXMLElement *videoFlagElement = [TBXML childElementNamed:@"VideoFlag" parentElement:tbxml.rootXMLElement];
    TBXMLElement *audioFlagElement = [TBXML childElementNamed:@"audioFlag" parentElement:tbxml.rootXMLElement];
    TBXMLElement *fileFlagElement = [TBXML childElementNamed:@"fileFlag" parentElement:tbxml.rootXMLElement];
    TBXMLElement *warrantyServiceInfoE = [TBXML childElementNamed:@"warrantyServiceInfo" parentElement:tbxml.rootXMLElement];
    TBXMLElement *modelNumberE = [TBXML childElementNamed:@"modelNumber" parentElement:tbxml.rootXMLElement];
    
    float shortDescSizeFont = 13.0;
    if(DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        shortDescSizeFont = 18.0;
       // strSizeFont = 18.0;
       // warrantyInfoSizeFont = 18.0;
    }
    
    if(productIdElement)
        productIdStr = [[TBXML textForElement:productIdElement]copy];
    
    if(productNameElement)
        productNameStr = [[TBXML textForElement:productNameElement]copy];
    
    if (productShortDescriptionElement!= nil)
    {
        prodShortDescription = [[TBXML textForElement:productShortDescriptionElement]copy];
        
        NSString *descShort = prodShortDescription;
        NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
        paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
        shortDescSize = [descShort boundingRectWithSize: CGSizeMake(180, 400) options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:shortDescSizeFont], NSParagraphStyleAttributeName: paragraph } context: nil].size;
        

        DLog(@"String size: %f",shortDescSize.height);
    }
    
    if (productLongDescriptionElement!= nil)
    {
        productLongDescriptionStr = [[TBXML textForElement:productLongDescriptionElement]copy];
        NSString *desc = productLongDescriptionStr;
        
        NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
        paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
        strSize = [desc boundingRectWithSize: CGSizeMake(180, 800) options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:shortDescSizeFont], NSParagraphStyleAttributeName: paragraph } context: nil].size;

        //strSize = [desc sizeWithFont:[UIFont systemFontOfSize:13]];
        DLog(@"String size: %f",strSize.height);
    }
    if(imagePathElement)
        imagePathStr = [[[TBXML textForElement:imagePathElement]stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]copy];
    
    if(videoFlagElement)
        videoFlagStr = [TBXML textForElement:videoFlagElement];
    
    if(audioFlagElement)
        audioFlagStr = [TBXML textForElement:audioFlagElement];
    
    if(fileFlagElement)
        fileFlagStr = [TBXML textForElement:fileFlagElement];
    
    if(warrantyServiceInfoE != nil && ![[TBXML textForElement:warrantyServiceInfoE]isEqualToString:@"N/A"]){
        //[TBXML textForElement:warrantyServiceInfoE];
        [attributeArray addObject:@"Warranty or Service Information"];
        [valueArray addObject:[TBXML textForElement:warrantyServiceInfoE]];
        
        NSString *warrantyInfo = [valueArray objectAtIndex:0];
        NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
        paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
        warrantyInfoSize = [warrantyInfo boundingRectWithSize: CGSizeMake(180, 400) options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:shortDescSizeFont], NSParagraphStyleAttributeName: paragraph } context: nil].size;

        DLog(@"String size: %f",warrantyInfoSize.height);
    }
    if(modelNumberE != nil && ![[TBXML textForElement:modelNumberE]isEqualToString:@"N/A"]){
        //[TBXML textForElement:warrantyServiceInfoE];
        [attributeArray addObject:@"Model Number"];
        [valueArray addObject:[TBXML textForElement:modelNumberE]];
    }
    TBXMLElement *productAttributesLstElement = [TBXML childElementNamed:@"productAttributeslst" parentElement:tbxml.rootXMLElement];
    if (productAttributesLstElement != nil) {
        
        TBXMLElement *ProdcutAttributesElement = [TBXML childElementNamed:@"ProdcutAttributes" parentElement:productAttributesLstElement];
        while (ProdcutAttributesElement!=nil) {
            
            TBXMLElement *attributeNameElement = [TBXML childElementNamed:@"attributeName" parentElement:ProdcutAttributesElement];
            TBXMLElement *displayValueElement = [TBXML childElementNamed:@"displayValue" parentElement:ProdcutAttributesElement];
            if (attributeNameElement!=nil)
                [attributeArray addObject:[TBXML textForElement:attributeNameElement]];
            if (displayValueElement!=nil)
                [valueArray addObject:[TBXML textForElement:displayValueElement]];
            
            ProdcutAttributesElement = [TBXML nextSiblingNamed:@"ProdcutAttributes" searchFromElement:ProdcutAttributesElement];
        }
    }
    TBXMLElement *productImageslstE = [TBXML childElementNamed:@"productImageslst" parentElement:tbxml.rootXMLElement];
    if (productImageslstE != nil) {
        TBXMLElement *ProductDetailsEle = [TBXML childElementNamed:@"ProductDetails" parentElement:productImageslstE];
        while (ProductDetailsEle) {
            TBXMLElement *productMediaPathElement = [TBXML childElementNamed:@"productMediaPath" parentElement:ProductDetailsEle];
            [productImageArray addObject:[[TBXML textForElement:productMediaPathElement]stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]];
            ProductDetailsEle = [TBXML nextSiblingNamed:@"ProductDetails" searchFromElement:ProductDetailsEle];
        }
    }
    //[tbxml release];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    //  if (scrollView.tag == 5) {
    int page = floor((self.imageScroll.contentOffset.x - self.imageScroll.frame.size.width / 2) / self.imageScroll.frame.size.width) + 1;
    //     int page = (self.imageScroll.contentOffset.x )/120 ;
    //DLog(@"page = %i",page);
    imagePgcontrol.currentPage = page;
    if (page == [productImageArray count]-1) {
        scrollRightImg.hidden = YES;
        scrollLeftImg.hidden = NO;
    }else if (page == 0) {
        scrollLeftImg.hidden = YES;
        scrollRightImg.hidden = NO;
    }else {
        scrollLeftImg.hidden = NO;
        scrollRightImg.hidden = NO;
    }
    // }
    
}

-(IBAction)scrollImageRight:(id)sender
{
    scrollLeftImg.hidden = NO;
    CGRect frame;
    frame.origin.x = self.imageScroll.frame.size.width * (imagePgcontrol.currentPage +1);
    frame.origin.y = 10;
    frame.size = self.imageScroll.frame.size;
    [self.imageScroll scrollRectToVisible:frame animated:YES];
    //    self.imageScroll.contentOffset = CGPointMake(self.imageScroll.contentOffset.x + 120, self.imageScroll.contentOffset.y);
}
-(IBAction)scrollImageLeft:(id)sender{
    scrollRightImg.hidden = NO;
    CGRect frame;
    frame.origin.x = self.imageScroll.frame.size.width * (imagePgcontrol.currentPage -1);
    frame.origin.y = 10;
    frame.size = self.imageScroll.frame.size;
    [self.imageScroll scrollRectToVisible:frame animated:YES];
    //     self.imageScroll.contentOffset = CGPointMake(self.imageScroll.contentOffset.x - 120, self.imageScroll.contentOffset.y);
}

- (void)makeImageFullScreen:(UIButton *)sender
{
    UAModalPanel *modalPanel = [[UAModalPanel alloc] init];
    if (IOS7 && IS_IPHONE5)
        modalPanel.frame = CGRectMake(0, 0, SCREEN_WIDTH, 460);//Width 320
    else
        modalPanel.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 60.0);//Width 320 height 420
    
    fsIVC = [[FSImageViewController alloc] init] ;
    if ([productImageArray count] > 1) {
        DLog(@"This guy clicked it - %ld", (long)sender.tag);
        fsIVC.imgUrlString = [productImageArray objectAtIndex:sender.tag];
    }
    else
    {
        fsIVC.imgUrlString = imagePathStr;
    }
    fsIVC.view.frame = CGRectMake(modalPanel.frame.origin.x+ 30, modalPanel.frame.origin.y+ 30, modalPanel.frame.size.width - 60, modalPanel.frame.size.height-60);
    [modalPanel addSubview:fsIVC.view];
    [self.view addSubview:modalPanel];
    [modalPanel show];
    // [modalPanel release];
}

- (void)renderProductDetailScreen {
    
    float labelFont = 13.0;
   // float titleLabelFont = 16.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
      //  titleLabelFont = 21.0;
        labelFont = 18;
    }
    
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:[NSString stringWithFormat:@"%@",productNameStr] forView:self withHambergur:NO];
    
  
    //[titleLabel release];
    
    //product image
    AsyncImageView *asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(12,10,100,100)];
    asyncImageView.backgroundColor = [UIColor clearColor];
    asyncImageView.layer.shadowColor = [UIColor blackColor].CGColor;
    asyncImageView.layer.shadowOffset = CGSizeMake(0, 1);
    asyncImageView.layer.shadowOpacity = 1;
    asyncImageView.layer.shadowRadius = 5.0;
    asyncImageView.clipsToBounds = NO;
    
    if (![imagePathStr isEqualToString:@"N/A"]) {
        
        [asyncImageView loadImage:imagePathStr];
        [scrollView addSubview:asyncImageView];
        if ([productImageArray count] >1) {
            self.imageScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(17, 10, 120, 120)];
            
            imagePgcontrol = [[CustomPageControl alloc] initWithFrame:CGRectMake(17, 138, 100, 15)];
            imagePgcontrol.hidesForSinglePage = YES;
            self.imageScroll.tag = 5;
            self.imageScroll.delegate = self;
            imagePgcontrol.numberOfPages=[productImageArray count];
            
            [self loadImageToScroll];
            // [self.imageScroll layoutSubviews];
            scrollLeftImg = [UIButton buttonWithType:UIButtonTypeCustom];
            scrollLeftImg.frame = CGRectMake(3, 70, 9, 13);
            [scrollLeftImg setImage:[UIImage imageNamed:@"left_arrow.png"] forState:UIControlStateNormal];
            [scrollLeftImg addTarget:self action:@selector(scrollImageLeft:) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:scrollLeftImg];
            scrollRightImg = [UIButton buttonWithType:UIButtonTypeCustom];
            scrollRightImg.frame = CGRectMake(140, 70, 9, 13);
            [scrollRightImg setImage:[UIImage imageNamed:@"right_arrow.png"] forState:UIControlStateNormal];
            [scrollRightImg addTarget:self action:@selector(scrollImageRight:) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:scrollRightImg];
            [scrollView addSubview:self.imageScroll];
            
            [imagePgcontrol setCurrentPage:0];
            [scrollView addSubview:imagePgcontrol];
            scrollLeftImg.hidden = YES;
            
            ReleaseAndNilify(imagePgcontrol);
            
            //     timer =  [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(loadImage) userInfo:nil repeats:YES];
            
            ReleaseAndNilify(imageScroll);
        }
        //       }
        else {
            UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
            imageButton.frame = CGRectMake(12, 10, 100, 100);
            [imageButton addTarget:self action:@selector(makeImageFullScreen:) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:imageButton];
        }
    }
    ReleaseAndNilify(asyncImageView);
    //product name label
    UILabel *prodName = [[UILabel alloc]initWithFrame:CGRectMake(150, 10, SCREEN_WIDTH - 150, 60)];//width 164
    prodName.numberOfLines = 3;
    [prodName setLineBreakMode:NSLineBreakByWordWrapping];
    prodName.backgroundColor = [UIColor clearColor];
    prodName.font = [UIFont systemFontOfSize:labelFont];
    
    if ([[HubCitiManager sharedManager]shareFromTL] == YES)
    {
        //product price label
        UILabel *prodPrice = [[UILabel alloc]initWithFrame:CGRectMake(150, 75, SCREEN_WIDTH - 150, 20)];//width 160
        UILabel *salePrice = [[UILabel alloc]initWithFrame:CGRectMake(150, 90, SCREEN_WIDTH - 150, 20)];//width 160
        
        prodPrice.backgroundColor = [UIColor clearColor];
        salePrice.backgroundColor = [UIColor clearColor];
        prodPrice.font = [UIFont systemFontOfSize:labelFont];
        salePrice.font =[UIFont systemFontOfSize:labelFont];
        salePrice.textColor = [UIColor redColor];
        NSString *regPriceString = [defaults  objectForKey:@"regPrice"];
        if (![regPriceString isEqual: @"N/A"] || ![regPriceString isEqual: @""]) {
            prodPrice.text = [NSString stringWithFormat:NSLocalizedString(@"Regular Price: %@",@"Regular Price: %@"), regPriceString];
        }
        
        NSString *salePriceString = [defaults  objectForKey:@"salePrice"];
        if (![salePriceString isEqual: @"N/A"] || ![salePriceString isEqual: @""]) {
            salePrice.text = [NSString stringWithFormat:NSLocalizedString(@"Sale Price: %@  ",@"Sale Price: %@  "),salePriceString];
        }
        
        [scrollView addSubview:prodPrice];
        [scrollView addSubview:salePrice];
        // [prodPrice release];
        //[salePrice release];
    }
    
    if ([productNameStr isEqualToString:@"N/A"])
        prodName.text = @"";
    else
        prodName.text = productNameStr;
    
    [scrollView addSubview:prodName];
    ReleaseAndNilify(prodName);
    
    attributTable.frame = CGRectMake(0.0, 115.0, SCREEN_WIDTH, 199.0);//÷÷ width 320.0
    
    // product short description
    if (![prodShortDescription isEqualToString:@"N/A"]) {
        UIWebView *prodDesc = [[UIWebView alloc]initWithFrame:CGRectMake(5, 115, SCREEN_WIDTH - 10, shortDescSize.height + 10)];//310
        [prodDesc loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:%fpx;font-family:Helvetica;color:#000;'>%@",labelFont,prodShortDescription] baseURL:nil];
        prodDesc.backgroundColor = [UIColor clearColor];
        prodDesc.delegate = self;
        [scrollView addSubview:prodDesc];
        
        attributTable.frame = CGRectMake(0, prodDesc.frame.origin.y + shortDescSize.height + 5, SCREEN_WIDTH, 199);// width SCREEN_WIDTH
        //  [prodDesc release];
    }
    
    //product long description
    if (![productLongDescriptionStr isEqualToString:@"N/A"]){
        
        UILabel *lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, attributTable.frame.origin.y, SCREEN_WIDTH, 1.0)];//320.0
        [lineLabel setBackgroundColor:[UIColor lightGrayColor]];
        [scrollView addSubview:lineLabel];
        
        UIWebView *prodDesc = [[UIWebView alloc]initWithFrame:CGRectMake(5, lineLabel.frame.origin.y + 2, SCREEN_WIDTH - 10, strSize.height + 10)];//width 310
        
        [prodDesc loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:%fpx;font-family:Helvetica;color:#000;'>%@",labelFont,productLongDescriptionStr] baseURL:nil];
        
        prodDesc.backgroundColor = [UIColor clearColor];
        prodDesc.delegate = self;
        [prodDesc.scrollView setShowsHorizontalScrollIndicator:NO];
        [scrollView addSubview:prodDesc];
        
        attributTable.frame = CGRectMake(0, attributTable.frame.origin.y + strSize.height + 10, SCREEN_WIDTH, 199);//width 320
        //  [prodDesc release];
        
        // [lineLabel release];
    }
    
    if (![videoFlagStr isEqualToString:@"N/A"]) {
        video.enabled = YES;
    }
    if (![audioFlagStr isEqualToString:@"N/A"]) {
        audio.enabled = YES;
    }
    if (![fileFlagStr isEqualToString:@"N/A"]) {
        otherInfo.enabled = YES;
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    CGRect frame = webView.frame;
    
    frame.size.height = 5.0f;
    
    webView.frame = frame;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //CGSize webViewTextSize = [webView sizeThatFits:CGSizeMake(1.0f, 1.0f)];  // Pass about any size
    CGSize webViewTextSize = [webView sizeThatFits:CGSizeZero];
    CGRect webViewFrame = webView.frame;
    webViewFrame.size.height = webViewTextSize.height;
    webView.frame = webViewFrame;
    
    //Disable bouncing in webview
    for (id subview in webView.subviews)
    {
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
        {
            [subview setBounces:NO];
        }
    }
}



-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [attributeArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([[attributeArray objectAtIndex:0] isEqualToString:@"warrantyServiceInfo"] &&indexPath.row == 0 ) {
        if ((warrantyInfoSize.height+5) <= 60) {
            return 60;
        }
        return (warrantyInfoSize.height+5);
    }
    return 60.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell ;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //cell.textLabel.text = [attributeArray objectAtIndex:indexPath.row];
    float valueLabelFont = 12.0, attributeLabelFont = 12.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        valueLabelFont = 17.0;
        attributeLabelFont = 17.0;
    }
    
    if ([attributeArray count] >0) {
        
        if ([[attributeArray objectAtIndex:0] isEqualToString:@"warrantyServiceInfo"] &&indexPath.row == 0 ) {
            
            UITextView *warrantyInfoText = [[UITextView alloc]init];
            warrantyInfoText.frame = CGRectMake(150.0,(tableView.rowHeight-warrantyInfoSize.height)/2,SCREEN_WIDTH - 160,warrantyInfoSize.height); //width 160
            warrantyInfoText.text = [valueArray objectAtIndex:0];
            warrantyInfoText.editable = NO;
            warrantyInfoText.backgroundColor = [UIColor clearColor];
            warrantyInfoText.scrollEnabled = NO;
            [cell addSubview:warrantyInfoText];
            // [warrantyInfoText release];
            
        }
        else{
            UILabel *valueLabel = [[UILabel alloc]initWithFrame:CGRectMake(150, 5, SCREEN_WIDTH - 160, 40)];//width 160
            
            valueLabel.backgroundColor = [UIColor clearColor];
            valueLabel.textColor = [UIColor blackColor];
            valueLabel.numberOfLines = 2;
            [valueLabel setLineBreakMode:NSLineBreakByWordWrapping];
            valueLabel.font = [UIFont boldSystemFontOfSize:valueLabelFont];
            if (![[valueArray objectAtIndex:indexPath.row]isEqualToString:@"N/A"])
                valueLabel.text = [valueArray objectAtIndex:indexPath.row];
            
            [cell addSubview:valueLabel];
            
            ReleaseAndNilify(valueLabel);
        }
        UILabel *attributeLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, SCREEN_WIDTH - 180, 40)];//width 140
        attributeLabel.backgroundColor = [UIColor clearColor];
        attributeLabel.textColor = [UIColor colorWithRGBInt:0x112e72];
        attributeLabel.numberOfLines = 2;
        [attributeLabel setLineBreakMode:NSLineBreakByWordWrapping];
        attributeLabel.font = [UIFont boldSystemFontOfSize:attributeLabelFont];
        if (![[attributeArray objectAtIndex:indexPath.row]isEqualToString:@"N/A"])
            attributeLabel.text = [attributeArray objectAtIndex:indexPath.row];
        [cell addSubview:attributeLabel];
        ReleaseAndNilify(attributeLabel);
        
    }
    
    return cell;
}

-(IBAction)audiosButtonClicked:(id)sender{
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    else {
        
        NSMutableString *urlString = [BASE_URL mutableCopy];
        [urlString appendString:@"find/mediainfo"];
        
        NSMutableString *reqStr=[NSMutableString stringWithFormat:@"<ProductDetailsRequest><userId>%@</userId><productId>%@</productId><mediaType>audio</mediaType><prodListId>1</prodListId><hubCitiId>%@</hubCitiId></ProductDetailsRequest>",[defaults  objectForKey:KEY_USERID],
                                 [defaults  objectForKey:KEY_PRODUCTID],[defaults valueForKey:KEY_HUBCITIID]];
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
        
        if ([UtilityManager isNullOrEmptyString:responseXml])
        {
            ReleaseAndNilify(responseXml);
            return;
        }
        [defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
        //[responseXml release];
        [defaults setObject:@"Audios" forKey:@"Display"];
        AudioVideoList *audioScreen = [[AudioVideoList alloc]initWithNibName:@"AudioVideoList"
                                                                      bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:audioScreen animated:NO];
        //[audioScreen release];
    }
}
-(IBAction)videosButtonClicked:(id)sender{
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    else {
        
        NSMutableString *urlString = [BASE_URL mutableCopy];
        [urlString appendString:@"find/mediainfo"];
        
        NSMutableString *reqStr=[NSMutableString stringWithFormat:@"<ProductDetailsRequest><userId>%@</userId><productId>%@</productId><mediaType>video</mediaType><prodListId>1</prodListId><hubCitiId>%@</hubCitiId></ProductDetailsRequest>",[defaults  objectForKey:KEY_USERID],
                                 [defaults  objectForKey:KEY_PRODUCTID],[defaults valueForKey:KEY_HUBCITIID]];
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
        
        if ([UtilityManager isNullOrEmptyString:responseXml])
        {
            ReleaseAndNilify(responseXml);
            return;
        }
        DLog(@"resp = %@",responseXml);
        [defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
        //[responseXml release];
        
        [defaults setObject:@"Videos" forKey:@"Display"];
        AudioVideoList *videoScreen = [[AudioVideoList alloc]initWithNibName:@"AudioVideoList" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:videoScreen animated:NO];
        //[videoScreen release];
    }
}
-(IBAction)otherInfoClicked:(id)sender{
    
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    else {
        
        NSMutableString *urlString = [BASE_URL mutableCopy];
        [urlString appendString:@"find/mediainfo"];
        
        NSMutableString *reqStr=[NSMutableString stringWithFormat:@"<ProductDetailsRequest><userId>%@</userId><productId>%@</productId><mediaType>other</mediaType><prodListId>1</prodListId><hubCitiId>%@</hubCitiId></ProductDetailsRequest>",[defaults  objectForKey:KEY_USERID],
                                 [defaults  objectForKey:KEY_PRODUCTID],[defaults valueForKey:KEY_HUBCITIID]];
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
        if ([UtilityManager isNullOrEmptyString:responseXml])
        {
            ReleaseAndNilify(responseXml);
            return;
        }
        [defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
        //[responseXml release];
        
        [defaults setObject:@"OtherInfo" forKey:@"Display"];
        AudioVideoList *otherInfoScreen = [[AudioVideoList alloc]initWithNibName:@"AudioVideoList"
                                                                          bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:otherInfoScreen animated:NO];
        //[otherInfoScreen release];
    }
}
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }`
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void) viewWillDisappear:(BOOL)animated
{
    /* if (timer != nil) {
     
     
     if ([timer isValid] == YES) {
     [timer invalidate];
     timer = nil;
     }
     }*/
    [super viewWillDisappear:YES];
    
}
- (void)viewDidUnload {
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


@end

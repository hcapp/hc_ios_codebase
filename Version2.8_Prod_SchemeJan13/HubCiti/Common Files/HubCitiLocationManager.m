//
//  ScanSeeLocationManager.m
//  Scansee
//
//  Created by Chaitra on 19/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HubCitiLocationManager.h"
#import <CoreLocation/CoreLocation.h>
#import "CLLocation (Strings).h"

@implementation HubCitiLocationManager

@synthesize locationManager;
@synthesize locationMeasurements;
@synthesize bestEffortAtLocation;


static HubCitiLocationManager *sharedLocationManager = nil;

+ (id)locationManager {
    @synchronized(self) {
        if(sharedLocationManager == nil)
            sharedLocationManager = [[super allocWithZone:NULL] init];
    }
    return sharedLocationManager;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self locationManager];
}
- (id)copyWithZone:(NSZone *)zone {
    return self;
}

-(void) updateLocationinfo : _delegate{
	
    delegate = _delegate;
    
	if(self.locationMeasurements !=nil)
	{
		//[self.locationMeasurements release];
		//self.locationMeasurements = nil;	
	}
	self.locationMeasurements = [NSMutableArray array];
	
    self.locationManager = [[CLLocationManager alloc] init] ;
    locationManager.delegate = self;
	
    // This is the most important property to set for the manager. It ultimately determines how the manager will
    // attempt to acquire location and thus, the amount of power that will be consumed.
    //locationManager.desiredAccuracy = [[setupInfo objectForKey:kSetupInfoKeyAccuracy] doubleValue];
	locationManager.desiredAccuracy = 10.0;
	[locationManager startUpdatingLocation];
}


// location services


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    // store all of the measurements, just so we can see what kind of data we might receive
    [locationMeasurements addObject:newLocation];
    // test the age of the location measurement to determine if the measurement is cached
    // in most cases you will not want to rely on cached measurements
    NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
    if (locationAge > 100.0) return;
    // test that the horizontal accuracy does not indicate an invalid measurement
    if (newLocation.horizontalAccuracy < 0) return;
    // test the measurement to see if it is more accurate than the previous measurement
    //if (bestEffortAtLocation == nil || bestEffortAtLocation.horizontalAccuracy > newLocation.horizontalAccuracy) {
	if (bestEffortAtLocation == nil){
        // store the location as the "best effort"
        self.bestEffortAtLocation = newLocation;
        // test the measurement to see if it meets the desired accuracy
        //
        // IMPORTANT!!! kCLLocationAccuracyBest should not be used for comparison with location coordinate or altitidue 
        // accuracy because it is a negative value. Instead, compare against some predetermined "real" measure of 
        // acceptable accuracy, or depend on the timeout to stop updating. This sample depends on the timeout.
        //
        if (newLocation.horizontalAccuracy <= locationManager.desiredAccuracy) {
            // we have a measurement that meets our requirements, so we can stop updating the location
            // 
            // IMPORTANT!!! Minimize power usage by stopping the location manager as soon as possible.
            //
            [self stopUpdatingLocation:NSLocalizedString(@"Acquired Location", @"Acquired Location")];
            // we can also cancel our previous performSelector:withObject:afterDelay: - it's no longer necessary
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stopUpdatingLocation:) object:nil];
        }
    }
    // update the display with the new location data
	NSString *gpsValu=@"NA";
	DLog(@"gpsValu1:%@",gpsValu);
	gpsValu = bestEffortAtLocation.localizedCoordinateString;
	
	//NSMutableArray *items =[[NSMutableArray alloc] init];
	NSArray *items = [gpsValu componentsSeparatedByString:@","];
	if([items count]>=2)
	{
	//	gpsValu = [NSString stringWithFormat:@"lat:%f - long:%f",[[items objectAtIndex:0] doubleValue],[[items objectAtIndex:1] doubleValue]];
		latValue = [[NSString stringWithFormat:@"%f",[[items objectAtIndex:0] doubleValue]] copy];
		longValue = [[NSString stringWithFormat:@"%f",[[items objectAtIndex:1] doubleValue]] copy];
        
        [defaults setValue:latValue forKey:KEY_LATITUDE];
        [defaults setValue:longValue forKey:KEY_LONGITUDE];
		
		DLog(@"latValue:%@",latValue);
		DLog(@"longValue:%@",longValue);
		
	}
	if(latValue && [latValue length]>0 && longValue && [longValue length]>0)
	{
		[self stopUpdatingLocation:NSLocalizedString(@"Acquired Location", @"Acquired Location")];
		// we can also cancel our previous performSelector:withObject:afterDelay: - it's no longer necessary
		[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stopUpdatingLocation:) object:nil];
	}
	
	
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    // The location "unknown" error simply means the manager is currently unable to get the location.
    // We can ignore this error for the scenario of getting a single location fix, because we already have a 
    // timeout that will stop the location manager to save power.
    if ([error code] != kCLErrorLocationUnknown) {
        [self stopUpdatingLocation:NSLocalizedString(@"Error", @"Error")];
    }
}


- (void)stopUpdatingLocation:(NSString *)state {
	
	[locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
		
	[delegate updatedLatlongValue:latValue longitudeValue:longValue];
	
}

@end

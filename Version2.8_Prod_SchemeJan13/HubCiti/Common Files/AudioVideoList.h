//
//  AudioVideoList.h
//  ScanSee
//
//  Created by ajit on 5/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "CommonUtility.h"

@interface AudioVideoList : UITableViewController<CustomizedNavControllerDelegate> {
	
    IBOutlet UITableView *tv;
    NSArray *audioArray;
	NSArray *videoArray;
	NSMutableArray *mediaPathArray ,*mediaArray,*pmLstIDArray;
	MPMoviePlayerViewController *moviePlayerView;
	AVAudioPlayer *audioPlayer;
    NSArray *listItems;
}
-(void) parseResponseXml : (NSString *) response ;
@end

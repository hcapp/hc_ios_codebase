//
//  ProductPage.h
//  Scansee
//
//  Created by ajit on 9/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"
#import "LocationManager.h"
BOOL isFromProductPage;

#define STOCK_ONLINE		0
#define STOCK_NEARBY		1
#define STOCK_REVIEWS		2
#define STOCK_PRODUCTINFO	3
#define STOCK_COUPONS		4

@interface StockHeader : NSObject
{
	NSString	*m_StockHeader;
	int			m_StockType;
}

@property (nonatomic, strong) NSString	*m_StockHeader;
@property (nonatomic) int		m_StockType;

@end

typedef enum webServicesproductpage
{
 GETPRODUCTSUMMARY,
 FETCHPRODUCTATTRIBUTES
}webServicesproductpageState;


@interface ProductPage : UIViewController<UITableViewDelegate,UITableViewDataSource ,LocationManagerDelegate,CustomizedNavControllerDelegate >
{
    webServicesproductpageState iWebRequestState;
	IBOutlet UITableView *productPageTable;
    IBOutlet UIButton *callbtn;
    IBOutlet UIButton *aboutBtn;
    IBOutlet UIButton *wishBtn;
    IBOutlet UIButton *prefBtn;
    IBOutlet UIButton *rateBtn;
	NSMutableArray *stockHeaderArray, *stockImagePathArray;
	NSString *prodName,*prodId,*prodDesc,*prodImage;
	NSMutableString *minPrice,*totalStores;
	NSString *totalLocations, *lowestPrice;
	int nextPage,pageSize,pageStart;
	NSString *apiUrl,*vendorName;
	NSMutableArray *labelValueArray, *detailLabelValueArray;
	BOOL clrFlag;
	NSString *couponStatus, *loyaltyStatus, *rebateStatus;
	NSString *rating;
	NSString *responseXml;
	UIButton *adRibbonBtn;
	NSIndexPath *selectedIndex;
	UIActivityIndicatorView *activity ;
    
    NSMutableArray *retIdArray,*retListIDArray;
    NSMutableArray *retLocIdArray;
}

@property(nonatomic,strong)UITableView *productPageTable;
@property(nonatomic,strong)UIButton *adRibbonBtn;

-(void)returnToMainPage:(id)sender;
-(void)aboutClicked:(id)sender;
-(void)wishListClicked:(id)sender;
-(void) preferences : (id) sender;
-(void)rateShareClicked:(id)sender;
-(void)parseTheXml:(NSString *)response;
-(void) refresh;
-(void) addToList:(id) sender;

@end

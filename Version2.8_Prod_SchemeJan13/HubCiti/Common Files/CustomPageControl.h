//
//  CustomPageControl.h
//  Scansee
//
//  Created by Chaitra on 03/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomPageControl : UIPageControl
{
    UIImage* activeImage;
    UIImage* inactiveImage;
    UIView *view;
}
@property(nonatomic, strong) UIImage* activeImage;
@property(nonatomic, strong) UIImage* inactiveImage;
@property(nonatomic, strong) UIView *view;
//@property (nonatomic, retain) UIColor *dotColorCurrentPage;
//@property (nonatomic, retain) UIColor *dotColorOtherPage;

@end

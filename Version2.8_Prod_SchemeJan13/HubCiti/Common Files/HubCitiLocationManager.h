//
//  ScanSeeLocationManager.h
//  Scansee
//
//  Created by Chaitra on 19/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol HubCitiLocationManagerDelegate

-(void) updatedLatlongValue :(NSString *) latValue longitudeValue:(NSString *)longValue;

@end

@interface HubCitiLocationManager : NSObject <CLLocationManagerDelegate> {

	id <HubCitiLocationManagerDelegate> delegate ;
	
	CLLocationManager *locationManager;
    NSMutableArray *locationMeasurements;
    CLLocation *bestEffortAtLocation;
	NSString *latValue;
	NSString *longValue;

}
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSMutableArray *locationMeasurements;
@property (nonatomic, strong) CLLocation *bestEffortAtLocation;

+(id) locationManager;

-(void) updateLocationinfo : _delegate ;

- (void)stopUpdatingLocation:(NSString *)state ;

@end

//
//  AudioPlayer.m
//  iPhoneStreamingPlayer
//
//  Created by Matt Gallagher on 28/10/08.
//  Copyright Matt Gallagher 2008. All rights reserved.
//
//  This software is provided 'as-is', without any express or implied
//  warranty. In no event will the authors be held liable for any damages
//  arising from the use of this software. Permission is granted to anyone to
//  use this software for any purpose, including commercial applications, and to
//  alter it and redistribute it freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not
//     claim that you wrote the original software. If you use this software
//     in a product, an acknowledgment in the product documentation would be
//     appreciated but is not required.
//  2. Altered source versions must be plainly marked as such, and must not be
//     misrepresented as being the original software.
//  3. This notice may not be removed or altered from any source
//     distribution.
//

#import "AudioPlayer.h"
#import "AudioStreamer.h"
#import <QuartzCore/CoreAnimation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <CFNetwork/CFNetwork.h>
#import "MainMenuViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@implementation AudioPlayer

//
// setButtonImage:
//
// Used to change the image on the playbutton. This method exists for
// the purpose of inter-thread invocation because
// the observeValueForKeyPath:ofObject:change:context: method is invoked
// from secondary threads and UI updates are only permitted on the main thread.
//
// Parameters:
//    image - the image to set on the play button.
//
- (void)setButtonImage:(UIImage *)image
{
    [button.layer removeAllAnimations];
    if (!image)
    {
        [button setImage:[UIImage imageNamed:@"playbutton.png"] forState:0];
    }
    else
    {
        [button setImage:image forState:0];
        
        if ([button.currentImage isEqual:[UIImage imageNamed:@"loadingbutton.png"]])
        {
            [self spinButton];
        }
    }
}

//
// destroyStreamer
//
// Removes the streamer, the UI update timer and the change notification
//
- (void)destroyStreamer
{
    if (streamer)
    {
        [[NSNotificationCenter defaultCenter]
         removeObserver:self
         name:ASStatusChangedNotification
         object:streamer];
        [progressUpdateTimer invalidate];
        progressUpdateTimer = nil;
        
        [streamer stop];
        //[streamer release];
        streamer = nil;
    }
}

//
// createStreamer
//
// Creates or recreates the AudioStreamer object.
//
- (void)createStreamer
{
    if (streamer)
    {
        return;
    }
    
    [self destroyStreamer];
    NSString *urlStr = [NSString stringWithFormat:@"%@",[defaults valueForKey:@"MediaPath"]];
    downloadSourceField.text = urlStr;
    
    DLog(@"Media Path:%@", downloadSourceField.text);
   	NSString *escapedValue =
    (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                          nil,
                                                                          (CFStringRef)downloadSourceField.text,
                                                                          NULL,
                                                                          NULL,
                                                                          kCFStringEncodingUTF8))
    ;
    
    NSURL *url = [NSURL URLWithString:escapedValue];
    //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[defaults valueForKey:@"MediaPath"]]];
    streamer = [[AudioStreamer alloc] initWithURL:url];
    
    progressUpdateTimer =
    [NSTimer
     scheduledTimerWithTimeInterval:0.1
     target:self
     selector:@selector(updateProgress:)
     userInfo:nil
     repeats:YES];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(playbackStateChanged:)
     name:ASStatusChangedNotification
     object:streamer];
}

//
// viewDidLoad
//
// Creates the volume slider, sets the default path for the local file and
// creates the streamer immediately if we already have a file at the local
// location.
//
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:[NSString stringWithFormat:@"%@",[defaults valueForKey:@"MediaName"]] forView:self withHambergur:NO];
    
    //[titleLabel release];
    
    view1.userInteractionEnabled = NO;
    view1.alpha = 0.5;
    // configure the back button
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];

    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0.0, 0.0, 30.0, 30.0);
    
    SdImageView *backImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [backImage loadImage:[defaults valueForKey:@"bkImgPath"]];
    [btn addSubview:backImage];
    //[btn setBackgroundImage:[UIImage imageNamed:@"backBtn_up.png"] forState:UIControlStateHighlighted];
    
    [btn addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = newBackButton;
    //[newBackButton release];
    
    //    UIButton *mainMenubtn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [mainMenubtn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:mainMenubtn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    
    //UIImage *image = [UIImage imageNamed:@"House-Icon25"];
    UIButton *bttn = [UIButton buttonWithType:UIButtonTypeCustom];
    [bttn setFrame:CGRectMake(0, 0, 30,30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:bttn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [bttn addSubview:homeImage];
    
    
    [bttn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:bttn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    // //[mainPage release];
    
    MPVolumeView *volumeView = [[MPVolumeView alloc] initWithFrame:volumeSlider.bounds] ;
    [volumeSlider addSubview:volumeView];
    [volumeView sizeToFit];
    
    [self setButtonImage:[UIImage imageNamed:@"playbutton.png"]];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    MPVolumeView *volumeView = [[MPVolumeView alloc] initWithFrame:volumeSlider.bounds] ;
    [volumeSlider addSubview:volumeView];
    [volumeView sizeToFit];
    
    [self setButtonImage:[UIImage imageNamed:@"playbutton.png"]];
}

-(void)viewDidUnload{
    [super viewDidUnload];
    if([streamer isPlaying] || [streamer isPaused])
        [streamer stop];
}
//
// spinButton
//
// Shows the spin button when the audio is loading. This is largely irrelevant
// now that the audio is loaded from a local file.
//
- (void)spinButton
{
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    CGRect frame = [button frame];
    button.layer.anchorPoint = CGPointMake(0.5, 0.5);
    button.layer.position = CGPointMake(frame.origin.x + 0.5 * frame.size.width, frame.origin.y + 0.5 * frame.size.height);
    [CATransaction commit];
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanFalse forKey:kCATransactionDisableActions];
    [CATransaction setValue:[NSNumber numberWithFloat:2.0] forKey:kCATransactionAnimationDuration];
    
    CABasicAnimation *animation;
    animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = [NSNumber numberWithFloat:0.0];
    animation.toValue = [NSNumber numberWithFloat:2 * M_PI];
    animation.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionLinear];
    animation.delegate = self;
    [button.layer addAnimation:animation forKey:@"rotationAnimation"];
    
    [CATransaction commit];
}

//
// animationDidStop:finished:
//
// Restarts the spin animation on the button when it ends. Again, this is
// largely irrelevant now that the audio is loaded from a local file.
//
// Parameters:
//    theAnimation - the animation that rotated the button.
//    finished - is the animation finised?
//
- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)finished
{
    if (finished)
    {
        [self spinButton];
    }
}

//
// buttonPressed:
//
// Handles the play/stop button. Creates, observes and starts the
// audio streamer when it is a play button. Stops the audio streamer when
// it isn't.
//
// Parameters:
//    sender - normally, the play/stop button.
//
- (IBAction)buttonPressed:(id)sender
{
    if ([button.currentImage isEqual:[UIImage imageNamed:@"playbutton.png"]])
    {
        //[downloadSourceField resignFirstResponder];
        
        [self createStreamer];
        [self setButtonImage:[UIImage imageNamed:@"loadingbutton.png"]];
        [streamer start];
        //unhide view 1
        view1.userInteractionEnabled = YES;
        view1.alpha = 1.0;
    }
    else
    {
        //[streamer stop];
        [streamer pause];
    }
}

//
// sliderMoved:
//
// Invoked when the user moves the slider
//
// Parameters:
//    aSlider - the slider (assumed to be the progress slider)
//
- (IBAction)sliderMoved:(UISlider *)aSlider
{
    if (streamer.duration)
    {
        double newSeekTime = (aSlider.value / 100.0) * streamer.duration;
        [streamer seekToTime:newSeekTime];
    }
}

//
// playbackStateChanged:
//
// Invoked when the AudioStreamer
// reports that its playback status has changed.
//
- (void)playbackStateChanged:(NSNotification *)aNotification
{
    if ([streamer isWaiting])
    {
        [self setButtonImage:[UIImage imageNamed:@"loadingbutton.png"]];
    }
    else if ([streamer isPlaying])
    {
        [self setButtonImage:[UIImage imageNamed:@"pausebutton.png"]];
    }
    else if ([streamer isIdle])
    {
        [self destroyStreamer];
        [self setButtonImage:[UIImage imageNamed:@"playbutton.png"]];
    }
}

// Invoked when the AudioStreamer
// reports that its playback progress has changed.
//
- (void)updateProgress:(NSTimer *)updatedTimer
{
    if (streamer.bitRate != 0.0)
    {
        double progress = streamer.progress;
        double duration = streamer.duration;
        
        if (duration > 0)
        {
            [positionLabel setText:
             [NSString stringWithFormat:@"Time Played: %.1f/%.1f seconds",
              progress,
              duration]];
            [progressSlider setEnabled:YES];
            [progressSlider setValue:100 * progress / duration];
        }
        else
        {
            [progressSlider setEnabled:NO];
        }
    }
    else
    {
        positionLabel.text = @"Time Played:";
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)sender
{
    [sender resignFirstResponder];
    [self createStreamer];
    return YES;
}

- (IBAction) stopButtonClicked:(id)sender{
    if([streamer isPlaying])
        [streamer stop];
}




-(void)returnToMainPage:(id)sender{
    
    //if([streamer isPlaying] || [streamer isPaused])
    //[streamer stop];
    [self destroyStreamer];
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

//
// dealloc
//
// Releases instance memory.
//
- (void)dealloc
{
    
    //[streamer stop];
    [self destroyStreamer];
    if (progressUpdateTimer)
    {
        [progressUpdateTimer invalidate];
        progressUpdateTimer = nil;
    }
    
}

-(void)backButtonPressed{
    //if([streamer isPlaying] || [streamer isPaused])
    //[streamer stop];
    [self destroyStreamer];
    
    [self.navigationController popViewControllerAnimated:NO];
    
    
}

@end

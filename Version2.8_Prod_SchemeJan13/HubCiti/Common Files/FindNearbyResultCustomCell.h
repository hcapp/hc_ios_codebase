//
//  FindNearbyResultCustomCell.h
//  Scansee
//
//  Created by ajit on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FindNearbyResultCustomCell : UITableViewCell {
	UILabel *priceLabel;
	UILabel *titleLabel;
	UILabel *subTitleLabel;
}

@property(nonatomic,strong)UILabel *priceLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subTitleLabel;

-(void)setData:(NSDictionary *)dict;

@end

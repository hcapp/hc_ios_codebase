//
//  CreateEditAccountTableViewCell.h
//  HubCiti
//
//  Created by service on 9/23/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateEditAccountTableViewCell : UITableViewCell

@property(nonatomic , strong) UILabel* labelField;
@property(nonatomic , strong) UITextField* valueField;
@property(nonatomic , strong) UITextView* valueViewField;



@end

//
//  AppDelegate.m
//  HubCiti
//
//  Created by Ajit on 9/4/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "SFHFKeychainUtils.h"
#import "CLLocation (Strings).h"
#import "APNSListViewController.h"
#import "WebBrowserViewController.h"
#import "HTTPClient.h"
#import "HotDealDetailViewController.h"
#import "NewCouponDetailViewController.h"
#import "RetailerSpecialOffersViewController.h"
#import "NativeSpecialOfferViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "MFSideMenu.h"
#import "SideViewController.h"
#import "ContainerSubViewController.h"
#import "CombinationCategoryViewController.h"
#import "UserInformationViewController.h"
#import "RegistrationViewController.h"
#import "TutorialViewController.h"
#import "BlockViewController.h"

extern BOOL isSameUser;
@interface AppDelegate () <HTTPClientDelegate>

@end

@implementation AppDelegate

@synthesize loginViewController,tempUserString;
@synthesize navigationController;
@synthesize share = _share;
@synthesize locationManager;
@synthesize locationMeasurements;
@synthesize bestEffortAtLocation;
@synthesize isVideoOrientationcalled;
@synthesize isLogistics;
@synthesize badgeNumber,message, notificationListArr, notificationMsgList;
@synthesize rssFeedListDOArr, templateTypeName;
@synthesize guestUser,guestPassword,window,loginDO, emailCountString;




// This method will call for above an eqal to IOS version 6.0
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    // If called from Video Screen then allow orientation otherwise portrait only
    if(isVideoOrientationcalled)
        return UIInterfaceOrientationMaskAll;
    else if (isLogistics){
        return UIInterfaceOrientationMaskLandscape;
    }
    else
        return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    // Reset All Bool value
    
    
   //[NewRelicAgent startWithApplicationToken:@"AA8c3d6e89b0564f3a3e1b0e3e6ea1ed2217fd9d48"];//Marble Falls
   // [NewRelicAgent startWithApplicationToken:@"AAdd87748ab38be365979dbd6f73467d614f07cdac"];//Tyler
    // [NewRelicAgent startWithApplicationToken:@"AA143d40522e38eccd9223eb62081f67eb08b855d4"];//Rockwall Area(unwanted)
    //[NewRelicAgent startWithApplicationToken:@"AAdf8d8eda9ce48e6dc0fadbe304408f41518544bb"];//Addison
    // [NewRelicAgent startWithApplicationToken:@"AA3ba385119be6778f569d59aa3f538a5b3e53617b"];//Killeen (Central TX News)
    //[NewRelicAgent startWithApplicationToken:@"AAfce935d76d6d5417f6599facf08072af50e386a4"];//Rockwall HubCiti
   // [NewRelicAgent startWithApplicationToken:@"AA516d309126397ed31a7c2ef169dea46d12d0720b"]; //Austin
  // [NewRelicAgent startWithApplicationToken:@"AAedddf5e84ed8bb6fd47288931455c6e53bb486ac"]; //Mt Airy
  //  [NewRelicAgent startWithApplicationToken:@"AA10f387afbd23287deedaccc619c44071db098b87"]; //colorado couty
    
   // [NewRelicAgent startWithApplicationToken:@"AAdf8d8eda9ce48e6dc0fadbe304408f41518544bb"];//Elgin
   // [NewRelicAgent startWithApplicationToken:@"AAe3e4fc7ae0e738896e1e3943888d61c68455fd2c"];//HC Nav
   // [NewRelicAgent startWithApplicationToken:@"AAc6d433636671791cab9e9c59e73baf65a252d515"];//Go Taylor Texas
    
 

    [defaults setBool:NO forKey:@"ViewMore"];
    [defaults setInteger:0 forKey:@"currentIndex"];
    [defaults setValue:nil forKey:@"bannerImgScroll"];
    [defaults setInteger:0 forKey:@"currentIndexSubPage"];
    [defaults setValue:nil forKey:@"ModifiedDate"];
    [defaults setValue:nil forKey:@"scrollModifiedDate"];
    [defaults setValue:nil forKey:@"blockModifiedDate"];
    [defaults setValue:nil forKey:@"hamburgerImg"];
    [defaults setValue:nil forKey:@"sortedCitiIDs"];
    [SharedManager setGps_allow_flag:NO];
    [defaults setValue:@"NO" forKey:@"allowingGPS"];
    [defaults setValue:nil forKey:@"backbuttonCategory"];
    [defaults setValue:nil forKey:@"backbuttonphoto"];
    notificationListArr = [[NSMutableArray alloc]init];
    notificationMsgList = [[NSMutableArray alloc]init];
    [defaults setBool:NO forKey:@"pushOnKill"];
    
    NSDictionary *tmpDic = [launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
    
    //if tmpDic is not nil, then your app is launched due to an APNs push, therefore check this NSDictionary for further information
    if (tmpDic != nil) {
        DLog(@" - launch options dict has something ");
        DLog(@" - badge number is %@ ", [[tmpDic objectForKey:@"aps"] objectForKey:@"badge"]);
        DLog(@" - ");
       
        [defaults setBool:YES forKey:@"pushOnKill"];
        [self showActivityIndicator];
        
        //get the notification message to be displayed
        message = [[NSString alloc]initWithString:[[[tmpDic objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"loc-key"]];
        
        if ([HUBCITIKEY isEqualToString:@"Tyler19"]) {
            
            for (int i=0; i<[notificationMsgList count]; i++) {
                if([[notificationMsgList objectAtIndex:i]containsString:@"of the day"]|| [[notificationMsgList objectAtIndex:i]containsString:@"your daily deal."]){
                    if ([message containsString:@"of the day"] || [message containsString:@"your daily deal."]) {
                        [notificationMsgList removeAllObjects];
                        [notificationListArr removeAllObjects];
                        break;
                    }
                    
                }
            }
            
        }
        
        [notificationMsgList addObject:message];
        
        [defaults setValue:message forKey:@"message"];
        //end
        
        //set the app icon badge number
         badgeNumber = [[[tmpDic objectForKey:@"aps"] objectForKey:@"badge"]intValue];
        
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeNumber];
        [defaults setInteger:badgeNumber forKey:@"BadgeNumber"];//used in wish list
        //end
        //NSError *error;
         if ([[tmpDic objectForKey:@"aps"] objectForKey:@"body"] != NULL){
            
              id json = [[[tmpDic objectForKey:@"aps"] objectForKey:@"body"]copy];
            
            //            NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
            //
            //            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            
            [notificationListArr addObject:json];
            NSDictionary* dict = [notificationListArr objectAtIndex:(notificationListArr.count - 1)];
            
            if (notificationListArr.count > 1) {
                
                [notificationListArr insertObject:dict atIndex:0];
                [notificationListArr removeObjectAtIndex:(notificationListArr.count - 1)];
            }
            
            NSArray* uniqueArray = [self removeDuplicate : notificationListArr];
            notificationListArr = [uniqueArray mutableCopy];
            
            [defaults setValue:notificationListArr forKey:@"PushNotifyResponse"];
            
             [defaults setObject:@"1" forKey:@"fromDidFinishLaunchingWithNotification"];
            
        }

        
        
    }
    
    self.share = [[FTShare alloc] initWithReferencedController:nil];
    
    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    [defaults setValue:nil forKey:KEY_LATITUDE];
    [defaults setValue:nil forKey:KEY_LONGITUDE];
    [defaults setValue:nil forKey:KEY_MAINMENUID];
    
    linkIdArray = [[NSMutableArray alloc]init];
    [linkIdArray addObject:@"0"];
    
    deptIdArray = [[NSMutableArray alloc]init];
    [deptIdArray addObject:@"0"];
    
    typeIdArray = [[NSMutableArray alloc]init];
    [typeIdArray addObject:@"0"];
    
    
    selIndexArrayFind = [[NSMutableArray alloc]init];
    retailerIdArray = [[NSMutableArray alloc]init];
    retailerLocIdArray = [[NSMutableArray alloc]init];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] ;
    // Override point for customization after application launch.
    
    //setting some common configuration across the application
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
    
    //application navigation bar appearance
    
    //    CGRect rect = CGRectMake(0, 0, 1, 1);
    //    UIGraphicsBeginImageContext(rect.size);
    //    CGContextRef context = UIGraphicsGetCurrentContext();
    //    CGContextSetFillColorWithColor(context, [[UIColor blackColor ]CGColor]);
    //
    //    CGContextFillRect(context, rect);
    //    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    //    UIGraphicsEndImageContext();
    
    
    
    
    //    UIImage *image = [UIImage imageNamed:@"NavBar_Bg.png"];
    //
    //    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    //    UIGraphicsBeginImageContext(rect.size);
    //    CGContextRef context = UIGraphicsGetCurrentContext();
    //    CGContextClipToMask(context, rect, image.CGImage);
    //    CGContextSetFillColorWithColor(context, [[UIColor redColor] CGColor]);
    //    CGContextFillRect(context, rect);
    //    UIImage *img1 = UIGraphicsGetImageFromCurrentImageContext();
    //    UIGraphicsEndImageContext();
    //
    //    UIImage *flippedImage = [UIImage imageWithCGImage:img1.CGImage
    //                                                scale:1.0 orientation: UIImageOrientationDownMirrored];
    
    
    //    yourUIImageView.image = flippedImage;
    
    
    
    //    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"Transparent.png"] forBarMetrics:UIBarMetricsDefault];
    //    [[UINavigationBar appearance]setBackgroundColor:[UIColor blackColor]];
    
    
    
    
    //globally customize tableviewcell selection style as part of ios7 fix
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRGBInt:0x036EEC];
    bgColorView.layer.masksToBounds = YES;
    
    [[UITableViewCell appearance]setSelectedBackgroundView:bgColorView];
    
    //searchbar appearance
    if (IOS7){
        [[UITableView appearance]setSeparatorInset:UIEdgeInsetsZero];
        [[UISearchBar appearance]setSearchBarStyle:UISearchBarStyleDefault];
        // [[UISearchBar appearance] setBackgroundImage:[UIImage imageNamed:@"NavBar_Bg.png"] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefaultPrompt];
    }
    
    else{
        [[UISearchBar appearance]setTintColor: [UIColor blackColor]];
        [[UISearchBar appearance]setBarStyle:UIBarStyleBlack];
        // [[UISearchBar appearance] setBackgroundImage:[UIImage imageNamed:@"NavBar_Bg.png"]];
    }
    
    
    //check in keychain if the password already exists - if 'NO' then only generate
    NSError *error;
    NSString *str = [SFHFKeychainUtils getPasswordForUsername:@"User" andServiceName:@"DeviceID" error:&error];
    //NSlog(@"%@",str);
    
    if (str == NULL){
        //since udid is deprecated in ios < 6.0, using CFUUIDCreate and storing in UserDefaults
        if (![defaults valueForKey:KEY_DEVICEID]){//because CFUUID is different everytime
            if ([[[UIDevice currentDevice] systemVersion] floatValue] < 6.0){
                
                // Create universally unique identifier (object)
                CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
                
                // Get the string representation of CFUUID object.
                NSString *uuidStr = ( NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuidObject));
                CFRelease(uuidObject);
                [defaults setValue:uuidStr forKey:KEY_DEVICEID];
            }
            else{
                
                [defaults setValue:[UIDevice currentDevice].identifierForVendor.UUIDString forKey:KEY_DEVICEID];
            }
        }
        //Store deviceID into keychain
        [SFHFKeychainUtils storeUsername:@"User" andPassword:[defaults valueForKey:KEY_DEVICEID] forServiceName:@"DeviceID" updateExisting:YES error:&error];
        //Store deviceID into keychain
    }
    else
        [defaults setValue:str forKey:KEY_DEVICEID];
    
    DLog(@"%@",[defaults valueForKey:KEY_DEVICEID]);
    
    /* Required as the system pop-up for location services will show only upon calling "startUpdatingLocation"*/
    //self.locationMeasurements = [NSMutableArray array];
    if(self.locationMeasurements !=nil)
    {
        //[self.locationMeasurements release];
        //self.locationMeasurements = nil;
    }
    self.locationMeasurements = [NSMutableArray array];
    
    self.locationManager = [[CLLocationManager alloc] init] ;
    self.locationManager.delegate = self;
    // This is the most important property to set for the manager. It ultimately determines how the manager will
    // attempt to acquire location and thus, the amount of power that will be consumed.
    //locationManager.desiredAccuracy = [[setupInfo objectForKey:kSetupInfoKeyAccuracy] doubleValue];
    
    locationManager.desiredAccuracy = 10.0;
    
    //locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //locationManager.distanceFilter = 5.0;
    
    if(![defaults boolForKey:@"didFinish"]){
        [defaults setBool:YES forKey:@"didFinish"];
    // iOS 8 - request location services via requestWhenInUseAuthorization.
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }else {
        // iOS 7 - We can't use requestWhenInUseAuthorization -- we'll get an unknown selector crash!
        // Instead, you just start updating location, and the OS will take care of prompting the user
        // for permissions.
        [self.locationManager startUpdatingLocation];
    }
    }
    [defaults setBool:NO forKey:@"isMainMenuSignUp"];
    [defaults setValue:nil forKey:@"login_DO"];
    /****send request to design custom UI screen in login ******/
    // [self requestForSplash];
    [self requestForCustomLoginUI];
    sleep(2);
    
    [self sendResetBadgeRequest];
    
    [self onSplashScreenExpired];
    
    
    
    //version check functionality
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    
    [xmlStr appendFormat:@"<AuthenticateUser><appVersion>%@</appVersion>",AppVersion];
    [xmlStr appendFormat:@"<platform>IOS</platform><hubCitiKey>%@</hubCitiKey></AuthenticateUser>",HUBCITIKEY];
    
    DLog(@"Version check req:%@",xmlStr);
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:VERSIONCHECK_URL withParam:xmlStr]];
    
    // [xmlStr release];
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
    {
        ReleaseAndNilify(responseXml);
        return NO;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    
    TBXMLElement *forcedUpdateFlagElement = [TBXML childElementNamed:@"forcefulUpdateVersionFlag" parentElement:tbXml.rootXMLElement];
    
    if ([[TBXML textForElement:forcedUpdateFlagElement]boolValue] == 1) {
        
        TBXMLElement *updateVersionTextElement = [TBXML childElementNamed:@"updateVersionText" parentElement:tbXml.rootXMLElement];
        TBXMLElement *latestAppVerUrlElement = [TBXML childElementNamed:@"latestAppVerUrl" parentElement:tbXml.rootXMLElement];
        
        [defaults setValue:[TBXML textForElement:latestAppVerUrlElement] forKey:KEY_LINK];
        
        if (updateVersionTextElement!=nil){
            
            [self request_updateDeviceDetails];
            
            //give forced update popup
            
            UIAlertController * alert1;
          
                alert1=[UIAlertController alertControllerWithTitle:[TBXML textForElement:updateVersionTextElement] message:nil preferredStyle:UIAlertControllerStyleAlert];
           
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[defaults valueForKey:KEY_LINK]]];
                                     exit(0);

                                 }];
            [alert1 addAction:ok];
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert1 animated:YES completion:nil];
          
            //[alert1 release];
            return NO;
        }
    }
    else {
        //user has the latest version
    }
    
    
    return YES;
}
- (ScrollTemplateViewController *)scrollViewController {
    return [[ScrollTemplateViewController alloc] initWithNibName:@"ScrollTemplateViewController" bundle:nil];
}
- (CombinationViewController *)combViewController {
    return [[CombinationViewController alloc] init];
}

- (BlockViewController *)BlockViewController {
    return [[BlockViewController alloc] initWithNibName:@"BlockViewController" bundle:nil];
}

- (CustomizedNavController *)navigationControllerNews
{
    
    
    if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Combination News Template"])
    {
        navigationController =  [[CustomizedNavController alloc] initWithRootViewController:[self combViewController]];
        [navigationController hideHambergerButton: YES];
        [navigationController hideBackButton:YES];
        return navigationController;
        
    }
    else if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"News Tile Template"])
    {
        navigationController =  [[CustomizedNavController alloc] initWithRootViewController:[self BlockViewController]];
        [navigationController hideHambergerButton: YES];
        [navigationController hideBackButton:YES];
        return navigationController;
        
    }
    else
    {
        navigationController =  [[CustomizedNavController alloc] initWithRootViewController:[self scrollViewController]];
        [navigationController hideHambergerButton: YES];
        [navigationController hideBackButton:YES];
        return navigationController;
        
    }
    
}

- (void) registerPushNotifications:(UIApplication *) application {
    
    if ([application respondsToSelector:@selector(registerForRemoteNotifications)]) {// iOS 8.0 and later
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound) categories:nil]];
        [application registerForRemoteNotifications];
    } else { // below iOS 8.0
        
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
}

#pragma mark - Service Respone methods

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];

}


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    // Update Device Details successfully sent to the server
    // [self responseData:responseObject];
    
}


-(void) request_updateDeviceDetails
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    if ([defaults valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    
    if ([defaults valueForKey:KEY_DEVICEID]) {
        [parameters setValue:[defaults valueForKey:KEY_DEVICEID] forKey:@"deviceId"];
    }
    
    [parameters setValue:@"IOS" forKey:@"platform"];
    
    if (AppVersion) {
        [parameters setValue:AppVersion forKey:@"appVersion"];
    }
    [parameters setValue:[[UIDevice currentDevice] systemVersion]forKey:@"osVersion"];
    
    DLog(@"parametr: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/updatedevicedetails",BASE_URL];
    
    DLog(@"Url: %@",urlString);
    [client sendRequest: parameters : urlString];
    
}

//required for FTShare
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    if (self.share.facebook) {
        return [self.share.facebook handleOpenURL:url];
    }
    
    if([[url path] containsString:@"/SSQR"]){
        
        [container setMenuState:MFSideMenuStateClosed];
        [defaults setBool:YES forKey:@"fromShareLink"];
        NSString *urlStr = [url absoluteString];
        //if(IOS9)
       // urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]
        urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
        [defaults setValue:urlStr forKey:KEY_URL];
        NSDictionary *dict = [self parseQueryString:[url query]];
        [defaults setValue:dict forKey:@"UrlQuery"];
        if([defaults boolForKey:@"LoginSuccess"]==NO){
            
//            
//            UIAlertController * alert1;
//                alert1=[UIAlertController alertControllerWithTitle:@"Please Log in to view the Shared Page" message:nil preferredStyle:UIAlertControllerStyleAlert];
//                
//            UIAlertAction* ok = [UIAlertAction
//                                 actionWithTitle:@"OK"
//                                 style:UIAlertActionStyleDefault
//                                 handler:^(UIAlertAction * action){
//                                     {//for SSQR
//                                         
//                                         //if 'ok' button tapped take user to Shared screen
//                                         if([Network currentReachabilityStatus]==0)
//                                         {
//                                             [UtilityManager showAlert:nil msg:@"Network Failure. Please try later"];
//                                            
//                                             //[alert1 release];
//                                         }
//                                         
//                                         if ([defaults boolForKey:@"LoginSuccess"]==YES) {
//                                             if (userIdentifier){
//                                                 [UtilityManager showAlert:nil msg:@"Please Sign up to view the Shared Page"];
//                                                                                                 // [alert1 release];
//                                             }
//                                             else {
//                                                 [defaults setBool:NO forKey:@"fromShareLink"];
//                                                 [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:nil];
//                                                 WebBrowserViewController *webBrowser = [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
//                                                 [container.centerViewController pushViewController:webBrowser animated:NO];
//                                                 // [webBrowser release];
//                                             }
//                                         }
//                                         else{
//                                             if ([defaults boolForKey:@"rememberMe"]==YES){
//                                                 [self requestForLogin];
//                                             }
//                                             else{
//                                              
//                                             }
//                                         }
//                                     }
//                                 }];
//            [alert1 addAction:ok];
//            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert1 animated:YES completion:nil];
                        //[alert1 release];
        }
        else {
            [defaults setBool:NO forKey:@"fromShareLink"];
            [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:nil];
            WebBrowserViewController *webBrowser = [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [container.centerViewController pushViewController:webBrowser animated:NO];
            //[webBrowser release];
        }
    }
    return YES;
}

-(NSArray*) removeDuplicate : (NSMutableArray*) responseObj
{
    NSSet *set = [NSSet setWithArray:responseObj];
    NSArray *uniqueArray = [set allObjects];
    return uniqueArray;
}

-(void) pushalertViewClicked{//if 'view' button tapped take user to Push Notification list screen
    message = @"";
    [container setMenuState:MFSideMenuStateClosed];
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:nil msg:@"Network Failure. Please try later"];
    }
    
    if ([defaults boolForKey:@"LoginSuccess"]==YES) {
        if (userIdentifier){
           
            [defaults setBool:YES forKey:@"canDisplayAPNSListScreen"];
            [UtilityManager showAlert:nil msg:@"Please Sign up to view the Notification"];
        
        }
        else if ([defaults boolForKey:@"canDisplayAPNSListScreen"]==NO) {
            [defaults setBool:YES forKey:@"canDisplayAPNSListScreen"];
            
            [self sendResetBadgeRequest];
            
            NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
            
            
            // You can pass your index here
            
            
            for(int i = 0 ; i < [viewControllers count] ; ++i) {
                id viewController = [viewControllers objectAtIndex:i];
                if ([viewController isKindOfClass: [APNSListViewController class]]) {
                    [viewControllers removeObjectAtIndex: i];
                }
                NSLog(@"viewc:%@",viewController);
            }
            
            self.navigationController.viewControllers = viewControllers;
            
            
            
            viewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
            
            for(int i = 0 ; i < [viewControllers count] ; ++i) {
                id viewController = [viewControllers objectAtIndex:i];
                
                NSLog(@"viewc:%@",viewController);
            }
            
            
//            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
//
//            for (UIViewController *aViewController in allViewControllers)
//            {
//                if ([aViewController isKindOfClass: [APNSListViewController class]]) {
//                    
//                    [self.navigationController popViewControllerAnimated:NO];
//                }
//            }
            //APNSListViewController *pushotifyList = [[APNSListViewController alloc] initWithNibName:@"APNSListViewController" bundle:[NSBundle mainBundle]];
            rssFeedListDOArr = [[NSMutableArray alloc]init];
            //DLog(@"pushNoteList %@", pushNoteList);
            
            if ([defaults valueForKey:@"PushNotifyResponse"]) {
                
                NSMutableArray* notificationMsgListpush = [defaults valueForKey:@"PushNotifyResponse"];
                // NSArray* uniqueArray = [self removeDuplicate : notificationMsgListpush];
                [self parsePushNotify:notificationMsgListpush];
                
            }
            
            //DLog(@"notificationListArr %@", notificationListArr);
            //pushotifyList.pushNoteList = notificationListArr;
            //[self.navigationController pushViewController:pushotifyList animated:NO];
            //[pushotifyList release];
        }
    }
    else{
        if ([defaults boolForKey:@"rememberMe"]==YES){
            
            
            [defaults setBool:YES forKey:@"canDisplayAPNSListScreen"];
             [UtilityManager showAlert:nil msg:@"Please Login to view the Notification"];
           
        }
        else
        {
            [defaults setBool:YES forKey:@"canDisplayAPNSListScreen"];
            [UtilityManager showAlert:nil msg:@"Please Login to view the Notification"];
        }
    }
}



-(void)parsePushNotify:(NSMutableArray*)responseObj{
    
    //  NSArray * responseObj = [self compareExpDate : responseObject];
    
    
    // TBXML *tbxml = [[TBXML tbxmlWithXMLString:str] ;
    //    NSError *error;
    //
    //    //NSDictionary *pushData = responseObj;
    //
    //
    //    NSData *data = [responseObj dataUsingEncoding:NSUTF8StringEncoding];
    //
    //    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];//response object is your response from server as NSData
    NSMutableArray *notificationExpireFilter = [responseObj mutableCopy];
    NSMutableArray *indexes = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [responseObj count]; i++) {
        
        NSDictionary *dic=[responseObj objectAtIndex:i];
        
        NSArray *rssFeedArr = nil; //[[NSArray alloc]init];
        NSArray *dealArr = nil;//[[NSArray alloc]init];
        
        if ([dic valueForKey:@"rssFeedList"]) {
            rssFeedArr = [dic valueForKey:@"rssFeedList"];
            RssFeedListDO *obj_rssFeedListDO = [[RssFeedListDO alloc]init];
            for (NSDictionary *dictionary in rssFeedArr) {
                //Staff *staff = [[Staff alloc] init];
                obj_rssFeedListDO.title = [dictionary objectForKey:@"title"];
                obj_rssFeedListDO.link = [dictionary objectForKey:@"link"];
                
            }
            
            [rssFeedListDOArr addObject:obj_rssFeedListDO];
        }
        if ([dic valueForKey:@"dealList"]) {
            dealArr = [dic valueForKey:@"dealList"];
            RssFeedListDO *obj_rssFeedListDO = [[RssFeedListDO alloc]init];
            for (NSDictionary *dictionary in dealArr) {
                //Staff *staff = [[Staff alloc] init];
                obj_rssFeedListDO.dealName  = [dictionary objectForKey:@"dealName"];
                obj_rssFeedListDO.type  = [dictionary objectForKey:@"type"];
                obj_rssFeedListDO.splUrl = [dictionary objectForKey:@"splUrl"];
                obj_rssFeedListDO.dealId = [dictionary objectForKey:@"dealId"];
                obj_rssFeedListDO.retailerId = [dictionary objectForKey:@"retailerId"];
                obj_rssFeedListDO.retailLocationId = [dictionary objectForKey:@"retailLocationId"];
                obj_rssFeedListDO.endDate = [dictionary objectForKey:@"endDate"];
                obj_rssFeedListDO.endTime = [dictionary objectForKey:@"endTime"];
            }
            
            if (![HUBCITIKEY isEqualToString:@"Tyler19"])
            {
                
                
                int compare = [self checkForExpiry : obj_rssFeedListDO.endDate : obj_rssFeedListDO.endTime];
                
                if (compare > 0) {
                    
                    NSLog(@"don't add expired coupon");
                    [indexes addObject:[NSNumber numberWithInt:i]];
                }
                else
                {
                    [rssFeedListDOArr addObject:obj_rssFeedListDO];
                }
            }
            else{
                
                [rssFeedListDOArr addObject:obj_rssFeedListDO];
            }
            
            
        }
    }
    
    DLog(@"rssFeedListDOArr %@", rssFeedListDOArr);
    if(indexes.count>0)
    {
        for(int i=0;i<indexes.count;i++){
            NSInteger indexDel= [[indexes objectAtIndex:i] integerValue];
            
            if(notificationExpireFilter.count > 0)
                [notificationExpireFilter removeObjectAtIndex:indexDel];
            
            // remove expired deals from global array
            
            if(notificationListArr.count > 0)
                [notificationListArr removeObjectAtIndex:indexDel];
        }
    }
    
    if (rssFeedListDOArr.count == 0) {
        [UtilityManager showAlert:nil msg:@"Deals has expired"];
        
        return;
    }
    
    if(responseObj.count > rssFeedListDOArr.count)
    {
        [UtilityManager showAlert:nil msg:@"Some Deals are expired"];
        [defaults setValue:notificationExpireFilter forKey:@"PushNotifyResponse"];
        
    }
    
    if ([rssFeedListDOArr count] < 2) {
        
        RssFeedListDO *obj_rssFeedListDO ;
        obj_rssFeedListDO = [rssFeedListDOArr objectAtIndex:0];
        
        //[defaults setBool:YES forKey:@"detailsPage"];
        
        if (obj_rssFeedListDO.title == nil) {
            
            
            if ([obj_rssFeedListDO.type isEqualToString:@"Hotdeals"]) {
                [defaults setValue:obj_rssFeedListDO.dealId forKey:@"HotDealId"];
                HotDealDetailViewController *hdd = [[HotDealDetailViewController alloc] initWithNibName:@"HotDealDetailViewController" bundle:[NSBundle mainBundle]];
                [container.centerViewController pushViewController:hdd animated:NO];
                // [hdd release];
            }
            
            else if([obj_rssFeedListDO.type isEqualToString:@"Coupons"]){
                [defaults setValue:obj_rssFeedListDO.dealId forKey:@"couponId"];
                NewCouponDetailViewController *obj_CouponGalleryDetail = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
               [container.centerViewController pushViewController:obj_CouponGalleryDetail animated:NO];
                // //[obj_CouponGalleryDetail release];
            }
            
            else if ([obj_rssFeedListDO.type isEqualToString:@"SpecialOffers"]){
                
                if (obj_rssFeedListDO.splUrl == nil) {
                    [defaults setValue:obj_rssFeedListDO.dealId forKey:@"nativePageId"];
                    [defaults setValue:obj_rssFeedListDO.retailLocationId forKey:@"retailLocationID"];
                    [defaults setValue:obj_rssFeedListDO.retailerId forKey:@"retailerId"];
                    NativeSpecialOfferViewController *splOfferVC = [[NativeSpecialOfferViewController alloc]initWithNibName:@"NativeSpecialOfferViewController" bundle:[NSBundle mainBundle]];
                    [container.centerViewController pushViewController:splOfferVC animated:NO];
                    // //[splOfferVC release];
                }
                
                else{
                    
                    [defaults setValue:obj_rssFeedListDO.splUrl forKey:KEY_URL];
                    
                    WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                    [container.centerViewController pushViewController:urlDetail animated:NO];
                    ////[urlDetail release];
                }
            }
        }
        else{
            [defaults setValue:obj_rssFeedListDO.link forKey:KEY_URL];
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [container.centerViewController pushViewController:urlDetail animated:NO];
            ////[urlDetail release];
        }
    }
    else{
        APNSListViewController *pushotifyList = [[APNSListViewController alloc] initWithNibName:@"APNSListViewController" bundle:[NSBundle mainBundle]];
        pushotifyList.rssFeedListDOArr = [[NSMutableArray alloc]init];
        pushotifyList.rssFeedListDOArr = rssFeedListDOArr;
        [container.centerViewController pushViewController:pushotifyList animated:NO];
    }
    
    
    
    
    //    TBXMLElement *rssFeedList = [TBXML childElementNamed:@"rssFeedList" parentElement:tbxml.rootXMLElement];
    //    if(rssFeedList){
    //
    //    TBXMLElement *RSSFeedMessage = [TBXML childElementNamed:@"RSSFeedMessage" parentElement:rssFeedList];
    //
    //
    //    while (RSSFeedMessage!=nil) {
    //        RssFeedListDO *obj_rssFeedListDO = [[RssFeedListDO alloc]init];
    //        TBXMLElement *titleElement = [TBXML childElementNamed:@"title" parentElement:RSSFeedMessage];
    //        TBXMLElement *linkElement = [TBXML childElementNamed:@"link" parentElement:RSSFeedMessage];
    //        if (linkElement) {
    //            obj_rssFeedListDO.link = [[NSString alloc]initWithString:[TBXML textForElement:linkElement]];
    //        }
    //        if(titleElement)
    //            obj_rssFeedListDO.title = [[NSString alloc]initWithString:[TBXML textForElement:titleElement]];
    //        RSSFeedMessage = [TBXML nextSiblingNamed:@"RSSFeedMessage" searchFromElement:RSSFeedMessage];
    //        [rssFeedListDOArr addObject:obj_rssFeedListDO];
    //    }
    //    }
    //
    //    TBXMLElement *dealList = [TBXML childElementNamed:@"dealList" parentElement:tbxml.rootXMLElement];
    //    if (dealList) {
    //        TBXMLElement *dealElement = [TBXML childElementNamed:@"Deal" parentElement:dealList];
    //        while (dealElement!=nil) {
    //            RssFeedListDO *obj_rssFeedListDO = [[RssFeedListDO alloc]init];
    //
    //            TBXMLElement *dealIdElement = [TBXML childElementNamed:@"dealId" parentElement:dealElement];
    //            TBXMLElement *dealNameElement = [TBXML childElementNamed:@"dealName" parentElement:dealElement];
    //            TBXMLElement *dealDescElement = [TBXML childElementNamed:@"dealDesc" parentElement:dealElement];
    //            TBXMLElement *typeElement = [TBXML childElementNamed:@"type" parentElement:dealElement];
    //            TBXMLElement *splUrlElement = [TBXML childElementNamed:@"splUrl" parentElement:dealElement];
    //
    //
    //            if(dealIdElement)
    //                obj_rssFeedListDO.dealId = [[NSString alloc]initWithString:[TBXML textForElement:dealIdElement]];
    //
    //            if(dealNameElement)
    //                obj_rssFeedListDO.dealName = [[NSString alloc]initWithString:[TBXML textForElement:dealNameElement]];
    //
    //            if(dealDescElement)
    //                obj_rssFeedListDO.dealDesc = [[NSString alloc]initWithString:[TBXML textForElement:dealDescElement]];
    //
    //            if(typeElement)
    //                obj_rssFeedListDO.type = [[NSString alloc]initWithString:[TBXML textForElement:typeElement]];
    //
    //            if(splUrlElement)
    //                obj_rssFeedListDO.splUrl = [[NSString alloc]initWithString:[TBXML textForElement:splUrlElement]];
    //
    //            dealElement = [TBXML nextSiblingNamed:@"Deal" searchFromElement:dealElement];
    //            [rssFeedListDOArr addObject:obj_rssFeedListDO];
    //        }
    //
    //    }
}


-(int) checkForExpiry : (NSString*) endDate : (NSString*) endTime
{
    NSString* dateString;
    if (endDate.length > 0  && endTime.length > 0) {
        
        dateString = [NSString stringWithFormat:@"%@ %@", endDate, endTime];
    }
    NSDateFormatter *datePickerFormat = [[NSDateFormatter alloc] init];
    [datePickerFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    datePickerFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    
    
    NSString *today = [datePickerFormat stringFromDate:[NSDate date]];
    NSDate *currentDate = [datePickerFormat dateFromString:today];
    NSDate *serverDate = [datePickerFormat dateFromString:dateString];
    
    NSComparisonResult result;
    
    result = [currentDate compare:serverDate]; // comparing two dates
    
    if(result == NSOrderedAscending)
    {
        NSLog(@"current date is less than server date");
        return -1;
        
    }
    else if(result == NSOrderedDescending)
    {
        NSLog(@"current date is greater than server date ");
        return 1;
    }
    
    else if(result == NSOrderedSame)
    {
        NSLog(@"both are equal");
        return 0;
    }
    return 0;
    
}









//-(void)requestForSplash
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//    [xmlStr appendFormat:@"<LoginFlowDetails><pageType>Splash Image</pageType>"];
//    [xmlStr appendFormat:@"<hubCitiKey>%@</hubCitiKey></LoginFlowDetails>",HUBCITIKEY];
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/loginflow",BASE_URL];
//
//    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
//    [self parseSplashData:responseXML];
//    ReleaseAndNilify(xmlStr);
//
//}
//
//
//-(void)parseSplashData:(NSString *)respo {
//
//    if ([UtilityManager isResponseXMLNullOrEmpty:respo])
//    {
//        [defaults setObject:@"Default.png" forKey:@"SplashImage"];
//    }
//    else{
//        TBXML *tbXml = [TBXML tbxmlWithXMLString:respo];
//
//        TBXMLElement *splashImgElement = [TBXML childElementNamed:@"splashImg" parentElement:tbXml.rootXMLElement];
//        TBXMLElement *smallLogoElement = [TBXML childElementNamed:@"smallLogo" parentElement:tbXml.rootXMLElement];
//
//        if(splashImgElement != NULL) {
//
//            [defaults setObject:[TBXML textForElement:splashImgElement] forKey:@"SplashImage"];
//        }
//
//        if (smallLogoElement!=nil)
//            [defaults setValue:[TBXML textForElement:smallLogoElement] forKey:HC_LOGOIMGPATH];
//    }
//     [self requestForCustomLoginUI];
//     sleep(3);
//    [self onSplashScreenExpired];
//}
//

//-(void)showSplash
//{
//    if([defaults objectForKey:@"SplashImage"] != NULL)
//    {
//        UIViewController *modalViewController = [[[UIViewController alloc] init];
//
//        splashScreen = [[[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, VIEW_FRAME_WIDTH, VIEW_FRAME_HEIGHT)] ;
//        splashScreen.contentMode = UIViewContentModeScaleToFill;
//        splashScreen.backgroundColor = [UIColor clearColor];
//        [splashScreen loadImage:[defaults valueForKey:@"SplashImage"]];
//
//        [modalViewController.view addSubview:splashScreen];
//        [self.window addSubview:modalViewController.view];
//
//        [self.window makeKeyAndVisible];
//       // [self requestForCustomLoginUI];
//
//      //  [self performSelector:@selector(onSplashScreenExpired) withObject:nil afterDelay:5.0];
//
//        [self.window addSubview:splashScreen];
//    }
//    else
//    {
//        UIImageView *splash1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, VIEW_FRAME_WIDTH, VIEW_FRAME_HEIGHT)];
//        [splash1 setImage:[UIImage imageNamed:@"Default.png"]];
//        [self.window addSubview:splash1];
//        [splash1 release];
//    }
//
//    [self requestForCustomLoginUI];
//
//    [self performSelector:@selector(onSplashScreenExpired) withObject:nil afterDelay:3.0];
//}

-(void) setTrainingView
{

    TutorialViewController* tutViewController = [[TutorialViewController alloc] init];
    navigationController = [[CustomizedNavController alloc] initWithRootViewController:tutViewController];
    container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:navigationController
                                                    leftMenuViewController:nil
                                                    rightMenuViewController:nil];
    
    
    [self.window setRootViewController:container];
    [self.window makeKeyAndVisible];
    
    

}

-(void) onSkipTutorial : (NSNotification *)noty
{
    TutorialViewController* tv = noty.object;
    if ([tv.viewType isEqualToString:@"SignUp"]) {
        
        
        
        self.loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        self.loginViewController.login_DO = loginDO;
        navigationController = [[CustomizedNavController alloc] initWithRootViewController:self.loginViewController];
        //Navigate to main menu page
        [navigationController hideHambergerButton: YES];
        [navigationController hideBackButton:YES];
        
    
        userIdentifier=TRUE;
      
        
        RegistrationViewController *regView = [[RegistrationViewController alloc]initWithNibName:@"RegistrationViewController" bundle:nil];
        
        regView.signup_DO = loginDO;
        
        container = [MFSideMenuContainerViewController
                                                        containerWithCenterViewController:navigationController
                                                        leftMenuViewController:nil
                                                        rightMenuViewController:nil];
        [container.centerViewController pushViewController:regView animated:NO];
        
        [self.window setRootViewController:container];
        [self.window makeKeyAndVisible];

    }
    else
    {
        
        if ([defaults boolForKey:@"newsTemplateExist"] == YES)
        {
            
            
            
                NSMutableString *xmlStr = [[NSMutableString alloc] init];
                [defaults setBool:NO forKey:@"LoginUserNews"];
                userIdentifier = TRUE;
                
                guestUser = @"WelcomeScanSeeGuest";
                guestPassword = @":::We@Love!!ScanSee?{People}";
                [defaults  setBool:NO forKey:@"rememberMe"];
                [SharedManager setGps_allow_flag:YES];
                [defaults setValue:@"YES" forKey:@"allowingGPS"];
            
                [xmlStr appendFormat:@"<UserDetails><userName><![CDATA[%@]]></userName>",guestUser];
                [xmlStr appendFormat:@"<password><![CDATA[%@]]></password>", guestPassword];
                [xmlStr appendFormat:@"<appVersion>%@</appVersion>",AppVersion];
                [xmlStr appendFormat:@"<deviceId>%@</deviceId>",[defaults valueForKey:KEY_DEVICEID]];
                [xmlStr appendFormat:@"<hubCitiKey>%@</hubCitiKey><platform>IOS</platform></UserDetails>",HUBCITIKEY];
                
                
                
                NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/getuserlogin",BASE_URL];
            
                
                NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
                
                ReleaseAndNilify(xmlStr);
                if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
                {
                    
                    return ;
                }
                
                TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
                TBXMLElement *htmlTag = [TBXML childElementNamed:@"head" parentElement:tbXml.rootXMLElement];
                if (htmlTag != nil)
                {
                    //            [UtilityManager showAlert:NSLocalizedString(@"Technical Problem",@"Technical Problem") msg:NSLocalizedString(@"please try again later",@"please try again later")];
                    return ;
                }
                
                TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
                if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
                {
                    TBXMLElement *userIdElement = [TBXML childElementNamed:KEY_USERID parentElement:tbXml.rootXMLElement];
                    TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:KEY_HUBCITIID parentElement:tbXml.rootXMLElement];
                    TBXMLElement *hubCitiNameElement = [TBXML childElementNamed:KEY_HUBCITINAME parentElement:tbXml.rootXMLElement];
                    TBXMLElement * isTempUserElement = [TBXML childElementNamed:@"isTempUser" parentElement:tbXml.rootXMLElement];
                    TBXMLElement * noEmailCountElement = [TBXML childElementNamed:@"noEmailCount" parentElement:tbXml.rootXMLElement];
                    if ([RegionApp isEqualToString:@"1"] && ![defaults valueForKey:@"sortedCitiIDs"]) {
                        [self sortCitiId : tbXml];
                    }
                    
                    
                    [defaults  setObject:[TBXML textForElement:userIdElement] forKey:KEY_USERID];
                    [defaults  setObject:[TBXML textForElement:hubCitiIdElement] forKey:KEY_HUBCITIID];
                    [defaults  setObject:[TBXML textForElement:hubCitiNameElement] forKey:KEY_HUBCITINAME];
                    //                if ([[TBXML textForElement:noEmailCountElement] isEqualToString:@"1"] )
                    //                {
                    ////                    [[NSNotificationCenter defaultCenter]
                    ////                     postNotificationName:@"emailCount"
                    ////                     object:self];
                    //                    NSNotification *notification;
                    //                    [NewsUtility emailCountDisplay:notification onViewController:self.window.rootViewController];
                    //                }
                   NSString *token = [defaults valueForKey:@"TOKEN_ID"];
                    if(token.length > 0)
                        [NewsUtility setUserSettings];
                    emailCountString = [TBXML textForElement:noEmailCountElement];
                    tempUserString =[TBXML textForElement:isTempUserElement];
                    
                    [self setNewsTemplate];
                
                
            }
            
        }
        else{
            
                
                NSMutableString *xmlStr = [[NSMutableString alloc] init];
                [defaults setBool:NO forKey:@"LoginUserNews"];
                userIdentifier = TRUE;
                
                guestUser = @"WelcomeScanSeeGuest";
                guestPassword = @":::We@Love!!ScanSee?{People}";
                [defaults  setBool:NO forKey:@"rememberMe"];
             [SharedManager setGps_allow_flag:YES];
                 [defaults setValue:@"YES" forKey:@"allowingGPS"];
                [xmlStr appendFormat:@"<UserDetails><userName><![CDATA[%@]]></userName>",guestUser];
                [xmlStr appendFormat:@"<password><![CDATA[%@]]></password>", guestPassword];
                [xmlStr appendFormat:@"<appVersion>%@</appVersion>",AppVersion];
                [xmlStr appendFormat:@"<deviceId>%@</deviceId>",[defaults valueForKey:KEY_DEVICEID]];
                [xmlStr appendFormat:@"<hubCitiKey>%@</hubCitiKey><platform>IOS</platform></UserDetails>",HUBCITIKEY];
                
                
                
                NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/getuserlogin",BASE_URL];
            
                
                NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
                
                ReleaseAndNilify(xmlStr);
                if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
                {
                    
                    return ;
                }
                
                TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
                TBXMLElement *htmlTag = [TBXML childElementNamed:@"head" parentElement:tbXml.rootXMLElement];
                if (htmlTag != nil)
                {
                    //            [UtilityManager showAlert:NSLocalizedString(@"Technical Problem",@"Technical Problem") msg:NSLocalizedString(@"please try again later",@"please try again later")];
                    return ;
                }
                
                TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
                if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
                {
                    TBXMLElement *userIdElement = [TBXML childElementNamed:KEY_USERID parentElement:tbXml.rootXMLElement];
                    TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:KEY_HUBCITIID parentElement:tbXml.rootXMLElement];
                    TBXMLElement *hubCitiNameElement = [TBXML childElementNamed:KEY_HUBCITINAME parentElement:tbXml.rootXMLElement];
                    TBXMLElement * isTempUserElement = [TBXML childElementNamed:@"isTempUser" parentElement:tbXml.rootXMLElement];
                    TBXMLElement * noEmailCountElement = [TBXML childElementNamed:@"noEmailCount" parentElement:tbXml.rootXMLElement];
                    if ([RegionApp isEqualToString:@"1"] && ![defaults valueForKey:@"sortedCitiIDs"]) {
                        [self sortCitiId : tbXml];
                    }
                   
                    
                    [defaults  setObject:[TBXML textForElement:userIdElement] forKey:KEY_USERID];
                    [defaults  setObject:[TBXML textForElement:hubCitiIdElement] forKey:KEY_HUBCITIID];
                    [defaults  setObject:[TBXML textForElement:hubCitiNameElement] forKey:KEY_HUBCITINAME];
                    NSString *token = [defaults valueForKey:@"TOKEN_ID"];
                    if(token.length > 0)
                        [NewsUtility setUserSettings];
                    emailCountString = [TBXML textForElement:noEmailCountElement];
                    tempUserString =[TBXML textForElement:isTempUserElement];
                    
                    self.loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
                    self.loginViewController.login_DO = loginDO;
                    navigationController = [[CustomizedNavController alloc] initWithRootViewController:self.loginViewController];
                    //Navigate to main menu page
                    [navigationController hideHambergerButton: YES];
                    [navigationController hideBackButton:YES];
                    mainMenuController = [[MainMenuViewController alloc] initWithNibName:@"MainMenuViewController" bundle:nil];
                    
                    container = [MFSideMenuContainerViewController
                                 containerWithCenterViewController:navigationController
                                 leftMenuViewController:nil
                                 rightMenuViewController:nil];
                    [container.centerViewController pushViewController:mainMenuController animated:NO];
                    
                    [self.window setRootViewController:container];
                    [self.window makeKeyAndVisible];
                    [self popUPForEmailCount:tempUserString: emailCountString];
                
                }
                
            
            
        }
        
        
    }
}

-(void)onSplashScreenExpired
{
    if(![defaults boolForKey:@"isFirstTimeTutorial"])
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onSkipTutorial:)
                                                     name:@"tutorial"
                                                   object:nil];
        
        [self setTrainingView];
        
        return ;
    }
    if ([defaults boolForKey:@"newsTemplateExist"] == YES)
    {
        if ([self authenticateUser])
        {
            //[defaults setValue:@"3" forKey:KEY_USERID];
            
            [self setNewsTemplate];
        }
        else
        {
            NSMutableString *xmlStr = [[NSMutableString alloc] init];
            [defaults setBool:NO forKey:@"LoginUserNews"];
            userIdentifier = TRUE;
            
            guestUser = @"WelcomeScanSeeGuest";
            guestPassword = @":::We@Love!!ScanSee?{People}";
            [defaults  setBool:NO forKey:@"rememberMe"];
            [SharedManager setGps_allow_flag:YES];
            [defaults setValue:@"YES" forKey:@"allowingGPS"];
            [xmlStr appendFormat:@"<UserDetails><userName><![CDATA[%@]]></userName>",guestUser];
            [xmlStr appendFormat:@"<password><![CDATA[%@]]></password>", guestPassword];
            [xmlStr appendFormat:@"<appVersion>%@</appVersion>",AppVersion];
            [xmlStr appendFormat:@"<deviceId>%@</deviceId>",[defaults valueForKey:KEY_DEVICEID]];
            [xmlStr appendFormat:@"<hubCitiKey>%@</hubCitiKey><platform>IOS</platform></UserDetails>",HUBCITIKEY];
            
            
            
            NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/getuserlogin",BASE_URL];
            
            
            NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
            
            ReleaseAndNilify(xmlStr);
            if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
            {
                
                return ;
            }
            
            TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
            TBXMLElement *htmlTag = [TBXML childElementNamed:@"head" parentElement:tbXml.rootXMLElement];
            if (htmlTag != nil)
            {
                //            [UtilityManager showAlert:NSLocalizedString(@"Technical Problem",@"Technical Problem") msg:NSLocalizedString(@"please try again later",@"please try again later")];
                return ;
            }
            
            TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
            if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
            {
                TBXMLElement *userIdElement = [TBXML childElementNamed:KEY_USERID parentElement:tbXml.rootXMLElement];
                TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:KEY_HUBCITIID parentElement:tbXml.rootXMLElement];
                TBXMLElement *hubCitiNameElement = [TBXML childElementNamed:KEY_HUBCITINAME parentElement:tbXml.rootXMLElement];
                TBXMLElement * isTempUserElement = [TBXML childElementNamed:@"isTempUser" parentElement:tbXml.rootXMLElement];
                TBXMLElement * noEmailCountElement = [TBXML childElementNamed:@"noEmailCount" parentElement:tbXml.rootXMLElement];
                if ([RegionApp isEqualToString:@"1"] && ![defaults valueForKey:@"sortedCitiIDs"]) {
                    [self sortCitiId : tbXml];
                }
               
                
                [defaults  setObject:[TBXML textForElement:userIdElement] forKey:KEY_USERID];
                [defaults  setObject:[TBXML textForElement:hubCitiIdElement] forKey:KEY_HUBCITIID];
                [defaults  setObject:[TBXML textForElement:hubCitiNameElement] forKey:KEY_HUBCITINAME];
                NSString *token = [defaults valueForKey:@"TOKEN_ID"];
                if(token.length > 0)
                    [NewsUtility setUserSettings];

                //                if ([[TBXML textForElement:noEmailCountElement] isEqualToString:@"1"] )
                //                {
                ////                    [[NSNotificationCenter defaultCenter]
                ////                     postNotificationName:@"emailCount"
                ////                     object:self];
                //                    NSNotification *notification;
                //                    [NewsUtility emailCountDisplay:notification onViewController:self.window.rootViewController];
                //                }
                emailCountString = [TBXML textForElement:noEmailCountElement];
                tempUserString =[TBXML textForElement:isTempUserElement];
                
                [self setNewsTemplate];
            }
            
        }
    }
    else
    {
        //	[splashScreen removeFromSuperview];
        
        /*******call next view after splash ********/
        
        BOOL result = [defaults  boolForKey:@"rememberMe"];
        if (result && [self authenticateUser])
        {
            self.loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
            self.loginViewController.login_DO = loginDO;
            navigationController = [[CustomizedNavController alloc] initWithRootViewController:self.loginViewController];
            //Navigate to main menu page
            
            mainMenuController = [[MainMenuViewController alloc] initWithNibName:@"MainMenuViewController" bundle:nil];
            [navigationController hideHambergerButton: YES];
            [navigationController hideBackButton:YES];
            container = [MFSideMenuContainerViewController
                                                            containerWithCenterViewController:navigationController
                                                            leftMenuViewController:nil
                                                            rightMenuViewController:nil];
            [container.centerViewController pushViewController:mainMenuController animated:NO];
            
            [self.window setRootViewController:container];
            [self.window makeKeyAndVisible];
            [self popUPForEmailCount:tempUserString: emailCountString];
            
            
        }
        
        else {
           
            
            NSMutableString *xmlStr = [[NSMutableString alloc] init];
            [defaults setBool:NO forKey:@"LoginUserNews"];
            userIdentifier = TRUE;
            
            guestUser = @"WelcomeScanSeeGuest";
            guestPassword = @":::We@Love!!ScanSee?{People}";
            [defaults  setBool:NO forKey:@"rememberMe"];
            [SharedManager setGps_allow_flag:YES];
            [defaults setValue:@"YES" forKey:@"allowingGPS"];
            [xmlStr appendFormat:@"<UserDetails><userName><![CDATA[%@]]></userName>",guestUser];
            [xmlStr appendFormat:@"<password><![CDATA[%@]]></password>", guestPassword];
            [xmlStr appendFormat:@"<appVersion>%@</appVersion>",AppVersion];
            [xmlStr appendFormat:@"<deviceId>%@</deviceId>",[defaults valueForKey:KEY_DEVICEID]];
            [xmlStr appendFormat:@"<hubCitiKey>%@</hubCitiKey><platform>IOS</platform></UserDetails>",HUBCITIKEY];
            
            
            
            NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/getuserlogin",BASE_URL];
           
            
            NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
            
            ReleaseAndNilify(xmlStr);
            if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
            {
                
                return ;
            }
            
            TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
            TBXMLElement *htmlTag = [TBXML childElementNamed:@"head" parentElement:tbXml.rootXMLElement];
            if (htmlTag != nil)
            {
                //            [UtilityManager showAlert:NSLocalizedString(@"Technical Problem",@"Technical Problem") msg:NSLocalizedString(@"please try again later",@"please try again later")];
                return ;
            }
            
            TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
            if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
            {
                TBXMLElement *userIdElement = [TBXML childElementNamed:KEY_USERID parentElement:tbXml.rootXMLElement];
                TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:KEY_HUBCITIID parentElement:tbXml.rootXMLElement];
                TBXMLElement *hubCitiNameElement = [TBXML childElementNamed:KEY_HUBCITINAME parentElement:tbXml.rootXMLElement];
                TBXMLElement * isTempUserElement = [TBXML childElementNamed:@"isTempUser" parentElement:tbXml.rootXMLElement];
                TBXMLElement * noEmailCountElement = [TBXML childElementNamed:@"noEmailCount" parentElement:tbXml.rootXMLElement];
                if ([RegionApp isEqualToString:@"1"] && ![defaults valueForKey:@"sortedCitiIDs"]) {
                    [self sortCitiId : tbXml];
                }
               
                
                [defaults  setObject:[TBXML textForElement:userIdElement] forKey:KEY_USERID];
                [defaults  setObject:[TBXML textForElement:hubCitiIdElement] forKey:KEY_HUBCITIID];
                [defaults  setObject:[TBXML textForElement:hubCitiNameElement] forKey:KEY_HUBCITINAME];
                NSString *token = [defaults valueForKey:@"TOKEN_ID"];
                if(token.length > 0)
                    [NewsUtility setUserSettings];

                emailCountString = [TBXML textForElement:noEmailCountElement];
                tempUserString =[TBXML textForElement:isTempUserElement];
                
                self.loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
                self.loginViewController.login_DO = loginDO;
                navigationController = [[CustomizedNavController alloc] initWithRootViewController:self.loginViewController];
                //Navigate to main menu page
                [navigationController hideHambergerButton: YES];
                [navigationController hideBackButton:YES];
                mainMenuController = [[MainMenuViewController alloc] initWithNibName:@"MainMenuViewController" bundle:nil];
                
                container = [MFSideMenuContainerViewController
                                                                containerWithCenterViewController:navigationController
                                                                leftMenuViewController:nil
                                                                rightMenuViewController:nil];
                [container.centerViewController pushViewController:mainMenuController animated:NO];
                
                [self.window setRootViewController:container];
                [self.window makeKeyAndVisible];
                [self popUPForEmailCount:tempUserString: emailCountString];
            }
            
        }

            
            
        
        }
    }
    


-(void) setNewsTemplate
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    SideViewController *leftMenuViewController;
    if(IPAD){
        leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
    }
    else{
        leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
        
    }
    leftMenuViewController.loginDo = loginDO;
    leftMenuViewController.isNewsTemplate = TRUE;
    container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:[self navigationControllerNews]
                                                    leftMenuViewController:leftMenuViewController
                                                    rightMenuViewController:nil];
    
    self.window.rootViewController = container;
        [self.window makeKeyAndVisible];
    [self popUPForEmailCount:tempUserString: emailCountString];
    
}

//authenticate user with login credentials if "remember me" enabled
-(BOOL) authenticateUser
{
    BOOL value = [defaults boolForKey:@"LoginUserNews"];
    NSLog(@"loginuser: %d",(int)value);
    if([Network currentReachabilityStatus]==0){
        
        [UtilityManager showAlert];
        return NO;
    }
    
    else if([defaults boolForKey:@"newsTemplateExist"] == YES)
    {
        NSMutableString *xmlStr = [[NSMutableString alloc] init];
        if (![defaults boolForKey:@"LoginUserNews"])
        {
            [defaults setBool:NO forKey:@"LoginUserNews"];
            guestUser = @"WelcomeScanSeeGuest";
            guestPassword = @":::We@Love!!ScanSee?{People}";
            [defaults  setBool:NO forKey:@"rememberMe"];
            [SharedManager setGps_allow_flag:YES];
            [defaults setValue:@"YES" forKey:@"allowingGPS"];
            userIdentifier = TRUE;
            [xmlStr appendFormat:@"<UserDetails><userName><![CDATA[%@]]></userName>",guestUser];
            [xmlStr appendFormat:@"<password><![CDATA[%@]]></password>", guestPassword];
            [xmlStr appendFormat:@"<appVersion>%@</appVersion>",AppVersion];
            [xmlStr appendFormat:@"<deviceId>%@</deviceId>",[defaults valueForKey:KEY_DEVICEID]];
            [xmlStr appendFormat:@"<hubCitiKey>%@</hubCitiKey><platform>IOS</platform></UserDetails>",HUBCITIKEY];
            
            
            
            NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/getuserlogin",BASE_URL];
            
            
            NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
            
            ReleaseAndNilify(xmlStr);
            if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
            {
                
                return NO;
            }
            
            TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
            TBXMLElement *htmlTag = [TBXML childElementNamed:@"head" parentElement:tbXml.rootXMLElement];
            if (htmlTag != nil)
            {
                //            [UtilityManager showAlert:NSLocalizedString(@"Technical Problem",@"Technical Problem") msg:NSLocalizedString(@"please try again later",@"please try again later")];
                return NO;
            }
            
            TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
            if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
            {
                TBXMLElement *userIdElement = [TBXML childElementNamed:KEY_USERID parentElement:tbXml.rootXMLElement];
                TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:KEY_HUBCITIID parentElement:tbXml.rootXMLElement];
                TBXMLElement *hubCitiNameElement = [TBXML childElementNamed:KEY_HUBCITINAME parentElement:tbXml.rootXMLElement];                TBXMLElement * isTempUserElement = [TBXML childElementNamed:@"isTempUser" parentElement:tbXml.rootXMLElement];
                TBXMLElement * noEmailCountElement = [TBXML childElementNamed:@"noEmailCount" parentElement:tbXml.rootXMLElement];
                if ([RegionApp isEqualToString:@"1"] && ![defaults valueForKey:@"sortedCitiIDs"]) {
                    [self sortCitiId : tbXml];
                }
                
                
                [defaults  setObject:[TBXML textForElement:userIdElement] forKey:KEY_USERID];
                [defaults  setObject:[TBXML textForElement:hubCitiIdElement] forKey:KEY_HUBCITIID];
                [defaults  setObject:[TBXML textForElement:hubCitiNameElement] forKey:KEY_HUBCITINAME];
//                if ([[TBXML textForElement:noEmailCountElement] isEqualToString:@"1"] )
//                {
////                    [[NSNotificationCenter defaultCenter]
////                     postNotificationName:@"emailCount"
////                     object:self];
//                    NSNotification *notification;
//                    [NewsUtility emailCountDisplay:notification onViewController:self.window.rootViewController];
//                }
                NSString *token = [defaults valueForKey:@"TOKEN_ID"];
                if(token.length > 0)
                    [NewsUtility setUserSettings];
                 emailCountString = [TBXML textForElement:noEmailCountElement];
                 tempUserString =[TBXML textForElement:isTempUserElement];
                
                return YES;
            }
            
            else
            {
                return NO;
                
            }
            
            
        }
        else
        {
            userIdentifier = FALSE;
            [defaults setBool:YES forKey:@"LoginUserNews"];
            BOOL returnValue =   [self setUserLoginCredential];
            return returnValue;
        }
        
    }
    
    else
    {
        BOOL returnValue =   [self setUserLoginCredential];
        return returnValue;
    }
    
    return NO;
}

-(BOOL) setUserLoginCredential
{
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserDetails><userName><![CDATA[%@]]></userName>",[defaults valueForKey:@"Username"]];
    [xmlStr appendFormat:@"<password><![CDATA[%@]]></password><appVersion>%@</appVersion>",[defaults valueForKey:@"Password"],AppVersion];
    [xmlStr appendFormat:@"<deviceId>%@</deviceId><hubCitiKey>%@</hubCitiKey><platform>IOS</platform></UserDetails>",[defaults valueForKey:KEY_DEVICEID],HUBCITIKEY];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/getuserlogin",BASE_URL];
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    
    ReleaseAndNilify(xmlStr);
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
    {
        return NO;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement *htmlTag = [TBXML childElementNamed:@"head" parentElement:tbXml.rootXMLElement];
    if (htmlTag != nil)
    {
        //            [UtilityManager showAlert:NSLocalizedString(@"Technical Problem",@"Technical Problem") msg:NSLocalizedString(@"please try again later",@"please try again later")];
        return NO;
    }
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        TBXMLElement *userIdElement = [TBXML childElementNamed:KEY_USERID parentElement:tbXml.rootXMLElement];
        TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:KEY_HUBCITIID parentElement:tbXml.rootXMLElement];
        TBXMLElement *hubCitiNameElement = [TBXML childElementNamed:KEY_HUBCITINAME parentElement:tbXml.rootXMLElement];
        TBXMLElement * isTempUserElement = [TBXML childElementNamed:@"isTempUser" parentElement:tbXml.rootXMLElement];
        TBXMLElement * noEmailCountElement = [TBXML childElementNamed:@"noEmailCount" parentElement:tbXml.rootXMLElement];
//        if([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
//           {
//               [SharedManager setGps_allow_flag:YES];
//               [defaults setValue:@"YES" forKey:@"allowingGPS"];
//           }
//        else
//        {
//            [SharedManager setGps_allow_flag:NO];
//            [defaults setValue:@"NO" forKey:@"allowingGPS"];
//        }
        if ([RegionApp isEqualToString:@"1"] && ![defaults valueForKey:@"sortedCitiIDs"]) {
            [self sortCitiId : tbXml];
        }
        
        
        
        [defaults  setObject:[TBXML textForElement:userIdElement] forKey:KEY_USERID];
        [defaults  setObject:[TBXML textForElement:hubCitiIdElement] forKey:KEY_HUBCITIID];
        [defaults  setObject:[TBXML textForElement:hubCitiNameElement] forKey:KEY_HUBCITINAME];
        //
        //        if ([[TBXML textForElement:noEmailCountElement] isEqualToString:@"1"] )
        //        {
        //            NSNotification *notification;
        //            [NewsUtility emailCountDisplay:notification onViewController:self.window.rootViewController];
        //
        //        }
        [NewsUtility setUserSettings];
        emailCountString = [TBXML textForElement:noEmailCountElement];
         tempUserString =[TBXML textForElement:isTempUserElement];
        
        return YES;
    }
    
    else
    {
        return NO;
        
    }
}

-(void) popUPForEmailCount: (NSString*) tempUserFlag  :(NSString*) emailCount
{
    if ([tempUserFlag isEqualToString:@"1"] )
    {
        //            [[NSNotificationCenter defaultCenter]
        //             postNotificationName:@"tempPassword"
        //             object:self];
        
        NSNotification *notification;
        [NewsUtility tempPasswordDisplay: notification onViewController:self.window.rootViewController];
    }
    
    if ([emailCount isEqualToString:@"1"] )
    {
        NSNotification *notification;
        [NewsUtility emailCountDisplay:notification onViewController:self.window.rootViewController];
        
    }
}


-(void) sortCitiId : (TBXML*) tbXml
{
    NSMutableArray*sortedCitiIDs = [[NSMutableArray alloc] init];
    TBXMLElement *cityListElement = [TBXML childElementNamed:KEY_CITILIST parentElement:tbXml.rootXMLElement];
    NSLog(@"%@",[TBXML textForElement:tbXml.rootXMLElement]);
    
    NSLog(@"%@",[TBXML textForElement:cityListElement]);
    
    TBXMLElement *cityDetailsElement = [TBXML childElementNamed:@"City" parentElement:cityListElement];
    NSMutableArray* cityIDS = [[NSMutableArray alloc] init];
    
    while (cityDetailsElement!=nil) {
        
        TBXMLElement *cityIdElement = [TBXML childElementNamed:@"cityId" parentElement:cityDetailsElement];
        
        
        if (cityIdElement!=nil) {
            [cityIDS addObject: [TBXML textForElement:cityIdElement]];
            
        }
        
        
        cityDetailsElement = [TBXML nextSiblingNamed:@"City" searchFromElement: cityDetailsElement];
    }
    
    NSMutableString *cities=[[NSMutableString alloc]init];
    NSMutableString *sortCities=[[NSMutableString alloc]init];
    
    for (int i=0; i< [cityIDS count]; i++) {
        
        [cities appendFormat:@"%@,",[cityIDS objectAtIndex:i]];
        [sortedCitiIDs addObject:[cityIDS objectAtIndex:i]];
        
    }
    
    if ([cities hasSuffix:@","]) {
        [cities  setString:[cities substringToIndex:[cities length]-1]];
    }
    
    //changed for server caching
    
    [sortedCitiIDs sortUsingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
        return [str1 compare:str2 options:(NSNumericSearch)];
    }];
    for (int i=0; i<[sortedCitiIDs count]; i++) {
        [sortCities appendFormat:@"%@,",[sortedCitiIDs objectAtIndex:i]];
        
    }
    
    if ([sortCities hasSuffix:@","]) {
        [sortCities  setString:[sortCities substringToIndex:[sortCities length]-1]];
    }
    
    [defaults setObject:sortCities forKey:@"sortedCitiIDs"];
    
    // call main menu
    [SharedManager setUserInfoFromSignup:NO];
    NSString* customerId = [defaults objectForKey:@"currentuserId"];
    if (customerId) {
        
        if (![customerId isEqualToString:[defaults valueForKey:KEY_USERID]]) {
            
            ReleaseAndNilify(cashedResponse);
            [defaults setValue: nil forKey: @"currentuserId"];
        }
        else {
            isSameUser = true;
        }
    }
    
    
}

/*
 *
 Method to send request to get login custom UI details.
 *
 */
-(void) navigationColor{
    
    //navigation bar ----
    
    NSLog(@"appdelegate color %@",[defaults valueForKey:@"titleBkGrdColor"]);
    [[UINavigationBar appearance]setBarTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]]];
    
    // [[UINavigationBar appearance]setBarTintColor:[UIColor blueColor]];
    
    //[[UINavigationBar appearance]setTranslucent:NO];
    
    if([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        
        [[UINavigationBar appearance] setTranslucent:NO];
    }
    else{
        
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"NavBar_Bg.png"] forBarMetrics:UIBarMetricsDefault];
        //  [[UINavigationBar appearance]setBackgroundColor:[UIColor blackColor]];
        
    }
    NSLog(@"Appdelegate:%@",[defaults valueForKey:@"titleTxtColor"]);
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]],
      NSForegroundColorAttributeName,[UIFont fontWithName:@"Helvetica-Bold" size:17.0],
      NSFontAttributeName,nil]];
    
    NSShadow *shadow = [[NSShadow alloc] init];
    [shadow setShadowColor:[UIColor darkGrayColor]];
    [shadow setShadowOffset:CGSizeMake(0.0, -1.0)];
    
    
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{
                                                        NSFontAttributeName : [UIFont boldSystemFontOfSize:16.0f],
                                                        NSForegroundColorAttributeName : [UIColor whiteColor],
                                                        NSShadowAttributeName : shadow } forState:UIControlStateNormal];
    
    
    
    //application toolbar appearance
    //    [[UIToolbar appearance] setBackgroundImage:[UIImage imageNamed:@"Transparent.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    [[UIToolbar appearance]setBackgroundColor:[UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]]];
    // [[UIToolbar appearance]setTranslucent:NO];
    
    if([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        
        [[UIToolbar appearance]setTranslucent:NO];
    }
    else{
        
        [[UIToolbar appearance] setBackgroundImage:[UIImage imageNamed:@"NavBar_Bg.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
        
    }
    
    
}
-(void)requestForCustomLoginUI
{
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<LoginFlowDetails><pageType>Login Page</pageType>"];
    [xmlStr appendFormat:@"<hubCitiKey>%@</hubCitiKey></LoginFlowDetails>",HUBCITIKEY];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/loginflow",BASE_URL];
   // NSString *urlString = [NSString stringWithFormat:@"http://10.10.220.58:9990/HubCiti2.8.2/firstuse/loginflow"];
    
    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    [self parseDataCustomLoginUI:responseXML];
    
    [self navigationColor];
    
    //    ReleaseAndNilify(xmlStr);
    
    //        NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //        [xmlStr appendFormat:@"<LoginFlowDetails><pageType>Login Page</pageType>"];
    //        [xmlStr appendFormat:@"<hubCitiKey>Hawaii10287</hubCitiKey></LoginFlowDetails>"];
    //
    //
    //        NSString *urlString = [NSString stringWithFormat:@"http://SDW2730:8080/HubCiti2.5/firstuse/loginflow"];
    //
    //        NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    //        [self parseDataCustomLoginUI:responseXML];
    //
    //        [self navigationColor];
    //
    //        ReleaseAndNilify(xmlStr);
    
    //        NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //        [xmlStr appendFormat:@"<LoginFlowDetails><pageType>Login Page</pageType>"];
    //        [xmlStr appendFormat:@"<hubCitiKey>Hawaii10287</hubCitiKey></LoginFlowDetails>"];
    //
    //
    //        NSString *urlString = [NSString stringWithFormat:@"http://SDW2730:8080/HubCiti2.5/firstuse/loginflow"];
    //
    //        NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    //        [self parseDataCustomLoginUI:responseXML];
    //
    //        [self navigationColor];
    //
    //        ReleaseAndNilify(xmlStr);
    
    
}


/*
 *
 Add Parsing logic for view design
 parse login screen custom UI data and strore details on Login Data object
 pass the login data object to login screen and release
 *
 */

-(void) parseDataCustomLoginUI : (NSString *) respo {
    
    loginDO = [[LoginDO alloc]init];
    
    
    
    
    
    if ([UtilityManager isResponseXMLNullOrEmpty:respo])
    {
        loginDO.fontColor = @"#ffffff";
        loginDO.backgroundColor  = @"#C0C0C0";
        loginDO.buttonFontColor = @"#ffffff";
        loginDO.buttonColor = @"#000000";
    }
    else
    {
        TBXML *tbXml = [TBXML tbxmlWithXMLString:respo];
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        int responseCode = [[TBXML textForElement:saveResponseCode] intValue];
        if(responseCode == 10000)
        {
            TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:@"hubCitiId" parentElement:tbXml.rootXMLElement];
            TBXMLElement *fontColorElement = [TBXML childElementNamed:@"fontColor" parentElement:tbXml.rootXMLElement];
            TBXMLElement *bkgndColorElement = [TBXML childElementNamed:@"bkgndColor" parentElement:tbXml.rootXMLElement];
            TBXMLElement *buttonFontColorElement = [TBXML childElementNamed:@"btnFontColor" parentElement:tbXml.rootXMLElement];
            TBXMLElement *buttonColorElement = [TBXML childElementNamed:@"btnColor" parentElement:tbXml.rootXMLElement];
            TBXMLElement *logoImgElement = [TBXML childElementNamed:@"logoImg" parentElement:tbXml.rootXMLElement];
            TBXMLElement *smallLogoElement = [TBXML childElementNamed:@"smallLogo" parentElement:tbXml.rootXMLElement];
            TBXMLElement *poweredByElement = [TBXML childElementNamed:@"poweredBy" parentElement:tbXml.rootXMLElement];
            
            
            TBXMLElement *bkImgPathElement = [TBXML childElementNamed:@"bkImgPath" parentElement:tbXml.rootXMLElement];
            TBXMLElement *homeImgPathElement = [TBXML childElementNamed:@"homeImgPath" parentElement:tbXml.rootXMLElement];
            TBXMLElement *titleTxtColorElement = [TBXML childElementNamed:@"titleTxtColor" parentElement:tbXml.rootXMLElement];
            TBXMLElement *titleBkGrdColorElement = [TBXML childElementNamed:@"titleBkGrdColor" parentElement:tbXml.rootXMLElement];
            
            TBXMLElement *templateTypeElement = [TBXML childElementNamed:@"templateName" parentElement:tbXml.rootXMLElement];
            TBXMLElement *hamburgerTypeElement = [TBXML childElementNamed:@"hamburgerImg" parentElement:tbXml.rootXMLElement];
            TBXMLElement *domainNameElement = [TBXML childElementNamed:@"domainName" parentElement:tbXml.rootXMLElement];
            if (domainNameElement != nil) {
                [defaults setValue:[TBXML textForElement:domainNameElement] forKey:@"DomainName"];
            }
            //hamburgerImg
            
            //[xmlStr appendFormat:@"<level>%@</level>",[defaults valueForKey:KEY_MENULEVEL]];
            
            if(hubCitiIdElement)
                [defaults setValue:[TBXML textForElement:hubCitiIdElement] forKey:KEY_HUBCITIID];
            
            if (fontColorElement != NULL) {
                loginDO.fontColor = [TBXML textForElement:fontColorElement];
            }
            if (bkgndColorElement != NULL) {
                loginDO.backgroundColor = [TBXML textForElement:bkgndColorElement];
            }
            if (buttonFontColorElement != NULL) {
                loginDO.buttonFontColor = [TBXML textForElement:buttonFontColorElement];
            }
            if (buttonColorElement != NULL) {
                loginDO.buttonColor = [TBXML textForElement:buttonColorElement];
            }
            if (logoImgElement != NULL) {
                loginDO.logoPath = [TBXML textForElement:logoImgElement];
            }
            if (smallLogoElement != NULL) {
                loginDO.smallLogoPath = [TBXML textForElement:smallLogoElement];
                [defaults setValue:[TBXML textForElement:smallLogoElement] forKey:HC_LOGOIMGPATH];
                
            }
            if (poweredByElement != NULL) {
                loginDO.poweredByPath = [TBXML textForElement:poweredByElement];
            }
            
            
            if ( bkImgPathElement!= NULL) {
                loginDO.bkImgPath = [TBXML textForElement:bkImgPathElement];
                [defaults setValue:loginDO.bkImgPath forKey:@"bkImgPath"];
            }
            if (homeImgPathElement != NULL) {
                loginDO.homeImgPath = [TBXML textForElement:homeImgPathElement];
                [defaults setValue:loginDO.homeImgPath forKey:@"homeImgPath"];
            }
            if (titleTxtColorElement != NULL) {
                loginDO.titleTxtColor = [TBXML textForElement:titleTxtColorElement];
                [defaults setValue:loginDO.titleTxtColor forKey:@"titleTxtColor"];
            }
            if (titleBkGrdColorElement != NULL) {
                loginDO.titleBkGrdColor = [TBXML textForElement:titleBkGrdColorElement];
                [defaults setValue:loginDO.titleBkGrdColor forKey:@"titleBkGrdColor"];
            }
            if (hamburgerTypeElement != NULL) {
                //loginDO.titleBkGrdColor = [TBXML textForElement:titleBkGrdColorElement];
                [defaults setValue:[TBXML textForElement:hamburgerTypeElement] forKey:@"hamburgerImg"];
            }
            
            
            if(templateTypeElement != NULL)
            {
                [defaults setValue:[TBXML textForElement:templateTypeElement] forKey:@"selectedTemplateName"];
                [defaults setBool:YES forKey:@"newsTemplateExist"];
                // ask whether it is needed
            }
            
            //            if(templateTypeElement == NULL)
            //            {
            //                [defaults setValue:@"Scrolling News Template" forKey:@"selectedTemplateName"];
            //                [defaults setBool:YES forKey:@"newsTemplateExist"];
            //                userIdentifier = TRUE;
            //            }
            else
            {
                [defaults setValue:@"HubCiti" forKey:@"selectedTemplateName"];
                [defaults setBool:NO forKey:@"newsTemplateExist"];
            }
            
        }
        else
        {
            loginDO.fontColor = @"#ffffff";
            loginDO.backgroundColor  = @"#C0C0C0";
            loginDO.buttonFontColor = @"#ffffff";
            loginDO.buttonColor = @"#000000";
        }
        
    }
    
}


/*
 *
 asynchronous req/resp delegate method
 *
 */
-(void)responseData:(NSString *) response
{
    
    //[self parseDataCustomLoginUI:response];
}
-(void)parse_FetchCityPreferences : (NSString *)response
{
    NSMutableArray*cityIDS = [[NSMutableArray alloc] init];
    NSMutableArray*sortedCitiIDs = [[NSMutableArray alloc] init];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]){
        
        TBXMLElement *cityListElement = [TBXML childElementNamed:@"cityList" parentElement:tbxml.rootXMLElement];
        
        NSLog(@"%@",[TBXML textForElement:tbxml.rootXMLElement]);
        
        NSLog(@"%@",[TBXML textForElement:cityListElement]);
        
        TBXMLElement *cityDetailsElement = [TBXML childElementNamed:@"City" parentElement:cityListElement];
        
        while (cityDetailsElement!=nil) {
            
            TBXMLElement *cityIdElement = [TBXML childElementNamed:@"cityId" parentElement:cityDetailsElement];
            
            if (cityIdElement!=nil) {
                [cityIDS addObject: [TBXML textForElement:cityIdElement]];
                
            }
            
            
            cityDetailsElement = [TBXML nextSiblingNamed:@"City" searchFromElement: cityDetailsElement];
        }
        
        NSMutableString *cities=[[NSMutableString alloc]init];
        NSMutableString *sortCities=[[NSMutableString alloc]init];
        
        for (int i=0; i< [cityIDS count]; i++) {
            
            [cities appendFormat:@"%@,",[cityIDS objectAtIndex:i]];
            [sortedCitiIDs addObject:[cityIDS objectAtIndex:i]];
            
        }
        
        if ([cities hasSuffix:@","]) {
            [cities  setString:[cities substringToIndex:[cities length]-1]];
        }
        
        //changed for server caching
        
        [sortedCitiIDs sortUsingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
            return [str1 compare:str2 options:(NSNumericSearch)];
        }];
        for (int i=0; i<[sortedCitiIDs count]; i++) {
            [sortCities appendFormat:@"%@,",[sortedCitiIDs objectAtIndex:i]];
            
        }
        
        if ([sortCities hasSuffix:@","]) {
            [sortCities  setString:[sortCities substringToIndex:[sortCities length]-1]];
        }
        
        [defaults setObject:sortCities forKey:@"sortedCitiIDs"];
        
        
        
        // call main menu
        [SharedManager setUserInfoFromSignup:NO];
        NSString* customerId = [defaults objectForKey:@"currentuserId"];
        if (customerId) {
            
            if (![customerId isEqualToString:[defaults valueForKey:KEY_USERID]]) {
                
                ReleaseAndNilify(cashedResponse);
                [defaults setValue: nil forKey: @"currentuserId"];
            }
            else {
                isSameUser = true;
            }
        }
        
    }
    else
        
    {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        return;
    }
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    if(pushBackground) //if coming from clicking notification from lock screen start activity indicator
    {
        pushBackground = false;
        
        if ([defaults boolForKey:@"LoginSuccess"]==YES) {
            if (userIdentifier){
            }
            else if ([defaults boolForKey:@"canDisplayAPNSListScreen"]==NO) {
                [self showActivityIndicator];
            }
        }
        
    }

    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HIDE" object:self];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    BOOL pushNotificationEnabled = [self pushNotificationsEnabled];
    if (userIdentifier) {
        [self removeActivityIndicator];
        if ([[defaults valueForKey:@"fromDidFinishLaunchingWithNotification"] isEqualToString:@"1"]) {
            [defaults setValue:@"0" forKey:@"fromDidFinishLaunchingWithNotification"];
            
           
                [defaults setBool:YES forKey:@"canDisplayAPNSListScreen"];
                [UtilityManager showAlert:nil msg:@"Please Sign up to view the Notification"];
            
        }
        if ([[UIApplication sharedApplication]applicationIconBadgeNumber])
            [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
        return;
    }
    if(pushNotificationEnabled){
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        //
        
        //     NSString *str = [defaults objectForKey:@"APNS"];
        if ([[defaults valueForKey:@"fromDidFinishLaunchingWithNotification"] isEqualToString:@"1"]) {
            [defaults setValue:@"0" forKey:@"fromDidFinishLaunchingWithNotification"];
            
            if(userIdentifier){
                [self removeActivityIndicator];
                [defaults setBool:YES forKey:@"canDisplayAPNSListScreen"];
                [UtilityManager showAlert:nil msg:@"Please Sign up to view the Notification"];
            }
            else
            {
            [self removeActivityIndicator];
            [self pushalertViewClicked];
            }
            // //[alert release];
        }
        
        else if ([[UIApplication sharedApplication]applicationIconBadgeNumber]) {
            
            //NSString *str = [defaults valueForKey:@"PushNotifyResponse"];
            
            //NSString *msg = [defaults valueForKey:@"message"];
            //
            //DLog(@"str ,  %@  msg %@", str, msg);
            
            //DLog(@"%@",[defaults objectForKey:KEY_HUBCITIID]);
             if(pushBack){
                [defaults setBool:NO forKey:@"pushInactive"];
                 
//                 [UtilityManager showAlert:@"finishlaunch1" msg:nil];
                 

                 pushBack = false;
                 
                 [self removeActivityIndicator];
                 [self pushalertViewClicked];
             }
             else{
//                 [UtilityManager showAlert:@"finishlaunch2" msg:nil];
                 
            [self removeActivityIndicator];
            [defaults setObject:@"0" forKey:@"APNS"];
            
            __block UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
            alertWindow.rootViewController =  [[UIViewController alloc] init];
            
            UIWindow *topWindow = [UIApplication sharedApplication].windows.lastObject;
            alertWindow.windowLevel = topWindow.windowLevel + 1;
            
            [alertWindow makeKeyAndVisible];
            
            UIAlertController  *alert1=[UIAlertController alertControllerWithTitle:@"Notification" message:[defaults valueForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* view = [UIAlertAction
                                   actionWithTitle:@"View"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action){
                                       [self pushalertViewClicked];
                                       alertWindow.hidden = YES;
                                       alertWindow = nil;
                                   }];
            [alert1 addAction:view];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action){
                                         [defaults setObject:@"0" forKey:@"APNS"];
                                         [alert1 dismissViewControllerAnimated:YES completion:nil];
                                         alertWindow.hidden = YES;
                                         alertWindow = nil;
                                     }];
            [alert1 addAction:cancel];
            [alertWindow.rootViewController presentViewController:alert1 animated:YES completion:nil];            // //[alert release];
             }
            
        }
        
    }
    else{
        [self sendResetBadgeRequest];
        
    }
}

-(void)parseNotification:(id)responseObj{
    
    //BOOL pushNotificationEnabled = [self pushNotificationsEnabled];
    if (userIdentifier) {
        return;
    }
    //if(pushNotificationEnabled){
    
    
    DLog(@"responseObj %@", responseObj);
    NSDictionary *responseData=responseObj;
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    DLog(@"responseCode %ld", (long)responseCode);
    DLog(@"responsestr %@", responseText);
    if (responseCode == 10000) {
        
        [defaults setValue:responseData forKey:@"PushNotifyResponse"];
        [defaults setValue:[responseData objectForKey:@"message"] forKey:@"message"];
        
        [defaults setObject:@"0" forKey:@"APNS"];
        
        
        __block UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        alertWindow.rootViewController =  [[UIViewController alloc] init];
        
        UIWindow *topWindow = [UIApplication sharedApplication].windows.lastObject;
        alertWindow.windowLevel = topWindow.windowLevel + 1;
        
        [alertWindow makeKeyAndVisible];
        
        UIAlertController  *alert1=[UIAlertController alertControllerWithTitle:@"Notification" message:[defaults valueForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* view = [UIAlertAction
                               actionWithTitle:@"View"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action){
                                   [self pushalertViewClicked];
                                   alertWindow.hidden = YES;
                                   alertWindow = nil;
                               }];
        [alert1 addAction:view];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     [defaults setObject:@"0" forKey:@"APNS"];
                                     [alert1 dismissViewControllerAnimated:YES completion:nil];
                                     alertWindow.hidden = YES;
                                     alertWindow = nil;
                                 }];
        [alert1 addAction:cancel];
        [alertWindow.rootViewController presentViewController:alert1 animated:YES completion:nil];
    }
    else{
      
    }
    //}
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(BOOL)getscanHistoryFlag{
    return scanHistoryFlag;
}
-(void)setscanHistoryFlag:(BOOL)flag{
    scanHistoryFlag = flag;
}

- (NSMutableArray*)getLinkIdArray
{
    return linkIdArray;
}
- (void)refreshLinkIdArray
{
    [linkIdArray removeAllObjects];
}

- (NSMutableArray*)getDeptIdArray
{
    return deptIdArray;
}
- (void)refreshDeptIdArray
{
    [deptIdArray removeAllObjects];
}


- (NSMutableArray*)getTypeIdArray
{
    return typeIdArray;
}
- (void)refreshTypeIdArray
{
    [typeIdArray removeAllObjects];
}

-(void) showActivityIndicator
{
    
    img = [[SdImageView alloc]initWithImage:[UIImage imageNamed:@"transparentBlack.png"]];
    img.frame = CGRectMake(85, 190, 150, 70);
    img.center = window.center;
    
    label = [[UILabel alloc]initWithFrame:CGRectMake(18, 7, 120, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.text = @"Refreshing Data";
    label.numberOfLines=0;
    label.lineBreakMode=NSLineBreakByWordWrapping;
    label.font = [UIFont boldSystemFontOfSize:13.0];
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.color = [UIColor colorWithRed:0 green:211 blue:255 alpha:1.0f];
    activityIndicator.center = img.center; //CGPointMake(160, 240);
    activityIndicator.frame = CGRectMake(65, 36, 20, 20);
    activityIndicator.hidesWhenStopped = YES;
    
    [img addSubview:label];
    [img addSubview:activityIndicator];
    [window addSubview:img];
    
    [window bringSubviewToFront:activityIndicator];
    [activityIndicator startAnimating];
    [window setUserInteractionEnabled:NO];
}

-(void) removeActivityIndicator{
    
    if (activityIndicator){
        activityIndicator.hidden = YES;
        [activityIndicator stopAnimating];
        [img removeFromSuperview];
        [window setUserInteractionEnabled:YES];
    }
}

-(NSString*)getBarCode{
    return barCode;
}
-(void)setBarCode:(NSString*)str{
    barCode = @"";
    barCode = str;
}

- (NSMutableArray*)selIndexArrayFind{
    return selIndexArrayFind;
    
}
- (void)RefreshIndexFindArray{
    [selIndexArrayFind removeAllObjects];
    
}
//Checks For What The User Has Decided.
//
// If Location Services Allowed             Return 0
// If No LocationServices But Zip Exists    Return 1
// If No Location and No Zip                Return 2
//
-(int)checkZipAndLocation
{
    if ([[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"NO"]) // GPS not Allowed
    {
        if ([defaults  valueForKey:KEYZIPCODE])
        {
            return 1; // Zip Code is Available
        }
        else
            return 2; // Zip Code nOt Available
    }
    return 0; // GPS Allowed
}

-(BOOL)locationServicesOn
{
    if (([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) || ([[HubCitiManager sharedManager]gps_allow_flag] == NO))
    {
        return NO;
    }
    else
    {
        return YES;
    }
    
}


-(NSArray*)SelIndexArray
{
    return selIndexArray;
}

-(void)RefreshIndexArray
{
    [selIndexArray removeAllObjects];
}

-(NSArray*)SelIndexArrayCart
{
    return selIndexArrayCart;
}

-(void)RefreshIndexArrayCart
{
    [selIndexArrayCart removeAllObjects];
}


-(NSArray*)selIndexArrayBasket
{
    return selIndexArrayBasket;
}

-(void)RefreshIndexArrayBasket
{
    //checkedBasket = NO;
    [selIndexArrayBasket removeAllObjects];
}
- (NSMutableArray*)SelIndexUserPreferenceArray{
    
    return selIndexUserPreferenceArray;
}
- (void)RefreshIndexUserPreferenceArray{
    [selIndexUserPreferenceArray removeAllObjects];
    
}

- (NSArray*)SelIndexWishListArray{
    return selIndexWishListArray;
    
}

- (void)RefreshIndexWishListArray{
    [selIndexWishListArray removeAllObjects];
    
}


- (NSArray*)SelIndexUserCategoryArray{
    return selIndexUserCategoryArray;
}

- (void)RefreshIndexUserCategoryArray{
    [selIndexUserCategoryArray removeAllObjects];
}

- (NSMutableArray*)getRetIdArray{
    return retailerIdArray;
}
- (void)RefreshRetIdArray{
    [retailerIdArray removeAllObjects];
}


- (NSMutableArray*)getRetLocIdArray{
    return retailerLocIdArray;
}
- (void)RefreshRetLocIdArray{
    [retailerLocIdArray removeAllObjects];
}

//used to shorten url for twitter share
-(NSString*)shortenURL:(NSString*)longString{
    
    NSString *urlstr =[longString stringByReplacingOccurrencesOfString:@" " withString:@""];///your url string
    //[urlstr ;
    NSString *apiEndpoint = [NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@",urlstr];
    // [apiEndpoint ;
    
    NSString *shortURL = [NSString stringWithContentsOfURL:[NSURL URLWithString:apiEndpoint]
                                                  encoding:NSASCIIStringEncoding
                                                     error:nil];
    DLog(@"Long: %@ - Short: %@",urlstr,shortURL);
    return shortURL;
}


#pragma mark Location Manager Interactions

/*
 * We want to get and store a location measurement that meets the desired accuracy. For this example, we are
 *      going to use horizontal accuracy as the deciding factor. In other cases, you may wish to use vertical
 *      accuracy, or both together.
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    // store all of the measurements, just so we can see what kind of data we might receive
    [locationMeasurements addObject:newLocation];
    // test the age of the location measurement to determine if the measurement is cached
    // in most cases you will not want to rely on cached measurements
    NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
    if (locationAge > 100.0) return;
    // test that the horizontal accuracy does not indicate an invalid measurement
    if (newLocation.horizontalAccuracy < 0) return;
    // test the measurement to see if it is more accurate than the previous measurement
    //if (bestEffortAtLocation == nil || bestEffortAtLocation.horizontalAccuracy > newLocation.horizontalAccuracy) {
    if (bestEffortAtLocation == nil){
        // store the location as the "best effort"
        self.bestEffortAtLocation = newLocation;
        // test the measurement to see if it meets the desired accuracy
        //
        // IMPORTANT!!! kCLLocationAccuracyBest should not be used for comparison with location coordinate or altitidue
        // accuracy because it is a negative value. Instead, compare against some predetermined "real" measure of
        // acceptable accuracy, or depend on the timeout to stop updating. This sample depends on the timeout.
        //
        if (newLocation.horizontalAccuracy <= locationManager.desiredAccuracy) {
            // we have a measurement that meets our requirements, so we can stop updating the location
            //
            // IMPORTANT!!! Minimize power usage by stopping the location manager as soon as possible.
            //
            [self stopUpdatingLocation:NSLocalizedString(@"Acquired Location", @"Acquired Location")];
            // we can also cancel our previous performSelector:withObject:afterDelay: - it's no longer necessary
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stopUpdatingLocation:) object:nil];
        }
    }
    // update the display with the new location data
    NSString *gpsValu=@"NA";
    //DLog(@"gpsValu1:%@",gpsValu);
    gpsValu = bestEffortAtLocation.localizedCoordinateString;
    
    //NSMutableArray *items =[[NSMutableArray alloc] init];
    NSArray *items = [gpsValu componentsSeparatedByString:@","];
    if([items count]>=2)
    {
      //  gpsValu = [NSString stringWithFormat:@"lat:%f - long:%f",[[items objectAtIndex:0] doubleValue],[[items objectAtIndex:1] doubleValue]];
        latValue = [[NSString stringWithFormat:@"%f",[[items objectAtIndex:0] doubleValue]] copy];
        longValue = [[NSString stringWithFormat:@"%f",[[items objectAtIndex:1] doubleValue]] copy];
        
    }
    if(latValue && [latValue length]>0 && longValue && [longValue length]>0)
    {
        [self stopUpdatingLocation:NSLocalizedString(@"Acquired Location", @"Acquired Location")];
        // we can also cancel our previous performSelector:withObject:afterDelay: - it's no longer necessary
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stopUpdatingLocation:) object:nil];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    // The location "unknown" error simply means the manager is currently unable to get the location.
    // We can ignore this error for the scenario of getting a single location fix, because we already have a
    // timeout that will stop the location manager to save power.
    
    if([defaults integerForKey:@"AppCounter"] != 1){
        [SharedManager setGps_allow_flag:NO];
        [defaults setValue:@"NO" forKey:@"allowingGPS"];
    }
    
    if ([error code] != kCLErrorLocationUnknown) {
        [self stopUpdatingLocation:NSLocalizedString(@"Error", @"Error")];
    }
}

- (void)stopUpdatingLocation:(NSString *)state {
    
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
    
    [defaults  setValue:latValue forKey:@"LatVal"];
    [defaults  setValue:longValue forKey:@"LongVal"];
    [defaults  setValue:latValue forKey:KEY_LATITUDE];
    [defaults  setValue:longValue forKey:KEY_LONGITUDE];
}



- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    // We only need to start updating location for iOS 8 -- iOS 7 users should have already
    // started getting location updates
    if (status == kCLAuthorizationStatusAuthorizedAlways ||
        status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [manager startUpdatingLocation];
    }
}
- (BOOL)pushNotificationsEnabled {
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(currentUserNotificationSettings)]) {
        UIUserNotificationType types = [[[UIApplication sharedApplication] currentUserNotificationSettings] types];
        
        return (types & UIUserNotificationTypeAlert);
    }
    else {
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        return (types & UIRemoteNotificationTypeAlert);
    }
    
    
}


#pragma mark APNS

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    //    BOOL pushNotificationEnabled = [self pushNotificationsEnabled];
    //    if(pushNotificationEnabled){
    NSString *tokenId = [[[[deviceToken description]stringByReplacingOccurrencesOfString:@"<" withString:@""]stringByReplacingOccurrencesOfString:@">" withString:@""]stringByReplacingOccurrencesOfString:@" " withString:@""];
   
   // [UtilityManager showAlert:tokenId msg:nil];
    NSLog(@"Formatted token id:%@", tokenId);
    [defaults setValue:tokenId forKey:@"TOKEN_ID"];
    if(![defaults boolForKey:@"pushEntered"] && [self pushNotificationsEnabled])
        [self nextRequestparse_update];
    [NSThread detachNewThreadSelector:@selector(performAction) toTarget:self withObject:nil];
    //  }
    
    /*
     * Some example cases where user notifcation may be warranted
     *
     * This code will alert users who try to enable notifications
     * from the settings screen, but cannot do so because
     * notications are disabled in some capacity through the settings
     * app.
     *
     */
}

-(void) performAction {
    
    if([Network currentReachabilityStatus]==0){
        
        [UtilityManager showAlert];
    }
    else {
        NSMutableString *xmlStr = [[NSMutableString alloc] init];
        [xmlStr appendFormat:@"<UserRegistrationInfo><deviceID>%@</deviceID>", [defaults valueForKey:KEY_DEVICEID]];
        [xmlStr appendFormat:@"<userTokenID><![CDATA[%@]]></userTokenID><platform>IOS</platform><hcKey>%@</hcKey></UserRegistrationInfo>",[defaults  valueForKey:@"TOKEN_ID"],HUBCITIKEY];
        
        NSMutableString *urlString = [BASE_URL mutableCopy] ;
        [urlString appendString:@"firstuse/registerpushnotify"];
        
        //
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:xmlStr]];
        DLog(@"%@",responseXml);
        // [xmlStr release];
              DLog(@"Token ID:%@",[defaults  valueForKey:@"TOKEN_ID"]);
      
        
        if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]) {
            ReleaseAndNilify(responseXml);
            return;
        }
        ReleaseAndNilify(responseXml);
    }
}
-(void)nextRequestparse_update{
    [defaults setBool:YES forKey:@"pushEntered"];
    BOOL value =[self pushNotificationsEnabled];
    NSLog(@"enabledpush:%d",value);
    NSString *pushNotifyString ;
    if(value)
        pushNotifyString = @"true";
    else
        pushNotifyString = @"false";
    
    
    BOOL enabled  = false;
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        enabled = [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
    }
    NSLog(@"enabled:%d",enabled);
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<UserSettings><userId>%@</userId><deviceId>%@</deviceId>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_DEVICEID]];
    [requestStr appendFormat:@"<pushNotify>%@</pushNotify><hubCitiId>%@</hubCitiId>",pushNotifyString,[defaults valueForKey:KEY_HUBCITIID]];
    if ([[defaults valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [requestStr appendFormat:@"<bLocService>true</bLocService>"];
    }
    else
    {
        [requestStr appendFormat:@"<bLocService>false</bLocService>"];
    }
    
    NSString * radius = [self checkForRadius];
    if(radius.length>0)
        
        [requestStr appendFormat:@"<localeRadius>%@</localeRadius></UserSettings>",radius];
    
    else
        [requestStr appendFormat:@"<localeRadius>50</localeRadius></UserSettings>"];
    
    NSString *urlReqString = [NSString stringWithFormat:@"%@firstuse/setusersettings",BASE_URL];
    NSString *responseXmlData = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlReqString withParam:requestStr]];
    
    NSLog(@"%@",responseXmlData);
}

//-(void) nextRequest{
//    [self performSelector:@selector(nextRequestparse_update) withObject:nil afterDelay:1.0];
//
//
//
//
//}
-(NSString *)checkForRadius
{
    NSString *reqStr = [NSString stringWithFormat:@"<AuthenticateUser><deviceID>%@</deviceID><userId>%@</userId><hubCitiId>%@</hubCitiId></AuthenticateUser>",[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusersettings",BASE_URL];
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:reqStr];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
    if (responseCodeElement != nil && ![[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
            return nil;
    }
    
    TBXMLElement *localeRadiusElement = [TBXML childElementNamed:@"localeRadius" parentElement:tbxml.rootXMLElement];
    
    NSString *userRadius = [TBXML textForElement:localeRadiusElement];
    return userRadius;
    
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *) error {
    DLog(@"Failed To Register For Remote Notifications With Error: %@", error);
}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    
//    [defaults setBool:YES forKey:@"push"];
//    BOOL pushNotificationEnabled = [self pushNotificationsEnabled];
//    if (userIdentifier) {
//        if(application.applicationState == UIApplicationStateInactive ||application.applicationState == UIApplicationStateBackground)
//            pushGuest = TRUE;
//        if ([[UIApplication sharedApplication]applicationIconBadgeNumber])
//            [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
//        return;
//    }
//    if(pushNotificationEnabled){
//        if(application.applicationState == UIApplicationStateInactive) {
//            pushBack = true;
//            [defaults setBool:YES forKey:@"pushInactive"];
//            pushBackground = true;
//            NSLog(@"Inactive");
//            
//            //Show the view with the content of the push
//            //completionHandler(UIBackgroundFetchResultNewData);
//            
//        } else if (application.applicationState == UIApplicationStateBackground) {
//            pushBack = true;
//            pushBackground = true;
//            [defaults setBool:YES forKey:@"pushInactive"];
//            NSLog(@"Background");
//            
//            //Refresh the local model
//            
//            // completionHandler(UIBackgroundFetchResultNewData);
//            
//        } else {
//            
//            NSLog(@"Active");
//            
//            //Show an in-app banner
//            
//            // completionHandler(UIBackgroundFetchResultNewData);
//            
//        }
//        
//        BOOL bodyPartIsNull = FALSE;
//        DLog(@"Received remote notification: %@", userInfo);
//        DLog(@"alert :%@ ",[[userInfo objectForKey:@"aps"] objectForKey:NSLocalizedString(@"alert",@"alert")]);
//        DLog(@"badge :%@ ",[[userInfo objectForKey:@"aps"] objectForKey:@"badge"]);
//        DLog(@"body :%@ ",[[userInfo objectForKey:@"aps"] objectForKey:@"body"]);
//        DLog(@"loc-key:%@ ",[[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"loc-key"]);
//        
//        
//        
//        //get the notification message to be displayed
//        message = [[NSString alloc]initWithString:[[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"loc-key"]];
//        
//        if ([HUBCITIKEY isEqualToString:@"Tyler19"]) {
//            
//            for (int i=0; i<[notificationMsgList count]; i++) {
//                if([[notificationMsgList objectAtIndex:i]containsString:@"of the day"]|| [[notificationMsgList objectAtIndex:i]containsString:@"your daily deal."]){
//                    if ([message containsString:@"of the day"] || [message containsString:@"your daily deal."]) {
//                        [notificationMsgList removeAllObjects];
//                        [notificationListArr removeAllObjects];
//                        break;
//                    }
//                    
//                }
//            }
//            
//        }
//        
//        [notificationMsgList addObject:message];
//        
//        [defaults setValue:message forKey:@"message"];
//        //end
//        
//        //set the app icon badge number
//        badgeNumber = [[[userInfo objectForKey:@"aps"] objectForKey:@"badge"]intValue];
//        
//        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeNumber];
//        [defaults setInteger:badgeNumber forKey:@"BadgeNumber"];//used in wish list
//        //end
//        //NSError *error;
//        if ([[userInfo objectForKey:@"aps"] objectForKey:@"body"] != NULL){
//            
//            id json = [[[userInfo objectForKey:@"aps"] objectForKey:@"body"]copy];
//            
//            
//            //            NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
//            //
//            //            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
//            
//            [notificationListArr addObject:json];
//            NSDictionary* dict = [notificationListArr objectAtIndex:(notificationListArr.count - 1)];
//            
//            if (notificationListArr.count > 1) {
//                
//                [notificationListArr insertObject:dict atIndex:0];
//                [notificationListArr removeObjectAtIndex:(notificationListArr.count - 1)];
//            }
//            
//            NSArray* uniqueArray = [self removeDuplicate : notificationListArr];
//            notificationListArr = [uniqueArray mutableCopy];
//            
//            [defaults setValue:notificationListArr forKey:@"PushNotifyResponse"];
//            
//            
//            
//        }
//        
//        else{//in case if 'body' part is null, then also just display the alert message with only "OK" button
//            
//            
//            bodyPartIsNull = TRUE;
//        }
//        
//        
//        switch (application.applicationState) {
//                
//            case UIApplicationStateActive:{
//                
//                if(bodyPartIsNull){
//                    [UtilityManager showAlert:@"Notification" msg:[defaults valueForKey:@"message"]];
//                }
//                else{
//                    __block UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
//                    alertWindow.rootViewController =  [[UIViewController alloc] init];
//                    
//                    UIWindow *topWindow = [UIApplication sharedApplication].windows.lastObject;
//                    alertWindow.windowLevel = topWindow.windowLevel + 1;
//                    
//                    [alertWindow makeKeyAndVisible];
//                    
//                    
//                    
//                    UIAlertController  *alert1=[UIAlertController alertControllerWithTitle:@"Notification" message:[defaults valueForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
//                    
//                    UIAlertAction* view = [UIAlertAction
//                                           actionWithTitle:@"View"
//                                           style:UIAlertActionStyleDefault
//                                           handler:^(UIAlertAction * action){
//                                               [self pushalertViewClicked];
//                                               alertWindow.hidden = YES;
//                                               alertWindow = nil;
//                                           }];
//                    [alert1 addAction:view];
//                    UIAlertAction* cancel = [UIAlertAction
//                                             actionWithTitle:@"Cancel"
//                                             style:UIAlertActionStyleDefault
//                                             handler:^(UIAlertAction * action){
//                                                 [defaults setObject:@"0" forKey:@"APNS"];
//                                                 [alert1 dismissViewControllerAnimated:YES completion:nil];
//                                                 alertWindow.hidden = YES;
//                                                 alertWindow = nil;
//                                             }];
//                    [alert1 addAction:cancel];
//                    [alertWindow.rootViewController presentViewController:alert1 animated:YES completion:nil];
//                    //
//                }
//                ////[alert release];
//                [defaults setObject:@"0" forKey:@"APNS"];
//            }
//                
//                break;
//            case UIApplicationStateInactive:{
//                
//                [defaults setObject:@"1" forKey:@"APNS"];
//                [defaults synchronize];
//                break;
//            }
//                
//            case UIApplicationStateBackground:{
//                
//                [defaults setObject:@"1" forKey:@"APNS"];
//                break;
//            }
//            default:
//                break;
//        }
//        
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        
//        
//        //completionHandler(UIBackgroundFetchResultNoData);
//    }
//    else{
//        [self sendResetBadgeRequest];
//        
//    }
//    
//    
//    
//}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    [defaults setBool:YES forKey:@"push"];
    BOOL pushNotificationEnabled = [self pushNotificationsEnabled];
    if (userIdentifier) {
         if(application.applicationState == UIApplicationStateInactive ||application.applicationState == UIApplicationStateBackground)
        pushGuest = TRUE;
        if ([[UIApplication sharedApplication]applicationIconBadgeNumber])
            [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
        return;
    }
    if(pushNotificationEnabled){
        if(application.applicationState == UIApplicationStateInactive) {
            pushBack = true;
            [defaults setBool:YES forKey:@"pushInactive"];
            pushBackground = true;
            NSLog(@"Inactive");
            
            //Show the view with the content of the push
            //completionHandler(UIBackgroundFetchResultNewData);
            
        } else if (application.applicationState == UIApplicationStateBackground) {
            pushBack = true;
            pushBackground = true;
            [defaults setBool:YES forKey:@"pushInactive"];
            NSLog(@"Background");
            
            //Refresh the local model
            
            // completionHandler(UIBackgroundFetchResultNewData);
            
        } else {
            
            NSLog(@"Active");
            
            //Show an in-app banner
            
            // completionHandler(UIBackgroundFetchResultNewData);
            
        }

        BOOL bodyPartIsNull = FALSE;
        DLog(@"Received remote notification: %@", userInfo);
        DLog(@"alert :%@ ",[[userInfo objectForKey:@"aps"] objectForKey:NSLocalizedString(@"alert",@"alert")]);
        DLog(@"badge :%@ ",[[userInfo objectForKey:@"aps"] objectForKey:@"badge"]);
        DLog(@"body :%@ ",[[userInfo objectForKey:@"aps"] objectForKey:@"body"]);
        DLog(@"loc-key:%@ ",[[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"loc-key"]);
        
        
        
        //get the notification message to be displayed
        message = [[NSString alloc]initWithString:[[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"loc-key"]];
        
        if ([HUBCITIKEY isEqualToString:@"Tyler19"]) {
            
            for (int i=0; i<[notificationMsgList count]; i++) {
                if([[notificationMsgList objectAtIndex:i]containsString:@"of the day"]|| [[notificationMsgList objectAtIndex:i]containsString:@"your daily deal."]){
                    if ([message containsString:@"of the day"] || [message containsString:@"your daily deal."]) {
                        [notificationMsgList removeAllObjects];
                        [notificationListArr removeAllObjects];
                        break;
                    }
                    
                }
            }
            
        }
        
        [notificationMsgList addObject:message];
        
        [defaults setValue:message forKey:@"message"];
        //end
        
        //set the app icon badge number
        badgeNumber = [[[userInfo objectForKey:@"aps"] objectForKey:@"badge"]intValue];
        
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeNumber];
        [defaults setInteger:badgeNumber forKey:@"BadgeNumber"];//used in wish list
        //end
        //NSError *error;
        if ([[userInfo objectForKey:@"aps"] objectForKey:@"body"] != NULL){
            
            id json = [[[userInfo objectForKey:@"aps"] objectForKey:@"body"]copy];
            
            
            //            NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
            //
            //            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            
            [notificationListArr addObject:json];
            NSDictionary* dict = [notificationListArr objectAtIndex:(notificationListArr.count - 1)];
            
            if (notificationListArr.count > 1) {
                
                [notificationListArr insertObject:dict atIndex:0];
                [notificationListArr removeObjectAtIndex:(notificationListArr.count - 1)];
            }
            
            NSArray* uniqueArray = [self removeDuplicate : notificationListArr];
            notificationListArr = [uniqueArray mutableCopy];
            
            [defaults setValue:notificationListArr forKey:@"PushNotifyResponse"];
            
            
            
        }
        
        else{//in case if 'body' part is null, then also just display the alert message with only "OK" button
            
            
            bodyPartIsNull = TRUE;
        }
        
        
        switch (application.applicationState) {
                
            case UIApplicationStateActive:{
                
                if(bodyPartIsNull){
                    [UtilityManager showAlert:@"Notification" msg:[defaults valueForKey:@"message"]];
                }
                else{
                    __block UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                    alertWindow.rootViewController =  [[UIViewController alloc] init];
                    
                    UIWindow *topWindow = [UIApplication sharedApplication].windows.lastObject;
                    alertWindow.windowLevel = topWindow.windowLevel + 1;
                    
                    [alertWindow makeKeyAndVisible];
                    
                    
                    
                    UIAlertController  *alert1=[UIAlertController alertControllerWithTitle:@"Notification" message:[defaults valueForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* view = [UIAlertAction
                                           actionWithTitle:@"View"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action){
                                               [self pushalertViewClicked];
                                               alertWindow.hidden = YES;
                                               alertWindow = nil;
                                           }];
                    [alert1 addAction:view];
                    UIAlertAction* cancel = [UIAlertAction
                                             actionWithTitle:@"Cancel"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action){
                                                 [defaults setObject:@"0" forKey:@"APNS"];
                                                 [alert1 dismissViewControllerAnimated:YES completion:nil];
                                                 alertWindow.hidden = YES;
                                                 alertWindow = nil;
                                             }];
                    [alert1 addAction:cancel];
                    [alertWindow.rootViewController presentViewController:alert1 animated:YES completion:nil];
                    //
                }
                ////[alert release];
                [defaults setObject:@"0" forKey:@"APNS"];
            }
                
                break;
            case UIApplicationStateInactive:{
                
                [defaults setObject:@"1" forKey:@"APNS"];
                [defaults synchronize];
                break;
            }
                
            case UIApplicationStateBackground:{
                
                [defaults setObject:@"1" forKey:@"APNS"];
                break;
            }
            default:
                break;
        }
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        //completionHandler(UIBackgroundFetchResultNoData);
    }
    else{
        [self sendResetBadgeRequest];
        
    }
    
    
    
    
    completionHandler(UIBackgroundFetchResultNoData);
}

-(void) sendResetBadgeRequest
{
    
    NSString* userid = [defaults valueForKey:KEY_USERID];
    NSString *urlString;
    if (userid.length > 0) {
        
        urlString = [NSString stringWithFormat:@"%@firstuse/resetbadge?hubCitiId=%@&deviceId=%@&userId=%@",BASE_URL,[defaults objectForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_USERID]];
    }
    else{
        urlString = [NSString stringWithFormat:@"%@firstuse/resetbadge?hubCitiId=%@&deviceId=%@",BASE_URL,[defaults objectForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_DEVICEID]];
        
    }
    
    
    
    NSString* responseJson = [ConnectionManager establishGetConnectionforJsonData:urlString];
    NSData *data = [responseJson dataUsingEncoding:NSUTF8StringEncoding];
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    
    if (responseCode  == 10000) {
        
        if ([responseText isEqualToString:@"Success"]) {
            
            [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
            NSLog(@"Success");
        }
    }
    
    }





#pragma mark login

-(void)requestForLogin
{
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserDetails><userName><![CDATA[%@]]></userName>",[defaults valueForKey:@"Username"]];
    [xmlStr appendFormat:@"<password><![CDATA[%@]]></password><appVersion>%@</appVersion>",[defaults valueForKey:@"Password"],AppVersion];
    [xmlStr appendFormat:@"<deviceId>%@</deviceId><hubCitiKey>%@</hubCitiKey><platform>IOS</platform></UserDetails>",[defaults valueForKey:KEY_DEVICEID],HUBCITIKEY];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/getuserlogin",BASE_URL];
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    [self parseLoginScreenData:responseXml];
    ReleaseAndNilify(xmlStr);
    
}

/*
 *
 Parsing logic for login screen
 *
 */
-(void)parseLoginScreenData:(NSString *) response
{
    DLog(@"response %@",response);
    if ([UtilityManager isResponseXMLNullOrEmpty:response])
        return;
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    int responseCode = [[TBXML textForElement:saveResponseCode] intValue];
    if(responseCode == 10000)
    {        
        TBXMLElement *userIdElement = [TBXML childElementNamed:KEY_USERID parentElement:tbXml.rootXMLElement];
        TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:KEY_HUBCITIID parentElement:tbXml.rootXMLElement];
        TBXMLElement *hubCitiNameElement = [TBXML childElementNamed:KEY_HUBCITINAME parentElement:tbXml.rootXMLElement];
        
       TBXMLElement * isTempUserElement = [TBXML childElementNamed:@"isTempUser" parentElement:tbXml.rootXMLElement];
        TBXMLElement * noEmailCountElement = [TBXML childElementNamed:@"noEmailCount" parentElement:tbXml.rootXMLElement];
       
        
        [defaults  setObject:[TBXML textForElement:userIdElement] forKey:KEY_USERID];
        [defaults  setObject:[TBXML textForElement:hubCitiIdElement] forKey:KEY_HUBCITIID];
        [defaults  setObject:[TBXML textForElement:hubCitiNameElement] forKey:KEY_HUBCITINAME];
        [container setMenuState:MFSideMenuStateClosed];
//        if([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
//        {
//            [SharedManager setGps_allow_flag:YES];
//            [defaults setValue:@"YES" forKey:@"allowingGPS"];
//        }
//        else
//        {
//            [SharedManager setGps_allow_flag:NO];
//            [defaults setValue:@"NO" forKey:@"allowingGPS"];
//        }
		[NewsUtility setUserSettings];
        
        if ([defaults boolForKey:@"canDisplayAPNSListScreen"]==YES && userIdentifier==FALSE) {
            //[defaults setBool:YES forKey:@"hideBackBtn"];
            [defaults setBool:YES forKey:@"fromLogin"];
            APNSListViewController *pushotifyList = [[APNSListViewController alloc] initWithNibName:@"APNSListViewController" bundle:[NSBundle mainBundle]];
            [container.centerViewController pushViewController:pushotifyList animated:NO];
            //[pushotifyList release];
            [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
        }
        else{
            //Navigate to main menu page
            if ([defaults boolForKey:@"canDisplayAPNSListScreen"]==YES && userIdentifier) {
                [UtilityManager showAlert:nil msg:@"Please Sign up to view the Notification"];
               
                // [alert1 release];
            }
            
            NSString* customerId = [defaults objectForKey:@"currentuserId"];
            if (customerId) {
                
                if (![customerId isEqualToString:[defaults valueForKey:KEY_USERID]]) {
                    
                    ReleaseAndNilify(cashedResponse);
                    [defaults setValue: nil forKey: @"currentuserId"];
                }
                else {
                    isSameUser = true;
                }
            }
            
            if ([RegionApp isEqualToString:@"1"] && ![defaults valueForKey:@"sortedCitiIDs"]) {
                [self sortCitiId : tbXml];
            }
            
            MainMenuViewController *mainView = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController" bundle:[NSBundle mainBundle]];
            [container.centerViewController pushViewController:mainView animated:NO];
            ReleaseAndNilify(mainView);
        }
        
        if ([[TBXML textForElement:noEmailCountElement] isEqualToString:@"1"] )
        {
            //            [[NSNotificationCenter defaultCenter]
            //             postNotificationName:@"emailCount"
            //             object:self];
            
            NSNotification *notification;
            [NewsUtility emailCountDisplay:notification onViewController:self.window.rootViewController];
        }
        if ([[TBXML textForElement:isTempUserElement] isEqualToString:@"1"] )
        {
            //            [[NSNotificationCenter defaultCenter]
            //             postNotificationName:@"tempPassword"
            //             object:self];
            
            NSNotification *notification;
            [NewsUtility tempPasswordDisplay: notification onViewController:self.window.rootViewController];
        }


    }
    
    else
    {
        TBXMLElement *msgElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
       
        [UtilityManager showAlert:nil msg:[TBXML textForElement:msgElement]];
        
               return;
        
    }
    
}
-(void)requestCityPrefrences
{
    //iWebRequestState = FetchCityPreferences;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    if ([SharedManager userInfoFromSignup] == YES)
    {
        [requestStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    }
    else
    {
        [requestStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId><userId>%@</userId>",[defaults valueForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_USERID]];
    }
    [requestStr appendFormat:@"</UserDetails>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getcitypref",BASE_URL];
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    [self parse_FetchCityPreferences:responseXml];
    
    
}

- (NSDictionary *)parseQueryString:(NSString *)query {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init] ;
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    
    for (NSString *pair in pairs) {
        NSArray *elements = [pair componentsSeparatedByString:@"="];
        NSString *key = [[elements objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *val = [[elements objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [dict setObject:val forKey:key];
    }
    return dict;
}

#pragma mark UserTracking

/*
 retId
 retlocId
 prodId
 eventId
 fundEvtId
 hubcitiId
 couponId
 hotdealId
 pageId - AnythingPageID/SpecialOfferID
 
 
 <retailerId>11</ retailerId >
 <retailerLocationId>11</retailerLocationId>	*<pageId>50</pageId> //Common node
 */

-(void)userTracking:(id)sender{
    NSURL *url = [NSURL URLWithString:[defaults valueForKey:KEY_URL]];
    NSString *newString = [[[url path] componentsSeparatedByCharactersInSet:
                            [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                           componentsJoinedByString:@""];
    
    int qrType = [newString intValue];
    
    NSDictionary *dict = [defaults valueForKey:@"UrlQuery"];
    [dict valueForKey:@"retId"];
    [dict valueForKey:@"retlocId"];
    [dict valueForKey:@"prodId"];
    [dict valueForKey:@"eventId"];
    [dict valueForKey:@"fundEvtId"];
    [dict valueForKey:@"couponId"];
    [dict valueForKey:@"hotdealId"];
    [dict valueForKey:@"pageId"];
    
    NSMutableString *reqStr =[[NSMutableString alloc]initWithFormat:@"<ShareProductInfo><qrTypeCode>%d</qrTypeCode><userId>%@</userId><hubcitiId>%@</hubcitiId>",qrType,[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    switch (qrType) {
        case 1000:
            [reqStr appendFormat:@"<pageId>%@</pageId></ShareProductInfo>",[dict valueForKey:@"prodId"]];
            break;
        case 2000:
            [reqStr appendFormat:@"<retailerId>%@</retailerId><retailerLocationId>%@</retailerLocationId></ShareProductInfo>",[dict valueForKey:@"retId"],[dict valueForKey:@"retlocId"]];
            break;
        case 2100:
            if ([dict valueForKey:@"retId"]!=nil) {
                [reqStr appendFormat:@"<retailerId>%@</retailerId><retailerLocationId>%@</retailerLocationId><pageId>%@</pageId></ShareProductInfo>",[dict valueForKey:@"retId"],[dict valueForKey:@"retlocId"],[dict valueForKey:@"prodId"]];
            }
            else{
                [reqStr appendFormat:@"<pageId>%@</pageId></ShareProductInfo>",[dict valueForKey:@"prodId"]];
            }
            
            break;
        case 2200:
            [reqStr appendFormat:@"<retailerId>%@</retailerId><retailerLocationId>%@</retailerLocationId><pageId>%@</pageId></ShareProductInfo>",[dict valueForKey:@"retId"],[dict valueForKey:@"retlocId"],[dict valueForKey:@"prodId"]];
            break;
        case 2400:
            [reqStr appendFormat:@"<pageId>%@</pageId></ShareProductInfo>",[dict valueForKey:@"eventId"]];
            break;
        case 2500:
            [reqStr appendFormat:@"<pageId>%@</pageId></ShareProductInfo>",[dict valueForKey:@"fundEvtId"]];
            break;
        case 2600:
            [reqStr appendFormat:@"<pageId>%@</pageId></ShareProductInfo>",[dict valueForKey:@"hotdealId"]];
            break;
        case 2700:
            [reqStr appendFormat:@"<pageId>%@</pageId></ShareProductInfo>",[dict valueForKey:@"couponId"]];
            break;
            
        default:
            break;
    }
    
    NSMutableString *userUrlStr=[[NSMutableString alloc]initWithFormat:@"%@ratereview/receiversharetrack",BASE_URL];
    DLog(@"%@",userUrlStr);
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:userUrlStr withParam:reqStr]];
    DLog(@"%@",response);
    //[reqStr release];
}

@end

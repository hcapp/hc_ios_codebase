//
//  HubCitiConstants.h
//  HubCiti
//
//  Created by Ajit on 9/4/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import "CommonUtility.h"
#import "HubCitiConnectionManager.h"
#import "HubCitiManager.h"
#import "CommonUtilityNews.h"

#ifndef HubCiti_HubCitiConstants_h
#define HubCiti_HubCitiConstants_h

#endif


typedef enum webServicescallTypes
{
    CATEGORIES_SET,
    SIGNUP_UPDATEUSER,
    GETUSERINFO,
    SIGNUP_SUCCESSRESPONSE,
    MENUDISPLAY,
    MAINMENU_GETCUSTOMUI,
    GETPARTNERS,
    CITIEXPRET,
    SEARCH_LIST,
    RETAILER_DETAIL,
    CATEGORIES_DISPLAY,
    FILTERS_RETAILERLIST,
    LOGIN_AUTHENTICATEUSER,
    LOGIN_FORGOTPASSWORD,
    CHANGE_PASSWORD,
    RETSUMMARY,
    PARTNERRET,
    GETFAVCATEGORIES,
    HubcitiAnythingInfo,
    appsitedetails,
    catsearch,
    EVENTSLIST,
    EVENTCATLIST,
    singlecatretailers,
    FUNDRAISER_DETAILS,
    FAQ,
    FundraisersList,
    FetchCityPreferences,
    PRODUCT_SHARE,
    APPSITE_SHARE,
    SPECIAL_OFFER_SHARE,
    ANYTHING_SHARE,
    EVENT_SHARE,
    FUNDRAISER_SHARE,
    HOTDEAL_SHARE,
    COUPONS_SHARE,
    BAND_SHARE,
    BAND_EVENT_SHARE,
     shareapplink,
    MAPLOGISTICS,
    GETFAVLOCATIONS,
    LOCATIONS_SET,
    SPECIALS_REQUEST,
    DEALS_REQUEST,
    COUPONS_REQUEST,
    FUNDRAISER_REQUEST,
    FUNDRAISER_LOCATION,
    UPDATE_DEVICE,
    BAND_CAT_TYPE,
    BANDS_EVENTS_INFO,
    COMB_TEMP_REQ,
    RESETPASSWORD,
    SORT_FILTER,
    COUPON_MAP,
    COUPON_MYACC_REQUEST,
    MULTIPLE_BANDS,
    NEWS_SHARE
    
    
}WebRequestState;

typedef enum MarqueeLabel
{
    SCROLL_LEFT_TO_RIGHT = 0,
    SCROLL_RIGHT_TO_LEFT,
    SCROLL_UP_TO_DOWN,
    SCROLL_DOWN_TO_UP,
    ROTATE_RIGHT_TO_LEFT,
    ROTATE_LEFT_TO_RIGHT,
    ROTATE_DOWN_TO_UP,
    ROTATE_UP_TO_DOWN
    
}MarqueeType;

#define PROD_ENVIRON [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"ProdKey"] boolValue]
/***************** BASE URL ********************************/
//#define BASE_URL @"http://10.11.200.64:8080/HubCiti1.6/"//dhruva SERVER 200.64
//#define BASE_URL @"http://sdw2730:8080/HubCiti2.4.3/"//kumar SERVER
//#define BASE_URL @"http://10.11.202.76:8080/HubCiti1.6/"//SPAN QA SERVER
//#define BASE_URL @"http://66.228.143.28:8080/HubCiti2.7/"//Etapmus QA SERVER // HubCiti_Test
//#define BASE_URL @"http://66.228.143.28:8080/HubCiti_Perf/"//Etapmus QA SERVER // HubCiti_Test
//#define BASE_URL @"http://10.10.222.152:9990/HubCiti_Toggle/"//Etapmus QA SERVER // HubCiti_Test
//#define BASE_URL @"https://app.scansee.net/HubCiti2.8/"//Production SERVER
//#define BASE_URL @"http://10.11.200.226:8080/HubCiti2.1/" // Kiran SERVER // 10.11.200.226
//#define BASE_URL @"http://sdw2107:9990/HubCiti2.6/" // Kumar SERVER // 10.11.200.226
//#define BASE_URL @"http://66.228.143.28:8080/HubCiti2.8/"//Etapmus QA SERVER // HubCiti_Test

#define BASE_URL (PROD_ENVIRON)? @"https://app.scansee.net/HubCiti2.8.3/": @"http://66.228.143.28:8080/HubCiti2.8.7/"


/***********************************************************/

/***************** HTML FILES URL **************************/
//#define HTML_URL @"http://sdw2730:8080/Images/hubciti/"//kumar SERVER
//#define HTML_URL @"http://10.11.202.76:8080/Images/hubciti/html/"//SPAN QA SERVER
//#define HTML_URL @"http://66.228.143.27:8080/Images/hubciti/html/"//Etapmus QA SERVER
//#define HTML_URL @"https://www.scansee.net/Images/hubciti/html/"//Production SERVER

#define HTML_URL (PROD_ENVIRON)? @"https://www.scansee.net/Images/hubciti/html/": @"http://66.228.143.27:8080/Images/hubciti/html/"
/***********************************************************/

/***************** QRCODE URL **************************/
//#define QRCODEURL @"http://66.228.143.28:8080" //Etapmus qa
//#define QRCODEURL @"www.scansee.net" //production
//#define QRCODEURL @"10.11.202.76:8080" //span qa

#define QRCODEURL (PROD_ENVIRON)? @"www.scansee.net": @"http://66.228.143.28:8080"
/*******************************************************/


//---------------------VERSIONCHECK_URL Configuration--------------------------
//#define VERSIONCHECK_URL @"https://app.scansee.net/ScanSeeVersionCheck/firstUse/versioncheckhubciti"//PRODUCTION SERVER
//#define VERSIONCHECK_URL @"http://10.11.202.76:8080/ScanSeeVersionCheck/firstUse/versioncheckhubciti"//SPAN PUBLIC SERVER
//#define VERSIONCHECK_URL @"http://66.228.143.28:8080/ScanSeeVersionCheck/firstUse/versioncheckhubciti"// Etapmus QA SERVER
//#define VERSIONCHECK_URL @"http://10.11.200.64:8080/ScanSeeVersionCheck/firstUse/versioncheckhubciti"//LOCAL SERVER

#define VERSIONCHECK_URL (PROD_ENVIRON)? @"https://app.scansee.net/ScanSeeVersionCheck/firstUse/versioncheckhubciti": @"http://66.228.143.28:8080/ScanSeeVersionCheck/firstUse/versioncheckhubciti"
//-----------------------------------------------------------------------------


#define HUBCITIKEY	[[NSBundle mainBundle] objectForInfoDictionaryKey:@"ProdHubcitiKey"]
//#define HUBCITIKEY	@"HCNewsTemplate2231" //@"spanqa.hubciti68" // RegionCityExphubCitiApp1143 Configure HubCiti Key for a build RegionApp15Oct2142 spanqa.regionapps3152 spanqa.regionsapp2070 AddisonTX34 Fort Davis16 //Marble Falls10   RockWall17        //Tyler19 // spanqa.regionsapp28   //Rockwall Area Test48" //AddisonTX2130 Austin49 //shrini2113 //spanqa.ahubciti3012106


#define RegionApp [[NSBundle mainBundle] objectForInfoDictionaryKey:@"RegionKey"]
//#define RegionApp @"1"
//dev
// HUBCITIKEY "RegionCityExphubCitiApp1143"   RegionApp "1"
//RegionApp15Oct2142 -1

//QA
//spanqa.regionapps3152 -1
//spanqa.regionsapp2070 -1
//AddisonTX2130 -0
//spanqa.ahubciti3012106-0
//Killeen2178-1
//HEB Test2156 - 1


//production
//AddisonTX34 -0
//Fort Davis16 -0
//Marble Falls10 -0
//RockWall17   (not there)
//Tyler19 -1
//spanqa.regionsapp28 -1
//Rockwall Area Test48 -1
//Killeen52 -1
//Austin49 -0
//TRCA55 - 0
//Mt Airy67 - 0
//HEB Test54 - 1
//MarbleFalls57 - 0        ---- The Marble Falls App  -----
//Tyler Test75  - 1

#define SharedManager [HubCitiManager sharedManager]
#define ConnectionManager [HubCitiConnectionManager connectionManager]
#define Location [LocationManager locationManager]
#define ProductSearch [ProductSearchUtility productSearch]
#define UtilityManager [CommonUtility utilityManager]
#define NewsUtility [CommonUtilityNews utilityManagers]
#define HubCitiAppDelegate (AppDelegate*)[[UIApplication sharedApplication] delegate]
#define Network  [Reachability reachabilityForInternetConnection]
#define ReleaseAndNilify(x)  x = nil;
#define IS_IPHONE5   ([[UIScreen mainScreen] bounds].size.height == 568)
#define IOS7 [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0
#define IOS9 [[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0
#define IOS8 [[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0
#define defaults [NSUserDefaults standardUserDefaults]
#define AppVersion [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

//AsyncImage Constants
#define REAL_CUSTOM_TIMEINTERVAL 3.0
#define REAL_DEFAULT_TIMEINTERVAL 60.0

//Screen Width & height
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)

#define IPAD  UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

#define VARIABLE_WIDTH(width) SCREEN_WIDTH * ((float)width/320)
#define VARIABLE_HEIGHT(height) SCREEN_HEIGHT * ((float)height/480)

#define bottomBarButtonHeight 50
#define bottomBarButtonHeight_iPad 80
#define MARQEE_HEIGHT 50
//Connection Manager Constants
#define HTTP_CONTLENGTH     @"Content-Length"
#define HTTP_CONTTYPE       @"Content-Type"
#define RESPONSETEXT        @"responseText"
#define RESPONSECODE        @"responseCode"

//Application Constants
#define KEY_CITILIST        @"cityList"
#define KEY_DEVICEID        @"deviceID"
#define KEY_USERID			@"userId"
#define MAINMENU			@"Main Menu"
#define SHARE               @" "
#define KEY_USERID			@"userId"
#define KEY_HUBCITIID		@"hubCitiId"
#define KEY_HUBCITINAME		@"hubCitiName"
#define KEY_MENULEVEL       @"MenuLevel"
#define KEY_TEMPLATEID      @"TemplateId"
#define KEY_MAINMENUID      @"mainMenuId"
#define KEY_MENULEVEL       @"MenuLevel"
#define KEY_LINK            @"link"//app link for forcefull update.
#define KEY_DONESORT        @"isDoneSubMenuGroupingAndSorting"
// Events
#define EVENTID             @"eventId"
#define EVENTLISTID         @"eventsListId"
#define EVENTNAME           @"eventName"

//Fundraiser
#define FUNDRAISERID        @"fundraiserId"
#define FUNDRAISERLISTID    @"fundraisersListId"
#define EVENTNAME           @"eventName"

#define KEY_LINKID          @"linkId"
#define KEY_RETAILERID		@"retailerId"
#define KEY_PAGEID             @"pageId"
#define KEY_MITEMID         @"mItemId"
#define KEY_MITEMNAME       @"mItemName"
#define KEY_RESPONSEXML     @"responseXml"
#define KEY_RTLNAME         @"retailerName"
#define KEY_DISTANCE        @"distance"
#define KEY_RLISTID         @"retListId"
#define KEY_BANDID          @"bandId"


#define KEY_BOTTOMBUTTONID  @"bottomBtnId"
#define KEY_BOTTOMBUTTONNAME  @"bottomBtnName"
#define KEY_CATID           @"catID"
#define KEY_LATITUDE        @"Latitude"
#define KEY_LONGITUDE       @"Longitude"

#define KEY_LATITUDE_TL     @"Lat"
#define KEY_LONGITUDE_TL    @"Long"

#define VALUE_COUPON        @"Coupon"
#define KEY_PRODUCTID		@"productId"
#define KEY_SCANTYPEID      @"scanTypeId"
#define KEY_PRODLISTID      @"prodListId"
#define KEY_PRODUCTID		@"productId"
#define KEY_PRODUCTNAME		@"productName"
#define KEY_PRODUCTDESC		@"productShortDescription"
#define KEY_PRODUCTIMGPATH	@"productImagePath"
#define KEY_PRODUCTPRICE	@"productPrice"
#define KEY_PRODUCTDETAIL	@"ProductDetail"
#define KEY_ALERTPRODID     @"alertProdId"
#define TAG_IMGPATH         @"imagePath"
#define KEY_CLRFLAG			@"CLRFlag"
#define KEY_CLR             @"CLR"
#define KEY_SEARCHFROM      @"searchFrom"
#define KEY_ANYPAGEID       @"anyPageId"

#define KEYZIPCODE_TL       @"ZipCode_TL"
#define KEYZIPCODE          @"ZipCode"

#define KEY_SHAREMSG        @"Share Message"
#define KEY_HYPERLINK       @"Hypelink"
#define KEY_URL             @"URL"
#define KEY_PRODUCTDETAIL	@"ProductDetail"
#define HC_LOGOIMGPATH      @"logoImagePath"
#define KEY_PRODUCTID       @"productId"
#define KEY_INIGMA_BARCODE  @"key_inigmaBarCode"
#define BottomButton        @"BottomButton"
#define KEY_MENUNAME        @"menuName"
#define KEY_INIGMA_BARCODETYPE  @"key_inigmaBarCodeType"

#define ASYNC_IMAGE_TAG 9999
#define LABEL_TAG 8888
#define DETAIL_LABEL_TAG 7777
#define MAX_NUM_API_PARTNER 30000

//Assumed number of categories does not exist beyond 20000
#define NUM_CATEGORIES 20000
#define NUM_PRODUCTINFOITEMS 10
#define REAL_CUSTOM_TIMEINTERVAL 3.0
#define REAL_DEFAULT_TIMEINTERVAL 60.0

#define VIEW_FRAME_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define VIEW_FRAME_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define IPAD_TEXTFIELD_HEIGHT 40
#define IPAD_LABEL_HEIGHT 30
#define IPAD_X_OFFSET  30
#define TEXTFIELD_HEIGHT 30
#define LABEL_HEIGHT 20
#define X_OFFSET 20
#define STATUSBAR_HEIGHT [UIApplication sharedApplication].statusBarFrame.size.height
#define NAVIGATION_HEIGHT (self.navigationController.navigationBar.frame.size.height)
#define EXCLUDE_BAR_HEIGHT STATUSBAR_HEIGHT+NAVIGATION_HEIGHT

// notification
static NSString * hamburgerImageChangeNotification;

#define DEVICE_TYPE UI_USER_INTERFACE_IDIOM()

// Share Type
#define KEY_SHARE_TYPE @"shareType"
#define NEWS_HEIGHT ((float)432 / (float) 768) * SCREEN_WIDTH // 432 - ipad height 768 - ipad width


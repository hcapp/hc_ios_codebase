//
//  ProductSearchUtility.h
//  Scansee
//
//  Created by Chaitra on 30/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HubCitiConnectionManager.h"

@protocol ProductSearchUtilityDelegate <NSObject>

-(void) returnProductInfo :(NSArray*)info;

@end

@interface ProductSearchUtility : NSObject<HubCitiConnectionManagerDelegate>
{
    NSMutableArray *productInfo[2];
    id <ProductSearchUtilityDelegate> delegate;
}
+ (id)productSearch ;

-(NSArray *) getProductInfo : (NSString *) searchKey lastVisitedRecord : (int) lastRecord;
-(void) getProductInfo : (NSString *) searchKey withDelegate:(id)m_delegate;

-(void) parseProductInfo : (NSString *) response;

@end

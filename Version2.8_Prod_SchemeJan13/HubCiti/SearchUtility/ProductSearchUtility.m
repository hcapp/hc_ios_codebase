
//
//  ProductSearchUtility.m
//  Scansee
//
//  Created by Chaitra on 30/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProductSearchUtility.h"

static ProductSearchUtility *sharedProductSearch= nil;

@implementation ProductSearchUtility


+ (id)productSearch {
    @synchronized(self) {
        if(sharedProductSearch == nil)
            sharedProductSearch = [[super allocWithZone:NULL] init];
        
    }
    return sharedProductSearch;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self productSearch] retain];
}
- (id)copyWithZone:(NSZone *)zone {
    return self;
}
- (id)retain {
    return self;
}
- (unsigned long)retainCount {
    return UINT_MAX; //denotes an object that cannot be released
}
- (oneway void)release {
    // never release
}
- (id)autorelease {
    return self;
}

-(NSArray *) getProductInfo : (NSString *) searchKey lastVisitedRecord:(int)lastRecord{
    
    productInfo[0] = [[NSMutableArray alloc] init];
    productInfo[1] = [[NSMutableArray alloc] init];
    
    NSMutableArray *info = [[[NSMutableArray alloc] init] autorelease];
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendString:@"<ProductDetail>"];
    [requestStr appendFormat:@"<userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<searchkey><![CDATA[%@]]></searchkey>", searchKey];
    [requestStr appendFormat:@"<lastVistedProductNo>%d</lastVistedProductNo>",lastRecord];
    [requestStr appendString:@"</ProductDetail>"];	
    
    NSMutableString *urlString = [[BASE_URL mutableCopy] autorelease];
    [urlString appendFormat:@"scannow/getProdForName"];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
    [requestStr release];
    ReleaseAndNilify(requestStr);
    if ([UtilityManager isNullOrEmptyString:responseXml]) {
        [responseXml release];
        ReleaseAndNilify(responseXml);
        return nil;
    }
    [self parseProductInfo:responseXml];
    
    [responseXml release];
    
    [info addObject:productInfo[0]];
    [info addObject:productInfo[1]];
    
    [productInfo[0] release];
    [productInfo[1] release];
    
    return info;
}



-(void) getProductInfo : (NSString *) searchKey withDelegate:(id)m_delegate
{
    delegate = m_delegate;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ProductDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<searchkey><![CDATA[%@]]></searchkey>", searchKey];
    
    //@Deepak: adding parameter for usertracking
    if ([defaults valueForKey:KEY_MAINMENUID])
    [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];

    if ([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    
    [requestStr appendString:@"</ProductDetail>"];
    
    NSMutableString *urlString = [[BASE_URL mutableCopy] autorelease];
    [urlString appendFormat:@"scannow/getsmartsearchprods"];
    
    
    [ConnectionManager establishConnectionForSmartSearch:requestStr base:urlString withDelegate:self];
    [requestStr release];
    ReleaseAndNilify(requestStr);
}


-(void)responseData:(NSString *) respo {
    productInfo[0] = [[NSMutableArray alloc] init];
    productInfo[1] = [[NSMutableArray alloc] init];
    
    NSMutableArray *info = [[[NSMutableArray alloc] init] autorelease];
    if ([UtilityManager isNullOrEmptyString:respo]) {
        ReleaseAndNilify(respo);
        return;
    }
    [self parseProductInfo:respo];
    
    //checking the first element in smart search result is equal to entered search string
    //DLog(@"productInfo[0]:%@ and SearchString:%@",[productInfo[0] objectAtIndex:0],[defaults valueForKey:@"SearchString"]);
    DLog(@"[productInfo[0] %@",productInfo[0]);
    DLog(@"%@",[defaults valueForKey:@"SearchString"]);
   // If smart search result is only one then it should not display, as it belongs to all cat like manula search
    if ([productInfo[0] count]<=1 || ![[productInfo[0] objectAtIndex:0]isEqualToString:[defaults valueForKey:@"SearchString"]]) {
        
        [productInfo[0] release];
        [productInfo[1] release];
        return;
    }
    
    [info addObject:productInfo[0]];
    [info addObject:productInfo[1]];
    
    if ([defaults boolForKey:@"ReturnResult"] == NO) {
        [productInfo[0] release];
        [productInfo[1] release];
        return;
    }
    [delegate returnProductInfo:info];
    [productInfo[0] release];
    [productInfo[1] release];
}

-(void) parseProductInfo : (NSString *) response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
	TBXMLElement *productDetailElement = [TBXML childElementNamed:@"ProductDetail" parentElement:tbXml.rootXMLElement];

	if (productDetailElement == nil) {
        
        //response = nil;
		return;
	}
    [productInfo[0] removeAllObjects];
    [productInfo[1] removeAllObjects];
	while (productDetailElement != nil) {
		TBXMLElement *categsElement = [TBXML childElementNamed:@"categs" parentElement:productDetailElement];
		TBXMLElement *parCatIdElement = [TBXML childElementNamed:@"parCatId" parentElement:productDetailElement];
        DLog(@"product list = %@ " , productInfo[0]);
        if (categsElement != nil ) {
             [productInfo[0] addObject:[TBXML textForElement:categsElement]];
        }
        if (parCatIdElement != nil) {
             [productInfo[1] addObject:[TBXML textForElement:parCatIdElement]];
        }
       
		productDetailElement = [TBXML nextSiblingNamed:KEY_PRODUCTDETAIL searchFromElement:productDetailElement];
	}
    
}

@end

//
//  CreateEditAccountTableViewCell.m
//  HubCiti
//
//  Created by service on 9/23/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "CreateEditAccountTableViewCell.h"

@implementation CreateEditAccountTableViewCell

@synthesize labelField;
@synthesize valueField,valueViewField;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.labelField = [self createLabel];
        self.valueField = [self createTextField];
        self.valueViewField = [self createTextView];
        
        [self.contentView addSubview:self.labelField];
        [self.contentView addSubview:self.valueField];
        [self.contentView addSubview:self.valueViewField];
       
       
        
    }
    
    return self;
}



-(UITextField*) createTextField
{
    UITextField* textField;
    textField = [[UITextField alloc]init];
    textField.backgroundColor = [UIColor clearColor];
    textField.layer.borderColor = [UIColor blackColor].CGColor;
    textField.layer.borderWidth = 1.0;
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.font = [UIFont systemFontOfSize:15];
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
   
    
    return textField;
}
-(UILabel*) createLabel
{
    UILabel * labelView = [[UILabel alloc] init] ;
    labelView.translatesAutoresizingMaskIntoConstraints=YES;
    labelView.lineBreakMode = NSLineBreakByWordWrapping;
    labelView.numberOfLines = 0;
    labelView.textColor = [UIColor blackColor];
    
    return labelView;
}
-(UITextView*) createTextView
{
    UITextView * textView= [[UITextView alloc]init];
    textView.backgroundColor = [UIColor clearColor];
    textView.layer.borderWidth = 1.0;
    textView.layer.borderColor = [UIColor blackColor].CGColor;
    textView.font = [UIFont systemFontOfSize:15];
    textView.keyboardType = UIKeyboardTypeDefault;
    textView.returnKeyType = UIReturnKeyDone;
    
    return textView;
}


@end

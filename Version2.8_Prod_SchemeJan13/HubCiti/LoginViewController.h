//
//  LoginViewController.h
//  HubCiti
//
//  Created by Anjana on 9/3/13.
//  Copyright (c) 2013 Anjana. All rights reserved.
//

/*******************************************
 Class designed to display the login page
 *****************************************/

#import <UIKit/UIKit.h>
#import "LoginDO.h"
#import "SignupDO.h"
#import "HubCitiConnectionManager.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "SdImageView.h"


_Bool userIdentifier;
@interface LoginViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate,HubCitiConnectionManagerDelegate>
{
    IBOutlet UIImageView *backgroundImage;
    IBOutlet UIImageView *bottomImageView;
    IBOutlet UIView *splashScreen;
    
    IBOutlet UIScrollView *scroll;
    //UIImageView *poweredByImg;
    SdImageView *topImageView, *poweredByImg;;
    UILabel *usernameLabel, *pwdLabel,*forgotPwdLabel,*remMeLabel,*powerByLabel,*versionLabel;
    UITextField *txtField_userName, *txtField_password;
    UIButton *loginButton;
    UIButton *signUpButton;
    UIButton *guestButton;
    UIButton *forgotPwdButton;
    UIButton *remMeButton;
    
    LoginDO *login_DO;
    SignupDO *signupDO;
    BOOL isRemember;
    BOOL sendEmail,forgotPasswordFlag,emailIdExists;
    
    WebRequestState iWebRequestState;  //create enum
    HubCitiConnectionManager *connectionManager;
    NSString *guestUser;
    NSString *guestPassword;
}
@property (nonatomic,strong) NSString *guestUser;
@property (nonatomic,strong) NSString *guestPassword;
@property (nonatomic,strong) UIAlertAction * okAction;
@property(nonatomic,strong)LoginDO *login_DO;
@property(nonatomic, strong)UITextField *txtField_userName, *txtField_password;
@property(nonatomic,strong) NSString * forgotEmailOrUser;
@property(nonatomic,strong) NSString * resetPasswordString;
@property(nonatomic,strong) NSString * tempPwdString;
@property(nonatomic,strong) NSString * noEmailCountString;

-(UILabel *)createLabel:(NSString*)title rect:(CGRect)rect;
- (UITextField *)createTxtField:(NSString*)placeHolder rect:(CGRect)rect;

@end

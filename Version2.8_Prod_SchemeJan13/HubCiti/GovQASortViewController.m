//
//  GovQASortViewController.m
//  HubCiti
//
//  Created by Ashika on 9/2/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "GovQASortViewController.h"


@interface GovQASortViewController (){
    CustomizedNavController *cusNav;
}

@end

@implementation GovQASortViewController

@synthesize sortAlphabet, sortName, sortType, sortObj;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    cusNav =(CustomizedNavController *) self.navigationController;
    
    [cusNav hideHambergerButton:YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    UIButton *btn = [UtilityManager customizeBarButtonItem:@"Done"];
    [btn addTarget:self action:@selector(doneButtonTouch:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = mainPage;
    //[mainPage release];
    
    UIButton *btncancel = [UtilityManager customizeBarButtonItem:NSLocalizedString(@"Cancel",@"Cancel")];
    [btncancel addTarget:self action:@selector(cancelButtonTouch:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* btncancelbar = [[UIBarButtonItem alloc] initWithCustomView:btncancel];
    self.navigationItem.rightBarButtonItem = btncancelbar;
  //  [btncancelbar release];
    
    
    self.navigationItem.title = @"Group & Sort";
    
    arrFilters = [[NSMutableArray alloc]initWithObjects:@"Alphabetically",@"Type", nil];
    arrSorting = [[NSMutableArray alloc]initWithObjects:@"Name",nil];
    
    sortAlphabet = sortObj.alphabetSelectedGovQA;
    sortType = sortObj.typeSelectedGovQA;
    sortName = sortObj.nameSelectedGovQA;

}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    tblGroupingNSorting = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-60) style:UITableViewStylePlain];
    tblGroupingNSorting.dataSource=self;
    tblGroupingNSorting.delegate=self;
    [tblGroupingNSorting setBackgroundColor:[UIColor whiteColor]];
    
    
    [self.view addSubview:tblGroupingNSorting];
    
    [tblGroupingNSorting reloadData];

}
-(void)doneButtonTouch:(id)sender{
    [defaults setValue:@"YES" forKey:@"isComingFromSorting"];
    if (!sortObj) {
        sortObj =[[SortAndFilter alloc]init];
    }
    sortObj.alphabetSelectedGovQA = sortAlphabet;
    sortObj.typeSelectedGovQA = sortType;
    sortObj.nameSelectedGovQA = sortName;
    [self saveCustomObject:sortObj key:@"SortObject"];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)saveCustomObject:(SortAndFilter *)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
    
}

-(void)cancelButtonTouch:(id)sender{
    [defaults setValue:nil forKey:@"isComingFromSorting"];
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 44.0;
    }
    else
    {
        return 70.0;
    }

    
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 26.0;
    }
    else
    {
        return 36.0;
    }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
           return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
        return [arrFilters count];
    else
        return [arrSorting count];
    
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *lblCatName ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 18)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
    }
    else
    {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(15, 1, SCREEN_WIDTH - 10, 28)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
    }
    if(section==0)
        [lblCatName setText:@" Group Items by"];
    else
        [lblCatName setText:@" Sort Items by"];
    
    [lblCatName setTextColor:[UIColor blackColor]];
    [lblCatName setBackgroundColor:[UIColor grayColor]];
    return lblCatName;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellSubMenuListView";
    
    UITableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor =  [UIColor clearColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }
        
    }
    
    if(indexPath.section == 0)
    {
        UILabel *lblEvtDate;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 20)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        }
        else
        {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 30)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
        }
        [lblEvtDate setText:[arrFilters objectAtIndex:indexPath.row]];
        [lblEvtDate setTextColor:[UIColor blackColor]];
        [lblEvtDate setBackgroundColor:[UIColor clearColor]];
        
        [cell.contentView addSubview:lblEvtDate];
       // [lblEvtDate release];
        
        if(indexPath.row == 0){
            if(sortAlphabet == YES){
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else{
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        else{
            if(sortType == YES){
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else{
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        
    }
    else
    {
        
        UILabel *lblEvtDate;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 20)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        }
        else
        {
            lblEvtDate = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, SCREEN_WIDTH - 20, 30)];
            [lblEvtDate setFont:[UIFont boldSystemFontOfSize:18]];
        }
        
        [lblEvtDate setText:[arrSorting objectAtIndex:indexPath.row]];
        [lblEvtDate setTextColor:[UIColor blackColor]];
        [lblEvtDate setBackgroundColor:[UIColor clearColor]];
        //[lblEvtDate setFont:[UIFont boldSystemFontOfSize:13]];
        [cell.contentView addSubview:lblEvtDate];
      //  [lblEvtDate release];
        
        
        if(indexPath.row == 0){
            if(sortName == YES){
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else{
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    // Set value to show checkmark in table row
    if(indexPath.section==0){
        
            sortSelectionval = (int)indexPath.row;
        if (indexPath.row == 0) {
            sortAlphabet = YES;
            sortType = NO;
        }
        else{
            sortAlphabet = NO;
            sortType = YES;
        }

            
      
    }
    
    else{
        
        filterSelectionVal = (int)indexPath.row;
        
        if (indexPath.row == 0) {
            sortName = YES;
            //sortType = NO;
        }
//        else{
//            sortName = NO;
//           // sortType = YES;
//        }
        
      
    }
    DLog(@"%d",filterSelectionVal);
    
    [tblGroupingNSorting reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

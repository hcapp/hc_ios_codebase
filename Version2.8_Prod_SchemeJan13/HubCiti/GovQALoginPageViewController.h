//
//  GovQALoginPageViewController.h
//  HubCiti
//
//  Created by Ashika on 8/17/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "CommonUtility.h"
@class AnyViewController;
@class EmailShareViewController;
@interface GovQALoginPageViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate,MFMessageComposeViewControllerDelegate, UIWebViewDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>{
        //UIImageView *poweredByImg;
    
    WebRequestState iWebRequestState;
    CommonUtility *common;
    UIScrollView *loginView;
    SdImageView *topImageView;
    SdImageView *poweredByImg;
    UILabel *topTextLabel,*topThirdTextLabel;//*topsecondTextLabel,
    UILabel *emailLabel, *pwdLabel,*forgotPwdLabel,*remMeLabel,*powerByLabel;
    UITextField *txtField_emailAddress, *txtField_password;
    UIButton *loginButton;
    UIButton *signUpButton;
    UIButton *guestButton;
    UIButton *forgotPwdButton;
    UIButton *remMeButton;
    NSMutableArray *arrBottomButtonDO;
    NSMutableArray *arrBottomButtonViewContainer;
    UITextField *activeTextField;
   
    BOOL isRemember,isRememberCheked;
    BOOL sendEmail,forgotPasswordFlag,emailIdExists;
    CGFloat animatedDistance;

    CustomizedNavController *cusNav;
    HubCitiConnectionManager *connectionManager;
    NSString *guestUser;
    NSString *guestPassword;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic,retain) NSString *guestUser;
@property (nonatomic,retain) NSString *guestPassword;
@property (nonatomic,retain) NSMutableArray *arrBottomButtonDO;

@property(nonatomic, retain)UITextField *txtField_eamilAddress, *txtField_password;

-(UILabel *)createLabel:(NSString*)title rect:(CGRect)rect;
- (UITextField *)createTxtField:(NSString*)placeHolder rect:(CGRect)rect;


@end

//
//  HubCitiManager.h
//  HubCiti
//
//  Created by Anjana on 9/13/13.
//  Copyright (c) 2013 ajit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HubCitiManager : NSObject
{
    BOOL wishList;
	BOOL list;
	BOOL termsOfUse;
	BOOL gps_allow_flag;
	BOOL hotDealToolBar;
	BOOL wishListProdInfo;
    BOOL Wishcolor;
	BOOL shareFromTL;
	BOOL addToWL;
	BOOL HotDealsplash;
	BOOL refreshProdPage;
	BOOL userInfoFromSignup;
    BOOL myGallery;
    BOOL refreshList;
    BOOL shareEmailCoupon;
    BOOL shareEmailHotDeal;
    BOOL shareEmailSpecialOffer;
    BOOL shareEmailSpecialOfferUrl;
    BOOL shareAppsite;
    BOOL timeFlag;
    BOOL enableAddCoupon;//used in gallery module and coupons
    BOOL enableRedeemCoupon;//used in gallery module and coupons
    BOOL refreshCategories;//used in hot deals to decide on changing the picker contents
    BOOL firstTimeCancel;
    BOOL smartSearchStatus;
    BOOL fromSL;
    BOOL callScanner;
    BOOL refMngLoyTable;//used to check whether to refresh manage loyalty table or not
    NSString *addRemLoyaltyCard;
    BOOL couLoySearchActive;//to find out whether search is active in coupon/loyalty
    BOOL refreshGallery;
    int  filtersCount;
    BOOL refreshAustinRetailers;
    
    BOOL dealsCouponDisplay;
    BOOL dealsHotDealDisplay;
   
    int segmentIndex; //used in gallerylist screen to decide which segment to highlight
    int typeIndex;//0 for claimed, 1 for used and 2 for expired
    
    int retAffId, retAffId_Cat;
    int retGroupId, retGroupId_Cat;
    NSString *sessionId;                //sessionID for GovQA
    int scrollIndicator,submenuScrollIndicator; //current page indicator for pagination

}
@property (nonatomic, strong) NSString *responseXmlString;
@property (nonatomic, strong) NSString *sessionId;          //sessionID for GovQA
@property (nonatomic) BOOL dealsCouponDisplay;
@property (nonatomic) BOOL dealsHotDealDisplay;
@property (nonatomic) BOOL wishList;
@property (nonatomic) BOOL list;
@property (nonatomic) BOOL termsOfUse;
@property (nonatomic) BOOL gps_allow_flag;
@property (nonatomic) BOOL hotDealToolBar;
@property (nonatomic) BOOL wishListProdInfo;
@property (nonatomic) BOOL Wishcolor;
@property (nonatomic) BOOL shareFromTL;
@property (nonatomic) BOOL addToWL;
@property (nonatomic) BOOL HotDealsplash;
@property (nonatomic) BOOL refreshProdPage;
@property (nonatomic) BOOL userInfoFromSignup;
@property (nonatomic) BOOL myGallery;
@property (nonatomic) BOOL refreshList;
@property (nonatomic) BOOL shareEmailCoupon;
@property (nonatomic) BOOL shareEmailHotDeal;
@property (nonatomic) BOOL shareEmailSpecialOffer;
@property (nonatomic) BOOL shareEmailSpecialOfferUrl;
@property (nonatomic) BOOL shareAppsite;
@property (nonatomic) BOOL timeFlag;
@property (nonatomic) BOOL enableAddCoupon;
@property (nonatomic) BOOL enableRedeemCoupon;
@property (nonatomic) BOOL refreshCategories;
@property (nonatomic) BOOL firstTimeCancel;
@property (nonatomic) BOOL smartSearchStatus;
@property (nonatomic) BOOL fromSL;
@property (nonatomic) BOOL callScanner;
@property (nonatomic, strong)NSString *addRemLoyaltyCard;
@property (nonatomic)BOOL refMngLoyTable;
@property (nonatomic)BOOL couLoySearchActive;
@property (nonatomic)BOOL refreshGallery;
@property (readwrite) int filtersCount;//to store the partners count and decide to navigate accordingly
@property (readwrite) BOOL refreshAustinRetailers;//to check for refreshing the austin experience retailer table
@property (readwrite) int segmentIndex;
@property (readwrite) int typeIndex;
@property (readwrite) int scrollIndicator; //current page indicator for pagination
@property (readwrite) int submenuScrollIndicator;
@property (readwrite) int retAffId, retAffId_Cat;
@property (readwrite) int retGroupId, retGroupId_Cat;

@property (readwrite) int localSpecilas;

+ (id)sharedManager;
@end

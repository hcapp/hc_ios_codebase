//
//  ClaimBusinessResponse.h
//  HubCiti
//
//  Created by Nikitha on 8/23/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RetListClaimBusResponse.h"

@interface ClaimBusinessResponse : NSObject
@property(nonatomic,strong) NSString * responseCode;
@property(nonatomic,strong) NSString * responseText;
@property (nonatomic,strong) NSArray <RetListClaimBusResponse> * retDetailList;
@end

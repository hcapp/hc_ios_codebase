//
//  CategoriesViewController.h
//  Scansee
//
//  Created by Ajit on 2/21/13.
//
//

#import <UIKit/UIKit.h>
#import "PreferredCategoriesScreen.h"
#import "CommonUtility.h"
#import "AboutAndPrivacyScreen.h"
#import "CityExperienceViewController.h"
typedef enum webServicesCategory{
    GETCATFORGROUP,
    GETCATFORPARTNER,
    USERCAT,
    USERINFO,
    CITIEXPRETAILERS,
    PARTNERRETAILERS
}webServicesCategoryState;

@interface CategoryDO : NSObject{
    
    NSString *catId, *catName, *catImg;
}
@property (nonatomic, strong)NSString *catId, *catName, *catImg;

@end

@class CityExperienceViewController;
@interface CategoriesViewController : UIViewController<UITableViewDelegate,UITableViewDataSource, UISearchBarDelegate,CustomizedNavControllerDelegate>{

    IBOutlet UIButton *cityExpBtn, *filtersBtn, *settingsBtn, *aboutBtn;
    IBOutlet UITableView *categoriesTable;
    IBOutlet UISearchBar *searchBar1;
    CommonUtility *common;
    NSMutableArray *catIdArray,*catNameArray, *catImgArray;
    
    int retGroupIdForCat, retAffIdForCat, tagValue;
    CityExperienceViewController *cevc;
    webServicesCategoryState webRequestState;
    
    CategoryDO *categoryObject;
    NSMutableArray *catObjArray;
}
@property (readwrite) int retGroupIdForCat, retAffIdForCat, tagValue;
@property(nonatomic,strong) CityExperienceViewController *cityExp;
@end

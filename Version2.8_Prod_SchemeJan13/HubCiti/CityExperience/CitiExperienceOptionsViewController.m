//
//  CitiExperienceOptionsViewController.m
//  HubCiti
//
//  Created by Keerthi on 02/06/14.
//  Copyright (c) 2014 span infotech india ptv. ltd. All rights reserved.
//

#import "CitiExperienceOptionsViewController.h"
#import "CityExperienceViewController.h"
#import "RetailersListViewController.h"
#import "LocationDetailsViewController.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "CommonUtility.h"
#import "FilterRetailersList.h"

#import "RetailerAnnotation.h"
#import "CitiExpMapViewController.h"


@interface CitiExperienceOptionsViewController ()

@end

@implementation CitiExperienceOptionsViewController

@synthesize optionsTable;
@synthesize latitude,longitude,retailerName,retailAddress,retailerId,retailLocationId,retListId,distanceCE;
@synthesize longArray,latArray,arrOptions,retailAddressArr,retailerNameArr,retailerIdArray,retailLocationIdArray,distanceCEArray,retListIdArray,sortFilterObj,cityExpId;
NSMutableArray *annArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setAccessibilityLabel:@"CityOptions"];
    // Do any additional setup after loading the view from its nib.
    //customize back button
   
    self.navigationItem.hidesBackButton = YES;
   // //[back release];
    
    arrOptions = [[NSArray alloc]initWithObjects:@"Show Map",@"Sort",nil];
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
         optionsTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, ([arrOptions count]*70)) style:UITableViewStylePlain];
    }
    else
    {
    optionsTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, ([arrOptions count]*44)) style:UITableViewStylePlain];
    }
    optionsTable.dataSource=self;
    optionsTable.delegate=self;
    [optionsTable setBackgroundColor:[UIColor whiteColor]];
    [optionsTable setAccessibilityLabel:@"tableOptions"];
    [optionsTable setAccessibilityIdentifier:@"optionsTable"];
    self.title=@"Map/Sort";
    [self.view addSubview:optionsTable];
    
    [optionsTable reloadData];
//    [btn3 addTarget:self action:@selector(cityExperienceClicked:) forControlEvents:UIControlEventTouchUpInside];
//    btn3.tag = 3;
//    
//            switch ([SharedManager filtersCount])
//            {
//                case 0:
//                {
//                    [btn3 setBackgroundImage:[UIImage imageNamed:@"tab_account-up.png"] forState:UIControlStateNormal];
//                    [btn3 setBackgroundImage:[UIImage imageNamed:@"tab_account-down.png"] forState:UIControlStateHighlighted];
//                    [btn3 removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
//                    [btn3 addTarget:self action:@selector(request_GetUserInfo:) forControlEvents:UIControlEventTouchUpInside];
//                    
//                    break;
//                }
//                default:
//                {
//                    [btn3 setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_filters.png"] forState:UIControlStateNormal];
//                    [btn3 setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_filters.png"] forState:UIControlStateHighlighted];
//                    [btn3 removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
//                    [btn3 addTarget:self action:@selector(filtersClicked:) forControlEvents:UIControlEventTouchUpInside];
//                    
//                    break;
//                }
//            }
//    
//    [btn1 setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_find.png"] forState:UIControlStateNormal];
//    [btn1 setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_find.png"] forState:UIControlStateHighlighted];
//    [btn1 addTarget:self action:@selector(navigateToFindView) forControlEvents:UIControlEventTouchUpInside];
//    btn1.tag = 1;
//    
//    [btn2 setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_nearBy.png"] forState:UIControlStateNormal];
//    [btn2 setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_nearBy.png"] forState:UIControlStateHighlighted];
//    [btn2 addTarget:self action:@selector(navigateToWhatsNearBy) forControlEvents:UIControlEventTouchUpInside];
//    btn2.tag = 2;
//    
//    [btn4 setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_share.png"] forState:UIControlStateNormal];
//    [btn4 setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_share.png"] forState:UIControlStateHighlighted];
//    [btn4 addTarget:self action:@selector(shareClicked:) forControlEvents:UIControlEventTouchUpInside];
//    btn4.tag = 4;
}


-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}
-(void)allocateArrays{
    
    if(annArray)
    {
        [annArray removeAllObjects];
        annArray = nil;
    }
    annArray = [[NSMutableArray alloc] init];
}


#pragma mark navigation bar button actions
-(void)popBackToPreviousPage{
  
        [self.navigationController popViewControllerAnimated:NO];
    
}



#pragma mark tableView delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
       return [arrOptions count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        return 70;
    }
    else
    {
    return 44;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        static NSString *CellIdentifier = @"CellOptionsListView";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
            cell.backgroundColor =  [UIColor clearColor];
            
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        UILabel *lblOption ;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        lblOption= [[UILabel alloc]initWithFrame:CGRectMake(20, 12, SCREEN_WIDTH - 20, 40)];
        }
        else
        {
        lblOption= [[UILabel alloc]initWithFrame:CGRectMake(20, 12, SCREEN_WIDTH - 20, 20)];
        }
        [lblOption setText:[arrOptions objectAtIndex:indexPath.row]];
        [lblOption setTextColor:[UIColor blackColor]];
        [lblOption setBackgroundColor:[UIColor clearColor]];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
         [lblOption setFont:[UIFont boldSystemFontOfSize:22]];
        }
        else
        {
        [lblOption setFont:[UIFont boldSystemFontOfSize:16]];
        }
        lblOption.textColor = [UIColor colorWithRGBInt:0x112e72];
        [cell.contentView addSubview:lblOption];
        //[lblOption release];
        return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(indexPath.row==0 ){
        if ([latArray count]>0) {
     
        CitiExpMapViewController *citiExpMap=[[CitiExpMapViewController alloc]initWithNibName:@"CitiExpMapViewController" bundle:Nil];
        
        citiExpMap.latArray = [[NSMutableArray alloc]init];
        citiExpMap.longArray= [[NSMutableArray alloc]init];
        citiExpMap.retailerNameArr= [[NSMutableArray alloc]init];
        citiExpMap.retailerIdArray= [[NSMutableArray alloc]init];
        citiExpMap.retailAddressArr= [[NSMutableArray alloc]init];
        citiExpMap.retailLocationIdArray= [[NSMutableArray alloc]init];
        citiExpMap.retListIdArray= [[NSMutableArray alloc]init];
        citiExpMap.distanceCEArray= [[NSMutableArray alloc]init];
        for (int i=0; i<[latArray count]; i++) {
             [citiExpMap setValue:[latArray objectAtIndex:i] forKey:@"latitude"]; //[[NSString alloc]initWithString:[latArray objectAtIndex:i]];citiExpMap.latitude=
            [citiExpMap setValue:[longArray objectAtIndex:i] forKey:@"longitude"];
            [citiExpMap setValue:[retailerNameArr objectAtIndex:i] forKey:@"retailerName"];
            [citiExpMap setValue:[retailerIdArray objectAtIndex:i] forKey:@"retailerId"];
            [citiExpMap setValue:[retailAddressArr objectAtIndex:i] forKey:@"retailAddress"];
            [citiExpMap setValue:[retailLocationIdArray objectAtIndex:i] forKey:@"retailLocationId"];
            [citiExpMap setValue:[retListIdArray objectAtIndex:i] forKey:@"retListId"];
            [citiExpMap setValue:[distanceCEArray objectAtIndex:i] forKey:@"distanceCE"];

            
            
            [citiExpMap.latArray addObject: [citiExpMap valueForKey:@"latitude"]];
            [citiExpMap.longArray addObject: [citiExpMap valueForKey:@"longitude"]];
            [citiExpMap.retailerNameArr addObject: [citiExpMap valueForKey:@"retailerName"]];
            [citiExpMap.retailerIdArray addObject: [citiExpMap valueForKey:@"retailerId"]];
            [citiExpMap.retailAddressArr addObject: [citiExpMap valueForKey:@"retailAddress"]];
            [citiExpMap.retailLocationIdArray addObject: [citiExpMap valueForKey:@"retailLocationId"]];
            [citiExpMap.retListIdArray addObject: [citiExpMap valueForKey:@"retListId"]];
            [citiExpMap.distanceCEArray addObject: [citiExpMap valueForKey:@"distanceCE"]];
        }
        
        [self.navigationController pushViewController:citiExpMap animated:NO];
        //[citiExpMap release];

    }
        else
        {
            [UtilityManager showAlert:nil msg:NSLocalizedString(@"Retailers are not available to plot on map",@"Retailers are not available to plot on map")];
        }

    }
    else{
//        CitiExpSortViewController *citiExpSort=[[CitiExpSortViewController alloc]initWithNibName:@"CitiExpSortViewController" bundle:Nil];
//        if(iSwipeViewController)
//            [iSwipeViewController release];
        
        iSwipeViewController = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
        iSwipeViewController.module = @"CitiEXP";//Find All , Find Single , Events, CitiEXP
        NSArray *viewControllers = [self.navigationController viewControllers];
        long vIndex=[viewControllers count]-2;
        iSwipeViewController.delegate = [self.navigationController.viewControllers objectAtIndex:vIndex];
        iSwipeViewController.sortFilObj = [[SortAndFilter alloc]initWithObject:sortFilterObj];
        iSwipeViewController.cityExpId = cityExpId;
        [defaults setInteger:100 forKey:@"CityExpTableTag"];
        [self.navigationController pushViewController:iSwipeViewController animated:YES];
    }
       [optionsTable deselectRowAtIndexPath:[optionsTable indexPathForSelectedRow]  animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  ClaimBusinessInfoResponse.h
//  HubCiti
//
//  Created by Nikitha on 10/4/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomerInfoListResponse.h"

@interface ClaimBusinessInfoResponse : NSObject
@property(nonatomic,strong) NSString * responseCode;
@property(nonatomic,strong) NSString * responseText;
@property (nonatomic,strong) NSArray <CustomerInfoListResponse> * customerInfoList;
@end

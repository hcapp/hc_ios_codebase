//
//  CurrentSpecialsViewController.h
//  HubCiti
//
//  Created by Kitty on 27/04/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FundraiserListViewController;
@interface CurrentSpecialsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,CustomizedNavControllerDelegate>{
    
    IBOutlet UITableView *tblCurrentSpecial;
   
    UIColor *cellColor,*fontColor;
    NSMutableArray *hotDealsArray;
}
@property(nonatomic,strong) FundraiserListViewController* iFundraiserListViewController;
@end

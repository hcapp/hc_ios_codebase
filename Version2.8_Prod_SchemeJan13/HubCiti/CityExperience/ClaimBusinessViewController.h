//
//  ClaimBusinessViewController.h
//  HubCiti
//
//  Created by Nikitha on 8/18/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerInfoListResponse.h"

@interface ClaimBusinessViewController : UIViewController<UITextFieldDelegate,CustomizedNavControllerDelegate,UIScrollViewDelegate>
{
 
    IBOutlet UIScrollView *scrollvieww;
    IBOutlet UILabel *postalLabel;
    IBOutlet UIButton *submitButton;
    IBOutlet UILabel *phoneNumberLabel;
    IBOutlet UILabel *countryLabel;
    IBOutlet UILabel *stateLabel;
    IBOutlet UILabel *cityLabel;
    IBOutlet UILabel *addressLabel;
    IBOutlet UILabel *websiteLabel;
    IBOutlet UILabel *typeOfBusinessLabel;
    IBOutlet UILabel *businessnameLabel;
    IBOutlet UITextField *businessNameField;
    IBOutlet UITextField *typeOfBusinessField;
    IBOutlet UITextField *websiteField;
    IBOutlet UILabel *phoneNumLabel;
    IBOutlet UILabel *areaCodeLabel;
    IBOutlet UITextField *addressLine1Field;
    IBOutlet UITextField *addressLine2Field;
    IBOutlet UITextField *cityField;
    IBOutlet UITextField *stateField;
    IBOutlet UITextField *postalCodeFiled;
    IBOutlet UITextField *countryField;
    IBOutlet UITextField *areaCodeField;
    IBOutlet UITextField *phoneNumberField;
    IBOutlet UIButton *submitBtn;
    CGFloat animatedDistance;
    IBOutlet NSLayoutConstraint *address1Label;
    IBOutlet UITextField *mailingAddress1TxtField;
    IBOutlet UITextField *keywordTextField;
    IBOutlet UILabel *mailingAddress1Label;
    IBOutlet UITextField *mailingAddress2TxtField;
    IBOutlet UILabel *mailingAddress2Label;
    IBOutlet UITextField *mailingCityTxtField;
    IBOutlet UILabel *mailingCityLabel;
    IBOutlet UITextField *mailingStateTxtField;
    IBOutlet UILabel *mailingStateLabel;
    IBOutlet UITextField *mailingPostalTxtField;
    IBOutlet UILabel *mailingPostalLabel;
    IBOutlet UITextField *mailingCountryTxtField;
    IBOutlet UILabel *mailingCountryLabel;
    IBOutlet UILabel *mailingPhNumLabel;
    IBOutlet UITextField *mailingPhNumTxtField;
    IBOutlet UILabel *contactInfoLabel;
    IBOutlet UILabel *contactNameLabel;
    IBOutlet UILabel *emailAddressLabel;
    IBOutlet UILabel *streetAddressLabel;

}
@property (strong, nonatomic) IBOutlet UIButton *dropDownButton;

@property(nonatomic,strong) NSString * retailerLocationID;
@property(nonatomic,strong) NSString * msgInPopup;
@property(nonatomic,strong) NSArray * list_OfAppsiteName;
@property(nonatomic,strong) NSArray * list_OfAppsiteURL;
@property(nonatomic,strong) NSString * claimEmailID;
@property(nonatomic,strong) NSString * claimRetailerAddress;
@property(nonatomic,strong) NSString * claimRetailerName;
@property (strong, nonatomic) IBOutlet UIScrollView *claimFormScrollView;
@property (strong, nonatomic) IBOutlet UIView *claimFormView;
@property(strong,nonatomic) CustomerInfoListResponse * customer_InfoData;
@property (strong, nonatomic) IBOutlet UIWebView *bannerWebView;
@property (strong, nonatomic) IBOutlet UITextField *contactInfoField;
@property (strong, nonatomic) IBOutlet UITextField *emailAddressField;
@property (strong, nonatomic) IBOutlet UIButton *submitBtnOutlet;
- (IBAction)submitBtnPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewBannerOutlet;

@end

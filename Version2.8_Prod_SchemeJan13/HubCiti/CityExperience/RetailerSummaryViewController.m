//
//  RetailerSummaryViewController.m
//  Scansee
//
//  Created by ajit on 5/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RetailerSummaryViewController.h"
//#import "FindLocationDetailsViewController.h"
#import "WebBrowserViewController.h"
#import "MainMenuViewController.h"
#import "FindViewController.h"
#import "FindNearByRetailerMap.h"
#import "CurrentSpecials.h"
#import <CoreLocation/CoreLocation.h>
#import "CLLocation (Strings).h"
#import "EventsListViewController.h"
#import "AppDelegate.h"
#import "PreferredCategoriesScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "FindOptionsViewController.h"
#import "FundraiserListViewController.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "CurrentSpecialsViewController.h"
#import "SpecialOffersViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "ClaimBusinessViewController.h"
#import "ClaimBusinessInfoResponse.h"
#import "ClaimDropDownViewController.h"

@interface RetailerSummaryViewController ()
{
    UIButton * claimBtn;
    BOOL claimDone;
    UILabel *claimBtnLabel;
    NSString * requestURLForCategory;
    
}
@property(nonatomic,strong) ClaimBusinessInfoResponse * claimBusInfoResponse ;
@property(nonatomic,strong)  CustomerInfoListResponse * custInfoResponse;
@end

@implementation RetailerSummaryViewController
@synthesize retailerName,table_RetailerSummary,anyVC,emailSendingVC,userMailID,array_ClaimBusInfoData,custInfoResponse,mainCategoryNameArray;

//For 1.2.2
@synthesize distanceFromPreviousScreen;
@synthesize serviceUrl, xmlString,retailerAddress,claimBusInfoResponse;

#pragma mark - view lifecycle methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    
    //For 1.2.2
   // distanceFromPreviousScreen = [NSString stringWithFormat:@""];
    return self;
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    [self parse_BusinessInfoData:responseObject];
    
    
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation

{
    
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

- (BOOL)shouldAutorotate{
    
    return NO;
    
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    
    return UIInterfaceOrientationPortrait;
    
}



- (UIInterfaceOrientationMask)supportedInterfaceOrientations

{
    
    return UIInterfaceOrientationMaskPortrait;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    
    // Create down menu button
    UIImageView *homeLabel ;
    
    
    // Create up menu button
    homeLabel = [self createHomeButtonView];
    DWBubbleMenuButton *upMenuView;
    if(IPAD)
    {
        upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
                                                                          self.view.frame.size.height - homeLabel.frame.size.height - 60.f,
                                                                          homeLabel.frame.size.width,
                                                                          homeLabel.frame.size.height)
                                            expansionDirection:DirectionUp];
        
    }
    else
    {
        upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
                                                                          self.view.frame.size.height - homeLabel.frame.size.height - 40.f,
                                                                          homeLabel.frame.size.width,
                                                                          homeLabel.frame.size.height)
                                            expansionDirection:DirectionUp];
    }
    upMenuView.homeButtonView = homeLabel;
    
    upMenuView.delegate = self;
    
    [upMenuView addButtons:[self createDemoButtonArray]];
    
    [self.view addSubview:upMenuView];
    
    upMenuView.onLogOutPressed =^(){
        
        [self logOutPressed];
    } ;
    
    if([defaults boolForKey:@"isComingFromCliam"])
    {
        
        
        [self request_ForRetSummary];
        
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [defaults setBool:NO forKey:@"isComingFromCliam"];
    // For 1.2.2
    DLog(@"Distance - %@", distanceFromPreviousScreen);
    
    array_ClaimBusInfoData = [[NSMutableArray alloc]init];
    [self setUpUI];
    claimBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    if(IPAD)
    {
        
        claimBtn.frame = CGRectMake(0,self.view.frame.size.height*2 - VARIABLE_HEIGHT(20), SCREEN_WIDTH, VARIABLE_HEIGHT(54));
    }
    else
    {
        
        claimBtn.frame = CGRectMake(0,self.view.frame.size.height-VARIABLE_HEIGHT(12), SCREEN_WIDTH, VARIABLE_HEIGHT(52));
    }
    claimBtn.backgroundColor = [UIColor convertToHexString:@"#D5D5D5"];
    [claimBtn addTarget:self action:@selector(claimBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    claimBtn.tag = 1;
    [self.view addSubview:claimBtn];
    [self.view bringSubviewToFront:claimBtn];
    claimBtnLabel = [[UILabel alloc]init];
    claimBtnLabel.frame = CGRectMake(0,0,SCREEN_WIDTH - VARIABLE_HEIGHT(10), (IPAD)? VARIABLE_HEIGHT(40): VARIABLE_HEIGHT(52));
    //claimBtnLabel.text = [TBXML textForElement:claimTxtMsgElement];
    claimBtnLabel.numberOfLines=0;
    claimBtnLabel.tag =2;
    claimBtnLabel.lineBreakMode = NSLineBreakByWordWrapping;
    claimBtnLabel.textAlignment = NSTextAlignmentCenter;
    claimBtnLabel.textColor=[UIColor blackColor];
    claimBtnLabel.backgroundColor = [UIColor clearColor];
    claimBtnLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    claimBtnLabel.font = [UIFont systemFontOfSize:(IPAD)?15.0f:11.0f];
    //claimBtnLabel.backgroundColor = [UIColor convertToHexString:@"#D5D5D5"];
    [claimBtn addSubview:claimBtnLabel];
    // displaying share button
    
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    shareButton.frame=CGRectMake(0, 0, 20, 18);
    
    
    
    //customize back button
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBar.hidden = NO;
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30,30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    // //[mainPage release];
    
    
    
    //For displaying banner Ad for ScanSee Retailers
    float screenHight = ([UIScreen mainScreen].bounds.size.height);
    float screenWidth = ([UIScreen mainScreen].bounds.size.width);
    //    float navHeight = self.navigationController.navigationBar.frame.size.height;
    bannerAdImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHight+STATUSBAR_HEIGHT)];
    bannerAdImageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    bannerAdImageView.contentMode = UIViewContentModeScaleAspectFit;
    //    bannerAdImageView.clipsToBounds = YES;
    [bannerAdImageView setBackgroundColor:[UIColor whiteColor]];
    
    modalVC = [[UIViewController alloc] init];
    
    [self parse_RetSummary:[defaults  objectForKey:KEY_RESPONSEXML]];
    
    if ([bannerAdPath length] && ![bannerAdPath isEqualToString:@"N/A"]) {
        
        bannerAdPath = [bannerAdPath stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
        NSURL *bannerAdUrl = [NSURL URLWithString:bannerAdPath];
        [defaults setURL:bannerAdUrl forKey:@"AdUrl"];
        // NSData *imgData = [NSData dataWithContentsOfURL:bannerAdUrl];
        //UIImage *bannerAdImage = [UIImage imageWithData:imgData];
        [bannerAdImageView loadImage:bannerAdPath];
        
        [modalVC.view addSubview:bannerAdImageView];
        
        if ([[modalVC.view subviews] count])
        {
            [self presentViewController:modalVC animated:NO completion:nil];
            [self performSelector:@selector(removeBannerAd:) withObject:nil afterDelay:3.0];
        }
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
    [self setUpTableFrame];
    });
    // Do any additional setup after loading the view from its nib.
}



- (UIImageView *)createHomeButtonView { ///
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
    label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}
-(void) setUpUI
{
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        retailerName = [[UILabel alloc]initWithFrame:CGRectMake(50.0, 0.0, 200.0, 50.0)];
    }
    else
    {
        retailerName = [[UILabel alloc]initWithFrame:CGRectMake(50.0, 0.0, 400.0, 80.0)];
    }
    
    
    retailerName.font = [UIFont boldSystemFontOfSize:22.0];
    retailerName.textAlignment = NSTextAlignmentCenter;
    [retailerName setBackgroundColor:[UIColor clearColor]];
    retailerName.shadowColor = [UIColor grayColor];
    retailerName.shadowOffset = CGSizeMake(1,1);
    retailerName.lineBreakMode = NSLineBreakByWordWrapping;
    retailerName.numberOfLines = 2;
    
    bannerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        bannerBtn.frame = CGRectMake(0.0, 0.0, 320.0 /*280.0*/, 50.0);
    }
    else
    {
        bannerBtn.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, 90.0);
    }
    bannerBtn.backgroundColor = [UIColor clearColor];
    
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        table_RetailerSummary = [[UITableView alloc] initWithFrame:CGRectMake(0.0, retailerName.frame.origin.y+retailerName.frame.size.height+2, SCREEN_WIDTH, 380.0) style:UITableViewStylePlain];
    }
    else
    {
        table_RetailerSummary = [[UITableView alloc] initWithFrame:CGRectMake(0.0, retailerName.frame.origin.y+retailerName.frame.size.height+10, SCREEN_WIDTH,SCREEN_HEIGHT+200) style:UITableViewStylePlain];
    }
    //table_RetailerSummary.backgroundColor = [UIColor yellowColor];
    table_RetailerSummary.delegate = self;
    table_RetailerSummary.dataSource = self;
    [self.view addSubview:table_RetailerSummary];
    
    
}

-(void) setUpTableFrame
{
    CGRect tableFrame = self.table_RetailerSummary.frame;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        if ([titleArray count]<6) {
            CGFloat height = 90.0;//row height
            height *= titleArray.count;
            tableFrame.size.height = height;
        }
        else
        {
            if(!claimDone)
            {
            table_RetailerSummary.contentInset = UIEdgeInsetsMake(0, 0, 95, 0);
            }
            tableFrame.size.height=SCREEN_HEIGHT-(retailerName.frame.origin.y+retailerName.frame.size.height+10)-44;
        }
    }
    
    else if ([titleArray count]< 6){
        CGFloat height = 60.0;//row height
        height *= titleArray.count;
        tableFrame.size.height = height;
    }
    
    else{
        if(!claimDone)
        {
        table_RetailerSummary.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
        }
        if (IS_IPHONE5)
            tableFrame.size.height = 360+88;
        else
            tableFrame.size.height = 360;
        
    }
    self.table_RetailerSummary.frame = tableFrame;
    [self requestToGetPreferredLocations];
    [table_RetailerSummary reloadData];
    
}
- (NSArray *)createDemoButtonArray {
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    
    int i = 0;
    for (NSString *title in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:title] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        
        button.clipsToBounds = YES;
        button.tag = i++;
        
        [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
}
-(void) claimBtnClicked : (id) sender
{
    
    
    if (!claimDone)
    {
        if (userIdentifier==TRUE) {
            [NewsUtility signupPopUp:self];
        }
        else
        {
            ClaimBusinessViewController *claimBusVC = [[ClaimBusinessViewController alloc] initWithNibName:@"ClaimBusinessViewController" bundle:[NSBundle mainBundle]];
            claimBusVC.retailerLocationID = retLocId;
            claimBusVC.claimEmailID = userMailID;
            claimBusVC.claimRetailerName = retailerName.text;
            claimBusVC.claimRetailerAddress = retailerAddress;
            [self request_businessInfoData];
            claimBusVC.customer_InfoData = custInfoResponse;
            NSLog(@"email %@ - %@", userMailID, claimBusVC.claimEmailID);
            [self.navigationController pushViewController:claimBusVC animated:NO];
        }
    }
    
}
-(void) request_businessInfoData
{
    [HubCitiAppDelegate showActivityIndicator];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:retLocId forKey:@"retLocationId"];
    [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
//    [parameters setValue:@"6113" forKey:@"retLocationId"];
//    [parameters setValue:@"11" forKey:@"userId"];
//    [parameters setValue:@"10" forKey:@"hubCitiId"];
    
    
    DLog(@"parameter: %@",parameters);
    
//    NSString *urlString = [NSString stringWithFormat:@"http://10.10.220.231:9990/HubCiti2.9/thislocation/getclaimyourbusiness"];
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getclaimyourbusiness",BASE_URL];
    DLog(@"Url: %@",urlString);
    NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    [HubCitiAppDelegate removeActivityIndicator];
    NSLog(@"response %@",response);
    [self parse_BusinessInfoData:responseData];
    
}
-(void)parse_BusinessInfoData :(id) response
{
    if (response == nil)
        return;
    if (claimBusInfoResponse == nil) {
        claimBusInfoResponse = [[ClaimBusinessInfoResponse alloc] init];
    }
    [claimBusInfoResponse setValuesForKeysWithDictionary:response];
    NSLog(@"response = %@", claimBusInfoResponse);
    if ([claimBusInfoResponse.responseCode isEqualToString:@"10000"] )
    {
        if(claimBusInfoResponse.customerInfoList != nil)
        {
            for (int i = 0; i< claimBusInfoResponse.customerInfoList.count; i++)
            {
                custInfoResponse = [[CustomerInfoListResponse alloc]init];
                NSDictionary * dict_custInfoResponse = claimBusInfoResponse.customerInfoList[i];
                [custInfoResponse setValuesForKeysWithDictionary:dict_custInfoResponse];
            }
        }
    }
    else
    {
        [UtilityManager showAlert:nil msg:claimBusInfoResponse.responseText];
        
    }
 
}
- (void)test:(UIButton *)sender {
    
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
    [self shareClicked];
    
    switch (sender.tag) {
        case 0:{//facebook
            anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
            anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"APPSITE_SHARE"];
            //[self.view addSubview:anyVC.view];
            [UIView transitionWithView:self.view duration:0.5
                               options:UIViewAnimationOptionTransitionCurlDown
                            animations:^ { [self.view addSubview:anyVC.view]; }
                            completion:nil];
        }
            break;
        case 1:{//twitter
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    if (result == SLComposeViewControllerResultCancelled) {
                        DLog(@"Twitter Result: canceled");
                    } else {
                        DLog(@"Twitter Result: sent");
                        NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><retId>%@</retId><retLocId>%@</retLocId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_RETAILERID],[defaults valueForKey:@"retailLocationID"]];
                        
                        [NSThread detachNewThreadSelector:@selector(userTrack:) toTarget:self withObject:userXmlStr];
                    }
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        
                        [controller.view removeFromSuperview];
                    } else
                        [controller dismissViewControllerAnimated:YES completion:Nil];
                };
                controller.completionHandler =myBlock;
                
                //Adding the Text to the facebook post value from iOS
                [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                
                //Adding the URL to the facebook post value from iOS
                
                //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                
                //Adding the Image to the facebook post value from iOS
                //                [[defaults valueForKey:KEY_PRODUCTIMGPATH] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                
                [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[defaults valueForKey:KEY_PRODUCTIMGPATH] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]]]]];
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    [self.view addSubview:controller.view];
                    [self.view bringSubviewToFront:controller.view];
                }else
                    [self presentViewController:controller animated:YES completion:Nil];
                
                
            }
            else {
                [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
                }
        }
            
            
            break;
        case 2:{//text
            
            if ([self hasCellularCoverage])
            {
                if ([MFMessageComposeViewController canSendText]) {
                    
                    MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
                    [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    smsComposerController.messageComposeDelegate = self;
                    [self presentViewController:smsComposerController animated:YES completion:nil];
                }
            }
            else
            {
                [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
            }
            
            
        }
            break;
        case 3:{//email
            
            // [defaults setValue:APPSITE_SHARE forKey:KEY_SHARE_TYPE];
            emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
            emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"APPSITE_SHARE"];
            __typeof(self) __weak  obj = self;
            emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                
                [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
            } ;
            
            [emailSendingVC loadMail];
        }
            break;
            //case 4:
            // [actionSheet dismissWithClickedButtonIndex:4 animated:YES];
            // break;
        default:
            [defaults setBool:NO forKey:BottomButton];
            break;
    }
    
}


- (UIButton *)createButtonWithName:(NSString *)imageName {
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button sizeToFit];
    
    [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}



- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}



- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView{
    
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            
            _currentView.alpha =  1;
            
        }
        
    }
}

- (void)removeBannerAd:(id)indexPath {
    [bannerAdImageView removeFromSuperview];
    [[self presentedViewController] dismissViewControllerAnimated:NO completion:nil];
    
}

#pragma mark sender user tracking
-(void)userTrack:(NSString*)xmlStr
{
    
    NSMutableString *urlStr = [[NSMutableString alloc]init];
    [urlStr appendFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    DLog(@"%@",urlStr);
    NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:xmlStr]];
    DLog(@"%@",response);
    //[xmlStr release];
}

#pragma mark action sheet methods
-(void)shareClicked
{
    iWebRequestState = APPSITE_SHARE;
    
    //    [defaults setValue:retId forKey:KEY_RETAILERID];
    //    [defaults setValue:retLocId forKey:@"retailLocationID"];
    
    
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    
    
    [reqStr appendFormat:@"<ShareProductInfo><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [reqStr appendFormat:@"<retailerId>%@</retailerId><retailerLocationId>%@</retailerLocationId>",[defaults valueForKey:KEY_RETAILERID],[defaults valueForKey:@"retailLocationID"]];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/shareappsite",BASE_URL];
    DLog(@"%@",urlString);
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    
    [self responseData:response];
    
    
}


-(void)shareDetails{
    
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        [self shareClicked];
        
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        // [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
    }
}
-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Appsite Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    //    if(IPAD){
    //        actionSheet.popoverPresentationController.sourceView = btnCalendar;
    //        actionSheet.popoverPresentationController.sourceRect =  btnCalendar.bounds;
    //    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"APPSITE_SHARE"];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
                NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><retId>%@</retId><retLocId>%@</retLocId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_RETAILERID],[defaults valueForKey:@"retailLocationID"]];
                
                [NSThread detachNewThreadSelector:@selector(userTrack:) toTarget:self withObject:userXmlStr];
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        //                [[defaults valueForKey:KEY_PRODUCTIMGPATH] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[defaults valueForKey:KEY_PRODUCTIMGPATH] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
        
        
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    
    // [defaults setValue:APPSITE_SHARE forKey:KEY_SHARE_TYPE];
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"APPSITE_SHARE"];
    __typeof(self) __weak  obj = self;
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
}


-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
            NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><retId>%@</retId><retLocId>%@</retLocId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_RETAILERID],[defaults valueForKey:@"retailLocationID"]];
            
            [NSThread detachNewThreadSelector:@selector(userTrack:) toTarget:self withObject:userXmlStr];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
#pragma mark navigation bar button actions
-(void)popBackToPreviousPage{
    [linkID removeLastObject];
    
    [self.navigationController popViewControllerAnimated:NO];
}



-(void)returnToMainPage:(id)sender{
    
    showMapFlag=FALSE;
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

#pragma mark request methods
//get user favorite categories
//-(void)request_GetFavCategories{
//
//    iWebRequestState = GETFAVCATEGORIES;
//    if([Network currentReachabilityStatus]==0)
//    {
//        common = [[CommonUtility alloc]init];
//        [common showAlert];
//    }
//    else {
//
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqStr = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//
//        [ConnectionManager establishConnectionFor:reqStr base:urlString withDelegate:self];
//    }
//}

-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        common = [[CommonUtility alloc]init];
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        requestURLForCategory = urlString;
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
        [HubCitiAppDelegate removeActivityIndicator];
    }
}

#pragma mark product share parse
-(void)parseProductShare:(NSString*)response
{
    /*
     <RetailersDetails>
     <responseCode>10000</responseCode>
     <responseText>Success</responseText>
     <retImage>N/A</retImage>
     <shareText>I found this AppSite™ @ScanSee and thought you might be
     interested:</shareText>
     <city>MARBLE FALLS</city>
     <state>TX</state>
     <postalCode>78654</postalCode>
     <qrUrl>http://10.11.200.223:8080/SSQR/qr/2000.htm?key1=1155&key2=92955&key3=3
     </qrUrl>
     <retailerURL>http://www.ScanSee.com</retailerURL>
     <retailerId>1155</retailerId>
     <RetailLocationID>92955</RetailLocationID>
     <retName>madhu_ret</retName>
     <retailerAddress>madhu_ret</retailerAddress>
     </RetailersDetails>*/
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        //        TBXMLElement *retailerDetails = [TBXML childElementNamed:@"RetailersDetails" parentElement:tbXml.rootXMLElement];
        
        
        TBXMLElement *shareText=[TBXML childElementNamed:@"shareText" parentElement:tbXml.rootXMLElement];
        TBXMLElement *emailURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *imagePath = [TBXML childElementNamed:@"retImage" parentElement:tbXml.rootXMLElement];
        
        if (emailURL!=nil) {
            [defaults setValue:[TBXML textForElement:emailURL] forKey:KEY_HYPERLINK];
            NSLog(@"The link to be shared is %@",KEY_HYPERLINK);
        }
        
        if (shareText!=nil) {
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [defaults setObject:shareString forKey:KEY_SHAREMSG];
            NSLog(@"The share message value is %@",[defaults valueForKey:KEY_SHAREMSG]);
        }
        
        if (imagePath!=nil) {
            [defaults setObject:[TBXML textForElement:imagePath] forKey:KEY_PRODUCTIMGPATH];
            NSLog(@"The product image path is %@",KEY_PRODUCTIMGPATH);
        }
        
        
        
        
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            return;
        }
        
    }
}


#pragma mark parse methods

-(void)responseData: (NSString*)response{
    
    switch (iWebRequestState) {
        case GETFAVLOCATIONS:
            NSLog(@"responseData %@",response);
            [self parse_GetFavCategories:response];
            break;
        case APPSITE_SHARE:
            [self parseProductShare:response];
            break;
            
        default:
            break;
    }
    
}
//parse user favorite categories
-(void)parse_GetFavCategories:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    mainCategoryNameArray=[[NSMutableArray alloc]init];
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *MainCategoryDetailElement = [TBXML childElementNamed:@"MainCategory" parentElement:tbxml.rootXMLElement];
    while (MainCategoryDetailElement !=nil)
    {
        TBXMLElement *mainCategoryNameElement = [TBXML childElementNamed:@"parCatName" parentElement:MainCategoryDetailElement];
        [mainCategoryNameArray addObject:[TBXML textForElement:mainCategoryNameElement]];
         MainCategoryDetailElement = [TBXML nextSiblingNamed:@"MainCategory" searchFromElement:MainCategoryDetailElement];
    }
    [defaults setValue:mainCategoryNameArray forKey:@"categoryArray"];
    NSLog(@"elements %@",mainCategoryNameArray);
    //[settings release];
}

//parse retailer summary
-(void) parse_RetSummary:(NSString*)response {
    [HubCitiAppDelegate removeActivityIndicator];
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbXml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        TBXMLElement *eventExistElement = [TBXML childElementNamed:@"eventExist" parentElement:tbXml.rootXMLElement];
        eventExist = [[NSString alloc]initWithFormat:@"%@",[TBXML textForElement:eventExistElement]];
        
        TBXMLElement *fundExistElement = [TBXML childElementNamed:@"fundExist" parentElement:tbXml.rootXMLElement];
        fundExist = [[NSString alloc]initWithFormat:@"%@",[TBXML textForElement:fundExistElement]];
        
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *userMailIdElement = [TBXML childElementNamed:@"userMailId" parentElement:tbXml.rootXMLElement];
        TBXMLElement *claimExistElement = [TBXML childElementNamed:@"claimExist" parentElement:tbXml.rootXMLElement];
        TBXMLElement *claimTxtMsgElement = [TBXML childElementNamed:@"claimTxtMsg" parentElement:tbXml.rootXMLElement];
        
        
        
        if (RetailerDetailElement!=nil){
            TBXMLElement *retailerIdElement = [TBXML childElementNamed:@"retailerId" parentElement:RetailerDetailElement];
            TBXMLElement *retailerNameElement = [TBXML childElementNamed:@"retailerName" parentElement:RetailerDetailElement];
            TBXMLElement *retailLocationIdElement = [TBXML childElementNamed:@"retailLocationId" parentElement:RetailerDetailElement];
            TBXMLElement *retLatitudeElement = [TBXML childElementNamed:@"retLatitude" parentElement:RetailerDetailElement];
            TBXMLElement *retLongitudeElement = [TBXML childElementNamed:@"retLongitude" parentElement:RetailerDetailElement];
            TBXMLElement *ribbonAdURLElement = [TBXML childElementNamed:@"ribbonAdURL" parentElement:RetailerDetailElement];
            TBXMLElement *retailerAddressElement = [TBXML childElementNamed:@"retaileraddress1" parentElement:RetailerDetailElement];
            TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
            TBXMLElement *bannerAdImagePathElement = [TBXML childElementNamed:@"bannerAdImagePath" parentElement:RetailerDetailElement];
            TBXMLElement *ribbonAdImagePathElement = [TBXML childElementNamed:@"ribbonAdImagePath" parentElement:RetailerDetailElement];
            TBXMLElement *saleFlagElement = [TBXML childElementNamed:@"saleFlag" parentElement:RetailerDetailElement];
            TBXMLElement *retailerURLElement = [TBXML childElementNamed:@"retailerURL" parentElement:RetailerDetailElement];
            TBXMLElement *contactPhoneElement = [TBXML childElementNamed:@"contactPhone" parentElement:RetailerDetailElement];
            TBXMLElement *retDetIdElement = [TBXML childElementNamed:@"retDetId" parentElement:RetailerDetailElement];
         
            NSString * convertedClaimStr;
    
            
            
            convertedClaimStr = [[TBXML textForElement:claimTxtMsgElement] stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
            NSInteger length = [[convertedClaimStr componentsSeparatedByCharactersInSet:
                                 [NSCharacterSet newlineCharacterSet]] count];
            
            if(length >= 3){
                claimBtnLabel.frame = CGRectMake(0,0,SCREEN_WIDTH - VARIABLE_HEIGHT(10), (IPAD)? VARIABLE_HEIGHT(40): VARIABLE_HEIGHT(50));
            }
            else{
                claimBtnLabel.frame = CGRectMake(0,0,SCREEN_WIDTH - VARIABLE_HEIGHT(10), (IPAD)? VARIABLE_HEIGHT(40): VARIABLE_HEIGHT(52));
            }

            if([defaults boolForKey:@"isComingFromCliam"]){
                [defaults setBool:NO forKey:@"isComingFromCliam"];
                                claimBtnLabel.text = convertedClaimStr;
                if([[TBXML textForElement:claimExistElement] isEqualToString:@"1"])
                {
                    claimDone = TRUE;
                    claimBtn.hidden =YES;
                }
                else
                {
                    claimDone = FALSE;
                    //claimBtn.userInteractionEnabled = YES;
                }
                return;
                
            }
            
            
            
            
            titleArray = [[NSMutableArray alloc]init];
            subtitleArray = [[NSMutableArray alloc]init];
            indexArray = [[NSMutableArray alloc]init];
            imageUrlArray = [[NSMutableArray alloc]init];
            
            if (retDetIdElement!=nil){
                retDetId = [[TBXML textForElement:retDetIdElement]copy];
            }
            if (userMailIdElement != nil )
            {
                userMailID = [TBXML textForElement:userMailIdElement];
            }
            if (retailerIdElement!=nil) {
                retId = [[NSString alloc]initWithFormat:@"%@",[TBXML textForElement:retailerIdElement]];
                [defaults setValue:retId forKey:KEY_RETAILERID];
            }
            
            
            if (retailerNameElement!=nil) {
                
                retailerName.text = [TBXML textForElement:retailerNameElement];
                
                UILabel *lineLabel ;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0, retailerName.frame.origin.y+retailerName.frame.size.height, 320.0, 1.0)];
                }
                else
                {
                    lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0, retailerName.frame.origin.y+retailerName.frame.size.height + 10, SCREEN_WIDTH, 1.0)];
                }
                lineLabel.backgroundColor = [UIColor blackColor];
                [self.view addSubview:lineLabel];
                //  [lineLabel release];
                
                
                if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
                    table_RetailerSummary.frame = CGRectMake(0.0, lineLabel.frame.origin.y+1, SCREEN_WIDTH, SCREEN_HEIGHT-(retailerName.frame.origin.y+retailerName.frame.size.height+10)-44);
                }
                else if (IS_IPHONE5)
                    table_RetailerSummary.frame = CGRectMake(0.0, lineLabel.frame.origin.y+1, 320.0, 397.0+88);
                else
                    table_RetailerSummary.frame = CGRectMake(0.0, lineLabel.frame.origin.y+1, 320.0, 397.0);
                // [table_RetailerSummary reloadData];
                
                //                UIView *viewToRemove = [self.view viewWithTag:1];
                //                [viewToRemove removeFromSuperview];
                //
                //                UIView *viewToRemove1 = [self.view viewWithTag:2];
                //                [viewToRemove1 removeFromSuperview];
                
                
                
                claimBtnLabel.text = convertedClaimStr;
                if([[TBXML textForElement:claimExistElement] isEqualToString:@"1"])
                {
                    claimDone = TRUE;
                    claimBtn.hidden =YES;
                    //  claimBtn.userInteractionEnabled = NO;
                    //                    claimBtnLabel.backgroundColor = [UIColor redColor];
                    //                    claimBtn.backgroundColor = [UIColor redColor];
                    //claimBtn.enabled = NO;
                }
                else
                {
                    claimDone = FALSE;
                    // claimBtn.userInteractionEnabled = YES;
                }
                
                
                //set the page title as retailer name
                UILabel *titleLabel ;
                
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 320, 40)];
                    titleLabel.font = [UIFont boldSystemFontOfSize:16];
                }
                else
                {
                    titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, SCREEN_WIDTH, 70)];
                    titleLabel.font = [UIFont boldSystemFontOfSize:22];
                }
                
                CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
                [cus setTitle:[TBXML textForElement:retailerNameElement] forView:self withHambergur:NO];
               
                //[titleLabel release];
                
            }
            
            
            if (ribbonAdImagePathElement!=nil && ![[TBXML textForElement:ribbonAdImagePathElement]isEqualToString:@"N/A"]){
                
                [defaults  setObject:[TBXML textForElement:ribbonAdImagePathElement]
                              forKey:@"ribbonAdImagePath"];
                
                SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:bannerBtn.frame];
                asyncImageView.backgroundColor = [UIColor clearColor];
                [asyncImageView loadImage:[TBXML textForElement:ribbonAdImagePathElement]];
                asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
                
                [bannerBtn addSubview:asyncImageView];
                asyncImageView.userInteractionEnabled = NO;
                bannerBtn.userInteractionEnabled=YES;
                [self.view addSubview:bannerBtn];
                
                if (ribbonAdURLElement!=nil && ![[TBXML textForElement:ribbonAdURLElement] isEqualToString:@"N/A"]) {
                    [defaults setValue:[TBXML textForElement:ribbonAdURLElement] forKey:KEY_URL];
                    [defaults  setObject:[TBXML textForElement:ribbonAdURLElement]forKey:@"ribbonAdURL"];
                    
                    [bannerBtn addTarget:self action:@selector(bannerAdClicked:) forControlEvents:UIControlEventTouchUpInside];
                }
            }
            else if(ribbonAdImagePathElement == nil || [[TBXML textForElement:ribbonAdImagePathElement]isEqualToString:@"N/A"]){
                //[bannerBtn addSubview:retailerName];
                [defaults  setObject:[TBXML textForElement:ribbonAdImagePathElement]
                              forKey:@"ribbonAdImagePath"];
                [bannerBtn setTitle:retailerName.text forState:UIControlStateNormal];
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    bannerBtn.titleLabel.frame = CGRectMake(0, 40, 320, 40);
                    bannerBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18.0];
                }
                else
                {
                    bannerBtn.titleLabel.frame = CGRectMake(0, 40, SCREEN_WIDTH, 70);
                    bannerBtn.titleLabel.font = [UIFont boldSystemFontOfSize:24.0];
                }
                
                [bannerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                
                if (ribbonAdURLElement!=nil && ![[TBXML textForElement:ribbonAdURLElement] isEqualToString:@"N/A"]) {
                    [defaults setValue:[TBXML textForElement:ribbonAdURLElement] forKey:KEY_URL];
                    [bannerBtn addTarget:self action:@selector(bannerAdClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [defaults  setObject:[TBXML textForElement:ribbonAdURLElement]
                                  forKey:@"ribbonAdURL"];
                }
                [self.view addSubview:bannerBtn];
            }
            
            if (bannerAdImagePathElement)
                bannerAdPath = [[TBXML textForElement:bannerAdImagePathElement]copy];
            else
                bannerAdPath = @"";
            
            if (retailLocationIdElement!=nil) {
                retLocId = [[NSString alloc]initWithFormat:@"%@",[TBXML textForElement:retailLocationIdElement]];
                [defaults setValue:retLocId forKey:@"retailLocationID"];
            }
            
            if ([[TBXML textForElement:saleFlagElement]isEqualToString:@"true"]) {
                
                [titleArray addObject:@"See Current Deals!"];
                [subtitleArray addObject:@""];
                [indexArray addObject:[NSNumber numberWithInt:0]];//0 stands for current specials section
            }
            
            if (retailerAddressElement != nil && ![[TBXML textForElement:retailerAddressElement]isEqualToString:@"N/A"])
            {
                retailerAddress = [TBXML textForElement:retailerAddressElement];
                [titleArray addObject:@"Get Directions"];
                [subtitleArray addObject:[TBXML textForElement:retailerAddressElement]];
                [indexArray addObject:[NSNumber numberWithInt:1]];//1 stands for get direction section
                
                if([[HubCitiAppDelegate selIndexArrayFind] count])
                    [HubCitiAppDelegate  RefreshIndexFindArray];
                
                [defaults setObject:retailerName.text forKey:KEY_RTLNAME];
                
                [[HubCitiAppDelegate selIndexArrayFind] addObject:[TBXML textForElement:retailerAddressElement]];
                [[HubCitiAppDelegate selIndexArrayFind] addObject:@""];
                [[HubCitiAppDelegate selIndexArrayFind] addObject:@""];
                [[HubCitiAppDelegate selIndexArrayFind] addObject:@""];
                [[HubCitiAppDelegate selIndexArrayFind] addObject:@""];
                [[HubCitiAppDelegate selIndexArrayFind] addObject:@""];
                [[HubCitiAppDelegate selIndexArrayFind] addObject:@""];
                [[HubCitiAppDelegate selIndexArrayFind] addObject:@""];
                
                if (retLatitudeElement!=nil && ![[TBXML textForElement:retLatitudeElement] isEqualToString:@"N/A"])
                    [[HubCitiAppDelegate selIndexArrayFind] addObject:[TBXML textForElement:retLatitudeElement]];
                else {
                    [[HubCitiAppDelegate selIndexArrayFind] addObject:@""];
                    
                }
                if (retLongitudeElement!=nil && ![[TBXML textForElement:retLongitudeElement] isEqualToString:@"N/A"])
                    [[HubCitiAppDelegate selIndexArrayFind] addObject:[TBXML textForElement:retLongitudeElement]];
                else {
                    [[HubCitiAppDelegate selIndexArrayFind] addObject:@""];
                    
                }
            }
            if(![[TBXML textForElement:distanceElement] isEqualToString:@"N/A"])
                
                distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
            //Changed for 1.2.2
            if ((distanceElement!=nil && ![[TBXML textForElement:distanceElement] isEqualToString:@"N/A"]) || ![distanceFromPreviousScreen isEqualToString:@""] ) {
                
                UIImageView *pinImage ;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    pinImage = [[UIImageView alloc]initWithFrame:CGRectMake(290.0, 10.0, 20.0, 20.0)];
                }
                else
                {
                    pinImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-30, 10.0, 20.0, 20.0)];
                }
                pinImage.image = [UIImage imageNamed:@"PinDown.png"];
                pinImage.contentMode = UIViewContentModeCenter;
                pinImage.backgroundColor = [UIColor clearColor];
                [self.view addSubview:pinImage];
                
                UILabel *distLabel;
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    distLabel = [[UILabel alloc]initWithFrame:CGRectMake(260.0, pinImage.frame.origin.y+pinImage.frame.size.height, 60.0, 20.0)];
                    distLabel.font = [UIFont systemFontOfSize:12];
                }
                else
                {
                    distLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-80, pinImage.frame.origin.y+pinImage.frame.size.height, 60.0, 20.0)];
                    distLabel.font = [UIFont systemFontOfSize:16];
                }
                distLabel.backgroundColor = [UIColor clearColor];
                //Changed for 1.2.2.
                
                if (![distanceFromPreviousScreen isEqualToString:@""]) {
                    distLabel.text = distanceFromPreviousScreen;//[TBXML textForElement:distanceElement];
                }
                else
                {
                    
                    distLabel.text = [TBXML textForElement:distanceElement];
                }
                
                
                distLabel.adjustsFontSizeToFitWidth = YES;
                distLabel.minimumScaleFactor = 9.0;
                [self.view addSubview:distLabel];
                //[distLabel release];
            }
            
            if (contactPhoneElement != nil && ![[TBXML textForElement:contactPhoneElement]isEqualToString:@"N/A"]) {
                
                [titleArray addObject:NSLocalizedString(@"Call Location",@"Call Location")];
                NSMutableString *correctedPhone = [NSMutableString stringWithFormat:@"%@",[TBXML textForElement:contactPhoneElement]];
                
                if ([correctedPhone length]-1 >0)
                    [correctedPhone insertString:@"(" atIndex:0];
                
                if ([correctedPhone length]-1 >=4)
                    [correctedPhone insertString:@") " atIndex:4];
                
                if ([correctedPhone length]-1 >=9)
                    [correctedPhone insertString:@"-" atIndex:9];
                [subtitleArray addObject:correctedPhone];//[TBXML textForElement:contactPhoneElement]];
                //[imageUrlArray addObject:@"http://122.181.128.152:8080/Images/phone_icon.png"];
                [indexArray addObject:[NSNumber numberWithInt:2]];//2 stands for call store
            }
            if (retailerURLElement != nil && ![[TBXML textForElement:retailerURLElement]isEqualToString:@"N/A"]) {
                
                [titleArray addObject:NSLocalizedString(@"Browse Website",@"Browse Website")];
                [subtitleArray addObject:[TBXML textForElement:retailerURLElement]];
                //[imageUrlArray addObject:@"http://208.39.97.1:8080/Images/FNB_bwseIcn.png"];
                [indexArray addObject:[NSNumber numberWithInt:3]];//3 stands for browse website
            }
            
            TBXMLElement *retailerCreatedPagesListElement = [TBXML childElementNamed:@"retailerCreatedPageList" parentElement:tbXml.rootXMLElement];
            if (retailerCreatedPagesListElement!=nil){
                TBXMLElement *RetailerCreatedPagesElement = [TBXML childElementNamed:@"RetailerCreatedPages" parentElement:retailerCreatedPagesListElement];
                
                anythingPageListId = [[NSMutableArray alloc]init];
                pageId=[[NSMutableArray alloc]init];
                
                int i = 4;
                while(RetailerCreatedPagesElement!=nil){
                    TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:RetailerCreatedPagesElement];
                    TBXMLElement *pageTitleElement = [TBXML childElementNamed:@"pageTitle" parentElement:RetailerCreatedPagesElement];
                    TBXMLElement *pageImageElement = [TBXML childElementNamed:@"pageImage" parentElement:RetailerCreatedPagesElement];
                    TBXMLElement *pageIdElement = [TBXML childElementNamed:@"pageId" parentElement:RetailerCreatedPagesElement];
                    if (pageIdElement!=nil){
                        [pageId addObject:[TBXML textForElement:pageIdElement]];
                        DLog(@"pageId:%@",[pageId description]);
                    }
                    
                    
                    TBXMLElement *anythingPageListIDElement = [TBXML childElementNamed:@"anythingPageListId" parentElement:RetailerCreatedPagesElement];
                    
                    if (anythingPageListIDElement!=nil){
                        [anythingPageListId addObject:[TBXML textForElement:anythingPageListIDElement]];
                        DLog(@"anythingPageListId:%@",[anythingPageListId description]);
                    }
                    
                    [titleArray addObject:[TBXML textForElement:pageTitleElement]];
                    [subtitleArray addObject:[TBXML textForElement:pageLinkElement]];
                    [imageUrlArray addObject:[TBXML textForElement:pageImageElement]];
                    [indexArray addObject:[NSNumber numberWithInt:i]];//4 stands for retailer created pages
                    i++;
                    RetailerCreatedPagesElement = [TBXML nextSiblingNamed:@"RetailerCreatedPages" searchFromElement:RetailerCreatedPagesElement];
                }
            }
        }
        else {
            TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
            if (responseCodeElement!=nil) {
                
                TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
                [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
                return;
            }
        }
    }
}



#pragma mark tableview delegate and datasource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DLog(@"count:%lu",(unsigned long)[titleArray count]);
    return [titleArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 60.0;
    }
    else
    {
        return 90.0;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundColor = [UIColor whiteColor];
    
    UILabel *textLabel ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        textLabel = [[UILabel alloc]initWithFrame:CGRectMake(60.0, 8.0, 200.0, 20.0)];
    }
    else
    {
        textLabel = [[UILabel alloc]initWithFrame:CGRectMake(90.0, 8.0, 600.0, 40.0)];
    }
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.font = [UIFont boldSystemFontOfSize:16];
    textLabel.textColor = [UIColor colorWithRGBInt:0x112e72];
    
    UILabel *detailTextLabel ;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        detailTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(60.0, 35.0, 240.0, 20.0)];
    }
    else
    {
        detailTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(90.0, 35.0, 640.0, 40.0)];
        
    }
    detailTextLabel.backgroundColor = [UIColor clearColor];
    detailTextLabel.font = [UIFont boldSystemFontOfSize:13];
    detailTextLabel.numberOfLines = 1;
    detailTextLabel.textColor = [UIColor darkGrayColor];
    
    // cell.imageView.frame = CGRectMake(5, 5, 40, 40);
    
    SdImageView *asyncImageView;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(5, 5, 40, 40)];
    }
    else
    {
        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(5, 5, 70, 70)];
    }
    asyncImageView.backgroundColor = [UIColor clearColor];
    
    switch ([[indexArray objectAtIndex:indexPath.row]intValue]) {
            
        case 0:
            [asyncImageView loadImagefrommainBundel:@"curntSpcls.png"];
            [cell.contentView addSubview:asyncImageView];
            ReleaseAndNilify(asyncImageView);
            break;
            
        case 1:
            [asyncImageView loadImagefrommainBundel:@"FNB_getdirnivn.png"];
            detailTextLabel.text = [subtitleArray objectAtIndex:indexPath.row];
            [cell.contentView addSubview:asyncImageView];
            ReleaseAndNilify(asyncImageView);
            
            [cell.contentView addSubview:detailTextLabel];
            ReleaseAndNilify(detailTextLabel);
            break;
            
        case 2:
            [asyncImageView loadImagefrommainBundel:@"phone_icon.png"];
            [cell.contentView addSubview:asyncImageView];
            ReleaseAndNilify(asyncImageView);
            
            callStore = [UIButton buttonWithType:UIButtonTypeCustom];
            callStore.tag = indexPath.row;
            [callStore setEnabled:YES];
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                callStore.frame = CGRectMake(0, 0, 320, 60);
                
            }
            else
            {
                callStore.frame = CGRectMake(0, 0, SCREEN_WIDTH, 90);
                
            }
            [callStore setBackgroundColor:[UIColor clearColor]];
            [callStore setShowsTouchWhenHighlighted:YES];
            [callStore addTarget:self action:@selector(callPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:callStore];
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            detailTextLabel.text = [subtitleArray objectAtIndex:indexPath.row];
            [cell.contentView addSubview:detailTextLabel];
            ReleaseAndNilify(detailTextLabel);
            break;
            
        case 3:
            [asyncImageView loadImagefrommainBundel:@"FNB_bwseIcn.png"];
            [cell.contentView addSubview:asyncImageView];
            ReleaseAndNilify(asyncImageView);
            
            detailTextLabel.text = [subtitleArray objectAtIndex:indexPath.row];
            [cell.contentView addSubview:detailTextLabel];
            ReleaseAndNilify(detailTextLabel);
            break;
            
        default:
            //detailTextLabel.frame = CGRectMake(60.0, 35.0, 200.0, 20.0);
            
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                textLabel.frame = CGRectMake(60.0, 10.0, 200.0, 20.0);
                
            }
            else
            {
                textLabel.frame = CGRectMake(90.0, 10.0, 600.0, 40.0);
                
            }
            
            int count = 0;
            for (int i = 0; i<[indexArray count]; i++) {
                if ([[indexArray objectAtIndex:i]intValue]<4) {
                    count++;
                }
            }
            
            NSString *str = [imageUrlArray objectAtIndex:indexPath.row-count];
            [asyncImageView loadImage:str];
            asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
            [cell.contentView addSubview:asyncImageView];
            ReleaseAndNilify(asyncImageView);
            
            break;
    }
    
    textLabel.text = [titleArray objectAtIndex:indexPath.row];
    [cell.contentView addSubview:textLabel];
    ReleaseAndNilify(textLabel);
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ([[indexArray objectAtIndex:indexPath.row]intValue]) {
        case 0:{
            
            if ([[titleArray objectAtIndex:indexPath.row] isEqualToString:@"No Current Sales."]) {
                
                [UtilityManager showAlert:nil msg:@"ScanSee is working for you!  A request has been sent to this retailer for sales information.  Check back soon"];
            }
            else {
                
                //To get the selected retailer value for displaying it as title in next screen
                [defaults setObject:retailerName.text forKey:@"Selected Retailer"];
                DLog(@"Selected Retailer: %@",[defaults objectForKey:@"Selected Retailer"]);
                
//                CurrentSpecialsViewController *specialsPage = [[CurrentSpecialsViewController alloc]initWithNibName:@"CurrentSpecialsViewController" bundle:nil];
                
                CurrentSpecials *spl = [[CurrentSpecials alloc]initWithNibName:@"CurrentSpecials" bundle:nil];
                spl.couponFlag =YES;
                [self.navigationController pushViewController:spl animated:NO];

               // [self.navigationController pushViewController:specialsPage animated:NO];
                // [specialsPage release];
            }
        }
            break;
        case 1:{
            BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
            if (!lsFlag) {
                
                UIAlertController * alert;
                
                alert=[UIAlertController alertControllerWithTitle:@"Get Directions uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action){
                                        [self directionsPressed];
                                     }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                }
            
            else{
                [self directionsPressed];
            }
            
        }
            break;
            
        case 2:
            [self callPressed:(callStore)];
            
            break;
            
        case 3:{
            
            NSString *utStr = @"<browseWebClick>true</browseWebClick></UserTrackingData>";
            
            [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:utStr];
            
            [defaults  setObject:[subtitleArray objectAtIndex:indexPath.row] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
            break;
            
        }
            
            
        case 4:{
            if ([eventExist isEqualToString:@"1"]) {
                EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
                self.iEventsListViewController = iEventsListViewController;
                iEventsListViewController.isRetailerEvent = TRUE;
                iEventsListViewController.retailId = retId ;
                iEventsListViewController.retailLocationId = retLocId ;
                [self.navigationController pushViewController:iEventsListViewController animated:NO];
                //[iEventsListViewController release];;
            }
            
            else if ([fundExist isEqualToString:@"1"]){
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                iFundraiserListViewController.isFundraiserEvent = TRUE;
                iFundraiserListViewController.retailId = retId ;
                iFundraiserListViewController.retailLocationId = retLocId ;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
            }
            
            
            else{
                
                anythingShareFlag=YES;
                NSString *utStr;
                NSURL* url = [NSURL URLWithString:[subtitleArray objectAtIndex:indexPath.row]];
                //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
                // DLog(@"%@",name);
                if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                    [UtilityManager customUrlLoad:url];
                }
                else if(![[url absoluteString] containsString:@"3000.htm"]){
                    if ([anythingPageListId count]){
                        
                        utStr = [NSString stringWithFormat:@"<anythingPageListId>%@</anythingPageListId><anythingPageClick>true</anythingPageClick></UserTrackingData>",[anythingPageListId objectAtIndex:[[indexArray objectAtIndex:indexPath.row]intValue]-4]];
                        DLog(@"Val:%@",[anythingPageListId objectAtIndex:[[indexArray objectAtIndex:indexPath.row]intValue]-4]);
                        [defaults setObject:[pageId objectAtIndex:[[indexArray objectAtIndex:indexPath.row]intValue]-4] forKey:KEY_ANYPAGEID];
                    }
                    else{
                        utStr = @"<anythingPageClick>true</anythingPageClick></UserTrackingData>";
                    }
                    
                    [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:utStr];
                    
                    // Not a web video? Then lets just show the URL page.
                    
                    [defaults  setObject:[subtitleArray objectAtIndex:indexPath.row] forKey:KEY_URL];
                    
                    WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                    // [self.view addSubview:self.view.window.rootViewController.view];
                    // [self.view.window.rootViewController.view setHidden:YES];
                    [self.navigationController pushViewController:urlDetail animated:NO];
                    //[urlDetail release];
                }
                else{
                    [defaults setValue:[subtitleArray objectAtIndex:indexPath.row] forKey:KEY_URL];
                    
                    
                    BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                    if (!lsFlag) {
                        
                        UIAlertController * alert;
                        
                        alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction
                                             actionWithTitle:@"OK"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action){
                                                 [alert dismissViewControllerAnimated:NO completion:nil];
                                                 [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                             }];
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                        
                    }
                    else{
                        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                        
                        splOfferVC.isAppsiteLogisticsFlag = 1;
                         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                        [HubCitiAppDelegate setIsLogistics:YES];
                        
                        [self presentViewController:splOfferVC animated:YES completion:nil];
                    }
                    
                }
                
            }
            
            break;
            
        }
            
            
        case 5:{
            
            if ([fundExist isEqualToString:@"1"] && [eventExist isEqualToString:@"1"]){
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                iFundraiserListViewController.isFundraiserEvent = TRUE;
                iFundraiserListViewController.retailId = retId ;
                iFundraiserListViewController.retailLocationId = retLocId ;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
            }
            
            
            else{
                
                anythingShareFlag=YES;
                NSString *utStr;
                
                NSURL* url = [NSURL URLWithString:[subtitleArray objectAtIndex:indexPath.row]];
                //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
                //DLog(@"%@",name);
                
                if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                    [UtilityManager customUrlLoad:url];
                }
                
                else if(![[url absoluteString] containsString:@"3000.htm"]){
                    if ([anythingPageListId count]){
                        
                        utStr = [NSString stringWithFormat:@"<anythingPageListId>%@</anythingPageListId><anythingPageClick>true</anythingPageClick></UserTrackingData>",[anythingPageListId objectAtIndex:[[indexArray objectAtIndex:indexPath.row]intValue]-4]];
                        DLog(@"Val:%@",[anythingPageListId objectAtIndex:[[indexArray objectAtIndex:indexPath.row]intValue]-4]);
                        [defaults setObject:[pageId  objectAtIndex:[[indexArray objectAtIndex:indexPath.row]intValue]-4] forKey:KEY_ANYPAGEID];
                    }
                    else{
                        utStr = @"<anythingPageClick>true</anythingPageClick></UserTrackingData>";
                    }
                    
                    [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:utStr];
                    
                    // Not a web video? Then lets just show the URL page.
                    
                    [defaults  setObject:[subtitleArray objectAtIndex:indexPath.row] forKey:KEY_URL];
                    
                    WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                    // [self.view addSubview:self.view.window.rootViewController.view];
                    // [self.view.window.rootViewController.view setHidden:YES];
                    [self.navigationController pushViewController:urlDetail animated:NO];
                    //[urlDetail release];
                }
                else{
                    [defaults setValue:[subtitleArray objectAtIndex:indexPath.row] forKey:KEY_URL];
                    
                    
                    BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                    if (!lsFlag) {
                        
                        
                        UIAlertController * alert;
                        
                        alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction
                                             actionWithTitle:@"OK"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action){
                                                 [alert dismissViewControllerAnimated:NO completion:nil];
                                                 [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                             }];
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];

                    }
                    else{
                        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                        
                        splOfferVC.isAppsiteLogisticsFlag = 1;
                         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                        [HubCitiAppDelegate setIsLogistics:YES];
                        
                        [self presentViewController:splOfferVC animated:YES completion:nil];
                    }
                    
                }
                
            }
            
        }
            break;
            
            
        default:{
            
            NSString *utStr;
            
            
            
            anythingShareFlag=YES;
            NSURL* url = [NSURL URLWithString:[subtitleArray objectAtIndex:indexPath.row]];
            // NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            // DLog(@"%@",name);
            //            else if ([[titleArray objectAtIndex:indexPath.row] isEqualToString:@"Fundraiser"]){
            //                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            //                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
            //                //[iFundraiserListViewController release];
            //            }
            //
            
            //            else
            //            {
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if(![[url absoluteString] containsString:@"3000.htm"]){
                if ([anythingPageListId count]){
                    
                    utStr = [NSString stringWithFormat:@"<anythingPageListId>%@</anythingPageListId><anythingPageClick>true</anythingPageClick></UserTrackingData>",[anythingPageListId objectAtIndex:[[indexArray objectAtIndex:indexPath.row]intValue]-4]];
                    DLog(@"Val:%@",[anythingPageListId objectAtIndex:[[indexArray objectAtIndex:indexPath.row]intValue]-4]);
                    [defaults setObject:[pageId objectAtIndex:[[indexArray objectAtIndex:indexPath.row]intValue]-4] forKey:KEY_ANYPAGEID];
                }
                else{
                    utStr = @"<anythingPageClick>true</anythingPageClick></UserTrackingData>";
                }
                
                
                
                
                [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:utStr];
                
                // Not a web video? Then lets just show the URL page.
                
                [defaults  setObject:[subtitleArray objectAtIndex:indexPath.row] forKey:KEY_URL];
                
                WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                // [self.view addSubview:self.view.window.rootViewController.view];
                // [self.view.window.rootViewController.view setHidden:YES];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
                
                
            }
            else{
                
                [defaults setValue:[subtitleArray objectAtIndex:indexPath.row] forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                              [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                        [HubCitiAppDelegate setIsLogistics:YES];
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                        splOfferVC.isEventLogisticsFlag = 1;
                    
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];

                }
                
            }
            
            
        }
            
            break;
            
            
            //        }
            
    }
    [table_RetailerSummary deselectRowAtIndexPath:[table_RetailerSummary indexPathForSelectedRow] animated:YES];
}


-(void) callPressed: (UIButton *)sender{
    
    NSString *utStr = @"<callStoreClick>true</callStoreClick></UserTrackingData>";
    
    [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:utStr];
    
    int j= 0;
    for (int i = 0; i <[indexArray count]; i++) {
        
        if ([[indexArray objectAtIndex:i]intValue] == 2) {
            
            j = i;
        }
    }
    
    //DLog(@"Val:%@",[subtitleArray objectAtIndex  ]);
    NSString *phoneString  = [[[subtitleArray objectAtIndex:j] componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
    NSString *phoneNumber = [NSString stringWithFormat:@"tel:%@", phoneString];
    NSURL    *url= [NSURL URLWithString:phoneNumber];
    
    UIWebView *webview = [[UIWebView alloc] initWithFrame:[callStore frame]];
    webview.alpha = 0.0;
    
    [webview loadRequest:[NSURLRequest requestWithURL:url]];
    
    // Assume we are in a view controller and have access to self.view
    [self.view insertSubview:webview belowSubview:callStore];
    // [webview release];
}

//Chance's Edits

-(void) websitePressed: (int) index {
    
    [defaults  setObject:[infoValue objectAtIndex:index] forKey:KEY_URL];
    [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
    WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:urlDetail animated:NO];
    //[urlDetail release];
}

-(void) directionsPressed {
    
    NSString *utStr = @"<getDirClick>true</getDirClick></UserTrackingData>";
    [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:utStr];
    
    //Checking to see if LS is on, or if allowed to be. If not, we dont need to try and calculate directions, as we may not have a valid Lat and Long in the standardDefaults. This is why we were getting the Invalid Region error.
    
    if ([HubCitiAppDelegate checkZipAndLocation] != 0) {
        [defaults setValue:@"" forKey:@"Directions"];
    }
    else {
        if (![HubCitiAppDelegate locationServicesOn]) {
            [defaults setValue:@"" forKey:@"Directions"];
        }
        else {
            [defaults setValue:@"Directions" forKey:@"Directions"];
        }
    }
    
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse))
    {
        [defaults setValue:@"" forKey:KEY_LATITUDE];
        [defaults setValue:@"" forKey:KEY_LONGITUDE];
        
        [Location updateLocationinfo:self];
    }
    
    //[defaults  setObject:[infoValue objectAtIndex:sender.tag] forKey:KEY_URL];
    FindNearByRetailerMap *retailerMap = [[FindNearByRetailerMap alloc]initWithNibName:@"FindNearByRetailerMap" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:retailerMap animated:NO];
    //[retailerMap release];
}



#pragma mark - Location Service Methods
-(void) updatedLatlongValue :(NSString *) latValue1 longitudeValue:(NSString *)longValue1{
    
    [defaults setBool:YES forKey:@"gpsEnabled"];
    [defaults setObject:latValue1 forKey:KEY_LATITUDE];
    [defaults setObject:longValue1 forKey:KEY_LONGITUDE];
    
    //Added the setting for these keys because its what the Map is using.
    [defaults  setValue:latValue1 forKey:@"LatVal"];
    [defaults  setValue:longValue1 forKey:@"LongVal"];
}


-(void)userTracking:(NSString*)strToAppend{
    
    NSMutableString *reqStr = [[NSMutableString alloc] init];
    DLog(@"%@",retDetId);
    [reqStr appendFormat:@"<UserTrackingData><retDetailsId>%@</retDetailsId><retListId>%@</retListId>",retDetId,[defaults valueForKey:KEY_RLISTID]];
    [reqStr appendFormat:@"%@",strToAppend];
    
    NSMutableString *urlString = [BASE_URL mutableCopy];
    [urlString appendString:@"thislocation/utretsumclick"];
    
    NSString *response = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    DLog(@"%@",response);
    //[reqStr release];
}

//End Chance's Edits

-(void)popToSafari:(NSString *)urlString
{
    
    NSURL *vidURL = [NSURL URLWithString:urlString];
    
    if (![[UIApplication sharedApplication] openURL:vidURL])
        
        DLog(@"%@%@",@"Failed to open url:",[vidURL description]);
}




-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}


-(void)bannerAdClicked:(id)sender{
    
    NSString *utStr = @"<banADClick>true</banADClick></UserTrackingData>";
    
    [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:utStr];
    
    NSString *url = [defaults  objectForKey:@"ribbonAdURL"];
    [defaults  setObject:url forKey:KEY_URL];
    
    [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
    WebBrowserViewController *webView = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:webView animated:NO];
    //[webView release];
}

-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}
-(void) request_ForRetSummary
{
    //    [HubCitiAppDelegate showActivityIndicator];
    //    NSMutableString *requestStr = [[NSMutableString alloc] init];
    //    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    //    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",retLocId];
    //
    //    [requestStr appendFormat:@"<scanTypeId>%@</scanTypeId>",@"0"];
    //
    //    if([defaults valueForKey:KEY_MAINMENUID])
    //        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    //
    //        [requestStr appendFormat:@"<retListId>%@</retListId>",[defaults valueForKey:KEY_RLISTID]];
    //
    //
    //    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
    //
    //        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
    //         [defaults  valueForKey:KEY_LATITUDE]];
    //
    //    }
    //    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
    //
    //        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    //    }
    //    else if ( [defaults  valueForKey:@"postalCode"] && (![[defaults  valueForKey:@"postalCode"] isEqualToString:@""]))
    //        [requestStr appendFormat:@"<postalCode>%@</postalCode>",[defaults  valueForKey:@"postalCode"]];
    //
    //
    //    [requestStr appendFormat:@"<retailerId>%@</retailerId>",retId];
    //    if([defaults valueForKey:KEY_HUBCITIID])
    //        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    //    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    //
    //    DLog(@"requestStr = %@",requestStr);
    //  //  [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    //    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    //    [self parse_RetSummary:responseXML];
    //    [table_RetailerSummary reloadData];
    
    
    
    
    
    [HubCitiAppDelegate showActivityIndicator];
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",retLocId];
    
    [requestStr appendFormat:@"<scanTypeId>%@</scanTypeId>",@"0"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    [requestStr appendFormat:@"<retListId>%@</retListId>",[defaults valueForKey:KEY_RLISTID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    else if ( [defaults  valueForKey:@"postalCode"] && (![[defaults  valueForKey:@"postalCode"] isEqualToString:@""]))
        [requestStr appendFormat:@"<postalCode>%@</postalCode>",[defaults  valueForKey:@"postalCode"]];
    
    
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",retId];
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    
    DLog(@"requestStr = %@",requestStr);
    //  [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    NSString *responseXML = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    [self parse_RetSummary:responseXML];
    
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [ConnectionManager cancelSessionRequest : requestURLForCategory];
}
@end

//
//  WebBrowserViewController.m
//  Scansee
//
//  Created by ajit on 6/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WebBrowserViewController.h"
#import "RetailerSummaryViewController.h"
#import "MainMenuViewController.h"
#import "AppDelegate.h"
#import "FindOptionsViewController.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "RetailerSpecialOffersViewController.h"
#import "CurrentSpecials.h"
#import "SpecialOfferRetailersViewController.h"
#import "NativeSpecialOfferViewController.h"
#import "CurrentSpecialsViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "SpecialOffersViewController.h"
#import "CouponsInDealViewController.h"

@interface WebBrowserViewController ()
{
    BOOL isFirstWebPage;
    CGFloat contentHeight;
    NSString* newUrl,*newUrlString;
    UIButton * homeButton;
    DWBubbleMenuButton *upMenuView;
    NSMutableURLRequest* newRequest;
}

@end
//static const CGFloat kNavBarHeight = 52.0f;
//static const CGFloat kLabelHeight = 14.0f;
//static const CGFloat kMargin = 10.0f;
//static const CGFloat kSpacer = 2.0f;
//static const CGFloat kLabelFontSize = 12.0f;
//static const CGFloat kAddressHeight = 26.0f;

@implementation WebBrowserViewController
@synthesize webView;
@synthesize toolbar = mToolbar;
@synthesize back = mBack;
@synthesize forward = mForward;
@synthesize refresh = mRefresh;
@synthesize stop = mStop;
@synthesize activityIndicator;
@synthesize hDLongDesc,title,anyVC,emailSendingVC,comingFromClaim,comingFromSideMenu;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    configuration.allowsInlineMediaPlayback = YES;
    configuration.allowsAirPlayForMediaPlayback = YES;
    configuration.requiresUserActionForMediaPlayback = YES;
    configuration.allowsPictureInPictureMediaPlayback = YES;
    isFirstWebPage = false;
    if (hDLongDesc) {
        CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
        [cus setTitle:title forView:self withHambergur:NO];

        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            mWebView =[[UIWebView alloc]initWithFrame:CGRectMake(0, 0 , SCREEN_WIDTH, SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT)];
        }
        else
        {
            mWebView =[[UIWebView alloc]initWithFrame:CGRectMake(0, 0 , SCREEN_WIDTH, SCREEN_HEIGHT-EXCLUDE_BAR_HEIGHT)];
        }
        
        mWebView.delegate=self;
        
        if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
        {
            [mWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#000;'>%@",hDLongDesc] baseURL:nil];
        }
        else
        {
            [mWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:17px;font-family:Helvetica;color:#000;'>%@",hDLongDesc] baseURL:nil];
        }
        
        [self.view addSubview:mWebView];
        
        
    }
    
    else{
        
        //    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 40)];
        //    titleLabel.textAlignment = NSTextAlignmentCenter;
        //    titleLabel.textColor = [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]];
        //
        //    titleLabel.numberOfLines = 2;
        //    [titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        //    titleLabel.font = [UIFont boldSystemFontOfSize:16];
        //    titleLabel.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"Details", @"Details")];
        if (self.title.length > 0) {
            self.navigationItem.title = self.title;
        }
        else
            self.navigationItem.title = @"Details";
        
        //[titleLabel release];
        tempArray = [[NSMutableArray alloc]init];
        activityIndicator.center = self.view.center;
        
        // Do any additional setup after loading the view from its nib.
        NSAssert(self.back, @"Unconnected IBOutlet 'back'");
        NSAssert(self.forward, @"Unconnected IBOutlet 'forward'");
        NSAssert(self.refresh, @"Unconnected IBOutlet 'refresh'");
        NSAssert(self.stop, @"Unconnected IBOutlet 'stop'");
        self.webView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:configuration];
        int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
        self.webView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y, SCREEN_WIDTH, SCREEN_HEIGHT- 44-yVal);
       
        
        self.webView.navigationDelegate = self;
        self.webView.UIDelegate = self;
        
        //has been changed only for this location - as in DB they have not appended "http", it wont load
        NSMutableString *urlString ;
        urlString = [NSMutableString stringWithFormat:@"%@",[defaults valueForKey:KEY_URL]];
        
        NSString *string2;
        if ([urlString length]>7) {
            string2 = [urlString substringWithRange: NSMakeRange (0, 7)];
        }
        else
            string2 = [urlString copy];
        
        DLog(@"String2:%@", string2);
        
        if([string2 caseInsensitiveCompare:@"http://"] == NSOrderedSame || [string2 caseInsensitiveCompare:@"https:/"] == NSOrderedSame){
            
            urlString = [NSMutableString stringWithFormat:@"%@",[defaults valueForKey:KEY_URL]];
        }
        else{
            urlString = [NSMutableString stringWithString:@"http://"];
            [urlString appendFormat:@"%@",[defaults valueForKey:KEY_URL]];
        }
        
        NSURL* url;
        if ([urlString containsString:@" "])
        {
            NSError *error = nil;
            NSRegularExpression *regexExp = [NSRegularExpression regularExpressionWithPattern:@" " options:NSRegularExpressionCaseInsensitive error:&error];
            url =[NSURL URLWithString:[regexExp stringByReplacingMatchesInString:urlString options:0 range:NSMakeRange(0, [urlString length]) withTemplate:@"%20"]];
        }
        //  url = [NSURL URLWithString:[urlString urlEncodeUsingEncodingSpace:NSUTF8StringEncoding]];
        else
            url = [NSURL URLWithString:urlString];
        
        if (newUrlString.length == 0) {
            newUrlString = [urlString copy];
        }
        
        _request = [NSMutableURLRequest requestWithURL:url];
        
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
        
        NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
        [cookieProperties setObject:@"mobileApp" forKey:NSHTTPCookieName];
        [cookieProperties setObject:@"1" forKey:NSHTTPCookieValue];
        [cookieProperties setObject:[url host] forKey:NSHTTPCookieDomain];
        [cookieProperties setObject:[url host] forKey:NSHTTPCookieOriginURL];
        [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
        [cookieProperties setObject:@"0" forKey:NSHTTPCookieVersion];
        
        cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
        
        
        self.webView.allowsBackForwardNavigationGestures = YES;
        @autoreleasepool {
            [self.webView loadRequest:_request];
        }
        
        self.webView.backgroundColor=[UIColor clearColor];
        
        
        
        [self updateButtons];
         [self.view addSubview:self.webView];
        activityIndicator.center = self.webView.center;
        [self.webView addSubview:activityIndicator];
        [activityIndicator startAnimating];
    }
    
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [homeButton setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:homeButton.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [homeButton addSubview:homeImage];
    
    
    [homeButton addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    mainPage.style = UIBarButtonItemStylePlain;
    
    
    
    if ( [defaults boolForKey:@"trainingSlides"]) {
       self.navigationItem.rightBarButtonItem = nil;
    }
    else{
        self.navigationItem.rightBarButtonItem = mainPage;
    }
    if([defaults boolForKey:@"isCombo"] || [defaults boolForKey:@"isBlock"]){
     
        [defaults setBool:NO forKey:@"isCombo"];
        [defaults setBool:NO forKey:@"isBlock"];
        
        [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:[UIColor convertToHexString:self.titleColor], NSForegroundColorAttributeName,[UIFont fontWithName:@"Helvetica-Bold" size:17.0], NSFontAttributeName,nil]];
        
        self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:self.titleBackgColor];
        
        UIButton  *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame=CGRectMake(0.0, 0.0, 30.0, 30.0);
        
        [backButton addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationController.navigationBar.hidden=NO;
        self.navigationItem.leftBarButtonItem = back;
        [UtilityManager setTransperentImageColor:[defaults valueForKey:@"backbuttonColor"] forButton:backButton];
    }
    else{
    //customize back button
    if (shareFlag==true || anythingShareFlag==YES) {
        // displaying share button
        
        UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        shareButton.frame=CGRectMake(0, 0, 20, 18);
        
        UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:shareButton];
        
        
        
        //customize back button
        UIButton *backBtn = [UtilityManager customizeBackButton];
        
        [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        //self.navigationItem.leftBarButtonItem = back;
        self.navigationController.navigationBar.hidden=NO;
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        self.navigationItem.leftItemsSupplementBackButton=YES;
        self.navigationItem.leftBarButtonItems=@[back,item1];
        self.navigationItem.hidesBackButton = YES;
        
    }
    else
    {
        UIButton *backBtn = [UtilityManager customizeBackButton];
        
        [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        
        self.navigationController.navigationBar.hidden=NO;
        if ([defaults  boolForKey:@"hideBackBtn"] == YES){
            self.navigationItem.hidesBackButton = YES;
            self.navigationItem.leftBarButtonItem = nil;
        }
        else{
            self.navigationItem.leftBarButtonItem = back;
        }
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        
    }
    }
    
    
    
    if(([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)){
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoNowVisible:) name:@"UIMoviePlayerControllerDidEnterFullscreenNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoNowHidden:) name:@"UIMoviePlayerControllerWillExitFullscreenNotification" object:nil];
        
    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoNowVisible:) name:UIWindowDidBecomeVisibleNotification object:self.view.window];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoNowHidden:) name:UIWindowDidBecomeHiddenNotification object:self.view.window];
        
    }
}




-(void)activityIndicatorConstraints
{
    NSDictionary *viewsDictionary = @{@"activity":activityIndicator };
    activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-512-[activity]" options:0 metrics:nil views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-384-[activity]" options:0 metrics:nil views:viewsDictionary]];
}

- (void)videoNowVisible:(NSNotification *)notification
{
        if([[defaults valueForKey:@"PresentActionSheet"]isEqualToString:@"0"]){
            [HubCitiAppDelegate setIsVideoOrientationcalled:YES];
        }
   
    
}




- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [HubCitiAppDelegate setIsVideoOrientationcalled:NO];
    [self.navigationController setNavigationBarHidden:NO animated:NO];

    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
   // cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
    {
        
        [HubCitiAppDelegate application:[UIApplication sharedApplication] supportedInterfaceOrientationsForWindow:[HubCitiAppDelegate window]];
    }
    
    
    [defaults setValue:@"0" forKey:@"PresentActionSheet"];
    BOOL doesContain = [self.view.subviews containsObject:upMenuView];
    if(!doesContain){
    if (![defaults boolForKey:@"trainingSlides"]) {
        
        if (shareFlag==true || anythingShareFlag==YES) {
            [self addShareButton];
        }
    }
    }
    
}

-(void)addShareButton{
    // Create down menu button
    UIImageView *homeLabel ;
    
    
    // Create up menu button
    homeLabel = [self createHomeButtonView];
    if ([defaults boolForKey:@"trainingSlides"]) {
        homeLabel.hidden = true;
    }
    else{
        homeLabel.hidden = false;
    }
    upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
                                                                                          self.view.frame.size.height - homeLabel.frame.size.height - 60.f,
                                                                                          homeLabel.frame.size.width,
                                                                                          homeLabel.frame.size.height)
                                                            expansionDirection:DirectionUp];
    upMenuView.homeButtonView = homeLabel;
    
    upMenuView.delegate = self;
    
    [upMenuView addButtons:[self createDemoButtonArray]];
    
    [self.view addSubview:upMenuView];
    
    [self.view bringSubviewToFront:upMenuView];
    
    upMenuView.onLogOutPressed =^(){
        
        [self logOutPressed];
    } ;
    
    
}




- (UIImageView *)createHomeButtonView {
    
    
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
    label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}

- (NSArray *)createDemoButtonArray {
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    
    int i = 0;
    for (NSString *titl in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:titl] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        
        button.clipsToBounds = YES;
        button.tag = i++;
        
        [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
}

- (void)test:(UIButton *)sender {
    
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
    [self shareClicked];
    
    switch (sender.tag) {
        case 0:{//facebook
            anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
            //[self.view addSubview:anyVC.view];
            if (shareFlag==TRUE) {
                anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"ANYTHING_SHARE"];
            }
            else if(anythingShareFlag==TRUE)
            {
                anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
            }
            
            [UIView transitionWithView:self.view duration:0.5
                               options:UIViewAnimationOptionTransitionCurlDown
                            animations:^ { [self.view addSubview:anyVC.view]; }
                            completion:nil];
        }
            break;
        case 1:{//twitter
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                    if (result == SLComposeViewControllerResultCancelled) {
                        DLog(@"Twitter Result: canceled");
                    } else {
                        DLog(@"Twitter Result: sent");
                        NSMutableString *userXmlStr;
                        if (shareFlag==TRUE) {
                            userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><sPageId>%@</sPageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PAGEID]];
                        }
                        else {
                            if (requestAnythingFromMainMenu==FALSE) {
                                userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><aPageId>%@</aPageId><retId>%@</retId><retLocId>%@</retLocId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_ANYPAGEID],[defaults valueForKey:KEY_RETAILERID],[defaults valueForKey:@"retailLocationID"]];
                            }else{
                                userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><aPageId>%@</aPageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_ANYPAGEID]];
                            }
                        }
                        
                        
                        [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
                    }
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        
                        [controller.view removeFromSuperview];
                    } else
                        [controller.view removeFromSuperview];
                    [defaults setValue:@"0" forKey:@"PresentActionSheet"];
                };
                controller.completionHandler =myBlock;
                
                //Adding the Text to the facebook post value from iOS
                [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                
                //Adding the URL to the facebook post value from iOS
                
                //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                
                //Adding the Image to the facebook post value from iOS
                
                [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                
                if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                    [self.view addSubview:controller.view];
                    [self.view bringSubviewToFront:controller.view];
                }else
                    [self.view addSubview:controller.view];
                [self.view bringSubviewToFront:controller.view];
                
            }
            else {
                [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
            }
        }
            
            
            break;
        case 2:{//text
            
            if ([self hasCellularCoverage])
            {
                if ([MFMessageComposeViewController canSendText]) {
                    
                    MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
                    [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                    smsComposerController.messageComposeDelegate = self;
                    [self presentViewController:smsComposerController animated:YES completion:nil];
                }
            }
            else
            {
                [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
            }
            
            
        }
            break;
        case 3:{//email
            //[defaults setObject:PRODUCT_SHARE forKey:KEY_SHARE_TYPE];
            emailSendingVC = [[EmailShareViewController alloc]
                              initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
            if (anythingShareFlag==true) {
                emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"ANYTHING_SHARE"];
            }
            else if(shareFlag==true)
            {
                emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
            }
            __typeof(self) __weak  obj = self;
            emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                
                [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
            } ;
            
            [emailSendingVC loadMail];
        }
            break;
            
        default:
            [defaults setBool:NO forKey:BottomButton];
            break;
            
    }
    
}

- (UIButton *)createButtonWithName:(NSString *)imageName {
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button sizeToFit];
    
    [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}



- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView{
    
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}
- (void)videoNowHidden:(NSNotification *)notification
{
    if([[defaults valueForKey:@"PresentActionSheet"]isEqualToString:@"0"]){
        
        [HubCitiAppDelegate setIsVideoOrientationcalled:NO];
    }
   
}

-(void)removeCache
{
    self.webView.navigationDelegate = nil;
    self.webView = nil;
    self.toolbar = nil;
    self.back = nil;
    self.forward = nil;
    self.refresh = nil;
    self.stop = nil;
    self.activityIndicator = nil;
    [urlConnection cancel];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

-(void)popBackToPreviousPage{
    
    
    [defaults setBool:YES forKey:@"backfromWebView"];
    if (comingFromClaim == YES)
    {
        [defaults setBool:YES forKey:@"isComingFromCliam"];
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        for (UIViewController *aViewController in allViewControllers) {
            if ([aViewController isKindOfClass:[RetailerSummaryViewController class]]) {
                [self.navigationController popToViewController:aViewController animated:NO];
            }
        }
    }
    else
    if (shareFlag==YES) {
        shareFlag=NO;
        if (directFlag==YES) {
            //SpecialOfferRetailersViewController
            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
            for (UIViewController *aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[SpecialOfferRetailersViewController class]]) {
                    [self.navigationController popToViewController:aViewController animated:NO];
                }
            }
            
        }
        else
        {
            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
            for (UIViewController *aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[RetailerSpecialOffersViewController class]] || [aViewController isKindOfClass:[CurrentSpecials class]]) {
                    [self.navigationController popToViewController:aViewController animated:NO];
                }
            }
        }
    }
    else
    {
        if(self.webView.canGoBack && !comingFromSideMenu)
        {
            [activityIndicator stopAnimating];
            
            [self.webView goBack];
        }
        else{
           // [self removeCache];
            ReleaseAndNilify(hDLongDesc);
            shareFlag=FALSE;
            
            anythingShareFlag=FALSE;
            requestAnythingFromMainMenu=FALSE;
            [activityIndicator stopAnimating];
            
            [activityIndicator setHidden:TRUE];
            newUrl = @"";
            [HubCitiAppDelegate setIsVideoOrientationcalled:NO];
            [HubCitiAppDelegate setIsLogistics:NO];
            [defaults setValue:newUrlString forKey:KEY_URL];
            [self.view setHidden:TRUE];
            [self.navigationController popViewControllerAnimated:NO];
            [self updateButtons];

            
        }
        
        
    }
}


- (void)viewDidUnload
{
    self.webView = nil;
    self.toolbar = nil;
    self.back = nil;
    self.forward = nil;
    self.refresh = nil;
    self.stop = nil;
    self.activityIndicator = nil;
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
   return NO;
}


// MARK: -
// MARK: UI methods

/**
 * \brief Updates the forward, back and stop buttons.
 */
- (void)updateButtons
{
    self.forward.enabled = self.webView.canGoForward;
    self.back.enabled = self.webView.canGoBack;
    self.stop.enabled = self.webView.loading;
  
}



-(void) showDialog
{
    
    BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
    if (!lsFlag) {
       
            UIAlertController * alert;
            
            alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     [self presentSpecialOffersAfterDismissAleert];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    else
    {
        
        [self presentSpecialOffersAfterDismissAleert];
    }
    
}

-(void)presentSpecialOffersAfterDismissAleert {
    
    if (self.isViewLoaded && self.view.window != nil) {
        [activityIndicator stopAnimating];
        [defaults setValue:newUrl forKey:KEY_URL];
        
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        
        
            splOfferVC.isEventLogisticsFlag = 1;
            [HubCitiAppDelegate setIsLogistics:YES];
        
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
        
       
        
    }
    
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}


-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)webViewRefresh:(id)sender {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.webView.URL];
    [self.webView loadRequest:request];
}
- (IBAction)webViewForward:(id)sender {
    [self.webView goForward];
}

- (IBAction)webViewcancel:(id)sender {
    [self.webView stopLoading];
}
- (IBAction)webViewbackward:(id)sender {
    [self.webView goBack];
}

//https support

#pragma mark - Webview delegate

// Note: This method is particularly important. As the server is using a self signed certificate,
// we cannot use just UIWebView - as it doesn't allow for using self-certs. Instead, we stop the
// request in this method below, create an NSURLConnection (which can allow self-certs via the delegate methods
// which UIWebView does not have), authenticate using NSURLConnection, then use another UIWebView to complete
// the loading and viewing of the page. See connection:didReceiveAuthenticationChallenge to see how this works.

-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
     [activityIndicator startAnimating];
    [self updateButtons];
}
-(void) webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    {
        
        [activityIndicator stopAnimating];
        
        [self updateButtons];
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        __block NSString *resultString = nil;
        
        
        [self.webView evaluateJavaScript:@"document.readyState" completionHandler:^(id result, NSError *error) {
            if (error == nil) {
                if (result != nil) {
                    resultString = [NSString stringWithFormat:@"%@", result];
                    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0 && [resultString isEqualToString:@"complete"])
                    {
                        if(!isFirstWebPage)
                        {
                            contentHeight = self.webView.scrollView.contentSize.height; isFirstWebPage = true;
                        }
                        if ( self.webView.scrollView.contentSize.height < contentHeight && !self.webView.canGoBack)
                        {
                            [self.webView reload];
                            contentHeight = 0;
                        }
                    }
                    
                }
            } else {
                NSLog(@"evaluateJavaScript error : %@", error.localizedDescription);
            }
            
        }];
        [self.webView setHidden:FALSE];
        
        
    }
}


-(void) webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    [activityIndicator stopAnimating];
    [self updateButtons];
}


-(void) webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    //--ashika---
    //to handle blank screen while re-directing to correct URL
    if([[[navigationAction.request URL] absoluteString]  containsString:@"hashtag"]||[[[navigationAction.request URL] absoluteString]  containsString:@"blank"] ){
        
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
   
    DLog(@"Did start loading: %@ auth:%d", [[navigationAction.request URL] absoluteString], _authenticated);
    
   
    
    
    if (navigationAction.navigationType == UIWebViewNavigationTypeLinkClicked) {
        
        if ([[[navigationAction.request URL] absoluteString] containsString:@"itunes.apple.com"]) {
            [UtilityManager customUrlLoad:[navigationAction.request URL]];
        }
        else if ([[[navigationAction.request URL] absoluteString] containsString:@"3000.htm"]) {
            
            [self.webView stopLoading];
            
            newUrl = [[[navigationAction.request URL] absoluteString] copy];
            
            [self showDialog];
            
            // return  NO;
        }
        
    }
    
    
    
    if ([[[navigationAction.request URL] scheme] isEqual:@"mailto"])
    {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[navigationAction.request URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                //return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,(CFStringRef)value,CFSTR("")));
                    
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            // [mailViewController release];
            //return NO;
            
        }
        
        else {
            [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
        }
        
        //return NO;
    }
    
    if ([[navigationAction.request URL] fragment]) {
        
        
        [self.webView evaluateJavaScript:@"window.location.hash='#hashtag'" completionHandler:nil];
        
        //return YES;
    }
    
    if (!navigationAction.targetFrame) {
        [activityIndicator stopAnimating];
        activityIndicator.hidden = TRUE;
        
        
        NSURL *url = navigationAction.request.URL;
        
        if( [[url absoluteString] containsString:@"itunes.apple.com"]){
            UIApplication *app = [UIApplication sharedApplication];
            if ([app canOpenURL:url]) {
                [app openURL:url];
            }
            decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }
        else{
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [self.webView loadRequest:request];
        }
        
        
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    // return YES;
}
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction *action) {
                                                          completionHandler();
                                                      }]];
    [self presentViewController:alertController animated:YES completion:^{}];
}


//webkit authentication challenge delegate
- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler
{
    NSURLCredential * credential = [[NSURLCredential alloc] initWithTrust:[challenge protectionSpace].serverTrust];
    completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    [activityIndicator startAnimating];
    DLog(@"Did start loading: %@ auth:%d", [[request URL] absoluteString], _authenticated);
    
    if (!_authenticated && hDLongDesc.length == 0) {
        _authenticated = NO;
        
        urlConnection = [[NSURLConnection alloc] initWithRequest:_request delegate:self];
        
        [urlConnection start];
        
        return NO;
    }
    
    
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        
        if ([[[request URL] absoluteString] containsString:@"itunes.apple.com"]) {
            [UtilityManager customUrlLoad:[request URL]];
        }
        else if ([[[request URL] absoluteString] containsString:@"3000.htm"]) {
            
            [mWebView stopLoading];
           
            newUrl = [[[request URL] absoluteString] copy];
           
            [self showDialog];
            
            return  NO;
        }
        
    }
    
    
    
    if ([[[request URL] scheme] isEqual:@"mailto"])
    {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[request URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,(CFStringRef)value,CFSTR("")));
                    
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            // [mailViewController release];
            return NO;
            
        }
        
        else {
            [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
        }
        
        return NO;
    }
    
    if ([[request URL] fragment]) {
        
        return YES;
        [webView stringByEvaluatingJavaScriptFromString:@"window.location.hash='#hashtag'"];
        
        return YES;
    }
    
    
    
    return YES;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

//- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
//{
//	[self dismissModalViewControllerAnimated:YES];
//}

#pragma mark - NURLConnection delegate

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    DLog(@"WebController Got auth challange via NSURLConnection");
    
    if ([challenge previousFailureCount] == 0)
    {
        _authenticated = YES;
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        
        [challenge.sender useCredential:credential forAuthenticationChallenge:challenge];
        
    } else
    {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;
{
    DLog(@"WebController received response via NSURLConnection");
    NSString* mimeType = [response MIMEType];
    DLog(@"Mime Type:%@",mimeType);
    
    // remake a webview call now that authentication has passed ok.
    _authenticated = YES;
    [webView loadRequest:connection.originalRequest];
    
    // Cancel the URL connection otherwise we double up (webview + url connection, same url = no good!)
    [urlConnection cancel];
}

// We use this method is to accept an untrusted site which unfortunately we need to do, as our PVM servers are self signed.
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}
//https support

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
    [self updateButtons];
    
    
}



- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    [activityIndicator stopAnimating];
    
    [self updateButtons];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0 && [[webView stringByEvaluatingJavaScriptFromString:@"document.readyState"] isEqualToString:@"complete"])
    {
        if(!isFirstWebPage)
        {
            contentHeight = mWebView.scrollView.contentSize.height; isFirstWebPage = true;
        }
        if ( mWebView.scrollView.contentSize.height < contentHeight && !mWebView.canGoBack)
        {
            [mWebView reload];
            contentHeight = 0;
        }
    }
    [mWebView setHidden:FALSE];
    
    
}




- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [activityIndicator stopAnimating];
    [self updateButtons];
}





-(void)returnToMainPage:(id)sender
{
   // [self removeCache];
    showMapFlag=FALSE;
    anythingShareFlag=FALSE;
    requestAnythingFromMainMenu=FALSE;
    shareFlag=FALSE;
    
    CommonUtility *common = [[CommonUtility alloc] init];
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        [common popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
    
    
    
}

#pragma mark sender user tracking
-(void)userTracking:(NSString*)xmlStr
{
    
    NSMutableString *urlStr = [[NSMutableString alloc]init];
    [urlStr appendFormat:@"%@ratereview/sendersharetrack",BASE_URL];
    DLog(@"%@",urlStr);
    NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:xmlStr]];
    DLog(@"%@",response);
    //[xmlStr release];
}



-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}



#pragma mark share methods

-(void)shareClicked
{
    if (shareFlag==TRUE) {
        iWebRequestState = SPECIAL_OFFER_SHARE;
    }
    else if (anythingShareFlag ==TRUE)
    {
        iWebRequestState = ANYTHING_SHARE;
    }
    
    
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    [reqStr appendFormat:@"<ShareProductInfo>"];
    
    if (requestAnythingFromMainMenu==FALSE) {
        [reqStr appendFormat:@"<retailerId>%@</retailerId>",[defaults valueForKey:KEY_RETAILERID ]];
    }
    
    
    
    //    [reqStr appendFormat:@"<ShareProductInfo><retailerId>%@</retailerId>",[defaults valueForKey:KEY_RETAILERID ]];
    if (shareFlag==TRUE && [defaults valueForKey:KEY_PAGEID]) {
        [reqStr appendFormat:@"<pageId>%@</pageId>",[defaults valueForKey:KEY_PAGEID]];
    }
    else if (anythingShareFlag==TRUE)
    {
        [reqStr appendFormat:@"<pageId>%@</pageId>",[defaults valueForKey:KEY_ANYPAGEID]];
    }
    
    [reqStr appendFormat:@"<userId>%@</userId><hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharespecialoff",BASE_URL];
    DLog(@"%@",urlString);
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    
    [self responseData:response];
    
    //[reqStr release];
    
}


-(void)shareDetails{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        [self shareClicked];
        
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        // [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
        
    }
}


-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    if (shareFlag==TRUE) {
        anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"ANYTHING_SHARE"];
    }
    else if(anythingShareFlag==TRUE)
    {
        anyVC.fbShareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
    }
    
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
                NSMutableString *userXmlStr;
                if (shareFlag==TRUE) {
                    userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><sPageId>%@</sPageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PAGEID]];
                }
                else {
                    if (requestAnythingFromMainMenu==FALSE) {
                        userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><aPageId>%@</aPageId><retId>%@</retId><retLocId>%@</retLocId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_ANYPAGEID],[defaults valueForKey:KEY_RETAILERID],[defaults valueForKey:@"retailLocationID"]];
                    }else{
                        userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><aPageId>%@</aPageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_ANYPAGEID]];
                    }
                }
                
                
                [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller.view removeFromSuperview];
            [defaults setValue:@"0" forKey:@"PresentActionSheet"];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self.view addSubview:controller.view];
        [self.view bringSubviewToFront:controller.view];
        
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    //[defaults setObject:PRODUCT_SHARE forKey:KEY_SHARE_TYPE];
    emailSendingVC = [[EmailShareViewController alloc]
                      initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    if (anythingShareFlag==true) {
        emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"ANYTHING_SHARE"];
    }
    else if(shareFlag==true)
    {
        emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"SPECIAL_OFFER_SHARE"];
    }
    __typeof(self) __weak  obj = self;
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
}

-(void) showActionSheet {
    [defaults setValue:@"1" forKey:@"PresentActionSheet"];
    NSString *titleString;
    if (shareFlag==TRUE)
        titleString = @"Share Special Offer Via";
    else
        titleString =@"Share Anything Page Via";
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:titleString message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    //    if(IPAD){
    //        actionSheet.popoverPresentationController.sourceView = btnCalendar;
    //        actionSheet.popoverPresentationController.sourceRect =  btnCalendar.bounds;
    //    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}


-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
            NSMutableString *userXmlStr;
            if (shareFlag==TRUE) {
                userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><pageId>%@</pageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_PAGEID]];
            }
            else {
                userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam><aPageId>%@</aPageId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:KEY_ANYPAGEID]];
            }
            
            
            [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    [defaults setValue:@"0" forKey:@"PresentActionSheet"];
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark parse method
-(void)responseData: (NSString*)response{
    
    switch (iWebRequestState) {
            
        case SPECIAL_OFFER_SHARE:
        case ANYTHING_SHARE:
            [self parse_SpecialOfferShare:response];
            break;
        default:
            break;
    }
    
    
}

-(void)parse_SpecialOfferShare:(NSString*)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        //        TBXMLElement *retailerDetails = [TBXML childElementNamed:@"RetailersDetails" parentElement:tbXml.rootXMLElement];
        
        //        if (retailerDetails!=nil) {
        TBXMLElement *shareText=[TBXML childElementNamed:@"pageTitle" parentElement:tbXml.rootXMLElement];
        TBXMLElement *emailURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *imagePath = [TBXML childElementNamed:@"retImage" parentElement:tbXml.rootXMLElement];
        
        if (emailURL!=nil) {
            [defaults setValue:[TBXML textForElement:emailURL] forKey:KEY_HYPERLINK];
            
        }
        
        if (shareText!=nil) {
            NSMutableString *msgString = [[NSMutableString alloc]init];
            if (shareFlag==YES) {
                [msgString appendFormat:@"I found this special offer page in the %@ mobile app and thought you might be interested:",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            }
            else
            {
                [msgString appendFormat:@"I found this anything page in the %@ mobile app and thought you might be interested:",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            }
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [msgString appendFormat:@"%@",shareString];
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            
        }
        
        if (imagePath!=nil) {
            [defaults setObject:[TBXML textForElement:imagePath] forKey:KEY_PRODUCTIMGPATH];
        }
        
        //        }
        
        
        
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            return;
        }
        
    }
    
}

-(void) viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [self destroyWebView];
    [super didReceiveMemoryWarning];
    
}
- (void)destroyWebView
{
    [self.webView loadHTMLString:@"" baseURL:nil];
    [self.webView stopLoading];
    [self.webView setNavigationDelegate:nil];
    [self.webView removeFromSuperview];
    [self setWebView:nil];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[NSURLCache sharedURLCache] setDiskCapacity:0];
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
}

@end

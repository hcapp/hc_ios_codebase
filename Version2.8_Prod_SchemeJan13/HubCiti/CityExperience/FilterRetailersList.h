//
//  PartnersListViewController.h
//  Scansee
//
//  Created by Ajit on 2/18/13.
//
//

#import <UIKit/UIKit.h>
#import "Mapkit/MKMapView.h"
#import "RetailerSummaryViewController.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "SwipesViewController.h"
#import "GetUserInfoResponse.h"
NSDictionary *viewDictionary;
@class DealHotDealsList;
@class RetailersListViewController;
@class CityExperienceViewController;
@class FundraiserListViewController;
@class EventsListViewController;
@class FAQCategoryList;
@class AnyViewController;
@class EmailShareViewController;
BOOL fromFilters;
NSMutableArray *globalLatArray,*globalLongArray, *globalCompleteAddress;
NSMutableArray *globalNameArray,*globalAddressArray;
NSMutableArray *globalFilterRetailerArray;
NSMutableArray *globalRetailerListID, *globalRetailerID,*globalRetLocId;


@interface FilterRetailersListDO : NSObject{
    
    NSString *retailerName, *retailerId, *retailLocationID, *retailAddress, *retailAddress2, *retailCity, *retailState,*retailPostalCode, *retailOpenClose,*retailCompleteAddress;
    NSString *retRowNumber;
    NSString *retLatitude, *retLongitude, *retDistance, *retLogoImgPath;
    NSString *retbannerAdImgPath, *retRibbonAdImgPath, *retRibbonAdUrl, *retSaleFlag;
    NSString *retListId;
}
@property (nonatomic, strong)NSString *retRowNumber;
@property (nonatomic, strong)NSString *retailerName, *retailerId, *retailLocationID,*retailAddress, *retailAddress2,*retailCity, *retailState,*retailPostalCode,*retailOpenClose,*retailCompleteAddress;
@property (nonatomic, strong)NSString *retLatitude, *retLongitude, *retDistance, *retLogoImgPath;
@property (nonatomic, strong)NSString *retbannerAdImgPath, *retRibbonAdImgPath, *retRibbonAdUrl, *retSaleFlag;
@property (nonatomic, strong)NSString *retListId;


@end




@interface FilterRetailersList : UIViewController<UITableViewDelegate,UITableViewDataSource,MKMapViewDelegate,MFMessageComposeViewControllerDelegate, SwipesViewControllerDelegate,UITextViewDelegate,CustomizedNavControllerDelegate>{
    
    
    SortAndFilter *sortFilObject;
    
    SwipesViewController *iSwipeViewController;
    
    UITableView *partnersTable;
    int maxCount;
    int nextPage,lastVisitedRecord;
    NSMutableArray *rowNumberArray,*retailerIdArray,*retailerNameArray,*retailLocationIDArray;
    NSMutableArray *logoImagePathArray,*distanceArray,*retailAddressArray,*retailAddress2Array,*retailerCityArray,*retailerStateArray, *retailerPostalCodeArray,*bannerAdImagePathArray, *completeAddressArray;
    NSMutableArray *ribbonAdImagePathArray,*ribbonAdURLArray,*retListIDArray;
    
    NSMutableArray *saleFlagArray;
    MKMapView *mapView;
    NSMutableArray *annArray, *latArray, *longArray;
    BOOL isOnlyOnePartner;
    BOOL filterDescription;
    NSString *filterDesc;
    WebRequestState iWebRequestState;
    CommonUtility *common;
    RetailerSummaryViewController *rsvc;
    NSMutableArray *bottomButtonList;
    int bottomBtn;
    
    NSMutableArray *filterRetailersListObjArray;
    UITextView* textView;
    CustomizedNavController *cusNav;
}

@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,retain) FAQCategoryList *faqList;
@property(nonatomic,retain) UITextView* textView;
@property(nonatomic,retain) EventsListViewController* iEventsListViewController;
@property(nonatomic,retain) FundraiserListViewController *iFundraiserListViewController;
@property (assign)BOOL isOnlyOnePartner;
@property (assign)BOOL filterDescription;
@property (nonatomic,strong) NSString *filterDesc;
@property(nonatomic,strong) CityExperienceViewController *cityExp;
@property(nonatomic,strong) RetailersListViewController* retailerListScreen;
@property(nonatomic,strong) SortAndFilter *sortFilObject,*savedSortObj;
@property(nonatomic,strong) DealHotDealsList* hotDeals;
@property(nonatomic,strong) FilterRetailersList *filters;
@end

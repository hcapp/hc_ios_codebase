//
//  ClaimBusinessViewController.m
//  HubCiti
//
//  Created by Nikitha on 8/18/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "ClaimBusinessViewController.h"
#import "CustomAlertViewController.h"
#import "AppDelegate.h"
#import "ClaimBusinessResponse.h"
#import "RetListClaimBusResponse.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "WebBrowserViewController.h"
#import "ClaimDropDownViewController.h"
#define NUMBERS	@"0123456789"
#define LEGAL	@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "


@interface ClaimBusinessViewController ()<HTTPClientDelegate,UIPopoverPresentationControllerDelegate,ResetCategoryFieldDelegate>
{
    UITextField * activeField;
    
}
@property (nonatomic, strong) UIPopoverPresentationController *userDataPopover;
@property(nonatomic,retain) ClaimBusinessResponse * claimBusResponse ;
@end

@implementation ClaimBusinessViewController
@synthesize retailerLocationID,claimBusResponse,msgInPopup,list_OfAppsiteURL,list_OfAppsiteName,claimEmailID,claimRetailerAddress,claimRetailerName,customer_InfoData,bannerWebView,contactInfoField,emailAddressField,submitBtnOutlet,imageViewBannerOutlet;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerForKeyboardNotifications];
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"Business Info" forView:self withHambergur:NO];
    //self.navigationItem.title = @"Business Info";
    NSLog(@"Scroll height %f Viedw Height %f",_claimFormScrollView.frame.size.height,_claimFormView.frame.size.height);

   self.navigationItem.hidesBackButton = YES;
    
    //home button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30,30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    
   //set text to all the fields
    [self setContentInForm];
    
    
    //done and cancel button in number pad
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    postalCodeFiled.inputAccessoryView = numberToolbar;
    phoneNumberField.inputAccessoryView = numberToolbar;
    
    //keyboard disappear on tag outside the screen
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
 
    
    [self.view addGestureRecognizer:tap];

}
-(void)dismissKeyboard {
    [activeField resignFirstResponder];
}

-(void)cancelNumberPad{
    [activeField resignFirstResponder];
}
-(void)doneWithNumberPad{
    [activeField resignFirstResponder];
    
}


-(void) alertControllerDismiss: (id) alert
{
   // _claimFormScrollView.pagingEnabled = YES;
    [alert dismissViewControllerAnimated:YES completion:nil];
    
}


-(void) setContentInForm
{
    businessnameLabel.attributedText = [self getTagColor:businessnameLabel.text];
    typeOfBusinessLabel.attributedText = [self getTagColor:typeOfBusinessLabel.text];
    addressLabel.attributedText = [self getTagColor:addressLabel.text];
    phoneNumberLabel.attributedText = [self getTagColor:phoneNumberLabel.text];
    addressLabel.attributedText = [self getTagColor:addressLabel.text];
    cityLabel.attributedText = [self getTagColor:cityLabel.text];
    stateLabel.attributedText = [self getTagColor:stateLabel.text];
    postalLabel.attributedText = [self getTagColor:postalLabel.text];
    countryLabel.attributedText = [self getTagColor:countryLabel.text];
    contactInfoLabel.attributedText = [self getTagColor:contactInfoLabel.text];
    contactNameLabel.attributedText = [self getTagColor:contactNameLabel.text];
    streetAddressLabel.attributedText = [self getTagColor:streetAddressLabel.text];
    emailAddressLabel.attributedText = [self getTagColor:emailAddressLabel.text];
    
    
    //autopopulate all the data in the form
    businessNameField.text = [customer_InfoData name];
    typeOfBusinessField.text = [customer_InfoData type];
    websiteField.text = [customer_InfoData website];
    addressLine1Field.text = [customer_InfoData retAddress];
    addressLine2Field.text = [customer_InfoData retAddress2];   
    cityField.text = [customer_InfoData city];
    stateField.text = [customer_InfoData state];
    postalCodeFiled.text = [customer_InfoData postalCode];
    countryField.text = [customer_InfoData country];
    phoneNumberField.text = [customer_InfoData phone];
    
    
    //keywordTextField.text
    keywordTextField.text = [customer_InfoData keywords];
    
    
    
    
    
    if([customer_InfoData claimImg])
    {
        imageViewBannerOutlet.hidden = NO;
        bannerWebView.hidden = YES;
        NSString *utf = [[customer_InfoData claimImg] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
        imageViewBannerOutlet.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:utf]]];
    }
    else if ([customer_InfoData claimTxt])
    {
        imageViewBannerOutlet.hidden = YES;
        bannerWebView.hidden = NO;
        if (IPAD)
        {
             [bannerWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:19px;font-family:Helvetica;text-align: center;color:#000000;word-wrap: break-word'>%@",[customer_InfoData claimTxt]] baseURL:nil];
        }
        else
        {
           [bannerWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;text-align: center;color:#000000;word-wrap: break-word'>%@",[customer_InfoData claimTxt]] baseURL:nil];
        }
       
    }
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBar.hidden = NO;
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
   // _claimFormView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT+VARIABLE_HEIGHT(100));
    
    
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}

-(void)returnToMainPage:(id)sender{
    
    
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{

    [HubCitiAppDelegate removeActivityIndicator];
    [self parse_claimBusiness:responseObject];
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}
-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ([[[inRequest URL] scheme] isEqual:@"mailto"]) {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,(CFStringRef)value,
                                                                                                      CFSTR("")));
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            // [mailViewController release];
            return NO;
            
        }
        
        else {
            [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
            
            
        }
        
        return NO;
        
    }
    
    if ([[[inRequest URL] scheme] isEqual:@"tel"]) {
        NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
        
        NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
        NSString *phoneString  = [[defaultRecipient componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        NSString *phoneNumber = [NSString stringWithFormat:@"tel:%@", phoneString];
        NSURL    *url= [NSURL URLWithString:phoneNumber];
        
        UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        webview.alpha = 0.0;
        
        [webview loadRequest:[NSURLRequest requestWithURL:url]];
        
        // Assume we are in a view controller and have access to self.view
        [self.view insertSubview:webview belowSubview:bannerWebView];
   
        // [webview release];
        
        
        //        if ([[UIApplication sharedApplication] canOpenURL:[inRequest URL]])
        //        {
        //            NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
        //
        //            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
        //            NSString *phoneNumber = [@"tel://" stringByAppendingString:defaultRecipient];
        //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
        //
        //            return NO;
        //        }
        return NO;
    }
    
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        
        NSString *url = [[inRequest URL]absoluteString];
        [defaults setValue:url forKey:KEY_URL];
        //[defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
        
        WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:urlDetail animated:NO];
        //[urlDetail release];
        
        return NO;
    }
    return YES;
}

-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}
#pragma mark validations


-(BOOL)validateEmailAddress : (NSString *) email
{
    NSString *emailRegEx = @"[a-zA-Z0-9.\\-_]{1,32}@[a-zA-Z0-9.\\-_]{2,32}\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if ([emailTest evaluateWithObject:email] == YES)
        return TRUE;
    else
        return FALSE;
    
}


- (BOOL)myMobileNumberValidate:(NSString*)number
{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
        return TRUE;
    else
        return FALSE;
}
- (BOOL)pincodeValidate:(NSString*)number
{
    NSString *numberRegEx = @"[0-9]{5}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
        return TRUE;
    else
        return FALSE;
}
-(BOOL) urlValidate:(NSString *) url
{
    NSString *urlRegEx = @"((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?";
    //@"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    if ([nameTest evaluateWithObject:url] == YES)
        return TRUE;
    else
        return FALSE;
    

}
- (BOOL)usernameValidation:(NSString*)name
{
    NSString *nameRegEx = @"[A-Za-z ]{1,200}";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegEx];
    if ([nameTest evaluateWithObject:name] == YES)
        return TRUE;
    else
        return FALSE;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
// code for backspace detection
//    const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
//    int isBackSpace = strcmp(_char, "\b");
//    
//    if (isBackSpace == -8) {
//        NSLog(@"Backspace was pressed");
//        return NO;
//    }
    
    NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:LEGAL];
    NSCharacterSet *numCharSet = [NSCharacterSet characterSetWithCharactersInString:NUMBERS];
    switch (textField.tag) {
        case 1:{//businessname
            
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++)
                {
                    NSUInteger textLength = [textField.text length] + [string length] - range.length;
                    return (textLength > 200)? NO : YES;
                    
                    
                }
                
            }
        }
            break;
          
        case 3:{//website
            
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++)
                {
                    NSUInteger textLength = [textField.text length] + [string length] - range.length;
                    return (textLength > 200)? NO : YES;
                }
                
            }
        }
            break;
        case 14 ://keywords
        {
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++)
                {
                    NSUInteger textLength = [textField.text length] + [string length] - range.length;
                    return (textLength > 25)? NO : YES;
                }
                
            }

        }
            break;
        case 4:
        case 15:{//address 1
            
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++)
                {
                    NSUInteger textLength = [textField.text length] + [string length] - range.length;
                    return (textLength > 500)? NO : YES;
                }
                
            }
        }
            break;
        case 5:
        case 16:{//address 2
            
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++)
                {
                    NSUInteger textLength = [textField.text length] + [string length] - range.length;
                    return (textLength > 500)? NO : YES;
                }
                
            }
        }
            break;
        case 6:
        case 17:{//city
            
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++)
                {
                    unichar c = [string characterAtIndex:j];
                    if ([charSet characterIsMember:c]) {
                        NSUInteger textLength = [textField.text length] + [string length] - range.length;
                        return (textLength > 200)? NO : YES;
                    }
                    else {
                        return NO;
                    }
                    
                }
                
            }
        }
            
            
            break;
        case 7:
        case 18:{//state
            
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++)
                {
                    unichar c = [string characterAtIndex:j];
                    if ([charSet characterIsMember:c]) {
                        NSUInteger textLength = [textField.text length] + [string length] - range.length;
                        return (textLength > 200)? NO : YES;
                    }
                    else {
                        return NO;
                    }
                    
                }
                
            }
        }
            break;
        case 8:
        case 19://postal code
        {
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++) {
                    unichar c = [string characterAtIndex:j];
                    if ([numCharSet characterIsMember:c]) {
                        NSUInteger textLength = [textField.text length] + [string length] - range.length;
                        return (textLength > 5)? NO : YES;
                    }
                    else {
                        return NO;
                    }
                }
            }
        }
            break;
        case 9:
        case 20:{//country
            
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++)
                {
                    unichar c = [string characterAtIndex:j];
                    if ([charSet characterIsMember:c]) {
                        NSUInteger textLength = [textField.text length] + [string length] - range.length;
                        return (textLength > 200)? NO : YES;
                    }
                    else {
                        return NO;
                    }
                    
                }
                
            }
        }
            break;

        case 11:
        case 21://phone number
        {
            if (range.length == 1){
                return YES;
            }
            else {
                for (int j = 0; j<[string length]; j++) {
                    unichar c = [string characterAtIndex:j];
                    if ([numCharSet characterIsMember:c]) {
                        NSUInteger textLength = [textField.text length] + [string length] - range.length;
                        return (textLength > 10)? NO : YES;
                    }
                    else {
                        return NO;
                    }
                }
            }
        }
            break;
        default:
            return YES;
            break;
    }
    
    return YES;
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

//- (NSTimeInterval)keyboardAnimationDurationForNotification:(NSNotification*)notification
//{
//    NSDictionary* info = [notification userInfo];
//    NSValue* value = [info objectForKey:UIKeyboardAnimationDurationUserInfoKey];
//    NSTimeInterval duration = -2;
//    [value getValue:&duration];
//    return duration;
//}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.claimFormScrollView.contentInset = contentInsets;
    self.claimFormScrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height + (IPAD?100:75), 0.0);
    self.claimFormScrollView.contentInset = contentInsets;
    self.claimFormScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height + (IPAD?100:75);
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.claimFormScrollView scrollRectToVisible:activeField.frame animated:YES];
    }
}


-(NSMutableAttributedString*) getTagColor : (NSString*) text
{
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:text];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(text.length-1,1)];
    
    return string;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
      _claimFormScrollView.pagingEnabled = NO;
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    _claimFormScrollView.pagingEnabled = YES;
    activeField = textField;
 
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [activeField resignFirstResponder];
    
}
-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    scrollView.pagingEnabled = NO;
 }
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void) request_claimBusinnes
{

     NSString * textForPhone = [NSString stringWithFormat:@"%@",phoneNumberField.text];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:claimEmailID forKey:@"loginEmail"];
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    [parameters setValue:retailerLocationID forKey:@"retLocationId"];
    [parameters setValue:businessNameField.text forKey:@"name"];
    [parameters setValue:typeOfBusinessField.text forKey:@"type"];
    [parameters setValue:websiteField.text forKey:@"website"];
    [parameters setValue:addressLine1Field.text forKey:@"retAddress"];
    [parameters setValue:addressLine2Field.text forKey:@"retAddress2"];
    [parameters setValue:cityField.text forKey:@"city"];
    [parameters setValue:stateField.text forKey:@"state"];
    [parameters setValue:postalCodeFiled.text forKey:@"postalCode"];
    [parameters setValue:countryField.text forKey:@"country"];
    [parameters setValue:textForPhone forKey:@"phone"];
    [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    
    //contact name
    if (contactInfoField.text.length > 0)
    {
        [parameters setValue:contactInfoField.text forKey:@"custInfo"];
    }
    
    //email address
    if (emailAddressField.text.length > 0)
    {
        [parameters setValue:emailAddressField.text forKey:@"custEmail"];
    }
   
  
    //keywords
    if (keywordTextField.text.length > 0)
    {
        [parameters setValue:keywordTextField.text forKey:@"keywords"];
    }
    
    //mailing address 
    if (mailingAddress1TxtField.text.length > 0 && mailingAddress2TxtField.text.length > 0 && mailingCityTxtField.text.length > 0 && mailingStateTxtField.text.length > 0 && mailingPostalTxtField.text.length > 0 && mailingCountryTxtField.text.length > 0 && mailingPhNumTxtField.text.length > 0)
    {
        [parameters setValue:mailingAddress1TxtField.text forKey:@"mailAddress"];
        [parameters setValue:mailingAddress2TxtField.text forKey:@"mailAddress2"];
        [parameters setValue:mailingCityTxtField.text forKey:@"mailCity"];
        [parameters setValue:mailingStateTxtField.text forKey:@"mailState"];
        [parameters setValue:mailingPostalTxtField.text forKey:@"mailPostalCode"];
        [parameters setValue:mailingCountryTxtField.text forKey:@"mailCountry"];
        [parameters setValue:mailingPhNumTxtField.text forKey:@"mailPhone"];
        [parameters setValue:@"true" forKey:@"isMailAddress"];
    }
    else{
        [parameters setValue:@"false" forKey:@"isMailAddress"];
    }
    
    DLog(@"parameter: %@",parameters);
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
//     NSString *urlString = [NSString stringWithFormat:@"http://10.10.220.231:9990/HubCiti2.9/thislocation/claimyourbusiness"];
   NSString *urlString = [NSString stringWithFormat:@"%@thislocation/claimyourbusiness",BASE_URL];
    DLog(@"Url: %@",urlString);
    [HubCitiAppDelegate showActivityIndicator];
    [client sendRequest : parameters : urlString];
    
}


-(void) parse_claimBusiness :(id)response
{
    if (response == nil)
        return;
    if (claimBusResponse == nil) {
        claimBusResponse = [[ClaimBusinessResponse alloc] init];
    }
    [claimBusResponse setValuesForKeysWithDictionary:response];
    NSLog(@"response = %@", claimBusResponse);
    if ([claimBusResponse.responseCode isEqualToString:@"10000"] )
    {
        if(claimBusResponse.retDetailList != nil)
        {
            for (int i = 0; i< claimBusResponse.retDetailList.count; i++)
            {
                RetListClaimBusResponse * retListClaimRes = [[RetListClaimBusResponse alloc]init];
                NSDictionary * dict_retListClaimBus = claimBusResponse.retDetailList[i];
                [retListClaimRes setValuesForKeysWithDictionary:dict_retListClaimBus];
                if(retListClaimRes.claimTxtMsg != nil)
                {
                    msgInPopup = retListClaimRes.claimTxtMsg;
                }
                if(retListClaimRes.claimCategory != nil)
                {
                    list_OfAppsiteName = [retListClaimRes.claimCategory componentsSeparatedByString:@","];
                    NSLog(@"Appsite names %@",list_OfAppsiteName);
                }
                if(retListClaimRes.claimCategoryURL != nil)
                {
                    list_OfAppsiteURL = [retListClaimRes.claimCategoryURL componentsSeparatedByString:@","];
                    NSLog(@"Appsite url %@",list_OfAppsiteName);
                }
            }
        }
       
        
        [defaults setBool:YES forKey:@"isComingFromCliam"];
        NSString *url = list_OfAppsiteURL[0];
        [defaults  setObject:url forKey:KEY_URL];
        NSLog(@"Send response  %@ Default string %@",[defaults  objectForKey:@"weatherURL"],[defaults valueForKey:KEY_URL]);
        WebBrowserViewController *webView = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        webView.comingFromClaim = YES;
        [self.navigationController pushViewController:webView animated:NO];
        
    }
    else
    {
        [UtilityManager showAlert:nil msg:claimBusResponse.responseText];

    }
}
- (IBAction)claimDropDownButtonPressed:(id)sender
{
    NSLog(@"Array %@",[defaults valueForKey:KEY_RESPONSEXML]);
    ClaimDropDownViewController *claimDropDownVC = [[ClaimDropDownViewController alloc] initWithNibName:@"ClaimDropDownViewController" bundle:nil];
    claimDropDownVC.contentInTypeOfBusiness = typeOfBusinessField.text;
    claimDropDownVC.resetcatDelegate = self;
    claimDropDownVC.modalPresentationStyle = UIModalPresentationPopover;
    _userDataPopover = [claimDropDownVC popoverPresentationController];
    _userDataPopover.delegate = self;
    _userDataPopover.permittedArrowDirections = UIPopoverArrowDirectionUp;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        claimDropDownVC.preferredContentSize = CGSizeMake((SCREEN_WIDTH/2)+ 50, SCREEN_HEIGHT/2);
    }
    _userDataPopover.sourceRect = _dropDownButton.frame;
    _userDataPopover.sourceView = _claimFormView;
    [self presentViewController:claimDropDownVC animated:YES completion:nil];
}
-(void) updateCategoryTest:(NSString *)categoryName
{
    typeOfBusinessField.text = categoryName;
}
-(UIModalPresentationStyle) adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller
{
    return UIModalPresentationNone;
}
- (IBAction)submitBtnPressed:(id)sender
{
    _claimFormScrollView.pagingEnabled = NO;
    businessNameField.text = [businessNameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    typeOfBusinessField.text = [typeOfBusinessField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    websiteField.text = [websiteField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    addressLine1Field.text = [addressLine1Field.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    addressLine2Field.text = [addressLine2Field.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    cityField.text = [cityField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    stateField.text = [stateField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    countryField.text = [countryField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    contactInfoField.text = [contactInfoField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    mailingAddress1TxtField.text = [mailingAddress1TxtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    mailingAddress2TxtField.text = [mailingAddress2TxtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    keywordTextField.text = [keywordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    mailingCityTxtField.text = [mailingCityTxtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    mailingStateTxtField.text = [mailingStateTxtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    mailingPostalTxtField.text = [mailingPostalTxtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    mailingCountryTxtField.text = [mailingCountryTxtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    mailingPhNumTxtField.text = [mailingPhNumTxtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (![businessNameField.text length] || ![typeOfBusinessField.text length] || ![addressLine1Field.text length]  || ![cityField.text length] || ![stateField.text length] || ![postalCodeFiled.text length] || ![countryField.text length]  || ![phoneNumberField.text length] || ![contactInfoField.text length]|| ![emailAddressField.text length] )
    {
        UIAlertController * blankAlert;
        blankAlert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter missing fields", @"Please enter missing fields") message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action){
                                 //_claimFormScrollView.pagingEnabled = YES;
                             }];
        
        [blankAlert addAction:ok];
        [self presentViewController:blankAlert animated:YES completion:nil];
        [self performSelector:@selector(alertControllerDismiss:) withObject:blankAlert afterDelay:1.5];
        return;
    }
    
    else if ([businessNameField.text length] > 200)
    {
        
        UIAlertController * pwdAlert;
        pwdAlert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Business Name should be less than 200 characters", @"Business Name should be less than 200 characters") message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action){
                                 // _claimFormScrollView.pagingEnabled = YES;
                             }];
        
        [pwdAlert addAction:ok];
        [self presentViewController:pwdAlert animated:YES completion:nil];
        [self performSelector:@selector(alertControllerDismiss:) withObject:pwdAlert afterDelay:1.5];
        return;
    }
    else if ([websiteField.text length] > 200)
    {
        
        UIAlertController * pwdAlert;
        pwdAlert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Website should be less than 200 characters", @"Website should be less than 200 characters") message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action){
                                 // _claimFormScrollView.pagingEnabled = YES;
                             }];
        [pwdAlert addAction:ok];
        [self presentViewController:pwdAlert animated:YES completion:nil];
        [self performSelector:@selector(alertControllerDismiss:) withObject:pwdAlert afterDelay:1.5];
        return;
    }
    else if (![self urlValidate:websiteField.text]&& websiteField.text.length!=0)
    {
        UIAlertController * alert;
        alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter valid Website", @"Please enter valid Website") message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action){
                                 //_claimFormScrollView.pagingEnabled = YES;
                             }];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        [self performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:1.5];
        return;
        
    }
    else if ([keywordTextField.text length] > 400)
    {
        
        UIAlertController * pwdAlert;
        pwdAlert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Keywords should be less than 400 characters", @"Keywords should be less than 400 characters") message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action){
                                 // _claimFormScrollView.pagingEnabled = YES;
                             }];
        [pwdAlert addAction:ok];
        [self presentViewController:pwdAlert animated:YES completion:nil];
        [self performSelector:@selector(alertControllerDismiss:) withObject:pwdAlert afterDelay:1.5];
        return;
    }

    else if ([addressLine1Field.text length] > 500 || [mailingAddress1TxtField.text length] >500)
    {
        
        UIAlertController * pwdAlert;
        if([addressLine1Field.text length] > 500)
        {
            pwdAlert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Street Address should be less than 500 characters", @"Street Address should be less than 500 characters") message:nil preferredStyle:UIAlertControllerStyleAlert];
        }
        else {
            pwdAlert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Mailing Street Address should be less than 500 characters", @"Mailing Street Address should be less than 500 characters") message:nil preferredStyle:UIAlertControllerStyleAlert];
        }
        
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action){
                                 //_claimFormScrollView.pagingEnabled = YES;
                             }];
        
        [pwdAlert addAction:ok];
        [self presentViewController:pwdAlert animated:YES completion:nil];
        [self performSelector:@selector(alertControllerDismiss:) withObject:pwdAlert afterDelay:1.5];
        return;
    }
    
    else
        if ([cityField.text length] > 200 || [mailingCityTxtField.text length] > 200)
        {
          
            UIAlertController * pwdAlert;
            if ([cityField.text length] > 200)
            {
                pwdAlert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"City should be less than 200 characters", @"City should be less than 200 characters") message:nil preferredStyle:UIAlertControllerStyleAlert];
            }
            else
            {
                pwdAlert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Mailing City should be less than 200 characters", @"Mailing should be less than 200 characters") message:nil preferredStyle:UIAlertControllerStyleAlert];
            }
            
            
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction * action){
                                     // _claimFormScrollView.pagingEnabled = YES;
                                 }];
            
            [pwdAlert addAction:ok];
            [self presentViewController:pwdAlert animated:YES completion:nil];
            [self performSelector:@selector(alertControllerDismiss:) withObject:pwdAlert afterDelay:1.5];
            return;
        }
        else if ((![self usernameValidation:cityField.text]&& cityField.text.length!=0) || (![self usernameValidation:mailingCityTxtField.text]&& mailingCityTxtField.text.length!=0))
        {
            UIAlertController * alert;
            if (![self usernameValidation:cityField.text]&& cityField.text.length!=0)
            {
                alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter valid City", @"Please enter valid City") message:nil preferredStyle:UIAlertControllerStyleAlert];
            }
            
            else
            {
                alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter valid Mailing City", @"Please enter valid Mailing City") message:nil preferredStyle:UIAlertControllerStyleAlert];
            }
            
            
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction * action){
                                     // _claimFormScrollView.pagingEnabled = YES;
                                 }];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            [self performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:1.5];
            return;
            
        }
    
        else
            if ([stateField.text length] > 200 || [mailingStateTxtField.text length] > 200)
            {
                UIAlertController * pwdAlert;
                if ([stateField.text length] > 200)
                {
                    pwdAlert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"State/Province should be less than 200 characters", @"State/Province should be less than 200 characters") message:nil preferredStyle:UIAlertControllerStyleAlert];
                }
                else
                {
                    pwdAlert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Mailing State/Province should be less than 200 characters", @"Mailing State/Province should be less than 200 characters") message:nil preferredStyle:UIAlertControllerStyleAlert];
                }
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleCancel
                                     handler:^(UIAlertAction * action){
                                         //_claimFormScrollView.pagingEnabled = YES;
                                     }];
                
                [pwdAlert addAction:ok];
                [self presentViewController:pwdAlert animated:YES completion:nil];
                [self performSelector:@selector(alertControllerDismiss:) withObject:pwdAlert afterDelay:1.5];
                return;
            }
            else if ((![self usernameValidation:stateField.text]&& stateField.text.length!=0) || (![self usernameValidation:mailingStateTxtField.text]&& mailingStateTxtField.text.length!=0))
            {
                UIAlertController * alert;
                if (![self usernameValidation:stateField.text]&& stateField.text.length!=0)
                {
                    alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter valid State/Province", @"Please enter valid State/Province") message:nil preferredStyle:UIAlertControllerStyleAlert];
                }
                else
                {
                    alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter valid Mailing State/Province", @"Please enter valid Mailing State/Province") message:nil preferredStyle:UIAlertControllerStyleAlert];
                }
                
                
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleCancel
                                     handler:^(UIAlertAction * action){
                                         // _claimFormScrollView.pagingEnabled = YES;
                                     }];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                [self performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:1.5];
                return;
                
            }
    
            else if((![self pincodeValidate:postalCodeFiled.text]&& postalCodeFiled.text.length!=0) || (![self pincodeValidate:mailingPostalTxtField.text]&& mailingPostalTxtField.text.length!=0))
            {
                UIAlertController * alert;
                if (![self pincodeValidate:postalCodeFiled.text]&& postalCodeFiled.text.length!=0)
                {
                    alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter valid Postal / Zip Code", @"Please enter valid Postal / Zip Code") message:nil preferredStyle:UIAlertControllerStyleAlert];
                }
                else
                {
                    alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter valid Mailing Postal / Zip Code", @"Please enter valid Mailing Postal / Zip Code") message:nil preferredStyle:UIAlertControllerStyleAlert];
                }
                
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleCancel
                                     handler:^(UIAlertAction * action){
                                         // _claimFormScrollView.pagingEnabled = YES;
                                     }];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                [self performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:1.5];
                return;
                
            }
            else if ([countryField.text length] > 200 || [mailingCountryTxtField.text length] >200)
            {
                
                
                UIAlertController * pwdAlert;
                if ([countryField.text length] > 200)
                {
                    pwdAlert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Country should be less than 200 characters", @"Country should be less than 200 characters") message:nil preferredStyle:UIAlertControllerStyleAlert];
                }
                else
                {
                    pwdAlert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Mailing Country should be less than 200 characters", @"Mailing Country should be less than 200 characters") message:nil preferredStyle:UIAlertControllerStyleAlert];
                }
                
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleCancel
                                     handler:^(UIAlertAction * action){
                                         // _claimFormScrollView.pagingEnabled = YES;
                                     }];
                
                [pwdAlert addAction:ok];
                [self presentViewController:pwdAlert animated:YES completion:nil];
                [self performSelector:@selector(alertControllerDismiss:) withObject:pwdAlert afterDelay:1.5];
                return;
            }
            else if ((![self usernameValidation:countryField.text]&& countryField.text.length!=0) || (![self usernameValidation:mailingCountryTxtField.text]&& mailingCountryTxtField.text.length!=0))
            {
                UIAlertController * alert;
                if (![self usernameValidation:countryField.text]&& countryField.text.length!=0)
                {
                    alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter valid Country", @"Please enter valid Country") message:nil preferredStyle:UIAlertControllerStyleAlert];
                }
                else
                {
                    alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter valid Mailing Country", @"Please enter valid Mailing Country") message:nil preferredStyle:UIAlertControllerStyleAlert];
                }
                
                
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleCancel
                                     handler:^(UIAlertAction * action){
                                         //_claimFormScrollView.pagingEnabled = YES;
                                     }];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                [self performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:1.5];
                return;
                
            }
    
    
            else if((![self myMobileNumberValidate:phoneNumberField.text]&& phoneNumberField.text.length!=0) || (![self myMobileNumberValidate:mailingPhNumTxtField.text]&& mailingPhNumTxtField.text.length!=0))
            {
                UIAlertController * alert;
                if (![self myMobileNumberValidate:phoneNumberField.text]&& phoneNumberField.text.length!=0)
                {
                    alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter valid Phone Number", @"Please enter valid Phone Number") message:nil preferredStyle:UIAlertControllerStyleAlert];
                }
                else
                {
                    alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter valid Mailing Phone Number", @"Please enter valid Mailing Phone Number") message:nil preferredStyle:UIAlertControllerStyleAlert];
                }
                
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleCancel
                                     handler:^(UIAlertAction * action){
                                         // _claimFormScrollView.pagingEnabled = YES;
                                     }];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                [self performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:1.5];
                return;
                
            }
            else if (![self validateEmailAddress:emailAddressField.text] && emailAddressField.text.length !=0)
            {
                UIAlertController * alert;
                alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter valid Email Address", @"Please enter valid Email Address") message:nil preferredStyle:UIAlertControllerStyleAlert];
                
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleCancel
                                     handler:^(UIAlertAction * action){
                                         // _claimFormScrollView.pagingEnabled = YES;
                                     }];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                [self performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:1.5];
                return;
                
            }
            else if ([mailingAddress1TxtField.text length] || [mailingAddress2TxtField.text length]|| [mailingCityTxtField.text length] || [mailingStateTxtField.text length] || [mailingCountryTxtField.text length] || [mailingPostalTxtField.text length]|| [mailingPhNumTxtField.text length]){
             
                
                if(! ([mailingAddress1TxtField.text length] && [mailingAddress2TxtField.text length] && [mailingCityTxtField.text length] && [mailingStateTxtField.text length] && [mailingCountryTxtField.text length] && [mailingPostalTxtField.text length] && [mailingPhNumTxtField.text length])){
                    
                    UIAlertController * alert;
                    alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter all the fields of mailing address", @"Please enter all the fields of mailing address") message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleCancel
                                         handler:^(UIAlertAction * action){
                                             // _claimFormScrollView.pagingEnabled = YES;
                                         }];
                    
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    [self performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:1.5];
                    return;
                }
                else{
                     [self request_claimBusinnes];
                }
            }
            else
            {
                [self request_claimBusinnes];
                NSLog(@"DHF");
            }
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
@end

//
//  CurrentSpecialsViewController.m
//  HubCiti
//
//  Created by Kitty on 27/04/15.
//  Copyright (c) 2015 Keshavakarthik S. All rights reserved.
//

#import "CurrentSpecialsViewController.h"
#import "WebBrowserViewController.h"
#import "MainMenuViewController.h"
#import "CurrentSpecials.h"
#import "FundraiserListViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@interface CurrentSpecialsViewController ()

@end

@implementation CurrentSpecialsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    
    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    
    self.title = @"Specials";
    
    [self setTopBannerView];
    
    //[back release];
    // Call service to check the falg values for al tabs
    [self request_retspedealoff];
    tblCurrentSpecial.delegate = self;
    tblCurrentSpecial.dataSource = self;
}
-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}
-(void)popBackToPreviousPage{
    
    [self.navigationController popViewControllerAnimated:NO];
}

// Set Either RibbonBanner Image or Retailer Title for top view
-(void)setTopBannerView
{
    
    float ribbonHeight = 50.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        
        ribbonHeight = 80.0;
    }
    NSString *ribbonAdImagePathStr = [defaults  objectForKey:@"ribbonAdImagePath"];
    float Ypos = 0;
    
    
    if ([ribbonAdImagePathStr isEqualToString:@"N/A"] == NO)
    {
        
        UIButton *ribbonAdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [ribbonAdBtn addTarget:self action:@selector(ribbonAdBtnPressed)forControlEvents:UIControlEventTouchUpInside];
        ribbonAdBtn.frame = CGRectMake(0.0,0.0, SCREEN_WIDTH, ribbonHeight);//width 320.0
        
        NSString *str = [ribbonAdImagePathStr stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
        
        NSString *ribbonAdImageURLStr = [NSString stringWithFormat:@"%@", str];
        NSURL *ribbonAdImageURL = [NSURL URLWithString:ribbonAdImageURLStr];
        UIImage *ribbonAdImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:ribbonAdImageURL]];
        [ribbonAdBtn setImage:ribbonAdImage forState:UIControlStateNormal];
        
        
        UILabel *lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(ribbonAdBtn.frame.origin.x, ribbonAdBtn.frame.origin.y, ribbonAdBtn.frame.size.width, ribbonAdBtn.frame.size.height+2)];
        lineLabel.backgroundColor = [UIColor blackColor];
        [self.view addSubview:lineLabel];
        //[lineLabel release];
        
        
        
        [self.view addSubview:ribbonAdBtn];
        
        Ypos = ribbonHeight;
        
        if (IOS7)
        {
            tblCurrentSpecial.frame = CGRectMake(0, Ypos,SCREEN_WIDTH, SCREEN_HEIGHT-Ypos-100);//width 320
        }
        else
        {
            tblCurrentSpecial.frame = CGRectMake(0, Ypos, SCREEN_WIDTH, SCREEN_HEIGHT-Ypos-60);//width 320
        }
        
    }
    else
    {
        
        
        if (IOS7)
            tblCurrentSpecial.frame = CGRectMake(0, Ypos, SCREEN_WIDTH, SCREEN_HEIGHT-Ypos);//width 320
        else
            tblCurrentSpecial.frame = CGRectMake(0, Ypos, SCREEN_WIDTH, SCREEN_HEIGHT-Ypos-60);//width 320
        
    }
}


-(void)ribbonAdBtnPressed {
    NSString *url = [defaults  objectForKey:@"ribbonAdURL"];
    [defaults  setObject:url forKey:KEY_URL];
    //[HubCitiManager setShareFromTL:YES];
    [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
    WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:urlDetail animated:NO];
    //[urlDetail release];
}


-(void)request_retspedealoff
{
    //    iWebRequestState = RETSPEDEALOFF;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[defaults valueForKey:@"retailLocationID"]];
    [requestStr appendFormat:@"<retailId>%@</retailId>",[defaults valueForKey:KEY_RETAILERID]];
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></ProductDetailsRequest>",[defaults valueForKey:KEY_HUBCITIID]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retspedealoff",BASE_URL];
    
    NSString *rep = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    
    //    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    [self parse_retspedealoff:rep];
    
    //[requestStr release];
    
    
}

//????????
// Parse Sales Product Details
-(void)parse_retspedealoff:(NSString *)responseXml
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        
        TBXMLElement *SpecialOfferElement = [TBXML childElementNamed:@"SpecialOffer" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mColorElement = [TBXML childElementNamed:@"mColor" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mFontColorElement = [TBXML childElementNamed:@"mFontColor" parentElement:tbxml.rootXMLElement];
        if (mColorElement!=nil){
            NSString *mColor = [[TBXML textForElement:mColorElement]copy];
            cellColor = [UIColor convertToHexString:mColor];
        }
        if (mFontColorElement!=nil){
            NSString *mFontColor = [[TBXML textForElement:mFontColorElement]copy];
            fontColor = [UIColor convertToHexString:mFontColor];
        }
        
        if(SpecialOfferElement)
        {
            TBXMLElement *specialNames = [TBXML childElementNamed:@"specialNames" parentElement:SpecialOfferElement];
            NSString *splNames = [TBXML textForElement:specialNames];
            
            NSArray *splArray = [splNames componentsSeparatedByString:@","];
            NSLog(@"%@",splArray);
            hotDealsArray = [[NSMutableArray alloc]initWithArray:splArray];//initWithObjects:@"Coupons",@"Sales",@"Hot deals",@"Special offers",nil]
            NSLog(@"%@",hotDealsArray);
        }
    }
    
    
    
    
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    [tblCurrentSpecial reloadData];
    
}

/*
 *
 Navigate back to Main Menu screen page
 *
 */
-(void)returnToMainPage:(id)sender
{
    
    //return to main page
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSLog(@"%lu",(unsigned long)[hotDealsArray count]);
    return [hotDealsArray count];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        return 70.0;
    }
    return 50.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellCurrentSpecialOffer";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (cell) {
            cell.backgroundColor =  cellColor;
        }
        cell.layer.borderColor = [UIColor blackColor].CGColor;
    }
    else
    {
        NSArray *arr = [cell.contentView subviews];
        for(int i=0; i<[arr count]; i++)
        {
            UIView *view = [arr objectAtIndex:i];
            [view removeFromSuperview];
        }
        
        cell.userInteractionEnabled = YES;
    }
    // AsyncImageView *asyncImageView = nil;
    UILabel *label = nil;
    // UILabel *detailLabel = nil;
    //float      discountPriceFont = 12.0;
    float  label3Font = 14.0;
   // float  asyncImageViewHeight = 50.0, labelXValue = 60.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
       // textLabelFont = 17.0;
       // detailLabelFont = 17.0;
        //labelFont = 20.0;
      //  label2Font = 21.0;
        label3Font = 19.0;
        
       // priceLabelFont = 17.0;
       // salePriceFont = 17.0;
       // regPriceFont = 17.0;
        //discountPriceFont = 17.0;
        
      //  asyncImageViewWidth = 60.0;
      //  asyncImageViewHeight = 60.0;
       // labelXValue = 70.0;
    }
    
    
    if ( [hotDealsArray count] > indexPath.row ){
        label = [[UILabel alloc]initWithFrame:CGRectMake(15, 7, SCREEN_WIDTH - 70, 40)];//width 250
        label.font = [UIFont boldSystemFontOfSize:label3Font];
        if (fontColor) {
            [label setTextColor:fontColor];
        }
        label.numberOfLines = 2;
        label.text = [hotDealsArray objectAtIndex:indexPath.row];
        [cell.contentView addSubview:label];
        ReleaseAndNilify(label);
    }
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    CurrentSpecials *spl = [[CurrentSpecials alloc]initWithNibName:@"CurrentSpecials" bundle:nil
                            ];
    NSLog(@"%lu",(long)indexPath.row);
    NSLog(@"%@",hotDealsArray);
    if ([[hotDealsArray objectAtIndex:indexPath.row] isEqualToString:@"Sales"]) {
      //  spl.salesFlag =YES;
        [self.navigationController pushViewController:spl animated:NO];
    }
    
    
    else if([[hotDealsArray objectAtIndex:indexPath.row] isEqualToString:@"Special Offers"])
    {
      //  spl.specialOffFlag =YES;
        [self.navigationController pushViewController:spl animated:NO];
        
    }
    else if([[hotDealsArray objectAtIndex:indexPath.row] isEqualToString:@"Deals"])
    {
       // spl.hotDealFlag =YES;
        [self.navigationController pushViewController:spl animated:NO];
    }
    
    else if([[hotDealsArray objectAtIndex:indexPath.row] isEqualToString:@"Coupons"])
    {
        
        
        spl.couponFlag =YES;
        [self.navigationController pushViewController:spl animated:NO];
        
    }
    
    else if([[hotDealsArray objectAtIndex:indexPath.row] isEqualToString:@"Fundraisers"])
    {
        FundraiserListViewController *lSpl = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:nil];
        self.iFundraiserListViewController = lSpl;
        lSpl.retailId =  [defaults valueForKey:KEY_RETAILERID];
        lSpl.retailLocationId = [defaults valueForKey:@"retailLocationID"] ;
        lSpl.isFundraiserEvent = TRUE;
        [self.navigationController pushViewController:lSpl animated:YES];
        ReleaseAndNilify(lSpl);
    }
    
    
    
    [tblCurrentSpecial deselectRowAtIndexPath:[tblCurrentSpecial indexPathForSelectedRow] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

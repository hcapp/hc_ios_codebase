//
//  FilterListViewController.m
//  HubCiti
//
//  Created by Keerthi on 10/07/14.
//  Copyright (c) 2014 span infotech india ptv. ltd. All rights reserved.
//

#import "FilterListViewController.h"
#import "SettingsViewController.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "ButtonSetupViewController.h"
#import "SESpringBoard.h"
#import "bottomButtonView.h"
#import "CityExperienceViewController.h"
#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "SettingsViewController.h"
#import "FindViewController.h"
#import "AnythingPage.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "AppDelegate.h"
#import "SingleCatRetailers.h"
//#import "ScanNowScreen.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "EventsListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FAQCategoryList.h"
#import "FilterListViewController.h"
#import "bottomButtonDO.h"
#import "FundraiserListViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "SpecialOffersViewController.h"
#import "HTTPClient.h"
#import "GetUserInfoResponse.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponsInDealViewController.h"
@interface FilterListViewController ()
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@end

@implementation FilterListViewController{
    UIActivityIndicatorView *loading;
    bottomButtonView *view;
   
}
NSMutableArray *partnerListObjArray;
//@synthesize anyVC;
@synthesize infoResponse;
@synthesize anyVC,emailSendingVC;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - Service Respone methods



- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}





- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject

{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    
    
    
    [self responseData:responseObject];
    
    
    
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    [defaults setInteger:0 forKey:@"no_items"];
    [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    // Do any additional setup after loading the view from its nib.
    loading = nil;
    [self.view setAccessibilityLabel:@"FiltersList"];
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    UIButton *optBtn = [UtilityManager customizeBarButtonItem:@"Map/Sort"];
    [optBtn addTarget:self action:@selector(showOptions:) forControlEvents:UIControlEventTouchUpInside];
    optBtn.tag=1;
    opt = [[UIBarButtonItem alloc] initWithCustomView:optBtn];
    
    
    
    common = [[CommonUtility alloc]init];
    
    //customize back button
    
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    [self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"Mainmenu"];
    //    //[mainPage release];
    // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    
    //    titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 320, 40)];
    //    titleLabel.textAlignment = NSTextAlignmentCenter;
    //    titleLabel.textColor = [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]];
    //    titleLabel.backgroundColor = [UIColor clearColor];
    //    titleLabel.numberOfLines = 2;
    //    [titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    //    titleLabel.font = [UIFont boldSystemFontOfSize:16];
    //    titleLabel.text = NSLocalizedString(@"Choose Filter:",@"Choose Filter:");
//    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
//    [cus setTitle:@"Select Interest Area:" forView:self withHambergur:YES];
    //self.navigationItem.title = @"Choose Filter:";
    partnerListObjArray = [[NSMutableArray alloc]init];
    [self request_GetFilters];
    //[titleLabel release];
    
    
}

- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}


-(void)returnToMainPage:(id)sender{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


-(void)viewWillAppear:(BOOL)animated{
    
    if ([defaults integerForKey:@"no_items"] == 1) {
        
        [defaults setInteger:0 forKey:@"no_items"];
        [self.navigationController popViewControllerAnimated:NO];
    }
    
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideBackButton:NO];
    [cusNav hideHambergerButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    [defaults setValue:nil forKey:@"FilterRetTitle"];
    //    partnerListObjArray = [[NSMutableArray alloc]init];
    //    [self request_GetFilters];
    [super viewWillAppear:animated];
    
    
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}

-(void)releaseArrays{
    if (partnerListObjArray)
        ReleaseAndNilify(partnerListObjArray);
}


#pragma mark navigation bar button actions
-(void)popBackToPreviousPage{
    //[defaults setBool:NO forKey:@"ViewMore"];
    [linkID removeLastObject];
    
    [self.navigationController popViewControllerAnimated:NO];
}





-(void)settableViewOnScreen
{
    if(table_PartnerRetailerList==nil){
        int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
        if(bottomBtn==1){
            table_PartnerRetailerList = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-50) style:UITableViewStylePlain];
        }
        else{
            table_PartnerRetailerList = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-yVal) style:UITableViewStylePlain];
        }
        table_PartnerRetailerList.dataSource=self;
        table_PartnerRetailerList.delegate=self;
        [table_PartnerRetailerList setBackgroundColor:[UIColor whiteColor]];
        table_PartnerRetailerList.tableFooterView.hidden=YES;
        [table_PartnerRetailerList setAccessibilityIdentifier:@"filtersTable"];
        [self.view addSubview:table_PartnerRetailerList];
    }
}

-(void)request_GetFilters{
    
    NSString *urlString = [[NSString alloc]init];
    if([defaults valueForKey:KEY_MITEMID] && [defaults valueForKey:KEY_BOTTOMBUTTONID])
    {
        urlString = [NSString stringWithFormat:@"%@thislocation/getpartners?citiExperId=%@&userId=%@&bottomBtnId=%@&menuItemId=%@",BASE_URL,[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_BOTTOMBUTTONID],[defaults valueForKey:KEY_MITEMID]];
    }
    else if (([defaults valueForKey:KEY_MITEMID]==nil) && [defaults valueForKey:KEY_BOTTOMBUTTONID])
    {
        urlString = [NSString stringWithFormat:@"%@thislocation/getpartners?citiExperId=%@&userId=%@&bottomBtnId=%@",BASE_URL,[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    else if ([defaults valueForKey:KEY_MITEMID] && ([defaults valueForKey:KEY_BOTTOMBUTTONID] == nil))
    {
        urlString = [NSString stringWithFormat:@"%@thislocation/getpartners?citiExperId=%@&userId=%@&menuItemId=%@",BASE_URL,[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_MITEMID]];
    }
    else if (([defaults valueForKey:KEY_MITEMID] == nil) && ([defaults valueForKey:KEY_BOTTOMBUTTONID] == nil))
    {
        urlString = [NSString stringWithFormat:@"%@thislocation/getpartners?citiExperId=%@&userId=%@",BASE_URL,[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_USERID]];
    }
    iWebRequestState = GETPARTNERS;
    [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    
}




//parse filters list
-(void)parse_GetPartners:(NSString *)responseXml
{
    NSLog(@"%@",[defaults valueForKey:KEY_MAINMENUID]);
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbXml.rootXMLElement]]intValue];
    if ([[TBXML textForElement:responseCodeElement]intValue]==10000) {
        
        // [partnerListObjArray removeAllObjects];
        
        TBXMLElement *maxCountElement = [TBXML childElementNamed:@"maxCnt" parentElement:tbXml.rootXMLElement];
        maxCount = [[TBXML textForElement:maxCountElement]intValue];
        [SharedManager setFiltersCount:maxCount];
        
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbXml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        
        TBXMLElement *CitiExperienceElement = [TBXML childElementNamed:@"CitiExperience" parentElement:tbXml.rootXMLElement];
        
        while (CitiExperienceElement != nil) {
            partnerListDO = [[CEPartnerListDO alloc]init];
            /*
             *since pagination is not implemented remove all objects of array every time parsing. After implementing pagination check if (lastVisitedRecord == 0) then remove all objects
             *
             */
            
            TBXMLElement *retAffIdElement = [TBXML childElementNamed:@"retAffId" parentElement:CitiExperienceElement];
            TBXMLElement *retAffNameElement = [TBXML childElementNamed:@"retAffName" parentElement:CitiExperienceElement];
            TBXMLElement *retAffImgElement = [TBXML childElementNamed:@"retAffImg" parentElement:CitiExperienceElement];
            
            if (retAffIdElement != nil) {
                partnerListDO.PLretailerId = [TBXML textForElement:retAffIdElement];
            }
            
            if (retAffNameElement != nil) {
                partnerListDO.PLretailerName = [TBXML textForElement:retAffNameElement];
                DLog(@"%@",partnerListDO.PLretailerName);
            }
            if (retAffImgElement != nil) {
                partnerListDO.PLretailerLogo = [TBXML textForElement:retAffImgElement];
            }
            
            [partnerListObjArray addObject:partnerListDO];
            CitiExperienceElement = [TBXML nextSiblingNamed:@"CitiExperience" searchFromElement:CitiExperienceElement];
        }
        
        if(bottomBtn==1){
            bottomButtonList = [[NSMutableArray alloc]init];
            
            TBXMLElement *bottomList = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbXml.rootXMLElement];
            
            TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomList];
            
            while (BottomButtonElement != nil) {
                
                bottomButtonDO *bottomButton = [[bottomButtonDO alloc]init];
                
                TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                
                if(bottomBtnIDElement)
                    bottomButton.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                
                if(bottomBtnNameElement)
                    bottomButton.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                else
                    bottomButton.bottomBtnName = @" ";
                
                if(bottomBtnImgElement)
                    bottomButton.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                else
                    bottomButton.bottomBtnImg = @" ";
                
                if(bottomBtnImgOffElement)
                    bottomButton.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                else
                    bottomButton.bottomBtnImgOff = @" ";
                
                if(btnLinkTypeIDElement)
                    bottomButton.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                
                if(btnLinkTypeNameElement)
                    bottomButton.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                
                if(btnLinkIDElement){
                    bottomButton.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                    
                }
                
                if(positionElement)
                    bottomButton.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                
                
                
                BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                [bottomButtonList addObject:bottomButton];
                //[bottomButton release];
            }
        }
        
        if (partnerListObjArray.count == 1) {
            
            [defaults setValue:[[partnerListObjArray objectAtIndex:0]PLretailerName] forKey:@"Title"];
            [defaults setValue:[defaults valueForKey:@"Title"] forKey:@"FilterRetTitle"];
            [SharedManager setRetAffId:[[[partnerListObjArray objectAtIndex:0]PLretailerId]intValue]];
            [defaults setValue:[[partnerListObjArray objectAtIndex:0]PLretailerId] forKey:@"filterRetAffId"];
            
            FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
            [defaults setInteger:1 forKey:@"no_items"];
            self.filters = filters;
            
            [self.navigationController pushViewController:filters animated:NO];
            
            return;
        }
        
        if ([bottomButtonList count]>0) {
            [self setBottomBarMenu:bottomButtonList];
        }
        
        
        
        
        [self settableViewOnScreen];
        [table_PartnerRetailerList reloadData];
    }
    
    // Added outside the if-else so that bottom buttons are displayed
    // even if no records are found.
    else if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10005"])
    {
        
        
        //@Deepak: Parsed MainMenuID for userTracking
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbXml.rootXMLElement];
        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbXml.rootXMLElement]]intValue];
        if(mainMenuIdElement !=nil)
        {
            [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        if(bottomBtn==1){
            
            TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbXml.rootXMLElement];
            
            if(bottomButtonListElement !=nil)
            {
                TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
                bottomButtonList=[[NSMutableArray alloc]init];
                while(BottomButtonElement)
                {
                    
                    bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                    
                    
                    TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                    TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                    
                    
                    if(bottomBtnIDElement)
                        obj_findBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                    
                    if(bottomBtnNameElement)
                        obj_findBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                    else
                        obj_findBottomDO.bottomBtnName = @" ";
                    
                    if(bottomBtnImgElement)
                        obj_findBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                    else
                        obj_findBottomDO.bottomBtnImg = @" ";
                    
                    if(bottomBtnImgOffElement)
                        obj_findBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                    else
                        obj_findBottomDO.bottomBtnImgOff = @" ";
                    
                    if(btnLinkTypeIDElement)
                        obj_findBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                    
                    if(btnLinkTypeNameElement)
                        obj_findBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                    
                    if(btnLinkIDElement){
                        obj_findBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                        if(btnLinkTypeNameElement){
                            if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
                                [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
                        }
                    }
                    
                    if(positionElement)
                        obj_findBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                    
                    
                    
                    BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                    [bottomButtonList addObject:obj_findBottomDO];
                    //[obj_findBottomDO release];
                }
                if([bottomButtonList count] > 0)
                {
                    // Set Bottom Menu Button after read from response data
                    [self setBottomBarMenu:bottomButtonList];
                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                }
                else
                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            }
        }
        
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        [self settableViewOnScreen];
        [table_PartnerRetailerList reloadData];
    }
    
    else {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
       
    }
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"Select Interest Area:" forView:self withHambergur:YES];
    
}


-(void) setBottomBarMenu : (NSMutableArray *)bottomList
{
    NSMutableArray *arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-55, SCREEN_WIDTH, 55)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    
    for(int btnLoop= 0; btnLoop < [bottomList count]; btnLoop++)
    {
        bottomButtonDO *obj_bottomButtonDO = [bottomList objectAtIndex:btnLoop];
        
        view = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_bottomButtonDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-55, 80,55) imageName:obj_bottomButtonDO.bottomBtnImg img_off:obj_bottomButtonDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            view = [bottomButtonView initWithTitle:obj_bottomButtonDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[bottomList count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[bottomList count],bottomBarButtonHeight) imageName:obj_bottomButtonDO.bottomBtnImg img_off:obj_bottomButtonDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_bottomButtonDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
    }
    
}


-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}




// This method will call when any Bottom Button Will touched
-(void)bottomButtonPressed:(id)sender
{
    //[defaults setBool:NO forKey:@"ViewMore"];
    [defaults setBool:YES forKey:BottomButton];
    //    [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int) btn.tag;
    
    //NSlog(@"The tag is %d",tag);
    bottomButtonDO *obj_bottomBottomDO ;
    obj_bottomBottomDO = [bottomButtonList objectAtIndex:tag];
    if(!([obj_bottomBottomDO.btnLinkTypeName isEqualToString:@"Map"]||[obj_bottomBottomDO.btnLinkTypeName isEqualToString:@"SortFilter"])||[[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=28)
        [defaults setValue:obj_bottomBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    if ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_bottomBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_bottomBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_bottomBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_bottomBottomDO.btnLinkTypeName);
    if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            self.iEventsListViewController = iEventsListViewController;
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            // //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    
    else{ //other menu types
        switch ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                self.hotDeals=hotDeals;
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                // [hotDeals release];
                break;
                
            case 1://Scan Now
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                [EventsBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [NearbyBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                [self navigateToFindView];
            }
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                self.cityExp = citi;
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
                
                break;
                
            case 8://Visitors Bureau
                
                break;
                
            case 9://transportation
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=TRUE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_bottomBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
            
                
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                // [dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                self.faqList = faqCatList;
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                [FundraiserBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }break;
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        //  [inform release];
                    }
                }
                
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            /// [citPref release];
                        }
                    }
                }
                
            }
                break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                //privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
                
            default:
                break;
        }
    }
    
}

#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        return;
    
    [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    [self showActionSheet];
    
}

-(void) showActionSheet {
  
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }

    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init];
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}



-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark request methods

//get austin retailers
-(void)request_CitiExpRet{
    
    iWebRequestState = CITIEXPRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId><hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if(![defaults boolForKey:BottomButton])
    {
        if([defaults valueForKey:KEY_MITEMID])
            [requestStr appendFormat:@"<mItemId>%@</mItemId>",[defaults valueForKey:KEY_MITEMID]];
    }
    else{
        if([defaults valueForKey:KEY_BOTTOMBUTTONID]){
            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
            [defaults setBool:NO forKey:BottomButton];}
    }
    
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [requestStr appendFormat:@"<sortColumn>%@</sortColumn>",[defaults valueForKey:@"SortCitiExpBy"]];
    [requestStr appendFormat:@"<sortOrder>ASC</sortOrder>"];
    
    if ([SharedManager localSpecilas]) {
        [requestStr appendFormat:@"<locSpecials>1</locSpecials>"];
    }
    else{
        [requestStr appendFormat:@"<locSpecials>0</locSpecials>"];
    }
    
    //    [requestStr appendFormat:@"<catIds>0</catIds>"];
    
    [requestStr appendFormat:@"<citiExpId>%@</citiExpId><lowerLimit>0</lowerLimit></ThisLocationRequest>",[linkID objectAtIndex:[linkID count]-1]];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/citiexpret",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);
}



//get user favorite categories
//-(void)request_GetUserCat{
//
//    iWebRequestState = GETFAVCATEGORIES;
//    if([Network currentReachabilityStatus]==0)
//    {
//        [common showAlert];
//    }
//    else {
//
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//
//    }
//}

-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}

-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=TRUE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}







//-(void) navigateToScanNow
//{
//    [defaults setBool:YES forKey:@"KEY_SCANSPLASH"];
//    [defaults setValue:@"scanNow" forKey:KEY_SEARCHFROM];
//    [defaults setObject:@"YES" forKey:@"KEY_SCANNERCALLED"];
//
//    int menulevel = [[defaults valueForKey:KEY_MENULEVEL]integerValue];
//    if(menulevel > 1)
//        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
//    else
//        [defaults  setBool:NO forKey:@"DisplayBackBarButton"];
//
//
//    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
//    ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];
//    [self.navigationController pushViewController:viewScanNowScreen animated:NO];
//    [viewScanNowScreen release];
//}


-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}

-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    self.iEventsListViewController = iEventsListViewController;
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}


-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}



-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            if(menulevel > 1)
                [defaults  setBool:NO forKey:@"showBackButton"];
            else
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}

#pragma mark parse methods
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
            
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
        case CITIEXPRET:
            
            [self parse_CitiExpRet:response];
            
            break;
            //        case PARTNERRET:
            //            [self parse_PartnerRet:response];
            //            break;
        case GETPARTNERS:
            [self parse_GetPartners:response];
            break;
            
        case GETFAVLOCATIONS:
            [self parse_GetUserCat:response];
            break;
            
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
            
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
            //        case catsearch:
            //            [self parse_sscatsearch:response];
            //            break;
            
        default:
            break;
    }
}


-(void)parse_CitiExpRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [defaults setObject:nil forKey:@"Response_Austin"];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            [defaults setObject:response forKey:@"Response_Austin"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            self.cityExp = cevc;
            [defaults setInteger:100 forKey:@"CityExpTableTag"];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            //[cevc release];
        }
    }
}

//parse user favorite categories
-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}



#pragma mark tableView delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    if ((PLnextPage == 1)|| (PLlastVisitedRecord != 0))
        return ([partnerListObjArray count] + 1);
    else
        return [partnerListObjArray count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 60;
    }
    else
    {
        return 80;
    }
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"Cell";
    static NSString *cellIdentifier1 = @"More Cell";
    
    SdImageView *asyncImageView = nil;
    UILabel *label = nil;
    UITableViewCell *cell;
    
    
    if (indexPath.row != [partnerListObjArray count])
        cell = [table_PartnerRetailerList dequeueReusableCellWithIdentifier:cellIdentifier];
    else
        cell = [table_PartnerRetailerList dequeueReusableCellWithIdentifier:cellIdentifier1];
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    if (indexPath.row != [partnerListObjArray count]) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] ;
        
        CGRect frame;
        frame.origin.x = 5;
        frame.origin.y = 7;
        frame.size.width = 44;
        frame.size.height = 44;
        asyncImageView = [[SdImageView alloc] initWithFrame:frame] ;
        //   asyncImageView.contentMode = UIViewContentModeScaleAspectFill;
        asyncImageView.backgroundColor = [UIColor clearColor];
        asyncImageView.translatesAutoresizingMaskIntoConstraints=NO;
        [cell.contentView addSubview:asyncImageView];
        
        
        label = [[UILabel alloc] initWithFrame:frame];
        label.textColor = [UIColor colorWithRGBInt:0x112e72];
        label.translatesAutoresizingMaskIntoConstraints=NO;
        [cell.contentView addSubview:label];
        
        NSDictionary *viewsDictionary = @{@"image":asyncImageView,@"label":label};
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(7)-[image(44)]"] options:0 metrics:0 views:viewsDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(11)-[label(30)]"] options:0 metrics:0 views:viewsDictionary]];
            
            label.font = [UIFont boldSystemFontOfSize:16];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(44)]"] options:0 metrics:0 views:viewsDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(52)-[label(250)]"] options:0 metrics:0 views:viewsDictionary]];
            
            
        }
        else
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[image(70)]"] options:0 metrics:0 views:viewsDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(44)]"] options:0 metrics:0 views:viewsDictionary]];
            
            
            label.font = [UIFont boldSystemFontOfSize:20];
            
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(70)]"] options:0 metrics:0 views:viewsDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[label(600)]"] options:0 metrics:0 views:viewsDictionary]];
            
        }
        
        
        
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    else
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier1] ;
    
    if (indexPath.row == [partnerListObjArray count] && PLnextPage == 1) {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        // [cell.contentView addSubview:loading];
        //[loading startAnimating];
        
        //                label = [[[UILabel alloc] init] ;
        //                label.font = [UIFont boldSystemFontOfSize:16];
        //                label.textAlignment = NSTextAlignmentCenter;
        //                label.textColor = [UIColor colorWithRGBInt:0x112e72];
        //                label.text = @"View More Results...";
        //                label.translatesAutoresizingMaskIntoConstraints=NO;
        //                [cell.contentView addSubview:label];
        //
        //                NSDictionary *vDictionary=@{@"label":label};
        //                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[label(24)]-|"] options:0 metrics:0 views:vDictionary]];
        //
        //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(320)]-|"] options:0 metrics:0 views:vDictionary]];
        //                    label.font = [UIFont boldSystemFontOfSize:16];
        //                }
        //                else
        //                {
        //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[label(44)]-|"] options:0 metrics:0 views:vDictionary]];
        //
        //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(768)]-|"] options:0 metrics:0 views:vDictionary]];
        //                    label.font = [UIFont boldSystemFontOfSize:20];
        //                }
        
    }
    
    else if([partnerListObjArray count]>indexPath.row) {
        DLog(@"index:%ld", (long)indexPath.row);
        NSString *logoImageStr = [[partnerListObjArray objectAtIndex:indexPath.row]PLretailerLogo];
        if(![logoImageStr isEqualToString:@"N/A"] )
        {
            logoImageStr = [logoImageStr stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
            [asyncImageView loadImage:[NSString stringWithFormat:@"%@", logoImageStr]];
        }
        
        label.text = [NSString stringWithFormat:@"%@", [[partnerListObjArray objectAtIndex:indexPath.row]PLretailerName]];
        DLog(@"%@",label.text);
        
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [defaults setValue:[[partnerListObjArray objectAtIndex:indexPath.row]PLretailerName] forKey:@"Title"];
    [defaults setValue:[defaults valueForKey:@"Title"] forKey:@"FilterRetTitle"];
    [SharedManager setRetAffId:[[[partnerListObjArray objectAtIndex:indexPath.row]PLretailerId]intValue]];
    [defaults setValue:[[partnerListObjArray objectAtIndex:indexPath.row]PLretailerId] forKey:@"filterRetAffId"];
    //            [self request_PartnerRet:[[[partnerListObjArray objectAtIndex:indexPath.row]PLretailerId]intValue]];
    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
    self.filters = filters;
    // [defaults setValue:retAffName forKey:@"Title"];
    [self.navigationController pushViewController:filters animated:NO];
    
    [table_PartnerRetailerList deselectRowAtIndexPath:[table_PartnerRetailerList indexPathForSelectedRow] animated:YES];
    //[filters release];
    
}


-(void)navigateToFindView
{
    
    //    [defaults setValue:@"0" forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    
    navigatedFromMainMenu = FALSE;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)requestToGetUserData
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
//    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//    ReleaseAndNilify(xmlStr);
//}

-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}
-(void)parseGetUserData:(id)response
{    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}

-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}

@end

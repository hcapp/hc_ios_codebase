//
//  CustomAlertViewController.m
//  HubCiti
//
//  Created by Nikitha on 8/19/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "CustomAlertViewController.h"
#import "WebBrowserViewController.h"

@interface CustomAlertViewController ()

@end

@implementation CustomAlertViewController
@synthesize navVC,appsiteURL,appsiteNames,msg;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.1]];
   // UIButton *backBtn = [UtilityManager customizeBackButton];
    
//    [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    self.navigationItem.leftBarButtonItem = back;
    self.navigationItem.hidesBackButton = YES;
    [self setUpUI];
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void) setUpUI
{
    float YVal = 0;
    
    //alertview
    //    NSMutableArray * appsiteArray = [[ NSMutableArray alloc]initWithObjects:@"One one one one one one one one one",@"Two",@"Three",@"four",@"Five", nil];
    //    NSString * msg =@" Look at the below appsite if interested click on it";
    
    UIFont * msgFont = [UIFont boldSystemFontOfSize:(IPAD ? 17.0f:14.0f)];
    float  width =VARIABLE_WIDTH(200) ;
    float  height = [self getLabelSize:msg withSize: width-VARIABLE_WIDTH(20) withFont:msgFont] + appsiteNames.count*msgFont.lineHeight;
    
    UIView * alertView = [[UIView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH/2) - (width/2) , (SCREEN_HEIGHT/2) - ((height+VARIABLE_HEIGHT(60)+VARIABLE_HEIGHT(10*appsiteNames.count))/2), width, height+ VARIABLE_HEIGHT(60)+VARIABLE_HEIGHT(10*appsiteNames.count))];
    alertView.layer.borderColor = [[UIColor blackColor]CGColor];
    alertView.layer.borderWidth = 1;
    alertView.layer.cornerRadius = 3;
    alertView.backgroundColor = [UIColor convertToHexString:@"#F9F9F9"];
    //#87B6F3   #f1ede9
    //alertView.backgroundColor = [UIColor blueColor];
    [self.view addSubview:alertView];
    
    // label
    UILabel * msgLabel = [[UILabel alloc]initWithFrame:CGRectMake(VARIABLE_WIDTH(10), VARIABLE_HEIGHT(10), VARIABLE_WIDTH(180), [self getLabelSize:msg withSize:width-VARIABLE_WIDTH(20) withFont:msgFont])];
    msgLabel.text = msg;
    msgLabel.numberOfLines = 0;
    msgLabel.textColor = [UIColor blackColor];
    //    msgLabel.layer.borderColor = [[UIColor blackColor]CGColor];
    //    msgLabel.layer.borderWidth = 1;
    msgLabel.font = [UIFont systemFontOfSize:(IPAD ? 17.0f:14.0f)];
    msgLabel.lineBreakMode =NSLineBreakByWordWrapping;
    msgLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [alertView addSubview:msgLabel];
    YVal  = YVal+ VARIABLE_HEIGHT(10)+ msgLabel.frame.size.height;
    
    //button
    UIButton * appsiteBtn;
    for(int i=0;i<appsiteNames.count;i++)
    {
        appsiteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        appsiteBtn.frame = CGRectMake(VARIABLE_WIDTH(10), YVal + VARIABLE_HEIGHT(10), width - VARIABLE_WIDTH(20), VARIABLE_HEIGHT(15));
        NSString * bulletappend = [NSString stringWithFormat:@"\u2022  %@",appsiteNames[i]];
        [appsiteBtn setTitle:bulletappend forState:UIControlStateNormal];
        // [appsiteBtn setTitle:appsiteArray[i] forState:UIControlStateNormal];
        [appsiteBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        appsiteBtn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        appsiteBtn.tag = i;
        appsiteBtn.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 17.0f:14.0f)];
        appsiteBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        appsiteBtn.contentEdgeInsets = UIEdgeInsetsMake(0, VARIABLE_WIDTH(5), 0, 0);
        [alertView addSubview:appsiteBtn];
        [appsiteBtn addTarget:self action:@selector(appsiteBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        YVal = YVal+VARIABLE_HEIGHT(20);
        
    }
    
    
    
    //ok button
    UIButton * okBtn = [[UIButton alloc]initWithFrame:CGRectMake(VARIABLE_WIDTH(10), alertView.frame.size.height - VARIABLE_HEIGHT(40), VARIABLE_WIDTH(180), VARIABLE_HEIGHT(30))];
    okBtn.backgroundColor=[UIColor convertToHexString:@"#D5D5D5"];
    [okBtn setTitle:@"OK" forState:UIControlStateNormal];
    [okBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    okBtn.titleLabel.font = [UIFont boldSystemFontOfSize:(IPAD ? 17.0f:14.0f)];
    //    okBtn.layer.borderColor = [[UIColor blackColor]CGColor];
    //    okBtn.layer.borderWidth = 1;
    okBtn.layer.cornerRadius = 3;
    [okBtn addTarget:self action:@selector(calncelBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [alertView addSubview:okBtn];
}


-(void) appsiteBtnClicked :(id) sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIButton * btn = (UIButton*) sender;
    NSLog(@"syudfgh %ld@", (long)btn.tag);
    NSString *url = appsiteURL[btn.tag];
    [defaults  setObject:url forKey:KEY_URL];
    NSLog(@"Send response  %@ Default string %@",[defaults  objectForKey:@"weatherURL"],[defaults valueForKey:KEY_URL]);
    WebBrowserViewController *webView = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    webView.comingFromClaim = YES;
    [navVC pushViewController:webView animated:NO];
    
}
-(void)popBackToPreviousPage
{
    [self.navVC popViewControllerAnimated:NO];
}
-(CGFloat) getLabelSize : (NSString*) labelString withSize:(CGFloat) labelwidth withFont:(UIFont *) font
{
    
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    float noOflines = ceil(rect.size.height / font.lineHeight);
    
    
    return ceil(font.lineHeight*noOflines);
    
}
-(void)calncelBtnPressed : (id) sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [defaults setBool:YES forKey:@"isComingFromCliam"];
    NSString *url = appsiteURL[0];
    NSLog(@"URl :%@, Name : %@", appsiteURL[0],appsiteNames[0]);
    [defaults  setObject:url forKey:KEY_URL];
    NSLog(@"Send response  %@ Default string %@",[defaults  objectForKey:@"weatherURL"],[defaults valueForKey:KEY_URL]);
    WebBrowserViewController *webView = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    webView.comingFromClaim = YES;
    [navVC pushViewController:webView animated:NO];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

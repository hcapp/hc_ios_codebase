//
//  RetListClaimBusResponse.h
//  HubCiti
//
//  Created by Nikitha on 8/23/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol RetListClaimBusResponse;
@interface RetListClaimBusResponse : NSObject
@property(nonatomic,strong) NSString * claimTxtMsg;
@property(nonatomic,strong) NSString * claimCategory;
@property(nonatomic,strong) NSString * claimCategoryURL;
@end



//
//  ProductPage.m
//  Scansee
//
//  Created by ajit on 9/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "ProductPage.h"
#import "ProductPageCustomCell1.h"
#import "mainPageViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "WishListHomePage.h"
#import "RateShareViewController.h"
#import "FindNearbyOnlineStoresList.h"
#import "FindNearByResultListMapViewController.h"
#import "RateShareViewController.h"
#import "ProductCouponsList.h"
#import "WebBrowserViewController.h"
#import "UserSettings.h"
#import "ProductInformation.h"
#import "AsyncImageView.h"
#import "ShoppingListHomePage.h"
#import "ShoppingCartViewController.h"

BOOL couponsFlag;

@implementation StockHeader

@synthesize m_StockHeader;
@synthesize m_StockType;
@end

@implementation ProductPage
@synthesize productPageTable , adRibbonBtn;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppDelegate removeActivityIndicator];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    for (UIView *vw in [self.view subviews]) {
        if ([vw isKindOfClass:[UIActivityIndicatorView class]])
            [vw removeFromSuperview];
    }
	
    if ([defaults  objectForKey:@"refreshProd"] != nil)
        [defaults  removeObjectForKey:@"refreshProd"];
    
	activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	activity.hidesWhenStopped = YES;
	activity.frame = CGRectMake(135, 183, 50, 50);
	
	[self.view addSubview:activity];
	[activity startAnimating];
	
    prodName = @"";
    
    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = mainPage;
    [mainPage release];
    
    //customize back button
    UIButton *backBtn = [UtilityManager customizeBackButton];
    
    [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = back;
    self.navigationItem.hidesBackButton = YES;
    [back release];
    
    if ([[[self.navigationController viewControllers] objectAtIndex:[[self.navigationController viewControllers] count]-2 ] isKindOfClass:[ShoppingListHomePage class]] || [[[self.navigationController viewControllers] objectAtIndex:[[self.navigationController viewControllers] count]-2 ] isKindOfClass:[ShoppingCartViewController class]]) {
        [aboutBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_about.png"] forState:UIControlStateNormal];
        [aboutBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_about.png"] forState:UIControlStateHighlighted];
        [aboutBtn addTarget:self action:@selector(aboutClicked:) forControlEvents:UIControlEventTouchUpInside];
        aboutBtn.userInteractionEnabled = NO;
        
    }else {
        [aboutBtn setBackgroundImage:[UIImage imageNamed:@"tab_up_down_add_spngLst-02.png"] forState:UIControlStateNormal];
        [aboutBtn setBackgroundImage:[UIImage imageNamed:@"tab_up_down_add_spngLst.png"] forState:UIControlStateHighlighted];
        [aboutBtn addTarget:self action:@selector(addToList:) forControlEvents:UIControlEventTouchUpInside];
        aboutBtn.userInteractionEnabled = NO;
        
    }
    
    [wishBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_wishlist.png"] forState:UIControlStateNormal];
    [wishBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_wishlist.png"] forState:UIControlStateHighlighted];
    [wishBtn addTarget:self action:@selector(wishListClicked:) forControlEvents:UIControlEventTouchUpInside];
    wishBtn.userInteractionEnabled = NO;
    
    [prefBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_preferences.png"] forState:UIControlStateNormal];
    [prefBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_preferences.png"] forState:UIControlStateHighlighted];
    [prefBtn addTarget:self action:@selector(preferences:) forControlEvents:UIControlEventTouchUpInside];
    prefBtn.userInteractionEnabled = NO;
    
    [rateBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_rateshare.png"] forState:UIControlStateNormal];
    [rateBtn setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_rateshare.png"] forState:UIControlStateHighlighted];
    [rateBtn addTarget:self action:@selector(rateShareClicked:) forControlEvents:UIControlEventTouchUpInside];
	rateBtn.userInteractionEnabled = NO;
    
	//added 3rd condn as soetimes the ribbonAdImagePath coming as N/A even though AdUrl is there
	if (([[defaults valueForKey:@"AdUrl"]length]>0) && ([[ScanseeManager sharedManager]shareFromTL] == YES)
		&& ![[defaults  objectForKey:@"ribbonAdImagePath"]isEqualToString:@"N/A"]) {
		
		productPageTable = [[UITableView alloc]initWithFrame:CGRectMake(0.0, 71.0, 320.0, 299.0) style:UITableViewStylePlain];
		
		
		NSString *ribbonAdImagePathStr = [[defaults  objectForKey:@"ribbonAdImagePath"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
		
		adRibbonBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //adRibbonBtn.backgroundColor = [UIColor whiteColor];
		[adRibbonBtn addTarget:self action:@selector(ribbonAdBtnPressed)forControlEvents:UIControlEventTouchUpInside];
		adRibbonBtn.frame = CGRectMake(0.0,0.0, 320.0, 50.0);
		
        UILabel *saleprice = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, 320, 20)];
        saleprice.font = [UIFont fontWithName:@"Helvetica" size:13];
        saleprice.textAlignment = UITextAlignmentCenter;
        saleprice.backgroundColor = [UIColor whiteColor];
        saleprice.text = [NSString stringWithFormat:@"Sale Price : %@   ",[defaults  objectForKey:@"salePrice"]];
        [self.view addSubview:saleprice];
        ReleaseAndNilify(saleprice);

		NSURL *ribbonAdImageURL = [NSURL URLWithString:ribbonAdImagePathStr];
		UIImage *ribbonAdImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:ribbonAdImageURL]];
		[adRibbonBtn setImage:ribbonAdImage forState:UIControlStateNormal];
		
		[self.view addSubview:adRibbonBtn];
	}
	else if(!([[defaults valueForKey:@"AdUrl"]length]>0) && ([[ScanseeManager sharedManager]shareFromTL] == YES)){
		
		UILabel *retailerNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 0.0, 320.0, 50.0)];
		retailerNameLabel.backgroundColor = [UIColor whiteColor];
		retailerNameLabel.textColor = [UIColor blackColor];
		retailerNameLabel.font = [UIFont boldSystemFontOfSize:16];
		retailerNameLabel.textAlignment = UITextAlignmentCenter;
		retailerNameLabel.numberOfLines = 2;
        [retailerNameLabel setLineBreakMode:UILineBreakModeWordWrap];
		retailerNameLabel.text = [NSString stringWithFormat:@"%@",[defaults valueForKey:KEY_RTLNAME]];
		[self.view addSubview:retailerNameLabel];
        ReleaseAndNilify(retailerNameLabel);
		
        UILabel *saleprice = [[UILabel alloc] initWithFrame:CGRectMake(00, 40, 320, 30)];
        saleprice.font = [UIFont fontWithName:@"Helvetica" size:13];
        saleprice.backgroundColor = [UIColor whiteColor];
        saleprice.textAlignment = UITextAlignmentCenter;
        saleprice.text = [NSString stringWithFormat:@"Sale Price : %@   ",[defaults  objectForKey:@"salePrice"]];
        [self.view addSubview:saleprice];
        ReleaseAndNilify(saleprice);
		productPageTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 71, 320, 360) style:UITableViewStylePlain];
	}
	
	else {
		productPageTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 360) style:UITableViewStylePlain];
	}
	
	productPageTable.delegate = self;
	productPageTable.dataSource = self;
	[self.view addSubview:productPageTable];
	[self.view bringSubviewToFront:productPageTable];
	
	productPageTable.hidden = YES;
	
	stockHeaderArray = [[NSMutableArray alloc]init];
	stockImagePathArray = [[NSMutableArray alloc]init];
	labelValueArray = [[NSMutableArray alloc]init];
	detailLabelValueArray = [[NSMutableArray alloc]init];
    retIdArray = [[NSMutableArray alloc]init];
    retLocIdArray = [[NSMutableArray alloc]init];
    retListIDArray = [[NSMutableArray alloc]init];
	couponsFlag = 0;
    	
 	if([CLLocationManager  locationServicesEnabled] && !([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) &&[[ScanseeManager sharedManager]gps_allow_flag] == YES){
        
        [LocationManager updateLocationinfo:self];
	}
    
    else
    {
        NSMutableString *xmlString = [[NSMutableString alloc] init];
        [xmlString appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults  objectForKey:KEY_USERID]];
        [xmlString appendFormat:@"<productId>%@</productId>",[defaults  objectForKey:KEY_PRODUCTID]];
        
        if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[ScanseeManager sharedManager]gps_allow_flag] == YES && [[defaults valueForKey:KEY_LATITUDE] length])
            
            [xmlString appendFormat:@"<latitude>%@</latitude><longitude>%@</longitude>", [defaults valueForKey:KEY_LATITUDE],[defaults valueForKey:KEY_LONGITUDE]];
        else if ([[defaults objectForKey:@"ZipCode"] length])
            [xmlString appendFormat:@"<postalcode>%@</postalcode>", [defaults objectForKey:@"ZipCode"]] ;

        if ([[ScanseeManager sharedManager]shareFromTL] == YES)
            [xmlString appendFormat:@"<retailID>%@</retailID>",[defaults objectForKey:KEY_RETAILERID]];
        //@Deepak: Added MainMenuID in the request- userTracking for shoppinglist
        if([defaults  valueForKey:KEY_MAINMENUID])
         [xmlString appendFormat:@"<mainMenuID>%@</mainMenuID><scanTypeID>%@</scanTypeID>",[defaults  valueForKey:KEY_MAINMENUID],[defaults  valueForKey:KEY_SCANTYPEID]];
        if([defaults  valueForKey:@"saleListID"])
            [xmlString appendFormat:@"<saleListID>%@</saleListID>",[defaults  valueForKey:@"saleListID"]];
        //@Deepak: Adding prodListID code for UserTracking
        if([defaults  valueForKey:KEY_PRODLISTID])
            [xmlString appendFormat:@"<prodListID>%@</prodListID>",[defaults  valueForKey:KEY_PRODLISTID]];
        
        [xmlString appendString:@"</ProductDetailsRequest>"];
        
        NSString *urlString = [NSString stringWithFormat:@"%@shoppingList/getproductsummary",BASE_URL];
        
        responseXml = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:xmlString]];

        ReleaseAndNilify(xmlString);
        if([UtilityManager isNullOrEmptyString:responseXml]){
            return;
        }
        [self parseTheXml:responseXml];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 320, 40)];
        titleLabel.textAlignment = UITextAlignmentCenter;
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.numberOfLines = 2;
        [titleLabel setLineBreakMode:UILineBreakModeWordWrap];
        titleLabel.font = [UIFont boldSystemFontOfSize:16];
        titleLabel.text = [NSString stringWithFormat:@"%@",prodName];
        self.navigationItem.titleView = titleLabel;
        ReleaseAndNilify(titleLabel);
        
        [productPageTable setHidden:NO];
        [productPageTable reloadData];
        [activity stopAnimating];
        [activity removeFromSuperview];
    }
}

-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

- (NSString *)removeHtmlTags:(NSString *)string {
    
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString:string];
    
    while ([theScanner isAtEnd] == NO) {
        
        [theScanner scanUpToString:@"<" intoString:NULL] ;
        
        [theScanner scanUpToString:@">" intoString:&text] ;
        
        string = [string stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return string;
}

- (void) animateCellToButton: (UIButton *)sender
{
    AsyncImageView *asyncImageView = [[AsyncImageView alloc] init];
	asyncImageView.contentMode = UIViewContentModeScaleToFill;
	asyncImageView.backgroundColor = [UIColor clearColor];
    asyncImageView.frame = CGRectMake(5, 5, 50, 50);
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 230, 30)];
	label1.backgroundColor = [UIColor clearColor];
	label1.textColor = [UIColor blackColor];
	label1.font = [UIFont systemFontOfSize:14];
	label1.numberOfLines = 1;
	
	
	UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(60, 35, 230, 15)];
	label2.backgroundColor = [UIColor clearColor];
	label2.textColor = [UIColor grayColor];
	label2.font = [UIFont systemFontOfSize:12];
	label2.numberOfLines = 1;
    
    UIView *duplicateCell = [[UIView alloc] initWithFrame:CGRectMake(productPageTable.frame.origin.x, productPageTable.frame.origin.y, productPageTable.frame.size.width, productPageTable.rowHeight)];
    [duplicateCell setBackgroundColor:[UIColor whiteColor]];
    
    label1.text = prodName;
    label2.text = prodDesc;
    [duplicateCell addSubview:label1];
    
    if (![label2.text isEqualToString:@"N/A"])
        [duplicateCell addSubview:label2];
    
    if([[ScanseeManager sharedManager]shareFromTL] == YES)
        [asyncImageView loadImage:[NSString stringWithFormat:@"%@",[defaults valueForKey:@"PRODIMG"]]];
    
    else
        [asyncImageView loadImage:prodImage];
    
    [duplicateCell addSubview:asyncImageView];
    [label1 release];
    [label2 release];
    [asyncImageView release];
    
    [self.view addSubview:duplicateCell];
    [self scaleButtonUp:sender];
    [self.view bringSubviewToFront:sender];
    // [self.view addSubview:sender];
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationCurveLinear animations:^{
        CGAffineTransform scale = CGAffineTransformMakeScale(0.1,0.1);
        CGAffineTransform translate = CGAffineTransformMakeTranslation((sender.frame.origin.x + (sender.frame.size.width / 2)) - (duplicateCell.frame.size.width / 2), sender.frame.origin.y + (sender.frame.size.height / 2));
        duplicateCell.transform = CGAffineTransformConcat(scale, translate);
    }completion:^(BOOL finished) {
        
        if (finished)
        {
            [duplicateCell removeFromSuperview];
            [self scaleButtonDown:sender];
            [duplicateCell release];
        }
    }
     ];
}

-(void)scaleButtonUp:(UIButton *)sender
{
    UIImage *img = [sender backgroundImageForState:UIControlStateHighlighted];
    UIImage *img2 = [sender backgroundImageForState:UIControlStateNormal];
    [sender setImage:img forState:UIControlStateNormal];
    [sender setImage:img2 forState:UIControlStateHighlighted];
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationCurveLinear animations:^{
        CGAffineTransform scale = CGAffineTransformMakeScale(1.3,1.3);
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0,0);
        sender.transform = CGAffineTransformConcat(scale, translate);
    }completion:^(BOOL finished) {
        
        if (finished)
        {
        }
    }
     ];
}
-(void)scaleButtonDown:(UIButton *)sender
{
    UIImage *img = [sender backgroundImageForState:UIControlStateHighlighted];
    UIImage *img2 = [sender backgroundImageForState:UIControlStateNormal];
    
    [sender setImage:img2 forState:UIControlStateNormal];
    [sender setImage:img forState:UIControlStateHighlighted];
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationCurveLinear animations:^{
        CGAffineTransform scale = CGAffineTransformMakeScale(1.0,1.0);
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0,0);
        sender.transform = CGAffineTransformConcat(scale, translate);
    }completion:^(BOOL finished) {
        
        if (finished)
        {
        }
    }
     ];
}

-(void) addToList:(UIButton *) sender
{
    
    [[ScanseeManager sharedManager]setRefreshList:YES];
	NSMutableString *requestStr = [[NSMutableString alloc] init];
	[requestStr appendFormat:@"<AddShoppingList><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", prodId];
    [requestStr appendFormat:@"<productName><![CDATA[%@]]></productName>", prodName];
   
    [requestStr appendString:@"</ProductDetail></productDetails>"];
    //@Deepak: Added MainMenuID in the request- userTracking for shoppinglist
    [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID></AddShoppingList>", [defaults  valueForKey:KEY_MAINMENUID]];
	
	NSString *urlString = [NSString stringWithFormat:@"%@shoppingList/addtslbysearch",BASE_URL];
    
    NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    [requestStr release];
    
    if ([UtilityManager isNullOrEmptyString:response]) {
        return;
    }
	[defaults  setObject:response forKey:KEY_RESPONSEXML];
    
	TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
	TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
	TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];

    if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
        [self animateCellToButton:sender];
    }
    else {
        
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[TBXML textForElement:responseTextElement] delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
		[alert show];
		[alert release];
    }
}

- (void)ribbonAdBtnPressed {
	NSString *url = [defaults  objectForKey:@"ribbonAdURL"];
	[defaults  setObject:url forKey:KEY_URL];
	DLog(@"url value:%@",[defaults valueForKey:KEY_URL]);
	[[ScanseeManager sharedManager]setShareFromTL:YES];
    [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
	WebBrowserViewController *urlDetail = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:urlDetail animated:NO];
	[urlDetail release];
}

-(void) updatedLatlongValue :(NSString *) latValue longitudeValue:(NSString *)longValue
{
    if (latValue == NULL || longValue == NULL) {
        return;
    }
    
	[defaults setObject:latValue forKey:KEY_LATITUDE];
    [defaults setObject:longValue forKey:KEY_LONGITUDE];
	
    NSMutableString *xmlString = [[NSMutableString alloc] init];
	[xmlString appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults  objectForKey:KEY_USERID]];
	[xmlString appendFormat:@"<productId>%@</productId>",[defaults  objectForKey:KEY_PRODUCTID]];
    [xmlString appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",longValue,latValue];
	
    if ([[ScanseeManager sharedManager]shareFromTL] == YES)
        [xmlString appendFormat:@"<retailID>%@</retailID>",[defaults objectForKey:KEY_RETAILERID]];
    
    //@Deepak: Added MainMenuID in the request- userTracking for shoppinglist
    [xmlString appendFormat:@"<mainMenuID>%@</mainMenuID><scanTypeID>%@</scanTypeID>",[defaults  valueForKey:KEY_MAINMENUID],[defaults  valueForKey:KEY_SCANTYPEID]];
    
    //@Deepak: Adding prodListID code for UserTracking
    if([defaults  valueForKey:KEY_PRODLISTID])
        [xmlString appendFormat:@"<prodListID>%@</prodListID>",[defaults  valueForKey:KEY_PRODLISTID]];
    
	[xmlString appendString:@"</ProductDetailsRequest>"];
	
	NSString *urlString = [NSString stringWithFormat:@"%@shoppingList/getproductsummary",BASE_URL];
    responseXml = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:xmlString]];
    ReleaseAndNilify(xmlString);
    
    if([UtilityManager isNullOrEmptyString:responseXml]){
        return;
    }
    [defaults  setValue:responseXml forKey:KEY_RESPONSEXML];
	[self parseTheXml:responseXml];
	
	UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 320, 40)];
	titleLabel.textAlignment = UITextAlignmentCenter;
	titleLabel.textColor = [UIColor whiteColor];
	titleLabel.backgroundColor = [UIColor clearColor];
	titleLabel.numberOfLines = 2;
    [titleLabel setLineBreakMode:UILineBreakModeWordWrap];
	titleLabel.font = [UIFont boldSystemFontOfSize:16];
	titleLabel.text = [NSString stringWithFormat:@"%@",prodName];
	self.navigationItem.titleView = titleLabel;
	[titleLabel release];
	
	[productPageTable setHidden:NO];
	[productPageTable reloadData];
	[activity stopAnimating];
	[activity removeFromSuperview];
}

-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:YES];
	
    [self refresh];
	if ([[ScanseeManager sharedManager]refreshProdPage] == YES){
		[[ScanseeManager sharedManager]setRefreshProdPage:NO];
		responseXml = [[NSString alloc]initWithString:[defaults valueForKey:KEY_RESPONSEXML]];
        
		aboutBtn.userInteractionEnabled = NO;
        wishBtn.userInteractionEnabled = NO;
        prefBtn.userInteractionEnabled = NO;
        rateBtn.userInteractionEnabled = NO;
		[self parseTheXml:responseXml];
		[productPageTable reloadData];
	}
	//[productPageTable deselectRowAtIndexPath:selectedIndex animated:YES];
}

// refresh product Summary coming after adding/ deleting clr to/from gallery
-(void) refresh
{
    if ([defaults  objectForKey:@"refreshProd"] != nil) {
        if ([defaults  boolForKey:@"refreshProd"] == YES) {
            NSMutableString *xmlString = [[NSMutableString alloc] init];
            [xmlString appendFormat:@"<ProductDetailsRequest><userId>%@</userId>",[defaults  objectForKey:KEY_USERID]];
            [xmlString appendFormat:@"<productId>%@</productId>",[defaults  objectForKey:KEY_PRODUCTID]];
            
            if([[ScanseeManager sharedManager]shareFromTL] == YES){
                
                [xmlString appendFormat:@"<retailID>%@</retailID>",[defaults  objectForKey:KEY_RETAILERID]];
                
                if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[ScanseeManager sharedManager]gps_allow_flag] == YES && [[defaults valueForKey:KEY_LATITUDE] length])
                    
                    [xmlString appendFormat:@"<latitude>%@</latitude><longitude>%@</longitude>", [defaults valueForKey:KEY_LATITUDE],[defaults valueForKey:KEY_LONGITUDE]];
                else
                    [xmlString appendFormat:@"<postalcode>%@</postalcode>", [defaults objectForKey:@"ZipCode"]] ;
                
            }
            else if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[ScanseeManager sharedManager]gps_allow_flag] == YES)
                
            {
                [xmlString appendFormat:@"<latitude>%@</latitude><longitude>%@</longitude>", [defaults valueForKey:KEY_LATITUDE],[defaults valueForKey:KEY_LONGITUDE]];
            }
            //@Deepak: Added MainMenuID in the request- userTracking for shoppinglist
            [xmlString appendFormat:@"<mainMenuID>%@</mainMenuID><scanTypeID>%@</scanTypeID>",[defaults  valueForKey:KEY_MAINMENUID],[defaults  valueForKey:KEY_SCANTYPEID]];
            //@Deepak: Adding prodListID code for UserTracking
            if([defaults  valueForKey:KEY_PRODLISTID])
                [xmlString appendFormat:@"<prodListID>%@</prodListID>", [defaults  valueForKey:KEY_PRODLISTID]];
            
            [xmlString appendString:@"</ProductDetailsRequest>"];
            
            DLog(@"Request String for product page = %@", xmlString);
            
            NSString *urlString = [NSString stringWithFormat:@"%@shoppingList/getproductsummary",BASE_URL];
            NSString *response = [ConnectionManager establishPostConnection:urlString withParam:xmlString];
            [xmlString release];
            
            if ([UtilityManager isNullOrEmptyString:response]) {
                return;
            }
            aboutBtn.userInteractionEnabled = NO;
            wishBtn.userInteractionEnabled = NO;
            prefBtn.userInteractionEnabled = NO;
            rateBtn.userInteractionEnabled = NO;
            [defaults  setValue:response forKey:KEY_RESPONSEXML];
            
            responseXml = [[NSString alloc]initWithString:[defaults valueForKey:KEY_RESPONSEXML]];
            
            [self parseTheXml:responseXml];
            [productPageTable reloadData];
            [defaults  removeObjectForKey:@"refreshProd"];
        }
    }
}

-(void)parseTheXml:(NSString *)response{
    
   // if ([stockHeaderArray count]){
        [stockHeaderArray removeAllObjects];
        [stockImagePathArray removeAllObjects];
        [labelValueArray removeAllObjects];
        [detailLabelValueArray removeAllObjects];
        couponsFlag = 0;
   // }
    
	TBXML *tbxml = [[TBXML tbxmlWithXMLString:response] retain];
    TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
    if (responseText!=nil){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@" " message:[TBXML textForElement:responseText] delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
    }
	
	//Online Store part
	TBXMLElement *OSE = [TBXML childElementNamed:@"OnlineStores" parentElement:tbxml.rootXMLElement];
	if(OSE != nil){
		TBXMLElement *PIPE = [TBXML childElementNamed:@"stockImagePath" parentElement:OSE];

        if (PIPE != nil)
            [stockImagePathArray addObject:[TBXML textForElement:PIPE]];
		
		TBXMLElement *SHE = [TBXML childElementNamed:@"stockHeader" parentElement:OSE];
		NSString *stockHeaderStr = [TBXML textForElement:SHE];
		
		StockHeader *stockHdr = [[[StockHeader alloc] init] autorelease];
		stockHdr.m_StockHeader = [[stockHeaderStr copy] autorelease];
		stockHdr.m_StockType = STOCK_ONLINE;
		[stockHeaderArray addObject:stockHdr];
		
		// chaitra
        
		TBXMLElement *OSMDE = [TBXML childElementNamed:@"onlineStoresMetaData" parentElement:OSE];
		if (OSMDE != nil) {
            
            TBXMLElement *MPE = [TBXML childElementNamed:@"minPrice" parentElement:OSMDE];
            NSString *minPriceStr = [TBXML textForElement:MPE];
            DLog(@"MinPrice = %@",minPriceStr);
            [labelValueArray addObject:minPriceStr];
            
            
            TBXMLElement *TSE = [TBXML childElementNamed:@"totalStores" parentElement:OSMDE];
            NSString *totalStoresStr = [NSString stringWithFormat:@"%@ Stores", [TBXML textForElement:TSE]];
            
            DLog(@"totalStores = %@",totalStoresStr);
            [detailLabelValueArray addObject:totalStoresStr];
        }
        else {
            [labelValueArray addObject:@" "];
            [detailLabelValueArray addObject:@""];
        }
	}//end Online Store part
    
	//Find Nearby part
	TBXMLElement *FNDE = [TBXML childElementNamed:@"FindNearByDetailsResultSet" parentElement:tbxml.rootXMLElement];
	
	if (FNDE != nil) {
		
		TBXMLElement *PIPE = [TBXML childElementNamed:@"stockImagePath" parentElement:FNDE];
        if (PIPE != nil)
            [stockImagePathArray addObject:[TBXML textForElement:PIPE]];
		
		TBXMLElement *SHE = [TBXML childElementNamed:@"stockHeader" parentElement:FNDE];
		NSString *stockHeaderStr = [TBXML textForElement:SHE];
		StockHeader *stockHdr = [[[StockHeader alloc] init] autorelease];
		stockHdr.m_StockHeader = [[stockHeaderStr copy] autorelease];
		stockHdr.m_StockType = STOCK_NEARBY;
		[stockHeaderArray addObject:stockHdr];
		
		TBXMLElement *FNMDE = [TBXML childElementNamed:@"findNearByMetaData" parentElement:FNDE];
        
		TBXMLElement *TLE = [TBXML childElementNamed:@"totalLocations" parentElement:FNMDE];
		NSString *totalLocationsStr = [TBXML textForElement:TLE];
		[detailLabelValueArray addObject:totalLocationsStr];

        //For ScanSee Data
        TBXMLElement *FindNearbyDetailsElement = [TBXML childElementNamed:@"FindNearByDetails" parentElement:FNDE];
        TBXMLElement *retLocIdElement = [TBXML childElementNamed:@"retLocId" parentElement:FindNearbyDetailsElement];
        if (retLocIdElement!=nil) {
            
            while (FindNearbyDetailsElement!=nil){
                
                TBXMLElement *retIdElement = [TBXML childElementNamed:@"retailerId" parentElement:FindNearbyDetailsElement];
                
                TBXMLElement *retListIDElement = [TBXML childElementNamed:KEY_RLISTID parentElement:FindNearbyDetailsElement];
                if(retListIDElement != nil)
                    [retListIDArray addObject:[TBXML textForElement:retIdElement]];
                
                
                [retIdArray addObject:[TBXML textForElement:retIdElement]];
                [retLocIdArray addObject:[TBXML textForElement:retLocIdElement]];
                
                FindNearbyDetailsElement = [TBXML nextSiblingNamed:@"FindNearByDetails" searchFromElement:FindNearbyDetailsElement];
                
            }
        }
		
		TBXMLElement *LPE = [TBXML childElementNamed:@"lowestPrice" parentElement:FNMDE];
		NSString *lowestPriceStr = [TBXML textForElement:LPE];
		
		if ([lowestPriceStr isEqualToString:@"N/A"] == NO)
			[labelValueArray addObject:lowestPriceStr];
		else
			[labelValueArray addObject:@" "];
	}//end Find Nearby part
	
	//Reviews part
	TBXMLElement *ProdRatingReviewElement = [TBXML childElementNamed:@"ProductRatingReview" parentElement:tbxml.rootXMLElement];
	
	if (ProdRatingReviewElement != nil) {
        
		TBXMLElement *TotalReviewsElement = [TBXML childElementNamed:@"totalReviews" parentElement:ProdRatingReviewElement];
		NSString *totalReviewsStr = [TBXML textForElement:TotalReviewsElement];
        
		TBXMLElement *stockImgPathElement = [TBXML childElementNamed:@"stockImagePath" parentElement:ProdRatingReviewElement];
        if (stockImgPathElement != nil)
            [stockImagePathArray addObject:[TBXML textForElement:stockImgPathElement]];
		
		TBXMLElement *StockHeaderElement = [TBXML childElementNamed:@"stockHeader" parentElement:ProdRatingReviewElement];
		NSString *stockHeaderStr = [TBXML textForElement:StockHeaderElement];
		StockHeader *stockHdr = [[[StockHeader alloc] init] autorelease];
		stockHdr.m_StockHeader = [[stockHeaderStr copy] autorelease];
		stockHdr.m_StockType = STOCK_REVIEWS;
		[stockHeaderArray addObject:stockHdr];
		
		TBXMLElement *UserRatingInfoElement = [TBXML childElementNamed:@"userRatingInfo" parentElement:ProdRatingReviewElement];
        
        int userCount = 0;
		if (UserRatingInfoElement != nil) {
			
			TBXMLElement *AvgRatingElement = [TBXML childElementNamed:@"avgRating" parentElement:UserRatingInfoElement];
			NSString *avgRatingStr = [[TBXML textForElement:AvgRatingElement]retain];
			rating = [avgRatingStr copy] ;
            ReleaseAndNilify(avgRatingStr);
            
            TBXMLElement *noOfUsersRatedElement = [TBXML childElementNamed:@"noOfUsersRated" parentElement:UserRatingInfoElement];
            if(noOfUsersRatedElement)
                userCount = [[TBXML textForElement:noOfUsersRatedElement] intValue];
		}
        
        [detailLabelValueArray addObject:[NSString stringWithFormat:@"%@                              %i Ratings",totalReviewsStr,userCount]];
		[labelValueArray addObject:@" "];
	}//end Reviews part
	
	//Product Info part
	TBXMLElement *PDE = [TBXML childElementNamed:KEY_PRODUCTDETAIL parentElement:tbxml.rootXMLElement];
	if (PDE != nil) {
		
		TBXMLElement *SIPE = [TBXML childElementNamed:@"stockImagePath" parentElement:PDE];
        if(SIPE!=nil)
            [stockImagePathArray addObject:[TBXML textForElement:SIPE]];

		TBXMLElement *PNE = [TBXML childElementNamed:KEY_PRODUCTNAME parentElement:PDE];
		NSString *prodNameStr = [TBXML textForElement:PNE];
		prodName = [prodNameStr copy];
		[defaults  setObject:prodName forKey:KEY_PRODUCTNAME];
		
		TBXMLElement *PIDE = [TBXML childElementNamed:KEY_PRODUCTID parentElement:PDE];
		NSString *prodIdStr = [TBXML textForElement:PIDE];
		prodId = [prodIdStr copy];
		
		
		TBXMLElement *PLDE = [TBXML childElementNamed:@"productLongDescription" parentElement:PDE];
        TBXMLElement *ProductShortDescription = [TBXML childElementNamed:@"productShortDescription" parentElement:PDE];
        if(ProductShortDescription != nil && ![[TBXML textForElement:ProductShortDescription]isEqualToString:@"N/A"]){
            NSString *prodShortDescStr = [TBXML textForElement:ProductShortDescription];
            prodDesc = [prodShortDescStr copy];
        }
        else {
            NSString *prodLongDescStr = [TBXML textForElement:PLDE];
            prodDesc = [prodLongDescStr copy];
        }
		TBXMLElement *IPE = [TBXML childElementNamed:TAG_IMGPATH parentElement:PDE];
		NSString *imagePathStr = [TBXML textForElement:IPE];
		prodImage = [imagePathStr copy];
		
		TBXMLElement *CLRFE = [TBXML childElementNamed:KEY_CLRFLAG parentElement:PDE];
		NSString *clrFlagStr = [TBXML textForElement:CLRFE];
		clrFlag = [clrFlagStr boolValue];
		
		[labelValueArray addObject:@" "];
		[detailLabelValueArray addObject:@" "];
		
	}//end Product Info part
	
	//CLR part
	TBXMLElement *CLRIE = [TBXML childElementNamed:@"CLRInfo" parentElement:tbxml.rootXMLElement];
	if (CLRIE != nil) {
		couponsFlag = 1;
		TBXMLElement *IPE = [TBXML childElementNamed:@"stockImagePath" parentElement:CLRIE];
		
        if(IPE != nil)
            [stockImagePathArray addObject:[TBXML textForElement:IPE]];
		
		TBXMLElement *SHE = [TBXML childElementNamed:@"stockHeader" parentElement:CLRIE];
		NSString *stockHeaderStr = [TBXML textForElement:SHE];
		StockHeader *stockHdr = [[[StockHeader alloc] init] autorelease];
		stockHdr.m_StockHeader = stockHeaderStr;
		stockHdr.m_StockType = STOCK_COUPONS;
		[stockHeaderArray addObject:stockHdr];
		
		TBXMLElement *CE = [TBXML childElementNamed:@"couponFlag" parentElement:CLRIE];
		couponStatus = [[TBXML textForElement:CE] retain];
		
		TBXMLElement *LE = [TBXML childElementNamed:@"loyaltyFlag" parentElement:CLRIE];
		loyaltyStatus = [[TBXML textForElement:LE]retain];
		
		TBXMLElement *REBE = [TBXML childElementNamed:@"rebateFlag" parentElement:CLRIE];
		rebateStatus = [[TBXML textForElement:REBE]retain];
		
		[detailLabelValueArray addObject:@" "];
		[labelValueArray addObject:@" "];
	}//end CLR part
	
	[tbxml release];
    
	CGFloat y_Axis = productPageTable.frame.origin.y;
    DLog(@" y = %g ((([stockHeaderArray count] + 1) * 60.0) - productPageTable.frame.origin.y-50) = %f",y_Axis,((([stockHeaderArray count] + 1) * 60.0) - productPageTable.frame.origin.y-50));
    
    CGRect newTVframe;
    if (((([stockHeaderArray count] + 1) * 60.0) + productPageTable.frame.origin.y) > 366 ) {
        newTVframe = CGRectMake(productPageTable.frame.origin.x,
                                productPageTable.frame.origin.y,
                                productPageTable.frame.size.width,
                                366 - productPageTable.frame.origin.y );
    }else{
        
        newTVframe = CGRectMake(productPageTable.frame.origin.x,
                                productPageTable.frame.origin.y,
                                productPageTable.frame.size.width,
                                ([stockHeaderArray count] + 1) * 60.0);
    }
    
	productPageTable.frame = newTVframe;
	
    aboutBtn.userInteractionEnabled = YES;
    wishBtn.userInteractionEnabled = YES;
    prefBtn.userInteractionEnabled = YES;
    rateBtn.userInteractionEnabled = YES;
    
    DLog(@"%@",prodDesc);
    prodDesc = [[self removeHtmlTags:prodDesc]copy];
    DLog(@"%@",prodDesc);
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	
	return [stockHeaderArray count]+1;
	
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 60.0;
}


-(void) preferences : (id) sender{
	
	if([Network currentReachabilityStatus]==0)
	{
		[UtilityManager showAlert];
	}
	else {
		UserSettings *settings = [[UserSettings alloc] initWithNibName:@"UserSettings" bundle:[NSBundle mainBundle]];
		[self.navigationController pushViewController:settings animated:NO];
		[settings release];
		
	}
}
/*
 - (void)singleTap:(UIButton *)sender {
 // single tap does nothing for now
 
 }
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
    productPageTable.backgroundColor = [UIColor whiteColor];
	
	AsyncImageView *asyncImageView = [[[AsyncImageView alloc] init] autorelease];
	asyncImageView.contentMode = UIViewContentModeScaleToFill;
	asyncImageView.backgroundColor = [UIColor clearColor];
    asyncImageView.frame = CGRectMake(5, 5, 50, 50);
	
	UILabel *label1 = [[[UILabel alloc]initWithFrame:CGRectMake(60, 5, 230, 30)] autorelease];
	label1.backgroundColor = [UIColor clearColor];
	label1.textColor = [UIColor blackColor];
	label1.font = [UIFont boldSystemFontOfSize:14];
	label1.numberOfLines = 1;
	
	
	UILabel *label2 = [[[UILabel alloc]initWithFrame:CGRectMake(60, 35, 230, 15)] autorelease];
	label2.backgroundColor = [UIColor clearColor];
	label2.textColor = [UIColor grayColor];
	label2.font = [UIFont boldSystemFontOfSize:12];
	label2.numberOfLines = 1;
	
	
	if (indexPath.row == 0 && ![prodName isEqualToString:@""])  {
		static NSString *CellIdentifier = @"Cell";
		
		//taken for prod image zoom feature
		//UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
		
		UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		
		
		label1.text = prodName;
		label2.text = prodDesc;
		[cell addSubview:label1];
		
		if (![label2.text isEqualToString:@"N/A"])
			[cell addSubview:label2];
		
		if([[ScanseeManager sharedManager]shareFromTL] == YES)
            [asyncImageView loadImage:[NSString stringWithFormat:@"%@",[defaults valueForKey:@"PRODIMG"]]];
        
		else
            [asyncImageView loadImage:prodImage];
        
		[cell addSubview:asyncImageView];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		return cell;
	}
	
	else if(indexPath.row!=0 && [stockHeaderArray count]) {
		static NSString *CellIdentifier1 = @"Cell1";
		
		ProductPageCustomCell1 *cell1 ;//= (ProductPageCustomCell1*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
		// if (cell1 == nil){
		cell1 = [[[ProductPageCustomCell1 alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier1] autorelease];
		
		// }
		StockHeader *stkHdr = [stockHeaderArray objectAtIndex:indexPath.row - 1];
		DLog(@"8888888888888888888888 %@", stkHdr.m_StockHeader);
		
		label1.text = stkHdr.m_StockHeader;

		label2.text = [detailLabelValueArray objectAtIndex:indexPath.row - 1];
		[cell1 addSubview:label1];
		
		if (![label2.text isEqualToString:@"N/A"])
			[cell1 addSubview:label2];
		
		asyncImageView.frame = CGRectMake(5, 10, 40, 40);
        [asyncImageView loadImage:[stockImagePathArray objectAtIndex:indexPath.row - 1]];
		[cell1 addSubview:asyncImageView];
		
		cell1.label.text = [labelValueArray objectAtIndex:indexPath.row - 1];
        
		cell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
		if (couponsFlag == 1 && indexPath.row == [stockHeaderArray count] && [stkHdr.m_StockHeader isEqualToString:@"Discount"]) {
			
			UIImageView * couponImage = [[[UIImageView alloc] initWithFrame:CGRectMake(274, 3, 14, 14)] autorelease];
			UIImageView * loyaltyImage = [[[UIImageView alloc] initWithFrame:CGRectMake(274, 21, 14, 14)] autorelease];
			UIImageView * rebateImage = [[[UIImageView alloc] initWithFrame:CGRectMake(274, 39, 14, 14)] autorelease];
			
			couponImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"C-%@.png", couponStatus]];
			loyaltyImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"L-%@.png", loyaltyStatus]];
			rebateImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"R-%@.png", rebateStatus]];
			
			[cell1 addSubview:couponImage];
			[cell1 addSubview:loyaltyImage];
			[cell1 addSubview:rebateImage];
			
		}
		
		if ([stkHdr.m_StockHeader isEqualToString:@"Reviews"]) {
			
			float avgRating_f = [rating floatValue];
			int avgRating_i = [rating intValue];
			int i;
			i =0;
			do{
				UIImageView *image1 = [[UIImageView alloc] init]; //WithFrame:CGRectMake(200, 14, 21, 21)];
				image1.frame = CGRectMake((180+(i*23)) , 10, 21, 21);
				if (i < avgRating_i)
					image1.image = [UIImage imageNamed:@"Red_fullstar.png"];
				else
					image1.image = [UIImage imageNamed:@"greyStar.png"];
				
				[cell1 addSubview:image1];
				i++;
			} while (i < 5);
			
			float diff =avgRating_f - (float)avgRating_i;
			if (diff >= 0.5) {
				UIImageView *image2 = [[UIImageView alloc] init]; //WithFrame:CGRectMake(200, 14, 21, 21)];
				image2.frame = CGRectMake((180+(avgRating_i*23)) , 10, 21, 21);
				image2.image = [UIImage imageNamed:@"Red_Halfstar.png"];
				
				[cell1 addSubview:image2];
			}
            
		}
		
		return cell1;
		
	}
    else {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        return cell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	selectedIndex = indexPath;
    
    if(indexPath.row == 0){
        {
            // Launch respective view controller
            
            if ([prodId isEqualToString:@"0"]) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@" " message:NSLocalizedString(@"Product details not available!",@"Product details not available!") delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
                [UtilityManager performSelector:@selector(dismiss:) withObject:alert afterDelay:2];
                [alert show];
                [alert release];
            }
            else{
                
                NSString *urlString = [NSString stringWithFormat:@"%@wishlist/fetchproudctattributes?userId=%@&productId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],prodId];
                
                NSString *response = [ConnectionManager establishGetConnection:urlString];
                
                if ([UtilityManager isNullOrEmptyString:response]){
                    return;
                }
                [defaults setObject:response forKey:@"responseXml1"];
                ProductInformation *prodInfo = [[ProductInformation alloc]initWithNibName:@"ProductInformation" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:prodInfo animated:NO];
                [prodInfo release];
            }
        }
    }
    
	if (indexPath.row > 0){
		
        StockHeader *stkHdr = [stockHeaderArray objectAtIndex:indexPath.row - 1];
		switch (stkHdr.m_StockType) {
			case STOCK_ONLINE: {
                          
				// Launch respective view controller
				[defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
                [defaults  setObject:prodId forKey:KEY_PRODUCTID];
				FindNearbyOnlineStoresList *onlineStores = [[FindNearbyOnlineStoresList alloc]initWithNibName:@"FindNearbyOnlineStoresList" bundle:[NSBundle mainBundle]];
				[self.navigationController pushViewController:onlineStores animated:NO];
				[onlineStores release];
			}
				break;
			case STOCK_NEARBY: {
				// Launch respective view controller
				[defaults  setObject:responseXml forKey:KEY_RESPONSEXML];
				FindNearByResultListMapViewController *localStores = [[FindNearByResultListMapViewController alloc]initWithNibName:@"FindNearByResultListMapViewController" bundle:[NSBundle mainBundle]];
                
                [AppDelegate RefreshRetIdArray];
                [AppDelegate RefreshRetLocIdArray];
                if ([retLocIdArray count]){//means only in case of scansee data
                    [[AppDelegate getRetIdArray]addObjectsFromArray:retIdArray];
                    [[AppDelegate getRetLocIdArray]addObjectsFromArray:retLocIdArray];
                }
				[self.navigationController pushViewController:localStores animated:NO];
				[localStores release];
				
			}
				break;
			case STOCK_REVIEWS: {
				// Launch respective view controller
				RateShareViewController *rateShare = [[RateShareViewController alloc]initWithNibName:@"RateShareViewController" bundle:[NSBundle mainBundle]];
				[self.navigationController pushViewController:rateShare animated:NO];
				[rateShare release];
			}
				break;
				
			case STOCK_PRODUCTINFO: {
				// Launch respective view controller
				
				if ([prodId isEqualToString:@"0"]) {
					UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@" " message:NSLocalizedString(@"Network appears to be down. Please try later",@"Network appears to be down. Please try later")  delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
					[UtilityManager performSelector:@selector(dismiss:) withObject:alert afterDelay:2];
					[alert show];
					[alert release];
				}
				else{
					
					NSString *urlString = [NSString stringWithFormat:@"%@wishlist/fetchproudctattributes?userId=%@&productId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],prodId];
					
                    NSString *response = [ConnectionManager establishGetConnection:urlString];
                    
					if ([UtilityManager isNullOrEmptyString:response]){
                        return;
                    }
					[defaults setObject:response forKey:@"responseXml1"];
					ProductInformation *prodInfo = [[ProductInformation alloc]initWithNibName:@"ProductInformation" bundle:[NSBundle mainBundle]];
					[self.navigationController pushViewController:prodInfo animated:NO];
					[prodInfo release];
				}
				DLog(@"Productinfo");
			}
				break;
                
			case STOCK_COUPONS: {
                
				// Launch respective view controller
				if (clrFlag == 0 ) {
					UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No C/L/R available" delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
					[UtilityManager performSelector:@selector(dismiss:) withObject:alert afterDelay:2];
					[alert show];
					[alert release];
					
				}
				else {
					if([Network currentReachabilityStatus]==0)
					{
						[UtilityManager showAlert];
					}
					else {
						ProductCouponsList *couponsScreen = [[ProductCouponsList alloc]initWithNibName:@"ProductCouponsList" bundle:[NSBundle mainBundle]];
						[self.navigationController pushViewController:couponsScreen animated:NO];
						[couponsScreen release];
					}
				}
				DLog(@"Coupons");
			}
				break;
			default:
				break;
		}
	}
    [productPageTable deselectRowAtIndexPath:selectedIndex animated:YES];
}

-(void)returnToMainPage:(id)sender{
	[defaults  removeObjectForKey:@"AdUrl"];
	[UtilityManager popBackToViewController:[mainPageViewController class] inNavigationController:self.navigationController];
    
}
-(void)aboutClicked:(id)sender{
    
	if([Network currentReachabilityStatus]==0)
	{
		[UtilityManager showAlert];
	}else {
		AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
		privacy.responseXml =[NSMutableString stringWithFormat:@"%@About_ScanSee.html", B_URL];
		[defaults  setObject:@"About" forKey:@"Title"];
		[self.navigationController pushViewController:privacy animated:NO];
		[privacy release];
	}
}
-(void)wishListClicked:(UIButton *)sender{
	
  	NSMutableString *requestStr = [[NSMutableString alloc] init];
	[requestStr appendFormat:@"<ProductDetailsRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
	[requestStr appendFormat:@"<productDetails><ProductDetail><productId>%@</productId>", prodId];
	[requestStr appendFormat:@"<productName><![CDATA[%@]]></productName></ProductDetail></productDetails>", prodName];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuID>%@</mainMenuID>", [defaults  valueForKey:KEY_MAINMENUID]];
    [requestStr appendString:@"</ProductDetailsRequest>"];

	NSString *urlString = [NSString stringWithFormat:@"%@wishlist/addWishListProd",BASE_URL];
	NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    [requestStr release];
    
	if ([UtilityManager isNullOrEmptyString:response]){
		return;
    }
	[defaults  setObject:response forKey:KEY_RESPONSEXML];
	TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
	TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
	TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
	if ([[TBXML textForElement:responseCodeElement] isEqualToString:@"10000"]) {
        
        [self animateCellToButton:sender];
        
		[[ScanseeManager sharedManager]setAddToWL:YES];
		[defaults  removeObjectForKey:KEY_SEARCHFROM];
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[TBXML textForElement:responseTextElement] delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
}

-(void)viewListClicked:(id)sender{
	[UtilityManager pushPopToList:self.navigationController];
}
-(void)rateShareClicked:(id)sender{
    
	if([Network currentReachabilityStatus]==0)
	{
		UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error!",@"Error!") message:NSLocalizedString(@"Network is not available",@"Network is not available") delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
		[alert show];
		[alert release];
	}else {
		
		[[ScanseeManager sharedManager]setHotDealToolBar:NO];
        DLog(@"Product ID:%@", prodId);
		[defaults  setObject:prodId forKey:KEY_PRODUCTID];
		RateShareViewController *rateShareVC = [[RateShareViewController alloc] initWithNibName:@"RateShareViewController" bundle:[NSBundle mainBundle]];
		[self.navigationController pushViewController:rateShareVC animated:NO];
		[rateShareVC release];
	}
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[stockHeaderArray release];
	[stockImagePathArray release];
	[labelValueArray release];
	[detailLabelValueArray release];
    [super dealloc];
}


@end

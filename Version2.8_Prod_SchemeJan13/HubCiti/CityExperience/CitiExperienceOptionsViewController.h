//
//  CitiExperienceOptionsViewController.h
//  HubCiti
//
//  Created by Keerthi on 02/06/14.
//  Copyright (c) 2014 span infotech india ptv. ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipesViewController.h"


@interface CitiExperienceOptionsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,CustomizedNavControllerDelegate>
{
    UITableView *optionsTable;
    NSArray *arrOptions;
    SwipesViewController *iSwipeViewController;
    
    WebRequestState iWebRequestState;
    CommonUtility *common;
    NSMutableArray *latArray;
    NSMutableArray *longArray;
    NSMutableArray *retailerNameArr;
    NSMutableArray *retailAddressArr;
    NSMutableArray *retailLocationIdArray;
    NSMutableArray *retailerIdArray ;
    NSMutableArray *retListIdArray;
    NSMutableArray *distanceCEArray;
    NSString *latitude,*longitude,*retailerName,*retailAddress,*retailLocationId,*retailerId,*retListId,*distanceCE;
}

@property(nonatomic,strong)UITableView *optionsTable;
@property(nonatomic,strong)NSArray *arrOptions;
@property(nonatomic,strong)NSString *latitude,*longitude,*retailerName,*retailAddress,*retailLocationId,*retailerId,*retListId,*distanceCE,*cityExpId;
@property(nonatomic,strong)NSMutableArray *latArray;
@property(nonatomic,strong)NSMutableArray *longArray;
@property(nonatomic,strong)NSMutableArray *retailerNameArr;
@property(nonatomic,strong)NSMutableArray *retailAddressArr;
@property(nonatomic,strong)NSMutableArray *retailLocationIdArray;
@property(nonatomic,strong)NSMutableArray *retailerIdArray;
@property(nonatomic,strong)NSMutableArray *retListIdArray;
@property(nonatomic,strong)NSMutableArray *distanceCEArray;
@property(nonatomic,strong)SortAndFilter *sortFilterObj;
@end

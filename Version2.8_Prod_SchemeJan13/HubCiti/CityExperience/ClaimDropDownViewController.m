//
//  ClaimDropDownViewController.m
//  HubCiti
//
//  Created by Nikitha on 12/7/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "ClaimDropDownViewController.h"
#import "ClaimBusinessViewController.h"
#import "ClaimDropDownTableCellTableViewCell.h"
@interface ClaimDropDownViewController ()
{
    NSMutableArray *myArray;
}
@property (strong, nonatomic) IBOutlet UITableView *dropDownTableViewOutlet;
@property(strong,nonatomic) ClaimBusinessViewController * claimBusinessObject;

@end

@implementation ClaimDropDownViewController
@synthesize dropDownTableViewOutlet,array_CategoryNames,claimBusinessObject,resetcatDelegate,contentInTypeOfBusiness;
- (void)viewDidLoad {
    myArray = [[NSMutableArray alloc]init];
    [super viewDidLoad];
    array_CategoryNames = [[NSMutableArray alloc]initWithArray:[defaults valueForKey:@"categoryArray"]];
   [dropDownTableViewOutlet registerNib:[UINib nibWithNibName:@"ClaimDropDownTableCellTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    for (int i =0; i<[contentInTypeOfBusiness componentsSeparatedByString:@","].count; i++)
    {
        if (contentInTypeOfBusiness.length >= 1)
        {
        [myArray addObject:[[contentInTypeOfBusiness componentsSeparatedByString:@","] objectAtIndex:i]];
        }
    }

    //NSArray *myArray = [contentInTypeOfBusiness componentsSeparatedByString:@","];
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return array_CategoryNames.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        return 30.0;
    }
    else
    {
        return 40.0;
        
    }
}
-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   ClaimDropDownTableCellTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

//    if (cell == nil) {
//        ClaimDropDownTableCellTableViewCell  *cell  = [[ClaimDropDownTableCellTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
//    }
  
    [cell setDataToCell:array_CategoryNames[indexPath.row] typeofBusiness:myArray categories:array_CategoryNames[indexPath.row]];
   
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // [tableView reloadData];
    ClaimDropDownTableCellTableViewCell *cell = (ClaimDropDownTableCellTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (cell.checkImageOutlet.image == nil)
    {
        //[myArray removeObjectAtIndex:indexPath.row];
       [myArray addObject:cell.tableViewLabelOutlet.text];
       cell.checkImageOutlet.image = [UIImage imageNamed:@"icon_tick.png"];
    }
    else
    {
        for(int i =0 ;i<myArray.count;i++)
        {
            if ([myArray[i] isEqualToString:array_CategoryNames[indexPath.row]])
            {
                [myArray removeObjectAtIndex:i];
                cell.checkImageOutlet.image = nil;
            }
        }
       
    }
    NSString * selectedStr;
    selectedStr = [myArray componentsJoinedByString:@","];
    [resetcatDelegate updateCategoryTest:selectedStr];
    //[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end

//
//  CustomerInfoListResponse.h
//  HubCiti
//
//  Created by Nikitha on 10/4/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol CustomerInfoListResponse;
@interface CustomerInfoListResponse : NSObject
@property(nonatomic,strong) NSString * name;
@property(nonatomic,strong) NSString * retAddress;
@property(nonatomic,strong) NSString * retAddress2;
@property(nonatomic,strong) NSString * city;
@property(nonatomic,strong) NSString * state;
@property(nonatomic,strong) NSString * postalCode;
@property(nonatomic,strong) NSString * type;
@property(nonatomic,strong) NSString * website;
@property(nonatomic,strong) NSString * country;
@property(nonatomic,strong) NSString * phone;
@property(nonatomic,strong) NSString * claimImg;
@property(nonatomic,strong) NSString * claimTxt;
@property(nonatomic,strong) NSString * isMailAddress;
@property(nonatomic,strong) NSString * keywords;
@end


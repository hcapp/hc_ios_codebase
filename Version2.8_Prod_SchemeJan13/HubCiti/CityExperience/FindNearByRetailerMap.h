//
//  FindNearByRetailerMap.h
//  Scansee
//
//  Created by Sandeep S on 12/12/11.
//  Copyright 2011 span infotech india ptv. ltd. All rights reserved.
//

#import <MapKit/MKMapView.h>
#import <CoreLocation/CoreLocation.h>
#import "MapView.h"
#import "Place.h"

@interface FindNearByRetailerMap : UIViewController <MKMapViewDelegate,CustomizedNavControllerDelegate>
{
    
    IBOutlet MKMapView *mapView;
    NSString *d_latitude;
    NSString *d_longitude;
    NSString *m_lat;
    NSString *m_long;
    NSString *address;
}

@property(nonatomic,strong)IBOutlet MKMapView *mapView;

-(void) showMap;
-(void) getDirection;

@end

//
//  WebBrowserViewController.h
//  Scansee
//
//  Created by ajit on 6/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import <WebKit/WebKit.h>


#import "DWBubbleMenuButton.h"
@class AnyViewController;
@class EmailShareViewController;

BOOL shareFlag;
BOOL anythingShareFlag;


@interface WebBrowserViewController : UIViewController<UIWebViewDelegate,WKUIDelegate, WKNavigationDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,DWBubbleMenuViewDelegate,CustomizedNavControllerDelegate> {
    UIWebView *mWebView;
    UIToolbar* mToolbar;
    UIBarButtonItem* mBack;
    UIBarButtonItem* mForward;
    UIBarButtonItem* mRefresh;
    UIBarButtonItem* mStop;
    UIActivityIndicatorView *activityIndicator;
    NSMutableArray *tempArray;
 
    NSURLConnection *urlConnection;
    NSMutableURLRequest *_request;
    NSString *_Oldrequest;
    BOOL _authenticated;
    WebRequestState iWebRequestState;
    NSHTTPCookie *cookie ;
}
@property(nonatomic,assign) BOOL comingFromClaim;
@property(nonatomic,assign) BOOL comingFromSideMenu;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property (nonatomic, strong) WKWebView* webView;
@property (nonatomic, strong) IBOutlet UIToolbar* toolbar;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* back;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* forward;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* refresh;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* stop;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, copy) NSString *hDLongDesc,*title;
@property(nonatomic,strong) NSString* titleColor,* titleBackgColor;
- (void)updateButtons;

@end

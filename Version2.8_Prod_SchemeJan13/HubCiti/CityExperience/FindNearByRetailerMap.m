//
//  FindNearByRetailerMap.m
//  Scansee
//
//  Created by Sandeep S on 12/12/11.
//  Copyright 2011 span infotech india ptv. ltd. All rights reserved.
//

#import "FindNearByRetailerMap.h"
#import "RetailerAnnotation.h"
#import "AppDelegate.h"
#import "MainMenuViewController.h"
#import "FindOptionsViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@implementation FindNearByRetailerMap

@synthesize mapView;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    //    UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
    //    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //    self.navigationItem.rightBarButtonItem = mainPage;
    //    //[mainPage release];
    //
    //UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    //[mainPage release];
    //customize back button
      self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    if([[HubCitiAppDelegate selIndexArrayFind]count]>0)
        address = [[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :0];
    
    if([[HubCitiAppDelegate selIndexArrayFind]count]>8)
        d_latitude = [[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :8];
    
    if([[HubCitiAppDelegate selIndexArrayFind]count]>9)
        d_longitude = [[HubCitiAppDelegate selIndexArrayFind] objectAtIndex :9];
    
    //	if(! [[defaults valueForKey:@"Directions"]isEqualToString:@"Directions"])
    //    {
    //        self.title = [defaults valueForKey:KEY_RTLNAME];
    //        mapView.hidden = NO;
    //        [mapView setMapType:MKMapTypeStandard];
    //        [mapView setZoomEnabled:YES];
    //        [mapView setScrollEnabled:YES];
    //        [mapView setDelegate:self];
    //
    //
    //
    //        CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake([d_latitude doubleValue],[d_longitude doubleValue]);
    //
    //        CLLocationCoordinate2D northEast = southWest;
    //        RetailerAnnotation *ann = [[RetailerAnnotation alloc] init];
    //        ann.coordinate = CLLocationCoordinate2DMake([d_latitude doubleValue],
    //                                                    [d_longitude doubleValue]);
    //        [mapView addAnnotation:ann];
    //        //[annArray addObject:ann];
    //        [ann setTitle:[defaults valueForKey:KEY_RTLNAME]];
    //        [ann setSubtitle:address];
    //
    //        southWest.latitude = MIN(southWest.latitude, ann.coordinate.latitude);
    //        southWest.longitude = MIN(southWest.longitude, ann.coordinate.longitude);
    //
    //        northEast.latitude = MAX(northEast.latitude, ann.coordinate.latitude);
    //        northEast.longitude = MAX(northEast.longitude, ann.coordinate.longitude);
    //
    //        CLLocation *locSouthWest = [[CLLocation alloc] initWithLatitude:southWest.latitude longitude:southWest.longitude];
    //        CLLocation *locNorthEast = [[CLLocation alloc] initWithLatitude:northEast.latitude longitude:northEast.longitude];
    //
    //        // This is a diag distance (if you wanted tighter you could do NE-NW or NE-SE)
    //       // CLLocationDistance meters = [locSouthWest distanceFromLocation:locNorthEast];
    //
    //        MKCoordinateRegion region;
    //        region.center.latitude = (southWest.latitude + northEast.latitude) / 2.0;
    //        region.center.longitude = (southWest.longitude + northEast.longitude) / 2.0;
    //        //zoom out from location
    //        region.span.latitudeDelta = 0.0125;//meters / 111319.5;
    //        region.span.longitudeDelta = 0.0;
    //
    //
    //        MKCoordinateRegion savedRegion = [mapView regionThatFits:region];
    //        [mapView setRegion:savedRegion animated:YES];
    //        [self.view addSubview:mapView];
    //
    //        [locSouthWest release];
    //        [locNorthEast release];
    //        ReleaseAndNilify(ann);
    //    }
    //    else
    //    {
    //@keerthi adjusted mapview height for iphone5
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:[defaults valueForKey:KEY_RTLNAME] forView:self withHambergur:NO];
    
    MapView* mapView1;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPad) {
        
        mapView1 = [[MapView alloc] initWithFrame:
                    CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)] ;
        
    }
    else if(IS_IPHONE5)
    {
        mapView1 = [[MapView alloc] initWithFrame:
                    CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+88)] ;
    }
    else{
        mapView1 = [[MapView alloc] initWithFrame:
                    CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)] ;
    }
    self.mapView.showsUserLocation = YES;
    [self.view addSubview:mapView1];
    
    Place* source = [[Place alloc] init] ;
    source.name = NSLocalizedString(@"You are Here",@"You are Here");
    //source.addr = @"Address";
    m_lat = [defaults valueForKey:@"LatVal"];
    m_long = [defaults valueForKey:@"LongVal"];
    //m_lat = @"31.546456";
    //m_long = @"-96.545452";
    double tempLat;
    double tempLong;
    tempLat = [m_lat doubleValue];
    tempLong = [m_long doubleValue];
    source.latitude = tempLat;
    source.longitude = tempLong;
    
    Place* destination = [[Place alloc] init] ;
    destination.name = [defaults valueForKey:KEY_RTLNAME];
    destination.addr = address;
    double D_tempLat;
    double D_tempLong;
    D_tempLat = [d_latitude doubleValue];
    D_tempLong = [d_longitude doubleValue];
    //destination.latitude =31.956523;
    //destination.longitude = -96.985625;
    destination.latitude =D_tempLat;
    destination.longitude = D_tempLong;
    
    
    [mapView1 showRouteFrom:source to:destination];
    
    //    }
    // Do any additional setup after loading the view from its nib.
}
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void) showMap
{
    
}
-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}
-(void) getDirection
{
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    MKPinAnnotationView* pinView = nil;
    
    if ([annotation isKindOfClass:[RetailerAnnotation class]])
    {
        // try to dequeue an existing pin view first
        static NSString* RetailerAnnotationIdentifier = @"retailerAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:RetailerAnnotationIdentifier];
        
        if (!pinView)
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
                                                  initWithAnnotation:annotation reuseIdentifier:RetailerAnnotationIdentifier] ;
            customPinView.pinTintColor = [UIColor purpleColor];
            //customPinView.pinTintColor = [UIColor purpleColor];
            customPinView.animatesDrop = YES;
            customPinView.canShowCallout = YES;
            // UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            //customPinView.rightCalloutAccessoryView = infoButton;
            
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
    }
    pinView.canShowCallout = YES;
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    DLog(@"calloutAccessoryControlTapped:");
}



-(void)returnToMainPage:(id)sender{
    showMapFlag=FALSE;
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //  [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    
    else
    {
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

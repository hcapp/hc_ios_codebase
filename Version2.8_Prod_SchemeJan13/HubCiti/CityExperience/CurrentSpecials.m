//
//  CurrentSpecials.m
//  Scansee
//
//  Created by ramanan on 19/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CurrentSpecials.h"
#import "specialofferlistDO.h"
#import "rethotdealsDO.h"
#import "couponViewDO.h"
#import "salesProductDO.h"
#import "MainMenuViewController.h"
#import "HotDealDetailViewController.h"
#import "WebBrowserViewController.h"
#import "NewCouponDetailViewController.h"
#import "SpecialOffersViewController.h"
#import "ProductPage.h"
#import "bottomButtonView.h"
#import "LoginViewController.h"
#import "NativeSpecialOfferViewController.h"
#import "SpecialOfferRetailersViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "CurrentSpecialsTableViewCell.h"


@implementation CurrentSpecials{
    UIActivityIndicatorView *loading;
    
}

@synthesize couponFlag,tableForCoupons;

#pragma mark -
#pragma mark View Loading
-(void)viewDidLoad
{
    [super viewDidLoad];
    loading = nil;
    
   
    couponListArray = [[NSMutableArray alloc]init];
    [tableForCoupons registerNib:[UINib nibWithNibName:@"CurrentSpecialsTableViewCell" bundle:nil] forCellReuseIdentifier:@"specialCell"];

    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
   
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    self.title = @"Deals";

    self.navigationItem.hidesBackButton = YES;
    if(couponFlag)
    {
        [couponListArray removeAllObjects];
        [self request_retloccoup];
    }
}




-(void)returnToMainPage:(id)sender
{
    
    //return to main page
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //   [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    //self.view.backgroundColor = [UIColor colorWithRGBInt:0x084178];
    self.view.backgroundColor = [UIColor whiteColor];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
   
    // Refresh Array when coming Back from HotDeal/Coupon
    if ([SharedManager refreshGallery])
    {
         if(iWebRequestState == RETLOCCOUP)
        {
            iWebRequestState= RETLOCCOUP;
            
            if([[defaults valueForKey:@"strSelectedTypeforDeal"]isEqualToString:@"N/A"])
            {
                [couponListArray removeAllObjects];
                [self request_retloccoup];
            }
        }
        
        [SharedManager setRefreshGallery:NO];
    }
    [super viewWillAppear:animated];
    
}

// Parse Coupon Details
-(void)parse_retloccoup:(NSString *)responseXml
{
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
        return;
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbxml.rootXMLElement];
        NextPageFlagCoupons = 0;
        if(nextPageElement)
            NextPageFlagCoupons = (int)[[TBXML textForElement:nextPageElement]integerValue];
        
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        
        TBXMLElement *couponDetailsElement = [TBXML childElementNamed:@"coupDetails" parentElement:tbxml.rootXMLElement];
        if (couponDetailsElement != nil)
        {
            TBXMLElement *couponDtlElement = [TBXML childElementNamed:@"CouponDetails" parentElement:couponDetailsElement];
            [defaults setValue:nil forKey:@"couponListID"];
            
            while (couponDtlElement != nil) {
                
                couponViewDO *obj_couponViewDO = [[couponViewDO alloc]init];
                
                TBXMLElement *couponNameElement = [TBXML childElementNamed:@"couponName" parentElement:couponDtlElement];
                TBXMLElement *couponIdElement = [TBXML childElementNamed:@"couponId" parentElement:couponDtlElement];
                TBXMLElement *couponImagePathElement = [TBXML childElementNamed:@"couponImagePath" parentElement:couponDtlElement];
                TBXMLElement *couponLongDescriptionElement = [TBXML childElementNamed:@"couponLongDescription" parentElement:couponDtlElement];
                TBXMLElement *usageElement = [TBXML childElementNamed:@"usage" parentElement:couponDtlElement];
                TBXMLElement *viewableOnWebElement = [TBXML childElementNamed:@"viewableOnWeb" parentElement:couponDtlElement];
                 TBXMLElement *isFeaturedElement = [TBXML childElementNamed:@"isFeatured" parentElement:couponDtlElement];
                
                TBXMLElement *couponListIDElement = [TBXML childElementNamed:@"couponListID" parentElement:couponDtlElement];
                
                if(couponNameElement)
                    obj_couponViewDO.couponName = [TBXML textForElement:couponNameElement];
                if(couponIdElement)
                    obj_couponViewDO.couponId = [TBXML textForElement:couponIdElement];
                
                if(couponImagePathElement)
                    obj_couponViewDO.couponImagePath = [TBXML textForElement:couponImagePathElement];
                
                if(couponLongDescriptionElement)
                    obj_couponViewDO.couponLongDescription = [TBXML textForElement:couponLongDescriptionElement];
                
                if(usageElement)
                    obj_couponViewDO.usage = [TBXML textForElement:usageElement];
                
                if(viewableOnWebElement)
                    obj_couponViewDO.viewableOnWeb = [TBXML textForElement:viewableOnWebElement];
                if (isFeaturedElement)
                {
                    obj_couponViewDO.isFeatured = [TBXML textForElement:isFeaturedElement];
                }
                
                
                if(couponListIDElement)
                    obj_couponViewDO.couponListID = [TBXML textForElement:couponListIDElement];
                
                
                couponDtlElement = [TBXML nextSiblingNamed:@"CouponDetails" searchFromElement:couponDtlElement];
                
                [couponListArray addObject:obj_couponViewDO];
                //[obj_couponViewDO release];
            }
        }
        if ([couponListArray count]==1) {
            fromCurrentSpecials = TRUE;
            couponViewDO *obj_couponViewDO = [couponListArray objectAtIndex:0];
            [defaults setValue:obj_couponViewDO.couponId forKey:@"couponId"];
            [defaults setValue:obj_couponViewDO.couponListID forKey:@"couponListId"];
            [defaults  setValue:obj_couponViewDO.couponImagePath forKey:@"imgPath"];
            
            NewCouponDetailViewController *obj_CouponGalleryDetail = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
            obj_CouponGalleryDetail.fromspecialCoupons = true;
            [self.navigationController pushViewController:obj_CouponGalleryDetail animated:NO];
            [self skipMiddleScreen];
            //[obj_CouponGalleryDetail release];
        }
    }
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        [loading stopAnimating];
        ReleaseAndNilify(loading);
        
    }
    
    [tableForCoupons reloadData];
    [defaults setBool:NO forKey:@"ViewMore"];
    
}
-(void) skipMiddleScreen
{
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    
    // You can pass your index here
    NSInteger vcCount = [viewControllers count];
    
    for(NSInteger i = vcCount - 1  ; i >= 0 ;i--) {
        id viewController = [viewControllers objectAtIndex:i];
        if ([viewController isKindOfClass: [self class]]) {
            [viewControllers removeObjectAtIndex: i];
            break;
        }
        
        NSLog(@"viewc:%@",viewController);
    }
    
    self.navigationController.viewControllers = viewControllers;
}

// Request for Coupon List
-(void)request_retloccoup
{
    iWebRequestState = RETLOCCOUP;
    [defaults setValue:@"N/A" forKey:@"strSelectedTypeforDeal"];
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[defaults valueForKey:@"retailLocationID"]];
    [requestStr appendFormat:@"<lastVisitedNo>%lu</lastVisitedNo>",(unsigned long)[couponListArray count]];
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",[defaults valueForKey:KEY_RETAILERID]];
    
    // For User Tracking
    if ([defaults valueForKey:KEY_RLISTID] && [defaults valueForKey:KEY_MAINMENUID]){
        
        [requestStr appendFormat:@"<retListId>%@</retListId>",[defaults valueForKey:KEY_RLISTID]];
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    }
    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    //NSString *urlString = [NSString stringWithFormat:@"http://10.10.220.58:9990/HubCiti_Coupon/thislocation/retloccoup"];
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retloccoup",BASE_URL];
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
        
        NSLog(@"Response %@",response);
        [self parse_retloccoup:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    }

    
}


// handle server respobse Here
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
       
        case RETLOCCOUP:
        {
            NSLog(@"Response %@",response);
            [self parse_retloccoup:response];
        }
            break;
            
        case DEFAULT:
            break;
            
        default:
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return couponListArray.count;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        return 70.0;
    }
    return 60.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    couponViewDO *obj_couponViewDO = [couponListArray objectAtIndex:indexPath.row];
    CurrentSpecialsTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"specialCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"right_arrow.png"]];
    [cell setDetailData:obj_couponViewDO.couponName description:obj_couponViewDO.couponLongDescription CouponImage:obj_couponViewDO.couponImagePath FeaturedOrNot:obj_couponViewDO.isFeatured];
    return cell;
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    if(NextPageFlagCoupons==1 && indexPath.row == [couponListArray count]) {
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"]) {
            [defaults setBool:YES forKey:@"ViewMore"];
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self request_retloccoup];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tableForCoupons reloadData];
                });
            });
            
        }
    }
    
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [defaults setBool:NO forKey:@"ViewMore"];
    if([Network currentReachabilityStatus]==0)
    {
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
    }
    else
    {
        couponViewDO *obj_couponViewDO = [couponListArray objectAtIndex:indexPath.row];
        [defaults setValue:obj_couponViewDO.couponId forKey:@"couponId"];
        [defaults setValue:obj_couponViewDO.couponListID forKey:@"couponListId"];
        [defaults  setValue:obj_couponViewDO.couponImagePath forKey:@"imgPath"];
        
        NewCouponDetailViewController *obj_CouponGalleryDetail = [[NewCouponDetailViewController alloc]initWithNibName:@"NewCouponDetailViewController" bundle:[NSBundle mainBundle]];
        obj_CouponGalleryDetail.fromspecialCoupons = true;
        [self.navigationController pushViewController:obj_CouponGalleryDetail animated:NO];
        //[obj_CouponGalleryDetail release];
    }
    
    
}


-(void)popBackToPreviousPage{
    [defaults setBool:NO forKey:@"ViewMore"];
    
    [self.navigationController popViewControllerAnimated:NO];
}


-(void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


@end

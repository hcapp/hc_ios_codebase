//
//  CurrentSpecialsTableViewCell.m
//  HubCiti
//
//  Created by Nikitha on 1/20/17.
//  Copyright © 2017 Keshavakarthik S. All rights reserved.
//

#import "CurrentSpecialsTableViewCell.h"

@implementation CurrentSpecialsTableViewCell
@synthesize couponNameLabel,descriptionLabel,imageViewForSpecials,featuredImageView,featuredImage;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void) setDetailData : (NSString *) couponName description : (NSString *) couponDescription CouponImage : (NSString * ) couponImage FeaturedOrNot : (NSString *) featuredFlag
{
    couponNameLabel.text = nil;
    descriptionLabel.text = nil;
    imageViewForSpecials.image = nil;
    featuredImage.image = nil;
    couponNameLabel.text = couponName;
    descriptionLabel.text = couponDescription;
    [imageViewForSpecials loadImage:couponImage];
    if ([featuredFlag isEqualToString:@"true"])
    {
        featuredImage.image = [UIImage imageNamed:@"featured_icon.png"];
        featuredImageView.hidden = NO;
    }
    else
    {
        featuredImage.image = nil;
        featuredImageView.hidden = YES;
    }
}
@end

//
//  ClaimDropDownViewController.h
//  HubCiti
//
//  Created by Nikitha on 12/7/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ResetCategoryFieldDelegate <NSObject>
@optional
-(void) updateCategoryTest : (NSString*) categoryName;
@end

@interface ClaimDropDownViewController : UIViewController
@property (nonatomic,weak) id <ResetCategoryFieldDelegate> resetcatDelegate;
@property(nonatomic,strong) NSMutableArray * array_CategoryNames;
@property(nonatomic,strong) NSString * contentInTypeOfBusiness;
@end

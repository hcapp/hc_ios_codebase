//
//  CityExperienceViewController.h
//  Scansee
//
//  Created by Ajit on 1/18/13.
//
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"
#import "CityExperienceDO.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "SwipesViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
NSDictionary *viewDictionary;
@class AnyViewController;
@class EmailShareViewController;
bool navigatedFromCity;
bool popFromCity;
@class FilterListViewController;
@class RetailersListViewController;
@class DealHotDealsList;
@class FilterRetailersList;
@class EventsListViewController;
@class FundraiserListViewController;
@class FAQCategoryList;
@interface CityExperienceViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,HubCitiConnectionManagerDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,SwipesViewControllerDelegate,UISearchBarDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>{
    
     UITableView *table_RetailerList;
    //IBOutlet UIButton *btn1, *btn2, *btn3, *btn4;
    
    //int retAffId;
    int nextPage,PLnextPage;
    int maxCount;
    
    NSMutableArray *PLretailerIdArray,*PLretailerNameArray,*PLretailerLogoArray;
    int PLlastVisitedRecord;
    
    CommonUtility *common;
    int tagValue;
    BOOL fromMainPage;
    
    //NSMutableArray *annArray, *latArray, *longArray;
    NSMutableArray *catIdArray, *catNameArray;
    
    UILabel *titleLabel;
    UIBarButtonItem *opt;
    UIBarButtonItem *showMap;
    UIBarButtonItem* mainPage;
    
    NSString *retAffName;
    
   WebRequestState iWebRequestState;
    
    CityExperienceDO *cityExpDO;
    CEPartnerListDO *partnerListDO;
    CECategoryDO *CityCatDo;
    
    NSMutableArray *cityBottomButtonDO;
    NSMutableArray *cityBottomButtonViewContainer;
    NSMutableArray *bottomButtonList;
    int bottomBtn;
    UISearchBar * search_Experience;
    SwipesViewController *iSwipeViewController;
    UISegmentedControl *segment_Experience;
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,retain) FAQCategoryList *faqList;
@property(nonatomic,retain) FilterListViewController *filterList;
@property(nonatomic,retain) FundraiserListViewController* iFundraiserListViewController;
@property(nonatomic,retain) EventsListViewController *iEventsListViewController;
@property(nonatomic,retain) FilterRetailersList *pvc;
@property(nonatomic,retain) RetailersListViewController *retailerListScreen;
@property(nonatomic,retain) DealHotDealsList* hotDeals;
@property (readwrite) int tagValue;
@property(nonatomic,retain) SortAndFilter *sortFilterObj;
@property(nonatomic,retain) CityExperienceViewController *cityExp;
@property(nonatomic,retain)NSString *cityExpId;

-(void)parse_HubcitiAnythingInfo:(NSString*)response;

@end

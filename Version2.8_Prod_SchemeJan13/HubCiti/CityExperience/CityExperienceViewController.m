//
//  CityExperienceViewController.m
//  Scansee
//
//  Created by Ajit on 1/18/13.
//
//

#import "CityExperienceViewController.h"
#import "LoginViewController.h"
#import "RetailerSummaryViewController.h"
#import "PreferredCategoriesScreen.h"
#import "AboutAndPrivacyScreen.h"
#import "UserInformationViewController.h"
#import "CommonUtility.h"
#import "CategoriesViewController.h"
#import "FilterRetailersList.h"
#import "MainMenuViewController.h"
#import "EmailShareViewController.h"
#import "LocationDetailsViewController.h"
#import "RetailersListViewController.h"
#import "AsyncImageView.h"
#import "TBXML.h"
#import "CitiExperienceOptionsViewController.h"
#import "FindOptionsSortViewController.h"
#import "bottomButtonDO.h"
#import "SingleCatRetailers.h"
#import "EventsListViewController.h"
#import "AlertsList.h"
#import "SettingsViewController.h"
#import "FAQCategoryList.h"
#import "AppDelegate.h"
//#import "ScanNowScreen.h"
#import "WebBrowserViewController.h"
#import "AnythingPage.h"
#import "CouponsViewController.h"
#import "bottomButtonView.h"
#import "FilterListViewController.h"
#import "FundraiserListViewController.h"
#import "DealHotDealsList.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "CitiExpMapViewController.h"
#import "SpecialOffersViewController.h"
#include "HTTPClient.h"
#include "GetUserInfoResponse.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "AppDelegate.h"
#import "HTTPClient.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponsInDealViewController.h"

@interface CityExperienceViewController ()<HTTPClientDelegate>
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;
@property (nonatomic, assign) NSInteger oldSegmentedIndex;


-(void)parse_appsitedetails:(NSString*)response;
@end

@implementation CityExperienceViewController{
    UIActivityIndicatorView *loading;
    bottomButtonView *view;
    UIFont *addressFont;
    float numberOfLines;
    BOOL isLoading;
}
//@synthesize anyVC;
@synthesize infoResponse,oldSegmentedIndex;
@synthesize anyVC,emailSendingVC;
CitiExperienceOptionsViewController *iCitiExperienceOptionsViewController;
static int objIndex = 0;

NSMutableArray *arrCitiExpCategoryInfo,*cityExpObjArray;

static int lastVisitedRecord = 0;
@synthesize tagValue,sortFilterObj,cityExpId;

#pragma mark view methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - Service Respone methods

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    isLoading = false;
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    [self responseData:responseObject];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    isLoading = false;
     [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    [self.view setAccessibilityLabel:@"Uniquely Marble Falls"];
    addressFont = [UIFont systemFontOfSize:(IPAD ? 15.0f:10.0f)];
    loading = nil;
    iCitiExperienceOptionsViewController = [[CitiExperienceOptionsViewController alloc]initWithNibName:@"CitiExperienceOptionsViewController" bundle:[NSBundle mainBundle]];
    
    arrCitiExpCategoryInfo = [[NSMutableArray alloc]init];
    //     [defaults setValue:nil forKey:@"SelectedCityIds"];
    
    sortFilterObj = [[SortAndFilter alloc]init];
    sortFilterObj.distanceSelected = true;
    
    
    UIButton *showMapBtn = [UtilityManager customizeBarButtonItem:@"Show Map"];
    [showMapBtn addTarget:self action:@selector(swapListMap:) forControlEvents:UIControlEventTouchUpInside];
    showMapBtn.tag=1;
    showMap = [[UIBarButtonItem alloc] initWithCustomView:showMapBtn];
    
    
    
    common = [[CommonUtility alloc]init];
    
    //customize back button
//    UIButton *backBtn = [UtilityManager customizeBackButton];
//    
//    [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    
//    self.navigationItem.hidesBackButton = YES;
//    
//    UIButton *hamburgerBtn = [NewsUtility customizedSideMenu:false];
//    UIBarButtonItem *hamburger = [[UIBarButtonItem alloc] initWithCustomView:hamburgerBtn];
//    
//    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:back,hamburger, nil]];
   
    self.navigationItem.hidesBackButton = YES;
    // [back release];
    
    //    titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 320, 40)];
    //    titleLabel.textAlignment = NSTextAlignmentCenter;
    //    titleLabel.textColor = [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]];
    //    titleLabel.backgroundColor = [UIColor clearColor];
    //    titleLabel.numberOfLines = 2;
    //    [titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    //    titleLabel.font = [UIFont boldSystemFontOfSize:16];
    //    titleLabel.text = NSLocalizedString(@"Choose Your Location:",@"Choose Your Location:");
    //self.navigationItem.title = @"Choose Your Location:";
    //home button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
    
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    
    
    //Navigation bar title text, back button and home button
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@"Choose Your Location:" forView:self withHambergur:YES];
    //[titleLabel release];
    
//    UISwipeGestureRecognizer *mSwipeUpRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
//    [mSwipeUpRecognizer setDirection: UISwipeGestureRecognizerDirectionLeft];
//    [self.view addGestureRecognizer:mSwipeUpRecognizer];
//    
//    UISwipeGestureRecognizer *mSwipeUpRecognizerRht = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
//    [mSwipeUpRecognizerRht setDirection: UISwipeGestureRecognizerDirectionRight];
//    [self.view addGestureRecognizer:mSwipeUpRecognizerRht];
    
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
    
    [self allocateArrays];
    nextPage = 0;
    [defaults setValue:nil forKey:@"cityExpMitemId"];
    [self setUIForExperience];
    [self request_CitiExpRet:0 withObj:sortFilterObj];
   
    
}
-(void) setUIForExperience
{
   
    //Segment controll
    
    segment_Experience = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Distance", @"Name", @"Map", nil]];
   // segment_Experience.frame =CGRectMake(0, - VARIABLE_HEIGHT(4), SCREEN_WIDTH, VARIABLE_HEIGHT(45));
    // Vertical constrains
    
    
    
    [segment_Experience setSelectedSegmentIndex:0];
   //  [segment_Experience setSegmentedControlStyle:UISegmentedControlStylePlain];
    [segment_Experience setBackgroundColor:[UIColor grayColor]];
    CGFloat widthForSegment;
    if(IPAD){
        widthForSegment =(SCREEN_WIDTH/3);
    }
    else{
        widthForSegment = (SCREEN_WIDTH/3)+1;
    }
    [segment_Experience setWidth:widthForSegment forSegmentAtIndex:0];
    [segment_Experience setWidth:widthForSegment forSegmentAtIndex:1];
    [segment_Experience setWidth:widthForSegment forSegmentAtIndex:2];
    
    [self setSegmentColor:segment_Experience]; //add customized color
    
    segment_Experience.translatesAutoresizingMaskIntoConstraints = NO;
    [segment_Experience addTarget:self action:@selector(viewSelectionChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segment_Experience];
    viewDictionary = NSDictionaryOfVariableBindings(segment_Experience);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-2)-[segment_Experience(48)]"] options:0 metrics:0 views:viewDictionary]];
    
    
    // Horizontal constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(-2)-[segment_Experience(%f)]",SCREEN_WIDTH+4] options:0 metrics:0 views:viewDictionary]];


    
}
-(void) setTableViewFrame
{
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    if (!table_RetailerList) {
        if(bottomBtn==1){
            table_RetailerList = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-50-44) style:UITableViewStylePlain];
        }
        else{
            table_RetailerList = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-44) style:UITableViewStylePlain];
        }
        table_RetailerList.dataSource=self;
        table_RetailerList.delegate=self;
        [table_RetailerList setBackgroundColor:[UIColor whiteColor]];
        table_RetailerList.tableFooterView.hidden=YES;
        [self.view addSubview:table_RetailerList];
    }
    if (IPAD)
    {
    table_RetailerList.contentInset = UIEdgeInsetsMake(0, 0,VARIABLE_HEIGHT(2.5), 0);
    }
    

}
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
-(void)viewWillAppear:(BOOL)animated{
  
  
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideBackButton:NO];
    [cusNav hideHambergerButton: NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    
    
    ReleaseAndNilify(table_RetailerList);
    table_RetailerList.scrollEnabled = YES;
    table_RetailerList.allowsSelection = YES;
    [super viewWillAppear:YES];
    [arrCitiExpCategoryInfo removeAllObjects];
    if([segment_Experience selectedSegmentIndex] == 2){
        
        [segment_Experience setSelectedSegmentIndex:oldSegmentedIndex];
        [self setSegmentColor:segment_Experience];
        
    }
    if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
        [self allocateArrays];
        nextPage = 0;
        sortFilterObj = [[SortAndFilter alloc]initWithObject:[self loadCustomObjectWithKey:@"SortFilterObject"]];
        DLog(@"%d", sortFilterObj.distanceSelected);
        DLog(@"%d", sortFilterObj.alphabeticallySelected);
        if(sortFilterObj.distanceSelected)
        {
            [segment_Experience setSelectedSegmentIndex:0];
            [self setSegmentColor:segment_Experience];
        }
        if(sortFilterObj.alphabeticallySelected)
        {
            [segment_Experience setSelectedSegmentIndex:1];
            [self setSegmentColor:segment_Experience];
        }
        
        [self request_CitiExpRet:0 withObj:sortFilterObj];
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        [defaults setValue:nil forKey:@"SortFilterObject"];
    }
    
}

-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}

- (void)addItemViewController:(SwipesViewController *)controller didFinishEnteringItem:(SortAndFilter *)item
{
   
    
    //  DLog(@"%d",sortFilterObj.distanceSelected);
    for (UIView *addItemView in self.view.subviews)
        addItemView.userInteractionEnabled=YES;
    iSwipeViewController= nil;
    ReleaseAndNilify(table_RetailerList);
    sortFilterObj = [[SortAndFilter alloc]initWithObject:item];
    table_RetailerList.scrollEnabled = YES;
    table_RetailerList.allowsSelection = YES;
    [super viewWillAppear:YES];
    [arrCitiExpCategoryInfo removeAllObjects];
    [self allocateArrays];
    nextPage = 0;
    if(sortFilterObj.distanceSelected)
    {
        [segment_Experience setSelectedSegmentIndex:0];
        [self setSegmentColor:segment_Experience];
    }
    if(sortFilterObj.alphabeticallySelected)
    {
        [segment_Experience setSelectedSegmentIndex:1];
        [self setSegmentColor:segment_Experience];
    }
    [self request_CitiExpRet:0 withObj:item];
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
    [defaults setValue:nil forKey:@"SortFilterObject"];
}


- (SortAndFilter *)loadCustomObjectWithKey:(NSString *)key {
    
    NSData *encodedObject = [defaults objectForKey:key];
    SortAndFilter *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}


//allocate the city exp object array
-(void)allocateArrays{
    
    if(cityExpObjArray)
    {
        [cityExpObjArray removeAllObjects];
        cityExpObjArray = nil;
    }
    cityExpObjArray = [[NSMutableArray alloc]init];
    
}


//release object arrays
-(void)releaseArrays{
    
    if (cityExpObjArray)
        ReleaseAndNilify(cityExpObjArray);
    
}



//-(void)settableViewOnScreen
//{
//    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
//    //Segment controll
//    
//    segment_Experience = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Distance", @"Name", @"Map", nil]];
//    segment_Experience.frame =CGRectMake(0, 0, SCREEN_WIDTH, VARIABLE_HEIGHT(30));
//    [segment_Experience setSelectedSegmentIndex:0];
//    [segment_Experience setBackgroundColor:[UIColor grayColor]];
//    [segment_Experience setSegmentedControlStyle:UISegmentedControlStylePlain];
//    [segment_Experience setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:0];
//    [segment_Experience setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:1];
//    [segment_Experience setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:2];
//    [self setSegmentColor:segment_Experience]; //add customized color
//    
//    segment_Experience.translatesAutoresizingMaskIntoConstraints = NO;
//    [segment_Experience addTarget:self action:@selector(viewSelectionChanged:) forControlEvents:UIControlEventValueChanged];
//    [self.view addSubview:segment_Experience];
//    yVal = yVal+ segment_Experience.frame.size.height;
//    
//    if (!table_RetailerList) {
//        if(bottomBtn==1){
//            table_RetailerList = [[UITableView alloc]initWithFrame:CGRectMake(0, segment_Experience.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT-yVal-50) style:UITableViewStylePlain];
//        }
//        else{
//            table_RetailerList = [[UITableView alloc]initWithFrame:CGRectMake(0, segment_Experience.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT-yVal) style:UITableViewStylePlain];
//        }
//    }
//    table_RetailerList.dataSource=self;
//    table_RetailerList.delegate=self;
//    [table_RetailerList setBackgroundColor:[UIColor whiteColor]];
//    table_RetailerList.tableFooterView.hidden=YES;
//    [self.view addSubview:table_RetailerList];
//    
//}
-(void)viewSelectionChanged:(id)sender
{
    
    switch ([segment_Experience selectedSegmentIndex])
    {
          
        case 0:{
            ReleaseAndNilify(table_RetailerList);
            [defaults setBool:NO forKey:@"ViewMore"];
            oldSegmentedIndex = 0;
            lastVisitedRecord =0 ;
            [self setSegmentColor:segment_Experience];
            sortFilterObj.distanceSelected = TRUE;
             sortFilterObj.alphabeticallySelected = FALSE;
            [self allocateArrays];
            [self request_CitiExpRet:0 withObj:sortFilterObj];
        }
            break;
        case 1:
        {
            ReleaseAndNilify(table_RetailerList);
              [defaults setBool:NO forKey:@"ViewMore"];
             oldSegmentedIndex = 1;
            lastVisitedRecord =0;
            [self setSegmentColor:segment_Experience];
            sortFilterObj.distanceSelected = FALSE;
            sortFilterObj.alphabeticallySelected = TRUE;
             [self allocateArrays];
            [self request_CitiExpRet:0 withObj:sortFilterObj];
           // [self requestForBandList:nil];
            
        }
            break;
        case 2:
        {
            lastVisitedRecord = 0;
            if ([cityExpObjArray  count]>0) {
                
                CitiExpMapViewController *citiExpMap=[[CitiExpMapViewController alloc]initWithNibName:@"CitiExpMapViewController" bundle:Nil];
                
                citiExpMap.latArray = [[NSMutableArray alloc]init];
                citiExpMap.longArray= [[NSMutableArray alloc]init];
                citiExpMap.retailerNameArr= [[NSMutableArray alloc]init];
                citiExpMap.retailerIdArray= [[NSMutableArray alloc]init];
                citiExpMap.retailAddressArr= [[NSMutableArray alloc]init];
                citiExpMap.retailLocationIdArray= [[NSMutableArray alloc]init];
                citiExpMap.retListIdArray= [[NSMutableArray alloc]init];
                citiExpMap.distanceCEArray= [[NSMutableArray alloc]init];
                for (int i=0; i<[cityExpObjArray count]; i++) {
                    cityExpDO=[cityExpObjArray objectAtIndex:i];
                    [citiExpMap setValue:cityExpDO.latitude forKey:@"latitude"]; //[[NSString alloc]initWithString:[latArray objectAtIndex:i]];citiExpMap.latitude=
                    [citiExpMap setValue:cityExpDO.longitude forKey:@"longitude"];
                    [citiExpMap setValue:cityExpDO.retailerName forKey:@"retailerName"];
                    [citiExpMap setValue:cityExpDO.retailerId forKey:@"retailerId"];
                    [citiExpMap setValue:cityExpDO.retailAddress forKey:@"retailAddress"];
                    [citiExpMap setValue:cityExpDO.retailLocationId forKey:@"retailLocationId"];
                    [citiExpMap setValue:cityExpDO.retListId forKey:@"retListId"];
                    [citiExpMap setValue:cityExpDO.distanceCE forKey:@"distanceCE"];
                    
                    
                    
                    [citiExpMap.latArray addObject: [citiExpMap valueForKey:@"latitude"]];
                    [citiExpMap.longArray addObject: [citiExpMap valueForKey:@"longitude"]];
                    [citiExpMap.retailerNameArr addObject: [citiExpMap valueForKey:@"retailerName"]];
                    [citiExpMap.retailerIdArray addObject: [citiExpMap valueForKey:@"retailerId"]];
                    [citiExpMap.retailAddressArr addObject: [citiExpMap valueForKey:@"retailAddress"]];
                    [citiExpMap.retailLocationIdArray addObject: [citiExpMap valueForKey:@"retailLocationId"]];
                    [citiExpMap.retListIdArray addObject: [citiExpMap valueForKey:@"retListId"]];
                    [citiExpMap.distanceCEArray addObject: [citiExpMap valueForKey:@"distanceCE"]];
                }
                
                
                [self.navigationController pushViewController:citiExpMap animated:NO];
            
    }
            else
            {
            
                    [segment_Experience setSelectedSegmentIndex:oldSegmentedIndex];
                    [self setSegmentColor:segment_Experience];
                
                [UtilityManager showAlert:nil msg:NSLocalizedString(@"Retailers are not available to plot on map",@"Retailers are not available to plot on map")];
            }
        }
    
}
}

-(void) setSegmentColor:(UISegmentedControl *) segmentSender
{
    
    for (int i=0; i<[segmentSender.subviews count]; i++)
    {
        if ([[segmentSender.subviews objectAtIndex:i]isSelected] )
        {
            UIColor *tintcolor=[UIColor colorWithRed:80/255.0 green:199/255.0 blue:228/255.0 alpha:1.0];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
            //break;
        }
        else {
            UIColor *tintcolor=[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
        }
    }
    for (int i=0; i<[segmentSender.subviews count]; i++)
    {
        if (![[segmentSender.subviews objectAtIndex:i]isSelected] )
        {
            UIColor *tintcolor=[UIColor darkGrayColor];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
            // break;
        }
        
    }
    
    float segmentFont = 14.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        segmentFont = 18.0;
    }
    NSDictionary *attributes = @{
                                 NSFontAttributeName : [UIFont boldSystemFontOfSize:segmentFont],
                                 NSForegroundColorAttributeName : [UIColor blackColor]};
    [segmentSender setTitleTextAttributes:attributes forState:UIControlStateSelected];
    
    NSDictionary *attributes2 = @{
                                  NSFontAttributeName : [UIFont boldSystemFontOfSize:segmentFont],
                                  NSForegroundColorAttributeName : [UIColor whiteColor]};
    [segmentSender setTitleTextAttributes:attributes2 forState:UIControlStateNormal];
    
}


-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
    
    }
    
    
}
-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    /*if ([TWTweetComposeViewController canSendTweet])
     {
     TWTweetComposeViewController *tweetSheet =
     [[[TWTweetComposeViewController alloc] init];
     
     UIImage *img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]];
     [tweetSheet addImage:img];
     //[tweetSheet addURL:[NSURL URLWithString:androidDownloadLink]];
     [tweetSheet setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
     
     [self presentViewController:tweetSheet animated:YES];
     
     TWTweetComposeViewControllerCompletionHandler
     completionHandler = ^(TWTweetComposeViewControllerResult result)
     {
     switch (result)
     {
     case TWTweetComposeViewControllerResultCancelled:
     DLog(@"Twitter Result: canceled");
     break;
     case TWTweetComposeViewControllerResultDone:{
     DLog(@"Twitter Result: sent");
     }
     break;
     default:
     DLog(@"Twitter Result: default");
     break;
     }
     [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
     [self dismissModalViewControllerAnimated:YES];
     };
     [tweetSheet setCompletionHandler:completionHandler];
     }
     */
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
        }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
}

-(void) showActionSheet {
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = view;
        actionSheet.popoverPresentationController.sourceRect = view.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}




- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark navigation bar button actions
-(void)popBackToPreviousPage{
    if (isLoading) {
        return;
    }
    [defaults setBool:NO forKey:@"ViewMore"];
    [FindSingleBottomButtonID removeLastObject];
    
    [linkID removeLastObject];
    popFromCity=true;
    [self.navigationController popViewControllerAnimated:NO];
}




-(void)returnToMainPage:(id)sender
{
    if (isLoading) {
        return;
    }
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

//Options button pressed
//-(void) showOptions : (UIBarButtonItem *) sender{
//
//    ReleaseAndNilify(table_RetailerList);
//
//    iCitiExperienceOptionsViewController.latArray = [[NSMutableArray alloc]init];
//    iCitiExperienceOptionsViewController.longArray= [[NSMutableArray alloc]init];
//    iCitiExperienceOptionsViewController.retailerNameArr= [[NSMutableArray alloc]init];
//    iCitiExperienceOptionsViewController.retailerIdArray= [[NSMutableArray alloc]init];
//    iCitiExperienceOptionsViewController.retailAddressArr= [[NSMutableArray alloc]init];
//    iCitiExperienceOptionsViewController.retailLocationIdArray= [[NSMutableArray alloc]init];
//    iCitiExperienceOptionsViewController.retListIdArray= [[NSMutableArray alloc]init];
//    iCitiExperienceOptionsViewController.distanceCEArray= [[NSMutableArray alloc]init];
//
//    for (int i=0; i<[cityExpObjArray count]; i++) {
//        cityExpDO=[cityExpObjArray objectAtIndex:i];
//        [iCitiExperienceOptionsViewController setValue:cityExpDO.latitude forKey:@"latitude"];
//        [iCitiExperienceOptionsViewController setValue:cityExpDO.longitude forKey:@"longitude"];
//        [iCitiExperienceOptionsViewController setValue:cityExpDO.retailerName forKey:@"retailerName"];
//        [iCitiExperienceOptionsViewController setValue:cityExpDO.retailerId forKey:@"retailerId"];
//        [iCitiExperienceOptionsViewController setValue:cityExpDO.retailAddress forKey:@"retailAddress"];
//        [iCitiExperienceOptionsViewController setValue:cityExpDO.retailLocationId forKey:@"retailLocationId"];
//        [iCitiExperienceOptionsViewController setValue:cityExpDO.retListId forKey:@"retListId"];
//        [iCitiExperienceOptionsViewController setValue:cityExpDO.distanceCE forKey:@"distanceCE"];
//        NSLog(@"%@",[iCitiExperienceOptionsViewController valueForKey:@"latitude"]);
//
//        [iCitiExperienceOptionsViewController.latArray addObject: [iCitiExperienceOptionsViewController valueForKey:@"latitude"]];
//        [iCitiExperienceOptionsViewController.longArray addObject: [iCitiExperienceOptionsViewController valueForKey:@"longitude"]];
//        [iCitiExperienceOptionsViewController.retailerNameArr addObject: [iCitiExperienceOptionsViewController valueForKey:@"retailerName"]];
//        [iCitiExperienceOptionsViewController.retailerIdArray addObject: [iCitiExperienceOptionsViewController valueForKey:@"retailerId"]];
//        [iCitiExperienceOptionsViewController.retailAddressArr addObject: [iCitiExperienceOptionsViewController valueForKey:@"retailAddress"]];
//        [iCitiExperienceOptionsViewController.retailLocationIdArray addObject: [iCitiExperienceOptionsViewController valueForKey:@"retailLocationId"]];
//        [iCitiExperienceOptionsViewController.retListIdArray addObject: [iCitiExperienceOptionsViewController valueForKey:@"retListId"]];
//        [iCitiExperienceOptionsViewController.distanceCEArray addObject: [iCitiExperienceOptionsViewController valueForKey:@"distanceCE"]];
//
//    }
//
//    iCitiExperienceOptionsViewController.sortFilterObj = [[SortAndFilter alloc]initWithObject:sortFilterObj];
//    iCitiExperienceOptionsViewController.cityExpId =  [linkID objectAtIndex:[linkID count]-1];
//    [self.navigationController pushViewController:iCitiExperienceOptionsViewController animated:NO];
//    [iCitiExperienceOptionsViewController release];
//
//}
//

#pragma mark Setting of Bottom Tab Bar Buttons
-(void) setBottomBarMenu : (NSMutableArray *)bottomList
{
    NSMutableArray *arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-55, SCREEN_WIDTH, 55)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    for(int btnLoop= 0; btnLoop < [bottomList count]; btnLoop++)
    {
        bottomButtonDO *obj_findBottomDO = [bottomList objectAtIndex:btnLoop];
        
        view = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            view.contentMode = UIViewContentModeCenter;
            view = [bottomButtonView initWithTitle:obj_findBottomDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-55, 80,55) imageName:obj_findBottomDO.bottomBtnImg img_off:obj_findBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            view = [bottomButtonView initWithTitle:obj_findBottomDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[bottomList count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[bottomList count],bottomBarButtonHeight) imageName:obj_findBottomDO.bottomBtnImg img_off:obj_findBottomDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [view setAccessibilityLabel:obj_findBottomDO.btnLinkTypeName];
        [view setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:view];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
    }
    
}

#pragma mark tableView delegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float width = (IPAD ? 570.0f:195.0f);
    float noOflines = [self getLabelSize: [self retailerAddress:indexPath] withSize:width withFont:addressFont];
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        if(noOflines<2)
            return 55;
        else
            return 65;
    }
    else
    {
        return 85;
        
        
    }
    
}



-(CGFloat) getLabelSize : (NSString*) labelString withSize:(CGFloat) labelwidth withFont:(UIFont *) font
{
    
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    CGFloat noOflines = ceil(rect.size.height / font.lineHeight);
    
    
    return noOflines;
    
}

-(NSString *) retailerAddress:(NSIndexPath *)indexPath{
    NSString *address = @"";
    if([cityExpObjArray count]){
        if (indexPath.row != [cityExpObjArray count]){
            if((![[[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress2] isEqualToString:@"N/A"])&& [[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress2].length>0){
                address =  [NSString stringWithFormat:@"%@, %@",
                            [[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress1],[[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress2]];
            }
            else{
                address = [NSString stringWithFormat:@"%@",
                           [[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress1]];
            }
        }
    }
    return address;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(sortFilterObj.distanceSelected==YES ||(sortFilterObj.alphabeticallySelected==YES  && [cityExpObjArray count]))// [defaults setValue:@"category" forKey:@"GroupCitiExpBy"];
    {
        return 20.0;
    }
    else{
        return 0.0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    
    if(nextPage)
        return [cityExpObjArray count]+1;
    
    else
        return [cityExpObjArray count];
    
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //    CECategoryDO *iCitiExpCategoryDO = [arrCitiExpCategoryInfo objectAtIndex:section];
    UILabel *lblCatName;
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH - 10, 18)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
    }
    else
    {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH - 10, 24)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
    }
    
    [lblCatName setTextColor:[UIColor blackColor]];
    [lblCatName setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
    
    //[lblCatName setText:[NSString stringWithFormat:@" Sort by Distance"]];
    
    if (sortFilterObj.distanceSelected == YES) {
        [lblCatName setText:[NSString stringWithFormat:@" Sorted by Distance"]];
    }
    else if (sortFilterObj.alphabeticallySelected == YES) {
        [lblCatName setText:[NSString stringWithFormat:@" Sorted by Name"]];
    }
    return lblCatName;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *cellIdentifier = @"Cell";
    static NSString *cellIdentifier1 = @"More Cell";
    
    SdImageView *asyncImageView = nil;
    UILabel *label = nil;
    UILabel *detailLabel = nil;
    UILabel * addressLabel = nil;
    UITableViewCell *cell;
 
    if (indexPath.row != [cityExpObjArray count])
        cell = [table_RetailerList dequeueReusableCellWithIdentifier:cellIdentifier];
    
    else
        cell = [table_RetailerList dequeueReusableCellWithIdentifier:cellIdentifier1];
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    if (indexPath.row != [cityExpObjArray count]) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] ;
        
        CGRect frame;
        frame.origin.x = 5;
        frame.origin.y = 7;
        frame.size.width = 44;
        frame.size.height = 44;
        asyncImageView = [[SdImageView alloc] initWithFrame:frame] ;
        //asyncImageView.contentMode = UIViewContentModeScaleAspectFill;
        asyncImageView.backgroundColor = [UIColor clearColor];
        asyncImageView.translatesAutoresizingMaskIntoConstraints=NO;
        [cell.contentView addSubview:asyncImageView];

        
        label = [[UILabel alloc] init] ;
        label.translatesAutoresizingMaskIntoConstraints=NO;
        label.textColor = [UIColor colorWithRGBInt:0x112e72];
        [cell.contentView addSubview:label];
        
      
        addressLabel = [[UILabel alloc] init] ;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        addressLabel.numberOfLines = 2;
        }
        else
        {
            addressLabel.numberOfLines = 1;
        }
        addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        addressLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        addressLabel.translatesAutoresizingMaskIntoConstraints=NO;
        [cell.contentView addSubview:addressLabel];
        
       
        detailLabel = [[UILabel alloc] init] ;
        detailLabel.numberOfLines = 1;
        detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        detailLabel.translatesAutoresizingMaskIntoConstraints=NO;
        [cell.contentView addSubview:detailLabel];
        
        float width = (IPAD ? 570.0f:195.0f);
        numberOfLines =[ self getLabelSize:[self retailerAddress:indexPath] withSize:width withFont:addressFont];
        
        NSDictionary *viewsDictionary = @{@"image":asyncImageView,@"label":label,@"addresslabel": addressLabel,@"detaillabel":detailLabel};
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(7)-[image(44)]"] options:0 metrics:0 views:viewsDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(2)-[label(20)]"] options:0 metrics:0 views:viewsDictionary]];
            if(numberOfLines<2){
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(21)-[addresslabel(15)]"] options:0 metrics:0 views:viewsDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(38)-[detaillabel(10)]"] options:0 metrics:0 views:viewsDictionary]];
            }
            else{
                
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(23)-[addresslabel(25)]"] options:0 metrics:0 views:viewsDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(49)-[detaillabel(10)]"] options:0 metrics:0 views:viewsDictionary]];
                
            }
            label.font = [UIFont boldSystemFontOfSize:14];
            addressLabel.font = [UIFont boldSystemFontOfSize:10];
            detailLabel.font = [UIFont boldSystemFontOfSize:10];
            addressLabel.textColor = [UIColor darkGrayColor];
            detailLabel.textColor = [UIColor darkGrayColor];
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(44)]"] options:0 metrics:0 views:viewsDictionary]];
            if(cityExpObjArray.count>0){
            if ([[[cityExpObjArray objectAtIndex:indexPath.row]saleFlag] boolValue])
            {
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(52)-[label(145)]"] options:0 metrics:0 views:viewsDictionary]];
            }
            else
            {
              [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(52)-[label(195)]"] options:0 metrics:0 views:viewsDictionary]];
            }
                
            }
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(52)-[addresslabel(195)]"] options:0 metrics:0 views:viewsDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(52)-[detaillabel(195)]"] options:0 metrics:0 views:viewsDictionary]];

        }
        else
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[image(70)]"] options:0 metrics:0 views:viewsDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(22)]"] options:0 metrics:0 views:viewsDictionary]];
            if(numberOfLines<2){
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(32)-[addresslabel(30)]"] options:0 metrics:0 views:viewsDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(63)-[detaillabel(15)]"] options:0 metrics:0 views:viewsDictionary]];
            }
        else{
            
            
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(32)-[addresslabel(30)]"] options:0 metrics:0 views:viewsDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(63)-[detaillabel(15)]"] options:0 metrics:0 views:viewsDictionary]];
            
        }
        
        
            label.font = [UIFont boldSystemFontOfSize:19];
            addressLabel.font = [UIFont boldSystemFontOfSize:15];
            detailLabel.font = [UIFont boldSystemFontOfSize:15];
            addressLabel.textColor = [UIColor darkGrayColor];
            detailLabel.textColor = [UIColor darkGrayColor];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(70)]"] options:0 metrics:0 views:viewsDictionary]];
            if(cityExpObjArray.count>0){
                
                if ([[[cityExpObjArray objectAtIndex:indexPath.row]saleFlag] boolValue])
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[label(520)]"] options:0 metrics:0 views:viewsDictionary]];
                }
                else
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[label(570)]"] options:0 metrics:0 views:viewsDictionary]];
                }
            }
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[addresslabel(570)]"] options:0 metrics:0 views:viewsDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[detaillabel(570)]"] options:0 metrics:0 views:viewsDictionary]];
        }
        
    }
    else
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier1] ;
    
    
    if (indexPath.row == [cityExpObjArray count] && nextPage == 1) {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;

     
    }
    
    else if (indexPath.row == [cityExpObjArray count] && nextPage == 0) {
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    else if([cityExpObjArray count]>indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        NSString *logoImageStr = [[cityExpObjArray objectAtIndex:indexPath.row]logoImagePath];
        if(![logoImageStr isEqualToString:@"N/A"] )
        {
            [asyncImageView loadImage:[NSString stringWithFormat:@"%@", logoImageStr]];
        }
        
        label.text = [NSString stringWithFormat:@"%@", [[cityExpObjArray objectAtIndex:indexPath.row]retailerName]];

        if(![[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress2] || [[[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress2] isEqualToString:@"N/A"])
        {
       addressLabel.text = [NSString stringWithFormat:@"%@",
                            [[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress1]];
       }
        else
        {
         
            addressLabel.text = [NSString stringWithFormat:@"%@, %@",
                                [[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress1],
                                 [[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress2]];
            
        }
        
        detailLabel.text = [NSString stringWithFormat:@"%@, %@, %@",[[cityExpObjArray objectAtIndex:indexPath.row]city],
                             [(CityExperienceDO*)[cityExpObjArray objectAtIndex:indexPath.row]state],[[cityExpObjArray objectAtIndex:indexPath.row] postalcode]];
        
        if ([[[cityExpObjArray objectAtIndex:indexPath.row]saleFlag] boolValue]) {
            
            UIImageView *specImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"retSpecialsIcon_o.png"]];
           
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                
                specImage.frame = CGRectMake(205, -8, 40, 40);
            }
            else
            {
                specImage.frame = CGRectMake(605, -4, 40, 40);
            }
            
            [cell addSubview:specImage];
            
        }
        UILabel *mileLabel = [[UILabel alloc]init];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            
            mileLabel.frame = CGRectMake(255, -8, 65, 40);

            mileLabel.font = [UIFont boldSystemFontOfSize:10];
        }
        else
        {
            mileLabel.frame = CGRectMake(655, -4, 98, 40);
            mileLabel.font = [UIFont boldSystemFontOfSize:15];
        }
        mileLabel.textColor = [UIColor darkGrayColor];
        mileLabel.text = [[cityExpObjArray objectAtIndex:indexPath.row]distanceCE];
        [cell addSubview:mileLabel];
        if(![[[cityExpObjArray objectAtIndex:indexPath.row]locationOpen] isEqualToString:@"N/A"] && [[cityExpObjArray objectAtIndex:indexPath.row]locationOpen] != nil)
        {
        UILabel * avail = [[UILabel alloc]init];
        if(DEVICE_TYPE == UIUserInterfaceIdiomPhone)
        {
            avail.frame = CGRectMake(255, 15, 40, 40);
            avail.font = [UIFont boldSystemFontOfSize:10];
        }
        else
        {
            avail.frame = CGRectMake(655, 20, 50, 40);
            avail.font = [UIFont boldSystemFontOfSize:15];
        }
        avail.textColor = [UIColor darkGrayColor];
        avail.text = [[cityExpObjArray objectAtIndex:indexPath.row]locationOpen];
        [cell addSubview:avail];
        }
        
    }
   
    return cell;
   
}


/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *cellIdentifier = @"Cell";
    static NSString *cellIdentifier1 = @"More Cell";
    
    SdImageView *asyncImageView = nil;
    UILabel *label = nil;
    UILabel *detailLabel = nil;
    UILabel * addressLabel = nil;
    UITableViewCell *cell;
    
    //    if([defaults valueForKey:@"GroupCitiExpBy"])
    //    {
    //
    //        CECategoryDO *iCitiExpCategoryDO = [arrCitiExpCategoryInfo objectAtIndex:indexPath.section];
    //
    //            if (indexPath.row != [iCitiExpCategoryDO.catObjArray count])
    //                cell = [table_RetailerList dequeueReusableCellWithIdentifier:cellIdentifier];
    //            else
    //                cell = [table_RetailerList dequeueReusableCellWithIdentifier:cellIdentifier1];
    //
    //            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    //
    //            if (indexPath.row != [iCitiExpCategoryDO.catObjArray count]) {
    //                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] ;
    //
    //                CGRect frame;
    //                frame.origin.x = 5;
    //                frame.origin.y = 7;
    //                frame.size.width = 44;
    //                frame.size.height = 44;
    //                asyncImageView = [[[AsyncImageView alloc] init] ;
    //                asyncImageView.contentMode = UIViewContentModeScaleAspectFill;
    //                asyncImageView.backgroundColor = [UIColor clearColor];
    //                asyncImageView.translatesAutoresizingMaskIntoConstraints=NO;
    //                [cell.contentView addSubview:asyncImageView];
    //
    //                frame.origin.x = 52;
    //                frame.origin.y = 0;
    //                frame.size.width = 250;
    //                frame.size.height = 24;
    //                label = [[[UILabel alloc] init] ;
    //                label.textColor = [UIColor colorWithRGBInt:0x112e72];
    //                label.translatesAutoresizingMaskIntoConstraints=NO;
    //                [cell.contentView addSubview:label];
    //
    //                frame.origin.x = 52;
    //                frame.origin.y = 24;
    //                frame.size.width = 250;
    //                frame.size.height = 24;
    //                detailLabel = [[[UILabel alloc] init] ;
    //                detailLabel.numberOfLines = 1;
    //                detailLabel.translatesAutoresizingMaskIntoConstraints=NO;
    //                [cell.contentView addSubview:detailLabel];
    //
    //                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //
    //                NSDictionary *viewsDictionary = @{@"image":asyncImageView,@"label":label,@"detaillabel":detailLabel};
    //                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
    //
    //                    // Vertical constrains
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(7)-[image(44)]-|"] options:0 metrics:0 views:viewsDictionary]];
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[label(24)]-|"] options:0 metrics:0 views:viewsDictionary]];
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(24)-[detaillabel(24)]-|"] options:0 metrics:0 views:viewsDictionary]];
    //                    label.font = [UIFont boldSystemFontOfSize:16];
    //                    detailLabel.font = [UIFont systemFontOfSize:12];
    //                    // Horizontal constraints
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(44)]-|"] options:0 metrics:0 views:viewsDictionary]];
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(52)-[label(200)]-|"] options:0 metrics:0 views:viewsDictionary]];
    //                   [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(52)-[detaillabel(200)]-|"] options:0 metrics:0 views:viewsDictionary]];
    //
    //
    //                }
    //                else
    //                {
    //                    // Vertical constrains
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[image(70)]-|"] options:0 metrics:0 views:viewsDictionary]];
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(7)-[label(34)]-|"] options:0 metrics:0 views:viewsDictionary]];
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(42)-[detaillabel(34)]-|"] options:0 metrics:0 views:viewsDictionary]];
    //
    //                    label.font = [UIFont boldSystemFontOfSize:20];
    //                    detailLabel.font = [UIFont systemFontOfSize:16];
    //
    //                    // Horizontal constraints
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(70)]-|"] options:0 metrics:0 views:viewsDictionary]];
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[label(600)]-|"] options:0 metrics:0 views:viewsDictionary]];
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[detaillabel(600)]-|"] options:0 metrics:0 views:viewsDictionary]];
    //                }
    //
    //            }
    //            else
    //                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier1] ;
    //
    //
    //            if (indexPath.row == [iCitiExpCategoryDO.catObjArray count] && nextPage == 1) {
    //
    //                //CGRect frame;
    //                label = [[[UILabel alloc] init] ;
    //                label.textAlignment = NSTextAlignmentCenter;
    //                label.textColor = [UIColor colorWithRGBInt:0x112e72];
    //                label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
    //                label.translatesAutoresizingMaskIntoConstraints = NO;
    //                [cell.contentView addSubview:label];
    //
    //                NSDictionary *vDictionary=@{@"label":label};
    //                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[label(24)]-|"] options:0 metrics:0 views:vDictionary]];
    //
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(320)]-|"] options:0 metrics:0 views:vDictionary]];
    //                    label.font = [UIFont boldSystemFontOfSize:16];
    //                }
    //                else
    //                {
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[label(44)]-|"] options:0 metrics:0 views:vDictionary]];
    //
    //                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(768)]-|"] options:0 metrics:0 views:vDictionary]];
    //                    label.font = [UIFont boldSystemFontOfSize:20];
    //                }
    //
    //            }
    //
    //            else if (indexPath.row == [iCitiExpCategoryDO.catObjArray count] && nextPage == 0) {
    //
    //                cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //            }
    //
    //            else if([iCitiExpCategoryDO.catObjArray count]>indexPath.row) {
    //
    //                NSString *logoImageStr = [[iCitiExpCategoryDO.catObjArray objectAtIndex:indexPath.row]logoImagePath];
    //                if(![logoImageStr isEqualToString:@"N/A"] )
    //                {
    //                    [asyncImageView loadImage:[NSString stringWithFormat:@"%@", logoImageStr]];
    //                }
    //
    //                label.text = [NSString stringWithFormat:@"%@", [[iCitiExpCategoryDO.catObjArray objectAtIndex:indexPath.row]retailerName]];
    //                detailLabel.text = [NSString stringWithFormat:@"%@ , %@",
    //                                    [[iCitiExpCategoryDO.catObjArray objectAtIndex:indexPath.row]distanceCE],
    //                                    [[iCitiExpCategoryDO.catObjArray objectAtIndex:indexPath.row]retailAddress]];
    //
    //                if ([[[iCitiExpCategoryDO.catObjArray objectAtIndex:indexPath.row]saleFlag] isEqualToString:@"true"]) {
    //
    //                    UIImageView *specImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"retSpecialsIcon_o.png"]];
    //
    //
    //                    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
    //                        specImage.frame = CGRectMake(250, -8, 40, 40);
    //                    }
    //                    else
    //                    {
    //                        specImage.frame = CGRectMake(650, -8, 40, 40);
    //                    }
    //                    [cell addSubview:specImage];
    //                    [specImage release];
    //                }
    //
    //            }
    //        return cell;
    //    }
    //    else{
    if (indexPath.row != [cityExpObjArray count])
        cell = [table_RetailerList dequeueReusableCellWithIdentifier:cellIdentifier];
    
    else
        cell = [table_RetailerList dequeueReusableCellWithIdentifier:cellIdentifier1];
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    if (indexPath.row != [cityExpObjArray count]) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] ;
        
        CGRect frame;
        frame.origin.x = 5;
        frame.origin.y = 7;
        frame.size.width = 44;
        frame.size.height = 44;
        asyncImageView = [[SdImageView alloc] initWithFrame:frame] ;
        //asyncImageView.contentMode = UIViewContentModeScaleAspectFill;
        asyncImageView.backgroundColor = [UIColor clearColor];
        asyncImageView.translatesAutoresizingMaskIntoConstraints=NO;
        [cell.contentView addSubview:asyncImageView];
        
        
        label = [[UILabel alloc] init] ;
        label.translatesAutoresizingMaskIntoConstraints=NO;
        label.textColor = [UIColor colorWithRGBInt:0x112e72];
        [cell.contentView addSubview:label];
        
        
        addressLabel = [[UILabel alloc] init] ;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            addressLabel.numberOfLines = 2;
        }
        else
        {
            addressLabel.numberOfLines = 1;
        }
        addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        addressLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        addressLabel.translatesAutoresizingMaskIntoConstraints=NO;
        [cell.contentView addSubview:addressLabel];
        
        
        detailLabel = [[UILabel alloc] init] ;
        detailLabel.numberOfLines = 1;
        detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        detailLabel.translatesAutoresizingMaskIntoConstraints=NO;
        [cell.contentView addSubview:detailLabel];
        
        float width = (IPAD ? 570.0f:195.0f);
        numberOfLines =[ self getLabelSize:[self retailerAddress:indexPath] withSize:width withFont:addressFont];
        
        NSDictionary *viewsDictionary = @{@"image":asyncImageView,@"label":label,@"addresslabel": addressLabel,@"detaillabel":detailLabel};
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(7)-[image(44)]"] options:0 metrics:0 views:viewsDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(2)-[label(20)]"] options:0 metrics:0 views:viewsDictionary]];
            if(numberOfLines<2){
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(21)-[addresslabel(15)]"] options:0 metrics:0 views:viewsDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(38)-[detaillabel(10)]"] options:0 metrics:0 views:viewsDictionary]];
            }
            else{
                
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(23)-[addresslabel(25)]"] options:0 metrics:0 views:viewsDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(49)-[detaillabel(10)]"] options:0 metrics:0 views:viewsDictionary]];
                
            }
            label.font = [UIFont boldSystemFontOfSize:14];
            addressLabel.font = [UIFont boldSystemFontOfSize:10];
            detailLabel.font = [UIFont boldSystemFontOfSize:10];
            addressLabel.textColor = [UIColor darkGrayColor];
            detailLabel.textColor = [UIColor darkGrayColor];
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(44)]"] options:0 metrics:0 views:viewsDictionary]];
            if(cityExpObjArray.count>0){
                if ([[[cityExpObjArray objectAtIndex:indexPath.row]saleFlag] isEqualToString:@"true"])
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(52)-[label(145)]"] options:0 metrics:0 views:viewsDictionary]];
                }
                else
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(52)-[label(195)]"] options:0 metrics:0 views:viewsDictionary]];
                }
                
            }
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(52)-[addresslabel(195)]"] options:0 metrics:0 views:viewsDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(52)-[detaillabel(195)]"] options:0 metrics:0 views:viewsDictionary]];
            
        }
        else
        {
            // Vertical constrains
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[image(70)]"] options:0 metrics:0 views:viewsDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[label(22)]"] options:0 metrics:0 views:viewsDictionary]];
            if(numberOfLines<2){
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(32)-[addresslabel(30)]"] options:0 metrics:0 views:viewsDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(63)-[detaillabel(15)]"] options:0 metrics:0 views:viewsDictionary]];
            }
            else{
                
                
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(32)-[addresslabel(30)]"] options:0 metrics:0 views:viewsDictionary]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(63)-[detaillabel(15)]"] options:0 metrics:0 views:viewsDictionary]];
                
            }
            
            
            label.font = [UIFont boldSystemFontOfSize:19];
            addressLabel.font = [UIFont boldSystemFontOfSize:15];
            detailLabel.font = [UIFont boldSystemFontOfSize:15];
            addressLabel.textColor = [UIColor darkGrayColor];
            detailLabel.textColor = [UIColor darkGrayColor];
            
            // Horizontal constraints
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(70)]"] options:0 metrics:0 views:viewsDictionary]];
            if(cityExpObjArray.count>0){
                
                if ([[[cityExpObjArray objectAtIndex:indexPath.row]saleFlag] isEqualToString:@"true"])
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[label(520)]"] options:0 metrics:0 views:viewsDictionary]];
                }
                else
                {
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[label(570)]"] options:0 metrics:0 views:viewsDictionary]];
                }
            }
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[addresslabel(570)]"] options:0 metrics:0 views:viewsDictionary]];
            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[detaillabel(570)]"] options:0 metrics:0 views:viewsDictionary]];
        }
        
    }
    else
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier1] ;
    
    
    if (indexPath.row == [cityExpObjArray count] && nextPage == 1) {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
        //            CGRect frame;
        //            label = [[[UILabel alloc] initWithFrame:frame] ;
        //            label.textAlignment = NSTextAlignmentCenter;
        //            label.textColor = [UIColor colorWithRGBInt:0x112e72];
        //            label.text = NSLocalizedString(@"View More Results...",@"View More Results...");
        //            label.translatesAutoresizingMaskIntoConstraints = NO;
        //            [cell.contentView addSubview:label];
        //
        //            NSDictionary *vDictionary=@{@"label":label};
        //            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        //                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(16)-[label(24)]"] options:0 metrics:0 views:vDictionary]];
        //
        //                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[label(320)]"] options:0 metrics:0 views:vDictionary]];
        //                label.font = [UIFont boldSystemFontOfSize:16];
        //            }
        //            else
        //            {
        //                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-16-[label(44)]"] options:0 metrics:0 views:vDictionary]];
        //
        //                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-0-[label(768)]"] options:0 metrics:0 views:vDictionary]];
        //                label.font = [UIFont boldSystemFontOfSize:20];
        //            }
    }
    
    else if (indexPath.row == [cityExpObjArray count] && nextPage == 0) {
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    else if([cityExpObjArray count]>indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        NSString *logoImageStr = [[cityExpObjArray objectAtIndex:indexPath.row]logoImagePath];
        if(![logoImageStr isEqualToString:@"N/A"] )
        {
            [asyncImageView loadImage:[NSString stringWithFormat:@"%@", logoImageStr]];
        }
        
        label.text = [NSString stringWithFormat:@"%@", [[cityExpObjArray objectAtIndex:indexPath.row]retailerName]];
        //        NSString * address1 = @"usdhfu iusdhf hrfier uwrheeyriuyhrtrtyghfyugtuhg iuerhyf";
        //        NSString * address2 = @"dhf hdsf";
        if(![[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress2] || [[[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress2] isEqualToString:@"N/A"])
        {
            addressLabel.text = [NSString stringWithFormat:@"%@",
                                 [[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress1]];
        }
        else
        {
            
            addressLabel.text = [NSString stringWithFormat:@"%@, %@",
                                 [[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress1],
                                 [[cityExpObjArray objectAtIndex:indexPath.row]retailerAddress2]];
            
        }
        
        detailLabel.text = [NSString stringWithFormat:@"%@, %@, %@",[[cityExpObjArray objectAtIndex:indexPath.row]city],
                            [(CityExperienceDO*)[cityExpObjArray objectAtIndex:indexPath.row]state],[[cityExpObjArray objectAtIndex:indexPath.row] postalcode]];
        
        if ([[[cityExpObjArray objectAtIndex:indexPath.row]saleFlag] isEqualToString:@"true"]) {
            
            UIImageView *specImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"retSpecialsIcon_o.png"]];
            // specImage.frame = CGRectMake(250, -8, 40, 40);
            
            //                NSDictionary *sDictionary = @{@"spec":specImage};
            //                specImage.translatesAutoresizingMaskIntoConstraints=NO;
            if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                
                specImage.frame = CGRectMake(205, -8, 40, 40);
            }
            else
            {
                specImage.frame = CGRectMake(605, -4, 40, 40);
            }
            
            [cell addSubview:specImage];
            //[specImage release];
        }
        UILabel *mileLabel = [[UILabel alloc]init];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            
            mileLabel.frame = CGRectMake(255, -8, 65, 40);
            //            mileLabel.layer.borderColor = [[UIColor blueColor ]CGColor];
            //            mileLabel.layer.borderWidth = 1.0;
            mileLabel.font = [UIFont boldSystemFontOfSize:10];
        }
        else
        {
            mileLabel.frame = CGRectMake(655, -4, 98, 40);
            mileLabel.font = [UIFont boldSystemFontOfSize:15];
        }
        mileLabel.textColor = [UIColor darkGrayColor];
        mileLabel.text = [[cityExpObjArray objectAtIndex:indexPath.row]distanceCE];
        [cell addSubview:mileLabel];
        if(![[[cityExpObjArray objectAtIndex:indexPath.row]locationOpen] isEqualToString:@"N/A"] && [[cityExpObjArray objectAtIndex:indexPath.row]locationOpen] != nil)
        {
            UILabel * avail = [[UILabel alloc]init];
            if(DEVICE_TYPE == UIUserInterfaceIdiomPhone)
            {
                avail.frame = CGRectMake(255, 15, 40, 40);
                avail.font = [UIFont boldSystemFontOfSize:10];
            }
            else
            {
                avail.frame = CGRectMake(655, 20, 50, 40);
                avail.font = [UIFont boldSystemFontOfSize:15];
            }
            avail.textColor = [UIColor darkGrayColor];
            avail.text = [[cityExpObjArray objectAtIndex:indexPath.row]locationOpen];
            [cell addSubview:avail];
        }
        
    }
    
    return cell;
    //    }
}

*/


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (cityExpObjArray.count > 0)
    {
        lastVisitedRecord = [[[cityExpObjArray objectAtIndex:[cityExpObjArray count]-1]rowNumber]intValue];
    }
    
    if (indexPath.row == [cityExpObjArray count] && nextPage == 1) {
        // This is the last cell
        if (![defaults boolForKey:@"ViewMore"] && lastVisitedRecord !=0) {
            [defaults setBool:YES forKey:@"ViewMore"];
            segment_Experience.userInteractionEnabled = NO;
            
            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
            dispatch_async(dispatchQueue, ^(void){
                [self request_CitiExpRet:lastVisitedRecord withObj:sortFilterObj];
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [loading stopAnimating];
                    segment_Experience.userInteractionEnabled = YES;
                    if(iSwipeViewController){
                        for (UIView *displayView in self.view.subviews)
                            displayView.userInteractionEnabled=NO;
                        iSwipeViewController.view.userInteractionEnabled = YES;
                    }
                    
                });
            });
            
        }
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (isLoading) {
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:NO];
        return;
    }
    //    if([defaults valueForKey:@"GroupCitiExpBy"])
    //    {
    //        CECategoryDO *iCitiExpCategoryDO = [arrCitiExpCategoryInfo objectAtIndex:indexPath.section];
    //
    //            if (indexPath.row == [iCitiExpCategoryDO.catObjArray count]) {
    //
    //                if (nextPage == 1) {
    ////                    lastVisitedRecord = [[[iCitiExpCategoryDO.catObjArray objectAtIndex:[iCitiExpCategoryDO.catObjArray count]-1]rowNumber]intValue];
    //                    lastVisitedRecord= (int)[cityExpObjArray
    //                                        count];
    //                    [self request_CitiExpRet:lastVisitedRecord];
    //                    //[self viewMoreResults:self];// More records to be shown...pagination
    //                    DLog(@"NEXT PAGE - %d", nextPage);
    //                }
    //
    //                [table_RetailerList deselectRowAtIndexPath:[table_RetailerList indexPathForSelectedRow] animated:YES];
    //                return;
    //            }
    //            [defaults  setObject:[[iCitiExpCategoryDO.catObjArray objectAtIndex:indexPath.row]retailLocationId]
    //                          forKey:@"retailLocationID"];
    //
    //            [defaults  setObject:[[iCitiExpCategoryDO.catObjArray objectAtIndex:indexPath.row]retailerId]
    //                          forKey:KEY_RETAILERID];
    //
    //            [defaults  setObject:[[iCitiExpCategoryDO.catObjArray objectAtIndex:indexPath.row]ribbonAdImagePath]
    //                          forKey:@"ribbonAdImagePath"];
    //
    //            [defaults setObject:[[iCitiExpCategoryDO.catObjArray objectAtIndex:indexPath.row]retailerName]
    //                         forKey:KEY_RTLNAME];
    //
    //            [defaults setObject:[[iCitiExpCategoryDO.catObjArray objectAtIndex:indexPath.row]retListId]
    //                         forKey:KEY_RLISTID];
    //
    //
    //
    //            [linkID insertObject:@"0" atIndex:[linkID count]];
    //            //@Deepak: for user Tracking
    //
    //            [defaults setObject:[[iCitiExpCategoryDO.catObjArray objectAtIndex:indexPath.row]retListId] forKey:KEY_RLISTID];
    //            [defaults setInteger:100 forKey:@"CityExpTableTag"];
    //
    //            [self request_RetSummary:indexPath];
    //
    //    }
    //    else{
    
    
    [defaults setBool:NO forKey:@"ViewMore"];
    if (indexPath.row == [cityExpObjArray count]) {
        
        //            if (nextPage == 1) {
        //                [self viewMoreResults:self];// More records to be shown...pagination
        //                DLog(@"NEXT PAGE - %d", nextPage);
        //            }
        
        [table_RetailerList deselectRowAtIndexPath:[table_RetailerList indexPathForSelectedRow] animated:YES];
        return;
    }
    [defaults  setObject:[[cityExpObjArray objectAtIndex:indexPath.row]retailLocationId]
                  forKey:@"retailLocationID"];
    DLog(@"retailLocationID %@",[[cityExpObjArray objectAtIndex:indexPath.row]retailLocationId]);
    [defaults  setObject:[[cityExpObjArray objectAtIndex:indexPath.row]retailerId]
                  forKey:KEY_RETAILERID];
    
    [defaults  setObject:[[cityExpObjArray objectAtIndex:indexPath.row]ribbonAdImagePath]
                  forKey:@"ribbonAdImagePath"];
    
    [defaults setObject:[[cityExpObjArray objectAtIndex:indexPath.row]retailerName]
                 forKey:KEY_RTLNAME];
    
    [defaults setObject:[[cityExpObjArray objectAtIndex:indexPath.row]retListId]
                 forKey:KEY_RLISTID];
    
    [linkID insertObject:@"0" atIndex:[linkID count]];
    
    //@Deepak: for user Tracking
    
    [defaults setObject:[[cityExpObjArray objectAtIndex:indexPath.row]retListId] forKey:KEY_RLISTID];
    [defaults setInteger:100 forKey:@"CityExpTableTag"];
    
    [self request_RetSummary:indexPath];
    //    }
    [table_RetailerList deselectRowAtIndexPath:[table_RetailerList indexPathForSelectedRow] animated:YES];
}

#pragma mark request methods


//get retailers under a partner
-(void)request_PartnerRet:(int)retailerAffId{
    
    iWebRequestState = PARTNERRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    //    else if ([defaults valueForKey:KEY_MITEMID])
    //        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    
    [requestStr appendFormat:@"<lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",retailerAffId];
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/partnerret",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    ReleaseAndNilify(requestStr);
}


#pragma mark - Service Respone methods



//City Experience Retailers
-(void) request_CitiExpRet:(int)lastRecord withObj:(SortAndFilter*)sortFilterObject{
    
    // Json Request:
    isLoading = true;
    iWebRequestState = CITIEXPRET;
    
    NSMutableDictionary* param = [[NSMutableDictionary alloc] init];
    
    [param setObject:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    
    [param setObject:[defaults  valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    
    if ([defaults valueForKey:KEY_MAINMENUID])
        [param setObject:[defaults  valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    if ([defaults valueForKey:KEY_MITEMID])
        [param setObject:[defaults  valueForKey:KEY_MITEMID] forKey:@"mItemId"];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [param setObject:[defaults  valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        [param setObject:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [param setObject:[defaults  valueForKey:KEYZIPCODE] forKey:@"postalCode"];
    }
   
    
    [param setObject:@"ASC" forKey:@"sortOrder"];
    
    if (sortFilterObject.localSpecialSelected) {
        [param setObject:@"1" forKey:@"locSpecials"];
    }
    else{
        [param setObject:@"0" forKey:@"locSpecials"];
    }
    
    [param setObject:[NSString stringWithFormat:@"%d", lastRecord] forKey:@"lowerLimit"];
    [param setObject:[linkID objectAtIndex:[linkID count]-1] forKey:@"citiExpId"];
    
    if ([CityExperienceBottomButtonID count]>=1)
            [param setObject:[CityExperienceBottomButtonID objectAtIndex:[CityExperienceBottomButtonID count]-1] forKey:@"bottomBtnId"];
    
    else
    {
        if ([defaults  objectForKey:KEY_BOTTOMBUTTONID]) {
            [param setObject:[defaults  objectForKey:KEY_BOTTOMBUTTONID] forKey:@"bottomBtnId"];
        }
    }
    
    
    if (sortFilterObject.selectedInterestsIds) {
        [param setObject:sortFilterObj.selectedInterestsIds forKey:@"interests"];
    }
    if (sortFilterObject.distanceSelected) {
        [param setObject:@"distance" forKey:@"sortColumn"];
        
    }
    
    if (sortFilterObject.alphabeticallySelected) {
         [param setObject:@"atoz" forKey:@"sortColumn"];
        
    }
    
    if (sortFilterObject.selectedCitiIds) {
         [param setObject:sortFilterObj.selectedCitiIds forKey:@"cityIds"];
    }
    
    
    if (sortFilterObject.selectedCatIds) {
        [param setObject:sortFilterObj.selectedCatIds forKey:@"catIds"];
        
    }
    [param setObject:@"IOS" forKey:@"plateform"];
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [timeFormatter setTimeZone:timeZone];
    [timeFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *timeInUTC = [timeFormatter stringFromDate:currentTime];
    NSLog(@"Current time %@", timeInUTC);
    [param setObject:timeInUTC forKey:@"requestedTime"];
     NSString *urlString = [NSString stringWithFormat:@"%@thislocation/citiexpretjson",BASE_URL];
    DLog(@"Url: %@",urlString);
    DLog(@"parameter: %@",param);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    if (![defaults boolForKey:@"ViewMore"]) {
        [HubCitiAppDelegate showActivityIndicator];
    }
    else{
        [HubCitiAppDelegate removeActivityIndicator];
    }
    
    [client sendRequest : param : urlString];
    
    /*iWebRequestState = CITIEXPRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    if ([defaults valueForKey:KEY_MITEMID])
    {
        [defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"cityExpMitemId"];
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else if ([CityExperienceBottomButtonID count]>=1)
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>",[CityExperienceBottomButtonID objectAtIndex:[CityExperienceBottomButtonID count]-1]];
    
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    
    if (sortFilterObject.selectedInterestsIds) {
        [requestStr appendFormat:@"<interests>%@</interests>",sortFilterObj.selectedInterestsIds];
    }
    
    if (sortFilterObject.distanceSelected) {
        [requestStr appendFormat:@"<sortColumn>distance</sortColumn>"];
    }
    
    if (sortFilterObject.alphabeticallySelected) {
        [requestStr appendFormat:@"<sortColumn>atoz</sortColumn>"];
    }
    
    if (sortFilterObject.selectedCitiIds) {
        [requestStr appendFormat:@"<cityIds>%@</cityIds>",sortFilterObj.selectedCitiIds];
    }
    
    [requestStr appendFormat:@"<sortOrder>ASC</sortOrder>"];
    
    if (sortFilterObject.localSpecialSelected) {
        [requestStr appendFormat:@"<locSpecials>1</locSpecials>"];
    }
    else{
        [requestStr appendFormat:@"<locSpecials>0</locSpecials>"];
    }
    
    
    
    if (sortFilterObject.selectedCatIds) {
        [requestStr appendFormat:@"<catIds>%@</catIds>",sortFilterObj.selectedCatIds];
    }
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [timeFormatter setTimeZone:timeZone];
    [timeFormatter setDateFormat:@"HH:mm:ss"];
    NSString *timeInUTC = [timeFormatter stringFromDate:currentTime];
    NSLog(@"Current time %@", timeInUTC);
    
    
    
    [requestStr appendFormat:@"<requestedTime>%@</requestedTime>",timeInUTC];
    
    [requestStr appendFormat:@"<citiExpId>%@</citiExpId><lowerLimit>%d</lowerLimit></ThisLocationRequest>",[linkID objectAtIndex:[linkID count]-1],lastRecord];
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/citiexpret",BASE_URL];
    //NSString *urlString = [NSString stringWithFormat:@"http://10.10.221.27:9990/HubCiti_Toggle/thislocation/citiexpret"];
    
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
        
        
        [self parse_CitiExpRet:response];
        
    }
    else{
        [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    }
    
    */



}


//Retailer Summary
- (void)request_RetSummary:(NSIndexPath*)index {
    
    objIndex = (int)index.row;
    //    if([[defaults valueForKey:@"GroupCitiExpBy"]isEqualToString:@"type"])
    //    {
    //        CECategoryDO *iCitiExpCategoryDO = [arrCitiExpCategoryInfo objectAtIndex:index.section];
    //         [self releaseArrays];
    //        cityExpObjArray = [[NSMutableArray alloc] initWithArray:iCitiExpCategoryDO.catObjArray];
    //    }
    
    iWebRequestState = RETSUMMARY;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[defaults  objectForKey:@"retailLocationID"]];
    
    
    
    //For User Tracking
    if([cityExpObjArray count] > objIndex && [defaults valueForKey:KEY_MAINMENUID]) //retListId, mainMenuId and scanTypeId should go together
    {
        [defaults setObject:[[cityExpObjArray objectAtIndex:objIndex]retListId] forKey:KEY_RLISTID];
        [requestStr appendFormat:@"<retListId>%@</retListId>",[[cityExpObjArray objectAtIndex:objIndex]retListId]];
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId><scanTypeId>0</scanTypeId>",[defaults valueForKey:KEY_MAINMENUID]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",[defaults  valueForKey:KEY_RETAILERID]];
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    [urlString appendString:@"thislocation/retsummary"];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);
}

//upon clicking Accounts
-(void)request_GetUserInfo:(id)sender
{
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    iWebRequestState = GETUSERINFO;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    ReleaseAndNilify(xmlStr);
}

//get user favorite categories
//-(void)request_GetFavCategories{
//
//    iWebRequestState = GETFAVCATEGORIES;
//    if([Network currentReachabilityStatus]==0)
//    {
//        [common showAlert];
//    }
//    else {
//
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//
//    }
//}


#pragma mark parse methods
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case RETSUMMARY:
        {
            [self parse_RetSummary:response];
        }
            break;
            
        case CITIEXPRET:
        {
            [self parse_CitiExpRet:response];
        }
            break;
            
        case GETUSERINFO:
        {
            [self parse_GetUserInfo:response];
        }
            break;
            
        case GETFAVLOCATIONS:
        {
            [self parseGetPreferredCategories:response];
        }
            break;
            
        case HubcitiAnythingInfo:
        {
            [self parse_HubcitiAnythingInfo:response];
        }
            break;
            
        case appsitedetails:
        {
            [self parse_appsitedetails:response];
        }
            break;
            
        default:
            break;
    }
    
}

//parse user favorite categories
-(void)parseGetPreferredCategories:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    // [settings release];
}



//parse city experience retailers
-(void) parse_CitiExpRet:(NSString *)response
{
    /*
     
     <RetailersDetails>
     <responseCode>10000</responseCode>
     <responseText>Success</responseText>
     <bottomBtn>0</bottomBtn>
     <maxCnt>16</maxCnt>
     <nextPage>0</nextPage>
     <retGroupImg>http://10.11.202.76:8080/Images/Exp_Group_Icon.png
     </retGroupImg>
     <retAffCount>3</retAffCount>
     <retAffId>1132</retAffId>
     <retAffName>Filter2Test3</retAffName>
     <mainMenuId>9265</mainMenuId>
     <sortOrder>Asc</sortOrder>
     <sortBy>City</sortBy>
     <groupBy>atoz</groupBy>
     <catList>
     <CategoryInfo>
     <groupContent>D</groupContent>
     <retailerList>
     <Retailer>
					<rowNumber>11</rowNumber>
					<retailerId>813</retailerId>
					<retailLocationId>91863</retailLocationId>
					<retailerName>dfds</retailerName>
					<distance>0 mi</distance>
					<retailAddress>116 W 14th St # 1,MARBLE FALLS,78654,TX
					</retailAddress>
					<logoImagePath>http://10.11.202.76:8080/Images/imageNotFound.png
					</logoImagePath>
					<bannerAdImagePath>N/A</bannerAdImagePath>
					<ribbonAdImagePath>N/A</ribbonAdImagePath>
					<ribbonAdURL>N/A</ribbonAdURL>
					<saleFlag>false</saleFlag>
					<latitude>30.422401</latitude>
					<longitude>-97.796676</longitude>
					<retListId>139090</retListId>
					<city>MARBLE FALLS</city>
     </Retailer>
     <Retailer>
					<rowNumber>12</rowNumber>
					<retailerId>835</retailerId>
					<retailLocationId>91898</retailLocationId>
					<retailerName>dfgdf</retailerName>
					<distance>0 mi</distance>
					<retailAddress>116 W 14th St # 1,MARBLE FALLS,78654,TX
					</retailAddress>
					<logoImagePath>http://10.11.202.76:8080/Images/imageNotFound.png
					</logoImagePath>
					<bannerAdImagePath>N/A</bannerAdImagePath>
					<ribbonAdImagePath>N/A</ribbonAdImagePath>
					<ribbonAdURL>N/A</ribbonAdURL>
					<saleFlag>false</saleFlag>
					<latitude>30.422401</latitude>
					<longitude>-97.796676</longitude>
					<retListId>139091</retListId>
					<city>MARBLE FALLS</city>
     </Retailer>
     </retailerList>
     </CategoryInfo>
     <CategoryInfo>
     <groupContent>F</groupContent>
     <retailerList>
     <Retailer>
					<rowNumber>1</rowNumber>
					<retailerId>31</retailerId>
					<retailLocationId>6595</retailLocationId>
					<retailerName>Famous Footwear Brown Shoe</retailerName>
					<distance>0 mi</distance>
					<retailAddress>116 W 14th St # 1,AUSTIN,78728,TX</retailAddress>
					<logoImagePath>http://localhost:8080/Images/retailer/31/Sears.png
					</logoImagePath>
					<bannerAdImagePath>N/A</bannerAdImagePath>
					<ribbonAdImagePath>N/A</ribbonAdImagePath>
					<ribbonAdURL>N/A</ribbonAdURL>
					<saleFlag>false</saleFlag>
					<latitude>30.422401</latitude>
					<longitude>-97.796676</longitude>
					<retListId>139080</retListId>
					<city>AUSTIN</city>
     </Retailer>
     <Retailer>
					<rowNumber>2</rowNumber>
					<retailerId>31</retailerId>
					<retailLocationId>6620</retailLocationId>
					<retailerName>Famous Footwear Brown Shoe</retailerName>
					<distance>0 mi</distance>
					<retailAddress>116 W 14th St # 1,AUSTIN,78756,TX</retailAddress>
					<logoImagePath>http://localhost:8080/Images/retailer/31/Sears.png
					</logoImagePath>
					<bannerAdImagePath>N/A</bannerAdImagePath>
					<ribbonAdImagePath>N/A</ribbonAdImagePath>
					<ribbonAdURL>N/A</ribbonAdURL>
					<saleFlag>false</saleFlag>
					<latitude>30.422401</latitude>
					<longitude>-97.796676</longitude>
					<retListId>139081</retListId>
					<city>AUSTIN</city>
     </Retailer>
     <Retailer>
					<rowNumber>3</rowNumber>
					<retailerId>31</retailerId>
					<retailLocationId>6642</retailLocationId>
					<retailerName>Famous Footwear Brown Shoe</retailerName>
					<distance>0 mi</distance>
					<retailAddress>116 W 14th St # 1,AUSTIN,78728,TX</retailAddress>
					<logoImagePath>http://localhost:8080/Images/retailer/31/Sears.png
					</logoImagePath>
					<bannerAdImagePath>N/A</bannerAdImagePath>
					<ribbonAdImagePath>N/A</ribbonAdImagePath>
					<ribbonAdURL>N/A</ribbonAdURL>
					<saleFlag>false</saleFlag>
					<latitude>30.422401</latitude>
					<longitude>-97.796676</longitude>
					<retListId>139082</retListId>
					<city>AUSTIN</city>
     </Retailer>
					.
					.
					
     </retailerList>
     </CategoryInfo>
     .
     .
     .
     .
     </catList>
     </RetailersDetails>
     
     
     
     
     */
    
    /*TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if([[TBXML textForElement:responseCodeElement]intValue]==10000) {
        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbXml.rootXMLElement]]intValue];
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbXml.rootXMLElement];
        nextPage = [[TBXML textForElement:nextPageElement] intValue];
        DLog(@"%d",nextPage);
        
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbXml.rootXMLElement];
        if(mainMenuElement)
            [defaults setValue:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
        
        
        TBXMLElement *retAffIDElement = [TBXML childElementNamed:@"retAffId" parentElement:tbXml.rootXMLElement];
        [SharedManager setRetAffId :[[TBXML textForElement:retAffIDElement]intValue]];
        
        TBXMLElement *retAffCountElement = [TBXML childElementNamed:@"retAffCount" parentElement:tbXml.rootXMLElement];
        if(retAffCountElement)
            [SharedManager setFiltersCount:[[TBXML textForElement:retAffCountElement]intValue]];
        else
            [SharedManager setFiltersCount:0];
        
        TBXMLElement *retAffNameElement = [TBXML childElementNamed:@"retAffName" parentElement:tbXml.rootXMLElement];
        if (retAffNameElement!=nil)
            retAffName = [[TBXML textForElement:retAffNameElement]copy];
        [defaults setValue:retAffName forKey:@"Title"];
        
        //             TBXMLElement *categoryListElement = [TBXML childElementNamed:@"catList" parentElement:tbXml.rootXMLElement];
        //             if(categoryListElement)
        //             {
        //
        //                 TBXMLElement *CategoryInfoElement = [TBXML childElementNamed:@"CategoryInfo" parentElement:categoryListElement];
        //                while (CategoryInfoElement)
        //                {
        //                    CityCatDo = [[CECategoryDO alloc]init];
        //                    CityCatDo.catObjArray = [[NSMutableArray alloc]init];
        //                 BOOL isCatRepeted = NO;
        ////                 TBXMLElement *categoryIdElement = [TBXML childElementNamed:@"categoryId" parentElement:CategoryInfoElement];
        //                 TBXMLElement *groupContentElement = [TBXML childElementNamed:@"groupContent" parentElement:CategoryInfoElement];
        //
        //
        //
        ////                 if(categoryIdElement)
        ////                     CityCatDo.catId = [TBXML textForElement:categoryIdElement];
        //                 if(groupContentElement)
        //                     CityCatDo.catName = [TBXML textForElement:groupContentElement];
        //
        //
        //                     // Check if Last Object of Category and Incoming First Category both are same then append data
        //                     if([arrCitiExpCategoryInfo count] > 0 && ![CityCatDo.catName isEqualToString:@"N/A"])
        //                     {
        //                         CECategoryDO *iCitiExpCategoryDOrepted = [[[CECategoryDO alloc]init];
        //
        //                         for(int i=0; i<[arrCitiExpCategoryInfo count]; i++)
        //                         {
        //                             iCitiExpCategoryDOrepted = [arrCitiExpCategoryInfo objectAtIndex:i];
        //                             isCatRepeted = NO;
        //                             if([iCitiExpCategoryDOrepted.catName isEqualToString:CityCatDo.catName])
        //                             {
        //                                 isCatRepeted = YES;
        //
        //                                 [CityCatDo.catObjArray addObjectsFromArray:iCitiExpCategoryDOrepted.catObjArray];
        //
        //                                 [arrCitiExpCategoryInfo replaceObjectAtIndex:i withObject:CityCatDo];
        //                                 break;
        //                             }
        //
        //                         }
        //                         if(isCatRepeted==NO)
        //                         {
        //                            [arrCitiExpCategoryInfo addObject:CityCatDo];
        //                         }
        //
        //                     }
        //                     else
        //                     {
        //                         if (![CityCatDo.catName isEqualToString:@"N/A"])
        //                             [arrCitiExpCategoryInfo addObject:CityCatDo];
        //
        //                     }
        
        TBXMLElement *retailerListElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        
        TBXMLElement *retailerElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerListElement];
        
        while (retailerElement != nil) {
            cityExpDO = [[CityExperienceDO alloc]init];
            TBXMLElement *rowNumberElement = [TBXML childElementNamed:@"rowNumber" parentElement:retailerElement];
            TBXMLElement *retailerIdElement = [TBXML childElementNamed:KEY_RETAILERID parentElement:retailerElement];
            TBXMLElement *retailerNameElement = [TBXML childElementNamed:KEY_RTLNAME parentElement:retailerElement];
            TBXMLElement *retailLocationIDElement = [TBXML childElementNamed:@"retailLocationId" parentElement:retailerElement];
            TBXMLElement *distanceElement = [TBXML childElementNamed:KEY_DISTANCE parentElement:retailerElement];
            TBXMLElement *retailAddressElement = [TBXML childElementNamed:@"completeAddress" parentElement:retailerElement];
            TBXMLElement *logoImagePathElement = [TBXML childElementNamed:@"logoImagePath" parentElement:retailerElement];
            TBXMLElement *bannerAdImagePathElement = [TBXML childElementNamed:@"bannerAdImagePath" parentElement:retailerElement];
            TBXMLElement *ribbonAdImagePathElement = [TBXML childElementNamed:@"ribbonAdImagePath" parentElement:retailerElement];
            TBXMLElement *ribbonAdURLElement = [TBXML childElementNamed:@"ribbonAdURL" parentElement:retailerElement];
            TBXMLElement *latitudeElement = [TBXML childElementNamed:@"retLatitude" parentElement:retailerElement];
            TBXMLElement *longitudeElement = [TBXML childElementNamed:@"retLongitude" parentElement:retailerElement];
            TBXMLElement *saleFlagElement = [TBXML childElementNamed:@"saleFlag" parentElement:retailerElement];
            TBXMLElement *retaileraddress1Element = [TBXML childElementNamed:@"retaileraddress1" parentElement:retailerElement];
            TBXMLElement *retaileraddress2Element = [TBXML childElementNamed:@"retaileraddress2" parentElement:retailerElement];
            TBXMLElement *cityElement = [TBXML childElementNamed:@"city" parentElement:retailerElement];
            TBXMLElement *stateElement = [TBXML childElementNamed:@"state" parentElement:retailerElement];
            TBXMLElement *postalCodeElement = [TBXML childElementNamed:@"postalCode" parentElement:retailerElement];
            TBXMLElement * locationOpenElement = [TBXML childElementNamed:@"locationOpen" parentElement:retailerElement];
            
            //@Deepak: Parse retListID for UserTracking
            TBXMLElement *retListIDElement = [TBXML childElementNamed:KEY_RLISTID parentElement:retailerElement];
            if (retListIDElement != nil)
            {
                cityExpDO.retListId = [TBXML textForElement:retListIDElement];
            }
            
            if (rowNumberElement != nil) {
                
                cityExpDO.rowNumber = [TBXML textForElement:rowNumberElement];
            }
            
            if (retailerIdElement != nil) {
                cityExpDO.retailerId = [TBXML textForElement:retailerIdElement];
            }
            
            if (retailerNameElement != nil) {
                cityExpDO.retailerName = [TBXML textForElement:retailerNameElement];
            }
            
            if (retailLocationIDElement != nil) {
                
                cityExpDO.retailLocationId = [TBXML textForElement:retailLocationIDElement];
            }
            
            if (logoImagePathElement != nil) {
                cityExpDO.logoImagePath = [TBXML textForElement:logoImagePathElement ];
            }
            
            
            if (distanceElement != nil) {
                cityExpDO.distanceCE = [TBXML textForElement:distanceElement];
            }
            if (retaileraddress1Element != nil) {
                cityExpDO.retailerAddress1 = [TBXML textForElement:retaileraddress1Element];
            }
            if (retaileraddress2Element != nil) {
                cityExpDO.retailerAddress2 = [TBXML textForElement:retaileraddress2Element];
            }
            
            if (locationOpenElement != nil)
            {
                cityExpDO.locationOpen = [TBXML textForElement:locationOpenElement];
            }
            
            if (cityElement != nil) {
                cityExpDO.city = [TBXML textForElement:cityElement];
            }
            if (stateElement != nil) {
                cityExpDO.state = [TBXML textForElement:stateElement];
            }
            if (postalCodeElement != nil) {
                cityExpDO.postalcode = [TBXML textForElement:postalCodeElement];
            }
            
            if (retailAddressElement != nil) {
                
                cityExpDO.retailAddress = [TBXML textForElement:retailAddressElement];
            }
            
            if (bannerAdImagePathElement != nil) {
                
                cityExpDO.bannerAdImagePath = [TBXML textForElement:bannerAdImagePathElement ];
            }
            
            if (ribbonAdImagePathElement != nil) {
                cityExpDO.ribbonAdImagePath = [TBXML textForElement:ribbonAdImagePathElement ];
            }
            
            if (ribbonAdURLElement != nil) {
                cityExpDO.ribbonAdURL = [TBXML textForElement:ribbonAdURLElement ];
            }
            
            if (latitudeElement!= nil){
                cityExpDO.latitude = [TBXML textForElement:latitudeElement];
            }
            if (longitudeElement!= nil){
                cityExpDO.longitude = [TBXML textForElement:longitudeElement];
            }
            
            if (saleFlagElement != nil){
                cityExpDO.saleFlag = [TBXML textForElement:saleFlagElement];
            }
            [cityExpObjArray addObject:cityExpDO];
            //                        [CityCatDo.catObjArray addObject:cityExpDO];
            
            retailerElement = [TBXML nextSiblingNamed:@"RetailerDetail" searchFromElement:retailerElement];
            
        }
        //                    CategoryInfoElement = [TBXML nextSiblingNamed:@"CategoryInfo" searchFromElement:CategoryInfoElement];
        //                }
        //             }
        
        if (bottomBtn==1) {
            
            bottomButtonList = [[NSMutableArray alloc]init];
            
            TBXMLElement *bottomList = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbXml.rootXMLElement];
            
            TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomList];
            
            while (BottomButtonElement != nil) {
                
                bottomButtonDO *bottomButton = [[bottomButtonDO alloc]init];
                
                TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                
                if(bottomBtnIDElement)
                    bottomButton.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                
                if(bottomBtnNameElement)
                    bottomButton.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                else
                    bottomButton.bottomBtnName = @" ";
                
                if(bottomBtnImgElement)
                    bottomButton.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                else
                    bottomButton.bottomBtnImg = @" ";
                
                if(bottomBtnImgOffElement)
                    bottomButton.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                else
                    bottomButton.bottomBtnImgOff = @" ";
                
                if(btnLinkTypeIDElement)
                    bottomButton.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                
                if(btnLinkTypeNameElement)
                    bottomButton.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                
                if(btnLinkIDElement){
                    bottomButton.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                    
                }
                
                if(positionElement)
                    bottomButton.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                
                
                
                BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                [bottomButtonList addObject:bottomButton];
                //[bottomButton release];
            }
        }
        if ([bottomButtonList count]>0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self setBottomBarMenu:bottomButtonList];
            });
            
        }
        if(![defaults boolForKey:@"ViewMore"])
        {
            [table_RetailerList setContentOffset:CGPointZero animated:NO];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self setTableViewFrame];
        });
        
        
        //         [table_RetailerList reloadData];
        
    }
    
    else if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]==10005){
        
        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbXml.rootXMLElement]]intValue];
        
        if (bottomBtn==1) {
            
            bottomButtonList = [[NSMutableArray alloc]init];
            
            TBXMLElement *bottomList = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbXml.rootXMLElement];
            
            TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomList];
            
            while (BottomButtonElement != nil) {
                
                bottomButtonDO *bottomButton = [[bottomButtonDO alloc]init];
                
                TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                
                if(bottomBtnIDElement)
                    bottomButton.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                
                if(bottomBtnNameElement)
                    bottomButton.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                else
                    bottomButton.bottomBtnName = @" ";
                
                if(bottomBtnImgElement)
                    bottomButton.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                else
                    bottomButton.bottomBtnImg = @" ";
                
                if(bottomBtnImgOffElement)
                    bottomButton.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                else
                    bottomButton.bottomBtnImgOff = @" ";
                
                if(btnLinkTypeIDElement)
                    bottomButton.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                
                if(btnLinkTypeNameElement)
                    bottomButton.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                
                if(btnLinkIDElement){
                    bottomButton.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                    
                }
                
                if(positionElement)
                    bottomButton.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                
                
                
                BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                [bottomButtonList addObject:bottomButton];
                //[bottomButton release];
            }
        }
        if ([bottomButtonList count]>0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self setBottomBarMenu:bottomButtonList];
            });
        }
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        NSString *response=[TBXML textForElement:responseTextElement];
        if([response isEqualToString:@"No Records Found."])
        {
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:response];
        }
        else{
            NSString *responseTextStr = [[TBXML textForElement:responseTextElement] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
            [UtilityManager showFormatedAlert:responseTextStr];
        }
        //[UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:[NSString stringWithFormat:@"%@", yourAttributedString]];
        if(![defaults boolForKey:@"ViewMore"])
        {
            [table_RetailerList setContentOffset:CGPointZero animated:NO];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self setTableViewFrame];
        });
        
    }
    
    else {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        NSString *responseTextStr = [TBXML textForElement:responseTextElement] ;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        [loading stopAnimating];
        ReleaseAndNilify(loading);
        
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        [table_RetailerList reloadData];
    });
    
    
    [defaults setBool:NO forKey:@"ViewMore"];
     */
   
    NSDictionary* responseData = (NSDictionary*) response;
    
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    bottomBtn = [[responseData objectForKey:@"bottomBtn"] intValue];
    if (responseCode == 10000) {
        
        
        nextPage =  [[responseData objectForKey:@"nextPage"] intValue];
        [defaults setValue:[responseData objectForKey:KEY_MAINMENUID] forKey:KEY_MAINMENUID];
        [SharedManager setRetAffId :[[responseData objectForKey:@"retAffId"]intValue]];
        int retAffCountElement = [[responseData objectForKey:@"retAffCount"]intValue];
        
        if(retAffCountElement)
            [SharedManager setFiltersCount:retAffCountElement];
        else
            [SharedManager setFiltersCount:0];
        
        NSString* retAffNameElement = [responseData objectForKey:@"retAffName"];
        if (retAffNameElement!=nil)
            [defaults setValue:retAffNameElement forKey:@"Title"];
        NSArray* retailerElement = [responseData objectForKey:@"retailerDetail"];
        for(int i = 0; i< retailerElement.count; i++){
            
            cityExpDO = [[CityExperienceDO alloc]init];
            cityExpDO.rowNumber = [[retailerElement objectAtIndex:i] objectForKey:@"rowNumber"];
            cityExpDO.retailerId = [[retailerElement objectAtIndex:i] objectForKey:@"retailerId"];
            cityExpDO.retailerName = [[retailerElement objectAtIndex:i] objectForKey:@"retailerName"];
            cityExpDO.retailLocationId = [[retailerElement objectAtIndex:i] objectForKey:@"retailLocationId"];
            cityExpDO.retailerAddress1 = [[retailerElement objectAtIndex:i] objectForKey:@"retaileraddress1"];
            cityExpDO.retailerAddress2 = [[retailerElement objectAtIndex:i] objectForKey:@"retaileraddress2"];
            cityExpDO.logoImagePath = [[retailerElement objectAtIndex:i] objectForKey:@"logoImagePath"];
            cityExpDO.city = [[retailerElement objectAtIndex:i] objectForKey:@"city"];
            cityExpDO.state = [[retailerElement objectAtIndex:i] objectForKey:@"state"];
            cityExpDO.postalcode = [[retailerElement objectAtIndex:i] objectForKey:@"postalCode"];
            cityExpDO.distanceCE = [[retailerElement objectAtIndex:i] objectForKey:@"distance"];
            cityExpDO.retailAddress = [[retailerElement objectAtIndex:i] objectForKey:@"completeAddress"];
            cityExpDO.bannerAdImagePath = [[retailerElement objectAtIndex:i] objectForKey:@"bannerAdImagePath"];
            cityExpDO.ribbonAdImagePath = [[retailerElement objectAtIndex:i] objectForKey:@"ribbonAdImagePath"];
            cityExpDO.ribbonAdURL = [[retailerElement objectAtIndex:i] objectForKey:@"ribbonAdURL"];
            cityExpDO.latitude = [[retailerElement objectAtIndex:i] objectForKey:@"retLatitude"];
            cityExpDO.longitude = [[retailerElement objectAtIndex:i] objectForKey:@"retLongitude"];
            cityExpDO.saleFlag = [[retailerElement objectAtIndex:i] objectForKey:@"saleFlag"];
            cityExpDO.retListId = [[retailerElement objectAtIndex:i] objectForKey:@"retListId"];
            cityExpDO.locationOpen = [[retailerElement objectAtIndex:i] objectForKey:@"locationOpen"];
            
            [cityExpObjArray addObject:cityExpDO];
            
        }
        
        if (bottomBtn==1) {
            
            bottomButtonList = [[NSMutableArray alloc]init];
            NSArray* bottomArray = [responseData objectForKey:@"bottomBtnList"];
            for(int i = 0; i< bottomArray.count; i++){
                bottomButtonDO *bottomButton = [[bottomButtonDO alloc]init];
                bottomButton.bottomBtnID  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnID"];
                bottomButton.bottomBtnName  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnName"];
                bottomButton.bottomBtnImg  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImg"];
                bottomButton.bottomBtnImgOff  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImgOff"];
                bottomButton.btnLinkTypeID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeID"];
                bottomButton.btnLinkTypeName  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeName"];
                bottomButton.btnLinkID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkID"];
                bottomButton.position  = [[bottomArray objectAtIndex:i] objectForKey:@"position"];
                [bottomButtonList addObject:bottomButton];
                
            }
        }
        
        if ([bottomButtonList count]>0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self setBottomBarMenu:bottomButtonList];
            });
            
        }
        if(![defaults boolForKey:@"ViewMore"])
        {
            [table_RetailerList setContentOffset:CGPointZero animated:NO];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self setTableViewFrame];
        });
        

    }
    else if(responseCode == 10005)
    {
        if (bottomBtn==1) {
            
            bottomButtonList = [[NSMutableArray alloc]init];
            NSArray* bottomArray = [responseData objectForKey:@"bottomBtnList"];
            for(int i = 0; i< bottomArray.count; i++){
                bottomButtonDO *bottomButton = [[bottomButtonDO alloc]init];
                bottomButton.bottomBtnID  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnID"];
                bottomButton.bottomBtnName  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnName"];
                bottomButton.bottomBtnImg  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImg"];
                bottomButton.bottomBtnImgOff  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImgOff"];
                bottomButton.btnLinkTypeID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeID"];
                bottomButton.btnLinkTypeName  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeName"];
                bottomButton.btnLinkID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkID"];
                bottomButton.position  = [[bottomArray objectAtIndex:i] objectForKey:@"position"];
                [bottomButtonList addObject:bottomButton];
                
            }
        }
        if ([bottomButtonList count]>0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self setBottomBarMenu:bottomButtonList];
            });
        }
       
        if([responseText isEqualToString:@"No Records Found."])
        {
            isLoading = false;
            [self setTableViewFrame];
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseText];
            return;
        }
        else{
            isLoading = false;
            [self setTableViewFrame];
            NSString *responseTextStr = [responseText stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
            [UtilityManager showFormatedAlert:responseTextStr];
            return;
        }
      
        if(![defaults boolForKey:@"ViewMore"])
        {
            [table_RetailerList setContentOffset:CGPointZero animated:NO];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self setTableViewFrame];
        });


    }
    else{
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseText];
        [loading stopAnimating];
        isLoading = false;
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
      
       
        [table_RetailerList reloadData];
        });

    if([defaults boolForKey:@"ViewMore"])
    {
        [loading stopAnimating];
        [defaults setBool:NO forKey:@"ViewMore"];
    }
    
    isLoading = false;
  
}



//parse retailer summary
-(void)parse_RetSummary:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [[cityExpObjArray objectAtIndex:objIndex]distanceCE];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}


-(void)parse_GetUserInfo:(id)response
{
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}


#pragma mark pagination
-(void)viewMoreResults:(id)sender{
    
    if (nextPage == 1) {//fetch next set of city experience retailers
        lastVisitedRecord = [[[cityExpObjArray objectAtIndex:[cityExpObjArray count]-1]rowNumber]intValue];
        [self request_CitiExpRet:lastVisitedRecord withObj:sortFilterObj];
    }
    else {
        return;
    }
}



-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    navigatedFromMainMenu=false;
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //  [retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            [defaults  setBool:NO forKey:@"showBackButton"];
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                // [locationDetailsScreen release];
            }
        }
    }
}
//Call Find Service and Class
-(void)navigateToFindView
{
    
    [defaults  setBool:NO forKey:@"findsplash"];
    [defaults setBool:YES forKey:BottomButton];
    //[defaults setValue:@"0" forKey:KEY_BOTTOMBUTTONID];
    //[defaults setValue:nil forKey:KEY_MITEMID];
    navigatedFromCity=true;
    //[FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    //[self navigateToFindView];
    navigatedFromMainMenu=false;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}


-(void)navigateToFind{
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if(menulevel > 1)
        [defaults  setBool:NO forKey:@"findsplash"];
    else
        [defaults  setBool:YES forKey:@"findsplash"];
    //navigatedFromMainMenu = TRUE;
    // navigatedTOFindFromMainMenu = TRUE;
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    // [mmvc release];
    
}
#pragma mark - Bottom Bar buttons new additions
//@ added by Keshava : Dynamic implementation of Bottom Bar buttons
-(void)bottomButtonPressed:(id)sender
{
    if (isLoading) {
        return;
    }
    
    [defaults setBool:YES forKey:BottomButton];
    
    [defaults setBool:NO forKey:@"ViewMore"];
    //    [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //[defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    
    //NSlog(@"The tag is %d",tag);
    bottomButtonDO *obj_bottomBottomDO;
    obj_bottomBottomDO = [bottomButtonList objectAtIndex:tag];
    
    
    if ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=26||[[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=27||[[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=28)
        [defaults setValue:obj_bottomBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    
    if ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_bottomBottomDO.btnLinkID!=NULL){
            [defaults setValue:obj_bottomBottomDO.btnLinkID forKey:KEY_LINKID];
            [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        }
    }
    [defaults setValue:obj_bottomBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_bottomBottomDO.btnLinkTypeName);
    if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            self.iEventsListViewController = iEventsListViewController;
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            // [iEventsListViewController release];
            
        }
        else
            return;
    }
    
    
    else{ //other menu types
        switch ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                self.hotDeals = hotDeals;
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                //  [hotDeals release];
                break;
                
            case 1://Scan Now
            {
                //[self navigateToScanNow];
            }
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                // [alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                [EventsBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [NearbyBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                
                [self navigateToFindView];
            }
                break;
                
            case 6://City Experience
            {
                //                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                //                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                //                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
            {
                
            }
                
                break;
                
            case 8://Visitors Bureau
            {
                
            }
                
                break;
                
            case 9://transportation
            {
                
            }
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE)
                {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:
            {//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {[self shareClicked:nil];}
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=FALSE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_bottomBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                if ([SharedManager filtersCount] == 0){
                    
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    
                    CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    self.cityExp = cevc;
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    [SharedManager setRefreshAustinRetailers:YES];
                    //[cevc release];
                    
                    
                    FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    self.pvc = pvc;
                    pvc.isOnlyOnePartner = YES;
                    [self.navigationController pushViewController:pvc animated:NO];
                    // [pvc release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    self.filterList = filterList;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    // [filterList release];
                    //                    [self request_GetFilters];
                }
                
            }
                break;
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
            }
                break;
                
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                
                //[dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                self.faqList = faqCatList;
                // [faqCatList release];
                
            }
                break;
            case 21://Fundraiser
                
            {
                [FundraiserBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //  [iFundraiserListViewController release];
                
            }break;
            case 22:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }
                break;
            case 23:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }
                break;
            case 24:{
                if (userIdentifier==TRUE) {
                   [NewsUtility signupPopUp:self];
                }
                else
                {
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        //  [inform release];
                    }
                    
                }
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                       [NewsUtility signupPopUp:self];
                    }
                    else
                    {
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            // [citPref release];
                        }
                        
                    }
                }
                
            }
                
                break;
            case 26://Map
            {
                if ([cityExpObjArray  count]>0) {
                    
                    CitiExpMapViewController *citiExpMap=[[CitiExpMapViewController alloc]initWithNibName:@"CitiExpMapViewController" bundle:Nil];
                    
                    citiExpMap.latArray = [[NSMutableArray alloc]init];
                    citiExpMap.longArray= [[NSMutableArray alloc]init];
                    citiExpMap.retailerNameArr= [[NSMutableArray alloc]init];
                    citiExpMap.retailerIdArray= [[NSMutableArray alloc]init];
                    citiExpMap.retailAddressArr= [[NSMutableArray alloc]init];
                    citiExpMap.retailLocationIdArray= [[NSMutableArray alloc]init];
                    citiExpMap.retListIdArray= [[NSMutableArray alloc]init];
                    citiExpMap.distanceCEArray= [[NSMutableArray alloc]init];
                    for (int i=0; i<[cityExpObjArray count]; i++) {
                        cityExpDO=[cityExpObjArray objectAtIndex:i];
                        [citiExpMap setValue:cityExpDO.latitude forKey:@"latitude"]; //[[NSString alloc]initWithString:[latArray objectAtIndex:i]];citiExpMap.latitude=
                        [citiExpMap setValue:cityExpDO.longitude forKey:@"longitude"];
                        [citiExpMap setValue:cityExpDO.retailerName forKey:@"retailerName"];
                        [citiExpMap setValue:cityExpDO.retailerId forKey:@"retailerId"];
                        [citiExpMap setValue:cityExpDO.retailAddress forKey:@"retailAddress"];
                        [citiExpMap setValue:cityExpDO.retailLocationId forKey:@"retailLocationId"];
                        [citiExpMap setValue:cityExpDO.retListId forKey:@"retListId"];
                        [citiExpMap setValue:cityExpDO.distanceCE forKey:@"distanceCE"];
                        
                        
                        
                        [citiExpMap.latArray addObject: [citiExpMap valueForKey:@"latitude"]];
                        [citiExpMap.longArray addObject: [citiExpMap valueForKey:@"longitude"]];
                        [citiExpMap.retailerNameArr addObject: [citiExpMap valueForKey:@"retailerName"]];
                        [citiExpMap.retailerIdArray addObject: [citiExpMap valueForKey:@"retailerId"]];
                        [citiExpMap.retailAddressArr addObject: [citiExpMap valueForKey:@"retailAddress"]];
                        [citiExpMap.retailLocationIdArray addObject: [citiExpMap valueForKey:@"retailLocationId"]];
                        [citiExpMap.retListIdArray addObject: [citiExpMap valueForKey:@"retListId"]];
                        [citiExpMap.distanceCEArray addObject: [citiExpMap valueForKey:@"distanceCE"]];
                    }
                    
                    
                    [self.navigationController pushViewController:citiExpMap animated:NO];
                    //  [citiExpMap release];
                    
                    
                }
                else
                {
                    [UtilityManager showAlert:nil msg:NSLocalizedString(@"Retailers are not available to plot on map",@"Retailers are not available to plot on map")];
                }
                
                
            }
                break;
                
            case 27:
            case 28://Sort/Filter
            {
                /*if(iSwipeViewController)
                 [iSwipeViewController release];*/
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                [defaults setValue:[defaults valueForKey:@"cityExpMitemId"] forKey:KEY_MITEMID];
                SwipesViewController *iSwipeViewController1 = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                iSwipeViewController1.module = @"CitiEXP";//Find All , Find Single , Events, CitiEXP
                NSArray *viewControllers = [self.navigationController viewControllers];
                long vIndex=[viewControllers count]-2;
                iSwipeViewController1.delegate = [self.navigationController.viewControllers objectAtIndex:vIndex];
                iSwipeViewController1.sortFilObj = [[SortAndFilter alloc]initWithObject:sortFilterObj];
                iSwipeViewController1.cityExpId = [linkID objectAtIndex:[linkID count]-1];
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                [self.navigationController pushViewController:iSwipeViewController1 animated:YES];
            }
                break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
               // privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
                
            default:
                break;
        }
    }
}

//-(void)requestToGetUserData
//{
//    NSMutableString *xmlStr = [[NSMutableString alloc] init];
//    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
//    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
//    ReleaseAndNilify(xmlStr);
//}
-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}


/*
 *
 Parsing logic when response received for getting user details.
 Method called when navigated from Settings screen.
 *
 */

#pragma - mark new bottom bar methods
//-(void) navigateToScanNow
//{
//    [defaults setBool:YES forKey:@"KEY_SCANSPLASH"];
//    [defaults setValue:@"scanNow" forKey:KEY_SEARCHFROM];
//    [defaults setObject:@"YES" forKey:@"KEY_SCANNERCALLED"];
//
//    int menulevel = [[defaults valueForKey:KEY_MENULEVEL]integerValue];
//    if(menulevel > 1)
//        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
//    else
//        [defaults  setBool:NO forKey:@"DisplayBackBarButton"];
//
//
//    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
//    ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];
//    [self.navigationController pushViewController:viewScanNowScreen animated:NO];
//    [viewScanNowScreen release];
//
//}

-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}

-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    self.iEventsListViewController = iEventsListViewController;
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    // [iEventsListViewController release];
}

-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=TRUE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

//-(void)request_GetUserCat{
//
//    iWebRequestState = GETFAVCATEGORIES;
//    if([Network currentReachabilityStatus]==0)
//    {
//        [common showAlert];
//    }
//    else {
//
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//
//    }
//}

-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}


-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    // [iMainMenuViewController release];
}


-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}

-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            //NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString] containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            // [anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}

-(void)parse_GetUserCat:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}


-(void)parse_GetFilters:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    else{
        
        TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
        TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT
                                                    parentElement:tbxml.rootXMLElement];
        
        if ([[TBXML textForElement:saveResponseCode]intValue] != 10000) {
            
            [UtilityManager showAlert:nil msg:[TBXML textForElement:saveResponseText]];
            return;
        }
        
        else {
            
            if ([UtilityManager isResponseXMLNullOrEmpty:response]){
                ReleaseAndNilify(response);
                // return;
            }
            
            [defaults setObject:response forKey:@"Response_Filters"];
            CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
            self.cityExp = cevc;
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            [cevc setTagValue:200];
            [SharedManager setRefreshAustinRetailers:YES];
            [self.navigationController pushViewController:cevc animated:NO];
            // [cevc release];
        }
    }
}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }

}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    for (NSOperation *operation in [HTTPClient sharedHTTPClient].manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        NSLog(@"operation:%@",operation);
        AFHTTPRequestOperation *operations = (AFHTTPRequestOperation*)operation;
        if([[[[operations request] URL] absoluteString] containsString:@"thislocation/citiexpretjson"]){
            [operation cancel];
        }
        
    }
    
    //   [[HTTPClient sharedHTTPClient].manager.operationQueue cancelAllOperations];
    
}


@end


//
//  PartnersListViewController.m
//  Scansee
//
//  Created by Ajit on 2/18/13.
//
//

#import "FilterRetailersList.h"
#import "PreferredCategoriesScreen.h"
#import "CategoriesViewController.h"
#import "RetailerSummaryViewController.h"
#import "RetailerAnnotation.h"
#import "MainMenuViewController.h"
#import "RetailersListViewController.h"
#import "LocationDetailsViewController.h"
#import "EmailShareViewController.h"
#import "LoginViewController.h"
#import "TBXML.h"
#import "FindOptionsViewController.h"

#import "ButtonSetupViewController.h"
#import "bottomButtonView.h"
#import "SingleCatRetailers.h"
#import "EventsListViewController.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "HubCitiConstants.h"
#import "SettingsViewController.h"
#import "FilterListViewController.h"
#import "FAQCategoryList.h"

#import "SpecialOffersViewController.h"
#import "RetailersListViewController.h"
#import "math.h"
#import "MainMenuViewController.h"
//#import "SubCategoriesViewController.h"
#import "LocationDetailsViewController.h"
#import "RetailerSummaryViewController.h"
//Added for Location Update
#import "CLLocation (Strings).h"
#import "bottomButtonDO.h"
#import "bottomButtonView.h"
#import "SettingsViewController.h"
#import "UserInformationViewController.h"
#import "AboutAndPrivacyScreen.h"
#import "UIColor+ExtraColorSpecifications.h"
#import "ButtonSetupViewController.h"
#import "SESpringBoard.h"
#import "bottomButtonView.h"
#import "CityExperienceViewController.h"
#import "SettingsViewController.h"
#import "FindViewController.h"
#import "AnythingPage.h"
#import "DealHotDealsList.h"
#import "AlertsList.h"
#import "AppDelegate.h"
#import "SingleCatRetailers.h"
//#import "ScanNowScreen.h"
#import "EventsListViewController.h"
#import "CouponsViewController.h"
#import "AnyViewController.h"
#import "WebBrowserViewController.h"
#import "EmailShareViewController.h"
#import "SubMenuGroupingAndSortingViewController.h"
#import "GalleryList.h"
#import "LoginViewController.h"
#import "FAQCategoryList.h"
#import "MapKitDisplayViewController.h"
#import "FindOptionsViewController.h"
#import "Mapkit/MKMapView.h"
#import "RetailerAnnotation.h"
#import "FilterListViewController.h"
#import "FundraiserListViewController.h"
#import "CityPreferenceViewController.h"
#import "UserSettingsController.h"
#import "HTTPClient.h"
#import "GetUserInfoResponse.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "CouponsMyAccountsViewController.h"
#import "CouponsInDealViewController.h"
@interface FilterRetailersList ()<HTTPClientDelegate>
{
    NSInteger numberOfRows;
    int yVal;
    UISegmentedControl *segment;
    NSNumber *lastRecord;
    bottomButtonView *bottomBtnView;
    UIFont *addressFont;
    float numberOfLines;
    BOOL isLoading;
}
@property (nonatomic, assign) NSInteger oldSegmentedIndex;
@end




@implementation FilterRetailersListDO

@synthesize retailerName, retailerId, retailLocationID, retailAddress, retailAddress2, retailCity,retailState, retailPostalCode, retailOpenClose, retailCompleteAddress;
@synthesize retLatitude, retLongitude, retDistance, retLogoImgPath;
@synthesize retbannerAdImgPath, retRibbonAdImgPath, retRibbonAdUrl, retSaleFlag;
@synthesize retListId;
@synthesize retRowNumber;
@end

@implementation FilterRetailersList{
    UIActivityIndicatorView *loading;
}
@synthesize isOnlyOnePartner,filterDescription;
@synthesize filterDesc,anyVC,emailSendingVC;
@synthesize sortFilObject,textView,savedSortObj;
@synthesize infoResponse;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)filtersOptionsReleaseArrays
{
    if ([globalLatArray count]>0) {
        [globalLatArray removeAllObjects];
        //[globalLatArray release];
    }
    if ([globalLongArray count]>0) {
        [globalLongArray removeAllObjects];
        //[globalLongArray release];
    }
    if( [globalNameArray count]>0)
    {
        [globalNameArray removeAllObjects];
        //[globalNameArray release];
    }
    if( [globalCompleteAddress count]>0)
    {
        [globalCompleteAddress removeAllObjects];
      
    }
    if([globalAddressArray count]) {
        
        [globalAddressArray removeAllObjects];
        //[globalAddressArray release];
    }
    if([globalRetailerListID count]) {
        
        [globalRetailerListID removeAllObjects];
        //[globalRetailerListID release];
    }
    if([globalRetLocId count]) {
        
        [globalRetLocId removeAllObjects];
        //[globalRetLocId release];
    }
    if([globalRetailerID count]) {
        
        [globalRetailerID removeAllObjects];
        //[globalRetailerID release];
    }
    
    
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{

    isLoading = false;
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
  }





- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject

{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    
    
    
    
    [self responseData:responseObject];
    
    
    
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    [self.view setAccessibilityLabel:@"filterRetailers"];
    loading = nil;
    [defaults setValue:nil forKey:@"filterRetailListMitemId"];
    addressFont = [UIFont systemFontOfSize:(IPAD ? 15.0f:10.0f)];
 
    
    filterRetailersListObjArray = [[NSMutableArray alloc]init];
    sortFilObject = [[SortAndFilter alloc]init];
    sortFilObject.distanceSelected = true;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
    
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    if(showMapFlag==TRUE)
    {
        
        self.navigationItem.hidesBackButton = YES;
        
        CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
        [cus setTitle:@"Choose Your Location:" forView:self withHambergur:NO];
       
        [self showMap];
    }
    else{
        
        self.navigationItem.hidesBackButton = YES;
        //[back release];
        [self allocateArrays];
        globalLatArray = [[NSMutableArray alloc]init];
        globalCompleteAddress = [[NSMutableArray alloc]init];
        globalLongArray = [[NSMutableArray alloc]init];
        globalNameArray=[[NSMutableArray alloc]init];
        globalAddressArray=[[NSMutableArray alloc]init];
        globalRetailerListID = [[NSMutableArray alloc]init];
        globalRetailerID =[[NSMutableArray alloc]init];
        globalRetLocId = [[NSMutableArray alloc]init];
        [self filtersOptionsReleaseArrays];
        savedSortObj = [[SortAndFilter alloc] init];
        
        [self customizedSegmentController];
        if(segment.selectedSegmentIndex == 0){
            savedSortObj.distanceSelected = YES;
            savedSortObj.alphabeticallySelected = NO;
        }
        else if(segment.selectedSegmentIndex == 1){
            savedSortObj.distanceSelected = NO;
            savedSortObj.alphabeticallySelected = YES;
        }
        
    }
    [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
    // Do any additional setup after loading the view from its nib.
    yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    if(showMapFlag==FALSE)
    {
        [self request_PartnerRet:0 withObj:sortFilObject];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    if([segment selectedSegmentIndex] == 2){
        
        [segment setSelectedSegmentIndex:_oldSegmentedIndex];
        [self setSegmentColor:segment];
        
    }
    
    filterDesc=nil;
   
    self.view.userInteractionEnabled = YES;
    
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if([defaults valueForKey:@"filterRetAffId"]){
        [SharedManager setRetAffId:[[defaults valueForKey:@"filterRetAffId"] intValue]];
       
    }
    
    if(showMapFlag==TRUE)
    {

        cusNav =(CustomizedNavController *) self.navigationController;
        cusNav.customNavBardelegate = self;
        [cusNav hideHambergerButton:YES];
        [cusNav hideBackButton:NO];
        [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
        CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
        [cus setTitle:@"Choose Your Location:" forView:self withHambergur:NO];

        //self.title=NSLocalizedString(@" Choose Your Location:", @"  Choose Your Location:");
        [self showMap];
    }
    else
    {
        cusNav =(CustomizedNavController *) self.navigationController;
        cusNav.customNavBardelegate = self;
        [cusNav hideBackButton:NO];
        [cusNav hideHambergerButton:NO];
        [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
       
       

        if([defaults valueForKey:@"FilterRetTitle"])
            [cusNav setTitle:[defaults valueForKey:@"FilterRetTitle"] forView:self withHambergur:YES];
        
        else
            [cusNav setTitle:[defaults valueForKey:@"Title"] forView:self withHambergur:YES];
        
        [self.navigationController setNavigationBarHidden:NO];
        mapView.hidden = YES;
        
        if([defaults valueForKey:@"isComingFromGroupingandSorting"])
        {
            [defaults setBool:NO forKey:@"ViewMore"];
            //[self allocateArrays];
            nextPage = 0;
            lastVisitedRecord = 0;
            sortFilObject = [[SortAndFilter alloc]initWithObject:[self loadCustomObjectWithKey:@"SortFilterObject"]];
            if(sortFilObject.distanceSelected)
            {
                segment.selectedSegmentIndex = 0;
            }
            else if (sortFilObject.alphabeticallySelected)
            {
                segment.selectedSegmentIndex = 1;
            }
            [self setSegmentColor:segment]; //add customized color
            
            segment.translatesAutoresizingMaskIntoConstraints = NO;
            [segment addTarget:self action:@selector(viewSelectionChanged:) forControlEvents:UIControlEventValueChanged];
            DLog(@"%d", sortFilObject.distanceSelected);
            DLog(@"%d", sortFilObject.alphabeticallySelected);
            [self refreshArrays];
            //[filterRetailersListObjArray removeAllObjects];
            [self request_PartnerRet:0 withObj:sortFilObject];
            [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
            [defaults setValue:nil forKey:@"SortFilterObject"];
            [iSwipeViewController.view removeFromSuperview];
        }
        
    }

    if (iSwipeViewController) {
        [self.view bringSubviewToFront:iSwipeViewController.view];
    }
    
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}

- (SortAndFilter *)loadCustomObjectWithKey:(NSString *)key {
    
    NSData *encodedObject = [defaults objectForKey:key];
    SortAndFilter *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}

- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
    
}
-(void)returnToMainPage:(id)sender
{
    if (isLoading) {
        return;
    }
    showMapFlag = FALSE;
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

- (void)addItemViewController:(SwipesViewController *)controller didFinishEnteringItem:(SortAndFilter *)item
{
    if([defaults valueForKey:@"isComingFromGroupingandSorting"])
    {
        //  DLog(@"%d",sortFilterObj.distanceSelected);
        ReleaseAndNilify(partnersTable);
        sortFilObject = [[SortAndFilter alloc]initWithObject:item];
        
        
        partnersTable.scrollEnabled = YES;
        partnersTable.allowsSelection = YES;
        if(sortFilObject.distanceSelected)
        {
            segment.selectedSegmentIndex = 0;
        }
        else if (sortFilObject.alphabeticallySelected)
        {
            segment.selectedSegmentIndex = 1;
        }
        [self setSegmentColor:segment]; //add customized color
        [super viewWillAppear:YES];
        [filterRetailersListObjArray removeAllObjects];
        [self allocateArrays];
        nextPage = 0;
        [self request_PartnerRet:0 withObj:sortFilObject];
        [defaults setValue:nil forKey:@"isComingFromGroupingandSorting"];
        [defaults setValue:nil forKey:@"SortFilterObject"];
    }
}



-(void)refreshArrays{
    [retailerIdArray removeAllObjects];
    [rowNumberArray removeAllObjects];
    [retailLocationIDArray removeAllObjects];
    [logoImagePathArray removeAllObjects];
    [bannerAdImagePathArray removeAllObjects];
    [ribbonAdImagePathArray removeAllObjects];
    [ribbonAdURLArray removeAllObjects];
    [saleFlagArray removeAllObjects];
    [retailerNameArray removeAllObjects];
    [retailAddressArray removeAllObjects];
    [distanceArray removeAllObjects];
    [retListIDArray removeAllObjects];
    [latArray removeAllObjects];
    [longArray removeAllObjects];
    
    
}




-(void)settableViewOnScreen
{
    //int yPos=0;
    int yTVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    if(filterDescription){
        filterDescription=NO;
        [textView removeFromSuperview];
         UIFont *fontForlabel = [UIFont systemFontOfSize:IPAD ? 15 : 12];
        CGRect frame = CGRectMake(0, 44, SCREEN_WIDTH,[self getTextViewSize:filterDesc:fontForlabel]);
        textView = [[UITextView alloc]initWithFrame:frame];
        textView.backgroundColor = [UIColor clearColor];
        textView.layer.borderColor = [UIColor clearColor].CGColor;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
            textView.font = [UIFont systemFontOfSize:15];
        }
        else
        {
            textView.font = [UIFont systemFontOfSize:12];
        }
        textView.editable=NO;
        textView.text=filterDesc;
        [self.view addSubview:textView];
        
       // yPos=textView.frame.origin.y+textView.frame.size.height;
    }
    if(partnersTable == nil){
        
        if(bottomBtn==1){
            partnersTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 44+textView.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT-yTVal-50-44 - textView.frame.size.height) style:UITableViewStylePlain];
        }
        else{
            partnersTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 44+textView.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT-yTVal-44- textView.frame.size.height) style:UITableViewStylePlain];
        }    partnersTable.dataSource=self;
        partnersTable.delegate=self;
        [partnersTable setBackgroundColor:[UIColor whiteColor]];
        partnersTable.tableFooterView.hidden=YES;
        [partnersTable setAccessibilityLabel:@"partnersTable"];
        [self.view addSubview:partnersTable];
    }
}

-(float) getTextViewSize : (NSString*) descString :(UIFont *) fontLabel
{
   // float fontSize;
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
       // fontSize=15;
    }
    else
    {
       // fontSize=12;
    }
    
    
    CGSize constraint = CGSizeMake(SCREEN_WIDTH, CGFLOAT_MAX);
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    CGSize size = [descString boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: fontLabel, NSParagraphStyleAttributeName: paragraph } context: nil].size;
    return size.height +20;
    
    
}


-(void)request_PartnerRet:(int)lowerLimit withObj:(SortAndFilter *)sortFilOb{
    isLoading = true;
    iWebRequestState = FILTERS_RETAILERLIST;
    
    NSMutableDictionary* param = [[NSMutableDictionary alloc] init];
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [timeFormatter setTimeZone:timeZone];
    [timeFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *timeInUTC = [timeFormatter stringFromDate:currentTime];
    NSLog(@"Current time %@", timeInUTC);
    [param setValue:timeInUTC forKey:@"requestedTime"];
    [param setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    [param setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    if ([defaults valueForKey:KEY_MAINMENUID])
        [param setValue:[defaults valueForKey:KEY_MAINMENUID] forKey:@"mainMenuId"];
    if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [param setValue:[defaults valueForKey:KEY_BOTTOMBUTTONID] forKey:@"bottomBtnId"];
    if ([defaults valueForKey:KEY_MITEMID])
    {
        [defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"filterRetailListMitemId"];
        [param setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
    }
    [param setValue:@"IOS" forKey:@"platform"];
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [param setValue:[defaults  valueForKey:KEY_LONGITUDE] forKey:@"longitude"];
        [param setValue:[defaults  valueForKey:KEY_LATITUDE] forKey:@"latitude"];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [param setValue:[defaults  valueForKey:KEYZIPCODE] forKey:@"postalCode"];
      
    }
    
    if (sortFilObject.selectedInterestsIds) {
         [param setValue:sortFilObject.selectedInterestsIds forKey:@"interests"];
    }
    
    if (sortFilOb.distanceSelected) {
        [param setValue:@"distance" forKey:@"sortColumn"];
    }
    
    if (sortFilOb.alphabeticallySelected) {
        [param setValue:@"atoz" forKey:@"sortColumn"];
        
    }
    
    if (sortFilOb.selectedCitiIds) {
        [param setValue:sortFilOb.selectedCitiIds forKey:@"cityIds"];
        
    }
    [param setValue:@"ASC" forKey:@"sortOrder"];
    
    if (sortFilOb.localSpecialSelected) {
        [param setValue:@"1" forKey:@"locSpecials"];
       
    }
    else{
        [param setValue:@"0" forKey:@"locSpecials"];
        
    }
    
    
    if (sortFilOb.selectedCatIds) {
         [param setValue:sortFilOb.selectedCatIds forKey:@"catIds"];
        
    }
     [param setValue:[NSString stringWithFormat:@"%d",lowerLimit] forKey:@"lowerLimit"];
    [param setValue:[NSString stringWithFormat:@"%d",[SharedManager retAffId]] forKey:@"retAffId"];
  
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/partnerretjson",BASE_URL];
   // NSString *urlString = [NSString stringWithFormat:@"%@thislocation/partnerretjson",@"http://10.10.221.2:9990/HubCiti_Coupon/"];
    
    NSLog(@"url:%@",urlString);
    NSLog(@"param:%@",param);
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    if (![defaults boolForKey:@"ViewMore"]) {
       
        [HubCitiAppDelegate showActivityIndicator];
        [client sendRequest : param : urlString];
    }
    else{
        [HubCitiAppDelegate removeActivityIndicator];
        NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:param];
        [HubCitiAppDelegate removeActivityIndicator];
        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
        id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        DLog(@"%@",responseData);
        [self parse_PartnersList:responseData];
    }
    
    
    
    
    /*iWebRequestState = FILTERS_RETAILERLIST;
    
    
    
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    dateFormatter.dateFormat = @"hh:mm:ss";
    NSLog(@"The Current Time is %@",[dateFormatter stringFromDate:now]);
    
    
    NSLog(@"%@",[defaults valueForKey:KEY_MAINMENUID]);
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    else  if ([defaults valueForKey:KEY_MITEMID]){
        [defaults setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"filterRetailListMitemId"];
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    
    [requestStr appendFormat:@"<requestedTime>%@</requestedTime>",[dateFormatter stringFromDate:now]];
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    if (sortFilObject.selectedInterestsIds) {
        [requestStr appendFormat:@"<interests>%@</interests>",sortFilObject.selectedInterestsIds];
    }
    
    if (sortFilOb.distanceSelected) {
        [requestStr appendFormat:@"<sortColumn>distance</sortColumn>"];
    }
    
    if (sortFilOb.alphabeticallySelected) {
        [requestStr appendFormat:@"<sortColumn>atoz</sortColumn>"];
    }
    
    if (sortFilOb.selectedCitiIds) {
        [requestStr appendFormat:@"<cityIds>%@</cityIds>",sortFilOb.selectedCitiIds];
    }
    
    [requestStr appendFormat:@"<sortOrder>ASC</sortOrder>"];
    
    if (sortFilOb.localSpecialSelected) {
        [requestStr appendFormat:@"<locSpecials>1</locSpecials>"];
    }
    else{
        [requestStr appendFormat:@"<locSpecials>0</locSpecials>"];
    }
    
    
    
    if (sortFilOb.selectedCatIds) {
        [requestStr appendFormat:@"<catIds>%@</catIds>",sortFilOb.selectedCatIds];
    }
    
    //    if ([SharedManager localSpecilas]) {
    //        [requestStr appendFormat:@"<locSpecials>1</locSpecials>"];
    //    }
    //    else{
    //        [requestStr appendFormat:@"<locSpecials>0</locSpecials>"];
    //    }
    //
    //    if ([defaults valueForKey:@"filtersSort"]==nil) {
    //        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    //    }
    //    else
    //    {
    //        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    //    }
    //
    //
    
    [requestStr appendFormat:@"<lowerLimit>%d</lowerLimit><retAffId>%d</retAffId></ThisLocationRequest>",lowerLimit,[SharedManager retAffId]];  //<catIds>0</catIds>
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/partnerret",BASE_URL];
    
    //NSString *urlString = [NSString stringWithFormat:@"http://10.10.221.27:9990/HubCiti_Toggle/thislocation/partnerret"];
    
    // NSString *response=[ConnectionManager establishPostConnection:urlString withParam:requestStr]; // ConnectionFor:requestStr base:urlString withDelegate:self];
    if ([defaults boolForKey:@"ViewMore"]) {
        NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
        
        
        [defaults setValue:response forKey:@"Response_Filters"];
        
        if([defaults valueForKey:@"Response_Filters"])
        {
            //        [self allocateArrays];
            
            //[self performSelector:@selector(nextRequestparse_update:) withObject:response afterDelay:0.5];
            
            
            [self parse_PartnersList:[defaults valueForKey:@"Response_Filters"]];
            
            [defaults setValue:nil forKey:@"Response_Filters"];
            //
            
        }
        
        
    }
    else{
        [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    }
    
    
    
    
    ReleaseAndNilify(requestStr);*/
}

-(void)nextRequestparse_update: (id)responseData{
    
    [self parse_PartnersList:[defaults valueForKey:@"Response_Filters"]];
    [HubCitiAppDelegate removeActivityIndicator];
    [defaults setValue:nil forKey:@"Response_Filters"];
}

-(void) navigateToWhatsNearBy
{
    self.navigationController.navigationBarHidden = NO;
    navigatedFromMainMenu=false;
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
    }
    else
    {
        // If GPS Not allowed but ZIP code is available
        if(gpsFlag)
        {
            [defaults setBool:YES forKey:@"gpsEnabled"];
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            self.retailerListScreen = retailerListScreen;
            [self.navigationController pushViewController:retailerListScreen animated:NO];
            //[retailerListScreen release];
            
        }
        else
        {
            if(zipCodeStatus == 1)
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            else
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            [defaults  setBool:NO forKey:@"showBackButton"];
            if (userIdentifier==TRUE) {
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                self.retailerListScreen = retailerListScreen;
                [self.navigationController pushViewController:retailerListScreen animated:NO];
                //[retailerListScreen release];
            }
            else
            {
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:locationDetailsScreen animated:NO];
                //[locationDetailsScreen release];
            }
        }
    }
}
//Call Find Service and Class
-(void)navigateToFindView
{
    [defaults  setBool:NO forKey:@"findsplash"];
    [defaults setValue:@"0" forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    // [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    //[self navigateToFindView];
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:mmvc animated:YES];
    //[mmvc release];
}

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}





-(void)bottomButtonPressed:(id)sender
{
    if (isLoading) {
        return;
    }
    
    [defaults setBool:NO forKey:@"ViewMore"];
    [defaults setBool:YES forKey:BottomButton];
    //    [defaults setValue:nil forKey:KEY_MAINMENUID];
    //    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    //    [defaults setValue:nil forKey:KEY_MITEMID];
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    SingleEventFromMainMenu=FALSE;
    
    
    UIButton *btn = (UIButton*)sender;
    int tag = (int) btn.tag;
    
    //NSlog(@"The tag is %d",tag);
    bottomButtonDO *obj_bottomBottomDO;
    obj_bottomBottomDO = [bottomButtonList objectAtIndex:tag];
    if( [[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=28||[[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=27||[[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=26){
        [defaults setValue:obj_bottomBottomDO.bottomBtnID forKey:KEY_BOTTOMBUTTONID];
    }
    if ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]!=16) {
        if(obj_bottomBottomDO.btnLinkID!=NULL){
            if (![obj_bottomBottomDO.btnLinkTypeName isEqualToString:@"Map"]){
                [defaults setValue:obj_bottomBottomDO.btnLinkID forKey:KEY_LINKID];
                [linkID addObject:[defaults valueForKey:KEY_LINKID]];
            }
        }
    }
    [defaults setValue:obj_bottomBottomDO.btnLinkTypeName forKey:KEY_MITEMNAME];
    
    DLog(@"value:%@",obj_bottomBottomDO.btnLinkTypeName);
    if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            [FindSingleBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
            //save the category name and navigate to dining screen
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:singleCat animated:NO];
            ReleaseAndNilify(singleCat);
        }
        
        else
            return;
    }
    
    else if ([obj_bottomBottomDO.btnLinkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means Event - single category
        
        NSArray *nameArray = [obj_bottomBottomDO.btnLinkTypeName componentsSeparatedByString:@"-"];
        if ([nameArray count]>1){
            if (SingleEventCatID==Nil) {
                SingleEventCatID=[[NSMutableArray alloc]init];
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            else
            {
                SingleCatFlag = TRUE;
                // [SingleEventCatID addObject:obj_bottomBottomDO.btnLinkID];
                [SingleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
            }
            
            //save the category name and navigate to EventSingleCategory screen
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            self.iEventsListViewController = iEventsListViewController;
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            [self.navigationController pushViewController:iEventsListViewController animated:NO];
            //[iEventsListViewController release];;
            
        }
        else
            return;
    }
    
    
    else{ //other menu types
        switch ([[obj_bottomBottomDO.dict_linkTypeName_BottomButton objectForKey:obj_bottomBottomDO.btnLinkTypeName]intValue]) {
                
            case 0://Hot Deals
            {
                [defaults  setBool:YES forKey:@"HotDealsplash"];
                DealHotDealsList *hotDeals = [[DealHotDealsList alloc]initWithNibName:@"DealHotDealsList" bundle:nil];
                self.hotDeals = hotDeals;
                [self.navigationController pushViewController:hotDeals animated:NO];
            }
                //[hotDeals release];
                break;
                
            case 1://Scan Now
                //[self navigateToScanNow];
                break;
                
            case 2:{//Alerts
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                [self.navigationController pushViewController:alert animated:NO];
                //[alert release];
            }
                break;
                
            case 3://Events
            {
                if (MultipleEventCatID==Nil) {
                    MultipleEventCatID=[[NSMutableArray alloc]init];
                    MultipleCatFlag = TRUE;
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                else
                {
                    [MultipleEventCatID addObject:obj_bottomBottomDO.bottomBtnID];
                }
                [EventsBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                [self navigateToEventList];
            }
                break;
            case 4://Whats NearBy
            {
                [NearbyBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                [self navigateToWhatsNearBy];
            }
                break;
                
            case 5://Find
            {
                [FindBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                if([defaults valueForKey:@"findFlag"]){
                    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONNAME];
                }
                [self navigateToFindView];
            }
                break;
                
            case 6://City Experience
            {
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                [CityExperienceBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                CityExperienceViewController *citi = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:nil];
                [self.navigationController pushViewController:citi animated:NO];
            }
                break;
                
            case 7://City Services
                
                break;
                
            case 8://Visitors Bureau
                
                break;
                
            case 9://transportation
                
                break;
                
            case 10://Preference
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                }
                else
                {
                    [self requestToGetPreferredLocations];
                }
            }
                break;
                
            case 11:{//About
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                [self.navigationController pushViewController:aboutScreen animated:NO];
                //[aboutScreen release];
            }
                break;
                
            case 12://Share
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {[self shareClicked:nil];}
            }
                break;
                
            case 13:
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else
                {//settings
                    SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:svc animated:NO];
                    //[svc release];
                }
            }
                break;
            case 14:{//AnythingPage
                requestAnythingFromMainMenu=TRUE;
                [UtilityManager req_hubcitiAnyThingPage:self];
                break;
            }
            case 15:{//AppSite
                [self request_appsitedetails];
                break;
            }
            case 16: // SubMenu
            {
                //Added for navigation between menu levels
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                [array addObject:[NSString stringWithFormat:@"%@",obj_bottomBottomDO.btnLinkID]];
                [HubCitiAppDelegate refreshLinkIdArray];
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                [array1 addObject:@"0"];
                [HubCitiAppDelegate refreshDeptIdArray];
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                [array2 addObject:@"0"];
                [HubCitiAppDelegate refreshTypeIdArray];
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                [self navigateToSubMenu];
                break;
            }
            case 17: // Filters
            {
                popFromCity=YES;
                if ([SharedManager filtersCount] == 0){
                    
                    [UtilityManager showAlert:nil msg:@"No Records Found"];
                }
                
                else if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    [defaults setInteger:100 forKey:@"CityExpTableTag"];
                    self.cityExp = cevc;
                    [SharedManager setRefreshAustinRetailers:YES];
                    //[cevc release];
                    
                    
                    FilterRetailersList *filters = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    self.filters = filters;
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filters animated:NO];
                    //[filters release];
                }
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    [self.navigationController pushViewController:filterList animated:NO];
                    //[filterList release];
                    
                    //[self request_GetPartners];
                }
                
            }
                break;
                
                
            case 18: // Coupon
            {
                [self navigateToCouponsViewController];
                break;
            }
            case 19: // Deals
            {
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
                //[dvc release];
                
                break;
            }
                
            case 20:
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:faqCatList animated:NO];
                self.faqList = faqCatList;
                //[faqCatList release];
                
            }
                break;
                
            case 21:
                
            {
                [FundraiserBottomButtonID addObject:[defaults valueForKey:KEY_BOTTOMBUTTONID]];
                
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"FundraiserListViewController" bundle:[NSBundle mainBundle]];
                self.iFundraiserListViewController = iFundraiserListViewController;
                [self.navigationController pushViewController:iFundraiserListViewController animated:NO];
                //[iFundraiserListViewController release];
                
            }break;
                
            case 22:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETUSERINFO;
                    [self requestToGetUserData];
                }
            }break;
            case 23:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    iWebRequestState = GETFAVLOCATIONS;
                    [self requestToGetPreferredLocations];
                }
                
            }break;
                
            case 24:
                
            {
                if (userIdentifier==TRUE) {
                    [NewsUtility signupPopUp:self];
                    [defaults setBool:NO forKey:BottomButton];
                }
                else{
                    
                    if([Network currentReachabilityStatus]==0)
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!")  msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                    }
                    else{
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        [self.navigationController pushViewController:inform animated:NO];
                        //[inform release];
                    }
                    
                }
                
            }break;
            case 25:
                
            {
                if ([RegionApp isEqualToString:@"1"]) {
                    if (userIdentifier==TRUE) {
                        [NewsUtility signupPopUp:self];
                        [defaults setBool:NO forKey:BottomButton];
                    }
                    else{
                        
                        if([Network currentReachabilityStatus]==0)
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        }
                        else{
                            [SharedManager setUserInfoFromSignup:NO];
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            [self.navigationController pushViewController:citPref animated:NO];
                            //[citPref release];
                        }
                    }
                }
                
            }break;
                
            case 26://Map
            {
                ReleaseAndNilify(partnersTable);
                fromFilters=TRUE;
                fromFind=FALSE;
                fromNearBy=FALSE;
                //added
                if ([RegionApp isEqualToString:@"1"]){
                    fromRegionFind = FALSE;//from region app
                }
                
                if ([latArray count] == 0 || [longArray count] == 0) {
                    [self filtersOptionsReleaseArrays];
                }
                [self filtersOptionsReleaseArrays];
                for(int i = 0; i<completeAddressArray.count; i++)
                {
                    [globalCompleteAddress addObject:[completeAddressArray objectAtIndex:i]];
                }
                for (int i=0; i<[latArray count]; i++) {
                    
                    [globalLatArray addObject:[latArray objectAtIndex:i]];
                }
                for (int j =0 ; j<[longArray count]; j++) {
                    
                    [globalLongArray addObject:[longArray objectAtIndex:j]];
                }
                
                for (int i=0; i<[retailerNameArray count]; i++) {
                    
                    [globalNameArray addObject:[retailerNameArray objectAtIndex:i]];
                }
            
                for (int i=0; i<[retailerNameArray count]; i++) {
                    
                    [globalNameArray addObject:[retailerNameArray objectAtIndex:i]];
                }
                for (int j=0; j<[retailAddressArray count]; j++) {
                    
                    [globalAddressArray addObject:[retailAddressArray objectAtIndex:j]];
                }
                for (int j=0; j<[retListIDArray count]; j++) {
                    
                    [globalRetailerListID addObject:[retListIDArray objectAtIndex:j]];
                }
                for (int j=0; j<[retailerIdArray count]; j++) {
                    
                    [globalRetailerID addObject:[retailerIdArray objectAtIndex:j]];
                }
                for (int j=0; j<[retailLocationIDArray count]; j++) {
                    
                    [globalRetLocId addObject:[retailLocationIDArray objectAtIndex:j]];
                }
                if ([globalLatArray  count] > 0) {
                    
                    
                    //[self showMap];
                    showMapFlag=TRUE;
                    
                    FilterRetailersList *iFindRetailersList = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    [self.navigationController pushViewController:iFindRetailersList animated:NO];
                    // [iFindRetailersList release];
                    
                }
                else{
                    [UtilityManager showAlert:nil msg:NSLocalizedString(@"Retailers are not available to plot on map",@"Retailers are not available to plot on map")];
                    }
            }
                break;
            case 27:
            case 28://Sort/Filter
            {
                showMapFlag = FALSE;
                //                if(iSwipeViewController)
                //                    [iSwipeViewController release];
                
                if([defaults valueForKey:@"filterRetailListMitemId"])
                    [defaults setValue:[defaults valueForKey:@"filterRetailListMitemId"] forKey:KEY_MITEMID];
                
                SwipesViewController* iSwipeViewController1 = [[SwipesViewController alloc]initWithNibName:@"SwipesViewController" bundle:[NSBundle mainBundle]];
                NSArray *viewControllers = [self.navigationController viewControllers];
                long vIndex=[viewControllers count]-2;
                iSwipeViewController1.delegate = [self.navigationController.viewControllers objectAtIndex:vIndex];
                iSwipeViewController1.module = @"Filter";//Find All , Find Single , Events, CitiEXP
                iSwipeViewController1.filterID =  [SharedManager retAffId];
                //          iSwipeViewController.categoryName = typeStr;
                //          iSwipeViewController.catId = catId;
                iSwipeViewController1.sortFilObj = [[SortAndFilter alloc]initWithObject:sortFilObject];
                //        iSwipeViewController.srchKey =searchKey;
                
                [self.navigationController pushViewController:iSwipeViewController1 animated:NO];
                
            }
                break;
            case 29:
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:dvc animated:NO];
            }
                break;
            case 30:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
                //privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self.navigationController pushViewController:privacy animated:NO];
            }
                break;
                
            default:
                break;
        }
    }
    
}


#pragma mark Setting of Bottom Tab Bar Buttons
-(void) setBottomBarMenu : (NSMutableArray *)bottomList
{
    NSMutableArray *arrBottomButtonViewContainer = [[NSMutableArray alloc]init];
    
    // int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
    {
        UIImageView *tabImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-yVal-55, SCREEN_WIDTH, 55)];
        tabImage.backgroundColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
        [self.view addSubview:tabImage];
        [tabImage setAccessibilityValue:@"Bottom"];
        [self.view bringSubviewToFront:tabImage];
        //[tabImage release];
    }
    
    
    for(int btnLoop= 0; btnLoop < [bottomList count]; btnLoop++)
    {
        bottomButtonDO *obj_bottomButtonDO = [bottomList objectAtIndex:btnLoop];
        
        bottomBtnView = [[bottomButtonView alloc]init];
        
        if( DEVICE_TYPE==UIUserInterfaceIdiomPad)
        {
            bottomBtnView.contentMode = UIViewContentModeCenter;
            bottomBtnView = [bottomButtonView initWithTitle:obj_bottomButtonDO.bottomBtnName frame:CGRectMake(52+140*btnLoop + 52*btnLoop, SCREEN_HEIGHT-yVal-55, 80,55) imageName:obj_bottomButtonDO.bottomBtnImg img_off:obj_bottomButtonDO.bottomBtnImgOff delegate:self tag:btnLoop];
            
        }
        else
            bottomBtnView = [bottomButtonView initWithTitle:obj_bottomButtonDO.bottomBtnName frame:CGRectMake(btnLoop*SCREEN_WIDTH/[bottomList count], SCREEN_HEIGHT-yVal-bottomBarButtonHeight, SCREEN_WIDTH/[bottomList count],bottomBarButtonHeight) imageName:obj_bottomButtonDO.bottomBtnImg img_off:obj_bottomButtonDO.bottomBtnImgOff delegate:self tag:btnLoop];
        
        [bottomBtnView setAccessibilityLabel:obj_bottomButtonDO.btnLinkTypeName];
        [bottomBtnView setAccessibilityValue:@"Bottom"];
        [arrBottomButtonViewContainer addObject:bottomBtnView];
        [self.view addSubview:bottomBtnView];
        [self.view bringSubviewToFront:bottomBtnView];
    }
    
}

#pragma mark action sheet methods
-(void)shareClicked:(id)sender{
    if (userIdentifier==TRUE) {
        [NewsUtility signupPopUp:self];
    }
    else
    {
        if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
            return;
        
        [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
        [self showActionSheet];
        
    }
}


-(void) showActionSheet {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        actionSheet.popoverPresentationController.sourceView = bottomBtnView;
        actionSheet.popoverPresentationController.sourceRect = bottomBtnView.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void) faceBookCliked {//facebook
    anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [self.view addSubview:anyVC.view]; }
                    completion:nil];
}


-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [self.view addSubview:controller.view];
            [self.view bringSubviewToFront:controller.view];
        }else
            [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [emailSendingVC loadMail];
    
}




-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
            }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)backClicked{
    
    [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
}

-(void)popBackToPreviousPage{
    //    if(isOnlyOnePartner)
    //    {
    //        isOnlyOnePartner = NO;
    //        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    //    }
    //    else
    if (isLoading) {
        return;
    }
    showMapFlag=FALSE;
    [defaults setBool:NO forKey:@"ViewMore"]; 
    
   [self.navigationController popViewControllerAnimated:NO];
    
    
}


#pragma mark request methods
//City Experience Retailers
-(void) request_CitiExpRet{
    
    iWebRequestState = CITIEXPRET;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    else if ([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    [requestStr appendFormat:@"<sortColumn>%@</sortColumn>",[defaults valueForKey:@"SortCitiExpBy"]];
    [requestStr appendFormat:@"<sortOrder>ASC</sortOrder>"];
    
    if ([SharedManager localSpecilas]) {
        [requestStr appendFormat:@"<locSpecials>1</locSpecials>"];
    }
    else{
        [requestStr appendFormat:@"<locSpecials>0</locSpecials>"];
    }
    
    //    [requestStr appendFormat:@"<catIds>0</catIds>"];
    
    [requestStr appendFormat:@"<citiExpId>%@</citiExpId><lowerLimit>0</lowerLimit></ThisLocationRequest>",[linkID objectAtIndex:[linkID count]-1]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/citiexpret",BASE_URL];
    // [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:requestStr]];
    
    [self parse_CitiExpRet:responseXml];
    
    ReleaseAndNilify(responseXml);
    //[requestStr release];
}

-(void)request_RetSummary:(id)indexPath1{
    
    NSIndexPath *ipath = indexPath1;
    iWebRequestState = RETSUMMARY;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[defaults  objectForKey:@"retailLocationID"]];
    
    //For user tracking
    if([retListIDArray count] > ipath.row && [defaults valueForKey:KEY_MAINMENUID])//retListId, mainMenuId and scanTypeid should go together
    {
        [defaults setObject:[retListIDArray objectAtIndex:ipath.row] forKey:KEY_RLISTID];
        [requestStr appendFormat:@"<retListId>%@</retListId>",[retListIDArray objectAtIndex:ipath.row]];
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId><scanTypeId>0</scanTypeId>",[defaults valueForKey:KEY_MAINMENUID]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",[defaults  valueForKey:KEY_RETAILERID]];
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    NSString *urlString =  [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);
}

#pragma mark response parse methods

-(void) responseData:(NSString*)response{
    
    switch (iWebRequestState) {
        case FILTERS_RETAILERLIST:
        {
            [defaults setValue:response forKey:@"Response_Filters"];
            
            if([defaults valueForKey:@"Response_Filters"])
            {
                //        [self allocateArrays];
                [HubCitiAppDelegate removeActivityIndicator];
                //[self performSelector:@selector(nextRequestparse_update:) withObject:response afterDelay:0.5];
                
                
                [self parse_PartnersList:[defaults valueForKey:@"Response_Filters"]];
                
                [defaults setValue:nil forKey:@"Response_Filters"];
                //
                
            }
            
        }
            break;
        case CITIEXPRET:
            [self parse_CitiExpRet:response];
            break;
        case GETFAVLOCATIONS:
            [self parse_GetFavCategories:response];
            break;
        case RETSUMMARY:
            [self parse_RetSummary:response];
            break;
        case appsitedetails:
            [self parse_appsitedetails:response];
            break;
        case HubcitiAnythingInfo:
            [self parse_HubcitiAnythingInfo:response];
            break;
        case GETUSERINFO:
        {
            [self parseGetUserData:response];
        }
            break;
        default:
            break;
    }
}

//parse user favorite categories
-(void)parse_GetFavCategories:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    //[settings release];
}

-(void) parse_CitiExpRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        
        [defaults setObject:nil forKey:@"Response_Austin"];
        return;
    }
    
    else{
        [SharedManager setRefreshAustinRetailers:YES];
        [defaults setObject:response forKey:@"Response_Austin"];
    }
}


#pragma mapView delegate methods
// Swap betWeen tableView and MapView

-(void)showMap
{
    //    [self allocateArrays];
    mapView.hidden = NO;
    mapView = [[MKMapView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    [mapView setDelegate:self];
  //  CLLocationCoordinate2D southWest;
   // CLLocationCoordinate2D northEast;
    if ([globalLatArray count]){
        
//        southWest = CLLocationCoordinate2DMake([[globalLatArray objectAtIndex:0] doubleValue],
//                                               [[globalLongArray objectAtIndex:0] doubleValue]);
        //northEast = southWest;
    }
    
    for(int i=0; i<[globalLatArray count]; i++)
    {
        RetailerAnnotation *ann = [[RetailerAnnotation alloc] init];
        ann.coordinate = CLLocationCoordinate2DMake([[globalLatArray objectAtIndex:i] doubleValue],
                                                    [[globalLongArray objectAtIndex:i] doubleValue]);
        [mapView addAnnotation:ann];
        [annArray addObject:ann];
        [ann setTitle:[globalNameArray objectAtIndex:i]];
        [ann setSubtitle:[globalCompleteAddress objectAtIndex:i]];
        ReleaseAndNilify(ann);
        
    }
    
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in mapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    [mapView setVisibleMapRect:zoomRect animated:YES];
    
    [self.view addSubview:mapView];
}



-(void)optionsShow:(UIBarButtonItem *) sender {
    ReleaseAndNilify(partnersTable);
    fromFilters=TRUE;
    fromFind=FALSE;
    fromNearBy=FALSE;
    //added
    if ([RegionApp isEqualToString:@"1"]){
        fromRegionFind = FALSE;//from region app
    }
    //    if ([latArray count] == 0 || latArray == nil || [longArray count] == 0 || longArray == nil)
    //    {
    //        return;
    //    }
    if ([latArray count] == 0 || [longArray count] == 0) {
        [self filtersOptionsReleaseArrays];
    }
    [self filtersOptionsReleaseArrays];
    for (int i=0; i<[latArray count]; i++) {
        
        [globalLatArray addObject:[latArray objectAtIndex:i]];
    }
    for (int j =0 ; j<[longArray count]; j++) {
        
        [globalLongArray addObject:[longArray objectAtIndex:j]];
    }
    
    for (int i=0; i<[retailerNameArray count]; i++) {
        
        [globalNameArray addObject:[retailerNameArray objectAtIndex:i]];
    }
    for (int j=0; j<[retailAddressArray count]; j++) {
        
        [globalAddressArray addObject:[retailAddressArray objectAtIndex:j]];
    }
    for (int j=0; j<[retListIDArray count]; j++) {
        
        [globalRetailerListID addObject:[retListIDArray objectAtIndex:j]];
    }
    for (int j=0; j<[retailerIdArray count]; j++) {
        
        [globalRetailerID addObject:[retailerIdArray objectAtIndex:j]];
    }
    for (int j=0; j<[retailLocationIDArray count]; j++) {
        
        [globalRetLocId addObject:[retailLocationIDArray objectAtIndex:j]];
    }
    for (int j=0; j<[completeAddressArray count]; j++) {
        
        [globalCompleteAddress addObject:[completeAddressArray objectAtIndex:j]];
    }
    
    
    FindOptionsViewController *iFindOptionsListViewContoller = [[FindOptionsViewController alloc]initWithNibName:@"FindOptionsViewController" bundle:[NSBundle mainBundle]];
    iFindOptionsListViewContoller.filterId = [SharedManager retAffId];
    iFindOptionsListViewContoller.srtFilterObj = [[SortAndFilter alloc]initWithObject:sortFilObject];
    
    for (int i=0; i<[filterRetailersListObjArray count]; i++) {
        //        [globalFilterRetailerArray addObject:[filterRetailersListObjArray objectAtIndex:i]];
    }
    
    //    [self.navigationController pushViewController:iFindOptionsListViewContoller animated:NO];
    //    [iFindOptionsListViewContoller release];
    
    //    FindOptionsViewController *iFindOptionsListViewContoller = [[FindOptionsViewController alloc]initWithNibName:@"FindOptionsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iFindOptionsListViewContoller animated:NO];
    //[iFindOptionsListViewContoller release];
}


//-(void) swapListMap : (UIBarButtonItem *) sender {
//
//	switch (sender.tag) {
//		case 2:
//		{
//			self.navigationItem.rightBarButtonItem.tag = 1 ;
//            UIButton *showMapBtn = [UtilityManager customizeBarButtonItem:@"Show Map"];
//            //            [showMapBtn setFrame:CGRectMake(250.0f, 0.0f, 70.0f, 30.0f)];
//            //            [showMapBtn setImage:[UIImage imageNamed:@"showMapBtn_down.png"] forState:UIControlStateNormal];
//            //            [showMapBtn setImage:[UIImage imageNamed:@"showMapBtn_up.png"] forState:UIControlStateHighlighted];
//            [showMapBtn addTarget:self action:@selector(swapListMap:) forControlEvents:UIControlEventTouchUpInside];
//            showMapBtn.tag=1;
//            [self.navigationItem.rightBarButtonItem setCustomView:showMapBtn];
//
//			mapView.hidden = YES;
//			partnersTable.hidden = NO;
//			[mapView removeAnnotations:annArray];
//		}
//			break;
//		case 1:
//		{
//            DLog(@"%d - %d", [latArray count], [longArray count]);
//			self.navigationItem.rightBarButtonItem.tag = 2 ;
//			//self.navigationItem.rightBarButtonItem.title = NSLocalizedString(@"Show List",@"Show List") ;
//            UIButton *showListBtn = [UtilityManager customizeBarButtonItem:@"Show List"];
//            //            [showListBtn setFrame:CGRectMake(250.0f, 0.0f, 70.0f, 30.0f)];
//            //            [showListBtn setImage:[UIImage imageNamed:@"showListBtn_down.png"] forState:UIControlStateNormal];
//            //            [showListBtn setImage:[UIImage imageNamed:@"showListBtn_up.png"] forState:UIControlStateHighlighted];
//            [showListBtn addTarget:self action:@selector(swapListMap:) forControlEvents:UIControlEventTouchUpInside];
//            showListBtn.tag=2;
//            [self.navigationItem.rightBarButtonItem setCustomView:showListBtn];
//
//			mapView.hidden = NO;
//			partnersTable.hidden = YES;
//
//			mapView.hidden = NO;
//			[mapView setMapType:MKMapTypeStandard];
//			[mapView setZoomEnabled:YES];
//			[mapView setScrollEnabled:YES];
//			[mapView setDelegate:self];
//            CLLocationCoordinate2D southWest;
//            CLLocationCoordinate2D northEast;
//			if ([latArray count] == 0 || latArray == nil || [longArray count] == 0 || longArray == nil)
//            {
//                return;
//            }
//            //check is reqd as in case only scansee data is there and no gooPLe data it would crash
//            if ([latArray count]){
//
//                southWest = CLLocationCoordinate2DMake([[latArray objectAtIndex:0] doubleValue],
//                                                       [[longArray objectAtIndex:0] doubleValue]);
//                northEast = southWest;
//            }
//
//            for(int i=0; i<[latArray count]; i++)
//            {
//                RetailerAnnotation *ann = [[RetailerAnnotation alloc] init];
//                ann.coordinate = CLLocationCoordinate2DMake([[latArray objectAtIndex:i] doubleValue],
//                                                            [[longArray objectAtIndex:i] doubleValue]);
//                [mapView addAnnotation:ann];
//                [annArray addObject:ann];
//                [ann setTitle:[globalNameArray objectAtIndex:i]];
//                [ann setSubtitle:[globalAddressArray objectAtIndex:i]];
//                ReleaseAndNilify(ann);
//
//            }
//
//            MKMapRect zoomRect = MKMapRectNull;
//            for (id <MKAnnotation> annotation in mapView.annotations)
//            {
//                MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
//                MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
//                zoomRect = MKMapRectUnion(zoomRect, pointRect);
//            }
//            [mapView setVisibleMapRect:zoomRect animated:YES];
//
//		}
//			break;
//		default:
//			break;
//	}
//}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    MKPinAnnotationView* pinView = nil;
    
    if ([annotation isKindOfClass:[RetailerAnnotation class]])
    {
        // try to dequeue an existing pin view first
        static NSString* RetailerAnnotationIdentifier = @"retailerAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:RetailerAnnotationIdentifier];
        
        if (!pinView)
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
                                                  initWithAnnotation:annotation reuseIdentifier:RetailerAnnotationIdentifier] ;
            //customPinView.pinColor = MKPinAnnotationColorPurple;
            customPinView.animatesDrop = YES;
            
            customPinView.canShowCallout = YES;
            UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            customPinView.rightCalloutAccessoryView = infoButton;
            
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
    }
    pinView.canShowCallout = YES;
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    for (int j = 0 ; j < [globalNameArray count]; j++) {
        if ([[globalNameArray objectAtIndex:j] isEqualToString:view.annotation.title]) {
            
            
            NSLog(@"%@",[globalRetLocId objectAtIndex:j]);
            NSIndexPath *ip = [NSIndexPath indexPathForRow:j inSection:0];
            [linkID insertObject:@"0" atIndex:[linkID count]];
            [self getRetailerDetails:ip];
            return;
        }
    }
}


#pragma mark request methods

-(void)navigateToCouponsViewController
{
    [defaults setBool:YES forKey:@"myGallerysplash"];
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iCouponsViewController animated:NO];
    //[iCouponsViewController release];
}




-(void) navigateToSubMenu
{
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:iMainMenuViewController animated:NO];
    //[iMainMenuViewController release];
}


-(void)request_HubcitiAnythingInfo{
    
    iWebRequestState = HubcitiAnythingInfo;
    requestAnythingFromMainMenu=TRUE;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfo",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><pageId>%@</pageId>",[linkID objectAtIndex:[linkID count]-1]];
    
    if (![defaults boolForKey:BottomButton]){
        
        if([defaults valueForKey:KEY_MITEMID])
            [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else{
        if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    }
    
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}

-(void)request_appsitedetails{
    
    iWebRequestState = appsitedetails;
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if(![defaults boolForKey:BottomButton]){
        if ([defaults valueForKey:KEY_MITEMID])
            [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    }
    else {
        
        if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
            [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
}



//-(void)request_GetUserCat{
//
//    iWebRequestState = GETFAVCATEGORIES;
//    if([Network currentReachabilityStatus]==0)
//    {
//        [common showAlert];
//    }
//    else {
//
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//
//    }
//}

-(void)requestToGetPreferredLocations
{
    iWebRequestState= GETFAVLOCATIONS;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
    }
}


-(void)navigateToEventList
{
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    self.iEventsListViewController = iEventsListViewController;
    [self.navigationController pushViewController:iEventsListViewController animated:NO];
    //[iEventsListViewController release];;
}




//-(void) navigateToScanNow
//{
//    [defaults setBool:YES forKey:@"KEY_SCANSPLASH"];
//    [defaults setValue:@"scanNow" forKey:KEY_SEARCHFROM];
//    [defaults setObject:@"YES" forKey:@"KEY_SCANNERCALLED"];
//
//    int menulevel = [[defaults valueForKey:KEY_MENULEVEL]integerValue];
//    if(menulevel > 1)
//        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
//    else
//        [defaults  setBool:NO forKey:@"DisplayBackBarButton"];
//
//
//    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
//    ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];
//    [self.navigationController pushViewController:viewScanNowScreen animated:NO];
//    [viewScanNowScreen release];
//
//}



//get user favorite categories
//-(void)request_GetFavCategories{
//
//    iWebRequestState = GETFAVCATEGORIES;
//    if([Network currentReachabilityStatus]==0)
//    {
//        common = [[[CommonUtility alloc]init];
//        [common showAlert];
//    }
//    else {
//
//        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
//        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
//
//        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
//
//    }
//}

-(void)getRetailerDetails:(id)indexPath1{
    
    NSIndexPath *ipath = indexPath1;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>", [globalRetLocId objectAtIndex:ipath.row] ];
    //[defaults  objectForKey:@"retailLocationID"]
    
    
    [defaults setValue:[globalRetLocId objectAtIndex:ipath.row] forKey:@"retailLocationID"];
    
    // @Deepak: Added this code for "getretsummary" in UserTracking
    [requestStr appendFormat:@"<scanTypeId>%@</scanTypeId>",@"0"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if([globalRetailerListID count] > ipath.row)
    {
        [requestStr appendFormat:@"<retListId>%@</retListId>",[globalRetailerListID objectAtIndex:ipath.row]];
        [defaults setObject:[globalRetailerListID objectAtIndex:ipath.row] forKey:KEY_RLISTID];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    else if ( [defaults  valueForKey:@"postalCode"] && (![[defaults  valueForKey:@"postalCode"] isEqualToString:@""]))
        [requestStr appendFormat:@"<postalCode>%@</postalCode>",[defaults  valueForKey:@"postalCode"]];
    
    
    [requestStr appendFormat:@"<retailerId>%@</retailerId>",[globalRetailerID objectAtIndex:ipath.row]];
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    
    DLog(@"requestStr = %@",requestStr);
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    ReleaseAndNilify(requestStr);
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]){
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
    {
        [defaults  setObject:responseXml forKey:KEY_RESPONSEXML] ;
        
        RetailerSummaryViewController *retailerSummaryPage = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        
        retailerSummaryPage.distanceFromPreviousScreen = [distanceArray objectAtIndex:ipath.row];
        [self.navigationController pushViewController:retailerSummaryPage animated:NO];
        //[retailerSummaryPage release];
    }
    else
    {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
}



//need to be modified later
//-(void)filtersClicked:(id)sender{
//
//    CityExperienceViewController *ausExp = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
//    [defaults setInteger:200 forKey:@"CityExpTableTag"];
//    [ausExp setTagValue:200];
//    [ausExp release];
//    [self.navigationController popViewControllerAnimated:NO];
//
//}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(filterRetailersListObjArray.count > 0)
    {
        lastVisitedRecord = [[[filterRetailersListObjArray objectAtIndex:[filterRetailersListObjArray count]-1]retRowNumber]intValue];
    }
    
    //    if (indexPath.row == [filterRetailersListObjArray count] && nextPage == 1) {
    //        // This is the last cell
    //        if (![defaults boolForKey:@"ViewMore"] && lastVisitedRecord!=0) {
    //            [defaults setBool:YES forKey:@"ViewMore"];
    //
    //            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
    //            dispatch_sync(dispatchQueue, ^(void){
    //                //[self fetchNextPage:self];// More records to be shown...pagination
    //
    //                [self request_PartnerRet:lastVisitedRecord withObj:sortFilObject];
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    [partnersTable reloadData];
    //                });
    //            });
    //
    //        }
    //    }
    
    switch ([segment selectedSegmentIndex]) {
        case 0:
        case 1:
        {
            
            if (indexPath.row == [filterRetailersListObjArray count] && nextPage == 1){
                if (![defaults boolForKey:@"ViewMore"] && lastVisitedRecord!=0) {
                    
                    [defaults setBool:YES forKey:@"ViewMore"];
//                    segment.userInteractionEnabled = NO;
//                    partnersTable.userInteractionEnabled = NO;
                    self.view.userInteractionEnabled = NO;
                    
                    dispatch_queue_t dispatchQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(dispatchQueue, ^(void){
                        [self fetchNextFiltersList];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            
                            //[loading stopAnimating];
                            
                            
//                            segment.userInteractionEnabled = YES;
//                            partnersTable.userInteractionEnabled = YES;
                            self.view.userInteractionEnabled = YES;
                            
                            [defaults setBool:NO forKey:@"ViewMore"];
                        });
                        
                    });
                }
            }
            
        }
            break;
        default:
            break;
    }
    
    
    
}



-(void) fetchNextFiltersList{
    if (nextPage == 1)
    {
        
        [self request_PartnerRet:lastVisitedRecord withObj:sortFilObject];
        
    }
    else
    {
        return;
    }
}

#pragma tableView delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [defaults setBool:NO forKey:@"ViewMore"];
    if (isLoading) {
        [partnersTable deselectRowAtIndexPath:[partnersTable indexPathForSelectedRow] animated:YES];
        return;
    }
    /*if (indexPath.row == [filterRetailersListObjArray count]) {
        //        if (nextPage == 1) {
        //            [self fetchNextPage:self];// More records to be shown...pagination
        //            DLog(@"NEXT PAGE - %d", nextPage);
        //        }
        [partnersTable deselectRowAtIndexPath:[partnersTable indexPathForSelectedRow] animated:YES];
        return;
    }*/
    
    [defaults  setObject:[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailLocationID]
                  forKey:@"retailLocationID"];
    
    [defaults  setObject:[retailerIdArray objectAtIndex:indexPath.row]
                  forKey:KEY_RETAILERID];
    
    [defaults  setObject:[[filterRetailersListObjArray objectAtIndex:indexPath.row]retRibbonAdImgPath]
                  forKey:@"ribbonAdImagePath"];
    
    [defaults setObject:[retailerNameArray objectAtIndex:indexPath.row]
                 forKey:KEY_RTLNAME];
    [linkID insertObject:@"0" atIndex:[linkID count]];
    //@Deepak: for user Tracking
    [defaults setObject:[retListIDArray objectAtIndex:indexPath.row] forKey:KEY_RLISTID];
    
    //first send the req, get the response, show the splash if there and then parse the response
    rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
    rsvc.distanceFromPreviousScreen = [[filterRetailersListObjArray objectAtIndex:indexPath.row]retDistance];
    [self request_RetSummary:indexPath];
    
    [partnersTable deselectRowAtIndexPath:[partnersTable indexPathForSelectedRow] animated:YES];
}

//-(void)fetchNextPage:(id)sender{
//
//    lastVisitedRecord = [[rowNumberArray objectAtIndex:[rowNumberArray count]-1 ]intValue];
//    [self request_PartnerRet:lastVisitedRecord];
//
//}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    float width = [self getWidth:indexPath];
    
    
    
    
    
    float noOfline =[ self getLabelSize:[self retailerAddress:indexPath] withSize:width withFont:addressFont];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        if(noOfline<2)
            return 50;
        else
            return  66;
    }
    else
    {
        return 90;
    }
}
-(CGFloat) getLabelSize : (NSString*) labelString withSize:(CGFloat) labelwidth withFont:(UIFont *) font
{
    
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    CGFloat noOflines = ceil(rect.size.height / font.lineHeight);
    
    
    return noOflines;
    
}
-(float) getWidth: (NSIndexPath*) indexPath{
    float width = (IPAD ? 500:180.0f);
    if([filterRetailersListObjArray count]){
        if (indexPath.row != [filterRetailersListObjArray count]){
            if([[[filterRetailersListObjArray objectAtIndex:indexPath.row]retSaleFlag] boolValue])
            {
                width = (IPAD ? 450.0f:160.0f);
            }
            else{
                
                width = (IPAD ? 500:180.0f);
            }
        }
    }
    return width;
}
-(NSString *) retailerAddress:(NSIndexPath *)indexPath{
    NSString *address = @"";
    if([filterRetailersListObjArray count]){
        
        if (indexPath.row != [filterRetailersListObjArray count]){
            if((![[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress2] isEqualToString:@"N/A"])&& [[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress2].length>0){
                address =  [NSString stringWithFormat:@"%@, %@",
                            [[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress],[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress2]];
            }
            else{
                address = [NSString stringWithFormat:@"%@",
                           [[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress]];
            }
        }
    }
    return address;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch ([segment selectedSegmentIndex])
    {
        case 0:
        {
            if (nextPage == 1)
                numberOfRows = [filterRetailersListObjArray count] + 1;
            else
                numberOfRows = ([filterRetailersListObjArray count]);
            break;
        }
        case 1:
        {
            if (nextPage == 1)
                numberOfRows = [filterRetailersListObjArray count] + 1;
            else
                numberOfRows = [filterRetailersListObjArray count];
            break;
        }
            
            
    }
    
    
    
    return numberOfRows;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(sortFilObject.distanceSelected==YES ||(sortFilObject.alphabeticallySelected==YES  && [filterRetailersListObjArray count] !=0))// [defaults setValue:@"category" forKey:@"GroupCitiExpBy"];
    {
        return 20.0;
    }
    else{
        return 0.0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *lblCatName;
    
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH - 10, 18)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:15]];
    }
    else
    {
        lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, SCREEN_WIDTH - 10, 24)];
        [lblCatName setFont:[UIFont boldSystemFontOfSize:20]];
    }
    
    [lblCatName setTextColor:[UIColor blackColor]];
    [lblCatName setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
    
    //[lblCatName setText:[NSString stringWithFormat:@" Sort by Distance"]];
    
    if (sortFilObject.distanceSelected == YES) {
        [lblCatName setText:[NSString stringWithFormat:@" Sorted by Distance"]];
    }
    else if (sortFilObject.alphabeticallySelected == YES) {
        [lblCatName setText:[NSString stringWithFormat:@" Sorted by Name"]];
    }
    return lblCatName;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *cell;
    switch ([segment selectedSegmentIndex]) {
        case 0:
        case 1:
        {
            
            
            static NSString *cellIdentifier = @"Cell";
            static NSString *cellIdentifier1 = @"More Cell";
            
            SdImageView *asyncImageView = nil;
            UILabel *retailerName = nil;
            UILabel *address12 = nil;
            UILabel *cityStateCode = nil;
            UILabel *distanceLabel = nil;
            UILabel *openCloseLabel = nil;
            
            if (indexPath.row != [filterRetailersListObjArray count])
                cell = [partnersTable dequeueReusableCellWithIdentifier:cellIdentifier];
            else
                cell = [partnersTable dequeueReusableCellWithIdentifier:cellIdentifier1];
            
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            if (indexPath.row != [filterRetailersListObjArray count]) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] ;
                
                               //                frame.origin.x = 5;
                //                frame.origin.y = 7;
                //                frame.size.width = 44;
                //                frame.size.height = 44;
                asyncImageView = [[SdImageView alloc] init] ;
                // asyncImageView.contentMode = UIViewContentModeScaleAspectFill;
                asyncImageView.backgroundColor = [UIColor clearColor];
                asyncImageView.translatesAutoresizingMaskIntoConstraints=NO;
                [cell.contentView addSubview:asyncImageView];
                
                //                frame.origin.x = 52;
                //                frame.origin.y = 0;
                //                frame.size.width = 250;
                //                frame.size.height = 24;
                
                retailerName = [[UILabel alloc] init] ;
                retailerName.translatesAutoresizingMaskIntoConstraints=NO;
                retailerName.textColor = [UIColor colorWithRGBInt:0x112e72];
                [cell.contentView addSubview:retailerName];
                
                //                frame.origin.x = 52;
                //                frame.origin.y = 24;
                //                frame.size.width = 250;
                //                frame.size.height = 48;
                address12 = [[UILabel alloc] init] ;
                if(IPAD)
                {
                    address12.numberOfLines = 1;
                }
                else
                {
                    address12.numberOfLines = 2;
                }
                address12.textColor = [UIColor darkGrayColor];
                address12.translatesAutoresizingMaskIntoConstraints=NO;
                [cell.contentView addSubview:address12];
                
                
                //                frame.origin.x = 52;
                //                frame.origin.y = 48;
                //                frame.size.width = 250;
                //                frame.size.height = 24;
                cityStateCode = [[UILabel alloc] init] ;
                cityStateCode.numberOfLines = 1;
                cityStateCode.textColor = [UIColor darkGrayColor];
                cityStateCode.translatesAutoresizingMaskIntoConstraints=NO;
                [cell.contentView addSubview:cityStateCode];
                
                distanceLabel = [[UILabel alloc] init] ;
                distanceLabel.textColor = [UIColor darkGrayColor];
                distanceLabel.translatesAutoresizingMaskIntoConstraints=NO;
                [cell.contentView addSubview:distanceLabel];
                
                openCloseLabel = [[UILabel alloc]init];
                openCloseLabel.textColor = [UIColor darkGrayColor];
                openCloseLabel.translatesAutoresizingMaskIntoConstraints=NO;
                [cell.contentView addSubview:openCloseLabel];
                
                
                float width;
                if([[[filterRetailersListObjArray objectAtIndex:indexPath.row]retSaleFlag] boolValue])
                    width = (IPAD ? 450.0f:160.0f);
                else
                    width = (IPAD ? 500:180.0f);
                numberOfLines =[ self getLabelSize:[self retailerAddress:indexPath] withSize:width withFont:addressFont];
                NSDictionary *viewsDictionary = @{@"image":asyncImageView,@"retailerName":retailerName,@"address12":address12, @"cityStateCode" :cityStateCode,@"distanceLabel" :distanceLabel,@"openCloseLabel" :openCloseLabel};
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    
                    // Vertical constrains
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[image(40)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[retailerName(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    if(numberOfLines<2){
                        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(15)-[address12(20)]"] options:0 metrics:0 views:viewsDictionary]];
                        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(28)-[cityStateCode(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    }
                    else{
                        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(14)-[address12(40)]"] options:0 metrics:0 views:viewsDictionary]];
                        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(43)-[cityStateCode(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    }
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(2)-[distanceLabel(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(25)-[openCloseLabel(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    retailerName.font = [UIFont boldSystemFontOfSize:14];
                    address12.font = [UIFont boldSystemFontOfSize:10];
                    cityStateCode.font = [UIFont boldSystemFontOfSize:10];
                    distanceLabel.font = [UIFont boldSystemFontOfSize:10];
                    openCloseLabel.font = [UIFont boldSystemFontOfSize:10];
                    // Horizontal constraints
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(40)]"] options:0 metrics:0 views:viewsDictionary]];
                    if([filterRetailersListObjArray count]>indexPath.row)
                    {
                        if([[[filterRetailersListObjArray objectAtIndex:indexPath.row]retSaleFlag] boolValue])
                        {
                             [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(59)-[retailerName(140)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(59)-[address12(160)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(59)-[cityStateCode(160)]"] options:0 metrics:0 views:viewsDictionary]];
                        }
                        else
                        {
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(59)-[retailerName(180)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(59)-[address12(180)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(59)-[cityStateCode(180)]"] options:0 metrics:0 views:viewsDictionary]];
                        }

                    }
                   
                    
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(247)-[distanceLabel(80)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(247)-[openCloseLabel(50)]"] options:0 metrics:0 views:viewsDictionary]];
                    
                    
                }
                else
                {
                    // Vertical constrains
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[image(70)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(7)-[retailerName(22)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(35)-[address12(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(57)-[cityStateCode(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[distanceLabel(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(40)-[openCloseLabel(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    
                    retailerName.font = [UIFont boldSystemFontOfSize:20];
                    address12.font = [UIFont boldSystemFontOfSize:15];
                    cityStateCode.font = [UIFont boldSystemFontOfSize:15];
                    openCloseLabel.font = [UIFont boldSystemFontOfSize:15];
                    
                    // Horizontal constraints
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(70)]"] options:0 metrics:0 views:viewsDictionary]];
                    if([filterRetailersListObjArray count]>indexPath.row)
                    {
                        if([[[filterRetailersListObjArray objectAtIndex:indexPath.row]retSaleFlag] boolValue])
                        {
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[retailerName(450)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[address12(450)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[cityStateCode(450)]"] options:0 metrics:0 views:viewsDictionary]];
                        }
                        else
                        {
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[retailerName(500)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[address12(500)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[cityStateCode(500)]"] options:0 metrics:0 views:viewsDictionary]];
                        }
                    }
                  
                    
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(630)-[distanceLabel(80)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(630)-[openCloseLabel(50)]"] options:0 metrics:0 views:viewsDictionary]];
                }
                
            }
            else
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier1] ;
            
            
            if (indexPath.row == [filterRetailersListObjArray count] && nextPage == 1) {
                loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                loading.center = cell.contentView.center;
                CGRect frame;
                
                frame.origin.x = SCREEN_WIDTH/2;
                frame.origin.y = 5;
                frame.size.width = 40;
                frame.size.height = 40;
                
                if(DEVICE_TYPE == UIUserInterfaceIdiomPad)
                    
                    loading.frame = frame;
                
                loading.color = [UIColor blackColor];
                loading.tag = 31;
                [cell.contentView addSubview:loading];
                [loading startAnimating];
                cell.userInteractionEnabled = NO;
                
            }
            
            else if (indexPath.row == [filterRetailersListObjArray count] && nextPage == 0) {
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            else if([filterRetailersListObjArray count]>indexPath.row) {
                
                
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                NSString *logoImageStr = [[filterRetailersListObjArray objectAtIndex:indexPath.row]retLogoImgPath];
                if(![logoImageStr isEqualToString:@"N/A"] )
                {
                    [asyncImageView loadImage:[NSString stringWithFormat:@"%@", logoImageStr]];
                }
                
                retailerName.text = [NSString stringWithFormat:@"%@", [[filterRetailersListObjArray objectAtIndex:indexPath.row]retailerName]];
                
                if([[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress2])
                {
                    address12.text = [NSString stringWithFormat:@"%@, %@",[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress],[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress2]];
                    //address12.numberOfLines = 2;
                }
                else
                {
                    address12.text = [[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress];
                    //address12.numberOfLines = 1;
                }
                cityStateCode.text = [NSString stringWithFormat:@"%@, %@, %@",[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailCity],[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailState],[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailPostalCode]];
                distanceLabel.text = [[filterRetailersListObjArray objectAtIndex:indexPath.row]retDistance];
                
                if(([[filterRetailersListObjArray objectAtIndex:indexPath.row]retailOpenClose].length != 0) && (![[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailOpenClose] isEqualToString:@"N/A"]))
                {
                    openCloseLabel.text = [[filterRetailersListObjArray objectAtIndex:indexPath.row]retailOpenClose] ;
                }
                
                if ([[[filterRetailersListObjArray objectAtIndex:indexPath.row]retSaleFlag] boolValue]) {
                    
                    UIImageView *specImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"retSpecialsIcon_o.png"]];
                    // specImage.frame = CGRectMake(250, -8, 40, 40);
                    
                    //                NSDictionary *sDictionary = @{@"spec":specImage};
                    //                specImage.translatesAutoresizingMaskIntoConstraints=NO;
                    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                        
                        specImage.frame = CGRectMake(200, -8, 40, 40);
                    }
                    else
                    {
                        specImage.frame = CGRectMake(580, 0, 40, 40);
                    }
                    
                    [cell addSubview:specImage];
                    // [specImage release];
                }
                
            }
            return cell;
        }
            
            break;
        default:
            break;
            
    }
    
    return cell;
    
}

/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *cell;
    switch ([segment selectedSegmentIndex]) {
        case 0:
        case 1:
        {
            
            
            static NSString *cellIdentifier = @"Cell";
            static NSString *cellIdentifier1 = @"More Cell";
            
            SdImageView *asyncImageView = nil;
            UILabel *retailerName = nil;
            UILabel *address12 = nil;
            UILabel *cityStateCode = nil;
            UILabel *distanceLabel = nil;
            UILabel *openCloseLabel = nil;
            
            if (indexPath.row != [filterRetailersListObjArray count])
                cell = [partnersTable dequeueReusableCellWithIdentifier:cellIdentifier];
            else
                cell = [partnersTable dequeueReusableCellWithIdentifier:cellIdentifier1];
            
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            if (indexPath.row != [filterRetailersListObjArray count]) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] ;
                
                //                frame.origin.x = 5;
                //                frame.origin.y = 7;
                //                frame.size.width = 44;
                //                frame.size.height = 44;
                asyncImageView = [[SdImageView alloc] init] ;
                // asyncImageView.contentMode = UIViewContentModeScaleAspectFill;
                asyncImageView.backgroundColor = [UIColor clearColor];
                asyncImageView.translatesAutoresizingMaskIntoConstraints=NO;
                [cell.contentView addSubview:asyncImageView];
                
                //                frame.origin.x = 52;
                //                frame.origin.y = 0;
                //                frame.size.width = 250;
                //                frame.size.height = 24;
                
                retailerName = [[UILabel alloc] init] ;
                retailerName.translatesAutoresizingMaskIntoConstraints=NO;
                retailerName.textColor = [UIColor colorWithRGBInt:0x112e72];
                [cell.contentView addSubview:retailerName];
                
                //                frame.origin.x = 52;
                //                frame.origin.y = 24;
                //                frame.size.width = 250;
                //                frame.size.height = 48;
                address12 = [[UILabel alloc] init] ;
                if(IPAD)
                {
                    address12.numberOfLines = 1;
                }
                else
                {
                    address12.numberOfLines = 2;
                }
                address12.textColor = [UIColor darkGrayColor];
                address12.translatesAutoresizingMaskIntoConstraints=NO;
                [cell.contentView addSubview:address12];
                
                
                //                frame.origin.x = 52;
                //                frame.origin.y = 48;
                //                frame.size.width = 250;
                //                frame.size.height = 24;
                cityStateCode = [[UILabel alloc] init] ;
                cityStateCode.numberOfLines = 1;
                cityStateCode.textColor = [UIColor darkGrayColor];
                cityStateCode.translatesAutoresizingMaskIntoConstraints=NO;
                [cell.contentView addSubview:cityStateCode];
                
                distanceLabel = [[UILabel alloc] init] ;
                distanceLabel.textColor = [UIColor darkGrayColor];
                distanceLabel.translatesAutoresizingMaskIntoConstraints=NO;
                [cell.contentView addSubview:distanceLabel];
                
                openCloseLabel = [[UILabel alloc]init];
                openCloseLabel.textColor = [UIColor darkGrayColor];
                openCloseLabel.translatesAutoresizingMaskIntoConstraints=NO;
                [cell.contentView addSubview:openCloseLabel];
                
                
                float width;
                if([[[filterRetailersListObjArray objectAtIndex:indexPath.row]retSaleFlag] isEqualToString:@"true"])
                    width = (IPAD ? 450.0f:160.0f);
                else
                    width = (IPAD ? 500:180.0f);
                numberOfLines =[ self getLabelSize:[self retailerAddress:indexPath] withSize:width withFont:addressFont];
                NSDictionary *viewsDictionary = @{@"image":asyncImageView,@"retailerName":retailerName,@"address12":address12, @"cityStateCode" :cityStateCode,@"distanceLabel" :distanceLabel,@"openCloseLabel" :openCloseLabel};
                if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                    
                    // Vertical constrains
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[image(40)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[retailerName(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    if(numberOfLines<2){
                        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(15)-[address12(20)]"] options:0 metrics:0 views:viewsDictionary]];
                        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(28)-[cityStateCode(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    }
                    else{
                        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(14)-[address12(40)]"] options:0 metrics:0 views:viewsDictionary]];
                        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(43)-[cityStateCode(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    }
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(2)-[distanceLabel(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(25)-[openCloseLabel(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    retailerName.font = [UIFont boldSystemFontOfSize:14];
                    address12.font = [UIFont boldSystemFontOfSize:10];
                    cityStateCode.font = [UIFont boldSystemFontOfSize:10];
                    distanceLabel.font = [UIFont boldSystemFontOfSize:10];
                    openCloseLabel.font = [UIFont boldSystemFontOfSize:10];
                    // Horizontal constraints
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(40)]"] options:0 metrics:0 views:viewsDictionary]];
                    if([filterRetailersListObjArray count]>indexPath.row)
                    {
                        if([[[filterRetailersListObjArray objectAtIndex:indexPath.row]retSaleFlag] isEqualToString:@"true"])
                        {
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(59)-[retailerName(140)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(59)-[address12(160)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(59)-[cityStateCode(160)]"] options:0 metrics:0 views:viewsDictionary]];
                        }
                        else
                        {
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(59)-[retailerName(180)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(59)-[address12(180)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(59)-[cityStateCode(180)]"] options:0 metrics:0 views:viewsDictionary]];
                        }
                        
                    }
                    
                    
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(247)-[distanceLabel(80)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(247)-[openCloseLabel(50)]"] options:0 metrics:0 views:viewsDictionary]];
                    
                    
                }
                else
                {
                    // Vertical constrains
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[image(70)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(7)-[retailerName(22)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(35)-[address12(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(57)-[cityStateCode(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[distanceLabel(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(40)-[openCloseLabel(20)]"] options:0 metrics:0 views:viewsDictionary]];
                    
                    retailerName.font = [UIFont boldSystemFontOfSize:20];
                    address12.font = [UIFont boldSystemFontOfSize:15];
                    cityStateCode.font = [UIFont boldSystemFontOfSize:15];
                    openCloseLabel.font = [UIFont boldSystemFontOfSize:15];
                    
                    // Horizontal constraints
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[image(70)]"] options:0 metrics:0 views:viewsDictionary]];
                    if([filterRetailersListObjArray count]>indexPath.row)
                    {
                        if([[[filterRetailersListObjArray objectAtIndex:indexPath.row]retSaleFlag] isEqualToString:@"true"])
                        {
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[retailerName(450)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[address12(450)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[cityStateCode(450)]"] options:0 metrics:0 views:viewsDictionary]];
                        }
                        else
                        {
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[retailerName(500)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[address12(500)]"] options:0 metrics:0 views:viewsDictionary]];
                            [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(90)-[cityStateCode(500)]"] options:0 metrics:0 views:viewsDictionary]];
                        }
                    }
                    
                    
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(630)-[distanceLabel(80)]"] options:0 metrics:0 views:viewsDictionary]];
                    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(630)-[openCloseLabel(50)]"] options:0 metrics:0 views:viewsDictionary]];
                }
                
            }
            else
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier1] ;
            
            
            if (indexPath.row == [filterRetailersListObjArray count] && nextPage == 1) {
                loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                loading.center = cell.contentView.center;
                CGRect frame;
                
                frame.origin.x = SCREEN_WIDTH/2;
                frame.origin.y = 5;
                frame.size.width = 40;
                frame.size.height = 40;
                
                if(DEVICE_TYPE == UIUserInterfaceIdiomPad)
                    
                    loading.frame = frame;
                
                loading.color = [UIColor blackColor];
                loading.tag = 31;
                [cell.contentView addSubview:loading];
                [loading startAnimating];
                cell.userInteractionEnabled = NO;
                
            }
            
            else if (indexPath.row == [filterRetailersListObjArray count] && nextPage == 0) {
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            else if([filterRetailersListObjArray count]>indexPath.row) {
                
                //                NSString *fullAddress = [[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress];
                //                NSString *match = @",";
                //                NSString *add1;
                //                NSString *statecode;
                //
                //                NSScanner *scanner = [NSScanner scannerWithString:fullAddress];
                //                [scanner scanUpToString:match intoString:&add1];
                //
                //                [scanner scanString:match intoString:nil];
                //                statecode = [fullAddress substringFromIndex:scanner.scanLocation];
                //
                //                NSLog(@"preTel: %@", add1);
                //                NSLog(@"postTel: %@", statecode);
                
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                NSString *logoImageStr = [[filterRetailersListObjArray objectAtIndex:indexPath.row]retLogoImgPath];
                if(![logoImageStr isEqualToString:@"N/A"] )
                {
                    [asyncImageView loadImage:[NSString stringWithFormat:@"%@", logoImageStr]];
                }
                
                retailerName.text = [NSString stringWithFormat:@"%@", [[filterRetailersListObjArray objectAtIndex:indexPath.row]retailerName]];
                //                address12.text = [NSString stringWithFormat:@"%@ , %@",
                //                                    [[filterRetailersListObjArray objectAtIndex:indexPath.row]retDistance],
                //                                    [[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress]];
                if([[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress2])
                {
                    address12.text = [NSString stringWithFormat:@"%@, %@",[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress],[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress2]];
                    //address12.numberOfLines = 2;
                }
                else
                {
                    address12.text = [[filterRetailersListObjArray objectAtIndex:indexPath.row]retailAddress];
                    //address12.numberOfLines = 1;
                }
                cityStateCode.text = [NSString stringWithFormat:@"%@, %@, %@",[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailCity],[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailState],[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailPostalCode]];
                distanceLabel.text = [[filterRetailersListObjArray objectAtIndex:indexPath.row]retDistance];
                
                if(([[filterRetailersListObjArray objectAtIndex:indexPath.row]retailOpenClose].length != 0) && (![[[filterRetailersListObjArray objectAtIndex:indexPath.row]retailOpenClose] isEqualToString:@"N/A"]))
                {
                    openCloseLabel.text = [[filterRetailersListObjArray objectAtIndex:indexPath.row]retailOpenClose] ;
                }
                
                if ([[[filterRetailersListObjArray objectAtIndex:indexPath.row]retSaleFlag] isEqualToString:@"true"]) {
                    
                    UIImageView *specImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"retSpecialsIcon_o.png"]];
                    // specImage.frame = CGRectMake(250, -8, 40, 40);
                    
                    //                NSDictionary *sDictionary = @{@"spec":specImage};
                    //                specImage.translatesAutoresizingMaskIntoConstraints=NO;
                    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
                        
                        specImage.frame = CGRectMake(200, -8, 40, 40);
                    }
                    else
                    {
                        specImage.frame = CGRectMake(580, 0, 40, 40);
                    }
                    
                    [cell addSubview:specImage];
                    // [specImage release];
                }
                
            }
            return cell;
        }
            
            break;
        default:
            break;
            
    }
    
    return cell;
    
}
*/


#pragma mark parse methods

-(void)parse_PartnersList:(NSString *)response{
   
    if(![defaults boolForKey:@"ViewMore"])
    {
        partnersTable = nil;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self settableViewOnScreen];
        });
        [partnersTable setContentOffset:CGPointMake(0, 0)];
       
        [self refreshData];
    }
    
    NSDictionary* responseData = (NSDictionary*) response;
    
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    
    bottomBtn = [[responseData objectForKey:@"bottomBtn"] intValue];
    isLoading = false;
    if (responseCode == 10000) {
        
        nextPage =  [[responseData objectForKey:@"nextPage"] intValue];
        
        [defaults setValue:[responseData objectForKey:KEY_MAINMENUID] forKey:KEY_MAINMENUID];
        
        filterDesc = [responseData objectForKey:@"sDescription"];
        if (filterDesc.length > 0) {
            filterDescription = YES;
        }
        
        NSArray* retailerElement = [responseData objectForKey:@"retailers"];
        for(int i = 0; i< retailerElement.count; i++){
            
            FilterRetailersListDO *filterRetailerObj = [[FilterRetailersListDO alloc]init];
            filterRetailerObj.retRowNumber = [[retailerElement objectAtIndex:i] objectForKey:@"rowNumber"];
            filterRetailerObj.retListId = [[retailerElement objectAtIndex:i] objectForKey:@"retListId"];
            filterRetailerObj.retailerId = [[retailerElement objectAtIndex:i] objectForKey:KEY_RETAILERID];
            filterRetailerObj.retailerName = [[retailerElement objectAtIndex:i] objectForKey:KEY_RTLNAME];
            filterRetailerObj.retDistance = [[retailerElement objectAtIndex:i] objectForKey:@"distance"];
            filterRetailerObj.retailAddress = [[retailerElement objectAtIndex:i] objectForKey:@"retailAddress1"];
            filterRetailerObj.retailAddress2 = [[retailerElement objectAtIndex:i] objectForKey:@"retailAddress2"];
            
            [rowNumberArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"rowNumber"]];
            [retListIDArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"retListId"]];
            [retailerIdArray addObject:[[retailerElement objectAtIndex:i] objectForKey:KEY_RETAILERID]];
            [retailerNameArray addObject:[[retailerElement objectAtIndex:i] objectForKey:KEY_RTLNAME]];
            [distanceArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"distance"]];
            NSString* add1 = [[retailerElement objectAtIndex:i] objectForKey:@"retailAddress1"];
            
            NSString* add2 = [[retailerElement objectAtIndex:i] objectForKey:@"retailAddress2"];
            if (add1.length > 0) {
                [retailAddressArray addObject:add1];
            }
            if (add2.length > 0) {
                [retailAddress2Array addObject:add2];
            }
            
            filterRetailerObj.retailOpenClose = [[retailerElement objectAtIndex:i] objectForKey:@"locationOpen"];
            [retailerStateArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"locationOpen"]];
            
            NSString* citi = [[retailerElement objectAtIndex:i] objectForKey:@"city"];
            NSString* state = [[retailerElement objectAtIndex:i] objectForKey:@"state"];
            NSString* zip = [[retailerElement objectAtIndex:i] objectForKey:@"postalCode"];
            [retailerStateArray addObject:state];
            [retailerCityArray addObject:citi];
            [retailerPostalCodeArray addObject:zip];
            
            filterRetailerObj.retailCity = citi;
            filterRetailerObj.retailState = state;
            filterRetailerObj.retailPostalCode = zip;
            filterRetailerObj.retailLocationID = [[retailerElement objectAtIndex:i] objectForKey:@"retailLocationId"];
            filterRetailerObj.retLatitude = [[retailerElement objectAtIndex:i] objectForKey:@"latitude"];
            filterRetailerObj.retLongitude =  [[retailerElement objectAtIndex:i] objectForKey:@"longitude"];
            
            
            [retailLocationIDArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"retailLocationId"]];
            [latArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"latitude"]];
            [longArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"longitude"]];
            [completeAddressArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"completeAddress"]];
            
            filterRetailerObj.retLogoImgPath = [[retailerElement objectAtIndex:i] objectForKey:@"logoImagePath"];
            filterRetailerObj.retbannerAdImgPath = [[retailerElement objectAtIndex:i] objectForKey:@"bannerAdImagePath"];
            filterRetailerObj.retRibbonAdImgPath = [[retailerElement objectAtIndex:i] objectForKey:@"ribbonAdImagePath"];
            filterRetailerObj.retRibbonAdUrl = [[retailerElement objectAtIndex:i] objectForKey:@"ribbonAdURL"];
            filterRetailerObj.retSaleFlag = [[retailerElement objectAtIndex:i] objectForKey:@"saleFlag"];
            
            [logoImagePathArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"logoImagePath"]];
            [bannerAdImagePathArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"bannerAdImagePath"]];
            [ribbonAdImagePathArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"ribbonAdImagePath"]];
            [ribbonAdURLArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"ribbonAdURL"]];
            [saleFlagArray addObject:[[retailerElement objectAtIndex:i] objectForKey:@"saleFlag"]];
            
            [filterRetailersListObjArray addObject:filterRetailerObj];
        }
        
        if (bottomBtn==1) {
            
            bottomButtonList = [[NSMutableArray alloc]init];
            
            NSArray* bottomArray = [responseData objectForKey:@"bottomBtnList"];
            
            for(int i = 0; i< bottomArray.count; i++){
                
                bottomButtonDO *bottomButton = [[bottomButtonDO alloc]init];
                
                bottomButton.bottomBtnID  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnID"];
                
                bottomButton.bottomBtnName  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnName"];
                
                bottomButton.bottomBtnImg  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImg"];
                
                bottomButton.bottomBtnImgOff  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImgOff"];
                
                bottomButton.btnLinkTypeID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeID"];
                
                bottomButton.btnLinkTypeName  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeName"];
                
                bottomButton.btnLinkID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkID"];
                
                bottomButton.position  = [[bottomArray objectAtIndex:i] objectForKey:@"position"];
                
                [bottomButtonList addObject:bottomButton];
                
                
                
            }
            
        }
        
        if ([bottomButtonList count]>0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setBottomBarMenu:bottomButtonList];
            });
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [HubCitiAppDelegate removeActivityIndicator];
            [self settableViewOnScreen];
        });
        
        
    }
    else if(responseCode == 10005)
    {
        
        if (bottomBtn==1) {
            
            bottomButtonList = [[NSMutableArray alloc]init];
            
            NSArray* bottomArray = [responseData objectForKey:@"bottomBtnList"];
            
            for(int i = 0; i< bottomArray.count; i++)  {
                
                bottomButtonDO *bottomButton = [[bottomButtonDO alloc]init];
                
                bottomButton.bottomBtnID  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnID"];
                
                bottomButton.bottomBtnName  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnName"];
                
                bottomButton.bottomBtnImg  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImg"];
                
                bottomButton.bottomBtnImgOff  = [[bottomArray objectAtIndex:i] objectForKey:@"bottomBtnImgOff"];
                
                bottomButton.btnLinkTypeID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeID"];
                
                bottomButton.btnLinkTypeName  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkTypeName"];
                
                bottomButton.btnLinkID  = [[bottomArray objectAtIndex:i] objectForKey:@"btnLinkID"];
                
                bottomButton.position  = [[bottomArray objectAtIndex:i] objectForKey:@"position"];
                NSString* filter  = [[bottomArray objectAtIndex:i] objectForKey:@"Filters"];
                [SharedManager setRetGroupId: [filter intValue]];
                
                [bottomButtonList addObject:bottomButton];
                
            }
            
            if([bottomButtonList count] > 0)
            {
                // Set Bottom Menu Button after read from response data
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self setBottomBarMenu:bottomButtonList];
                });
                [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
            }
            else
                [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            
        }
        
        if([responseText isEqualToString:@"No Records Found."])
        {
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseText];
        }
        else{
            if (responseText.length == 0) {
                [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"No Records Found."];
                return;
            }
            else{
                    NSString *responseTextStr = [responseText stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
                    [UtilityManager showFormatedAlert:responseTextStr];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self settableViewOnScreen];
        });
        
       
    }
    
    else
    {
       
        if(responseText.length == 0)
        {
           responseText = @"No Records found";
        }
        
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseText];
       
        [loading stopAnimating];
        
        ReleaseAndNilify(loading);
        
        
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        
         [defaults setBool:NO forKey:@"ViewMore"];
         [partnersTable reloadData];
         [loading stopAnimating];
        
    });
    

  /*  if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbXml.rootXMLElement]]intValue];
    if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
        // [self allocateArrays];
        
        TBXMLElement *nextPageElement = [TBXML childElementNamed:@"nextPage" parentElement:tbXml.rootXMLElement];
        
        nextPage = [[TBXML textForElement:nextPageElement] intValue];
        DLog(@"%d",nextPage);
        
        //added by ashika
        TBXMLElement *sDescription = [TBXML childElementNamed:@"sDescription" parentElement:tbXml.rootXMLElement];
        if(sDescription != nil){
            
            filterDescription = YES;
            filterDesc = [TBXML textForElement:sDescription];
        }
        
        TBXMLElement *retailersElement = [TBXML childElementNamed:@"retailers" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerElement = [TBXML childElementNamed:@"Retailer" parentElement:retailersElement];
        
        //@Deepak: Main Menu and retListID parsed for user tracking
        TBXMLElement *mainMenuIDElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbXml.rootXMLElement];
        if(mainMenuIDElement !=nil)
        {
            [defaults setObject:[TBXML textForElement:mainMenuIDElement] forKey:KEY_MAINMENUID];
        }
        
        while (RetailerElement != nil) {
            FilterRetailersListDO *filterRetailerObj = [[FilterRetailersListDO alloc]init];
            
            TBXMLElement *rowNumberElement = [TBXML childElementNamed:@"rowNumber" parentElement:RetailerElement];
            TBXMLElement *retailerIdElement = [TBXML childElementNamed:KEY_RETAILERID parentElement:RetailerElement];
            TBXMLElement *retailerNameElement = [TBXML childElementNamed:KEY_RTLNAME parentElement:RetailerElement];
            TBXMLElement *retailLocationIDElement = [TBXML childElementNamed:@"retailLocationId" parentElement:RetailerElement];
            TBXMLElement *distanceElement = [TBXML childElementNamed:KEY_DISTANCE parentElement:RetailerElement];
            TBXMLElement *retailAddress1Element = [TBXML childElementNamed:@"retailAddress1" parentElement:RetailerElement];
            TBXMLElement *retailAddress2Element = [TBXML childElementNamed:@"retailAddress2" parentElement:RetailerElement];
            TBXMLElement *logoImagePathElement = [TBXML childElementNamed:@"logoImagePath" parentElement:RetailerElement];
            TBXMLElement *bannerAdImagePathElement = [TBXML childElementNamed:@"bannerAdImagePath" parentElement:RetailerElement];
            TBXMLElement *ribbonAdImagePathElement = [TBXML childElementNamed:@"ribbonAdImagePath" parentElement:RetailerElement];
            TBXMLElement *ribbonAdURLElement = [TBXML childElementNamed:@"ribbonAdURL" parentElement:RetailerElement];
            TBXMLElement *latitudeElement = [TBXML childElementNamed:@"latitude" parentElement:RetailerElement];
            TBXMLElement *longitudeElement = [TBXML childElementNamed:@"longitude" parentElement:RetailerElement];
            TBXMLElement *saleFlagElement = [TBXML childElementNamed:@"saleFlag" parentElement:RetailerElement];
            TBXMLElement *cityElement = [TBXML childElementNamed:@"city" parentElement:RetailerElement];
            TBXMLElement *stateElement = [TBXML childElementNamed:@"state" parentElement:RetailerElement];
            TBXMLElement *postalCodeElement = [TBXML childElementNamed:@"postalCode" parentElement:RetailerElement];
            TBXMLElement *openCloseElement = [TBXML childElementNamed:@"locationOpen" parentElement:RetailerElement];
            TBXMLElement *completeAddressElement = [TBXML childElementNamed:@"completeAddress" parentElement:RetailerElement];
            
            //@Deepak: Main Menu and retListID parsed for user tracking
            TBXMLElement *retListIDElement = [TBXML childElementNamed:KEY_RLISTID parentElement:RetailerElement];
            //            if(retListIDElement != nil)
            //            {
            //                [retListIDArray addObject:[TBXML textForElement:retListIDElement]];
            //            }
            //
            //            if (rowNumberElement != nil) {
            //
            //                [rowNumberArray addObject:[TBXML textForElement:rowNumberElement]];
            //            }
            //
            //            if (retailerIdElement != nil) {
            //                [retailerIdArray addObject:[TBXML textForElement:retailerIdElement]];
            //            }
            //
            //            if (retailerNameElement != nil) {
            //                [retailerNameArray addObject:[TBXML textForElement:retailerNameElement]];
            //            }
            //
            //            if (retailLocationIDElement != nil) {
            //
            //                [retailLocationIDArray addObject:[TBXML textForElement:retailLocationIDElement]];
            //            }
            //
            //            if (logoImagePathElement != nil) {
            //                [logoImagePathArray addObject:[TBXML textForElement:logoImagePathElement]];
            //            }
            //
            //
            //            if (distanceElement != nil) {
            //                [distanceArray addObject:[TBXML textForElement:distanceElement]];
            //            }
            //
            //            if (retailAddressElement != nil) {
            //
            //                [retailAddressArray addObject:[TBXML textForElement:retailAddressElement]];
            //            }
            //
            //            if (bannerAdImagePathElement != nil) {
            //
            //                [bannerAdImagePathArray addObject:[TBXML textForElement:bannerAdImagePathElement ]];
            //            }
            //
            //            if (ribbonAdImagePathElement != nil) {
            //                [ribbonAdImagePathArray addObject:[TBXML textForElement:ribbonAdImagePathElement ]];
            //            }
            //
            //            if (ribbonAdURLElement != nil) {
            //                [ribbonAdURLArray addObject:[TBXML textForElement:ribbonAdURLElement ]];
            //            }
            //            if (latitudeElement != nil)
            //                [latArray addObject:[TBXML textForElement:latitudeElement]];
            //            if (longitudeElement != nil)
            //                [longArray addObject:[TBXML textForElement:longitudeElement]];
            //
            //            if (saleFlagElement != nil) {
            //                [saleFlagArray addObject:[TBXML textForElement:saleFlagElement]];
            //            }
            
            if(retListIDElement != nil)
            {
                filterRetailerObj.retListId = [TBXML textForElement:retListIDElement];
                [retListIDArray addObject:[TBXML textForElement:retListIDElement]];
            }
            
            if (rowNumberElement != nil) {
                
                filterRetailerObj.retRowNumber = [TBXML textForElement:rowNumberElement];
            }
            
            if (retailerIdElement != nil) {
                filterRetailerObj.retailerId = [TBXML textForElement:retailerIdElement];
                [retailerIdArray addObject:[TBXML textForElement:retailerIdElement]];
            }
            
            if (retailerNameElement != nil) {
                filterRetailerObj.retailerName = [TBXML textForElement:retailerNameElement];
                [retailerNameArray addObject:[TBXML textForElement:retailerNameElement]];
            }
            
            if (retailLocationIDElement != nil) {
                
                filterRetailerObj.retailLocationID = [TBXML textForElement:retailLocationIDElement];
                [retailLocationIDArray addObject:[TBXML textForElement:retailLocationIDElement]];
            }
            
            if (logoImagePathElement != nil) {
                filterRetailerObj.retLogoImgPath = [TBXML textForElement:logoImagePathElement];
            }
            
            
            if (distanceElement != nil) {
                filterRetailerObj.retDistance = [TBXML textForElement:distanceElement];
                [distanceArray addObject:[TBXML textForElement:distanceElement]];
            }
            
            if (retailAddress1Element != nil) {
                
                filterRetailerObj.retailAddress = [TBXML textForElement:retailAddress1Element];
                [retailAddressArray addObject:[TBXML textForElement:retailAddress1Element]];
            }
            if(retailAddress2Element != nil)
            {
                filterRetailerObj.retailAddress2 = [TBXML textForElement:retailAddress2Element];
                [retailAddress2Array addObject:[TBXML textForElement:retailAddress2Element]];
            }
            if(completeAddressElement != nil)
            {
                filterRetailerObj.retailCompleteAddress = [TBXML textForElement:completeAddressElement];
                [completeAddressArray addObject:[TBXML textForElement:completeAddressElement]];
            }
            if(postalCodeElement != nil)
            {
                filterRetailerObj.retailPostalCode = [TBXML textForElement:postalCodeElement];
                [retailerPostalCodeArray addObject:[TBXML textForElement:postalCodeElement]];
            }
            if(cityElement != nil)
            {
                filterRetailerObj.retailCity = [TBXML textForElement:cityElement];
                [retailerCityArray addObject:[TBXML textForElement:cityElement]];
            }
            if(stateElement != nil)
            {
                filterRetailerObj.retailState = [TBXML textForElement:stateElement];
                [retailerStateArray addObject:[TBXML textForElement:stateElement]];
            }
            
            if (bannerAdImagePathElement != nil) {
                
                filterRetailerObj.retbannerAdImgPath = [TBXML textForElement:bannerAdImagePathElement];
            }
            
            if (ribbonAdImagePathElement != nil) {
                filterRetailerObj.retRibbonAdImgPath = [TBXML textForElement:ribbonAdImagePathElement];
            }
            
            if (ribbonAdURLElement != nil) {
                filterRetailerObj.retRibbonAdUrl = [TBXML textForElement:ribbonAdURLElement];
            }
            if (latitudeElement != nil){
                filterRetailerObj.retLatitude = [TBXML textForElement:latitudeElement];
                [latArray addObject:[TBXML textForElement:latitudeElement]];
            }
            if (longitudeElement != nil){
                filterRetailerObj.retLongitude = [TBXML textForElement:longitudeElement];
                [longArray addObject:[TBXML textForElement:longitudeElement]];
            }
            
            if (saleFlagElement != nil) {
                filterRetailerObj.retSaleFlag = [TBXML textForElement:saleFlagElement];
            }
            if(openCloseElement != nil)
            {
                filterRetailerObj.retailOpenClose = [TBXML textForElement:openCloseElement];
            }
            [filterRetailersListObjArray addObject:filterRetailerObj];
            
            RetailerElement = [TBXML nextSiblingNamed:@"Retailer" searchFromElement:RetailerElement];
        }
        
        if(bottomBtn==1){
            bottomButtonList = [[NSMutableArray alloc]init];
            
            TBXMLElement *bottomList = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbXml.rootXMLElement];
            
            TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomList];
            
            while (BottomButtonElement != nil) {
                
                bottomButtonDO *bottomButton = [[bottomButtonDO alloc]init];
                
                TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                
                if(bottomBtnIDElement)
                    bottomButton.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                
                if(bottomBtnNameElement)
                    bottomButton.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                else
                    bottomButton.bottomBtnName = @" ";
                
                if(bottomBtnImgElement)
                    bottomButton.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                else
                    bottomButton.bottomBtnImg = @" ";
                
                if(bottomBtnImgOffElement)
                    bottomButton.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                else
                    bottomButton.bottomBtnImgOff = @" ";
                
                if(btnLinkTypeIDElement)
                    bottomButton.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                
                if(btnLinkTypeNameElement)
                    bottomButton.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                
                if(btnLinkIDElement){
                    bottomButton.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                    
                }
                
                if(positionElement)
                    bottomButton.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                
                
                
                BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                [bottomButtonList addObject:bottomButton];
                //[bottomButton release];
            }
        }
        
        if ([bottomButtonList count]>0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setBottomBarMenu:bottomButtonList];
            });
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self settableViewOnScreen];
        });
        //    [partnersTable reloadData];
        //[self performSelector:@selector(reloading) withObject:nil afterDelay:0.2];
        
        
    }
    
    
    else if ([[TBXML textForElement:responseCodeElement]isEqualToString:@"10005"])
    {
        
        
        //@Deepak: Parsed MainMenuID for userTracking
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbXml.rootXMLElement];
        bottomBtn = [[TBXML textForElement:[TBXML childElementNamed:@"bottomBtn" parentElement:tbXml.rootXMLElement]]intValue];
        if(mainMenuIdElement !=nil)
        {
            [defaults setObject:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        //added by ashika
        TBXMLElement *sDescription = [TBXML childElementNamed:@"sDescription" parentElement:tbXml.rootXMLElement];
        if(sDescription != nil){
            
            filterDescription = YES;
            filterDesc = [TBXML textForElement:sDescription];
        }
        
        if(bottomBtn==1){
            
            TBXMLElement *bottomButtonListElement = [TBXML childElementNamed:@"bottomBtnList" parentElement:tbXml.rootXMLElement];
            
            if(bottomButtonListElement !=nil)
            {
                TBXMLElement *BottomButtonElement = [TBXML childElementNamed:@"BottomButton" parentElement:bottomButtonListElement];
                bottomButtonList=[[NSMutableArray alloc]init];
                while(BottomButtonElement)
                {
                    
                    bottomButtonDO *obj_findBottomDO = [[bottomButtonDO alloc]init];
                    
                    
                    TBXMLElement *bottomBtnIDElement = [TBXML childElementNamed:@"bottomBtnID" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnNameElement = [TBXML childElementNamed:@"bottomBtnName" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnImgElement = [TBXML childElementNamed:@"bottomBtnImg" parentElement:BottomButtonElement];
                    TBXMLElement *bottomBtnImgOffElement = [TBXML childElementNamed:@"bottomBtnImgOff" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkTypeIDElement = [TBXML childElementNamed:@"btnLinkTypeID" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkTypeNameElement = [TBXML childElementNamed:@"btnLinkTypeName" parentElement:BottomButtonElement];
                    TBXMLElement *btnLinkIDElement = [TBXML childElementNamed:@"btnLinkID" parentElement:BottomButtonElement];
                    TBXMLElement *positionElement = [TBXML childElementNamed:@"position" parentElement:BottomButtonElement];
                    
                    
                    if(bottomBtnIDElement)
                        obj_findBottomDO.bottomBtnID = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnIDElement]];
                    
                    if(bottomBtnNameElement)
                        obj_findBottomDO.bottomBtnName = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnNameElement]];
                    else
                        obj_findBottomDO.bottomBtnName = @" ";
                    
                    if(bottomBtnImgElement)
                        obj_findBottomDO.bottomBtnImg = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgElement]];
                    else
                        obj_findBottomDO.bottomBtnImg = @" ";
                    
                    if(bottomBtnImgOffElement)
                        obj_findBottomDO.bottomBtnImgOff = [[NSString alloc]initWithString:[TBXML textForElement:bottomBtnImgOffElement]];
                    else
                        obj_findBottomDO.bottomBtnImgOff = @" ";
                    
                    if(btnLinkTypeIDElement)
                        obj_findBottomDO.btnLinkTypeID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeIDElement]];
                    
                    if(btnLinkTypeNameElement)
                        obj_findBottomDO.btnLinkTypeName = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkTypeNameElement]];
                    
                    if(btnLinkIDElement){
                        obj_findBottomDO.btnLinkID = [[NSString alloc]initWithString:[TBXML textForElement:btnLinkIDElement]];
                        if(btnLinkTypeNameElement){
                            if ([[TBXML textForElement:btnLinkTypeNameElement]isEqualToString:@"Filters"]) //for filters
                                [SharedManager setRetGroupId:[[TBXML textForElement:btnLinkIDElement]intValue]];
                        }
                    }
                    
                    if(positionElement)
                        obj_findBottomDO.position = [[NSString alloc]initWithString:[TBXML textForElement:positionElement]];
                    
                    
                    
                    BottomButtonElement = [TBXML nextSiblingNamed:@"BottomButton" searchFromElement:BottomButtonElement];
                    [bottomButtonList addObject:obj_findBottomDO];
                    //[obj_findBottomDO release];
                }
                if([bottomButtonList count] > 0)
                {
                    // Set Bottom Menu Button after read from response data
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self setBottomBarMenu:bottomButtonList];
                    });
                    [defaults setBool:YES forKey:@"isBottomButtonAvailable"];
                }
                else
                    [defaults setBool:NO forKey:@"isBottomButtonAvailable"];
            }
        }
        
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        
        NSString *response=[TBXML textForElement:saveResponseText];
        if([response isEqualToString:@"No Records Found."])
        {
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:response];
        }
        else{
            NSString *responseTextStr = [[TBXML textForElement:saveResponseText] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
            [UtilityManager showFormatedAlert:responseTextStr];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self settableViewOnScreen];
        });
    }
    
    
    
    else
    {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        NSString *responseTextStr;
        if([TBXML textForElement:saveResponseText].length)
        {
            responseTextStr = [TBXML textForElement:saveResponseText];
        }
        else
        {
            responseTextStr = @"No Records found";
        }
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        // [self showFormatedAlert:responseTextStr];
        [loading stopAnimating];
        ReleaseAndNilify(loading);
        
        
    }
    //    [self performSelector:@selector(reloading) withObject:nil afterDelay:0.2];
    dispatch_async(dispatch_get_main_queue(), ^{
        if(![defaults boolForKey:@"ViewMore"])
        {
            [partnersTable setContentOffset:CGPointZero animated:YES];
            
            [partnersTable reloadData];
            [HubCitiAppDelegate removeActivityIndicator];
        }
        else
        {
            [partnersTable reloadData];
            [HubCitiAppDelegate removeActivityIndicator];
        }
        
        [defaults setBool:NO forKey:@"ViewMore"];
        
        [loading stopAnimating];
    });
    ReleaseAndNilify(loading);
    
*/
    
    
}



-(void)reloading
{
    [partnersTable reloadData];
    
}





-(void)parse_appsitedetails:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc1 = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc1.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
        //  [rsvc1 release];
    }
}

-(void)allocateArrays{
    
    retListIDArray = [[NSMutableArray alloc]init];
    latArray = [[NSMutableArray alloc]init];
    longArray = [[NSMutableArray alloc]init];
    saleFlagArray = [[NSMutableArray alloc]init];
    annArray = [[NSMutableArray alloc]init];
    rowNumberArray = [[NSMutableArray alloc]init];
    retailerIdArray = [[NSMutableArray alloc]init];
    retailerNameArray = [[NSMutableArray alloc]init];
    retailLocationIDArray = [[NSMutableArray alloc]init];
    logoImagePathArray = [[NSMutableArray alloc]init];
    distanceArray = [[NSMutableArray alloc]init];
    retailAddressArray = [[NSMutableArray alloc]init];
    retailAddress2Array = [[NSMutableArray alloc]init];
    retailerStateArray = [[NSMutableArray alloc]init];
    retailerCityArray = [[NSMutableArray alloc]init];
    completeAddressArray = [[NSMutableArray alloc]init];
    retailerPostalCodeArray = [[NSMutableArray alloc]init];
    bannerAdImagePathArray = [[NSMutableArray alloc]init];
    ribbonAdImagePathArray = [[NSMutableArray alloc]init];
    ribbonAdURLArray = [[NSMutableArray alloc]init];
    if(filterRetailersListObjArray)
    {
        [filterRetailersListObjArray removeAllObjects];
        filterRetailersListObjArray = nil;
    }
    filterRetailersListObjArray = [[NSMutableArray alloc]init];
    
}

-(void)releaseArrays{
    ReleaseAndNilify(saleFlagArray);
    ReleaseAndNilify(retListIDArray);
    ReleaseAndNilify(rowNumberArray);
    ReleaseAndNilify(retailerIdArray);
    ReleaseAndNilify(retailerNameArray);
    ReleaseAndNilify(retailLocationIDArray);
    ReleaseAndNilify(logoImagePathArray);
    ReleaseAndNilify(distanceArray);
    ReleaseAndNilify(retailAddressArray);
    ReleaseAndNilify(retailAddress2Array);
    ReleaseAndNilify(retailerCityArray);
    ReleaseAndNilify(retailerStateArray);
    ReleaseAndNilify(retailerPostalCodeArray);
    ReleaseAndNilify(bannerAdImagePathArray);
    ReleaseAndNilify(ribbonAdImagePathArray);
    ReleaseAndNilify(ribbonAdURLArray);
    if (filterRetailersListObjArray)
        ReleaseAndNilify(filterRetailersListObjArray);
}





#pragma mark parse methods
-(void)parse_RetSummary:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else {
        
        [defaults setObject:response forKey:KEY_RESPONSEXML] ;
        [self.navigationController pushViewController:rsvc animated:NO];
        //[rsvc release];
    }
}



-(void)parse_HubcitiAnythingInfo:(NSString*)response{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] != 10000) {
        
        TBXMLElement *responseText = [TBXML childElementNamed:RESPONSETEXT
                                                parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseText]];
        return;
    }
    
    else {
        TBXMLElement *mediaPathElement = [TBXML childElementNamed:@"mediaPath" parentElement:tbxml.rootXMLElement];
        TBXMLElement *pageLinkElement = [TBXML childElementNamed:@"pageLink" parentElement:tbxml.rootXMLElement];
        TBXMLElement *mainMenuIdElement = [TBXML childElementNamed:@"mainMenuId" parentElement:tbxml.rootXMLElement];
        
        if (mainMenuIdElement!=nil && ![[TBXML textForElement:mainMenuIdElement]isEqualToString:@"N/A"]) {
            [defaults  setValue:[TBXML textForElement:mainMenuIdElement] forKey:KEY_MAINMENUID];
        }
        
        if (![[TBXML textForElement: pageLinkElement] isEqualToString:@"N/A"]){//means its of type website
            NSURL *url = [NSURL URLWithString:(NSString*)[TBXML textForElement: pageLinkElement]];
            // NSString *name =[NSString stringWithFormat:@"%@",[url.pathComponents objectAtIndex:3]];
            [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                // [defaults setValue:url forKey:KEY_URL];
                
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [alert dismissViewControllerAnimated:NO completion:nil];
                                               [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
            }
            
            else{
                [defaults setValue:[TBXML textForElement: pageLinkElement] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:urlDetail animated:NO];
                //[urlDetail release];
            }
        }
        
        
        
        else if ([[TBXML textForElement: mediaPathElement] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:response forKey:KEY_RESPONSEXML];
            
            AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:anythingPage animated:NO];
            //[anythingPage release];
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[TBXML textForElement: mediaPathElement] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:urlDetail animated:NO];
            //[urlDetail release];
        }
    }
    [linkID removeLastObject];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)requestToGetUserData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    DLog(@"parameter: %@",parameters);
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
    //    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    //    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    //    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    //    ReleaseAndNilify(xmlStr);
}
-(void)parseGetUserData:(id)response
{
    
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}
-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }
}
-(void) customizedSegmentController{
    
    
    segment=[[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Distance",@"Name",@"Map", nil]];
    
    [segment setSelectedSegmentIndex:0];
    [segment setBackgroundColor:[UIColor grayColor]];
    [segment setSegmentedControlStyle:UISegmentedControlStylePlain];
    [segment setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:0];
    [segment setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:1];
    [segment setWidth:((SCREEN_WIDTH/3)+1) forSegmentAtIndex:2];
    
    [self setSegmentColor:segment]; //add customized color
    
    segment.translatesAutoresizingMaskIntoConstraints = NO;
    [segment addTarget:self action:@selector(viewSelectionChanged:) forControlEvents:UIControlEventValueChanged];
    
    
    
    // inset for table view
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad)
    {
        [partnersTable setContentInset:UIEdgeInsetsMake(0, 0, VARIABLE_HEIGHT(25), 0)];
    }
    else
    {
        [partnersTable setContentInset:UIEdgeInsetsMake(0, 0, VARIABLE_HEIGHT(40), 0)];
    }
    
    viewDictionary = NSDictionaryOfVariableBindings(segment);
    
    [self.view addSubview:segment];
    //set constraits for segment controller
    
    // Vertical constrains
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(-2)-[segment(48)]"] options:0 metrics:0 views:viewDictionary]];
    
    
    // Horizontal constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(-2)-[segment(%f)]",SCREEN_WIDTH+4] options:0 metrics:0 views:viewDictionary]];
    
    
    
    
    //    int j=0;
    //add image to segment controller
    //    for (CALayer *layer in [segment.layer sublayers])
    //    {
    //        j++;
    //        long index=[[segment.layer sublayers] indexOfObject:layer];
    //        NSLog(@"segment: %ld",index);
    //        UIImage *image;
    //
    //            if(j==1){
    //                image=[UIImage imageNamed:@"band1.png"];
    //
    //            }
    //            else if(j==2){
    //                image=[UIImage imageNamed:@"events1.png"];
    //
    //            }
    //
    //
    //        UIImageView *imageView;
    //
    //        if(([[layer.sublayers.lastObject name] isEqualToString:@"Event"] || [[layer.sublayers.lastObject name] isEqualToString:@"Band"]))
    //
    //        {
    //            [layer.sublayers.lastObject removeFromSuperlayer];
    //
    //        }
    //
    //
    //        if (  DEVICE_TYPE==UIUserInterfaceIdiomPad )
    //        {
    //            if(j==1){
    //            imageView=[[UIImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(50),VARIABLE_HEIGHT(7), 25, 25)];
    //            }
    //            else{
    //                imageView=[[UIImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(35),VARIABLE_HEIGHT(7), 25, 25)];
    //            }
    //        }
    //        else{
    //            if(j==1){
    //            imageView=[[UIImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(30),VARIABLE_HEIGHT(12), 20, 20)];
    //            }
    //            else{
    //                imageView=[[UIImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(7),VARIABLE_HEIGHT(12), 20, 20)];
    //            }
    //        }
    //
    //
    //
    //        imageView.image=image;
    //        imageView.layer.anchorPoint=layer.anchorPoint;
    //        imageView.backgroundColor=[UIColor clearColor];
    //        if(j==1)
    //            imageView.layer.name=@"Band";
    //        else
    //            imageView.layer.name=@"Event";
    //        [layer insertSublayer:imageView.layer atIndex:(unsigned int)layer.sublayers.count];
    //
    //
    //    }
    
}
-(void) setSegmentColor:(UISegmentedControl *) segmentSender
{
    
    for (int i=0; i<[segmentSender.subviews count]; i++)
    {
        if ([[segmentSender.subviews objectAtIndex:i]isSelected] )
        {
            UIColor *tintcolor=[UIColor colorWithRed:80/255.0 green:199/255.0 blue:228/255.0 alpha:1.0];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
            
            //break;
        }
        else {
            UIColor *tintcolor=[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
        }
    }
    for (int i=0; i<[segmentSender.subviews count]; i++)
    {
        if (![[segmentSender.subviews objectAtIndex:i]isSelected] )
        {
            UIColor *tintcolor=[UIColor darkGrayColor];
            [[segmentSender.subviews objectAtIndex:i] setTintColor:tintcolor];
            // break;
        }
        
    }
    
    float segmentFont = 14.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        segmentFont = 18.0;
    }
    NSDictionary *attributes = @{
                                 NSFontAttributeName : [UIFont boldSystemFontOfSize:segmentFont],
                                 NSForegroundColorAttributeName : [UIColor blackColor]};
    [segmentSender setTitleTextAttributes:attributes forState:UIControlStateSelected];
    
    NSDictionary *attributes2 = @{
                                  NSFontAttributeName : [UIFont boldSystemFontOfSize:segmentFont],
                                  NSForegroundColorAttributeName : [UIColor whiteColor]};
    [segmentSender setTitleTextAttributes:attributes2 forState:UIControlStateNormal];

    
}
-(void) refreshData
{
    if(filterRetailersListObjArray)
    {
        [filterRetailersListObjArray removeAllObjects];
        
    }
    [self filtersOptionsReleaseArrays];
    if(retailerIdArray)
    {
        [retailerIdArray removeAllObjects];
    }
    
    if(retailerNameArray)
    {
        [retailerNameArray removeAllObjects];
    }
    if(retListIDArray)
    {
        [retListIDArray removeAllObjects];
    }
    if(retailAddressArray)
    {
        [retailAddressArray removeAllObjects];
    }
    if(latArray)
    {
        [latArray removeAllObjects];
    }
    if(longArray)
    {
        [longArray removeAllObjects];
    }
    if(retailLocationIDArray)
    {
        [retailLocationIDArray removeAllObjects];
    }
    if(completeAddressArray)
    {
        [completeAddressArray removeAllObjects];
    }
    
   
}

-(void)viewSelectionChanged:(id)sender
{
    
    switch ([segment selectedSegmentIndex])    {
            
        case 0:
        {
            
            _oldSegmentedIndex = 0;
            [defaults setBool:NO forKey:@"ViewMore"];
           
            [self setSegmentColor:segment];
          
            sortFilObject.distanceSelected = YES;
            sortFilObject.alphabeticallySelected = NO;
            
            [self performSelector:@selector(nextRequestPartner:) withObject:sortFilObject afterDelay:0.1];
           
        }
            break;
        case 1:
        {
            _oldSegmentedIndex = 1;
            [defaults setBool:NO forKey:@"ViewMore"];
            [self setSegmentColor:segment];
            sortFilObject.alphabeticallySelected = YES;
            sortFilObject.distanceSelected = NO;
           
            [self performSelector:@selector(nextRequestPartner:) withObject:sortFilObject afterDelay:0.1];
            
            
        }
            break;
        case 2:{
           // [[HTTPClient sharedHTTPClient].manager.operationQueue cancelAllOperations];
           // ReleaseAndNilify(partnersTable);
            fromFilters=TRUE;
            fromFind=FALSE;
            fromNearBy=FALSE;
            //added
            if ([RegionApp isEqualToString:@"1"]){
                fromRegionFind = FALSE;//from region app
            }
            
            if ([latArray count] == 0 || [longArray count] == 0) {
                [self filtersOptionsReleaseArrays];
            }
            [self filtersOptionsReleaseArrays];
            
            for (int i=0; i<[latArray count]; i++) {
                
                [globalLatArray addObject:[latArray objectAtIndex:i]];
            }
            for (int j =0 ; j<[longArray count]; j++) {
                
                [globalLongArray addObject:[longArray objectAtIndex:j]];
            }
            
            for (int i=0; i<[retailerNameArray count]; i++) {
                
                [globalNameArray addObject:[retailerNameArray objectAtIndex:i]];
            }
            for (int j=0; j<[retailAddressArray count]; j++) {
                
                [globalAddressArray addObject:[retailAddressArray objectAtIndex:j]];
            }
            for (int j=0; j<[retListIDArray count]; j++) {
                
                [globalRetailerListID addObject:[retListIDArray objectAtIndex:j]];
            }
            for (int j=0; j<[retailerIdArray count]; j++) {
                
                [globalRetailerID addObject:[retailerIdArray objectAtIndex:j]];
            }
            for (int j=0; j<[retailLocationIDArray count]; j++) {
                
                [globalRetLocId addObject:[retailLocationIDArray objectAtIndex:j]];
            }
            for (int j=0; j<[completeAddressArray count]; j++) {
                
                [globalCompleteAddress addObject:[completeAddressArray objectAtIndex:j]];
            }
            
            if ([globalLatArray  count] > 0) {
                
                showMapFlag=TRUE;
                
                FilterRetailersList *iFindRetailersList = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:iFindRetailersList animated:NO];
                
                
            }
            else{
                [segment setSelectedSegmentIndex:_oldSegmentedIndex];
                [self setSegmentColor:segment];
                [UtilityManager showAlert:nil msg:NSLocalizedString(@"Retailers are not available to plot on map",@"Retailers are not available to plot on map")];
            }
            
            
        }
            
    }
    
}

-(void)nextRequestPartner:(SortAndFilter*)sortFilterObj
{
    [defaults setBool:NO forKey:@"ViewMore"];
    [self request_PartnerRet:0 withObj:sortFilObject];
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    for (NSOperation *operation in [HTTPClient sharedHTTPClient].manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        NSLog(@"operation:%@",operation);
        AFHTTPRequestOperation *operations = (AFHTTPRequestOperation*)operation;
        if([[[[operations request] URL] absoluteString] containsString:@"thislocation/partnerretjson"]){
            [operation cancel];
        }
        
    }
    
    //   [[HTTPClient sharedHTTPClient].manager.operationQueue cancelAllOperations];
    
}

@end

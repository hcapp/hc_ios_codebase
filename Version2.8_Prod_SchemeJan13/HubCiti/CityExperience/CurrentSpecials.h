//
//  CurrentSpecials.h
//  Scansee
//
//  Created by ramanan on 19/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
BOOL fromCurrentSpecials;
typedef enum webServicesSpecialOffer
{
    DEFAULT,
    RETSPEDEALOFF,
    GETSPECIALOFFERLIST,
    GETRETHOTDEALS,
    RETLOCCOUP,
    GETPRODUCTS,
    HD_CLAIM,
    HD_EXPIRED,
    HD_USED
}webServicesSpecialOfferState;

@interface CurrentSpecials : UIViewController<UITableViewDelegate, UITableViewDataSource,CustomizedNavControllerDelegate>
{
    webServicesSpecialOfferState iWebRequestState;
    NSMutableArray *couponListArray;
    BOOL couponFlag;
    int NextPageFlagCoupons;
}

@property(assign,readwrite)BOOL couponFlag;

@property (strong, nonatomic) IBOutlet UITableView *tableForCoupons;
@end

//
//  CurrentSpecialsTableViewCell.h
//  HubCiti
//
//  Created by Nikitha on 1/20/17.
//  Copyright © 2017 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrentSpecialsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *couponNameLabel;
@property (strong, nonatomic) IBOutlet UIView *featuredImageView;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
-(void) setDetailData : (NSString *) couponName description : (NSString *) couponDescription CouponImage : (NSString * ) couponImage FeaturedOrNot : (NSString *) featuredFlag;
@property (strong, nonatomic) IBOutlet SdImageView *imageViewForSpecials;

@property (strong, nonatomic) IBOutlet SdImageView *featuredImage;

@end

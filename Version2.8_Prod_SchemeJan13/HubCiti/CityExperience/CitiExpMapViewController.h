//
//  CitiExpMapViewController.h
//  HubCiti
//
//  Created by Keerthi on 03/06/14.
//  Copyright (c) 2014 span infotech india ptv. ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MKMapView.h>


@class DisplayMap;
@interface CitiExpMapViewController : UIViewController<MKMapViewDelegate,CustomizedNavControllerDelegate>{
     IBOutlet MKMapView *mapView;
    WebRequestState iWebRequestState;
    NSMutableArray *latArray;
    NSMutableArray *longArray;
    NSMutableArray *retailerNameArr;
    NSMutableArray *retailAddressArr;
    NSMutableArray *retailLocationIdArray;
    NSMutableArray *retailerIdArray ;
    NSMutableArray *retListIdArray;
    NSMutableArray *distanceCEArray;
    NSString *latitude,*longitude,*retailerName,*retailAddress,*retailLocationId,*retailerId,*retListId,*distanceCE;
    int selectedindexRow;
}

@property(nonatomic,strong)NSMutableArray * cityExpObjArray;
@property(nonatomic,strong)NSString *latitude,*longitude,*retailerName,*retailAddress,*retailLocationId,*retailerId,*retListId,*distanceCE;
@property(nonatomic,strong)NSMutableArray *latArray;
@property(nonatomic,strong)NSMutableArray *longArray;
@property(nonatomic,strong)NSMutableArray *retailerNameArr;
@property(nonatomic,strong)NSMutableArray *retailAddressArr;
@property(nonatomic,strong)NSMutableArray *retailLocationIdArray;
@property(nonatomic,strong)NSMutableArray *retailerIdArray;
@property(nonatomic,strong)NSMutableArray *retListIdArray;
@property(nonatomic,strong)NSMutableArray *distanceCEArray;

@end

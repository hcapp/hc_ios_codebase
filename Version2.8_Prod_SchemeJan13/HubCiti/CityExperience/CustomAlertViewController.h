//
//  CustomAlertViewController.h
//  HubCiti
//
//  Created by Nikitha on 8/19/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertViewController : UIViewController
@property(nonatomic,strong) UINavigationController * navVC;
@property(nonatomic,strong) NSString * msg;
@property(nonatomic,strong) NSArray * appsiteNames;
@property(nonatomic,strong) NSArray * appsiteURL;
@end

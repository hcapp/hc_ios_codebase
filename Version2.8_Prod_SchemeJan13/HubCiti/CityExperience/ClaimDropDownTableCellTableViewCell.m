//
//  ClaimDropDownTableCellTableViewCell.m
//  HubCiti
//
//  Created by Nikitha on 12/23/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "ClaimDropDownTableCellTableViewCell.h"

@implementation ClaimDropDownTableCellTableViewCell
@synthesize tableViewLabelOutlet,checkImageOutlet;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setDataToCell:(NSString*)titleOfCell  typeofBusiness:(NSArray*)typeOFBusinessArray   categories:(NSString*)completeArrayOfCategories
{
    checkImageOutlet.image =nil;
    tableViewLabelOutlet.text = titleOfCell;
    tableViewLabelOutlet.font = [UIFont systemFontOfSize:(IPAD?17:11)];
    for(int i= 0;i<typeOFBusinessArray.count;i++)
    {
        if ([completeArrayOfCategories isEqualToString:typeOFBusinessArray[i]])
        {
            checkImageOutlet.image = [UIImage imageNamed:@"icon_tick.png"];
            
        }
    }
}
@end

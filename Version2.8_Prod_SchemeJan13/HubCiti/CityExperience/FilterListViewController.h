//
//  FilterListViewController.h
//  HubCiti
//
//  Created by Keerthi on 10/07/14.
//  Copyright (c) 2014 span infotech india ptv. ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"
#import "CityExperienceDO.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
@class DealHotDealsList;
@class FilterRetailersList;
@class RetailersListViewController;
@class CityExperienceViewController;
@class EventsListViewController;
@class FundraiserListViewController;
@class FAQCategoryList;
@class AnyViewController;
@class EmailShareViewController;
@interface FilterListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMessageComposeViewControllerDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>{
    UITableView *table_PartnerRetailerList;
    UILabel *titleLabel;
    int maxCount;
    UIBarButtonItem *opt;
    WebRequestState iWebRequestState;
    CEPartnerListDO *partnerListDO;
    CommonUtility *common;
    int PLnextPage;
    NSMutableArray *PLretailerIdArray,*PLretailerNameArray,*PLretailerLogoArray;
    int PLlastVisitedRecord,bottomBtn;
    NSMutableArray *bottomButtonList;
    CustomizedNavController *cusNav;
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,retain) FAQCategoryList *faqList;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,retain) CityExperienceViewController *cityExp;
@property(nonatomic,retain) RetailersListViewController* retailerListScreen;
@property(nonatomic,retain) DealHotDealsList *hotDeals;
@property(nonatomic,retain) FilterRetailersList *filters;
@property(nonatomic,retain) EventsListViewController* iEventsListViewController;
@property(nonatomic,retain) FundraiserListViewController *iFundraiserListViewController;
@end

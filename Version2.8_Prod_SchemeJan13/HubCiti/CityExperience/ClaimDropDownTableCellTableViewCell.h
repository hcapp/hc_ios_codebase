//
//  ClaimDropDownTableCellTableViewCell.h
//  HubCiti
//
//  Created by Nikitha on 12/23/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClaimDropDownTableCellTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *tableViewLabelOutlet;

@property (strong, nonatomic) IBOutlet UIImageView *checkImageOutlet;

- (void)setDataToCell:(NSString*)titleOfCell  typeofBusiness:(NSArray*)typeOFBusinessArray   categories:(NSString*)completeArrayOfCategories;
@end

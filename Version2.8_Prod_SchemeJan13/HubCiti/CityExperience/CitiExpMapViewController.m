//
//  CitiExpMapViewController.m
//  HubCiti
//
//  Created by Keerthi on 03/06/14.
//  Copyright (c) 2014 span infotech india ptv. ltd. All rights reserved.
//

#import "CitiExpMapViewController.h"
#import "RetailerAnnotation.h"
#import "CityExperienceViewController.h"
#import "RetailersListViewController.h"
#import "LocationDetailsViewController.h"
#import "AnyViewController.h"
#import "EmailShareViewController.h"
#import "CommonUtility.h"
#import "FilterRetailersList.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"


@interface CitiExpMapViewController ()

@end

@implementation CitiExpMapViewController
@synthesize latitude,longitude,retailerName,retailAddress,retailerId,retailLocationId,retListId,distanceCE;
@synthesize longArray,latArray,retailAddressArr,retailerNameArr,retailerIdArray,retailLocationIdArray,distanceCEArray,retListIdArray;
NSMutableArray *annArray;

@synthesize cityExpObjArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setAccessibilityLabel:@"CityMap"];
    // Do any additional setup after loading the view from its nib.
    //customize back button
       self.navigationItem.hidesBackButton = YES;
    
    
    //home button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
    
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;

    ////[back release];

//    DLog(@"%d",cityExpObjArray.count);
    
//    [btn3 addTarget:self action:@selector(cityExperienceClicked:) forControlEvents:UIControlEventTouchUpInside];
//    btn3.tag = 3;
//    
//    switch ([SharedManager filtersCount])
//    {
//        case 0:
//        {
//            [btn3 setBackgroundImage:[UIImage imageNamed:@"tab_account-up.png"] forState:UIControlStateNormal];
//            [btn3 setBackgroundImage:[UIImage imageNamed:@"tab_account-down.png"] forState:UIControlStateHighlighted];
//            [btn3 removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
//            [btn3 addTarget:self action:@selector(request_GetUserInfo:) forControlEvents:UIControlEventTouchUpInside];
//            
//            break;
//        }
//        default:
//        {
//            [btn3 setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_filters.png"] forState:UIControlStateNormal];
//            [btn3 setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_filters.png"] forState:UIControlStateHighlighted];
//            [btn3 removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
//            [btn3 addTarget:self action:@selector(filtersClicked:) forControlEvents:UIControlEventTouchUpInside];
//            
//            break;
//        }
//    }
//    
//    [btn1 setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_find.png"] forState:UIControlStateNormal];
//    [btn1 setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_find.png"] forState:UIControlStateHighlighted];
//    [btn1 addTarget:self action:@selector(navigateToFindView) forControlEvents:UIControlEventTouchUpInside];
//    btn1.tag = 1;
//    
//    [btn2 setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_nearBy.png"] forState:UIControlStateNormal];
//    [btn2 setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_nearBy.png"] forState:UIControlStateHighlighted];
//    [btn2 addTarget:self action:@selector(navigateToWhatsNearBy) forControlEvents:UIControlEventTouchUpInside];
//    btn2.tag = 2;
//    
//    [btn4 setBackgroundImage:[UIImage imageNamed:@"tab_btn_up_share.png"] forState:UIControlStateNormal];
//    [btn4 setBackgroundImage:[UIImage imageNamed:@"tab_btn_down_share.png"] forState:UIControlStateHighlighted];
//    [btn4 addTarget:self action:@selector(shareClicked:) forControlEvents:UIControlEventTouchUpInside];
//    btn4.tag = 4;
//
//
}
-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}



-(void)viewWillAppear:(BOOL)animated{
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [super viewWillAppear:animated];
  
    [mapView removeAnnotations:annArray];
    [self setUpMapView];
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 200, 40)];
//    titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleLabel.textColor = [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]];
//    titleLabel.backgroundColor = [UIColor clearColor];
//    titleLabel.numberOfLines = 2;
//    [titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
//    titleLabel.font = [UIFont boldSystemFontOfSize:16];
//    titleLabel.text = NSLocalizedString(@"Choose Your Location:",@"Choose Your Location:");
    self.navigationItem.title = @"Choose Your Location:";
   // [titleLabel release];
}
-(void)allocateArrays{
    
    if(annArray)
    {
        [annArray removeAllObjects];
        annArray = nil;
    }
    annArray = [[NSMutableArray alloc] init];
}


#pragma mark navigation bar button actions
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)setUpMapView{
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    [mapView setDelegate:self];
    
   // CLLocationCoordinate2D southWest;
    //CLLocationCoordinate2D northEast;
    //check is reqd as in case only scansee data is there and no gooPLe data it would crash
    if ([latArray count]){
        
//        southWest = CLLocationCoordinate2DMake([[latArray objectAtIndex:0]doubleValue],
//                                               [[longArray objectAtIndex:0]doubleValue]);
       // northEast = southWest;
        
        for(int i=0; i<[latArray count]; i++)
        {
            RetailerAnnotation *ann = [[RetailerAnnotation alloc] init];
            ann.coordinate = CLLocationCoordinate2DMake([[latArray objectAtIndex:i] doubleValue],
                                                        [[longArray objectAtIndex:i] doubleValue]);
            [mapView addAnnotation:ann];
            [annArray addObject:ann];
            [ann setTitle:[retailerNameArr objectAtIndex:i] ];
            [ann setSubtitle:[retailAddressArr objectAtIndex:i]];
            ReleaseAndNilify(ann);
        }
        
        MKMapRect zoomRect = MKMapRectNull;
        for (id <MKAnnotation> annotation in mapView.annotations)
        {
            MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
            MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
            zoomRect = MKMapRectUnion(zoomRect, pointRect);
        }
        [mapView setVisibleMapRect:zoomRect animated:YES];
    }
    else{//if cityExpObjArray count = 0
        return;
    }
}


#pragma mark MapView delegate methods
-(void)alertUser
{
    [UtilityManager showAlert:nil msg:@"Please signup to use share. Visit home page to signup"];
}


-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
	MKPinAnnotationView* pinView=nil;
	if ([annotation isKindOfClass:[RetailerAnnotation class]])
    {
        // try to dequeue an existing pin view first
        static NSString* RetailerAnnotationIdentifier = @"retailerAnnotationIdentifier";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:RetailerAnnotationIdentifier];
        
        if (!pinView)
        {
            // if an existing pin view was not available, create one
            MKPinAnnotationView* customPinView = [[MKPinAnnotationView alloc]
												   initWithAnnotation:annotation reuseIdentifier:RetailerAnnotationIdentifier] ;
            //customPinView.pinColor = MKPinAnnotationColorPurple;
            customPinView.animatesDrop = YES;
            
            customPinView.canShowCallout = YES;
            UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            customPinView.rightCalloutAccessoryView = infoButton;
            return customPinView;
        }
        else
        {
            pinView.annotation = annotation;
        }
    }
	pinView.canShowCallout = YES;
	return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    for (int j = 0 ; j < [longArray count]; j++) {
        if ([[retailerNameArr objectAtIndex:j] isEqualToString:view.annotation.title]) {
            //[self getRetailerDetails:j];
            
            [defaults setValue:[retailLocationIdArray objectAtIndex:j] forKey:@"retailLocationID"];
            [defaults  setValue:[retailerIdArray objectAtIndex:j] forKey:KEY_RETAILERID];
            [linkID insertObject:@"0" atIndex:[linkID count]];
            [self request_retsummary:j];
            return;
        }
    }
}









-(void)request_retsummary:(int)arrayIndex
{
    selectedindexRow = arrayIndex;
    
    iWebRequestState = RETSUMMARY;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    if([latArray count] > arrayIndex){
        [requestStr appendFormat:@"<retailerId>%@</retailerId>",[retailerIdArray objectAtIndex:arrayIndex]];
        
        [requestStr appendFormat:@"<retailLocationId>%@</retailLocationId>",[retailLocationIdArray objectAtIndex:arrayIndex]];
        [requestStr appendFormat:@"<retListId>%@</retListId>",[retListIdArray objectAtIndex:arrayIndex]];
    }
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    
    [requestStr appendFormat:@"<scanTypeId>%@</scanTypeId>",@"0"];
    
    if([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    if([defaults valueForKey:KEY_HUBCITIID])
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId></RetailerDetail>",[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/retsummary",BASE_URL];
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
   // //[requestStr release];
}




////in case of only 1 partner
//-(void)request_PartnerRet{
//    
//    iWebRequestState = PARTNERRET;
//    NSMutableString *requestStr = [[NSMutableString alloc] init];
//    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
//    
//    //For User Tracking
//    if ([defaults valueForKey:KEY_MAINMENUID])
//        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
//    
//    if(![defaults boolForKey:BottomButton]){
//        if ([defaults valueForKey:KEY_MITEMID])
//            [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
//    }
//    else {
//        
//        if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
//            [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
//    }
//    
//    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
//        
//        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
//         [defaults  valueForKey:KEY_LATITUDE]];
//        
//    }
//    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
//        
//        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
//    }
//    if ([defaults valueForKey:@"filtersSort"]==nil) {
//        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
//    }
//    else
//    {
//        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
//    }
//
//    
//    [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId><lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>0</catIds></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[SharedManager retAffId]];
//    
//    NSMutableString *urlString = [[BASE_URL mutableCopy] ;
//    [urlString appendString:@"thislocation/partnerret"];
//    
//    //iWebRequestState = GETRETFORPARTNER;
//    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
//    
//}
//
//
////in case of multiple partners
//-(void)request_GetPartners{
//    
//    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getpartners?citiExperId=%@",BASE_URL,[defaults valueForKey:KEY_LINKID]];
//    
//    iWebRequestState = GETPARTNERS;
//    [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
//    
//}


#pragma mark parse methods
-(void)responseData:(NSString *) response
{
    switch (iWebRequestState)
    {
        case RETSUMMARY:
            [self parse_RetSummary:response];
            break;
            
//        case PARTNERRET:
//            [self parse_PartnerRet:response];
//            break;
            
//        case GETPARTNERS:
//            [self parse_GetPartners:response];
            break;
            
        default:
            break;
    }
    
}


//parse retailer summary
-(void)parse_RetSummary:(NSString*)response
{
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
    else
    {
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        rsvc.distanceFromPreviousScreen = [distanceCEArray objectAtIndex:selectedindexRow];
        [SharedManager setRefreshAustinRetailers:NO];
        [self.navigationController pushViewController:rsvc animated:NO];
       // //[rsvc release];
    }
}



- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent"  msg:@"Please try later"];
        }
            break;
            
        default:
            break;
    }
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
	[self dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

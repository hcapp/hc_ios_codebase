
//
//  CategoriesViewController.m
//  Scansee
//
//  Created by Ajit on 2/21/13.
//
//

#import "CategoriesViewController.h"
#import "FilterRetailersList.h"
#import "UserInformationViewController.h"
#import "MainMenuViewController.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@implementation CategoryDO

@synthesize catId,catName,catImg;

@end

@interface CategoriesViewController ()

@end

@implementation CategoriesViewController
extern NSMutableArray *cityExpObjArray;
//extern NSMutableArray *rowNumberArray;
//extern NSMutableArray *retailerIdArray,*retailerNameArray,*retailLocationIDArray;
//extern NSMutableArray *logoImagePathArray,*distanceArray,*retailAddressArray,*bannerAdImagePathArray;
//extern NSMutableArray *ribbonAdImagePathArray,*ribbonAdURLArray;
extern NSMutableArray *annArray;

@synthesize retAffIdForCat, retGroupIdForCat,tagValue;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IOS7 == NO)
        searchBar1.tintColor = [UIColor blackColor];
    categoriesTable.tag = tagValue;
    DLog(@"%ld",(long)categoriesTable.tag);
    self.title = @"Category";
    
    //customize back button
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    
    [btn addSubview:homeImage];
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    
    ////[mainPage release];
    
    
    common = [[CommonUtility alloc]init];
    
    [cityExpBtn addTarget:self action:@selector(cityExpClicked) forControlEvents:UIControlEventTouchUpInside];
    cityExpBtn.showsTouchWhenHighlighted = NO;
    
    switch ([SharedManager filtersCount])
    {
        case 0:
        {
            [filtersBtn setBackgroundImage:[UIImage imageNamed:@"tab_account-up.png"] forState:UIControlStateNormal];
            [filtersBtn setBackgroundImage:[UIImage imageNamed:@"tab_account-down.png"] forState:UIControlStateHighlighted];
            [filtersBtn removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
            [filtersBtn addTarget:self action:@selector(request_GetUserInfo) forControlEvents:UIControlEventTouchUpInside];
            filtersBtn.showsTouchWhenHighlighted = NO;
            break;
        }
        default:
        {
            [filtersBtn setImage:[UIImage imageNamed:@"tab_btn_up_filters"] forState:UIControlStateNormal];
            [filtersBtn setImage:[UIImage imageNamed:@"tab_btn_down_filters"] forState:UIControlStateHighlighted];
            [filtersBtn addTarget:self action:@selector(filtersClicked) forControlEvents:UIControlEventTouchUpInside];
            filtersBtn.showsTouchWhenHighlighted = NO;
        }
            break;
    }
    
    
    [settingsBtn setImage:[UIImage imageNamed:@"tab_btn_up_settings"] forState:UIControlStateNormal];
    [settingsBtn setImage:[UIImage imageNamed:@"tab_btn_down_settings"] forState:UIControlStateHighlighted];
    [settingsBtn addTarget:self action:@selector(settingsClicked) forControlEvents:UIControlEventTouchUpInside];
    settingsBtn.showsTouchWhenHighlighted = NO;
    
    [aboutBtn setImage:[UIImage imageNamed:@"tab_btn_up_about"] forState:UIControlStateNormal];
    [aboutBtn setImage:[UIImage imageNamed:@"tab_btn_down_about"] forState:UIControlStateHighlighted];
    [aboutBtn addTarget:self action:@selector(aboutClicked) forControlEvents:UIControlEventTouchUpInside];
    aboutBtn.showsTouchWhenHighlighted = NO;
    
    catObjArray = [[NSMutableArray alloc]init];
    
    switch (categoriesTable.tag) {
        case 100:
            [self request_GetCatForGroup:[[defaults valueForKey:KEY_LINKID]intValue]];
            
            if([defaults valueForKey:@"retGroupImg"])
            {
                NSString *strNormalImage = [defaults valueForKey:@"retGroupImg"];
                UIImage *imgNormalImage = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strNormalImage]]];
                [cityExpBtn setBackgroundImage:imgNormalImage forState:UIControlStateNormal];
                // [imgNormalImage release];
                
                NSString *strHighlightImage = [defaults valueForKey:@"retGroupImg_On"];
                UIImage *highlightImage = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strHighlightImage]]];
                [cityExpBtn setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
                // [highlightImage release];
            }
            
            //[ausExpBtn setImage:[UIImage imageNamed:@"tab_btn_down_austinExp"] forState:UIControlStateNormal];
            switch ([SharedManager filtersCount])
        {
            case 0:
            {
                [filtersBtn setBackgroundImage:[UIImage imageNamed:@"tab_account-up.png"] forState:UIControlStateNormal];
                [filtersBtn setBackgroundImage:[UIImage imageNamed:@"tab_account-down.png"] forState:UIControlStateHighlighted];
                [filtersBtn removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
                [filtersBtn addTarget:self action:@selector(request_GetUserInfo) forControlEvents:UIControlEventTouchUpInside];
                break;
            }
            default:
            {
                [filtersBtn setImage:[UIImage imageNamed:@"tab_btn_up_filters"] forState:UIControlStateNormal];
                [filtersBtn setImage:[UIImage imageNamed:@"tab_btn_down_filters"] forState:UIControlStateHighlighted];
                [filtersBtn addTarget:self action:@selector(filtersClicked) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
        }
            break;
            
        case 200:{
            
            if([defaults valueForKey:@"retGroupImg"])
            {
                NSString *strNormalImage = [defaults valueForKey:@"retGroupImg"];
                UIImage *imgNormalImage = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strNormalImage]]];
                [cityExpBtn setBackgroundImage:imgNormalImage forState:UIControlStateNormal];
                //[imgNormalImage release];
                
                NSString *strHighlightImage = [defaults valueForKey:@"retGroupImg_On"];
                UIImage *highlightImage = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strHighlightImage]]];
                [cityExpBtn setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
                //[highlightImage release];
            }
            
            [self request_GetCatForPartner:retAffIdForCat];
            
            switch ([SharedManager filtersCount])
            {
                case 0:
                {
                    [filtersBtn setImage:[UIImage imageNamed:@"tab_account-down.png"] forState:UIControlStateNormal];
                }
                    break;
                    
                default:
                {
                    [filtersBtn setImage:[UIImage imageNamed:@"tab_btn_down_filters"] forState:UIControlStateNormal];
                }
                    break;
            }
        }
            break;
    }
    
    //[categoriesTable reloadData];
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];

}

#pragma mark navigationbar items actions
-(void)popBackToPreviousPage{
    [self.navigationController popViewControllerAnimated:NO];
}



-(void)returnToMainPage:(id)sender{
    
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    
    else
    {
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

#pragma mark search delegate methods
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    //searchBarTextDidBeginEditing
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    //searchBarTextDidEndEditing
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    //textDidChange
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![searchBar.text length])
        return;
    
    switch (categoriesTable.tag) {
        case 100:
            [self request_CitiExpRet:0 searchStr:searchBar.text];
            break;
            
        case 200:
            [self request_PartnerRet:retAffIdForCat searchStr:searchBar.text];
            break;
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    [UIView beginAnimations:@"slideOut" context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.3];
    [UIView commitAnimations];
}
#pragma mark tableView delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [catObjArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"] ;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    //add category icons to cell
    SdImageView *asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(5, 2, 40, 40)] ;
    asyncImageView.contentMode = UIViewContentModeScaleToFill;
    asyncImageView.backgroundColor = [UIColor clearColor];
    asyncImageView.layer.cornerRadius = 10.0;
    [asyncImageView loadImage:[[catObjArray objectAtIndex:indexPath.row]catImg]];
    
    //add category name label to cell
    UILabel *catName = [[UILabel alloc]initWithFrame:CGRectMake(50, 0, 250, 40)];
    catName.backgroundColor = [UIColor clearColor];
    catName.textColor = [UIColor blackColor];
    catName.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    catName.text = [[catObjArray objectAtIndex:indexPath.row]catName];
    
    [cell addSubview:catName];
    [cell addSubview:asyncImageView];
    ReleaseAndNilify(catName);
    ReleaseAndNilify(asyncImageView);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (tableView.tag) {
        case 100:
            [self request_CitiExpRet:[[[catObjArray objectAtIndex:indexPath.row]catId]intValue] searchStr:@""];
            break;
            
        case 200:
            
            [self request_PartnerRet:[[[catObjArray objectAtIndex:indexPath.row]catId]intValue] searchStr:@""];
            break;
    }
}

#pragma mark bottom bar buttons actions
-(void)cityExpClicked{
    
    [defaults setInteger:100 forKey:@"CityExpTableTag"];
    [SharedManager setRefreshAustinRetailers:YES];
    
    NSArray *viewControllers = [self.navigationController viewControllers];
    for(int i = 0 ; i < [viewControllers count] ; ++i) {
        id viewController = [viewControllers objectAtIndex:i];
        
        if([viewController isKindOfClass:[CityExperienceViewController class]]) {
            [self.navigationController popToViewController:viewController animated:NO];
            break;
        }
    }
    
}

-(void)filtersClicked{
    
    if (categoriesTable.tag == 200) {
        return;
    }
    
    int i = [SharedManager filtersCount];
    switch (i) {
        case 1:{
            [self request_PartnerRet:0 searchStr:@""];
            
        }
            break;
            
        default:{
            
            [defaults setInteger:200 forKey:@"CityExpTableTag"];
            switch (categoriesTable.tag) {
                case 100:
                    
                    [self.navigationController popViewControllerAnimated:NO];
                    break;
                    
                case 200:{
                    int count = (int)[self.navigationController.viewControllers count];
                    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:count-3] animated:NO];
                }
                    break;
            }
        }
            break;
    }
}

-(void)settingsClicked{
    
    [self request_GetUserCat];
}

-(void)aboutClicked{
    //on click of about
    
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        
        AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
        privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
        //privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
        [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
        privacy.comingFromPrivacyScreen = NO;
        [self.navigationController pushViewController:privacy animated:NO];
        //[privacy release];
    }
    
}

#pragma mark request methods
//categories from City Experience
-(void)request_GetCatForGroup: (int) retGroupId{
    
    webRequestState = GETCATFORGROUP;
    NSString *reqStr = [NSString stringWithFormat:@"<ThisLocationRequest><hubCitiId>%@</hubCitiId><retGroupId>%@</retGroupId></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_LINKID]];
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getcatforgroup",BASE_URL];
    
    [ConnectionManager establishConnectionFor:reqStr base:urlString withDelegate:self];
    
}

//categories from partners
-(void)request_GetCatForPartner: (int) retAffId{
    webRequestState = GETCATFORPARTNER;
    NSString *reqStr = [NSString stringWithFormat:@"<ThisLocationRequest><hubCitiId>%@</hubCitiId><retAffId>%d</retAffId></ThisLocationRequest>",[defaults valueForKey:KEY_HUBCITIID],retAffIdForCat];
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/getcatforpartner",BASE_URL];
    [ConnectionManager establishConnectionFor:reqStr base:urlString withDelegate:self];
    
}

//get user info
-(void)request_GetUserInfo
{
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserDetails><userId>%@</userId><hubCitiId>%@</hubCitiId></UserDetails>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    webRequestState = USERINFO;
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo",BASE_URL];
    [ConnectionManager establishConnectionFor:xmlStr base:urlString withDelegate:self];
    ReleaseAndNilify(xmlStr);
}

//get user favorite categories
-(void)request_GetUserCat{
    
    webRequestState = USERCAT;
    if([Network currentReachabilityStatus]==0)
    {
        [common showAlert];
    }
    else {
        
        NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusercat",BASE_URL];
        NSString *reqString = [NSString stringWithFormat:@"<UserDetails><userId>%@</userId></UserDetails>",[defaults valueForKey:KEY_USERID]];
        
        [ConnectionManager establishConnectionFor:reqString base:urlString withDelegate:self];
        
    }
}

//get austin retailers
-(void)request_CitiExpRet:(int)catId searchStr:(NSString*)searchStr{
    
    webRequestState = CITIEXPRETAILERS;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId><hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    if ([searchStr length])
        [requestStr appendFormat:@"<searchKey>%@</searchKey>",searchStr];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    else if ([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    [requestStr appendFormat:@"<sortColumn>%@</sortColumn>",[defaults valueForKey:@"SortCitiExpBy"]];
    [requestStr appendFormat:@"<sortOrder>ASC</sortOrder>"];
    if ([SharedManager localSpecilas]) {
        [requestStr appendFormat:@"<locSpecials>1</locSpecials>"];
    }
    else{
        [requestStr appendFormat:@"<locSpecials>0</locSpecials>"];
    }
    
    [requestStr appendFormat:@"<citiExpId>%@</citiExpId><catIds>%d</catIds><lowerLimit>0</lowerLimit></ThisLocationRequest>",[defaults valueForKey:KEY_LINKID],catId];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/citiexpret",BASE_URL];
    
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);
}

-(void)request_PartnerRet:(int)catId searchStr:(NSString*)searchStr{
    
    webRequestState = PARTNERRETAILERS;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<ThisLocationRequest><userId>%@</userId><hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    if ([searchStr length])
        [requestStr appendFormat:@"<searchKey>%@</searchKey>",searchStr];
    
    //For User Tracking
    if ([defaults valueForKey:KEY_MAINMENUID])
        [requestStr appendFormat:@"<mainMenuId>%@</mainMenuId>",[defaults valueForKey:KEY_MAINMENUID]];
    
    else if ([defaults valueForKey:KEY_MITEMID])
        [requestStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    else if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults  objectForKey:KEY_BOTTOMBUTTONID]];
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        [requestStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         [defaults  valueForKey:KEY_LATITUDE]];
        
    }
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        [requestStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
    }
    if ([defaults valueForKey:@"filtersSort"]==nil) {
        [requestStr appendFormat:@"<sortColumn>Distance</sortColumn><sortOrder>Asc</sortOrder>"];
    }
    else
    {
        [requestStr appendFormat:@"<sortColumn>%@</sortColumn><sortOrder>Asc</sortOrder>",[defaults valueForKey:@"filtersSort"]];
    }
    
    
    [requestStr appendFormat:@"<lowerLimit>0</lowerLimit><retAffId>%d</retAffId><catIds>%d</catIds></ThisLocationRequest>",retAffIdForCat,catId] ;
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/partnerret",BASE_URL];
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);
}

#pragma mark parse methods
-(void)responseData:(NSString *)response
{
    switch (webRequestState)
    {
        case USERINFO:
            [self parse_GetUserData:response];
            break;
            
        case USERCAT:
            [self parse_GetFavCategories:response];
            break;
            
        case GETCATFORGROUP:
            [self parse_GetCatForGroup:response];
            break;
            
        case GETCATFORPARTNER:
            [self parse_GetCatForPartner:response];
            break;
            
        case CITIEXPRETAILERS:
            [self parse_CitiExpRet:response];
            break;
            
        case PARTNERRETAILERS:
            [self parse_PartnerRet:response];
            break;
            
        default:
            break;
    }
}
//parse user favorite categories
-(void)parse_GetFavCategories:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]){
        return;
    }
    
    [defaults  setObject:response forKey:KEY_RESPONSEXML];
    PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:settings animated:NO];
    ////[settings release];
}
/*
 *
 Parsing logic when response received for getting user details.
 Method called when navigated from Settings screen.
 *
 */
-(void)parse_GetUserData:(NSString*)response
{
    if ([UtilityManager isNullOrEmptyString:response]){
        [UtilityManager showAlert:@"Error!" msg:@"Please try after sometime"];
        return;
    }
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
    {
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        [SharedManager setUserInfoFromSignup:NO];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:inform animated:NO];
        //[inform release];
    }
    else {
        TBXMLElement *saveResponseText = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:saveResponseText];
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    ReleaseAndNilify(tbxml);
}

-(void)parse_GetCatForGroup:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]) {
        return;
    }
    else{
        [catObjArray removeAllObjects];
        TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
        TBXMLElement *CategoryInfoElement = [TBXML childElementNamed:@"CategoryInfo" parentElement:tbXml.rootXMLElement];
        
        //@Deepak: retGroupImg Image parse and display at the bottom tab bar
        
        if([defaults valueForKey:@"retGroupImg"])
        {
            NSString *strNormalImage = [defaults valueForKey:@"retGroupImg"];
            NSString *strHighlightedImage = [defaults valueForKey:@"retGroupImg_On"];
            
            UIImage *imgNormalImage = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strNormalImage]]];
            UIImage *imgHighlightedImage = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strHighlightedImage]]];
            
            [cityExpBtn setBackgroundImage:imgNormalImage forState:UIControlStateNormal];
            [cityExpBtn setBackgroundImage:imgHighlightedImage forState:UIControlStateHighlighted];
            [cityExpBtn setBackgroundColor:[UIColor colorWithRed:0.1098 green:0.1098 blue:0.1098 alpha:1]];
            
            //[imgNormalImage release];
            //[imgHighlightedImage release];
            
        }
        else
        {
            // [cityExpBtn setImage:[UIImage imageNamed:@"tab_btn_up_austinExp"] forState:UIControlStateNormal];
            // [cityExpBtn setImage:[UIImage imageNamed:@"tab_btn_down_austinExp"] forState:UIControlStateHighlighted];
            
        }
        
        while (CategoryInfoElement != nil) {
            categoryObject = [[CategoryDO alloc]init];
            TBXMLElement *categoryIdElement = [TBXML childElementNamed:@"categoryId" parentElement:CategoryInfoElement];
            TBXMLElement *categoryNameElement = [TBXML childElementNamed:@"categoryName" parentElement:CategoryInfoElement];
            TBXMLElement *catImgElement = [TBXML childElementNamed:@"catImg" parentElement:CategoryInfoElement];
            
            if (categoryIdElement!=nil && categoryNameElement!=nil && catImgElement!=nil){
                categoryObject.catId = [TBXML textForElement:categoryIdElement];
                categoryObject.catName = [TBXML textForElement:categoryNameElement];
                categoryObject.catImg = [TBXML textForElement:catImgElement];
                [catObjArray addObject:categoryObject];
            }
            CategoryInfoElement = [TBXML nextSiblingNamed:@"CategoryInfo" searchFromElement:CategoryInfoElement];
        }
        [categoriesTable reloadData];
    }
}

-(void)parse_GetCatForPartner:(NSString*)response{
    
    if ([UtilityManager isNullOrEmptyString:response]) {
        return;
    }
    else
    {
        [catObjArray removeAllObjects];
        TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
        TBXMLElement *CategoryInfoElement = [TBXML childElementNamed:@"CategoryInfo" parentElement:tbXml.rootXMLElement];
        
        if([defaults valueForKey:@"retGroupImg"])
        {
            NSString *strNormalImage = [defaults valueForKey:@"retGroupImg"];
            UIImage *imgNormalImage = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strNormalImage]]];
            [cityExpBtn setBackgroundImage:imgNormalImage forState:UIControlStateNormal];
            // [imgNormalImage release];
            
            NSString *strHighlightImage = [defaults valueForKey:@"retGroupImg_On"];
            UIImage *highlightImage = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strHighlightImage]]];
            [cityExpBtn setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
            // [highlightImage release];
        }
        else
        {
            [cityExpBtn setImage:[UIImage imageNamed:@"tab_btn_up_austinExp"] forState:UIControlStateNormal];
            [cityExpBtn setImage:[UIImage imageNamed:@"tab_btn_down_austinExp"] forState:UIControlStateHighlighted];
            
        }
        
        while (CategoryInfoElement != nil) {
            categoryObject = [[CategoryDO alloc]init];
            TBXMLElement *categoryIdElement = [TBXML childElementNamed:@"categoryId" parentElement:CategoryInfoElement];
            TBXMLElement *categoryNameElement = [TBXML childElementNamed:@"categoryName" parentElement:CategoryInfoElement];
            TBXMLElement *catImgElement = [TBXML childElementNamed:@"catImg" parentElement:CategoryInfoElement];
            
            if (categoryIdElement!=nil && categoryNameElement!=nil && catImgElement!=nil){
                categoryObject.catId = [TBXML textForElement:categoryIdElement];
                categoryObject.catName = [TBXML textForElement:categoryNameElement];
                categoryObject.catImg = [TBXML textForElement:catImgElement];
                [catObjArray addObject:categoryObject];
            }
            
            CategoryInfoElement = [TBXML nextSiblingNamed:@"CategoryInfo" searchFromElement:CategoryInfoElement];
        }
        [categoriesTable reloadData];
    }
}

-(void)parse_CitiExpRet:(NSString*)response{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:response]){
        return;
    }
    
    else{
        
        TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        
        if (responseCodeElement != nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            [defaults setObject:nil forKey:@"Response_Austin"];
            return;
        }
        
        [defaults setObject:response forKey:@"Response_Austin"];
        CityExperienceViewController *cityExp = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
        self.cityExp = cityExp;
        [annArray removeAllObjects];
        [cityExpObjArray removeAllObjects];
        [defaults setInteger:100 forKey:@"CityExpTableTag"];
        [cityExp setTagValue:100];
        [SharedManager setRefreshAustinRetailers:YES];
        [self.navigationController popViewControllerAnimated:NO];
        // //[cevc release];
    }
}

//parse the partner retailers list
-(void)parse_PartnerRet:(NSString*)response{
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    
    if (responseCodeElement != nil && ![[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        //[searchBar resignFirstResponder];
        return;
    }
    else{
        [defaults setObject:response forKey:@"Response_Filters"];
        [self.navigationController popViewControllerAnimated:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

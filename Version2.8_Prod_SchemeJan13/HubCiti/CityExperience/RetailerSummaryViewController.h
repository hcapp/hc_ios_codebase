//
//  RetailerSummaryViewController.h
//  Scansee
//
//  Created by ajit on 5/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtility.h"
#import <CoreLocation/CoreLocation.h>
#import "AnyViewController.h"
#import <MessageUI/MessageUI.h>
#import <Twitter/Twitter.h>
#import "Config.h"
#import "CityExperienceDO.h"
#import "DWBubbleMenuButton.h"
@class EventsListViewController;
@class FundraiserListViewController;
@class AnyViewController;
@class EmailShareViewController;
////
@interface RetailerSummaryViewController : UIViewController<UITableViewDataSource ,UITableViewDelegate,MFMessageComposeViewControllerDelegate, DWBubbleMenuViewDelegate,CustomizedNavControllerDelegate,HTTPClientDelegate>
{
    
    NSString *latValue;
    NSString *longValue;
    
    UITableView *table_RetailerSummary;
    UILabel *retailerName;
    NSString * claimExist;
    
    NSMutableArray *infoKeys ,*infoValue;
    UIButton *callStore;
    
    //Chance's Edits
    UIButton *getDirections;
    UIButton *website;
    NSString *imgPathDir;
    NSString *imgPathUrl;
    //End Chance's Edits
    
    NSMutableArray *titleArray;
    NSMutableArray *subtitleArray; 
    NSMutableArray *imageUrlArray;
    NSMutableArray *indexArray;//to track the selected section
    
    NSString *retImgUrl;
    NSString *retLocId;
    NSString *retId;
    NSString *ribbonAdURLString;
    UIButton *bannerBtn;
    NSString *bannerAdPath;
    
    NSString *eventExist;
    NSString *fundExist;
    //Added To handle Difference in Mileage for version 1.2.2. Will have to change the way the value is handled in the future.
    NSString *distanceFromPreviousScreen;
    
    //For appsite sharing
    IBOutlet UIButton *shareBtn, *findBtn, *settingsBtn, *abtBtn;
    AnyViewController *anyVC;
    NSMutableString *serviceUrl;
    NSMutableString *xmlString;
    NSString *retDetId;
    NSMutableArray *anythingPageListId,*pageId;
    
    WebRequestState iWebRequestState;
    CommonUtility *common;
    
    SdImageView *bannerAdImageView;
    UIViewController *modalVC;
    
     NSString *androidDownloadLink;
    
}
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@property(nonatomic,strong) FundraiserListViewController *iFundraiserListViewController;
@property(nonatomic,strong) EventsListViewController* iEventsListViewController;
@property (nonatomic, strong)IBOutlet UITableView *table_RetailerSummary;
@property (nonatomic, strong)IBOutlet UILabel *retailerName;;
@property (nonatomic, strong) NSString *distanceFromPreviousScreen;;
@property (nonatomic, strong) NSMutableString *serviceUrl,*xmlString;
@property(nonatomic,strong) NSString * userMailID;
@property(nonatomic,strong) NSString * retailerAddress;
@property(nonatomic,strong) NSMutableArray * array_ClaimBusInfoData;
@property(nonatomic,strong) NSMutableArray *mainCategoryNameArray;;
-(void) callPressed: (UIButton *)sender;

@end

//
//  EventGroupListDetails.h
//  HubCiti
//
//  Created by Lakshmi H R on 7/19/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventListDetails.h"

@protocol EventGroupListDetails;

@interface EventGroupListDetails : NSObject

@property(nonatomic,strong) NSArray<EventListDetails> * eventList;
@property(nonatomic,strong) NSString *groupContent;
@property (nonatomic,strong) NSMutableArray *eventGrpArray;

@end

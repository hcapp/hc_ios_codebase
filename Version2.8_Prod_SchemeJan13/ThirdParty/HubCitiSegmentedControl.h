//
//  ScanSeeSegmentedControl.h
//  SegmentedControlTest
//
//  Created by Chance Ivey on 9/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HubCitiSegmentedControl : UISegmentedControl

-(void)setSegmentColor;
@end

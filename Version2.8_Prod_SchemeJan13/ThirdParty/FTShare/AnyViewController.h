//
//  AnyViewController.h
//  FTShareView
//
//  Created by Francesco on 19/10/2011.
//  Copyright (c) 2011 Fuerte International. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTShare.h"
#import "HubCitiConstants.h"

@interface AnyViewController : UIViewController <FTShareFacebookDelegate, FTShareEmailDelegate,UITextViewDelegate>{
    UIImageView *imageView;
    UIImageView *imageViewBG;
    UIActivityIndicatorView *activityIndicator;
    UILabel *label;
    NSMutableString *fbShareType;
    WebRequestState flag;
}

@property(nonatomic, strong)UIImageView *imageView;
@property(nonatomic, strong)UIImageView *imageViewBG;
@property(nonatomic, strong)NSMutableString *fbShareType;
@property(nonatomic, strong)UITextView *productInfo;
@property(atomic) BOOL fromSideMenu;
@end

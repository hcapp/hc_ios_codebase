//
//  UBTiwtterMessageView.m
//  UBMMedia
//
//  Created by Francesco on 09/01/2012.
//  Copyright (c) 2012 Lost Bytes. All rights reserved.
//

#import "FTShareMessageController.h"
#import "UIColor+Tools.h"

#define MAX_TWITTER_CHARS   140
#define ALERT_TWITTER_CHARS 10


@implementation FTShareMessageController

@synthesize textView = _textView;
@synthesize delegate = _delegate;
@synthesize message = _message;
@synthesize charsLeftLabel = _charsLeftLabel;
@synthesize type = _type;


#pragma mark actions

- (void)tweetAction {
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareMessageController:didFinishWithMessage:)]) {
        [self.delegate shareMessageController:self didFinishWithMessage:self.message];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cancelAction {
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareMessageControllerDidCancel:)]) {
        [self.delegate shareMessageControllerDidCancel:self];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    self.message = nil;
}

#pragma mark setters

- (NSInteger)twitterCharactersForString:(NSString *)string {
    static NSString *httpRegEx = @"http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w-./?%&=]*)?";
    
    if (string.length ==0) return 0;
    int subtruct = 0;
    NSRange range = NSMakeRange(0, 0);
    while (YES) {
        int start = (int)(range.location + range.length);
        int length = (int)(string.length - start);
        range = [string rangeOfString:httpRegEx options:NSRegularExpressionSearch range:NSMakeRange(start, length)];
        if (range.location != NSNotFound) {
            if (range.length > 20) subtruct += (range.length - 20);
        }
        else return (string.length - subtruct);
    }
    
    
}

- (void)setMessage:(NSString *)message {
    [_message release];
    _message = [message retain] ;
    
    NSInteger length = message.length;
    NSInteger remaining = MAX_TWITTER_CHARS - length;
    
    [self.navigationItem.rightBarButtonItem setEnabled:(remaining >= 0 || (self.type == FTShareMessageControllerTypeFacebook))];
}

#pragma mark initialize

- (id)initWithMessage:(NSString *)message type:(FTShareMessageControllerType)type andelegate:(id)delegate
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Initialization code
        self.message = message;
        _delegate = delegate;
        self.type = type;
       
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //Edits For The PopUp - CI
    //self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    [self setUpAsPopup];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareMessageController:didDisappearWithMessage:)]) {
        [self.delegate shareMessageController:self didDisappearWithMessage:self.message];
    }
}

//Edits For The PopUp - CI
- (void) setUpAsPopup
{
    
    //Setup
    self.view.frame = CGRectMake(20, 20, 280, 220);
    self.view.backgroundColor = [UIColor clearColor];
    
    //Background Image
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"twShareBackground.png"]];
    [backgroundImageView setFrame:CGRectMake(0, 0, 280, 220)];
    [self.view addSubview:backgroundImageView];
    [backgroundImageView release];
    
    //ToolBar
    UIBarButtonItem *share = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Share",@"Share") style:UIBarButtonItemStylePlain target:self action:@selector(tweetAction:)];
   // UIImageView *fbBannerIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"facebookBanner.png"]];
   // fbBannerIV.contentMode = UIViewContentModeScaleAspectFit;
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] init];//initWithCustomView:fbBannerIV];
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel",@"Cancel") style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(10, 10, 260, 44)];
    [spacer setWidth:toolbar.frame.size.width - 70 - 70 - 5];
    //  DLog(@"toolbar - %f, cancel - %f, share - %f", toolbar.frame.size.width, cancel., share.width);
    [toolbar setItems:[NSArray arrayWithObjects:cancel,spacer, share, nil]];
    [toolbar setTintColor:[UIColor colorWithRed:116.0/255.0 green:175.0/255.0 blue:179.0/255.0 alpha:1.0]];
    [self.view addSubview:toolbar];
    [toolbar release];
	[share release];
    [spacer release];
   // [fbBannerIV release];
    
    //ProductInfoText
    UITextView *productInfo = [[UITextView alloc] initWithFrame:CGRectMake(15, 95, 200, 120)];
    [_textView setFrame:CGRectMake(15,95,210,120)];
    _textView.font = [UIFont systemFontOfSize:14];
	_textView.scrollEnabled = NO;
	//productInfo.editable = NO ;
    [_textView setBackgroundColor:[UIColor clearColor]];
	_textView.text = self.message;
	[self.view addSubview:_textView];
    [productInfo release];
}
//Edits For The PopUp - CI
-(void)dismiss
{  
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareMessageControllerDidCancel:)]) {
        [self.delegate shareMessageControllerDidCancel:self];
    }
    [self.view removeFromSuperview];
    self.message = nil;
    
}
#pragma mark textview delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSString *resultString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    //int lenght = [self twitterCharactersForString:resultString];

    self.message = resultString;
    return YES;
}


- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    
    [self resignFirstResponder];
    return YES;
    
}

@end

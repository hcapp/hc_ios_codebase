//
//  AnyViewController.m
//  FTShareView
//
//  Created by Francesco on 19/10/2011.
//  Copyright (c) 2011 Fuerte International. All rights reserved.
//

#import "AnyViewController.h"
#import "AppDelegate.h"
#import "Config.h"


#import "FTShare.h"

@implementation AnyViewController
@synthesize imageView,imageViewBG,productInfo;
@synthesize fbShareType,fromSideMenu;

- (FTShare *)shareInstance {
   AppDelegate *appDel = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    [appDel.share setReferencedController:self];
    
    // set up sharing components
    [appDel.share setUpFacebookWithAppID:kIKFacebookAppID permissions:FTShareFacebookPermissionRead|FTShareFacebookPermissionPublish|FTShareFacebookPermissionOffLine andDelegate:self];
    [appDel.share setUpEmailWithDelegate:self];
    
    return appDel.share;
}

- (void)share:(id)sender {
    
    //self.navigationItem.backBarButtonItem.enabled = NO;
    self.navigationItem.rightBarButtonItem.enabled = NO;
    self.navigationItem.hidesBackButton = YES;
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.color = [UIColor blackColor];
    activityIndicator.frame = CGRectMake((280 / 2)- 25, (self.view.frame.size.height / 2) - 50, 50, 50);
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.hidden = NO;
    [self.view addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    //self.view.userInteractionEnabled = NO;
    
   FTShare *share = [self shareInstance];
  [defaults setValue:@"0" forKey:@"PresentActionSheet"];
    [share shareThroughFacebook];
    [self performSelector:@selector(removeLoadingIndicators) withObject:nil afterDelay:10];

}

- (void)getUSerFriends {
    FTShare *share = [self shareInstance];
    FTShareFacebookData *fbData = [[FTShareFacebookData alloc] init];
    [fbData setType:FTShareFacebookRequestTypeFriends];
    [fbData setHttpType:FTShareFacebookHttpTypeGet];
    [share shareViaFacebook:fbData];
}

- (void)fbUploadImage {
    FTShare *share = [self shareInstance];
    FTShareFacebookData *fbData = [[FTShareFacebookData alloc] init];
    [fbData setType:FTShareFacebookRequestTypeAlbum];
    [fbData setHttpType:FTShareFacebookHttpTypePost];
    FTShareFacebookPhoto *photo = [FTShareFacebookPhoto facebookPhotoFromImage:[UIImage imageNamed:@"Cornholio.jpg"]];
    [photo setMessage:NSLocalizedString(@"I am the great Cornholio;",@"I am the great Cornholio;")];
    [fbData setUploadPhoto:photo];
    [share shareViaFacebook:fbData];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    productInfo = [[UITextView alloc] init];
    productInfo.delegate = self;
    [self setupAsPopUp];
}

#pragma mark FTShare Facebook
/**
 * If using Action Sheet this is ised to set the data to share.
 *
 */

- (FTShareFacebookData *)facebookShareData {
        
    FTShareFacebookData *data = [[FTShareFacebookData alloc] init];
   // [data setMessage:@"Great Find At ScanSee"];
   
    //commented for later use
    if ([productInfo.text isEqualToString:@"Enter text to post on wall"]) {
        [data setMessage:@""];
    }
    else{
        DLog(@"%@",productInfo.text);
    [data setMessage:[NSString stringWithFormat:@"%@",productInfo.text]];
    }
    
    if (![[defaults objectForKey:KEY_HYPERLINK] isEqualToString:@"N/A"]){
        DLog(@"%@",[defaults valueForKey:KEY_HYPERLINK]);
        [data setLink:[NSString stringWithFormat:@"%@",[defaults objectForKey:KEY_HYPERLINK]]];

        //[data setName:NSLocalizedString(@"View Details",@"View Details")];
//        [data setName:[NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]]];
//        [data setName:NSLocalizedString(@"Visit iTunes",@"Visit iTunes")];
    }
    [data setCaption:@" "];
    
//    if([defaults  objectForKey:KEY_SHAREMSG])
//        [data setDescription:[NSString stringWithFormat:@"%@",[defaults  objectForKey:KEY_SHAREMSG]]];
    
    if([defaults  objectForKey:KEY_PRODUCTIMGPATH])
       [data setPicture:[NSString stringWithFormat:@"%@",[defaults objectForKey:KEY_PRODUCTIMGPATH]]];
    
    
    [data setType:FTShareFacebookRequestTypePost];
    [data setHttpType:FTShareFacebookHttpTypePost];
    [data setHasControllerSupport:YES];  // set to YES to use message controller
    
    return data ;
}
/**
 * Called if facebook request had no type. Provide path like "me/feeds" and override httpMethod with GET, POST or DELETE
 *
 */
- (NSString *)facebookPathForRequestofMethodType:(NSString **)httpMethod {
    *httpMethod = @"GET";
    return @"me/friends";
}

/**
 * Called when Facebook finishes to login. Error is nil for successfull operation
 *
 */
- (void)facebookDidLogin:(NSError *)error {
       DLog(@"Facebook did %@ login with Error: [%@]",(error == nil)? @"" : @"NOT", [error localizedDescription]);
    if (error!=nil) {
        
        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"The operation couldn’t be completed.",@"The operation couldn’t be completed.")];
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
        self.navigationItem.rightBarButtonItem.enabled = YES;
        self.navigationItem.hidesBackButton = NO;
    }
}

/**
 * Called when FAcebook finishes to post. Error is nil for successfull operation
 *
 */
- (void)facebookDidPost:(NSError *)error {
    DLog(@"Facebook has %@ posted with Error: [%@]",(error == nil)? @"" : @"NOT", [error localizedDescription]);
    if (error!=nil)
    {
        FTShareFacebook *fb = [[FTShareFacebook alloc]init];
        [fb authorize];
    }
    else{
        UIAlertController * alert;
        
        alert=[UIAlertController alertControllerWithTitle:@"Facebook" message:NSLocalizedString(@"Shared",@"Shared") preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:nil];
        [UtilityManager performSelector:@selector(alertControllerDismiss:) withObject:alert afterDelay:1.5];
        
        
        NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        for (NSHTTPCookie* cookie in
             [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
            [cookies deleteCookie:cookie];
        }
        
        
        if (fbShareType!=nil) {
            
            NSString *typeShare = [[NSString alloc]initWithString:fbShareType];
        if ([typeShare isEqualToString:@"PRODUCT_SHARE"]) {
            flag=PRODUCT_SHARE;
        }
        else if([typeShare isEqualToString:@"SPECIAL_OFFER_SHARE"])
        {
            flag=SPECIAL_OFFER_SHARE;
            
        }
        else if([typeShare isEqualToString:@"ANYTHING_SHARE"])
        {
            flag=ANYTHING_SHARE;
            
        }
        else if([typeShare isEqualToString:@"APPSITE_SHARE"])
        {
            flag=APPSITE_SHARE;
        }
        else if([typeShare isEqualToString:@"EVENT_SHARE"])
        {
            flag=EVENT_SHARE;
        }
        else if([typeShare isEqualToString:@"FUNDRAISER_SHARE"])
        {
            flag=FUNDRAISER_SHARE;
        }
        else if([typeShare isEqualToString:@"BAND_SHARE"])
        {
            flag = BAND_SHARE;
        }
        else if([typeShare isEqualToString:@"BAND_EVENT_SHARE"])
        {
            flag = BAND_EVENT_SHARE;
        }
        //flag=[defaults valueForKey:KEY_SHARE_TYPE];
        NSMutableString *xmlStr = [[NSMutableString alloc] init];
        [xmlStr appendFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Facebook</shrTypNam>",[defaults valueForKey:KEY_MAINMENUID]];
        
        
        
        switch (flag) {
            case PRODUCT_SHARE:
                [xmlStr appendFormat:@"<prodId>%@</prodId></UserTrackingData>",[defaults valueForKey:KEY_PRODUCTID]];
                break;
            
            case SPECIAL_OFFER_SHARE:
                [xmlStr appendFormat:@"<sPageId>%@</sPageId></UserTrackingData>",[defaults valueForKey:KEY_PAGEID]];
                break;
            case ANYTHING_SHARE:
                 if (requestAnythingFromMainMenu==FALSE) {
                     [xmlStr appendFormat:@"<aPageId>%@</aPageId><retId>%@</retId><retLocId>%@</retLocId></UserTrackingData>",[defaults valueForKey:KEY_ANYPAGEID],[defaults valueForKey:KEY_RETAILERID],[defaults valueForKey:@"retailLocationID"]];
                 }
                 else{
                [xmlStr appendFormat:@"<aPageId>%@</aPageId></UserTrackingData>",[defaults valueForKey:KEY_ANYPAGEID]];
                 }
                break;
            case APPSITE_SHARE:
                [xmlStr appendFormat:@"<retId>%@</retId><retLocId>%@</retLocId></UserTrackingData>",[defaults valueForKey:KEY_RETAILERID],[defaults valueForKey:@"retailLocationID"]];
                break;
            case EVENT_SHARE:
                [xmlStr appendFormat:@"<eventId>%@</eventId></UserTrackingData>",[defaults valueForKey:EVENTID]];
                break;
            case FUNDRAISER_SHARE:
                [xmlStr appendFormat:@"<fundId>%@</fundId></UserTrackingData>",[defaults valueForKey:FUNDRAISERID]];
                break;
            case BAND_SHARE:
                [xmlStr appendFormat:@"<bandId>%@</bandId></UserTrackingData>",[defaults valueForKey:KEY_BANDID]];
                break;
            case BAND_EVENT_SHARE:
                [xmlStr appendFormat:@"<bandEventId>%@</bandEventId></UserTrackingData>", [defaults valueForKey:@"BandEventID"]];
                break;
                
            default:
                break;
        }
        
        [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:xmlStr];
        }
       // [ConnectionManager establishConnectionFor:xmlStr base:urlStr withDelegate:self];
    }
    
    
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
    self.navigationItem.rightBarButtonItem.enabled = YES;
    self.navigationItem.hidesBackButton = NO;
}

#pragma userTracking
-(void)userTracking:(NSString*)xmlStr
{
 
    NSMutableString *urlStr = [[NSMutableString alloc]init];
    [urlStr appendFormat:@"%@ratereview/sendersharetrack",BASE_URL];
 DLog(@"%@",urlStr);
  NSString *response=[[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlStr withParam:xmlStr]];
    DLog(@"%@",response);
   // [xmlStr release];
}

/**
 * Called when FAcebook finishes a request with data.
 *
 */
- (void)facebookDidReceiveResponse:(id)response {
  //  DLog(@"Facebook Response: %@", [response description]);
    [self.view removeFromSuperview];
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    DLog(@"%@", [error localizedDescription]);
    DLog(@"Err details: %@", [error description]);
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
    self.navigationItem.rightBarButtonItem.enabled = YES;
    self.navigationItem.hidesBackButton = NO;
};

- (void)removeLoadingIndicators
{
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
    //self.navigationItem.rightBarButtonItem.enabled = YES;
    self.navigationItem.hidesBackButton = NO;
}

#pragma mark FTShare Mail

/**
 * If using Action Sheet this is ised to set the data to share.
 *
 */
- (FTShareEmailData *)mailShareData {
    FTShareEmailData *data = [[FTShareEmailData alloc] init];
    [data setSubject:NSLocalizedString(@"check out this site",@"check out this site")];
    [data setPlainBody:@"Crazy for iOS apps!\n http://www.fuerteint.com"];
    [data setHtmlBody:@"<h2>Crazy for iOS apps!</h2><a href='http://www.fuerteint.com'>fuerteint.com</a>"];
    
    return data;
}


/**
 * Called when main finishes to post.
 *
 */
- (void)mailSent:(MFMailComposeResult)result {
    //manage mail result
    //DLog(@"Mail %@ sent", (result == MFMailComposeResultSent)? @"" : @"NOT");
}


-(void)setupAsPopUp
{
    
    float viewXValue = 20, viewYValue = 20;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        
        viewXValue = (SCREEN_WIDTH - 280)/2;
        viewYValue = (SCREEN_HEIGHT - 220 - 50)/2;
    }
    //Setup
    if(fromSideMenu)
        self.view.frame = CGRectMake(viewXValue, IPAD?viewYValue: viewYValue+VARIABLE_HEIGHT(50), 280, 220);
    else
        self.view.frame = CGRectMake(viewXValue, viewYValue, 280, 220);
    
    self.view.backgroundColor = [UIColor clearColor];
    
    //Background Image
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fbShareBackground.png"]];
    [backgroundImageView setFrame:CGRectMake(0, 0, 280, 220)];
    [self.view addSubview:backgroundImageView];
    //[backgroundImageView release];
    
    //ToolBar
    UIBarButtonItem *share = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Share",@"Share") style:UIBarButtonItemStylePlain target:self action:@selector(share:)];
    UIImageView *fbBannerIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"facebookBanner.png"]];
    //    fbBannerIV.backgroundColor = [UIColor blackColor];
    fbBannerIV.contentMode = UIViewContentModeScaleAspectFit;
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithCustomView:fbBannerIV];
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel",@"Cancel") style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(10, 10, 260, 44)];
    [spacer setWidth:toolbar.frame.size.width - 70 - 70 - 5];
    //  DLog(@"toolbar - %f, cancel - %f, share - %f", toolbar.frame.size.width, cancel., share.width);
    [toolbar setItems:[NSArray arrayWithObjects:cancel,spacer, share, nil]];
    [toolbar setTintColor:[UIColor colorWithRed:57.0/255.0 green:87.0/255.0 blue:154.0/255.0 alpha:1.0]];
    [toolbar setBarTintColor:[UIColor blackColor]];
    [self.view addSubview:toolbar];
    // [toolbar release];
    //[share release];
    //[spacer release];
    // [fbBannerIV release];
    
    //ProductInfoText
    
    [productInfo setFrame:CGRectMake(15, 97, 180, 115)];
    productInfo.font = [UIFont systemFontOfSize:14];
    productInfo.scrollEnabled = YES;
    productInfo.editable = YES ;
    //    productInfo.scrollEnabled = NO;
    //    productInfo.editable = NO ;
    [productInfo setBackgroundColor:[UIColor clearColor]];
    [productInfo setReturnKeyType:UIReturnKeyDone];
    [productInfo setText:@"Enter text to post on wall"];
    [productInfo setTextColor:[UIColor lightGrayColor]];
    //	productInfo.text = [NSString stringWithFormat:@"%@\n%@",[defaults  objectForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]];
    [self.view addSubview:productInfo];
    //[productInfo release];
    
    
    //ImageView Adder
    
    NSString *imagePath;
    //if(IOS9)
   
    
    if ([[defaults  objectForKey:KEY_PRODUCTIMGPATH] rangeOfString:@"%"].location == NSNotFound){
        
        imagePath = [[[defaults  objectForKey:KEY_PRODUCTIMGPATH]copy] stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]];
    }
    else{
        imagePath = [[defaults  objectForKey:KEY_PRODUCTIMGPATH] copy] ;
    }
    //	}
    if ([imagePath isEqualToString:@"N/A"] || [UtilityManager isNullOrEmptyString:imagePath]) {
        imagePath = @"https://www.scansee.net/Images/imageNotFound.png";
    }
    NSLog(@"keypath2:%@",[defaults  objectForKey:KEY_PRODUCTIMGPATH]);
    [defaults  setObject:imagePath forKey:KEY_PRODUCTIMGPATH];
    
    
    NSData *thedata =  [NSData dataWithContentsOfURL:[NSURL URLWithString:imagePath]];
    //[thedata writeToFile:localFilePath atomically:YES];
    UIImage *img1 = [[UIImage alloc] initWithData:thedata];
	   
    UIImageView *imgFrame = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"shareImgFrame.png"]];
    imgFrame.frame = CGRectMake(185,90,80,80);
    [self.view addSubview:imgFrame];
    //[imgFrame release];
    
    
    self.imageView = [[UIImageView alloc] initWithImage:img1];
    imageView.frame = CGRectMake(196,101,58,58);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:imageView];
    
    UIImageView *pprClip = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"paperClip.png"]];
    pprClip.frame = CGRectMake(235, 140, 34, 20);
    [self.view addSubview:pprClip];
    //[pprClip release];
    
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if (productInfo.textColor == [UIColor lightGrayColor]) {
        productInfo.text = @"";
        productInfo.textColor = [UIColor blackColor];
    }
    
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if(productInfo.text.length == 0){
        productInfo.textColor = [UIColor lightGrayColor];
        productInfo.text = @"Enter text to post on wall";
        [productInfo resignFirstResponder];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if(productInfo.text.length == 0){
            productInfo.textColor = [UIColor lightGrayColor];
            productInfo.text = @"Enter text to post on wall";
            [productInfo resignFirstResponder];
        }
        return NO;
    }
    
    return YES;
}

-(void)dismiss
{
    [defaults setValue:@"0" forKey:@"PresentActionSheet"];
    [self.view removeFromSuperview];
}

#pragma mark usertracking

-(void)responseData:(NSString*)response{
    NSLog(@"%@",response);
}

@end

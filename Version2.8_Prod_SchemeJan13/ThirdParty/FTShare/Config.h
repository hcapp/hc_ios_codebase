//
//  Config.h
//  FTShare
//
//  Created by Francesco on 19/10/2011.
//  Copyright (c) 2011 Fuerte International. All rights reserved.
//

#ifndef FTShare_Config_h
#define FTShare_Config_h

//#define kIKFacebookAppID          @"241805932524393" //Span account - used for internal testing
//#define kIKFacebookAppID        @"479089565435382"  //ScanSee account - used for client releases
//#define kIKFacebookAppID        @"1376916979220697"  //HubCiti647 account - used for client releases
//#define kIKFacebookAppID        @"1586293501619323"  //Marble Falls account - used for client releases // fb1586293501619323
//#define kIKFacebookAppID        @"1623145254596773"  //Tyler account - used for client releases //fb1623145254596773
//#define kIKFacebookAppID        @"1442520716040921"  //Addison account - used for client releases
//#define kIKFacebookAppID        @"1576238119317785"  //Rockwall account - used for client releases      //fb1576238119317785
//#define kIKFacebookAppID        @"1663027670587727"  //Fort Davis account - used for client releases
//#define kIKFacebookAppID        @"676316552511688"  //Rockwall Area Region Test account - used for client releases      //fb676316552511688(unwanted)
//#define kIKFacebookAppID        @"1658747434368982"  //Killeen account - used for client releases //fb1658747434368982
//#define kIKFacebookAppID @"420023741526162" //Austin account - used for client releases //fb420023741526162
//#define kIKFacebookAppID @"1755343911371856" //Mt Airy account - used for client releases //fb1755343911371856

#define kIKFacebookAppID [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FbId"]
#endif

//
//  sdImageView.m
//  HubCiti
//
//  Created by Ashika on 1/12/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "SdImageView.h"

@implementation SdImageView

@synthesize activityIndicator;

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
    }
    return self;
}
-(void) loadImage:(NSString *)urlStr{
    if (urlStr == nil)
    {
        return;
    }
    NSURL *url;
    if (urlStr == nil || [urlStr isEqualToString:@"N/A"])
        urlStr = @"https://app.scansee.net/Images/imageNotFound.png";
    
   activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    CGPoint pt = CGPointMake(self.center.x - self.frame.origin.x, self.center.y - self.frame.origin.y);
//    activityIndicator.center = pt;

//    CGRect activityIndicatorBounds = activityIndicator.bounds;
//    float x = (self.bounds.size.width - activityIndicatorBounds.size.width) / 2.0;
//    float y = (self.bounds.size.height - activityIndicatorBounds.size.height) / 2.0;
    
   // activityIndicator.frame = CGRectMake(x, y, activityIndicatorBounds.size.width, activityIndicatorBounds.size.height);
    activityIndicator.center=CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    activityIndicator.hidesWhenStopped = YES;
    if ([urlStr rangeOfString:@"%"].location == NSNotFound){
        url = [NSURL URLWithString:[NSString stringWithString:[urlStr stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]]];
    }
    
    else{
        url = [NSURL URLWithString:[NSString stringWithString:urlStr]];
    }

    [self addSubview:activityIndicator];
    [activityIndicator startAnimating];
    
    [self sd_setImageWithURL:url placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
    }];
    
}

-(void)loadImagefrommainBundel:(NSString*)imageName
{
    UIImage *image = [UIImage imageNamed:imageName];
    if (image)
    {
//        UIImageView *imageView = [[UIImageView alloc]
//                                  initWithImage:image] ;
        [self setImage:image];
        [self setNeedsLayout];
     //   [imageView setNeedsLayout]; // is this necessary if superview gets setNeedsLayout?
     //   [self setNeedsLayout];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

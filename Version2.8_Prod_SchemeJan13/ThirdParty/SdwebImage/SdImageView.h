//
//  sdImageView.h
//  HubCiti
//
//  Created by Ashika on 1/12/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

@interface SdImageView : UIImageView{
    UIActivityIndicatorView *activityIndicator;
}
@property(nonatomic,strong)  UIActivityIndicatorView *activityIndicator;
-(void) loadImage:(NSString *)url;

-(void)loadImagefrommainBundel:(NSString*)imageName;
@end

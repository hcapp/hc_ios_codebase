//
//  SlideshowViewController.m
//  Scansee
//  @Description -  Class to play and handle the slide show
//  Created by Deepak Agarwal on 22nd April 2013
//  Copyright SPANSYSTEM INC. All rights reserved.
//
#import "SlideshowViewController.h"
#import "SlideImageDispalyViewController.h"
#import "MainMenuViewController.h"

@implementation SlideshowViewController

@synthesize viewControllers;

// Override initWithNibName:bundle: to load the view using a nib file then perform additional customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil 
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) 
	{
        // Custom initialization
    }
    return self;
}

- (void)setisComingFromViewMore:(BOOL)val
{
    isComingFromViewMore = val;
}

// load the scroll view with number of pages which is to display
- (void)loadScrollViewWithPage:(int)page
{
    if (page < 0) return;
    if (page >= count) return;
	
    // replace the placeholder if necessary with the inside view controller objects to load the image
    SlideImageDispalyViewController *controller = [viewControllers objectAtIndex:page];
    if ((NSNull *)controller == [NSNull null]) 
	{
        controller = [[SlideImageDispalyViewController alloc] initWithPageNumber:page];
		[controller setEncURL:[ImageArrayControllers objectAtIndex:page]]; // This will add the loading image URL to the controller class
        [viewControllers replaceObjectAtIndex:page withObject:controller];
       // [controller release];
    }
	
	
    // add the controller's view to the scroll view and set the frame size for the inside controller class
    if (nil == controller.view.superview) 
	{
        CGRect frame = scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        [scrollView addSubview:controller.view];
    }

}




// called when scrolled by page control
- (void)scrollViewDidScroll:(UIScrollView *)sender 
{
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    if (pageControlUsed)
	{
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
	
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
	
    // A possible optimization would be to unload the views+controllers which are no longer visible
}

// when user start to drag on the page
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
	pageControlUsed = NO;
}


// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

// When page changed then this method called
- (IBAction)changePage:(id)sender
{
    int page = (int)pageControl.currentPage;
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    // update the scroll view to the appropriate page
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [scrollView scrollRectToVisible:frame animated:YES];
    // Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}

// left button touched event handling , show the previous page if exist
- (IBAction)leftButtonTouched
{
		// make dragging enabled for user
		pageControlUsed = NO;
		int page = (int)pageControl.currentPage;
		if (page <= 0) return;
		if (page >= count) return;
		pageControl.currentPage = page - 1;
		[self AutoChangePages:page - 1];
}

// Right button touched event handling, show the next page if available
- (IBAction)rightButtonTouched
{

		// make dragging enabled for user
		pageControlUsed = NO;
		
		int page = (int)pageControl.currentPage;
		if (page < 0) return;
		if (page >= count) return;
		pageControl.currentPage = page+1;
		[self AutoChangePages:page + 1];
}

//When Click send user to previous screen
-(IBAction)crossButtonTouched
{
    if(imgView)
        [imgView removeFromSuperview];
    
        MainMenuViewController *imainPageViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:imainPageViewController animated:NO];
       // [imainPageViewController release];
    
}

// To play the slide automatically
- (void)AutoChangePages:(int)page
{	
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    // update the scroll view to the appropriate page
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [scrollView scrollRectToVisible:frame animated:YES];
    // Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}

- (void)setImageArray:(NSMutableArray*)arr
{
    if(ImageArrayControllers)
    {
        //[ImageArrayControllers release];
        ImageArrayControllers = nil;
    }
    ImageArrayControllers = [[NSMutableArray alloc]initWithArray:arr];
}

// Set the info which is to draw
- (void)setItemInfoCount:(int)data
{
	count = data;
}

// Implement viewDidLoad to do additional setup after loading the view.
- (void)viewDidLoad 
{
	// This is to set the previous button disabled at the start
		//preToolButton.enabled = NO;
	[super viewDidLoad];
}

// call every time when view will be appear
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view setBackgroundColor:[UIColor colorWithRed:0.4980 green:0.4980 blue:0.4980 alpha:1]];
		// view controllers are created lazily
		// in the meantime, load the array with placeholders which will be replaced on demand
		viewControllers = [[NSMutableArray alloc] init];
		for (unsigned i = 0; i < count; i++)
		{
			[viewControllers addObject:[NSNull null]];
		}
		
		// a page is the width of the scroll view
		scrollView.pagingEnabled = YES;
		scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * count, scrollView.frame.size.height);
		scrollView.showsHorizontalScrollIndicator = NO;
		scrollView.showsVerticalScrollIndicator = NO;
		scrollView.scrollsToTop = NO;
		scrollView.delegate = self;
        pageControl.currentPage = 0;
		pageControl.numberOfPages = count;
		scrollView.scrollEnabled = YES;
		pageControlUsed = NO;
		
		scrollView.userInteractionEnabled = YES;
		toolBar.hidden = NO;
		// pages are created on demand
		// load the visible page
		// load the page on either side to avoid flashes when the user starts scrolling

    //customize back button
    UIButton *backBtn = [UtilityManager customizeBarButtonItem:MAINMENU];
    
    [backBtn addTarget:self action:@selector(MainMenuClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = back;
    self.navigationItem.hidesBackButton = YES;
    //[back release];
    
    
   imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ScanSeeTransparentLogo.png"]];
    self.navigationItem.titleView = imgView;
   // [imgView release];
    
    UILabel *screenName = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 70, 25)];
    screenName.text = @"Tips";
    screenName.backgroundColor = [UIColor clearColor];
    screenName.textColor = [UIColor whiteColor];
    screenName.font = [UIFont systemFontOfSize:12];
    UIBarButtonItem *lb = [[UIBarButtonItem alloc] initWithCustomView:screenName];
    self.navigationItem.rightBarButtonItem = lb;
    
    //[screenName release];
   // [lb release];
    
		[self loadScrollViewWithPage:0];
		//[self loadScrollViewWithPage:1];
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
}


-(void)MainMenuClicked
{
    if(imgView)
        [imgView removeFromSuperview];

        MainMenuViewController *imainPageViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:imainPageViewController animated:NO];
       // [imainPageViewController release];
}


- (void)removeallMemory
{
    if(viewControllers)
    {
        for(int i=0; i< [viewControllers count]; i++)
        {
            
            if([[viewControllers objectAtIndex:i] isKindOfClass:[SlideImageDispalyViewController class]])
            {
                [[[viewControllers objectAtIndex:i]view]removeFromSuperview];
            }
            
           // [[viewControllers objectAtIndex:i]release];
        }
        //[viewControllers release];
    }
   /* if(ImageArrayControllers)
    {
        for(int i=0; i< [ImageArrayControllers count]; i++)
        {
            [[ImageArrayControllers objectAtIndex:i]release];
        }
        [ImageArrayControllers release];
    }*/
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


// free all the memory


@end


//
//  SlideImageDispalyViewController
//  Scansee
//  @Description -  Class to play and handle the slide show
//  Created by Deepak Agarwal on 22nd April 2013
//  Copyright SPANSYSTEm INC. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface SlideImageDispalyViewController : UIViewController <UITextViewDelegate,NSURLConnectionDelegate>
{
   // IBOutlet UILabel *pageNumberLabel;
    int pageNumber;
	NSString *encURL;
	
	BOOL isImageDone;
	UIActivityIndicatorView *fetchingImage;
	NSMutableData		*imageDataFromServer;
	NSURLConnection		*httpConnection;
	UIImageView *imageView;
	UIImage *image;
}

- (UIImage*)getImageData;
- (id)initWithPageNumber:(int)page;
- (void)setEncURL:(NSString *)URL;
- (BOOL)getIsimageDone;
- (void)connection:	(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;  
- (void)connection:	(NSURLConnection *)connection didReceiveData:(NSData *)data;  
- (void)connection:	(NSURLConnection *)connection didFailWithError:(NSError *)error;  
- (void)connectionDidFinishLoading:	(NSURLConnection *)connection; 
- (void)cancelHttpCommunications;
- (void)makeImageRequest;
- (void)drawImageOnScreen;
@end

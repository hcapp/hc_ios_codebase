//
//  SlideImageDispalyViewController.m
//  Scansee
//  @Description -  Class to play and handle the slide show. This will load the image from HTTP on the view
//  Created by Deepak Agarwal on 22nd April 2013
//  Copyright SPANSYSTEMs INC. All rights reserved.
//



#import "SlideImageDispalyViewController.h"

@implementation SlideImageDispalyViewController


// Load the view nib and initialize the pageNumber ivar.
- (id)initWithPageNumber:(int)page
{
    if (self = [super initWithNibName:@"SlideShowView" bundle:nil])
	{
        pageNumber = page;
    }
    return self;
}



// Set the label and background color when the view has finished loading.
- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.view.backgroundColor = [UIColor clearColor];
	[self makeImageRequest];
}



// set the enclosure url
- (void)setEncURL:(NSString *)URL
{
	//[encURL release];
	encURL = URL;
	//[encURL ;
}



// Make the request for Image to server
- (void)makeImageRequest
{    
	isImageDone = NO;
	// Make request for enclouser url for image
	// start animation till get response

    // Create the request.
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:encURL]
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:60.0];
    // create the connection with the request
    // and start loading the data
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (theConnection) {
        // Create the NSMutableData to hold the received data.
        // receivedData is an instance variable declared elsewhere.
        imageDataFromServer = [NSMutableData data];
    } else {
        // Inform the user that the connection failed.
    }
	if (fetchingImage)
	{
		[fetchingImage removeFromSuperview];
		[fetchingImage stopAnimating];
		//[fetchingImage release];
	}
	[self.view clearsContextBeforeDrawing];
	self.view.backgroundColor = [UIColor clearColor];
	
	fetchingImage = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
	CGRect screen = [UIScreen mainScreen].bounds;
	CGRect newFrame = CGRectMake(screen.size.width/2 - fetchingImage.frame.size.width/2, (screen.size.height/2 - fetchingImage.frame.size.height/2)-70, fetchingImage.frame.size.width, fetchingImage.frame.size.height);
	fetchingImage.frame = newFrame;
	[fetchingImage startAnimating];	
	[self.view addSubview:fetchingImage];
	[self.view setNeedsDisplay];
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response  
{
    [imageDataFromServer setLength:0];
	// Initialise the mutable data object
}  



- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data  
{  
	[imageDataFromServer appendData:data];
}  



- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error  
{  
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

	// Release the imageDataFromServer
	//[imageDataFromServer release];
	
	// stop the animation
	if (fetchingImage)
	{
		[fetchingImage removeFromSuperview];
		[fetchingImage stopAnimating];
		//[fetchingImage release];
	}
	// free connection
	//[connection release];
	httpConnection = nil;
	// Image scaling work to dispaly in slide show, scale in proper ratio of width and hight.
	//[imageView release];
	imageView = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@""]];
	[self drawImageOnScreen];	
}  



// Recive the response from the server
- (void)connectionDidFinishLoading:(NSURLConnection *)connection  
{  
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	// Load the new image
	//[image release];
	image = [[UIImage alloc] initWithData: [NSData dataWithBytes:[imageDataFromServer mutableBytes] length:[imageDataFromServer length]]];
	
	// Release the imageDataFromServer
	//[imageDataFromServer release];
	
	// stop the animation
	if (fetchingImage)
	{
		[fetchingImage removeFromSuperview];
		[fetchingImage stopAnimating];
		//[fetchingImage release];
	}
	
	// free connection
	//[connection release];
	httpConnection = nil;
	// Image scaling work to dispaly in slide show, scale in proper ratio of width and hight.
	//[imageView release];
	imageView = [[UIImageView alloc] initWithImage: image];
	[self drawImageOnScreen];
}  




// Method to draw the image on the screen after scalling of image
- (void)drawImageOnScreen
{
	CGRect frame = imageView.frame;
	CGSize framesize = frame.size;
	float availableScalingWidth = 320.0;
	float availableScalingHeight = self.view.frame.size.height;
	float scalingHeight = 0;
	float scalingWidth = 0;
	CGRect imgFrame;
	if(framesize.width > availableScalingWidth || framesize.height > availableScalingHeight)
	{
		// if width is grater
		if(framesize.width > framesize.height)
		{
			float widthHeightPercent = (framesize.width * 100) / framesize.height;
			scalingHeight = (availableScalingWidth * 100) / widthHeightPercent;
			scalingWidth = (scalingHeight * widthHeightPercent) / 100;
		}
		// if height is graters
		else if(framesize.height >= framesize.width)
		{
			float widthHeightPercent = (framesize.height * 100) / framesize.width;
			scalingWidth = (availableScalingHeight * 100) / widthHeightPercent;
			scalingHeight = (scalingWidth * widthHeightPercent) / 100;
		}
		float xPos = (availableScalingWidth - scalingWidth)/2;
		float yPos = (availableScalingHeight - scalingHeight)/2;
		imgFrame = CGRectMake(xPos, yPos, scalingWidth, scalingHeight);
	}
	else
	{
		float xPosition = (availableScalingWidth - framesize.width)/2;
		float yPosition = (availableScalingHeight - framesize.height)/2;
		imgFrame = CGRectMake( xPosition, yPosition, framesize.width, framesize.height);
	}
	imageView.frame = imgFrame;
	[self.view addSubview:imageView];
	isImageDone = YES;	
	
	// Mark for redisplay
	
	[self.view clearsContextBeforeDrawing];
	self.view.backgroundColor = [UIColor clearColor];
	[self.view setNeedsDisplay];	
}



// Get the data image request work done or not
- (BOOL)getIsimageDone
{
	return isImageDone;
}


// Method to get the image data
- (UIImage*)getImageData
{
	return image;
}


// cancle ongoing http connection
- (void)cancelHttpCommunications
{
	if(httpConnection)
	{
		[httpConnection cancel];
		httpConnection = nil;
	}
}



@end

//
//  SlideshowViewController.h
//  Scansee
//  @Description -  Class to play and handle the slide show 
//  Created by Deepak Agarwal on 22nd April 2013
//  Copyright SPANSYSTEm INC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIEvent.h>


@interface SlideshowViewController : UIViewController <UIScrollViewDelegate,UIWebViewDelegate>
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIPageControl *pageControl;

	IBOutlet UIToolbar *toolBar;
	IBOutlet UIBarButtonItem *toolGallery;
	IBOutlet UIBarButtonItem *preToolButton;
	IBOutlet UIBarButtonItem *nextToolButton;
	int chaneToAutoPageNumber;
    NSMutableArray *viewControllers;
    NSMutableArray *ImageArrayControllers;
    // To be used when scrolls originate from the UIPageControl
    BOOL pageControlUsed;
	int count;

	UIImageView *imgView;
	IBOutlet UIBarButtonItem *barPageControl;
    
    BOOL isComingFromViewMore;
}
@property(nonatomic,strong)NSMutableArray *viewControllers;

- (IBAction)crossButtonTouched;
- (IBAction)leftButtonTouched;
- (IBAction)rightButtonTouched;
- (void)AutoChangePages:(int)page;
- (IBAction)changePage:(id)sender;
- (void)loadScrollViewWithPage:(int)page;
- (void)scrollViewDidScroll:(UIScrollView *)sender;
- (void)setItemInfoCount:(int)data;
- (void)setImageArray:(NSMutableArray*)arr;
- (void)setisComingFromViewMore:(BOOL)val;
@end

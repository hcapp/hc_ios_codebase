//
//  UIColor+ExtraColorSpecifications.m
//

@implementation UIColor(ExtraColorSpecifications)

+ (UIColor *)colorWithRGBInt:(NSUInteger)rgbInt {
    NSUInteger blue = rgbInt & 255;
    NSUInteger green = (rgbInt >> 8) & 255;
    NSUInteger red = (rgbInt >> 16) & 255;
    return [UIColor colorWithRedInt:red greenInt:green blueInt:blue alphaInt:255];
}
+ (UIColor *)colorWithRGBAInt:(NSUInteger)rgbaInt {
    NSUInteger alpha = rgbaInt & 255;
    NSUInteger blue = (rgbaInt >> 8) & 255;
    NSUInteger green = (rgbaInt >> 16) & 255;
    NSUInteger red = (rgbaInt >> 24) & 255;
    return [UIColor colorWithRedInt:red greenInt:green blueInt:blue alphaInt:alpha];    
}
+ (UIColor *)colorWithRedInt:(NSUInteger)red greenInt:(NSUInteger)green blueInt:(NSUInteger)blue alphaInt:(NSUInteger)alpha {
    if (red > 255) red = 255;
    if (green > 255) green = 255;
    if (blue > 255) blue = 255;
    if (alpha > 255) alpha = 255;
    return [UIColor colorWithRed:red / 255.0 green:green / 255.0 blue:blue / 255.0 alpha:alpha / 255.0];
}

+ (UIColor *)convertToHexString:(NSString *)stringToConvert
{
    NSString *noHashString = [stringToConvert stringByReplacingOccurrencesOfString:@"#" withString:@""]; // remove the #
    NSScanner *scanner = [NSScanner scannerWithString:noHashString];
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]]; // remove + and $
    
    unsigned hex;
    if (![scanner scanHexInt:&hex]) return nil;
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:1.0f];
}

@end

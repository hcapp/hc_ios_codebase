//
//  UIColor+ExtraColorSpecifications.h


@interface UIColor(ExtraColorSpecifications)
+ (UIColor *)colorWithRGBInt:(NSUInteger)rgbInt; // Assumes alpha is 0xFF
+ (UIColor *)colorWithRGBAInt:(NSUInteger)rgbaInt;
+ (UIColor *)colorWithRedInt:(NSUInteger)red greenInt:(NSUInteger)green blueInt:(NSUInteger)blue alphaInt:(NSUInteger)alpha;
+ (UIColor *)convertToHexString:(NSString *)stringToConvert;
@end

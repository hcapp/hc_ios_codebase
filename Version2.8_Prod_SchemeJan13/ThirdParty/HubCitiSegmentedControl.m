//
//  ScanSeeSegmentedControl.m
//  SegmentedControlTest
//
//  Created by Chance Ivey on 9/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HubCitiSegmentedControl.h"

@implementation HubCitiSegmentedControl

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
          [self addActions];
        
        // Initialization code
    }
       
    return self;
}

-(id)initWithItems:(NSArray *)items
{
    self = [super initWithItems:items];
    if (self) {
    }
    [self addActions];
    [self performSelector:@selector(setSegmentColor)];
   
    return self;
    
}

-(void)setSelectedSegmentIndex:(NSInteger)selectedSegmentIndex
{
    [super setSelectedSegmentIndex:selectedSegmentIndex];
    [self setSegmentColor];
}

-(void) addActions
{
    [self setSegmentColor];
    
    
    
    [self setSegmentedControlStyle:UISegmentedControlStyleBar];
    [self addTarget:self action:@selector(setSegmentColor) forControlEvents:UIControlEventAllEvents];
}
-(void) setSegmentColor
{
    
    for (int i=0; i<[self.subviews count]; i++) 
    {
        if ([[self.subviews objectAtIndex:i]isSelected] ) 
        {               
            UIColor *tintcolor=[UIColor colorWithRed:80/255.0 green:199/255.0 blue:228/255.0 alpha:1.0];
            [[self.subviews objectAtIndex:i] setTintColor:tintcolor];
            //break;
        }
        else {
            UIColor *tintcolor=[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0];
            [[self.subviews objectAtIndex:i] setTintColor:tintcolor];
        }
    }
    
    //customized the text color as part of ios7 fix
    float segmentFont = 14.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        segmentFont = 18.0;
    }
    UIColor *textColor = [UIColor whiteColor];
    UIFont *textFont = [UIFont boldSystemFontOfSize:segmentFont];
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName : textFont,
                                 NSForegroundColorAttributeName : textColor};
    [self setTitleTextAttributes:attributes
                        forState:UIControlStateNormal];
    //end

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

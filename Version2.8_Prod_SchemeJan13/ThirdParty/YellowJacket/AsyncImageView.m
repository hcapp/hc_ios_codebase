//
//  AsyncImageView.m
//  YellowJacket
//
//  Created by Wayne Cochran on 7/26/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "AsyncImageView.h"
#import "ImageCacheObject.h"
#import "ImageCache.h"

//
// Key's are URL strings.
// Value's are ImageCacheObject's
//
static ImageCache *imageCache = nil;

@implementation AsyncImageView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    // Drawing code
}


- (void)dealloc {
    [connection cancel];
    [connection release];
    [data release];
    [spinnyAI release];
    [super dealloc];
}

-(void)loadImageFromURL:(NSURL*)url {
    
    if (connection != nil) {
        [connection cancel];
        [connection release];
        connection = nil;
    }
    if (data != nil) {
        [data release];
        data = nil;
    }
    
    if (imageCache == nil) // lazily create image cache
        imageCache = [[ImageCache alloc] initWithMaxSize:4*1024*1024];  // 4 MB Image cache
    
    [str release];
    str = [[url absoluteString] copy];
    
    UIImage *cachedImage = [imageCache imageForKey:str];
    
    if (cachedImage != nil) {
        if ([[self subviews] count] > 0) {
            [[[self subviews] objectAtIndex:0] removeFromSuperview];
        }
        UIImageView *imageView = [[[UIImageView alloc] initWithImage:cachedImage] autorelease];
        imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.autoresizingMask =
        UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:imageView];
        imageView.frame = self.bounds;
        [imageView setNeedsLayout]; // is this necessary if superview gets setNeedsLayout?
        [self setNeedsLayout];
        return;
    }
    
#define SPINNY_TAG 5555
    
    spinnyAI = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinnyAI.tag = SPINNY_TAG;
    CGPoint pt = CGPointMake(self.center.x - self.frame.origin.x, self.center.y - self.frame.origin.y);
    spinnyAI.center = pt;
    [spinnyAI startAnimating];
    [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(timerMethod:)
                                   userInfo:nil repeats:YES];
    [self addSubview:spinnyAI];
    // [spinnyAI release];
    
    //#ifndef TIME_CUSTOMINTERVAL
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:REAL_DEFAULT_TIMEINTERVAL];
    
    /*
     #else
     
     NSURLRequest *request = [NSURLRequest requestWithURL:url
     cachePolicy:NSURLRequestUseProtocolCachePolicy
     timeoutInterval:REAL_CUSTOM_TIMEINTERVAL];
     
     #endif
     */
    //NSTimer *timerConnection = [[NSTimer alloc] init];
    //timerWithTimeInterval:(NSTimeInterval)ti invocation:nil repeats:NO;
    //NSTimeInterval timeInterval;
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    //[NSTimer timerWithTimeInterval:120.0 target:self selector:@selector(stopAsyncConnection) userInfo:nil repeats:NO];
    
    //[self connectionDidFinishLoading:connection];
}

// Display and load image from the local app bundle
-(void)loadImagefrommainBundel:(NSString*)imageName
{
    UIImage *image = [UIImage imageNamed:imageName];
    if (image)
    {
        UIImageView *imageView = [[[UIImageView alloc]
                                   initWithImage:image] autorelease];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.autoresizingMask =
        UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:imageView];
        imageView.frame = self.bounds;
        [imageView setNeedsLayout]; // is this necessary if superview gets setNeedsLayout?
        [self setNeedsLayout];
    }
}

-(void)loadImage:(NSString*)urlStr {
    
    if (urlStr == nil || [urlStr isEqualToString:@"N/A"])
        urlStr = @"https://app.scansee.net/Images/imageNotFound.png";
    
    if (connection != nil) {
        [connection cancel];
        [connection release];
        connection = nil;
    }
    if (data != nil) {
        [data release];
        data = nil;
    }
    
    if (imageCache == nil) // lazily create image cache
        imageCache = [[ImageCache alloc] initWithMaxSize:4*1024*1024];  // 2 MB Image cache
    
    NSURL *url;
    
    if ([urlStr rangeOfString:@"%"].location == NSNotFound){
        url = [NSURL URLWithString:[NSString stringWithString:[urlStr stringByAddingPercentEncodingWithAllowedCharacters:[UtilityManager getAllowedNSCharacterSet]]]];
    }
    
    else{
        url = [NSURL URLWithString:[NSString stringWithString:urlStr]];
    }
    
    str = [[url absoluteString] copy];
    UIImage *cachedImage = [imageCache imageForKey:str];
    
    if (cachedImage != nil) {
        if ([[self subviews] count] > 0) {
            [[[self subviews] objectAtIndex:0] removeFromSuperview];
        }
        UIImageView *imageView = [[[UIImageView alloc] initWithImage:cachedImage] autorelease];
        imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.autoresizingMask =
        UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:imageView];
        imageView.frame = self.bounds;
        [imageView setNeedsLayout]; // is this necessary if superview gets setNeedsLayout?
        [self setNeedsLayout];
        return;
    }
    
#define SPINNY_TAG 5555
    
    spinnyAI = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinnyAI.tag = SPINNY_TAG;
    CGPoint pt = CGPointMake(self.center.x - self.frame.origin.x, self.center.y - self.frame.origin.y);
    spinnyAI.center = pt;
    [spinnyAI startAnimating];
    [NSTimer scheduledTimerWithTimeInterval:REAL_DEFAULT_TIMEINTERVAL target:self selector:@selector(timerMethod:)
                                   userInfo:nil repeats:YES];
    [self addSubview:spinnyAI];
    // [spinnyAI release];
    
    //#ifndef TIME_CUSTOMINTERVAL
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:REAL_DEFAULT_TIMEINTERVAL];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}


-(void) timerMethod : (NSTimer *) theTimer
{
    [spinnyAI stopAnimating];
    [theTimer invalidate];
    [connection cancel];
    connection = nil;
    
}

-(void)stopAsyncConnection
{
    [self connectionDidFinishLoading:connection];
}

- (void)connection:(NSURLConnection *)connection
    didReceiveData:(NSData *)incrementalData {
    if (data==nil) {
        data = [[NSMutableData alloc] init];
    }
    [data appendData:incrementalData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)aConnection {
    [connection release];
    connection = nil;
    
    UIView *spinny = [self viewWithTag:SPINNY_TAG];
    [spinny removeFromSuperview];
    
    if ([[self subviews] count] > 0) {
        [[[self subviews] objectAtIndex:0] removeFromSuperview];
    }
    
    UIImage *image = [UIImage imageWithData:data];
    if (image){
        
        [imageCache insertImage:image withSize:[data length] forKey:str];
        
        UIImageView *imageView = [[[UIImageView alloc]
                                   initWithImage:image] autorelease];
        imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.autoresizingMask =
        UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:imageView];
        imageView.frame = self.bounds;
        [imageView setNeedsLayout]; // is this necessary if superview gets setNeedsLayout?
        [self setNeedsLayout];
        [data release];
        data = nil;
    }
}

#pragma mark nsurlconnection methods
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}
-(void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

@end

//
//  AsyncImageView.h
//  YellowJacket
//
//  Created by Wayne Cochran on 7/26/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//


//
// Code heavily lifted from here:
// http://www.markj.net/iphone-asynchronous-table-image/
//

#import <UIKit/UIKit.h>


@interface AsyncImageView : UIView <NSURLConnectionDelegate>{
    NSURLConnection *connection;
    NSMutableData *data;
    NSString *str; // key for image cache dictionary
    UIActivityIndicatorView *spinnyAI;
}

-(void)loadImageFromURL:(NSURL*)url;
-(void)loadImage:(NSString*)str;
-(void) timerMethod : (NSTimer *) theTimer;
-(void)loadImagefrommainBundel:(NSString*)imageName;
@end

//
//  Category.h
//  CustomCellTest
//
//  Created by Punit Sindhwani on 7/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Category : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *list;
@property (nonatomic, strong) NSString *name1;
@property (nonatomic, strong) NSArray *list1;

@end

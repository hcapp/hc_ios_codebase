


//
//  NewsDetailViewController.m
//  pageSwipe
//
//  Created by Ashika on 5/24/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import "NewsDetailViewController.h"
#import "HubCitiConstants.h"
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "HTTPClient.h"
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "WebBrowserViewController.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "AnyViewController.h"

@interface NewsDetailViewController ()<HTTPClientDelegate>{
    NSMutableArray *detailScreenArray;
    float yPos;
    NSInteger webviewHeight;
    BOOL webViewload;
    UIActivityIndicatorView *activityIndicator;
}

@end

@implementation NewsDetailViewController
@synthesize newsID,anyVC,emailSendingVC;
BOOL shareClikedFlag;
#pragma mark - Service Respone methods
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
 }

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    DLog(@"parameter: %@",responseObject);
    
    [self responseData:responseObject];
    
    
}




- (void)viewDidLoad {
    [super viewDidLoad];
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    scrollView.delegate = self;
    scrollView.scrollEnabled = YES;
    [self.view addSubview:scrollView];
    
    //activity indicator
   
    activityIndicator = [[UIActivityIndicatorView alloc]
                         initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame = CGRectMake(SCREEN_WIDTH/2 - VARIABLE_WIDTH(11), SCREEN_HEIGHT/2- VARIABLE_HEIGHT(30) , VARIABLE_WIDTH(22), VARIABLE_HEIGHT(22));
    //activityIndicator.center = self.view.center;
    [scrollView addSubview:activityIndicator];

   
    self.title =@"Details";
    
    //home button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
    
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;

    //customized back button navigation bar
    
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    self.navigationItem.hidesBackButton = YES;
    
    
    detailScreenArray = [[NSMutableArray alloc] init];
    [self requestForDetailScreen];
    //share button
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

-(void) popBackToPreviousPage{
    // [defaults setValue:@"mainmenu" forKey:@"mainmenu"];
    [defaults setBool:YES forKey:@"isComingFromDetail"];
    [defaults setBool:NO forKey:@"isCommingFromWebView"];
    [self.navigationController popViewControllerAnimated:NO];
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    // Create down menu button
    
    
    UIImageView *homeLabel = [self createHomeButtonView];
    // Create up menu button
    upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - homeLabel.frame.size.width - 10.f,
                                                                      self.view.frame.size.height - homeLabel.frame.size.height - 10.f,
                                                                      homeLabel.frame.size.width,
                                                                      homeLabel.frame.size.height)
                                        expansionDirection:DirectionUp];
    upMenuView.homeButtonView = homeLabel;
    upMenuView.delegate = self;
    [upMenuView addButtons:[self createDemoButtonArray]];
    [self.view addSubview:upMenuView];
    upMenuView.onLogOutPressed =^(){
        [self logOutPressed];
    } ;

    
}
-(void) requestForDetailScreen{
    
    [HubCitiAppDelegate showActivityIndicator];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    NSString *urlString = [NSString stringWithFormat:@"%@band/getnewsdetail?newsID=%@",BASE_URL,newsID];
    NSLog(@"Parameters:%@",parameter);
    
    HTTPClient *client = [[HTTPClient alloc] init];
    client.delegate = self;
    DLog(@"Url: %@",urlString);
    [client sendGetRequest : nil : urlString];
    
}
-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(void) responseData: (id) responseObject
{
    [activityIndicator startAnimating];
   
    NSDictionary *responseData=responseObject;
    
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    if(responseCode == 10000){
        
        detailScreenArray = [[responseData objectForKey:@"items"] mutableCopy];
        float descFontSize;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
            descFontSize=18;
        }
        else
        {
            descFontSize=16;
        }
        CGFloat ypos=0;
        
        CGFloat titleHeight = [self getLabelSize:[[detailScreenArray objectAtIndex:0] objectForKey:@"title"] : descFontSize];
        ypos = ypos+titleHeight + VARIABLE_HEIGHT(5);
        if([[detailScreenArray objectAtIndex:0] objectForKey:@"image"]){
            ypos = ypos+VARIABLE_HEIGHT(150)+ VARIABLE_HEIGHT(5);
        }
        if ([[detailScreenArray objectAtIndex:0] objectForKey:@"author"]) {
            CGFloat titleHeight = [self getLabelSize:[[detailScreenArray objectAtIndex:0] objectForKey:@"author"]];
            ypos = ypos+titleHeight + VARIABLE_HEIGHT(5);
        }
        if ([[detailScreenArray objectAtIndex:0] objectForKey:@"date"])
        {
            ypos = ypos+VARIABLE_HEIGHT(16) + VARIABLE_HEIGHT(5);
            
        }
        webViewload = TRUE;
        longDescWebView = [[UIWebView alloc]init];
        longDescWebView.delegate = self;
        CGRect frame;
        frame.origin.x = VARIABLE_WIDTH(5);
        frame.origin.y = ypos + VARIABLE_HEIGHT(10);
        frame.size.width = SCREEN_WIDTH - 2*VARIABLE_WIDTH(10);
        frame.size.height = webviewHeight;
        longDescWebView.frame = frame;
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#000;word-wrap: break-word'>%@",[[detailScreenArray objectAtIndex:0] objectForKey:@"lDesc"]] baseURL:nil];
        }
        else
        {
            [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:15px;font-family:Helvetica;color:#000;word-wrap: break-word'>%@",[[detailScreenArray objectAtIndex:0] objectForKey:@"lDesc"]] baseURL:nil];
        }
        
        
        
    }
    else{
        [UtilityManager showAlert:nil msg:responseText];
        return;
    }
    
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //    Math.max( document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight );
    NSString *output = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"];
    NSLog(@"height: %@", output);
    webviewHeight = [output integerValue];
    if(webViewload){
        webViewload = FALSE;
        [self setupUI];
    }
    
}

-(void) setupUI {
    float descFontSize;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
        descFontSize=18;
    }
    else
    {
        descFontSize=16;
    }
    
    UILabel *titleLbl;
    CGFloat titleHeight = [self getLabelSize:[[detailScreenArray objectAtIndex:0] objectForKey:@"title"] : descFontSize];
    titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(10),VARIABLE_HEIGHT(5), SCREEN_WIDTH - VARIABLE_WIDTH(20), titleHeight)];
    titleLbl.numberOfLines = 0;
    titleLbl.textAlignment = NSTextAlignmentCenter;
    //  titleLbl.lineBreakMode = NSLineBreakByWordWrapping;
    titleLbl.text =[[detailScreenArray objectAtIndex:0] objectForKey:@"title"];
    titleLbl.textColor = [UIColor blackColor];
    titleLbl.font = [UIFont boldSystemFontOfSize:descFontSize];
    [scrollView addSubview:titleLbl];
    
    yPos = yPos+titleLbl.frame.size.height + VARIABLE_HEIGHT(5);
    
    
    // ImageView
    SdImageView *asyncImageView ;
    if([[detailScreenArray objectAtIndex:0] objectForKey:@"image"]){
        asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(30),yPos+VARIABLE_HEIGHT(10), SCREEN_WIDTH - 2*VARIABLE_WIDTH(30), VARIABLE_HEIGHT(150))];
        
        asyncImageView.backgroundColor = [UIColor clearColor];
        asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
        [asyncImageView loadImage:[[detailScreenArray objectAtIndex:0] objectForKey:@"image"]];
        
        [scrollView addSubview:asyncImageView];
        
        yPos = yPos+asyncImageView.frame.size.height + VARIABLE_HEIGHT(5);
        
    }
    
    UILabel* author;
    
    if ([[detailScreenArray objectAtIndex:0] objectForKey:@"author"]) {
        CGFloat titleHeight = [self getLabelSize:[[detailScreenArray objectAtIndex:0] objectForKey:@"author"]];
        
        author = [[UILabel alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(10),yPos + VARIABLE_HEIGHT(10), SCREEN_WIDTH - VARIABLE_WIDTH(10), titleHeight)];
        author.text =[NSString stringWithFormat:@"Author: %@", [[detailScreenArray objectAtIndex:0] objectForKey:@"author"]];
        author.textColor = [UIColor grayColor];
        titleLbl.numberOfLines = 0;
        // titleLbl.lineBreakMode = NSLineBreakByWordWrapping;
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
            author.font = [UIFont boldSystemFontOfSize:15];
        }
        else{
            author.font = [UIFont boldSystemFontOfSize:12];
        }
        [scrollView addSubview:author];
        
        yPos = yPos+author.frame.size.height + VARIABLE_HEIGHT(5);
        
    }
    
    UILabel *dateLbl;
    
    if ([[detailScreenArray objectAtIndex:0] objectForKey:@"date"])
    {
        dateLbl = [[UILabel alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(10),yPos + VARIABLE_HEIGHT(10), SCREEN_WIDTH - VARIABLE_WIDTH(10), VARIABLE_HEIGHT(16))];
        dateLbl.text =[NSString stringWithFormat:@"Published On: %@  %@",[[detailScreenArray objectAtIndex:0] objectForKey:@"date"],[[detailScreenArray objectAtIndex:0] objectForKey:@"time"]];
        dateLbl.textColor = [UIColor grayColor];
        if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
            dateLbl.font = [UIFont boldSystemFontOfSize:15];
        }
        else{
            dateLbl.font = [UIFont boldSystemFontOfSize:12];
        }
        [scrollView addSubview:dateLbl];
        
        yPos = yPos+dateLbl.frame.size.height+ VARIABLE_HEIGHT(5);
    }
    
    //    UILabel* sDesc;
    //    if([[detailScreenArray objectAtIndex:0] objectForKey:@"sDesc"])
    //    {
    //        sDesc = [[UILabel alloc] init];
    //        sDesc.text = [[detailScreenArray objectAtIndex:0] objectForKey:@"sDesc"];
    //        sDesc.font = [UIFont boldSystemFontOfSize:14];
    //        sDesc.numberOfLines = 0;
    //        CGFloat height = [self calculateLabelHeight: sDesc.text labelWidth:SCREEN_WIDTH-2*VARIABLE_HEIGHT(15) fontSize:14];
    //        sDesc.frame = CGRectMake(VARIABLE_WIDTH(5), yPos , SCREEN_WIDTH - 2*VARIABLE_WIDTH(10),height);
    //
    //        [scrollView addSubview:sDesc];
    //
    //         yPos = yPos + sDesc.frame.size.height+ VARIABLE_HEIGHT(5);
    //    }
    
    
    
    if([[detailScreenArray objectAtIndex:0] objectForKey:@"lDesc"])
    {
        NSString* htmlWithoutTag = [self stringByStrippingHTML : [[detailScreenArray objectAtIndex:0] objectForKey:@"lDesc"]];
        CGFloat height;
        if(IPAD)
            height= [self calculateLabelHeight: htmlWithoutTag labelWidth:SCREEN_WIDTH-2*VARIABLE_HEIGHT(10) fontSize:15];
        
        else
            height= [self calculateLabelHeight: htmlWithoutTag labelWidth:SCREEN_WIDTH-2*VARIABLE_HEIGHT(10) fontSize:13];
        NSLog(@"%f",height);
        longDescWebView = [[UIWebView alloc]init];
        longDescWebView.delegate = self;
        longDescWebView.opaque = NO;
        longDescWebView.scrollView.bounces = NO;
        longDescWebView.layer.cornerRadius = 5.0;
        longDescWebView.backgroundColor = [UIColor clearColor];
        longDescWebView.frame = CGRectMake(VARIABLE_WIDTH(5), yPos + VARIABLE_HEIGHT(10), SCREEN_WIDTH - 2*VARIABLE_WIDTH(10),webviewHeight);
        longDescWebView.backgroundColor = [UIColor clearColor];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:#000;word-wrap: break-word'>%@",[[detailScreenArray objectAtIndex:0] objectForKey:@"lDesc"]] baseURL:nil];
        }
        else
        {
            [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:15px;font-family:Helvetica;color:#000;word-wrap: break-word'>%@",[[detailScreenArray objectAtIndex:0] objectForKey:@"lDesc"]] baseURL:nil];
        }
        
        
        [scrollView addSubview:longDescWebView];
        
        yPos = yPos+ longDescWebView.frame.size.height + VARIABLE_HEIGHT(5);
    }
    
    if([[detailScreenArray objectAtIndex:0] objectForKey:@"videoLink"]){
        
        CGRect frame = CGRectMake(VARIABLE_WIDTH(5), yPos + VARIABLE_HEIGHT(10), SCREEN_WIDTH - 2*VARIABLE_WIDTH(10),VARIABLE_HEIGHT(200));
        NSURL *videoURL = [NSURL URLWithString: [[detailScreenArray objectAtIndex:0] objectForKey:@"videoLink"]];
        [self playVideo : frame : videoURL];
        
    }
    [activityIndicator stopAnimating];
    [scrollView setContentSize:CGSizeMake(SCREEN_WIDTH, yPos + VARIABLE_HEIGHT(50))];
    scrollView.contentInset = UIEdgeInsetsMake(0, 0, VARIABLE_HEIGHT(10), 0);
    
    
}

-(void) playVideo : (CGRect) frame : (NSURL*) url
{
    
//    if([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
//    {
//        
//        _moviePlayer = [[MPMoviePlayerViewController alloc] init];
//        _moviePlayer.view.frame = frame;
//        [_moviePlayer.moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
//        [_moviePlayer.moviePlayer setShouldAutoplay:YES];
//        [_moviePlayer.moviePlayer setFullscreen:NO animated:YES];
//        _moviePlayer.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
//        [_moviePlayer.moviePlayer setContentURL:url];
//        [scrollView addSubview:_moviePlayer.view];
//        [_moviePlayer.moviePlayer prepareToPlay];
//        [_moviePlayer.moviePlayer play];
//        yPos = yPos+ _moviePlayer.view.frame.size.height+5;
//    }
//    else{
    {
        AVPlayer *player = [AVPlayer playerWithURL:url];
        AVPlayerViewController *playerViewController = [AVPlayerViewController new];
        playerViewController.view.frame = frame;
        playerViewController.player = player;
        [self addChildViewController:playerViewController];
        [scrollView addSubview:playerViewController.view];
        [player pause];
        
        yPos = yPos+ playerViewController.view.frame.size.height+5;
    }
    
}
-(float) getLabelSize : (NSString*) labelString :(float)fontSize
{
    if ([labelString isEqualToString:@" "])
    {
        return 0;
    }
    float descFontSize;
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
        descFontSize=fontSize;
    }
    else
    {
        descFontSize=fontSize;
    }
    CGSize constraint = CGSizeMake(SCREEN_WIDTH, CGFLOAT_MAX);
    
    
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    CGSize size = [labelString boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:descFontSize], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    return size.height +20;
    
}

-(float) getLabelSize : (NSString*) labelString
{
    if ([labelString isEqualToString:@" "])
    {
        return 0;
    }
    float descFontSize;
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
        descFontSize=15;
    }
    else
    {
        descFontSize=12;
    }
    CGSize constraint = CGSizeMake(SCREEN_WIDTH, CGFLOAT_MAX);
    
    
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    CGSize size = [labelString boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:descFontSize], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    return size.height +20;

    
}

- (NSString *)stringByStrippingHTML : (NSString*) htmlStr
{
    NSRange r;
    NSString *s = htmlStr ;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound) s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

-(int)calculateLabelHeight:(NSString*)text labelWidth:(int)labelWidth fontSize:(int)fontSizeText
{
    CGSize constraint;
    CGSize size ;
    CGFloat heightWebview ;
    constraint = CGSizeMake(labelWidth, 20000.0f);
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    size = [text boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:fontSizeText], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    
    heightWebview = MAX(size.height, 40.0f);
    return heightWebview;

}


-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ([[[inRequest URL] scheme] isEqual:@"tel"]) {
        NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
        
        NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
        NSString *phoneString  = [[defaultRecipient componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        NSString *phoneNumber = [NSString stringWithFormat:@"tel:%@", phoneString];
        NSURL    *url= [NSURL URLWithString:phoneNumber];
        
        UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        webview.alpha = 0.0;
        
        [webview loadRequest:[NSURLRequest requestWithURL:url]];
        
        // Assume we are in a view controller and have access to self.view
        [self.view insertSubview:webview belowSubview:longDescWebView]; //ask ashika
        
        return NO;
    }
    if ([[[inRequest URL] scheme] isEqual:@"mailto"]) {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,
                                                                                                                   (CFStringRef)value,
                                                                                                                   CFSTR("")));
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            // [mailViewController release];
            return NO;
            
        }
        
        else {
            [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
        }
        
        return NO;
        
    }
    
    
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        
        NSString *url = [[inRequest URL]absoluteString];
        [defaults setValue:url forKey:KEY_URL];
        //[defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
        
        WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:urlDetail animated:NO];
        //[urlDetail release];
        
        return NO;
    }
    return YES;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];// ModalViewControllerAnimated:YES];
}


#pragma mark floating share button

- (UIImageView *)createHomeButtonView
{
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
    label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}

- (NSArray *)createDemoButtonArray
{
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    int i = 0;
    for (NSString *title in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:title] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        button.clipsToBounds = YES;
        button.tag = i++;
        [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
}

- (void)test:(UIButton *)sender {
    
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
    shareClikedFlag=TRUE;
    [self shareClicked];
    
    if (shareClikedFlag==TRUE) {
        
        
        switch (sender.tag) {
            case 0:{//facebook
                anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
                
                
                /////////change type
                
                anyVC.fbShareType=nil;
                [UIView transitionWithView:self.view duration:0.5
                                   options:UIViewAnimationOptionTransitionCurlDown
                                animations:^ { [self.view addSubview:anyVC.view]; }
                                completion:nil];
            }
                break;
            case 1:{//twitter
                if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                    
                    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                    
                    SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                        if (result == SLComposeViewControllerResultCancelled) {
                            DLog(@"Twitter Result: canceled");
                        } else {
                            DLog(@"Twitter Result: sent");
//
                        }
                        
                        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                            
                            [controller.view removeFromSuperview];
                        } else
                            [controller dismissViewControllerAnimated:YES completion:Nil];
                    };
                    controller.completionHandler =myBlock;
                    
                    //Adding the Text to the facebook post value from iOS
                    [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:@"twitterNewsshare"],[defaults valueForKey:KEY_HYPERLINK]]];
                
                    
                    [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        [self.view addSubview:controller.view];
                        [self.view bringSubviewToFront:controller.view];
                    }else
                        [self presentViewController:controller animated:YES completion:Nil];
                    
                    
                }
                else {
                    [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
                }
            }
                
                
                break;
            case 2:{//text
                
                if ([self hasCellularCoverage])
                {
                    if ([MFMessageComposeViewController canSendText]) {
                        
                        MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
                        [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                        smsComposerController.messageComposeDelegate = self;
                        [self presentViewController:smsComposerController animated:YES completion:nil];
                    }
                }
                else
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];  // //[alert release];
                }
                
            }
                break;
            case 3:{//email
                //[defaults setValue:FUNDRAISER_SHARE forKey:KEY_SHARE_TYPE];
                emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
                emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"NEWS_SHARE"];
             ///change this
                
                
                
                
               [defaults setValue:newsID forKey:@"newsID"];
                __typeof(self) __weak  obj = self;
                emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                    
                    [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
                } ;
                
                [emailSendingVC loadMail];
            }
                break;
                
            default:
                shareClikedFlag=FALSE;
                [defaults setBool:NO forKey:BottomButton];
                break;
        }
    }
    else
    {
        switch (sender.tag)
        {
            default:
                break;
        }
        
    }
    
}
-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}
- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
        }
            break;
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
            
            //[alert release];
        }
            break;
            
        default:
            break;
    }
    
     [defaults setValue:nil forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView
{
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}

#pragma mark share coupon request response

-(void)shareClicked
{
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    [reqStr appendFormat:@"<ShareProductInfo><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [reqStr appendFormat:@"<newsId>%@</newsId>",newsID];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_HUBCITIID]];
    NSLog(@"request : %@",reqStr);
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharenewsbymedia",BASE_URL];
    NSLog(@"URl : %@",urlString);
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    NSLog(@"response %@",response);
    [self parse_shareCoupons:response];
    
}

-(void)parse_shareCoupons:(NSString*)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        
        TBXMLElement *shareText=[TBXML childElementNamed:@"title" parentElement:tbXml.rootXMLElement];
        TBXMLElement *qrURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *newsImagePath = [TBXML childElementNamed:@"image" parentElement:tbXml.rootXMLElement];
        TBXMLElement *shreMsg = [TBXML childElementNamed:@"shareText" parentElement:tbXml.rootXMLElement];
        
        if (qrURL!=nil) {
            [defaults setValue:[TBXML textForElement:qrURL] forKey:KEY_HYPERLINK];
            NSLog(@"The link to be shared is %@",KEY_HYPERLINK);
        }
        
        if (shareText!=nil) {
            NSMutableString *msgString = [[NSMutableString alloc]init];
            [msgString appendFormat:@"%@",[TBXML textForElement:shreMsg]];
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [msgString appendFormat:@"%@",shareString];
            NSLog(@"%@----------->",msgString);
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            [defaults setObject:shareString forKey:@"twitterNewsshare"];
            NSLog(@"The share message value is %@",KEY_SHAREMSG);
        }
        
        if (newsImagePath!=nil) {
            [defaults setObject:[TBXML textForElement:newsImagePath] forKey:KEY_PRODUCTIMGPATH];
            NSLog(@"The product image path is %@",KEY_PRODUCTIMGPATH);
        }
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            return;
        }
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

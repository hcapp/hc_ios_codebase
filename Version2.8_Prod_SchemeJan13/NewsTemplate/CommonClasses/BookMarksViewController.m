//
//  BookMarksViewController.m
//  HubCiti
//
//  Created by Ashika on 6/1/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "BookMarksViewController.h"
#import "HubCitiConstants.h"
#import "HTTPClient.h"
#import "AppDelegate.h"
#import "BookMarkGetRequest.h"
#import "SideNavigationResponse.h"
#import <QuartzCore/QuartzCore.h>
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "CombinationViewController.h"
#import "ScrollTemplateViewController.h"
#import "SideViewController.h"

@interface BookMarksViewController ()<HTTPClientDelegate>{
    
    NSMutableArray *bookMarkArray,*sectionsArray,*bookMarkId,*sectionId,*sideNavId,*secSideNavId;
    NSMutableArray *sideNavigationArray,*sectionsSideNavArray;
    
    NSMutableDictionary *allBookMarks,*allSections;
    NSMutableArray *row1,*row2;
    NSMutableArray *tableBookMarkLoad,*section1,*section2,*tableSideMenuLoad;
    
    UIBarButtonItem *buttonRight;
    UIButton *sectionButton, *sidenavButton;
    BOOL isbookMark,isSideNav;
    NSMutableArray *tableArrayObject;
    NSMutableArray *newsCategories,*hubcitiCategoies,*tableDataObject;
    BOOL bookMarkCliked,sideNavClicked;
    UIActivityIndicatorView *activityIndicator ;
    SdImageView *img;
    BOOL otherSection;
    CGFloat ipadvalue;
    
}
@property(strong,nonatomic) BookMarkGetRequest *bookMarkGetResponse;
@property(strong,nonatomic) BookMarkGetRequest *sideMenuGetResponse;
@property(nonatomic,strong) SideNavigationResponse *sideNavResponse;
@property(nonatomic,strong) MainMenuResponse  *menuResponse;
@end

@implementation BookMarksViewController
@synthesize sideNavResponse,menuResponse,comingFromSideNews;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Service Respone methods
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    DLog(@"parameter: %@",responseObject);
    
    [self responseData:responseObject];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    if(IPAD)
        ipadvalue= VARIABLE_HEIGHT(65);
    else
        ipadvalue = VARIABLE_HEIGHT(90);
    
    row1 = [[NSMutableArray alloc] initWithObjects:@"Top stories",@"politics",@"sports", nil];
    row2 = [[NSMutableArray alloc] initWithObjects:@"Top stories",@"news",@"government",@"tech" ,@"politics",@"sports",@"news",@"government",@"tech",nil];
    section1 = [[NSMutableArray alloc] initWithObjects:@"BookMark bar",row1, nil];
    section2 = [[NSMutableArray alloc] initWithObjects:@"Sections",row2, nil];
    
    tableBookMarkLoad =[[NSMutableArray alloc] init];
    
    bookMarkArray = [[NSMutableArray alloc] init];
    sectionsArray = [[NSMutableArray alloc] init];
    sideNavigationArray = [[NSMutableArray alloc] init];
    sectionsSideNavArray = [[NSMutableArray alloc] init];
    
    tableSideMenuLoad = [[NSMutableArray alloc] init];
    
    
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    scrollView.delegate = self;
    scrollView.scrollEnabled = YES;
    [self.view addSubview:scrollView];
    
    //customized back button navigation bar
    self.navigationItem.title = @"Settings";
    
    UIButton *btncancel = [UtilityManager customizeBarButtonItem:NSLocalizedString(@"Done",@"Done")];
    [btncancel addTarget:self action:@selector(donePressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* btncancelbar = [[UIBarButtonItem alloc] initWithCustomView:btncancel];
    self.navigationItem.rightBarButtonItem = btncancelbar;
    
    
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    self.navigationItem.hidesBackButton = YES;
    
    
    [self setButtons];
    
    // default drop down
    if(!comingFromSideNews)
    [self clickedonNavCategory: sidenavButton];
    // Do any additional setup after loading the view from its nib.
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [defaults setBool:NO forKey:@"isCommingFromWebView"];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
}


-(void) popBackToPreviousPage{
    // [defaults setValue:@"mainmenu" forKey:@"mainmenu"];
    
    [self.navigationController popViewControllerAnimated:NO];
}
#pragma set up UI
-(void) setButtonsForView: (UIButton *) button :(NSString *) labelText : (CGFloat ) yVal : (CGFloat ) height{
    
    button.frame = CGRectMake(0, yVal, SCREEN_WIDTH, height);
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor convertToHexString:@"#EFEFEF"];
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    }
    else
    {
        button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    }
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button.layer setShadowOffset:CGSizeMake(4, 4)];
    [button.layer setShadowColor:[[UIColor convertToHexString:@"#AAAAAA"] CGColor]];
    [button.layer setShadowOpacity:0.5];
    button.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [button setTitle:labelText forState:UIControlStateNormal];
    
    
    UIImageView *arrowImage;
    if(DEVICE_TYPE==UIUserInterfaceIdiomPhone)
    {
        arrowImage = [[UIImageView alloc]initWithFrame:CGRectMake(300, 17, 12, 13)];
    }
    else
    {
        arrowImage = [[UIImageView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-20, 31, 12, 13)];
    }
    [arrowImage setBackgroundColor:[UIColor clearColor]];
    [button addSubview:arrowImage];
    
    //height = height +VARIABLE_HEIGHT(36);
    [arrowImage setImage:[UIImage imageNamed:@"arrow_right"]];
    arrowImage.tag = 1;
    [self.view addSubview:button];
    
}
-(void) setButtons{
    
    NSArray *buttonData;
    if(!comingFromSideNews)
   buttonData = [[NSArray alloc] initWithObjects:@"Side Navigation", nil];
    else
   buttonData = [[NSArray alloc] initWithObjects:@"Favorites",@"Side Navigation", nil];
    
    if(!comingFromSideNews){
        sidenavButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [sidenavButton addTarget:self action:@selector(clickedonNavCategory:) forControlEvents:UIControlEventTouchUpInside];
        sidenavButton.tag = 0;
        [self setButtonsForView:sidenavButton :[buttonData objectAtIndex:0] :0 :VARIABLE_HEIGHT(36)];
    }
    
    else{
    
       for(int i=0;i<2;i++){
        if(i==0){
            sectionButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [sectionButton addTarget:self action:@selector(clickedonCategory:) forControlEvents:UIControlEventTouchUpInside];
            sectionButton.tag = i;
            [self setButtonsForView:sectionButton :[buttonData objectAtIndex:i] :0 :VARIABLE_HEIGHT(36)];
        }
        else if(i==1){
            
            sidenavButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [sidenavButton addTarget:self action:@selector(clickedonNavCategory:) forControlEvents:UIControlEventTouchUpInside];
            sidenavButton.tag = i;
            [self setButtonsForView:sidenavButton :[buttonData objectAtIndex:i] :VARIABLE_HEIGHT(38) :VARIABLE_HEIGHT(36)];
        }
    }
    
    }
    

    
}
-(void) donePressed
{
    
    [HubCitiAppDelegate showActivityIndicator];
    [self performSelector:@selector(nextOnDonePress) withObject:nil afterDelay:0.5];
}
-(void)nextOnDonePress{
   
    [defaults setBool:YES forKey:@"isDonePressed"];
    NSMutableString *BookmarkOrder=[[NSMutableString alloc]init];
    NSMutableString *NavigationID=[[NSMutableString alloc]init];
    //
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if(bookMarkCliked){
        if([bookMarkArray count]>0){
            
            
            for (int i=0; i<[bookMarkArray count]; i++) {
                [BookmarkOrder appendFormat:@"%@!~~!",[[bookMarkArray objectAtIndex:i] objectAtIndex:0]];
            }
            
            
            if ([BookmarkOrder hasSuffix:@"!~~!"]) {
                [BookmarkOrder  setString:[BookmarkOrder substringToIndex:[BookmarkOrder length]-1]];
            }
            
            [parameters setValue:BookmarkOrder forKey:@"bkMarkOrder"];
        }
        else{
            [parameters setValue:@"0" forKey:@"bkMarkOrder"];
        }
    }
    
    if(sideNavClicked){
        
        if([sideNavigationArray count]){
            for (int i=0; i<[sideNavigationArray count]; i++) {
                if( [[[sideNavigationArray objectAtIndex:i] objectAtIndex:2] intValue]  == 1)
                    [NavigationID appendFormat:@"%@-F,",[[sideNavigationArray objectAtIndex:i] objectAtIndex:1]];
                else
                    
                    [NavigationID appendFormat:@"%@-C,",[[sideNavigationArray objectAtIndex:i] objectAtIndex:1]];
            }
            
            
            if ([NavigationID hasSuffix:@","]) {
                [NavigationID  setString:[NavigationID substringToIndex:[NavigationID length]-1]];
            }
            [parameters setValue:NavigationID forKey:@"navigOrder"];
            
        }
        else{
            [parameters setValue:@"0" forKey:@"navigOrder"];
        }
        
    }
    //    [parameters setValue:@"3,2,4,1" forKey:@"bkMarkOrder"];
    //    [parameters setValue:@"2-C,3-C,75212-F,1-C,75211-F" forKey:@"navigOrder"];
    
    if([defaults valueForKey:KEY_USERID])
    {
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
        
    }
    if([defaults valueForKey:KEY_HUBCITIID])
    {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
        
    }

    NSLog(@"parametr:%@",parameters);
  

     NSString *urlString = [NSString stringWithFormat:@"%@band/updatenewscategriesandhubfunctnorder",BASE_URL];
    NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    [self parse_update:responseData];

}
-(void) clickedonNavCategory : (id) sender{
    UIButton *btnCat = (UIButton*)sender;
    
    if(isbookMark){
        isbookMark = FALSE;
        bookMarkTable.hidden = TRUE;
        [self setImage:sectionButton];
    }
    
    [self setImage :btnCat];
    if(comingFromSideNews){
    CGRect frame = sidenavButton.frame;
    frame.origin.y=VARIABLE_HEIGHT(38);
    frame.origin.x= 0;
    sidenavButton.frame= frame;
    }
    
    if(!isSideNav){
        isSideNav = TRUE;
        
        sideNavigationTable.hidden = NO;
        if (!sideNavigationTable) {
            sideNavClicked = TRUE;
            [self request_getnewscategriesandhubfunctn];
            
            
        }
    }
    else{
        isSideNav = FALSE;
        sideNavigationTable.hidden = TRUE;
    }
    
    
}

-(void) clickedonCategory : (id) sender{
    
    
    UIButton *btnCat = (UIButton*)sender;
    if(isSideNav){
        isSideNav = FALSE;
        sideNavigationTable.hidden = TRUE;
        [self setImage:sidenavButton];
    }
    
    [self setImage :btnCat];
    
    
    
    
    if(isbookMark){
        CGRect frame = sidenavButton.frame;
        frame.origin.y=VARIABLE_HEIGHT(38);
        frame.origin.x= 0;
        sidenavButton.frame= frame;
        
        bookMarkTable.hidden = TRUE;
        
    }
    else{
        bookMarkTable.hidden = NO;
        if (!bookMarkTable) {
            bookMarkCliked = TRUE;
            [self request_getBookMark];
            
        }
        else{
            
            CGRect frame = sidenavButton.frame;
            CGFloat value = (VARIABLE_HEIGHT(20)*2+((VARIABLE_HEIGHT(30)*(bookMarkArray.count+sectionsArray.count))))+VARIABLE_HEIGHT(40);
            
            if(value>(SCREEN_HEIGHT-ipadvalue))
                frame.origin.y = SCREEN_HEIGHT-ipadvalue;
            else
                frame.origin.y = value;
            sidenavButton.frame = frame;
            
        }
        
        
    }
    
    
    
    if(!isbookMark){
        isbookMark = TRUE;
    }
    else{
        isbookMark = FALSE;
    }
}

-(void) setImage: (UIButton *) btnCat{
    
    for(id subview in [btnCat subviews]){
        
        if([subview isMemberOfClass:[UIImageView class]]){
            
            UIImageView *imageView = (UIImageView *)subview;
            int imageTag = (int)imageView.tag;
            if(imageTag == 1){
                [imageView setImage:[UIImage imageNamed:@"arrow_down"]];
                imageView.tag = 2;
            }
            else if(imageTag == 2){
                [imageView setImage:[UIImage imageNamed:@"arrow_right"]];
                imageView.tag = 1;
            }
            [btnCat addSubview:imageView];
            
        }
    }
    
    
}
#pragma  api calls

-(void) responseData:(id)responseData{
    switch (iWebState) {
        case BOOKMARK_GET:
            [self parse_getBookMark:responseData];
            break;
        case BOOKMARK_UPDATE:
            //[self parse_updateBookMark:response];
            break;
        case SIDENAVIGATION_GET:
            [self parse_getnewscategriesandhubfunctn:responseData];
            break;
            
        default:
            break;
    }
    
}
-(void) parse_update:(id)responseObject{
    /* {
     "hubCitiId":"82",
     "userId":"3",
     "bkMarkOrder":"4,2,6,5,1,3",
     "navigOrder":"2-C,1-C,1213-F",
     "templateId":"27"
     }*/
    [HubCitiAppDelegate removeActivityIndicator];
    NSDictionary *responseData=responseObject;
    
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    
    if(responseCode==10000)
    {
        SideViewController *leftMenuViewController;
        if(IPAD){
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
        }
        else{
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
            
        }
        if(comingFromSideNews)
        leftMenuViewController.isNewsTemplate = TRUE;
    
           
        
        [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        
       [self.navigationController popViewControllerAnimated:NO];
        
    }
    else{
        
        [UtilityManager showAlert:nil msg:responseText];
    }
}

-(void) nextRequestparse_getSideNav: (id)responseData{
    [self parse_getSideNav:responseData];
    
    
}

-(void) parse_getSideNav:(id) responseObject{
    newsCategories =[[NSMutableArray alloc] init];
    hubcitiCategoies = [[NSMutableArray alloc] init];
    
    sideNavResponse = [[SideNavigationResponse alloc] init];
    [sideNavResponse setValuesForKeysWithDictionary:responseObject];
    
    if([sideNavResponse.responseCode isEqualToString:@"10000"]){
        if(sideNavResponse.listCatDetails){
            
            for(int i=0;i<sideNavResponse.listCatDetails.count;i++){
                
                SideNavigationList *sideNavList = [[SideNavigationList alloc] init];
                NSDictionary *dict =sideNavResponse.listCatDetails[i];
                [sideNavList setValuesForKeysWithDictionary:dict];
                
                
                if(sideNavList.listMainCat){
                    
                    for(int i=0;i<sideNavList.listMainCat.count;i++){
                        SideNavigationNewsCat *sideNavnewsCat = [[SideNavigationNewsCat alloc] init];
                        NSDictionary *dict = sideNavList.listMainCat[i];
                        [sideNavnewsCat setValuesForKeysWithDictionary:dict];
                        NSLog(@"string :%@", [sideNavnewsCat sortOrder]);
                        [newsCategories addObject:sideNavnewsCat];
                        
                    }
                }
                else{
                    for(int i=0;i<sideNavList.menuList.count;i++){
                        menuResponse = [[MainMenuResponse alloc] init];
                        NSDictionary *dictRes = sideNavList.menuList[i];
                        [menuResponse setValuesForKeysWithDictionary:dictRes];
                        for(int i=0;i<menuResponse.arMItemList.count;i++){
                            MenuItemList *menuItemList = [[MenuItemList alloc] init];
                            NSDictionary *dictmenu = menuResponse.arMItemList[i];
                            [menuItemList setValuesForKeysWithDictionary:dictmenu];
                            if(![menuItemList.linkTypeName isEqualToString:@"News"]){
                                [hubcitiCategoies addObject:menuItemList];
                            }
                            [NewsUtility saveMenuData : menuResponse];
                        }
                        
                    }
                }
                
            }
            
        }
        
        //tableArray =[[NSMutableArray alloc] initWithObjects:newsCategory, hubcitiCategory,nil];
        tableArrayObject =[[NSMutableArray alloc] init];
        tableDataObject = [[NSMutableArray alloc] init];
        
        int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
        if(menulevel>1){
            [tableDataObject addObject:@"Home"];
            [tableArrayObject addObject:@"Home"];
        }
        NSMutableArray *tableCopy =[[NSMutableArray alloc] init];
        for(int i=0;i<newsCategories.count;i++){
            
            [tableCopy addObject:[newsCategories objectAtIndex:i]];
            
        }
        for(int i=0;i<hubcitiCategoies.count;i++)
            
        {
            [tableCopy addObject:[hubcitiCategoies objectAtIndex:i]];
            
        }
        NSSortDescriptor *aSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES selector:@selector(localizedStandardCompare:)];
        [tableCopy sortUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]];
        for(int i=0;i<tableCopy.count;i++){
            [tableArrayObject addObject:[tableCopy objectAtIndex:i]];
        }
        if(menulevel>1){
            
            
            for(int i=1;i<tableArrayObject.count;i++){
                
                if([[tableArrayObject objectAtIndex:i] isKindOfClass:[SideNavigationNewsCat class]]){
                    [tableDataObject addObject:[[tableArrayObject objectAtIndex:i] catName]];
                }
                else{
                    [tableDataObject addObject:[[tableArrayObject objectAtIndex:i] mItemName]];
                }
            }
            
        }
        else{
            
            
            for(int i=0;i<tableArrayObject.count;i++){
                
                if([[tableArrayObject objectAtIndex:i] isKindOfClass:[SideNavigationNewsCat class]]){
                    [tableDataObject addObject:[[tableArrayObject objectAtIndex:i] catName]];
                }
                else{
                    [tableDataObject addObject:[[tableArrayObject objectAtIndex:i] mItemName]];
                }
            }
        }
        
               NSLog(@"%@",tableDataObject);
        
        
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        if([defaults valueForKey:KEY_USERID])
        {
            [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
            
        }
        if([defaults valueForKey:KEY_HUBCITIID])
        {
            [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
            
        }
        
        if(menulevel>1){
            
            NSString *linkId= [[HubCitiAppDelegate getLinkIdArray]objectAtIndex:menulevel-1];
            [parameters setValue:linkId forKey:@"linkId"];
            NSString *level = [defaults valueForKey:KEY_MENULEVEL];
            [parameters setValue:level forKey:@"level"];
        }
        else{
            
            [parameters setValue:@"0" forKey:@"linkId"];
        }
        //   [HubCitiAppDelegate removeActivityIndicator];
        //  [HubCitiAppDelegate showActivityIndicator];
        //        [parameters setValue:@"3" forKey:@"userId"];
        //        [parameters setValue:@"10" forKey:@"hubCitiId"];
        [parameters setValue:@"1" forKey:@"isBkMark"];
        NSString *urlString =[NSString stringWithFormat:@"%@band/getnewstopnavigationmenu",BASE_URL];
        NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
        id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        [self performSelector:@selector(nextRequestParse_getBookMarkData:) withObject:responseData afterDelay:0.5];
        
    }
    else{
        [HubCitiAppDelegate removeActivityIndicator];
        [UtilityManager showAlert:nil msg:sideNavResponse.responseText];
    }
    
}
-(void)nextRequestParse_getBookMarkData:(id)responseData{
    
    [self parse_getBookMarkData:responseData];
}
-(void) parse_getBookMarkData:(id)responseData{
    [HubCitiAppDelegate removeActivityIndicator];
    _bookMarkGetResponse =[[BookMarkGetRequest alloc] init];
    [_bookMarkGetResponse setValuesForKeysWithDictionary:responseData];
    NSMutableArray *bookMarkReturnArray = [[NSMutableArray alloc] init];
    if([_bookMarkGetResponse.responseCode isEqualToString:@"10000"]){
        
        if(_bookMarkGetResponse.bookMarkList){
            
            for(int i=0;i<_bookMarkGetResponse.bookMarkList.count;i++){
                
                BookMarkListDetail *bookMarkList = [[BookMarkListDetail alloc] init];
                NSDictionary *dictList = _bookMarkGetResponse.bookMarkList[i];
                [bookMarkList setValuesForKeysWithDictionary:dictList];
                
                if(bookMarkList.listCatDetails){
                    
                    for(int j=0;j<bookMarkList.listCatDetails.count;j++){
                        
                        BookMarkListcatDetails *catDetail = [[BookMarkListcatDetails alloc] init];
                        NSDictionary *dictCat= bookMarkList.listCatDetails[j];
                        [catDetail setValuesForKeysWithDictionary:dictCat];
                        
                        [bookMarkReturnArray addObject:[catDetail parCatName]];
                        
                        
                    }
                }
                
            }
        }
        NSMutableArray *notiArray =[[NSMutableArray alloc] initWithObjects:bookMarkReturnArray,tableArrayObject,tableDataObject,hubcitiCategoies, nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationName" object:notiArray];
        [self.navigationController popViewControllerAnimated:NO];
      
    }
    else{
        [HubCitiAppDelegate removeActivityIndicator];
        [UtilityManager showAlert:nil msg:_bookMarkGetResponse.responseText];
       
    }
    
}
-(void) saveMenuData
{
  
    if (self.menuResponse.downLoadLink){
        
        [defaults setValue:self.menuResponse.downLoadLink forKey:KEY_HYPERLINK];
    }
    if (self.menuResponse.androidLink){
        
        
        [defaults setValue:self.menuResponse.androidLink forKey:@"androidDownloadLink"];
    }
    if (self.menuResponse.appIconImg){
        
        [defaults setValue:self.menuResponse.appIconImg forKey:KEY_PRODUCTIMGPATH];
    }
    
    if (self.menuResponse.menuName)
        [defaults setValue:self.menuResponse.menuName forKey:KEY_MENUNAME];
    
    [defaults setValue:@"0" forKey:@"noOfColumns"];
    if(self.menuResponse.noOfColumns)
        [defaults setValue:[NSString stringWithFormat:@"%d", self.menuResponse.noOfColumns]  forKey:@"noOfColumns"];
    if(self.menuResponse.retGroupImg){
        [defaults setValue:self.menuResponse.retGroupImg forKey:@"retGroupImg"];
        
        //add the highlighted imagepath
        NSString *path = [defaults valueForKey:@"retGroupImg"];
        NSString *extn = [path pathExtension]; // This is .png in this case
        NSString *finalPath = [[[path stringByDeletingPathExtension] stringByAppendingString:@"_On"] stringByAppendingPathExtension:extn];
        [defaults setValue:finalPath forKey:@"retGroupImg_On"];
    }
    else{
        [defaults setValue:nil forKey:@"retGroupImg"];
        [defaults setValue:nil forKey:@"retGroupImg_On"];
    }
    
    if(self.menuResponse.departFlag)
        [defaults setValue:[NSString stringWithFormat:@"%d", self.menuResponse.departFlag]  forKey:@"departFlag"];
    else
        [defaults setValue:@"0"  forKey:@"departFlag"];
    
    NSLog(@"dept: %@",[defaults valueForKey:@"departFlag"]);
    if(self.menuResponse.typeFlag)
        [defaults setValue:[NSString stringWithFormat:@"%d", self.menuResponse.typeFlag] forKey:@"typeFlag"];
    else
        [defaults setValue:@"0"  forKey:@"typeFlag"];
    
    if(self.menuResponse.menuId)
        [defaults setValue:self.menuResponse.menuId forKey:@"menuId"];
    if(self.menuResponse.isRegApp)
        [defaults setValue:[NSString stringWithFormat:@"%d", self.menuResponse.isRegApp] forKey:RegionApp];
    
    [defaults setValue:self.menuResponse.hubCitiId forKey:KEY_HUBCITIID];
    
    if(self.menuResponse.mBannerImg)
        [defaults setValue:self.menuResponse.mBannerImg forKey:@"TopBannerImage"];
    else
        [defaults setValue:Nil forKey:@"TopBannerImage"];
    if([menuResponse.templateName isEqualToString:@"Two Image Template"]||[menuResponse.templateName isEqualToString:@"Square Image Template"]||[menuResponse.templateName isEqualToString:@"Four Tile Template"]||[menuResponse.templateName isEqualToString:@"Two Tile Template"]||[menuResponse.templateName isEqualToString:@"Two Tile Template With Banner"]){
        
        if([menuResponse.isDisplayLabel intValue]==1){
            [defaults setValue:@"1" forKey:@"isDisplayLabel"];
        }
    }
    else{
        [defaults setValue:nil forKey:@"isDisplayLabel"];
    }
    [SharedManager setFiltersCount:self.menuResponse.retAffCount];
    [defaults setValue:self.menuResponse.retAffName forKey:@"Title"];
    [SharedManager setRetAffId:self.menuResponse.retAffId];
    
}


-(void) request_getnewscategriesandhubfunctn{
    [HubCitiAppDelegate showActivityIndicator];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    iWebState = SIDENAVIGATION_GET;
    if([defaults valueForKey:KEY_USERID])
    {
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
        
    }
    if([defaults valueForKey:KEY_HUBCITIID])
    {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
        
    }
    [parameters setValue:@"true" forKey:@"sideNaviPersonalizatn"];

    NSLog(@"%@",parameters);
      // NSString *urlString = [NSString stringWithFormat:@"http://10.10.221.155:9990/HubCiti_Coupon/band/getnewscategriesandhubfunctn"];
   NSString *urlString =[NSString stringWithFormat:@"%@band/getnewscategriesandhubfunctn",BASE_URL];
    NSLog(@"%@",urlString);
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
}
-(void) parse_getnewscategriesandhubfunctn: (id) responseData{
    
    
    _sideMenuGetResponse =[[BookMarkGetRequest alloc] init];
    [_sideMenuGetResponse setValuesForKeysWithDictionary:responseData];
    
    if([_sideMenuGetResponse.responseCode isEqualToString:@"10000"]){
        
        if(_sideMenuGetResponse.bookMarkList){
            
            for(int i=0;i<_sideMenuGetResponse.bookMarkList.count;i++){
                
                BookMarkListDetail *bookMarkList = [[BookMarkListDetail alloc] init];
                bookMarkList.bookMarkArray =[[NSMutableArray alloc] init];
                bookMarkList.sectionsArray = [[NSMutableArray alloc] init];
                NSDictionary *dictList = _sideMenuGetResponse.bookMarkList[i];
                [bookMarkList setValuesForKeysWithDictionary:dictList];
                [tableSideMenuLoad addObject:bookMarkList];
                if(bookMarkList.listCatDetails){
                    
                    for(int j=0;j<bookMarkList.listCatDetails.count;j++){
                        
                        BookMarkListcatDetails *catDetail = [[BookMarkListcatDetails alloc] init];
                        NSDictionary *dictCat= bookMarkList.listCatDetails[j];
                        [catDetail setValuesForKeysWithDictionary:dictCat];
                        if(i==0){
                            NSArray *array = [[NSArray alloc] initWithObjects:[catDetail parCatName],[catDetail parCatId],[catDetail isHubFunctn], nil];
                            [sideNavigationArray addObject:array];
                            
                            
                        }
                        else{
                            NSArray *array = [[NSArray alloc] initWithObjects:[catDetail parCatName],[catDetail parCatId],[catDetail isHubFunctn], nil];
                            if([[catDetail isBkMark] integerValue] == 0)
                            [sectionsSideNavArray addObject:array];
                            
                        }
                    }
                }
                
            }
        }
    }
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    if(!comingFromSideNews){
        sideNavigationTable = [[UITableView alloc]initWithFrame:CGRectMake(0, VARIABLE_HEIGHT(36), SCREEN_WIDTH, SCREEN_HEIGHT-yVal-VARIABLE_HEIGHT(36)) style:UITableViewStylePlain];

    }
    else{
    sideNavigationTable = [[UITableView alloc]initWithFrame:CGRectMake(0, VARIABLE_HEIGHT(72), SCREEN_WIDTH, SCREEN_HEIGHT-yVal-VARIABLE_HEIGHT(72)) style:UITableViewStylePlain];
    }
    
    
    sideNavigationTable.dataSource=self;
    sideNavigationTable.delegate=self;
    [sideNavigationTable setBackgroundColor:[UIColor clearColor]];
    sideNavigationTable.tableFooterView.hidden=YES;
    sideNavigationTable.scrollEnabled=YES;
    sideNavigationTable.translatesAutoresizingMaskIntoConstraints=YES;
    sideNavigationTable.userInteractionEnabled=YES;
    [self.view addSubview:sideNavigationTable];
    [sideNavigationTable setEditing:YES animated:YES];
    
    [sideNavigationTable reloadData];
    
}
-(void) request_getBookMark{
    
    [HubCitiAppDelegate showActivityIndicator];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    iWebState = BOOKMARK_GET;
    if([defaults valueForKey:KEY_USERID])
    {
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
        
    }
    if([defaults valueForKey:KEY_HUBCITIID])
    {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
        
    }
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    if(menulevel>1){
        
        NSString *linkId= [[HubCitiAppDelegate getLinkIdArray]objectAtIndex:menulevel-1];
        [parameters setValue:linkId forKey:@"linkId"];
        NSString *level = [defaults valueForKey:KEY_MENULEVEL];
        [parameters setValue:level forKey:@"level"];
    }
    else{
        
        [parameters setValue:@"0" forKey:@"linkId"];
    }
    
    //    [parameters setValue:@"3" forKey:@"userId"];
    //    [parameters setValue:@"10" forKey:@"hubCitiId"];
    [parameters setValue:@"0" forKey:@"isBkMark"];
    NSString *urlString =[NSString stringWithFormat:@"%@band/getnewstopnavigationmenu",BASE_URL];
    
    NSLog(@"parameter %@", parameters);
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    DLog(@"Url: %@",urlString);
    [client sendRequest : parameters : urlString];
    
    
}

-(void) parse_getBookMark: (id) responseData{
    
    _bookMarkGetResponse =[[BookMarkGetRequest alloc] init];
    [_bookMarkGetResponse setValuesForKeysWithDictionary:responseData];
    
    if([_bookMarkGetResponse.responseCode isEqualToString:@"10000"]){
        
        if(_bookMarkGetResponse.bookMarkList){
            
            for(int i=0;i<_bookMarkGetResponse.bookMarkList.count;i++){
                
                BookMarkListDetail *bookMarkList = [[BookMarkListDetail alloc] init];
                bookMarkList.bookMarkArray =[[NSMutableArray alloc] init];
                bookMarkList.sectionsArray = [[NSMutableArray alloc] init];
                NSDictionary *dictList = _bookMarkGetResponse.bookMarkList[i];
                [bookMarkList setValuesForKeysWithDictionary:dictList];
                [tableBookMarkLoad addObject:bookMarkList];
                if(bookMarkList.listCatDetails){
                    
                    for(int j=0;j<bookMarkList.listCatDetails.count;j++){
                        
                        BookMarkListcatDetails *catDetail = [[BookMarkListcatDetails alloc] init];
                        NSDictionary *dictCat= bookMarkList.listCatDetails[j];
                        [catDetail setValuesForKeysWithDictionary:dictCat];
                        if(i==0){
                            NSArray *array = [[NSArray alloc] initWithObjects:[catDetail parCatName],[catDetail parCatId], nil];
                            [bookMarkArray addObject:array];
                            
                            
                            
                        }
                        else{
                            NSArray *array = [[NSArray alloc] initWithObjects:[catDetail parCatName],[catDetail parCatId], nil];
                            if([[catDetail isBkMark] integerValue] == 0)
                            [sectionsArray addObject:array];
                            
                        }
                    }
                }
                
            }
        }
        
    }
    
    //    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    
    
    CGRect frame = sidenavButton.frame;
    CGFloat heightTable;
    CGFloat value = (VARIABLE_HEIGHT(20)*2+((VARIABLE_HEIGHT(30)*(bookMarkArray.count+sectionsArray.count))))+VARIABLE_HEIGHT(40);
    
    if(value>(SCREEN_HEIGHT-ipadvalue)){
        frame.origin.y =SCREEN_HEIGHT-ipadvalue;
        heightTable= (SCREEN_HEIGHT-ipadvalue)-VARIABLE_HEIGHT(40);
        
    }
    else{
        frame.origin.y = value;
        heightTable =VARIABLE_HEIGHT(20)*2+((VARIABLE_HEIGHT(30)*(bookMarkArray.count+sectionsArray.count)));
    }
    sidenavButton.frame = frame;
    
    
    
    bookMarkTable = [[UITableView alloc]initWithFrame:CGRectMake(0, VARIABLE_HEIGHT(36), SCREEN_WIDTH, heightTable) style:UITableViewStylePlain];
    
    
    bookMarkTable.dataSource=self;
    bookMarkTable.delegate=self;
    [bookMarkTable setBackgroundColor:[UIColor clearColor]];
    bookMarkTable.tableFooterView.hidden=YES;
    bookMarkTable.scrollEnabled=YES;
    bookMarkTable.translatesAutoresizingMaskIntoConstraints=YES;
    bookMarkTable.userInteractionEnabled=YES;
    [self.view addSubview:bookMarkTable];
    [bookMarkTable setEditing:YES animated:YES];
    
    
    [bookMarkTable reloadData];
    
}

#pragma tableview delegate

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return VARIABLE_HEIGHT(30);
}
-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return VARIABLE_HEIGHT(20);
}
-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 1) // Don't move the first row
        return NO;
    
    return YES;
}

-(int) indexOfRowClickedTable:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath withsourceArray:(NSMutableArray *)sectionOne sectionArray:(NSMutableArray *)sectionTwo{
    int i =0;
    for(int j=0;j<sectionOne.count; j++){
        
        if([[[sectionTwo objectAtIndex:indexPath.row] objectAtIndex:0] isEqualToString:[[sectionOne objectAtIndex:j] objectAtIndex:0]]){
            i=1;
            break;
            
        }
    }
    return i;
}


-(UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int i=0;
    if(indexPath.section == 0)
        return UITableViewCellEditingStyleDelete;
    else
        
    {
        if(tableView == bookMarkTable)
            i = [self indexOfRowClickedTable:tableView editingStyleForRowAtIndexPath:indexPath withsourceArray:bookMarkArray sectionArray:sectionsArray];
        else  if(tableView == sideNavigationTable)
            i = [self indexOfRowClickedTable:tableView editingStyleForRowAtIndexPath:indexPath withsourceArray:sideNavigationArray sectionArray:sectionsSideNavArray];
        
        if(i==0)
            return UITableViewCellEditingStyleInsert;
        else
            return UITableViewCellEditingStyleDelete;
        
        
    }
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
    
}

-(NSMutableArray *) commitEditTable:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath with_sourceArray: (NSMutableArray *)sectionOne sectionsArray: (NSMutableArray *)sectionTwo{
    
    int i=0;
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        if(indexPath.section == 0){
            
            [sectionTwo addObject:[sectionOne objectAtIndex:indexPath.row]];
            NSLog(@"%ld",(long)indexPath.row);
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sectionTwo.count-1 inSection:1]] withRowAnimation:UITableViewRowAnimationRight];
            [sectionOne removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
        }
        // delete data from first section array if deleted from second section
        else{
            
            int j;
            for(j=0;j<sectionOne.count; j++){
                if([[[sectionTwo objectAtIndex:indexPath.row] objectAtIndex:0] isEqualToString:[[sectionOne objectAtIndex:j] objectAtIndex:0]]){
                    
                    i=1;
                    break;
                    
                }
            }
            
            if(i==1){
                
                [sectionOne removeObjectAtIndex:j];
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:j inSection:indexPath.section-1]] withRowAnimation:UITableViewRowAnimationFade];
                
            }
            
        }
        
    }
    
    else if(editingStyle == UITableViewCellEditingStyleInsert){
        
        [sectionOne addObject:[sectionTwo objectAtIndex:indexPath.row]];
        NSLog(@"%ld",(long)indexPath.row);
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sectionOne.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
        
        
        [sectionTwo removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }
    return sectionOne;
}


-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    //   int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    if(tableView == bookMarkTable){
        bookMarkArray =  [self commitEditTable:tableView commitEditingStyle:editingStyle forRowAtIndexPath:indexPath with_sourceArray:bookMarkArray sectionsArray: sectionsArray];
        
        CGRect tableFrame = bookMarkTable.frame;
        tableFrame.size.width =SCREEN_WIDTH;
        
        
        CGRect frame = sidenavButton.frame;
        frame.origin.x = 0;
        CGFloat value = (VARIABLE_HEIGHT(20)*2+((VARIABLE_HEIGHT(30)*(bookMarkArray.count+sectionsArray.count))))+VARIABLE_HEIGHT(40);
        if(value>(SCREEN_HEIGHT-ipadvalue)){
            frame.origin.y =SCREEN_HEIGHT-ipadvalue;
            tableFrame.size.height = (SCREEN_HEIGHT-ipadvalue)-VARIABLE_HEIGHT(40);
        }
        else{
            frame.origin.y = value;
            tableFrame.size.height =VARIABLE_HEIGHT(20)*2+((VARIABLE_HEIGHT(30)*(bookMarkArray.count+sectionsArray.count)));
        }
        sidenavButton.frame = frame;
        
        //        CGFloat value2 = (VARIABLE_HEIGHT(20)*2+((VARIABLE_HEIGHT(30)*(bookMarkArray.count+sectionsArray.count))));
        
        bookMarkTable.frame = tableFrame;
        
    }
    else if(tableView == sideNavigationTable){
        sideNavigationArray = [self commitEditTable:tableView commitEditingStyle:editingStyle forRowAtIndexPath:indexPath with_sourceArray:sideNavigationArray sectionsArray: sectionsSideNavArray];
        /*CGRect tableFrame = sideNavigationTable.frame;
         tableFrame.size.width =SCREEN_WIDTH;
         tableFrame.size.height = VARIABLE_HEIGHT(20)*2+((VARIABLE_HEIGHT(30)*(sideNavigationArray.count+sectionsSideNavArray.count)));
         sideNavigationTable.frame = tableFrame;*/
    }
    
    [tableView reloadData];
}



-(NSMutableArray *) movingRowsofTable:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath withArray: (NSMutableArray *) sectionOne{
    
    if(otherSection){
        otherSection = NO;
        NSArray *array =[sectionOne objectAtIndex:sourceIndexPath.row];
        [sectionOne removeObjectAtIndex:sourceIndexPath.row];
        [sectionOne addObject:array];
    }
    else{
        NSArray *array =[sectionOne objectAtIndex:sourceIndexPath.row];
        [sectionOne removeObjectAtIndex:sourceIndexPath.row];
        [sectionOne insertObject:array atIndex:destinationIndexPath.row];
    }
    return sectionOne;
}

-(void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    
    if(tableView == bookMarkTable){
        bookMarkArray = [self movingRowsofTable:tableView moveRowAtIndexPath:sourceIndexPath toIndexPath:destinationIndexPath withArray:bookMarkArray];
        
    }
    else if(tableView == sideNavigationTable){
        sideNavigationArray = [self movingRowsofTable:tableView moveRowAtIndexPath:sourceIndexPath toIndexPath:destinationIndexPath withArray:sideNavigationArray];
    }
    [tableView reloadData];
    
}


-(NSIndexPath *) tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath{
    
    
    NSInteger totalRow = [tableView numberOfRowsInSection:sourceIndexPath.section];
    
    if (sourceIndexPath.section != proposedDestinationIndexPath.section) {
        otherSection = YES;
        return [NSIndexPath indexPathForRow:totalRow-1 inSection:sourceIndexPath.section];
    }
    
    return proposedDestinationIndexPath;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger returnValue;
    if(tableView == bookMarkTable){
        returnValue= tableBookMarkLoad.count;
    }
    else {
        returnValue=  tableSideMenuLoad.count;
    }
    return returnValue;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger numberOfRows;
    if(tableView == bookMarkTable){
        if(section == 0){
            numberOfRows = bookMarkArray.count;
        }
        else {
            numberOfRows = sectionsArray.count;
        }
    }
    else{
        if(section == 0){
            numberOfRows = sideNavigationArray.count;
        }
        else {
            numberOfRows = sectionsSideNavArray.count;
        }
        
    }
    return numberOfRows;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    BookMarkListDetail *bookMarkList;
    float labelFont = 14.0;
    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
        labelFont = 18.0;
    }
    if(tableView == bookMarkTable){
        bookMarkList = [tableBookMarkLoad objectAtIndex:section];
    }
    else{
        bookMarkList = [tableSideMenuLoad objectAtIndex:section];
    }
    
    UILabel *lblCatName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 18)];
    lblCatName.textAlignment = NSTextAlignmentCenter;
    [lblCatName setTextColor:[UIColor blackColor]];
    [lblCatName setBackgroundColor:[UIColor colorWithRGBInt:0xAAB3BC]];
    [lblCatName setFont:[UIFont fontWithName:@"Helvetica" size:labelFont]];
    if(tableView == sideNavigationTable){
        if(section == 0)
            [lblCatName setText:[NSString stringWithFormat:@" %@",@"Side Navigation"]];
        else
            [lblCatName setText:[NSString stringWithFormat:@" %@",[bookMarkList header]]];
    }
    else{
        [lblCatName setText:[NSString stringWithFormat:@" %@",[bookMarkList header]]];
    }
    return lblCatName;
    
    
}-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"myCell"];
    if(tableView == bookMarkTable){
        if(indexPath.section == 0)
            cell.textLabel.text =[[bookMarkArray objectAtIndex:indexPath.row] objectAtIndex:0];
        else
            cell.textLabel.text =[[sectionsArray objectAtIndex:indexPath.row] objectAtIndex:0];
    }
    else if(tableView == sideNavigationTable){
        if(indexPath.section == 0)
            
            cell.textLabel.text =[[sideNavigationArray objectAtIndex:indexPath.row] objectAtIndex:0];
        else
            cell.textLabel.text =[[sectionsSideNavArray objectAtIndex:indexPath.row] objectAtIndex:0];
        
    }
    if(IPAD)
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:17];
    else
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
    tableView.sectionIndexBackgroundColor = [UIColor darkGrayColor];
    
    return cell;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

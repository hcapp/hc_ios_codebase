//
//  CommonUtilityNews.m
//  HubCiti
//
//  Created by Ashika on 6/9/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "CommonUtilityNews.h"
#import "MFSideMenuContainerViewController.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "BlockViewController.h"
#import "MFSideMenu.h"
#import "AppDelegate.h"
#import "MainMenuViewController.h"
#import "SideViewController.h"
#import "UserInformationViewController.h"



static CommonUtilityNews *sharedUtilityManager = nil;
@implementation CommonUtilityNews

@synthesize signUpDelegate;
//Get the shared instance and create it if necessary
+(id)utilityManagers{
    @synchronized(self){
        if (sharedUtilityManager == nil){
            
            sharedUtilityManager = [[super allocWithZone:NULL]init];
        }
    }
    return sharedUtilityManager;
}

//We can still have a regular init method, that will get called first time the singleton is used
-(id)init{
    
    self = [super init];
    if (self) {
        // Work your initialising magic here as you normally would
    }
    
    return self;
}
-(void) setUserSettings
{
    
    NSString *reqStr = [NSString stringWithFormat:@"<AuthenticateUser><deviceID>%@</deviceID><userId>%@</userId><hubCitiId>%@</hubCitiId></AuthenticateUser>",[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusersettings",BASE_URL];
    
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:reqStr];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
    if (responseCodeElement != nil && ![[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
        
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        return;
    }
    
    TBXMLElement *localeRadiusElement = [TBXML childElementNamed:@"localeRadius" parentElement:tbxml.rootXMLElement];
    TBXMLElement *pushNotifyElement = [TBXML childElementNamed:@"pushNotify" parentElement:tbxml.rootXMLElement];
    TBXMLElement *bLocServiceElement = [TBXML childElementNamed:@"bLocService" parentElement:tbxml.rootXMLElement];
    if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"false"])
    {
        [SharedManager setGps_allow_flag:NO];
        [defaults setValue:@"NO" forKey:@"allowingGPS"];
    }
    else if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"true"])
    {
        [SharedManager setGps_allow_flag:YES];
        [defaults setValue:@"YES" forKey:@"allowingGPS"];
    }
 

    NSString * radius  = [TBXML textForElement:localeRadiusElement];
    NSString * pushNotifyString;
    if(userIdentifier){
        pushNotifyString = @"false";
    }
    else{
    if ([[TBXML textForElement:pushNotifyElement] isEqualToString:@"true"]) {
        pushNotifyString = @"true";
    }
    else {
        pushNotifyString = @"false";
    }
    }
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<UserSettings><userId>%@</userId><deviceId>%@</deviceId>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_DEVICEID]];
    [requestStr appendFormat:@"<pushNotify>%@</pushNotify><hubCitiId>%@</hubCitiId>",pushNotifyString,[defaults valueForKey:KEY_HUBCITIID]];
    if ([[defaults valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
    {
        [requestStr appendFormat:@"<bLocService>true</bLocService>"];
    }
    else
    {
        [requestStr appendFormat:@"<bLocService>false</bLocService>"];
    }
    
    
    if(radius.length>0)
    {
        [requestStr appendFormat:@"<localeRadius>%@</localeRadius></UserSettings>",radius];
    }
    else
    {
        if(userIdentifier)
            [requestStr appendFormat:@"<localeRadius>99</localeRadius></UserSettings>"];
        else
            [requestStr appendFormat:@"<localeRadius>50</localeRadius></UserSettings>"];
    }
    
    NSString *urlReqString = [NSString stringWithFormat:@"%@firstuse/setusersettings",BASE_URL];
  
    NSString *responseXmlData = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlReqString withParam:requestStr]];
    
    NSLog(@"%@",responseXmlData);
    
    
    
}

-(void) pushMainmenu : (UIViewController *) viewVC{
    MainMenuViewController *mainView = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController" bundle:[NSBundle mainBundle]];
    
    //[viewVC.menuContainerViewController setLeftMenuViewController:nil];
//    [viewVC.menuContainerViewController setCenterViewController:[[CustomizedNavController alloc]
//                                                                 initWithRootViewController:mainView]];
   [viewVC.menuContainerViewController.centerViewController pushViewController:mainView animated:NO];
    
}

-(void) pushViewFromHamberger : (UIViewController *) viewVC{
    //  SideMenuLeftViewController *leftMenuViewController = [[SideMenuLeftViewController alloc] init];
    BOOL value = [defaults boolForKey:@"LoginUserNews"];
    NSLog(@"loginuser: %d",(int)value);
    
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    // int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    
    [self refreshLinkDeptArray];
    if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Combination News Template"]){
        // MainMenuViewController *mainmenu=(main);
        CombinationViewController *comb =   [[CombinationViewController alloc] init];
        //  mainmenu.combView = comb;
        [viewVC.menuContainerViewController.centerViewController pushViewController:comb animated:NO];
    }
    else if([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"News Tile Template"]){
        BlockViewController *block =  [[BlockViewController alloc] initWithNibName:@"BlockViewController" bundle:nil];
        [viewVC.menuContainerViewController.centerViewController pushViewController:block animated:NO];
    }
    else{
        ScrollTemplateViewController *scroll= [[ScrollTemplateViewController alloc] initWithNibName:@"ScrollTemplateViewController" bundle:nil];
        NSLog(@"linkId: %@",[HubCitiAppDelegate getLinkIdArray] );
        
        [viewVC.menuContainerViewController.centerViewController pushViewController:scroll animated:NO];
        
    }
    
//    SideViewController *leftMenuViewController;
//    if(IPAD){
//        leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
//    }
//    else{
//        leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
//        
//    }
//    
//    leftMenuViewController.isNewsTemplate = TRUE;
//    [viewVC.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
    
    //  [viewVC.menuContainerViewController setCenterViewController:[self navigationRoot]];
    
}

- (ScrollTemplateViewController *)scrollingView {
    return [[ScrollTemplateViewController alloc] initWithNibName:@"ScrollTemplateViewController" bundle:nil];
}
- (CombinationViewController *)combinationalView {
    return [[CombinationViewController alloc] init];
}

- (BlockViewController *)BlockView {
    return [[BlockViewController alloc] initWithNibName:@"BlockViewController" bundle:nil];
}
-(CustomizedNavController *) navigationRoot{
    CustomizedNavController *navBar = [HubCitiAppDelegate navigationController];
    if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
        
        return [navBar
                initWithRootViewController:[self combinationalView]];
        
        
    }
    else if([[defaults valueForKey:@"centerViewController"] isEqualToString:@"News Tile Template"]){
        return [navBar
                initWithRootViewController:[self BlockView]];
    }
    else {
        return [navBar
                initWithRootViewController:[self scrollingView]];
        
    }
    
}
- (CustomizedNavController *)navigationControllerNews
{
    CustomizedNavController *navBar = [HubCitiAppDelegate navigationController];
    if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Combination News Template"])
    {
        return [navBar
                initWithRootViewController:[self combinationalView]];
    }
    else if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"News Tile Template"])
    {
        return [navBar
                initWithRootViewController:[self BlockView]];
    }
    
    else
    {
        return [navBar
                initWithRootViewController:[self scrollingView]];
    }
    
}
-(void) setNewsAsSubMenu:(UIViewController *) viewController : (NSInteger)level{
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    NSLog(@"%d--->%@",menulevel,[HubCitiAppDelegate getLinkIdArray]);
    
    
    [viewController.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    
    
    if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Combination News Template"]){
          CombinationViewController *comb =   [[CombinationViewController alloc] init];
     
        [viewController.navigationController pushViewController:comb animated:NO];
    }
    else if([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"News Tile Template"]){
        BlockViewController *block =  [[BlockViewController alloc] initWithNibName:@"BlockViewController" bundle:nil];
        [viewController.navigationController pushViewController:block animated:NO];
    }
    else{
        ScrollTemplateViewController *scroll= [[ScrollTemplateViewController alloc] initWithNibName:@"ScrollTemplateViewController" bundle:nil];
        NSLog(@"linkId: %@",[HubCitiAppDelegate getLinkIdArray] );
        
        [viewController.navigationController pushViewController:scroll animated:NO];
        
    }
    
}

-(void) setLoginVCAsRoot{
    LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    AppDelegate *app =HubCitiAppDelegate;
    loginViewController.login_DO = app.loginDO;
     CustomizedNavController *navigationController = [HubCitiAppDelegate navigationController];
    
    SideViewController *leftMenuViewController;
    if(IPAD){
        leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
    }
    else{
        leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
        
    }
    leftMenuViewController.isNewsTemplate = FALSE;
    //Navigate to main menu page
    MainMenuViewController  *mainMenuController = [[MainMenuViewController alloc] initWithNibName:@"MainMenuViewController" bundle:nil];
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:[navigationController initWithRootViewController:loginViewController]
                                                    leftMenuViewController:leftMenuViewController
                                                    rightMenuViewController:nil];
    [container.centerViewController pushViewController:mainMenuController animated:NO];
    app.window.rootViewController = container;
}

-(void) setNewsTemplateAsRoot{
    [defaults setBool:YES forKey:@"newsTemplateExist"];
    //   SideMenuLeftViewController *leftMenuViewController = [[SideMenuLeftViewController alloc] init];
    SideViewController *leftMenuViewController;
    if(IPAD){
        leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
    }
    else{
        leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
        
    }
    leftMenuViewController.isNewsTemplate = TRUE;
    AppDelegate *app =HubCitiAppDelegate;
    leftMenuViewController.loginDo = app.loginDO;
    
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:[self navigationControllerNews]
                                                    leftMenuViewController:leftMenuViewController
                                                    rightMenuViewController:nil];
    
    
    app.window.rootViewController = container;
    
}

-(void) saveMenuData : (MainMenuResponse*) menuResponse
{
    if (menuResponse.downLoadLink){
        
        [defaults setValue:menuResponse.downLoadLink forKey:KEY_HYPERLINK];
    }
    if (menuResponse.androidLink){
        
        [defaults setValue:menuResponse.androidLink forKey:@"androidDownloadLink"];
        
    }
    else
        [defaults setValue:@"" forKey:@"androidDownloadLink"];
    
    if (menuResponse.appIconImg){
        
        [defaults setValue:menuResponse.appIconImg forKey:KEY_PRODUCTIMGPATH];
    }
    
    if (menuResponse.menuName)
        [defaults setValue:menuResponse.menuName forKey:KEY_MENUNAME];
    
    [defaults setValue:@"0" forKey:@"noOfColumns"];
    if(menuResponse.noOfColumns)
        [defaults setValue:[NSString stringWithFormat:@"%d", menuResponse.noOfColumns]  forKey:@"noOfColumns"];
    if(menuResponse.retGroupImg){
        [defaults setValue:menuResponse.retGroupImg forKey:@"retGroupImg"];
        
        //add the highlighted imagepath
        NSString *path = [defaults valueForKey:@"retGroupImg"];
        NSString *extn = [path pathExtension]; // This is .png in this case
        NSString *finalPath = [[[path stringByDeletingPathExtension] stringByAppendingString:@"_On"] stringByAppendingPathExtension:extn];
        [defaults setValue:finalPath forKey:@"retGroupImg_On"];
    }
    else{
        [defaults setValue:nil forKey:@"retGroupImg"];
        [defaults setValue:nil forKey:@"retGroupImg_On"];
    }
    
    if(menuResponse.departFlag)
        [defaults setValue:[NSString stringWithFormat:@"%d", menuResponse.departFlag]  forKey:@"departFlag"];
    else
        [defaults setValue:@"0"  forKey:@"departFlag"];
    
    NSLog(@"dept: %@",[defaults valueForKey:@"departFlag"]);
    if(menuResponse.typeFlag)
        [defaults setValue:[NSString stringWithFormat:@"%d", menuResponse.typeFlag] forKey:@"typeFlag"];
    else
        [defaults setValue:@"0"  forKey:@"typeFlag"];
    
    
    if(menuResponse.isRegApp)
        [defaults setValue:[NSString stringWithFormat:@"%d", menuResponse.isRegApp] forKey:RegionApp];
    
    // [defaults setValue:menuResponse.hubCitiId forKey:KEY_HUBCITIID];
    [SharedManager setFiltersCount:menuResponse.retAffCount];
    [defaults setValue:menuResponse.retAffName forKey:@"Title"];
    [SharedManager setRetAffId:menuResponse.retAffId];
    
}

-(void) refreshLinkDeptArray {
    
    [defaults setValue:@"NONE" forKey:@"sortOrder"];
    [defaults setValue:nil forKey:@"commaSeperatedCities"];
    
    [HubCitiAppDelegate refreshLinkIdArray];
    
    [[HubCitiAppDelegate getLinkIdArray]addObject:@"0"];
    
    
    DLog(@"%lu",(unsigned long)[[HubCitiAppDelegate getLinkIdArray]count]);
    
    [selectedFilterOption setValue:@"0" forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
    [selectedSortOption setValue:@"-1" forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
    DLog(@"%ld",(long)[[selectedSortOption valueForKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]]integerValue]);
    
    [HubCitiAppDelegate refreshDeptIdArray];
    [[HubCitiAppDelegate getDeptIdArray]addObject:@"0"];
    [deptId setValue:@"0" forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
    
    DLog(@"%lu",(unsigned long)[[HubCitiAppDelegate getDeptIdArray]count]);
    
    DLog(@"%@",[deptId valueForKey:[[HubCitiAppDelegate getLinkIdArray] lastObject]]);
    
    [HubCitiAppDelegate refreshTypeIdArray];
    [[HubCitiAppDelegate getTypeIdArray]addObject:@"0"];
    [typeId setValue:@"0" forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
    
    [selectedSortCityDic setObject:[NSNull null] forKey:[[HubCitiAppDelegate getLinkIdArray]lastObject]];
    
}

-(UIButton*) customizedSideMenu:(BOOL) mainmenu{
    
    UIButton *hamburger = [UIButton buttonWithType:UIButtonTypeCustom];
    if(mainmenu)
        hamburger.frame = CGRectMake(0, 0, VARIABLE_WIDTH(18), VARIABLE_HEIGHT(16));
    else
        hamburger.frame = CGRectMake(VARIABLE_WIDTH(30), 1, VARIABLE_WIDTH(20), 25);
    UIImageView * imageView1 = [[UIImageView alloc]initWithFrame:hamburger.frame];
    imageView1.image = [UIImage imageNamed:@"hamburger2.png"];
    [hamburger setBackgroundImage:[UIImage imageNamed:@"hamburger2.png"] forState:UIControlStateNormal];
    
    return hamburger;
}

- (void) tempPasswordDisplay:(NSNotification*)notification onViewController : (UIViewController*) viewController
{
    __block NSString *password = @"";
    UIAlertController * alertResetPassword=   [UIAlertController
                                               alertControllerWithTitle:@"Reset Password"
                                               message:nil
                                               preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* submitResetPwd = [UIAlertAction
                                     actionWithTitle:@"Submit"
                                     style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                     {
                                         [alertResetPassword dismissViewControllerAnimated:YES completion:nil];
                                         
                                         UITextField * textField1 = alertResetPassword.textFields.firstObject;
                                         UITextField * textField2 = alertResetPassword.textFields.lastObject;
                                         if ([textField1.text length] == 0 )
                                         {
                                             UIAlertController * alertWrongPwd=   [UIAlertController
                                                                                   alertControllerWithTitle:@"Please enter New Password"
                                                                                   message:nil
                                                                                   preferredStyle:UIAlertControllerStyleAlert];
                                             UIAlertAction* ok = [UIAlertAction
                                                                  actionWithTitle:@"OK"
                                                                  style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action)
                                                                  {
                                                                      [alertWrongPwd dismissViewControllerAnimated:YES completion:nil];
                                                                      [self presentAlertViewController:alertResetPassword OnViewController:viewController];
                                                                      
                                                                  }];
                                             
                                             [alertWrongPwd addAction:ok];
                                             
                                             [self presentAlertViewController:alertWrongPwd OnViewController:viewController];
                                             
                                             
                                         }
                                         else if ([textField2.text length] == 0)
                                         {
                                             UIAlertController * alertWrongPwd=   [UIAlertController
                                                                                   alertControllerWithTitle:@"Please enter Confirm Password"
                                                                                   message:nil
                                                                                   preferredStyle:UIAlertControllerStyleAlert];
                                             
                                             UIAlertAction* ok = [UIAlertAction
                                                                  actionWithTitle:@"OK"
                                                                  style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action)
                                                                  {
                                                                      [alertWrongPwd dismissViewControllerAnimated:YES completion:nil];
                                                                      [self presentAlertViewController:alertResetPassword OnViewController:viewController];
                                                                      
                                                                  }];
                                             
                                             [alertWrongPwd addAction:ok];
                                             
                                             
                                             [self presentAlertViewController:alertWrongPwd OnViewController:viewController];
                                         }
                                         else if ([textField1.text length] < 6)
                                         {
                                             UIAlertController * lessPwd=   [UIAlertController
                                                                             alertControllerWithTitle:@"Password must be minimum of 6 characters"
                                                                             message:nil
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                                             
                                             UIAlertAction* ok = [UIAlertAction
                                                                  actionWithTitle:@"OK"
                                                                  style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action)
                                                                  {
                                                                      [lessPwd dismissViewControllerAnimated:YES completion:nil];
                                                                      [self presentAlertViewController:alertResetPassword OnViewController:viewController];
                                                                      
                                                                  }];
                                             
                                             [lessPwd addAction:ok];
                                             [self presentAlertViewController:lessPwd OnViewController:viewController];
                                         }
                                         
                                         
                                         
                                         else if (![textField1.text isEqualToString:textField2.text])
                                         {
                                             UIAlertController * alertWrongPwd=   [UIAlertController
                                                                                   alertControllerWithTitle:@"Password Mismatch"
                                                                                   message:nil
                                                                                   preferredStyle:UIAlertControllerStyleAlert];
                                             
                                             UIAlertAction* ok = [UIAlertAction
                                                                  actionWithTitle:@"OK"
                                                                  style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action)
                                                                  {
                                                                      [alertWrongPwd dismissViewControllerAnimated:YES completion:nil];
                                                                      [self presentAlertViewController:alertResetPassword OnViewController:viewController];
                                                                      
                                                                  }];
                                             
                                             [alertWrongPwd addAction:ok];
                                             [self presentAlertViewController:alertWrongPwd OnViewController:viewController];
                                         }
                                         
                                         
                                         else
                                         {
                                             password = textField1.text;
                                             [defaults setValue:password forKey:@"Password"];
                                             NSLog(@"password after changing new one %@",[defaults valueForKey:@"Password"]);
                                             [self request_ResetPassword:password];
                                             [alertResetPassword dismissViewControllerAnimated:YES completion:nil];
                                         }
                                         
                                         
                                     }];
    [alertResetPassword addAction:submitResetPwd];
    
    
    
    [alertResetPassword addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"New Password";
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.secureTextEntry = YES;
    }];
    [alertResetPassword addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Confirm Password";
        textField.secureTextEntry = YES;
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    [self presentAlertViewController:alertResetPassword OnViewController:viewController];
    
    // return password;
    
}
-(void) request_ResetPassword :(NSString *) newPassword
{
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    [xmlStr appendFormat:@"<UserRegistrationInfo><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [xmlStr appendFormat:@"<hubCitiKey>%@</hubCitiKey>",HUBCITIKEY];
    NSLog(@"%@",HUBCITIKEY);
    [xmlStr appendFormat:@"<password>%@</password></UserRegistrationInfo>", newPassword];
    //  NSString *urlString = [NSString stringWithFormat:@"http://sdw2730:8080/HubCiti2.6/firstuse/v2/resetpassword"];
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/v2/resetpassword",BASE_URL];
    NSLog(@"XMl string %@",xmlStr);
    NSString *responseXML =  [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    [self parseResetPassword:responseXML];
    
}
-(void) parseResetPassword :(NSString *) response
{
    if ([UtilityManager isNullOrEmptyString:response]) {
        ReleaseAndNilify(response);
        return;
    }
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement * responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    TBXMLElement * responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
    
    if ([TBXML textForElement:responseCodeElement] != nil)
    {
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:[TBXML textForElement:responseTextElement] message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
        
        NSLog(@"Reset Password : %@",[TBXML textForElement:responseTextElement]);
        
        //[alert release];
    }
    
}
- (void)emailCountDisplay:(NSNotification*)notification onViewController : (UIViewController*) viewController
{
    __block UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    alertWindow.rootViewController =  [[UIViewController alloc] init];
    
    UIWindow *topWindow = [UIApplication sharedApplication].windows.lastObject;
    alertWindow.windowLevel = topWindow.windowLevel + 1;
    
    [alertWindow makeKeyAndVisible];
    
    
    
    
    
    
    UIAlertController * emailAlert=[UIAlertController alertControllerWithTitle:@"Warning!" message:@"Please configure your email so that we can send an email to reset password if you forget." preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* okEmail = [UIAlertAction
                              actionWithTitle:@"OK"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [emailAlert dismissViewControllerAnimated:YES completion:nil];
                                  
                                  [self  requestToGetUserData: viewController];
                                  //                                  UserInformationViewController *userInfo = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
                                  //                                   dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1*(double) NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                  //                                   [[[[[UIApplication sharedApplication] delegate] window] rootViewController].navigationController pushViewController:userInfo animated:NO];
                                  //                                       [viewController.menuContainerViewController.centerViewController pushViewController:userInfo animated:NO];
                                  //   });
                                  alertWindow.hidden = YES;
                                  alertWindow = nil;
                                  
                              }];
    UIAlertAction* cancelEmail = [UIAlertAction
                                  actionWithTitle:@"Cancel"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      
                                      [emailAlert dismissViewControllerAnimated:YES completion:nil];
                                      alertWindow.hidden = YES;
                                      alertWindow = nil;
                                      
                                  }];
    
    [emailAlert addAction:okEmail];
    [emailAlert addAction:cancelEmail];
     [alertWindow.rootViewController presentViewController:emailAlert animated:YES completion:nil];
   // [self presentAlertViewController:emailAlert OnViewController:viewController];
    // return 0;
}


- (void)presentAlertViewController:(UIAlertController*)alert OnViewController:(UIViewController*)vc {
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1*(double) NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
    //    });
    
}
-(void)requestToGetUserData : (UIViewController*) viewController
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if ([defaults  valueForKey:KEY_USERID]) {
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
    }
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    DLog(@"parameter: %@",parameters);
    NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
    [HubCitiAppDelegate removeActivityIndicator];
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    DLog(@"%@",responseData);
    [self parseGetUserData:responseData:viewController];
    
    
}


-(void)parseGetUserData:(id)response : (UIViewController*) viewController
{
    if (response == nil)
        return;
    if (infoResponse == nil) {
        infoResponse = [[GetUserInfoResponse alloc] init];
    }
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    NSLog(@"response = %@", infoResponse);
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        [SharedManager setUserInfoFromSignup:NO];
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        [viewController.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
        [viewController.menuContainerViewController.centerViewController pushViewController:inform animated:NO];
        //[inform release];
    }
    else{
        NSString *responseTextStr = infoResponse.responseText;
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
    }
    
    
}

-(void) signupPopUp:(id)_delete{
    signUpDelegate = _delete;
    UIAlertController * alert;
    
    alert=[UIAlertController alertControllerWithTitle:@"Sign up for free to use this feature" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Continue"
                         style:UIAlertActionStyleCancel
                         handler:nil];
    [alert addAction:ok];
    UIAlertAction* signup = [UIAlertAction
                             actionWithTitle:@"Sign up"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action){
                                 [signUpDelegate logOutPressed];
                             }];
    [alert addAction:signup];
    
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
}

-(void) setGPSAsAllowForUser
{
    NSString *reqStr = [NSString stringWithFormat:@"<AuthenticateUser><deviceID>%@</deviceID><userId>%@</userId><hubCitiId>%@</hubCitiId></AuthenticateUser>",[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusersettings",BASE_URL];
   
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:reqStr];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
    if (responseCodeElement != nil && ![[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
        
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        return;
    }
    
    TBXMLElement *localeRadiusElement = [TBXML childElementNamed:@"localeRadius" parentElement:tbxml.rootXMLElement];
    TBXMLElement *pushNotifyElement = [TBXML childElementNamed:@"pushNotify" parentElement:tbxml.rootXMLElement];
    TBXMLElement *bLocServiceElement = [TBXML childElementNamed:@"bLocService" parentElement:tbxml.rootXMLElement];
    if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"false"])
    {
        [SharedManager setGps_allow_flag:NO];
        [defaults setValue:@"NO" forKey:@"allowingGPS"];
    }
    else if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"true"])
    {
        [SharedManager setGps_allow_flag:YES];
        [defaults setValue:@"YES" forKey:@"allowingGPS"];
    }
    NSString * radius  = [TBXML textForElement:localeRadiusElement];
    NSString * pushNotifyString;
    if(userIdentifier){
        pushNotifyString = @"false";
    }
    else{
        if ([[TBXML textForElement:pushNotifyElement] isEqualToString:@"true"]) {
            pushNotifyString = @"true";
        }
        else {
            pushNotifyString = @"false";
        }
    }
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<UserSettings><userId>%@</userId><deviceId>%@</deviceId>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_DEVICEID]];
    [requestStr appendFormat:@"<pushNotify>%@</pushNotify><hubCitiId>%@</hubCitiId>",pushNotifyString,[defaults valueForKey:KEY_HUBCITIID]];
    [requestStr appendFormat:@"<bLocService>true</bLocService>"];
    [SharedManager setGps_allow_flag:YES];
    [defaults setValue:@"YES" forKey:@"allowingGPS"];
    
    
    if(radius.length>0)
    {
        [requestStr appendFormat:@"<localeRadius>%@</localeRadius></UserSettings>",radius];
    }
    else
    {
        if(userIdentifier)
            [requestStr appendFormat:@"<localeRadius>99</localeRadius></UserSettings>"];
        else
            [requestStr appendFormat:@"<localeRadius>50</localeRadius></UserSettings>"];
    }
    
    
    NSString *urlReqString = [NSString stringWithFormat:@"%@firstuse/setusersettings",BASE_URL];
    NSString *responseXmlData = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlReqString withParam:requestStr]];
    
    NSLog(@"%@",responseXmlData);
    
}
@end

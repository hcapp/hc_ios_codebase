//
//  CommonUtilityNews.h
//  HubCiti
//
//  Created by Ashika on 6/9/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainMenuResponse.h"
#import "HubCitiConnectionManager.h"
#import "GetUserInfoResponse.h"

@protocol signUpDelegate
-(void)logOutPressed;
@end

@interface CommonUtilityNews : NSObject
{
    HubCitiConnectionManager *connectionManager;
    GetUserInfoResponse* infoResponse ;
    
  
}
@property (nonatomic,weak) id <signUpDelegate> signUpDelegate;
-(void) signupPopUp:(id)_delete;
-(void) refreshLinkDeptArray;
-(void) setUserSettings;
-(void) pushMainmenu:(UIViewController *) viewVC;
-(void) setNewsAsSubMenu:(UIViewController *) viewController : (NSInteger)level;
-(void) pushViewFromHamberger:(UIViewController *) viewVC;
-(void) setLoginVCAsRoot;
-(void) setNewsTemplateAsRoot;
-(void) setGPSAsAllowForUser;
-(UIButton*) customizedSideMenu:(BOOL)mainmenu;
-(void) saveMenuData : (MainMenuResponse*) menuResponse;
- (void)tempPasswordDisplay:(NSNotification*)notification onViewController : (UIViewController*) viewController;
- (void)emailCountDisplay:(NSNotification*)notification onViewController : (UIViewController*) viewController;
+(id)utilityManagers;
@end

//
//  BookMarksViewController.h
//  HubCiti
//
//  Created by Ashika on 6/1/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum webServicescallType
{
    BOOKMARK_GET,
    BOOKMARK_UPDATE,
    SIDENAVIGATION_GET,
    SIDENAVIGATION_UPDATE
}Webstate;
@interface BookMarksViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CustomizedNavControllerDelegate>{
    
    Webstate iWebState;
    UITableView *bookMarkTable,*sideNavigationTable;
    UIScrollView *scrollView;
}

@property(nonatomic)BOOL comingFromSideNews;
@end

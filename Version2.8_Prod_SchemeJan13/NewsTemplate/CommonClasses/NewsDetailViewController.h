//
//  NewsDetailViewController.h
//  pageSwipe
//
//  Created by Ashika on 5/24/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "DWBubbleMenuButton.h"
#import "EmailShareViewController.h"
@class EmailShareViewController;


@interface NewsDetailViewController : UIViewController<UIWebViewDelegate,UIScrollViewDelegate,DWBubbleMenuViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,CustomizedNavControllerDelegate>{
    UIWebView *longDescWebView;
    UIScrollView *scrollView;
     DWBubbleMenuButton *upMenuView;
}
@property(strong,nonatomic) NSNumber *newsID;

@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@end

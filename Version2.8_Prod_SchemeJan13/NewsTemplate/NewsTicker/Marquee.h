//
//  Marquee.h
//  HubCiti
//
//  Created by service on 10/4/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HubCitiConstants.h"
#import  "UIColor+ExtraColorSpecifications.h"
@protocol NewsTickerDelegate <NSObject>
- (void) navigateToNewsDetailScreen : (NSNumber*) newsID;
@end
@interface Marquee : UIView <UIScrollViewDelegate>
{
    NSMutableArray* titleArray;
    NSMutableArray * newsIDArray;
    NSMutableArray* array_TicketNewsDetail;
    UIScrollView* scrollView;
    NSTimer* scrollingTimer;
    CGFloat contentWidth,contentHeight;
    UITapGestureRecognizer *TapRecognizer;
    CGFloat textWidth,textHeight;
    BOOL startScrolling;
    MarqueeType  marqType;    
    UITapGestureRecognizer* tapRecognizer;
    UILabel* rotateBtn;
    __block int count;
}
@property (nonatomic,weak) id <NewsTickerDelegate> newsTickerDelegate;
@property(nonatomic,strong) NSString* textColor;
@property(nonatomic,strong) NSString* tickerBkgrdColor;

- (id) initWithFrame:(CGRect)frame;
-(void) startAnimateTickerWithType : (MarqueeType) type : (NSMutableArray*) tickerNewsDetail : (UIView*) senderView;
-(void) buttonTapped : (UITapGestureRecognizer*)sender;
@end

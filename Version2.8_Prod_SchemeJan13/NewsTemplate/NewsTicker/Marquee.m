//
//  Marquee.m
//  HubCiti
//
//  Created by service on 10/4/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "Marquee.h"
#import "NewsDetailViewController.h"
#import "MainMenuViewController.h"

#define HORIZONTAL_SCROLLING_TIME_INTERVAL   0.01
#define VERTICAL_SCROLLING_TIME_INTERVAL   0.02
#define SCROLLING_PIXEL_DISTANCE  0.5
#define HORIZONTAL_TEXT_GAP 20
#define VERTICAL_TEXT_GAP 10
#define FONT_SIZE 14


@implementation Marquee
@synthesize newsTickerDelegate;

#pragma mark INITILISE NEWS TICKER:

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

-(void) startAnimateTickerWithType : (MarqueeType) type : (NSMutableArray*) tickerNewsDetail : (UIView*) senderView
{
    marqType = type;
    titleArray = [[NSMutableArray alloc]init];
       newsIDArray = [[NSMutableArray alloc]init];
    array_TicketNewsDetail = [tickerNewsDetail copy];
    for (int i = 0; i< tickerNewsDetail.count; i++)
    {
        [titleArray insertObject:[[tickerNewsDetail objectAtIndex:i] title] atIndex:i];
    }
    for (int i = 0; i< tickerNewsDetail.count; i++)
    {
        [newsIDArray insertObject:[[tickerNewsDetail objectAtIndex:i] rowCount] atIndex:i];
    }
   
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, MARQEE_HEIGHT)];
    scrollView.backgroundColor = [UIColor convertToHexString:self.tickerBkgrdColor];
    scrollView.delegate = self;
    scrollView.userInteractionEnabled = TRUE;
    [senderView addSubview:scrollView];
    
    
    switch (type) {
        case SCROLL_LEFT_TO_RIGHT:
        case SCROLL_RIGHT_TO_LEFT:
        case SCROLL_UP_TO_DOWN:
        case SCROLL_DOWN_TO_UP:
        {
            if (type == SCROLL_LEFT_TO_RIGHT || type == SCROLL_UP_TO_DOWN) {
                titleArray = [[[titleArray reverseObjectEnumerator] allObjects] copy];
                newsIDArray = [[[newsIDArray reverseObjectEnumerator] allObjects] copy];
            }
            [self scrollTickerNews];
        }
        break;
       
        case ROTATE_RIGHT_TO_LEFT:
        case ROTATE_LEFT_TO_RIGHT:
        case ROTATE_DOWN_TO_UP:
        case ROTATE_UP_TO_DOWN:
        {
            [self rotateTickerNews];
        }
            break;
            
        default:
            break;
    }
    
}

#pragma mark TEXT DIMENSION CALCULATION:

-(void) calculateAllHeight
{
    for (int i = 0; i< titleArray.count; i++) {
        
        CGSize labelSize = [self labelSizeForText:titleArray[i] forFont:[UIFont boldSystemFontOfSize:FONT_SIZE]];
        
        textHeight += labelSize.height;
    }
    
}
-(void) calculateAllWidth
{
    for (int i = 0; i< titleArray.count; i++) {
        
        CGSize labelSize = [self labelSizeForText:titleArray[i] forFont:[UIFont boldSystemFontOfSize:FONT_SIZE]];
        
        textWidth += labelSize.width ;
    }
    
}


- (CGSize)labelSizeForText:(NSString *)text forFont:(UIFont *)font {
    
    CGFloat textwidth = 0.0;
   
    if(marqType == SCROLL_LEFT_TO_RIGHT || marqType == SCROLL_RIGHT_TO_LEFT){
        
        textwidth = MAXFLOAT;
    }
    else{
        
        textwidth = self.bounds.size.width;
    }
    
    CGRect labelRect = [text
                        boundingRectWithSize:CGSizeMake(textwidth, MARQEE_HEIGHT)
                        options:NSStringDrawingUsesLineFragmentOrigin
                        attributes:@{
                                     NSFontAttributeName :font
                                     }
                        context:nil];
    
    return labelRect.size;
    
}

#pragma mark SCROLL NEWS TICKER:

-(void) scrollTickerNews
{
    NSInteger x = 0;
    NSInteger y = 0;
    CGFloat width = 0.0,height = 0.0;
    
    [self calculateAllWidth];
    [self calculateAllHeight];
    
    for (int i = 0; i< titleArray.count; i++) {
        
        tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonTapped:)];
        [tapRecognizer setNumberOfTapsRequired:1];
        UILabel* lbl = [[UILabel alloc] init];
        CGSize labelSize = [self labelSizeForText:titleArray[i] forFont:[UIFont boldSystemFontOfSize:FONT_SIZE]];
        
        switch (marqType) {
                
            case SCROLL_LEFT_TO_RIGHT:
            {
                lbl.frame = CGRectMake(x ,(MARQEE_HEIGHT - labelSize.height)/2,-(labelSize.width + HORIZONTAL_TEXT_GAP) ,labelSize.height);
                
                x -= (labelSize.width + HORIZONTAL_TEXT_GAP);
                
                width += (labelSize.width + HORIZONTAL_TEXT_GAP);

            }
                
                break;
                
            case SCROLL_RIGHT_TO_LEFT:
            {
                lbl.frame = CGRectMake(SCREEN_WIDTH + x,(MARQEE_HEIGHT - labelSize.height)/2,labelSize.width + HORIZONTAL_TEXT_GAP,labelSize.height);
                
                x += labelSize.width + HORIZONTAL_TEXT_GAP;
                
                width += (labelSize.width + HORIZONTAL_TEXT_GAP);

            }
                break;
            case SCROLL_UP_TO_DOWN:
            {
                
                lbl.lineBreakMode = NSLineBreakByWordWrapping;
                lbl.frame = CGRectMake(10,y,self.bounds.size.width - 20, - (labelSize.height + VERTICAL_TEXT_GAP));
                int no_lines = labelSize.height/FONT_SIZE;
                if (no_lines > 2) {
                    no_lines = 2;
                }
                
                lbl.numberOfLines = no_lines;
                
                y -= (labelSize.height + VERTICAL_TEXT_GAP);
                
                height += (labelSize.height + VERTICAL_TEXT_GAP);

            }
                break;
            case SCROLL_DOWN_TO_UP:
            {
               
                lbl.lineBreakMode = NSLineBreakByWordWrapping;
                lbl.frame = CGRectMake(10,(y + MARQEE_HEIGHT),self.bounds.size.width - 20 ,labelSize.height);
                int no_lines = labelSize.height/FONT_SIZE;
                if (no_lines > 2) {
                    no_lines = 2;
                }
                
                lbl.numberOfLines = no_lines;
                
                y += labelSize.height + VERTICAL_TEXT_GAP;
                
                height += (labelSize.height + VERTICAL_TEXT_GAP);
            }
                break;
                
            default:
                break;
        }
        
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.userInteractionEnabled = true;
        lbl.textColor = [UIColor convertToHexString:self.textColor];
        lbl.text = titleArray[i];
        lbl.font = [UIFont boldSystemFontOfSize:FONT_SIZE];
        //[lbl sizeToFit];
        lbl.tag = i;
        [scrollView addSubview:lbl];
        [lbl addGestureRecognizer:tapRecognizer];
       
    }

   [self setContentSize : width : height];
  
}

-(void) setContentSize : (CGFloat) width : (CGFloat) height
{
    switch (marqType) {
            
        case SCROLL_LEFT_TO_RIGHT:
        {
            contentWidth = (2 * SCREEN_WIDTH + width);
            [scrollView setContentSize:CGSizeMake(textWidth, MARQEE_HEIGHT)];
        }
            break;
        case SCROLL_RIGHT_TO_LEFT:
        {
            contentWidth = 2 * SCREEN_WIDTH + width;
            [scrollView setContentSize:CGSizeMake(contentWidth, MARQEE_HEIGHT)];
            
        }
            break;
        case SCROLL_UP_TO_DOWN:
        {
            contentHeight = 2 * MARQEE_HEIGHT + height;
            [scrollView setContentSize:CGSizeMake(SCREEN_WIDTH, contentHeight)];
            
        }
            break;
        case SCROLL_DOWN_TO_UP:
        {
            contentHeight = 2 * MARQEE_HEIGHT + height;
            [scrollView setContentSize:CGSizeMake(SCREEN_WIDTH, contentHeight)];
        }
            break;
            
        default:
            break;
    }
    
    [self startScrolling];
    
}

- (void)startScrolling {
    
    CGFloat timeInterval = 0.0;
    
    if (!startScrolling) {
        startScrolling = YES;
        
        if (marqType == SCROLL_UP_TO_DOWN || marqType == SCROLL_DOWN_TO_UP) {
            timeInterval = VERTICAL_SCROLLING_TIME_INTERVAL;
        }
        else{
            timeInterval = HORIZONTAL_SCROLLING_TIME_INTERVAL;
        }
        scrollingTimer = [NSTimer scheduledTimerWithTimeInterval:timeInterval
                                                          target:self
                                                        selector:@selector(scroll:)
                                                        userInfo:nil
                                                         repeats:YES];
    }
}


- (void)stopScrolling {
    if (startScrolling) {
        [scrollingTimer invalidate];
        scrollingTimer = nil;
        startScrolling = NO;
    }
}

- (void)scroll:(NSTimer *)timer {
    
    switch (marqType) {
            
        case SCROLL_LEFT_TO_RIGHT:
        {
            if ([scrollView contentOffset].x <  - (SCREEN_WIDTH + textWidth + (titleArray.count - 1)*HORIZONTAL_TEXT_GAP ))
            {
                
                [scrollView setContentOffset:CGPointMake(0 , 0)];
                
            }
            
            CGPoint point = [scrollView contentOffset];
            
            point.x -= SCROLLING_PIXEL_DISTANCE;
            
            [scrollView setContentOffset:point];
        }
            break;
        case SCROLL_RIGHT_TO_LEFT:
        {
            if ([scrollView contentOffset].x >= contentWidth - SCREEN_WIDTH) {
                [scrollView setContentOffset:CGPointMake(0, 0)];
            }
            CGPoint point = [scrollView contentOffset];
            point.x += SCROLLING_PIXEL_DISTANCE;
            [scrollView setContentOffset:point];
        }
            break;
        case SCROLL_UP_TO_DOWN:
        {
            if ([scrollView contentOffset].y < -(MARQEE_HEIGHT + textHeight + (titleArray.count - 1)*VERTICAL_TEXT_GAP))
            {
                [scrollView setContentOffset:CGPointMake(0 , 0)];
            }
            
            CGPoint point = [scrollView contentOffset];
            
            point.y -= SCROLLING_PIXEL_DISTANCE;
            
            [scrollView setContentOffset:point];
        }
            break;
        case SCROLL_DOWN_TO_UP:
        {
            if ([scrollView contentOffset].y > contentHeight - MARQEE_HEIGHT)
            {
                [scrollView setContentOffset:CGPointMake(0 , 0)];
            }
            
            CGPoint point = [scrollView contentOffset];
            
            point.y += SCROLLING_PIXEL_DISTANCE;
            
            [scrollView setContentOffset:point];

        }
            break;
        
            
        default:
            break;
    }
    
}


#pragma mark ROTATE NEWS TICKER:

-(void) rotateTickerNews
{
    
    if (marqType == ROTATE_LEFT_TO_RIGHT || marqType == ROTATE_UP_TO_DOWN) {
        titleArray = [[[titleArray reverseObjectEnumerator] allObjects] copy];
        newsIDArray = [[[newsIDArray reverseObjectEnumerator] allObjects] copy];
    }
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonTapped:)];
    [scrollView addGestureRecognizer:tapRecognizer];
    count = 1;
    
    [self rotateNewsText : 0];
    
}
-(void) rotateNewsText : (int) i
{
    
    if(i < titleArray.count) {
        
        rotateBtn = [[UILabel alloc] init];
        
        CGSize labelSize = [self labelSizeForText:titleArray[i] forFont:[UIFont boldSystemFontOfSize:FONT_SIZE]];
        
        scrollView.userInteractionEnabled = true;
        rotateBtn.numberOfLines = 2;
        rotateBtn.font = [UIFont systemFontOfSize:FONT_SIZE];
        rotateBtn.lineBreakMode = NSLineBreakByWordWrapping;
        rotateBtn.textAlignment = NSTextAlignmentCenter;
        switch (marqType) {
            case ROTATE_LEFT_TO_RIGHT:
            {
                rotateBtn.frame = CGRectMake(-labelSize.width,(MARQEE_HEIGHT - labelSize.height)/2 ,self.bounds.size.width,labelSize.height);
            }
                break;
            case ROTATE_RIGHT_TO_LEFT:
            {
                rotateBtn.frame = CGRectMake(SCREEN_WIDTH + labelSize.width,(MARQEE_HEIGHT - labelSize.height)/2 ,self.bounds.size.width,labelSize.height);

            }
                break;
            case ROTATE_UP_TO_DOWN:
            {
                rotateBtn.frame = CGRectMake((SCREEN_WIDTH - (labelSize.width + 20))/2 + 20, - labelSize.height ,300,labelSize.height);
            }
                break;
            case ROTATE_DOWN_TO_UP:
            {
                rotateBtn.frame = CGRectMake((SCREEN_WIDTH - (labelSize.width + 20))/2 + 20,MARQEE_HEIGHT + labelSize.height,300,labelSize.height);
            }
                break;
                
            default:
                break;
        }
        
        rotateBtn.userInteractionEnabled = true;
        rotateBtn.textColor = [UIColor convertToHexString:self.textColor];
        rotateBtn.text = titleArray[i];
        rotateBtn.font = [UIFont boldSystemFontOfSize:FONT_SIZE];
       // [rotateBtn sizeToFit];
        rotateBtn.tag = i;
        
        [scrollView addSubview:rotateBtn];
        
        
        [UIView animateWithDuration:0.5 delay:0.0
        options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
        animations:^{
            rotateBtn.alpha = 1.0; // fade in
                             
            switch (marqType) {
                case ROTATE_LEFT_TO_RIGHT:
                {
                    rotateBtn.frame = CGRectMake((SCREEN_WIDTH - self.bounds.size.width)/2 , rotateBtn.frame.origin.y, rotateBtn.frame.size.width, rotateBtn.frame.size.height);
                }
                    break;
                case ROTATE_RIGHT_TO_LEFT:
                {
                    rotateBtn.frame = CGRectMake((SCREEN_WIDTH - self.bounds.size.width)/2 , rotateBtn.frame.origin.y, rotateBtn.frame.size.width, rotateBtn.frame.size.height);
                    
                }
                    break;
                case ROTATE_UP_TO_DOWN:
                {
                    rotateBtn.frame = CGRectMake(rotateBtn.frame.origin.x, (MARQEE_HEIGHT - (labelSize.height))/2, rotateBtn.frame.size.width, rotateBtn.frame.size.height);
                }
                    break;
                case ROTATE_DOWN_TO_UP:
                {
                    rotateBtn.frame = CGRectMake(rotateBtn.frame.origin.x, (MARQEE_HEIGHT - (labelSize.height))/2, rotateBtn.frame.size.width, rotateBtn.frame.size.height);
                }
                    break;
                    
                default:
                    break;
            }
            
        }
        completion:^(BOOL finished) {
            
            [UIView animateWithDuration:1
                                  delay:2
                                options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                             animations:^{
                                 rotateBtn.alpha = 0.3; // fade out
                                 
                                 switch (marqType) {
                                     case ROTATE_LEFT_TO_RIGHT:
                                     {
                                         rotateBtn.frame = CGRectMake(SCREEN_WIDTH + textWidth, rotateBtn.frame.origin.y, rotateBtn.frame.size.width, rotateBtn.frame.size.height);
                                     }
                                         break;
                                     case ROTATE_RIGHT_TO_LEFT:
                                     {
                                         rotateBtn.frame = CGRectMake(-(labelSize.width + 30), rotateBtn.frame.origin.y, rotateBtn.frame.size.width, rotateBtn.frame.size.height);
                                         
                                     }
                                         break;
                                     case ROTATE_UP_TO_DOWN:
                                     {
                                         rotateBtn.frame = CGRectMake(rotateBtn.frame.origin.x, MARQEE_HEIGHT + labelSize.height, rotateBtn.frame.size.width, rotateBtn.frame.size.height);
                                     }
                                         break;
                                     case ROTATE_DOWN_TO_UP:
                                     {
                                         rotateBtn.frame = CGRectMake(rotateBtn.frame.origin.x, - labelSize.height, rotateBtn.frame.size.width, rotateBtn.frame.size.height);
                                     }
                                         break;
                                         
                                     default:
                                         break;
                                 }
                                 
                             }
                             completion:^(BOOL finished) {
                                 [rotateBtn removeFromSuperview];
                                 if (count > titleArray.count - 1) {
                                     count = 0;
                                 }
                                 [self rotateNewsText:count];
                                 
                                 count = count+1;
                             }
             ];
            
                         }
         ];
        
    }
    
}

#pragma mark Button Action:

-(void) buttonTapped : (UITapGestureRecognizer*)sender {
    
    switch (marqType) {
            
        case ROTATE_RIGHT_TO_LEFT:
        case ROTATE_LEFT_TO_RIGHT:
        case ROTATE_DOWN_TO_UP:
        case ROTATE_UP_TO_DOWN:
        {
            [self.newsTickerDelegate navigateToNewsDetailScreen:newsIDArray[count-1]];
            
        }
            break;
        case SCROLL_RIGHT_TO_LEFT:
        case SCROLL_UP_TO_DOWN:
        case SCROLL_LEFT_TO_RIGHT:
        case SCROLL_DOWN_TO_UP:
        {
                [self.newsTickerDelegate navigateToNewsDetailScreen:newsIDArray[sender.view.tag]];

        }
            break;
            
        default:
            break;
    }
    [scrollView removeFromSuperview];
}
-(void) scrollViewDidScroll:(UIScrollView *)scrollViewNewsTicker
{
    scrollViewNewsTicker.scrollEnabled = NO;
}

@end

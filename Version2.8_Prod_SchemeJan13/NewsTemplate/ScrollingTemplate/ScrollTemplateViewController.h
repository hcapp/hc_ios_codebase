//
//  ScrollTemplateViewController.h
//  pageSwipe
//
//  Created by Nikitha on 5/23/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"

@interface ScrollTemplateViewController : UIViewController<UIPageViewControllerDataSource, UIPageViewControllerDelegate>
{
    NSMutableArray * widthArray;
    UIAlertAction* zipSave;
}

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *buttonLeft;
@property(nonatomic,strong)  NSMutableArray * buttonArray;
@property(nonatomic,strong)  NSMutableArray * bookMarkArr;
@property(nonatomic,strong)  NSMutableArray* backButtonColorArr;
@property (nonatomic,assign) NSUInteger bookMarkselected;
@property(nonatomic,strong) NSMutableArray * responseTextArray;
@property(nonatomic,assign) BOOL setBoolForRefreshAndBtn;
@property(nonatomic,assign) BOOL photoVideoExist;
@property(nonatomic,assign) BOOL fromSideMenu;

@end

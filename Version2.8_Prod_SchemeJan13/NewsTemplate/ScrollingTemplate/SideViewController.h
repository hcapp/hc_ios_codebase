//
//  SideViewController.h
//  HubCiti
//
//  Created by Ashika on 6/25/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JKExpandTableView.h"

#import "LocationManager.h"

#import <MessageUI/MessageUI.h>

#import "FindLocationServiceViewController.h"

#import <Social/Social.h>

#import "LoginDo.h"



@class DealHotDealsList;

@class FilterRetailersList;

@class RetailersListViewController;

@class CityExperienceViewController;

@class EventsListViewController;

@class FundraiserListViewController;

@class FilterListViewController;

@class FAQCategoryList;

@class ButtonSetupViewController;

@class AnyViewController;

@class EmailShareViewController;

@class BandViewController;

@class WebBrowserViewController;
@class BandsNearByEventsMapTable;

typedef enum webServiceTypes

{
    
    BOOKMARKS_GETs,
    
    SIDENAVIGATIONS_GETs,
    
    HUBCITI_SIDENAVIGATIONS_GETs
    
}Webstatess;





@interface SideViewController : UIViewController<JKExpandTableViewDelegate, JKExpandTableViewDataSource,HubCitiConnectionManagerDelegate,UITextFieldDelegate,LocationManagerDelegate,MFMessageComposeViewControllerDelegate,UITextFieldDelegate>{
    
    Webstatess iWebState;
    
    NSString *androidDownloadLink;
    
    NSString *retAffName;
    
    WebRequestState iWebRequestState;
    
    CGRect cellRectInTable;
    UITableViewCell *selectedCell;
    CommonUtility *common;
    UIAlertAction* zipSave;

}

@property(nonatomic,strong) NSMutableArray *bookMarkArray;

@property(nonatomic,strong) WebBrowserViewController *urlDetail;

@property(nonatomic,strong) EmailShareViewController *emailSendingVC;

@property(nonatomic,strong) AnyViewController * anyVC;

@property(nonatomic,strong) ButtonSetupViewController* iViewController;

@property(nonatomic,strong) FAQCategoryList *faqList;



@property(nonatomic,strong) FilterListViewController *filterList;

@property(nonatomic,strong) FundraiserListViewController *iFundraiserListViewController;

@property(nonatomic,strong) EventsListViewController* iEventsListViewController;

@property(nonatomic,strong) CityExperienceViewController *cityExp;

@property(nonatomic,strong) FilterRetailersList *filters;

@property(nonatomic,strong) RetailersListViewController* retailerListScreen;

@property(nonatomic,strong) DealHotDealsList* hotDeals;

@property(nonatomic,strong) BandViewController *bandViewPage;

@property(nonatomic,strong) LoginDO *loginDo;
@property(assign) BOOL isSideMenu;

@property(assign) BOOL isNewsTemplate;

@property (weak, nonatomic) IBOutlet JKExpandTableView *sidemenuTableView;
@property(nonatomic,strong) BandsNearByEventsMapTable *todayEventList;

@end

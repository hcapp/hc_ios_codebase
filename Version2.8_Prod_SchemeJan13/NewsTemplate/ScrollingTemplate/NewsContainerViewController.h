//
//  NewsContainerViewController.h
//  pageSwipe
//
//  Created by Nikitha on 5/23/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>

@interface NewsContainerViewController : UIViewController<WKUIDelegate, WKNavigationDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
     UIActivityIndicatorView *activityIndicator;
    
}

@property(nonatomic,assign) NSUInteger  index;
@property(nonatomic,strong) NSString * btnName;
@property(nonatomic,strong) NSString * nonFeedLink;
@property(nonatomic,strong) NSString* backButtonColor;
@property(nonatomic,strong)  UITableView *newsTableView;
@property(nonatomic,strong) UIFont * titleFont;
@property(nonatomic,strong) UIFont * subTitleFont;
@property(nonatomic,strong) UIFont *bodyFont;
@property (nonatomic,strong) UIRefreshControl * pullToRefresh;
@property(nonatomic,strong) NSMutableArray * array_ScrollNewsData;
@property(strong,nonatomic) NSNumber * totalNumOfCells;
@property(strong,nonatomic) NSNumber * nextPage;
@property(strong,nonatomic) NSString * responseText;
@property(assign,nonatomic) NSUInteger  numberOfBookMarks;
@property(strong,nonatomic) NSMutableArray * receivedResponseText;
@property(nonatomic) BOOL refreshOrButtonAction;
@property(nonatomic) BOOL refreshMade;
@property (nonatomic,strong) NSMutableArray*toSendArray;
@property(nonatomic,strong) NSMutableArray * bookMarkList;
@property(nonatomic,strong) UICollectionView * scrollCollectionView;
@property(nonatomic,strong) UIView *footer;
@property(nonatomic) BOOL bannerChanged;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *buttonRight;

@property (nonatomic, strong) WKWebView* webView;
@property (nonatomic, strong) IBOutlet UIToolbar* toolbar;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* back;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* forward;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* refresh;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* stop;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

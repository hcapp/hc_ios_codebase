//
//  SearchBtnViewController.h
//  pageSwipe
//
//  Created by Nikitha on 5/19/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchBtnViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,CustomizedNavControllerDelegate>
{
    NSTimer * timer;
}
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarOutlet;
@property (strong, nonatomic) IBOutlet UITableView *searchTableView;
@property (strong, nonatomic) NSArray * data_Before_Search;
@property(nonatomic,strong) NSNumber * nextPage;
@property(nonatomic,strong) NSNumber * totalNumOfCells;
@property(nonatomic,strong) NSMutableArray * array_searchNewsList;
@property(nonatomic,strong) NSString * responseText;
@property(nonatomic,strong) UIFont * titleFont;
@property(nonatomic,strong) UIFont * subTitleFont;
@property(nonatomic,strong) UIFont *bodyFont;
@property(nonatomic,strong) NSString * searchBarText;


@end

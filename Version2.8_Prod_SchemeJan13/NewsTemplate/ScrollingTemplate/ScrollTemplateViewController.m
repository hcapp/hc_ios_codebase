//
//  ScrollTemplateViewController.m
//  pageSwipe
//
//  Created by Nikitha on 5/23/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import "ScrollTemplateViewController.h"
#import "NewsContainerViewController.h"
#import "BookMarksViewController.h"
#import "SearchBtnViewController.h"
#import "HubCitiConstants.h"
#import "AppDelegate.h"
#import "BookMarkGetRequest.h"
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "NewsDetailViewController.h"
#import "PhotoView.h"
#import "SideViewController.h"
#import "WebBrowserViewController.h"

@interface ScrollTemplateViewController ()< UIScrollViewDelegate,UIGestureRecognizerDelegate, HTTPClientDelegate,UITextFieldDelegate,CustomizedNavControllerDelegate>
{
    
    UIScrollView * buttonScroll;
    UIButton * buttonList;
    NewsContainerViewController * contentViewController;
    float xVal;
    CGFloat newOffset;
    UIButton * moreBtn;
    UIButton * rightSearchBtn;
    UIButton * leftSearchBtn;
    UIView * tabBarView;
    CustomizedNavController *cusNav ;
    
}
@property(nonatomic,strong) BookMarkGetRequest * obj_bkMarkListResponse;

@end

@implementation ScrollTemplateViewController

@synthesize pageViewController,buttonArray,buttonLeft,bookMarkselected,obj_bkMarkListResponse,responseTextArray,setBoolForRefreshAndBtn,fromSideMenu,bookMarkArr,backButtonColorArr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
    
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    [self parse_bookmarkCategories:responseObject];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [defaults setBool:NO forKey:@"isCommingFromWebView"];
    [defaults setBool:NO forKey:@"isDonePressed"];
    [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
   
    [defaults setBool:NO forKey:@"firstPopUp"];
    [defaults setBool:YES forKey:@"NoPopUp"];
    if(fromSideMenu){
        NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        
        
        // You can pass your index here
        NSInteger vcCount = [viewControllers count];
        
        for(NSInteger i = vcCount - 2  ; i >= 0 ;i--) {
            id viewController = [viewControllers objectAtIndex:i];
            if ([viewController isKindOfClass: [ScrollTemplateViewController class]]) {
                [viewControllers removeObjectAtIndex: i];
                break;
            }
            
            
        }
        NSLog(@"viewc:%@",viewControllers);
        self.navigationController.viewControllers = viewControllers;
    }
    
    NSLog(@"---->%@",[defaults valueForKey:KEY_HUBCITIID]);
    //add hamburger button at top
    
    
    self.navigationItem.hidesBackButton = YES;
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageViewController.delegate = self;
    self.pageViewController.dataSource = self;
    
    
    pageViewController.view.frame =CGRectMake(0, VARIABLE_HEIGHT(28), self.view.frame.size.width, SCREEN_HEIGHT);
    [self addChildViewController:self.pageViewController];
    [[self view] addSubview:[self.pageViewController view]];
    
    
    [self.pageViewController didMoveToParentViewController:self];
    
    
//    //setup
//    [self setUpTopTabBar];
    
//    if (bookMarkselected)
//    {
//        [defaults setValue:nil forKey:@"bannerImgScroll"];
//        [self setColor : bookMarkselected : nil];
//        
//        [defaults setInteger:bookMarkselected forKey:@"currentIndex"];
//    }
//    else{
//        
//        [defaults setInteger:bookMarkselected forKey:@"currentIndex"];
//    }
    
    //setup popup for the first
    if (userIdentifier==FALSE && ![defaults boolForKey:@"pushOnKill"]){
        int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
        if (menulevel<=1) {
            [self checkLocationStatus];
            
            [self checkForRadius];
        }
    }
    else if([defaults boolForKey:@"pushOnKill"]){
        [defaults setBool:NO forKey:@"pushOnKill"];
    }

    if( bookMarkselected  == 0)
    {
        [defaults setValue:nil forKey:@"bannerImgScroll"];
    }
    
    //    responseTextArray = [[NSMutableArray alloc]init];
    //    for (int i=0;i< buttonArray.count ; i++)
    //    {
    //        [responseTextArray addObject:@""];
    //    }
    //
    //    NSArray *viewControllers = [NSArray arrayWithObject:[self viewControllerAtIndex:bookMarkselected]];
    //    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    //
    
    
}


-(void) initMenuData
{
    
    
    if (FindBottomButtonID!=Nil)
    {
        [FindBottomButtonID removeAllObjects];
    }
    else
    {
        if (FindBottomButtonID==Nil) {
            FindBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (EventsBottomButtonID!=Nil)
    {
        [EventsBottomButtonID removeAllObjects];
    }
    else
    {
        if (EventsBottomButtonID==Nil) {
            EventsBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (FilterBottomButtonID!=Nil)
    {
        [FilterBottomButtonID removeAllObjects];
    }
    else
    {
        if (FilterBottomButtonID==Nil) {
            FilterBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    
    if (FindSingleBottomButtonID!=Nil)
    {
        [FindSingleBottomButtonID removeAllObjects];
    }
    else
    {
        if (FindSingleBottomButtonID==Nil) {
            FindSingleBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (CityExperienceBottomButtonID!=Nil)
    {
        [CityExperienceBottomButtonID removeAllObjects];
    }
    else
    {
        if (CityExperienceBottomButtonID==Nil) {
            CityExperienceBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (FundraiserBottomButtonID!=Nil)
    {
        [FundraiserBottomButtonID removeAllObjects];
    }
    else
    {
        if (FundraiserBottomButtonID==Nil) {
            FundraiserBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (NearbyBottomButtonID!=Nil)
    {
        [NearbyBottomButtonID removeAllObjects];
    }
    else
    {
        if (NearbyBottomButtonID==Nil) {
            NearbyBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    
    
    if (linkID==NULL) {
        linkID=[[NSMutableArray alloc]init];
    }
    else
    {
        
        [linkID removeAllObjects];
        
    }
    
    if (dateCreated==NULL) {
        dateCreated = [[NSMutableDictionary alloc]init];
    }
    
    if (cashedResponse==NULL) {
        cashedResponse = [[NSMutableDictionary alloc]init];
    }
    
    
    if (deptId==NULL) {
        deptId = [[NSMutableDictionary alloc]init];
    }
    
    if (typeId==NULL) {
        typeId = [[NSMutableDictionary alloc]init];
    }
    
    if (selectedSortOption==NULL) {
        selectedSortOption=[[NSMutableDictionary alloc]init];
    }
    
    if (selectedFilterOption==NULL) {
        selectedFilterOption=[[NSMutableDictionary alloc]init];
    }
    
    if (selectedSortCityDic==NULL) {
        selectedSortCityDic=[[NSMutableDictionary alloc]init];
        
    }
    [defaults setBool:NO forKey:BottomButton];
    
    if([defaults valueForKey:KEYZIPCODE] && [[defaults valueForKey:KEYZIPCODE]isEqualToString:@"N/A"])
    {
        [defaults setValue:nil forKey:KEYZIPCODE];
    }
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    if(menulevel<=1){
        [NewsUtility refreshLinkDeptArray];
    }
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}

-(void) viewWillAppear:(BOOL)animated
{
   
    [super viewWillAppear:animated];
    if ([defaults boolForKey:@"isCommingFromWebView"]) {
        return;
    }
     [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [defaults setBool:YES forKey:@"LoginSuccess"];
    [defaults setBool:NO forKey:@"canDisplayAPNSListScreen"];
     cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideBackButton:YES];
    [cusNav hideHambergerButton: NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    NSLog(@" current index is %ld",(long)[defaults integerForKey:@"currentIndex"]);
    if (userIdentifier==FALSE)
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {// iOS 8.0 and later
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        } else { // below iOS 8.0
            
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        }
    }
    
    [self initMenuData];
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    if(menulevel > 1)
    {
         [cusNav hideBackButton:NO];
    }
    if(menulevel<=1){
        // [[NSNotificationCenter defaultCenter] postNotificationName:@"homeClicked" object:nil];
        [defaults setValue:@"Scrolling News Template" forKey:@"centerViewController"];
    }
    // tab bar with book marks
    NSLog(@"center:%@",[defaults valueForKey:@"centerViewController"]);
    [defaults setBool:YES forKey:@"NoPopUp"];
    
    //setup side menu
    if(![defaults boolForKey:@"isDonePressed"])
    {
        SideViewController *leftMenuViewController;
        if(IPAD){
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
        }
        else{
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
            
        }
        leftMenuViewController.isNewsTemplate = TRUE;
        if(menulevel > 1)
        {
            leftMenuViewController.isSideMenu = YES;
        }
        [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
       // [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }

    
    if( [defaults boolForKey:@"isDonePressed"])
    {
        [defaults setInteger:0 forKey:@"currentIndex"];
        [defaults setBool:NO forKey:@"isDonePressed"];
//        [self setUpTopTabBar];
    }
    [self setUpTopTabBar];
    if ([defaults boolForKey:@"isComingFromDetail"])
    {
        int indexNum = 0;
        for (int i= 0;i<buttonArray.count ;i++)
        {
            if ([buttonArray[i] isEqualToString:[defaults valueForKey:@"categoryNameInScroll"]])
            {
                [defaults setValue:nil forKey:@"categoryNameInScroll"];
                indexNum = i;
                break;
            }
        }
        [self setColor:indexNum :nil];
        [defaults setInteger:indexNum forKey:@"currentIndex"];
        [defaults setObject:nil forKey:@"isComingFromDetail"];
    }
    else if (bookMarkselected)
    {
        [defaults setValue:nil forKey:@"bannerImgScroll"];
        [self setColor : bookMarkselected : nil];
        
        [defaults setInteger:bookMarkselected forKey:@"currentIndex"];
    }
    else{
        
        [defaults setInteger:bookMarkselected forKey:@"currentIndex"];
    }

    
    responseTextArray = [[NSMutableArray alloc]init];
    for (int i=0;i< buttonArray.count ; i++)
    {
        [responseTextArray addObject:@""];
    }
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]], NSForegroundColorAttributeName,[UIFont fontWithName:@"Helvetica-Bold" size:17.0], NSFontAttributeName,nil]];
    NSArray *viewControllers;
  
    {
        viewControllers = [NSArray arrayWithObject:[self viewControllerAtIndex:[defaults integerForKey:@"currentIndex"]]];
    }
    
    
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    
    
    
    
    
}
- (CGFloat)widthOfString:(NSString *)labelString withFont:(UIFont *)font
{
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:labelString attributes:attributes] size].width;
    
}
-(void) setUpTopTabBar
{
    tabBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, VARIABLE_HEIGHT(30))];
    tabBarView.backgroundColor = [UIColor convertToHexString:@"#f1f1f1"];
    [self.view addSubview:tabBarView];
    //Left Search Button for old news search
    //    leftSearchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    leftSearchBtn.frame =CGRectMake(VARIABLE_WIDTH(10), tabBarView.frame.size.height/2 - VARIABLE_HEIGHT(15)/2, VARIABLE_WIDTH(15),VARIABLE_HEIGHT(15));
    //    [leftSearchBtn setBackgroundImage:[UIImage imageNamed:@"search2.png"] forState:UIControlStateNormal];
    //    //[tabBarView addSubview:leftSearchBtn];
    //    [leftSearchBtn addTarget:self action:@selector(leftSearchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    //Scroll View for book marks
    float width;
    if(IPAD)
    {
        width=(SCREEN_WIDTH-VARIABLE_WIDTH(50))/4;
    }
    else
    {
        width=(SCREEN_WIDTH-VARIABLE_WIDTH(50))/3;
    }
    buttonScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(VARIABLE_WIDTH(15), 0, SCREEN_WIDTH-VARIABLE_WIDTH(50), VARIABLE_HEIGHT(30))];
    buttonScroll.showsHorizontalScrollIndicator = YES;
    buttonScroll.showsVerticalScrollIndicator = NO;
    buttonScroll.alwaysBounceVertical = NO;
    buttonScroll.delegate = self;
    buttonScroll.backgroundColor = [UIColor convertToHexString:@"#f1f1f1"];
    [tabBarView addSubview:buttonScroll];
    
    
    // More button
    //    moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    moreBtn.frame = CGRectMake(buttonScroll.frame.size.width + buttonScroll.frame.origin.x +VARIABLE_WIDTH(15) , 0 ,VARIABLE_WIDTH(45), VARIABLE_HEIGHT(30));
    //    //  moreBtn.backgroundColor=[UIColor blueColor];
    //    [tabBarView addSubview:moreBtn];
    //
    //    // image for more button
    //    UIImageView * moreImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, moreBtn.frame.size.height/2 - VARIABLE_HEIGHT(6) , VARIABLE_HEIGHT(12),VARIABLE_HEIGHT(12))];
    //    moreImage.image = [UIImage imageNamed:@"more2.png"];
    //    [moreBtn addSubview:moreImage];
    //
    //    //label for more button
    //    UILabel * moreLabel = [[UILabel alloc]initWithFrame:CGRectMake(moreImage.frame.size.width + VARIABLE_WIDTH(4), 0,moreBtn.frame.size.width-moreImage.frame.size.width, VARIABLE_HEIGHT(30))];
    //    moreLabel.text = @"More";
    //    moreLabel.font = [UIFont systemFontOfSize:(IPAD ? 16.0f:12.0f)];
    //    moreLabel.textColor = [UIColor darkGrayColor];
    //    [moreBtn addSubview:moreLabel];
    //    [moreBtn addTarget:self action:@selector(moreButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    //Right Search button for Current news search
    rightSearchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightSearchBtn.frame =CGRectMake(SCREEN_WIDTH- VARIABLE_WIDTH(28),tabBarView.frame.size.height/2 - VARIABLE_HEIGHT(15)/2, VARIABLE_WIDTH(15),VARIABLE_HEIGHT(15));
    [rightSearchBtn setBackgroundImage:[UIImage imageNamed:@"search2.png"] forState:UIControlStateNormal];
    [tabBarView addSubview:rightSearchBtn];
    [rightSearchBtn addTarget:self action:@selector(rightSearchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    // Array to display all the book mark button in scroll view
    xVal=0;
    [self request_newsListing];
    // buttonArray = [[NSMutableArray alloc]initWithObjects:@"Top Stories",nil];
    newOffset=0;
    widthArray = [[NSMutableArray alloc]init];
    for (int i=0;i< buttonArray.count ; i++)
    {
        [widthArray addObject:@""];
    }
    //buttonArray = [[NSMutableArray alloc]initWithObjects:@"Mablefalls",@"jdhfiohg",@"marble falls marblefalls marble falls marble falls",@"khdfi", nil];
    if (buttonArray.count == 1)
    {
        float width_For_One_Cat =(SCREEN_WIDTH-VARIABLE_WIDTH(50));
        for(int i=0;i<buttonArray.count;i++)
        {
            buttonList = [UIButton buttonWithType:UIButtonTypeCustom];
            buttonList.frame = CGRectMake(0, 0,width_For_One_Cat, VARIABLE_HEIGHT(30));
            [buttonList setTitle:buttonArray[i] forState:UIControlStateNormal];
            buttonList.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 16.0f:12.0f)];
            buttonList.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonList.titleLabel.lineBreakMode=NSLineBreakByTruncatingTail;
            [buttonList setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            buttonList.tag = i;
            buttonList.userInteractionEnabled = NO;
            if( buttonList.tag == 0)
            {
                
                [buttonList setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            [buttonList addTarget:self action:@selector(bookMarkBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            [buttonScroll addSubview:buttonList];
            xVal = xVal +width;
        }
        // COntent size for book mark scroll view
        [buttonScroll setContentSize:CGSizeMake(xVal,0)];
    }
    
    else if (buttonArray.count == 2)
    {
        float width_For_Two_Cat = (SCREEN_WIDTH-VARIABLE_WIDTH(50))/2;
        for(int i=0;i<buttonArray.count;i++)
        {
            buttonList = [UIButton buttonWithType:UIButtonTypeCustom];
            buttonList.frame = CGRectMake(xVal, 0,width_For_Two_Cat, VARIABLE_HEIGHT(30));
            [buttonList setTitle:buttonArray[i] forState:UIControlStateNormal];
            buttonList.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 16.0f:12.0f)];
            
            buttonList.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonList.titleLabel.lineBreakMode=NSLineBreakByTruncatingTail;
            [buttonList setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            buttonList.tag = i;
            if( buttonList.tag == 0)
            {
                UIView * line= [[UIView alloc]initWithFrame:CGRectMake(xVal,VARIABLE_HEIGHT(28),width_For_Two_Cat,VARIABLE_HEIGHT(2))];
                line.backgroundColor = [UIColor blackColor];
                [buttonScroll addSubview:line];
                [buttonList setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            [buttonList addTarget:self action:@selector(bookMarkBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            [buttonScroll addSubview:buttonList];
            xVal = xVal +width_For_Two_Cat;
        }
        // COntent size for book mark scroll view
        [buttonScroll setContentSize:CGSizeMake(xVal,0)];
        
    }
    else if (buttonArray.count == 3)
    {
        float width_For_Three_Cat = (SCREEN_WIDTH-VARIABLE_WIDTH(50))/3;
        for(int i=0;i<buttonArray.count;i++)
        {
            buttonList = [UIButton buttonWithType:UIButtonTypeCustom];
            buttonList.frame = CGRectMake(xVal, 0,width_For_Three_Cat, VARIABLE_HEIGHT(30));
            [buttonList setTitle:buttonArray[i] forState:UIControlStateNormal];
            buttonList.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 16.0f:12.0f)];
            
            buttonList.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonList.titleLabel.lineBreakMode=NSLineBreakByTruncatingTail;
            [buttonList setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            buttonList.tag = i;
            if( buttonList.tag == 0)
            {
                UIView * line= [[UIView alloc]initWithFrame:CGRectMake(xVal,VARIABLE_HEIGHT(28),width_For_Three_Cat,VARIABLE_HEIGHT(2))];
                line.backgroundColor = [UIColor blackColor];
                [buttonScroll addSubview:line];
                [buttonList setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            [buttonList addTarget:self action:@selector(bookMarkBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            [buttonScroll addSubview:buttonList];
            xVal = xVal +width_For_Three_Cat;
        }
        // COntent size for book mark scroll view
        [buttonScroll setContentSize:CGSizeMake(xVal,0)];
        
    }
    
    
    
    else
    {
        for(int i=0;i<buttonArray.count;i++)
        {
            
            
            buttonList = [UIButton buttonWithType:UIButtonTypeCustom];
            buttonList.frame = CGRectMake(xVal, 0,width, VARIABLE_HEIGHT(30));
            NSLog(@" %f %f %f ",buttonList.frame.origin.x, buttonList.frame.origin.y + buttonList.frame.size.height , buttonList.frame.size.width);
            
            [buttonList setTitle:buttonArray[i] forState:UIControlStateNormal];
            buttonList.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 16.0f:12.0f)];
            
            
            buttonList.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonList.titleLabel.lineBreakMode=NSLineBreakByTruncatingTail;
            [buttonList setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            buttonList.tag = i;
            if( buttonList.tag == 0)
            {
                UIView * line= [[UIView alloc]initWithFrame:CGRectMake(xVal,VARIABLE_HEIGHT(28),width,VARIABLE_HEIGHT(2))];
                line.backgroundColor = [UIColor blackColor];
                [buttonScroll addSubview:line];
                [buttonList setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            [buttonList addTarget:self action:@selector(bookMarkBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            [buttonScroll addSubview:buttonList];
            xVal = xVal +width;
        }
        
        
        // COntent size for book mark scroll view
        [buttonScroll setContentSize:CGSizeMake(xVal,0)];
    }
    
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait;
}

-(void) request_newsListing
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    [parameters setValue:@"1" forKey:@"isBkMark"];
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    if(menulevel>1){
        
        NSString *linkId= [[HubCitiAppDelegate getLinkIdArray]objectAtIndex:menulevel-1];
        [parameters setValue:linkId forKey:@"linkId"];
        NSString *level = [defaults valueForKey:KEY_MENULEVEL];
        [parameters setValue:level forKey:@"level"];
    }
    else{
        
        [parameters setValue:@"0" forKey:@"linkId"];
    }
    
    
    DLog(@"parameter: %@",parameters);
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@band/getnewstopnavigationmenu",BASE_URL];
    NSLog(@"URL : %@",urlString);
    
    
    NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    [HubCitiAppDelegate removeActivityIndicator];
     NSLog(@"Json : %@",responseData);
    [self parse_bookmarkCategories:responseData];
}
-(void)popBackToPreviousPage
{
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
     for (UIViewController *aViewController in allViewControllers)
     {
         NSLog(@"view controller %@", aViewController);
     }
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    [[HubCitiAppDelegate getLinkIdArray]removeLastObject];
    NSString * levelString;
    if(menulevel > 1)
        levelString = [NSString stringWithFormat:@"%d", menulevel-1];
    else
        levelString = [NSString stringWithFormat:@"%d", menulevel];
        [defaults setValue:levelString forKey:KEY_MENULEVEL];

    [self.navigationController popViewControllerAnimated:NO];
}

-(void) parse_bookmarkCategories : (id) responseData
{
    if (responseData == nil)
        return;
    if ( obj_bkMarkListResponse == nil)
    {
        obj_bkMarkListResponse = [[BookMarkGetRequest alloc]init];
    }
    
    [defaults setValue:[responseData objectForKey:@"bannerImg"] forKey:@"bannerImgScroll"];
    [defaults setValue:[responseData objectForKey:@"weatherURL"] forKey:@"weatherURL"];
    //[defaults setValue:[responseData objectForKey:@"bkImgPath"] forKey:@"bkImgPath"];
    //[defaults setValue:[responseData objectForKey:@"titleBkGrdColor"] forKey:@"titleBkGrdColor"];
    [defaults setValue:[responseData objectForKey:@"homeImgPath"] forKey:@"homeImgPath"];
    //[defaults setValue:[responseData objectForKey:@"hamburgerImg"] forKey:@"hamburgerImg"];
    
    [obj_bkMarkListResponse setValuesForKeysWithDictionary:responseData];
    NSLog(@"response = %@", obj_bkMarkListResponse);
    if ([obj_bkMarkListResponse.responseCode isEqualToString:@"10000"] )
    {
        
        bookMarkArr = [[NSMutableArray alloc]init];
        buttonArray = [[NSMutableArray alloc]init];
        if (obj_bkMarkListResponse.bookMarkList != nil)
        {
            for (int i=0; i< obj_bkMarkListResponse.bookMarkList.count; i++)
            {
                BookMarkListDetail * bookMarkListDetails = [[BookMarkListDetail alloc]init];
                NSDictionary * dict_bookMarkListDetails = obj_bkMarkListResponse.bookMarkList[i];
                [bookMarkListDetails setValuesForKeysWithDictionary:dict_bookMarkListDetails];
                if (bookMarkListDetails.listCatDetails !=nil)
                {
                    for(int j=0;j< bookMarkListDetails.listCatDetails.count; j++)
                    {
                        BookMarkListcatDetails * obj_catDetailsList = [[BookMarkListcatDetails alloc]init];
                        NSDictionary * dict_catListDetails = bookMarkListDetails.listCatDetails[j];
                        [obj_catDetailsList setValuesForKeysWithDictionary:dict_catListDetails];
                        [buttonArray addObject:obj_catDetailsList.parCatName];
                        [bookMarkArr addObject:obj_catDetailsList.nonfeedlink.length ? obj_catDetailsList.nonfeedlink : @"N/A"];
                        [backButtonColorArr addObject:obj_catDetailsList.backButtonColor.length ? obj_catDetailsList.backButtonColor : @"N/A"];
                    }
                }
                
                
            }
        }
    }
    else
    {
        [UtilityManager showAlert:nil msg:obj_bkMarkListResponse.responseText];
    
    }
    
}


- (void)leftSideMenuButtonPressed:(id)sender
{
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
     
    }];
    
}



-(void) rightSearchBtnClicked : (id) sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNotification" object:self];
    SearchBtnViewController *searchData = [[SearchBtnViewController alloc]initWithNibName:@"SearchBtnViewController" bundle:nil];
    searchData.data_Before_Search = buttonArray;
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [self.navigationController pushViewController:searchData animated:NO];
 
    
}

-(void) leftSearchBtnClicked : (id) sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNotification" object:self];
    SearchBtnViewController *searchData = [[SearchBtnViewController alloc]initWithNibName:@"SearchBtnViewController" bundle:nil];
    searchData.data_Before_Search = buttonArray;
    [self.navigationController pushViewController:searchData animated:NO];
    
}

- (NewsContainerViewController *)viewControllerAtIndex:(NSUInteger)index
{
   
    contentViewController = [[NewsContainerViewController alloc] initWithNibName:@"NewsContainerViewController" bundle:nil];
    bookMarkselected = 0;
    contentViewController.index = index;
    if (buttonArray.count > 0) {
        contentViewController.btnName = buttonArray[index];
        contentViewController.nonFeedLink = bookMarkArr[index];
        contentViewController.backButtonColor = backButtonColorArr[index];
         [defaults setValue:contentViewController.backButtonColor forKey:@"backbuttonColor"];
        
    }
    
    
    contentViewController.receivedResponseText = responseTextArray;
    contentViewController.bookMarkList = buttonArray;
    contentViewController.refreshOrButtonAction = setBoolForRefreshAndBtn;
    setBoolForRefreshAndBtn = NO;
    NSLog(@"Number of buttons %lu", (unsigned long)buttonArray.count);
    contentViewController.numberOfBookMarks = buttonArray.count;
    [self.pageViewController addChildViewController:contentViewController];
    [contentViewController didMoveToParentViewController:self.pageViewController];
    contentViewController.view.frame = CGRectMake(0, VARIABLE_HEIGHT(28), SCREEN_WIDTH, SCREEN_HEIGHT);
    contentViewController.view.backgroundColor = [UIColor whiteColor];
    
    return contentViewController;
    
}


-(void) bookMarkBtnClicked : (id) sender
{
    
    if ([sender isMemberOfClass:[UIButton class]])
    {
        setBoolForRefreshAndBtn = YES;
        UIButton *btn = (UIButton *)sender;
        [self setColor : btn.tag : btn];
        for(int i=0;i<buttonArray.count;i++)
        {
            if([btn.currentTitle isEqualToString:buttonArray[i]])
            {
                [defaults setInteger:i forKey:@"currentIndex"];
                UIViewController *initialViewController;
                
                //                if( NSOrderedSame==[buttonArray[i] caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [buttonArray[i] caseInsensitiveCompare:@"Videos"]) {
                //                    initialViewController = (PhotoView*)[self addPhotoViewController:i];
                //                }
                //                else
                {
                    initialViewController =  (NewsContainerViewController*)[self viewControllerAtIndex:i];
                }
                NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
                [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
                //                [self addChildViewController:self.pageViewController];
                //                pageViewController.view.frame =CGRectMake(0, VARIABLE_HEIGHT(30), SCREEN_WIDTH, SCREEN_HEIGHT);
                //                [[self view] addSubview:[self.pageViewController view]];
                //                [self.pageViewController didMoveToParentViewController:self];
            }
        }
    }
}

//-(void) setColor : (NSInteger) tag : (id) btn
//{
//    for (id subview in [buttonScroll subviews])
//    {
//        if ([subview isMemberOfClass:[UIButton class]])
//        {
//            UIButton *prevButton= (UIButton*) subview;
//            if(prevButton.tag == tag)
//            {
//                UIButton * currentBtn = prevButton;
//                [currentBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//                UIView * line= [[UIView alloc]initWithFrame:CGRectMake(currentBtn.frame.origin.x ,VARIABLE_HEIGHT(28),currentBtn.frame.size.width,VARIABLE_HEIGHT(2))];
//                line.backgroundColor = [UIColor blackColor];
//                [buttonScroll addSubview:line];
//
//            }
//            else
//            {
//                UIView * line= [[UIView alloc]initWithFrame:CGRectMake(prevButton.frame.origin.x ,VARIABLE_HEIGHT(28),prevButton.frame.size.width,VARIABLE_HEIGHT(2))];
//                line.backgroundColor = [UIColor convertToHexString:@"#f1f1f1"];
//                [buttonScroll addSubview:line];
//                [prevButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//            }
//        }
//
//    }
//
//
//
//  // float width=(SCREEN_WIDTH-VARIABLE_WIDTH(50))/4;
////    UIButton *prevBtn;
////    UIButton *nextBtn;
////    UIButton * preprevBtn;
////    UIButton *currentBtn;
////   // int value=0;
////    for (id subview in [buttonScroll subviews])
////    {
////
////        if ([subview isMemberOfClass:[UIButton class]])
////        {
////
////
////           currentBtn = (UIButton*) subview;
////
////            if(currentBtn.tag == tag-1)
////            {
////                preprevBtn = currentBtn;
////            }
////           else if(currentBtn.tag == tag)
////            {
////               prevBtn = currentBtn;
////            }
////          else  if(currentBtn.tag  == prevBtn.tag+1)
////            {
////                nextBtn = currentBtn;
////            }
////        }
////    }
////
////
////    if(buttonArray.count -1 == tag){
////
////        CGFloat mainTitleSize = preprevBtn.frame.origin.x+preprevBtn.frame.size.width;
////        int start = mainTitleSize;
////        //newOffset = start;
////        [buttonScroll setContentOffset:CGPointMake(start, 0.0) animated:YES];
////
////    }
////    else
////    {
////
////            NSLog(@"prevbutton %f scroll %f",prevBtn.frame.size.width+prevBtn.frame.origin.x , buttonScroll.frame.size.width+ buttonScroll.frame.origin.x );
////
////
////            if (prevBtn.frame.size.width+prevBtn.frame.origin.x <= buttonScroll.contentSize.width )
////            {
////                if ( nextBtn.frame.origin.x<=buttonScroll.contentSize.width && nextBtn.frame.origin.x+nextBtn.frame.size.width >buttonScroll.frame.size.width+ buttonScroll.frame.origin.x  )
////                {
////                    CGFloat mainTitleSize = preprevBtn.frame.origin.x+preprevBtn.frame.size.width;
////                    int start = mainTitleSize;
////                    NSLog(@"mainTitleSize:%f",mainTitleSize);
////                   // newOffset = start;
////                    [buttonScroll setContentOffset:CGPointMake(start, 0.0) animated:YES];
////
////                }
////            }
////            else
////            {
////                [buttonScroll setContentOffset:CGPointMake(0.0,0.0) animated:YES];
////                newOffset=0;
////            }
////
////
//////    if (tag >=2)
//////    {
//////        int start  = (tag - 2)*width;
//////        newOffset = start;
//////
//////        if (tag < buttonArray.count-1 )
//////        {
//////            [buttonScroll setContentOffset:CGPointMake(start, 0.0) animated:YES];
//////        }
//////        else if(bookMarkselected && tag==buttonArray.count-1){
//////            [buttonScroll setContentOffset:CGPointMake((buttonArray.count-4) * width, 0.0) animated:YES];
//////        }
//////
//////
//////    }
////
////    }
////
//
//    float width=(SCREEN_WIDTH-VARIABLE_WIDTH(125))/3;
//    if (tag >=2)
//    {
//        NSString * oneWidth = widthArray[tag-2];
//        int start = [oneWidth intValue];
//        newOffset = start;
//
//        if (tag < buttonArray.count-1 )
//        {
//            [buttonScroll setContentOffset:CGPointMake(start, 0.0) animated:YES];
//        }
//        else if(bookMarkselected && tag==buttonArray.count-1){
//            [buttonScroll setContentOffset:CGPointMake((buttonArray.count-3) * width, 0.0) animated:YES];
//        }
//
//
//    }
//    else
//    {
//        [buttonScroll setContentOffset:CGPointMake(0.0,0.0) animated:YES];
//        newOffset=0;
//    }
//
//
//
//}
-(void) setColor : (NSInteger) tag : (id) btn
{
    for (id subview in [buttonScroll subviews])
    {
        if ([subview isMemberOfClass:[UIButton class]])
        {
            UIButton *prevButton= (UIButton*) subview;
            if(prevButton.tag == tag)
            {
                UIButton * currentBtn = prevButton;
                [currentBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                UIView * line= [[UIView alloc]initWithFrame:CGRectMake(currentBtn.frame.origin.x ,VARIABLE_HEIGHT(28),currentBtn.frame.size.width,VARIABLE_HEIGHT(2))];
                line.backgroundColor = [UIColor blackColor];
                [buttonScroll addSubview:line];
                
            }
            else
            {
                UIView * line= [[UIView alloc]initWithFrame:CGRectMake(prevButton.frame.origin.x ,VARIABLE_HEIGHT(28),prevButton.frame.size.width,VARIABLE_HEIGHT(2))];
                line.backgroundColor = [UIColor convertToHexString:@"#f1f1f1"];
                [buttonScroll addSubview:line];
                [prevButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
        }
        
    }
    
    float width;
    int value;
    if(IPAD)
    {
        width =(SCREEN_WIDTH-VARIABLE_WIDTH(50))/4;
        value = 3;
        
        
    }
    else
    {
        width=(SCREEN_WIDTH-VARIABLE_WIDTH(50))/3;
        value=2;
        
    }
    if (tag >=value)
    {
        int start  = (tag - (value-1))*width;
        newOffset = start;
        
        if (tag < buttonArray.count-1 )
        {
            [buttonScroll setContentOffset:CGPointMake(start, 0.0) animated:YES];
        }
        else if(bookMarkselected && tag==buttonArray.count-1){
            [buttonScroll setContentOffset:CGPointMake((buttonArray.count-(value+1)) * width, 0.0) animated:YES];
        }
        else if([defaults boolForKey:@"isComingFromDetail"] && tag==buttonArray.count-1){
            [buttonScroll setContentOffset:CGPointMake((buttonArray.count-(value+1)) * width, 0.0) animated:YES];
        }
        
        
    }
    else
    {
        [buttonScroll setContentOffset:CGPointMake(0.0,0.0) animated:YES];
        newOffset=0;
    }
    
    
    
}
-(CGSize)calculateLabelWidth:(NSString*) labelString withWidth:(CGFloat) labelwidth
{
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:(IPAD ? 20.0f:12.0f)]};
    
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, VARIABLE_HEIGHT(20))
                                            options:NSStringDrawingUsesDeviceMetrics
                                         attributes:attributes
                                            context:nil];
    return rect.size;
}
#pragma delegate for UIScrollView
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //  newOffset=scrollView.contentOffset.x;
    
    newOffset=scrollView.contentOffset.x ;
    
}

# pragma Delegates and data source for UIPageViewController

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    
    NSUInteger index;
    NSLog(@"Parent %@",viewController);
    //    if ( [viewController isKindOfClass:[PhotoView class]])
    //    {
    //        index = ((PhotoView*) viewController).index;
    //    }
    //    else
    {
        index = ((NewsContainerViewController*) viewController).index;
    }
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    //    if( NSOrderedSame==[buttonArray[index] caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [buttonArray[index] caseInsensitiveCompare:@"Videos"])
    //    {
    //        return [self addPhotoViewController:index];
    //    }
    //    else
    {
        return  [self viewControllerAtIndex:index];
    }
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index;
    NSLog(@"Parent %@",viewController);
    //    if ( [viewController isKindOfClass:[PhotoView class]])
    //    {
    //   index = ((PhotoView*) viewController).index;
    //    }
    //    else
    {
        index = ((NewsContainerViewController*) viewController).index;
    }
    
    
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == buttonArray.count) {
        return nil;
    }
    //    if( NSOrderedSame==[buttonArray[index] caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [buttonArray[index] caseInsensitiveCompare:@"Videos"])
    //    {
    //        return [self addPhotoViewController:index];
    //    }
    //    else
    {
        return  [self viewControllerAtIndex:index];
    }
    
}

- (void)pageViewController:(UIPageViewController *)pageViewControllerDelegate didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed
{
    [HubCitiAppDelegate removeActivityIndicator];
    NSInteger   currentIndex;
    NSInteger  previousIndex;
    
    if ( [pageViewControllerDelegate isKindOfClass:[PhotoView class]])
    {
        currentIndex = [(PhotoView*)[pageViewControllerDelegate.viewControllers firstObject]index];
        previousIndex= [(PhotoView*)[previousViewControllers firstObject] index];
    }
    else
    {
        currentIndex = [(NewsContainerViewController*)[pageViewControllerDelegate.viewControllers firstObject] index];
        previousIndex= [(NewsContainerViewController*)[previousViewControllers firstObject] index];
    }
    NSLog(@"Current is %lu  previous =  %lu", (unsigned long)currentIndex,(unsigned long)previousIndex);
    [self setColor:currentIndex :nil];
    
    [defaults setInteger:currentIndex forKey:@"currentIndex"];
    NSLog(@"%d firstpop ",[defaults  boolForKey:@"firstPopUp"]);
    
    
    
    if ( [responseTextArray[currentIndex] length] == 0)
    {
        [defaults setBool:YES forKey:@"NoPopUp"];
    }
    if ([responseTextArray[currentIndex] length] != 0 && ![responseTextArray[currentIndex] isEqualToString:@"1" ])
    {
        [UtilityManager showAlert:nil msg:responseTextArray[currentIndex]];
        
    }
    
    NSLog(@" response text when succeed %@", responseTextArray[currentIndex]);
    
}



- (void)viewDidLayoutSubviews {
    // self.scrollView.contentOffset = self.contentOffset;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
//- (void)showResetPasswordAlert:(NSNotification *)notification {
//     [[CommonUtilityNews utilityManagers] tempPasswordDisplay:notification onViewController:self];
//
//}
//- (void)showEmailCountAlert:(NSNotification *)notification
//{
//     [[CommonUtilityNews utilityManagers] emailCountDisplay:notification onViewController:self];
//
//}
#pragma mark functions for popup for the first time
-(void)checkLocationStatus{
    
    
    
    if([defaults integerForKey:@"AppCounter"] == 1)
        
    {
        
        // If Devive Level GPS and App Level GPS both ON
        
        if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [[defaults valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
            
        {
            
//            [SharedManager setGps_allow_flag:YES];
//            
//            [defaults setValue:@"YES" forKey:@"allowingGPS"];
            
            [Location updateLocationinfo:self];
            
        }
        
        // IF Device Level GPS ON but Application Level GPS OFF
        
        else if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [[defaults valueForKey:@"allowingGPS"] isEqualToString:@"NO"])
            
        {
            if(!fromSideMenu)
            {
            [self gpsAllowDontAllowPopup];
            }
            
            
        }
        
        // IF Application level GPS off
        
        else if ([[defaults valueForKey:@"allowingGPS"] isEqualToString:@"NO"]) {
            
            
            if(!fromSideMenu) {
            [self gpsAllowDontAllowPopup];
            }
        }
        
    }
    
    
    
    else{
        
        
        
        [defaults setInteger:1 forKey:@"AppCounter"];
        
        if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [CLLocationManager locationServicesEnabled])
            
        {
            
//            [SharedManager setGps_allow_flag:YES];
//            
//            [defaults setValue:@"YES" forKey:@"allowingGPS"];
            
            [Location updateLocationinfo:self];
            
        }
        
        else {
            
//            [SharedManager setGps_allow_flag:NO];
//            
//            [defaults setValue:@"NO" forKey:@"allowingGPS"];
            
            [self checkForZipCode];
            
        }
        
    }
    
}

-(void)checkForRadius

{
    
    NSString *reqStr = [NSString stringWithFormat:@"<AuthenticateUser><deviceID>%@</deviceID><userId>%@</userId><hubCitiId>%@</hubCitiId></AuthenticateUser>",[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusersettings",BASE_URL];
   
    
    
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:reqStr];
    
    
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    
    TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
    
    if (responseCodeElement != nil && ![[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        
        
        return;
        
    }
    
    
    
    TBXMLElement *localeRadiusElement = [TBXML childElementNamed:@"localeRadius" parentElement:tbxml.rootXMLElement];
    TBXMLElement *bLocServiceElement = [TBXML childElementNamed:@"bLocService" parentElement:tbxml.rootXMLElement];
    if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"false"])
    {
        [SharedManager setGps_allow_flag:NO];
        [defaults setValue:@"NO" forKey:@"allowingGPS"];
    }
    else if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"true"])
    {
        [SharedManager setGps_allow_flag:YES];
        [defaults setValue:@"YES" forKey:@"allowingGPS"];
    }

    
    
    NSString *userRadius = [TBXML textForElement:localeRadiusElement];
    
    if(userRadius.length > 0)
        
    {
        
        [defaults setValue:userRadius forKey:@"FindRadius"];
        
        NSLog(@"my radius is %@",[defaults valueForKey:@"FindRadius"]);
        
    }
}
-(void) gpsAllowDontAllowPopup{
    
    
    UIAlertController * alert;
    alert=[UIAlertController alertControllerWithTitle:nil message:@"Application uses your current location to provide information about retailers and products near you. Do you wish to allow to access your location?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* allow = [UIAlertAction
                            actionWithTitle:@"Allow"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action){
                                //Ajit's edit
                               // [SharedManager setGps_allow_flag:YES];
                                [NewsUtility setGPSAsAllowForUser];
                                
                                //If LS Allowed, but LS are off
                                if ([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) {
                                    [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
                                    
                                }
                                [Location updateLocationinfo:self];
                                //[defaults setValue:@"YES" forKey:@"allowingGPS"];
                                
                            }];
    [alert addAction:allow];
    
    UIAlertAction* dontAllow = [UIAlertAction
                                actionWithTitle:@"Don't Allow"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action){
                                    //[SharedManager setGps_allow_flag:NO];
                                    [defaults setValue:@"" forKey:KEY_LATITUDE];//reset the values
                                    [defaults setValue:@"" forKey:KEY_LONGITUDE];
                                    [self checkForZipCode];
                                    //[defaults setValue:@"NO" forKey:@"allowingGPS"];
                                    [alert dismissViewControllerAnimated:NO completion:nil];
                                }];
    [alert addAction:dontAllow];
    
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
    
}

//zip code checking
-(void)checkForZipCode

{
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/fetchuserlocationpoints?userId=%@",BASE_URL,[defaults objectForKey:KEY_USERID]];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
    
    
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml] ;
    
    TBXMLElement *postalCodeElement = [TBXML childElementNamed:@"postalCode" parentElement:tbxml.rootXMLElement];
    
    
    
    if (postalCodeElement == nil) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
        
        int responseCode = [[TBXML textForElement:[TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement]] intValue];
        
        if (responseCode == 10005) {
            
            //If The User Doesnt Have a Zip on file, Ask them if they want to enter one
            
            UIAlertController * alert; //enter a zipcode alert
            alert=[UIAlertController alertControllerWithTitle:nil message:@"Would you like to enter a zipcode for us to use instead?" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* no = [UIAlertAction
                                 actionWithTitle:@"No"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     //Warn them that they are completely off the grid as far as ScanSee is concerned
                                     [defaults setValue:nil forKey:KEYZIPCODE];
                                     [UtilityManager showAlert:nil msg:@"You have chosen not to search by your Zipcode. If you would like to search by Zipcode in the future, please go to the User Information screen."];
                                 }];
            [alert addAction:no];
            UIAlertAction* yes = [UIAlertAction
                                  actionWithTitle:@"Yes"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action){
                                      [self showZipEnter];
                                  }];
            [alert addAction:yes];
            
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
            
            
            return;
            
            
            
            
            
            ReleaseAndNilify(responseXml);
            
            return;
            
        }
        
        else {
            
            // Banner the message given by the server
            
            [UtilityManager showAlert:@"Info" msg:responseTextStr];
            
            //[tbxml release];
            
            ReleaseAndNilify(responseXml);
            
            return;
            
        }
        
    }
    
    
    
    NSString *postalCodeStr = [TBXML textForElement:postalCodeElement];
    
    
    
    //Sets the ZipCode to the key for use in other view controllers
    
    [defaults setValue:postalCodeStr forKey:KEYZIPCODE];
    
    
    
    //set for Austin Experience req when clicked from main menu
    
    [defaults setObject:postalCodeStr forKey:@"postalCode"];
    
    
    
    //[tbxml release];
    
    ReleaseAndNilify(responseXml);
    
    //[self getMainMenuItems];
    
    return;
    
}



//Alert that shows a Zip Enter Box

-(void)showZipEnter
{
    
    // use UIAlertController
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Please enter your zip code"
                               message:nil
                               preferredStyle:UIAlertControllerStyleAlert];
    
    zipSave = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action){
                                         //Do Some action here
                                         UITextField *textField = alert.textFields[0];
                                         if ([textField.text length])
                                             [self postZip:textField.text];
                                         else
                                             return;
                                         
                                     }];
    zipSave.enabled = NO;
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       NSLog(@"cancel btn");
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       [defaults setValue:nil forKey:KEYZIPCODE];
                                                       
                                                       [UtilityManager showAlert:nil msg:@"You have chosen not to search by your Zipcode. If you would like to search by Zipcode in the future, please go to the User Information screen on the bottom toolbar of the homepage."];
                                                       
                                                       
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:zipSave];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        [textField setTag:31];
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.font = [UIFont systemFontOfSize:16];
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

//Posts Zip Code to User

-(void)postZip:(NSString *)zipCodeText

{
    
    DLog(@"Posting Zip");
    
    
    
    if([Network currentReachabilityStatus]==0)
        
    {
        
        [UtilityManager showAlert:@"Error!" msg:@"Network is not available"];
        
    }
    
    else {
        
        
        
        NSMutableString *urlString = [BASE_URL mutableCopy] ;
        
        [urlString appendFormat:@"thislocation/updateusrzipcode?userId=%@&zipcode=%@", [defaults valueForKey:KEY_USERID], zipCodeText];
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
        
        if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]) {
            
            ReleaseAndNilify(responseXml);
            
            return;
            
        }
        
        
        
        //NEED TO SET ERROR HANDLING OF RESPONSE. WILL BE ABLE TO WHEN CAN BE TESTED
        
        DLog(@"RESPONSE - %@",responseXml);
        
        [defaults setValue:zipCodeText forKey:KEYZIPCODE];
        
        // [self getMainMenuItems];
        
        
        
        // [responseXml release];
        
        
        
    }
    
}


@end

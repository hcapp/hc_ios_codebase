//
//  SearchBtnViewController.m
//  pageSwipe
//
//  Created by Nikitha on 5/19/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import "SearchBtnViewController.h"
#import "ScrollTemplateViewController.h"
#import "NewsContainerViewController.h"
#import "HubCitiConstants.h"
#import "AppDelegate.h"
#import "ScrollTemplateResponse.h"
#import "NewsDetailViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@interface SearchBtnViewController ()<HTTPClientDelegate>
{
    
    NSArray * data_After_Search;
    NewsContainerViewController * newsContentViewController;
    NSUInteger selectedIndex;
    UIActivityIndicatorView *loading;
    UIColor *oldColor;
    CGFloat bodyHeight ;
    CGFloat subtitltHeight ;
    BOOL scrollViewEnabled,fetchNext;
    UIView* containerView;
}
@property (nonatomic,strong) ScrollTemplateResponse * obj_searchNewsList;
@end

@implementation SearchBtnViewController
@synthesize searchBarOutlet,searchTableView,data_Before_Search,obj_searchNewsList,nextPage,totalNumOfCells,array_searchNewsList,responseText,titleFont,subTitleFont,bodyFont,searchBarText;

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    if (fetchNext) {
        [self hideFooter];
    }
    
    [HubCitiAppDelegate removeActivityIndicator];
    [self parse_searchNewsList:responseObject];
    [searchTableView reloadData];
    scrollViewEnabled = TRUE;
    
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    scrollViewEnabled = FALSE;
    for (NSOperation *operation in [HTTPClient sharedHTTPClient].manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        NSLog(@"operation:%@",operation);
        AFHTTPRequestOperation *operations = (AFHTTPRequestOperation*)operation;
        if([[[[operations request] URL] absoluteString] containsString:@"band/getnewsscroltemplate"]){
            [operation cancel];
        }
        
    }

}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    scrollViewEnabled = TRUE;
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error
{
    if (fetchNext) {
        [self hideFooter];
    }
    scrollViewEnabled = TRUE;
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    totalNumOfCells = [[NSNumber alloc]initWithInt:0];
    array_searchNewsList = [[NSMutableArray alloc]init];
    nextPage = [[NSNumber alloc]init];
    [defaults setBool:NO forKey:@"ViewMoreNews"];
    searchBarOutlet.delegate = self;
    
    // Font size for title, subtitle and body for news in each cell
    titleFont = [UIFont boldSystemFontOfSize:(IPAD ? 18.0f:14.0f)];
    subTitleFont = [UIFont systemFontOfSize:(IPAD ? 12.0f:10.0f)];
    bodyFont = [UIFont systemFontOfSize:(IPAD ? 16.0f:13.0f)];
    
    //customized back button
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    self.navigationItem.hidesBackButton = YES;
    
    self.navigationItem.title = @"Search";
    
    
    self.navigationItem.hidesBackButton = YES;
    
    //home button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
    
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;

    
    // self.navigationController.navigationBar.barTintColor = [UIColor blueColor];
    // Do any additional setup after loading the view from its nib.
}


-(void) popBackToPreviousPage{
    // [defaults setValue:@"mainmenu" forKey:@"mainmenu"];
    
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) request_searchedData
{
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    [parameters setValue:@"2" forKey:@"isSideBar"];
    [parameters setValue:searchBarText forKey:@"searchKey"];
    [parameters setValue:totalNumOfCells forKey:@"lowerLimit"];
    [parameters setValue:@"1" forKey:@"level"];
    
    //    if([defaults valueForKey:KEY_HUBCITIID])
    //    {
    //        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    //    }
    //    [parameters setValue:@"2" forKey:@"isSideBar"];
    //    [parameters setValue:searchBarText forKey:@"searchKey"];
    //    [parameters setValue:@"0" forKey:@"lowerLimit"];
    //      [parameters setValue:totalNumOfCells forKey:@"lowerLimit"];
    
    DLog(@"parameter: %@",parameters);
    NSString *urlString = [NSString stringWithFormat:@"%@band/getnewsscroltemplate",BASE_URL];
    NSLog(@"URL String %@", urlString);
    
    if ([nextPage intValue] == 1)
    {
        NSLog(@"Json = %@", parameters);
        HTTPClient* client = [HTTPClient sharedHTTPClient];
        client.delegate = self;
        NSLog(@"URL = %@", urlString);
        [client sendRequest:parameters : urlString];
        
    }
    else
    {
        [HubCitiAppDelegate showActivityIndicator];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        [manager.requestSerializer setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [manager POST:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            [HubCitiAppDelegate removeActivityIndicator];
            [self parse_searchNewsList:responseObject];
            [searchTableView reloadData];
            scrollViewEnabled = TRUE;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [HubCitiAppDelegate removeActivityIndicator];
            NSLog(@"Error: %@", error);
        }];
    }
    
}
-(void) hideFooter
{
    fetchNext = false;
    [loading stopAnimating];
    [containerView removeFromSuperview];
    scrollViewEnabled = FALSE;
    searchTableView.tableFooterView = nil;
    searchTableView.userInteractionEnabled = TRUE;
}
-(void)parse_searchNewsList : (id) response
{
    if (response == nil)
    {
        return;
    }
    if(obj_searchNewsList == nil)
    {
        obj_searchNewsList = [[ScrollTemplateResponse alloc]init];
    }
    [obj_searchNewsList setValuesForKeysWithDictionary:response];
    NSLog(@"Response = %@",obj_searchNewsList);
    if([obj_searchNewsList.responseCode isEqualToString:@"10000"])
    {
        if (obj_searchNewsList.nextPage)
        {
            nextPage = obj_searchNewsList.nextPage;
        }
        if(obj_searchNewsList.lowerLimitFlag)
        {
            totalNumOfCells = obj_searchNewsList.lowerLimitFlag;
        }
        if(obj_searchNewsList.bannerImg)
        {
            [defaults setValue:obj_searchNewsList.bannerImg forKey:@"bannerImg"];
        }
        if(obj_searchNewsList.items != nil)
        {
            for(int i=0;i<obj_searchNewsList.items.count;i++)
            {
                ScrollTemplateItems * searchNewsItems = [[ScrollTemplateItems alloc]init];
                NSDictionary * dict_searchNewsItems = obj_searchNewsList.items[i];
                [searchNewsItems setValuesForKeysWithDictionary:dict_searchNewsItems];
                [array_searchNewsList addObject:searchNewsItems];
            }
            
        }
        
    }
    else
    {
        if (timer){
            [timer invalidate];
            timer = nil;
        }
        [array_searchNewsList removeAllObjects];
        [searchTableView reloadData];
        scrollViewEnabled = TRUE;
        nextPage = 0;
        [UtilityManager showAlert:nil msg:obj_searchNewsList.responseText];
        
    }
    
}

-(void) setHeight : (NSUInteger ) indexPath
{
    subtitltHeight = 0;
    bodyHeight = 0;
    if ( [[array_searchNewsList objectAtIndex:indexPath] author] || [(ScrollTemplateItems*)[array_searchNewsList objectAtIndex:indexPath] date] || [(ScrollTemplateItems*)[array_searchNewsList objectAtIndex:indexPath]time])
    {
        subtitltHeight = subTitleFont.lineHeight;
    }
    if ( [[array_searchNewsList objectAtIndex:indexPath] sDesc])
    {
        bodyHeight = bodyFont.lineHeight;
    }
    
}

-(CGFloat) getLabelSize : (NSString*) labelString withSize:(CGFloat) labelwidth withFont:(UIFont *) font
{
    
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    float noOflines = ceil(rect.size.height / font.lineHeight);
    NSLog(@"no of line = %f %@", noOflines, labelString);
    
    return ceil(font.lineHeight*noOflines);
    
}

#pragma Delegates and data source for UITableView
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    if ([nextPage intValue] == 1)
    //    {
    //        return [array_searchNewsList count] + 1;
    //    }
    //    else
    //    {
    return [array_searchNewsList count];
    //}
    
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width ;
    CGFloat firstrow=0 ;
    
    float rowheight;
    //first row for all the category news should have big image
    //    if (indexPath.row != array_searchNewsList.count)
    //    {
    if(IPAD)
    {
        width  = SCREEN_WIDTH - (VARIABLE_HEIGHT(45) + VARIABLE_WIDTH(30));
    }
    else
        width = SCREEN_WIDTH - (VARIABLE_HEIGHT(60) + VARIABLE_WIDTH(30));
    [self setHeight : indexPath.row];
    
    
    rowheight = [self getLabelSize:[[array_searchNewsList objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont]+subtitltHeight+ (ceil)(2 *bodyHeight);
    
    
    if (IPAD)
    {
        if( VARIABLE_HEIGHT(45) > (rowheight+firstrow))
        {
            return VARIABLE_HEIGHT(65);
        }
        else
        {
            return rowheight+firstrow + VARIABLE_HEIGHT(20);
        }
        
    }
    else
    {
        if( VARIABLE_HEIGHT(60) > (rowheight+firstrow))
        {
            return VARIABLE_HEIGHT(80);
        }
        else
        {
            return rowheight+firstrow+VARIABLE_HEIGHT(20);
        }
    }
    
    
    
}
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    if([nextPage intValue]==1 && indexPath.row == [array_searchNewsList count] ) {
//        // This is the last cell
//        if (![defaults boolForKey:@"ViewMoreNews"] && totalNumOfCells !=0) {
//            [defaults setBool:YES forKey:@"ViewMoreNews"];
//
//            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
//            dispatch_sync(dispatchQueue, ^(void){
//                [self fetchNextBandEvents];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [searchTableView reloadData];
//
//                });
//            });
//
//        }
//    }
//
//}

-(void)fetchNextSearchNews
{
    scrollViewEnabled = FALSE;
    fetchNext = true;
    [HubCitiAppDelegate removeActivityIndicator];
    
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    loading.color = [UIColor blackColor];
    [loading startAnimating];
    CGFloat height = loading.frame.size.height + 6.f;
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, height)];
    containerView.backgroundColor = [UIColor clearColor];
    [containerView addSubview:loading];
    loading.center = CGPointMake(self.view.bounds.size.width / 2, height / 2.f);
    searchTableView.tableFooterView = containerView;
    searchTableView.userInteractionEnabled = false;
    [self request_searchedData];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (!scrollViewEnabled)
    {
        return;
    }
    CGSize contentSize = scrollView.contentSize;
    CGPoint contentOffset = scrollView.contentOffset;
    
    if (contentSize.height < SCREEN_HEIGHT && [nextPage intValue]  == 0) {
        return;
    }
    
    if ((contentOffset.y + scrollView.bounds.size.height > contentSize.height) && [nextPage intValue] == 1)
    {
        [self fetchNextSearchNews];
    }
}


- (NewsContainerViewController *)viewControllerAtIndex:(NSUInteger)index
{
    newsContentViewController = [[NewsContainerViewController alloc] initWithNibName:@"NewsContainerViewController" bundle:nil];
    newsContentViewController.index = selectedIndex;
    newsContentViewController.view.frame = CGRectMake(0, 114, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    newsContentViewController.view.backgroundColor = [UIColor whiteColor];
    return newsContentViewController;
}

#pragma Delegates for UISearchBar
-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    nextPage = 0;
    totalNumOfCells = [[NSNumber alloc]initWithInt:0];
    self.searchBarText = searchText;
    if (timer){
        [timer invalidate];
        timer = nil;
    }
    if(self.searchBarText.length == 0)
    {
        if (timer){
            [timer invalidate];
            timer = nil;
        }
        [array_searchNewsList removeAllObjects];
        [HubCitiAppDelegate removeActivityIndicator];
        [searchTableView reloadData];
        scrollViewEnabled = TRUE;
        
    }
    if(self.searchBarText.length >= 3)
    {
        
        if (![timer isValid]){
            array_searchNewsList = [[NSMutableArray alloc]init];
            timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(request_searchedData) userInfo:nil repeats:NO];
        }
    }
    
}
-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    nextPage = 0;
    totalNumOfCells = [[NSNumber alloc]initWithInt:0];
    if (timer){
        [timer invalidate];
        timer = nil;
    }
    array_searchNewsList = [[NSMutableArray alloc]init];
    [self request_searchedData];
    [searchBarOutlet resignFirstResponder];
    // [HubCitiAppDelegate removeActivityIndicator];
    //[searchTableView reloadData];
    
    
}
-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    nextPage = 0;
    totalNumOfCells = [[NSNumber alloc]initWithInt:0];
    if (timer){
        [timer invalidate];
        timer = nil;
    }
    [searchBarOutlet resignFirstResponder];
}
- (CGFloat)widthOfString:(NSString *)labelString withFont:(UIFont *)font
{
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:labelString attributes:attributes] size].width;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    NSArray *subviews=[cell.contentView subviews];
    
    [subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    
    
    CGFloat width;
    UILabel *title;
    UIImageView *bigimage;
    SdImageView * sdWebSmallImage;
    UIImageView *image;
    UILabel *subTitle;
    UILabel *body;
    float xPos;
    float yVal;
    
    UILabel * newsDate;
    UILabel * newsTime;
    //first cell for top stories with the bigger image
    
    if (indexPath.row != [array_searchNewsList count])
    {
        if(IPAD)
        {
            width = SCREEN_WIDTH - (VARIABLE_HEIGHT(45) + VARIABLE_WIDTH(30));
        }
        else
            width = SCREEN_WIDTH - (VARIABLE_HEIGHT(60) + VARIABLE_WIDTH(30));
        
        
//        if ([[[array_searchNewsList objectAtIndex:indexPath.row] imgPosition] isEqualToString:@"Right"])
//        {
//            if (IPAD)
//            {
//                image =[[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - (VARIABLE_HEIGHT(45) + VARIABLE_WIDTH(15)), VARIABLE_HEIGHT(10), VARIABLE_HEIGHT(45),VARIABLE_HEIGHT(45))];
//            }
//            else
//            {
//                image =[[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - (VARIABLE_HEIGHT(60) + VARIABLE_WIDTH(15)), VARIABLE_HEIGHT(10), VARIABLE_HEIGHT(60),VARIABLE_HEIGHT(60))];
//            }
//            
//            
//            xPos = VARIABLE_WIDTH(5);
//        }
    //else
        {
            if (IPAD)
            {
                image =[[UIImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(5), VARIABLE_HEIGHT(10), VARIABLE_HEIGHT(45),VARIABLE_HEIGHT(45))];
            }
            else
            {
                image =[[UIImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(5), VARIABLE_HEIGHT(10), VARIABLE_HEIGHT(60),VARIABLE_HEIGHT(60))];
            }
            image.contentMode = UIViewContentModeScaleAspectFit;
            xPos = image.frame.origin.x+ image.frame.size.width + VARIABLE_WIDTH(10);
        }
        [self setHeight:indexPath.row];
        NSString * truncatedAutor = [[array_searchNewsList objectAtIndex:indexPath.row]author];
        yVal = VARIABLE_HEIGHT(5);
        //title text in each cell
        NSLog(@"%f big image size",  bigimage.frame.origin.x+bigimage.frame.size.height);
        if ([[array_searchNewsList objectAtIndex:indexPath.row]title])
        {
            title= [[UILabel alloc] initWithFrame:CGRectMake(xPos, yVal , width , [self getLabelSize:[[array_searchNewsList objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont])];
            yVal = yVal + title.frame.size.height;
        }
        
        float authorXPos = xPos;
        if ([[array_searchNewsList objectAtIndex:indexPath.row]author])
        {
            if ([truncatedAutor length] >15)
            {
                truncatedAutor = [truncatedAutor substringToIndex:15];
                truncatedAutor = [truncatedAutor stringByAppendingString:@"..."];
            }
            else
            {
                truncatedAutor = truncatedAutor;
            }
            
            subTitle = [[UILabel alloc]initWithFrame:CGRectMake(authorXPos, yVal, [self widthOfString:truncatedAutor withFont:subTitleFont], subtitltHeight)];
            // yVal = yVal + subTitle.frame.origin.y+subTitle.frame.size.height;
            authorXPos = authorXPos + subTitle.frame.size.width + VARIABLE_WIDTH(5);
            
        }
        // subTitle.backgroundColor = [UIColor redColor];
        if ( [(ScrollTemplateItems*)[array_searchNewsList objectAtIndex:indexPath.row] date])
        {
            newsDate = [[UILabel alloc] initWithFrame:CGRectMake(authorXPos , yVal, [self widthOfString:[(ScrollTemplateItems*)[array_searchNewsList objectAtIndex:indexPath.row] date] withFont:subTitleFont], subtitltHeight)];
            // yVal = yVal +newsDate.frame.origin.y+newsDate.frame.size.height;
            authorXPos = authorXPos + newsDate.frame.size.width + VARIABLE_WIDTH(5);
        }
        if ( [(ScrollTemplateItems*)[array_searchNewsList objectAtIndex:indexPath.row] time])
        {
            newsTime = [[UILabel alloc] initWithFrame:CGRectMake(authorXPos , yVal, [self widthOfString:[(ScrollTemplateItems*)[array_searchNewsList objectAtIndex:indexPath.row] time] withFont:subTitleFont], subtitltHeight)];
            // yVal = yVal + newsTime.frame.origin.y+newsTime.frame.size.height;
            //authorXPos = authorXPos + newsTime.frame.size.width ;
            
        }
        
        
        if ([[array_searchNewsList objectAtIndex:indexPath.row]author])
        {
            yVal = yVal + subTitle.frame.size.height+ VARIABLE_HEIGHT(5);
        }
        else if ( [(ScrollTemplateItems*)[array_searchNewsList objectAtIndex:indexPath.row] date])
        {
            yVal = yVal +newsDate.frame.size.height+ VARIABLE_HEIGHT(5);
        }
        else  if ( [(ScrollTemplateItems*)[array_searchNewsList objectAtIndex:indexPath.row] time])
        {
            yVal = yVal + newsTime.frame.size.height+ VARIABLE_HEIGHT(5);
        }
        else
        {
            yVal = yVal + VARIABLE_HEIGHT(5);
        }
        //            newsDate.backgroundColor = [UIColor redColor];
        //            subTitle.backgroundColor = [UIColor redColor];
        //            newsTime.backgroundColor = [UIColor redColor];
        //Body text for each cell
        if ([[array_searchNewsList objectAtIndex:indexPath.row] sDesc])
        {
            body = [[UILabel alloc]initWithFrame:CGRectMake(xPos, yVal , width ,(ceil)(2 *bodyHeight))];
           // yVal = yVal + body.frame.size.height;
            
        }
        
        
        
        //title alignment
        title.numberOfLines = 0;
        title.textColor = [UIColor blackColor];
        title.font =titleFont;
        title.text = [[array_searchNewsList objectAtIndex:indexPath.row] title];
        [cell.contentView addSubview:title];
        
        
        //Smaller image at right corner for cells other than first cell
        
        sdWebSmallImage = [[SdImageView alloc]initWithFrame:image.frame] ;
        [sdWebSmallImage loadImage:[(ScrollTemplateItems*)([array_searchNewsList objectAtIndex:indexPath.row]) image]];
        [cell.contentView addSubview:sdWebSmallImage];
        
        //subtitle
        // subTitle.numberOfLines = 0;
        if([[array_searchNewsList objectAtIndex:indexPath.row] author])
        {
            subTitle.lineBreakMode =NSLineBreakByWordWrapping;
            subTitle.textColor = [UIColor grayColor];
            subTitle.font = subTitleFont;
            subTitle.lineBreakMode = NSLineBreakByTruncatingTail;
            subTitle.text =[[array_searchNewsList objectAtIndex:indexPath.row] author];
            [cell.contentView addSubview:subTitle];
        }
        
        if ([(ScrollTemplateItems*) [array_searchNewsList objectAtIndex:indexPath.row]date])
        {
            newsDate.textColor = [UIColor grayColor];
            newsDate.font = subTitleFont;
            newsDate.text = [(ScrollTemplateItems*) [array_searchNewsList objectAtIndex:indexPath.row]date];
            [cell.contentView addSubview:newsDate];
        }
        if ([(ScrollTemplateItems*) [array_searchNewsList objectAtIndex:indexPath.row]time])
        {
            newsTime.textColor = [UIColor grayColor];
            newsTime.font = subTitleFont;
            newsTime.text = [(ScrollTemplateItems*) [array_searchNewsList objectAtIndex:indexPath.row]time];
            [cell.contentView addSubview:newsTime];
        }
        
        //body
        
        if(indexPath.row != 0 )
        {
            body.numberOfLines = 2;
        }
        else{
            body.numberOfLines = 0;
            
        }
        
        body.lineBreakMode = NSLineBreakByTruncatingTail;
        body.textColor = [UIColor blackColor];
        body.font = bodyFont;
        body.text =[[array_searchNewsList objectAtIndex:indexPath.row] sDesc];
        [cell.contentView addSubview:body];
        
    }
    //    else if (indexPath.row == [array_searchNewsList count] && [nextPage intValue]== 1)
    //    {
    //
    //        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //        CGRect frame;
    //        if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
    //            frame.origin.x = 52;
    //            frame.origin.y = 12;
    //            frame.size.width = 250;
    //            frame.size.height = 24;
    //        }
    //        else{
    //            frame.origin.x = 70;
    //            frame.origin.y = 20;
    //            frame.size.width = 600;
    //            frame.size.height = 30;
    //        }
    //        loading.frame = frame;
    //
    //        //loading.center = cell.contentView.center;
    //        loading.color = [UIColor blackColor];
    //
    //
    //        [cell.contentView addSubview:loading];
    //        [loading startAnimating];
    //        cell.userInteractionEnabled = NO;
    //    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NewsDetailViewController *nd =[[NewsDetailViewController alloc] init];
    nd.newsID = [[array_searchNewsList objectAtIndex:indexPath.row] itemID];
    [self.navigationController pushViewController:nd animated:NO];
}

@end

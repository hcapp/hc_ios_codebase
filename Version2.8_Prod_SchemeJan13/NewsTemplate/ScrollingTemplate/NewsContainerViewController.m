//
//  NewsContainerViewController.m
//  pageSwipe
//
//  Created by Nikitha on 5/23/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import "NewsContainerViewController.h"
#import "ScrollTemplateViewController.h"
#import "HubCitiConstants.h"
#import "AppDelegate.h"
#import "ScrollTemplateResponse.h"
#import "NewsDetailViewController.h"
#import "SideNavigationResponse.h"
#import "CollectionViewCell.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "PhotoDetailViewController.h"
#import "PhotoFolderViewController.h"
#import "WebBrowserViewController.h"

@interface NewsContainerViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource, HTTPClientDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    SdImageView*  weatherImageView;
    CustomizedNavController *cusNav;
    UIView * contentView;
    UIActivityIndicatorView *loading;
    CGFloat bodyHeight ;
    CGFloat subtitltHeight ;
    BOOL scrollViewEnabled,fetchNext,viewmore;
    UIView* containerView;
    SdImageView *bannerSdImageView;
    NSMutableArray *imageArray,*videoArray, *generatedThumbnails;
    
    UIImageView *playImg;
    UIImage *dummyImg;
    NSUInteger thumbnailCount;
    NSMutableDictionary* imageDictionary,*photoDictionary;
    
    NSMutableURLRequest* _request;
    NSHTTPCookie *cookie;
}
@property (nonatomic,strong) ScrollTemplateResponse * obj_ScrollTempResponse;
@property(nonatomic,strong) MainMenuResponse  *menuResponse;

@end
@implementation NewsContainerViewController
@synthesize toolbar = mToolbar;
@synthesize back = mBack;
@synthesize forward = mForward;
@synthesize refresh = mRefresh;
@synthesize stop = mStop;
@synthesize activityIndicator;
@synthesize index,btnName,newsTableView,bodyFont,titleFont,subTitleFont,pullToRefresh;
@synthesize obj_ScrollTempResponse,array_ScrollNewsData, totalNumOfCells,nextPage, responseText;
@synthesize  receivedResponseText,refreshOrButtonAction,refreshMade,numberOfBookMarks,menuResponse,bookMarkList,scrollCollectionView,footer,toSendArray,bannerChanged,buttonRight,nonFeedLink;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    
    if (fetchNext) {
        [self hideFooter];
    }
    [self parse_ScrollNewsData:responseObject];
    if( NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"])
    {
        [scrollCollectionView reloadData];
    }
    else
    {
        [newsTableView reloadData];
    }
    scrollViewEnabled = TRUE;
    
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    scrollViewEnabled = FALSE;
    [weatherImageView removeFromSuperview];
    for (NSOperation *operation in [HTTPClient sharedHTTPClient].manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        NSLog(@"operation:%@",operation);
        AFHTTPRequestOperation *operations = (AFHTTPRequestOperation*)operation;
        if([[[[operations request] URL] absoluteString] containsString:@"band/getnewsscroltemplate"]){
            [operation cancel];
        }
        
    }
    
 //   [[HTTPClient sharedHTTPClient].manager.operationQueue cancelAllOperations];
    
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    if( NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"])
    {
        viewmore = false;
    }
    scrollViewEnabled = TRUE;
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error
{
    if (fetchNext) {
        [self hideFooter];
    }
    scrollViewEnabled = TRUE;
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}

-(void) refreshTableData
{
    
    nextPage = 0;
    totalNumOfCells = [[NSNumber alloc]initWithInt:0];
    scrollViewEnabled = FALSE;
    [HubCitiAppDelegate removeActivityIndicator];
    refreshOrButtonAction = YES;
    refreshMade = YES;
    [self request_ScrollNewsData];
}

-(void) getNotification
{
    if (refreshMade || fetchNext) {
        return;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    imageDictionary = [[NSMutableDictionary alloc] init];
    photoDictionary = [[NSMutableDictionary alloc] init];
    
    NSLog(@"Parent view controller %@",self.parentViewController.view);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getNotification)
                                                 name:@"sendNotification"
                                               object:nil];
    
    
    totalNumOfCells = [[NSNumber alloc]initWithInt:0];
    array_ScrollNewsData = [[NSMutableArray alloc]init];
    nextPage = [[NSNumber alloc]init];
    self.view.backgroundColor = [UIColor brownColor];
    NSLog(@" refresh clicked ? %d",refreshOrButtonAction);
   
    mToolbar.hidden = true;
    
    if (nonFeedLink.length > 0 && ![nonFeedLink isEqualToString:@"N/A"]) {
        // web view
        [defaults setBool:YES forKey:@"isCommingFromWebView"];
        [self setupWebView];
        [self setNavTitle];
        [self setWheatherIcon];
        
    }
    
    else if( NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"]) {
        
        // Collection View
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        scrollCollectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - VARIABLE_WIDTH(10), SCREEN_HEIGHT)collectionViewLayout:layout];
        //  scrollCollectionView = [[UICollectionView alloc]initWithFrame: CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        
        thumbnailCount = 0;
        videoArray = [[NSMutableArray alloc]init];
        playImg = [[UIImageView alloc]init];
        playImg.image = [UIImage imageNamed:@"play-icon-stroke.png"];
        
        imageArray = [[NSMutableArray alloc]init];
        generatedThumbnails = [[NSMutableArray alloc]init];
        dummyImg = [[UIImage alloc]init];
        dummyImg = [UIImage imageNamed:@"video-not-present.png"];
        
        scrollCollectionView.backgroundColor = [UIColor whiteColor];
        // Do any additional setup after loading the view from its nib.
        [self.scrollCollectionView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"collCell"];
        
        
        scrollCollectionView.delegate = self;
        scrollCollectionView.dataSource = self;
        [scrollCollectionView setBackgroundColor:[UIColor whiteColor]];
        scrollCollectionView.scrollEnabled =YES;
        scrollCollectionView.userInteractionEnabled = YES;
        [self.view addSubview:scrollCollectionView];
    }
    else
    {
        //Table View to display news
        newsTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        newsTableView.delegate = self;
        newsTableView.dataSource = self;
        [newsTableView setBackgroundColor:[UIColor whiteColor]];
        newsTableView.scrollEnabled =YES;
        newsTableView.userInteractionEnabled = YES;
        [self.view addSubview:newsTableView];
    }
    
    if(IPAD)
    {
        newsTableView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0);
        scrollCollectionView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0);
    }
    else
    {
        newsTableView.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);
        scrollCollectionView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0);
    }
    
    //pull to refresh
    if( NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"])
    {
        pullToRefresh = [[UIRefreshControl alloc] init];
        self.pullToRefresh.tintColor = [UIColor blackColor];
        [self.scrollCollectionView addSubview:pullToRefresh];
        [pullToRefresh addTarget:self action:@selector(refreshTableData) forControlEvents:UIControlEventValueChanged];
    }
    else
    {
        
        pullToRefresh = [[UIRefreshControl alloc] init];
        self.pullToRefresh.tintColor = [UIColor blackColor];
        [self.newsTableView addSubview:pullToRefresh];
        [pullToRefresh addTarget:self action:@selector(refreshTableData) forControlEvents:UIControlEventValueChanged];
    }
    
    
    // Font size for title, subtitle and body for news in each cell
    titleFont = [UIFont boldSystemFontOfSize:(IPAD ? 18.0f:14.0f)];
    subTitleFont = [UIFont systemFontOfSize:(IPAD ? 12.0f:10.0f)];
    bodyFont = [UIFont systemFontOfSize:(IPAD ? 16.0f:13.0f)];
    
    if(!(nonFeedLink.length > 0 && ![nonFeedLink isEqualToString:@"N/A"])) {
        [defaults setValue:nil forKey:@"bannerImgScroll"];
        [self request_ScrollNewsData];
    }
   
    
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void) viewWillDisappear:(BOOL)animated
//{
//
//}


-(void) request_ScrollNewsData
{
    
    [HubCitiAppDelegate removeActivityIndicator];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    [parameters setValue:btnName forKey:@"catName"];
    [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    [parameters setValue:totalNumOfCells forKey:@"lowerLimit"];
    NSString *menulevel = [defaults valueForKey:KEY_MENULEVEL];
    
    [parameters setValue:menulevel  forKey:@"level"];
    int level = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    if(level>1){
        
        NSString *linkId= [[HubCitiAppDelegate getLinkIdArray]objectAtIndex:level-1];
        [parameters setValue:linkId forKey:@"linkId"];
    }
    else{
        
        [parameters setValue:@"0" forKey:@"linkId"];
    }
    if( NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"])
    {
        [parameters setValue:@"0" forKey:@"isSideBar"];
    }
    if ([defaults objectForKey:@"scrollModifiedDate"]) {
        [parameters setValue:[defaults objectForKey:@"scrollModifiedDate"]  forKey:@"dateCreated"];
    }
    
    DLog(@"parameter: %@",parameters);
    NSString *urlString = [NSString stringWithFormat:@"%@band/getnewsscroltemplate",BASE_URL];
    NSLog(@"%@ super view", self.view.superview);
    
    if ([nextPage intValue] == 1)
    {
        for (UIScrollView *view in self.parentViewController.view.subviews) {
            
            if ([view isKindOfClass:[UIScrollView class]])
            {
                NSLog(@"Recognizer ");
                view.scrollEnabled = NO;
            }
        }
        
        NSLog(@"Json = %@", parameters);
        HTTPClient* client = [HTTPClient sharedHTTPClient];
        client.delegate = self;
        NSLog(@"URL = %@", urlString);
        [client sendRequest:parameters : urlString];
        
        
    }
    else
    {
        if( refreshMade == YES)
        {
            [HubCitiAppDelegate removeActivityIndicator];
        }
        else
        {
            [HubCitiAppDelegate showActivityIndicator];
        }
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        [manager.requestSerializer setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [manager POST:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            NSLog(@"template changed %@",[responseObject objectForKey:@"templateChanged"]);
            if (!([[responseObject objectForKey:@"newtempName"] isEqualToString:@"Scrolling News Template"] && [responseObject objectForKey:@"templateChanged"] == [NSNumber numberWithBool:1]))
            {
                if(! (NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"]))
                {
                    [HubCitiAppDelegate removeActivityIndicator];
                }
            }
            //            if( NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"])
            //            {
            //                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) ,^(void){
            //                    [self parse_ScrollNewsData:responseObject];
            //                });
            //            }
            //            else
            {
                [self parse_ScrollNewsData:responseObject];
            }
            
            if(!( NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"]))
            {
                [newsTableView reloadData];
            }
            scrollViewEnabled = TRUE;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [HubCitiAppDelegate removeActivityIndicator];
            if (refreshMade) {
                refreshMade = NO;
                [pullToRefresh endRefreshing];
            }
            NSLog(@"Error: %@", error);
        }];
    }
    
}

-(void) hideFooter
{
    fetchNext = false;
    [loading stopAnimating];
    [containerView removeFromSuperview];
    scrollViewEnabled = FALSE;
    newsTableView.tableFooterView = nil;
    newsTableView.userInteractionEnabled = TRUE;
    [footer removeFromSuperview];
    for (UIScrollView *view in self.parentViewController.view.subviews) {
        
        if ([view isKindOfClass:[UIScrollView class]])
        {
            NSLog(@"Recognizer ");
            view.scrollEnabled = YES;
        }
    }
}

-(void) parse_ScrollNewsData : (id) responseData
{
    if( refreshMade == YES)
    {
        refreshMade = NO;
        
        array_ScrollNewsData = [[NSMutableArray alloc]init];
        generatedThumbnails = [[NSMutableArray alloc]init];
        thumbnailCount = 0;
        
    }
    
    if (responseData == nil)
        return;
    if (obj_ScrollTempResponse == nil) {
        obj_ScrollTempResponse = [[ScrollTemplateResponse alloc] init];
    }
    [obj_ScrollTempResponse setValuesForKeysWithDictionary:responseData];
    if (refreshOrButtonAction == YES)
    {
        nextPage = 0;
        refreshOrButtonAction = NO;
        [pullToRefresh endRefreshing];
        if ( ![obj_ScrollTempResponse.responseCode isEqualToString:@"10000"])
        {
            if((NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"]))
            {
                [HubCitiAppDelegate removeActivityIndicator];
            }
            [UtilityManager showAlert:nil msg:obj_ScrollTempResponse.responseText];
            return;
            
        }
        
        
    }
    NSLog(@"response = %@", obj_ScrollTempResponse);
    // cusNav.customNavBardelegate = self;
   
    
    
    
    if ([obj_ScrollTempResponse.responseCode isEqualToString:@"10000"] )
    {
        
        if(obj_ScrollTempResponse.templateChanged)
        {
            if (obj_ScrollTempResponse.newtempName)
            {
                [defaults setInteger:0 forKey:@"currentIndex"];
                [defaults setValue:nil forKey:@"bannerImgScroll"];
                [defaults setInteger:0 forKey:@"currentIndexSubPage"];
                if([obj_ScrollTempResponse.newtempName isEqualToString:@"Combination News Template"])
                { [cusNav hideBackButton:YES];
                    [cusNav hideHambergerButton: YES];
                    [defaults setValue:nil forKey:@"scrollModifiedDate"];
                    [defaults setValue:@"Combination News Template" forKey:@"selectedTemplateName"];
                    [NewsUtility setNewsTemplateAsRoot];
                    return;
                }
                else if([obj_ScrollTempResponse.newtempName isEqualToString:@"News Tile Template"] )
                {
                    [cusNav hideBackButton:YES];
                    [cusNav hideHambergerButton: YES];
                    [defaults setValue:nil forKey:@"scrollModifiedDate"];
                    [defaults setValue:@"News Tile Template" forKey:@"selectedTemplateName"];
                    [NewsUtility setNewsTemplateAsRoot];
                    return;
                    
                }
                else if ([obj_ScrollTempResponse.newtempName isEqualToString:@"Scrolling News Template"])
                {
                    
                    [defaults setValue:@"Scrolling News Template" forKey:@"selectedTemplateName"];
                    //[self getSideNav];
                    [HubCitiAppDelegate removeActivityIndicator];
                    [self parseSuccessScroll : responseData];
                    return;
                    
                }
                else
                {
                    [cusNav hideBackButton:YES];
                    [cusNav hideHambergerButton: YES];
                    [defaults setValue:nil forKey:@"scrollModifiedDate"];
                    [defaults setValue:@"HubCiti" forKey:@"selectedTemplateName"];
                    [defaults setBool:NO forKey:@"newsTemplateExist"];
                    NSArray *keys = [cashedResponse allKeys];
                    for (int i = 0 ; i < [keys count]; i++)
                    {
                        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
                            [cashedResponse removeObjectForKey:keys[i]];
                        }
                        
                    }
                    
                    ReleaseAndNilify(selectedSortOption);
                    ReleaseAndNilify(selectedFilterOption);
                    ReleaseAndNilify(selectedSortCityDic);
                    ReleaseAndNilify(typeId);
                    ReleaseAndNilify(deptId);
                    
                    [NewsUtility setLoginVCAsRoot];
                    return;
                }
                
            }
        }
        [self parseSuccessScroll : responseData];
        
    }
    else
    {
        if(obj_ScrollTempResponse.hamburgerImg)
        {
            if(![[defaults valueForKey:@"hamburgerImg"] isEqualToString:obj_ScrollTempResponse.hamburgerImg])
            {
                [defaults setValue:obj_ScrollTempResponse.hamburgerImg forKey:@"hamburgerImg"];
                [cusNav changeHambugerImage];
            }

            
        }
        if((NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"]))
        {
            [HubCitiAppDelegate removeActivityIndicator];
        }
        NSLog(@"%@ before adding", receivedResponseText);
        nextPage = 0;
        NSLog(@"Integer of current index %ld",(long)[defaults integerForKey:@"currentIndex"]);
        if (receivedResponseText.count > 0) {
            
            [receivedResponseText removeObjectAtIndex:index];
            [receivedResponseText insertObject:obj_ScrollTempResponse.responseText atIndex:index];
        }
        
        NSLog(@"array of response text %@", receivedResponseText);
        ScrollTemplateViewController *responseViewController = [[ScrollTemplateViewController alloc]init];
        responseViewController.responseTextArray = receivedResponseText;
        
        NSLog(@"%d bool ",[defaults boolForKey:@"NoPopUp"]);
        NSLog(@"%ld current ",(long)[defaults integerForKey:@"currentIndex"]);
        NSLog(@" index %lu  current %ld  first pop up flag %d", (unsigned long)index,(long)[defaults integerForKey:@"currentIndex"],[defaults boolForKey:@"firstPopUp"]);
        
        if ([defaults boolForKey:@"NoPopUp"] && index == [defaults integerForKey:@"currentIndex"])
        {
            [defaults setBool:NO forKey:@"NoPopUp"];
            
            if (receivedResponseText.count > 0) {
                [UtilityManager showAlert:nil msg:receivedResponseText[index]];
            }
            else{
                [UtilityManager showAlert:nil msg:obj_ScrollTempResponse.responseText];
                }
            
        }
        
    }
    
    if( NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"])
    {
        
        viewmore = false;
    }
    
}

-(void) setNavTitle
{
    bannerSdImageView = [[SdImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(40), 0, 130, 35)];
    bannerSdImageView.backgroundColor = [UIColor clearColor];
    bannerSdImageView.tag = 1;
    //asyncImageView.layer.cornerRadius = 5.0f;
    bannerSdImageView.contentMode = UIViewContentModeScaleToFill;
    [bannerSdImageView loadImage: [defaults valueForKey:@"bannerImgScroll"]];
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(40), 0, 130, 35)];
    bannerSdImageView.frame = titleView.bounds;
    [titleView addSubview:bannerSdImageView];
    if ([defaults valueForKey:@"bannerImgScroll"] != nil)
    {
        [self.navigationController.navigationBar.topItem setTitleView:titleView];
    }

}
-(void) setWheatherIcon
{
    
    
    if (IPAD)
    {
        weatherImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, -2, VARIABLE_WIDTH(18), VARIABLE_HEIGHT(18))];
    }
    else
    {
        weatherImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, VARIABLE_WIDTH(20), VARIABLE_HEIGHT(20))];
    }
    weatherImageView.backgroundColor = [UIColor clearColor];
    weatherImageView.tag = 2;
    //asyncImageView.layer.cornerRadius = 5.0f;
    weatherImageView.contentMode = UIViewContentModeScaleAspectFit;
    [weatherImageView loadImagefrommainBundel: @"weathericon-w.png"];
    //        UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, VARIABLE_WIDTH(20), VARIABLE_HEIGHT(20))];
    //        weatherImageView.frame = titleView.bounds;
    UIButton *weatherBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    if(IPAD)
    {
        weatherBtn.frame = CGRectMake(0, -2, VARIABLE_WIDTH(18), VARIABLE_HEIGHT(18));
    }
    else
    {
        weatherBtn.frame = CGRectMake(0, 0, VARIABLE_WIDTH(20), VARIABLE_HEIGHT(20));
    }
    [weatherBtn addSubview:weatherImageView];
    [weatherBtn addTarget:self action:@selector(weatherButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    buttonRight= [[UIBarButtonItem alloc] initWithCustomView:weatherBtn];
    self.navigationController.navigationBar.topItem.rightBarButtonItem = buttonRight;
    

}
-(void) parseSuccessScroll : (id) response
{
    if (obj_ScrollTempResponse.bkImgPath)
    {
        if(![[defaults valueForKey:@"bkImgPath"] isEqualToString:obj_ScrollTempResponse.bkImgPath])
        {
            [cusNav changeBackImage:obj_ScrollTempResponse.bkImgPath];
        }
        [defaults setValue:obj_ScrollTempResponse.bkImgPath forKey:@"bkImgPath"];
    }
    if(obj_ScrollTempResponse.titleTxtColor)
    {
        [defaults setValue:obj_ScrollTempResponse.titleTxtColor forKey:@"titleTxtColor"];
    }
    if(obj_ScrollTempResponse.titleBkGrdColor)
    {
        [defaults setValue:self.obj_ScrollTempResponse.titleBkGrdColor forKey:@"titleBkGrdColor"];
    }
    if(obj_ScrollTempResponse.homeImgPath)
    {
        [defaults setValue:obj_ScrollTempResponse.homeImgPath forKey:@"homeImgPath"];
    }
    if(obj_ScrollTempResponse.hamburgerImg)
    {
         if(![[defaults valueForKey:@"hamburgerImg"] isEqualToString:obj_ScrollTempResponse.hamburgerImg])
         {
             [defaults setValue:obj_ScrollTempResponse.hamburgerImg forKey:@"hamburgerImg"];
             [cusNav changeHambugerImage];
         }
        
    }
    if(obj_ScrollTempResponse.nextPage)
    {
        nextPage = obj_ScrollTempResponse.nextPage;
    }
    if(obj_ScrollTempResponse.lowerLimitFlag)
    {
        totalNumOfCells = obj_ScrollTempResponse.lowerLimitFlag;
    }
    if(obj_ScrollTempResponse.weatherURL)
    {
        [defaults setValue:obj_ScrollTempResponse.weatherURL forKey:@"weatherURL"];
        
        [self setWheatherIcon];
        
        
        //Weather Icon
        //        UIButton *weatherBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //        weatherBtn.frame = CGRectMake(0, 0, VARIABLE_WIDTH(18), VARIABLE_HEIGHT(18));
        //        UIImageView * weatherImageView = [[UIImageView alloc]initWithFrame:weatherBtn.frame];
        //        weatherImageView.image = [UIImage imageNamed:@"events1"];
        //        [weatherBtn addSubview:weatherImageView];
        //        [weatherBtn addTarget:self action:@selector(weatherButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        //        buttonRight= [[UIBarButtonItem alloc] initWithCustomView:weatherBtn];
        //        weatherBtn.tag = 2;
        //        self.navigationController.navigationItem.rightBarButtonItem = buttonRight;
    }

    
    if( obj_ScrollTempResponse.bannerImg)
    {
        if ([defaults valueForKey:@"bannerImgScroll"])
        {
            NSLog(@"banner path : %@",[defaults valueForKey:@"bannerImgScroll"]);
            if (![[defaults valueForKey:@"bannerImgScroll"] isEqualToString:obj_ScrollTempResponse.bannerImg])
            {
                [defaults setValue:obj_ScrollTempResponse.bannerImg forKey:@"bannerImgScroll"];
                bannerChanged = 1;
                [self setNavTitle];
            }
        }
        else
        {
            bannerChanged = 1;
            [defaults setValue:obj_ScrollTempResponse.bannerImg forKey:@"bannerImgScroll"];
            [self setNavTitle];
        }
        
        if (bannerChanged == 1)
        {
            [self setNavTitle];
            bannerChanged = 0;
        }
        
       
        
    }
    if ( obj_ScrollTempResponse.items != nil)
    {
        if( NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"])
        {
            if(!viewmore)
            {
                array_ScrollNewsData = [[NSMutableArray alloc]init];
                generatedThumbnails = [[NSMutableArray alloc]init];
                thumbnailCount = 0;
            }
        }
        
        // array_ScrollNewsData = [[NSMutableArray alloc]init];
        for (int i=0;i< obj_ScrollTempResponse.items.count;i++)
        {
            ScrollTemplateItems * scrollItemResponseData = [[ScrollTemplateItems alloc]init];
            NSDictionary * dict_scrollItemData = obj_ScrollTempResponse.items[i];
            [scrollItemResponseData setValuesForKeysWithDictionary:dict_scrollItemData];
            [array_ScrollNewsData addObject:scrollItemResponseData];
            
        }
        
    }
    //Thumbnails generation
    /* if( NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"])
     {
     for(int i = (int)thumbnailCount ; i < array_ScrollNewsData.count; i++)
     {
     if([[array_ScrollNewsData objectAtIndex:i]videoLink])
     {
     imageArray = [[NSMutableArray alloc]initWithArray:[self convertToArrayFromCommaSeparated:[[array_ScrollNewsData objectAtIndex:i]videoLink]]];
     NSString *urlString = [imageArray objectAtIndex:0];
     
     NSURL *sourceURL = [NSURL URLWithString:urlString];
     if ([[UIDevice currentDevice].systemVersion intValue] >= 8)
     {
     
     AVAsset *asset = [AVAsset assetWithURL:sourceURL];
     AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
     CMTime time = CMTimeMake(3, 2);
     
     CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
     
     
     UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
     CGImageRelease(imageRef);
     if(thumbnail)
     {
     [generatedThumbnails addObject:thumbnail];
     }
     else
     {
     [generatedThumbnails addObject:dummyImg];
     }
     
     thumbnailCount++;
     
     
     
     }
     
     
     }
     else
     {
     [generatedThumbnails addObject:dummyImg];
     thumbnailCount++;
     }
     }
     
     dispatch_async(dispatch_get_main_queue(), ^ {
     [scrollCollectionView reloadData];
     scrollViewEnabled = TRUE;
     
     [HubCitiAppDelegate removeActivityIndicator];
     });
     }
     */
    
    [HubCitiAppDelegate removeActivityIndicator];
    scrollViewEnabled = TRUE;
    [scrollCollectionView reloadData];
    
    
    
    
    [defaults setValue:obj_ScrollTempResponse.modifiedDate forKey:@"scrollModifiedDate"];
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]],
      NSForegroundColorAttributeName,[UIFont fontWithName:@"Helvetica-Bold" size:17.0],
      NSFontAttributeName,nil]];
    [receivedResponseText removeObjectAtIndex:index];
    [receivedResponseText insertObject:@"1" atIndex:index];
    ScrollTemplateViewController *responseViewController = [[ScrollTemplateViewController alloc]init];
    responseViewController.responseTextArray = [receivedResponseText mutableCopy];
    NSLog(@"%@ yugdsufiashdf ",responseViewController.responseTextArray);
}

-(void)weatherButtonClicked : (id) sender
{
    NSString *url = [defaults  objectForKey:@"weatherURL"];
    [defaults  setObject:url forKey:KEY_URL];
    NSLog(@"Send response  %@ Default string %@",[defaults  objectForKey:@"weatherURL"],[defaults valueForKey:KEY_URL]);
    WebBrowserViewController *webView = [[WebBrowserViewController alloc]initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:webView animated:NO];
}



-(CGFloat) getLabelSize : (NSString*) labelString withSize:(CGFloat) labelwidth withFont:(UIFont *) font
{
    
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    float noOflines = ceil(rect.size.height / font.lineHeight);
    
    
    return ceil(font.lineHeight*noOflines);
    
}
#pragma Delegates and data source for UITableView
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    if ([nextPage intValue] == 1)
    //    {
    //        return [array_ScrollNewsData count] + 1;
    //    }
    //    else
    //    {
    return [array_ScrollNewsData count];
    //    }
    
}
-(void) setHeight : (NSUInteger ) indexPath
{
    subtitltHeight = 0;
    bodyHeight = 0;
    if ( [[array_ScrollNewsData objectAtIndex:indexPath] author] || [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath] date] || [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath]time])
    {
        subtitltHeight = subTitleFont.lineHeight;
    }
    
    if ( [[array_ScrollNewsData objectAtIndex:indexPath] sDesc])
    {
        bodyHeight = bodyFont.lineHeight;
    }
    
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width ;
    CGFloat firstrow=0 ;
    
    float rowheight;
    //first row for all the category news should have big image
    
    if(indexPath.row == 0){
        if(IPAD)
        {
            width = SCREEN_WIDTH - VARIABLE_WIDTH(20);
        }
        else
        {
            width = SCREEN_WIDTH - VARIABLE_WIDTH(20);
        }
        firstrow =VARIABLE_HEIGHT(180);
        
    }
    else
    {
        if(IPAD)
        {
            width  = SCREEN_WIDTH - (VARIABLE_HEIGHT(45) + VARIABLE_WIDTH(30));
        }
        else
            width = SCREEN_WIDTH - (VARIABLE_HEIGHT(60) + VARIABLE_WIDTH(30));
    }
    [self setHeight : indexPath.row];
    
    if(indexPath.row == 0)
    {
        CGRect rectFirtsTilte = [[[array_ScrollNewsData objectAtIndex:indexPath.row] sDesc] boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : bodyFont} context:nil];
        float noOflines = ceil(rectFirtsTilte.size.height / bodyFont.lineHeight);
        CGFloat heightOFFirstCell;
        if (noOflines > 4)
        {
            heightOFFirstCell = ceil(bodyFont.lineHeight*4);
        }
        else
        {
            heightOFFirstCell = ceil(bodyFont.lineHeight*noOflines);
        }
        
        
        
        rowheight = [self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont]+subtitltHeight+ heightOFFirstCell;
        
    }
    else
    {
        rowheight = [self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont]+subtitltHeight+ (ceil)(2 *bodyHeight);
    }
    
    if (IPAD)
    {
        if( VARIABLE_HEIGHT(45) > (rowheight+firstrow))
        {
            return VARIABLE_HEIGHT(65);
        }
        else
        {
            return rowheight+firstrow + VARIABLE_HEIGHT(20);
        }
        
    }
    else
    {
        
        if( VARIABLE_HEIGHT(60) > (rowheight+firstrow))
        {
            return VARIABLE_HEIGHT(80);
        }
        else
        {
            CGFloat height = rowheight+firstrow+VARIABLE_HEIGHT(20);
            return height;
        }
    }
    
    
    
    
    //    else
    //    {
    //        return VARIABLE_HEIGHT(50);
    //    }
}


//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    if([nextPage intValue]==1 && indexPath.row == [array_ScrollNewsData count] ) {
//        // This is the last cell
//        if (![defaults boolForKey:@"ViewMoreNews"] && totalNumOfCells !=0) {
//            [defaults setBool:YES forKey:@"ViewMoreNews"];
////            pullToRefresh = nil;
////            [pullToRefresh removeFromSuperview];
//            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
//            dispatch_sync(dispatchQueue, ^(void){
//                [self fetchNextNews];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [newsTableView reloadData];
////                    pullToRefresh = [[UIRefreshControl alloc] init];
////                    self.pullToRefresh.tintColor = [UIColor blackColor];
////                    [self.newsTableView addSubview:pullToRefresh];
////                    [pullToRefresh addTarget:self action:@selector(refreshTableData) forControlEvents:UIControlEventValueChanged];
////
//                });
//            });
//
//        }
//    }
//
//}

-(void)fetchNextNews
{
    if( NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [btnName caseInsensitiveCompare:@"Videos"])
    {
        viewmore = true;
        scrollViewEnabled = NO;
        fetchNext = true;
        [HubCitiAppDelegate removeActivityIndicator];
        
        loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        loading.color = [UIColor blackColor];
        
        
        [loading startAnimating];
        CGFloat height = loading.frame.size.height + 6.f;
        
        if(IPAD)
        {
            footer = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-height - VARIABLE_HEIGHT(40) - VARIABLE_HEIGHT(18) , self.view.bounds.size.width, height)];
        }
        else
        {
            footer = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-height - VARIABLE_HEIGHT(64)- VARIABLE_HEIGHT(18) , self.view.bounds.size.width, height)];
        }
        
        footer.backgroundColor = [UIColor whiteColor];
        [footer addSubview:loading];
        loading.center = CGPointMake(self.view.bounds.size.width / 2, height / 2.f);
        [self.view addSubview:footer];
        scrollCollectionView.userInteractionEnabled = false;
        [self request_ScrollNewsData];
        
    }
    else
    {
        scrollViewEnabled = FALSE;
        fetchNext = true;
        [HubCitiAppDelegate removeActivityIndicator];
        
        loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        loading.color = [UIColor blackColor];
        [loading startAnimating];
        CGFloat height = loading.frame.size.height + 6.f;
        containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, height)];
        containerView.backgroundColor = [UIColor clearColor];
        [containerView addSubview:loading];
        loading.center = CGPointMake(self.view.bounds.size.width / 2, height / 2.f);
        newsTableView.tableFooterView = containerView;
        newsTableView.userInteractionEnabled = false;
        [self request_ScrollNewsData];
    }
    
}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    
    
    if (!scrollViewEnabled)
    {
        return;
    }
    CGSize contentSize = scrollView.contentSize;
    CGPoint contentOffset = scrollView.contentOffset;
    
    if (contentSize.height < SCREEN_HEIGHT && [nextPage intValue]  == 0) {
        return;
    }
    
    if ((contentOffset.y + scrollView.bounds.size.height > contentSize.height) && [nextPage intValue] == 1)
    {
        [self fetchNextNews];
    }
    
}

- (CGFloat)widthOfString:(NSString *)labelString withFont:(UIFont *)font
{
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:labelString attributes:attributes] size].width;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    NSArray *subviews=[cell.contentView subviews];
    
    [subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    
    CGFloat width;
    UILabel *title;
    UIImageView *bigimage;
    SdImageView * sdwebBigImage;
    SdImageView * sdWebSmallImage;
    UIImageView *image;
    UILabel *subTitle;
    UILabel * newsDate;
    UILabel * newsTime;
    UILabel *body;
    float xPos;
    float yVal = 0 ;
    
    //first cell for top stories with the bigger image
    
    if (indexPath.row != [array_ScrollNewsData count])
    {
        if(indexPath.row == 0 )
        {
            if (IPAD)
            {
                width = SCREEN_WIDTH - VARIABLE_WIDTH(20);
            }
            else
                width = SCREEN_WIDTH - VARIABLE_WIDTH(20);
            bigimage=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - VARIABLE_WIDTH(10), VARIABLE_HEIGHT(180))];
            sdwebBigImage = [[SdImageView alloc]initWithFrame:bigimage.frame] ;
            sdwebBigImage.contentMode = UIViewContentModeScaleAspectFit;
            [sdwebBigImage loadImage:[(ScrollTemplateItems*)([array_ScrollNewsData objectAtIndex:indexPath.row]) image]];
            [bigimage addSubview:sdwebBigImage];
            [cell.contentView addSubview:bigimage];
            yVal = bigimage.frame.origin.y+ bigimage.frame.size.height +VARIABLE_HEIGHT(5);
        }
        else
        {
            if(IPAD)
            {
                width = SCREEN_WIDTH - (VARIABLE_HEIGHT(45) + VARIABLE_WIDTH(30));
            }
            else
                width = SCREEN_WIDTH - (VARIABLE_HEIGHT(60) + VARIABLE_WIDTH(30));
        }
        
        
        if ([[[array_ScrollNewsData objectAtIndex:indexPath.row] imgPosition] isEqualToString:@"Right"])
        {
            if (IPAD)
            {
                image =[[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - (VARIABLE_HEIGHT(45) + VARIABLE_WIDTH(15)), VARIABLE_HEIGHT(10), VARIABLE_HEIGHT(45),VARIABLE_HEIGHT(45))];
            }
            else
            {
                image =[[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - (VARIABLE_HEIGHT(60) + VARIABLE_WIDTH(15)), VARIABLE_HEIGHT(10), VARIABLE_HEIGHT(60),VARIABLE_HEIGHT(60))];
            }
            
            
            xPos = VARIABLE_WIDTH(5);
        }
        else
        {
            if (IPAD)
            {
                image =[[UIImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(5), VARIABLE_HEIGHT(10), VARIABLE_HEIGHT(45),VARIABLE_HEIGHT(45))];
            }
            else
            {
                image =[[UIImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(5), VARIABLE_HEIGHT(10), VARIABLE_HEIGHT(60),VARIABLE_HEIGHT(60))];
            }
            
            image.contentMode = UIViewContentModeScaleAspectFit;
            xPos = image.frame.origin.x+ image.frame.size.width + VARIABLE_WIDTH(10);
        }
        [self setHeight:indexPath.row];
        NSString * truncatedAutor = [[array_ScrollNewsData objectAtIndex:indexPath.row]author];
        if (indexPath.row == 0)
        {
            
            if ([[array_ScrollNewsData objectAtIndex:indexPath.row]title])
            {
                title= [[UILabel alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(5), yVal , width , [self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont])];
                yVal = yVal +title.frame.size.height;
            }
            float authorXPos=VARIABLE_WIDTH(5);
            if ([[array_ScrollNewsData objectAtIndex:indexPath.row]author])
            {
                if ([truncatedAutor length] >15)
                {
                    truncatedAutor = [truncatedAutor substringToIndex:15];
                    truncatedAutor = [truncatedAutor stringByAppendingString:@"..."];
                }
                else
                {
                    truncatedAutor = truncatedAutor;
                }
                
                subTitle = [[UILabel alloc]initWithFrame:CGRectMake(authorXPos, yVal,[self widthOfString:truncatedAutor withFont:subTitleFont], subtitltHeight)];
                
                authorXPos = authorXPos + subTitle.frame.size.width + VARIABLE_WIDTH(5) ;
            }
            
            
            
            if ( [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date])
            {
                newsDate = [[UILabel alloc] initWithFrame:CGRectMake(authorXPos, yVal, [self widthOfString:[(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date] withFont:subTitleFont], subtitltHeight)];
                NSLog(@"Scroll NEws height %f",newsDate.frame.size.height);
                authorXPos = authorXPos + newsDate.frame.size.width + VARIABLE_WIDTH(5) ;
            }
            if ( [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] time])
            {
                newsTime = [[UILabel alloc] initWithFrame:CGRectMake(authorXPos , yVal, [self widthOfString:[(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] time] withFont:subTitleFont], subtitltHeight)];
                
               // authorXPos = authorXPos + newsTime.frame.size.width ;
            }
            if ([[array_ScrollNewsData objectAtIndex:indexPath.row]author])
            {
                yVal = yVal +subTitle.frame.size.height;
            }
            else if ( [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date])
            {
                NSLog(@"News Height %f",newsDate.frame.size.height);
                yVal = yVal +newsDate.frame.size.height;
            }
            else  if ( [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] time])
            {
                yVal = yVal + newsTime.frame.size.height;
            }
            
            
            
            
            
            //            newsDate.backgroundColor = [UIColor redColor];
            //            newsTime.backgroundColor = [UIColor redColor];
            //            subTitle.backgroundColor = [UIColor redColor];
            //Body text for each cell
            if ([[array_ScrollNewsData objectAtIndex:indexPath.row] sDesc])
            {
                CGRect rectFirtsTilte = [[[array_ScrollNewsData objectAtIndex:indexPath.row] sDesc] boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : bodyFont} context:nil];
                float noOflines = ceil(rectFirtsTilte.size.height / bodyFont.lineHeight);
                CGFloat heightOFFirstCell;
                if (noOflines > 4)
                {
                    heightOFFirstCell = ceil(bodyFont.lineHeight*4);
                }
                else
                {
                    heightOFFirstCell = ceil(bodyFont.lineHeight*noOflines);
                }
                
                body = [[UILabel alloc]initWithFrame:CGRectMake(VARIABLE_WIDTH(5), yVal + VARIABLE_HEIGHT(5), width, heightOFFirstCell)];
              //  yVal = yVal + body.frame.size.height;
            }
        }
        else
        {
            yVal = VARIABLE_HEIGHT(5);
            //title text in each cell
            NSLog(@"%f big image size",  bigimage.frame.origin.x+bigimage.frame.size.height);
            if ([[array_ScrollNewsData objectAtIndex:indexPath.row]title])
            {
                title= [[UILabel alloc] initWithFrame:CGRectMake(xPos, yVal , width , [self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont])];
                yVal = yVal + title.frame.size.height;
            }
            float authorXPos = xPos;
            if ([[array_ScrollNewsData objectAtIndex:indexPath.row]author])
            {
                if ([truncatedAutor length] >15)
                {
                    truncatedAutor = [truncatedAutor substringToIndex:15];
                    truncatedAutor = [truncatedAutor stringByAppendingString:@"..."];
                }
                else
                {
                    truncatedAutor = truncatedAutor;
                }
                
                subTitle = [[UILabel alloc]initWithFrame:CGRectMake(authorXPos, yVal, [self widthOfString:truncatedAutor withFont:subTitleFont], subtitltHeight)];
                // yVal = yVal + subTitle.frame.origin.y+subTitle.frame.size.height;
                authorXPos = authorXPos + subTitle.frame.size.width + VARIABLE_WIDTH(5);
                
            }
            // subTitle.backgroundColor = [UIColor redColor];
            if ( [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date])
            {
                newsDate = [[UILabel alloc] initWithFrame:CGRectMake(authorXPos , yVal, [self widthOfString:[(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date] withFont:subTitleFont], subtitltHeight)];
                // yVal = yVal +newsDate.frame.origin.y+newsDate.frame.size.height;
                authorXPos = authorXPos + newsDate.frame.size.width + VARIABLE_WIDTH(5);
            }
            if ( [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] time])
            {
                newsTime = [[UILabel alloc] initWithFrame:CGRectMake(authorXPos , yVal, [self widthOfString:[(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] time] withFont:subTitleFont], subtitltHeight)];
                // yVal = yVal + newsTime.frame.origin.y+newsTime.frame.size.height;
              //  authorXPos = authorXPos + newsTime.frame.size.width ;
                
            }
            if ([[array_ScrollNewsData objectAtIndex:indexPath.row]author])
            {
                yVal = yVal + subTitle.frame.size.height+ VARIABLE_HEIGHT(5);
            }
            else if ( [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date])
            {
                yVal = yVal +newsDate.frame.size.height+ VARIABLE_HEIGHT(5);
            }
            else  if ( [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] time])
            {
                yVal = yVal + newsTime.frame.size.height+ VARIABLE_HEIGHT(5);
            }
            else
            {
                yVal = yVal + VARIABLE_HEIGHT(5);
            }
            //            newsDate.backgroundColor = [UIColor redColor];
            //            subTitle.backgroundColor = [UIColor redColor];
            //            newsTime.backgroundColor = [UIColor redColor];
            //Body text for each cell
            if ([[array_ScrollNewsData objectAtIndex:indexPath.row] sDesc])
            {
                body = [[UILabel alloc]initWithFrame:CGRectMake(xPos, yVal , width ,(ceil)(2 *bodyHeight))];
               // yVal = yVal + body.frame.size.height;
                
            }
            
        }
        
        CGRect frame = [tableView rectForRowAtIndexPath:indexPath];
        NSLog(@"%f---> %F",frame.size.height,title.frame.size.height+subTitle.frame.size.height+body.frame.size.height);
        
        
        //title alignment
        title.numberOfLines = 0;
        title.textColor = [UIColor blackColor];
        title.font =titleFont;
        title.text = [[array_ScrollNewsData objectAtIndex:indexPath.row] title] ;
        [cell.contentView addSubview:title];
        
        
        //Smaller image at right corner for cells other than first cell
        if(indexPath.row != 0 )
        {
            sdWebSmallImage = [[SdImageView alloc]initWithFrame:image.frame] ;
            [sdWebSmallImage loadImage:[(ScrollTemplateItems*)([array_ScrollNewsData objectAtIndex:indexPath.row]) image]];
            [cell.contentView addSubview:sdWebSmallImage];
        }
        //subtitle
        // subTitle.numberOfLines = 0;
        if([[array_ScrollNewsData objectAtIndex:indexPath.row] author])
        {
            subTitle.lineBreakMode =NSLineBreakByWordWrapping;
            subTitle.textColor = [UIColor grayColor];
            subTitle.font = subTitleFont;
            subTitle.lineBreakMode = NSLineBreakByTruncatingTail;
            subTitle.text = truncatedAutor;
            [cell.contentView addSubview:subTitle];
        }
        if ([(ScrollTemplateItems*) [array_ScrollNewsData objectAtIndex:indexPath.row]date])
        {
            newsDate.textColor = [UIColor grayColor];
            newsDate.font = subTitleFont;
            newsDate.text = [(ScrollTemplateItems*) [array_ScrollNewsData objectAtIndex:indexPath.row]date];
            [cell.contentView addSubview:newsDate];
        }
        if ([(ScrollTemplateItems*) [array_ScrollNewsData objectAtIndex:indexPath.row]time])
        {
            newsTime.textColor = [UIColor grayColor];
            newsTime.font = subTitleFont;
            newsTime.text = [(ScrollTemplateItems*) [array_ScrollNewsData objectAtIndex:indexPath.row]time];
            [cell.contentView addSubview:newsTime];
        }
        //body
        
        if(indexPath.row != 0 )
        {
            body.numberOfLines = 2;
        }
        else{
            body.numberOfLines = 0;
            
        }
        
        body.lineBreakMode = NSLineBreakByTruncatingTail;
        body.textColor = [UIColor blackColor];
        body.font = bodyFont;
        body.text =[[array_ScrollNewsData objectAtIndex:indexPath.row] sDesc];
        [cell.contentView addSubview:body];
        
    }
    //    else if (indexPath.row == [array_ScrollNewsData count] && [nextPage intValue]== 1)
    //    {
    //
    //        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //        CGRect frame;
    //        if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
    //            frame.origin.x = 52;
    //            frame.origin.y = 12;
    //            frame.size.width = 250;
    //            frame.size.height = 24;
    //        }
    //        else{
    //            frame.origin.x = 70;
    //            frame.origin.y = 20;
    //            frame.size.width = 600;
    //            frame.size.height = 30;
    //        }
    //        loading.frame = frame;
    //
    //        //loading.center = cell.contentView.center;
    //        loading.color = [UIColor blackColor];
    //
    //
    //        [cell.contentView addSubview:loading];
    //        [loading startAnimating];
    //        cell.userInteractionEnabled = NO;
    //    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NewsDetailViewController *nd =[[NewsDetailViewController alloc] init];
    [defaults setValue:btnName forKey:@"categoryNameInScroll"];
    nd.newsID = [[array_ScrollNewsData objectAtIndex:indexPath.row]itemID];
    [self.parentViewController.navigationController pushViewController:nd animated:NO];
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return array_ScrollNewsData.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"collCell";
    
    
    CollectionViewCell *cell = nil;
    
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"CollectionViewCell" owner:self options:nil];
    }
    
    cell.cellImage.image = nil;
    
    
    
    if(array_ScrollNewsData.count!=0 && indexPath.row != array_ScrollNewsData.count)
    {
        if([[array_ScrollNewsData objectAtIndex:indexPath.row]title])
        {
            cell.cellLabel.text = [[array_ScrollNewsData objectAtIndex:indexPath.row]title];
        }
        if(IPAD){
            cell.cellLabel.font = [UIFont systemFontOfSize:18];
        }
        
        cell.cellLabel.textColor = [UIColor blackColor];
        
        cell.cellLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        if(NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"])
        {
            cell.playIcon.hidden = true;
            if([([array_ScrollNewsData objectAtIndex:indexPath.row]) image])
                
            {
                imageArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[(ScrollTemplateItems*)([array_ScrollNewsData objectAtIndex:indexPath.row]) image]]];
                
                NSString* urlStr = imageArray[0];
                
                if ([photoDictionary objectForKey:urlStr]) {
                    
                    cell.cellImage.image = [photoDictionary objectForKey:urlStr];
                }
                else{
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                        
                        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlStr]];
                        
                        UIImage* image = [[UIImage alloc] initWithData:imageData];
                        
                        
                        if(image){
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                cell.cellImage.image = image;
                                [photoDictionary setObject:image forKey:urlStr];
                            });
                            
                        }
                    });
                }
            }
            
            
        }
        else
            if(NSOrderedSame==[btnName caseInsensitiveCompare:@"Videos"])
            {
                cell.playIcon.hidden = false;
                @try{
                    NSArray* videoarray = [self convertToArrayFromCommaSeparated:[[array_ScrollNewsData objectAtIndex:indexPath.row] videoLink]];
                    
                    NSString* url = videoarray[0];
                    UIImage *image = [UIImage imageNamed:@"video-not-present.png"];
                    cell.cellImage.image = image;
                    cell.cellImage.contentMode = UIViewContentModeScaleToFill;
                    
                    if ([imageDictionary objectForKey:url]) {
                        
                        cell.cellImage.image = [imageDictionary objectForKey:url];
                        cell.playIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
                    }
                    else
                    {
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                            
                            
                            AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:url]];
                            
                            //  Get thumbnail at the very start of the video
                            CMTime time = CMTimeMake(3, 2);
                            
                            
                            //  Get image from the video at the given time
                            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                            imageGenerator.appliesPreferredTrackTransform = YES;
                            
                            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
                            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
                            CGImageRelease(imageRef);
                            
                            if(thumbnail){
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    cell.cellImage.image = thumbnail;
                                    cell.playIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
                                    [imageDictionary setObject:thumbnail forKey:url];
                                    
                                });
                                
                            }
                        });
                    }
                }
                @catch (NSException *exception) {
                    // Do nothing
                    cell.playIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
                }
                
                
                imageArray = [[NSMutableArray alloc]init];
                if([([array_ScrollNewsData objectAtIndex:indexPath.row]) videoLink])
                    
                {
                    imageArray = [[NSMutableArray alloc]initWithArray:[self convertToArrayFromCommaSeparated:[(ScrollTemplateItems*)([array_ScrollNewsData objectAtIndex:indexPath.row]) videoLink]]];
                    
                }
                
            }
        
        
        cell.photoCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)imageArray.count];
        
        
        if(IPAD)
        {
            cell.photoCount.font = [UIFont systemFontOfSize:18];
        }
        
        cell.photoImg.image = [UIImage imageNamed:@"camera-icon.png"];
    }
    cell.userInteractionEnabled = YES;
    collectionView.userInteractionEnabled = YES;
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(NSOrderedSame==[btnName caseInsensitiveCompare:@"Photos"])
    {
        toSendArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[(ScrollTemplateItems*)([array_ScrollNewsData objectAtIndex:indexPath.row]) image]]];
    }
    if(NSOrderedSame==[btnName caseInsensitiveCompare:@"Videos"])
    {
        toSendArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[(ScrollTemplateItems*)([array_ScrollNewsData objectAtIndex:indexPath.row])videoLink]]];
        
    }
    if(toSendArray.count == 1)
    {
        PhotoDetailViewController *evt  = [[PhotoDetailViewController alloc]initWithNibName:@"PhotoDetailViewController" bundle:nil];
        evt.lDescription = [[array_ScrollNewsData objectAtIndex:indexPath.row]lDesc];
        //evt.imgNumber = indexPath.row;
        evt.photoArray = [[NSMutableArray alloc]initWithArray:toSendArray];
        evt.catName = btnName;
        evt.catColor = [[array_ScrollNewsData objectAtIndex:indexPath.row]catColor];
        evt.titleColor = [[array_ScrollNewsData objectAtIndex:indexPath.row]catTxtColor];
        evt.newsID = [[array_ScrollNewsData objectAtIndex:indexPath.row]itemID];
        [self.navigationController pushViewController:evt animated:YES];
        
    }
    else if(toSendArray.count == 0)
    {
        [UtilityManager showAlert:nil msg:@"Video not found"];
    }
    else
    {
        PhotoFolderViewController* evt = [[PhotoFolderViewController alloc] initWithNibName:@"PhotoFolderViewController" bundle:nil];
        evt.imgArray = [[NSMutableArray alloc]initWithArray:toSendArray];
        evt.longD = [[array_ScrollNewsData objectAtIndex:indexPath.row]lDesc];
        evt.catName = btnName;
        evt.catColor = [[array_ScrollNewsData objectAtIndex:indexPath.row]catColor];
        evt.titleColor = [[array_ScrollNewsData objectAtIndex:indexPath.row]catTxtColor];
        evt.newsID = [[array_ScrollNewsData objectAtIndex:indexPath.row]itemID];
        [self.navigationController pushViewController:evt animated:YES];
    }
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //CGRect screenRect = [[UIScreen mainScreen] bounds];
   // CGFloat screenWidth = screenRect.size.width;
  //  CGFloat screenHeight = screenRect.size.height;
    float cellwidth = (scrollCollectionView.frame.size.width)/2  ; //Replace the divisor with the column count requirement. Make sure to have it in float.
    float cellheigth = (scrollCollectionView.frame.size.height )/3 - (2*3);
    CGSize size = CGSizeMake(cellwidth, cellheigth);
    
    return size;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0; // This is the minimum inter item spacing, can be more
}

-(NSArray *)convertToArrayFromCommaSeparated:(NSString*)string{
    return [string componentsSeparatedByString:@","];
}
-(UIImage*) drawImage:(UIImage*) fgImage
              inImage:(UIImage*) bgImage bigImageSize:(CGSize)bgSize
              atPoint:(CGPoint)  point
             withSize:(CGSize) size
{
    
    UIGraphicsBeginImageContextWithOptions(bgSize, FALSE, 0.0);
    [bgImage drawInRect:CGRectMake(0 , 0, bgSize.width , bgSize.height)];
    [fgImage drawInRect:CGRectMake(point.x, point.y, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(void) setupWebView
{
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    configuration.allowsInlineMediaPlayback = YES;
    configuration.allowsAirPlayForMediaPlayback = YES;
    configuration.requiresUserActionForMediaPlayback = YES;
    configuration.allowsPictureInPictureMediaPlayback = YES;
    mToolbar.hidden = false;
    activityIndicator.center = self.view.center;
    
    self.webView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:configuration];
    int yVal = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    self.webView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y, SCREEN_WIDTH, SCREEN_HEIGHT- 44 - yVal - 30);
    
    
    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;
    
    //has been changed only for this location - as in DB they have not appended "http", it wont load
    NSMutableString *urlString ;
    urlString = [NSMutableString stringWithFormat:@"%@",nonFeedLink];
    
    NSString *string2;
    if ([urlString length]>7) {
        string2 = [urlString substringWithRange: NSMakeRange (0, 7)];
    }
    else
        string2 = [urlString copy];
    
    DLog(@"String2:%@", string2);
    
    if([string2 caseInsensitiveCompare:@"http://"] == NSOrderedSame || [string2 caseInsensitiveCompare:@"https:/"] == NSOrderedSame){
        
        urlString = [NSMutableString stringWithFormat:@"%@",nonFeedLink];
    }
    else{
        urlString = [NSMutableString stringWithString:@"http://"];
        [urlString appendFormat:@"%@",nonFeedLink];
    }
    
    NSURL* url;
    if ([urlString containsString:@" "])
    {
        NSError *error = nil;
        NSRegularExpression *regexExp = [NSRegularExpression regularExpressionWithPattern:@" " options:NSRegularExpressionCaseInsensitive error:&error];
        url =[NSURL URLWithString:[regexExp stringByReplacingMatchesInString:urlString options:0 range:NSMakeRange(0, [urlString length]) withTemplate:@"%20"]];
    }
    //  url = [NSURL URLWithString:[urlString urlEncodeUsingEncodingSpace:NSUTF8StringEncoding]];
    else
        url = [NSURL URLWithString:urlString];
    
    
    
    _request = [NSMutableURLRequest requestWithURL:url];
    
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    
    NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
    [cookieProperties setObject:@"mobileApp" forKey:NSHTTPCookieName];
    [cookieProperties setObject:@"1" forKey:NSHTTPCookieValue];
    [cookieProperties setObject:[url host] forKey:NSHTTPCookieDomain];
    [cookieProperties setObject:[url host] forKey:NSHTTPCookieOriginURL];
    [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
    [cookieProperties setObject:@"0" forKey:NSHTTPCookieVersion];
    
    cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
    
    
    self.webView.allowsBackForwardNavigationGestures = YES;
    [self.webView loadRequest:_request];
    self.webView.backgroundColor=[UIColor clearColor];
    
    
    
    [self updateButtons];
    [self.view addSubview:self.webView];
    activityIndicator.center = self.webView.center;
    [self.webView addSubview:activityIndicator];
    [activityIndicator startAnimating];
    
    
}

- (void)updateButtons
{
    self.forward.enabled = self.webView.canGoForward;
    self.back.enabled = self.webView.canGoBack;
    self.stop.enabled = self.webView.loading;
    
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}


-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    
    return UIInterfaceOrientationMaskPortrait;
}


- (IBAction)webViewRefresh:(id)sender {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.webView.URL];
    [self.webView loadRequest:request];
}
- (IBAction)webViewForward:(id)sender {
    [self.webView goForward];
}

- (IBAction)webViewcancel:(id)sender {
    [self.webView stopLoading];
}
- (IBAction)webViewbackward:(id)sender {
    [self.webView goBack];
}


-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    [activityIndicator startAnimating];
    [self updateButtons];
}

-(void) webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    
        [activityIndicator stopAnimating];
        
        [self updateButtons];
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
}


-(void) webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    [activityIndicator stopAnimating];
    [self updateButtons];
}

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction *action) {
                                                          completionHandler();
                                                      }]];
    [self presentViewController:alertController animated:YES completion:^{}];
}


//webkit authentication challenge delegate
- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler
{
    NSURLCredential * credential = [[NSURLCredential alloc] initWithTrust:[challenge protectionSpace].serverTrust];
    completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
}

-(void) webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    //--ashika---
    //to handle blank screen while re-directing to correct URL
    if([[[navigationAction.request URL] absoluteString]  containsString:@"hashtag"]||[[[navigationAction.request URL] absoluteString]  containsString:@"blank"] ){
        
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    
    if ([[navigationAction.request URL] fragment]) {
        
        
        [self.webView evaluateJavaScript:@"window.location.hash='#hashtag'" completionHandler:nil];
        
        //return YES;
    }
    if (navigationAction.navigationType == UIWebViewNavigationTypeLinkClicked) {
        
        if ([[[navigationAction.request URL] absoluteString] containsString:@"itunes.apple.com"]) {
            [UtilityManager customUrlLoad:[navigationAction.request URL]];
        }
        
    }
    
    
    
    if ([[[navigationAction.request URL] scheme] isEqual:@"mailto"])
    {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [[mailViewController navigationBar] setTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]]];
            
            NSArray *rawURLparts = [[[navigationAction.request URL] resourceSpecifier] componentsSeparatedByString:@"?"];
            if (rawURLparts.count > 2) {
                //return NO; // invalid URL
            }
            
            NSMutableArray *toRecipients = [NSMutableArray array];
            NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
            if (defaultRecipient.length) {
                [toRecipients addObject:defaultRecipient];
            }
            
            if (rawURLparts.count == 2) {
                NSString *queryString = [rawURLparts objectAtIndex:1];
                
                NSArray *params = [queryString componentsSeparatedByString:@"&"];
                for (NSString *param in params) {
                    NSArray *keyValue = [param componentsSeparatedByString:@"="];
                    if (keyValue.count != 2) {
                        continue;
                    }
                    NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
                    NSString *value = [keyValue objectAtIndex:1];
                    
                    value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault,(CFStringRef)value,CFSTR("")));
                    
                    
                    if ([key isEqualToString:@"subject"]) {
                        [mailViewController setSubject:value];
                    }
                    
                    if ([key isEqualToString:@"body"]) {
                        [mailViewController setMessageBody:value isHTML:NO];
                    }
                    
                    if ([key isEqualToString:@"to"]) {
                        [toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
                    }
                    
                    if ([key isEqualToString:@"cc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setCcRecipients:recipients];
                    }
                    
                    if ([key isEqualToString:@"bcc"]) {
                        NSArray *recipients = [value componentsSeparatedByString:@","];
                        [mailViewController setBccRecipients:recipients];
                    }
                }
            }
            
            [mailViewController setToRecipients:toRecipients];
            [self presentViewController:mailViewController animated:YES completion:nil];
            // [mailViewController release];
            //return NO;
            
        }
        
        else {
            [UtilityManager showAlert:NSLocalizedString(@"Cannot Send",@"Cannot Send") msg:NSLocalizedString(@"Please go to the Mail applications and set up an email account",@"Please go to the Mail applications and set up an email account")];
        }
        
        //return NO;
    }
    if (!navigationAction.targetFrame) {
        [activityIndicator stopAnimating];
        activityIndicator.hidden = TRUE;
        
        
        NSURL *url = navigationAction.request.URL;
        
        if( [[url absoluteString] containsString:@"itunes.apple.com"]){
            UIApplication *app = [UIApplication sharedApplication];
            if ([app canOpenURL:url]) {
                [app openURL:url];
            }
            decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }
        else{
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [self.webView loadRequest:request];
        }
        
        
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    
}

@end

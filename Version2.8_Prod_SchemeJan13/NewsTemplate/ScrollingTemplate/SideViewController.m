//

//  SideViewController.m

//  HubCiti

//

//  Created by Ashika on 6/24/16.

//  Copyright © 2016 Keshavakarthik S. All rights reserved.

//



#import "SideViewController.h"

#import "MFSideMenu.h"

#import "ScrollTemplateViewController.h"

#import "HubCitiConstants.h"

#import "SideNavigationList.h"

#import "HTTPClient.h"

#import "AppDelegate.h"

#import "SideNavigationResponse.h"

#import "BookMarkGetRequest.h"

#import "RegistrationViewController.h"

#import "BookMarksViewController.h"

#import "SettingsViewController.h"

#import "UserInformationViewController.h"

#import "AboutAndPrivacyScreen.h"

#import "UIColor+ExtraColorSpecifications.h"

#import "ButtonSetupViewController.h"

#import "SESpringBoard.h"

#import "bottomButtonView.h"

#import "CityExperienceViewController.h"

#import "FilterRetailersList.h"

#import "PreferredCategoriesScreen.h"

#import "AboutAndPrivacyScreen.h"

#import "SettingsViewController.h"

#import "FindViewController.h"

#import "AnythingPage.h"

//#import "HotDealsList.h"

#import "AlertsList.h"

#import "AppDelegate.h"

#import "SingleCatRetailers.h"

#import "ScanNowScreen.h"

#import "LocationDetailsViewController.h"

#import "RetailersListViewController.h"

#import "EventsListViewController.h"

#import "CouponsViewController.h"

#import "AnyViewController.h"

#import "WebBrowserViewController.h"

#import "EmailShareViewController.h"

#import "SubMenuGroupingAndSortingViewController.h"

#import "GalleryList.h"

#import "LoginViewController.h"

#import "FAQCategoryList.h"

#import "FilterListViewController.h"

#import "FundraiserListViewController.h"

#import "CouponsMyAccountsViewController.h"

#import "SwipesViewController.h"

#import "SpecialOffersViewController.h"

#import "GovQALoginPageViewController.h"

#import "ReportProblemViewController.h"

#import "HTTPClient.h"

#import "MainMenuRequest.h"

#import "MainMenuResponse.h"

#import "MenuItemList.h"

#import "BottomItemList.h"

#import "UserSettingsController.h"
#import "CouponsInDealViewController.h"
#import "CityPreferenceViewController.h"

#import "GetUserInfoResponse.h"
#import "BandViewController.h"
#import "LoginViewController.h"
#import "CombinationViewController.h"
#import "CombinationCategoryViewController.h"
#import "PhotoView.h"
#import "BandsNearByEventsMapTable.h"
extern BOOL isSameUser;
@interface SideViewController ()<HTTPClientDelegate>{
    
    NSMutableArray *tableArray;
    NSMutableArray *newsCategory,*hubcitiCategory,*tableData;
    NSString *menuItemName;
    CustomizedNavController *navigationcontroller;
    UIActivityIndicatorView *activityIndicator;
    SdImageView *img;
    
    
}

@property(nonatomic,strong) SideNavigationResponse *sideNavResponse;
@property(nonatomic,strong) MainMenuResponse  *menuResponse;
@property(strong,nonatomic) BookMarkGetRequest *bookMarkGetResponse;
@property(nonatomic,strong) GetUserInfoResponse* infoResponse ;



@end

@implementation SideViewController

@synthesize bookMarkArray,sideNavResponse,menuResponse,infoResponse,loginDo,sidemenuTableView,isSideMenu, isNewsTemplate,todayEventList;
static NSMutableDictionary *dict_linkTypeName_BottomButton = nil;
static NSMutableDictionary *dict_linkTypeName_MenuButton = nil;

#pragma mark - Service Respone methods



- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
    
}



- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject

{
    [HubCitiAppDelegate removeActivityIndicator];
    
    DLog(@"parameter: %@",responseObject);
    
    
    if(iWebRequestState == GETUSERINFO || iWebRequestState == GETFAVLOCATIONS || iWebRequestState == HubcitiAnythingInfo|| iWebRequestState ==appsitedetails)
        [self responseData:responseObject];
    else
        [self responseDataAsync:responseObject];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
        
        if (dict_linkTypeName_BottomButton == nil) {//assigning number to each bottom buttons - in order to
            
            dict_linkTypeName_BottomButton = [[NSMutableDictionary alloc]init];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:0] forKey:@"Hot Deals"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:1] forKey:@"Scan Now"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:2] forKey:@"Alerts"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:3] forKey:@"Events"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:4] forKey:@"Whats NearBy"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:5] forKey:@"Find"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:6] forKey:@"City Experience"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:7] forKey:@"City Services"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:8] forKey:@"Visitors Bureau"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:9] forKey:@"Transportation"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:10] forKey:@"Preference"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:11] forKey:@"About"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:12] forKey:@"Share"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:13] forKey:@"Settings"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:14] forKey:@"AnythingPage"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:15] forKey:@"AppSite"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:16] forKey:@"SubMenu"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:17] forKey:@"Filters"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:18] forKey:@"Coupon"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:19] forKey:@"Deals"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:20] forKey:@"FAQ"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:21] forKey:@"Fundraisers"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:22] forKey:@"User Information"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:23] forKey:@"Category Favorites"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:24] forKey:@"Location Preferences"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:25] forKey:@"City Favorites"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:26] forKey:@"Map"];
            
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:27] forKey:@"SortFilter"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:29] forKey:@"My Deals"];
            [dict_linkTypeName_BottomButton setObject:[NSNumber numberWithInt:30] forKey:@"Privacy Policy"];
            
            
            
        }
        
        
        
        if (dict_linkTypeName_MenuButton == nil) {//assigning number to each menu buttons - in order to
            
            dict_linkTypeName_MenuButton = [[NSMutableDictionary alloc]init];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:0] forKey:@"City Experience"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:1] forKey:@"Coupon"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:2] forKey:@"AppSite"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:3] forKey:@"AnythingPage"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:4] forKey:@"Alerts"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:5] forKey:@"Deals"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:6] forKey:@"Find"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:7] forKey:@"Scan Now"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:8] forKey:@"Whats NearBy"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:9] forKey:@"SubMenu"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:10] forKey:@"Events"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:11] forKey:@"Preference"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:12] forKey:@"About"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:13] forKey:@"Settings"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:14] forKey:@"Share"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:15] forKey:@"Filters"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:16] forKey:@"FAQ"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:17] forKey:@"Fundraisers"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:18] forKey:@"User Information"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:19] forKey:@"Category Favorites"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:20] forKey:@"Location Preferences"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:21] forKey:@"City Favorites"];
            
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:10] forKey:@"Band Events"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:22] forKey:@"Feedback"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:23] forKey:@"Playing Today"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:24] forKey:@"My Deals"];
            [dict_linkTypeName_MenuButton setObject:[NSNumber numberWithInt:25] forKey:@"Privacy Policy"];
            
        }
        
    }
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    [sidemenuTableView setBackgroundColor:[UIColor whiteColor]];
    tableArray = [[NSMutableArray alloc] init];
    tableData =[[NSMutableArray alloc] init];
    bookMarkArray = [[NSMutableArray alloc] init];
    

    if(self.isNewsTemplate)
        [self request_getBookMark];
    [self requestForHubcitiSidenavigation];
    
    
    
    if(IPAD)
        sidemenuTableView.contentInset = UIEdgeInsetsMake(0, 0,VARIABLE_HEIGHT(70), 0);
    else
        sidemenuTableView.contentInset = UIEdgeInsetsMake(0, 0,VARIABLE_HEIGHT(30), 0);
    [sidemenuTableView setDataSourceDelegate:self];
    
    [sidemenuTableView setTableViewDelegate:self];
    
    
   
    
}


-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
   [ [NSNotificationCenter defaultCenter] removeObserver:self name:@"notificationName"
object:nil];
    
}
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    
}

-(void) setGuestUserSettings
{
    
    
    NSString * pushNotifyString = @"true";
           NSMutableString *requestStr = [[NSMutableString alloc] init];
    [requestStr appendFormat:@"<UserSettings><userId>%@</userId><deviceId>%@</deviceId>", [defaults  valueForKey:KEY_USERID],[defaults valueForKey:KEY_DEVICEID]];
    [requestStr appendFormat:@"<pushNotify>%@</pushNotify><hubCitiId>%@</hubCitiId>",pushNotifyString,[defaults valueForKey:KEY_HUBCITIID]];
    [requestStr appendFormat:@"<bLocService>true</bLocService>"];
   

    
    
            [requestStr appendFormat:@"<localeRadius>99</localeRadius></UserSettings>"];
    
    NSString *urlReqString = [NSString stringWithFormat:@"%@firstuse/setusersettings",BASE_URL];
    
    NSString *responseXmlData = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlReqString withParam:requestStr]];
    
    NSLog(@"%@",responseXmlData);
    
    
    
}



#pragma mark - JKExpandTableViewDelegate

// return YES if more than one child under this parent can be selected at the same time.  Otherwise, return NO.

// it is permissible to have a mix of multi-selectables and non-multi-selectables.

- (BOOL) shouldSupportMultipleSelectableChildrenAtParentIndex:(NSInteger) parentIndex {
    
    if ((parentIndex % 2) == 0) {
        
        return NO;
        
    } else {
        
        return YES;
        
    }
    
    
    
    
    
}



- (void) tableView:(UITableView *)tableView didSelectCellAtChildIndex:(NSInteger) childIndex withInParentCellIndex:(NSInteger) parentIndex {
    
   [defaults setBool:NO forKey:@"isCommingFromWebView"];
    SideNavigationNewsCat *array =[tableArray objectAtIndex:parentIndex];
    NSLog(@"tablearaay: %@",array.mainCatList);
    
    NSMutableArray *childArray = [array.mainCatList mutableCopy];
    CombinationCategoryViewController *newsByCategoryVC = [[CombinationCategoryViewController alloc] initWithNibName:@"CombinationCategoryViewController" bundle:nil];
    newsByCategoryVC.catName = [[childArray objectAtIndex:childIndex] objectForKey:@"subCatName"];
    newsByCategoryVC.catColor = [[tableArray objectAtIndex:parentIndex] catColor];
    newsByCategoryVC.titleColor = [(SideNavigationNewsCat *)[tableArray objectAtIndex:parentIndex] catTxtColor];
    
    newsByCategoryVC.subCategoryExist = YES;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    [self.menuContainerViewController.centerViewController pushViewController:newsByCategoryVC animated:NO];
    
    
    
}

- (void) tableView:(UITableView *)tableView didSelectParentCellAtIndex:(NSInteger) parentIndex
{
    [defaults setBool:NO forKey:@"isCommingFromWebView"];
    
    NSIndexPath *index =[NSIndexPath indexPathForRow:parentIndex inSection:0];
    selectedCell  = [tableView cellForRowAtIndexPath:index];
    cellRectInTable = [tableView rectForRowAtIndexPath:index];
    
    NSLog(@"parent cell");
    {
        
        int i=0;
        int l=0;
        NSString *news= [tableData objectAtIndex:parentIndex];
        
        //login logout pressed
        if([news isEqualToString:@"Login/SignUp"]||[news isEqualToString:@"Logout"]){
            
            if(userIdentifier == true){
                
                               
                [defaults setBool:NO forKey:@"LoginSuccess"];
                
                [defaults setValue:@"1" forKey:KEY_MENULEVEL];
                
                [defaults setBool:YES forKey:@"isMainMenuSignUp"];
                
                [self setGuestUserSettings];
                
                LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
                AppDelegate *app =HubCitiAppDelegate;
                if([defaults boolForKey:@"newsTemplateExist"]){
                    NSArray *keys = [cashedResponse allKeys];
                    for (int i = 0 ; i < [keys count]; i++)
                    {
                        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
                            [cashedResponse removeObjectForKey:keys[i]];
                        }
                        
                    }
                    
                    ReleaseAndNilify(selectedSortOption);
                    ReleaseAndNilify(selectedFilterOption);
                    ReleaseAndNilify(selectedSortCityDic);
                    ReleaseAndNilify(typeId);
                    ReleaseAndNilify(deptId);
                    
                    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
                    
                    
                    
                    [defaults setValue:[defaults valueForKey:@"centerViewController"] forKey:@"centerViewController"];
                    
                }
                viewController.login_DO = app.loginDO;
                [self pushViewController:viewController];
                
            }
            
            else{
                ScrollTemplateViewController *viewController;
                CombinationViewController *viewControllerCV;
                BlockViewController *viewControllerBT;
                MainMenuViewController *viewControllerMM;
                NSArray *controllers;

                    if([self authenticate]){
                        [NewsUtility setUserSettings];
                       // [SharedManager setGps_allow_flag:YES];
                        
                        [defaults setValue:@"" forKey:KEY_LATITUDE];//reset the values
                        
                        [defaults setValue:@"" forKey:KEY_LONGITUDE];
                        
                        //[defaults setValue:nil forKey:@"sortedCitiIDs"];
                        
                       // [defaults setValue:@"NO" forKey:@"allowingGPS"];
                         [defaults setValue:@"1" forKey:KEY_MENULEVEL];
                                               [tableData removeObjectAtIndex:parentIndex];
                        [tableData insertObject:@"Login/SignUp" atIndex:parentIndex];
                        //[sidemenuTableView reloadData];
                        NSLog(@" region :%@ ---- city id =%@",RegionApp,[defaults valueForKey:@"sortedCitiIDs"]);
                        if ([RegionApp isEqualToString:@"1"] /*&& ![defaults valueForKey:@"sortedCitiIDs"]*/) {
                            [self requestCityPrefrences];
                        }
                        SideViewController *leftMenuViewController;
                        if([defaults boolForKey:@"newsTemplateExist"]){
                        if(IPAD){
                            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
                        }
                        else{
                            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
                            
                        }
                        leftMenuViewController.isNewsTemplate = TRUE;
                        if([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
                            
                            viewController = [[ScrollTemplateViewController alloc] initWithNibName:@"ScrollTemplateViewController" bundle:nil];
                            controllers = [NSArray arrayWithObject:viewController];
                            
                        }
                        
                        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"News Tile Template"]){
                            
                            
                            viewControllerBT= [[BlockViewController alloc] initWithNibName:@"BlockViewController" bundle:nil];
                            controllers = [NSArray arrayWithObject:viewControllerBT];
                        }
                        else{
                            viewControllerCV =   [[CombinationViewController alloc] init];
                            controllers = [NSArray arrayWithObject:viewControllerCV];
                        }
                        CustomizedNavController *navigationController = self.menuContainerViewController.centerViewController;
                        
                        navigationController.viewControllers = controllers;
                              leftMenuViewController.isNewsTemplate = TRUE;
                        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
                        [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
                        }
                        else{
                            
                            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
                            viewControllerMM = [[MainMenuViewController alloc] initWithNibName:@"MainMenuViewController" bundle:nil];
                            [self.menuContainerViewController.centerViewController pushViewController:viewControllerMM animated:NO];
                            
                        }

                    }
                
                
                    
                }
                
                
                
            }
            
        //News settings clicked
        else if([news isEqualToString:@"News Settings"])
        {
            NSLog(@"News settings clicked");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNotification" object:self];
            
            
            if (userIdentifier == TRUE)
            {
                [NewsUtility signupPopUp:self];
                
            }
            else
            {
                BookMarksViewController *bk =[[BookMarksViewController alloc] initWithNibName:@"BookMarksViewController" bundle:nil];
                if(self.isNewsTemplate)
                    bk.comingFromSideNews = TRUE;
               else
                   bk.comingFromSideNews = FALSE;
                    
                [self.menuContainerViewController.centerViewController pushViewController:bk animated:NO];
               
            }
            
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
 
        }
        
        //home clicked
        else if([news isEqualToString:@"Home"]){
            NSLog(@"home clicked");
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            NSLog(@"center:%@",[defaults valueForKey:@"centerViewController"]);
            if([defaults boolForKey:@"newsTemplateExist"]){
                
                //  [NewsUtility pushViewFromHamberger:self];
                if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
                    [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.menuContainerViewController.centerViewController];
                    
                }
                else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
                    [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.menuContainerViewController.centerViewController];
                }
                else{
                    [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.menuContainerViewController.centerViewController];
                    
                }
            }
            
            else
            {
                [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.menuContainerViewController.centerViewController];
            }
            
            
            
        }
        
        
        
        //category or functionality cliked
        else{
            
            //check if bookmark section data and selected row data are same if same pass the index to viewController
            
            int j;
            if([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Scrolling News Template"]){
                
                for(j=0;j<bookMarkArray.count; j++){
                    
                    if([news isEqualToString:[bookMarkArray objectAtIndex:j]]){
                        
                        i=1;
                        
                        break;
                       
                    }
                    
                }
                
            }
            
            if(i==1){
                
                
                ScrollTemplateViewController *viewController = [[ScrollTemplateViewController alloc] initWithNibName:@"ScrollTemplateViewController" bundle:nil];
                
                viewController.bookMarkselected = j;
                viewController.fromSideMenu = TRUE;
                [defaults setBool:YES forKey:@"NoPopUp"];
              [self.menuContainerViewController.centerViewController pushViewController:viewController animated:NO];
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
                
            }
            
            else{
                //check if functionality
                
                
                int k;
                
                for(k=0;k<hubcitiCategory.count; k++){
                    
                    if([news isEqualToString:[[hubcitiCategory objectAtIndex:k] mItemName]]){
                        
                        l=1;
                        
                        break;
                        
                        
                        
                    }
                    
                }
                
                
                
                if(l==1){
                    
                    
                    
                    [self callSubClassAccordingToSelection:(int)parentIndex];
                    
                    
                    
                }
                
                else{
                    // photo or video
                    if( NSOrderedSame==[[tableData objectAtIndex:parentIndex] caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [[tableData objectAtIndex:parentIndex] caseInsensitiveCompare:@"Videos"])
                    {
                        
                        PhotoView *photo = [[PhotoView alloc] initWithNibName:@"PhotoView" bundle:nil];
                        photo.categoryName = [[tableArray objectAtIndex:parentIndex] catName];
                        photo.catColor = [[tableArray objectAtIndex:parentIndex] catColor];
                        photo.titleColor = [[tableArray objectAtIndex:parentIndex] catTxtColor];

                        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
                        [self.menuContainerViewController.centerViewController pushViewController:photo animated:NO];
                        
                    }
                    else{
                      //category clicked
                        
                        CombinationCategoryViewController *newsByCategoryVC = [[CombinationCategoryViewController alloc] initWithNibName:@"CombinationCategoryViewController" bundle:nil];
                        newsByCategoryVC.catName = [[tableArray objectAtIndex:parentIndex] catName];
                        newsByCategoryVC.catColor = [[tableArray objectAtIndex:parentIndex] catColor];
                        newsByCategoryVC.titleColor = [(SideNavigationNewsCat *)[tableArray objectAtIndex:parentIndex] catTxtColor];
                        
                        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
                        [self.menuContainerViewController.centerViewController pushViewController:newsByCategoryVC animated:NO];
                        
                    }
                }
                
            }
            
        }
        
        
    }
}

- (void) tableView:(UITableView *)tableView didDeselectCellAtChildIndex:(NSInteger) childIndex withInParentCellIndex:(NSInteger) parentIndex {
    
    
    
}


#pragma mark - JKExpandTableViewDataSource

- (NSInteger) numberOfParentCells {
    
    return [tableData count];
    
}



- (NSInteger) numberOfChildCellsUnderParentIndex:(NSInteger) parentIndex {
    NSInteger countArray = [tableData count]>1? [tableData count] -2 :[tableData count]-1;
    
    if(parentIndex < countArray && [tableArray count]>0){
        if([[tableArray objectAtIndex:parentIndex] isKindOfClass:[SideNavigationNewsCat class]]){
            SideNavigationNewsCat *array =[tableArray objectAtIndex:parentIndex];
            if(array.mainCatList){
                NSLog(@"tablearaay: %@--->%@",array.mainCatList,[tableArray objectAtIndex:parentIndex]);
                
                NSMutableArray *childArray = [array.mainCatList mutableCopy];
                
                return [childArray count];
            }
            else return 0;
        }
        else return 0;
    }
    else return 0;
}



- (NSString *) labelForParentCellAtIndex:(NSInteger) parentIndex {
    
    return [NSString stringWithFormat:@"%@", [tableData objectAtIndex:parentIndex]];
    
}



- (NSString *) labelForCellAtChildIndex:(NSInteger) childIndex withinParentCellIndex:(NSInteger) parentIndex {
    
    SideNavigationNewsCat *array =[tableArray objectAtIndex:parentIndex];
    NSLog(@"tablearaay: %@",array.mainCatList);
    
    NSMutableArray *childArray = [array.mainCatList mutableCopy];
    
    
    return [[childArray objectAtIndex:childIndex] objectForKey:@"subCatName"];
    
}



- (BOOL) shouldDisplaySelectedStateForCellAtChildIndex:(NSInteger) childIndex withinParentCellIndex:(NSInteger) parentIndex {
    
    
    
    return NO;
    
}



-(void) viewWillAppear:(BOOL)animated{
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationFromBMsettings:) name:@"notificationName" object:nil];
    
    [super viewWillAppear:animated];
    
}

-(void)notificationHome:(NSNotification*)notification
{
    NSLog(@"Notification called");
    
    tableArray = [[NSMutableArray alloc] init];
    
    tableData =[[NSMutableArray alloc] init];
    
    bookMarkArray = [[NSMutableArray alloc] init];
    
    
    if(self.isNewsTemplate)
    
    
        [self request_getBookMark];
    
      
   
        [self requestForHubcitiSidenavigation];
    
}

-(void)notificationFromBMsettings:(NSNotification*)notification

{
    
    NSMutableArray *responseArray ;
    
    responseArray = notification.object;
    
    bookMarkArray = [[NSMutableArray alloc] init];
    
    hubcitiCategory = [[NSMutableArray alloc] init];
    
    tableArray = [[NSMutableArray alloc] init];
    
    tableData =[[NSMutableArray alloc] init];
    
    bookMarkArray = [responseArray objectAtIndex:0];
    
    tableArray = [responseArray objectAtIndex:1];
    
    tableData = [responseArray objectAtIndex:2];
    
    hubcitiCategory = [responseArray objectAtIndex:3];
    [tableArray addObject:@"News Settings"];
   // [tableData addObject:@"News Settings"];
    
    if(userIdentifier == TRUE){
        
        [tableData addObject:@"Login/SignUp"];
        
    }
    
    else{
        
        [tableData addObject:@"Logout"];
        
    }
    
    [sidemenuTableView setDataSourceDelegate:self];
    [sidemenuTableView setTableViewDelegate:self];
    
    [sidemenuTableView reloadData];
    
    
    
    
}



#pragma  api calls



-(void) responseDataAsync:(id)responseData{
    
    switch (iWebState) {
            
            //        case BOOKMARKS_GET:
            
            //            [self parse_getBookMark:responseData];
            
            //            break;
            
        case SIDENAVIGATIONS_GETs:
            
            [self parse_getSideNav:responseData];
            break;
            
        case HUBCITI_SIDENAVIGATIONS_GETs:
            [self parse_getHubcitiSideNav:responseData];
            break;
            
        default:
            
            break;
            
    }
    
    
    
}



-(void)requestForHubcitiSidenavigation
{

    [self showActivityIndicator];
    iWebState = HUBCITI_SIDENAVIGATIONS_GETs;
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if([defaults valueForKey:KEY_HUBCITIID])
    {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    }
    if([defaults valueForKey:KEY_USERID])
    {
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    }
    

        [parameters setValue:@"1" forKey:@"level"];
        [parameters setValue:@"0" forKey:@"linkId"];
    
    [parameters setValue:@"true" forKey:@"sideNaviPersonalizatn"];
   
    
    NSLog(@"%@",parameters);
    
  
    NSLog(@"%@",parameters);
    
    NSString *urlString =[NSString stringWithFormat:@"%@firstuse/v2/hubsidemenudisplay",BASE_URL];
    
    //NSString *urlString =[NSString stringWithFormat:@"http://10.10.221.139:9990/HubCiti2.8.7/firstuse/v2/hubsidemenudisplay"];
    NSLog(@"%@",urlString);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    
    [manager.requestSerializer setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        
        NSLog(@"response: %@",responseObject);
        [self parse_getSideNav:responseObject];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
        
        NSLog(@"Error: %@", error);
        
    }];
    
}

-(void) requestForSideNavigation{
    
    
    [self showActivityIndicator];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    iWebState = SIDENAVIGATIONS_GETs;
    
    if([defaults valueForKey:KEY_USERID])
        
    {
       
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
       
    }
    
    if([defaults valueForKey:KEY_HUBCITIID])
        
    {
        
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
        
    }
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    if(menulevel>1){
        
        NSString *linkId= [[HubCitiAppDelegate getLinkIdArray]objectAtIndex:menulevel-1];
        [parameters setValue:linkId forKey:@"linkId"];
        NSString *level = [defaults valueForKey:KEY_MENULEVEL];
        [parameters setValue:level forKey:@"level"];
    }
    else{
        
        [parameters setValue:@"0" forKey:@"linkId"];
    }
  
    NSLog(@"%@",parameters);
   
    NSString *urlString =[NSString stringWithFormat:@"%@band/getnewssidenavigationmenu",BASE_URL];
    
    NSLog(@"%@",urlString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    
    [manager.requestSerializer setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        NSLog(@"response: %@",responseObject);
        
        [self parse_getSideNav:responseObject];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
    }];
    
}



-(void) showActivityIndicator
{
    if(!activityIndicator){
        img = [[SdImageView alloc]initWithImage:[UIImage imageNamed:@"transparentBlack.png"]];
        img.frame = CGRectMake(85, (SCREEN_HEIGHT/2)-VARIABLE_HEIGHT(40), 150, 70);
        // img.center = self.view.center;
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, 7, 120, 20)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        label.text = @"Refreshing Data";
        label.numberOfLines=0;
        label.lineBreakMode=NSLineBreakByWordWrapping;
        label.font = [UIFont boldSystemFontOfSize:13.0];
        
        activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityIndicator.color = [UIColor colorWithRed:0 green:211 blue:255 alpha:1.0f];
        activityIndicator.center = img.center; //CGPointMake(160, 240);
        activityIndicator.frame = CGRectMake(65, 36, 20, 20);
        activityIndicator.hidesWhenStopped = YES;
        
        [img addSubview:label];
        [img addSubview:activityIndicator];
        [self.view addSubview:img];
        
        // [window bringSubviewToFront:activityIndicator];
        [activityIndicator startAnimating];
        [self.view setUserInteractionEnabled:NO];
    }
}

-(void) removeActivityIndicator{
    
    activityIndicator.hidden = YES;
    [activityIndicator stopAnimating];
    activityIndicator = nil;
    [img removeFromSuperview];
    [self.view setUserInteractionEnabled:YES];
    
}

-(void)parse_getHubcitiSideNav:(id)responseObject
{
    [self removeActivityIndicator];
    hubcitiCategory = [[NSMutableArray alloc] init];
    menuResponse = [[MainMenuResponse alloc] init];
    [menuResponse setValuesForKeysWithDictionary:responseObject];
    if([menuResponse.responseCode isEqualToString:@"10000"]){
        if(menuResponse.arMItemList)
        {
            for(int i=0;i<menuResponse.arMItemList.count;i++){
                
                MenuItemList *menuItemList = [[MenuItemList alloc] init];
                
                
                NSDictionary *dictmenu = menuResponse.arMItemList[i];
                
                
                @try
                {
                    [menuItemList setValuesForKeysWithDictionary:dictmenu];
                }
                @catch (NSException *exception)
                {
                    return;
                }
                
                
                [hubcitiCategory addObject:menuItemList];
            }
            
        }
        [NewsUtility saveMenuData : menuResponse];
        
        
        
        
    }
    //    else
    //    {
    ////        UILabel *errorLabel = [[UILabel alloc]init];
    ////        //errorLabel.backgroundColor = [UIColor darkGrayColor];
    ////        errorLabel.textColor = [UIColor darkGrayColor];
    ////        errorLabel.numberOfLines = 0;
    ////        NSString *respText;
    ////        if(menuResponse.responseText != nil)
    ////        {
    ////            respText = menuResponse.responseText;
    ////        }
    ////        else
    ////        {
    ////            respText = @"No Records Found";
    ////        }
    ////        errorLabel.text = respText;
    ////        errorLabel.frame = CGRectMake(VARIABLE_WIDTH(5), (SCREEN_HEIGHT-VARIABLE_HEIGHT(150))/2, sidemenuTableView.frame.size.width - VARIABLE_WIDTH(30), VARIABLE_HEIGHT(100));
    ////        [sidemenuTableView addSubview:errorLabel];
    ////        errorLabel.textAlignment = NSTextAlignmentCenter;

    
    //    }
    tableArray =[[NSMutableArray alloc] init];
    
    tableData = [[NSMutableArray alloc] init];
    
    
    NSMutableArray *tableCopy =[[NSMutableArray alloc] init];
    
    for(int i=0;i<hubcitiCategory.count;i++)
        
    {
        [tableCopy addObject:[hubcitiCategory objectAtIndex:i]];
        
    }
    for(int i=0;i<tableCopy.count;i++){
        [tableArray addObject:[tableCopy objectAtIndex:i]];
    }
    for(int i=0;i<tableArray.count;i++){
        [tableData addObject:[[tableArray objectAtIndex:i] mItemName]];
    }
    
    if(userIdentifier == TRUE){
        
        [tableData addObject:@"Login/SignUp"];
        
    }
    
    else{
        
        [tableData addObject:@"Logout"];
        
    }
    
    
    
    NSLog(@"%@",tableData);
    
    [sidemenuTableView setDataSourceDelegate:self];
    [sidemenuTableView setTableViewDelegate:self];
    
    [sidemenuTableView reloadData];
}


-(void) parse_getSideNav:(id) responseObject{
    [self removeActivityIndicator];
    newsCategory =[[NSMutableArray alloc] init];
    
    hubcitiCategory = [[NSMutableArray alloc] init];
    
    sideNavResponse = [[SideNavigationResponse alloc] init];
    
    [sideNavResponse setValuesForKeysWithDictionary:responseObject];
    
    if([sideNavResponse.responseCode isEqualToString:@"10000"]){
        
        if(sideNavResponse.listCatDetails){
          
            for(int i=0;i<sideNavResponse.listCatDetails.count;i++){
               
                SideNavigationList *sideNavList = [[SideNavigationList alloc] init];
                
                NSDictionary *dict =sideNavResponse.listCatDetails[i];
                
                [sideNavList setValuesForKeysWithDictionary:dict];
                
                
                if(sideNavList.listMainCat){
                    
                    for(int i=0;i<sideNavList.listMainCat.count;i++){
                        
                        SideNavigationNewsCat *sideNavnewsCat = [[SideNavigationNewsCat alloc] init];
                        
                        NSDictionary *dict = sideNavList.listMainCat[i];
                        
                        [sideNavnewsCat setValuesForKeysWithDictionary:dict];
                        
                        NSLog(@"string :%@", [sideNavnewsCat sortOrder]);
                        
                        [newsCategory addObject:sideNavnewsCat];
                        
                    }
                    
                }
                
                else{
                    
                    for(int i=0;i<sideNavList.menuList.count;i++){
                        
                        menuResponse = [[MainMenuResponse alloc] init];
                        
                        NSDictionary *dictRes = sideNavList.menuList[i];
                        
                        [menuResponse setValuesForKeysWithDictionary:dictRes];
                        
                        for(int i=0;i<menuResponse.arMItemList.count;i++){
                            
                            MenuItemList *menuItemList = [[MenuItemList alloc] init];
                            
                            NSDictionary *dictmenu = menuResponse.arMItemList[i];
                            
                            [menuItemList setValuesForKeysWithDictionary:dictmenu];
                            if(![menuItemList.linkTypeName isEqualToString:@"News"]){
                                [hubcitiCategory addObject:menuItemList];
                            }
                            [NewsUtility saveMenuData : menuResponse];
                            
                        }
                       
                    }
                    
                }
                
                
                
            }
            
            
            
        }
        
    }
    
    
    ///added new implementation
    
    tableArray =[[NSMutableArray alloc] init];
    
    tableData = [[NSMutableArray alloc] init];
  
    NSMutableArray *tableCopy =[[NSMutableArray alloc] init];
    for(int i=0;i<newsCategory.count;i++){
        
        [tableCopy addObject:[newsCategory objectAtIndex:i]];
        
    }
    for(int i=0;i<hubcitiCategory.count;i++)
        
    {
        [tableCopy addObject:[hubcitiCategory objectAtIndex:i]];
        
    }
    NSSortDescriptor *aSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES selector:@selector(localizedStandardCompare:)];
    [tableCopy sortUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]];
    for(int i=0;i<tableCopy.count;i++){
        [tableArray addObject:[tableCopy objectAtIndex:i]];
    }
    
        for(int i=0;i<tableArray.count;i++){
            
            if([[tableArray objectAtIndex:i] isKindOfClass:[SideNavigationNewsCat class]]){
                [tableData addObject:[[tableArray objectAtIndex:i] catName]];
            }
            else{
                
                
                
                if([[[tableArray objectAtIndex:i] mItemName] isEqualToString:@"Login/Logout"]){
                    if(userIdentifier == TRUE){
                        [tableData addObject:@"Login/SignUp"];
                    }
                    
                    else{
                        [tableData addObject:@"Logout"];
                       
                    }
                    
                }
                else
                [tableData addObject:[[tableArray objectAtIndex:i] mItemName]];
            }
        }
  
    
   // [tableData addObject:@"News Settings"];
    if(![tableData count])
    {
    if(userIdentifier == TRUE){
        
        [tableData addObject:@"Login/SignUp"];
        
    }
    
    else{
        
        [tableData addObject:@"Logout"];
        
    }
    
    }
    
    NSLog(@"%@",tableData);
    
    [sidemenuTableView setDataSourceDelegate:self];
    [sidemenuTableView setTableViewDelegate:self];
    
    [sidemenuTableView reloadData];
    
    
    
}



-(void) request_getBookMark{
    
    
    
    // [HubCitiAppDelegate showActivityIndicator];
    
    NSLog(@"---->%@",[defaults valueForKey:KEY_HUBCITIID]);
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    //iWebState = BOOKMARKS_GET;
    
    if([defaults valueForKey:KEY_USERID])
        
    {
        
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
        
    }
    
    if([defaults valueForKey:KEY_HUBCITIID])
        
    {
        
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
        
    }
    
    [parameters setValue:@"1" forKey:@"isBkMark"];
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    NSLog(@"%d--->%@",menulevel,[HubCitiAppDelegate getLinkIdArray]);
    if(menulevel>1){
        NSLog(@"%d",menulevel);
        NSString *linkId= [[HubCitiAppDelegate getLinkIdArray]objectAtIndex:menulevel-1];
        [parameters setValue:linkId forKey:@"linkId"];
        NSString *level = [defaults valueForKey:KEY_MENULEVEL];
        [parameters setValue:level forKey:@"level"];
    }
    else{
        
        [parameters setValue:@"0" forKey:@"linkId"];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@band/getnewstopnavigationmenu",BASE_URL];
    
    
    
    NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    [self parse_getBookMark:responseData];
    
    
    
}

-(void) parse_getBookMark:(id) responseObject{
    
    _bookMarkGetResponse =[[BookMarkGetRequest alloc] init];
    
    [_bookMarkGetResponse setValuesForKeysWithDictionary:responseObject];
    
    
    if([_bookMarkGetResponse.responseCode isEqualToString:@"10000"]){
        
        if(_bookMarkGetResponse.bookMarkList){
            
            for(int i=0;i<_bookMarkGetResponse.bookMarkList.count;i++){
                
                BookMarkListDetail *bookMarkList = [[BookMarkListDetail alloc] init];
                
                NSDictionary *dictList = _bookMarkGetResponse.bookMarkList[i];
                
                [bookMarkList setValuesForKeysWithDictionary:dictList];
                
                if(bookMarkList.listCatDetails){
                    
                    for(int j=0;j<bookMarkList.listCatDetails.count;j++){
                        
                        BookMarkListcatDetails *catDetail = [[BookMarkListcatDetails alloc] init];
                        
                        NSDictionary *dictCat= bookMarkList.listCatDetails[j];
                        
                        [catDetail setValuesForKeysWithDictionary:dictCat];
                        
                        [bookMarkArray addObject:[catDetail parCatName]];
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}





-(void) saveMenuData

{
    
    //Navigation bar
    
    //    if(self.menuResponse.titleBkGrdColor)
    //
    //    {
    //
    //        [defaults setValue:self.menuResponse.titleBkGrdColor forKey:@"titleBkGrdColor"];
    //
    //    }
    //
    //    if(self.menuResponse.titleTxtColor)
    //
    //    {
    //
    //        [defaults setValue:self.menuResponse.titleTxtColor forKey:@"titleTxtColor"];
    //
    //    }
    //
    //    if(self.menuResponse.homeImgPath)
    //
    //    {
    //
    //        [defaults setValue:self.menuResponse.homeImgPath forKey:@"homeImgPath"];
    //
    //    }
    //
    //    if(self.menuResponse.bkImgPath)
    //
    //    {
    //
    //        [defaults setValue:self.menuResponse.bkImgPath forKey:@"bkImgPath"];
    //
    //    }
    //
    if (self.menuResponse.downLoadLink){
        
        
        
        [defaults setValue:self.menuResponse.downLoadLink forKey:KEY_HYPERLINK];
        
    }
    
    if (self.menuResponse.androidLink){
        
        
        
        androidDownloadLink = self.menuResponse.androidLink;
        
    }
    
    else
        
        androidDownloadLink = @"";
    
    
    
    [defaults setValue:self.menuResponse.androidLink forKey:@"androidDownloadLink"];
    
    
    
    if (self.menuResponse.appIconImg){
        
        
        
        [defaults setValue:self.menuResponse.appIconImg forKey:KEY_PRODUCTIMGPATH];
        
    }
    
    
    
    if (self.menuResponse.menuName)
        
        [defaults setValue:self.menuResponse.menuName forKey:KEY_MENUNAME];
    
    
    
    [defaults setValue:@"0" forKey:@"noOfColumns"];
    
    if(self.menuResponse.noOfColumns)
        
        [defaults setValue:[NSString stringWithFormat:@"%d", self.menuResponse.noOfColumns]  forKey:@"noOfColumns"];
    
    if(self.menuResponse.retGroupImg){
        
        [defaults setValue:self.menuResponse.retGroupImg forKey:@"retGroupImg"];
        
        
        
        //add the highlighted imagepath
        
        NSString *path = [defaults valueForKey:@"retGroupImg"];
        
        NSString *extn = [path pathExtension]; // This is .png in this case
        
        NSString *finalPath = [[[path stringByDeletingPathExtension] stringByAppendingString:@"_On"] stringByAppendingPathExtension:extn];
        
        [defaults setValue:finalPath forKey:@"retGroupImg_On"];
        
    }
    
    else{
        
        [defaults setValue:nil forKey:@"retGroupImg"];
        
        [defaults setValue:nil forKey:@"retGroupImg_On"];
        
    }
    
    
    
    if(self.menuResponse.departFlag)
        
        [defaults setValue:[NSString stringWithFormat:@"%d", self.menuResponse.departFlag]  forKey:@"departFlag"];
    
    else
        
        [defaults setValue:@"0"  forKey:@"departFlag"];
    
    
    
   
    
    if(self.menuResponse.typeFlag)
        
        [defaults setValue:[NSString stringWithFormat:@"%d", self.menuResponse.typeFlag] forKey:@"typeFlag"];
    
    else
        
        [defaults setValue:@"0"  forKey:@"typeFlag"];
    
    
    
    //if(self.menuResponse.menuId)
        
     //   [defaults setValue:self.menuResponse.menuId forKey:@"menuId"];
    
    if(self.menuResponse.isRegApp)
        
        [defaults setValue:[NSString stringWithFormat:@"%d", self.menuResponse.isRegApp] forKey:RegionApp];
    
    
    
        
    [SharedManager setFiltersCount:self.menuResponse.retAffCount];
    
    [defaults setValue:self.menuResponse.retAffName forKey:@"Title"];
    
    [SharedManager setRetAffId:self.menuResponse.retAffId];
    
    
    
}



//-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

//

//    return VARIABLE_HEIGHT(40);

//}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

//    return 1;

//}

//

//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

//

//    // NSMutableArray *array = [tableArray objectAtIndex:section];

//    return tableData.count;

//}

//

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

//    static NSString *CellIdentifier = @"Cell";

//

//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

//    if (cell == nil) {

//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

//    }

//    if(IPAD)

//        cell.textLabel.font = [UIFont systemFontOfSize:18];

//    else

//        cell.textLabel.font = [UIFont systemFontOfSize:15];

//    cell.textLabel.text = [NSString stringWithFormat:@"%@", [tableData objectAtIndex:indexPath.row]];

//

//    return cell;

//}


-(void)requestCityPrefrences
{
    //iWebRequestState = FetchCityPreferences;
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    if ([SharedManager userInfoFromSignup] == YES)
    {
        [requestStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    }
    else
    {
        [requestStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId><userId>%@</userId>",[defaults valueForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_USERID]];
    }
    [requestStr appendFormat:@"</UserDetails>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getcitypref",BASE_URL];
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    [self parse_FetchCityPreferences:responseXml];
    
    
}
-(void)parse_FetchCityPreferences : (NSString *)response
{
    NSMutableArray*cityIDS = [[NSMutableArray alloc] init];
    NSMutableArray*sortedCitiIDs = [[NSMutableArray alloc] init];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]){
        
        TBXMLElement *cityListElement = [TBXML childElementNamed:@"cityList" parentElement:tbxml.rootXMLElement];
        
        NSLog(@"%@",[TBXML textForElement:tbxml.rootXMLElement]);
        
        NSLog(@"%@",[TBXML textForElement:cityListElement]);
        
        TBXMLElement *cityDetailsElement = [TBXML childElementNamed:@"City" parentElement:cityListElement];
        
        while (cityDetailsElement!=nil) {
            
            TBXMLElement *cityIdElement = [TBXML childElementNamed:@"cityId" parentElement:cityDetailsElement];
            
            if (cityIdElement!=nil) {
                [cityIDS addObject: [TBXML textForElement:cityIdElement]];
                
            }
            
            
            cityDetailsElement = [TBXML nextSiblingNamed:@"City" searchFromElement: cityDetailsElement];
        }
        
        NSMutableString *cities=[[NSMutableString alloc]init];
        NSMutableString *sortCities=[[NSMutableString alloc]init];
        
        for (int i=0; i< [cityIDS count]; i++) {
            
            [cities appendFormat:@"%@,",[cityIDS objectAtIndex:i]];
            [sortedCitiIDs addObject:[cityIDS objectAtIndex:i]];
            
        }
        
        if ([cities hasSuffix:@","]) {
            [cities  setString:[cities substringToIndex:[cities length]-1]];
        }
        
        //changed for server caching
        
        [sortedCitiIDs sortUsingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
            return [str1 compare:str2 options:(NSNumericSearch)];
        }];
        for (int i=0; i<[sortedCitiIDs count]; i++) {
            [sortCities appendFormat:@"%@,",[sortedCitiIDs objectAtIndex:i]];
            
        }
        
        if ([sortCities hasSuffix:@","]) {
            [sortCities  setString:[sortCities substringToIndex:[sortCities length]-1]];
        }
        
        [defaults setObject:sortCities forKey:@"sortedCitiIDs"];
        
        
        
        // call main menu
        [SharedManager setUserInfoFromSignup:NO];
        NSString* customerId = [defaults objectForKey:@"currentuserId"];
        if (customerId) {
            
            if (![customerId isEqualToString:[defaults valueForKey:KEY_USERID]]) {
                
                ReleaseAndNilify(cashedResponse);
                [defaults setValue: nil forKey: @"currentuserId"];
            }
            else {
                isSameUser = true;
            }
        }
        
    }
    else
        
    {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        return;
    }
    
    
}

-(BOOL) authenticate{
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    
    
    [defaults setBool:NO forKey:@"LoginUserNews"];
    
    NSString *guestUser = @"WelcomeScanSeeGuest";
    
    NSString *guestPassword = @":::We@Love!!ScanSee?{People}";
    
    [defaults  setBool:NO forKey:@"rememberMe"];
    
    [defaults  setValue:@"NO" forKey:@"allowingGPS"];
    
    userIdentifier = TRUE;
    
    [xmlStr appendFormat:@"<UserDetails><userName><![CDATA[%@]]></userName>",guestUser];
    [xmlStr appendFormat:@"<password><![CDATA[%@]]></password>", guestPassword];
    [xmlStr appendFormat:@"<appVersion>%@</appVersion>",AppVersion];
    [xmlStr appendFormat:@"<deviceId>%@</deviceId>",[defaults valueForKey:KEY_DEVICEID]];
    [xmlStr appendFormat:@"<hubCitiKey>%@</hubCitiKey><platform>IOS</platform></UserDetails>",HUBCITIKEY];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/login",BASE_URL];
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:xmlStr];
    
    
    
    ReleaseAndNilify(xmlStr);
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseXml])
        
    {
        
        return NO;
        
    }
    
    
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:responseXml];
    
    TBXMLElement *htmlTag = [TBXML childElementNamed:@"head" parentElement:tbXml.rootXMLElement];
    
    if (htmlTag != nil)
        
    {
        
        //            [UtilityManager showAlert:NSLocalizedString(@"Technical Problem",@"Technical Problem") msg:NSLocalizedString(@"please try again later",@"please try again later")];
        
        return NO;
        
    }
    
    
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    
    if([[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"])
        
    {
        
        TBXMLElement *userIdElement = [TBXML childElementNamed:KEY_USERID parentElement:tbXml.rootXMLElement];
        
        TBXMLElement *hubCitiIdElement = [TBXML childElementNamed:KEY_HUBCITIID parentElement:tbXml.rootXMLElement];
        
        TBXMLElement *hubCitiNameElement = [TBXML childElementNamed:KEY_HUBCITINAME parentElement:tbXml.rootXMLElement];
        
        
        
        [defaults  setObject:[TBXML textForElement:userIdElement] forKey:KEY_USERID];
        
        [defaults  setObject:[TBXML textForElement:hubCitiIdElement] forKey:KEY_HUBCITIID];
        
        [defaults  setObject:[TBXML textForElement:hubCitiNameElement] forKey:KEY_HUBCITINAME];
        
        return YES;
        
    }
    
    
    
    else
        
    {
        
        return NO;
        
        
        
    }
    
    
    
}

#pragma mark - UITableViewDelegate




- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation

{
    
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

- (BOOL)shouldAutorotate{
    
    return NO;
    
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    
    return UIInterfaceOrientationPortrait;
    
}



- (UIInterfaceOrientationMask)supportedInterfaceOrientations



{
    
    return UIInterfaceOrientationMaskPortrait;
    
}



-(void) pushViewController:(id) viewpushController{
    
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
        [self.menuContainerViewController.centerViewController pushViewController:viewpushController animated:NO];
   
//    if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Combination News Template"]||[[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"News Tile Template"]||[[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Scrolling News Template"]){
//        int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
//        if(menulevel>1 && [viewpushController isKindOfClass:[MainMenuViewController class] ]) {
            SideViewController *leftMenuViewController;
            if(IPAD){
                leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
            }
            else{
                leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
                
            }
            leftMenuViewController.isNewsTemplate = FALSE;
            [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
//        }
//    }

}



-(void)callSubClassAccordingToSelection:(int)Selectedtag

{
    
    //[defaults setValue:@"10091" forKey:KEY_USERID];
    
    
    
    fromBack = YES;
    
    //    [defaults setValue:nil forKey:KEY_MAINMENUID];
    
    [defaults setValue:nil forKey:KEY_BOTTOMBUTTONID];
    
    [defaults setValue:nil forKey:KEY_MITEMID];
    
    [defaults setValue:nil forKey:KEY_MITEMNAME];
    
    
    
    MenuItemList* obj_mainMenuDO = [tableArray objectAtIndex:Selectedtag];
    
    // NSDictionary* dictList = menuResponse.arMItemList[Selectedtag];
    
    // [obj_mainMenuDO setValuesForKeysWithDictionary:dictList];
    
    
    
    
    
    [defaults setValue:[NSString stringWithFormat:@"%d", obj_mainMenuDO.mItemId] forKey:KEY_MITEMID];
    
    [defaults setValue:obj_mainMenuDO.linkId forKey:KEY_LINKID];
    
    if ([defaults valueForKey:KEY_LINKID]!=NULL) {
        
        
        
        [linkID addObject:[defaults valueForKey:KEY_LINKID]];
        
    }
    
    
    
    [defaults setValue:obj_mainMenuDO.mItemName forKey:KEY_MITEMNAME];
    
    DLog(@"%@",[defaults valueForKey:KEY_MITEMNAME]);
    
    if([obj_mainMenuDO.linkTypeName isEqualToString:@"GovQA"]){
        
        
        
        BOOL isRemember = [defaults boolForKey:@"govQARememberMe"];
        
        DLog(@"session Id = %@", [SharedManager sessionId]);
        
        if ([SharedManager sessionId]) {
            
            if(isRemember){
                
                
                
                ReportProblemViewController *reportObj = [[ReportProblemViewController alloc]initWithNibName:@"ReportProblemViewController" bundle:[NSBundle mainBundle]] ;
                
                // reportObj.arrBottomBtnDO = arrMenuBottomButtonDO;
                
                [self pushViewController:reportObj];
                
            }
            
            else
                
            {
                
                ReportProblemViewController *reportObj = [[ReportProblemViewController alloc]initWithNibName:@"ReportProblemViewController" bundle:[NSBundle mainBundle]] ;
                
                //reportObj.arrBottomBtnDO = arrMenuBottomButtonDO;
                
                [self pushViewController:reportObj];
                
                
                
            }
            
            
            
        }
        
        else
            
        {
            
            GovQALoginPageViewController *svc = [[GovQALoginPageViewController alloc]initWithNibName:@"GovQALoginPageViewController" bundle:[NSBundle mainBundle]] ;
            
            // svc.arrBottomButtonDO=arrMenuBottomButtonDO;
            
            [self pushViewController:svc];
            
        }
        
    }
    
    else if([obj_mainMenuDO.linkTypeName isEqualToString:@"Band"]){
        
        
        
        BandViewController *dvc = [[BandViewController alloc]initWithNibName:@"BandViewController" bundle:[NSBundle mainBundle]];
        
        self.bandViewPage = dvc;
        NSLog(@"BandMenuName %@", [defaults valueForKey:KEY_MITEMNAME]);
        dvc.bandPageName = [defaults valueForKey:KEY_MITEMNAME];
        
        [self pushViewController:dvc];
        
        
        
    }
    
    else if ([obj_mainMenuDO.linkTypeName rangeOfString:@"FindSingleCategory"].location != NSNotFound){ //means find - single category
        
        
        
        NSArray *nameArray = [obj_mainMenuDO.linkTypeName componentsSeparatedByString:@"-"];
        
        if ([nameArray count]>1){
            
            
            
            //save the category name and navigate to dining screen
            
            SingleCatRetailers *singleCat = [[SingleCatRetailers alloc]initWithNibName:@"SingleCatRetailers" bundle:nil];
             [defaults setValue:[NSString stringWithFormat:@"%d", obj_mainMenuDO.mItemId] forKey:@"findSingCatMitemid"];
            singleCat.categoryName = [[nameArray objectAtIndex:1]copy];
            
            singleCat.catId = [[defaults valueForKey:KEY_LINKID]copy];
            
            [self pushViewController:singleCat];
            
            ReleaseAndNilify(singleCat);
            
            
        }
        
    }
    
    
    
    else if ([obj_mainMenuDO.linkTypeName rangeOfString:@"EventSingleCategory"].location != NSNotFound){ //means event - single category
        
        
        
        NSArray *nameArray = [obj_mainMenuDO.linkTypeName componentsSeparatedByString:@"-"];
        
        if ([nameArray count]>1){
            
            if (SingleEventCatID==Nil) {
                
                SingleEventCatID=[[NSMutableArray alloc]init];
                
                SingleCatFlag = TRUE;
                
                SingleEventFromMainMenu = TRUE;
                
                //[SingleEventCatID addObject:obj_mainMenuDO.linkId];
                
                [SingleEventCatID addObject:[NSString stringWithFormat:@"%ld",(long)obj_mainMenuDO.linkId]];
                
            }
            
            else
                
            {
                
                SingleCatFlag = TRUE;
                
                SingleEventFromMainMenu = TRUE;
                
                //[SingleEventCatID addObject:obj_mainMenuDO.linkId];
                
                [SingleEventCatID addObject:[NSString stringWithFormat:@"%ld",(long)obj_mainMenuDO.linkId]];
                
            }
            
            
            
            //save the category name and navigate to Event single category screen
            
            EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
            
            self.iEventsListViewController = iEventsListViewController;
            
            iEventsListViewController.categoryName = [[nameArray objectAtIndex:1]copy];
            
            [self pushViewController:iEventsListViewController];
            
            
            
            //[iEventsListViewController release];;
            
            
            
        }
        
        
        
    }
    
    
    
    else{ //other menu types
        
        
        
        switch ([[dict_linkTypeName_MenuButton objectForKey:obj_mainMenuDO.linkTypeName]intValue]){
                
                
                
            case 0:{ //city experience
                
                
                
                
                
                CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                
                [defaults setInteger:100 forKey:@"CityExpTableTag"];
                
                self.cityExp = cevc;
                
                [SharedManager setRefreshAustinRetailers:YES];
                
                [self pushViewController:cevc];             //[cevc release];
                
                
                
            }
                
                break;
                
                
                
            case 1: //coupon
                
                [self navigateToCouponsViewController];
                
                break;
                
                
                
            case 2: //appsite
                
                [self request_appsitedetails];
                
                break;
                
                
                
            case 3: //anything page
            {
                requestAnythingFromMainMenu=TRUE;
                anythingShareFlag=YES;
                [self request_HubcitiAnythingInfo];
            }
                //[UtilityManager req_hubcitiAnyThingPage:self];
                
                break;
                
                
                
            case 4:{//alerts
                
                AlertsList *alert = [[AlertsList alloc]initWithNibName:@"AlertsList" bundle:nil];
                
                [self pushViewController:alert];                //[alert release];
                
                
                
            }
                
                break;
                
                
                
            case 5:{//deals
                
                
                
                CouponsInDealViewController *dvc = [[CouponsInDealViewController alloc]initWithNibName:@"CouponsInDealViewController" bundle:[NSBundle mainBundle]];
                
                [self pushViewController:dvc];                     //[dvc release];
                
                
                
                
                
                
                
                
                
            }
                
                break;
                
                
                
            case 6://find
            {
               [defaults setValue:@"subMenu"  forKey:KEY_BOTTOMBUTTONNAME];
                
                [defaults setValue:@"true" forKey:@"findFlag"];
                
                [defaults setValue:[NSString stringWithFormat:@"%d", obj_mainMenuDO.mItemId] forKey:@"findCatMitemid"];
                
                menuItemName=obj_mainMenuDO.mItemName;
                
                [self navigateToFindView];
                
               
            }
                
                break;
                
                
                
            case 7://scan now
                
                [self navigateToScanNow];
                
                break;
                
                
                
            case 8://whats nearby
                
                [defaults setValue:@"distance" forKey:@"NearByRetDistance"];
                
                [self navigateToWhatsNearBy];
                
                break;
                
                
                
            case 9:{//submenu
                
                
                
                //Added for navigation between menu levels
                
                NSMutableArray *array = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getLinkIdArray]];
                
                [array addObject:[NSString stringWithFormat:@"%@",obj_mainMenuDO.linkId]];
                
                [HubCitiAppDelegate refreshLinkIdArray];
                
                [[HubCitiAppDelegate getLinkIdArray]addObjectsFromArray:array];
                
                
                
                
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getDeptIdArray]];
                
                [array1 addObject:@"0"];
                
                [HubCitiAppDelegate refreshDeptIdArray];
                
                [[HubCitiAppDelegate getDeptIdArray]addObjectsFromArray:array1];
                
                
                
                NSMutableArray *array2 = [NSMutableArray arrayWithArray:[HubCitiAppDelegate getTypeIdArray]];
                
                [array2 addObject:@"0"];
                
                [HubCitiAppDelegate refreshTypeIdArray];
                
                [[HubCitiAppDelegate getTypeIdArray]addObjectsFromArray:array2];
                
                
                
                
                
                int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
                
                NSString *levelString = [NSString stringWithFormat:@"%d", menulevel+1];
                
                [defaults setValue:levelString forKey:KEY_MENULEVEL];
                
                
                
                
                
                
                
                [self navigateToSubMenu];
                
            }
                
                break;
                
                
                
            case 10://events
                
                //[self navigateToEventList];
                
            {
                
                EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
                
                self.iEventsListViewController = iEventsListViewController;
                
                if([obj_mainMenuDO.linkTypeName isEqualToString:@"Band Events"])
                    
                    iEventsListViewController.bandEvent = [NSNumber numberWithInt:1];
                
                
                
                [self pushViewController:iEventsListViewController];
                
            }
                
                break;
                
                
                
            case 11://preference
                
                if (userIdentifier==TRUE) {
                    
                    [NewsUtility signupPopUp:self];
                }
                
                else
                    
                {
                    
                    [self requestToGetPreferredLocations];
                    
                }
                
                break;
                
                
                
            case 12:{//about
                
                AboutAndPrivacyScreen *aboutScreen = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                
                aboutScreen.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/About_Us.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];                
                [defaults  setObject:@"About" forKey:@"AboutPrivacyTitle"];
                aboutScreen.comingFromPrivacyScreen = NO;
                
                [self pushViewController:aboutScreen];
                
                //[aboutScreen release];
                
            }
                
                break;
                
                
                
            case 13:if (userIdentifier==TRUE) {
                
                [NewsUtility signupPopUp:self];
                
            }
                
            else
                
            {//settings
                
                SettingsViewController *svc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
                
                if(self.isNewsTemplate)
                {
                    svc.fromNewsSideSetting = TRUE;
                }
                else
                {
                    svc.fromNewsSideSetting = FALSE;
                }
                [self pushViewController:svc];
                
                //[svc release];
                
            }
                
                break;
                
                
                
            case 14://share
                
                if (userIdentifier==TRUE) {
                    
                    [NewsUtility signupPopUp:self];
                    
                }
                
                else
                    
                {
                   
                    [self shareClicked:selectedCell];
                
                }
                
                break;
                
                
                
            case 15:{//filters
                
                
                
                /*if ([SharedManager filtersCount] == 1){ //navigate to FilterRetailersList - only 1 filter
                    
                    
                    
                    
                    
                    CityExperienceViewController *cevc = [[CityExperienceViewController alloc]initWithNibName:@"CityExperienceViewController" bundle:[NSBundle mainBundle]];
                    
                    self.cityExp = cevc;
                    
                    [SharedManager setRefreshAustinRetailers:YES];
                    
                    //[cevc release];
                    
                    
                    
                    FilterRetailersList *pvc = [[FilterRetailersList alloc]initWithNibName:@"FilterRetailersList" bundle:[NSBundle mainBundle]];
                    
                    self.filters = pvc;
                    
                    pvc.isOnlyOnePartner = YES;
                    
                    [self pushViewController:pvc];              //[pvc release];
                    
                }
                
                
                
                else if ([SharedManager filtersCount] >1){ //navigate to city experience - multiple filters
                    
                    
                    
                    FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                    
                    self.filterList = filterList;
                    
                    // [defaults setValue:retAffName forKey:@"Title"];
                    
                    [self pushViewController:filterList];   //[filterList release];
                    
                }*/
                
                FilterListViewController *filterList = [[FilterListViewController alloc]initWithNibName:@"FilterListViewController" bundle:[NSBundle mainBundle]];
                
                self.filterList = filterList;
             
                
                [self pushViewController:filterList];
                
            }
                
                break;
                
                
                
            case 16:
                
                
                
            {
                
                FAQCategoryList *faqCatList=[[FAQCategoryList alloc]initWithNibName:@"FAQCategoryList" bundle:[NSBundle mainBundle]];
                
                [self pushViewController:faqCatList];
                
                
                
                self.faqList = faqCatList;
                
                //[faqCatList release];
                
                
                
                
                
            }break;
                
                
                
            case 17:
                
                
                
            {
                
                FundraiserListViewController *iFundraiserListViewController = [[FundraiserListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
                
                self.iFundraiserListViewController = iFundraiserListViewController;
                
                [self pushViewController:iFundraiserListViewController];                //[iFundraiserListViewController release];
                
                
                
            }break;
                
            case 18:
                
                
                
            {            if (userIdentifier==TRUE) {
                
                [NewsUtility signupPopUp:self];
            }
                
            else{
                
                iWebRequestState = GETUSERINFO;;
                
                [self requestToGetUserData];
                
            }
                
            }break;
                
            case 19:
                
                
                
            {
                
                if (userIdentifier==TRUE) {
                    
                    [NewsUtility signupPopUp:self];
                }
                
                else{
                    
                    iWebRequestState = GETFAVLOCATIONS;
                    
                    [self requestToGetPreferredLocations];
                    
                    
                    
                }
                
            }break;
                
                
                
            case 20:
                
                
                
            {
                
                if (userIdentifier==TRUE) {
                    
                    [NewsUtility signupPopUp:self];
                    
                }
                
                else{
                    
                    if([Network currentReachabilityStatus]==0)
                        
                    {
                        [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                        
                        
                    }
                    
                    else{
                        
                        
                        
                        UserSettingsController *inform = [[UserSettingsController alloc] initWithNibName:@"UserSettingsController" bundle:[NSBundle mainBundle]];
                        
                        [self pushViewController:inform];                        //[inform release];
                        
                    }
                    
                }
                
                
                
                
                
            }break;
                
            case 21:
                
                
                
            {
                
                if ([RegionApp isEqualToString:@"1"]) {
                    
                    if (userIdentifier==TRUE) {
                        
                        [NewsUtility signupPopUp:self];
                        
                    }
                    
                    else
                        
                    {
                        
                        if([Network currentReachabilityStatus]==0)
                            
                        {
                            [UtilityManager showAlert:NSLocalizedString(@"Error!",@"Error!") msg:NSLocalizedString(@"Network is not available",@"Network is not available")];
                            
                            
                        }
                        
                        else{
                            
                            [SharedManager setUserInfoFromSignup:NO];
                            
                            CityPreferenceViewController *citPref = [[CityPreferenceViewController alloc]initWithNibName:@"CityPreferenceViewController" bundle:nil];
                            if(self.isNewsTemplate){
                                citPref.isNewsSideCityPref = TRUE;
                            }
                            else
                            {
                                citPref.isNewsSideCityPref = FALSE;
                            }
                            [self pushViewController:citPref];
                            
                            //    [citPref release];
                            
                        }
                        
                    }
                    
                }
                
                
                
            }break;
                
            case 22:
                break;
            case 23:
            {
                BandsNearByEventsMapTable *eventList = [[BandsNearByEventsMapTable alloc] initWithNibName:@"BandsNearByEventsMapTable" bundle:nil];
                self.todayEventList = eventList;
                eventList.fromTodayEvents = TRUE;
                [self pushViewController:eventList];
                
            }
                break;
            case 24 :
            {
                CouponsMyAccountsViewController *dvc = [[CouponsMyAccountsViewController alloc]initWithNibName:@"CouponsMyAccountsViewController" bundle:[NSBundle mainBundle]];
                [self pushViewController:dvc];
            }
                break;
            case 25:
            {
                AboutAndPrivacyScreen *privacy = [[AboutAndPrivacyScreen alloc] initWithNibName:@"AboutAndPrivacyScreen" bundle:nil];
                privacy.responseXml =[NSMutableString stringWithFormat:@"%@/Images/hubciti/html/%@/Privacy_Policy.html", [defaults valueForKey:@"DomainName"],[defaults valueForKey:KEY_HUBCITIID]];
              //  privacy.responseXml = [NSMutableString stringWithFormat:@"%@%@/Privacy_Policy.html",HTML_URL,[defaults valueForKey:KEY_HUBCITIID]];
                [defaults  setObject:@"Privacy" forKey:@"AboutPrivacyTitle"];
                privacy.comingFromPrivacyScreen = YES;
                [self pushViewController:privacy];
            }
                break;

            default:
                
                break;
                
        }
        
    }
    
}





#pragma mark action sheet methods

-(void)shareClicked:(id)sender{
    
    
    
    if (![defaults valueForKey:KEY_HYPERLINK] || [[defaults valueForKey:KEY_HYPERLINK] isEqualToString:@"N/A"])
        
        return;
    
    
    
    [defaults setValue:@"Please download the HubCiti App from below links:" forKey:KEY_SHAREMSG];
    [self showActionSheet:selectedCell];
    
}
-(void) showActionSheet :(id)sender{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share Applink Via" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        [self faceBookCliked];
    }];
    
    UIAlertAction *twitter = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        [self twitterClicked];
    }];
    
    
    UIAlertAction *text = [UIAlertAction actionWithTitle:@"Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        [self textClicked];
    }];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        [self emailClick];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:faceBook];
    [actionSheet addAction:twitter];
    [actionSheet addAction:text];
    [actionSheet addAction:email];
    [actionSheet addAction:cancel];
    if(IPAD){
        
        actionSheet.popoverPresentationController.sourceView = selectedCell;
        actionSheet.popoverPresentationController.sourceRect = selectedCell.bounds;
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:actionSheet animated:YES completion:nil];
    });
    
    
    
}

-(void) faceBookCliked {//facebook
    _anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
    _anyVC.fromSideMenu = TRUE;
    //[self.view addSubview:anyVC.view];
    [UIView transitionWithView:[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^ { [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:_anyVC.view]; }
                    completion:nil];
}

-(void) twitterClicked{//twitter
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                DLog(@"Twitter Result: canceled");
            } else {
                DLog(@"Twitter Result: sent");
            }
            
            if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                
                [controller.view removeFromSuperview];
            } else
                [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
        
        //Adding the URL to the facebook post value from iOS
        
        //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
        
        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
            [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:controller.view];
            [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] bringSubviewToFront:controller.view];
        }else
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:controller animated:YES completion:Nil];
    }
    else {
        [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
       
    }
}


-(void) textClicked{//text
    
    if ([self hasCellularCoverage])
    {
        if ([MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
            [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
            smsComposerController.messageComposeDelegate = self;
            [self presentViewController:smsComposerController animated:YES completion:nil];
        }
    }
    else
    {
        [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];
       
    }
    
    
}

-(void) emailClick{//email
    __typeof(self) __weak  obj = self;
    _emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
    _emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
        
        [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
    } ;
    
    [_emailSendingVC loadMail];
    
}


-(BOOL)hasCellularCoverage

{
    
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    
    
    
    if (!carrier.isoCountryCode) {
        
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        
        return NO;
        
    }
    
    return YES;
    
}



- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result

{
    
    switch (result) {
            
        case MessageComposeResultCancelled:
            
            
            
            break;
            
            
            
        case MessageComposeResultSent:{
            [UtilityManager showAlert:nil msg:@"Message Sent"];
            
            
        }
            
            break;
            
        case MessageComposeResultFailed:{
            [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
           
            
        }
            
            break;
            
            
            
        default:
            
            break;
            
    }
    
    
    
    [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
    
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    
}







-(void)requestToGetPreferredLocations

{
    
    iWebRequestState= GETFAVLOCATIONS;
    
    if([Network currentReachabilityStatus]==0)
        
    {
        
        [common showAlert];
        
    }
    
    else {
        
        NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@firstuse/getuserloccat?userId=%@&hubCitiId=%@",BASE_URL,[defaults  objectForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
        
        
        
        
        
        [ConnectionManager establishConnectionForGet:urlString withDelegate:self];
        
    }
    
}





-(void)request_HubcitiAnythingInfo{
   
    requestAnythingFromMainMenu=TRUE;    
    anythingShareFlag=YES;

    NSMutableDictionary* param = [[NSMutableDictionary alloc] init];
    
    iWebRequestState = HubcitiAnythingInfo;
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/gethubcitianythinginfojson",BASE_URL];
    NSLog(@"url = %@", urlString);
    [param setValue:[linkID objectAtIndex:[linkID count]-1] forKey:@"pageId"];
    [defaults setObject:[linkID objectAtIndex:[linkID count]-1]forKey:KEY_ANYPAGEID];
    
    [param setValue:@"IOS" forKey:@"platform"];
    [param setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    
    if ([defaults valueForKey:KEY_MITEMID])
        [param setValue:[defaults valueForKey:KEY_MITEMID] forKey:@"mItemId"];
    if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        [param setValue:[defaults valueForKey:KEY_BOTTOMBUTTONID] forKey:@"bottomBtnId"];
    
    
    [HubCitiAppDelegate showActivityIndicator];
    NSLog(@"param = %@", param);
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    [client sendRequest:param :urlString];

    
    
}



-(void)request_appsitedetails{
    
    
    
    iWebRequestState = appsitedetails;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@thislocation/appsitedetails",BASE_URL];
    
    NSMutableString *reqStr = [[NSMutableString alloc]initWithFormat:@"<RetailerDetail><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    
    
    
    if ([defaults valueForKey:KEY_MITEMID])
        
        [reqStr appendFormat:@"<mItemId>%@</mItemId><platform>IOS</platform>",[defaults valueForKey:KEY_MITEMID]];
    
    
    
    else if ([defaults valueForKey:KEY_BOTTOMBUTTONID])
        
        [reqStr appendFormat:@"<bottomBtnId>%@</bottomBtnId><platform>IOS</platform>",[defaults valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    
    if (!([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [SharedManager gps_allow_flag] == YES && [[defaults  valueForKey:KEY_LATITUDE]length]){
        
        
        
        [reqStr appendFormat:@"<longitude>%@</longitude><latitude>%@</latitude>",[defaults  valueForKey:KEY_LONGITUDE],
         
         [defaults  valueForKey:KEY_LATITUDE]];
        
        
        
    }
    
    else if ([[defaults  valueForKey:KEYZIPCODE]length]){
        
        
        
        [reqStr appendFormat:@"<postalCode><![CDATA[%@]]></postalCode>", [defaults  valueForKey:KEYZIPCODE]];
        
    }
    
    
    
    [reqStr appendFormat:@"<linkId>%@</linkId><hubCitiId>%@</hubCitiId></RetailerDetail>",[linkID objectAtIndex:[linkID count]-1],[defaults valueForKey:KEY_HUBCITIID]];
    
    [ConnectionManager establishConnectionFor:reqStr base:urlStr withDelegate:self];
    
}



-(void)presentSpecialOffersAfterDismissAleert {
    if (self.isViewLoaded && self.view.window != nil)
    {
        SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
        splOfferVC.isAppsiteLogisticsFlag = 1;
         if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
        [HubCitiAppDelegate setIsLogistics:YES];
        [self presentViewController:splOfferVC animated:YES completion:nil];
    }
    
}


#pragma mark textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField.tag == 31){
        NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        if (range.length == 1){
            NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [zipSave setEnabled:(finalString.length >= 5)];
            return YES;
        }
        else {
            for (int j = 0; j<[string length]; j++) {
                unichar c = [string characterAtIndex:j];
                if ([charSet characterIsMember:c]) {
                    NSUInteger textLength = [textField.text length] + [string length] - range.length;
                    [zipSave setEnabled:(textLength >= 5)];
                    return (textLength > 5)? NO : YES;
                }
                else {
                    return NO;
                }
            }
        }
        
    }
    return YES;
}

//Alert that shows a Zip Enter Box

-(void)showZipEnter
{
    
    // use UIAlertController
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Please enter your zip code"
                               message:nil
                               preferredStyle:UIAlertControllerStyleAlert];
    
    zipSave = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action){
                                         //Do Some action here
                                         UITextField *textField = alert.textFields[0];
                                         if ([textField.text length])
                                             [self postZip:textField.text];
                                         else
                                             return;
                                         
                                     }];
    zipSave.enabled = NO;
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       NSLog(@"cancel btn");
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       [defaults setValue:nil forKey:KEYZIPCODE];
                                                       
                                                       [UtilityManager showAlert:nil msg:@"You have chosen not to search by your Zipcode. If you would like to search by Zipcode in the future, please go to the User Information screen on the bottom toolbar of the homepage."];
                                                       
                                                       
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:zipSave];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        [textField setTag:31];
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.font = [UIFont systemFontOfSize:16];
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}







#pragma mark parse methods

-(void)responseData:(NSString *) response

{
    
    switch (iWebRequestState)
    
    {
            
            
            
        case GETUSERINFO:
            
        {
            
            [self parseGetUserData:response];
            
        }
            
            break;
            
            
            
        case GETFAVLOCATIONS:
            
            [self parse_GetUserCat:response];
            
            break;
            
            
            
        case HubcitiAnythingInfo:
            
            [self parse_HubcitiAnythingInfo:response];
            
            break;
            
            
            
        case appsitedetails:
            
            [self parse_appsitedetails:response];
            
            break;
            
            //        case catsearch:
            
            //            [self parse_sscatsearch:response];
            
            //            break;
            
            
            
        default:
            
            break;
            
    }
    
}

//parse user favorite categories

-(void)parse_GetUserCat:(NSString*)response{
    
    
    
    if ([UtilityManager isNullOrEmptyString:response]){
        [UtilityManager showAlert:@"Error!" msg:@"Please try after sometime"];
        
        return;
        
    }
    
    
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response] ;
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    
    
    if (responseCodeElement != nil && [[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {//means we have got proper response
        
        
        
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        
        PreferredCategoriesScreen *settings = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
        
        [self pushViewController:settings];        //[settings release];
        
        
        
    }
    
    
    
    else {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
        
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
        
        
    }
    
    ReleaseAndNilify(tbxml);
    
}



-(void)parse_HubcitiAnythingInfo:(id)responseData{
    
    
    DLog(@"%@",responseData);
    
    NSInteger responseCode=[[responseData objectForKey:RESPONSECODE] integerValue];
    
    NSString *responseText=[responseData objectForKey:RESPONSETEXT];
    
    if (responseCode != 10000) {
        [UtilityManager showAlert:nil msg:responseText];
        return;
    }
    else
    {
        if (![[responseData objectForKey:@"mainMenuId"] isEqual:nil])
        {
        [defaults  setValue:[responseData objectForKey:@"mainMenuId"] forKey:KEY_MAINMENUID];
        }
        
        if (![[responseData objectForKey:@"pageLink"] isEqualToString:@"N/A"])
        {
            NSURL *url = [NSURL URLWithString:(NSString*)[responseData objectForKey:@"pageLink"]];
            [defaults setValue:[responseData objectForKey:@"pageLink"] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            if ([[url absoluteString] containsString:@"itunes.apple.com"]) {
                [UtilityManager customUrlLoad:url];
            }
            else if([[url absoluteString ]containsString:@"3000.htm"]){
                
                BOOL lsFlag = [HubCitiAppDelegate locationServicesOn];
                if (!lsFlag) {
                    
                    UIAlertController * alert;
                    
                    alert=[UIAlertController alertControllerWithTitle:@"Event Logistics uses your iPhone's Location Services to locate you on the map. Please turn on Location Services in iPhone Settings" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                           [alert dismissViewControllerAnimated:NO completion:nil];             [self performSelector:@selector(presentSpecialOffersAfterDismissAleert) withObject:nil afterDelay:0.75];
                                         }];
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
                else{
                    SpecialOffersViewController *splOfferVC =[[SpecialOffersViewController alloc]init];
                    
                    splOfferVC.isAppsiteLogisticsFlag = 1;
                     if (![[defaults valueForKey:KEY_URL] containsString:@"Portrait"])
                    [HubCitiAppDelegate setIsLogistics:YES];
                    
                    [self presentViewController:splOfferVC animated:YES completion:nil];
                }
                
                
            }
            else{
                [defaults setValue:[responseData objectForKey:@"pageLink"] forKey:KEY_URL];
                [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
                
                WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
                urlDetail.comingFromSideMenu = TRUE;
                [self pushViewController:urlDetail];
                
            }
            
        }
        
        else if ([[responseData objectForKey:@"mediaPath"] isEqualToString:@"N/A"]){//means its MYO anything page
            
            [defaults setValue:responseData forKey:KEY_RESPONSEXML];
            
              AnythingPage *anythingPage = [[AnythingPage alloc] initWithNibName:@"AnythingPage" bundle:[NSBundle mainBundle]];
            [self pushViewController:anythingPage];
            
        }
        
        else{//means its one among audio, video, image and pdf
            
            [defaults setValue:[responseData objectForKey:@"mediaPath"] forKey:KEY_URL];
            [defaults setValue:@"Mainmenu" forKey:@"rightbarbuttontitle"];
            
            WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
            urlDetail.comingFromSideMenu = TRUE;
            [self pushViewController:urlDetail];
        }
    }
    [linkID removeLastObject];

}



-(void)parse_appsitedetails:(NSString*)response{
    
    
    
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]intValue]!=10000) {
        
        
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
       
        
        return;
        
    }
    
    else {
        
        TBXMLElement *retailerDetailElement = [TBXML childElementNamed:@"retailerDetail" parentElement:tbXml.rootXMLElement];
        
        TBXMLElement *RetailerDetailElement = [TBXML childElementNamed:@"RetailerDetail" parentElement:retailerDetailElement];
        
        TBXMLElement *distanceElement = [TBXML childElementNamed:@"distance" parentElement:RetailerDetailElement];
        
        [defaults  setObject:response forKey:KEY_RESPONSEXML] ;
        
        RetailerSummaryViewController *rsvc = [[RetailerSummaryViewController alloc]initWithNibName:@"RetailerSummaryViewController" bundle:[NSBundle mainBundle]];
        
        rsvc.distanceFromPreviousScreen = [TBXML textForElement:distanceElement];
        
        [SharedManager setRefreshAustinRetailers:NO];
        
        [self pushViewController:rsvc];
        
        //[rsvc release];
        
    }
    
}





-(void)requestToGetUserData

{
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    if ([defaults  valueForKey:KEY_USERID]) {
        
        [parameters setValue:[defaults  valueForKey:KEY_USERID] forKey:@"userId"];
        
    }
    
    if ([defaults valueForKey:KEY_HUBCITIID]) {
        
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
        
    }
    
    DLog(@"parameter: %@",parameters);
    
    
    
    HTTPClient *client = [HTTPClient sharedHTTPClient];
    
    client.delegate = self;
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getuserinfo_json",BASE_URL];
    
    DLog(@"Url: %@",urlString);
    
    [client sendRequest : parameters : urlString];
    
    
    
   
    
}

-(void)parseGetUserData:(id)response

{
    
    
    
    
    
    if (response == nil)
        
        return;
    
    if (infoResponse == nil) {
        
        infoResponse = [[GetUserInfoResponse alloc] init];
        
    }
    
    
    
    
    
    [infoResponse setValuesForKeysWithDictionary:response];
    
    
    
    NSLog(@"response = %@", infoResponse);
    
    if ([infoResponse.responseCode isEqualToString:@"10000"] ){
        
        [SharedManager setUserInfoFromSignup:NO];
        
        [defaults  setObject:response forKey:KEY_RESPONSEXML];
        
        UserInformationViewController *inform = [[UserInformationViewController alloc] initWithNibName:@"UserInformationViewController" bundle:[NSBundle mainBundle]];
        
        [self pushViewController:inform];
        
        
        
        //[inform release];
        
    }
    
    else{
        
        NSString *responseTextStr = infoResponse.responseText;
        
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:responseTextStr];
        
        
        
    }
    
    
    
    
    
}





#pragma mark navigate to viewcontroller



-(void)navigateToCouponsViewController

{
    
    [defaults setBool:YES forKey:@"myGallerysplash"];
    
    [defaults setBool:YES forKey:@"CalledFromMainMenu"];
    
    CouponsViewController *iCouponsViewController = [[CouponsViewController alloc]initWithNibName:@"CouponsViewController" bundle:[NSBundle mainBundle]];
    
    [self pushViewController:iCouponsViewController];
    
    
    
    //[iCouponsViewController release];
    
}



-(void) navigateToWhatsNearBy

{
    
    self.navigationController.navigationBarHidden = NO;
    
    navigatedFromMainMenu=TRUE;
    
    BOOL gpsFlag = [HubCitiAppDelegate locationServicesOn];
    
    int zipCodeStatus = [HubCitiAppDelegate checkZipAndLocation];
    
    
    
    
    
    // If application level "GPS ON" but Device level "GPS off" .. Show alert to user to turn "GPS ON" at device setting
    
    if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) && [[defaults  valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
        
    {
        [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
        
    }
    
    else
        
    {
        
        // If GPS Not allowed but ZIP code is available
        
        if(gpsFlag)
            
        {
            
            [defaults setBool:YES forKey:@"gpsEnabled"];
            
            RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
            
            self.retailerListScreen = retailerListScreen;
            
            [self pushViewController:retailerListScreen];
            
            
            
            //[retailerListScreen release];
            
            
            
        }
        
        else
            
        {
            
            if(zipCodeStatus == 1)
                
                [defaults setValue:[defaults valueForKey:KEYZIPCODE] forKey:KEYZIPCODE_TL];
            
            else
                
                [defaults setValue:nil forKey:KEYZIPCODE_TL];
            
            
            
            int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
            
            if(menulevel > 1)
                
                [defaults  setBool:NO forKey:@"showBackButton"];
            
            else
                
                [defaults  setBool:YES forKey:@"showBackButton"];
            
            
            
            if (userIdentifier==TRUE) {
                
                RetailersListViewController *retailerListScreen = [[RetailersListViewController alloc]initWithNibName:@"RetailersListViewController" bundle:[NSBundle mainBundle]];
                
                self.retailerListScreen = retailerListScreen;
                
                [self pushViewController:retailerListScreen];
                
                //[retailerListScreen release];
                
            }
            
            else
                
            {
                
                LocationDetailsViewController *locationDetailsScreen = [[LocationDetailsViewController alloc]initWithNibName:@"LocationDetailsViewController" bundle:[NSBundle mainBundle]];
                
                [self pushViewController:locationDetailsScreen];
                
                //[locationDetailsScreen release];
                
            }
            
        }
        
    }
    
}





-(void)navigateToEventList

{
    
    
    
    EventsListViewController *iEventsListViewController = [[EventsListViewController alloc]initWithNibName:@"EventsListViewController" bundle:[NSBundle mainBundle]];
    
    self.iEventsListViewController = iEventsListViewController;
    
    [self pushViewController:iEventsListViewController];
    
    
    
    //[iEventsListViewController release];;
    
}











-(void) navigateToScanNow

{
    
    [defaults setBool:YES forKey:@"KEY_SCANSPLASH"];
    
    [defaults setValue:@"scanNow" forKey:KEY_SEARCHFROM];
    
    [defaults setObject:@"YES" forKey:@"KEY_SCANNERCALLED"];
    
    
    
    int menulevel = [[defaults valueForKey:KEY_MENULEVEL] intValue];
    
    if(menulevel > 1)
        
        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
    
    else
        
        [defaults  setBool:YES forKey:@"DisplayBackBarButton"];
    
    
    
    
    
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    ScanNowScreen *viewScanNowScreen = [[ScanNowScreen alloc] init];
    
    [self pushViewController:viewScanNowScreen];
    
    //[viewScanNowScreen release];
    
    
    
}





//Call CityExperiance Servie and Class

-(void) navigateToSubMenu

{
    
    
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    [cusNav hideBackButton:YES];
    [cusNav hideHambergerButton:YES];
    
    
    MainMenuViewController  *iMainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController"                                                            bundle:[NSBundle mainBundle]];
    
    [self pushViewController:iMainMenuViewController];
    
    
    
    //[iMainMenuViewController release];
    
}







//Call Find Service and Class

-(void)navigateToFindView

{
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    
    if(menulevel > 1)
        
        [defaults  setBool:NO forKey:@"findsplash"];
    
    else
        
        [defaults  setBool:YES forKey:@"findsplash"];
    
    navigatedFromMainMenu = TRUE;
    
    // navigatedTOFindFromMainMenu = TRUE;
    
    FindViewController  *mmvc = [[FindViewController alloc]initWithNibName:@"FindViewController"                                                            bundle:[NSBundle mainBundle]];
    
    if(menuItemName)
        
        mmvc.findTitle=menuItemName;
    
    
    
    [self pushViewController:mmvc];
    
    //[mmvc release];
    
}





-(void)request_utgetmainmenuid

{
    
    
    
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    
    [requestStr appendFormat:@"<MenuItem><userId>%@</userId>", [defaults  valueForKey:KEY_USERID]];
    
    
    
    if([defaults  valueForKey:KEY_BOTTOMBUTTONID])
        
        [requestStr appendFormat:@"<bottomBtnId>%@</bottomBtnId>", [defaults  valueForKey:KEY_BOTTOMBUTTONID]];
    
    
    
    if([defaults  valueForKey:KEY_MITEMID])
        
        [requestStr appendFormat:@"<mItemId>%@</mItemId>", [defaults  valueForKey:KEY_MITEMID]];
    
    
    
    if([defaults  valueForKey:KEY_HUBCITIID])
        
        [requestStr appendFormat:@"<hubCitiId>%@</hubCitiId>", [defaults  valueForKey:KEY_HUBCITIID]];
    
    
    
    [requestStr appendFormat:@"<platform>%@</platform>",@"IOS"];
    
    
    
    [requestStr appendString:@"</MenuItem>"];
    
    
    
    
    
    NSMutableString *urlString = [BASE_URL mutableCopy] ;
    
    [urlString appendFormat:@"firstuse/utgetmainmenuid"];
    
    
    
    NSString *response=[ConnectionManager establishPostConnection:urlString withParam:requestStr];
    
    [self parse_utgetmainmenuid:response];
    
    
    
    //[requestStr release];
    
}



-(void)parse_utgetmainmenuid:(NSString*)responseString

{
    
    if ([UtilityManager isResponseXMLNullOrEmpty:responseString])
        
        return;
    
    
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseString];
    
    
    
    TBXMLElement *saveResponseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement];
    
    if ([[TBXML textForElement:saveResponseCode]isEqualToString:@"10000"])
        
    {
        
        TBXMLElement *mainMenuElement = [TBXML childElementNamed:KEY_MAINMENUID parentElement:tbxml.rootXMLElement];
        
        if(mainMenuElement !=nil)
            
        {
            
            [defaults setObject:[TBXML textForElement:mainMenuElement] forKey:KEY_MAINMENUID];
            
        }
        
    }
    
    
    
}

#pragma mark Location Methods



// Checks if the user has a Zip Code associated with them

-(void)checkForZipCode

{
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/fetchuserlocationpoints?userId=%@",BASE_URL,[defaults objectForKey:KEY_USERID]];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
    
    
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml] ;
    
    TBXMLElement *postalCodeElement = [TBXML childElementNamed:@"postalCode" parentElement:tbxml.rootXMLElement];
    
    
    
    if (postalCodeElement == nil) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
        
        int responseCode = [[TBXML textForElement:[TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement]] intValue];
        
        if (responseCode == 10005) {
            
            //If The User Doesnt Have a Zip on file, Ask them if they want to enter one

            UIAlertController * alert; //enter a zipcode alert
            alert=[UIAlertController alertControllerWithTitle:nil message:@"Would you like to enter a zipcode for us to use instead?" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* no = [UIAlertAction
                                 actionWithTitle:@"No"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     //Warn them that they are completely off the grid as far as ScanSee is concerned
                                     [defaults setValue:nil forKey:KEYZIPCODE];
                                     [UtilityManager showAlert:nil msg:@"You have chosen not to search by your Zipcode. If you would like to search by Zipcode in the future, please go to the User Information screen."];
                                 }];
            [alert addAction:no];
            UIAlertAction* yes = [UIAlertAction
                                  actionWithTitle:@"Yes"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action){
                                      [self showZipEnter];
                                  }];
            [alert addAction:yes];
            
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
            
            
            return;

            
            
            
            
            ReleaseAndNilify(responseXml);
            
            return;
            
        }
        
        else {
            
            // Banner the message given by the server
            
            [UtilityManager showAlert:@"Info" msg:responseTextStr];
            
            //[tbxml release];
            
            ReleaseAndNilify(responseXml);
            
            return;
            
        }
        
    }
    
    
    
    NSString *postalCodeStr = [TBXML textForElement:postalCodeElement];
    
    
    
    //Sets the ZipCode to the key for use in other view controllers
    
    [defaults setValue:postalCodeStr forKey:KEYZIPCODE];
    
    
    
    //set for Austin Experience req when clicked from main menu
    
    [defaults setObject:postalCodeStr forKey:@"postalCode"];
    
    
    
    //[tbxml release];
    
    ReleaseAndNilify(responseXml);
    
    //[self getMainMenuItems];
    
    return;
    
}



//Posts Zip Code to User

-(void)postZip:(NSString *)zipCodeText

{
    
    DLog(@"Posting Zip");
    
    
    
    if([Network currentReachabilityStatus]==0)
        
    {
        
        [UtilityManager showAlert:@"Error!" msg:@"Network is not available"];
        
    }
    
    else {
        
        
        
        NSMutableString *urlString = [BASE_URL mutableCopy] ;
        
        [urlString appendFormat:@"thislocation/updateusrzipcode?userId=%@&zipcode=%@", [defaults valueForKey:KEY_USERID], zipCodeText];
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
        
        if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]) {
            
            ReleaseAndNilify(responseXml);
            
            return;
            
        }
        
        
        
        //NEED TO SET ERROR HANDLING OF RESPONSE. WILL BE ABLE TO WHEN CAN BE TESTED
        
        DLog(@"RESPONSE - %@",responseXml);
        
        [defaults setValue:zipCodeText forKey:KEYZIPCODE];
        
        // [self getMainMenuItems];
        
        
        
        // [responseXml release];
        
        
        
    }
    
}


-(void) gpsAllowDontAllowPopup{
    
    
    UIAlertController * alert;
    alert=[UIAlertController alertControllerWithTitle:nil message:@"Application uses your current location to provide information about retailers and products near you. Do you wish to allow to access your location?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* allow = [UIAlertAction
                            actionWithTitle:@"Allow"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action){
                                //Ajit's edit
                                [NewsUtility setGPSAsAllowForUser];
                               // [SharedManager setGps_allow_flag:YES];
                                
                                //If LS Allowed, but LS are off
                                if ([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) {
                                    [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
                                    
                                }
                                [Location updateLocationinfo:self];
                                //[defaults setValue:@"YES" forKey:@"allowingGPS"];
                                
                            }];
    [alert addAction:allow];
    
    UIAlertAction* dontAllow = [UIAlertAction
                                actionWithTitle:@"Don't Allow"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action){
                                   // [SharedManager setGps_allow_flag:NO];
                                    [defaults setValue:@"" forKey:KEY_LATITUDE];//reset the values
                                    [defaults setValue:@"" forKey:KEY_LONGITUDE];
                                    [self checkForZipCode];
                                   // [defaults setValue:@"NO" forKey:@"allowingGPS"];
                                    [alert dismissViewControllerAnimated:NO completion:nil];
                                }];
    [alert addAction:dontAllow];
    
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
    
}

-(void)checkLocationStatus{
    
    
    
    if([defaults integerForKey:@"AppCounter"] == 1)
        
    {
        
        // If Devive Level GPS and App Level GPS both ON
        
        if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [[defaults valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
            
        {
            
//            [SharedManager setGps_allow_flag:YES];
//            
//            [defaults setValue:@"YES" forKey:@"allowingGPS"];
            
            [Location updateLocationinfo:self];
            
        }
        
        // IF Device Level GPS ON but Application Level GPS OFF
        
        else if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [[defaults valueForKey:@"allowingGPS"] isEqualToString:@"NO"])
            
        {
            
           [self gpsAllowDontAllowPopup];
            
            
        }
        
        // IF Application level GPS off
        
        else if ([[defaults valueForKey:@"allowingGPS"] isEqualToString:@"NO"]) {
            
            
            
           [self gpsAllowDontAllowPopup];
        }
        
    }
    
    
    
    else{
        
        
        
        [defaults setInteger:1 forKey:@"AppCounter"];
        
        if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [CLLocationManager locationServicesEnabled])
            
        {
            
//            [SharedManager setGps_allow_flag:YES];
//            
//            [defaults setValue:@"YES" forKey:@"allowingGPS"];
            
            [Location updateLocationinfo:self];
            
        }
        
        else {
            
//            [SharedManager setGps_allow_flag:NO];
//            
//            [defaults setValue:@"NO" forKey:@"allowingGPS"];
            
            [self checkForZipCode];
            
        }
        
    }
    
}

-(void) updatedLatlongValue :(NSString *) latValue longitudeValue:(NSString *)longValue{
    
    if (latValue == NULL || longValue == NULL) {
        
        return;
        
    }
    
    
    
    [defaults setObject:latValue forKey:KEY_LATITUDE];
    
    [defaults setObject:longValue forKey:KEY_LONGITUDE];
    
    
    
}




-(void)checkForRadius

{
    
    NSString *reqStr = [NSString stringWithFormat:@"<AuthenticateUser><deviceID>%@</deviceID><userId>%@</userId><hubCitiId>%@</hubCitiId></AuthenticateUser>",[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusersettings",BASE_URL];
   
    
    
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:reqStr];
    
    
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    
    TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
    
    if (responseCodeElement != nil && ![[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
       
        
        return;
        
    }
    
    
    
    TBXMLElement *localeRadiusElement = [TBXML childElementNamed:@"localeRadius" parentElement:tbxml.rootXMLElement];
    TBXMLElement *bLocServiceElement = [TBXML childElementNamed:@"bLocService" parentElement:tbxml.rootXMLElement];
    if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"false"])
    {
        [SharedManager setGps_allow_flag:NO];
        [defaults setValue:@"NO" forKey:@"allowingGPS"];
    }
    else if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"true"])
    {
        [SharedManager setGps_allow_flag:YES];
        [defaults setValue:@"YES" forKey:@"allowingGPS"];
    }
    
    
    
    NSString *userRadius = [TBXML textForElement:localeRadiusElement];
    
    if(userRadius.length > 0)
        
    {
        
        [defaults setValue:userRadius forKey:@"FindRadius"];
        
        NSLog(@"my radius is %@",[defaults valueForKey:@"FindRadius"]);
        
    }
    
    
    
    
    
}


/*
 
 #pragma mark - Navigation
 
 
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 
 // Get the new view controller using [segue destinationViewController].
 
 // Pass the selected object to the new view controller.
 
 }
 
 */
-(void) logOutPressed
{
    
    
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    
    [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    AppDelegate *app =HubCitiAppDelegate;
    viewController.login_DO = app.loginDO;
    
    [self pushViewController:viewController];
    
}


@end


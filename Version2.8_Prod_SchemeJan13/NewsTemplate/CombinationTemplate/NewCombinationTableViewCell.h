//
//  NewCombinationTableViewCell.h
//  HubCiti
//
//  Created by Apple on 6/30/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SdImageView.h"
//#import "CombinationTableViewCell.h"

static NSString *CollectionViewCellIdentifier = @"CombinationCollectionCell";

@interface CombinationIndexedCollectionView : UICollectionView

@property (nonatomic, strong) NSIndexPath *indexPath;



@end




@interface NewCombinationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *sectionButton;
@property (weak, nonatomic) IBOutlet CombinationIndexedCollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *viewMoreButton;

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath;

@end

@interface CombinationCollectionCell : UICollectionViewCell
@property(nonatomic,weak) IBOutlet SdImageView* icon;
@property(nonatomic,weak) IBOutlet UILabel* title,*subTitle;

@property (weak, nonatomic) IBOutlet UIImageView *playIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeightForPhotos;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *LevelHeight;



@end

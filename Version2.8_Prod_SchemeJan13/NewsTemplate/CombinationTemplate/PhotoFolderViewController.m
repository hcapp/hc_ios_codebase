//
//  DetailViewController.m
//  pageSwipe
//
//  Created by Lakshmi H R on 5/18/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import "PhotoFolderViewController.h"
#import "CollectionViewCell.h"
//#import "SideMenuTableViewController.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "PhotoDetailViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "AppDelegate.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"

@interface PhotoFolderViewController ()
{
    NSMutableArray  *generatedThumbnails;
    UIImageView *playImg;
    UIImage *dummyImg;
    NSUInteger thumbnailCount;
    NSMutableDictionary* imageDictionary,*photoDictionary;
    UIButton *backButton;
}
@end

@implementation PhotoFolderViewController

@synthesize imgArray,longD,catName,catColor,titleColor,backButtonColor,newsID;

- (void)viewDidLoad {
    [super viewDidLoad];
    imageDictionary = [[NSMutableDictionary alloc] init];
    photoDictionary = [[NSMutableDictionary alloc] init];
    thumbnailCount = 0;
    playImg = [[UIImageView alloc]init];
    // Do any additional setup after loading the view from its nib.
    [self.collView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"collCell"];
    
    if (NSOrderedSame==[catName caseInsensitiveCompare:@"Photos"]) {
        self.title = @"Photos";
    }
    else if (NSOrderedSame==[catName caseInsensitiveCompare:@"Videos"])
    {
        self.title = @"Videos";
    }
    generatedThumbnails = [[NSMutableArray alloc]init];
    dummyImg = [UIImage alloc];
    dummyImg = [UIImage imageNamed:@"video-not-present.png"];
    [HubCitiAppDelegate showActivityIndicator];
    
   
    //home button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
    
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;
    
    
    self.navigationItem.hidesBackButton = YES;
    
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame=CGRectMake(0.0, 0.0, 30.0, 30.0);
    
    [backButton addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationItem.leftBarButtonItem = back;
    
    [UtilityManager setTransperentImageColor:backButtonColor forButton:backButton];
    
    [HubCitiAppDelegate removeActivityIndicator];
    [_collView reloadData];
    
    
}
-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [cusNav hideBackButtonForTransperentImage];
    
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:catColor];
    
    [cusNav setTitle:catName forView:self withHambergur:NO withColor:titleColor];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    
   
    
    
    [super viewWillAppear:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]], NSForegroundColorAttributeName,[UIFont fontWithName:@"Helvetica-Bold" size:17.0], NSFontAttributeName,nil]];
    
}
-(void)popBackToPreviousPage
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)leftSideMenuButtonPressed
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        //send bookmark array to side menu on hambrger click
        if(!(self.menuContainerViewController.menuState == MFSideMenuStateClosed)){
            
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"notificationName" object:buttonArray];
            
        }
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return imgArray.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"collCell";
    
    
    CollectionViewCell *cell = nil;
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"CollectionViewCell" owner:self options:nil];
    }
    //    UIImageView *cellImage = (UIImageView *)[cell.contentView viewWithTag:100];
    //    UILabel *colLabel = (UILabel *)[cell.contentView viewWithTag:200];
    //    colLabel.text = @"Former Facebook Workers: We Routinely Suppressed Conservative News";
    //
    //    cellImage.image = [UIImage imageNamed:@"images1.jpg"];
    //    UILabel *photoNum = (UILabel *)[cell.contentView viewWithTag:500];
    //    photoNum.text = @"6";
    //    UIImageView *photoImg = (UIImageView *)[cell.contentView viewWithTag:400];
    //    photoImg.image = [UIImage imageNamed:@"camera.png"];
    if(indexPath.row != imgArray.count)
    {
        cell.imgSubView.hidden = YES;
        cell.cellLabel.text = @"Former Facebook Workers: We Routinely Suppressed Conservative News";
        cell.cellLabel.hidden = YES;
        if(IPAD){
            cell.cellLabel.font = [UIFont systemFontOfSize:18];
        }
        
        cell.cellLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        cell.cellImage.frame = cell.frame;
        if([imgArray objectAtIndex:indexPath.row] && (NSOrderedSame==[catName caseInsensitiveCompare:@"Photos"]))
        {
            cell.playIcon.hidden = true;
            NSString* urlStr = [imgArray objectAtIndex:indexPath.row];
            
            if ([photoDictionary objectForKey:urlStr]) {
                
                cell.cellImage.image = [photoDictionary objectForKey:urlStr];
            }
            else{
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                    
                    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlStr]];
                    
                    UIImage* image = [[UIImage alloc] initWithData:imageData];
                    
                    
                    if(image){
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            cell.cellImage.image = image;
                            [photoDictionary setObject:image forKey:urlStr];
                        });
                        
                    }
                });
            }
        }

        }
        if([imgArray objectAtIndex:indexPath.row] && (NSOrderedSame==[catName caseInsensitiveCompare:@"Videos"]))
        {
            cell.playIcon.hidden = false;
            @try{
              
                NSString* url = [imgArray objectAtIndex:indexPath.row];
                UIImage *image = [UIImage imageNamed:@"video-not-present.png"];
                cell.cellImage.image = image;
                cell.cellImage.contentMode = UIViewContentModeScaleAspectFit;
                
                if ([imageDictionary objectForKey:url]) {
                    
                    cell.cellImage.image = [imageDictionary objectForKey:url];
                    cell.playIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
                }
                else
                {
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                        
                        
                        AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:url]];
                        
                        //  Get thumbnail at the very start of the video
                        CMTime time = CMTimeMake(3, 2);
                        
                        
                        //  Get image from the video at the given time
                        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                        imageGenerator.appliesPreferredTrackTransform = YES;
                        
                        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
                        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
                        CGImageRelease(imageRef);
                        
                        if(thumbnail){
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                cell.cellImage.image = thumbnail;
                                cell.playIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
                                [imageDictionary setObject:thumbnail forKey:url];
                                
                            });
                            
                        }
                    });
                }
            }
            @catch (NSException *exception) {
                // Do nothing
                cell.playIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
            }

        }
        
    
    
    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
   // CGRect screenRect = [[UIScreen mainScreen] bounds];
   // CGFloat screenWidth = screenRect.size.width;
    //CGFloat screenHeight = screenRect.size.height;
    float cellwidth = (_collView.frame.size.width-5)/2 -(1*3) ; //Replace the divisor with the column count requirement. Make sure to have it in float.
    float cellheigth = _collView.frame.size.height/3 - (5*3);
    CGSize size = CGSizeMake(cellwidth, cellheigth);
    
    return size;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5; // This is the minimum inter item spacing, can be more
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    PhotoFolderViewController* evt = [[PhotoFolderViewController alloc] initWithNibName:@"PhotoFolderViewController" bundle:nil];
    //    self.phview = evt;
    //
    //    //NewsDetailViewController * evt = [[NewsDetailViewController alloc] initWithNibName:@"NewsDetailViewController" bundle:nil];
    //
    //    [self.navigationController pushViewController:evt animated:YES];
    //    //[self.collView deselectRowAtIndexPath:[self.collView indexPathsForSelectedItems] animated:NO];
    //VideoViewController *evt = [[VideoViewController alloc]initWithNibName:@"VideoViewController" bundle:nil];
    PhotoDetailViewController* evt = [[PhotoDetailViewController alloc] initWithNibName:@"PhotoDetailViewController" bundle:nil];
    evt.imgNumber = indexPath.row;
    evt.catName = catName;
    evt.lDescription = longD;
    evt.catColor = catColor;
    evt.titleColor = titleColor;
    //evt.imgNumber = indexPath.row;
    evt.photoArray = imgArray;
    evt.newsID = newsID;
    [self.navigationController pushViewController:evt animated:YES];
    
}
-(UIImage*) drawImage:(UIImage*) fgImage
              inImage:(UIImage*) bgImage bigImageSize:(CGSize)bgSize
              atPoint:(CGPoint)  point
             withSize:(CGSize) size
{
    
    UIGraphicsBeginImageContextWithOptions(bgSize, FALSE, 0.0);
    [bgImage drawInRect:CGRectMake(0 , 0, bgSize.width , bgSize.height)];
    [fgImage drawInRect:CGRectMake(point.x, point.y, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
//- (UIEdgeInsets)collectionView: (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(0,5,0,5);
//}// top, left, bottom, right }

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

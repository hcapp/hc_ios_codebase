
//
//  CombinationViewController.m
//  CombinationTabledCollectionView
//
//  Created by Ravindra on 2016-05-16.
//  Copyright (c) 2016 Ravindra. All rights reserved.
//



#import "CombinationViewController.h"
#import "NewCombinationTableViewCell.h"
#import "BannerTableViewCell.h"
#import "HubCitiConstants.h"
#import "VerticalTableViewCell.h"
#import "ContainerSubViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "CombinationCategoryViewController.h"
#import "CombinationObj.h"
#import "HTTPClient.h"
#import "AppDelegate.h"
#import "VerticalTableViewCell.h"
#import "BookMarksViewController.h"
#import "SearchBtnViewController.h"
#import "NewsDetailViewController.h"
#import "SideNavigationResponse.h"
#import "MainMenuResponse.h"
#import "CommonUtilityNews.h"
#import "CommonUtility.h"
#import "PhotoView.h"
#import <AVKit/AVKit.h>
#import "WebBrowserViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "PhotoDetailViewController.h"
#import "VideoViewController.h"
#import "ScrollTemplateItems.h"
#import "PhotoFolderViewController.h"
#import "CollectionFooterReusableView.h"
#import "SideViewController.h"

#define VERTICAL_CELL_HEIGHT_IPHONE 71
#define VERTICAL_CELL_HEIGHT_IPAD 100
@interface CombinationViewController () <HTTPClientDelegate,MainTableViewCellDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIActivityIndicatorView* loading;
    NSMutableArray * resultCategoryList,*bookMarkList;
    BOOL fetchNext,isRefresh;
    UIBarButtonItem* buttonLeft;
    UIButton* moreButton;
    UILabel* titleLbl;
    UIButton* Search;
    UIButton *hamburger;
    UIRefreshControl* refreshControl;
    UIView* containerView;
    WebRequestState iWebRequestState;
    SdImageView *bannerSdImageView ;
    UIFont *titleFont,*subTitleFont;
    NSMutableDictionary* imageDictionary;
    UIView *secondView;
    UIScrollView * buttonScroll;
    float xVal;
    CGFloat newOffset;
    UIView *rightBorder;
    UIView *leftBorder;
    UIButton * buttonList;
    CGPoint lastContentOffset;
    CustomizedNavController *cusNav;
    NSMutableArray* nonFeedArr;
}

@property(nonatomic,strong) MainMenuResponse  *menuResponse;
@property (nonatomic, strong) NSMutableDictionary *contentOffsetDictionary;
@property (nonatomic) BOOL scrollViewEnabled;
@end

@implementation CombinationViewController
@synthesize scrollViewEnabled,buttonArray;



-(void) topbuttonClicked : (id) sender
{
    if (fetchNext || isRefresh) {
        return;
    }
    UIButton* btn  = (UIButton*) sender;
    
    switch (btn.tag) {
        case 1: //  Humberger Button
        {
            
            [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
                
            }];
        }
            break;
        case 2: // More Button
        {
            if (userIdentifier == TRUE)
            {
                UIAlertController * alert;
                
                alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Sign up for free to use this feature",@"Sign up for free to use this feature") message:nil preferredStyle:UIAlertControllerStyleAlert];
                
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"Continue"
                                     style:UIAlertActionStyleCancel
                                     handler:nil];
                [alert addAction:ok];
                UIAlertAction* signup = [UIAlertAction
                                         actionWithTitle:@"Sign up"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [defaults setBool:YES forKey:@"isMainMenuSignUp"];
                                             
                                             [defaults setValue:@"1" forKey:KEY_MENULEVEL];
                                             LoginViewController *login = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
                                             
                                             AppDelegate *app = HubCitiAppDelegate;
                                             login.login_DO = app.loginDO;
                                             [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
                                             [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
                                             [self.menuContainerViewController.centerViewController pushViewController:login animated:NO];
                                             
                                         }];
                [alert addAction:signup];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
            else
            {
                BookMarksViewController *bk =[[BookMarksViewController alloc] initWithNibName:@"BookMarksViewController" bundle:nil];
                [self.navigationController pushViewController:bk animated:NO];
            }
        }
            break;
        case 3: // Search
        {
            SearchBtnViewController *searchData = [[SearchBtnViewController alloc]initWithNibName:@"SearchBtnViewController" bundle:nil];
            [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
            [self.navigationController pushViewController:searchData animated:NO];
        }
            break;
            
        default:
            break;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isRefresh || fetchNext)
        return;
    
    NSString* catName = [_combinationObj.categoryList[indexPath.section] objectForKey:@"catName"];
    NSDictionary* itemList = [_combinationObj.categoryList[indexPath.section] objectForKey:@"itemList"];
    
    
    if((NSOrderedSame==[catName caseInsensitiveCompare:@"Photos"]) || (NSOrderedSame==[catName caseInsensitiveCompare:@"Videos"]))
    {
        
        NSMutableArray* toSendArray;
        if(NSOrderedSame==[catName caseInsensitiveCompare:@"Photos"])
        {
            toSendArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[itemList objectForKey:@"image"]]];
        }
        else{
            toSendArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[itemList objectForKey:@"videoLink"]]];
        }
        if(toSendArray.count == 0)
        {
            [UtilityManager showAlert:nil msg:@"Video not found"];
            
            
        }
        else if(toSendArray.count == 1)
        {
            PhotoDetailViewController *evt  = [[PhotoDetailViewController alloc]initWithNibName:@"PhotoDetailViewController" bundle:nil];
            evt.lDescription = [itemList objectForKey:@"lDesc"];
            evt.photoArray = [[NSMutableArray alloc]initWithArray:toSendArray];
            evt.catName = catName;
            evt.newsID = [itemList objectForKey:@"itemID"];
            [self.navigationController pushViewController:evt animated:YES];
            
        }
        else
        {
            PhotoFolderViewController* evt = [[PhotoFolderViewController alloc] initWithNibName:@"PhotoFolderViewController" bundle:nil];
            evt.imgArray = [[NSMutableArray alloc]initWithArray:toSendArray];
            evt.longD = [itemList objectForKey:@"lDesc"];
            
            evt.catName = catName;
            
            evt.newsID = [itemList objectForKey:@"itemID"];
            [self.navigationController pushViewController:evt animated:YES];
        }
    }
    
    else{
        NewsDetailViewController *newsDetail =[[NewsDetailViewController alloc] initWithNibName:@"NewsDetailViewController" bundle:nil];
        newsDetail.newsID = [itemList objectForKey:@"itemID"];
        [self.navigationController pushViewController:newsDetail animated:NO];
    }
    
    
}


-(void) addMenu
{
    cusNav.customNavBardelegate = self;
    [cusNav hideBackButton:YES];
    [cusNav hideHambergerButton: NO];
    
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    if (menulevel > 1)
    {
        [cusNav hideBackButton:NO];
    }
    
}
-(void)popBackToPreviousPage
{
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    NSString * levelString;
    if(menulevel > 1)
        levelString = [NSString stringWithFormat:@"%d", menulevel-1];
    else
        levelString = [NSString stringWithFormat:@"%d", menulevel];
    [[HubCitiAppDelegate getLinkIdArray]removeLastObject];
    [defaults setValue:levelString forKey:KEY_MENULEVEL];
    
    [self.navigationController popViewControllerAnimated:NO];
}
-(void) addMore // Add More Button
{
    [moreButton removeFromSuperview];
    [Search removeFromSuperview];
    
    moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame = CGRectMake(SCREEN_WIDTH - (IPAD ? VARIABLE_WIDTH(70):VARIABLE_WIDTH(95)), IPAD ? 5 :  VARIABLE_HEIGHT(12), IPAD ? VARIABLE_WIDTH(40) : VARIABLE_WIDTH(60), VARIABLE_HEIGHT(25));
    moreButton.tag = 2;
    [moreButton addTarget:self action:@selector(topbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIImageView * moreImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, VARIABLE_WIDTH(18), VARIABLE_HEIGHT(16))];
    moreImage.image = [UIImage imageNamed:@"more2.png"];
    [moreButton  addSubview:moreImage];
    UILabel * moreLbl = [[UILabel alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(25), - 2, VARIABLE_WIDTH(60), VARIABLE_HEIGHT(20))];
    moreLbl.text = @"More";
    moreLbl.textColor = [UIColor blackColor];
    moreLbl.font = [UIFont systemFontOfSize:14];
    
    [moreButton addSubview:moreLbl];
    
}

-(void) addSearch // Add Search Button
{
    [moreButton removeFromSuperview];
    [Search removeFromSuperview];
    Search = [UIButton buttonWithType:UIButtonTypeCustom];
    
    Search.frame = CGRectMake(SCREEN_WIDTH - VARIABLE_WIDTH(25) ,12, 20, 20);
    
    Search.tag = 3;
    [Search setBackgroundImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    [Search addTarget:self action:@selector(topbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)setNavigationBar
{
    [self addMenu];
    // [self addMore];
    [self addSearch];
    
    [self.navigationController.navigationBar addSubview:Search];
    // [self.navigationController.navigationBar addSubview:moreButton];
    
    if (_combinationObj.bannerImg.length > 0) {
        bannerSdImageView.hidden = false;
        [bannerSdImageView loadImage: _combinationObj.bannerImg];
        [self.navigationController.navigationBar addSubview : bannerSdImageView];
    }
    
}

-(void) refreshTableView
{
    isRefresh = true;
    self.scrollViewEnabled = false;
    resultCategoryList = [[NSMutableArray alloc] init];
    bookMarkList = [[NSMutableArray alloc] init];
    [self fetchCategoryData:@"0"];
}
-(void) setUpTopTabBar
{
    //    tabBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, VARIABLE_HEIGHT(30))];
    //    tabBarView.backgroundColor = [UIColor convertToHexString:@"#f1f1f1"];
    //    [self.view addSubview:tabBarView];
    //    //Left Search Button for old news search
    //    leftSearchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    leftSearchBtn.frame =CGRectMake(VARIABLE_WIDTH(10), tabBarView.frame.size.height/2 - VARIABLE_HEIGHT(15)/2, VARIABLE_WIDTH(15),VARIABLE_HEIGHT(15));
    //    [leftSearchBtn setBackgroundImage:[UIImage imageNamed:@"search2.png"] forState:UIControlStateNormal];
    //    [tabBarView addSubview:leftSearchBtn];
    //    [leftSearchBtn addTarget:self action:@selector(leftSearchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    secondView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,VARIABLE_HEIGHT(30))];
    
    secondView.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:0.5];
    
    buttonScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(VARIABLE_WIDTH(120), 0, SCREEN_WIDTH - VARIABLE_WIDTH(120), VARIABLE_HEIGHT(30))];
    
    
    float width=(SCREEN_WIDTH-buttonScroll.frame.origin.x)/3;
    buttonScroll.showsHorizontalScrollIndicator = YES;
    buttonScroll.showsVerticalScrollIndicator = NO;
    buttonScroll.alwaysBounceVertical = NO;
    buttonScroll.delegate = self;
    secondView.backgroundColor =  [UIColor convertToHexString:@"#C7D0D2"];
    [secondView addSubview:buttonScroll];
    [self.view addSubview:secondView];
    NSLog(@"%f button scroll height %f", buttonScroll.frame.size.height, VARIABLE_HEIGHT(20));
    
    
    //Array for book mark buttons
    xVal=0;
    
    newOffset=0;
    
    // Array to display all the book mark button in scroll view
    xVal=0;
    // [self request_newsListing];
    // buttonArray = [[NSMutableArray alloc]initWithObjects:@"Top Stories",nil];
    newOffset=0;
    if (buttonArray.count == 1)
    {
        for(int i=0;i<buttonArray.count;i++)
        {
            buttonList = [UIButton buttonWithType:UIButtonTypeCustom];
            
            buttonList.frame = CGRectMake(buttonScroll.frame.size.width/2 - width/2, 0,width + 25, VARIABLE_HEIGHT(30));
            leftBorder = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, buttonScroll.frame.size.height)];
            leftBorder.backgroundColor = [UIColor whiteColor];
            [buttonList addSubview:leftBorder];
            [buttonList setTitle:buttonArray[i] forState:UIControlStateNormal];
            buttonList.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 20.0f:12.0f)];
            buttonList.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonList.titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
            [buttonList setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            buttonList.tag = i;
            buttonList.titleLabel.numberOfLines = 1;
            
            rightBorder = [[UIView alloc] initWithFrame:CGRectMake(buttonList.frame.size.width, 0, 1, buttonScroll.frame.size.height)];
            rightBorder.backgroundColor = [UIColor whiteColor];
            [buttonList addSubview:rightBorder];
            
            //            if( buttonList.tag == 0)
            //            {
            //                buttonList.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
            //
            //
            //            }
            [buttonList addTarget:self action:@selector(bookMarkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [buttonScroll addSubview:buttonList];
            
            xVal = xVal +width;
        }
        // COntent size for book mark scroll view
        
        [buttonScroll setContentSize:CGSizeMake(xVal,0)];
    }
    
    else if (buttonArray.count == 2)
    {
        float width_For_Two_Cat = (SCREEN_WIDTH-buttonScroll.frame.origin.x)/2;
        for(int i=0;i<buttonArray.count;i++)
        {
            CGSize mainTitleSize = [self calculateLabelWidth:buttonArray[i] withWidth:width_For_Two_Cat];
            buttonList = [UIButton buttonWithType:UIButtonTypeCustom];
            buttonList.titleLabel.numberOfLines = 1;
            
            buttonList.frame = CGRectMake(xVal, 0,mainTitleSize.width + VARIABLE_WIDTH(30), VARIABLE_HEIGHT(30));
            [buttonList setTitle:buttonArray[i] forState:UIControlStateNormal];
            buttonList.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 20.0f:12.0f)];
            
            buttonList.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonList.titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
            [buttonList setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            buttonList.tag = i;
            //            if( buttonList.tag == 0)
            //            {
            //
            //                buttonList.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:1];
            //            }
            [buttonList addTarget:self action:@selector(bookMarkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [buttonScroll addSubview:buttonList];
            xVal = xVal +buttonList.frame.size.width;
        }
        // COntent size for book mark scroll view
        [buttonScroll setContentSize:CGSizeMake(xVal + 40,0)];
        
    }
    
    
    else
    {
        //width = (SCREEN_WIDTH-VARIABLE_WIDTH(120))/3;
        
        for(int i=0;i<buttonArray.count;i++)
        {
            width =  (SCREEN_WIDTH - buttonScroll.frame.origin.x)/3;
            
            
            
            
            CGSize mainTitleSize = [self calculateLabelWidth:buttonArray[i] withWidth:width];
            //CGSize mainTitleSize = [buttonArray[i] boundingRectWithSize:(width, VARIABLE_HEIGHT(20) options:NSStringDrawingUsesDeviceMetrics context:NSStringDrawingUsesLineFragmentOrigin];
            buttonList = [UIButton buttonWithType:UIButtonTypeCustom];
            
            
            // width = [self calculateLabelWidth:buttonArray[i] withWidth:width];
            buttonList.frame = CGRectMake(xVal, 0,mainTitleSize.width + VARIABLE_WIDTH(30), VARIABLE_HEIGHT(30));
            NSLog(@" %f %f %f ",buttonList.frame.origin.x, buttonList.frame.origin.y + buttonList.frame.size.height , buttonList.frame.size.width);
            
            [buttonList setTitle:buttonArray[i] forState:UIControlStateNormal];
            buttonList.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 20.0f:12.0f)];
            
            buttonList.titleLabel.numberOfLines = 1;
            
            buttonList.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonList.titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
            [buttonList setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            buttonList.tag = i;
            //            if( buttonList.tag == 0)
            //            {
            //
            //                buttonList.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
            //            }
            [buttonList addTarget:self action:@selector(bookMarkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [buttonScroll addSubview:buttonList];
            xVal = xVal +buttonList.frame.size.width;
        }
        
        
        // COntent size for book mark scroll view
        [buttonScroll setContentSize:CGSizeMake(xVal + 40,0)];
    }
    
    
    
}

-(CGSize)calculateLabelWidth:(NSString*) labelString withWidth:(CGFloat) labelwidth
{
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:(IPAD ? 20.0f:12.0f)]};
    
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, VARIABLE_HEIGHT(20))
                                            options:NSStringDrawingUsesDeviceMetrics
                                         attributes:attributes
                                            context:nil];
    return rect.size;
}

-(void)bookMarkButtonClicked: (id) sender
{
    if (isRefresh) {
        return;
    }
    int index = (int)[sender tag];
    
    NSString* feedlink = [_combinationObj.bookMarkList[index] objectForKey:@"nonfeedlink"];
    
    if (feedlink.length > 0) {
        
        [defaults setValue:feedlink forKey:KEY_URL];
       
        [defaults setBool:YES forKey:@"isCombo"];
        [defaults setValue:[_combinationObj.bookMarkList[index] objectForKey:@"backButtonColor"] forKey:@"backbuttonColor"];
        
        WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        urlDetail.title = [_combinationObj.bookMarkList[index] objectForKey:@"catName"];
        urlDetail.titleColor = [_combinationObj.bookMarkList[index] objectForKey:@"catTxtColor"];
        urlDetail.titleBackgColor = [_combinationObj.bookMarkList[index] objectForKey:@"catColor"];
        
        [self.navigationController pushViewController:urlDetail animated:NO];
        
        return;
    }

    if(NSOrderedSame==[[_combinationObj.bookMarkList[index] objectForKey:@"catName"] caseInsensitiveCompare:@"Photos"] || NSOrderedSame==[[_combinationObj.bookMarkList[index] objectForKey:@"catName"] caseInsensitiveCompare:@"Videos"])
    {
        PhotoView *newsDetail = [[PhotoView alloc] initWithNibName:@"PhotoView" bundle:nil];
        newsDetail.categoryName = [_combinationObj.bookMarkList[index] objectForKey:@"catName"];
        newsDetail.catColor = [_combinationObj.bookMarkList[index] objectForKey:@"catColor"];
        newsDetail.titleColor = [_combinationObj.bookMarkList[index] objectForKey:@"catTxtColor"];
        [self.navigationController pushViewController:newsDetail animated:NO];
    }
    else
    {
        CombinationCategoryViewController *newsDetail =[[CombinationCategoryViewController alloc] initWithNibName:@"CombinationCategoryViewController" bundle:nil];
        newsDetail.catName = [_combinationObj.bookMarkList[index] objectForKey:@"catName"];
        newsDetail.catColor = [_combinationObj.bookMarkList[index] objectForKey:@"catColor"];
        newsDetail.titleColor = [_combinationObj.bookMarkList[index] objectForKey:@"catTxtColor"];
        [self.navigationController pushViewController:newsDetail animated:NO];
        //[self performSelector:@selector(navigateToNextView:) withObject:newsDetail afterDelay:1.0];
        
    }
}
//-(void)navigateToNextView:(CombinationCategoryViewController*)newsDetail
//{
//     [self.navigationController pushViewController:newsDetail animated:NO];
//}
-(void) setColor : (NSInteger) tag : (id) btn
{
    for (id subview in [buttonScroll subviews])
    {
        if ([subview isMemberOfClass:[UIButton class]])
        {
            UIButton *prevButton= (UIButton*) subview;
            if(prevButton.tag == tag)
            {
                UIButton * currentBtn = prevButton;
                //[currentBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                currentBtn.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
                rightBorder.backgroundColor = [UIColor whiteColor];
                leftBorder.backgroundColor = [UIColor whiteColor];
                
                
            }
            else
            {
                rightBorder.backgroundColor = [UIColor whiteColor];
                leftBorder.backgroundColor = [UIColor whiteColor];
                prevButton.backgroundColor = [UIColor clearColor];
            }
        }
        
    }
    
    float width;
    
    if((buttonArray.count)==2)
    {
        width = (SCREEN_WIDTH - VARIABLE_WIDTH(120))/2;
    }
    
    else
    {
        width=(SCREEN_WIDTH-VARIABLE_WIDTH(120))/3;
    }
    
    CGSize moveSize = [self calculateLabelWidth:buttonArray[tag] withWidth:width];
    
    if (tag >=2)
    {
        int start  = (tag - 1)*width;
        newOffset = start;
        
        if (tag < buttonArray.count-1 )
        {
            [buttonScroll setContentOffset:CGPointMake(start, 0.0) animated:YES];
        }
        else if( tag==buttonArray.count-1){
            [buttonScroll setContentOffset:CGPointMake((buttonArray.count-3) * moveSize.width, 0.0) animated:YES];
        }
        
        [buttonScroll setContentOffset:CGPointMake(moveSize.width + 40, 0.0) animated:YES];
    }
    else if ((buttonArray.count == 2) && (tag == 1))
    {
        [buttonScroll setContentOffset:CGPointMake(moveSize.width + 40, 0.0) animated:YES];
        
    }
    else
    {
        [buttonScroll setContentOffset:CGPointMake(0.0,0.0) animated:YES];
        newOffset=0;
    }
    
    
    
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    nonFeedArr = [[NSMutableArray alloc] init];
    [defaults setBool:NO forKey:@"isDonePressed"];
    [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    self.view.backgroundColor = [UIColor whiteColor];
    self.combTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, VARIABLE_HEIGHT(30), SCREEN_WIDTH, SCREEN_HEIGHT-VARIABLE_HEIGHT(30) ) style:UITableViewStylePlain];
    self.combTableView.dataSource = self;
    self.combTableView.delegate = self;
    self.combTableView.contentInset = UIEdgeInsetsMake(0, 0,VARIABLE_HEIGHT(60), 0);
    [self.view addSubview:self.combTableView];
    
    imageDictionary = [[NSMutableDictionary alloc] init];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(showResetPasswordAlert:)
    //                                                 name:@"tempPassword"
    //                                               object:nil];
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(showEmailCountAlert:)
    //                                                 name:@"emailCount"
    //                                               object:nil];
    titleFont = [UIFont boldSystemFontOfSize:(IPAD ? 16.0f:14.0f)];
    subTitleFont = [UIFont systemFontOfSize:(IPAD ? 14.0f:12.0f)];
    
    UINib *nib = [UINib nibWithNibName:@"NewCombinationTableViewCell" bundle: nil];
    [self.combTableView registerNib:nib forCellReuseIdentifier:@"NewCellIdentifier"];
    //self.tableView.frame = CGRectMake(0, VARIABLE_HEIGHT(30), SCREEN_WIDTH, SCREEN_HEIGHT-VARIABLE_HEIGHT(30));
    //[self.combTableView setContentInset:UIEdgeInsetsMake(VARIABLE_HEIGHT(30),0,0,0)];
    
    bannerSdImageView = [[SdImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 130)/2,0,130,40)];
    bannerSdImageView.backgroundColor = [UIColor clearColor];
    
    refreshControl = [[UIRefreshControl alloc]init];
    //[self.tableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTableView) forControlEvents:UIControlEventValueChanged];
    [self.combTableView addSubview:refreshControl];
    
    
    if (resultCategoryList == nil) {
        resultCategoryList = [[NSMutableArray alloc] init];
    }
    if (bookMarkList == nil) {
        bookMarkList = [[NSMutableArray alloc] init];
    }
    
    
    self.contentOffsetDictionary = [NSMutableDictionary dictionary];
    self.navigationItem.hidesBackButton = YES;
    
    //popup for the first time
    if (userIdentifier==FALSE && ![defaults boolForKey:@"pushOnKill"]){
        int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
        if (menulevel<=1) {
            [self checkLocationStatus];
            
            [self checkForRadius];
        }
    }
    else if([defaults boolForKey:@"pushOnKill"]){
        [defaults setBool:NO forKey:@"pushOnKill"];
    }
    
    
    
}

-(void) viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    cusNav =(CustomizedNavController *) self.navigationController;
  
    
    [HubCitiAppDelegate showActivityIndicator];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    BOOL value = [defaults boolForKey:@"LoginUserNews"];
    [defaults setBool:YES forKey:@"LoginSuccess"];
    [defaults setBool:NO forKey:@"canDisplayAPNSListScreen"];
    NSLog(@"loginuser: %d",(int)value);
    if (userIdentifier==FALSE)
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {// iOS 8.0 and later
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        } else { // below iOS 8.0
            
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        }
    }
    
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    [self setNavigationBar];
    [self initMenuData];
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    
    if(menulevel<=1){
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"homeClicked" object:nil];
        [defaults setValue:@"Combination News Template" forKey:@"centerViewController"];
    }
    if(![defaults boolForKey:@"isDonePressed"])
    {
        SideViewController *leftMenuViewController;
        if(IPAD){
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
        }
        else{
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
            
        }
        
        leftMenuViewController.isNewsTemplate = TRUE;
        if(menulevel > 1)
        {
            leftMenuViewController.isSideMenu = YES;
        }
        [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
        //[self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    if ([defaults boolForKey:@"isDonePressed"])
    {
        [defaults setBool:NO forKey:@"isDonePressed"];
    }
    
    [self fetchCategoryData :@"0"];
    buttonArray = [[NSMutableArray alloc]init];
    self.scrollViewEnabled = YES;
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]], NSForegroundColorAttributeName,[UIFont fontWithName:@"Helvetica-Bold" size:17.0], NSFontAttributeName,nil]];
    
}


-(void) initMenuData
{
    
    
    if (FindBottomButtonID!=Nil)
    {
        [FindBottomButtonID removeAllObjects];
    }
    else
    {
        if (FindBottomButtonID==Nil) {
            FindBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (EventsBottomButtonID!=Nil)
    {
        [EventsBottomButtonID removeAllObjects];
    }
    else
    {
        if (EventsBottomButtonID==Nil) {
            EventsBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (FilterBottomButtonID!=Nil)
    {
        [FilterBottomButtonID removeAllObjects];
    }
    else
    {
        if (FilterBottomButtonID==Nil) {
            FilterBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    
    if (FindSingleBottomButtonID!=Nil)
    {
        [FindSingleBottomButtonID removeAllObjects];
    }
    else
    {
        if (FindSingleBottomButtonID==Nil) {
            FindSingleBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (CityExperienceBottomButtonID!=Nil)
    {
        [CityExperienceBottomButtonID removeAllObjects];
    }
    else
    {
        if (CityExperienceBottomButtonID==Nil) {
            CityExperienceBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (FundraiserBottomButtonID!=Nil)
    {
        [FundraiserBottomButtonID removeAllObjects];
    }
    else
    {
        if (FundraiserBottomButtonID==Nil) {
            FundraiserBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (NearbyBottomButtonID!=Nil)
    {
        [NearbyBottomButtonID removeAllObjects];
    }
    else
    {
        if (NearbyBottomButtonID==Nil) {
            NearbyBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    
    
    if (linkID==NULL) {
        linkID=[[NSMutableArray alloc]init];
    }
    else
    {
        
        [linkID removeAllObjects];
        
    }
    
    if (dateCreated==NULL) {
        dateCreated = [[NSMutableDictionary alloc]init];
    }
    
    if (cashedResponse==NULL) {
        cashedResponse = [[NSMutableDictionary alloc]init];
    }
    
    
    if (deptId==NULL) {
        deptId = [[NSMutableDictionary alloc]init];
    }
    
    if (typeId==NULL) {
        typeId = [[NSMutableDictionary alloc]init];
    }
    
    if (selectedSortOption==NULL) {
        selectedSortOption=[[NSMutableDictionary alloc]init];
    }
    
    if (selectedFilterOption==NULL) {
        selectedFilterOption=[[NSMutableDictionary alloc]init];
    }
    
    if (selectedSortCityDic==NULL) {
        selectedSortCityDic=[[NSMutableDictionary alloc]init];
        
    }
    [defaults setBool:NO forKey:BottomButton];
    
    if([defaults valueForKey:KEYZIPCODE] && [[defaults valueForKey:KEYZIPCODE]isEqualToString:@"N/A"])
    {
        [defaults setValue:nil forKey:KEYZIPCODE];
    }
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    if(menulevel<=1){
        [NewsUtility refreshLinkDeptArray];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    
    NSInteger index = ((CombinationIndexedCollectionView *)collectionView).indexPath.section;
    
    NSString* groupContent = [_combinationObj.categoryList[index] objectForKey:@"groupContent"];
    NSArray* itemList = [_combinationObj.categoryList[index] objectForKey:@"itemList"];
    
    
    if ([groupContent isEqualToString:@"Two Across"])
    {
        if (itemList.count < 2) {
            return CGSizeMake(0,0);
        }
        return CGSizeMake(collectionView.frame.size.width/2 , collectionView.frame.size.height);
    }
    if ([groupContent isEqualToString:@"Three Across"]){
        
        if (itemList.count < 3) {
            return CGSizeMake(0,0);
        }
        return CGSizeMake(collectionView.frame.size.width/3 , collectionView.frame.size.height);
    }
    
    return CGSizeMake(0,0);
    
}



- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger index = ((CombinationIndexedCollectionView *)collectionView).indexPath.section;
    
    UICollectionReusableView *reusableview = nil;
    CollectionFooterReusableView* footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
    [footer.viewMoreButton addTarget:self action:@selector(listbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    footer.viewMoreButton.tag = index;
    reusableview = footer;
    
    return reusableview;
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    bannerSdImageView.hidden = true;
    [moreButton removeFromSuperview];
    [Search removeFromSuperview];
    self.scrollViewEnabled = NO;
    for (NSOperation *operation in [HTTPClient sharedHTTPClient].manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        NSLog(@"operation:%@",operation);
        AFHTTPRequestOperation *operations = (AFHTTPRequestOperation*)operation;
        if([[[[operations request] URL] absoluteString] containsString:@"band/getnewscombtemplate"]){
            [operation cancel];
        }
        
    }
    
}

- (void) didSelectRows:(NSIndexPath*)indexPath : (int) index
{
    NSString* catName = [_combinationObj.categoryList[index] objectForKey:@"catName"];
    NSArray* itemList = [_combinationObj.categoryList[index] objectForKey:@"itemList"];
    NSString* catColor = [_combinationObj.categoryList[index] objectForKey:@"catColor"];
    if (isRefresh || fetchNext)
        return;
    if((NSOrderedSame==[catName caseInsensitiveCompare:@"Photos"]) || (NSOrderedSame==[catName caseInsensitiveCompare:@"Videos"]))
    {
        NSMutableArray* toSendArray;
        if(NSOrderedSame==[catName caseInsensitiveCompare:@"Photos"])
        {
            toSendArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[itemList[indexPath.row] objectForKey:@"image"]]];
        }
        else{
            toSendArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[itemList[indexPath.row] objectForKey:@"videoLink"]]];
        }
        
        if(toSendArray.count == 0)
        {
            [UtilityManager showAlert:nil msg:@"Video not found"];
        }
        
        else if(toSendArray.count == 1)
        {
            PhotoDetailViewController *evt  = [[PhotoDetailViewController alloc]initWithNibName:@"PhotoDetailViewController" bundle:nil];
            evt.lDescription = [itemList[indexPath.row] objectForKey:@"lDesc"];
            evt.photoArray = [[NSMutableArray alloc]initWithArray:toSendArray];
            evt.catName = catName;
            evt.catColor = catColor;
             NSArray* arr  = [_combinationObj.categoryList[index] objectForKey:@"itemList"];
            evt.newsID = [arr[indexPath.row] objectForKey:@"itemID"];
            [self.navigationController pushViewController:evt animated:YES];
            
        }
        else
        {
            PhotoFolderViewController* evt = [[PhotoFolderViewController alloc] initWithNibName:@"PhotoFolderViewController" bundle:nil];
            evt.imgArray = [[NSMutableArray alloc]initWithArray:toSendArray];
            evt.longD = [itemList[indexPath.row] objectForKey:@"lDesc"];
            evt.catName = catName;
            evt.catColor = catColor;
             NSArray* arr  = [_combinationObj.categoryList[index] objectForKey:@"itemList"];
            evt.newsID = [arr[indexPath.row] objectForKey:@"itemID"];
            [self.navigationController pushViewController:evt animated:YES];
        }
    }
    
    else{
        NewsDetailViewController *newsDetail =[[NewsDetailViewController alloc] initWithNibName:@"NewsDetailViewController" bundle:nil];
        NSArray* arr  = [_combinationObj.categoryList[index] objectForKey:@"itemList"];
        newsDetail.newsID = [arr[indexPath.row] objectForKey:@"itemID"];
        [self.navigationController pushViewController:newsDetail animated:NO];
    }
    
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait;
}


-(void) fetchCategoryData : (NSString*) nextPage
{
    [HubCitiAppDelegate removeActivityIndicator];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    iWebRequestState = COMB_TEMP_REQ;
    if ([nextPage integerValue] > 0) {
        
        self.scrollViewEnabled = NO;
        fetchNext = true;
        //[HubCitiAppDelegate removeActivityIndicator];
        
        loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        loading.color = [UIColor blackColor];
        [loading startAnimating];
        CGFloat height = loading.frame.size.height + 6.f;
        containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, height)];
        containerView.backgroundColor = [UIColor clearColor];
        [containerView addSubview:loading];
        loading.center = CGPointMake(self.view.bounds.size.width / 2, height / 2.f);
        self.combTableView.tableFooterView = containerView;
        self.combTableView.userInteractionEnabled = false;
    }
    else{
        resultCategoryList = [[NSMutableArray alloc] init];
        bookMarkList = [[NSMutableArray alloc] init];
    }
    
    [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    [parameters setValue:nextPage  forKey:@"lowerLimit"];
    NSString *menulevel = [defaults valueForKey:KEY_MENULEVEL];
    [parameters setValue:menulevel  forKey:@"level"];
    int level = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    if(level>1){
        
        NSString *linkId= [[HubCitiAppDelegate getLinkIdArray]objectAtIndex:level-1];
        [parameters setValue:linkId forKey:@"linkId"];
        
    }
    else{
        
        [parameters setValue:@"0" forKey:@"linkId"];
    }
    if ([defaults objectForKey:@"ModifiedDate"]) {
        [parameters setValue:[defaults objectForKey:@"ModifiedDate"]  forKey:@"dateCreated"];
    }
    
    NSLog(@"Json = %@", parameters);
    HTTPClient* client = [HTTPClient sharedHTTPClient];
    client.delegate = self;
    
    if (!isRefresh && !fetchNext) {
        [HubCitiAppDelegate showActivityIndicator];
    }
//    NSString *urlString = [NSString stringWithFormat:@"%@band/getnewscombtemplate", @"http://10.10.221.155:9990/HubCiti2.8.2/"];
    
     NSString *urlString = [NSString stringWithFormat:@"%@band/getnewscombtemplate",BASE_URL];
    NSLog(@"URL = %@", urlString);
    
    [client sendRequest:parameters : urlString];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _combinationObj.categoryList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
}

-(void) feedNewsTapped : (id) sender
{
    if (isRefresh)
        return;
    
    UIButton* btn = (UIButton*) sender;
    NSString* feedlink = [_combinationObj.categoryList[btn.tag] objectForKey:@"nonfeedlink"];
    
    [defaults setValue:[_combinationObj.categoryList[btn.tag] objectForKey:@"backButtonColor"] forKey:@"backbuttonColor"];
    
    [defaults setValue:feedlink forKey:KEY_URL];
    
    [defaults setBool:YES forKey:@"isCombo"];
    WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
    urlDetail.title = [_combinationObj.categoryList[btn.tag] objectForKey:@"catName"];
    urlDetail.titleColor = [_combinationObj.categoryList[btn.tag] objectForKey:@"catTxtColor"];
    urlDetail.titleBackgColor = [_combinationObj.categoryList[btn.tag] objectForKey:@"catColor"];
    
    [self.navigationController pushViewController:urlDetail animated:NO];
}
-(void) listbuttonClicked : (id) sender
{
    if (isRefresh)
        return;
    
    int index = (int)[sender tag];
    
    
    if(NSOrderedSame==[[_combinationObj.categoryList[index] objectForKey:@"catName"] caseInsensitiveCompare:@"Photos"] || NSOrderedSame==[[_combinationObj.categoryList[index] objectForKey:@"catName"] caseInsensitiveCompare:@"Videos"])
    {
        PhotoView *newsDetail = [[PhotoView alloc] initWithNibName:@"PhotoView" bundle:nil];
        newsDetail.categoryName = [_combinationObj.categoryList[index] objectForKey:@"catName"];
        newsDetail.catColor = [_combinationObj.categoryList[index] objectForKey:@"catColor"];
        newsDetail.titleColor = [_combinationObj.categoryList[index] objectForKey:@"catTxtColor"];
        [self.navigationController pushViewController:newsDetail animated:NO];
    }
    else
    {
        CombinationCategoryViewController *newsDetail =[[CombinationCategoryViewController alloc] initWithNibName:@"CombinationCategoryViewController" bundle:nil];
        newsDetail.catName = [_combinationObj.categoryList[index] objectForKey:@"catName"];
        newsDetail.catColor = [_combinationObj.categoryList[index] objectForKey:@"catColor"];
        newsDetail.titleColor = [_combinationObj.categoryList[index] objectForKey:@"catTxtColor"];
        [self.navigationController pushViewController:newsDetail animated:NO];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"CellIdentifier";
    static NSString *CellIdentifierVertical = @"CellIdentifierVertical";
    //    static NSString *CellCollectionViewIdentifier = @"CellCollectionViewIdentifier";
    static NSString *NewCellIdentifier = @"NewCellIdentifier";
    static NSString *FeedCellIdentifier = @"FeedCellIdentifier";
    
    UITableViewCell *cell = nil;
    
    
    
    NSString* groupContent = [_combinationObj.categoryList[indexPath.section] objectForKey:@"groupContent"];
    
    if ([groupContent isEqualToString:@"Banner"]) {
        //cell = (BannerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        //if (!cell)
        {
            
            cell = [[BannerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
        }
        
        NSDictionary* arr = [_combinationObj.categoryList[indexPath.section] objectForKey:@"itemList"];
        ((BannerTableViewCell *)cell).bannerImageView.frame = CGRectMake(0, VARIABLE_HEIGHT(25), SCREEN_WIDTH, NEWS_HEIGHT);
        CGRect frame = ((BannerTableViewCell *)cell).bannerImageView.frame;
        
        
        ((BannerTableViewCell *)cell).transView.frame = CGRectMake(0,frame.origin.y + frame.size.height - (IPAD ?VARIABLE_HEIGHT(32) :  VARIABLE_HEIGHT(55)),SCREEN_WIDTH,IPAD ? VARIABLE_HEIGHT(32) : VARIABLE_HEIGHT(55));
        
        NSString* autor = [arr objectForKey:@"author"];
        NSString* title = [arr objectForKey:@"title"];
        
        if (autor.length == 0) {
            ((BannerTableViewCell *)cell).transView.frame = CGRectMake(0,frame.origin.y + frame.size.height - VARIABLE_HEIGHT(32),SCREEN_WIDTH,VARIABLE_HEIGHT(32));
        }
        
        if (autor.length > 0) {
            ((BannerTableViewCell *)cell).titleLbl.text = autor;
            ((BannerTableViewCell *)cell).titleLbl.font = [UIFont boldSystemFontOfSize:10];
            ((BannerTableViewCell *)cell).titleLbl.hidden = false;
            ((BannerTableViewCell *)cell).titleLbl.textColor = [UIColor lightGrayColor];
        }
        if (title.length > 0 && autor.length > 0) {
            ((BannerTableViewCell *)cell).subtitleLbl.frame = CGRectMake(5,IPAD ? 10 : 20,SCREEN_WIDTH - VARIABLE_WIDTH(30),VARIABLE_HEIGHT(32));
            ((BannerTableViewCell *)cell).subtitleLbl.font = [UIFont boldSystemFontOfSize:12];
            ((BannerTableViewCell *)cell).subtitleLbl.hidden = false;
            ((BannerTableViewCell *)cell).subtitleLbl.text = title;
            ((BannerTableViewCell *)cell).subtitleLbl.textColor = [UIColor whiteColor];
        }
        if (title.length > 0 && autor.length == 0) {
            ((BannerTableViewCell *)cell).subtitleLbl.text = title;
            ((BannerTableViewCell *)cell).subtitleLbl.font = [UIFont boldSystemFontOfSize:12];
            ((BannerTableViewCell *)cell).subtitleLbl.frame = CGRectMake(5,0,SCREEN_WIDTH - VARIABLE_WIDTH(30),VARIABLE_HEIGHT(32));
            ((BannerTableViewCell *)cell).titleLbl.hidden = true;
            ((BannerTableViewCell *)cell).subtitleLbl.textColor = [UIColor whiteColor];
        }
        
        ((BannerTableViewCell *)cell).sectionButton.frame = CGRectMake(0, 0, SCREEN_WIDTH, VARIABLE_HEIGHT(25));
        
        [((BannerTableViewCell *)cell).sectionButton addTarget:self action:@selector(listbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        ((BannerTableViewCell *)cell).sectionButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        ((BannerTableViewCell *)cell).sectionButton.contentHorizontalAlignment =  UIControlContentHorizontalAlignmentLeft;
        [((BannerTableViewCell *)cell).sectionButton setBackgroundColor:[UIColor convertToHexString:[[_combinationObj.categoryList objectAtIndex:indexPath.section] objectForKey:@"catColor"]]];
        [((BannerTableViewCell *)cell).sectionButton setTitle:[[_combinationObj.categoryList[indexPath.section] objectForKey:@"catName"] uppercaseString]forState:UIControlStateNormal];
        
        NSString* textColor =[[_combinationObj.categoryList objectAtIndex:indexPath.section] objectForKey:@"catTxtColor"];
        
        [((BannerTableViewCell *)cell).sectionButton setTitleColor: (textColor.length ? [UIColor convertToHexString: textColor] : [UIColor whiteColor] )forState:UIControlStateNormal];
        ((BannerTableViewCell *)cell).sectionButton.tag = indexPath.section;
        
        ((BannerTableViewCell *)cell).PlayIcon.frame = CGRectMake((SCREEN_WIDTH - 40)/2, (((BannerTableViewCell *)cell).bannerImageView.frame.size.height - 40)/2 , 40, 40);
        
        
        NSString* catname = [[_combinationObj.categoryList[indexPath.section] objectForKey:@"catName"] uppercaseString];
        
        if ([catname isEqualToString:@"VIDEOS"]){
            NSString* url = [[arr objectForKey:@"videoLink"] componentsSeparatedByString:@","][0];
            
            ((BannerTableViewCell *)cell).bannerImageView.backgroundColor = [UIColor blackColor];
            
            ((BannerTableViewCell *)cell).bannerImageView.contentMode = UIViewContentModeScaleToFill;
            
            if ([imageDictionary objectForKey:url]) {
                
                ((BannerTableViewCell *)cell).bannerImageView.image = [imageDictionary objectForKey:url];
                ((BannerTableViewCell *)cell).PlayIcon.hidden = false;
                ((BannerTableViewCell *)cell).PlayIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
                [((BannerTableViewCell *)cell).bannerImageView addSubview:((BannerTableViewCell *)cell).PlayIcon];
                
            }
            else{
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                    
                    AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:url]];
                    
                    CMTime time = CMTimeMakeWithSeconds(3, 2);
                    
                    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                    imageGenerator.appliesPreferredTrackTransform = YES;
                    
                    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
                    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
                    CGImageRelease(imageRef);
                    if (thumbnail) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            ((BannerTableViewCell *)cell).bannerImageView.image = thumbnail;
                            [imageDictionary setObject:thumbnail forKey:url];
                            ((BannerTableViewCell *)cell).PlayIcon.hidden = false;
                            ((BannerTableViewCell *)cell).PlayIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
                            [((BannerTableViewCell *)cell).bannerImageView addSubview:((BannerTableViewCell *)cell).PlayIcon];
                            
                        });
                    }
                    
                });
            }
            
        }
        else{
            
            ((BannerTableViewCell *)cell).PlayIcon.hidden = TRUE;
            SdImageView *asyncImageView ;
            asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , NEWS_HEIGHT)];
            asyncImageView.contentMode = UIViewContentModeScaleAspectFit;
            NSString* imageLink = [[arr objectForKey:@"image"] componentsSeparatedByString:@","][0];
            
            [asyncImageView loadImage:imageLink];
            [((BannerTableViewCell *)cell).bannerImageView addSubview:asyncImageView];
        }
        
        
    }
    else if ([groupContent isEqualToString:@"List"]) {
        MainTableViewCell  * cellVertical;
        // cellVertical = (MainTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifierVertical];
        // if (cellVertical == nil)
        {
            cellVertical =  [[MainTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierVertical];
            
        }
        
        
        NSArray* itemList = [_combinationObj.categoryList[indexPath.section] objectForKey:@"itemList"];
        
        NSArray* dict = [_combinationObj.categoryList copy];
        [defaults setValue:dict forKey:@"List"];
        cellVertical.selectedIndex = indexPath.section;
        
        cellVertical.delegate = self;
        if (itemList.count > 3 ) {
            cellVertical.mainTableView.tableFooterView.hidden = FALSE;
            
            cellVertical.mainTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, IPAD ? 3*VERTICAL_CELL_HEIGHT_IPAD : 3*VERTICAL_CELL_HEIGHT_IPHONE);
        }
        else
        {
            cellVertical.mainTableView.tableFooterView.hidden = FALSE;
            cellVertical.mainTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, IPAD ? (itemList.count)*VERTICAL_CELL_HEIGHT_IPAD : (itemList.count)*VERTICAL_CELL_HEIGHT_IPHONE);
        }
        
        
        
        cell = cellVertical;
        cellVertical.tag = indexPath.section;
        cellVertical.moreButton.tag = indexPath.section;
        [cellVertical.moreButton addTarget:self action:@selector(listbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    else if ([groupContent isEqualToString:@"Two Across"] || [groupContent isEqualToString:@"Three Across"] )
    {
        cell = (NewCombinationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:NewCellIdentifier forIndexPath:indexPath];
        
        
        
        // if (!cell) {
        
        
        //   cell = [[CombinationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellCollectionViewIdentifier];
        
        // ((NewCombinationTableViewCell *)cell).sectionButton.frame = CGRectMake(0, 0, SCREEN_WIDTH, VARIABLE_HEIGHT(25));
        //  ((NewCombinationTableViewCell *)cell).frame = CGRectMake(0, VARIABLE_HEIGHT(25), SCREEN_WIDTH,  144 + 50 +VARIABLE_HEIGHT(25));
        
        [((NewCombinationTableViewCell *)cell).sectionButton addTarget:self action:@selector(listbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        ((NewCombinationTableViewCell *)cell).sectionButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        ((NewCombinationTableViewCell *)cell).sectionButton.contentHorizontalAlignment =  UIControlContentHorizontalAlignmentLeft;
        
        UIImageView *_arrowRight = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - VARIABLE_WIDTH(25), 4 , VARIABLE_WIDTH(20), VARIABLE_HEIGHT(16))];
        
        _arrowRight.image = [UIImage imageNamed:@"rightArrow.png"];
        
        [((NewCombinationTableViewCell *)cell).sectionButton addSubview:_arrowRight];
        
        [((NewCombinationTableViewCell *)cell).sectionButton setBackgroundColor:[UIColor convertToHexString:[[_combinationObj.categoryList objectAtIndex:indexPath.section] objectForKey:@"catColor"]]];
        [((NewCombinationTableViewCell *)cell).sectionButton setTitle: [[_combinationObj.categoryList[indexPath.section] objectForKey:@"catName"] uppercaseString] forState:UIControlStateNormal];
        
        NSString* textColor =[[_combinationObj.categoryList objectAtIndex:indexPath.section] objectForKey:@"catTxtColor"];
        
        [((NewCombinationTableViewCell *)cell).sectionButton setTitleColor:(textColor.length ? [UIColor convertToHexString: textColor] : [UIColor whiteColor] ) forState:UIControlStateNormal];
        ((NewCombinationTableViewCell *)cell).sectionButton.tag = indexPath.section ;
        
        //  }
        
        
    }
    else{
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:FeedCellIdentifier];
        
        UIButton* sectionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        sectionButton.frame = CGRectMake(0, 0, SCREEN_WIDTH, VARIABLE_HEIGHT(25));
        [sectionButton addTarget:self action:@selector(feedNewsTapped:) forControlEvents:UIControlEventTouchUpInside];
        sectionButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        sectionButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        sectionButton.contentHorizontalAlignment =  UIControlContentHorizontalAlignmentLeft;
        [sectionButton setTitle:[[_combinationObj.categoryList[indexPath.section] objectForKey:@"catName"] uppercaseString] forState:UIControlStateNormal];
        NSString* textColor =[[_combinationObj.categoryList objectAtIndex:indexPath.section] objectForKey:@"catTxtColor"];
        [sectionButton setBackgroundColor:[UIColor convertToHexString:[[_combinationObj.categoryList objectAtIndex:indexPath.section] objectForKey:@"catColor"]]];
        
        [sectionButton setTitleColor:(textColor.length ? [UIColor convertToHexString: textColor] : [UIColor whiteColor] ) forState:UIControlStateNormal];
        sectionButton.tag = indexPath.section ;
        UIImageView *_arrowRight = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - VARIABLE_WIDTH(25), 4 , VARIABLE_WIDTH(20), VARIABLE_HEIGHT(16))];
        
        _arrowRight.image = [UIImage imageNamed:@"rightArrow.png"];
        
        [sectionButton addSubview:_arrowRight];
        
        [cell.contentView addSubview:sectionButton];
        
    }
    
    
    return cell;
}

-(void) moreButtonClicked : (id) sender
{
    if (isRefresh)
        return;
    
    int index = (int)[sender tag];
    
    CombinationCategoryViewController *newsDetail =[[CombinationCategoryViewController alloc] initWithNibName:@"CombinationCategoryViewController" bundle:nil];
    newsDetail.catName = [_combinationObj.categoryList[index] objectForKey:@"catName"];
    newsDetail.catColor = [_combinationObj.categoryList[index] objectForKey:@"catColor"];
    newsDetail.titleColor = [_combinationObj.categoryList[index] objectForKey:@"catTxtColor"];
    [self.navigationController pushViewController:newsDetail animated:NO];
    
}

/*-(void)tableView:(UITableView *)tableView didEndDisplayingCell:cell forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 NSString* groupContent = [_combinationObj.categoryList[indexPath.section] objectForKey:@"groupContent"];
 
 if (([groupContent isEqualToString:@"Two Across"] || [groupContent isEqualToString:@"Three Across"]) && [cell isKindOfClass:[NewCombinationTableViewCell class]]) {
 
 NSInteger index = ((NewCombinationTableViewCell *)cell).collectionView.indexPath.row;
 
 CGFloat horizontalOffset = ((NewCombinationTableViewCell *)cell).collectionView.contentOffset.x;
 self.contentOffsetDictionary[[@(index) stringValue]] = @(horizontalOffset);
 
 }
 
 }*/


-(void)tableView:(UITableView *)tableView willDisplayCell:cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* groupContent = [_combinationObj.categoryList[indexPath.section] objectForKey:@"groupContent"];
    
    if (([groupContent isEqualToString:@"Two Across"] || [groupContent isEqualToString:@"Three Across"]) && [cell isKindOfClass:[NewCombinationTableViewCell class]]) {
        [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
        
        // NSInteger index = ((NewCombinationTableViewCell *)cell).collectionView.indexPath.row;
        
        // CGFloat horizontalOffset = [self.contentOffsetDictionary[[@(index) stringValue]] floatValue];
        
        [((NewCombinationTableViewCell *)cell).collectionView setContentOffset: CGPointMake(0, 0)];
        
    }
    
    
}


#pragma mark - UITableViewDelegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* groupContent = [_combinationObj.categoryList[indexPath.section] objectForKey:@"groupContent"];
    NSArray* itemList = [_combinationObj.categoryList[indexPath.section] objectForKey:@"itemList"];
    if ([groupContent isEqualToString:@"Banner"])
    {
        return NEWS_HEIGHT + VARIABLE_HEIGHT(25);
    }
    else if ([groupContent isEqualToString:@"List"])
    {
        if (itemList.count > 3)
        {
            return  IPAD ? 3*VERTICAL_CELL_HEIGHT_IPAD :  3*VERTICAL_CELL_HEIGHT_IPHONE;
        }
        
        return  IPAD ? (itemList.count)*VERTICAL_CELL_HEIGHT_IPAD :  (itemList.count)*VERTICAL_CELL_HEIGHT_IPHONE;
    }
    else if ([groupContent isEqualToString:@"Two Across"] || [groupContent isEqualToString:@"Three Across"])
    {
        return  IPAD ? 265 : 200 ;
    }
    else{
        return VARIABLE_HEIGHT(25);
    }
}


#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger index = ((CombinationIndexedCollectionView *)collectionView).indexPath.section;
    return [[_combinationObj.categoryList[index] objectForKey:@"itemList"]  count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = ((CombinationIndexedCollectionView *)collectionView).indexPath.section;
    
    NSString* groupContent = [_combinationObj.categoryList[index] objectForKey:@"groupContent"];
    
    if ([groupContent isEqualToString:@"Two Across"])
    {
        return CGSizeMake(collectionView.frame.size.width/2 , collectionView.frame.size.height);
    }
    if ([groupContent isEqualToString:@"Three Across"]){
        
        return CGSizeMake(collectionView.frame.size.width/3 , collectionView.frame.size.height );
    }
    
    return CGSizeMake(0,0);
}
-(NSArray *)convertToArrayFromCommaSeparated:(NSString*)string{
    return [string componentsSeparatedByString:@","];
}

-(void) callSubView : (UICollectionView *)collectionView : (NSIndexPath*) indexPath
{
    NSInteger index = ((CombinationIndexedCollectionView *)collectionView).indexPath.section;
    NSArray* itemList = [_combinationObj.categoryList[index] objectForKey:@"itemList"];
    NSString* catColor = [_combinationObj.categoryList[index] objectForKey:@"catColor"];
    NSString* titleColor = [itemList[indexPath.row] objectForKey:@"titleColor"];
    NSString* catName = [itemList[indexPath.row] objectForKey:@"catName"];
    
    
    NSMutableArray* toSendArray;
    if(NSOrderedSame==[catName caseInsensitiveCompare:@"Photos"])
    {
        toSendArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[itemList[indexPath.row] objectForKey:@"image"]]];
    }
    else{
        toSendArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[itemList[indexPath.row] objectForKey:@"videoLink"]]];
    }
    
    if(toSendArray.count == 0)
    {
        [UtilityManager showAlert:nil msg:@"Video not found"];
    }
    else if(toSendArray.count == 1)
    {
        PhotoDetailViewController *evt  = [[PhotoDetailViewController alloc]initWithNibName:@"PhotoDetailViewController" bundle:nil];
        evt.lDescription = [itemList[indexPath.row] objectForKey:@"lDesc"];
        evt.photoArray = [[NSMutableArray alloc]initWithArray:toSendArray];
        evt.catName = catName;
        evt.titleColor = titleColor;
        evt.catColor = catColor;
        
        [self.navigationController pushViewController:evt animated:YES];
        
    }
    else
    {
        PhotoFolderViewController* evt = [[PhotoFolderViewController alloc] initWithNibName:@"PhotoFolderViewController" bundle:nil];
        evt.imgArray = [[NSMutableArray alloc]initWithArray:toSendArray];
        evt.longD = [itemList[indexPath.row] objectForKey:@"lDesc"];
        evt.catName = catName;
        evt.titleColor = titleColor;
        evt.catColor = catColor;
        
        
        [self.navigationController pushViewController:evt animated:YES];
    }
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (isRefresh)
        return;
    NSLog(@"clicked");
    NSLog(@"indexpathRow = %ld", (long) indexPath.row);
    NSInteger index = ((CombinationIndexedCollectionView *)collectionView).indexPath.section;
    NSString* catName = [_combinationObj.categoryList[index] objectForKey:@"catName"];
    
    if((NSOrderedSame==[catName caseInsensitiveCompare:@"Photos"]) || (NSOrderedSame==[catName caseInsensitiveCompare:@"Videos"]))
    {
        [self callSubView : collectionView : indexPath];
    }
    
    else{
        NewsDetailViewController *newsDetail =[[NewsDetailViewController alloc] initWithNibName:@"NewsDetailViewController" bundle:nil];
        NSArray* arr  = [_combinationObj.categoryList[index] objectForKey:@"itemList"];
        newsDetail.newsID = [arr[indexPath.row] objectForKey:@"itemID"];
        [self.navigationController pushViewController:newsDetail animated:NO];
    }
    
    
    
}


-(CGFloat) getLabelSize : (NSString*) labelString withSize:(CGFloat) labelwidth withFont:(UIFont *) font
{
    
    
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    float noOflines = ceil(rect.size.height / font.lineHeight);
    
    
    return ceil(font.lineHeight*noOflines);
    
}

-(NSString *)convertHTML:(NSString *)html {
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CombinationCollectionCell*cell = (CombinationCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    
    NSInteger index = ((CombinationIndexedCollectionView *)collectionView).indexPath.section;
    
    NSArray* itemlist = [_combinationObj.categoryList[index] objectForKey:@"itemList"];
    //  @"http://hdwpro.com/wp-content/uploads/2016/06/Sunrise-Amazing-Wallpaper.jpeg";
    NSString* url;
    
    if ([[[_combinationObj.categoryList[index] objectForKey:@"catName"] uppercaseString] isEqualToString:@"VIDEOS"])
    {
        
        cell.icon.backgroundColor = [UIColor blackColor];
        cell.icon.contentMode = UIViewContentModeScaleToFill;
        cell.playIcon.hidden = false;
        url = [[[itemlist objectAtIndex:indexPath.row] objectForKey:@"videoLink"] componentsSeparatedByString:@","][0];
        if ([imageDictionary objectForKey:url]) {
            
            cell.icon.image = [imageDictionary objectForKey:url];
            cell.playIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
        }
        else
        {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                
                
                AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:url]];
                
                //  Get thumbnail at the very start of the video
                CMTime time = CMTimeMake(3, 2);
                
                
                //  Get image from the video at the given time
                AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                imageGenerator.appliesPreferredTrackTransform = YES;
                
                CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
                UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
                CGImageRelease(imageRef);
                
                if(thumbnail){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        cell.icon.image = thumbnail;
                        cell.playIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
                        [imageDictionary setObject:thumbnail forKey:url];
                        
                    });
                    
                }
            });
        }
        
        
    }
    else{
        cell.playIcon.hidden = true;
        url = [[[itemlist objectAtIndex:indexPath.row] objectForKey:@"image"] componentsSeparatedByString:@","][0];
        cell.icon.contentMode = UIViewContentModeScaleAspectFit;
        [cell.icon loadImage:url];
    }
    
    NSString* autor = [[itemlist objectAtIndex:indexPath.row] objectForKey:@"author"];
    NSString* title = [[itemlist objectAtIndex:indexPath.row] objectForKey:@"title"];
    NSString* subTitle = [[itemlist objectAtIndex:indexPath.row] objectForKey:@"sDesc"];
    
   
    
    cell.title.textAlignment = NSTextAlignmentLeft;
    cell.subTitle.textAlignment = NSTextAlignmentLeft;
    
  
    if (autor.length > 0) {
        cell.title.hidden = false;
        cell.title.font = [UIFont boldSystemFontOfSize:10];
        cell.title.numberOfLines = 1;
        [cell.title setText:autor];
        cell.title.textColor = [UIColor grayColor];
    }
    if (title.length > 0 && autor.length > 0) {
        cell.subTitle.text = title;
        cell.subTitle.font = [UIFont boldSystemFontOfSize:12];
        cell.subTitle.numberOfLines = 2;
        cell.subTitle.hidden = false;
        cell.subTitle.textColor = [UIColor blackColor];
    }
    if (title.length > 0 && autor.length == 0) {
        cell.title.text = title;
        cell.title.numberOfLines = 2;
        cell.title.font = [UIFont boldSystemFontOfSize:12];
        cell.subTitle.hidden = false;
        cell.title.textColor = [UIColor blackColor];
        
        cell.subTitle.text = [self convertHTML:subTitle];
        cell.subTitle.font = [UIFont systemFontOfSize:12];
        cell.subTitle.textColor = [UIColor blackColor];
        cell.subTitle.numberOfLines = 1;

    }
    
    
    
    return cell;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout
                                                                     *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 1.0;
}
/*
 - (UIEdgeInsets)collectionView:
 (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
 
 return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
 }
 */


#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    lastContentOffset = scrollView.contentOffset;
}
- (void) scrollViewDidEndDecelerating: (UIScrollView *) scrollView
{
    lastContentOffset = scrollView.contentOffset;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ((scrollView.contentOffset.x != lastContentOffset.x) && scrollView == buttonScroll)
    {
        buttonScroll.frame = CGRectMake(VARIABLE_WIDTH(3), 0, SCREEN_WIDTH - VARIABLE_WIDTH(6), VARIABLE_HEIGHT(30));
    }
    
    
    if ([scrollView isKindOfClass:[UICollectionView class]]) {
        
        CGFloat horizontalOffset = scrollView.contentOffset.x;
        
        CombinationIndexedCollectionView *collectionView = (CombinationIndexedCollectionView *)scrollView;
        NSInteger index = collectionView.indexPath.row;
        self.contentOffsetDictionary[[@(index) stringValue]] = @(horizontalOffset);
    }
    else{
        if(scrollView != buttonScroll)
        {
            if (!self.scrollViewEnabled) {
                return;
            }
            
            CGSize contentSize = scrollView.contentSize;
            CGPoint contentOffset = scrollView.contentOffset;
            if (contentSize.height < SCREEN_HEIGHT && !_combinationObj.nextPage) {
                return;
            }
            if ((contentOffset.y + scrollView.bounds.size.height > contentSize.height) && _combinationObj.nextPage) {
                [self fetchCategoryData :_combinationObj.lowerLimitFlag];
            }
        }
        
    }
}

#pragma mark - HTTPClient Methods



- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    
    [self responseData:responseObject];
    
    
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error
{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
    
    if (isRefresh) {
        
        isRefresh = false;
        [refreshControl endRefreshing];
    }
    
    if (fetchNext) {
        
        [self hideFooter];
    }
    self.scrollViewEnabled = TRUE;
    
}

-(void)responseData:(id) responseObject
{
    switch (iWebRequestState)
    {
        case COMB_TEMP_REQ:
        {
            [self parseResponse:responseObject];
        }
            break;
            
        default:
            break;
    }
}
-(void) hideFooter
{
    fetchNext = false;
    [loading stopAnimating];
    [containerView removeFromSuperview];
    self.scrollViewEnabled = false;
    self.combTableView.tableFooterView = nil;
    self.combTableView.userInteractionEnabled = TRUE;
}
-(void) parseResponse : (id) responseObject
{
    if (!fetchNext) {
        
        [self.combTableView setContentOffset:CGPointMake(0, 0)];
    }
    
    if (isRefresh) {
        
        
        isRefresh = false;
        [refreshControl endRefreshing];
    }
    
    if (fetchNext) {
        
        [self hideFooter];
    }
    
    
    else
    {
        NSString* tempType = [responseObject objectForKey:@"newtempName"];
        
        if(!([tempType isEqualToString:@"Combination News Template"] && [responseObject objectForKey:@"templateChanged"] == [NSNumber numberWithBool:1]))
        {
            [HubCitiAppDelegate removeActivityIndicator];
        }
    }
    NSString* responseCode = [responseObject objectForKey:@"responseCode"];
    
    if([responseCode isEqualToString:@"10000"])
    {
        _combinationObj = [[CombinationObj alloc] init];
        [_combinationObj setValuesForKeysWithDictionary:responseObject];
        
        @try{
            [_combinationObj setValuesForKeysWithDictionary:responseObject];
        }
        @catch (NSException *exception) {
            // Do nothing
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
            return;
        }
       
        
        if(_combinationObj.templateChanged)
        {
            if(_combinationObj.newtempName)
            {
                [defaults setInteger:0 forKey:@"currentIndex"];
                [defaults setValue:nil forKey:@"bannerImgScroll"];
                [defaults setValue:nil forKey:@"bannerImgScroll"];
                [defaults setInteger:0 forKey:@"currentIndexSubPage"];
                
                if([_combinationObj.newtempName isEqualToString:@"Combination News Template"] ){
                    
                    
                    [defaults setValue:@"Combination News Template" forKey:@"selectedTemplateName"];
                    //  [self getSideNav];
                    [HubCitiAppDelegate removeActivityIndicator];
                    [self parseCombinationData : responseObject];
                    
                    return;
                }
                else if([_combinationObj.newtempName isEqualToString:@"News Tile Template"] )
                {
                    [cusNav hideBackButton:YES];
                    [cusNav hideHambergerButton: YES];
                    [defaults setValue:nil forKey:@"ModifiedDate"];
                    [defaults setValue:@"News Tile Template" forKey:@"selectedTemplateName"];
                    [NewsUtility setNewsTemplateAsRoot];
                    return;
                    
                }
                else if([_combinationObj.newtempName isEqualToString:@"Scrolling News Template"] ){
                    [cusNav hideBackButton:YES];
                    [cusNav hideHambergerButton: YES];
                    [defaults setValue:nil forKey:@"ModifiedDate"];
                    [defaults setValue:@"Scrolling News Template" forKey:@"selectedTemplateName"];
                    [NewsUtility setNewsTemplateAsRoot];
                    return;
                }
                else{
                    [cusNav hideBackButton:YES];
                    [cusNav hideHambergerButton: YES];
                    [defaults setValue:nil forKey:@"ModifiedDate"];
                    [defaults setValue:@"HubCiti" forKey:@"selectedTemplateName"];
                    [defaults setBool:NO forKey:@"newsTemplateExist"];
                    
                    NSArray *keys = [cashedResponse allKeys];
                    for (int i = 0 ; i < [keys count]; i++)
                    {
                        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
                            [cashedResponse removeObjectForKey:keys[i]];
                        }
                        
                    }
                    
                    ReleaseAndNilify(selectedSortOption);
                    ReleaseAndNilify(selectedFilterOption);
                    ReleaseAndNilify(selectedSortCityDic);
                    ReleaseAndNilify(typeId);
                    ReleaseAndNilify(deptId);
                    
                    [NewsUtility setLoginVCAsRoot];
                    return;
                }
            }
        }
        
        [self parseCombinationData : responseObject];
    }
    
    if([responseCode isEqualToString:@"10001"])
    {
        if (_combinationObj.hamburgerImg)
        {
            if (![[defaults valueForKey:@"hamburgerImg"] isEqualToString:_combinationObj.hamburgerImg])
            {[defaults setValue:_combinationObj.hamburgerImg forKey:@"hamburgerImg"];
                [cusNav changeHambugerImage];
            }
            
        }
        NSString *response= [responseObject objectForKey:@"responseText"];
        [UtilityManager showAlert:nil msg:response];
        
        return;
        
    }
    
    else if([responseCode isEqualToString:@"10002"])
    {
        if (_combinationObj.hamburgerImg)
        {
            if (![[defaults valueForKey:@"hamburgerImg"] isEqualToString:_combinationObj.hamburgerImg])
            {
                [defaults setValue:_combinationObj.hamburgerImg forKey:@"hamburgerImg"];
                [cusNav changeHambugerImage];
            }
            
        }
        NSString *response= [responseObject objectForKey:@"responseText"];
        [UtilityManager showAlert:nil msg:response];
        
        return;
        
    }
    else if([responseCode isEqualToString:@"10005"])
    {
        if (_combinationObj.hamburgerImg)
        {
            if (![[defaults valueForKey:@"hamburgerImg"] isEqualToString:_combinationObj.hamburgerImg])
            {
                [defaults setValue:_combinationObj.hamburgerImg forKey:@"hamburgerImg"];
                [cusNav changeHambugerImage];
            }
            
        }
        NSString *response= [responseObject objectForKey:@"responseText"];
        
        if ([response isEqualToString:@"null"]) {
            
            response = @"No Records Found.";
            
        }
        if([response isEqualToString:@"No Records Found."])
        {
            [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:response];
        }
        else{
            
            NSString *response= [responseObject objectForKey:@"responseText"];
            NSString *responseTextStr = [response stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
            
            [UtilityManager showFormatedAlert:responseTextStr];
        }
    }
    
    
}

-(void) parseCombinationData : (id) responseObject
{
    
    NSMutableArray* categoryList = [[NSMutableArray alloc] init];
    NSMutableArray* bookMarkCategory= [[NSMutableArray alloc] init];
    
    NSArray* catList = [responseObject objectForKey:@"categoryList"];
    
    
    for(int i = 0; i< catList.count;i++) {
        
        
        
        if ([[catList[i] objectForKey:@"groupContent"] isEqualToString:@"List"]) {
            
            NSMutableArray *itemList = [[catList[i] objectForKey:@"itemList"] mutableCopy];
            NSMutableDictionary* category = [[NSMutableDictionary alloc] init];
            NSMutableDictionary *bannerDictionary = [itemList.firstObject mutableCopy];
            [category setValue:[catList[i] objectForKey:@"catName"] forKey:@"catName"];
            [category setValue:[catList[i] objectForKey:@"catColor"] forKey:@"catColor"];
            [category setValue:[catList[i] objectForKey:@"catTxtColor"] forKey:@"catTxtColor"];
            [category setValue:@"Banner" forKey:@"groupContent"];
            [category setValue:bannerDictionary forKey:@"itemList"];
            
            [categoryList addObject:category];
            
            
        }
        [bookMarkCategory addObject:catList[i]];
        [categoryList addObject:catList[i]];
        [buttonArray addObject:[catList[i] objectForKey:@"catName"]];
    }
    for(int i = 0; i< categoryList.count;i++) {
        
        NSMutableDictionary *listDictionary = [categoryList[i] mutableCopy];
        if ([[listDictionary objectForKey:@"groupContent"] isEqualToString:@"List"]) {
            NSMutableArray *itemList = [[listDictionary objectForKey:@"itemList"] mutableCopy];
            [itemList removeObjectAtIndex:0];
            [listDictionary setValue:itemList forKey:@"itemList"];
            
            categoryList[i] = listDictionary ;
            
        }
        [resultCategoryList addObject:categoryList[i]];
        
        
    }
    
    for(int i = 0; i< bookMarkCategory.count;i++) {
        [bookMarkList addObject:bookMarkCategory[i]];
    }
    
    _combinationObj.bookMarkList = [bookMarkList copy];
    _combinationObj.categoryList = [resultCategoryList copy];
    
   
    
    if (_combinationObj.bkImgPath)
    {
        if (![[defaults valueForKey:@"bkImgPath"] isEqualToString:_combinationObj.bkImgPath])
        {
            [cusNav changeBackImage:_combinationObj.bkImgPath];
        }
        [defaults setValue:_combinationObj.bkImgPath forKey:@"bkImgPath"];
    }
    if(_combinationObj.titleTxtColor)
    {
        [defaults setValue:_combinationObj.titleTxtColor forKey:@"titleTxtColor"];
    }
    if(_combinationObj.titleBkGrdColor)
    {
        [defaults setValue:_combinationObj.titleBkGrdColor forKey:@"titleBkGrdColor"];
    }
    if(_combinationObj.homeImgPath)
    {
        [defaults setValue:_combinationObj.homeImgPath forKey:@"homeImgPath"];
    }
    if (_combinationObj.hamburgerImg)
    {
        if (![[defaults valueForKey:@"hamburgerImg"] isEqualToString:_combinationObj.hamburgerImg])
        {
             [defaults setValue:_combinationObj.hamburgerImg forKey:@"hamburgerImg"];
            [cusNav changeHambugerImage];
        }
       
    }
    
    [defaults setObject:_combinationObj.modifiedDate forKey:@"ModifiedDate"];
    [self setNavigationBar];
    [self.combTableView reloadData];
    self.scrollViewEnabled = true;
    [self setUpTopTabBar];
    
    
}



//- (void)showResetPasswordAlert:(NSNotification *)notification {
//    [[CommonUtilityNews utilityManagers] tempPasswordDisplay:notification onViewController:self];
//
//}
//- (void)showEmailCountAlert:(NSNotification *)notification
//{
//    [[CommonUtilityNews utilityManagers] emailCountDisplay:notification onViewController:self];
//
//}


#pragma mark functions for popup for the first time
-(void)checkLocationStatus{
    
    
    
    if([defaults integerForKey:@"AppCounter"] == 1)
        
    {
        
        // If Devive Level GPS and App Level GPS both ON
        
        if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [[defaults valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
            
        {
            
//            [SharedManager setGps_allow_flag:YES];
//            
//            [defaults setValue:@"YES" forKey:@"allowingGPS"];
            
            [Location updateLocationinfo:self];
            
        }
        
        // IF Device Level GPS ON but Application Level GPS OFF
        
        else if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [[defaults valueForKey:@"allowingGPS"] isEqualToString:@"NO"])
            
        {
            
            [self gpsAllowDontAllowPopup];
            
            
        }
        
        // IF Application level GPS off
        
        else if ([[defaults valueForKey:@"allowingGPS"] isEqualToString:@"NO"]) {
            
            
            
            [self gpsAllowDontAllowPopup];
        }
        
    }
    
    
    
    else{
        
        
        
        [defaults setInteger:1 forKey:@"AppCounter"];
        
        if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [CLLocationManager locationServicesEnabled])
            
        {
            
//            [SharedManager setGps_allow_flag:YES];
//            
//            [defaults setValue:@"YES" forKey:@"allowingGPS"];
//            
            [Location updateLocationinfo:self];
            
        }
        
        else {
            
//            [SharedManager setGps_allow_flag:NO];
//            
//            [defaults setValue:@"NO" forKey:@"allowingGPS"];
            
            [self checkForZipCode];
            
        }
        
    }
    
}

-(void)checkForRadius

{
    
    NSString *reqStr = [NSString stringWithFormat:@"<AuthenticateUser><deviceID>%@</deviceID><userId>%@</userId><hubCitiId>%@</hubCitiId></AuthenticateUser>",[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusersettings",BASE_URL];
   
    
    
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:reqStr];
    
    
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    
    TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
    
    if (responseCodeElement != nil && ![[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        
        
        return;
        
    }
    
    
    
    TBXMLElement *localeRadiusElement = [TBXML childElementNamed:@"localeRadius" parentElement:tbxml.rootXMLElement];
    TBXMLElement *bLocServiceElement = [TBXML childElementNamed:@"bLocService" parentElement:tbxml.rootXMLElement];
    if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"false"])
    {
        [SharedManager setGps_allow_flag:NO];
        [defaults setValue:@"NO" forKey:@"allowingGPS"];
    }
    else if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"true"])
    {
        [SharedManager setGps_allow_flag:YES];
        [defaults setValue:@"YES" forKey:@"allowingGPS"];
    }

    
    
    
    NSString *userRadius = [TBXML textForElement:localeRadiusElement];
    
    if(userRadius.length > 0)
        
    {
        
        [defaults setValue:userRadius forKey:@"FindRadius"];
        
        NSLog(@"my radius is %@",[defaults valueForKey:@"FindRadius"]);
        
    }
}
-(void) gpsAllowDontAllowPopup{
    
    
    UIAlertController * alert;
    alert=[UIAlertController alertControllerWithTitle:nil message:@"Application uses your current location to provide information about retailers and products near you. Do you wish to allow to access your location?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* allow = [UIAlertAction
                            actionWithTitle:@"Allow"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action){
                                //Ajit's edit
                               // [SharedManager setGps_allow_flag:YES];
                                [NewsUtility setGPSAsAllowForUser];
                                
                                //If LS Allowed, but LS are off
                                if ([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) {
                                    [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
                                    
                                }
                                [Location updateLocationinfo:self];
                                //[defaults setValue:@"YES" forKey:@"allowingGPS"];
                                
                            }];
    [alert addAction:allow];
    
    UIAlertAction* dontAllow = [UIAlertAction
                                actionWithTitle:@"Don't Allow"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action){
                                   // [SharedManager setGps_allow_flag:NO];
                                    [defaults setValue:@"" forKey:KEY_LATITUDE];//reset the values
                                    [defaults setValue:@"" forKey:KEY_LONGITUDE];
                                    [self checkForZipCode];
                                   // [defaults setValue:@"NO" forKey:@"allowingGPS"];
                                    [alert dismissViewControllerAnimated:NO completion:nil];
                                }];
    [alert addAction:dontAllow];
    
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
    
}

//zip code checking
-(void)checkForZipCode

{
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/fetchuserlocationpoints?userId=%@",BASE_URL,[defaults objectForKey:KEY_USERID]];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
    
    
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml] ;
    
    TBXMLElement *postalCodeElement = [TBXML childElementNamed:@"postalCode" parentElement:tbxml.rootXMLElement];
    
    
    
    if (postalCodeElement == nil) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
        
        int responseCode = [[TBXML textForElement:[TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement]] intValue];
        
        if (responseCode == 10005) {
            
            //If The User Doesnt Have a Zip on file, Ask them if they want to enter one
            
            UIAlertController * alert; //enter a zipcode alert
            alert=[UIAlertController alertControllerWithTitle:nil message:@"Would you like to enter a zipcode for us to use instead?" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* no = [UIAlertAction
                                 actionWithTitle:@"No"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     //Warn them that they are completely off the grid as far as ScanSee is concerned
                                     [defaults setValue:nil forKey:KEYZIPCODE];
                                     [UtilityManager showAlert:nil msg:@"You have chosen not to search by your Zipcode. If you would like to search by Zipcode in the future, please go to the User Information screen."];
                                 }];
            [alert addAction:no];
            UIAlertAction* yes = [UIAlertAction
                                  actionWithTitle:@"Yes"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action){
                                      [self showZipEnter];
                                  }];
            [alert addAction:yes];
            
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
            
            
            return;
            
            
            
            
            
            ReleaseAndNilify(responseXml);
            
            return;
            
        }
        
        else {
            
            // Banner the message given by the server
            
            [UtilityManager showAlert:@"Info" msg:responseTextStr];
            
            //[tbxml release];
            
            ReleaseAndNilify(responseXml);
            
            return;
            
        }
        
    }
    
    
    
    NSString *postalCodeStr = [TBXML textForElement:postalCodeElement];
    
    
    
    //Sets the ZipCode to the key for use in other view controllers
    
    [defaults setValue:postalCodeStr forKey:KEYZIPCODE];
    
    
    
    //set for Austin Experience req when clicked from main menu
    
    [defaults setObject:postalCodeStr forKey:@"postalCode"];
    
    
    
    //[tbxml release];
    
    ReleaseAndNilify(responseXml);
    
    //[self getMainMenuItems];
    
    return;
    
}



//Alert that shows a Zip Enter Box

-(void)showZipEnter
{
    
    // use UIAlertController
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Please enter your zip code"
                               message:nil
                               preferredStyle:UIAlertControllerStyleAlert];
    
    zipSave = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action){
                                         //Do Some action here
                                         UITextField *textField = alert.textFields[0];
                                         if ([textField.text length])
                                             [self postZip:textField.text];
                                         else
                                             return;
                                         
                                     }];
    zipSave.enabled = NO;
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       NSLog(@"cancel btn");
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       [defaults setValue:nil forKey:KEYZIPCODE];
                                                       
                                                       [UtilityManager showAlert:nil msg:@"You have chosen not to search by your Zipcode. If you would like to search by Zipcode in the future, please go to the User Information screen on the bottom toolbar of the homepage."];
                                                       
                                                       
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:zipSave];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        [textField setTag:31];
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.font = [UIFont systemFontOfSize:16];
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

//Posts Zip Code to User

-(void)postZip:(NSString *)zipCodeText

{
    
    DLog(@"Posting Zip");
    
    
    
    if([Network currentReachabilityStatus]==0)
        
    {
        
        [UtilityManager showAlert:@"Error!" msg:@"Network is not available"];
        
    }
    
    else {
        
        
        
        NSMutableString *urlString = [BASE_URL mutableCopy] ;
        
        [urlString appendFormat:@"thislocation/updateusrzipcode?userId=%@&zipcode=%@", [defaults valueForKey:KEY_USERID], zipCodeText];
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
        
        if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]) {
            
            ReleaseAndNilify(responseXml);
            
            return;
            
        }
        
        
        
        //NEED TO SET ERROR HANDLING OF RESPONSE. WILL BE ABLE TO WHEN CAN BE TESTED
        
        DLog(@"RESPONSE - %@",responseXml);
        
        [defaults setValue:zipCodeText forKey:KEYZIPCODE];
        
        // [self getMainMenuItems];
        
        
        
        // [responseXml release];
        
        
        
    }
    
}

@end

//
//  VideoViewController.m
//  HubCiti
//
//  Created by Lakshmi H R on 6/6/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "VideoViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
@interface VideoViewController ()


@end

@implementation VideoViewController

@synthesize videoUrl,catColor,titleColor;
- (void)viewDidLoad {
    [super viewDidLoad];
   
    UIButton * doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    doneBtn.frame = CGRectMake(0,0, 50,30);
   
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    doneBtn.tintColor = [UIColor convertToHexString:titleColor];
//    UIImageView * imageView1 = [[UIImageView alloc]initWithFrame:hamberger.frame];
//    imageView1.image = [UIImage imageNamed:@"image5.jpg"];
//    [hamberger addSubview:imageView1];
    [doneBtn addTarget:self action:@selector(doneClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* doneBarButton = [[UIBarButtonItem alloc]initWithCustomView:doneBtn];
    self.navigationItem.leftBarButtonItem = doneBarButton;

    if([[UIDevice currentDevice].systemVersion intValue] >= 8)
    {
        [self playMoviesUsingAVPlayer];
    }
   
    
    
      // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
   
    
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:catColor];
   
    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:@" " forView:self withHambergur:NO withColor:titleColor];

    
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    [super viewWillAppear:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]], NSForegroundColorAttributeName,[UIFont fontWithName:@"Helvetica-Bold" size:17.0], NSFontAttributeName,nil]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)playMoviesUsingAVPlayer
{

    
    _playerViewController = [AVPlayerViewController new];
    _playerViewController.player = [AVPlayer playerWithURL:[NSURL URLWithString:videoUrl]];
    _playerViewController.view.frame = self.view.bounds;
    _playerViewController.showsPlaybackControls = YES;
    _playerViewController.wantsFullScreenLayout = YES;
    self.view.autoresizesSubviews = YES;
    [self addChildViewController:_playerViewController];
    [self.view addSubview:_playerViewController.view];
    [_playerViewController.player play];


    

}
//- (void)playMovieUsingMPPlayer
//{
//   
//    // Make a URL
//   // NSURL *url = [NSURL URLWithString:[urlArray objectAtIndex:current]];
//   NSURL *url = [NSURL URLWithString:
//                 @"http://brightcove.vo.llnwd.net/v1/uds/pd/2322069287001/201606/2683/2322069287001_4932251272001_4932247648001.mp4"];
//    
//
////    http://www.apple.com/in/ios/videos/
//    // Initialize the MPMoviePlayerController object using url
//    _moviePlayer =  [[MPMoviePlayerViewController alloc]
//                     initWithContentURL:url];
//    _moviePlayer.view.frame = self.view.frame;
//    
//    // Add a notification. (It will call a "moviePlayBackDidFinish" method when _videoPlayer finish or stops the plying video)
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(moviePlayBackDidFinish:)
//                                                 name:MPMoviePlayerPlaybackDidFinishNotification
//                                               object:_moviePlayer];
//   [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(moviePlayBackDidFinish:)
//                                                 name:MPMovieFinishReasonPlaybackEnded
//                                               object:_moviePlayer];
//
////    [[NSNotificationCenter defaultCenter] addObserver:self
////                                             selector:@selector(movieEventFullscreenHandler:)
////                                                 name:MPMoviePlayerDidEnterFullscreenNotification
////                                               object:nil];
////    
//    // Set control style to default
//   
//   // _moviePlayer.controlStyle = MPMovieControlStyleDefault;
//    
//   
//    // Set shouldAutoplay to YES
//    _moviePlayer.moviePlayer.shouldAutoplay = YES;
//    
//   
//    
//    // Add _videoPlayer's view as subview to current view.
//    [self.view addSubview:_moviePlayer.view];
//    
//    // Set the screen to full.
//    [_moviePlayer.moviePlayer setFullscreen:YES animated:YES];
//    
////    _moviePlayer = [[MPMoviePlayerViewController alloc] init];
////    _moviePlayer.view.frame = self.view.frame;
////    [_moviePlayer.moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
////    
////    [_moviePlayer.moviePlayer setShouldAutoplay:YES];
////    
////    [_moviePlayer.moviePlayer setFullscreen:NO animated:YES];
////    
////    _moviePlayer.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
////    [_moviePlayer.moviePlayer setContentURL:url];
////    
////    [self.view addSubview:_moviePlayer.view];
////    
////    [_moviePlayer.moviePlayer prepareToPlay];
////    
////    [_moviePlayer.moviePlayer play]; 
////    
//
//}
//- (void)movieEventFullscreenHandler:(NSNotification*)notification {
//    [self.moviePlayer setFullscreen:NO animated:NO];
//    [self.moviePlayer setControlStyle:MPMovieControlStyleEmbedded];

//}
-(void)doneClicked
{
    [_playerViewController.view removeFromSuperview];
      [self.navigationController popViewControllerAnimated:YES];
}
//- (void) moviePlayBackDidFinish:(NSNotification*)notification {
//    MPMoviePlayerViewController *videoplayer = [notification object];
//    [[NSNotificationCenter defaultCenter]
//     removeObserver:self
//     name:MPMoviePlayerPlaybackDidFinishNotification
//     object:videoplayer];
//    if(notification == MPMovieFinishReasonPlaybackEnded)
//    {
//        if(videoplayer.moviePlayer. playbackState == MPMoviePlaybackStateSeekingForward)
//        {
//            NSString *stringToLoad = [urlArray objectAtIndex:current+1];
//            videoplayer = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:stringToLoad]];
//            
//            [videoplayer.moviePlayer play];
//            current = current+1;
//        }
//        else if (videoplayer.moviePlayer.playbackState == MPMoviePlaybackStateSeekingBackward)
//        {
//            NSString *stringToLoad = [urlArray objectAtIndex:current-1];
//            videoplayer = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:stringToLoad]];
//
//            [videoplayer.moviePlayer play];
//            current = current -1;
//        }
//    }
//    if ([videoplayer respondsToSelector:@selector(setFullscreen:animated:)])
//    {
//        // remove the video player from superview.
//        [videoplayer.view removeFromSuperview];
//    }
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

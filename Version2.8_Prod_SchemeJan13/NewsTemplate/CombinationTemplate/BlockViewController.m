
//
//  BlockViewController.m
//  HubCiti
//
//  Created by Lakshmi H R on 6/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//





#import "BlockViewController.h"
//#import "SideMenuTableViewController.h"
#import "MFSideMenu.h"
//#import "BlockViewCell.h"
#import "AppDelegate.h"
#import "SideNavigationResponse.h"
#import "MainMenuResponse.h"
#import "HubCitiConstants.h"
#import "BlockTemplateResponse.h"
#import "BlockTemplateItemsList.h"
#import "TileViewCell.h"
#import "SearchBtnViewController.h"
#import "BookMarksViewController.h"
#import "CombinationCategoryViewController.h"
#import "PhotoView.h"
#import "SideViewController.h"
#import "WebBrowserViewController.h"


@interface BlockViewController ()<UIScrollViewDelegate,UITextFieldDelegate,CustomizedNavControllerDelegate>
{
    UIScrollView * buttonScroll;
    BOOL scrollViewEnabled,fetchNext,viewmore;
    UIActivityIndicatorView *loading;
    UIButton* moreButton;
    UIButton* Search;
    UIButton* hamburger;
    NSMutableArray *imageArray;
    SdImageView *bannerSdImageView;
    float xVal;
    CGFloat newOffset;
    UIView *rightBorder;
    UIView *leftBorder;
    UIButton * buttonList;
    UIView *secondView;
    CGPoint lastContentOffset;
    CustomizedNavController *cusNav;
}
@property (nonatomic,strong) BlockTemplateResponse * obj_BlockTempResponse;

@end

@implementation BlockViewController
@synthesize collView,totalNumOfCells,refreshMade,refreshOrButtonAction,menuResponse,array_BlockNewsData,obj_BlockTempResponse,nextPage,responseText,receivedResponseText,index,pullToRefresh,footer, buttonArray,bannerChanged;

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    if (fetchNext) {
        [self hideFooter];
    }
    
    
    [self parse_BlockNewsData:responseObject];
//    [bannerSdImageView loadImage: [defaults valueForKey:@"bannerImgBlock"]];
//    if ([defaults valueForKey:@"bannerImgBlock"] != nil)
//    {
//        UIView* titleView;
//        if(IPAD)
//        {
//            titleView = [[UIView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(10), 0, VARIABLE_WIDTH(86), VARIABLE_HEIGHT(20))];
//        }
//        else
//        {
//            //titleView = [[UIView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(10), 0, VARIABLE_WIDTH(86), VARIABLE_HEIGHT(35))];
//            titleView = [[UIView alloc]initWithFrame:CGRectMake(10, 0, 86, 35)];
//        }
//        
//        bannerSdImageView.frame = titleView.bounds;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [titleView addSubview:bannerSdImageView];
//            [self.navigationController.navigationBar.topItem setTitleView:titleView];
//            self.view.userInteractionEnabled = YES;
//        });
//        
//    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [collView reloadData];
        //[HubCitiAppDelegate removeActivityIndicator];
        //[collView setContentOffset:CGPointZero animated:YES];
        scrollViewEnabled = TRUE;
    });
    
    
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error
{
    if (fetchNext) {
        [self hideFooter];
    }
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}
-(void) hideFooter
{
    fetchNext = false;
    [loading stopAnimating];
    [footer removeFromSuperview];
    scrollViewEnabled = TRUE;
    collView.userInteractionEnabled = TRUE;
}

-(void) setUpTopTabBar
{
    //    tabBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, VARIABLE_HEIGHT(30))];
    //    tabBarView.backgroundColor = [UIColor convertToHexString:@"#f1f1f1"];
    //    [self.view addSubview:tabBarView];
    //    //Left Search Button for old news search
    //    leftSearchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    leftSearchBtn.frame =CGRectMake(VARIABLE_WIDTH(10), tabBarView.frame.size.height/2 - VARIABLE_HEIGHT(15)/2, VARIABLE_WIDTH(15),VARIABLE_HEIGHT(15));
    //    [leftSearchBtn setBackgroundImage:[UIImage imageNamed:@"search2.png"] forState:UIControlStateNormal];
    //    [tabBarView addSubview:leftSearchBtn];
    //    [leftSearchBtn addTarget:self action:@selector(leftSearchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    secondView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,VARIABLE_HEIGHT(30))];
    
    secondView.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:0.5];
    
    buttonScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(VARIABLE_WIDTH(120), 0, SCREEN_WIDTH - VARIABLE_WIDTH(120), VARIABLE_HEIGHT(30))];
    
    
    float width=(SCREEN_WIDTH-buttonScroll.frame.origin.x)/3;
    buttonScroll.showsHorizontalScrollIndicator = YES;
    buttonScroll.showsVerticalScrollIndicator = NO;
    buttonScroll.alwaysBounceVertical = NO;
    buttonScroll.delegate = self;
    secondView.backgroundColor =  [UIColor convertToHexString:@"#C7D0D2"];
    [secondView addSubview:buttonScroll];
    [self.view addSubview:secondView];
    NSLog(@"%f button scroll height %f", buttonScroll.frame.size.height, VARIABLE_HEIGHT(20));
    
    
    //Array for book mark buttons
    xVal=0;
    
    newOffset=0;
    
    // Array to display all the book mark button in scroll view
    xVal=0;
    // [self request_newsListing];
    // buttonArray = [[NSMutableArray alloc]initWithObjects:@"Top Stories",nil];
    newOffset=0;
    if (buttonArray.count == 1)
    {
        for(int i=0;i<buttonArray.count;i++)
        {
            buttonList = [UIButton buttonWithType:UIButtonTypeCustom];
            
            buttonList.frame = CGRectMake(buttonScroll.frame.size.width/2 - width/2, 0,width, VARIABLE_HEIGHT(30));
            leftBorder = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, buttonScroll.frame.size.height)];
            leftBorder.backgroundColor = [UIColor whiteColor];
            [buttonList addSubview:leftBorder];
            [buttonList setTitle:buttonArray[i] forState:UIControlStateNormal];
            buttonList.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 20.0f:12.0f)];
            buttonList.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonList.titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
            [buttonList setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            buttonList.tag = i;
            buttonList.userInteractionEnabled = NO;
            rightBorder = [[UIView alloc] initWithFrame:CGRectMake(buttonList.frame.size.width, 0, 1, buttonScroll.frame.size.height)];
            rightBorder.backgroundColor = [UIColor whiteColor];
            [buttonList addSubview:rightBorder];
            
            //            if( buttonList.tag == 0)
            //            {
            //                buttonList.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
            //
            //
            //            }
            
            [buttonList addTarget:self action:@selector(bookMarkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [buttonScroll addSubview:buttonList];
            
            xVal = xVal +width;
        }
        // COntent size for book mark scroll view
        
        [buttonScroll setContentSize:CGSizeMake(xVal,0)];
    }
    
    else if (buttonArray.count == 2)
    {
        float width_For_Two_Cat = (SCREEN_WIDTH-buttonScroll.frame.origin.x)/2;
        for(int i=0;i<buttonArray.count;i++)
        {
            CGSize mainTitleSize = [self calculateLabelWidth:buttonArray[i] withWidth:width_For_Two_Cat];
            buttonList = [UIButton buttonWithType:UIButtonTypeCustom];
            buttonList.titleLabel.numberOfLines = 1;
            
            buttonList.frame = CGRectMake(xVal, 0,mainTitleSize.width + VARIABLE_WIDTH(30), VARIABLE_HEIGHT(30));
            [buttonList setTitle:buttonArray[i] forState:UIControlStateNormal];
            buttonList.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 20.0f:12.0f)];
            
            buttonList.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonList.titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
            [buttonList setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            buttonList.tag = i;
            //            if( buttonList.tag == 0)
            //            {
            //
            //                buttonList.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:1];
            //            }
            [buttonList addTarget:self action:@selector(bookMarkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [buttonScroll addSubview:buttonList];
            xVal = xVal +buttonList.frame.size.width;
        }
        // COntent size for book mark scroll view
        [buttonScroll setContentSize:CGSizeMake(xVal + 40,0)];
        
    }
    
    
    else
    {
        //width = (SCREEN_WIDTH-VARIABLE_WIDTH(120))/3;
        
        for(int i=0;i<buttonArray.count;i++)
        {
            width =  (SCREEN_WIDTH - buttonScroll.frame.origin.x)/3;
            
            
            
            
            CGSize mainTitleSize = [self calculateLabelWidth:buttonArray[i] withWidth:width];
            //CGSize mainTitleSize = [buttonArray[i] boundingRectWithSize:(width, VARIABLE_HEIGHT(20) options:NSStringDrawingUsesDeviceMetrics context:NSStringDrawingUsesLineFragmentOrigin];
            buttonList = [UIButton buttonWithType:UIButtonTypeCustom];
            
            
            // width = [self calculateLabelWidth:buttonArray[i] withWidth:width];
            buttonList.frame = CGRectMake(xVal, 0,mainTitleSize.width + VARIABLE_WIDTH(30), VARIABLE_HEIGHT(30));
            NSLog(@" %f %f %f ",buttonList.frame.origin.x, buttonList.frame.origin.y + buttonList.frame.size.height , buttonList.frame.size.width);
            
            [buttonList setTitle:buttonArray[i] forState:UIControlStateNormal];
            buttonList.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 20.0f:12.0f)];
            
            
            
            buttonList.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonList.titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
            [buttonList setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            buttonList.tag = i;
            //            if( buttonList.tag == 0)
            //            {
            //
            //                buttonList.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
            //            }
            [buttonList addTarget:self action:@selector(bookMarkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [buttonScroll addSubview:buttonList];
            xVal = xVal +buttonList.frame.size.width;
        }
        
        
        // COntent size for book mark scroll view
        [buttonScroll setContentSize:CGSizeMake(xVal + 40,0)];
    }
    
    
    
}

-(CGSize)calculateLabelWidth:(NSString*) labelString withWidth:(CGFloat) labelwidth
{
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:(IPAD ? 20.0f:12.0f)]};
    
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, VARIABLE_HEIGHT(20))
                                            options:NSStringDrawingUsesDeviceMetrics
                                         attributes:attributes
                                            context:nil];
    return rect.size;
}

-(void)bookMarkButtonClicked: (id) sender
{
    int buttonIndex = (int)[sender tag];
    
    NSString* feedlink = [[array_BlockNewsData objectAtIndex:buttonIndex] nonfeedlink];
    
    if (feedlink.length > 0) {
        
        [defaults setValue:feedlink forKey:KEY_URL];
       
        NSString* backbuttonColor = [[array_BlockNewsData objectAtIndex:buttonIndex] backButtonColor];
        
        [defaults setValue:backbuttonColor forKey:@"backbuttonColor"];
        
        [defaults setBool:YES forKey:@"isBlock"];
        
        WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        urlDetail.title = [[array_BlockNewsData objectAtIndex:buttonIndex] catName];
        urlDetail.titleColor = [[array_BlockNewsData objectAtIndex:buttonIndex] catTxtColor];
        urlDetail.titleBackgColor = [[array_BlockNewsData objectAtIndex:buttonIndex] catColor];
        [self.navigationController pushViewController:urlDetail animated:NO];
        
        return;
    }
    
    
    if(NSOrderedSame==[[buttonArray objectAtIndex:buttonIndex] caseInsensitiveCompare:@"Photos"] || NSOrderedSame==[[buttonArray objectAtIndex:buttonIndex] caseInsensitiveCompare:@"Videos"])
    {
        PhotoView *newsDetail = [[PhotoView alloc] initWithNibName:@"PhotoView" bundle:nil];
        newsDetail.categoryName = [buttonArray objectAtIndex:buttonIndex];
        newsDetail.catColor = [[array_BlockNewsData objectAtIndex:buttonIndex] catColor];
        newsDetail.titleColor = [[array_BlockNewsData objectAtIndex:buttonIndex] catTxtColor];
        
        [self.navigationController pushViewController:newsDetail animated:NO];
    }
    else
    {
        CombinationCategoryViewController *newsDetail =[[CombinationCategoryViewController alloc] initWithNibName:@"CombinationCategoryViewController" bundle:nil];
        newsDetail.catName = [buttonArray objectAtIndex:buttonIndex];
        newsDetail.catColor = [[array_BlockNewsData objectAtIndex:buttonIndex] catColor];
        newsDetail.titleColor = [[array_BlockNewsData objectAtIndex:buttonIndex] catTxtColor];
        
        
        [self.navigationController pushViewController:newsDetail animated:NO];
    }
}
-(void) setColor : (NSInteger) tag : (id) btn
{
    for (id subview in [buttonScroll subviews])
    {
        if ([subview isMemberOfClass:[UIButton class]])
        {
            UIButton *prevButton= (UIButton*) subview;
            if(prevButton.tag == tag)
            {
                UIButton * currentBtn = prevButton;
                //[currentBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                currentBtn.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
                rightBorder.backgroundColor = [UIColor whiteColor];
                leftBorder.backgroundColor = [UIColor whiteColor];
                
                
            }
            else
            {
                rightBorder.backgroundColor = [UIColor whiteColor];
                leftBorder.backgroundColor = [UIColor whiteColor];
                prevButton.backgroundColor = [UIColor clearColor];
            }
        }
        
    }
    
    float width;
    
    if((buttonArray.count)==2)
    {
        width = (SCREEN_WIDTH - VARIABLE_WIDTH(120))/2;
    }
    
    else
    {
        width=(SCREEN_WIDTH-VARIABLE_WIDTH(120))/3;
    }
    
    CGSize moveSize = [self calculateLabelWidth:buttonArray[tag] withWidth:width];
    
    if (tag >=2)
    {
        int start  = (tag - 1)*width;
        newOffset = start;
        
        if (tag < buttonArray.count-1 )
        {
            [buttonScroll setContentOffset:CGPointMake(start, 0.0) animated:YES];
        }
        else if( tag==buttonArray.count-1){
            [buttonScroll setContentOffset:CGPointMake((buttonArray.count-3) * moveSize.width, 0.0) animated:YES];
        }
        
        [buttonScroll setContentOffset:CGPointMake(moveSize.width + 40, 0.0) animated:YES];
    }
    else if ((buttonArray.count == 2) && (tag == 1))
    {
        [buttonScroll setContentOffset:CGPointMake(moveSize.width + 40, 0.0) animated:YES];
        
    }
    else
    {
        [buttonScroll setContentOffset:CGPointMake(0.0,0.0) animated:YES];
        newOffset=0;
    }
    
    
    
}
-(void) topbuttonClicked : (id) sender
{
    UIButton* btn  = (UIButton*) sender;
    
    switch (btn.tag) {
        case 1: //  Humberger Button
        {
            //self.scrollViewEnabled = YES;
            [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
                
            }];
        }
            break;
        case 2: // More Button
        {
            if (userIdentifier == TRUE)
            {
                UIAlertController * alert;
                
                alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Sign up for free to use this feature",@"Sign up for free to use this feature") message:nil preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"Continue"
                                     style:UIAlertActionStyleCancel
                                     handler:nil];
                [alert addAction:ok];
                
                UIAlertAction* signup = [UIAlertAction
                                         actionWithTitle:@"Sign up"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action){
                                             [defaults setBool:YES forKey:@"isMainMenuSignUp"];
                                             [defaults setValue:@"1" forKey:KEY_MENULEVEL];
                                             LoginViewController *login = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
                                             
                                             AppDelegate *app = HubCitiAppDelegate;
                                             login.login_DO = app.loginDO;
                                             [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
                                             [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
                                             [self.menuContainerViewController.centerViewController pushViewController:login animated:NO];
                                             
                                         }];
                [alert addAction:signup];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else
            {
                BookMarksViewController *bk =[[BookMarksViewController alloc] initWithNibName:@"BookMarksViewController" bundle:nil];
                [self.navigationController pushViewController:bk animated:NO];
            }
        }
            break;
        case 3: // Search
        {
            SearchBtnViewController *searchData = [[SearchBtnViewController alloc]initWithNibName:@"SearchBtnViewController" bundle:nil];
            [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
            [self.navigationController pushViewController:searchData animated:NO];
        }
            break;
            
        default:
            break;
    }
    
}
-(void)setNavigationBar
{
    [self addMenu];
    //  [self addMore];
    [self addSearch];
    
    
    
    [self.navigationController.navigationBar addSubview:Search];
    //[self.navigationController.navigationBar addSubview:moreButton];
    
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [defaults setBool:NO forKey:@"isDonePressed"];
    [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(showResetPasswordAlert:)
    //                                                 name:@"tempPassword"
    //                                               object:nil];
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(showEmailCountAlert:)
    //                                                 name:@"emailCount"
    //                                               object:nil];
    totalNumOfCells = [[NSNumber alloc]initWithInt:0];
    
    nextPage = [[NSNumber alloc]init];
    // Do any additional setup after loading the view from its nib.
    [self.collView registerNib:[UINib nibWithNibName:@"TileViewCell" bundle:nil] forCellWithReuseIdentifier:@"TileCell"];
    imageArray = [[NSMutableArray alloc]init];
    
    pullToRefresh = [[UIRefreshControl alloc] init];
    self.pullToRefresh.tintColor = [UIColor whiteColor];
    [collView addSubview:pullToRefresh];
    [pullToRefresh addTarget:self action:@selector(refreshTableData) forControlEvents:UIControlEventValueChanged];
    //Book Mark Scroll View
    
    self.navigationItem.hidesBackButton = YES;
    
    //popup for the first time
    if (userIdentifier==FALSE && ![defaults boolForKey:@"pushOnKill"]){
        int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
        if (menulevel<=1) {
            [self checkLocationStatus];
            
            [self checkForRadius];
        }
    }
    else if([defaults boolForKey:@"pushOnKill"]){
        [defaults setBool:NO forKey:@"pushOnKill"];
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [defaults setBool:YES forKey:@"LoginSuccess"];
    [defaults setValue:nil forKey:@"bannerImgBlock"];
    //bannerSdImageView.hidden = NO;
    [defaults setBool:NO forKey:@"canDisplayAPNSListScreen"];
    
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    
    NSLog(@"Text color %@ background color %@",[defaults valueForKey:@"titleTxtColor"],[defaults valueForKey:@"titleBkGrdColor"]);
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]], NSForegroundColorAttributeName,[UIFont fontWithName:@"Helvetica-Bold" size:17.0], NSFontAttributeName,nil]];
    
    //        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    //        self.navigationController.navigationBar.barTintColor = [UIColor grayColor];
    viewmore = false;
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    [self requestBlockViewTemplate];
    [self setNavigationBar];
    
    if(![defaults boolForKey:@"isDonePressed"])
    {
        SideViewController *leftMenuViewController;
        if(IPAD){
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
        }
        else{
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
            
        }
        leftMenuViewController.isNewsTemplate = TRUE;
        int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
        if(menulevel > 1)
        {
            leftMenuViewController.isSideMenu = YES;
        }
        [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
        // [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    
    if ([defaults boolForKey:@"isDonePressed"])
    {
        [defaults setBool:NO forKey:@"isDonePressed"];
    }
    
    if (userIdentifier==FALSE)
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {// iOS 8.0 and later
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        } else { // below iOS 8.0
            
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        }
    }
    
    [self initMenuData];
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    if(menulevel<=1){
        //        [[NSNotificationCenter defaultCenter] postNotificationName:@"homeClicked" object:nil];
        [defaults setValue:@"News Tile Template" forKey:@"centerViewController"];
    }
    self.view.userInteractionEnabled = NO;
    
    [HubCitiAppDelegate showActivityIndicator];
    
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}
- (void)viewWillDisappear:(BOOL)animated
{    
    [super viewWillDisappear:animated];
        [super viewWillDisappear:animated];

    [bannerSdImageView removeFromSuperview];
    bannerSdImageView.frame = CGRectZero;
    // [self.navigationController.navigationBar.topItem setTitleView:nil];
    [defaults setValue:nil forKey:@"bannerImgBlock"];
    [moreButton removeFromSuperview];
    [Search removeFromSuperview];
    scrollViewEnabled = NO;
    for (NSOperation *operation in [HTTPClient sharedHTTPClient].manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        NSLog(@"operation:%@",operation);
        AFHTTPRequestOperation *operations = (AFHTTPRequestOperation*)operation;
        if([[[[operations request] URL] absoluteString] containsString:@"band/getnewsblocktemplate"]){
            [operation cancel];
        }
        
    }
    //  [[HTTPClient sharedHTTPClient].manager.operationQueue cancelAllOperations];
}
-(void) refreshTableData
{
    nextPage = 0;
    totalNumOfCells = [[NSNumber alloc]initWithInt:0];
    refreshOrButtonAction = YES;
    refreshMade = YES;
    [self requestBlockViewTemplate];
}
-(void) addMore // Add More Button
{
    [moreButton removeFromSuperview];
    [Search removeFromSuperview];
    
    moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame = CGRectMake(SCREEN_WIDTH - (IPAD ? VARIABLE_WIDTH(70):VARIABLE_WIDTH(95)), IPAD ? 5 :  VARIABLE_HEIGHT(12), IPAD ? VARIABLE_WIDTH(40) : VARIABLE_WIDTH(60), VARIABLE_HEIGHT(25));
    moreButton.tag = 2;
    [moreButton addTarget:self action:@selector(topbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIImageView * moreImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, VARIABLE_WIDTH(18), VARIABLE_HEIGHT(16))];
    moreImage.image = [UIImage imageNamed:@"more2.png"];
    [moreButton  addSubview:moreImage];
    UILabel * moreLbl = [[UILabel alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(25), - 2, VARIABLE_WIDTH(60), VARIABLE_HEIGHT(20))];
    moreLbl.text = @"More";
    moreLbl.textColor = [UIColor blackColor];
    moreLbl.font = [UIFont systemFontOfSize:14];
    
    [moreButton addSubview:moreLbl];
    
}
-(void) addMenu
{
    cusNav.customNavBardelegate = self;
    [cusNav hideBackButton:YES];
    [cusNav hideHambergerButton: NO];
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    
    if (menulevel > 1)
    {
        [cusNav hideBackButton:NO];
    }
}
-(void)popBackToPreviousPage
{
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL]integerValue];
    NSString * levelString;
    if(menulevel > 1)
        levelString = [NSString stringWithFormat:@"%d", menulevel-1];
    else
        levelString = [NSString stringWithFormat:@"%d", menulevel];
    [[HubCitiAppDelegate getLinkIdArray]removeLastObject];

    [defaults setValue:levelString forKey:KEY_MENULEVEL];
    
    [self.navigationController popViewControllerAnimated:NO];
}
-(void) addSearch // Add Search Button
{
    [moreButton removeFromSuperview];
    [Search removeFromSuperview];
    
    Search = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (IPAD) {
        Search.frame = CGRectMake(SCREEN_WIDTH - VARIABLE_WIDTH(20) ,5, VARIABLE_WIDTH(14), VARIABLE_HEIGHT(16));
    }
    else{
        Search.frame = CGRectMake(SCREEN_WIDTH - VARIABLE_WIDTH(25) ,15, VARIABLE_WIDTH(18), VARIABLE_HEIGHT(17));
    }
    
    Search.tag = 3;
    [Search setBackgroundImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    [Search addTarget:self action:@selector(topbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
}




-(void) initMenuData
{
    
    
    if (FindBottomButtonID!=Nil)
    {
        [FindBottomButtonID removeAllObjects];
    }
    else
    {
        if (FindBottomButtonID==Nil) {
            FindBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (EventsBottomButtonID!=Nil)
    {
        [EventsBottomButtonID removeAllObjects];
    }
    else
    {
        if (EventsBottomButtonID==Nil) {
            EventsBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (FilterBottomButtonID!=Nil)
    {
        [FilterBottomButtonID removeAllObjects];
    }
    else
    {
        if (FilterBottomButtonID==Nil) {
            FilterBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    
    if (FindSingleBottomButtonID!=Nil)
    {
        [FindSingleBottomButtonID removeAllObjects];
    }
    else
    {
        if (FindSingleBottomButtonID==Nil) {
            FindSingleBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (CityExperienceBottomButtonID!=Nil)
    {
        [CityExperienceBottomButtonID removeAllObjects];
    }
    else
    {
        if (CityExperienceBottomButtonID==Nil) {
            CityExperienceBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (FundraiserBottomButtonID!=Nil)
    {
        [FundraiserBottomButtonID removeAllObjects];
    }
    else
    {
        if (FundraiserBottomButtonID==Nil) {
            FundraiserBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    if (NearbyBottomButtonID!=Nil)
    {
        [NearbyBottomButtonID removeAllObjects];
    }
    else
    {
        if (NearbyBottomButtonID==Nil) {
            NearbyBottomButtonID =[[NSMutableArray alloc]init];
        }
        
    }
    
    
    if (linkID==NULL) {
        linkID=[[NSMutableArray alloc]init];
    }
    else
    {
        
        [linkID removeAllObjects];
        
    }
    
    if (dateCreated==NULL) {
        dateCreated = [[NSMutableDictionary alloc]init];
    }
    
    if (cashedResponse==NULL) {
        cashedResponse = [[NSMutableDictionary alloc]init];
    }
    
    
    if (deptId==NULL) {
        deptId = [[NSMutableDictionary alloc]init];
    }
    
    if (typeId==NULL) {
        typeId = [[NSMutableDictionary alloc]init];
    }
    
    if (selectedSortOption==NULL) {
        selectedSortOption=[[NSMutableDictionary alloc]init];
    }
    
    if (selectedFilterOption==NULL) {
        selectedFilterOption=[[NSMutableDictionary alloc]init];
    }
    
    if (selectedSortCityDic==NULL) {
        selectedSortCityDic=[[NSMutableDictionary alloc]init];
        
    }
    [defaults setBool:NO forKey:BottomButton];
    
    if([defaults valueForKey:KEYZIPCODE] && [[defaults valueForKey:KEYZIPCODE]isEqualToString:@"N/A"])
    {
        [defaults setValue:nil forKey:KEYZIPCODE];
    }
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    if(menulevel<=1){
        [NewsUtility refreshLinkDeptArray];
    }
}

-(void) viewDidAppear : (BOOL)animated
{
    [super viewDidAppear:animated];
    scrollViewEnabled = true;
}
- (void)leftSideMenuButtonPressed
{
    //    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    //        //send bookmark array to side menu on hambrger click
    //        if(!(self.menuContainerViewController.menuState == MFSideMenuStateClosed)){
    //
    //            //[[NSNotificationCenter defaultCenter] postNotificationName:@"notificationName" object:buttonArray];
    //
    //        }
    //    }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return array_BlockNewsData.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TileCell";
    
    
    TileViewCell *cell = nil;
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"TileViewCell" owner:self options:nil];
    }
    if (indexPath.row != [array_BlockNewsData count] && array_BlockNewsData.count > 0)
        //       catName
        //       catImgPath
    {
        imageArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[[array_BlockNewsData objectAtIndex:indexPath.row] catImgPath]]];
        
        cell.blockLabel.font = [UIFont systemFontOfSize:(IPAD ? 16.0f:12.0f)];
        //cell.blockLabel.text = @"Former Facebook Workers: We Routinely Suppressed Conservative News";
        cell.blockLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [cell.blockImg.layer setBorderColor:[UIColor blackColor].CGColor];
        cell.blockImg.contentMode = UIViewContentModeScaleAspectFit;
        if(IPAD)
        {
            [cell.blockImg.layer setBorderWidth:10.0];
        }
        else
        {
            [cell.blockImg.layer setBorderWidth:5.0];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.blockLabel.text = [[array_BlockNewsData objectAtIndex:indexPath.row] catName];
            [cell.blockImg loadImage:[[array_BlockNewsData objectAtIndex:indexPath.row] catImgPath]];
        });
        
    }
    
    
    // cell.blockImg.image = [UIImage imageNamed:@"image1.jpg"];
    
    //    cell.photoCount.text = @"6";
    //
    //    cell.photoImg.image = [UIImage imageNamed:@"camera.png"];
    
    return cell;
    
}
-(NSArray *)convertToArrayFromCommaSeparated:(NSString*)string{
    return [string componentsSeparatedByString:@","];
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString* feedlink = [[array_BlockNewsData objectAtIndex:indexPath.row] nonfeedlink];
    
    if (feedlink.length > 0) {
        
        [defaults setValue:feedlink forKey:KEY_URL];
                
        [defaults setBool:YES forKey:@"isBlock"];
        NSString* backbuttonColor = [[array_BlockNewsData objectAtIndex:indexPath.row] backButtonColor];
        
        [defaults setValue:backbuttonColor forKey:@"backbuttonColor"];
        
        WebBrowserViewController *urlDetail= [[WebBrowserViewController alloc] initWithNibName:@"WebBrowserViewController" bundle:[NSBundle mainBundle]];
        urlDetail.title = [[array_BlockNewsData objectAtIndex:indexPath.row] catName];
        urlDetail.titleColor = [[array_BlockNewsData objectAtIndex:indexPath.row] catTxtColor];
        urlDetail.titleBackgColor = [[array_BlockNewsData objectAtIndex:indexPath.row] catColor];
        [self.navigationController pushViewController:urlDetail animated:NO];
        
        return;
    }

    
    
    if( NSOrderedSame==[[array_BlockNewsData[indexPath.row] catName] caseInsensitiveCompare:@"Photos"] ||NSOrderedSame== [[array_BlockNewsData[indexPath.row] catName] caseInsensitiveCompare:@"Videos"])
    {
        PhotoView *newsDetail = [[PhotoView alloc] initWithNibName:@"PhotoView" bundle:nil];
        newsDetail.categoryName = [array_BlockNewsData[indexPath.row] catName];
        newsDetail.catColor = [array_BlockNewsData[indexPath.row] catColor];
        newsDetail.titleColor = [array_BlockNewsData[indexPath.row]catTxtColor];
        [self.navigationController pushViewController:newsDetail animated:NO];
    }
    else{
        CombinationCategoryViewController *newsDetail =[[CombinationCategoryViewController alloc] initWithNibName:@"CombinationCategoryViewController" bundle:nil];
        newsDetail.catName = [array_BlockNewsData[indexPath.row] catName];
        newsDetail.catColor = [array_BlockNewsData[indexPath.row] catColor];
        newsDetail.titleColor = [array_BlockNewsData[indexPath.row]catTxtColor];
        [self.navigationController pushViewController:newsDetail animated:NO];
    }
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    float cellwidth = collView.frame.size.width/2 ; //Replace the divisor with the column count requirement. Make sure to have it in float.
    float cellheigth = collView.frame.size.height/3 ;
    CGSize size = CGSizeMake(cellwidth, cellheigth);
    
    return size;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0; // This is the minimum inter item spacing, can be more
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    lastContentOffset = scrollView.contentOffset;
}
- (void) scrollViewDidEndDecelerating: (UIScrollView *) scrollView
{
    lastContentOffset = scrollView.contentOffset;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x != lastContentOffset.x)
    {
        buttonScroll.frame = CGRectMake(VARIABLE_WIDTH(3), 0, SCREEN_WIDTH - VARIABLE_WIDTH(6), VARIABLE_HEIGHT(30));
    }
    
    
    if (!scrollViewEnabled)
    {
        return;
    }
    CGSize contentSize = scrollView.contentSize;
    CGPoint contentOffset = scrollView.contentOffset;
    
    if (contentSize.height < SCREEN_HEIGHT && [nextPage intValue]  == 0) {
        return;
    }
    
    if ((contentOffset.y + scrollView.bounds.size.height > contentSize.height) && [nextPage intValue] == 1)
    {
        [self fetchNextNews];
    }
}
-(void)fetchNextNews

{
    
    scrollViewEnabled = NO;
    fetchNext = true;
    viewmore = true;
    [HubCitiAppDelegate removeActivityIndicator];
    
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    loading.color = [UIColor whiteColor];
    [loading startAnimating];
    CGFloat height = loading.frame.size.height + 6.f;
    footer = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-height - VARIABLE_HEIGHT(64), self.view.bounds.size.width, height)];
    footer.backgroundColor = [UIColor blackColor];
    [footer addSubview:loading];
    loading.center = CGPointMake(self.view.bounds.size.width / 2, height / 2.f);
    [self.view addSubview:footer];
    collView.userInteractionEnabled = false;
    [self requestBlockViewTemplate];
    
    
}
//- (UIEdgeInsets)collectionView: (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(0,5,0,5);
//}// top, left, bottom, right }

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)leftSideMenuButtonPressed:(id)sender
{
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        //send bookmark array to side menu on hambrger click
        //        if(!(self.menuContainerViewController.menuState == MFSideMenuStateClosed)){
        //            [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationName" object:buttonArray];
        //
        //        }
    }];
    
}





-(void)requestBlockViewTemplate
{
    
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    //[parameters setValue:@"12195" forKey:@"userId"];
    [parameters setValue:totalNumOfCells forKey:@"lowerLimit"];
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    NSLog(@"%d--->%@",menulevel,[HubCitiAppDelegate getLinkIdArray]);
    if(menulevel>1){
        NSLog(@"%d",menulevel);
        NSString *linkId= [[HubCitiAppDelegate getLinkIdArray]objectAtIndex:menulevel-1];
        [parameters setValue:linkId forKey:@"linkId"];
        NSString *level = [defaults valueForKey:KEY_MENULEVEL];
        [parameters setValue:level forKey:@"level"];
    }
    else{
        
        [parameters setValue:@"0" forKey:@"linkId"];
    }
    ;
    //[parameters setValue:@"0" forKey:@"lowerLimit"];
    //[parameters setValue:@"0" forKey:@"isSideBar"];
    if ([defaults objectForKey:@"blockModifiedDate"]) {
        [parameters setValue:[defaults objectForKey:@"blockModifiedDate"]  forKey:@"dateCreated"];
    }
    
    DLog(@"parameter: %@",parameters);
    
    
//    NSString *urlString = [NSString stringWithFormat:@"%@band/getnewsblocktemplate",@"http://10.10.221.155:9990/HubCiti2.8.2/"];
    NSString *urlString = [NSString stringWithFormat:@"%@band/getnewsblocktemplate",BASE_URL];
    DLog(@"URL:%@",urlString);
    
    
    
    if ([nextPage intValue] == 1)
    {
        NSLog(@"Json = %@", parameters);
        HTTPClient* client = [HTTPClient sharedHTTPClient];
        client.delegate = self;
        NSLog(@"URL = %@", urlString);
        [client sendRequest:parameters : urlString];
        
    }
    else
    {
//        if( refreshMade == YES)
//        {
//            [HubCitiAppDelegate removeActivityIndicator];
//        }
//        else
        {
           // [HubCitiAppDelegate showActivityIndicator];
        }
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        [manager.requestSerializer setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [manager POST:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            NSLog(@"template changed %@",[responseObject objectForKey:@"templateChanged"]);
            if (!([[responseObject objectForKey:@"newtempName"] isEqualToString:@"News Tile Template"] && [responseObject objectForKey:@"templateChanged"] == [NSNumber numberWithBool:1]))
            {
                [HubCitiAppDelegate removeActivityIndicator];
            }
            [self parse_BlockNewsData:responseObject];
            
            
            
            //
            //self.navigationItem.titleView = [[UIView alloc] initWithFrame:CGRectZero];
            dispatch_async(dispatch_get_main_queue(), ^{
                [collView reloadData];
                //[HubCitiAppDelegate removeActivityIndicator];
                //[collView setContentOffset:CGPointZero animated:YES];
                scrollViewEnabled = TRUE;
                           });
            
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [HubCitiAppDelegate removeActivityIndicator];
            
            NSLog(@"Error: %@", error);
        }];
    }
    
    
    
}
-(void)parse_BlockNewsData:(id)responseData
{
    
    
    
    if( refreshMade == YES)
    {
        refreshMade = NO;
        array_BlockNewsData = [[NSMutableArray alloc]init];
    }
    
    if (responseData == nil)
        return;
    if (obj_BlockTempResponse == nil) {
        obj_BlockTempResponse = [[BlockTemplateResponse alloc] init];
    }
    @try{
        [obj_BlockTempResponse setValuesForKeysWithDictionary:responseData];
    }
    @catch (NSException *exception) {
        // Do nothing
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
        return;
    }
    
    if (refreshOrButtonAction == YES)
    {
        nextPage = 0;
        refreshOrButtonAction = NO;
        [pullToRefresh endRefreshing];
        if ( ![obj_BlockTempResponse.responseCode isEqualToString:@"10000"])
        {
            [UtilityManager showAlert:nil msg:obj_BlockTempResponse.responseText];
            return;
            
        }
        
        
    }
    NSLog(@"response for block template = %@", obj_BlockTempResponse);
    
    
    
    if ([obj_BlockTempResponse.responseCode isEqualToString:@"10000"] )
    {
       
        
        if(obj_BlockTempResponse.templateChanged)
        {
            if (obj_BlockTempResponse.newtempName)
            {
                [defaults setInteger:0 forKey:@"currentIndex"];
                [defaults setValue:nil forKey:@"bannerImgBlock"];
                [defaults setInteger:0 forKey:@"currentIndexSubPage"];
                if([obj_BlockTempResponse.newtempName isEqualToString:@"Combination News Template"])
                {
                    [cusNav hideBackButton:YES];
                    [cusNav hideHambergerButton: YES];
                    
                    [defaults setValue:nil forKey:@"blockModifiedDate"];
                    [defaults setValue:@"Combination News Template" forKey:@"selectedTemplateName"];
                    [NewsUtility setNewsTemplateAsRoot];
                    return;
                }
                else if ([obj_BlockTempResponse.newtempName isEqualToString:@"News Tile Template"])
                {
                    
                    [defaults setValue:@"News Tile Template" forKey:@"selectedTemplateName"];
                    // [self getSideNav];
                    [self parseSuccessBlock : responseData];
                    return;
                    
                }
                else if ([obj_BlockTempResponse.newtempName isEqualToString:@"Scrolling News Template"])
                {
                    [cusNav hideBackButton:YES];
                    [cusNav hideHambergerButton: YES];
                    [defaults setValue:nil forKey:@"blockModifiedDate"];
                    [defaults setValue:@"Scrolling News Template" forKey:@"selectedTemplateName"];
                    [NewsUtility setNewsTemplateAsRoot];
                    return;
                    
                }
                
                else
                {
                    [cusNav hideBackButton:YES];
                    [cusNav hideHambergerButton: YES];
                    [defaults setValue:nil forKey:@"blockModifiedDate"];
                    [defaults setValue:@"HubCiti" forKey:@"selectedTemplateName"];
                    [defaults setBool:NO forKey:@"newsTemplateExist"];
                    NSArray *keys = [cashedResponse allKeys];
                    for (int i = 0 ; i < [keys count]; i++)
                    {
                        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
                            [cashedResponse removeObjectForKey:keys[i]];
                        }
                        
                    }
                    
                    ReleaseAndNilify(selectedSortOption);
                    ReleaseAndNilify(selectedFilterOption);
                    ReleaseAndNilify(selectedSortCityDic);
                    ReleaseAndNilify(typeId);
                    ReleaseAndNilify(deptId);
                    
                    [NewsUtility setLoginVCAsRoot];
                    return;
                }
                
            }
        }
        [self parseSuccessBlock : responseData];
        
    }
    else
    {
        if(obj_BlockTempResponse.hamburgerImg)
        {
            if (![[defaults valueForKey:@"hamburgerImg"] isEqualToString:obj_BlockTempResponse.hamburgerImg])
            {
                [defaults setValue:obj_BlockTempResponse.hamburgerImg forKey:@"hamburgerImg"];
                [cusNav changeHambugerImage];
            }
            
        }
        [UtilityManager showAlert:nil msg:obj_BlockTempResponse.responseText];
        self.view.userInteractionEnabled = YES;

    }
    viewmore = false;
    
    
}
-(void) parseSuccessBlock : (id) response
{
    self.view.userInteractionEnabled = NO;
    array_BlockNewsData = [[NSMutableArray alloc]init];
    buttonArray = [[NSMutableArray alloc]init];
    
    
    
    if(obj_BlockTempResponse.nextPage)
    {
        nextPage = obj_BlockTempResponse.nextPage;
    }
    if(obj_BlockTempResponse.lowerLimitFlag)
    {
        totalNumOfCells = obj_BlockTempResponse.lowerLimitFlag;
    }
    if( obj_BlockTempResponse.bannerImg)
    {
        if ([defaults valueForKey:@"bannerImgBlock"])
        {
            NSLog(@"banner path : %@",[defaults valueForKey:@"bannerImgBlock"]);
            if (![[defaults valueForKey:@"bannerImgBlock"] isEqualToString:obj_BlockTempResponse.bannerImg])
            {
                [defaults setValue:obj_BlockTempResponse.bannerImg forKey:@"bannerImgBlock"];
                bannerChanged = 1;
            }
        }
        else
        {
            bannerChanged = 1;
            [defaults setValue:obj_BlockTempResponse.bannerImg forKey:@"bannerImgBlock"];
        }
        
        if (bannerChanged == 1)
        {
            
            if(IPAD)
            {
                bannerSdImageView = [[SdImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(10), 0, VARIABLE_WIDTH(130), VARIABLE_HEIGHT(20))];
            }
            else
            {
                bannerSdImageView = [[SdImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(10), 0, VARIABLE_WIDTH(130), VARIABLE_HEIGHT(35))];
                //bannerSdImageView = [[SdImageView alloc]initWithFrame:CGRectMake(10, 0, 86, 35)];
            }
            
            
            bannerSdImageView.backgroundColor = [UIColor clearColor];
            bannerSdImageView.tag = 1;
            //asyncImageView.layer.cornerRadius = 5.0f;
            bannerSdImageView.contentMode = UIViewContentModeScaleToFill;
            [bannerSdImageView loadImage: [defaults valueForKey:@"bannerImgBlock"]];
            
            UIView* titleView;
            if(IPAD)
            {
                titleView = [[UIView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(10), 0, VARIABLE_WIDTH(130), VARIABLE_HEIGHT(20))];
            }
            else
            {
                //titleView = [[UIView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(10), 0, VARIABLE_WIDTH(86), VARIABLE_HEIGHT(35))];
                titleView = [[UIView alloc]initWithFrame:CGRectMake(10, 0, 130, 35)];
            }
            
            bannerSdImageView.frame = titleView.bounds;
            dispatch_async(dispatch_get_main_queue(), ^{
                [titleView addSubview:bannerSdImageView];
                
                
                if ([defaults valueForKey:@"bannerImgBlock"] != nil)
                {
                    [self.navigationController.navigationBar.topItem setTitleView:titleView];
                    [HubCitiAppDelegate removeActivityIndicator];
                    
                }
            });
            bannerChanged = 0;
        }
        
    }
    else
    {
        [HubCitiAppDelegate removeActivityIndicator];
    }
    if(obj_BlockTempResponse.bkImgPath)
    {
         if (![[defaults valueForKey:@"bkImgPath"] isEqualToString:obj_BlockTempResponse.bkImgPath])
         {
             [cusNav changeBackImage:obj_BlockTempResponse.bkImgPath];
         }
        [defaults setValue:obj_BlockTempResponse.bkImgPath forKey:@"bkImgPath"];
    }
    if(obj_BlockTempResponse.homeImgPath)
    {
        [defaults setValue:obj_BlockTempResponse.homeImgPath forKey:@"homeImgPath"];
    }
    if(obj_BlockTempResponse.titleTxtColor)
    {
        [defaults setValue:obj_BlockTempResponse.titleTxtColor forKey:@"titleTxtColor"];
    }
    if(obj_BlockTempResponse.titleBkGrdColor)
    {
        [defaults setValue:obj_BlockTempResponse.titleBkGrdColor forKey:@"titleBkGrdColor"];
    }
    if(obj_BlockTempResponse.hamburgerImg)
    {
        if (![[defaults valueForKey:@"hamburgerImg"] isEqualToString:obj_BlockTempResponse.hamburgerImg])
        {
            [defaults setValue:obj_BlockTempResponse.hamburgerImg forKey:@"hamburgerImg"];
            [cusNav changeHambugerImage];
        }
        
    }
    if(obj_BlockTempResponse.weatherURL)
    {
        [defaults setValue:obj_BlockTempResponse.weatherURL forKey:@"weatherURL"];
    }
    if ( obj_BlockTempResponse.items != nil)
    {
        if(!viewmore)
        {
            array_BlockNewsData = [[NSMutableArray alloc]init];
        }
        for (int i=0;i< obj_BlockTempResponse.items.count;i++)
        {
            BlockTemplateItemsList * blockItemResponseData = [[BlockTemplateItemsList alloc]init];
            NSDictionary * dict_blockItemData = obj_BlockTempResponse.items[i];
            @try{
                [blockItemResponseData setValuesForKeysWithDictionary:dict_blockItemData];
            }
            @catch (NSException *exception) {
                // Do nothing
                [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
                return;
            }
            
            [array_BlockNewsData addObject:blockItemResponseData];
            [buttonArray addObject:[blockItemResponseData catName]];
            
        }
        
    }
    [defaults setValue:obj_BlockTempResponse.modifiedDate forKey:@"blockModifiedDate"];
    [receivedResponseText removeObjectAtIndex:index];
    [receivedResponseText insertObject:@"1" atIndex:index];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setUpTopTabBar];
        self.view.userInteractionEnabled = YES;
        

        
        
    });
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait;
}
- (IBAction)refreshClicked:(id)sender {
}



//- (void)showResetPasswordAlert:(NSNotification *)notification {
//     [[CommonUtilityNews utilityManagers] tempPasswordDisplay:notification onViewController:self];
//
//}
//- (void)showEmailCountAlert:(NSNotification *)notification
//{
//   [[CommonUtilityNews utilityManagers] emailCountDisplay:notification onViewController:[[[[UIApplication sharedApplication] delegate] window] rootViewController]];
//
//}


#pragma mark functions for popup for the first time
-(void)checkLocationStatus{
    
    
    
    if([defaults integerForKey:@"AppCounter"] == 1)
        
    {
        
        // If Devive Level GPS and App Level GPS both ON
        
        if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [[defaults valueForKey:@"allowingGPS"] isEqualToString:@"YES"])
            
        {
            
//            [SharedManager setGps_allow_flag:YES];
//            
//            [defaults setValue:@"YES" forKey:@"allowingGPS"];
            
            [Location updateLocationinfo:self];
            
        }
        
        // IF Device Level GPS ON but Application Level GPS OFF
        
        else if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [[defaults valueForKey:@"allowingGPS"] isEqualToString:@"NO"])
            
        {
            
            [self gpsAllowDontAllowPopup];
            
            
        }
        
        // IF Application level GPS off
        
        else if ([[defaults valueForKey:@"allowingGPS"] isEqualToString:@"NO"]) {
            
            
            
            [self gpsAllowDontAllowPopup];
        }
        
    }
    
    
    
    else{
        
        
        
        [defaults setInteger:1 forKey:@"AppCounter"];
        
        if(([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CLLocationManager  authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [CLLocationManager locationServicesEnabled])
            
        {
            
//            [SharedManager setGps_allow_flag:YES];
//            
//            [defaults setValue:@"YES" forKey:@"allowingGPS"];
            
            [Location updateLocationinfo:self];
            
        }
        
        else {
            
//            [SharedManager setGps_allow_flag:NO];
//            
//            [defaults setValue:@"NO" forKey:@"allowingGPS"];
            
            [self checkForZipCode];
            
        }
        
    }
    
}

-(void)checkForRadius

{
    
    NSString *reqStr = [NSString stringWithFormat:@"<AuthenticateUser><deviceID>%@</deviceID><userId>%@</userId><hubCitiId>%@</hubCitiId></AuthenticateUser>",[defaults valueForKey:KEY_DEVICEID],[defaults valueForKey:KEY_USERID],[defaults valueForKey:KEY_HUBCITIID]];
    
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getusersettings",BASE_URL];
   
    
    
    
    NSString *responseXml = [ConnectionManager establishPostConnection:urlString withParam:reqStr];
    
    
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml];
    
    
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    
    TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
    
    if (responseCodeElement != nil && ![[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]) {
        
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        
        
        
        return;
        
    }
    
    
    
    TBXMLElement *localeRadiusElement = [TBXML childElementNamed:@"localeRadius" parentElement:tbxml.rootXMLElement];
    TBXMLElement *bLocServiceElement = [TBXML childElementNamed:@"bLocService" parentElement:tbxml.rootXMLElement];
    if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"false"] )
    {
        [SharedManager setGps_allow_flag:NO];
        [defaults setValue:@"NO" forKey:@"allowingGPS"];
    }
    else  if ( [[TBXML textForElement:bLocServiceElement] isEqualToString:@"true"] )
    {
        [SharedManager setGps_allow_flag:YES];
        [defaults setValue:@"YES" forKey:@"allowingGPS"];
    }
    
    
    NSString *userRadius = [TBXML textForElement:localeRadiusElement];
    
    if(userRadius.length > 0)
        
    {
        
        [defaults setValue:userRadius forKey:@"FindRadius"];
        
        NSLog(@"my radius is %@",[defaults valueForKey:@"FindRadius"]);
        
    }
}



-(void) gpsAllowDontAllowPopup{
    
    
    UIAlertController * alert;
    alert=[UIAlertController alertControllerWithTitle:nil message:@"Application uses your current location to provide information about retailers and products near you. Do you wish to allow to access your location?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* allow = [UIAlertAction
                            actionWithTitle:@"Allow"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action){
                                [NewsUtility setGPSAsAllowForUser];
                                //Ajit's edit
                                //[SharedManager setGps_allow_flag:YES];
                                
                                //If LS Allowed, but LS are off
                                if ([CLLocationManager  authorizationStatus] == kCLAuthorizationStatusDenied) {
                                    [UtilityManager showAlert:nil msg:@"Please go to your iPhone Settings and turn on Location Services for Application."];
                                    
                                }
                                [Location updateLocationinfo:self];
                                //[defaults setValue:@"YES" forKey:@"allowingGPS"];
                                
                            }];
    [alert addAction:allow];
    
    UIAlertAction* dontAllow = [UIAlertAction
                                actionWithTitle:@"Don't Allow"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action){
                                   // [SharedManager setGps_allow_flag:NO];
                                    [defaults setValue:@"" forKey:KEY_LATITUDE];//reset the values
                                    [defaults setValue:@"" forKey:KEY_LONGITUDE];
                                    [self checkForZipCode];
                                    //[defaults setValue:@"NO" forKey:@"allowingGPS"];
                                    [alert dismissViewControllerAnimated:NO completion:nil];
                                }];
    [alert addAction:dontAllow];
    
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
    
}

//zip code checking
-(void)checkForZipCode

{
    
    NSString *urlString = [NSString stringWithFormat:@"%@thislocation/fetchuserlocationpoints?userId=%@",BASE_URL,[defaults objectForKey:KEY_USERID]];
    
    NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
    
    
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:responseXml] ;
    
    TBXMLElement *postalCodeElement = [TBXML childElementNamed:@"postalCode" parentElement:tbxml.rootXMLElement];
    
    
    
    if (postalCodeElement == nil) {
        
        TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbxml.rootXMLElement];
        
        NSString *responseTextStr = [TBXML textForElement:responseTextElement];
        
        int responseCode = [[TBXML textForElement:[TBXML childElementNamed:RESPONSECODE parentElement:tbxml.rootXMLElement]] intValue];
        
        if (responseCode == 10005) {
            
            //If The User Doesnt Have a Zip on file, Ask them if they want to enter one
            
            UIAlertController * alert; //enter a zipcode alert
            alert=[UIAlertController alertControllerWithTitle:nil message:@"Would you like to enter a zipcode for us to use instead?" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* no = [UIAlertAction
                                 actionWithTitle:@"No"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     //Warn them that they are completely off the grid as far as ScanSee is concerned
                                     [defaults setValue:nil forKey:KEYZIPCODE];
                                     [UtilityManager showAlert:nil msg:@"You have chosen not to search by your Zipcode. If you would like to search by Zipcode in the future, please go to the User Information screen."];
                                 }];
            [alert addAction:no];
            UIAlertAction* yes = [UIAlertAction
                                  actionWithTitle:@"Yes"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action){
                                      [self showZipEnter];
                                  }];
            [alert addAction:yes];
            
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alert animated:YES completion:nil];
            
            
            return;
            
            
            
            
            
            ReleaseAndNilify(responseXml);
            
            return;
            
        }
        
        else {
            
            // Banner the message given by the server
            
            [UtilityManager showAlert:@"Info" msg:responseTextStr];
            
            //[tbxml release];
            
            ReleaseAndNilify(responseXml);
            
            return;
            
        }
        
    }
    
    
    
    NSString *postalCodeStr = [TBXML textForElement:postalCodeElement];
    
    
    
    //Sets the ZipCode to the key for use in other view controllers
    
    [defaults setValue:postalCodeStr forKey:KEYZIPCODE];
    
    
    
    //set for Austin Experience req when clicked from main menu
    
    [defaults setObject:postalCodeStr forKey:@"postalCode"];
    
    
    
    //[tbxml release];
    
    ReleaseAndNilify(responseXml);
    
    //[self getMainMenuItems];
    
    return;
    
}



//Alert that shows a Zip Enter Box

-(void)showZipEnter
{
    
    // use UIAlertController
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Please enter your zip code"
                               message:nil
                               preferredStyle:UIAlertControllerStyleAlert];
    
    zipSave = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action){
                                         //Do Some action here
                                         UITextField *textField = alert.textFields[0];
                                         if ([textField.text length])
                                             [self postZip:textField.text];
                                         else
                                             return;
                                         
                                     }];
    zipSave.enabled = NO;
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       NSLog(@"cancel btn");
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       [defaults setValue:nil forKey:KEYZIPCODE];
                                                       
                                                       [UtilityManager showAlert:nil msg:@"You have chosen not to search by your Zipcode. If you would like to search by Zipcode in the future, please go to the User Information screen on the bottom toolbar of the homepage."];
                                                       
                                                       
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:zipSave];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        [textField setTag:31];
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.font = [UIFont systemFontOfSize:16];
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

//Posts Zip Code to User

-(void)postZip:(NSString *)zipCodeText

{
    
    DLog(@"Posting Zip");
    
    
    
    if([Network currentReachabilityStatus]==0)
        
    {
        
        [UtilityManager showAlert:@"Error!" msg:@"Network is not available"];
        
    }
    
    else {
        
        
        
        NSMutableString *urlString = [BASE_URL mutableCopy] ;
        
        [urlString appendFormat:@"thislocation/updateusrzipcode?userId=%@&zipcode=%@", [defaults valueForKey:KEY_USERID], zipCodeText];
        
        NSString *responseXml = [[NSString alloc] initWithString:[ConnectionManager establishGetConnection:urlString]];
        
        if ([UtilityManager isResponseXMLNullOrEmpty:responseXml]) {
            
            ReleaseAndNilify(responseXml);
            
            return;
            
        }
        
        
        
        //NEED TO SET ERROR HANDLING OF RESPONSE. WILL BE ABLE TO WHEN CAN BE TESTED
        
        DLog(@"RESPONSE - %@",responseXml);
        
        [defaults setValue:zipCodeText forKey:KEYZIPCODE];
        
        // [self getMainMenuItems];
        
        
        
        // [responseXml release];
        
        
        
    }
    
}
@end

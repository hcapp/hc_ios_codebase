//
//  PhotoView.m
//  pageSwipe
//
//  Created by Lakshmi H R on 5/18/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import "PhotoView.h"
#import "CollectionViewCell.h"
#import "CombinationViewController.h"
#import "MFSideMenu.h"
#import "VideoViewController.h"
#import "HubCitiConstants.h"
#import "NewsDetailViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "AppDelegate.h"
#import "BlockViewController.h"
#import "ScrollTemplateViewController.h"
#import "PhotoDetailViewController.h"
#import "SideViewController.h"



@interface PhotoView ()
{
    BOOL scrollViewEnabled,fetchNext,viewmore;
    UIActivityIndicatorView *loading;
    NSMutableArray *imageArray,*videoArray, *generatedThumbnails;
    
    UIImageView *playImg;
    UIImage *dummyImg;
    NSUInteger thumbnailCount;
    NSMutableDictionary* imageDictionary;
    NSMutableDictionary* photoDictionary;
    CustomizedNavController *cusNav;
    UIButton *backButton;
}

@end

@implementation PhotoView
@synthesize phview;
@synthesize categoryName,totalNumOfCells,refreshMade,obj_ScrollTempResponse,refreshOrButtonAction,nextPage,pullToRefresh,array_ScrollNewsData,responseText,lDescArray,titleColor,catColor,isComingFromScroll;
@synthesize toSendArray, index, receivedResponseText;


- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    if (fetchNext) {
        [self hideFooter];
    }
    [self parse_ScrollNewsData:responseObject];
    [_collView reloadData];
    scrollViewEnabled = TRUE;
    
    
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error
{
    if (fetchNext) {
        [self hideFooter];
    }
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
  }
-(void) hideFooter
{
    fetchNext = false;
    [loading stopAnimating];
    [_footer removeFromSuperview];
    scrollViewEnabled = TRUE;
    _collView.userInteractionEnabled = TRUE;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [defaults setBool:NO forKey:@"isDonePressed"];
     [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    nextPage = [[NSNumber alloc]init];
    photoDictionary = [[NSMutableDictionary alloc] init];
    imageDictionary = [[NSMutableDictionary alloc] init];
    array_ScrollNewsData = [[NSMutableArray alloc]init];
    thumbnailCount = 0;
    videoArray = [[NSMutableArray alloc]init];
    playImg = [[UIImageView alloc]init];
    playImg.image = [UIImage imageNamed:@"play-icon-stroke.png"];
    
    imageArray = [[NSMutableArray alloc]init];
    generatedThumbnails = [[NSMutableArray alloc]init];
    dummyImg = [[UIImage alloc]init];
    dummyImg = [UIImage imageNamed:@"video-not-present.png"];
    self.view.userInteractionEnabled = TRUE;
    totalNumOfCells = [[NSNumber alloc]initWithInt:0];
    
    
    //Hamburger button
    self.navigationItem.hidesBackButton = YES;
    
    //Home button
    //int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    
        UIButton * home = [UIButton buttonWithType:UIButtonTypeCustom];
        home.frame = CGRectMake(0,0, 30, 30);
        [home  addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
        SdImageView *homeImage= [[SdImageView alloc]initWithFrame:home.frame];
        [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
        
        [home addSubview:homeImage];
        UIBarButtonItem* homeBarButton = [[UIBarButtonItem alloc]initWithCustomView:home];
        self.navigationItem.rightBarButtonItem = homeBarButton;
        
   
    //backbuttontransperent
   
    
    [defaults setValue:nil forKey:@"backbuttonphoto"];
    
    
    // Do any additional setup after loading the view from its nib.
    [self.collView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"collCell"];
    
    if (NSOrderedSame==[categoryName caseInsensitiveCompare:@"Photos"]) {
        self.title = @"Photos";
    }
    else if (NSOrderedSame==[categoryName caseInsensitiveCompare:@"Videos"])
    {
        self.title = @"Videos";
    }
    
    
    pullToRefresh = [[UIRefreshControl alloc] init];
    
    
    self.pullToRefresh.tintColor = [UIColor whiteColor];
    
    [_collView addSubview:pullToRefresh];
    [pullToRefresh addTarget:self action:@selector(refreshTableData) forControlEvents:UIControlEventValueChanged];
    [self request_ScrollNewsData];
   
    
    
}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    scrollViewEnabled = true;
}
-(void)viewWillAppear:(BOOL)animated
{
    cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideBackButton:NO];
    [cusNav hideHambergerButton: NO];
    [cusNav hideBackButtonForTransperentImage];
    
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:catColor];
    [cusNav setTitle:categoryName forView:self withHambergur:YES withColor:titleColor];
   
   
    
    
    
    viewmore = false;
    [super viewWillAppear:YES];
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    
    backButton = [UtilityManager customizeBackButtonSubCatColor];
    
    [backButton addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationController.navigationBar.hidden=NO;
    [self.navigationController.navigationBar addSubview:backButton];
    if([defaults valueForKey:@"backbuttonphoto"])
    [UtilityManager setTransperentImageColor:[defaults valueForKey:@"backbuttonphoto"] forButton:backButton];

    
    if(![defaults boolForKey:@"isDonePressed"])
    {
        SideViewController *leftMenuViewController;
        if(IPAD){
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
        }
        else{
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
            
        }
        leftMenuViewController.isNewsTemplate = FALSE;
        if(menulevel > 1)
        {
            leftMenuViewController.isSideMenu = YES;
        }
        [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
        //[self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    if ([defaults boolForKey:@"isDonePressed"])
    {
        [defaults setBool:NO forKey:@"isDonePressed"];
    }
}
-(void)popBackToPreviousPage
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)viewWillDisappear:(BOOL)animated
{
      [super viewWillDisappear:YES];
    [backButton removeFromSuperview];
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]], NSForegroundColorAttributeName,[UIFont fontWithName:@"Helvetica-Bold" size:17.0], NSFontAttributeName,nil]];
    scrollViewEnabled = NO;
    for (NSOperation *operation in [HTTPClient sharedHTTPClient].manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        NSLog(@"operation:%@",operation);
        AFHTTPRequestOperation *operations = (AFHTTPRequestOperation*)operation;
        if([[[[operations request] URL] absoluteString] containsString:@"band/getnewsscroltemplate"]){
            [operation cancel];
        }
        
    }

}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}


-(void) refreshTableData
{
    nextPage = 0;
    totalNumOfCells = [[NSNumber alloc]initWithInt:0];
    [HubCitiAppDelegate removeActivityIndicator];
    refreshOrButtonAction = YES;
    refreshMade = YES;
    [self request_ScrollNewsData];
}
- (void)leftSideMenuButtonPressed
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        //send bookmark array to side menu on hambrger click
        if(!(self.menuContainerViewController.menuState == MFSideMenuStateClosed)){
            
        }
    }];
}


-(void)returnToMainPage:(id)sender{
    
    if (refreshMade || fetchNext) {
        return;
    }
    // [self performSelector:@selector(returnToHome) withObject:nil afterDelay:2];
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
    }
    else
    {
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (!scrollViewEnabled)
    {
        return;
    }
    CGSize contentSize = scrollView.contentSize;
    CGPoint contentOffset = scrollView.contentOffset;
    
    if (contentSize.height < SCREEN_HEIGHT && [nextPage intValue]  == 0) {
        return;
    }
    
    if ((contentOffset.y + scrollView.bounds.size.height > contentSize.height) && [nextPage intValue] == 1)
    {
        [self fetchNextNews];
    }
}

-(void)fetchNextNews

{
    viewmore = true;
    scrollViewEnabled = NO;
    fetchNext = true;
    [HubCitiAppDelegate removeActivityIndicator];
    
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    
    loading.color = [UIColor whiteColor];
    
    [loading startAnimating];
    
    CGFloat height = loading.frame.size.height + 6.f;
    
    if(IPAD)
    {
        _footer = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-height - VARIABLE_HEIGHT(40) , self.view.bounds.size.width, height)];
    }
    else
    {
        _footer = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-height - VARIABLE_HEIGHT(64), self.view.bounds.size.width, height)];
    }
    
    
    _footer.backgroundColor = [UIColor blackColor];
    
    [_footer addSubview:loading];
    loading.center = CGPointMake(self.view.bounds.size.width / 2, height / 2.f);
    [self.view addSubview:_footer];
    _collView.userInteractionEnabled = false;
    [self request_ScrollNewsData];
    
}
-(void) request_ScrollNewsData
{
    
    [HubCitiAppDelegate removeActivityIndicator];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    //[parameters setValue:@"8" forKey:@"hubCitiId"];
    
    [parameters setValue:categoryName forKey:@"catName"];
    //[parameters setValue:@"Tennis" forKey:@"catName"];
    [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    //[parameters setValue:@"2113" forKey:@"userId"];
    [parameters setValue:totalNumOfCells forKey:@"lowerLimit"];
    [parameters setValue:@"1" forKey:@"isSideBar"];
    NSString *level = [defaults valueForKey:KEY_MENULEVEL];
    [parameters setValue:level forKey:@"level"];
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    NSLog(@"%d--->%@",menulevel,[HubCitiAppDelegate getLinkIdArray]);
    if(menulevel>1){
        NSLog(@"%d",menulevel);
        NSString *linkId= [[HubCitiAppDelegate getLinkIdArray]objectAtIndex:menulevel-1];
        [parameters setValue:linkId forKey:@"linkId"];
    }
    else{
        
        [parameters setValue:@"0" forKey:@"linkId"];
    }
    
    
    DLog(@"parameter: %@",parameters);
//     NSString *urlString = [NSString stringWithFormat:@"http://10.10.221.8:9990/HubCiti_BckBtn/band/getnewsscroltemplate"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@band/getnewsscroltemplate",BASE_URL];
    NSLog(@"%@",urlString);
    
    
    
    if ([nextPage intValue] == 1)
    {
        NSLog(@"Json = %@", parameters);
        HTTPClient* client = [HTTPClient sharedHTTPClient];
        client.delegate = self;
        NSLog(@"URL = %@", urlString);
        [client sendRequest:parameters : urlString];
        
    }
    else
    {
        if( refreshMade == YES)
        {
            [HubCitiAppDelegate removeActivityIndicator];
        }
        else
        {
            [HubCitiAppDelegate showActivityIndicator];
        }
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        [manager.requestSerializer setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [manager POST:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            NSLog(@"template changed %@",[responseObject objectForKey:@"templateChanged"]);
            // [self parse_ScrollNewsData:responseObject];
            //            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) ,^(void){
            //                [self parse_ScrollNewsData:responseObject];
            //            });
            [self parse_ScrollNewsData:responseObject];
            
            //            SdImageView *bannerSdImageView = [[SdImageView alloc] initWithFrame:CGRectMake(10, 0, 86, 35)];
            //            bannerSdImageView.backgroundColor = [UIColor clearColor];
            //            //asyncImageView.layer.cornerRadius = 5.0f;
            //            bannerSdImageView.contentMode = UIViewContentModeScaleAspectFit;
            //            [bannerSdImageView loadImage: [defaults valueForKey:@"bannerImg"]];
            //
            //            [self.navigationController.navigationBar.topItem setTitleView:bannerSdImageView];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [HubCitiAppDelegate removeActivityIndicator];
            
            NSLog(@"Error: %@", error);
        }];
    }
    
}
-(void) parse_ScrollNewsData : (id) responseData
{
    NSLog(@"Response data %@", responseData);
    if (refreshMade == YES)
    {
        refreshMade = NO;
        array_ScrollNewsData = [[NSMutableArray alloc]init];
        generatedThumbnails = [[NSMutableArray alloc]init];
        lDescArray = [[NSMutableArray alloc]init];
        thumbnailCount = 0;
    }
    if (responseData == nil)
        return;
    if (obj_ScrollTempResponse == nil) {
        obj_ScrollTempResponse = [[ScrollTemplateResponse alloc] init];
    }
    @try{
        [obj_ScrollTempResponse setValuesForKeysWithDictionary:responseData];
    }
    @catch (NSException *exception) {
        // Do nothing
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
        return;
    }
    
    if (refreshOrButtonAction == YES)
    {
        nextPage = 0;
        refreshOrButtonAction = NO;
        [pullToRefresh endRefreshing];
        if ( ![obj_ScrollTempResponse.responseCode isEqualToString:@"10000"])
        {
            [UtilityManager showAlert:nil msg:obj_ScrollTempResponse.responseText];
            return;
            
        }
        
        
    }
    NSLog(@"response = %@", obj_ScrollTempResponse);
    
    
    
    
    if ([obj_ScrollTempResponse.responseCode isEqualToString:@"10000"] )
    {
        
        
        [self parseSuccessScroll : responseData];
        
    }
    else
    {
        
        nextPage = 0;
        if(obj_ScrollTempResponse.backButtonColor){
            [defaults setValue:obj_ScrollTempResponse.backButtonColor forKey:@"backbuttonphoto"];
            [UtilityManager setTransperentImageColor:obj_ScrollTempResponse.backButtonColor forButton:backButton];
        }
        
        [UtilityManager showAlert:nil msg:obj_ScrollTempResponse.responseText];
    }
    viewmore = false;
    
    
    
    
    
}

-(void) parseSuccessScroll : (id) response
{
    
    if(obj_ScrollTempResponse.nextPage)
    {
        nextPage = obj_ScrollTempResponse.nextPage;
    }
    if(obj_ScrollTempResponse.lowerLimitFlag)
    {
        totalNumOfCells = obj_ScrollTempResponse.lowerLimitFlag;
    }
    if( obj_ScrollTempResponse.bannerImg)
    {
        [defaults setValue:obj_ScrollTempResponse.bannerImg forKey:@"bannerImgSubPage"];
    }
    
    if(obj_ScrollTempResponse.backButtonColor){
        [defaults setValue:obj_ScrollTempResponse.backButtonColor forKey:@"backbuttonphoto"];
    [UtilityManager setTransperentImageColor:obj_ScrollTempResponse.backButtonColor forButton:backButton];
    }
    if ( obj_ScrollTempResponse.items != nil)
    {
        if(!viewmore)
        {
            array_ScrollNewsData = [[NSMutableArray alloc]init];
            generatedThumbnails = [[NSMutableArray alloc]init];
            thumbnailCount = 0;
        }
        NSLog(@"Beginning of scroll: array_ScrollNewsData.count = %ld, generatedThumbnails.count = %ld",(unsigned long)array_ScrollNewsData.count,(unsigned long)generatedThumbnails.count);
        
        for (int i=0;i< obj_ScrollTempResponse.items.count;i++)
        {
            ScrollTemplateItems * scrollItemResponseData = [[ScrollTemplateItems alloc]init];
            NSDictionary * dict_scrollItemData = obj_ScrollTempResponse.items[i];
            @try{
                [scrollItemResponseData setValuesForKeysWithDictionary:dict_scrollItemData];
            }
            @catch (NSException *exception) {
                // Do nothing
                [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
                return;
            }
            
            
            [array_ScrollNewsData addObject:scrollItemResponseData];
            
            
        }
        
        
        
    }
    //Thumbnails generation
    /* for(int i = (int)thumbnailCount ; i < array_ScrollNewsData.count; i++)
     {
     if([[array_ScrollNewsData objectAtIndex:i]videoLink] && (NSOrderedSame==[categoryName caseInsensitiveCompare:@"Videos"]) )
     {
     imageArray = [[NSMutableArray alloc]initWithArray:[self convertToArrayFromCommaSeparated:[[array_ScrollNewsData objectAtIndex:i]videoLink]]];
     NSString *urlString = [imageArray objectAtIndex:0];
     
     NSURL *sourceURL = [NSURL URLWithString:urlString];
     if ([[UIDevice currentDevice].systemVersion intValue] >= 8)
     {
     
     AVAsset *asset = [AVAsset assetWithURL:sourceURL];
     AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
     CMTime time = CMTimeMake(1, 1);
     
     CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
     
     
     UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
     CGImageRelease(imageRef);
     if(thumbnail)
     {
     [generatedThumbnails addObject:thumbnail];
     }
     else
     {
     [generatedThumbnails addObject:dummyImg];
     }
     
     thumbnailCount++;
     
     
     
     }
     
     
     }
     else
     {
     [generatedThumbnails addObject:dummyImg];
     thumbnailCount++;
     }
     }
     
     dispatch_async(dispatch_get_main_queue(), ^ {
     
     [_collView reloadData];
     NSLog(@"During reload: array_ScrollNewsData.count = %ld, generatedThumbnails.count = %ld",array_ScrollNewsData.count,generatedThumbnails.count);
     
     scrollViewEnabled = TRUE;
     [HubCitiAppDelegate removeActivityIndicator];
     });
     */
    [HubCitiAppDelegate removeActivityIndicator];
    scrollViewEnabled = TRUE;
    [_collView reloadData];
    
    [receivedResponseText removeObjectAtIndex:index];
    [receivedResponseText insertObject:@"1" atIndex:index];
    ScrollTemplateViewController *responseViewController = [[ScrollTemplateViewController alloc]init];
    responseViewController.responseTextArray = [receivedResponseText mutableCopy];
    
    //[defaults setValue:obj_ScrollTempResponse.modifiedDate forKey:@"scrollModifiedDate"];
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return array_ScrollNewsData.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"collCell";
    
    
    CollectionViewCell *cell = nil;
    
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if(indexPath.row != array_ScrollNewsData.count)
    {
        if([[array_ScrollNewsData objectAtIndex:indexPath.row]title])
        {
            cell.cellLabel.text = [[array_ScrollNewsData objectAtIndex:indexPath.row]title];
        }
        if(IPAD){
            cell.cellLabel.font = [UIFont systemFontOfSize:18];
        }
        
        cell.cellLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        if(NSOrderedSame==[categoryName caseInsensitiveCompare:@"Photos"])
        {
            cell.playIcon.hidden = true;
            if([([array_ScrollNewsData objectAtIndex:indexPath.row]) image])
                
            {
                imageArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[(ScrollTemplateItems*)([array_ScrollNewsData objectAtIndex:indexPath.row]) image]]];
                
                NSString* urlStr = imageArray[0];
                
                if ([photoDictionary objectForKey:urlStr]) {
                    
                    cell.cellImage.image = [photoDictionary objectForKey:urlStr];
                }
                else{
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                        
                        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlStr]];
                        
                        UIImage* image = [[UIImage alloc] initWithData:imageData];
                        
                        
                        if(image){
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                cell.cellImage.image = image;
                                [photoDictionary setObject:image forKey:urlStr];
                            });
                            
                        }
                    });
                }
            }
            
            
        }
        else
            if(NSOrderedSame==[categoryName caseInsensitiveCompare:@"Videos"])
            {
                cell.playIcon.hidden = false;
                @try{
                    NSArray* videoarray = [self convertToArrayFromCommaSeparated:[[array_ScrollNewsData objectAtIndex:indexPath.row] videoLink]];
                    
                    NSString* url = videoarray[0];
                    UIImage *image = [UIImage imageNamed:@"video-not-present.png"];
                    cell.cellImage.image = image;
                    cell.cellImage.contentMode = UIViewContentModeScaleAspectFit;
                    
                    if ([imageDictionary objectForKey:url]) {
                        
                        cell.cellImage.image = [imageDictionary objectForKey:url];
                        cell.playIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
                    }
                    else
                    {
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                            
                            
                            AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:url]];
                            
                            //  Get thumbnail at the very start of the video
                            CMTime time = CMTimeMake(3, 2);
                            
                            
                            //  Get image from the video at the given time
                            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                            imageGenerator.appliesPreferredTrackTransform = YES;
                            
                            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
                            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
                            CGImageRelease(imageRef);
                            
                            if(thumbnail){
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    cell.cellImage.image = thumbnail;
                                    cell.playIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
                                    [imageDictionary setObject:thumbnail forKey:url];
                                    
                                });
                                
                            }
                        });
                    }
                }
                @catch (NSException *exception) {
                    // Do nothing
                    cell.playIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
                }
                
                
                imageArray = [[NSMutableArray alloc]init];
                if([([array_ScrollNewsData objectAtIndex:indexPath.row]) videoLink])
                    
                {
                    imageArray = [[NSMutableArray alloc]initWithArray:[self convertToArrayFromCommaSeparated:[(ScrollTemplateItems*)([array_ScrollNewsData objectAtIndex:indexPath.row]) videoLink]]];
                    
                }
            }
        
        
        cell.photoCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)imageArray.count];
        
        
        if(IPAD)
        {
            cell.photoCount.font = [UIFont systemFontOfSize:18];
        }
        
        cell.photoImg.image = [UIImage imageNamed:@"camera-icon.png"];
    }
    cell.userInteractionEnabled = YES;
    collectionView.userInteractionEnabled = YES;
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(NSOrderedSame==[categoryName caseInsensitiveCompare:@"Photos"])
    {
        toSendArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[(ScrollTemplateItems*)([array_ScrollNewsData objectAtIndex:indexPath.row]) image]]];
    }
    if(NSOrderedSame==[categoryName caseInsensitiveCompare:@"Videos"])
    {
        toSendArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[(ScrollTemplateItems*)([array_ScrollNewsData objectAtIndex:indexPath.row])videoLink]]];
        
    }
    if(toSendArray.count == 1)
    {
        PhotoDetailViewController *evt  = [[PhotoDetailViewController alloc]initWithNibName:@"PhotoDetailViewController" bundle:nil];
        evt.lDescription = [[array_ScrollNewsData objectAtIndex:indexPath.row]lDesc];
        //evt.imgNumber = indexPath.row;
        evt.photoArray = [[NSMutableArray alloc]initWithArray:toSendArray];
        evt.catName = categoryName;
        evt.catColor = catColor;
        evt.titleColor = titleColor;
        evt.newsID = [[array_ScrollNewsData objectAtIndex:indexPath.row]itemID];
        [self.navigationController pushViewController:evt animated:YES];
        
    }
    else if(toSendArray.count == 0)
    {
        [UtilityManager showAlert:nil msg:@"Video not found"];
    }
    else
    {
        PhotoFolderViewController* evt = [[PhotoFolderViewController alloc] initWithNibName:@"PhotoFolderViewController" bundle:nil];
        evt.imgArray = [[NSMutableArray alloc]initWithArray:toSendArray];
        evt.longD = [[array_ScrollNewsData objectAtIndex:indexPath.row]lDesc];
        evt.backButtonColor = obj_ScrollTempResponse.backButtonColor;
        evt.catName = categoryName;
        evt.catColor = catColor;
        evt.titleColor = titleColor;
        evt.newsID = [[array_ScrollNewsData objectAtIndex:indexPath.row]itemID];
        [self.navigationController pushViewController:evt animated:YES];
    }
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
   // CGRect screenRect = [[UIScreen mainScreen] bounds];
   // CGFloat screenWidth = screenRect.size.width;
   // CGFloat screenHeight = screenRect.size.height;
    float cellwidth = (_collView.frame.size.width)/2  ; //Replace the divisor with the column count requirement. Make sure to have it in float.
    float cellheigth = (_collView.frame.size.height - VARIABLE_HEIGHT(20))/3 - (2*3);
    CGSize size = CGSizeMake(cellwidth, cellheigth);
    
    return size;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0; // This is the minimum inter item spacing, can be more
}

-(NSArray *)convertToArrayFromCommaSeparated:(NSString*)string{
    return [string componentsSeparatedByString:@","];
}
-(UIImage*) drawImage:(UIImage*) fgImage
              inImage:(UIImage*) bgImage bigImageSize:(CGSize)bgSize
              atPoint:(CGPoint)  point
             withSize:(CGSize) size
{
    
    UIGraphicsBeginImageContextWithOptions(bgSize, FALSE, 0.0);
    [bgImage drawInRect:CGRectMake(0 , 0, bgSize.width , bgSize.height)];
    [fgImage drawInRect:CGRectMake(point.x, point.y, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
//- (UIEdgeInsets)collectionView: (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(0,5,0,5);
//}// top, left, bottom, right }

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

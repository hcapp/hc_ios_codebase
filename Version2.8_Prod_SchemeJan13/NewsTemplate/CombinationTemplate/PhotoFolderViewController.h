//
//  DetailViewController.h
//  pageSwipe
//
//  Created by Lakshmi H R on 5/18/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PhotoFolderViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,CustomizedNavControllerDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *collView;
@property(strong,nonatomic) NSMutableArray *imgArray;
@property (nonatomic,strong)NSString *longD;
@property(nonatomic,strong)NSString *catName,*catColor,*titleColor,*backButtonColor;
@property(strong,nonatomic) NSNumber *newsID;
@end

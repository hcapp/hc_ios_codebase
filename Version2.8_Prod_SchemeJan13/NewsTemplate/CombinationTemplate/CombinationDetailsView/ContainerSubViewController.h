//
//  ContainerSubViewController.h
//  pageSwipe
//
//  Created by Nikitha on 5/23/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoView.h"
#import "ScrollTemplateResponse.h"

@protocol CustomizedButtonDelegate<NSObject>

@optional
-(void) setBackButtonColor:(NSString *) backlColor;
@end


@interface ContainerSubViewController : UIViewController
@property (nonatomic,weak) id <CustomizedButtonDelegate> customButtondelegate;
@property (nonatomic,strong) ScrollTemplateResponse * obj_ScrollTempResponse;
@property(nonatomic,strong) NSMutableArray * buttonData;
@property(nonatomic,assign) NSUInteger  index;
@property (nonatomic,strong) PhotoView *phview;
@property(nonatomic,strong) NSString* catName;
@property(nonatomic,strong) NSString* catColor;
@property(strong,nonatomic) NSNumber * totalNumOfCells;
@property(nonatomic) BOOL refreshMade;
@property(nonatomic) BOOL refreshOrButtonAction;
@property(strong,nonatomic) NSNumber * nextPage;
@property (nonatomic,strong) UIRefreshControl * pullToRefresh;
@property(nonatomic,strong) NSMutableArray * array_ScrollNewsData;
@property(strong,nonatomic) NSString * responseText;
@property(assign,nonatomic) NSUInteger  numberOfBookMarks;
@property(strong,nonatomic) NSMutableArray * receivedResponseText;
@property(nonatomic,strong) NSMutableArray * subCategoryList;
@property(nonatomic,strong) NSString *subPageName;
@property(nonatomic,strong) NSNumber * isSideBar;

@property (nonatomic, strong) IBOutlet UIToolbar* toolbar;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* back;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* forward;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* refresh;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* stop;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

//
//  BannerTableViewCell.h
//  CombinationTabledCollectionView
//
//  Created by Apple on 5/19/16.
//  Copyright © 2016 Ravindra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BannerTableViewCell : UITableViewCell
@property(nonatomic,strong) UIImageView* bannerImageView;
@property(nonatomic,strong) UIImageView* PlayIcon;
@property(nonatomic,strong) UIButton* sectionButton;
@property(nonatomic,strong) UIImageView * arrowRight;
@property (nonatomic,strong) UIView* transView;
@property (nonatomic,strong) UILabel* titleLbl;
@property (nonatomic,strong) UILabel* subtitleLbl;

@end

//
//  CombinationCategoryViewController.m
//  pageSwipe
//
//  Created by Nikitha on 5/23/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import "CombinationCategoryViewController.h"
//#import "SideMenuTableViewController.h"
#import "ContainerSubViewController.h"
#import "HubCitiConstants.h"
#import "HTTPClient.h"
#import "AppDelegate.h"
#import "CombinationViewController.h"
#import "ScrollTemplateViewController.h"
#import "BlockViewController.h"
#import "SideViewController.h"

@interface CombinationCategoryViewController ()< UIScrollViewDelegate,UIGestureRecognizerDelegate,CustomizedNavControllerDelegate,CustomizedButtonDelegate>
{
    
    UIScrollView * buttonScroll;
    UIButton * buttonList;
    ContainerSubViewController * contentViewController;
    float xVal;
    CGFloat newOffset;
    UIView *rightBorder;
    UIView *leftBorder;
    UIView *secondView;
    CGPoint lastContentOffset;
    CategoryListDetails *obj_categoryListDetails;
    SubCategoryDetails *obj_subCategoryDetails;
    CustomizedNavController *cusNav ;
    UIButton *backBtn;
}


@end

@implementation CombinationCategoryViewController

@synthesize pageViewController,buttonArray,responseTextArray,catName,catColor,titleColor,subCategoryExist;


@synthesize subCategorySelected,setBoolForRefreshAndBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
    
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    //[self parse_bookmarkCategories:responseObject];
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [defaults setBool:NO forKey:@"isDonePressed"];
    [[NSNotificationCenter defaultCenter] addObserver:cusNav selector:@selector(updateHamburgerImage) name:hamburgerImageChangeNotification object:nil];
    [defaults setBool:YES forKey:@"NoPopUpSubPage"];
    
   
    [self.navigationController.navigationBar.topItem setTitleView:nil];
    
    NSLog(@"Navigationbar bartintcolor:%@",  self.navigationController.navigationBar.barTintColor);
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    //Hamburger button
    
    self.navigationItem.hidesBackButton = YES;
    
    //Home button
   // int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
   
        UIButton * home = [UIButton buttonWithType:UIButtonTypeCustom];
        home.frame = CGRectMake(0,0, 30, 30);
        SdImageView *homeImage= [[SdImageView alloc]initWithFrame:home.frame];
        [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
        
        [home  addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [home addSubview:homeImage];
        UIBarButtonItem* homeBarButton = [[UIBarButtonItem alloc]initWithCustomView:home];
        self.navigationItem.rightBarButtonItem = homeBarButton;
  
    obj_categoryListDetails = [[CategoryListDetails alloc]init];
    obj_subCategoryDetails = [[SubCategoryDetails alloc]init];
    
    
    [defaults setValue:nil forKey:@"backbuttonCategory"];
   
    
    if (subCategoryExist)
    {
        ContainerSubViewController *catSubCatPage = [[ContainerSubViewController alloc]initWithNibName:@"ContainerSubViewController" bundle:nil];
        catSubCatPage.customButtondelegate = self;
        catSubCatPage.catName = catName;
        catSubCatPage.isSideBar = [NSNumber numberWithInt:3];
        [self addChildViewController:catSubCatPage];
        [self.view addSubview:catSubCatPage.view];
        [catSubCatPage didMoveToParentViewController:self];
    }
    else{
        [self requestSubCategories];
        
        if(buttonArray.count == 0)
        {
            ContainerSubViewController *catSubCatPage = [[ContainerSubViewController alloc]initWithNibName:@"ContainerSubViewController" bundle:nil];
            catSubCatPage.customButtondelegate = self;
            catSubCatPage.catName = catName;
            catSubCatPage.isSideBar = [NSNumber numberWithInt:1];
            [self addChildViewController:catSubCatPage];
            [self.view addSubview:catSubCatPage.view];
            [catSubCatPage didMoveToParentViewController:self];
            
        }
        
        
        else
        {
            self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
            self.pageViewController.delegate = self;
            self.pageViewController.dataSource = self;
            self.pageViewController.view.backgroundColor = [UIColor whiteColor];
            pageViewController.view.frame =CGRectMake(VARIABLE_WIDTH(5), VARIABLE_HEIGHT(30), self.view.frame.size.width, SCREEN_HEIGHT);
            [self addChildViewController:self.pageViewController];
            [[self view] addSubview:[self.pageViewController view]];
            
            
            [self.pageViewController didMoveToParentViewController:self];
        }
    }
    
    //setup
    if(buttonArray.count!=0)
    {
        [self setUpTopTabBar];
        //Book Mark Scroll View
        
        
    }
    
    if (subCategorySelected)
    {
        [self setColor : subCategorySelected : nil];
        
        [defaults setInteger:subCategorySelected forKey:@"currentIndexSubPage"];
    }
    else{
        
        [defaults setInteger:subCategorySelected forKey:@"currentIndexSubPage"];
    }
    
    
    
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    cusNav =(CustomizedNavController *) self.navigationController;
    [cusNav hideBackButton:NO];
    [cusNav hideHambergerButton: NO];
    [cusNav hideBackButtonForTransperentImage];
    
    //cusNav.customNavBardelegate = self;
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:catColor];
    
    [cusNav setTitle:catName forView:self withHambergur:YES withColor:titleColor];
    
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    NSLog(@" current index is %ld",(long)[defaults integerForKey:@"currentIndexSubPage"]);
    if (userIdentifier==FALSE)
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {// iOS 8.0 and later
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        } else { // below iOS 8.0
            
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        }
    }
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
   
    if(menulevel<=1){
        [NewsUtility refreshLinkDeptArray];
    }
    NSLog(@"center:%@",[defaults valueForKey:@"centerViewController"]);
   
    
    backBtn = [UtilityManager customizeBackButtonSubCatColor];
    
    [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];

    self.navigationController.navigationBar.hidden=NO;
    [self.navigationController.navigationBar addSubview:backBtn];
    if([defaults valueForKey:@"backbuttonCategory"])
   [UtilityManager setTransperentImageColor:[defaults valueForKey:@"backbuttonCategory"] forButton:backBtn];
    
    if(![defaults boolForKey:@"isDonePressed"])
    {
        SideViewController *leftMenuViewController;
        if(IPAD){
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
        }
        else{
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
            
        }
        leftMenuViewController.isNewsTemplate = FALSE;
        if(menulevel > 1)
        {
            leftMenuViewController.isSideMenu = YES;
        }
        [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
        //[self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    if ([defaults boolForKey:@"isDonePressed"])
    {
        [defaults setBool:NO forKey:@"isDonePressed"];
    }
    if(buttonArray.count != 0)
        //setup
    {
        // [self setUpTopTabBar];
        responseTextArray = [[NSMutableArray alloc]init];
        for (int i=0;i< buttonArray.count ; i++)
        {
            [responseTextArray addObject:@""];
        }
        NSArray *viewControllers;
       // NSInteger presentIndex = [defaults integerForKey:@"currentIndexSubPage"];
        
        
        viewControllers = [NSArray arrayWithObject:[self viewControllerAtIndex:[defaults integerForKey:@"currentIndexSubPage"]]];
        
        
        
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        
    }
    
    
    
}
-(void)popBackToPreviousPage
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    [self.navigationController popViewControllerAnimated:NO];
}
-(void) dealloc
{
    [ [NSNotificationCenter defaultCenter] removeObserver:cusNav name:hamburgerImageChangeNotification object:nil];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [backBtn removeFromSuperview];
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]], NSForegroundColorAttributeName,[UIFont fontWithName:@"Helvetica-Bold" size:17.0], NSFontAttributeName,nil]];
    
}
//delegate method
-(void) setBackButtonColor: (NSString*) backButtonColor{
    
    UIImage *image = [[UIImage imageNamed:@"back_icon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [backBtn setImage:image forState:UIControlStateNormal];
    backBtn.tintColor = [UIColor convertToHexString:backButtonColor];
    
}

-(void)returnToMainPage:(id)sender{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendDetailsNotification" object:self.navigationController];
    
    
    /*if([defaults boolForKey:@"newsTemplateExist"]){
     
     
     // [NewsUtility pushViewFromHamberger:self];
     if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"Combination News Template"]){
     [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
     
     }
     else if ([[defaults valueForKey:@"selectedTemplateName"] isEqualToString:@"News Tile Template"]){
     [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
     }
     else{
     [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
     }
     }
     else
     {
     
     [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
     }
     */
}
-(void) setUpTopTabBar
{
    //    tabBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, VARIABLE_HEIGHT(30))];
    //    tabBarView.backgroundColor = [UIColor convertToHexString:@"#f1f1f1"];
    //    [self.view addSubview:tabBarView];
    //    //Left Search Button for old news search
    //    leftSearchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    leftSearchBtn.frame =CGRectMake(VARIABLE_WIDTH(10), tabBarView.frame.size.height/2 - VARIABLE_HEIGHT(15)/2, VARIABLE_WIDTH(15),VARIABLE_HEIGHT(15));
    //    [leftSearchBtn setBackgroundImage:[UIImage imageNamed:@"search2.png"] forState:UIControlStateNormal];
    //    [tabBarView addSubview:leftSearchBtn];
    //    [leftSearchBtn addTarget:self action:@selector(leftSearchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    secondView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,VARIABLE_HEIGHT(30))];
    
    secondView.backgroundColor = [[UIColor convertToHexString:catColor]colorWithAlphaComponent:0.5];
    
    buttonScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(VARIABLE_WIDTH(120), 0, SCREEN_WIDTH- VARIABLE_WIDTH(120), VARIABLE_HEIGHT(30))];
    
    float width=(SCREEN_WIDTH-buttonScroll.frame.origin.x)/3;
    
    buttonScroll.showsHorizontalScrollIndicator = YES;
    buttonScroll.showsVerticalScrollIndicator = NO;
    buttonScroll.alwaysBounceVertical = NO;
    buttonScroll.delegate = self;
    [secondView addSubview:buttonScroll];
    [self.view addSubview:secondView];
    NSLog(@"%f button scroll height %f", buttonScroll.frame.size.height, VARIABLE_HEIGHT(20));
    
    
    //Array for book mark buttons
    xVal=0;
    
    newOffset=0;
    
    // Array to display all the book mark button in scroll view
    xVal=0;
    // [self request_newsListing];
    // buttonArray = [[NSMutableArray alloc]initWithObjects:@"Top Stories",nil];
    newOffset=0;
    if (buttonArray.count == 1)
    {
        for(int i=0;i<buttonArray.count;i++)
        {
            buttonList = [UIButton buttonWithType:UIButtonTypeCustom];
            
            buttonList.frame = CGRectMake(buttonScroll.frame.size.width/2 - width/2, 0,width, VARIABLE_HEIGHT(30));
            leftBorder = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, buttonScroll.frame.size.height)];
            leftBorder.backgroundColor = [UIColor whiteColor];
            [buttonList addSubview:leftBorder];
            [buttonList setTitle:buttonArray[i] forState:UIControlStateNormal];
            buttonList.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 20.0f:12.0f)];
            buttonList.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonList.titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
            [buttonList setTitleColor:[UIColor convertToHexString:titleColor] forState:UIControlStateNormal];
            buttonList.tag = i;
            buttonList.userInteractionEnabled = NO;
            rightBorder = [[UIView alloc] initWithFrame:CGRectMake(buttonList.frame.size.width, 0, 1, buttonScroll.frame.size.height)];
            rightBorder.backgroundColor = [UIColor whiteColor];
            [buttonList addSubview:rightBorder];
            
            if( buttonList.tag == 0)
            {
                buttonList.backgroundColor = [[UIColor convertToHexString:catColor] colorWithAlphaComponent:1];
                
                
            }
            [buttonList addTarget:self action:@selector(subCategoryButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [buttonScroll addSubview:buttonList];
            
            xVal = xVal +width;
        }
        // COntent size for book mark scroll view
        [buttonScroll setContentSize:CGSizeMake(xVal,0)];
    }
    
    else if (buttonArray.count == 2)
    {
        float width_For_Two_Cat = (SCREEN_WIDTH-buttonScroll.frame.origin.x)/2;
        for(int i=0;i<buttonArray.count;i++)
        {
            CGSize mainTitleSize = [self calculateLabelWidth:buttonArray[i] withWidth:width_For_Two_Cat];
            buttonList.titleLabel.numberOfLines = 1;
            buttonList = [UIButton buttonWithType:UIButtonTypeCustom];
            buttonList.frame = CGRectMake(xVal, 0,mainTitleSize.width + VARIABLE_WIDTH(30), VARIABLE_HEIGHT(30));
            [buttonList setTitle:buttonArray[i] forState:UIControlStateNormal];
            buttonList.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 20.0f:12.0f)];
            
            buttonList.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonList.titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
            [buttonList setTitleColor:[UIColor convertToHexString:titleColor] forState:UIControlStateNormal];
            buttonList.tag = i;
            if( buttonList.tag == 0)
            {
                
                buttonList.backgroundColor = [[UIColor convertToHexString:catColor]colorWithAlphaComponent:1];
            }
            [buttonList addTarget:self action:@selector(subCategoryButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [buttonScroll addSubview:buttonList];
            xVal = xVal +buttonList.frame.size.width;
        }
        // COntent size for book mark scroll view
        [buttonScroll setContentSize:CGSizeMake(xVal + 40,0)];
        
    }
    
    
    else
    {
        //width = (SCREEN_WIDTH-VARIABLE_WIDTH(120))/3;
        
        for(int i=0;i<buttonArray.count;i++)
        {
            width =  (SCREEN_WIDTH - buttonScroll.frame.origin.x)/3;
            
            
            
            
            CGSize mainTitleSize = [self calculateLabelWidth:buttonArray[i] withWidth:width];
            //CGSize mainTitleSize = [buttonArray[i] boundingRectWithSize:(width, VARIABLE_HEIGHT(20) options:NSStringDrawingUsesDeviceMetrics context:NSStringDrawingUsesLineFragmentOrigin];
            buttonList = [UIButton buttonWithType:UIButtonTypeCustom];
            
            
            // width = [self calculateLabelWidth:buttonArray[i] withWidth:width];
            buttonList.frame = CGRectMake(xVal, 0,mainTitleSize.width + VARIABLE_WIDTH(30), VARIABLE_HEIGHT(30));
            NSLog(@" %f %f %f ",buttonList.frame.origin.x, buttonList.frame.origin.y + buttonList.frame.size.height , buttonList.frame.size.width);
            
            [buttonList setTitle:buttonArray[i] forState:UIControlStateNormal];
            buttonList.titleLabel.font = [UIFont systemFontOfSize:(IPAD ? 20.0f:12.0f)];
            
            
            
            buttonList.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonList.titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
            [buttonList setTitleColor:[UIColor convertToHexString:titleColor] forState:UIControlStateNormal];
            buttonList.tag = i;
            if( buttonList.tag == 0)
            {
                
                buttonList.backgroundColor = [[UIColor convertToHexString:catColor] colorWithAlphaComponent:1];
            }
            [buttonList addTarget:self action:@selector(subCategoryButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [buttonScroll addSubview:buttonList];
            xVal = xVal +buttonList.frame.size.width;
        }
        
        
        // COntent size for book mark scroll view
        [buttonScroll setContentSize:CGSizeMake(xVal + 40,0)];
    }
    
    
    
}

-(CGSize)calculateLabelWidth:(NSString*) labelString withWidth:(CGFloat) labelwidth
{
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:(IPAD ? 20.0f:12.0f)]};
    
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, VARIABLE_HEIGHT(20))
                                            options:NSStringDrawingUsesDeviceMetrics
                                         attributes:attributes
                                            context:nil];
    return rect.size;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;// (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait;
}


-(void) requestSubCategories
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if(self.catName)
    {
        [parameters setValue:self.catName forKey:@"catName"];
        
    }
    [parameters setValue:catName forKey:@"catName"];
    //[parameters setValue:@"10" forKey:@"userId"];
    if([defaults valueForKey:KEY_USERID])
    {
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    }
    //10.10.221.8
    NSString *urlString = [NSString stringWithFormat:@"%@band/getnewsubcategories",BASE_URL];
   
  
    NSLog(@"%@",parameters);
    NSLog(@"%@",urlString);
    NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
    
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"%@",responseData);
    [HubCitiAppDelegate removeActivityIndicator];
    [self parse_subCategories:responseData];
    
}

-(void) parse_subCategories:(id)responseData
{
    if(responseData == nil)
        return;
    if (obj_categoryListDetails == nil)
    {
        obj_categoryListDetails = [[CategoryListDetails alloc]init];
    }
    @try{
        [obj_categoryListDetails setValuesForKeysWithDictionary:responseData];
    }
    @catch (NSException *exception) {
        // Do nothing
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
        return;
    }
    if ([obj_categoryListDetails.responseCode isEqualToString:@"10000"] )
    {
        buttonArray = [[NSMutableArray alloc]init];
        if(obj_categoryListDetails.listCatDetails != nil)
        {
            for(int i=0; i< obj_categoryListDetails.listCatDetails.count; i++)
            {
                obj_subCategoryDetails = [[SubCategoryDetails alloc]init];
                NSArray * subCatArray = [obj_categoryListDetails.listCatDetails[i] objectForKey:@"listCatDetails"];
                for( int j= 0; j< subCatArray.count; j++)
                {
                    NSDictionary *dict_catListDetails = subCatArray[j];
                    @try{
                        [obj_subCategoryDetails setValuesForKeysWithDictionary:dict_catListDetails];
                        
                    }
                    @catch (NSException *exception) {
                        // Do nothing
                        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
                        return;
                    }
                    
                    
                    [buttonArray addObject:obj_subCategoryDetails.subCatName];
                    
                }
                
            }
        }
    }
    else
    {
       
    }
    
    
    
    
}



- (void)leftSideMenuButtonPressed:(id)sender
{
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        //send bookmark array to side menu on hambrger click
        //        if(!(self.menuContainerViewController.menuState == MFSideMenuStateClosed)){
        //            [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationName" object:buttonArray];
        //
        //        }
    }];
    
}


- (ContainerSubViewController *)viewControllerAtIndex:(NSUInteger)index
{
    contentViewController = [[ContainerSubViewController alloc] initWithNibName:@"NewsContainerViewController" bundle:nil];
    contentViewController.customButtondelegate = self;
    subCategorySelected = 0;
    contentViewController.isSideBar = [NSNumber numberWithInt:3];
    contentViewController.index = index;
    contentViewController.catName = buttonArray[index];
    contentViewController.receivedResponseText = responseTextArray;
    contentViewController.subCategoryList = buttonArray;
    contentViewController.refreshOrButtonAction = setBoolForRefreshAndBtn;
    setBoolForRefreshAndBtn = NO;
    NSLog(@"Number of buttons %lu", (unsigned long)buttonArray.count);
    contentViewController.numberOfBookMarks = buttonArray.count;
    [self.pageViewController addChildViewController:contentViewController];
    [contentViewController didMoveToParentViewController:self.pageViewController];
    contentViewController.view.frame = CGRectMake(0, VARIABLE_HEIGHT(30), SCREEN_WIDTH, SCREEN_HEIGHT);
    contentViewController.view.backgroundColor = [UIColor whiteColor];
    return contentViewController;
    
}


-(void) subCategoryButtonClicked : (id) sender
{
    if ([sender isMemberOfClass:[UIButton class]])
    {
        setBoolForRefreshAndBtn = YES;
        UIButton *btn = (UIButton *)sender;
        [self setColor : btn.tag : btn];
        for(int i=0;i<buttonArray.count;i++)
        {
            if([btn.currentTitle isEqualToString:buttonArray[i]])
            {
                [defaults setInteger:i forKey:@"currentIndexSubPage"];
                ContainerSubViewController *initialViewController =  [self viewControllerAtIndex:i];
                NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
                [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
                //                [self addChildViewController:self.pageViewController];
                //                pageViewController.view.frame =CGRectMake(0, VARIABLE_HEIGHT(30), SCREEN_WIDTH, SCREEN_HEIGHT);
                //                [[self view] addSubview:[self.pageViewController view]];
                //                [self.pageViewController didMoveToParentViewController:self];
            }
        }
    }
}

-(void) setColor : (NSInteger) tag : (id) btn
{
    for (id subview in [buttonScroll subviews])
    {
        if ([subview isMemberOfClass:[UIButton class]])
        {
            UIButton *prevButton= (UIButton*) subview;
            if(prevButton.tag == tag)
            {
                UIButton * currentBtn = prevButton;
                //[currentBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                currentBtn.backgroundColor = [[UIColor convertToHexString:catColor] colorWithAlphaComponent:1];
                rightBorder.backgroundColor = [UIColor whiteColor];
                leftBorder.backgroundColor = [UIColor whiteColor];
                
                
            }
            else
            {
                rightBorder.backgroundColor = [UIColor whiteColor];
                leftBorder.backgroundColor = [UIColor whiteColor];
                prevButton.backgroundColor = [UIColor clearColor];
            }
        }
        
    }
    
    float width;
    
    if((buttonArray.count)==2)
    {
        width = (SCREEN_WIDTH - buttonScroll.frame.origin.x)/2;
    }
    
    else
    {
        width=(SCREEN_WIDTH-buttonScroll.frame.origin.x)/3;
    }
    // width = (SCREEN_WIDTH - buttonScroll.frame.origin.x);
    CGSize moveSize = [self calculateLabelWidth:buttonArray[tag] withWidth:width];
    
    if (tag >=2)
    {
        int start  = (tag - 1)*width;
        newOffset = start;
        
        if (tag < buttonArray.count-1 )
        {
            [buttonScroll setContentOffset:CGPointMake(start, 0.0) animated:YES];
        }
        else if(subCategorySelected && tag==buttonArray.count-1){
            [buttonScroll setContentOffset:CGPointMake((buttonArray.count-3) * moveSize.width, 0.0) animated:YES];
        }
        else if (tag == buttonArray.count -1)
        {
            [buttonScroll setContentOffset:CGPointMake((buttonArray.count-1) * moveSize.width, 0.0) animated:YES];
        }
        else
        {
            [buttonScroll setContentOffset:CGPointMake(moveSize.width + 40, 0.0) animated:YES];
        }
    }
    else if ((buttonArray.count == 2) && (tag == 1))
    {
        [buttonScroll setContentOffset:CGPointMake(moveSize.width + 40, 0.0) animated:YES];
        
    }
    else
    {
        [buttonScroll setContentOffset:CGPointMake(0.0,0.0) animated:YES];
        newOffset=0;
    }
    
    
    
}

#pragma delegate for UIScrollView
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    newOffset=scrollView.contentOffset.x ;
    if (scrollView.contentOffset.x != lastContentOffset.x)
    {
        buttonScroll.frame = CGRectMake(VARIABLE_WIDTH(3), 0, SCREEN_WIDTH - VARIABLE_WIDTH(6), VARIABLE_HEIGHT(30));
    }
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    lastContentOffset = scrollView.contentOffset;
}
- (void) scrollViewDidEndDecelerating: (UIScrollView *) scrollView
{
    lastContentOffset = scrollView.contentOffset;
}
# pragma Delegates and data source for UIPageViewController

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger  index = ((ContainerSubViewController*) viewController).index;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    
    NSUInteger index = ((ContainerSubViewController*) viewController).index;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == buttonArray.count) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (void)pageViewController:(UIPageViewController *)pageViewControllerDelegate didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed
{
    [HubCitiAppDelegate removeActivityIndicator];
    NSInteger   currentIndex= [(ContainerSubViewController*)[pageViewControllerDelegate.viewControllers firstObject] index];
    NSInteger  previousIndex= [(ContainerSubViewController*)[previousViewControllers firstObject] index];
    NSLog(@"Current is %lu  previous =  %lu", (unsigned long)currentIndex,(unsigned long)previousIndex);
    [self setColor:currentIndex :nil];
    
    [defaults setInteger:currentIndex forKey:@"currentIndexSubPage"];
    
    
    
    
    if ( [responseTextArray[currentIndex] length] == 0)
    {
        [defaults setBool:YES forKey:@"NoPopUpSubPage"];
    }
    if ([responseTextArray[currentIndex] length] != 0 && ![responseTextArray[currentIndex] isEqualToString:@"1" ])
    {
        [UtilityManager showAlert:nil msg:responseTextArray[currentIndex]];
      
    }
    
    NSLog(@" response text when succeed %@", responseTextArray[currentIndex]);
    
}



- (void)viewDidLayoutSubviews {
    // self.scrollView.contentOffset = self.contentOffset;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}





@end

//
//  CombinationCategoryViewController.h
//  pageSwipe
//
//  Created by Nikitha on 5/23/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"
#import "CategoryListDetails.h"
#import "SubCategoryDetails.h"

@interface CombinationCategoryViewController : UIViewController<UIPageViewControllerDataSource, UIPageViewControllerDelegate,UIScrollViewDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property(nonatomic,strong)  NSMutableArray * buttonArray;
@property(nonatomic,strong) NSString* catName;
@property(nonatomic,strong) NSString* catColor;
@property(nonatomic,strong) NSMutableArray * responseTextArray;
@property(nonatomic,strong) NSString* titleColor;

@property (nonatomic,assign) NSUInteger subCategorySelected;

@property(nonatomic,assign) BOOL setBoolForRefreshAndBtn;
@property(nonatomic,assign) BOOL subCategoryExist;



@end

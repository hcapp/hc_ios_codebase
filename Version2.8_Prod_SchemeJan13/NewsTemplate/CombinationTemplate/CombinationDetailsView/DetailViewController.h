//
//  DetailViewController.h
//  pageSwipe
//
//  Created by Lakshmi H R on 5/18/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
@property (strong,nonatomic) UIImage* detailImage;
@property (strong,nonatomic) UITextView *detailLD;
@property (strong,nonatomic) UITextView *detailSD;
@property (nonatomic,strong) UILabel *author;
@property (nonatomic,strong) UILabel *datefield;

@end

//
//  CollectionViewCell.h
//  pageSwipe
//
//  Created by Lakshmi H R on 5/19/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *cellImage;
@property (strong, nonatomic) IBOutlet UILabel *cellLabel;
@property (strong, nonatomic) IBOutlet UIImageView *photoImg;
@property (strong, nonatomic) IBOutlet UILabel *photoCount;
@property (strong, nonatomic) IBOutlet UIView *imgSubView;
@property (strong, nonatomic) IBOutlet UIImageView *playIcon;

@end

//
//  ContainerSubViewController.m
//  pageSwipe
//
//  Created by Nikitha on 5/23/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import "ContainerSubViewController.h"
#import "CombinationCategoryViewController.h"
#import "DetailViewController.h"
#import "PhotoView.h"
#import "BlockViewController.h"
#import "HubCitiConstants.h"
#import "NewsDetailViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "VideoViewController.h"
#import "AppDelegate.h"
#import "CombinationViewController.h"
#import "ScrollTemplateViewController.h"
#import "BlockViewController.h"
@interface ContainerSubViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource, HTTPClientDelegate>

{
    NSMutableArray *imageArray;
    CGFloat authourHeight ;
    CGFloat shortDescHeight;
    CGFloat titletHeight ;
    UIActivityIndicatorView *loading;
    UIView * contentView;
    UITableView *newsTableView;
    UIFont * titleFont;
    UIFont * authorFont,*bodyFont;
    
    UIRefreshControl *refreshControl;
    BOOL scrollViewEnabled,fetchNext,viewmore;
    UIView* containerView;
    
    
    
    bool *bigImagePresent;
    UIButton *backBtn;
}

@end

@implementation ContainerSubViewController

@synthesize buttonData,index,phview,totalNumOfCells,refreshMade,obj_ScrollTempResponse,refreshOrButtonAction,nextPage,pullToRefresh,array_ScrollNewsData,responseText,subCategoryList,receivedResponseText,numberOfBookMarks,subPageName,isSideBar,catColor;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    if (fetchNext) {
        [self hideFooter];
    }
    
    
    [self parse_ScrollNewsData:responseObject];
    [newsTableView reloadData];
    scrollViewEnabled = TRUE;
    
    
}
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error
{
    if (fetchNext) {
        [self hideFooter];
    }
    [HubCitiAppDelegate removeActivityIndicator];
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}

-(void) getDetailsNotification : (NSNotification *)notification
{
    if (refreshMade || fetchNext) {
        return;
    }
    else{
        CustomizedNavController* navigation = notification.object;
        if([defaults boolForKey:@"newsTemplateExist"]){
            
            
            // [NewsUtility pushViewFromHamberger:self];
            if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
                [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:navigation];
                
            }
            else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"News Tile Template"]){
                [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:navigation];
            }
            else{
                [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:navigation];
            }
        }
        else
        {
            
            [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:navigation];
        }
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //table view data
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getDetailsNotification:)
                                                 name:@"sendDetailsNotification"
                                               object:nil];
    
    imageArray = [[NSMutableArray alloc]init];
    totalNumOfCells = [[NSNumber alloc]initWithInt:0];
    array_ScrollNewsData = [[NSMutableArray alloc]init];
    nextPage = [[NSNumber alloc]init];
    // [defaults setBool:NO forKey:@"ViewMoreNewsSubPage"];
    NSLog(@"Parent view controller %@",self.parentViewController.view);
    //table view
    
    self.view.frame =CGRectMake(VARIABLE_WIDTH(5), 0, self.view.frame.size.width, SCREEN_HEIGHT);
    newsTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    newsTableView.delegate = self;
    newsTableView.dataSource = self;
    [newsTableView setBackgroundColor:[UIColor whiteColor]];
    newsTableView.scrollEnabled =YES;
    
    newsTableView.userInteractionEnabled = YES;
    [self.view addSubview:newsTableView];
    if(IPAD)
    {
        newsTableView.contentInset = UIEdgeInsetsMake(0, 0, 60, 0);
    }
    else
    {
        newsTableView.contentInset = UIEdgeInsetsMake(0, 0, 80, 0);
    }
    self.view.backgroundColor = [UIColor clearColor];
    titleFont = [UIFont boldSystemFontOfSize:(IPAD ? 20.0f:13.0f)];
    authorFont = [UIFont systemFontOfSize:(IPAD ? 14.0f:10.0f)];
    bodyFont = [UIFont systemFontOfSize:(IPAD ? 16.0f:13.0f)];
    
    newsTableView.bounces = YES;
    for (UIView *view in newsTableView.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            ((UIScrollView *)view).delegate = self;
            break;
        }
    }
    self.view.userInteractionEnabled = YES;
    
    
    
    
    
    
    pullToRefresh = [[UIRefreshControl alloc] init];
    self.pullToRefresh.tintColor = [UIColor blackColor];
    [newsTableView addSubview:pullToRefresh];
    [pullToRefresh addTarget:self action:@selector(refreshTableData) forControlEvents:UIControlEventValueChanged];
    [self request_ScrollNewsData];
    
    //[newsTableView setContentOffset:CGPointMake(0, newsTableView.contentOffset.y- refreshControl.frame.size.height) animated:YES];
    //    [refreshControl beginRefreshing];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
   
    viewmore = false;
    
    
    
    [super viewWillAppear:YES];
}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    scrollViewEnabled = true;
    
}
-(void)popBackToPreviousPage
{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void) hideFooter
{
    fetchNext = false;
    [loading stopAnimating];
    [containerView removeFromSuperview];
    scrollViewEnabled = false;
    newsTableView.tableFooterView = nil;
    newsTableView.userInteractionEnabled = TRUE;
    for (UIScrollView *view in self.parentViewController.view.subviews) {
        
        if ([view isKindOfClass:[UIScrollView class]])
        {
            NSLog(@"Recognizer ");
            view.scrollEnabled = YES;
        }
    }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    scrollViewEnabled = FALSE;
    for (NSOperation *operation in [HTTPClient sharedHTTPClient].manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        NSLog(@"operation:%@",operation);
        AFHTTPRequestOperation *operations = (AFHTTPRequestOperation*)operation;
        if([[[[operations request] URL] absoluteString] containsString:@"band/getnewsscroltemplate"]){
            [operation cancel];
        }
        
    }
    
}
-(void) refreshTableData
{
    nextPage = 0;
    totalNumOfCells = [[NSNumber alloc]initWithInt:0];
    [HubCitiAppDelegate removeActivityIndicator];
    refreshOrButtonAction = YES;
    refreshMade = YES;
    [self request_ScrollNewsData];
}

-(void) setHeight : (NSUInteger ) indexPath
{
    titletHeight = 0;
    authourHeight = 0;
    shortDescHeight = 0;
    if ( [[array_ScrollNewsData objectAtIndex:indexPath] author])
    {
        authourHeight= authorFont.lineHeight;
    }
    if([[array_ScrollNewsData objectAtIndex:indexPath] sDesc])
    {
        shortDescHeight = authorFont.lineHeight;
    }
    if ( [[array_ScrollNewsData objectAtIndex:indexPath] sDesc])
    {
        titletHeight = titleFont.lineHeight;
    }
    
}
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    if([nextPage intValue]==1 && indexPath.row == [array_ScrollNewsData count] ) {
//        // This is the last cell
//        if (![defaults boolForKey:@"ViewMoreNewsSubPage"] && totalNumOfCells !=0) {
//            [defaults setBool:YES forKey:@"ViewMoreNewsSubPage"];
//            //            pullToRefresh = nil;
//            //            [pullToRefresh removeFromSuperview];
//            dispatch_queue_t dispatchQueue = dispatch_queue_create("myQueue", NULL);
//            dispatch_sync(dispatchQueue, ^(void){
//                [self fetchNextNews];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [newsTableView reloadData];
//                    //                    pullToRefresh = [[UIRefreshControl alloc] init];
//                    //                    self.pullToRefresh.tintColor = [UIColor blackColor];
//                    //                    [self.newsTableView addSubview:pullToRefresh];
//                    //                    [pullToRefresh addTarget:self action:@selector(refreshTableData) forControlEvents:UIControlEventValueChanged];
//                    //
//                });
//            });
//
//        }
//    }
//
//}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (!scrollViewEnabled)
    {
        return;
    }
    CGSize contentSize = scrollView.contentSize;
    CGPoint contentOffset = scrollView.contentOffset;
    
    if (contentSize.height < SCREEN_HEIGHT && [nextPage intValue]  == 0) {
        return;
    }
    
    if ((contentOffset.y + scrollView.bounds.size.height > contentSize.height) && [nextPage intValue] == 1)
    {
        [self fetchNextNews];
    }
}
-(void)fetchNextNews

{
    viewmore = true;
    scrollViewEnabled = NO;
    fetchNext = true;
    [HubCitiAppDelegate removeActivityIndicator];
    
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    loading.color = [UIColor blackColor];
    [loading startAnimating];
    CGFloat height = loading.frame.size.height + 6.f;
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, height)];
    containerView.backgroundColor = [UIColor clearColor];
    [containerView addSubview:loading];
    loading.center = CGPointMake(self.view.bounds.size.width / 2, height / 2.f);
    newsTableView.tableFooterView = containerView;
    newsTableView.userInteractionEnabled = false;
    [self request_ScrollNewsData];
    
    
}
-(void) request_ScrollNewsData
{
    
    [HubCitiAppDelegate removeActivityIndicator];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
    //  [parameters setValue:@"8" forKey:@"hubCitiId"];
    
    [parameters setValue:_catName forKey:@"catName"];
    //[parameters setValue:@"Tennis" forKey:@"catName"];
    [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
    //[parameters setValue:@"2113" forKey:@"userId"];
    [parameters setValue:totalNumOfCells forKey:@"lowerLimit"];
    [parameters setValue:isSideBar forKey:@"isSideBar"];
    NSString *level = [defaults valueForKey:KEY_MENULEVEL];
    [parameters setValue:level forKey:@"level"];
    int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
    
    if(menulevel>1){
        NSLog(@"%d",menulevel);
        NSString *linkId= [[HubCitiAppDelegate getLinkIdArray]objectAtIndex:menulevel-1];
        [parameters setValue:linkId forKey:@"linkId"];
        
    }
    else{
        
        [parameters setValue:@"0" forKey:@"linkId"];
    }
    DLog(@"parameter: %@",parameters);
//     NSString *urlString = [NSString stringWithFormat:@"http://10.10.221.8:9990/HubCiti_BckBtn/band/getnewsscroltemplate"];
   NSString *urlString = [NSString stringWithFormat:@"%@band/getnewsscroltemplate",BASE_URL];
    NSLog(@"%@",urlString);
    
    
    
    if ([nextPage intValue] == 1)
    {
        for (UIScrollView *view in self.parentViewController.view.subviews) {
            
            if ([view isKindOfClass:[UIScrollView class]])
            {
                NSLog(@"Recognizer ");
                view.scrollEnabled = NO;
            }
        }
        NSLog(@"Json = %@", parameters);
        HTTPClient* client = [HTTPClient sharedHTTPClient];
        client.delegate = self;
        NSLog(@"URL = %@", urlString);
        [client sendRequest:parameters : urlString];
        
    }
    else
    {
        if( refreshMade == YES)
        {
            [HubCitiAppDelegate removeActivityIndicator];
        }
        else
        {
            [HubCitiAppDelegate showActivityIndicator];
        }
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        [manager.requestSerializer setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [manager POST:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            NSLog(@"template changed %@",[responseObject objectForKey:@"templateChanged"]);
            [self parse_ScrollNewsData:responseObject];
            
            
            //            SdImageView *bannerSdImageView = [[SdImageView alloc] initWithFrame:CGRectMake(10, 0, 86, 35)];
            //            bannerSdImageView.backgroundColor = [UIColor clearColor];
            //            //asyncImageView.layer.cornerRadius = 5.0f;
            //            bannerSdImageView.contentMode = UIViewContentModeScaleAspectFit;
            //            [bannerSdImageView loadImage: [defaults valueForKey:@"bannerImg"]];
            //
            //            [self.navigationController.navigationBar.topItem setTitleView:bannerSdImageView];
            [newsTableView reloadData];
            scrollViewEnabled = TRUE;
            [HubCitiAppDelegate removeActivityIndicator];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [HubCitiAppDelegate removeActivityIndicator];
            
            NSLog(@"Error: %@", error);
        }];
    }
    
}
-(void) parse_ScrollNewsData : (id) responseData
{
    if (refreshMade == YES)
    {
        refreshMade = NO;
        array_ScrollNewsData = [[NSMutableArray alloc]init];
    }
    if (responseData == nil)
        return;
    if (obj_ScrollTempResponse == nil) {
        obj_ScrollTempResponse = [[ScrollTemplateResponse alloc] init];
    }
    @try{
        [obj_ScrollTempResponse setValuesForKeysWithDictionary:responseData];
    }
    @catch (NSException *exception) {
        // Do nothing
        [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
        return;
    }
    
    if (refreshOrButtonAction == YES)
    {
        nextPage = 0;
        refreshOrButtonAction = NO;
        [pullToRefresh endRefreshing];
        if ( ![obj_ScrollTempResponse.responseCode isEqualToString:@"10000"])
        {
            [UtilityManager showAlert:nil msg:obj_ScrollTempResponse.responseText];
            return;
            
        }
        
        
    }
    NSLog(@"response = %@", obj_ScrollTempResponse);
    
    
    
    
    if ([obj_ScrollTempResponse.responseCode isEqualToString:@"10000"] )
    {
        
        
        [self parseSuccessScroll : responseData];
        
    }
    else
    {
        NSLog(@"%@ before adding", receivedResponseText);
        nextPage = 0;
        
        if(obj_ScrollTempResponse.backButtonColor){
            [defaults setValue:obj_ScrollTempResponse.backButtonColor forKey:@"backbuttonCategory"];
            [_customButtondelegate setBackButtonColor: obj_ScrollTempResponse.backButtonColor];
            
        }
        NSLog(@"Integer of current index %ld",(long)[defaults integerForKey:@"currentIndexSubPage"]);
        [receivedResponseText removeObjectAtIndex:index];
        [receivedResponseText insertObject:obj_ScrollTempResponse.responseText atIndex:index];
        NSLog(@"array of response text %@", receivedResponseText);
        CombinationCategoryViewController *responseViewController = [[CombinationCategoryViewController alloc]init];
        responseViewController.responseTextArray = receivedResponseText;
        
        NSLog(@"%d bool ",[defaults boolForKey:@"NoPopUpSubPage"]);
        NSLog(@"%ld current ",(long)[defaults integerForKey:@"currentIndexSubPage"]);
        NSLog(@" index %lu  current %ld  first pop up flag %d", (unsigned long)index,(long)[defaults integerForKey:@"currentIndexSubPage"],[defaults boolForKey:@"firstPopUp"]);
        
        if ([defaults boolForKey:@"NoPopUpSubPage"] && index == [defaults integerForKey:@"currentIndexSubPage"])
        {
            [defaults setBool:NO forKey:@"NoPopUpSubPage"];
            [UtilityManager showAlert:nil msg:obj_ScrollTempResponse.responseText];
        }
        viewmore = false;
        
    }
    
}

-(void) parseSuccessScroll : (id) response
{
    
    if(obj_ScrollTempResponse.nextPage)
    {
        nextPage = obj_ScrollTempResponse.nextPage;
    }
    if(obj_ScrollTempResponse.lowerLimitFlag)
    {
        totalNumOfCells = obj_ScrollTempResponse.lowerLimitFlag;
    }
    if( obj_ScrollTempResponse.bannerImg)
    {
        [defaults setValue:obj_ScrollTempResponse.bannerImg forKey:@"bannerImgSubPage"];
    }
    if(obj_ScrollTempResponse.subPageName)
    {
        subPageName = obj_ScrollTempResponse.subPageName;
    }
    
    if(obj_ScrollTempResponse.backButtonColor){
        [defaults setValue:obj_ScrollTempResponse.backButtonColor forKey:@"backbuttonCategory"];
        [_customButtondelegate setBackButtonColor: obj_ScrollTempResponse.backButtonColor];
        
    }
    
    if ( obj_ScrollTempResponse.items != nil)
    {
        if(!viewmore)
        {
            array_ScrollNewsData = [[NSMutableArray alloc]init];
        }
        for (int i=0;i< obj_ScrollTempResponse.items.count;i++)
        {
            ScrollTemplateItems * scrollItemResponseData = [[ScrollTemplateItems alloc]init];
            NSDictionary * dict_scrollItemData = obj_ScrollTempResponse.items[i];
            @try{
                [scrollItemResponseData setValuesForKeysWithDictionary:dict_scrollItemData];
            }
            @catch (NSException *exception) {
                // Do nothing
                [UtilityManager showAlert:NSLocalizedString(@"Info",@"Info") msg:@"There is a technical problem.\nPlease try after some time."];
                return;
            }
            
            [array_ScrollNewsData addObject:scrollItemResponseData];
            
        }
        
    }
    //[defaults setValue:obj_ScrollTempResponse.modifiedDate forKey:@"scrollModifiedDate"];
    [receivedResponseText removeObjectAtIndex:index];
    [receivedResponseText insertObject:@"1" atIndex:index];
    CombinationCategoryViewController *responseViewController = [[CombinationCategoryViewController alloc]init];
    responseViewController.responseTextArray = [receivedResponseText mutableCopy];
    NSLog(@"%@ yugdsufiashdf ",responseViewController.responseTextArray);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(CGFloat) getLabelSize : (NSString*) labelString withSize:(CGFloat) labelwidth withFont:(UIFont *) font
{
    
    ////    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    ////    float noOflines = ceil(rect.size.height / font.lineHeight);
    ////    NSLog(@"no of line = %f %@", noOflines, labelString);
    ////    NSLog(@"labelheight = %f %f",rect.size.height,font.lineHeight);
    ////    NSLog(@"returningheight = %f",ceil(font.lineHeight*noOflines)+VARIABLE_HEIGHT(10));
    ////    return ceil(font.lineHeight*noOflines) + VARIABLE_HEIGHT(10);
    //
    //    CGSize constraint = CGSizeMake(labelwidth, CGFLOAT_MAX);
    //    CGRect textRect = [labelString boundingRectWithSize:constraint
    //                                                options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
    //                                             attributes:@{NSFontAttributeName:font}
    //                                                context:nil];
    ////    if(IPAD)
    ////    {
    ////        return textRect.size.height;
    ////
    ////    }
    ////    else
    //    {
    //        return textRect.size.height + VARIABLE_HEIGHT(20);
    //    }
    //
    CGRect rect = [labelString boundingRectWithSize:CGSizeMake(labelwidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    float noOflines = ceil(rect.size.height / font.lineHeight);
    if(noOflines > 3)
    {
        noOflines = 3;
    }
    
    return ceil(font.lineHeight*noOflines);
    
}

#pragma Delegates and data source for UITableView
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    if ([nextPage intValue] == 1)
    //    {
    //        return [array_ScrollNewsData count] + 1;
    
    
    //    }
    //    else
    //    {
    return [array_ScrollNewsData count];
    //    }
    
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    CGFloat width ;
    CGFloat firstrow=0 ;
    float rowheight;
    float heightTitle=0;
    float heightsdesc =0;
    //CGFloat derivedTitleHeight;
    
    
    //first row with image has extra height
    if (indexPath.row != array_ScrollNewsData.count)
    {
        
        if([subPageName isEqualToString:@"Big Banner"] && indexPath.row == 0)
        {
            width = SCREEN_WIDTH - VARIABLE_WIDTH(20);
            
            firstrow =VARIABLE_HEIGHT(120)+ [self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont]+[self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] author] withSize:width withFont:authorFont] + VARIABLE_HEIGHT(30);
            
            NSLog(@"%f",firstrow);
        }
        else
        {
            width = SCREEN_WIDTH - VARIABLE_WIDTH(35)- (VARIABLE_HEIGHT(50)*(1.5));
        }
        
        
        CGRect rectFirtsTilte = [[[array_ScrollNewsData objectAtIndex:indexPath.row] title] boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : titleFont} context:nil];
        float noOflines = ceil(rectFirtsTilte.size.height / titleFont.lineHeight);
        
        if(noOflines==1)
            heightTitle = ceil(titleFont.lineHeight);
        else
            heightTitle = ceil(titleFont.lineHeight*noOflines);
        
        rectFirtsTilte = [[[array_ScrollNewsData objectAtIndex:indexPath.row] sDesc] boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : bodyFont} context:nil];
        noOflines = ceil(rectFirtsTilte.size.height / bodyFont.lineHeight);
       
        if (noOflines > 2)
        {
            heightsdesc = ceil(bodyFont.lineHeight*2);
        }
        else
        {
            heightsdesc = ceil(bodyFont.lineHeight*noOflines);
        }

        
        if([(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] author].length>0){
            rowheight =  heightTitle +[self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] author] withSize:width withFont:authorFont] +heightsdesc;
        }
        else if([(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date].length>0){
             rowheight =  heightTitle +[self getLabelSize:[(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date] withSize:width withFont:authorFont]+heightsdesc;
        }else if([(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] time].length>0){
             rowheight =  heightTitle +[self getLabelSize:[(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] time] withSize:width withFont:authorFont]+heightsdesc;
        }
        else{
             rowheight =  heightTitle +heightsdesc;
        }
        
       
        
        if([subPageName isEqualToString:@"Big Banner"] && indexPath.row == 0)
        {
            return firstrow;
        }
        
        if(rowheight +firstrow < VARIABLE_HEIGHT(60))
        {
            return VARIABLE_HEIGHT(65);
        }
        else{
            return rowheight+firstrow+ VARIABLE_HEIGHT(10);
        }
    }
    return 0;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    NSArray *subviews=[cell.contentView subviews];
    
    [subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    CGFloat width ; // width for label in first row and rest of rows
    UILabel *title = [[UILabel alloc] init];
    
    UILabel *tagLabel = [[UILabel alloc]init];
    UIView *newsView;
    SdImageView *bigimage;
    SdImageView *image =[[SdImageView alloc] init];
    UILabel *author = [[UILabel alloc]init];
    UILabel *shortDes = [[UILabel alloc]init];
    UILabel * newsDate;
    UILabel * newsTime;
    // float xPos;
    CGFloat subtitltHeight = 0;
    
    NSString *truncatedAutor;
    
    if (indexPath.row != [array_ScrollNewsData count] && [array_ScrollNewsData count] > 0 )
    {
        [self setHeight:indexPath.row];
        if([(ScrollTemplateItems*)([array_ScrollNewsData objectAtIndex:indexPath.row]) image])
        {
            imageArray = [NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:[(ScrollTemplateItems*)([array_ScrollNewsData objectAtIndex:indexPath.row]) image]]];
            
        }
        //first cell for top stories with the bigger image if present
        if([subPageName isEqualToString:@"Big Banner"] && indexPath.row == 0)
            
        {
            width = SCREEN_WIDTH - VARIABLE_WIDTH(20);
            if(IPAD)
            {
                bigimage=[[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH- VARIABLE_WIDTH(10), VARIABLE_HEIGHT(120)+[self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont]+authourHeight + VARIABLE_HEIGHT(30))];
                newsView = [[UIView alloc]initWithFrame:CGRectMake(0, bigimage.frame.origin.y+bigimage.frame.size.height-[self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont]- authourHeight -VARIABLE_HEIGHT(5) , SCREEN_WIDTH - VARIABLE_WIDTH(10), [self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont]+ authourHeight + VARIABLE_HEIGHT(5))];
                
            }
            else{
                bigimage=[[SdImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - VARIABLE_WIDTH(10) , VARIABLE_HEIGHT(120)+[self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont]+authourHeight + VARIABLE_HEIGHT(30))];
                
                newsView = [[UIView alloc]initWithFrame:CGRectMake(0, bigimage.frame.origin.y+bigimage.frame.size.height-[self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont]- authourHeight -VARIABLE_HEIGHT(5) , SCREEN_WIDTH - VARIABLE_WIDTH(10) , [self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont]+ authourHeight + VARIABLE_HEIGHT(5))];
            }
            
            bigimage.contentMode = UIViewContentModeScaleAspectFit;
            
            
            NSLog(@"height of bigimage = %f",bigimage.frame.size.height);
            NSLog(@"height of newsview = %f",newsView.frame.size.height);
            
            if(imageArray.count)
            {
                [bigimage loadImage:imageArray[0]];
            }
            [cell.contentView addSubview:bigimage];
            
            newsView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
            [cell.contentView addSubview:newsView];
            
            author.frame = CGRectMake(VARIABLE_WIDTH(10), newsView.frame.origin.y + VARIABLE_HEIGHT(5), width, authourHeight);
            
            title.frame =CGRectMake(VARIABLE_WIDTH(10), author.frame.origin.y+author.frame.size.height, width, [self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont]);
            
            // cell.contentView.backgroundColor = [[UIColor clearColor]colorWithAlphaComponent:1];
            
            [newsView addSubview:title];
            [newsView addSubview:author];
            
        }
        
        
        else
        {
            image.frame = CGRectMake(VARIABLE_WIDTH(10), VARIABLE_HEIGHT(10), (VARIABLE_HEIGHT(50)*(1.5)), VARIABLE_HEIGHT(50));
            
           
            
            if(imageArray.count)
            {
                [image loadImage:imageArray[0]];
            }
            image.contentMode = UIViewContentModeScaleAspectFit;
            [cell.contentView addSubview:image];
            
            width = SCREEN_WIDTH - VARIABLE_WIDTH(35)- (VARIABLE_HEIGHT(50)*(1.5));
            NSLog(@"%f",image.frame.size.width);
            
            
            tagLabel.frame = CGRectMake(image.frame.origin.x+image.frame.size.width+5+10, VARIABLE_HEIGHT(10), VARIABLE_WIDTH(40), VARIABLE_HEIGHT(15));
            if(IPAD)
            {
                title.frame =CGRectMake(image.frame.origin.x+image.frame.size.width+5+10, author.frame.origin.y+author.frame.size.height +VARIABLE_HEIGHT(15) - VARIABLE_HEIGHT(10), width, [self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont]);
            }
            else
            {
                title.frame =CGRectMake(image.frame.origin.x+image.frame.size.width+5+10, author.frame.origin.y+author.frame.size.height+3 , width, [self getLabelSize:[[array_ScrollNewsData objectAtIndex:indexPath.row] title] withSize:width withFont:titleFont]);
            }
            float yvalForSdsec = 0;
            truncatedAutor = [[array_ScrollNewsData objectAtIndex:indexPath.row]author];
            float authorXPos=0;
            
            if ( [[array_ScrollNewsData objectAtIndex:indexPath.row] author] || [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date] || [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row]time])
            {
                subtitltHeight = authorFont.lineHeight;
            }
            
            if([[array_ScrollNewsData objectAtIndex:indexPath.row]author].length > 0){
                if(IPAD){
                    if ([truncatedAutor length] >30)
                    {
                        truncatedAutor = [truncatedAutor substringToIndex:30];
                        truncatedAutor = [truncatedAutor stringByAppendingString:@"..."];
                    }
                    else
                    {
                        truncatedAutor = truncatedAutor;
                    }

                }
                else{
                
                if ([truncatedAutor length] >13)
                {
                    truncatedAutor = [truncatedAutor substringToIndex:13];
                    truncatedAutor = [truncatedAutor stringByAppendingString:@"..."];
                }
                else
                {
                    truncatedAutor = truncatedAutor;
                }
            }
                author = [[UILabel alloc]initWithFrame:CGRectMake(authorXPos, title.frame.origin.y+title.frame.size.height,[self widthOfString:truncatedAutor withFont:authorFont], subtitltHeight)];
                
                authorXPos = authorXPos + author.frame.size.width+image.frame.origin.x+image.frame.size.width+18 ;
            if(IPAD)
            {
                author.frame= CGRectMake(image.frame.origin.x+image.frame.size.width+5+10, title.frame.origin.y+title.frame.size.height, width, authourHeight);
            }
            else
            {
                author.frame= CGRectMake(image.frame.origin.x+image.frame.size.width+VARIABLE_HEIGHT(13), title.frame.origin.y+title.frame.size.height, width, authourHeight);
            }
                
                
                
                yvalForSdsec = yvalForSdsec + author.frame.origin.y+author.frame.size.height+VARIABLE_HEIGHT(2);
            }
            NSLog(@"dshsfjkhdrdjkhjkghhj----->:   %f",image.frame.origin.x+image.frame.size.width+VARIABLE_HEIGHT(13));
            NSLog(@"%f",author.frame.size.width);
            
            if ( [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date])
            {
                newsDate = [[UILabel alloc] initWithFrame:CGRectMake((authorXPos>0)? authorXPos: image.frame.origin.x+image.frame.size.width+((IPAD)?15:VARIABLE_HEIGHT(13)) , title.frame.origin.y+title.frame.size.height, [self widthOfString:[(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date] withFont:authorFont], subtitltHeight)];
                NSLog(@"dshsfjkhdrdjkhjkghhj----->:   %f",image.frame.origin.x+image.frame.size.width+VARIABLE_HEIGHT(13));
                authorXPos = (authorXPos>0)? authorXPos
                +newsDate.frame.size.width + VARIABLE_WIDTH(2): (image.frame.origin.x+image.frame.size.width+ ((IPAD)?15:VARIABLE_HEIGHT(13)))+newsDate.frame.size.width + VARIABLE_WIDTH(2);
                if(!yvalForSdsec)
                yvalForSdsec = yvalForSdsec + newsDate.frame.origin.y+newsDate.frame.size.height+VARIABLE_HEIGHT(2);
            }
            if ( [(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] time])
            {
                newsTime = [[UILabel alloc] initWithFrame:CGRectMake((authorXPos>0)? authorXPos:  (image.frame.origin.x+image.frame.size.width+ ((IPAD)?15:VARIABLE_HEIGHT(13))), title.frame.origin.y+title.frame.size.height, [self widthOfString:[(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] time] withFont:authorFont], subtitltHeight)];
                 if(!yvalForSdsec)
                yvalForSdsec = yvalForSdsec + newsTime.frame.origin.y+newsTime.frame.size.height+VARIABLE_HEIGHT(2);

                
            }
            if([[array_ScrollNewsData objectAtIndex:indexPath.row] sDesc])
            {
                
                CGRect rectFirtsTilte = [[[array_ScrollNewsData objectAtIndex:indexPath.row] sDesc] boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : bodyFont} context:nil];
                float noOflines = ceil(rectFirtsTilte.size.height / bodyFont.lineHeight);
                CGFloat heightOFFirstCell;
                if (noOflines > 2)
                {
                    heightOFFirstCell = ceil(bodyFont.lineHeight*2);
                }
                else
                {
                    heightOFFirstCell = ceil(bodyFont.lineHeight*noOflines);
                }
                
               
                
            shortDes.frame = CGRectMake(image.frame.origin.x+image.frame.size.width+5+10, (yvalForSdsec)? yvalForSdsec:title.frame.origin.y+title.frame.size.height +7, width, heightOFFirstCell);
                
            }
            
            title.textColor = [UIColor blackColor];
            author.textColor = [UIColor grayColor];
            newsTime.textColor = [UIColor grayColor];
            newsDate.textColor = [UIColor grayColor];
            shortDes.textColor = [UIColor blackColor];
            
        }
        if([[array_ScrollNewsData objectAtIndex:indexPath.row] author])
        {
            //author.lineBreakMode =NSLineBreakByWordWrapping;
            author.textAlignment = NSTextAlignmentLeft;
            author.font = authorFont;
            author.lineBreakMode = NSLineBreakByTruncatingTail;
            NSLog(@"%@",[[array_ScrollNewsData objectAtIndex:indexPath.row] author]);
            author.text =truncatedAutor;
            [cell.contentView addSubview:author];
        }
        if([(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date])
        {
            //author.lineBreakMode =NSLineBreakByWordWrapping;
            newsDate.textAlignment = NSTextAlignmentLeft;
            newsDate.font = authorFont;
            newsDate.lineBreakMode = NSLineBreakByTruncatingTail;
            NSLog(@"%@",[(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date]);
            newsDate.text =[(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] date];
            [cell.contentView addSubview:newsDate];
        }

        if([(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] time])
        {
            //author.lineBreakMode =NSLineBreakByWordWrapping;
            newsTime.textAlignment = NSTextAlignmentLeft;
            newsTime.font = authorFont;
            newsTime.lineBreakMode = NSLineBreakByTruncatingTail;
            NSLog(@"%@",[(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] time]);
            newsTime.text =[(ScrollTemplateItems*)[array_ScrollNewsData objectAtIndex:indexPath.row] time];
            [cell.contentView addSubview:newsTime];
        }

        if([[array_ScrollNewsData objectAtIndex:indexPath.row] sDesc])
        {
            //shortDes.textAlignment = NSTextAlignmentLeft;
            shortDes.numberOfLines = 2;

            shortDes.font = bodyFont;
            shortDes.lineBreakMode = NSLineBreakByTruncatingTail;
            shortDes.text =[[array_ScrollNewsData objectAtIndex:indexPath.row] sDesc];
            [cell.contentView addSubview:shortDes];
        }
        if([[array_ScrollNewsData objectAtIndex:indexPath.row] title])
        {
            title.numberOfLines = 3;
            title.lineBreakMode = NSLineBreakByTruncatingTail;
            
            
            title.font =titleFont;
            title.text = [[array_ScrollNewsData objectAtIndex:indexPath.row] title] ;
            [cell.contentView addSubview:title];
        }
        if([subPageName isEqualToString:@"Big Banner"] && indexPath.row == 0)
        {
            author.textColor = [UIColor whiteColor];
            title.textColor = [UIColor whiteColor];
        }
        else
        {
            author.textColor = [UIColor grayColor];
            title.textColor = [UIColor blackColor];
        }
        
        
    }
    
    else if (indexPath.row == [array_ScrollNewsData count] && [nextPage intValue]== 1)
    {
        
        loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGRect frame;
        if (DEVICE_TYPE == UIUserInterfaceIdiomPhone) {
            frame.origin.x = 52;
            frame.origin.y = 12;
            frame.size.width = 250;
            frame.size.height = 24;
        }
        else{
            frame.origin.x = 70;
            frame.origin.y = 20;
            frame.size.width = 600;
            frame.size.height = 30;
        }
        loading.frame = frame;
        
        //loading.center = cell.contentView.center;
        loading.color = [UIColor blackColor];
        
        
        [cell.contentView addSubview:loading];
        [loading startAnimating];
        cell.userInteractionEnabled = NO;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


-(NSArray *)convertToArrayFromCommaSeparated:(NSString*)string{
    return [string componentsSeparatedByString:@","];
}

- (CGFloat)widthOfString:(NSString *)labelString withFont:(UIFont *)font
{
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:labelString attributes:attributes] size].width;
    
}
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
////    PhotoView* evt = [[PhotoView alloc] initWithNibName:@"PhotoView" bundle:nil];
////    self.phview = evt;
//
//    NewsDetailViewController * evt = [[NewsDetailViewController alloc] initWithNibName:@"NewsDetailViewController" bundle:nil];
////    VideoViewController *evt = [[VideoViewController alloc]initWithNibName:@"VideoViewController" bundle:nil];
//
//
//    UINavigationController *nav= [[UINavigationController alloc] initWithRootViewController:evt];
//    nav.navigationBar.barTintColor =[UIColor blueColor];
//    [self.menuContainerViewController setLeftMenuViewController:nil];
//    [self.menuContainerViewController setCenterViewController:nav];
////    [self.navigationController pushViewController:evt animated:YES];
//    [newsTableView deselectRowAtIndexPath:[newsTableView indexPathForSelectedRow] animated:NO];
//}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NewsDetailViewController *nd =[[NewsDetailViewController alloc] init];
    
    nd.newsID = [[array_ScrollNewsData objectAtIndex:indexPath.row]itemID];
    [self.parentViewController.navigationController pushViewController:nd animated:NO];
}

@end


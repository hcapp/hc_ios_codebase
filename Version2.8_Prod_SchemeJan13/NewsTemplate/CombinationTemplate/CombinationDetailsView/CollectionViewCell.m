//
//  CollectionViewCell.m
//  pageSwipe
//
//  Created by Lakshmi H R on 5/19/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell
@synthesize photoImg;
@synthesize cellImage;
@synthesize cellLabel;
@synthesize photoCount;
@synthesize imgSubView;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [cellImage addSubview:imgSubView];
    [imgSubView bringSubviewToFront:cellImage];
    [cellImage setClipsToBounds:YES];
    
}

@end

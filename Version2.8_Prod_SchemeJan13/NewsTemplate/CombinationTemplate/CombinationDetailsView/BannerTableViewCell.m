//
//  BannerTableViewCell.h
//  CombinationTabledCollectionView
//
//  Created by Apple on 5/19/16.
//  Copyright © 2016 Ravindra. All rights reserved.
//

#import "BannerTableViewCell.h"

@implementation BannerTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (!(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) return nil;
    // arrow_right.png
    
    _sectionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    _arrowRight = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - VARIABLE_WIDTH(25), 4 , VARIABLE_WIDTH(20), VARIABLE_HEIGHT(16))];
    
    _arrowRight.image = [UIImage imageNamed:@"rightArrow.png"];
    _sectionButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    [_sectionButton addSubview:_arrowRight];
    [self.contentView addSubview:_sectionButton];
    
    _bannerImageView = [[UIImageView alloc]initWithFrame:CGRectZero];
    _bannerImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_bannerImageView];
    
    _transView = [[UIView alloc] init];
    _transView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [self.contentView  addSubview:_transView];
    
    _titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, SCREEN_WIDTH - VARIABLE_WIDTH(30), VARIABLE_HEIGHT(18))];
    _titleLbl.numberOfLines = 1;
    _titleLbl.font = [UIFont boldSystemFontOfSize:(IPAD ? 16.0f:14.0f)];
    _titleLbl.textColor = [UIColor whiteColor];
    [_transView addSubview:_titleLbl];
    
    _subtitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(5, _titleLbl.frame.origin.y +  _titleLbl.frame.size.height , SCREEN_WIDTH - VARIABLE_WIDTH(50), VARIABLE_HEIGHT(32))];
    _subtitleLbl.numberOfLines = 2;
    
    _subtitleLbl.font = [UIFont systemFontOfSize:(IPAD ? 14.0f:12.0f)];
    _subtitleLbl.textColor = [UIColor whiteColor];
    [_transView addSubview:_subtitleLbl];
    
    _PlayIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
    _PlayIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
    
    [_bannerImageView addSubview:_PlayIcon];
    
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
}





@end

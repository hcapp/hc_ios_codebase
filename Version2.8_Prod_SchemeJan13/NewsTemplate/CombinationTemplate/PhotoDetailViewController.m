//
//  PhotoDetailViewController.m
//  HubCiti
//
//  Created by Lakshmi H R on 6/8/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "PhotoDetailViewController.h"
#import "HubCitiConstants.h"
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "HTTPClient.h"
#import "AppDelegate.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "VideoViewController.h"
#import "AnyViewController.h"

@interface PhotoDetailViewController ()
{
   // NSString *lDesc;
  
    UIImage *imgName;
    SdImageView *asyncImageView ;
    UIImageView *playImg;
    UIButton *imageButton;
   // NSArray *newsImageArray,*lDescArray;
    UISwipeGestureRecognizer * swipeleft;
    UISwipeGestureRecognizer * swiperight;
    NSString *htmlWithoutTag;
    NSInteger webviewHeight;
    BOOL webViewload;
}
@end

@implementation PhotoDetailViewController
@synthesize imgNumber,anyVC,emailSendingVC;
@synthesize lDescription,photoArray,catName,catColor,titleColor,newsID;
BOOL shareClikedFlag;
#pragma mark - Service Respone methods
- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didFailWithError:(NSError *)error{
    
    [HubCitiAppDelegate removeActivityIndicator];
    
    [UtilityManager showAlert:@"Network Error" msg:[error localizedDescription]];
}

- (void)HTTPClient:(HTTPClient *)sharedHTTPClient didSucceedWithResponse:(id)responseObject
{
    [HubCitiAppDelegate removeActivityIndicator];
    DLog(@"parameter: %@",responseObject);
    
    [self responseData:responseObject];
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    scrollView.delegate = self;
    scrollView.scrollEnabled = YES;
    scrollView.bounces = NO;
   [self.view addSubview:scrollView];
    imageButton = [[UIButton alloc]init];
    asyncImageView = [[SdImageView alloc]init];
    
    playImg = [[UIImageView alloc]init];
    

    //navigation bar setup
  
   
    self.title =@"Detail";
    
    self.navigationItem.hidesBackButton = YES;
    float yPos = 0.0f;
    // ImageView
          if(IPAD){
            asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(20),yPos+VARIABLE_HEIGHT(10), self.view.bounds.size.width-VARIABLE_WIDTH(40), SCREEN_HEIGHT/2-VARIABLE_HEIGHT(20))];
        }
        else
        {
            asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0,yPos+VARIABLE_HEIGHT(10), SCREEN_WIDTH, SCREEN_HEIGHT/2-VARIABLE_HEIGHT(20))];
            
        }
    webViewload = TRUE;
    longDescWebView = [[UIWebView alloc]init];
    longDescWebView.delegate = self;
    CGRect frame;
    frame.origin.x = VARIABLE_WIDTH(5);
    frame.origin.y = asyncImageView.frame.origin.y + asyncImageView.frame.size.height + VARIABLE_HEIGHT(10) + VARIABLE_HEIGHT(10);
    frame.size.width = SCREEN_WIDTH - 2*VARIABLE_WIDTH(10);
    frame.size.height = webviewHeight;
    longDescWebView.frame = frame;

    
    if(IPAD){
        [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:18px;font-family:Helvetica;color:white;text-align:justify;word-wrap: break-word'>%@",lDescription] baseURL:nil];
    }
    else
    {
        [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:white;text-align:justify;word-wrap: break-word'>%@",lDescription] baseURL:nil];
    }

   [self setupUI];
    scrollView.backgroundColor = [UIColor blackColor];
    swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    swipeleft.delegate = self;
    
    swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
    swiperight.delegate = self;
    
    //home button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
    [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
    NSLog(@"%@ home icon ",[defaults valueForKey:@"homeImgPath"]);
    
    
    [btn addSubview:homeImage];
    
    
    [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
    mainPage.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.rightBarButtonItem = mainPage;

    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
    cusNav.customNavBardelegate = self;
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:NO];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:catColor];

    CustomizedNavController *cus = (CustomizedNavController*)self.navigationController;
    [cus setTitle:catName forView:self withHambergur:NO withColor:titleColor];
    
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    
    // Create down menu button
    UIImageView *homeLabel = [self createHomeButtonView];
    // Create up menu button
    upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - homeLabel.frame.size.width - 10.f,
                                                                      SCREEN_HEIGHT - homeLabel.frame.size.height - 80.f
                                                                      ,
                                                                      homeLabel.frame.size.width,
                                                                      homeLabel.frame.size.height)
                                        expansionDirection:DirectionUp];
    
   
  NSLog(@"%f------------>", SCREEN_HEIGHT - homeLabel.frame.size.height - 80.f);
//   upMenuView = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(10, 420, homeLabel.frame.size.width, homeLabel.frame.size.width) expansionDirection:DirectionUp];
    upMenuView.homeButtonView = homeLabel;
    upMenuView.delegate = self;
    [upMenuView addButtons:[self createDemoButtonArray]];
    

    [self.view addSubview:upMenuView];
    
    upMenuView.onLogOutPressed =^(){
        [self logOutPressed];
    } ;

    
}
-(void)viewWillDisappear:(BOOL)animated
{
     [super viewWillDisappear:YES];
    self.navigationController.navigationBar.barTintColor = [UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]];
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]], NSForegroundColorAttributeName,[UIFont fontWithName:@"Helvetica-Bold" size:17.0], NSFontAttributeName,nil]];

}

-(void) logOutPressed
{
    [defaults setValue:@"1" forKey:KEY_MENULEVEL];
    NSArray *keys = [cashedResponse allKeys];
    for (int i = 0 ; i < [keys count]; i++)
    {
        if (![[keys objectAtIndex:i] isEqualToString:@"0"]) {
            [cashedResponse removeObjectForKey:keys[i]];
        }
        
    }
    
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    // If GuestUser will click on SignUp Button
    if(userIdentifier == TRUE)
        [defaults setBool:YES forKey:@"isMainMenuSignUp"];
    
    [defaults setObject:[defaults valueForKey:KEY_USERID] forKey:@"currentuserId"];
    
    [defaults setBool:NO forKey:@"LoginSuccess"];
    
    if([defaults boolForKey:@"newsTemplateExist"])
    {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        AppDelegate *app =HubCitiAppDelegate;
        viewController.login_DO = app.loginDO;
        
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    [UIView animateWithDuration:1.5 delay:1.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
     if(imgNumber < photoArray.count-1)
     {
         
         [asyncImageView loadImage:[photoArray objectAtIndex:imgNumber+1]];
         [imageButton addSubview:asyncImageView];
         imageButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
     
         if(lDescription.length != 0)
         {
              htmlWithoutTag = [self stringByStrippingHTML : lDescription];
             CGFloat height = [self calculateLabelHeight: htmlWithoutTag labelWidth:SCREEN_WIDTH-2*VARIABLE_HEIGHT(15) fontSize:14];
             
             
             longDescWebView.frame = CGRectMake(VARIABLE_WIDTH(5), asyncImageView.frame.origin.y + asyncImageView.frame.size.height + VARIABLE_HEIGHT(10), SCREEN_WIDTH - 2*VARIABLE_WIDTH(10),height);
             //longDescWebView.backgroundColor = [UIColor blackColor];
             
             [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:white;text-align:justify;word-wrap: break-word'>%@",lDescription] baseURL:nil];

         }
        
         
         
     }
     else
     {
         [asyncImageView loadImage:[photoArray objectAtIndex:0]];
         [imageButton addSubview:asyncImageView];
         if(lDescription.length != 0)
         {
             htmlWithoutTag = [self stringByStrippingHTML : lDescription];
             CGFloat height = [self calculateLabelHeight: htmlWithoutTag labelWidth:SCREEN_WIDTH-2*VARIABLE_HEIGHT(15) fontSize:14];
             
             
             longDescWebView.frame = CGRectMake(VARIABLE_WIDTH(5), asyncImageView.frame.origin.y + asyncImageView.frame.size.height + VARIABLE_HEIGHT(10), SCREEN_WIDTH - 2*VARIABLE_WIDTH(10),height);
             //longDescWebView.backgroundColor = [UIColor blackColor];
             
             [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:white;text-align:justify;word-wrap: break-word'>%@",lDescription] baseURL:nil];
         }
        
     }
    }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
    
        imgNumber = imgNumber + 1;
    if(imgNumber > photoArray.count)
    {
        imgNumber = 0;
    }
//    if(imgNumber == newsImage.count)
//    {
//        
//        swipeleft.enabled = NO;
//    }
}
-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        
        //[NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
        
    }
    else
    {
        
        
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if(imgNumber > 0)
    {
        [asyncImageView loadImage:[photoArray objectAtIndex:imgNumber-1]];
        [imageButton addSubview:asyncImageView];
        if(lDescription.length != 0)
        {
            htmlWithoutTag = [self stringByStrippingHTML : lDescription];
            
            CGFloat height = [self calculateLabelHeight: htmlWithoutTag labelWidth:SCREEN_WIDTH-2*VARIABLE_HEIGHT(15) fontSize:14];
            
            
            longDescWebView.frame = CGRectMake(VARIABLE_WIDTH(5), asyncImageView.frame.origin.y + asyncImageView.frame.size.height + VARIABLE_HEIGHT(10), SCREEN_WIDTH - 2*VARIABLE_WIDTH(10),height);
            //longDescWebView.backgroundColor = [UIColor blackColor];
            
            [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:white;text-align:justify;word-wrap: break-word'>%@",lDescription] baseURL:nil];
        }

    }
    else
    {
        [asyncImageView loadImage:[photoArray objectAtIndex:photoArray.count - 1]];
        [imageButton addSubview:asyncImageView];
        if(lDescription.length != 0)
        {
            htmlWithoutTag = [self stringByStrippingHTML : lDescription];
            CGFloat height = [self calculateLabelHeight: htmlWithoutTag labelWidth:SCREEN_WIDTH-2*VARIABLE_HEIGHT(15) fontSize:14];
            
            
            longDescWebView.frame = CGRectMake(VARIABLE_WIDTH(5), asyncImageView.frame.origin.y + asyncImageView.frame.size.height + VARIABLE_HEIGHT(10), SCREEN_WIDTH - 2*VARIABLE_WIDTH(10),height);
            //longDescWebView.backgroundColor = [UIColor blackColor];
            
            [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:white;text-align:justify;word-wrap: break-word'>%@",lDescription] baseURL:nil];
            

        }
    }
    imgNumber = imgNumber - 1;

//    if(imgNumber == newsImage.count)
//    {
//        swiperight.enabled = NO;
//    }
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    
    return YES;
}
-(void)popBackToPreviousPage
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void) responseData: (id) responseObject{
    

}

-(void) setupUI {
    htmlWithoutTag = [[NSString alloc]init];
    
    float yPos = 0.0f;
     // ImageView
   
    if(photoArray[imgNumber])
    {
        
        BOOL doesContain = [imageButton.subviews containsObject:asyncImageView];
        if(!doesContain){
        
        if(IPAD){
           asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(VARIABLE_WIDTH(20),yPos+VARIABLE_HEIGHT(10), self.view.bounds.size.width-VARIABLE_WIDTH(40), SCREEN_HEIGHT/2-VARIABLE_HEIGHT(20))];
        }
        else
        {
             asyncImageView = [[SdImageView alloc] initWithFrame:CGRectMake(0,yPos+VARIABLE_HEIGHT(10), SCREEN_WIDTH, SCREEN_HEIGHT/2-VARIABLE_HEIGHT(20))];
           
        }
        asyncImageView.clipsToBounds = YES;
        asyncImageView.backgroundColor = [UIColor blueColor];
        [asyncImageView loadImage:[photoArray objectAtIndex:imgNumber]];
        imageButton.frame = asyncImageView.frame; 
        [imageButton addSubview:asyncImageView];
        
        if( NSOrderedSame==[catName caseInsensitiveCompare:@"Videos"])
        {
            {
                NSString *urlString = [photoArray objectAtIndex:imgNumber];
                //NSURL *sourceURL = [NSURL URLWithString:@"http://brightcove.vo.llnwd.net/v1/uds/pd/2322069287001/201606/2683/2322069287001_4932251272001_4932247648001.mp4"];
                NSURL *sourceURL = [NSURL URLWithString:urlString];
                AVAsset *asset = [AVAsset assetWithURL:sourceURL];
                AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
                CMTime time = CMTimeMake(3, 2);
                CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
                UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
                CGImageRelease(imageRef);
                asyncImageView.image = thumbnail;
                playImg.frame = CGRectMake((asyncImageView.frame.size.width-VARIABLE_WIDTH(30))/2, (asyncImageView.frame.size.height-VARIABLE_WIDTH(30))/2, VARIABLE_WIDTH(30), VARIABLE_WIDTH(30));
                playImg.image = [UIImage imageNamed:@"play-icon-stroke.png"];
                [asyncImageView addSubview:playImg];

                
//                if ([[UIDevice currentDevice].systemVersion intValue] >= 8) {
//
//
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
//                        AVAsset *asset = [AVAsset assetWithURL:sourceURL];
//                        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
//                        CMTime time = CMTimeMake(1, 1);
//                        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
//                        
//                        
//                        dispatch_sync(dispatch_get_main_queue(), ^(void) {
//    
//                            
//                            
//                            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
//                            asyncImageView.image = thumbnail;
//                            playImg.frame = CGRectMake((asyncImageView.frame.size.width-VARIABLE_WIDTH(30))/2, (asyncImageView.frame.size.height-VARIABLE_WIDTH(30))/2, VARIABLE_WIDTH(30), VARIABLE_WIDTH(30));
//                            playImg.image = [UIImage imageNamed:@"play-icon-stroke.png"];
//                            [asyncImageView addSubview:playImg];
//                        });
//                    });
//                    
//                }
//                
                //cell.cellImage.image = [UIImage imageNamed:@"image1.jpg"];
                
                
                //[cell.imgSubView removeFromSuperview];
                //
                //    cell.photoCount.text = @"6";
                //
                //    cell.photoImg.image = [UIImage imageNamed:@"camera.png"];
                //
            }
            
            [imageButton addTarget:self action:@selector(imageClick) forControlEvents:UIControlEventTouchUpInside];
        }
        [scrollView addSubview:imageButton];
    
        yPos = yPos+imageButton.frame.size.height+15;
        }
    }
    
    if(lDescription.length != 0){
        htmlWithoutTag = [self stringByStrippingHTML : lDescription];
       // CGFloat height = [self calculateLabelHeight: htmlWithoutTag labelWidth:SCREEN_WIDTH-2*VARIABLE_HEIGHT(15) fontSize:14];
        
        
        longDescWebView = [[UIWebView alloc]init];
        longDescWebView.delegate = self;
        longDescWebView.opaque = NO;
        
        //longDescWebView.scrollView.showsVerticalScrollIndicator = YES;
        longDescWebView.scrollView.bounces = NO;
        longDescWebView.layer.cornerRadius = 5.0;
        longDescWebView.backgroundColor = [UIColor clearColor];
        longDescWebView.frame = CGRectMake(VARIABLE_WIDTH(5), asyncImageView.frame.origin.y + asyncImageView.frame.size.height + VARIABLE_HEIGHT(10), SCREEN_WIDTH - 2*VARIABLE_WIDTH(10),webviewHeight);
        longDescWebView.backgroundColor = [UIColor clearColor];
        if(IPAD){
            [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:18px;font-family:Helvetica;color:white;text-align:justify;word-wrap: break-word'>%@",lDescription] baseURL:nil];
        }
        else
        {
            [longDescWebView loadHTMLString:[NSString stringWithFormat:@"<div style='font-size:13px;font-family:Helvetica;color:white;text-align:justify;word-wrap: break-word'>%@",lDescription] baseURL:nil];
        }
        
        [scrollView addSubview:longDescWebView];
        
        yPos = yPos+ longDescWebView.frame.size.height+5;

    }
//    [scrollView setContentSize:CGSizeMake(SCREEN_WIDTH, yPos + VARIABLE_HEIGHT(50))];
//    scrollView.contentInset = UIEdgeInsetsMake(0, 0, VARIABLE_HEIGHT(10), 0);
    [scrollView setContentSize:CGSizeMake(SCREEN_WIDTH, yPos+80)];
    scrollView.contentInset = UIEdgeInsetsMake(0, 0, VARIABLE_HEIGHT(20), 0);
    
    
    
    
}
-(void)imageClick
{
    VideoViewController  *evt = [[VideoViewController alloc]initWithNibName:@"VideoViewController" bundle:nil];
    
    evt.videoUrl = photoArray[imgNumber];
    evt.catColor = catColor;
    evt.titleColor = titleColor;
    [self.navigationController pushViewController:evt animated:YES];

    
}
-(float) getLabelSize : (NSString*) labelString
{
    if ([labelString isEqualToString:@" "])
    {
        return 0;
    }
    float descFontSize;
    
    if(DEVICE_TYPE==UIUserInterfaceIdiomPad){
        descFontSize=15;
    }
    else
    {
        descFontSize=12;
    }
    CGSize constraint = CGSizeMake(SCREEN_WIDTH, CGFLOAT_MAX);
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    CGSize size = [labelString boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:descFontSize], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    return size.height +20;
    
}

- (NSString *)stringByStrippingHTML : (NSString*) htmlStr
{
    NSRange r;
    NSString *s = htmlStr ;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound) s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

-(int)calculateLabelHeight:(NSString*)text labelWidth:(int)labelWidth fontSize:(int)fontSizeText
{
    CGSize constraint;
    CGSize size ;
    CGFloat heightWebview ;
    constraint = CGSizeMake(labelWidth, 20000.0f);
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    size = [text boundingRectWithSize: constraint options:NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:fontSizeText], NSParagraphStyleAttributeName: paragraph } context: nil].size;
    
    heightWebview = MAX(size.height+20, 40.0f);
    return heightWebview;
}


-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ([[[inRequest URL] scheme] isEqual:@"tel"]) {
        NSArray *rawURLparts = [[[inRequest URL] resourceSpecifier] componentsSeparatedByString:@"?"];
        
        NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
        NSString *phoneString  = [[defaultRecipient componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        NSString *phoneNumber = [NSString stringWithFormat:@"tel:%@", phoneString];
        NSURL    *url= [NSURL URLWithString:phoneNumber];
        
        UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        webview.alpha = 0.0;
        
        [webview loadRequest:[NSURLRequest requestWithURL:url]];
        
        // Assume we are in a view controller and have access to self.view
        [self.view insertSubview:webview belowSubview:longDescWebView]; //ask ashika
        
        return NO;
    }
    
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //    Math.max( document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight );
    NSString *output = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"];
    NSLog(@"height: %@", output);
    webviewHeight = [output integerValue];
    if(webViewload){
        webViewload = FALSE;
        [self setupUI];
    }
    else{
        
        if (webView.isLoading)
            return;
    }
    
}

#pragma mark floating share button

- (UIImageView *)createHomeButtonView
{
    UIImageView *label = nil;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    }
    else{
        label = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 70.f, 70.f)];
    }
    
    label.layer.cornerRadius = label.frame.size.height / 2.f;
   label.backgroundColor =[UIColor colorWithRed:0.f green:0.f blue:0.0f alpha:0.5f];
   
    label.image = [UIImage imageNamed:@"share-icon"];
    label.clipsToBounds = YES;
    
    return label;
}

- (NSArray *)createDemoButtonArray
{
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    int i = 0;
    for (NSString *title in @[@"facebook-icon", @"twitter-icon", @"chat-icon", @"email-icon"]) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:title] forState:UIControlStateNormal];
        
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            button.frame = CGRectMake(0.f, 0.f, 40.f, 40.f);
        }
        else{
            button.frame = CGRectMake(0.f, 0.f, 70.f, 70.f);
        }
        button.clipsToBounds = YES;
        button.tag = i++;
        [button addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        [buttonsMutable addObject:button];
    }
    
    return [buttonsMutable copy];
}

- (void)test:(UIButton *)sender {
    
    NSLog(@"Button tapped, tag: %ld", (long)sender.tag);
    shareClikedFlag=TRUE;
    [self shareClicked];
    
    if (shareClikedFlag==TRUE) {
        
        
        switch (sender.tag) {
            case 0:{//facebook
                anyVC = [[AnyViewController alloc] initWithNibName:nil bundle:nil];
                
                
                /////////change type
                
                anyVC.fbShareType=nil;
                [UIView transitionWithView:self.view duration:0.5
                                   options:UIViewAnimationOptionTransitionCurlDown
                                animations:^ { [self.view addSubview:anyVC.view]; }
                                completion:nil];
            }
                break;
            case 1:{//twitter
                if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                    
                    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                    
                    SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                        if (result == SLComposeViewControllerResultCancelled) {
                            DLog(@"Twitter Result: canceled");
                        } else {
                            DLog(@"Twitter Result: sent");
                            //                            NSMutableString *userXmlStr=[[NSMutableString alloc]initWithFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Twitter</shrTypNam><fundId>%@</fundId></UserTrackingData>",[defaults valueForKey:KEY_MAINMENUID],[defaults valueForKey:FUNDRAISERID]];
                            //
                            //                            [NSThread detachNewThreadSelector:@selector(userTracking:) toTarget:self withObject:userXmlStr];
                        }
                        
                        if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                            
                            [controller.view removeFromSuperview];
                        } else
                            [controller dismissViewControllerAnimated:YES completion:Nil];
                    };
                    controller.completionHandler =myBlock;
                    
                    //Adding the Text to the facebook post value from iOS
                    [controller setInitialText:[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:@"twitterNewsshare"],[defaults valueForKey:KEY_HYPERLINK]]];
                    
                    //Adding the URL to the facebook post value from iOS
                    
                    //                [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
                    
                    //Adding the Image to the facebook post value from iOS
                    
                    [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[defaults valueForKey:KEY_PRODUCTIMGPATH]]]]];
                    
                    if (DEVICE_TYPE == UIUserInterfaceIdiomPad) {
                        [self.view addSubview:controller.view];
                        [self.view bringSubviewToFront:controller.view];
                    }else
                        [self presentViewController:controller animated:YES completion:Nil];
                    
                    
                }
                else {
                    [UtilityManager showAlert:NSLocalizedString(@"Cannot Share",@"Cannot Share") msg:NSLocalizedString(@"Please Go To Settings and Set Up A Twitter Account",@"Please Go To Settings and Set Up A Twitter Account")];
                }
            }
                
                
                break;
            case 2:{//text
                
                if ([self hasCellularCoverage])
                {
                    if ([MFMessageComposeViewController canSendText]) {
                        
                        MFMessageComposeViewController *smsComposerController = [[MFMessageComposeViewController alloc] init] ;
                        [smsComposerController setBody:[NSString stringWithFormat:@"%@\n%@",[defaults valueForKey:KEY_SHAREMSG],[defaults valueForKey:KEY_HYPERLINK]]];
                        smsComposerController.messageComposeDelegate = self;
                        [self presentViewController:smsComposerController animated:YES completion:nil];
                    }
                }
                else
                {
                    [UtilityManager showAlert:NSLocalizedString(@"Oops, SMS not supported..",@"Oops, SMS not supported..") msg:NSLocalizedString(@"Looks like the sim card is missing or the device does not support texting",@"Looks like the sim card is missing or the device does not support texting")];  // //[alert release];
                }
                
            }
                break;
            case 3:{//email
                //[defaults setValue:FUNDRAISER_SHARE forKey:KEY_SHARE_TYPE];
                emailSendingVC = [[EmailShareViewController alloc]initWithNibName:@"EmailShareViewController" bundle:[NSBundle mainBundle]];
                emailSendingVC.shareType=[[NSMutableString alloc]initWithString:@"NEWS_SHARE"];
                ///change this
                
                
                
                
                [defaults setValue:newsID forKey:@"newsID"];
                __typeof(self) __weak  obj = self;
                emailSendingVC.updateBlock=^(MFMailComposeViewController* controllerNeededForPresenting){
                    
                    [obj presentViewController:controllerNeededForPresenting animated:NO completion:nil];
                } ;
                
                [emailSendingVC loadMail];
            }
                break;
                
            default:
                shareClikedFlag=FALSE;
                [defaults setBool:NO forKey:BottomButton];
                break;
        }
    }
    else
    {
        switch (sender.tag)
        {
            default:
                break;
        }
        
    }
    
}
-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    if (!carrier.isoCountryCode) {
        //        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    if (shareClikedFlag==TRUE) {
        switch (result) {
            case MessageComposeResultCancelled:
                
                break;
                
            case MessageComposeResultSent:{
                [UtilityManager showAlert:nil msg:@"Message Sent"];
                
                
            }
                break;
            case MessageComposeResultFailed:{
                [UtilityManager showAlert:@"Oops message not sent" msg:@"Please try later"];
            }
                break;
                
            default:
                break;
        }
        
        [defaults setValue:@"YES" forKey:@"isComingFromSubMenuGroupingAndSorting"];//setting this as it will call "viewWillAppear" after this
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        
        if (result == MessageComposeResultSent) {
            
            //For User Tracking
            //            NSMutableString *reqStr = [[NSMutableString alloc] init];
            //            [reqStr appendFormat:@"<UserTrackingData><mainMenuId>%@</mainMenuId><shrTypNam>Text</shrTypNam>",[defaults valueForKey:KEY_MAINMENUID]];
            //
            //            [reqStr appendFormat:@"<coupId>%@</coupId></UserTrackingData>", couponId];
            //
            //
            //            NSString *urlString = [NSString stringWithFormat:@"%@ratereview/updatesharetype",BASE_URL];
            //            DLog(@"%@",urlString);
            //            NSString *response = [[NSString alloc] initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
            //            DLog(@"%@",response);
            //            ReleaseAndNilify(response);
            
        }
    }
    
}

- (void)bubbleMenuButtonWillExpand:(DWBubbleMenuButton *)expandableView{
    
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =NO;
            _currentView.alpha =  0.2;
            
        }
    }
    
}


- (void)bubbleMenuButtonWillCollapse:(DWBubbleMenuButton *)expandableView
{
    for (UIView *_currentView in self.view.subviews) {
        if (![_currentView isKindOfClass:[DWBubbleMenuButton class]]) {
            _currentView.userInteractionEnabled =YES;
            _currentView.alpha =  1;
            
        }
        
    }
}

#pragma mark share coupon request response

-(void)shareClicked
{
    NSMutableString *reqStr=[[NSMutableString alloc]init];
    [reqStr appendFormat:@"<ShareProductInfo><userId>%@</userId>",[defaults valueForKey:KEY_USERID]];
    [reqStr appendFormat:@"<newsId>%@</newsId>",newsID];
    [reqStr appendFormat:@"<hubCitiId>%@</hubCitiId></ShareProductInfo>",[defaults valueForKey:KEY_HUBCITIID]];
    NSLog(@"request : %@",reqStr);
    
    NSString *urlString = [NSString stringWithFormat:@"%@ratereview/sharenewsbymedia",BASE_URL];
  
    NSLog(@"URl : %@",urlString);
    NSString *response = [[NSString alloc]initWithString:[ConnectionManager establishPostConnection:urlString withParam:reqStr]];
    NSLog(@"response %@",response);
    [self parse_shareCoupons:response];
    
}

-(void)parse_shareCoupons:(NSString*)response
{
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    TBXMLElement *responseCode = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    if (responseCode!=nil && [[TBXML textForElement:responseCode]intValue] == 10000){
        
        
        TBXMLElement *shareText=[TBXML childElementNamed:@"title" parentElement:tbXml.rootXMLElement];
        TBXMLElement *qrURL = [TBXML childElementNamed:@"qrUrl" parentElement:tbXml.rootXMLElement];
        TBXMLElement *newsImagePath = [TBXML childElementNamed:@"image" parentElement:tbXml.rootXMLElement];
        TBXMLElement *shreMsg=[TBXML childElementNamed:@"shareText" parentElement:tbXml.rootXMLElement];
        if (qrURL!=nil) {
            [defaults setValue:[TBXML textForElement:qrURL] forKey:KEY_HYPERLINK];
            NSLog(@"The link to be shared is %@",KEY_HYPERLINK);
        }
        
        if (shareText!=nil) {
            NSMutableString *msgString = [[NSMutableString alloc]init];
            [msgString appendFormat:@"%@",[TBXML textForElement:shreMsg]];
            NSString *shareString = [[NSString alloc]initWithString:[TBXML textForElement:shareText]];
            [msgString appendFormat:@"%@",shareString];

           
            [defaults setObject:msgString forKey:KEY_SHAREMSG];
            [defaults setObject:shareString forKey:@"twitterNewsshare"];
            NSLog(@"The share message value is %@",KEY_SHAREMSG);
        }
        
        if (newsImagePath!=nil) {
            [defaults setObject:[TBXML textForElement:newsImagePath] forKey:KEY_PRODUCTIMGPATH];
            NSLog(@"The product image path is %@",KEY_PRODUCTIMGPATH);
        }
    }
    else {
        TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
        if (responseCodeElement!=nil) {
            
            TBXMLElement *responseTextElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
            [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
            return;
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

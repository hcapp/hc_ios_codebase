//
//  VerticalTableViewCell.h
//  AFTabledCollectionView
//
//  Created by Apple on 5/19/16.
//  Copyright © 2016 Ash Furrow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HubCitiConstants.h"
#import "SdImageView.h"


@protocol MainTableViewCellDelegate;

@interface VerticalTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SubTitle_Y;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *Title_Y;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subTitleHeight;


@property(nonatomic,weak) IBOutlet UIImageView* playIcon;
@property(nonatomic,weak) IBOutlet UILabel* title,*subTitle;
@property(nonatomic,weak) IBOutlet SdImageView* icon;

@end

@interface MainTableViewCell : UITableViewCell
@property(nonatomic,assign) NSInteger selectedIndex;
@property(nonatomic,strong) NSMutableDictionary* imageDictionary;
@property(nonatomic,strong) UITableView *mainTableView;
@property(nonatomic,strong) UIButton * moreButton;
@property (nonatomic, weak) id<MainTableViewCellDelegate>delegate;
@property(nonatomic,assign) CGFloat imageHeight;



@end

#pragma mark - Protocol Methods

@protocol MainTableViewCellDelegate <NSObject>

@optional

// RETDocumentsTableViewController
- (void) didSelectRows:(NSIndexPath*)indexPath : (int) index;


@end

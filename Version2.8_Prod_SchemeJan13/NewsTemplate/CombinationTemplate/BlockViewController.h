//
//  BlockViewController.h
//  HubCiti
//
//  Created by Lakshmi H R on 6/2/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTTPClient.h"

@interface BlockViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,HTTPClientDelegate>
{
    UIAlertAction* zipSave;
}

@property(strong,nonatomic) NSNumber * totalNumOfCells;
@property (strong, nonatomic) IBOutlet UICollectionView *collView;
@property(nonatomic) BOOL refreshOrButtonAction;
@property(nonatomic) BOOL refreshMade;
@property(nonatomic,strong) MainMenuResponse  *menuResponse;
@property(strong,nonatomic) NSNumber * nextPage;
@property(strong,nonatomic) NSString * responseText;
@property(nonatomic,strong) NSMutableArray * array_BlockNewsData;
@property(strong,nonatomic) NSMutableArray * receivedResponseText;
@property(nonatomic,assign) NSUInteger  index;
@property (nonatomic,strong) UIRefreshControl * pullToRefresh;
@property(nonatomic,strong) UIView *footer;
@property(nonatomic,strong)  NSMutableArray * buttonArray;
@property(nonatomic) BOOL bannerChanged;

- (IBAction)refreshClicked:(id)sender;

@end

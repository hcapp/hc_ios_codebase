//
//  PhotoDetailViewController.h
//  HubCiti
//
//  Created by Lakshmi H R on 6/8/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "DWBubbleMenuButton.h"
#import "EmailShareViewController.h"
@interface PhotoDetailViewController : UIViewController<UIGestureRecognizerDelegate,UIScrollViewDelegate,UIWebViewDelegate,CustomizedNavControllerDelegate,DWBubbleMenuViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    UIWebView *longDescWebView;
    UIScrollView *scrollView;
DWBubbleMenuButton *upMenuView;
}
@property(nonatomic,strong) NSString* catColor;
@property(nonatomic,strong)NSString *titleColor;

@property (nonatomic) NSInteger imgNumber;
@property(strong,nonatomic) NSNumber *newsID;
@property(nonatomic,strong) NSString *catName;
@property (nonatomic,strong) NSMutableArray *photoArray;
@property (nonatomic,strong) NSString *lDescription;
@property(nonatomic,strong) EmailShareViewController *emailSendingVC;
@property(nonatomic,strong) AnyViewController * anyVC;
@end

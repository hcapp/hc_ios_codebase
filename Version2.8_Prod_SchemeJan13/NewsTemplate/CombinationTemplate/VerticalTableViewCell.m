//
//  VerticalTableViewCell.m
//  AFTabledCollectionView
//
//  Created by Apple on 5/19/16.
//  Copyright © 2016 Ash Furrow. All rights reserved.
//

#import "VerticalTableViewCell.h"
#import "HubCitiConstants.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ItemList.h"

@implementation VerticalTableViewCell

- (void) awakeFromNib {
    [super awakeFromNib];
}

@end


@interface MainTableViewCell () <UITableViewDelegate,UITableViewDataSource>
@end

@implementation MainTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _imageDictionary = [[NSMutableDictionary alloc] init];
        _mainTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        
        _mainTableView.delegate = self;
        _mainTableView.dataSource = self;
        
        UINib *nib = [UINib nibWithNibName:@"VerticalTableViewCell" bundle: nil];
        [_mainTableView registerNib:nib forCellReuseIdentifier:@"MyCell"];
        
        [self.contentView addSubview:_mainTableView];
        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _moreButton.frame = CGRectMake(0, 0, SCREEN_WIDTH,  VARIABLE_HEIGHT(60));
        [_moreButton setTitle:@"View More" forState : UIControlStateNormal];
        [_moreButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        _mainTableView.tableFooterView = _moreButton;
        
    }
    
    return self;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"index = %ld", (long)self.tag);
    [self.delegate didSelectRows:indexPath : (int) self.tag];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray* itemArr =  [defaults valueForKey:@"List"];
    NSArray* itemList = [itemArr[self.selectedIndex] objectForKey:@"itemList"];

    
    return itemList.count;
}

- (NSString *)stringByStrippingHTML : (NSString*) htmlStr
{
    
    NSRange r;
    NSString *s = htmlStr ;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound) s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
    
}



-(NSArray *)convertToArrayFromCommaSeparated:(NSString*)string{
    return [string componentsSeparatedByString:@","];
}

- (CGFloat)labelSizeForText:(NSString *)text forFont:(UIFont *)font {
    
   
    CGRect labelRect = [text
                        boundingRectWithSize:CGSizeMake((SCREEN_WIDTH - 15 - 106), MARQEE_HEIGHT)
                        options:NSStringDrawingUsesLineFragmentOrigin
                        attributes:@{
                                     NSFontAttributeName :font
                                     }
                        context:nil];
    
    return labelRect.size.height;
    
}

-(NSString *)convertHTML:(NSString *)html {
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    VerticalTableViewCell *cell = (VerticalTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"MyCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(cell == nil)
    {
        cell = [[VerticalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyCell"];
    }
   
    NSArray* itemArr =  [defaults valueForKey:@"List"];
    NSArray* itemList = [itemArr[self.selectedIndex] objectForKey:@"itemList"];

    
    
    
    // @"http://hdwpro.com/wp-content/uploads/2016/06/Sunrise-Amazing-Wallpaper.jpeg";
    
   // NSString* imageLink = [[[itemList objectAtIndex:indexPath.row] objectForKey:@"image"] componentsSeparatedByString:@","][0];
    
    NSString* autor = [[itemList objectAtIndex:indexPath.row] objectForKey:@"author"];
    NSString* title = [[itemList objectAtIndex:indexPath.row] objectForKey:@"title"];
    NSString* subTitle = [[itemList objectAtIndex:indexPath.row] objectForKey:@"sDesc"];
    NSString* catName = [[[itemList objectAtIndex:indexPath.row] objectForKey:@"catName"] uppercaseString];
    
    
    NSString* url;
    
    if ([catName isEqualToString:@"VIDEOS"])
    {
        cell.icon.backgroundColor = [UIColor blackColor];
        cell.icon.contentMode = UIViewContentModeScaleToFill;
        
        cell.playIcon.hidden = false;
        url = [[[itemList objectAtIndex:indexPath.row] objectForKey:@"videoLink"] componentsSeparatedByString:@","][0];
        if ([_imageDictionary objectForKey:url]) {
            
            cell.icon.image = [_imageDictionary objectForKey:url];
            cell.playIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
        }
        else
        {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                
                
                AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:url]];
                
                //  Get thumbnail at the very start of the video
                CMTime time = CMTimeMake(3, 2); // CMTimeMake(time, startframe)
                
                
                //  Get image from the video at the given time
                AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                imageGenerator.appliesPreferredTrackTransform = YES;
                
                CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
                UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
                CGImageRelease(imageRef);
                
                if(thumbnail){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        cell.icon.image = thumbnail;
                        cell.playIcon.image = [UIImage imageNamed:@"play-icon-stroke.png"];
                        [_imageDictionary setObject:thumbnail forKey:url];
                        
                    });
                    
                }
            });
        }
        
        
    }
    else{
        cell.playIcon.hidden = true;
        url = [[[itemList objectAtIndex:indexPath.row] objectForKey:@"image"] componentsSeparatedByString:@","][0];
        cell.icon.contentMode = UIViewContentModeScaleAspectFit;
        [cell.icon loadImage:url];
    }
    
    
    
    if (title.length > 0 && autor.length > 0) {
        
        cell.title.text = autor;
        cell.title.font = [UIFont boldSystemFontOfSize:10];
        cell.title.textColor = [UIColor lightGrayColor];
        cell.title.numberOfLines = 1;
        
        cell.subTitle.text = title;
        cell.subTitle.font = [UIFont boldSystemFontOfSize:12];
        cell.subTitle.textColor = [UIColor blackColor];
        cell.subTitle.numberOfLines = 2;
        
        CGFloat height = [self labelSizeForText:title forFont:[UIFont boldSystemFontOfSize:12]];
        int no_lines = height / 12 ;
        
        if (no_lines > 1) {
            cell.titleHeight.constant = 16;
            cell.subTitleHeight.constant = 32;
            cell.SubTitle_Y.constant = 15;
        }
        else{
            cell.SubTitle_Y.constant = 30;
            
        }
        
    }
    if (title.length > 0 && autor.length == 0) {
        cell.title.text = title;
        cell.title.font = [UIFont boldSystemFontOfSize:12];
        cell.subTitle.hidden = false;
        cell.title.numberOfLines = 2;
        cell.title.textColor = [UIColor blackColor];
        
        CGFloat height = [self labelSizeForText:title forFont:[UIFont boldSystemFontOfSize:12]];
        int no_lines = height / 12 ;
        if (no_lines == 1) {
           
            cell.titleHeight.constant = 20;
            cell.SubTitle_Y.constant = 30;
           
        }
        else{
           
            cell.titleHeight.constant = 32;
            cell.SubTitle_Y.constant = 15;
        }
        
        cell.subTitle.text = [self convertHTML:subTitle];//[self stringByStrippingHTML : subTitle];
        
        
        cell.subTitle.font = [UIFont systemFontOfSize:12];
        cell.subTitle.textColor = [UIColor blackColor];
        cell.subTitle.numberOfLines = 1;
    }
       
    return cell;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return  IPAD ?  104 : 84; 
}

@end

//
//  CombinationViewController
//
//  Created by Ravindra on 2016-05-16.
//  Copyright (c) 2016 Ravindra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CombinationObj.h"
#import "NewCombinationTableViewCell.h"

@interface CombinationViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate,CustomizedNavControllerDelegate>
{
    UIAlertAction* zipSave;
}
@property(nonatomic,strong) CombinationObj* combinationObj,*allcombinationObjResp;
@property(nonatomic,strong) UITableView* combTableView;
@property(nonatomic,strong)  NSMutableArray * buttonArray;
@end

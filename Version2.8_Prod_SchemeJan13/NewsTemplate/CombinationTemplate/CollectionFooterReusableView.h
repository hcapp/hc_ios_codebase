//
//  CollectionFooterReusableView.h
//  HubCiti
//
//  Created by service on 7/12/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionFooterReusableView : UICollectionReusableView
@property(nonatomic,weak) IBOutlet UIButton* viewMoreButton;
@end

//
//  NewCombinationTableViewCell.m
//  HubCiti
//
//  Created by Apple on 6/30/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import "NewCombinationTableViewCell.h"
#import "CollectionFooterReusableView.h"
@implementation CombinationIndexedCollectionView

@end

@implementation NewCombinationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
   
    
    UINib *nib = [UINib nibWithNibName:@"CombinationCollectionCell" bundle: nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:CollectionViewCellIdentifier];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionFooterReusableView" bundle:nil]
          forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                 withReuseIdentifier:@"FooterView"];
    
   /* [self.collectionView registerClass:[UICollectionReusableView class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                   withReuseIdentifier:@"FooterView"];*/
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.showsHorizontalScrollIndicator = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath
{
    self.collectionView.dataSource = dataSourceDelegate;
    self.collectionView.delegate = dataSourceDelegate;
    self.collectionView.indexPath = indexPath;
    [self.collectionView setContentOffset:self.collectionView.contentOffset animated:NO];
    
    [self.collectionView reloadData];
}


@end

@implementation CombinationCollectionCell
- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.contentView.translatesAutoresizingMaskIntoConstraints = YES;
}

@end


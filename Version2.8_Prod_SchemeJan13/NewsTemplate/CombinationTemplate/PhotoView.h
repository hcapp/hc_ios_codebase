//
//  PhotoView.h
//  pageSwipe
//
//  Created by Lakshmi H R on 5/18/16.
//  Copyright © 2016 Nikitha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScrollTemplateResponse.h"
#import "PhotoFolderViewController.h"


@interface PhotoView : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,HTTPClientDelegate,CustomizedNavControllerDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *collView;
@property (strong,nonatomic) PhotoFolderViewController *phview;
@property (strong,nonatomic) NSString *categoryName;
@property(nonatomic,strong) NSString* catColor;
@property(nonatomic,strong)NSString *titleColor;
@property(strong,nonatomic) NSNumber * totalNumOfCells;
@property(nonatomic) BOOL refreshMade;
@property(strong,nonatomic) NSString * responseText;
@property(nonatomic) BOOL refreshOrButtonAction;
@property(strong,nonatomic) NSNumber * nextPage;
@property(nonatomic,strong) NSMutableArray * array_ScrollNewsData;
@property (nonatomic,strong) UIRefreshControl * pullToRefresh;
@property (nonatomic,strong) ScrollTemplateResponse * obj_ScrollTempResponse;
@property (nonatomic,strong)NSMutableArray *lDescArray;
@property (nonatomic,strong) NSMutableArray*toSendArray;
@property(nonatomic,assign) BOOL isComingFromScroll;
@property(nonatomic,assign) NSUInteger index;
@property(nonatomic,strong) NSMutableArray * receivedResponseText;

@property(nonatomic,strong) UIView *footer;
@end

//
//  TileViewCell.h
//  HubCiti
//
//  Created by Lakshmi H R on 6/24/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TileViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet SdImageView *blockImg;
@property (strong, nonatomic) IBOutlet UILabel *blockLabel;
@end

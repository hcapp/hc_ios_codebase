//
//  VideoViewController.h
//  HubCiti
//
//  Created by Lakshmi H R on 6/6/16.
//  Copyright © 2016 Keshavakarthik S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface VideoViewController : UIViewController<CustomizedNavControllerDelegate>

@property (nonatomic, strong) AVPlayerViewController *playerViewController;


@property (nonatomic,strong)NSString *videoUrl;
@property(nonatomic,strong) NSString* catColor;
@property(nonatomic,strong)NSString *titleColor;
//@property (nonatomic,strong)NSMutableArray *urlArray;
//- (IBAction)playMovie:(id)sender;
-(void)playMoviesUsingAVPlayer;


@end

//
//  CityPreferenceViewController.h
//  HubCiti
//
//  Created by Keshava on 9/8/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityPreferenceViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *cityPrefTable;
    UIBarButtonItem *customItem;
    IBOutlet UIToolbar *toolbar;
    UIImageView *checkButton;
    
    NSMutableArray *parsedCities;
    NSMutableArray *allCityIDS;
    int  selectedCities;
    NSMutableArray *cityIDS;
    NSMutableArray *sortedCitiIDs;
    NSMutableArray *citySelectedFlags;
    NSMutableDictionary *prefferedCities;
    
   WebRequestState iWRState;
}
@property (assign) BOOL isNewsSideCityPref;
@end

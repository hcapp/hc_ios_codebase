//
//  CityPreferenceViewController.m
//  HubCiti
//
//  Created by Keshava on 9/8/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

#import "CityPreferenceViewController.h"
#import "MainMenuViewController.h"
#import "HubCitiConstants.h"
#import "PreferredCategoriesScreen.h"
#import "MFSideMenu.h"
#import "ScrollTemplateViewController.h"
#import "CombinationViewController.h"
#import "SideNavigationResponse.h"
#import "AppDelegate.h"
#import "BookMarkGetRequest.h"
#import "SideViewController.h"

@interface CityPreferenceViewController ()
{
    NSMutableArray *tableArrayObject;
    NSMutableArray *tableDataObject;
    NSMutableArray *hubcitiCategoies;
    BOOL savePressed;
    
}
@property(nonatomic,strong) MainMenuResponse  *menuResponse;
@end

@implementation CityPreferenceViewController
{
    // int count;
}
@synthesize isNewsSideCityPref;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setAccessibilityLabel:@"City Preferences"];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    
    
    if([defaults boolForKey:@"CityPrefUpdated"]){
        citySelectedFlags = [[NSMutableArray alloc]initWithArray:[defaults valueForKey:@"CitySelectedFlags"]];
        cityIDS = [[NSMutableArray alloc]initWithArray:[defaults valueForKey:@"CityIDs"]];
        prefferedCities = [[NSMutableDictionary alloc]initWithDictionary:[defaults valueForKey:@"PrefferedCities"]];
        parsedCities = [[NSMutableArray alloc]initWithArray:[defaults valueForKey:@"ParsedCities"]];
        [self updateCityPreferences];
        [defaults setBool:FALSE forKey:@"CityPrefUpdated"];
        
    }
    else  if(![defaults boolForKey:@"CityPrefUpdated"]) {
        
        parsedCities = [[NSMutableArray alloc]initWithObjects:@"All",nil];
        
        cityIDS = [[NSMutableArray alloc]initWithObjects:@"NULL",nil];
        
        citySelectedFlags = [[NSMutableArray alloc]initWithObjects:@"1",nil];
        
        
        selectedCities = (int)[parsedCities count];
        [self requestCityPrefrences];
        
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    CustomizedNavController *cusNav =(CustomizedNavController *) self.navigationController;
   
    [cusNav hideHambergerButton: YES];
    [cusNav hideBackButton:YES];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [super viewWillAppear : YES];
    //Set the tilte for the screen
    sortedCitiIDs=[[NSMutableArray alloc] init];
    //    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, 320, 40)];
    //    titleLabel.textAlignment = NSTextAlignmentCenter;
    //    titleLabel.textColor = [UIColor convertToHexString:[defaults valueForKey:@"titleTxtColor"]];
    //    titleLabel.backgroundColor = [UIColor clearColor];
    //    titleLabel.numberOfLines = 1;
    //    titleLabel.font = [UIFont boldSystemFontOfSize:16];
    //    titleLabel.text = NSLocalizedString(@"City Preferences",@"City Preferences");
    //    titleLabel.adjustsFontSizeToFitWidth = YES;
    self.navigationItem.title = @"City Preferences";
    //ReleaseAndNilify(titleLabel);
    
    //add save (or) next barButonItem
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    flexibleSpace.width = 247.0;
    customItem = [[UIBarButtonItem alloc] init];
    customItem.style = UIBarButtonItemStylePlain;
    NSArray *items;
    
    //customize back button
    UIButton *backBtn = [UtilityManager customizeBackButton];
    [backBtn addTarget:self action:@selector(popBackToPreviousPage) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    
    self.navigationItem.hidesBackButton = YES;
    
    
    if ([SharedManager userInfoFromSignup] == NO){
        
        self.navigationItem.leftBarButtonItem = back;
        
        //        UIButton *btn = [UtilityManager customizeBarButtonItem:MAINMENU];
        //        [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
        //        UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
        //        self.navigationItem.rightBarButtonItem = mainPage;
        //        [ self.navigationItem.rightBarButtonItem setAccessibilityLabel:@"MainMenu"];
        //        ReleaseAndNilify(mainPage);
        // UIImage *image = [UIImage imageNamed:@"House-Icon25.png"];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:CGRectMake(0, 0, 30, 30)];
        SdImageView *homeImage= [[SdImageView alloc]initWithFrame:btn.frame];
        [homeImage loadImage:[defaults valueForKey:@"homeImgPath"]];
        
        [btn addSubview:homeImage];
        
        [btn addTarget:self action:@selector(returnToMainPage:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* mainPage = [[UIBarButtonItem alloc] initWithCustomView:btn];
        mainPage.style = UIBarButtonItemStylePlain;
        
        self.navigationItem.rightBarButtonItem = mainPage;
        ////[mainPage release];
        
        
        UIButton *saveBtn = [UtilityManager customizeBarButtonItem:@"Save"];
        [saveBtn setAccessibilityLabel:@"save"];
        [saveBtn addTarget:self action:@selector(savePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [customItem setCustomView:saveBtn];
        
        items = [NSArray arrayWithObjects:flexibleSpace, customItem, nil];
        
    }
    else {
        
        UIButton *nextBtn = [UtilityManager customizeBarButtonItem:@"Next"];
        [nextBtn addTarget:self action:@selector(navigateToNextPage:) forControlEvents:UIControlEventTouchUpInside];
        
        [customItem setCustomView:nextBtn];
        
        self.navigationItem.leftBarButtonItem = nil;
        
        //Adding back bottom bar button
        UIButton *backBottomBtn = [UtilityManager customizeBarButtonItem:@"Back"];
        [backBottomBtn addTarget:self action:@selector(popBackFromBottom) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* backBottomBarBtn = [[UIBarButtonItem alloc] initWithCustomView:backBottomBtn];
        
        items = [NSArray arrayWithObjects:backBottomBarBtn, flexibleSpace, customItem, nil];
        ReleaseAndNilify(backBottomBarBtn);
        
    }
    
    ReleaseAndNilify(back);
    //  toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-44, SCREEN_WIDTH, 44)];
    // toolbar.barStyle = UIBarStyleBlackOpaque;
    [toolbar setBarTintColor:[UIColor convertToHexString:[defaults valueForKey:@"titleBkGrdColor"]]];
    [toolbar setItems:items animated:NO];
    ReleaseAndNilify(flexibleSpace);
    ReleaseAndNilify(customItem );
    
    
    
    /***** Logic to prevent View from expanding for iOS7 version *******/
    dispatch_async(dispatch_get_main_queue(), ^{
        if([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    });
    
    
    
    /************ send request to get the user details from server **************/
    
    
    
    
    //Create tableview
    cityPrefTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-110)];
    cityPrefTable.dataSource = self;
    cityPrefTable.delegate = self;
    [cityPrefTable setBackgroundColor:[UIColor convertToHexString:@"#E6E6E6"]];
    [cityPrefTable setSeparatorColor:[UIColor blackColor]];
    [self.view addSubview:cityPrefTable];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - mark Button controls
-(void)popBackToPreviousPage{
    
    
    [self.navigationController popViewControllerAnimated:NO];
    
}
-(void)popBackFromBottom{
    NSLog(@"%lu",(unsigned long)cityIDS.count);
    NSLog(@"%lu",(unsigned long)citySelectedFlags.count);
    NSLog(@"%lu",(unsigned long)prefferedCities.count);
    NSLog(@"%lu",(unsigned long)parsedCities.count);
    [defaults setBool:TRUE forKey:@"CityPrefUpdated"];
    [defaults setObject:citySelectedFlags forKey:@"CitySelectedFlags"];
    [defaults setObject:cityIDS forKey:@"CityIDs"];
    [defaults setObject:prefferedCities forKey:@"PrefferedCities"];
    [defaults setObject:parsedCities forKey:@"ParsedCities"];
    NSLog(@"%@",[defaults valueForKey:@"CitySelectedFlags"]);
    NSLog(@"%@",[defaults valueForKey:@"CityIDs"]);
    NSLog(@"%@",[defaults valueForKey:@"PrefferedCities"]);
    NSLog(@"%@",[defaults valueForKey:@"ParsedCities"]);
    
    [self updateCityPreferences];
    [SharedManager setUserInfoFromSignup:YES];
    [self.navigationController popViewControllerAnimated:NO];
    //    NSLog(@"%@",self.navigationController.viewControllers);
    //   [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3] animated:NO];
    
    
}



-(void)returnToMainPage:(id)sender
{
    if([defaults boolForKey:@"newsTemplateExist"]){
        [defaults setValue:@"1" forKey:KEY_MENULEVEL];
        
        // [NewsUtility pushViewFromHamberger:self];
        if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Combination News Template"]){
            [UtilityManager popBackToViewController:[CombinationViewController class] inNavigationController:self.navigationController];
            
        }
        else if ([[defaults valueForKey:@"centerViewController"] isEqualToString:@"Scrolling News Template"]){
            [UtilityManager popBackToViewController:[ScrollTemplateViewController class] inNavigationController:self.navigationController];
        }
        else{
            [UtilityManager popBackToViewController:[BlockViewController class] inNavigationController:self.navigationController];
        }
       
        
        
    }
    else
    {
        SideViewController *leftMenuViewController;
        if(IPAD){
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
        }
        else{
            leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
            
        }
        leftMenuViewController.isNewsTemplate = FALSE;
        [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
        [defaults setValue:@"1" forKey:KEY_MENULEVEL];
        [UtilityManager popBackToViewController:[MainMenuViewController class] inNavigationController:self.navigationController];
    }
}

-(void)savePressed:(id)sender
{
    [HubCitiAppDelegate showActivityIndicator];
    savePressed = TRUE;
    [self updateCityPreferences];
}

-(void)navigateToNextPage:(id)sender
{
    [SharedManager setUserInfoFromSignup:YES];
    [self updateCityPreferences];
    PreferredCategoriesScreen *preferredCategories = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:preferredCategories animated:NO];
    //[preferredCategories release];
}

#pragma - mark Table view delegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        return 44.0;
    }
    else
    {
        return 70.0;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [parsedCities count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellCityPrefList";
    
    UITableViewCell *cell ;
    //    if (cell == nil)
    //    {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    cell.backgroundColor =  [UIColor clearColor];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    //}
    
    
    //    else
    //    {
    //        NSArray *arr = [cell.contentView subviews];
    //        for(int i=0; i<[arr count]; i++)
    //        {
    //            UIView *view = [arr objectAtIndex:i];
    //            [view removeFromSuperview];
    //        }
    //
    //    }
    UILabel *lblCity;
    if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
        lblCity= [[UILabel alloc]initWithFrame:CGRectMake(40, 13, SCREEN_WIDTH - 20, 20)];
        [lblCity setFont:[UIFont systemFontOfSize:13.0]];
    }
    else
    {
        lblCity= [[UILabel alloc]initWithFrame:CGRectMake(70, 20, SCREEN_WIDTH - 20, 30)];
        [lblCity setFont:[UIFont systemFontOfSize:18.0]];
    }
    
    [lblCity setText:[parsedCities objectAtIndex:indexPath.row]];
    [lblCity setTextColor:[UIColor blackColor]];
    [lblCity setBackgroundColor:[UIColor clearColor]];
    
    [cell.contentView addSubview:lblCity];
    //[lblCity release];
    
    if ([[prefferedCities valueForKey:[cityIDS objectAtIndex:indexPath.row]]isEqualToString:@"1"]) {
        checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOn.png"]];
        // checkButton = [[UIButton alloc]init];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            
            checkButton.frame = CGRectMake(10, 15, 20, 20);
        }
        else
        {
            
            checkButton.frame = CGRectMake(20, 20, 30, 30);
        }
        
        checkButton.backgroundColor =[UIColor clearColor];
    }
    else
    {
        checkButton =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checkboxOff.png"]];
        if (DEVICE_TYPE==UIUserInterfaceIdiomPhone) {
            
            checkButton.frame = CGRectMake(10, 15, 20, 20);
        }
        else
        {
            checkButton.frame = CGRectMake(20, 20, 30, 30);
        }
        
        checkButton.backgroundColor =[UIColor clearColor];
        
    }
    
    [cell.contentView addSubview:checkButton];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self citySelected:indexPath.row];
    [tableView reloadData];
}

#pragma - mark Request methods

-(void)requestCityPrefrences
{
    iWRState = FetchCityPreferences;
    NSMutableString *requestStr = [[NSMutableString alloc] init];
    if ([SharedManager userInfoFromSignup] == YES)
    {
        [requestStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId>",[defaults valueForKey:KEY_HUBCITIID]];
    }
    else
    {
        [requestStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId><userId>%@</userId>",[defaults valueForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_USERID]];
    }
    [requestStr appendFormat:@"</UserDetails>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/getcitypref",BASE_URL];
    [ConnectionManager establishConnectionFor:requestStr base:urlString withDelegate:self];
    ReleaseAndNilify(requestStr);
    
}

-(void) updateCityPreferences
{
    //iWRState = UpdateCityPreferences;
    [defaults setValue:@"1" forKey:@"CityPreferenceUpdated"];
    fromBack = NO;
    ReleaseAndNilify(selectedSortOption);
    ReleaseAndNilify(selectedFilterOption);
    ReleaseAndNilify(selectedSortCityDic);
    ReleaseAndNilify(cashedResponse);
    ReleaseAndNilify(dateCreated);
    ReleaseAndNilify(typeId);
    ReleaseAndNilify(deptId);
    NSMutableString *cities=[[NSMutableString alloc]init];
    NSMutableString *sortCities=[[NSMutableString alloc]init];
    
    int idCount=0;
    
    
    for (int i=1; i<[parsedCities count]; i++) {
        
        if ([[prefferedCities valueForKey:[cityIDS objectAtIndex:i]]isEqualToString:@"1"]) {
            [cities appendFormat:@"%@,",[cityIDS objectAtIndex:i]];
            [sortedCitiIDs addObject:[cityIDS objectAtIndex:i]];
            idCount++;
        }
    }
    
    
    if ([cities hasSuffix:@","]) {
        [cities  setString:[cities substringToIndex:[cities length]-1]];
    }
    
    
    if(!([sortedCitiIDs count]>0)){
        
        for (int i=1; i<[cityIDS count]; i++) {
            
            [sortedCitiIDs addObject:[cityIDS objectAtIndex:i]];
            
        }
    }
    //changed for server caching
    
    [sortedCitiIDs sortUsingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
        return [str1 compare:str2 options:(NSNumericSearch)];
    }];
    for (int i=0; i<[sortedCitiIDs count]; i++) {
        [sortCities appendFormat:@"%@,",[sortedCitiIDs objectAtIndex:i]];
        
    }
    if ([sortCities hasSuffix:@","]) {
        [sortCities  setString:[sortCities substringToIndex:[sortCities length]-1]];
    }
    
    
    [defaults setObject:sortCities forKey:@"sortedCitiIDs"];
    
    
    NSMutableString *requestStr = [[NSMutableString alloc]init];
    
    if (idCount>0) {
        [requestStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId><userId>%@</userId><cityIds>%@</cityIds>",[defaults valueForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_USERID],cities];
    }
    else
    {
        [requestStr appendFormat:@"<UserDetails><hubCitiId>%@</hubCitiId><userId>%@</userId>",[defaults valueForKey:KEY_HUBCITIID],[defaults valueForKey:KEY_USERID]];
    }
    
    
    [requestStr appendFormat:@"</UserDetails>"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@firstuse/updatecitypref",BASE_URL];
    NSString *response = [ConnectionManager establishPostConnection:urlString withParam:requestStr];
    if(savePressed){
        [self performSelector:@selector(parseUpdate_response:) withObject:response afterDelay:0.5];
       
    }
    else
    {
        [self parse_UpdateCityPreferences:response];
    }
    ReleaseAndNilify(requestStr);
}
-(void) parseUpdate_response:(NSString *)response{
    [self parse_UpdateCityPreferences:response];
}
#pragma - mark Parse methods
-(void)responseData:(NSString *)response
{
    switch (iWRState) {
        case FetchCityPreferences:
            [self parse_FetchCityPreferences:response];
            break;
            
        default:
            break;
    }
}

-(void)parse_FetchCityPreferences : (NSString *)response
{
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:@"responseCode" parentElement:tbxml.rootXMLElement];
    if (responseCodeElement!=nil && [[TBXML textForElement:responseCodeElement]isEqualToString:@"10000"]){
        
        TBXMLElement *cityListElement = [TBXML childElementNamed:@"cityList" parentElement:tbxml.rootXMLElement];
        
        NSLog(@"%@",[TBXML textForElement:tbxml.rootXMLElement]);
        
        NSLog(@"%@",[TBXML textForElement:cityListElement]);
        
        TBXMLElement *cityDetailsElement = [TBXML childElementNamed:@"City" parentElement:cityListElement];
        
        while (cityDetailsElement!=nil) {
            
            TBXMLElement *cityIdElement = [TBXML childElementNamed:@"cityId" parentElement:cityDetailsElement];
            TBXMLElement *cityNameElement = [TBXML childElementNamed:@"cityName" parentElement:cityDetailsElement];
            TBXMLElement *isCityElement = [TBXML childElementNamed:@"isCityChecked" parentElement:cityDetailsElement];
            
            if (cityIdElement!=nil) {
                [cityIDS addObject: [TBXML textForElement:cityIdElement]];
                
            }
            
            if (cityNameElement) {
                [parsedCities addObject:[TBXML textForElement:cityNameElement]];
                
            }
            
            if (isCityElement) {
                [citySelectedFlags addObject:[TBXML textForElement:isCityElement]];
                if ([[TBXML textForElement:isCityElement]isEqualToString:@"0"]) {
                    [citySelectedFlags setObject:@"0" atIndexedSubscript:0];
                }
            }
            
            cityDetailsElement = [TBXML nextSiblingNamed:@"City" searchFromElement: cityDetailsElement];
        }
        prefferedCities=[[NSMutableDictionary alloc]initWithObjects:citySelectedFlags forKeys:cityIDS];
        selectedCities = (int)[parsedCities count];
        [cityPrefTable reloadData];
    }
    else
    {
        TBXMLElement *responseTextElement = [TBXML childElementNamed:@"responseText" parentElement:tbxml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:responseTextElement]];
        return;
    }
}

-(void)parse_UpdateCityPreferences : (NSString *)response
{
    DLog(@"response %@",response);
    TBXML *tbXml = [TBXML tbxmlWithXMLString:response];
    
    TBXMLElement *responseCodeElement = [TBXML childElementNamed:RESPONSECODE parentElement:tbXml.rootXMLElement];
    int responseCode = [[TBXML textForElement:responseCodeElement]intValue];
    
    //if success navigate to categories screen
    if (responseCode == 10000) {
        if([defaults valueForKey:@"CityPrefUpdated"]&&[SharedManager userInfoFromSignup] == YES){
            
        }
        else{
            if(savePressed){
                 savePressed = FALSE;
            if ([SharedManager userInfoFromSignup] == YES)
            {
                [SharedManager setUserInfoFromSignup:YES];
                PreferredCategoriesScreen *preferredCategories = [[PreferredCategoriesScreen alloc] initWithNibName:@"PreferredCategoriesScreen" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:preferredCategories animated:NO];
                //[preferredCategories release];
                
            }
            else
            {
                if (self.isNewsSideCityPref ){
                    [self getSideNav];
                }
                else if (!self.isNewsSideCityPref)
                {
                     [HubCitiAppDelegate removeActivityIndicator];
                    SideViewController *leftMenuViewController;
                    if(IPAD){
                        leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewControlleriPad" bundle:nil];
                    }
                    else{
                        leftMenuViewController = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
                        
                    }
                    leftMenuViewController.isNewsTemplate = FALSE;
                    [self.menuContainerViewController setLeftMenuViewController:leftMenuViewController];
                   
                    [UtilityManager showAlert:nil msg:NSLocalizedString(@"Saved Successfully",@"Saved Successfully")];
                   

                }
                
                else{
                    [HubCitiAppDelegate removeActivityIndicator];
                    [UtilityManager showAlert:nil msg:NSLocalizedString(@"Saved Successfully",@"Saved Successfully")];
                }
                
                
                // //[alert release];
                
            }
        }
        }
    }
    
    else if(responseCode == 10001)
    {
        TBXMLElement *msgElement = [TBXML childElementNamed:RESPONSETEXT parentElement:tbXml.rootXMLElement];
        [UtilityManager showAlert:nil msg:[TBXML textForElement:msgElement]];
        return;
        
    }
    
}


-(void) getSideNav{
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if([defaults valueForKey:KEY_USERID])
    {
        [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
        
    }
    if([defaults valueForKey:KEY_HUBCITIID])
    {
        [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
        
    }
    
    
    [parameters setValue:@"1" forKey:@"level"];
    [parameters setValue:@"0" forKey:@"linkId"];
    [parameters setValue:@"true" forKey:@"sideNaviPersonalizatn"];
    NSLog(@"%@",parameters);
    
    NSString *urlString =[NSString stringWithFormat:@"%@firstuse/v2/hubsidemenudisplay",BASE_URL];
     //  NSString *urlString = [NSString stringWithFormat:@"http://10.10.221.155:9990/HubCiti_Coupon/firstuse/hubsidemenudisplay"];
    NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    [self performSelector:@selector(nextRequestparse_getSideNav:) withObject:responseData afterDelay:0.5];
    
    
}

-(void) nextRequestparse_getSideNav: (id)responseData{
    [self parse_getSideNav:responseData];
    
    
}

-(void) parse_getSideNav:(id) responseObject{
    
    NSMutableArray *newsCategories =[[NSMutableArray alloc] init];
    hubcitiCategoies = [[NSMutableArray alloc] init];
    
    SideNavigationResponse *sideNavResponse = [[SideNavigationResponse alloc] init];
    [sideNavResponse setValuesForKeysWithDictionary:responseObject];
    
    if([sideNavResponse.responseCode isEqualToString:@"10000"]){
        if(sideNavResponse.listCatDetails){
            
            for(int i=0;i<sideNavResponse.listCatDetails.count;i++){
                
                SideNavigationList *sideNavList = [[SideNavigationList alloc] init];
                NSDictionary *dict =sideNavResponse.listCatDetails[i];
                [sideNavList setValuesForKeysWithDictionary:dict];
                
                
                if(sideNavList.listMainCat){
                    
                    for(int i=0;i<sideNavList.listMainCat.count;i++){
                        SideNavigationNewsCat *sideNavnewsCat = [[SideNavigationNewsCat alloc] init];
                        NSDictionary *dict = sideNavList.listMainCat[i];
                        [sideNavnewsCat setValuesForKeysWithDictionary:dict];
                        NSLog(@"string :%@", [sideNavnewsCat sortOrder]);
                        [newsCategories addObject:sideNavnewsCat];
                        
                    }
                }
                else{
                    for(int i=0;i<sideNavList.menuList.count;i++){
                        _menuResponse = [[MainMenuResponse alloc] init];
                        NSDictionary *dictRes = sideNavList.menuList[i];
                        [_menuResponse setValuesForKeysWithDictionary:dictRes];
                        for(int i=0;i<_menuResponse.arMItemList.count;i++){
                            MenuItemList *menuItemList = [[MenuItemList alloc] init];
                            NSDictionary *dictmenu = _menuResponse.arMItemList[i];
                            [menuItemList setValuesForKeysWithDictionary:dictmenu];
                            if(![menuItemList.linkTypeName isEqualToString:@"News"]){
                                [hubcitiCategoies addObject:menuItemList];
                            }
                            [NewsUtility saveMenuData : _menuResponse];
                        }
                        
                    }
                }
                
            }
            
        }
        
        //tableArray =[[NSMutableArray alloc] initWithObjects:newsCategory, hubcitiCategory,nil];
        tableArrayObject =[[NSMutableArray alloc] init];
        tableDataObject = [[NSMutableArray alloc] init];
        
        
//        int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
//        if(menulevel>1){
//            [tableDataObject addObject:@"Home"];
//            [tableArrayObject addObject:@"Home"];
//        }
        NSMutableArray *tableCopy =[[NSMutableArray alloc] init];
        for(int i=0;i<newsCategories.count;i++){
            
            [tableCopy addObject:[newsCategories objectAtIndex:i]];
            
        }
        for(int i=0;i<hubcitiCategoies.count;i++)
            
        {
            [tableCopy addObject:[hubcitiCategoies objectAtIndex:i]];
            
        }
        NSSortDescriptor *aSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES selector:@selector(localizedStandardCompare:)];
        [tableCopy sortUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]];
        for(int i=0;i<tableCopy.count;i++){
            [tableArrayObject addObject:[tableCopy objectAtIndex:i]];
        }
        
            
            for(int i=0;i<tableArrayObject.count;i++){
                
                if([[tableArrayObject objectAtIndex:i] isKindOfClass:[SideNavigationNewsCat class]]){
                    [tableDataObject addObject:[[tableArrayObject objectAtIndex:i] catName]];
                }
                else{
                    if([[[tableArrayObject objectAtIndex:i] mItemName] isEqualToString:@"Login/Logout"]){
                        if(userIdentifier == TRUE){
                            [tableDataObject addObject:@"Login/SignUp"];
                        }
                        
                        else{
                            [tableDataObject addObject:@"Logout"];
                            
                        }
                        
                    }
                    else
                        [tableDataObject addObject:[[tableArrayObject objectAtIndex:i] mItemName]];
                }
            }
        
        
        NSLog(@"%@",tableDataObject);
        
        
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        if([defaults valueForKey:KEY_USERID])
        {
            [parameters setValue:[defaults valueForKey:KEY_USERID] forKey:@"userId"];
            
        }
        if([defaults valueForKey:KEY_HUBCITIID])
        {
            [parameters setValue:[defaults valueForKey:KEY_HUBCITIID] forKey:@"hubCitiId"];
            
        }
        int menulevel = (int)[[defaults valueForKey:KEY_MENULEVEL] integerValue];
        if(menulevel>1){
            
            NSString *linkId= [[HubCitiAppDelegate getLinkIdArray]objectAtIndex:menulevel-1];
            [parameters setValue:linkId forKey:@"linkId"];
            NSString *level = [defaults valueForKey:KEY_MENULEVEL];
            [parameters setValue:level forKey:@"level"];
        }
        else{
            
            [parameters setValue:@"0" forKey:@"linkId"];
        }
        [parameters setValue:@"1" forKey:@"isBkMark"];
        NSString *urlString =[NSString stringWithFormat:@"%@band/getnewstopnavigationmenu",BASE_URL];
        NSString* response = [ConnectionManager establishPostConnectionforJsonData:urlString withParam:parameters];
        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
        id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        [self performSelector:@selector(nextRequestParse_getBookMarkData:) withObject:responseData afterDelay:0.5];
        
    }
    else{
        [HubCitiAppDelegate removeActivityIndicator];
        [UtilityManager showAlert:nil msg:NSLocalizedString(@"Saved Successfully",@"Saved Successfully")];
    }
    
}
-(void)nextRequestParse_getBookMarkData:(id)responseData{
    
    [self parse_getBookMarkData:responseData];
}
-(void) parse_getBookMarkData:(id)responseData{
    [HubCitiAppDelegate removeActivityIndicator];
    BookMarkGetRequest *bookMarkGetResponse =[[BookMarkGetRequest alloc] init];
    [bookMarkGetResponse setValuesForKeysWithDictionary:responseData];
    NSMutableArray *bookMarkReturnArray = [[NSMutableArray alloc] init];
    if([bookMarkGetResponse.responseCode isEqualToString:@"10000"]){
        
        if(bookMarkGetResponse.bookMarkList){
            
            for(int i=0;i<bookMarkGetResponse.bookMarkList.count;i++){
                
                BookMarkListDetail *bookMarkList = [[BookMarkListDetail alloc] init];
                NSDictionary *dictList = bookMarkGetResponse.bookMarkList[i];
                [bookMarkList setValuesForKeysWithDictionary:dictList];
                
                if(bookMarkList.listCatDetails){
                    
                    for(int j=0;j<bookMarkList.listCatDetails.count;j++){
                        
                        BookMarkListcatDetails *catDetail = [[BookMarkListcatDetails alloc] init];
                        NSDictionary *dictCat= bookMarkList.listCatDetails[j];
                        [catDetail setValuesForKeysWithDictionary:dictCat];
                        
                        [bookMarkReturnArray addObject:[catDetail parCatName]];
                        
                        
                    }
                }
                
            }
        }
        NSMutableArray *notiArray =[[NSMutableArray alloc] initWithObjects:bookMarkReturnArray,tableArrayObject,tableDataObject,hubcitiCategoies, nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationName" object:notiArray];
        [UtilityManager showAlert:nil msg:NSLocalizedString(@"Saved Successfully",@"Saved Successfully")];
    }
    else{
        [HubCitiAppDelegate removeActivityIndicator];
        [UtilityManager showAlert:nil msg:NSLocalizedString(@"Saved Successfully",@"Saved Successfully")];
        }
    
}


-(void)citySelected:(NSInteger)row
{
    
    if (row==0) {
        
        
        int j = (int)[parsedCities count];
        selectedCities=(int)[parsedCities count];
        [citySelectedFlags removeAllObjects];
        if ([[prefferedCities valueForKey:[cityIDS objectAtIndex:0]]isEqualToString:@"0"]) {
            for (int i=0; i<j; i++) {
                [citySelectedFlags insertObject:@"1" atIndex:i];
                [prefferedCities setObject:[citySelectedFlags objectAtIndex:i] forKey:[cityIDS objectAtIndex:i]];
            }
            //selectedCities=[parsedCities count];
        }
        else
        {
            selectedCities=1;
            for (int i=0; i<j; i++) {
                [citySelectedFlags insertObject:@"0" atIndex:i];
                [prefferedCities setObject:[citySelectedFlags objectAtIndex:i] forKey:[cityIDS objectAtIndex:i]];
            }
            
        }
    }
    else
    {
        if ([[prefferedCities valueForKey:[cityIDS objectAtIndex:row]]isEqualToString:@"1"])
        {
            [citySelectedFlags removeObjectAtIndex:row];
            [citySelectedFlags insertObject:@"0" atIndex:row];
            [prefferedCities setObject:[citySelectedFlags objectAtIndex:row] forKey:[cityIDS objectAtIndex:row]];
            selectedCities--;
            if (selectedCities!=[parsedCities count])
            {
                [citySelectedFlags insertObject:@"0" atIndex:0];
                [prefferedCities setObject:[citySelectedFlags objectAtIndex:0] forKey:[cityIDS objectAtIndex:0]];
            }
        }
        else
        {
            [citySelectedFlags removeObjectAtIndex:row];
            [citySelectedFlags insertObject:@"1" atIndex:row];
            [prefferedCities setObject:[citySelectedFlags objectAtIndex:row] forKey:[cityIDS objectAtIndex:row]];
            selectedCities++;
            if (selectedCities==[parsedCities count])
            {
                [citySelectedFlags insertObject:@"1" atIndex:0];
                [prefferedCities setObject:[citySelectedFlags objectAtIndex:0] forKey:[cityIDS objectAtIndex:0]];
            }
            
        }
    }
    
}


@end

//
//  RegionFindGSViewController.h
//  HubCiti
//
//  Created by Kitty on 09/09/14.
//  Copyright (c) 2014 ajit. All rights reserved.
//

BOOL isSubCat;
#import <UIKit/UIKit.h>

@interface RegionFindGSViewController :UIViewController<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    int screen_width,screen_height,cityFlag;
    int groupSelectionVal;
    int sortSelectionval;
    NSMutableArray* selectedCitiIds;
    UIImageView *checkButton;
    BOOL isExpanded,selectedFlag;
    UIView *view_picker;
    UIPickerView *dinningTypePickerView;
    UIToolbar *pickerToolbar;
    
     NSString *typeStr;
    
}



@property(nonatomic,strong)UITableView *tableGrpSort;

@property(nonatomic,strong)NSMutableArray *arrGroups;

@property(nonatomic,strong)NSMutableArray *arrSorting;

@property(nonatomic,strong)NSDictionary *citiesDic;

@property(nonatomic,strong)NSMutableDictionary *OptionsDic;

@property(nonatomic,strong)NSMutableArray *arrCities;

@property(nonatomic,strong)NSMutableArray *arrCitiId;

@property(nonatomic,strong)NSMutableArray *selectedCitiIds,*selectedFilterValueIds,*selectedFilterId;

@property(nonatomic,strong)NSMutableArray *arrDinningTypes;

@property(nonatomic,strong) NSString *typeStr,*catId;

@property(nonatomic,strong) NSString *srchKey;

@property (nonatomic, strong)NSMutableArray *filterCount,*filterId;

@end



